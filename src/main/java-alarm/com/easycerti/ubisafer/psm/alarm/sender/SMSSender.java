package com.easycerti.ubisafer.psm.alarm.sender;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.easycerti.ubisafer.psm.alarm.common.DBConnectionManager;

public class SMSSender implements AlarmSender {

	protected final Logger logger = LoggerFactory.getLogger(MessengerSender.class);
	DBConnectionManager db;
	public SMSSender(String driver, String url, String id, String pw) throws ClassNotFoundException, SQLException {
		if( driver == null || url == null || id == null || pw == null ) {
			throw new SQLException("sms db connection info is empty!!");
		}
		
		this.db = new DBConnectionManager(driver, url, id, pw);
	}
	
	@Override
	public void sendAlarm(String sender, String reciever, String title, String body, String link) {
		
		if( reciever == null || sender == null ) {
			return;
		}
		
		try {
			String sql = " INSERT INTO TBL_SUBMIT_QUEUE ( \n" + 
					" 	USR_ID \n" + 
					" 	, SMS_GB \n" + 
					" 	, USED_CD \n" + 
					" 	, RESERVED_FG \n" + 
					" 	, RESERVED_DTTM \n" + 
					" 	, RCV_PHN_ID \n" + 
					" 	, SND_PHN_ID \n" + 
					" 	, ASSIGN_CD \n" + 
					" 	, SND_MSG \n" + 
					" 	, CALLBACK_URL \n" + 
					" 	, CONTENT_CNT \n" + 
					" 	, SMS_STATUS \n" + 
					" ) VALUES ( \n" + 
					" 	'0000' \n" + 
					" 	, '1' \n" + 
					" 	, '00' \n" + 
					" 	, 'I' \n" + 
					" 	, DATE_FORMAT(now(), '%Y%m%d%H%i%s') \n" + 
					" 	, ? \n" + //rcv
					" 	, ? \n" + //snder
					" 	, '00' \n" + 
					" 	, ? \n" + //body 
					" 	, NULL \n" + 
					" 	, 0 \n" + 
					" 	, '0' \n" + 
					" ) ";
			
			Connection conn = this.db.getConn();
			PreparedStatement pstmt = conn.prepareStatement(sql);
			
			String[] recArr = reciever.split(",");
			
			for( String rec : recArr ) {
				int i = 1;
				pstmt.setString(i++, rec);
				pstmt.setString(i++, sender);
				pstmt.setString(i++, body);
				
				int executeUpdate = pstmt.executeUpdate();
			}
			
			this.db.closeManager(pstmt, conn);
			
		} catch (SQLException e) {
			logger.error("", e);
		}
		
		logger.debug("## SEND ALARM : SMS, {}", reciever);
	}

}
