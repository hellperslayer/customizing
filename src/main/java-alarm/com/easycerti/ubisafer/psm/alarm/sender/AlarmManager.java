package com.easycerti.ubisafer.psm.alarm.sender;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AlarmManager {

	private static final Logger logger = LoggerFactory.getLogger(AlarmManager.class);
	
	private static Map<String, AlarmSender> senderMap = new HashMap<>();
	
	public static void main(String[] args) {
		AlarmManager.init(
				  "oracle.jdbc.driver.OracleDriver"
				, "jdbc:oracle:thin:@192.1.1.97:1522/hjdb2"
				, "MSGUSR"
				, "msgusr#hjdb2"
				, "com.mysql.jdbc.Driver"//smsDriver
				, "jdbc:mysql://192.1.1.184:3306/dbo?characterEncoding=utf8"//smsUrl
				, "smsuser"//smsId
				, "smsuser#smsdb"//smsPw
				);
		//AlarmManager.sendMessage("MESSENGER", "개인정보통합모니터링시스템", "Z2019041", "알림 테스트", "본문입니다.", "http://192.1.1.185:8080/psm");
		//AlarmManager.sendMessage("MESSENGER,SMS", "개인정보통합모니터링시스템", "Z2019041,Z2019042", "알림 테스트", "본문입니다.", null);
		AlarmManager.sendMessage("MESSENGER,SMS", "개인정보통합모니터링시스템", "01053581158", "Z2019041", "01053581158", "알림 테스트", "본문입니다.", "http://192.1.1.184:8080/psm");
	}

	public static void init(
			String msDriver, String msUrl, String msId, String msPw
			, String smsDriver, String smsUrl, String smsId, String smsPw
	) {
		
		try {
			AlarmSender msgSender = new MessengerSender(msDriver, msUrl, msId, msPw);
			AlarmSender smsSender = new SMSSender(smsDriver, smsUrl, smsId, smsPw);
			
			senderMap.put("MESSENGER", msgSender);
			senderMap.put("SMS", smsSender);
		} catch (Exception e) {
			logger.error("", e);
		} 
	}
	
	public static void sendMessage(String alarmTypes, String sender, String senderPhone, String target, String targetPhone, String title, String body, String url ) {
		String[] alarmTypeArr = alarmTypes.split(",");
		
		for( String alarmType : alarmTypeArr ) {
			AlarmSender alarmSender = AlarmManager.senderMap.get(alarmType);
			
			if( "MESSENGER".equals(alarmType.toUpperCase()) ) {
				alarmSender.sendAlarm(sender, target, title, body, url);
			}
			else if( "SMS".equals(alarmType.toUpperCase()) ) {
				alarmSender.sendAlarm(senderPhone, targetPhone, title, body, url);
			}
			
		}
	}
}
