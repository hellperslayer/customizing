package com.easycerti.ubisafer.psm.alarm.sender;

public interface AlarmSender {
	public void sendAlarm(String sender, String target, String title, String body, String link);
}
