package com.easycerti.ubisafer.psm.alarm.sender;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.easycerti.ubisafer.psm.alarm.common.DBConnectionManager;

public class MessengerSender implements AlarmSender {
	protected final Logger logger = LoggerFactory.getLogger(MessengerSender.class);
	DBConnectionManager db;
	public MessengerSender(String driver, String url, String id, String pw) throws ClassNotFoundException, SQLException {
		if( driver == null || url == null || id == null || pw == null ) {
			throw new SQLException("messenger db connection info is empty!!");
		}
		
		this.db = new DBConnectionManager(driver, url, id, pw);
	}
	
	@Override
	public void sendAlarm(String sender, String reciever, String title, String body, String link) {
		try {
			String sql = "INSERT INTO t_notification(id, recipient, subject, content, url, sender, sendyn, senddate, enterdate) " + 
						 "VALUES(hibernate_sequence.nextval, ?, ?, ?, ?, ?, 'N', null, sysdate )";
			
			Connection conn = this.db.getConn();
			PreparedStatement pstmt = conn.prepareStatement(sql);
			
			String[] recArr = reciever.split(",");
			
			for( String rec : recArr ) {
				int i = 1;
				pstmt.setString(i++, rec);
				pstmt.setString(i++, title);
				pstmt.setString(i++, body);
				pstmt.setString(i++, link);
				pstmt.setString(i++, sender);
				
				int executeUpdate = pstmt.executeUpdate();
			}
			
			this.db.closeManager(pstmt, conn);
			
		} catch (SQLException e) {
			logger.error("", e);
		}
		
		logger.debug("## SEND ALARM : MESSENGER, {}", reciever);
	}

}
