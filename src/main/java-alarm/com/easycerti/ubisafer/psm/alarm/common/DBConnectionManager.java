package com.easycerti.ubisafer.psm.alarm.common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DBConnectionManager
{
  private final Logger logger = LoggerFactory.getLogger(getClass());
  String driver = "";
  String jdbc_url = "";
  String id = "";
  String pw = "";
  Connection conn = null;
  
	public DBConnectionManager(String driver, String jdbc_url, String id, String pw) throws ClassNotFoundException, SQLException {
		this.driver = driver;
		this.jdbc_url = jdbc_url;
		this.id = id;
		this.pw = pw;
		Class.forName(this.driver);
		this.conn = DriverManager.getConnection(this.jdbc_url, this.id, this.pw);
	}
  
	public Connection getConn() throws SQLException {
		if ((this.conn == null) || (this.conn.isClosed())) {
			this.conn = DriverManager.getConnection(this.jdbc_url, this.id, this.pw);
		}
		return this.conn;
	}
  
	public void closeConn() {
		try {
			if (this.conn != null) {
				this.conn.close();
			}
		} catch (Exception e) {
			this.logger.error("", e);
		}
	}
	
	public void closeManager(ResultSet rs, Statement pstmt) {
		try {
			if (rs != null) { rs.close(); }
			if (pstmt != null) { pstmt.close(); }
		} catch (Exception e) {
			this.logger.error("", e);
		}
	}
	
	public void closeManager(ResultSet rs, Statement pstmt, Connection conn) {
		closeManager(rs, pstmt);
		this.closeConn();
	}
	
	public void closeManager(Statement pstmt, Connection conn) {
		try {
			pstmt.close();
		} catch (SQLException e) {
			this.logger.error("", e);
		}
		this.closeConn();
	}
  
//	public int selectBizlogSeq() {
//		String sql = "select nextval('BIZ_LOG_SEQ') as biz_log_seq";
//		int seq = -1;
//
//		ResultSet rs = null;
//		try {
//			PreparedStatement pstmt = getConn().prepareStatement(sql);
//			rs = pstmt.executeQuery();
//			while (rs.next()) {
//				seq = rs.getInt("biz_log_seq");
//			}
//			rs.close();
//			pstmt.close();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return seq;
//	}
  
//	public int insertSample() {
//		String sql = "";
//		int ret = 0;
//		PreparedStatement preparedStmt = null;
//		try {
//			preparedStmt = getConn().prepareStatement(sql);
//			ret = preparedStmt.executeUpdate();
//			preparedStmt.close();
//		} catch (SQLException e) {
//			e.printStackTrace();
//			try {
//				preparedStmt.close();
//			} catch (SQLException e1) {
//				e1.printStackTrace();
//			}
//		}
//		return ret;
//	}
  
}
