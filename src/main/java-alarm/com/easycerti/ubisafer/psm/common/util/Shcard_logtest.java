package com.easycerti.ubisafer.psm.common.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;

import com.easycerti.eframe.psm.search.dao.ExtrtCondbyInqDao;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;
 
public class Shcard_logtest {
	
	@Autowired
	private static ExtrtCondbyInqDao extrtCondbyInqDao;
 
    public static void main(String[] args) {
    	
    	SimpleDateFormat format1 = new SimpleDateFormat ( "yyyyMMdd");
		SimpleDateFormat format2 = new SimpleDateFormat ( "HHmmss");
		SimpleDateFormat format3 = new SimpleDateFormat ( "yyyyMMddHH");
		SimpleDateFormat format4 = new SimpleDateFormat ( "HH");
				
		Date time = new Date();
				
		String time1 = format1.format(time);
		String time2 = format2.format(time);
		String time3 = format3.format(time);
		
		Calendar cal = Calendar.getInstance(); cal.setTime(time);
		cal.setTime(time);
		cal.add(Calendar.DATE, +1);
		
		SimpleDateFormat sdformat = new SimpleDateFormat("yyyyMMdd");
		
		String tomorrow = sdformat.format(cal.getTime());

		
		String fileName = "";
		
		//00 ~ 12 : 12H, nowDay
		//12 ~ 15 : 15H, nowDay
		//15 ~ 00 : 12H, now +1Day 
		if( 0 <= Integer.parseInt(format4.format(time)) && Integer.parseInt(format4.format(time)) < 12){ //0 ~ 12시
			fileName = "C:\\usr/local/PSM/summonLog/SC_PM_EVENT_REQ_12H."+time1;
		}else if( 12 <= Integer.parseInt(format4.format(time)) && Integer.parseInt(format4.format(time)) < 15) {
			fileName = "C:\\usr/local/PSM/summonLog/SC_PM_EVENT_REQ_15H."+time1;
		}else if( 15 <= Integer.parseInt(format4.format(time)) && Integer.parseInt(format4.format(time)) < 24) {
			fileName = "C:\\usr/local/PSM/summonLog/SC_PM_EVENT_REQ_12H."+tomorrow;
		}
    	
        //File file = new File("C:\\usr/local/PSM/summonLog/SC_PM_EVENT_REQ.20201216");
		File file = new File(fileName);
        String message ="";
        //message = "PM|PM-00211-202080-20200403-0000|111111|8888|본인의 가족 조회|222222|20200403|본인가족 조회사유 확인 요청|본인가족 조회사유 확인 요청 부탁드립니다.|20020414|YNe/OgCDta7eleXg6nM9/pyCdPzbgvVcwOH2sI%2BF9xg%3D|C";
        String str = "PM|PM-00274-202293-20200403-0000|111111|8888|업무시스템[XXXX] 임직원 조회(본인 제외)|222222|20200403|업무시스템[XXXX]  임직원 조회사유 확인요청|업무시스템[XXXX]  임직원 조회사유 확인요청 부탁드립니다|20020414|3oBfKM%2B0tytS9NFU9Gl0rJyCdPzbgvVcwOH2sI%2BF9xg%3D|C";
        int line_cnt = 0;
        String str2 = "TR000000000000000000";
		FileWriter fw = null;
		
		
		try {
			
			if (file.isFile()) { //파일이 있을 경우
				
				FileReader fr = new FileReader(file);
				BufferedReader br = new BufferedReader(fr);
				
				while (br.readLine() != null) {
					line_cnt++;
				}
				
				List<String> lines = java.nio.file.Files.readAllLines(file.toPath());
				lines.add(line_cnt-1, str);
				java.nio.file.Files.write(file.toPath(), lines);
				
				
		    }
		    else {	//파일이 없을 경우
		    	String msg;
		    	
		    	message = time1 + "ISM" + time2 + "\n";
		    	message += str + "\n";
		    	message += "TR000000000000000000\n";
		    	fw = new FileWriter(file);
		    	BufferedWriter bw = new BufferedWriter(fw);
		    	
		    	//기존 파일의 내용에 이어서 쓰려면 true를, 기존 내용을 없애고 새로 쓰려면 false를 지정한다.
		    	fw = new FileWriter(file, false);
				fw.write(message);
				fw.flush();
		    }

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fw != null)
					fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
    }
}


