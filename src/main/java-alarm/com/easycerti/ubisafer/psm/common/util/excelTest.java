package com.easycerti.ubisafer.psm.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
 
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
 
public class excelTest {
 
    public static void main(String[] args) {
        try {
            // 엑셀파일
            File file = new File("C:\\excelUpload/소명응답_업로드양식.xlsx");
 
            // 엑셀 파일 오픈
            XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(file));
            
            Cell cell = null;
            
            // 첫번재 sheet 내용 읽기
            for (Row row : wb.getSheetAt(0)) { 
                // 셋째줄부터..
                if (row.getRowNum() == 3) {
                	// 콘솔 출력
                    cell = row.createCell(0); cell.setCellValue("");	//소명코드
                    cell = row.createCell(1); cell.setCellValue("");	//응답자ID
                    cell = row.createCell(2); cell.setCellValue("");	//응답자 시스템명
                    cell = row.createCell(3); cell.setCellValue(""); //판정자
                    cell = row.createCell(4); cell.setCellValue(""); //판정자
                }
                
            }
            
            // 엑셀 파일 저장
            FileOutputStream fileOut = new FileOutputStream("C:\\excelUpload/소명응답_업로드양식2.xlsx");
            wb.write(fileOut);
        } catch (FileNotFoundException fe) {
            System.out.println("FileNotFoundException >> " + fe.toString());
        } catch (IOException ie) {
            System.out.println("IOException >> " + ie.toString());
        }
    }
}


