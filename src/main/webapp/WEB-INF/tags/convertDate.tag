<%@ tag language="java" pageEncoding="EUC-KR"%>
<%@ attribute name="value" %>
<%
StringBuffer sb = new StringBuffer(value);
if (value.length() == 6) {
	sb.insert(4, "-");
} else if (value.length() == 8) {
	sb.insert(4, "-");
	sb.insert(7, "-");
}
out.println(sb);
%>
