<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="rootPath" value="${pageContext.servletContext.contextPath}" scope="application"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=9" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>UBI SAFER-PSM</title>

<link href="${pageContext.servletContext.contextPath}/resources/css/common/reset.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.servletContext.contextPath}/resources/css/login/login.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.servletContext.contextPath}/resources/js/common/jquery-1.8.2.min.js" type="text/javascript" charset="UTF-8"></script>
<script src="${pageContext.servletContext.contextPath}/resources/js/common/ajax-util.js" type="text/javascript" charset="UTF-8"></script>
<script src="${pageContext.servletContext.contextPath}/resources/js/login/loginUser.js" type="text/javascript" charset="UTF-8"></script>

</head>
<body class="login_rwapper">

<!-- login -->
<div class="login_rwap">
	<div class="login">
		<h1 class="loginh1">UBI SAFER-PSM</h1>
		
		<div class="login_box">
			<h2 class="loginh2">개인정보보호 상시모니터링시스템</h2>

			<form class="login_form" method="POST">
				<!-- 접근자 타입 -->
				<p class="user_sel" style="font-weight: bold;">
					<input type="radio" name="userType" value="E" checked="checked" style="display:none;"/>
					PSM V3.0
				</p>
				
				<!-- ID / PW 입력 -->
				<div class="area">
					<input type="text" class="login_id" id="login_id" name="login_id" maxlength="" title="아이디" placeholder="아이디" />
					<!-- 사용자 로그인 시 비밀번호가 불필요 해서 주석처리 -->
						<span style="width:220px;border-radius: 5px;">
							<select id="system_seq" name="system_seq" style="width:200px;">
							<option value="">시스템 선택</option>
								<option value="01">unicorn</option>
								<option value="02">시스템2</option>														
							</select>			
						</span>
				</div>
				
				<!-- 로그인 버튼 -->
				<img src="${pageContext.servletContext.contextPath}/resources/image/login/btn_login.png" class="btn_login" id="btn_login"/>
			</form>
			
		</div> <!-- e:login_box -->
		
		<span style="MARGIN-LEFT: 55px; MARGIN-TOP: 40px">해당 제품은 Internet Explorer 9 이상버전에서 정상 동작합니다.</span>		<br>
				</div> <!-- e:login -->
    
	<div class="footer">
		
		<span>Copyright (C) 2015 <b>EASYCERTI</b>. All rights reserved.</span>	
		<span class="f_logo">대한민국 보안의 힘 (주)이지서티</span>
	</div> <!-- e: footer -->
	
</div><!-- e:login_rwap -->

</body>
</html>

<script type="text/javascript">
	var loginConfig = {
		"userLogin":"${rootPath}/loginUser.psm"
	};
</script>
