<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>비밀번호초기화</title>


<link
	href="${pageContext.servletContext.contextPath}/resources/css/pass-reset.css"
	rel="stylesheet" type="text/css" />
</head>
<link rel="shortcut icon" type="image/x-icon"
	href="${pageContext.servletContext.contextPath}/resources/image/common/favicon_H2.ico" />
<meta http-equiv="X-UA-Compatible" content="IE=9" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>UBI SAFER-PSM</title>

<link
	href="${pageContext.servletContext.contextPath}/resources/css/common/reset.css"
	rel="stylesheet" type="text/css" />

<c:if test="${chng_layout eq 0}">
	<link
		href="${pageContext.servletContext.contextPath}/resources/css/login/login_gun.css"
		rel="stylesheet" type="text/css" />
</c:if>
<c:if test="${chng_layout eq 1}">
	<link
		href="${pageContext.servletContext.contextPath}/resources/css/login/login.css"
		rel="stylesheet" type="text/css" />
</c:if>
<script
	src="${pageContext.servletContext.contextPath}/resources/js/common/jquery-1.8.2.min.js"
	type="text/javascript" charset="UTF-8"></script>
<script type="text/javascript"
	src="${pageContext.servletContext.contextPath}/resources/js/rsa/rsa.js"></script>
<script type="text/javascript"
	src="${pageContext.servletContext.contextPath}/resources/js/rsa/jsbn.js"></script>
<script type="text/javascript"
	src="${pageContext.servletContext.contextPath}/resources/js/rsa/prng4.js"></script>
<script type="text/javascript"
	src="${pageContext.servletContext.contextPath}/resources/js/rsa/rng.js"></script>

<script
	src="${pageContext.servletContext.contextPath}/resources/js/common/ajax-util.js"
	type="text/javascript" charset="UTF-8"></script>
<script
	src="${pageContext.servletContext.contextPath}/resources/js/login/reset.js"
	type="text/javascript" charset="UTF-8"></script>
<script
	src="${pageContext.servletContext.contextPath}/resources/js/common/common-prototype-util.js"
	type="text/javascript" charset="UTF-8"></script>
<link
	href="/psm/resources/assets/global/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="/psm/resources/assets/global/plugins/font-awesome/css/font-awesome_5.11.2.min.css"
	rel="stylesheet" type="text/css" />

<body>

	<div id="myModalPw" class="modal fade" tabindex="-1" aria-hidden="true">
		<div class="modal-dialog"
			style="position: relative; top: 20%; left: 0;">
			<div class="modal-content">
				<div class="modal-header" style="background-color: #32c5d2;">

					<span class="modal-title"><b style="color: white;">비밀번호
							변경</b></span>
				</div>
				<div class="modal-body">
					<div class="scroller">
						<div class="row">
							<div class="col-md-12">
								<form id="repasswordForm" method="post">
									<p>
										<b id="chkId">아이디 </b><input id="myId" type="text"
											class="col-md-12 form-control" autocomplete="off">
										<button type="button" id="chkIdBtn" class="btn btn-sm red btn-outline">확인</button>
									</p>
									<div id="infoTxt">
										
									</div>
									<p>
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" id="closeWin">
						<i class="fa fa-remove"> </i>창 닫기
					</button>

				</div>
			</div>
		</div>
	</div>

</body>
</html>