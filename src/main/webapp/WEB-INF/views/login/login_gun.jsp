<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="rootPath" value="${pageContext.servletContext.contextPath}" scope="application"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="shortcut icon" type="image/x-icon" href="${pageContext.servletContext.contextPath}/resources/image/common/favicon_H2.ico" />
<meta http-equiv="X-UA-Compatible" content="IE=9" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>UBI SAFER-PSM</title>

<link href="${pageContext.servletContext.contextPath}/resources/css/common/reset.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.servletContext.contextPath}/resources/css/common/style_popup.css" rel="stylesheet" type="text/css" />
<!-- 시군구 : 0 , 아카데미 : 1  -->
<c:if test="${chng_layout eq 0}">
	<link href="${pageContext.servletContext.contextPath}/resources/css/login/login_gun.css" rel="stylesheet" type="text/css" />
</c:if>
<c:if test="${chng_layout eq 1}">
	<link href="${pageContext.servletContext.contextPath}/resources/css/login/login.css" rel="stylesheet" type="text/css" />
</c:if>
<script src="${pageContext.servletContext.contextPath}/resources/js/common/jquery-1.8.2.min.js" type="text/javascript" charset="UTF-8"></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/rsa/rsa.js"></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/rsa/jsbn.js"></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/rsa/prng4.js"></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/rsa/rng.js"></script>

<script src="${pageContext.servletContext.contextPath}/resources/js/common/ajax-util.js" type="text/javascript" charset="UTF-8"></script>
<script src="${pageContext.servletContext.contextPath}/resources/js/login/login.js" type="text/javascript" charset="UTF-8"></script>
<script src="${pageContext.servletContext.contextPath}/resources/js/common/common-prototype-util.js" type="text/javascript" charset="UTF-8"></script>


<script>
 $(function(){
	 var limitTime = $('#limitTime').val();
	 var nowTime = $('#nowTime').val();
	 if(nowTime >= limitTime && limitTime.length !=0){
		 $('.popup_wrap2').show();
	 }
	 
	 var msg = "${msg}";
	 
	 if(msg!=null&&msg!=""){
		 alert(msg);
	 }
 });
</script>
</head>

<body class="login_rwapper">
<form id ="checkDate"></form>
<!-- login -->
<div class="login_rwap">
	<div class="login" style='position:absolute; z-index:5;'>
		<!-- 팝업 -->
		<h1 class="loginh1">UBI SAFER-PSM V3.0</h1>
		<div class="login_box">
			<h2 class="loginh2">
			<c:if test="${filename ne null }">
				<img src="resources/upload/${filename }" width="100" height="100" style="position: relative;top:-25px; left:140px;">
			</c:if>
			</h2>
			<form class="login_form">
				<input type="hidden" id="RSAModulus" value="${RSAModulus}"/>
        		<input type="hidden" id="RSAExponent" value="${RSAExponent}"/>
<!--         		<input type="hidden" id="USER_ID" name="USER_ID"> -->
<!--         		<input type="hidden" id="USER_PW" name="USER_PW"> -->

				<!-- 접근자 타입 -->
				<p class="user_sel" style="font-weight: bold;">
					<!-- <input type="radio" name="userType" value="E" />사용자  -->
					<input type="radio" name="userType" value="A" checked="checked" style="display:none;"/><!-- 관리자 -->
					UBI SAFER-PSM V4.0
				</p>
				
				<!-- ID / PW 입력 -->
				<div class="area">
					<input type="text" class="login_id" id="login_id" name="login_id" maxlength="" title="아이디" placeholder="아이디" />
					<input type="password" class="login_pw" id="login_pw" name="login_pw" maxlength="" title="비밀번호" placeholder="비밀번호" />
	<%-- 				<input type="text" value="${limitTime}"/>
					<input type = "text" value = "${nowTime}"/> --%>
				</div>
				
				<!-- 로그인 버튼 -->
				<img src="${pageContext.servletContext.contextPath}/resources/image/login/btn_login_gun.png" class="btn_login" id="btn_login"/>
			</form>
			
			<div class="footer">
				<span class="f_logo"><img src="${pageContext.servletContext.contextPath}/resources/image/login/logo_gun.png" class="logoimg"/></span> <br>
				<span style="MARGIN-LEFT: 55px; MARGIN-TOP: 40px">해당 제품은 Internet Explorer 9 이상버전에서 정상 동작합니다.</span>	<br>
				<span>Copyright (C) 2015 <b>EASYCERTI</b>. All rights reserved.</span>	
			</div> <!-- e: footer -->
		
		</div> <!-- e:login_box -->
		
	</div> <!-- e:login -->
    
	<div class="checkData" align="center" style="width: 100%; margin-top:220px; position:absolute; z-index:3;">
		<div class="popup_wrap2" align="center" style="display:none;">
		<!-- <div class="popup_wrap" align="center"> -->
			<div class="top_line left">
		        <h1 class="left">UBI SAFER-PSM</h1>
		        <span class="btn_close right" style="background:url(${pageContext.servletContext.contextPath}/resources/image/common/btn_close.png) left top no-repeat;">
		        	<!-- <a href="#" onclick="closeDate()">닫기</a> -->
		        </span>
		    </div> <!-- e:top_line -->
		
			    <div class="popup_area left" style="font-family:'굴림', Gulim, Arial, Helvetica, sans-serif;">
			    	<h2 class="repw-h2" align="left">
			    	<img id="repw2" src="${pageContext.servletContext.contextPath}/resources/image/common/title_repw.png" alt="유지보수 만료안내" title="유지보수 만료안내" align="left"/></h2>
			        <div class="con_repw left">
			            <div class="repw-text">
				           	<p align="left" style="font-size:17px;">정기점검 및 장애발생시 신속한 조치를 통해,안정적인 보안 서비스 운영을 위한 유지보수 서비스를 제공하겠습니다. </p><br/>
				           <p align="left" style="font-size:17px;font-weight:bold;">계약문의: TEL 02-865-5577</p>
				            <p class="repw-date f11" align="left" style="font-size:17px;">(152-742) 서울시 구로구 디지털로 33길 48, 9층 <br />
				            TEL 02-865-5577<br />
				            FAX : 02-6942-9999<br />
				            E-mail : easycerti@easycerti.com<br />
				            <br />
				            Copyright &copy; <a href="http://www.easycerti.com" target="_blank">easycerti</a>. All rights reserved.</p> 
			            </div>
			        </div>
			</div> <!-- e:popup_area -->
	
		</div> <!-- e:popup_wrap -->
	
	
	</div>
	
	
	
	
	<!-- rePassword div-->
	<div class="rePasswordDiv" align="center" style="width: 100%; height: 776px; margin-top:120px; position:absolute; z-index:1;">
		<div class="popup_wrap" align="center" style="display:none;">
		<!-- <div class="popup_wrap" align="center"> -->
			<div class="top_line left">
		        <h1 class="left">UBI SAFER-PSM</h1>
		        <span id="close_btn" class="btn_close right" style="background:url(${pageContext.servletContext.contextPath}/resources/image/common/btn_close.png) left top no-repeat;">
		        	<%-- <a href="#" onclick="rePasswordPass('${userSession.admin_user_id}')">닫기</a> --%>
		        </span>
		    </div> <!-- e:top_line -->
		    
		    <form method="post" id="rePasswordForm">
		    	<input type="hidden" name="isInitLogin" value="N" />
			    <div class="popup_area left" style="font-family:'굴림', Gulim, Arial, Helvetica, sans-serif;">
			    	<h2 class="repw-h2" align="left">
			    	<img src="${pageContext.servletContext.contextPath}/resources/image/common/title_repw.gif" alt="비밀번호 변경 안내" title="비밀번호 변경 안내" align="left"/></h2>
			        <div class="pop_col2 con_repw left">
			            <div class="repw-text">
				           	<p align="left">비밀번호의 주기적인 변경은 회원님의 정보를 안전하게 지킬 수 있는 가장 간단하고, 빠른 방법입니다. <br/>UBI SAFER-PSM은 3개월에 한 번 비밀번호 변경을 권장합니다.</p><br/>
				            <p align="left">* 비밀번호 분실 시 담당 업체 유지보수팀에 문의 하세요!</p>
				            <p class="repw-call f11" align="left">(152-742) 서울시 구로구 디지털로 33길 48, 9층 <br />
				            TEL : 02-865-5577<br />
				            FAX : 02-6942-9999<br />
				            E-mail : easycerti@easycerti.com<br />
				            <br />
				            Copyright &copy; <a href="http://www.easycerti.com" target="_blank">easycerti</a>. All rights reserved.</p> 
			            </div>
			        </div>
			        <div class="pop_col2 b_line right">
				        <div class="con_repw table_repw"  align="left">
				            <label style="background:url(${pageContext.servletContext.contextPath}/resources/image/common/option_bullet.gif) left 45% no-repeat;">
				            	현재 비밀번호
				            </label> 
				            <input type="password" name="currentPasswd"/>
				            <label style="background:url(${pageContext.servletContext.contextPath}/resources/image/common/option_bullet.gif) left 45% no-repeat;">
				            	새 비밀번호
				            </label> 
			            	<input type="password" name="newPasswd" />
							<p class="f11 recoment" align="left">
								비밀번호는 9 ~ 20자 사이의 문자, 숫자, 특수문자(!@#$%&*) 조합으로 구성되어야합니다. (공백문자 제외)" <br /> 
		            		</p>
				            <label style="background:url(${pageContext.servletContext.contextPath}/resources/image/common/option_bullet.gif) left 45% no-repeat;">
				            	새 비밀번호 확인
				            </label> 
			            	<input type="password" name="re_newPasswd" />
				            <div class="btn_repw center">
				            	<a class="btn blue" href="#" onclick="passwdChange();">비밀번호 변경하기</a>
				                <%-- <a class="btn gray" id="afterChange_btn" href="#" onclick="afterChange('${userSession.admin_user_id}');">다음에 변경하기</a> --%> 
				            </div>
				        </div>
			        </div>
			    </div> <!-- e:popup_area -->
		    </form>
		    <input type="hidden" id ="nowTime" value = "${nowTime}">
		    <input type="hidden" id ="limitTime" value = "${limitTime}">
		</div> <!-- e:popup_wrap -->
	</div><!-- rePassword div-->
</div><!-- e:login_rwap -->
</body>
</html>
