<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="rootPath" value="${pageContext.servletContext.contextPath}"
	scope="application" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=8" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>UBI SAFER-PSM</title>

<link rel="shortcut icon" type="image/x-icon" href="${rootPath}/resources/image/common/favicon_H2.ico" />
<link href="${pageContext.servletContext.contextPath}/resources/css/common/common.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" charset="UTF-8">
 		rootPath = '${rootPath}';	
 		
 		function showDetailText(){
 			document.getElementById('detailText').style.display='block';
 		}
 </script>
<%@include file="/WEB-INF/views/common/include/config.jsp"%>
<style type="text/css">
body { margin-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; }

#center { position:absolute; top:50%; left:40%; width:700px; height:500px; overflow:hidden; margin-top:-150px; margin-left:-100px;}

</style>
</head>

<body id="Error_style" class="page-header-fixed page-content-white page-sidebar-closed">
	<div id = "center">
	<div class = "number font-green"> <h1> ${title} </h1> </div>
		<table style="width:500px;">
					<tr>
						<td>
							CODE : ${statusCode}
						</td>
					</tr>
					<tr>
						<td>
							MESSAGE : ${message}
						</td>
					</tr>
					<tr>
						<td>
							<c:if test="${statusCode != 'UPC102V' && statusCode != 'UPC103V' && statusCode != 'UPC116V' }">
								<input type="button" value="상세보기" onclick="showDetailText()" />
								<textarea id="detailText" style="display:none;" rows="20" cols="100">${detailMessage}</textarea>
							</c:if>
						</td>
					</tr>
				</table>
		<c:set value="<%=session.getAttribute(\"error_link_login\") %>" var="errorLinkLogin"/>
		<c:choose>
			<c:when test="${errorLinkLogin eq 'Y' }">
			<br> 로그인 화면으로 이동하세요.
			<br> <a onclick = "logout()"> 로그인 페이지 </a>		
			</c:when>
			<c:otherwise>
<!-- 			<br> <a onclick = "window.open('','_self',''); window.close();"> 닫기 </a> -->
			</c:otherwise>
		</c:choose>
</body>

</html>

<script type="text/javascript">
function logout(){
	$.ajax({
		type: 'POST',
		url: '${rootPath}/logout.html',
		success: function(data) {
			var log_message = "로그아웃 성공 : ${userSession.admin_user_id}"; 
			
			$.ajax({
				type: 'POST',
				url: rootPath + '/managerActHist/add.html',
				data: {
					menu_id : null,
					log_message : log_message,
					log_action : 'SELECT'
				}
			});
			
			location.href = rootPath + "/loginView.html";
		},
		error: function() {
			var log_message = "로그아웃 실패"; 
			
			$.ajax({
				type: 'POST',
				url: rootPath + '/managerActHist/add.html',
				data: {
					menu_id : null,
					log_message : log_message,
					log_action : 'SELECT'
				}
			});
		}
	});
}
</script>