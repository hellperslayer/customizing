<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="rootPath" value="${pageContext.servletContext.contextPath}"
	scope="application" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>UBI SAFER-PSM</title>
<link rel="shortcut icon" type="image/x-icon" href="${rootPath}/resources/image/common/favicon_H2.ico" />
<script type="text/javascript" charset="UTF-8">
	rootPath = '${rootPath}';
	contextPath = '${pageContext.servletContext.contextPath}';
</script>
<%@include file="/WEB-INF/views/common/include/config.jsp"%>
<style type="text/css">
body { margin-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; }

#center { position:absolute; top:50%; left:40%; width:300px; height:200px; overflow:hidden; margin-top:-150px; margin-left:-100px;}

</style>
</head>

<body id="Error_style" class="page-header-fixed page-content-white page-sidebar-closed">
	<div id = "center">
	<div class = "number font-green"> <h1> Duplication Login </h1> </div>
		다른 IP로 접속하여 세션이 만료되었습니다.
		<c:set value="<%=session.getAttribute(\"error_link_login\") %>" var="errorLinkLogin"/>
		<c:choose>
			<c:when test="${errorLinkLogin eq 'Y' }">
			<br> 로그인 화면으로 이동하세요.
			<br> <a onclick = "logout()"> 로그인 페이지 </a>		
			</c:when>
			<c:otherwise>
<!-- 			<br> <a onclick = "window.open('','_self',''); window.close();"> 닫기 </a> -->
			</c:otherwise>
		</c:choose>
</body>

</html>

<script type="text/javascript">
function logout(){
	$.ajax({
		type: 'POST',
		url: '${rootPath}/logout.html',
		success: function(data) {
			/* var log_message = "로그아웃 성공 : ${userSession.admin_user_id}"; 
			
			$.ajax({
				type: 'POST',
				url: rootPath + '/managerActHist/add.html',
				data: {
					menu_id : null,
					log_message : log_message,
					log_action : 'SELECT'
				}
			}); */
			
			location.href = rootPath + "/loginView.html";
		},
		error: function() {
			var log_message = "로그아웃 실패"; 
			
			$.ajax({
				type: 'POST',
				url: rootPath + '/managerActHist/add.html',
				data: {
					menu_id : null,
					log_message : log_message,
					log_action : 'SELECT'
				}
			});
		}
	});
}
</script>