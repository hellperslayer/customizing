<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="rootPath" value="${pageContext.servletContext.contextPath}"
	scope="application" />
	
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>UBI SAFER-PSM</title>
<link rel="shortcut icon" type="image/x-icon" href="${rootPath}/resources/image/common/favicon_H2.ico" />
<script type="text/javascript" charset="UTF-8">
	rootPath = '${rootPath}';
	contextPath = '${pageContext.servletContext.contextPath}';
</script>
<%@include file="/WEB-INF/views/common/include/config.jsp"%>
</head>


<!-- <body id="leftStyle" class="page-header-fixed page-content-white"> -->
<body id="Error_style" class="page-header-fixed page-content-white page-sidebar-closed">
	<div class="page-wrapper">
		<!-- BEGIN HEADER -->
		<%@include file="/WEB-INF/views/layout/top_error.jsp"%>
		<div class="clearfix"></div>
		<div class="page-container">
			<!-- BEGIN SIDEBAR -->
			<%@include file="/WEB-INF/views/layout/left_new.jsp"%>
			<div class="page-content-wrapper">
				<div class="page-content">
					<tiles:insertAttribute name="body" />
				</div>
			</div>
		</div>
		<%@include file="/WEB-INF/views/layout/bottom.jsp"%>
	</div>

	<div class="quick-nav-overlay"></div>

</body>

</html>