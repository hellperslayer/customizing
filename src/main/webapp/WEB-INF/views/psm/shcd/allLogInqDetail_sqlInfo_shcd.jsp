<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="sqlCt" value="${fn:length(sqlPrivacyInfo)}"></c:set>
<c:if test="${sqlCt >1}">
<div class="row">
	<div class="col-md-12">
			<div id="pagingframe" style="margin-bottom: 8px;">
			<ul class="pagination" align="center" style="display: inline;">
				<c:forEach var="no" begin="1" end="${sqlCt }" step="1">
					<c:choose>
						<c:when test="${allLogSql.logSqlPageNum == no}">
							<li class="active" style="display: inline-table; margin-right: -10px;"><a style="color: #337ab7; background-color: lightblue; border-color: #ddd;">${no}</a></li>&nbsp;
						</c:when>
						<c:when test="${sqlPrivacyInfo[no-1] =='N'}">
							<li style="display: inline-table; margin-right: -10px;"><a style="background-color: lightgray;" onclick="goPageallLogInqDetail_sql(${no})">${no }</a></li>&nbsp;
						</c:when>
						<c:otherwise>
							<li style="display: inline-table; margin-right: -10px;"><a onclick="goPageallLogInqDetail_sql(${no})">${no }</a></li>&nbsp;
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</ul>
		</div>
	</div>
</div>
</c:if>


<div class="portlet-title">
	<div class="caption" style="width: 100%;">
		<i class="icon-settings font-dark"></i> <span
			class="caption-subject font-dark sbold uppercase">SQL 정보</span>
<%-- 		<c:if test="${sqlCt > 0}"> --%>
<!-- 		<div style="display: inline-block;"> -->
<!-- 			<div class="btn-group"> -->
<%-- 				<a onclick="logSqlExcel()"><img src="${rootPath}/resources/image/icon/XLS_3.png"></a> --%>
<!-- 			</div> -->
<!-- 		</div> -->
<%-- 		</c:if> --%>
	</div>
</div>
<div class="portlet-body" style="padding-top: 0px;">
	<div class="row" style="margin-bottom: 20px;">
		<div class="col-md-12">
			<table class="table table-striped table-bordered table-checkable dataTable no-footer">
				<tbody>
					<tr>
						<th style="width: 20%; text-align: center; vertical-align: middle;">SQL
<!-- 							<div style="margin-top: 3px;"> -->
<%-- 							<c:choose> --%>
<%-- 								<c:when test="${logSqlInfo.collect_yn=='Y' }"><p class="btn btn-xs green-haze" onclick="sqlCollectYn(${logSqlInfo.sql_seq}, 'N')"> --%>
<%-- 									<i class="fa fa-remove"></i>&nbsp;&nbsp;제외</p></c:when> --%>
<%-- 								<c:when test="${logSqlInfo.collect_yn=='N' }"><p class="btn btn-xs red" onclick="sqlCollectYn(${logSqlInfo.sql_seq}, 'Y')"> --%>
<%-- 										<i class="fa fa-check"></i>&nbsp;&nbsp;&nbsp;복원</p></c:when> --%>
<%-- 							</c:choose> --%>
<!-- 							</div> -->
						</th>
						<td style="width: 80%; text-align: left;">
							<div style="overflow-y: auto; word-break: break-all; max-height: 78px;"><ctl:nullCv nullCheck="${logSqlInfo.sql }" /></div>
						</td>
					</tr>
					<tr>
						<th style="width: 20%; text-align: center; vertical-align: middle;">파라미터</th>
						<td style="width: 80%; text-align: left;">
							<div style="overflow-y: auto; word-break: break-all; max-height: 78px;">
							<c:choose>
								<c:when test="${empty logSqlInfo.paramsList}">-</c:when>
								<c:when test="${fn:length(logSqlInfo.paramsList) ==1 && logSqlInfo.paramsList[0] ==''}">※ 파라미터 없음</c:when>
								<c:otherwise><c:forEach items="${logSqlInfo.paramsList }" var="pl" varStatus="status">${status.count } : ${pl}<br/></c:forEach>
								</c:otherwise>
							</c:choose>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="portlet-title">
	<div class="caption" style="display: inline-block;">
		<i class="icon-settings font-dark"></i> <span
			class="caption-subject font-dark sbold uppercase">SQL 결과</span>
	</div>
</div>
<c:if test="${sqlCt>0 }">
<div class="portlet-body" style="padding-top: 0px;">
	<div class="row" style="margin-bottom: 20px;">
		<div class="col-md-12">
			<div style="overflow-x: auto;">
			<table class="table table-striped table-bordered table-hover table-checkable dataTable no-footer" style="table-layout: auto;">
				<thead>
					<tr>
						<c:forEach items="${logSqlInfo.columnsList}" var="cl" varStatus="status">
							<th style="min-width:100px; text-align: center; vertical-align: middle;">${cl }</th>
						</c:forEach>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${empty logSqlInfoResult }">
							<tr><td colspan="${fn:length(logSqlInfo.columnsList)}">데이터가 없습니다.</td></tr>
						</c:when>
						<c:otherwise>
							<c:forEach items="${logSqlInfoResult }" var="result">
							<tr>
								<c:forEach items="${result.resultList }" var="rl">
									<c:choose>
										<c:when test="${fn:length(rl) >26}"><td style="text-align: center; word-break:break-all;" title="${rl }">${fn:substring(rl,0,25)}...</td></c:when>
										<c:otherwise><td style="text-align: center; word-break:break-all;">${rl }</td></c:otherwise>
									</c:choose>
								</c:forEach>
							</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
					
				</tbody>
			</table>
			</div>
		</div>
	</div>
</div>
</c:if>
<c:if test="${sqlCt<=0 }">
<div class="portlet-body">
	<div class="row" style="margin-bottom: 20px;">
		<div class="col-md-12">
			<table
				class="table table-striped table-bordered table-checkable dataTable no-footer">
				<tbody>
					<tr><td>데이터가 없습니다.</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
</c:if>
<div class="row">
	<div class="col-md-12" align="center">
		<!-- 페이징 영역 -->
		<c:if test="${allLogSql.logSqlResult.total_count > 0}">
			<div id="pagingframe">
				<p>
					<ctl:paginator currentPage="${allLogSql.logSqlResult.page_num}" rowBlockCount="${allLogSql.logSqlResult.size }"
						totalRowCount="${allLogSql.logSqlResult.total_count}" pageName="allLogInqDetailResult" />
				</p>
			</div>
		</c:if>
	</div>
</div>
