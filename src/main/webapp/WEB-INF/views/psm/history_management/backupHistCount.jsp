<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html>

<%-- 추후 안정화가 되면 카운트 정보를 노출. 안정화가 되기 전까지는 정상백업 여부만 표기
<table class="table table-striped table-bordered table-hover table-checkable dataTable no-footer">
	<thead><tr>
		<th width="40%" style="vertical-align: middle; text-align: center;">시스템명</td>
		<th width="30%" style="vertical-align: middle; text-align: center;">처리량</td>
		<th width="30%" style="vertical-align: middle; text-align: center;">이용량</td>
	</tr></thead>
	<tbody>
		<c:choose>
			<c:when test="${empty backUpDetailCtList }"><td colspan="3" style="vertical-align: middle;">데이터가 없습니다.</td></c:when>
			<c:otherwise>
				<c:forEach items="${backUpDetailCtList }" var="bl">
				<tr>
					<td style="vertical-align: middle; text-align: center;">${bl.system_name }</td>
					<td style="vertical-align: middle; text-align: right;">${bl.cnt }</td>
					<td style="vertical-align: middle; text-align: right;">${bl.cnt2 }</td>
				</tr>
				</c:forEach>
			</c:otherwise>
		</c:choose>
		
	</tbody>
</table> --%>

<table class="table table-striped table-bordered table-hover table-checkable dataTable no-footer">
	<thead><tr>
		<th width="50%" style="vertical-align: middle; text-align: center;">대상</td>
		<th width="50%" style="vertical-align: middle; text-align: center;">상태</td>
	</tr></thead>
	<tbody>
		<tr>
			<td style="vertical-align: middle; text-align: center;">다운로드로그</td>
			<td style="vertical-align: middle; text-align: center;">정상 백업</td>
		</tr>
		<c:forEach items="${systemName }" var="s">
		<tr>
			<td style="vertical-align: middle; text-align: center;">${s}</td>
			<td style="vertical-align: middle; text-align: center;">정상 백업</td>
		</tr>
		</c:forEach>
	</tbody>
</table>