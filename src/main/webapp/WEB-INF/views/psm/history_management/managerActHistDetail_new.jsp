<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>

<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/history_management/managerActHistDetail.js" type="text/javascript" charset="UTF-8"></script>


<h1 class="page-title">
	${currentMenuName}
	<!--bold font-blue-hoki <small>basic bootstrap tables with various options and styles</small> -->
</h1>
<div class="portlet light portlet-fit bordered">
	<div class="portlet-body">
		<!-- BEGIN FORM-->
		<form id="managerActHistDetailForm" method="POST"
			class="form-horizontal form-bordered form-row-stripped">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<th style="width: 15%; text-align: center;">
							<label class="control-label" for="form_control_1">관리자ID</label>
						</th>
						<td style="width: 35%; vertical-align: middle;" class="form-group form-md-line-input">
							<c:out value="${managerActHistDetail.admin_user_id }" />
						</td>
						<th style="text-align: center;">
							<label class="control-label">관리자명</label>
						</th>
						<td style="width: 35%; vertical-align: middle;" class="form-group form-md-line-input">
							<c:out value="${managerActHistDetail.admin_user_name }" />
						</td>
					</tr>
					<tr>
						<th style="text-align: center;">
							<label class="control-label">IP</label>
						</th>
						<td style="vertical-align: middle;" class="form-group form-md-line-input">
							 <c:out value="${managerActHistDetail.ip_address }" />
						</td>
						<th style="text-align: center;">
							<label class="control-label">일시</label>
						</th>
						<td style="vertical-align: middle;" class="form-group form-md-line-input">
							 <c:out value="${managerActHistDetail.log_datetime }" />
						</td>
					</tr>
					<tr>
						<th style="text-align: center;">
							<label class="control-label">접근메뉴</label>
						</th>
						<c:if test="${managerActHistDetail.l_category != null && managerActHistDetail.l_category != '' }">
							<c:if test="${managerActHistDetail.m_category == null || managerActHistDetail.m_category == '' }">
								<td>${managerActHistDetail.l_category }</td>
							</c:if>
							<c:if test="${managerActHistDetail.m_category != null && managerActHistDetail.m_category != '' }">
								<c:if test="${managerActHistDetail.s_category == null || managerActHistDetail.s_category == '' }">
									<td>${managerActHistDetail.l_category }
									> ${managerActHistDetail.m_category }</td>
								</c:if>
								<c:if test="${managerActHistDetail.s_category != null && managerActHistDetail.s_category != '' }">
									<td>${managerActHistDetail.l_category }
									> ${managerActHistDetail.m_category } >
									${managerActHistDetail.s_category }</td>
								</c:if>
							</c:if>
						</c:if>
						<c:if
							test="${managerActHistDetail.l_category == null || managerActHistDetail.l_category == '' }">
							<td></td>
						</c:if>

						<th style="text-align: center;">
							<label class="control-label">로그종류</label>
						</th>
						<td style="vertical-align: middle;" class="form-group form-md-line-input">
							 <c:out value="${managerActHistDetail.log_action }" />
						</td>
					</tr>
					<tr>
						<th style="text-align: center;">
							<label class="control-label">로그메시지</label>
						</th>
						<td style="vertical-align: middle;" class="form-group form-md-line-input" colspan="3">
							<textarea class="form-control" readonly="readonly" rows="10" cols="60"><c:forEach var="array" items="${fn:split(managerActHistDetail.log_message, '/') }">${array }
</c:forEach></textarea>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
			
		<div class="form-actions">
			<div class="row">
				<div class="col-md-offset-2 col-md-10" align="right"
					style="padding-right: 30px;">
					<button type="button"
						class="btn btn-sm grey-mint btn-outline sbold uppercase"
						onclick="moveManagerActHistList()">
						<i class="fa fa-list"></i> 목록
					</button>
				</div>
			</div>
		</div>

		<form id="managerActHistListForm" method="POST">
			<!-- 메뉴 관련 input 시작 -->
			<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" />
			<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
			<input type="hidden" name="current_menu_id" value="${currentMenuId }" />
			<!-- 메뉴 관련 input 끝 -->
			
			<!-- 페이지 번호 -->
			<input type="hidden" name="page_num" value="${paramBean.page_num }" />
			
			<!-- 검색조건 관련 input 시작 -->
			<input type="hidden" name="admin_user_id_1" value="${paramBean.admin_user_id_1 }" />
			<input type="hidden" name="admin_user_name" value="${paramBean.admin_user_name }" />
			<input type="hidden" name="search_from" value="${paramBean.search_from }" />
			<input type="hidden" name="search_to" value="${paramBean.search_to }" />
			<input type="hidden" name="isSearch" value="${paramBean.isSearch }" />
			<!-- 검색조건 관련 input 끝 -->
		</form>
		<!-- END FORM-->
	</div>
</div>

<script type="text/javascript">

	var managerActHistDetailConfig = {
		"listUrl":"${rootPath}/managerActHist/list.html"
	};
	
</script>

