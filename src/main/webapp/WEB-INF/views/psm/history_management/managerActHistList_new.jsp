<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script
	src="${rootPath}/resources/js/psm/history_management/managerActHistList.js"
	type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/ezDatepicker.js"
	type="text/javascript" charset="UTF-8"></script>
<script
	src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js"
	type="text/javascript"></script>

<style type="text/css">
.input_date {
	background-color: #eef1f5;
	padding: 0.3em 0.5em;
	border: 1px solid #e4e4e4;
	height: 30px;
	font-size: 12px;
	color: #555;
}
</style>

<h1 class="page-title">${currentMenuName}</h1>
<div class="portlet light portlet-fit portlet-datatable bordered">
	<%-- <div class="portlet-title">
		<div class="caption">
			<i class="icon-settings font-dark"></i> <span
				class="caption-subject font-dark sbold uppercase">${currentMenuName}
				리스트</span>
		</div>
		<div class="actions">
			<div class="btn-group">
				<a class="btn red btn-outline btn-circle" onclick="excelManagerActHistList()"
					data-toggle="dropdown"> <i class="fa fa-share"></i> <span
					class="hidden-xs"> 엑셀 </span> 
				</a>
			</div>
		</div>
	</div> --%>
	<div class="portlet-body">
		<div class="table-container">

			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">

				<div class="dataTables_scroll" style="position: relative;">
					<div>
						<div class="col-md-6"
							style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
							<%-- <div class="portlet box grey-salsa">
								<div class="portlet-title" style="background-color: #2B3643;">
									<div class="caption">
										<img src="${rootPath}/resources/image/icon/search.png"> 검색 & 엑셀
									</div>
									<div class="tools">
										<a href="javascript:;" class="expand"></a>
									</div>
									<div style="float: right; padding: 12px 10px 8px;"><b>${textSearch }</b></div>
								</div>


								<!-- portlet-body S -->
								<form id="managerActHistListForm" method="POST"
									class="portlet-body" style="display:none;">
									<div class="portlet-body">
										<div class="table-container">
											<div id="datatable_ajax_2_wrapper"
												class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
												<div class="row" style="padding-bottom: 5px;">
													<div class="col-md-4">
														<div class="col-md-3">
															◎ 일시:
														</div>
														<div class="col-md-9" align="left"
															style="margin-left: 0px; padding-left: 0px; padding-right: 0px;">
															<span
																class="date date-picker margin-bottom-5 input-small"
																data-date-format="yyyy-mm-dd"> <input type="text"
																style="width: 80px;" class="input_date" readonly=""
																name="search_from" placeholder="From"
																value="${search.search_fromWithHyphen}"> <span
																class="">
																	<button class="btn btn-sm default" type="button">
																		<i class="fa fa-calendar"></i>
																	</button>
															</span>
															</span> <i class="sim">&sim;</i> <span
																class="date date-picker input-small"
																data-date-format="yyyy-mm-dd"> <input type="text"
																style="width: 80px;" class="input_date" readonly=""
																name="search_to" placeholder="To"
																value="${search.search_toWithHyphen}"> <span
																class="">
																	<button class="btn btn-sm default" type="button">
																		<i class="fa fa-calendar"></i>
																	</button>
															</span>
															</span>
														</div>
													</div>
													<div class="col-md-4">
														<div class="col-md-5">
															◎ 관리자ID:
														</div>
														<div class="col-md-7">
															<input class="form-control input-small" type=""
																name="admin_user_id_1" value="${search.admin_user_id_1}" />
														</div>
													</div>
													<div class="col-md-4">
														<div class="col-md-4">
															◎ 관리자명:
														</div>
														<div class="col-md-8">
															<input class="form-control input-small" type=""
																name="admin_user_name" value="${search.admin_user_name}" />
														</div>
													</div>
												</div>
												<div class="row">
													<br>
												</div>
												<div class="row">
													<div class="col-md-12" align="right"
														style="padding-right: 20px;">
														<button type="reset"
															class="btn btn-sm red table-group-action-submit"
															onclick="resetOptions(managerActHistConfig['listUrl'])">
															<i class="fa fa-remove"></i> 취소
														</button>
														<button class="btn btn-sm green table-group-action-submit"
															onclick="moveManagerActHistList()">
															<i class="fa fa-check"></i> 검색
														</button>&nbsp;&nbsp;
														<!-- <a class="btn red btn-outline btn-circle" onclick="excelManagerActHistList()"
															data-toggle="dropdown"> <i class="fa fa-share"></i> <span
															class="hidden-xs"> 엑셀 </span> 
														</a> -->
														<a onclick="excelManagerActHistList()"><img width="62px;" src="${rootPath}/resources/image/icon/XLS_1.png"></a>
													</div>
												</div>
											</div>
											<!-- portlet-boyd E -->


											<!-- portlet-body S -->
											<!--                          <div class="portlet-body" style="margin-top:-15px;"> -->
											<!--                          <br> -->
											<!--                          </div> -->
											<!-- portlet-boyd E -->
										</div>
									</div>

									<input type="hidden" name="main_menu_id"
										value="${paramBean.main_menu_id }" /> <input type="hidden"
										name="sub_menu_id" value="${paramBean.sub_menu_id }" /> <input
										type="hidden" name="current_menu_id" value="${currentMenuId}" />
									<input type="hidden" name="page_num" value="${search.page_num}" />
								</form>
								<!-- portlet-boyd E -->

							</div> --%>
							
							<div class="portlet box grey-salt  ">
                            <div class="portlet-title" style="background-color: #2B3643;">
                                <div class="caption">
                                    <i class="fa fa-search"></i>검색 & 엑셀</div>
                                <div class="tools">
					                 <a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
					             </div>
                            </div>
                            <div id="searchBar" class="portlet-body form" >
                                <div class="form-body" style="padding-left:10px; padding-right:10px;">
                                    <div class="form-group">
                                        <form id="managerActHistListForm" method="POST" class="mt-repeater form-horizontal">
                                            <div data-repeater-list="group-a">
                                            <br>
                                                <div class="row">
                                                    <!-- jQuery Repeater Container -->
                                                    <div class="col-md-3">
                                                        <label class="control-label">기간</label>
                                                        <div
															class="input-group date-picker input-daterange"
															data-date="10/11/2012" data-date-format="yyyy-mm-dd">
															<input type="text" class="form-control" id="search_fr"
																name="search_from"
																value="${search.search_fromWithHyphen}"> <span
																class="input-group-addon"> &sim; </span> <input type="text"
																class="form-control" id="search_to" name="search_to"
																value="${search.search_toWithHyphen}">
														</div> </div>
                                                    <div class="col-md-2">
                                                        <label class="control-label">관리자ID</label>
                                                        <input class="form-control" 
																name="admin_user_id_1" value="${search.admin_user_id_1}" /></div>
                                                    <div class="col-md-2">
                                                        <label class="control-label">관리자명</label>
                                                        <br/>
                                                        <input class="form-control" 
																name="admin_user_name" value="${search.admin_user_name}" />
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="control-label">로그메세지</label>
                                                        <br/>
                                                        <input class="form-control" 
																name="log_message" value="${search.log_message}" />
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <hr/>
                                            <div align="right">
	                                            <button type="reset"
													class="btn btn-sm red-mint btn-outline sbold uppercase"
													onclick="resetOptions(managerActHistConfig['listUrl'])">
													<i class="fa fa-remove"></i> <font>초기화
												</button>
												<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
													onclick="moveManagerActHistList()">
													<i class="fa fa-search"></i> 검색
												</button>&nbsp;&nbsp;
												<%-- <a class="btn red btn-outline btn-circle"
													onclick="excelAllLogInqList('${search.total_count}')"> 
													<i class="fa fa-share"></i> <span class="hidden-xs"> 엑셀 </span>
												</a> --%>
												<a onclick="excelManagerActHistList()"><img src="${rootPath}/resources/image/icon/XLS_3.png"></a>
											</div>
											
											<input type="hidden" name="main_menu_id"
												value="${paramBean.main_menu_id }" /> <input type="hidden"
												name="sub_menu_id" value="${paramBean.sub_menu_id }" /> <input
												type="hidden" name="current_menu_id" value="${currentMenuId}" />
											<input type="hidden" name="page_num" value="${search.page_num}" />
											<input type="hidden" name="isSearch" value="${paramBean.isSearch }" />
											<input type="hidden" name="log_auth_id" value="" />
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
						</div>
					</div>

					<table style="border-top: 1px solid #e7ecf1"
						class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
						style="position: absolute; top: 0px; left: 0px; width: 100%;">

						<thead>
							<tr role="row" class="heading" style="background-color: #c0bebe;">
								<th width="6%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: center; text-align: center;">
									No.</th>
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: center; text-align: center;">
									관리자ID</th>
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; text-align: center;">
									관리자명</th>
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; text-align: center;">
									IP</th>
								<th width="12%"
									style="border-bottom: 1px solid #e7ecf1; text-align: center;">
									일시</th>
								<th width="12%"
									style="border-bottom: 1px solid #e7ecf1; text-align: center;">
									접근 메뉴</th>
								<th width="*%"
									style="border-bottom: 1px solid #e7ecf1; text-align: center;">
									로그메세지</th>
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; text-align: center;">
									로그종류</th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${empty managerActHistList}">
									<tr>
										<td colspan="8" align="center">데이터가 없습니다.</td>
									</tr>
								</c:when>
								<c:otherwise>
									<c:set value="${search.total_count}" var="count" />
									<c:forEach items="${managerActHistList}" var="managerActHist"
										varStatus="status">
										<tr onclick="moveManagerActHistDetail('${managerActHist.log_auth_id}')" style="cursor: pointer;">
											<td style="text-align: center;">${count - search.page_num * search.size + search.size}</td>
											<td style="text-align: center;">${managerActHist.admin_user_id }</td>
											<td style="text-align: center;">${managerActHist.admin_user_name }</td>
											<td style="text-align: center;">${managerActHist.ip_address }</td>
											<td style="text-align: center;"><fmt:formatDate
													value="${managerActHist.log_datetime }"
													pattern="yyyy-MM-dd kk:mm:ss" /></td>
											<c:if test="${managerActHist.l_category != null && managerActHist.l_category != '' }">
												<c:if test="${managerActHist.m_category == null || managerActHist.m_category == '' }">
													<td style="text-align: center;">${managerActHist.l_category }</td>
												</c:if>
												<c:if test="${managerActHist.m_category != null && managerActHist.m_category != '' }">
													<c:if test="${managerActHist.s_category == null || managerActHist.s_category == '' }">
														<td style="text-align: center;">${managerActHist.l_category }
														> ${managerActHist.m_category }</td>
													</c:if>
													<c:if test="${managerActHist.s_category != null && managerActHist.s_category != '' }">
														<td style="text-align: center;">${managerActHist.l_category }
														> ${managerActHist.m_category } >
														${managerActHist.s_category }</td>
													</c:if>
												</c:if>
											</c:if>
											<c:if test="${managerActHist.l_category == null || managerActHist.l_category == '' }">
												<td></td>
											</c:if>
											<td style="padding-right: 20px;" class="t_left"
												title="${managerActHist.log_message }"><c:choose>
													<c:when
														test="${fn:length(managerActHist.log_message) > 35}">
												${fn:substring(managerActHist.log_message, 0, 35)} ...
											</c:when>
													<c:otherwise>
												${managerActHist.log_message}
											</c:otherwise>
												</c:choose></td>
											<td style="text-align: center;">${managerActHist.log_action}</td>
										</tr>
										<c:set var="count" value="${count - 1 }" />
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
					<div class="dataTables_processing DTS_Loading"
						style="display: none;">Please wait ...</div>
				</div>
				<div class="row" style="padding: 10px;">
					<c:if test="${search.total_count > 0}">
						<div class="page left" id="pagingframe" align="center">
							<p>
								<ctl:paginator currentPage="${search.page_num}"
									rowBlockCount="${search.size}"
									totalRowCount="${search.total_count}" />
							</p>
						</div>
					</c:if>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var managerActHistConfig = {
		"listUrl" : "${rootPath}/managerActHist/list.html",
		"detailUrl" : "${rootPath}/managerActHist/detail.html",
		"downloadUrl" : "${rootPath}/managerActHist/download.html"
	};
</script>