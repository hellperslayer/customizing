<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script
	src="${rootPath}/resources/js/psm/history_management/backupHistList.js"
	type="text/javascript" charset="UTF-8"></script>
<script
	src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js"
	type="text/javascript"></script>


<style type="text/css">
.input_date {
	background-color: #eef1f5;
	padding: 0.3em 0.5em;
	border: 1px solid #e4e4e4;
	height: 30px;
	font-size: 12px;
	color: #555;
}
</style>

<h1 class="page-title">${currentMenuName}</h1>
<div class="portlet light portlet-fit portlet-datatable bordered">
	<div class="portlet-body">
		<div class="table-container">
			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
				<div class="row">
						<div class="col-md-6"
							style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
							<div class="portlet box grey-salsa">
								<div class="portlet-title" style="background-color: #2B3643;">
									<div class="caption">
										<i class="fa fa-search"></i> 검색 & 엑셀
									</div>
									<div class="tools">
							                 <a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
					             	</div>
								</div>
								<div id="searchBar" class="portlet-body form" >
									<div class="form-body" style="padding-left:10px; padding-right:10px;">
										<div class="form-group">
											<form id="backupHistListForm" method="POST" class="mt-repeater form-horizontal">
													<div class="row">
													<div class="col-md-2">
														<label class="control-label">기간</label>
														<div class="input-group date-picker input-daterange"
															data-date="10/11/2012" data-date-format="yyyy-mm-dd">
															<input type="text" class="form-control" id="search_fr" name="search_from" value="${search.search_fromWithHyphen}"> <span
																class="input-group-addon"> &sim; </span>
															<input type="text" class="form-control" id="search_to" name="search_to" value="${search.search_toWithHyphen}">
														</div>
													</div>
													<%-- 
													<div class="col-md-2">
														<label class="control-label">대상</label> 
														<select name="log_type" class="form-control">
															<option value="" ${search.log_type == null ? 'selected="selected"' : ''}>----전체----</option>
															<option value="BA" ${search.log_type == 'BA' ? 'selected="selected"' : ''}>접속기록</option>
															<option value="DN" ${search.log_type == 'DN' ? 'selected="selected"' : ''}>다운로드접속기록</option>
														</select>
													</div> --%>
													
													</div>
													<hr/>
													<div align="right">
														<button type="reset" class="btn btn-sm red-mint btn-outline sbold uppercase"
															onclick="resetOptions(backupHistConfig['listUrl'])">
															<i class="fa fa-remove"></i> 초기화
														</button>
														<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
															onclick="moveBackupHistList()">
															<i class="fa fa-check"></i> 검색
														</button>&nbsp;&nbsp;
														<a onclick="excelBackupHistList()"><img src="${rootPath}/resources/image/icon/XLS_3.png"></a>
													</div>
												<input type="hidden" name="main_menu_id" value="${search.main_menu_id }" /> 
												<input type="hidden" name="sub_menu_id" value="${search.sub_menu_id }" /> 
												<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
												<input type="hidden" name="page_num" value="${search.page_num}" />
												<input type="hidden" name="isSearch" value="${search.isSearch }" />
											</form>
											</div>
									</div>
								</div>
							</div>
						</div>
					<div align="left" style="color: red;">※ 백업주기는 ${backupDate}일입니다.</div>
					<div>
						<table style="border-top: 1px solid #e7ecf1"
							class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
							style="position: absolute; top: 0px; left: 0px; width: 100%; ">
							<thead>
								<tr role="row" class="heading" style="background-color: #c0bebe;">
									<th width="10%" style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
										번호</th>
									<!-- <th width="10%" style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
										대상</th> -->
									<th width="20%" style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
										일시</th>
									<th width="20%" style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
										로그</th>
									<th width="10%" style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center; padding: 0px;">
										파일크기</th>
									<th width="10%" style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center; padding: 0px;">
										암호화 확인</th>
									<th width="10%" style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center; padding: 0px;">
										위변조 확인</th>
									<th width="10%" style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center; padding: 0px;">
										상세보기</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${empty backupHistList}">
										<tr>
											<td colspan="8" align="center">데이터가 없습니다.</td>
										</tr>
									</c:when>
									<c:otherwise>
										<c:set value="${search.total_count}" var="count" />
										<c:forEach items="${backupHistList}" var="backupHist" varStatus="status">
											<tr>
												<td style="text-align: center;">${count - search.page_num * 10 + 10 }</td>
												<%-- <td style="text-align: center;">
													<c:choose>
														<c:when test="${backupHist.log_type=='BA' }">접속기록</c:when>
														<c:otherwise>다운로드접속기록</c:otherwise>
													</c:choose>
												</td> --%>
												<td style="text-align: center;">
													<fmt:formatDate value="${backupHist.backup_datetime }" pattern="yyyy-MM-dd kk:mm:ss"/></td>
												<td style="text-align: center;"><ctl:nullCv nullCheck="${backupHist.origin_data }" /></td>
												<td style="text-align: center;"><ctl:nullCv nullCheck="${backupHist.log_size }" /></td>
												<td style="text-align: center;">
													<a class="btn btn-xs blue btn-outline sbold uppercase" onclick="checkFile('${backupHist.backup_log_id}')"><i class="fa fa-check"></i> 확인</a>
												</td>
												<td style="text-align: center;">
													<a class="btn btn-xs blue btn-outline sbold uppercase" onclick="checkIntegrity('${backupHist.backup_log_id}')"><i class="fa fa-check"></i> 확인</a>
												</td>
												<td style="text-align: center;"><fmt:formatDate value="${backupHist.backup_datetime }" pattern="yyyyMMdd" var="dt"/>
													<a class="btn btn-xs blue btn-outline sbold uppercase" onclick="checkDetail('${dt}','${backupHist.log_type}')"><i class="fa fa-check"></i> 상세보기</a>
												</td>
											</tr>
											<c:set var="count" value="${count - 1 }" />
										</c:forEach>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>
					</div>
					<form id="menuSearchForm" method="POST">
						<input type="hidden" name="main_menu_id" value="${search.main_menu_id }" /> 
						<input type="hidden" name="sub_menu_id" value="${search.sub_menu_id }" /> 
						<input type="hidden" name="system_seq" value="${search.system_seq }" />
					</form>
					<div class="dataTables_processing DTS_Loading"
						style="display: none;">Please wait ...</div>
				</div>
				<div class="row" style="padding: 10px;">
					<!-- 페이징 영역 -->
					<c:if test="${search.total_count > 0}">
						<div id="pagingframe" align="center">
							<p>
								<ctl:paginator currentPage="${search.page_num}"
									rowBlockCount="${search.size}"
									totalRowCount="${search.total_count}" />
							</p>
						</div>
					</c:if>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog" style="position: relative ;left: 0;">
		<!-- Modal content -->
		<div class="modal-content">
			<div class="modal-header" style="background-color: #32c5d2;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">
					<b style="font-size: 13pt;">상세보기</b>
				</h4>
			</div>
			<div class="modal-body" style="background-color: #F9FFFF;" id="countInfo">
				
			</div>
			<div class="modal-footer">
				<a class="btn btn-sm red" href="#" data-dismiss="modal"> <i class="fa fa-remove"></i> 닫기 </a>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">

	var backupHistConfig = {
		"listUrl":"${rootPath}/backupHist/list.html"
		,"downloadUrl":"${rootPath}/backupHist/download.html"
	};

</script>