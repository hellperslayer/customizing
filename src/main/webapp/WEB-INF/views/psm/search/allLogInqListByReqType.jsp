<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>

<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/search/allLogInqList.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>


<h1 class="page-title">
	${currentMenuName}
	<!-- <small>Log-list</small> -->
</h1>
<div class="portlet light portlet-fit portlet-datatable bordered">
	<%-- <div class="portlet-title">
		<div class="caption">
			<i class="icon-settings font-dark"></i> <span
				class="caption-subject font-dark sbold uppercase">접속기록 리스트</span>
		</div>
		<div class="actions">
			<div class="btn-group">
				<a class="btn red btn-outline btn-circle"
					onclick="excelAllLogInqList('${search.total_count}')"
					data-toggle="dropdown"> <i class="fa fa-share"></i> <span
					class="hidden-xs"> 엑셀 </span>
				</a>
			</div>
		</div>
	</div> --%>
	<div class="portlet-body">
		<div class="table-container">

			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
				<!-- <div class="row">
		            <div class="col-md-8 col-sm-12">
		            	<div class="dataTables_info" id="datatable_ajax_2_info" role="status" aria-live="polite">Showing 22 to 30 of 178 entries</div>
		            </div>
		            
	            </div> -->
				<!-- 				<div class="dataTables_scroll" style="position: relative;"> -->
				<div>
					<div class="col-md-6" style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
						<div class="portlet box grey-salt  ">
                            <div class="portlet-title" style="background-color: #2B3643;">
                                <div class="caption">
                                    <i class="fa fa-search"></i>검색 & 엑셀 </div>
                                <div class="tools">
                                	<a href="" class="collapse"> </a>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <div class="form-body" style="padding-left:10px; padding-right:10px;">
                                    <div class="form-group">
                                        <form id="listForm" method="POST" class="mt-repeater form-horizontal">
                                            <div data-repeater-list="group-a">
                                                <div data-repeater-item class="mt-repeater-item">
                                                    <!-- jQuery Repeater Container -->
                                                    <div class="mt-repeater-input">
                                                        <label class="control-label">일시</label>
                                                        <br/>
                                                        <div class="input-group input-medium date-picker input-daterange"
															data-date="10/11/2012" data-date-format="yyyy-mm-dd">
															<input type="text" class="form-control" id="search_fr"
																name="search_from" value="${search.search_fromWithHyphen}"> 
															<span class="input-group-addon"> &sim; </span> 
															<input type="text" class="form-control" id="search_to" 
																name="search_to" value="${search.search_toWithHyphen}">
														</div> 
													</div>
                                                    <div class="mt-repeater-input">
                                                        <label class="control-label">기간선택</label>
                                                        <br/>
                                                        <select class="form-control"
															id="daySelect" name="daySelect"
															onclick="javascript:initDaySelect();">
															<option value="" class="daySelect_first"
																<c:if test='${search.daySelect eq ""}'>selected="selected"</c:if>>기간선택</option>
															<option value="Day"
																<c:if test='${search.daySelect eq "Day"}'>selected="selected"</c:if>>오늘</option>
															<option value="WeekDay"
																<c:if test='${search.daySelect eq "WeekDay"}'>selected="selected"</c:if>>일주일</option>
															<option value="MonthDay"
																<c:if test='${search.daySelect  eq "MonthDay"}'>selected="selected"</c:if>>전달
																1일부터 ~ 오늘</option>
															<option value="YearDay"
																<c:if test='${search.daySelect  eq "YearDay"}'>selected="selected"</c:if>>올해
																1월1일부터 ~ 오늘</option>
															<option value="TotalMonthDay"
																<c:if test='${search.daySelect eq "TotalMonthDay"}'>selected="selected"</c:if>>전달
																전체</option>
														</select>
													</div>
													
                                                    <div class="mt-repeater-input">
                                                        <label class="control-label">개인정보유형</label>
                                                        <br/>
                                                        <select name="privacyType" class="form-control">
															<option value="" ${search.privacyType == '' ? 'selected="selected"' : ''}>----- 전 체 -----</option>
															<c:if test="${empty CACHE_RESULT_TYPE}">
																<option>데이터 없음</option>
															</c:if>
															<c:if test="${!empty CACHE_RESULT_TYPE}">
																<c:forEach items="${CACHE_RESULT_TYPE}" var="i">
																	<option value="${i.key}" ${i.key==search.privacyType ? "selected=selected" : "" }>${i.value}</option>
																</c:forEach>
															</c:if>
														</select>
                                                    </div>
                                                    <div class="mt-repeater-input">
                                                        <label class="control-label">소속</label>
                                                        <br/>
                                                        <input type="text" class="form-control" name="dept_name" value="${search.dept_name}" />
                                                    </div>
                                                    <div class="mt-repeater-input">
                                                        <label class="control-label">사용자ID</label>
                                                        <br/>
                                                        <input type="text" class="form-control" name="emp_user_id" value="${search.emp_user_id}" />
                                                    </div>
                                                    <div class="mt-repeater-input">
                                                        <label class="control-label">사용자명</label>
                                                        <br/>
                                                        <input type="text" class="form-control" name="emp_user_name" value="${search.emp_user_name}" />
                                                    </div>
                                                    <div class="mt-repeater-input">
                                                        <label class="control-label">사용자IP</label>
                                                        <br/>
                                                        <input type="text" class="form-control" name="user_ip" value="${search.user_ip}" />
                                                    </div>
                                                    <div class="mt-repeater-input">
                                                   	 	<label class="control-label">접근행위</label>
                                                        <br/>
                                                        <select name="req_type" class="form-control">
															<option value="" ${search.req_type == '' ? 'selected="selected"' : ''}>----- 전 체 -----</option>
															<c:if test="${empty CACHE_REQ_TYPE}">
																<option>데이터 없음</option>
															</c:if>
															<c:if test="${!empty CACHE_REQ_TYPE}">
																<c:forEach items="${CACHE_REQ_TYPE}" var="i">
																	<option value="${i.key}" ${i.key==search.req_type ? "selected=selected" : "" }>${i.value}</option>
																</c:forEach>
															</c:if>
														</select>
                                                    </div>
                                                    <div class="mt-repeater-input">
                                                   	 	<label class="control-label">대상</label>
                                                        <br/>
                                                        <select name="log_gubun" class="form-control">
															<option value="user" ${search.log_gubun == 'user' ? 'selected="selected"' : ''}>사용자</option>
															<option value="manager" ${search.log_gubun == 'manager' ? 'selected="selected"' : ''}>운영자</option>
														</select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div align="right">
	                                            <button type="reset"
													class="btn btn-sm red-mint btn-outline sbold uppercase"
													onclick="resetOptions(allLogInqByReqTypeConfig['listUrl'])">
													<i class="fa fa-remove"></i> 취소
												</button>
												<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
													onclick="moveallLogInqListByReqType()">
													<i class="fa fa-search"></i> 검색
												</button>&nbsp;&nbsp;
												<%-- <a class="btn red btn-outline btn-circle"
													onclick="excelAllLogInqList('${search.total_count}')"> 
													<i class="fa fa-share"></i> <span class="hidden-xs"> 엑셀 </span>
												</a> --%>
												<a onclick="excelAllLogInqListByReqType('${log_count}')"><img src="${rootPath}/resources/image/icon/XLS_3.png"></a>
											</div>
											
											<input type="hidden" name="main_menu_id" value="${search.main_menu_id }" /> 
											<input type="hidden" name="sub_menu_id" value="${search.sub_menu_id }" /> 
											<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
											<%-- <input type="hidden" name="system_seq" value="${search.system_seq }" /> --%> 
<%-- 											<input type="hidden" name="page_num" value="${search.page_num}" />  --%>
											<input type="hidden" name="menuNum" id="menuNum" value="${menuNum}" />
											<input type="hidden" name="menuCh" id="menuCh" value="${menuCh}" />
											<input type="hidden" name="menuId" id="menuId" value="${search.sub_menu_id}" /> 
											<input type="hidden" name="menutitle" id="menutitle" value="${menuCh}${menuNum}" />
											<input type="hidden" name="dept_id" value="" /> 
											<input type="hidden" name="isSearch" value="${search.isSearch }" />
											<input type="hidden" name = "result_type"  id = "result_type" value=""/>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>
				</div>
				<div>
					<table style="border-top: 1px solid #e7ecf1"
						class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
						style="position: absolute; top: 0px; left: 0px; width: 100%;">

						<thead>
							<tr role="row" class="heading" style="background-color: #c0bebe;">
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									rank</th>
								<th width="15%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									소속</th>
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									사용자ID</th>
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center; padding: 0em;">
									사용자명</th>
								<th width="15%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									사용자IP</th>
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									접근행위</th>
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center; padding: 0em;">
									로그 조회 건수</th>
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									개인정보유형</th>
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center; padding: 0em;">
									개인정보 조회 건수</th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${empty allLogInq}">
									<tr>
										<td colspan="9" align="center">데이터가 없습니다.</td>
									</tr>
								</c:when>
								<c:otherwise>
<%-- 									<c:set value="${allLogInqList.page_total_count}" var="count" /> --%>
									<c:forEach items="${allLogInq}" var="allLogInq">
										<tr>
											<td style="text-align: center;"><ctl:nullCv nullCheck="${allLogInq.data1}" /></td>
											<td style="text-align: center;"><ctl:nullCv nullCheck="${allLogInq.dept_name}" /></td>
											<td style="text-align: center;"><ctl:nullCv nullCheck="${allLogInq.emp_user_id}" /></td>
											<td style="text-align: center;"><ctl:nullCv nullCheck="${allLogInq.emp_user_name}" /></td>
											<td style="text-align: center;"><ctl:nullCv nullCheck="${allLogInq.user_ip}" /></td>
											<td style="text-align: center;">
												<c:forEach items="${CACHE_REQ_TYPE}" var="i" varStatus="status">
													<c:if test="${i.key == allLogInq.req_type}">
														<ctl:nullCv nullCheck="${i.value}" />
													</c:if>
												</c:forEach>
											</td>
											<td style="text-align: center;"><fmt:formatNumber value="${allLogInq.cnt }" pattern="#,###" /></td>
											<td style="text-align: center;"><ctl:nullCv nullCheck="${allLogInq.privacy_desc}" /></td>
											<td style="text-align: center;"><fmt:formatNumber value="${allLogInq.cnt2 }" pattern="#,###" /></td>
										</tr>
<%-- 										<c:set var="count" value="${count - 1 }" /> --%>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
				</div>

				<form id="menuSearchForm" method="POST">
					<input type="hidden" name="main_menu_id" value="${search.main_menu_id }" /> 
					<input type="hidden" name="sub_menu_id" value="${search.sub_menu_id }" /> 
					<input type="hidden" name="system_seq" value="${search.system_seq }" />
					<input type="hidden" name="emp_user_id" value="${search.emp_user_id}" /> 
					<input type="hidden" name="search_from" value="${search.search_from}" /> 
					<input type="hidden" name="search_to" value="${search.search_to}" /> 
					<input type="hidden" name="user_ip" value="${search.user_ip}" /> 
					<input type="hidden" name="daySelect" value="${search.daySelect}" />
					<input type="hidden" name="emp_user_name" value="${search.emp_user_name }" />
				</form>
				
				<div class="row" style="padding: 10px;">
				(${log_count} rows)
				</div>
				<%-- <div class="row" style="padding: 10px;">
					<!-- <div class="col-md-8 col-sm-12">
			            <div class="dataTables_info">Showing 22 to 30 of 178 entries
			            </div>
		            </div>
		            <div class="col-md-4 col-sm-12"></div> -->
					<!-- 페이징 영역 -->
					<c:if test="${search.total_count > 0}">
						<div id="pagingframe" align="center">
							<p>
								<ctl:paginator currentPage="${search.page_num}"
									rowBlockCount="${search.size}"
									totalRowCount="${search.total_count}" />
							</p>
						</div>
					</c:if>
				</div> --%>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	/* $('#search_fr, #search_to').datepicker({
	 format: "yyyy-mm-dd",
	 language: "kr",
	 todayHighlight: true
	 });

	 $('#search_fr').datepicker()
	 .on('changeDate', function(ev){
	 var date1 = $('#search_fr').val();
	 $('#search_to').datepicker('setStartDate', date1);
	 });
	 $('#search_to').datepicker()
	 .on('changeDate', function(ev){
	 var date2 = $('#search_to').val();
	 $('#search_fr').datepicker('setEndDate', date2);
	 });  */
	 
	var allLogInqByReqTypeConfig = {
		"listUrl" : "${rootPath}/allLogInq/listByReqType.html",
		"downloadUrl" : "${rootPath}/allLogInq/downloadByReqType.html"
	};
</script>