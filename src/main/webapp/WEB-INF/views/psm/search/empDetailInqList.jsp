<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="MENU00047" />
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/search/empDetailInqList.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>

<div class="contents left">
	<!-- 메뉴명 -->
	<h3 class="page_name">${currentMenuName}</h3>

	<form id="listForm" method="POST">
		<!-- 검색영역 -->
		<div class="option_area left">
			<div class="optgroup">
				<div class="col3 left">
					<span>
						<label><span class="option_name">기간</span></label>
						<input name="search_from" type="text" class="calender search_fr" value="${search.search_fromWithHyphen}" />
						<i class="sim">&sim;</i>
						<input name="search_to" type="text" class="calender search_to" value="${search.search_toWithHyphen}" />
					</span>
					<span>
						<label><span class="option_name">사번</span></label>
						<input type="text" name="emp_user_id" value="${search.emp_user_id }" />
					</span>
				</div>
				<div class="col3 left">
					<span>
						<label><span class="option_name">기간선택</span></label>
						<select id="daySelect" name="daySelect" onclick="javascript:initDaySelect();">
							<option value="" <c:if test='${search.daySelect eq ""}'>selected="selected"</c:if>>기간선택</option>
							<option value="Day" <c:if test='${search.daySelect eq "Day"}'>selected="selected"</c:if>>오늘</option>
							<option value="WeekDay" <c:if test='${search.daySelect eq "WeekDay"}'>selected="selected"</c:if>>일주일</option>
							<option value="MonthDay" <c:if test='${search.daySelect  eq "MonthDay"}'>selected="selected"</c:if>>전달 1일부터 ~ 오늘</option>
							<option value="YearDay" <c:if test='${search.daySelect  eq "YearDay"}'>selected="selected"</c:if>>올해 1월1일부터 ~ 오늘</option>
							<option value="TotalMonthDay" <c:if test='${search.daySelect eq "TotalMonthDay"}'>selected="selected"</c:if>>전달 전체</option>
						</select>
					</span>
					<span>
						<label><span class="option_name">직원</span></label>
						<input type="text" name="emp_user_name" value="${search.emp_user_name }" />
					</span>
				</div>
				<div class="col3 left">
					<span>
						<label><span class="option_name">위험도</span></label>
						<select name="dng_val">
							<option value="" ${search.dng_val == '' ? 'selected="selected"' : ''}> ----- 선 택 ----- </option>
							<c:if test="${empty CACHE_DANGER_STATUS}">
								<option>데이터 없음</option>
							</c:if>
							<c:if test="${!empty CACHE_DANGER_STATUS}">
								<c:forEach items="${CACHE_DANGER_STATUS}" var="i" varStatus="z">
									<option value="${i.key}"${i.key==search.dng_val ? "selected=selected" : "" } >${ i.value}</option>					
								</c:forEach>
							</c:if>
						</select>
					</span>
					<span>
						<label><span class="option_name">부서</span></label>
						<input type="text" name="dept_name" value="${search.dept_name }"/>
					</span>
				</div>
				
				<!-- 버튼 영역 -->
				<div class="option_btn left">
					<p class="right">
						<a class="btn gray" onclick="resetOptions(empDetailInqConfig['listUrl'])">취소</a>
						<a class="btn blue" onclick="moveempDetailInqList()">검색</a>
					</p>
				</div>
			</div>
		</div>
		
		<!-- option_tab -->
		<div>
			<div class="option_tab" title="close" >
				<p class="close">close</p>
			</div>
			<div class="option_tab" title="open" style="display:none;">
				<p class="open">open</p>
			</div>
		</div>

		<input type="hidden" name="detailEmpCd" value=""/>
		<input type="hidden" name="detailStartDay" value=""/>
		<input type="hidden" name="detailEndDay" value=""/>
		<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }"/>
		<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }"/>
		<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
		<input type="hidden" name="page_num" value="${search.page_num}" />
	</form>
	
	<!-- 엑셀 다운로드 버튼 -->	
	<img class="btn_excel right" onclick="excelEmpDetailInqList('${search.total_count}')" src="${rootPath}/resources/image/common/btn_excel.gif" alt="엑셀다운로드" title="엑셀다운로드" />

	<!-- 리스트 영역 -->
	<div>
		<table class="table left">
			<colgroup>
				<col width="15%"/>				
				<col width="15%"/>				
				<col width="15%"/>				
				<col width="15%"/>				
				<col width="15%"/>				
				<col width="20%"/>				
			</colgroup>
			<thead>
				<tr>
					<th scope="col">기간</th>
					<th scope="col">사번</th>
					<th scope="col">사용자명</th>
					<th scope="col">부서</th>
					<th scope="col">위험지수/기간</th>
					<th scope="col">위험도</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty empDetailInqList.empDetailInq}">
						<tr>
			        		<td colspan="8" align="center">데이터가 없습니다.</td>
			        	</tr>
					</c:when>
					<c:otherwise>
						<c:set value="${empDetailInqList.page_total_count}" var="count"/>
						<c:forEach items="${empDetailInqList.empDetailInq}" var="empDetailInq" varStatus="status">
							<tr style ='cursor: pointer;' onclick="javascript:fnempDetailInqDetail('${empDetailInq.emp_user_id}','${empDetailInq.startday}','${empDetailInq.endday}');"	<c:if test="${status.count % 2 == 0 }"> class='tr_gray'</c:if>>
								<c:if test="${empDetailInq.startday ne null and empDetailInq.endday ne null}">
									<td>
										<fmt:parseDate value="${empDetailInq.startday}" pattern="yyyyMMdd" var="startday" /> 
										<fmt:formatDate value="${startday}" pattern="yyyy-MM-dd" />&nbsp;~&nbsp;
										<fmt:parseDate value="${empDetailInq.endday}" pattern="yyyyMMdd" var="endday" /> 
										<fmt:formatDate value="${endday}" pattern="yyyy-MM-dd" />
									</td>
								</c:if>
								<c:if test="${empDetailInq.startday eq null or empDetailInq.endday eq null}"><td>-</td></c:if>
								<td><ctl:nullCv nullCheck="${empDetailInq.emp_user_id}"/></td>
								<td><ctl:nullCv nullCheck="${empDetailInq.emp_user_name}"/></td>
								<td><ctl:nullCv nullCheck="${empDetailInq.dept_name}"/></td>
								<c:if test="${empDetailInq.sum_dng_val ne null}">
									<td>
										<fmt:formatNumber value="${empDetailInq.sum_dng_val }" type="number" />
									</td>
								</c:if>
								<c:if test="${empDetailInq.sum_dng_val eq null}">
									<td>-</td>
								</c:if>
								<c:if test="${!empty CACHE_DANGER_STATUS}">
									<c:forEach items="${CACHE_DANGER_STATUS}" var="i" varStatus="z">
										<c:if test="${empDetailInq.sum_dng_val_code == i.key}">
											<td>
												<ctl:nullCv nullCheck="${i.value}"/>
											</td>
										</c:if>
									</c:forEach>
								</c:if>
								
								</tr>
							<c:set var="count" value="${count - 1 }"/>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
		
		<!-- 페이징 영역 -->
		<c:if test="${search.total_count > 0}">
			<div class="page left" id="pagingframe">
				<p><ctl:paginator currentPage="${search.page_num}" rowBlockCount="${search.size}" totalRowCount="${search.total_count}" /></p>
			</div>
		</c:if>
	</div>
</div>

<script type="text/javascript">

	var empDetailInqConfig = {
		"listUrl":"${rootPath}/empDetailInq/list.html"
		,"detailUrl":"${rootPath}/empDetailInq/detail.html"
		,"downloadUrl" :"${rootPath}/empDetailInq/download.html"
	};
	
</script>

