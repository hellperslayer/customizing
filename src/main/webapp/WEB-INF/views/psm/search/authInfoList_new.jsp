<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/search/authInfoInqList.js"
	type="text/javascript" charset="UTF-8"></script>
<script
	src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js"
	type="text/javascript"></script>

<style> 
.rowpass {
	width: 20%; /* 너비는 변경될수 있습니다. */
	text-overflow: ellipsis; /* 위에 설정한 100px 보다 길면 말줄임표처럼 표시합니다. */
	white-space: nowrap; /* 줄바꿈을 하지 않습니다. */
	overflow: hidden; /* 내용이 길면 감춤니다 */
	display: block; /* ie6이상 현재요소를 블럭처리합니다. */
}
</style>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>


<input type="hidden" id="checkSystem" value="true"/>
<h1 class="page-title">${currentMenuName}</h1>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light portlet-fit portlet-datatable bordered">
			
			<div class="portlet-body">
				<div class="table-container">
					<div id="datatable_ajax_2_wrapper"
						class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">

<!-- 						<div class="raw"> -->
							<div class="col-md-6" style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
								
								<div class="portlet box grey-salt  ">
		                            <div class="portlet-title" style="background-color: #2B3643;">
		                                <div class="caption">
		                                    <i class="fa fa-search"></i>검색 & 엑셀</div>
	                                    <div class="tools">
							                 <a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
							             </div>
		                            </div>
		                            <div id="searchBar" class="portlet-body form">
		                                <div class="form-body" style="padding-left:10px; padding-right:10px;">
		                                    <div class="form-group">
		                                        <form id="listForm" method="POST" class="mt-repeater form-horizontal">
		                                            <div data-repeater-list="group-a">
		                                                <div data-repeater-item class="row">
		                                                    <!-- jQuery Repeater Container -->
		                                                    <div class="col-md-2">
															    <label class="control-label">기간선택</label><br/>
															    <select class="form-control" id="daySelect" name="daySelect" onclick="javascript:initDaySelect();">
																	<option value="" class="daySelect_first" <c:if test='${search.daySelect eq ""}'>selected="selected"</c:if>>직접입력</option>
																	<option value="Day" <c:if test='${search.daySelect eq "Day"}'>selected="selected"</c:if>>오늘</option>
																	<option value="WeekDay" <c:if test='${search.daySelect eq "WeekDay"}'>selected="selected"</c:if>>일주일</option>
																</select>
															</div>
		                                                    <div class="col-md-2">
																<label class="control-label">기간</label>
			                                                    <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
																	<input type="text" class="form-control" id="search_fr" name="search_from" value="${search.search_fromWithHyphen}"> 
																	<span class="input-group-addon"> &sim; </span> 
																	<input type="text" class="form-control" id="search_to" name="search_to" value="${search.search_toWithHyphen}">
																</div>
															</div>
															
															<div class="col-md-2">
		                                                        <label class="control-label">시스템</label>
		                                                        <br/>
		                                                        <select name="system_seq"
																	class="form-control">
																	<option value=""
																		${search.system_seq == '' ? 'selected="selected"' : ''}>
																		전 체 </option>
																	<c:if test="${empty systemMasterList}">
																		<option>시스템 없음</option>
																	</c:if>
																	<c:if test="${!empty systemMasterList}">
																		<c:forEach items="${systemMasterList}" var="i"
																			varStatus="z">
																			<option value="${i.system_seq}"
																				${i.system_seq==search.system_seq ? "selected=selected" : "" }>${ i.system_name}</option>
																		</c:forEach>
																	</c:if>
																</select>
															</div>
															<div class="col-md-2">
		                                                        <label class="control-label">타겟ID 부서</label>
		                                                        <br/>
		                                                        <select name="target_auth"
																	class="form-control">
																	<option value=""
																		${search.target_auth == '' ? 'selected="selected"' : ''}>
																		전 체 </option>
																	<c:if test="${!empty authInfoToDepartment}">
																		<c:forEach items="${authInfoToDepartment}" var="i"
																			varStatus="z">
																			<option value="${i.dept_id}"
																				${i.dept_id==search.target_auth ? "selected=selected" : "" }>${ i.dept_name}</option>
																		</c:forEach>
																	</c:if>
																</select>
															</div>
															<div class="col-md-2">
		                                                        <label class="control-label">타겟ID</label>
		                                                        <br/>
		                                                        <input type="text" class="form-control" name="target_id" value="${search.target_id}" />
		                                                    </div>
															<div class="col-md-2">
		                                                        <label class="control-label">행위</label>
		                                                        <br/>
		                                                        <select name="req_type" class="form-control">
																	<option value=""
																		${search.req_type == '' ? 'selected="selected"' : ''}>
																		전 체</option>
																		<option value="CR" ${search.req_type == 'CR' ? 'selected="selected"' : ''} >등록</option>
																		<option value="UD" ${search.req_type == 'UD' ? 'selected="selected"' : ''}>수정</option>
																		<option value="DL" ${search.req_type == 'DL' ? 'selected="selected"' : ''}>삭제</option>
																</select>
															</div>
														</div>
														<div class="row" style="padding-top:5px;">
															<div class="col-md-2">
		                                                        <label class="control-label">소속</label>
		                                                        <input type="text" class="form-control" name="dept_name" value="${search.dept_name}" />
		                                                    </div>
		                                                    <div class="col-md-2">
		                                                        <label class="control-label">사용자ID</label>
		                                                        <input type="text" class="form-control" name="emp_user_id" value="${search.emp_user_id }" />
		                                                    </div>
		                                                    <div class="col-md-2">
		                                                        <label class="control-label">사용자명</label>
		                                                        <input type="text" class="form-control" name="emp_user_name" value="${search.emp_user_name}" />
		                                                    </div>
		                                                    <div class="col-md-2">
		                                                        <label class="control-label">사용자IP</label>
		                                                        <input type="text" class="form-control" name="user_ip" value="${search.user_ip}" />
		                                                    </div>
														</div>
		                                            </div>
		                                            <hr/>
		                                            <div align="right">
			                                            <button type="reset"
															class="btn btn-sm red-mint btn-outline sbold uppercase"
															onclick="resetOptions(authInfoInqConfig['listUrl'])">
															<i class="fa fa-remove"></i> <font>초기화
														</button>
														<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
															onclick="moveauthInfoInqList()">
															<i class="fa fa-search"></i> 검색
														</button>&nbsp;&nbsp;
														<div class="btn-group">
															<a data-toggle="dropdown"> <img src="${rootPath}/resources/image/icon/XLS_3.png">
															</a>
															<ul class="dropdown-menu pull-right">
																<li><a onclick="excelauthInfoInqList('${search.total_count}')"> EXCEL </a></li>
																<li><a onclick="csvauthInfoInqList()"> CSV </a></li>
															</ul>
														</div>
													</div>
													
													<input type="hidden" name="detailLogSeq" value="" /> 
													<input type="hidden" name="detailProcDate" value="" /> 
													<input type="hidden" name="main_menu_id" value="${search.main_menu_id }" /> 
													<input type="hidden" name="sub_menu_id" value="${search.sub_menu_id }" /> 
													<input type="hidden" name="current_menu_id" value="${currentMenuId}" /> 
																										
													<input type="hidden" name="page_num" value="${search.page_num}" /> 
													<input type="hidden" name="menuNum" id="menuNum" value="${menuNum}" /> 
													<input type="hidden" name="menuCh" id="menuCh" value="${menuCh}" /> 
													<input type="hidden" name="menuId" id="menuId" value="${menuId}" /> 
													<input type="hidden" name="menutitle" id="menutitle" value="${menuCh}${menuNum}" />
													<input type="hidden" name="user_id" value="" />
													<input type="hidden" name="authInfoInqDetail_user_id" />
													<input type="hidden" name="isSearch" value="${search.isSearch}" />
		                                        </form>
		                                    </div>
		                                </div>
		                            </div>
		                        </div>
							</div>
<!-- 						</div> -->


						<div class="raw">
							<table style="border-top: 1px solid #e7ecf1;"
								class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
								role="grid">
								<colgroup>
									<col width="14%" />
									<col width="10%" />
									<col width="10%" />
									<col width="10%" />
									<col width="10%" />
									<col width="10%" />
									<col width="10%" />
									<col width="10%" />
									<col width="10%" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col" style="text-align: center;">일시</th>
										<th scope="col" style="text-align: center;">소속</th>
										<th scope="col" style="text-align: center;">관리자ID</th>
										<th scope="col" style="text-align: center;">관리자명</th>
										<th scope="col" style="text-align: center;">관리자IP</th>
										<th scope="col" style="text-align: center;">시스템명</th>
										<th scope="col" style="text-align: center;">타겟ID</th>
										<th scope="col" style="text-align: center;">타겟ID 부서</th>
										<th scope="col" style="text-align: center;">행위</th>
									</tr>
								</thead>
								<tbody>
									<c:choose>
										<c:when test="${empty authInfoInqList.authInfoInq}">
											<tr>
												<td colspan="9" align="center">데이터가 없습니다.</td>
											</tr>
										</c:when>
										<c:otherwise>
											<c:set value="${authInfoInqList.page_total_count}" var="count" />
											<c:forEach items="${authInfoInqList.authInfoInq}" var="authInfoInq"	varStatus="status1">
												<tr style='cursor: pointer;' onclick="javascript:fnAuthInfoInqDetail('${authInfoInq.log_seq}', '${authInfoInq.proc_date}');">
													<c:if
														test="${authInfoInq.proc_date ne null and authInfoInq.proc_time ne null}">
														<td width="14%" style="text-align: center;"><fmt:parseDate
																value="${authInfoInq.proc_date}" pattern="yyyyMMdd"
																var="proc_date" /> <fmt:formatDate value="${proc_date}"
																pattern="yy-MM-dd" /> <fmt:parseDate
																value="${authInfoInq.proc_time}" pattern="HHmmss"
																var="proc_time" /> <fmt:formatDate value="${proc_time}"
																pattern="HH:mm:ss" /></td>
													</c:if>
													<c:if
														test="${authInfoInq.proc_date eq null or authInfoInq.proc_time eq null}">
														<td>-</td>
													</c:if>
													<td width="10%" style="text-align: center;"><ctl:nullCv nullCheck="${authInfoInq.dept_name}" /></td>
													<td width="10%" style="text-align: center;"><ctl:nullCv nullCheck="${authInfoInq.emp_user_id}" /></td>
													<td width="10%" style="text-align: center;"><ctl:nullCv nullCheck="${authInfoInq.emp_user_name}" /></td>													
													<td width="10%" style="text-align: center;"><ctl:nullCv nullCheck="${authInfoInq.user_ip}" /></td>
													<td width="10%" style="text-align: center;"><ctl:nullCv nullCheck="${authInfoInq.system_name}" /></td>
													<td width="10%" style="text-align: center;"><ctl:nullCv nullCheck="${authInfoInq.target_id}" /></td>
													<td width="10%" style="text-align: center;"><ctl:nullCv nullCheck="${authInfoInq.target_auth_name}" /></td>
													<td width="10%" style="text-align: center;">
													<c:forEach items="${CACHE_REQ_TYPE}" var="i" varStatus="status">
													<c:if test="${i.key == authInfoInq.req_type}">
														<ctl:nullCv nullCheck="${i.value}" />
													</c:if>
													</c:forEach>
													</td>
												</tr>
												<c:set var="count" value="${count - 1 }" />
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
						</div>

						<div class="raw">
							<!-- 페이징 영역 -->
							<c:if test="${search.total_count > 0}">
								<div class="page left" id="pagingframe" align="center">
									<p>
										<ctl:paginator currentPage="${search.page_num}"
											rowBlockCount="${search.size}"
											totalRowCount="${search.total_count}" />
									</p>
								</div>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<form id="menuSearchForm" method="POST">
	<input type="hidden" name="main_menu_id" value="${search.main_menu_id }" /> 
	<input type="hidden" name="sub_menu_id" value="${search.sub_menu_id }" /> 
	<input type="hidden" name="emp_user_id" value="${search.emp_user_id}" /> 
	<input type="hidden" name="search_from" value="${search.search_from}" /> 
	<input type="hidden" name="search_to" value="${search.search_to}" /> 
	<input type="hidden" name="emp_user_name" value="${search.emp_user_name}" />
	<input type="hidden" name="user_ip" value="${search.user_ip}" /> 
	<input type="hidden" name="system_seq" value="${search.system_seq}" /> 
	<input type="hidden" name="daySelect" value="${search.daySelect}" />
</form>
<script type="text/javascript">
	var authInfoInqConfig = {
		"listUrl" : "${rootPath}/authInfoInq/list.html",
		"alistUrl" : "${rootPath}/allLogInq/list.html",
		"detailUrl" : "${rootPath}/authInfoInq/detail.html",
		"downloadUrl" : "${rootPath}/authInfoInq/download.html",
		"downloadCSVUrl" : "${rootPath}/authInfoInq/downloadCSV.html"
	};
</script>
