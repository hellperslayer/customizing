<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="MENU00495" />

<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/search/allLogInqList.js" type="text/javascript" charset="UTF-8"></script>

<h1 class="page-title">
	${currentMenuName} 
</h1>
 <form id="summonDetailForm" method="POST">
<input type="hidden" name="main_menu_id" value="${search.main_menu_id }" />
<input type="hidden" name="sub_menu_id" value="${search.sub_menu_id }" />
<input type="hidden" name="current_menu_id" value="${currentMenuId }" />

<input type="hidden" name="emp_user_id" value="${allLogInq.emp_user_id}" />
<input type="hidden" name="emp_user_name" value="${allLogInq.emp_user_name}" />
<input type="hidden" name="proc_date" value="${allLogInq.proc_date}" />
<input type="hidden" name="rule_nm" value="${summon.rule_nm}" />
<input type="hidden" name="rule_cd" value="${summon.rule_cd}" />
<input type="hidden" name="admin_user_id" value="${userSession.admin_user_id}" />
<input type="hidden" name="detailLogSeq" value="${search.detailLogSeq}"/>
<input type="hidden" name="detailEmpDetailSeq" value="${search.detailEmpDetailSeq}"/>
<div class="row">
<div class="portlet light portlet-fit bordered">
	
	 <div class="portlet-title">
		 <div class="caption">
			 <i class="icon-settings font-dark"></i>
			 <span class="caption-subject font-dark sbold uppercase">로그정보</span>
		 </div>
	 </div>
	 <div class="portlet-body">
		 <div class="row">
			 <div class="col-md-12">
				 <table id="user" class="table table-bordered table-striped">
					 <tbody>
						 <tr>
							 <th style="width:15%;text-align: center;">일시 </th>
							 <c:if test="${allLogInq.proc_date ne null and allLogInq.proc_time ne null}">
								<td style="width:35%;text-align: center;">
									<fmt:parseDate value="${allLogInq.proc_date}" pattern="yyyyMMdd" var="proc_date" /> 
									<fmt:formatDate value="${proc_date}" pattern="YY-MM-dd" />
									<fmt:parseDate value="${allLogInq.proc_time}" pattern="HHmmss" var="proc_time" /> 
									<fmt:formatDate value="${proc_time}" pattern="HH:mm:ss" />
								</td>
							</c:if>
							<c:if test="${allLogInq.proc_date eq null or allLogInq.proc_time eq null}"><td style="width:35%">-</td></c:if>
							 <th style="width:15%;text-align: center;"> 사용자ID </th>
							 <td style="width:35%;text-align: center;">
								 <ctl:nullCv nullCheck="${allLogInq.emp_user_id}"/>
							 </td>
						 </tr>
						 <tr>
							 <th style="text-align: center;"> 사용자명 </th>
							 <td style="text-align: center;">
								 <ctl:nullCv nullCheck="${allLogInq.emp_user_name}"/>
							 </td>
							 <th style="text-align: center;"> 사용자IP </th>
							 <td style="text-align: center;">
								 <ctl:nullCv nullCheck="${allLogInq.user_ip}"/>
							 </td>
						 </tr>
							<tr>
							 <th style="text-align: center;"> 시스템명 </th>
							 <td style="text-align: center;">
								 <ctl:nullCv nullCheck="${allLogInq.system_name}"/>
							 </td>
							 <th style="text-align: center;"> 개인정보유형 </th>
							 <td style="text-align: center;">
								 <ctl:nullCv nullCheck="${allLogInq.result_type}"/>
							 </td>
						 </tr>
						 <tr>
							 <th style="text-align: center;"> 수행업무 </th>
							 <td style="text-align: center;">
								 <c:forEach items="${CACHE_REQ_TYPE}" var="i" varStatus="status">
									<c:if test="${i.key == allLogInq.req_type}">
										<ctl:nullCv nullCheck="${i.value}"/>
									</c:if>
								</c:forEach>
							 </td>
							 <th style="text-align: center;"> 접근 경로 </th>
							 <td style="text-align: center;">
								 <ctl:nullCv nullCheck="${allLogInq.req_url}"/>
							 </td>
						 </tr>
						 <tr>
							 <th style="text-align: center;"> 부서정보 </th>
							 <td style="text-align: center;">
								 <ctl:nullCv nullCheck="${allLogInq.dept_name}"/>
							 </td>
							 <th style="text-align: center;"> 메뉴명 </th>
							 <td style="text-align: center;">
								 <c:if test="${allLogInq.scrn_name == null }">
								-
								</c:if>
								<c:if test="${allLogInq.scrn_name != null }">
									${allLogInq.scrn_name}
								</c:if>
								(
								<c:if test="${allLogInq.scrn_id == null }">
									-
								</c:if>
								<c:if test="${allLogInq.scrn_id != null }">
									${allLogInq.scrn_id}
								</c:if>
								)
							 </td>
						 </tr>
						 <tr>
							 <th style="text-align: center;"> 추출조건 </th>
							 <td style="text-align: center;" colspan="3">
								 <ctl:nullCv nullCheck="${summon.rule_nm}"/>
							 </td>
						 </tr>
					 </tbody>
				 </table>
			 </div>
		 </div>
	 </div>
	<%-- 
	
	 <div class="portlet-title">
		 <div class="caption">
			 <i class="icon-settings font-dark"></i>
			 <span class="caption-subject font-dark sbold uppercase">소명 요청</span>
		 </div>
	 </div>
	 <input type="hidden" name="rule_nm" value="${summon.rule_nm}" />
	 <div class="portlet-body">
		 <div class="row">
			 <div class="col-md-12">
				 <table id="user" class="table table-bordered table-striped">
					 <tbody>
						 <tr>
							 <td style="width:15%;text-align: center;">소명요청사유</td>
							 <td style="vertical-align: middle;" class="form-group form-md-line-input">
							<textarea class="form-control" name="description1" rows="5" cols="40">${summon.rule_nm}</textarea>
							<div class="form-control-focus"></div>
						</td>
						</tr>
					 </tbody>
				 </table>
			 </div>
		 </div>
	 </div>
	 --%>
	 
	 <div class="portlet-title">
		 <div class="caption">
			 <i class="icon-settings font-dark"></i>
			 <span class="caption-subject font-dark sbold uppercase">소명요청 E-MAIL</span>
		 </div>
	 </div>
	 <div class="portlet-body">
		 <div class="row">
			 <div class="col-md-12">
				 <table id="user" class="table table-bordered table-striped">
					 <tbody>
						<tr>
							<th style="text-align: left;"><label class="control-label">E-MAIL
							</label></th>
						 </tr>
						<tr>
							<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<input type="text" class="form-control" name="email_address" value="${allLogInq.email_address}">
							<div class="form-control-focus"></div></td>
						 </tr>
						<tr>
						 <tr>
							 <td style="width:5%;text-align: left;">내&nbsp;&nbsp;&nbsp;용 (소명 응답 URL은 내용 마지막에 자동으로 추가됩니다.)</td>
						 </tr>
						<tr>
							<td style="vertical-align: middle;" class="form-group form-md-line-input">
							<textarea class="form-control" name="description2" rows="7" cols="40">${responseSampleText}</textarea>
							<div class="form-control-focus"></div>
						</td>
						</tr>
					 </tbody>
				 </table>
			 </div>
		 </div>
	 </div>
	 <div class="form-actions">
			<br>
				<div class="row">
					<div class="col-md-offset-2 col-md-10" align="right"
						style="padding-right: 30px;">
						<button type="button"
							class="btn btn-sm dark btn-outline sbold uppercase"
							onclick="moveSummonDetail()">
							<i class="fa fa-list"></i> 목록
						</button>
						<c:choose>
							<c:when test="${empty exist}">
								<button type="button"
									class="btn btn-sm blue btn-outline sbold uppercase"
									onclick="addSummon()">
									<i class="fa fa-check"></i> 소명요청
								</button>
							</c:when>
							<%-- <c:otherwise>
								<button type="button"
									class="btn btn-sm green table-group-action-submit"
									onclick="saveAdminUser()">
									<i class="fa fa-check"></i> 수정
								</button>
								<button type="button"
									class="btn btn-sm red table-group-action-submit"
									onclick="removeAdminUser()">
									<i class="fa fa-close"></i> 삭제
								</button>
							</c:otherwise> --%>
						</c:choose>
					</div>
				</div>
			</div>
			
	 <div class="portlet-title">
		 <div class="caption">
			 <i class="icon-settings font-dark"></i>
			 <span class="caption-subject font-dark sbold uppercase">개인정보</span>
		 </div>
	 </div>
	 <div class="portlet-body">
		 <div class="row">
			 <div class="col-md-12">
				 <table id="user" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer">
					 <tbody>
						 <tr>
						 	 <th style="width:10%;text-align: center;">No.</th>
							 <th style="width:30%;text-align: center;">개인정보유형 </th>
							 <th style="width:30%;text-align: center;">개인정보내용</th>
							 <th style="width:30%;text-align: center;">탐지 예외 처리 </th>
						 </tr>
						 
						 <c:choose>
							<c:when test="${empty allLogInqDetailList}">
								<tr>
									<td colspan="8" align="center">데이터가 없습니다.</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:set value="${search.allLogInqDetail.total_count}" var="count"/>
								<c:forEach items="${allLogInqDetailList}" var="allLogInqDetailList"  varStatus="status">
									<tr>
										<td align="center">${count - search.allLogInqDetail.page_num * 20 + 20 }</td>
										<td align="center"<c:if test="${allLogInqDetailList.check_exc eq 1}"></c:if>><ctl:nullCv nullCheck="${allLogInqDetailList.result_type}"/></td>
										<td align="center"<c:if test="${allLogInqDetailList.check_exc eq 1}"></c:if>><ctl:nullCv nullCheck="${allLogInqDetailList.result_content}"/></td>
										<td align="center"><c:if test="${allLogInqDetailList.check_exc ne 1}"><p class="btn btn-xs green-haze" onclick="javascript:deletePrivacy(${allLogInqDetailList.biz_log_result_seq},${allLogInqDetailList.privacy_seq},'${allLogInqDetailList.result_content}')"><i class="fa fa-remove"></i>&nbsp;&nbsp;&nbsp;제외</p></c:if></td>
									</tr>
								<c:set var="count" value="${count - 1 }"/>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					 </tbody>
				 </table>
				 
			 </div>
			 
		 </div>
		 <div class="row">
		 	<div class="col-md-12" align="center">
		 		 <!-- 페이징 영역 -->
				<c:if test="${search.allLogInqDetail.total_count > 0}">
					<div id="pagingframe">
						<p><ctl:paginator currentPage="${search.allLogInqDetail.page_num}" rowBlockCount="${search.allLogInqDetail.size}" totalRowCount="${search.allLogInqDetail.total_count}" pageName="allLogInqDetail"/></p>
					</div>
				</c:if>
		 	</div>
		 </div>
		 
		 <div class="row">
		 	<div class="col-md-12" align="right">
		 		<!-- 버튼 영역 -->
				<div class="option_btn right" style="padding-right:10px;">
					<p class="right">
						<c:if test="${search.bbs_id eq null or empty search.bbs_id}">
							<a class="btn btn-sm default table-group-action-submit" onclick="javascript:goList();"><i class="fa fa-list"></i> 목록</a>
						</c:if>
						<c:if test="${search.bbs_id eq 'extrtCondbyInq' or search.bbs_id eq 'empDetailInq'}">
							<a class="btn btn-sm default table-group-action-submit" onclick="moveExtrtCondbyInqDetail();"><i class="fa fa-list"></i> 목록</a>
						</c:if>
						<c:if test="${search.bbs_id eq 'callingDemand'}">
							<a class="btn btn-sm default table-group-action-submit" onclick="moveExtrtCondbyInqDetail();"><i class="fa fa-list"></i> 목록</a>
						</c:if>
					</p>
				</div>
		 	</div>
		 </div>
	 </div>
	 
	 
	 
</div>
</div>

</form>
				 <form id="listForm" method="POST">
				<!-- 메뉴 관련 input 시작 -->
				<input type="hidden" name="main_menu_id" value="${search.main_menu_id }" />
				<input type="hidden" name="sub_menu_id" value="${search.sub_menu_id }" />
				<input type="hidden" name="current_menu_id" value="${currentMenuId }" />
				<!-- 메뉴 관련 input 끝 -->
				
				<!-- 페이지 번호 -->
				<input type="hidden" name="page_num" value="${search.page_num }" />
				<input type="hidden" name="allLogInqDetail_page_num" value="${search.allLogInqDetail.page_num }" />
				<input type="hidden" name="extrtCondbyInqDetail_page_num" value="${search.extrtCondbyInqDetail.page_num }" />
				<input type="hidden" name="empDetailInqDetail_page_num" value="${search.empDetailInqDetail.page_num }" />
				
				<!-- 검색조건 관련 input 시작 -->
				<input type="hidden" name="search_from" value="${search.search_from }" />
				<input type="hidden" name="search_to" value="${search.search_to }" />
				<input type="hidden" name="emp_user_id" value="${search.emp_user_id }" />
				<input type="hidden" name="privacy" value="${search.privacy }" />
				<input type="hidden" name="daySelect" value="${search.daySelect }" />
				<input type="hidden" name="user_ip" value="${search.user_ip }" />
				<input type="hidden" name="privacyType" value="${search.privacyType }" />
				<input type="hidden" name="system_seq" value="${search.system_seq }" />
				<input type="hidden" name="req_url" value="${search.req_url }" />
				<input type="hidden" name="emp_user_name" value="${search.emp_user_name}" />
				<input type="hidden" name="dept_name" value="${search.dept_name}" />
				<input type="hidden" name="rule_cd" value="${search.rule_cd }" />
				<input type="hidden" name="mapping_id" value="${search.mapping_id }" />
				<!-- 검색조건 관련 input 끝 -->
				
				<!-- 상세 검색 조건 input 시작 -->
				<input type="hidden" name="dng_val" value="${search.dng_val }"/>
				<input type="hidden" name="detailLogSeq" value="${search.detailLogSeq }"/>
				<input type="hidden" name="detailProcDate" value="${search.detailProcDate }"/>
				<input type="hidden" name="bbs_id" value="${search.bbs_id }"/>
				<input type="hidden" name="detailOccrDt" value="${search.detailOccrDt}"/>
				<input type="hidden" name="detailEmpCd" value="${search.detailEmpCd }"/>
				<input type="hidden" name="detailStartDay" value="${search.detailStartDay }"/>
				<input type="hidden" name="detailEndDay" value="${search.detailEndDay }"/>
				<input type="hidden" name="detailEmpDetailSeq" value="${search.detailEmpDetailSeq}"/>
				<input type="hidden" name="misdetect_pattern" value=""/>
				<input type="hidden" name="privacy_type" value=""/>
				<input type="hidden" name="biz_log_result_seq" value=""/>
				<!-- 상세 검색 조건 input 끝 -->
			</form>
<script type="text/javascript">

	var allLogInqConfig = {
		"listUrl":"${rootPath}/allLogInq/list.html"
		,"detailUrl":"${rootPath}/allLogInq/detail.html"
		,"addUrl":"${rootPath}/allLogInq/add.html"
		,"loginPage" : "${rootPath}/loginView.html"
		,"dummondetailUrl":"${rootPath}/extrtCondbyInq/summonDetail.html"
		,"addsummonUrl":"${rootPath}/extrtCondbyInq/summonadd.html"
	};
	/* 
	var moveSummonDetailConfig = {
		"detailUrl":"${rootPath}/extrtCondbyInq/detail.html"
	}; */
</script>
