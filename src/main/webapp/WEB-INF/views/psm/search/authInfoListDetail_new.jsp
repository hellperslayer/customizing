<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/search/authInfoInqList.js"
	type="text/javascript" charset="UTF-8"></script>
<link href="${rootPath}/resources/css/profile.min.css" rel="stylesheet"
	type="text/css">


<h1 class="page-title">
	${currentMenuName}
	<!--bold font-blue-hoki <small>basic bootstrap tables with various options and styles</small> -->
</h1>

<div class="contents left">


	<div class="row"
		style="background: #eef1f5; padding-top: 20px; padding-bottom: 20px">
		<input type="hidden" id="emp_user_id" name="emp_user_id"
			value="${authInfoInq.emp_user_id}">
		<div class="col-md-12">
			<!-- BEGIN PROFILE SIDEBAR -->
			<div class="profile-sidebar">
				<!-- PORTLET MAIN -->
				<div class="portlet light profile-sidebar-portlet "
					style="height: 147%"">
					<!-- SIDEBAR USERPIC -->
					<div class="profile-userpic" style="margin-bottom: -5px;">
						<img src="${rootPath}/resources/image/common/profile_user2.png"
							class="img-responsive" alt="profile">
					</div>
					<!-- END SIDEBAR USERPIC -->
					<!-- SIDEBAR USER TITLE -->
					<div class="profile-usertitle">
						<div class="profile-usertitle-name" style="margin-bottom: 10px;">
							<ctl:nullCv nullCheck="${authInfoInq.emp_user_name}" />
						</div>
					</div>
					<!-- END SIDEBAR USER TITLE -->
					<!-- SIDEBAR BUTTONS -->
				</div>
				<!-- END PORTLET MAIN -->
				<!-- PORTLET MAIN -->
				<div class="portlet light" style="height: 550px;">
					<!-- STAT -->
					<div class="row">
						<h4 class="profile-desc-title" style="padding-bottom: 10px;">프로필</h4>
						<div class="margin-top-20 profile-desc-link"
							style="margin-top: -20px !important;">
							<table class="table table-hover table-light">
								<tbody>
									<tr>
										<td><b>소속</b> : ${authInfoInq.dept_name}</td>
									</tr>
									<tr>
										<td><b>IP</b> : ${authInfoInq.user_ip}</td>
									</tr>
									<tr>
										<td><b>ID</b> : ${authInfoInq.emp_user_id}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="row list-separated profile-stat">
						<h4 class="profile-desc-title">업무 시스템</h4>
						<div class="tab-context">
							<div class="clearfix">
								<c:forEach items="${userinfo}" var="system" varStatus="status">
									<c:if test="${system.system_name != null}">
										<a style="cursor: default" class="btn btn-xs blue"
											style="margin-top: 5px; margin-bottom: 5px;">${system.system_name}
											<i class="fa fa-laptop"></i>
										</a>
									</c:if>
								</c:forEach>
							</div>
						</div>
					</div>
					<!-- END STAT -->
					<div class="row" style="margin-top: -15px !important;"></div>
				</div>
				<!-- END PORTLET MAIN -->
			</div>
			<!-- END BEGIN PROFILE SIDEBAR -->
			<!-- BEGIN PROFILE CONTENT -->
			<div class="profile-content">
				<div class="row">
					<div class="col-md-12">
						<div class="portlet light" style="min-height: 800px;">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-settings font-dark"></i> <span
										class="caption-subject font-dark sbold uppercase">로그정보</span>
								</div>
							</div>
							<div class="portlet-body">
								<table id="user" class="table table-bordered table-striped">
									<tbody>
										<tr>
											<th style="width: 15%; text-align: center;">일시</th>
											<c:if
												test="${authInfoInq.proc_date ne null and authInfoInq.proc_time ne null}">
												<td style="width: 35%; text-align: center;"><fmt:parseDate
														value="${authInfoInq.proc_date}" pattern="yyyyMMdd"
														var="proc_date" /> <fmt:formatDate value="${proc_date}"
														pattern="YY-MM-dd" /> <fmt:parseDate
														value="${authInfoInq.proc_time}" pattern="HHmmss"
														var="proc_time" /> <fmt:formatDate value="${proc_time}"
														pattern="HH:mm:ss" /></td>
											</c:if>
											<th style="width: 15%; text-align: center;">시스템명</th>
											<td style="width: 35%; text-align: center;"><ctl:nullCv
													nullCheck="${authInfoInq.system_name}" /></td>
										</tr>
										<tr>
											<th style="text-align: center;">수행업무</th>
											<td style="text-align: center;"><c:forEach
													items="${CACHE_REQ_TYPE}" var="i" varStatus="status">
													<c:if test="${i.key == authInfoInq.req_type}">
														<ctl:nullCv nullCheck="${i.value}" />
													</c:if>
												</c:forEach></td>
											<th style="text-align: center;">접근경로</th>
											<td style="text-align: center;"><ctl:nullCv
													nullCheck="${authInfoInq.req_url }" /></td>
										</tr>

									</tbody>
								</table>
							</div>

							<div class="portlet-title">
								<div class="caption" style="width: 100%;">
									<i class="icon-settings font-dark"></i> <span
										class="caption-subject font-dark sbold uppercase"
										style="float: left;">권한관리정보</span>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<table id="user"
										class="table table-bordered table-striped">
										<tbody>
											<tr>
												<th style="width: 15%; text-align: center;">타겟ID</th>
												<td style="width: 35%; text-align: center;"><ctl:nullCv
														nullCheck="${authInfoInq.target_id}" />
												<th style="width: 15%; text-align: center;">타겟이름</th>
												<td style="width: 35%; text-align: center;"><ctl:nullCv
														nullCheck="${authInfoInq.target_name}" /></td>
											</tr>

											<tr>
												<th style="text-align: center;">타겟ID 부서</th>
												<td style="width: 35%; text-align: center;"><ctl:nullCv
														nullCheck="${authInfoInq.target_auth_name}" /></td>
												<th style="text-align: center;">허용IP</th>
												<td style="width: 35%; text-align: center;"><ctl:nullCv
														nullCheck="${authInfoInq.allow_ip}" /></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>



							<form id="listForm" method="POST">
								<!-- 메뉴 관련 input 시작 -->
								<input type="hidden" name="main_menu_id"
									value="${paramBean.main_menu_id }" /> <input type="hidden"
									name="sub_menu_id" value="${paramBean.sub_menu_id }" /> <input
									type="hidden" name="current_menu_id" value="${currentMenuId }" />
								<!-- 메뉴 관련 input 끝 -->

								<!-- 페이지 번호 -->
								<input type="hidden" name="page_num" value="${search.page_num }" />

								<!-- 검색조건 관련 input 시작 -->
								<input type="hidden" name="search_from"
									value="${search.search_from }" /> <input type="hidden"
									name="search_to" value="${search.search_to }" /> <input
									type="hidden" name="emp_user_id" value="${search.emp_user_id }" />
								<input type="hidden" name="daySelect"
									value="${search.daySelect }" /> <input type="hidden"
									name="user_ip" value="${search.user_ip }" /> <input
									type="hidden" name="system_seq" value="${search.system_seq }" />
								<input type="hidden" name="req_url" value="${search.req_url }" />
								<input type="hidden" name="emp_user_name"
									value="${search.emp_user_name}" /> <input type="hidden"
									name="dept_name" value="${search.dept_name}" /> <input
									type="hidden" name="dept_id" value="" /> <input type="hidden"
									name="req_type" value="${search.req_type }" />
								<!-- 검색조건 관련 input 끝 -->

								<!-- 상세 검색 조건 input 시작 -->
								<input type="hidden" name="detailLogSeq"
									value="${search.detailLogSeq }" /> <input type="hidden"
									name="detailProcDate" value="${search.detailProcDate }" /> <input
									type="hidden" name="detailProcTime"
									value="${search.proc_time }" /> <input type="hidden"
									name="searchTime" value="${search.searchTime }" /> <input
									type="hidden" name="start_h" value="${search.start_h }" /> <input
									type="hidden" name="end_h" value="${search.end_h }" />
								<!-- 상세 검색 조건 input 끝 -->
							</form>

							<form id="privacyForm" method="POST">
								<input type="hidden" name="privacy" value="" /> <input
									type="hidden" name="privacyType" value="" /> <input
									type="hidden" name="system_seq" value="${allLogInq.system_seq}" />
								<input type="hidden" name="emp_user_id"
									value="${allLogInq.emp_user_id}" />
							</form>

							<div class="row">
								<div class="col-md-12" align="right">
									<!-- 버튼 영역 -->
									<div class="option_btn right" style="padding-right: 10px;">
										<p class="right">
											<a class="btn btn-sm grey-mint btn-outline sbold uppercase"
												onclick="javascript:goList();"><i class="fa fa-list"></i>
												목록</a>
										</p>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
			<!-- END PROFILE CONTENT -->
		</div>
	</div>

</div>
<!-- END CONTENT BODY -->

<form id="menuSearchForm" method="POST">
	<input type="hidden" name="main_menu_id"
		value="${paramBean.main_menu_id }" /> <input type="hidden"
		name="sub_menu_id" value="${paramBean.sub_menu_id }" /> <input
		type="hidden" name="system_seq" value="${paramBean.system_seq }" /> <input
		type="hidden" name="emp_user_id" value="${search.emp_user_id}" /> <input
		type="hidden" name="search_from" value="${search.search_from}" /> <input
		type="hidden" name="search_to" value="${search.search_to}" /> <input
		type="hidden" name="emp_user_name" value="${search.emp_user_name}" />
	<input type="hidden" name="user_ip" value="${search.user_ip}" /> <input
		type="hidden" name="daySelect" value="${search.daySelect}" />
</form>

<script type="text/javascript">
	var authInfoInqConfig = {
		"listUrl" : "${rootPath}/authInfoInq/list.html",
		"FUNDUrl" : "${rootPath}/authInfoInq/fundLogList.html",
		"loginPage" : "${rootPath}/loginView.html"
	};
</script>


