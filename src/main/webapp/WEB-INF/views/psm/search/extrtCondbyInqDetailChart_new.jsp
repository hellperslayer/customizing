<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/dashboard/highcharts.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/exporting.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/globalize.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/knockout-3.0.0.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/dx.chartjs.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/zoomingData.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/dashboard/vivagraph.js" type="text/javascript" charset="UTF-8"></script>

<script src="${rootPath}/resources/js/psm/search/extrtCondbyInqList.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/search/extrtCondbyInqDetailChart.js" type="text/javascript" charset="UTF-8"></script>






<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="${rootPath}/resources/css/css.css" rel="stylesheet" type="text/css">
<link href="${rootPath}/resources/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="${rootPath}/resources/css/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="${rootPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="${rootPath}/resources/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->

<link href="${rootPath}/resources/css/datatables.min.css" rel="stylesheet" type="text/css">
<link href="${rootPath}/resources/css/datatables.bootstrap.css" rel="stylesheet" type="text/css">
<link href="${rootPath}/resources/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css">

<!-- BEGIN THEME GLOBAL STYLES -->
<link href="${rootPath}/resources/css/components.min.css" rel="stylesheet" id="style_components" type="text/css">
<link href="${rootPath}/resources/css/plugins.min.css" rel="stylesheet" type="text/css">
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<%-- <link href="${rootPath}/resources/css/layout.min.css" rel="stylesheet" type="text/css"> --%>
<link href="${rootPath}/resources/css/custom.min.css" rel="stylesheet" type="text/css">
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="http://localhost:8080/PSM-3.0.2/abnormal/favicon.ico"> 

<script src="${rootPath}/resources/js/jquery.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/bootstrap.min.js" type="text/javascript"></script>

<!-- AMCHART SCRIPT START-->
<script src="${rootPath}/resources/js/amcharts.js"></script>
<script src="${rootPath}/resources/js/serial.js"></script>
<script src="${rootPath}/resources/js/export.min.js"></script>
<script src="${rootPath}/resources/js/amstock.js"></script>
<script src="${rootPath}/resources/js/xy.js"></script>
<link rel="stylesheet" href="${rootPath}/resources/css/export.css" type="text/css" media="all">
<script src="${rootPath}/resources/js/light.js"></script>
<!-- AMCHART SCRIPT END -->

<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="${rootPath}/resources/js/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="${rootPath}/resources/js/layout.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/demo.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/quick-sidebar.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/quick-nav.min.js" type="text/javascript"></script>
<link href="${rootPath}/resources/css/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color">

<script src="${rootPath}/resources/js/datatable.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/datatables.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/datatables.bootstrap.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/menu-util.js" type="text/javascript"></script>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
 

<script type="text/javascript" src="${rootPath}/resources/js/fabric.min.js" async=""></script>
<script type="text/javascript" src="${rootPath}/resources/js/FileSaver.min.js" async=""></script>

<link href="${rootPath}/resources/css/profile.min.css" rel="stylesheet" type="text/css">
<link href="${rootPath}/resources/css/profile.min.css" rel="stylesheet" type="text/css">

<script src="${rootPath}/resources/js/pie.js"></script>


<h1 class="page-title"> ${currentMenuName}
    <!--bold font-blue-hoki <small>basic bootstrap tables with various options and styles</small> -->
</h1>

<div class="contents left">
 		
<form id="listForm" method="POST">


	

<div class="row" style="background:#eef1f5; padding-top: 20px; padding-bottom: 20px">
	<input type="hidden" id="emp_user_id" name="emp_user_id" value="${data1.emp_user_id}">
	<div class="col-md-12">
		<!-- BEGIN PROFILE SIDEBAR -->
		<div class="profile-sidebar">
			<!-- PORTLET MAIN -->
			<div class="portlet light profile-sidebar-portlet "style=height:147%">
				<!-- SIDEBAR USERPIC -->
				<div class="profile-userpic" style="margin-bottom: -5px; ">
					<img src="${rootPath}/resources/image/profile_user.png" class="img-responsive" alt="profile">
				</div>
				<!-- END SIDEBAR USERPIC -->
				<!-- SIDEBAR USER TITLE -->
				<div class="profile-usertitle">
					<div class="profile-usertitle-name"style="margin-bottom: 10px; ">${data1.emp_user_name}</div>
					<%-- <div class="profile-usertitle-job"style="margin-bottom: 15px; ">(${data1.emp_user_id})</div> --%>
				</div>
				<!-- END SIDEBAR USER TITLE -->
				<!-- SIDEBAR BUTTONS -->
			</div>
			<!-- END PORTLET MAIN -->
			<!-- PORTLET MAIN -->
			<div class="portlet light " style="margin-top: -10px;">
				<!-- STAT -->
				<div class="row">
					<h4 class="profile-desc-title" style="padding-bottom: 10px; ">개인 PROFILE</h4>
					<div class="margin-top-20 profile-desc-link" style="margin-top: -20px !important;">
						<table class="table table-hover table-light">
	                        <tbody>
	                            <tr>
	                                <td> <b>소속</b> : ${data1.dept_name} </td>
	                            </tr>
	                            <tr>
	                                <td> <b>IP</b> : ${data1.user_ip}</td>
	                            </tr> 
	                            <tr>
	                            	<td> <b>ID</b> : ${data1.emp_user_id} </td>
	                            </tr>
	                        </tbody>
                        </table>
					</div>
				</div>
				<div class="row list-separated profile-stat">
				  <h4 class="profile-desc-title">업무 Style</h4>
					<div class="tab-context">
						<div class="clearfix fa-item">
							<c:forEach items="${data2}" var="system" varStatus="status">
							<span 
								<c:if test="${status.count % 4 == 0 }"> class="btn btn-xs red"</c:if>
								<c:if test="${status.count % 4 == 1 }"> class="btn btn-xs yellow"</c:if>
								<c:if test="${status.count % 4 == 2 }"> class="btn btn-xs green"</c:if>
								<c:if test="${status.count % 4 == 3 }"> class="btn btn-xs blue"</c:if>								
								style="margin-top: 5px; margin-bottom: 5px;">${system.system_name}
                                <i class="fa fa-laptop"></i>
	                       	 </span>
							</c:forEach>
	                    </div>
                    </div>
				</div>
				<!-- END STAT -->
				<div class="row" style="margin-top: -15px !important;">
					 <h4 class="profile-desc-title">오ㆍ남용 위험현황 <button class="btn btn-circle red-sunglo" type="button" style="margin-top: 8px; margin-right: 15px;">${data1.dng_grade}</button></h4>
					<span class="profile-desc-text">현재 ${data1.dng_grade} 단계입니다. </span>
					<br><br><div class="margin-top-20 profile-desc-link" style="margin-top: 2px !important;">
						<c:if test="${data1.dng_grade == '심각' }"><i class="fa fa-globe"></i><a><b>심각 단계</b>는 <b><br>전체적 지역본부에서 위험도 높은 개인정보  <br>확인 다수발생 </b><br>전사적 차원에서 대처가 필요합니다. </a> </c:if>
						<c:if test="${data1.dng_grade == '경계' }"><i class="fa fa-globe"></i><a><b>경계 단계</b>는 <b><br>위험도 높은 개인정보 확인 다수 발생,<br>다수의 지역본부 개인정보 확인 발생</b><br>지역본부 공동 대체가  대처필요합니다. </a> </c:if>
						<c:if test="${data1.dng_grade == '주의' }"><i class="fa fa-globe"></i><a><b>주의 단계</b>는 <b><br> 여러 항목의 개인정보 확인 발생 , <br>위험도 높은 개인정보 확인 발생</b><br>지역본부 공동 대체가  대처필요합니다. </a> </c:if>
						<c:if test="${data1.dng_grade == '관심' }"><i class="fa fa-globe"></i><a><b>관심 단계</b>는 <b><br>일부 개인정보 확인 발생 <br></b>위험도 낮은 개인정보 확인 발생이 필요합니다. </a> </c:if>
						<c:if test="${data1.dng_grade == '정상' }"><i class="fa fa-globe"></i><a><b>정상 단계</b>는 <b><br>전분야 정상적인 활동</b></a></c:if>
					</div>
				</div>
			</div>
			<!-- END PORTLET MAIN -->
		</div>
		<!-- END BEGIN PROFILE SIDEBAR -->
		<!-- BEGIN PROFILE CONTENT -->
		<div class="profile-content">
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light ">
						<div class="portlet-title tabbable-line">
							<div class="caption caption-md">
								<i class="icon-globe theme-font hide"></i> <span class="caption-subject font-blue-madison bold uppercase">Profile
									Account</span>
							</div>
						</div>
						<div class="portlet-body">
							<div class="tab-content">
								
								<div class="tab-pane active" id="tab_1_1">
								<div class="row">
									<div class="mt-element-step">
                                            <div class="row step-line">
                                                <div class="col-md-2 mt-step-col last error">
                                                    <div class="mt-step-number bg-white">
														<c:if test="${data1.dng_grade == '심각' }">5</c:if>
														<c:if test="${data1.dng_grade == '경계' }">4</c:if>
														<c:if test="${data1.dng_grade == '주의' }">3</c:if>
														<c:if test="${data1.dng_grade == '관심' }">2</c:if>
														<c:if test="${data1.dng_grade == '정상' }">1</c:if>
	
													</div>
                                                    <div class="mt-step-title uppercase font-grey-cascade">${data1.dng_grade}</div>
                                                </div>
                                                <div class="margin-top-40" >
                                                	 <div style=" margin-top: 5px; margin-bottom: 5px;"><span style="font-size:21px;"> ${data1.emp_user_name} 직원은 오남용 위험 요인 <font style="color:red">${data1.dng_grade} 단계의</font> 위험현황 입니다.
                                                	 </span> </div>  <div style=" margin-top: 5px; margin-bottom: 5px;">
                                                	 <span style="font-size:21px; "><br> <font style="color:red">오남용 위험요인을 확인하세요.</font></span>
                                                		 </div>
                                                </div>
                                            </div>                                            
                                    </div>
                                   
                                 </div>
                                 <div class="row margin-top-20" style="padding-top:140px;">
									<div class="m-grid m-grid-responsive-md m-grid-demo">
                                      <div class="m-grid-row" style="height:412px">
                                      <div class="m-grid-col m-grid-col-middle m-grid-col-center" style="padding: 10px; height: 400px;">
                                          <div class="portlet light bordered" style="margin-bottom:2px;">
                                             <div class="portlet-title">
                                             	<div class="caption">
                                						 <i class="icon-bar-chart font-green-haze"></i>
                                						 <span class="caption-subject bold uppercase font-red-haze">고유식별정보 과다사용</span>
                            						 </div>
                                             </div>
                                             <div class="poltlet-body">
                                              <div id="extrt_amchart_5" class="chart"></div>
                                           </div>
                                          </div>
                                 		</div>	
                                 		<div class="m-grid-col m-grid-col-middle m-grid-col-center" style="padding: 10px; height: 380px;">
                                         	<div class="portlet light bordered" style="margin-bottom:2px;">
                                              <div class="portlet-title">
                                              	<div class="caption">
                                 						 <i class="icon-bar-chart font-green-haze"></i>
                                 						 <span class="caption-subject bold uppercase font-red-haze">비정상 접근</span>
                             						 </div>
                                              </div>
                                              <div class="poltlet-body">
                                             	<div id="extrt_amchart_3" class="chart"></div>
                                             	</div>
                                          </div>
                                      </div>	
                             			<div class="m-grid-col m-grid-col-middle m-grid-col-center" style="padding: 10px; height: 400px;">
                                              <div class="portlet light bordered" style="margin-bottom:2px;">
                                                  <div class="portlet-title">
                                                  	<div class="caption">
                                     						 <i class="icon-bar-chart font-green-haze"></i>
                                     						 <span class="caption-subject bold uppercase font-red-haze">과다처리</span>
                                 						 </div>
                                                  </div>
                                                  <div class="poltlet-body">
                                                  	<div id="extrt_amchart_1" class="chart"></div>
                                                  </div>
                                              </div>
                                			</div>
                                         <div class="m-grid-col m-grid-col-middle m-grid-col-center" style="padding: 10px; height: 400px;">
                                         		<div class="portlet light bordered" style="margin-bottom:2px;">
                                                  <div class="portlet-title">
                                                  	<div class="caption">
                                     						 <i class="icon-bar-chart font-green-haze"></i>
                                     						 <span class="caption-subject bold uppercase font-red-haze">특정인 처리</span>
                                 						 </div>
                                                  </div>
                                                  <div class="poltlet-body">
                                                   <div id="extrt_amchart_2" class="chart"></div>
                                               </div>
                                              </div>
                                           </div>
                                          
                                          <div class="m-grid-col m-grid-col-middle m-grid-col-center" style="padding: 10px; height: 400px;">
                                          <div class="portlet light bordered" style="margin-bottom:2px;">
                                                  <div class="portlet-title">
                                                  	<div class="caption">
                                     						 <i class="icon-bar-chart font-green-haze"></i>
                                     						 <span class="caption-subject bold uppercase font-red-haze">특정시간대 처리</span>
                                 						 </div>
                                                  </div>
                                                  <div class="poltlet-body">
                                                   <div id="extrt_amchart_4" class="chart"></div>
                                                </div>
                                          </div>
                                     		</div>
                                      </div>
                                    </div>
                                </div>
								</div>
								
								<!-- END PRIVACY SETTINGS TAB -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END PROFILE CONTENT -->
	</div>
</div>

<!-- END CONTENT BODY -->
</form>
</div>
<!-- END CONTENT BODY -->


