<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/search/thresholdSetting.js" type="text/javascript" charset="UTF-8"></script>


<h1 class="page-title">
	${currentMenuName}
	<!--bold font-blue-hoki <small>basic bootstrap tables with various options and styles</small> -->
</h1>
<div class="portlet light portlet-fit bordered">
	<div class="portlet-body">
		<!-- BEGIN FORM-->
		<form id="thresholdDetailForm" action="" method="POST"
			class="form-horizontal form-bordered form-row-stripped">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<th style="width: 15%; text-align: center;"><label class="control-label">시스템 명 </label></th>
						<td style="width: 35%; vertical-align: center;" class="form-group form-md-line-input">
						<select name="system_seq" id="system_seq" class="ticket-assign form-control input-medium" onchange="changeThresholdSystem(this.value);">
							<option value="">----선택----</option>
                            <c:forEach items="${systemMasterList}" var="systemMaster" varStatus="status">
                                <option value="${systemMaster.system_seq}"
                                    <c:if test='${systemMaster.system_seq eq search.system_seq}'>selected="selected"</c:if>>${systemMaster.system_name}</option>
                            </c:forEach>
                        </select>
						</td>
					</tr>
					<tr>
                        <th style="width: 15%; text-align: center;"><label class="control-label">사용여부</label></th>
                        <td class="form-group form-md-line-input" style="vertical-align: middle;">
                            <div class="md-radio-inline">
                                <div class="md-radio">
                                    <input type="radio" id="checkbox1_1" name="threshold_use" value="Y" class="md-radiobtn"  ${fn:containsIgnoreCase(thresholdInfo.threshold_use,'Y')?'checked':'' }>
                                    <label for="checkbox1_1"> <span></span> <span  class="check"></span> <span class="box"></span> 사용 </label>
                                </div>
                                <div class="md-radio">
                                    <input type="radio" id="checkbox1_2" name="threshold_use" value="N" class="md-radiobtn" ${fn:containsIgnoreCase(thresholdInfo.threshold_use,'N')?'checked':'' }>
                                    <label for="checkbox1_2"> <span></span> <span class="check"></span> <span class="box"></span> 미사용 </label>
                                </div>
                            </div>
                        </td>
                    </tr>
					<tr id="set_threshold_box" onclick="event.cancelBubble=true">
                        <th style="width: 15%; text-align: center;"><label class="control-label">시스템 임계치 설정</label></th>
                        <td class="form-group form-md-line-input" style="vertical-align: middle;">
                            <div class="md-radio-inline">
                                <fmt:parseNumber var="total" type="number" value="${thresholdInfo.threshold }" /> 
                                <div class="md-radio">
                                    <input type="radio" id="threshold_num_1" name="threshold" value="10" class="md-radiobtn" <c:if test="${(total eq 10)}">checked</c:if>> 
                                    <label for="threshold_num_1"> <span></span> <span
                                        class="check"></span> <span class="box"></span> 10 건
                                    </label>
                                </div>
                                <div class="md-radio">
                                    <input type="radio" id="threshold_num_2" name="threshold" value="20"
                                        class="md-radiobtn" <c:if test="${total eq 20 }">checked</c:if>>
                                    <label for="threshold_num_2"> <span></span> <span
                                        class="check"></span> <span class="box"></span> 20 건
                                    </label>
                                </div>
                                <div class="md-radio">
                                    <input type="radio" id="threshold_num_3" name="threshold" value="30"
                                        class="md-radiobtn" <c:if test="${total eq 30 }">checked</c:if>>
                                    <label for="threshold_num_3"> <span></span> <span
                                        class="check"></span> <span class="box"></span> 30 건
                                    </label>
                                </div>
                                <div class="md-radio">
                                    <input type="radio" id="threshold_num_4" name="threshold" value="40"
                                        class="md-radiobtn" <c:if test="${total eq 40 }">checked</c:if>>
                                    <label for="threshold_num_4"> <span></span> <span
                                        class="check"></span> <span class="box"></span> 40 건
                                    </label>
                                </div>
                                <div class="md-radio">
                                    <input type="radio" id="threshold_num_5" name="threshold" value="50"
                                        class="md-radiobtn" <c:if test="${total eq 50 }">checked</c:if>>
                                    <label for="threshold_num_5"> <span></span> <span
                                        class="check"></span> <span class="box"></span> 50 건
                                    </label>
                                </div>
                                <div class="md-radio" style="margin-right: 0;">
                                    <input type="radio" id="threshold_num_6" name="threshold" value="etc"
                                        class="md-radiobtn" <c:if test="${(total gt 50) or((total mod 10) ne 0 )}">checked</c:if>>
                                    <label for="threshold_num_6"> <span></span> <span
                                        class="check"></span> <span class="box"></span> 기타
                                    </label>
                                </div>
                                <div id="threshold_input_box" class="md-radio">
                                    <input style="padding-left: 10px;" type="text" class="form-control" id="threshold_total_inbox" name="threshold_total_inbox" onkeyup="checkNumber();" value="${total }">
                                </div>
                            </div>
                        </td>
                    </tr>
				</tbody>
			</table>

				<!-- 메뉴 관련 input 시작 -->
				<%-- <input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" /> 
				<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" /> 
				<input type="hidden" name="current_menu_id" value="${currentMenuId }" /> 
				<input type="hidden" name="seq" type="text" value="${exceptionExtDetail.seq}" />
				<input type="hidden" name="type" type="text" value="" /> --%>
			     <!-- 메뉴 관련 input 끝 -->

			<!-- 페이지 번호 -->
			<input type="hidden" name="page_num" value="${search.page_num }" />
		</form>
		<!-- END FORM-->

		<!-- 버튼 영역 -->
		<div class="form-actions">
			<div class="row">
				<div class="col-md-offset-2 col-md-10" align="right" style="padding-right: 30px;">
                    <button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
                        onclick="updateThresholdSetting();">
                        <i class="fa fa-check"></i> &nbsp;저장
                    </button>
				</div>
			</div>
		</div>
		<div>
			<div class="caption">
	            <i class="icon-settings font-dark"></i>
	            <span class="caption-subject font-dark uppercase">다운로드매핑정보</span>
	        </div>
			<table style="margin-top: 10px;" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th width="5%" style="text-align: center;">번호</th>
						<th width="20%" style="text-align: center;">URL주소</th>
						<th width="15%" style="text-align: center;">파일대장명</th>										
						<th width="20%" style="text-align: center;">파일명</th>
						<th width="20%" style="text-align: center;">사유</th>
						<th width="10%" style="text-align: center;">사용여부</th>
						<th width="10%" style="text-align: center;">파일대장</th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${empty dLogMenuMappList}">
							<tr>
								<td colspan="7" align="center">데이터가 없습니다.</td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach items="${dLogMenuMappList}" var="dl" varStatus="status">
								<tr>
									<td style="text-align: center;">${status.count}</td>
									<c:choose>
										<c:when test="${fn:length(dl.url) > 100}">
											<td>${fn:substring(dl.url, 0, 100)}...</td>
										</c:when>
										<c:otherwise>
											<td><ctl:nullCv nullCheck="${dl.url}" /></td>
										</c:otherwise>
									</c:choose>
									<td style="text-align: center;"><ctl:nullCv nullCheck="${dl.menu_name}" /></td>
									<c:choose>
										<c:when test="${fn:length(dl.file_name) > 15}">
											<td style="text-align: center" title="${dl.file_name}">${fn:substring(dl.file_name, 0, 15)}...</td>
										</c:when>
										<c:otherwise>
											<td style="text-align: center"><ctl:nullCv nullCheck="${dl.file_name}" /></td>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${fn:length(dl.reason) > 25}">
											<td title="${dl.reason}">${fn:substring(dl.reason, 0, 25)}...</td>
										</c:when>
										<c:otherwise>
											<td><ctl:nullCv nullCheck="${dl.reason}" /></td>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${dl.use_yn == 'Y'}">
											<td style="text-align: center; vertical-align: middle;"><span
												class="label label-sm label-success">사용</span></td>
										</c:when>
										<c:otherwise>
											<td style="text-align: center; vertical-align: middle;"><span
												class="label label-sm label-warning ">미사용</span></td>
										</c:otherwise>
									</c:choose>
									<c:choose>
									   <c:when test="${dl.register_status eq 'Y' }">
									       <td style="text-align: center; vertical-align: middle;"><span class="label label-sm label-success">등록</span></td>
									   </c:when>
									   <c:otherwise>
									       <td style="text-align: center; vertical-align: middle;"><span class="label label-sm label-warning">미등록</span></td>
									   </c:otherwise>
									</c:choose>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
	var thresholdSettingConfig = {
		"listUrl" : "${rootPath}/allLogInq/thresholdSetting.html",
		"saveUrl" : "${rootPath}/allLogInq/updateThreshold.html",
	};
</script>