<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/search/allLogInqList.js" type="text/javascript" charset="UTF-8"></script>
<link href="${rootPath}/resources/css/profile.min.css" rel="stylesheet" type="text/css">
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>
<style>
.tabbable-custom>.nav-tabs>li.active{
border-top: 3px solid #578ebe;
}
</style>

<h1 class="page-title"> ${currentMenuName}
    <!--bold font-blue-hoki <small>basic bootstrap tables with various options and styles</small> -->
</h1>

<div class="contents left">
 		

<div class="row" style="background:#eef1f5; padding-top: 20px; padding-bottom: 20px">
	<input type="hidden" id="emp_user_id" name="emp_user_id" value="${allLogInq.emp_user_id}">
	<div class="col-md-12">
		<!-- BEGIN PROFILE SIDEBAR -->
		<div class="profile-sidebar">
			<!-- PORTLET MAIN -->
			<div class="portlet light profile-sidebar-portlet "style=height:147%">
				<!-- SIDEBAR USERPIC -->
				<div class="profile-userpic" style="margin-bottom: -5px; ">
					<img src="${rootPath}/resources/image/common/profile_user2.png" class="img-responsive" alt="profile">
				</div>
				<!-- END SIDEBAR USERPIC -->
				<!-- SIDEBAR USER TITLE -->
				<div class="profile-usertitle">
					<div class="profile-usertitle-name"style="margin-bottom: 10px; "><ctl:nullCv nullCheck="${allLogInq.emp_user_name}"/></div>
					<%-- <div class="profile-usertitle-job"style="margin-bottom: 15px; ">(${data1.emp_user_id})</div> --%>
				</div>
				<!-- END SIDEBAR USER TITLE -->
				<!-- SIDEBAR BUTTONS -->
			</div>
			<!-- END PORTLET MAIN -->
			<!-- PORTLET MAIN -->
			<div class="portlet light" style="height: 550px;">
				<!-- STAT -->
				<div class="row">
					<h4 class="profile-desc-title" style="padding-bottom: 10px; ">프로필</h4>
					<div class="margin-top-20 profile-desc-link" style="margin-top: -20px !important;">
						<table class="table table-hover table-light">
	                        <tbody>
	                            <tr>
	                                <td> <b>소속</b> :
	                                <c:choose>
	                                	<c:when test="${masterflag eq 'Y' }"> 
	                                	<a onclick="fnExtrtDeptDetailInfo('${allLogInq.dept_id }', '${allLogInq.proc_date }')"><ctl:nullCv nullCheck="${allLogInq.dept_name}"/></a>
	                                	</c:when>
	                                	<c:otherwise>
	                                	${allLogInq.dept_name}
	                                	</c:otherwise>
	                                </c:choose>
	                                </td>
	                            </tr>
	                            <tr>
	                                <td> <b>IP</b> : ${allLogInq.user_ip}</td>
	                            </tr> 
	                            <tr>
	                            	<td> <b>ID</b> : ${allLogInq.emp_user_id} </td>
	                            </tr>
	                        </tbody>
                        </table>
					</div>
				</div>
				<div class="row list-separated profile-stat">
				  <h4 class="profile-desc-title">업무 시스템</h4>
					<div class="tab-context">
						<div  class="clearfix">
							<c:forEach items="${data2}" var="system" varStatus="status">
								<c:if test="${system.system_name != null}">
									<a style="cursor:default" class="btn btn-xs blue"
										<%-- <c:if test="${status.count % 4 == 0 }"> class="btn btn-xs red"</c:if>
										<c:if test="${status.count % 4 == 1 }"> class="btn btn-xs yellow"</c:if>
										<c:if test="${status.count % 4 == 2 }"> class="btn btn-xs green"</c:if>
										<c:if test="${status.count % 4 == 3 }"> class="btn btn-xs blue"</c:if>			 --%>				
										style="margin-top: 5px; margin-bottom: 5px;">${system.system_name}
										<i class="fa fa-laptop"></i>
			                       	 </a>
								</c:if>								
							</c:forEach>
	                    </div>
                    </div>
				</div>
				<!-- END STAT -->
				<div class="row" style="margin-top: -15px !important;">					
					<%-- <br><br><div class="margin-top-20 profile-desc-link" style="margin-top: 2px !important;t">
						
						<c:if test="${data1.dng_grade == '심각' }"><i class="fa fa-globe"></i><span style="font-size: 15px;"><b style="color: red;">심각 단계</b>는 <b><br>위험도 높은 비정상 개인정보 처리가 다수 발생되어 전사 차원의 매우 심각한 수준의 손실이나 해당 업무의 실패 혹은 경쟁적 지위를 상실할 수 있는 단계입니다.</b></span> </c:if>
						<c:if test="${data1.dng_grade == '경계' }"><i class="fa fa-globe"></i><span style="font-size: 15px;"><b style="color: orange;">경계 단계</b>는 <b><br>다수의 비정상 개인정보 처리가 발생되어 매우 심각한 손실이나 해당 업무가 어려워질 수 있는 정도로 업무에 중대한 영향이 발생할 수 있는 단계입니다.</b></span></c:if>
						<c:if test="${data1.dng_grade == '주의' }"><i class="fa fa-globe"></i><span style="font-size: 15px;"><b style="color: yellow;">주의 단계</b>는 <b><br>여러 항목의 비정상 개인정보 처리가 발생되어 손실이나 해당 업무에 부정적인 영향을 줄 수 있는 정도의 문제가 발생할 수 있는 단계입니다. </b></span></c:if>
						<c:if test="${data1.dng_grade == '관심' }"><i class="fa fa-globe"></i><span style="font-size: 15px;"><b style="color: blue;">관심 단계</b>는 <b><br>일부 비정상 개인정보 처리가 발생되어 해당영역 또는 해당서비스에 작은 영향을 줄 수 있을 정도의 문제가 발생할 수 있는 단계입니다. </b></span></c:if>
						<c:if test="${data1.dng_grade == '정상' }"><i class="fa fa-globe"></i><span style="font-size: 15px;"><b style="color: green;">정상 단계</b>는 <b><br>업무 수행 상 허용된 범위 내에 개인정보 처리가 발생되어 업무적으로 영향이 거의 없는 정도로 파급효과가 미약한 단계입니다.</b></span></c:if>
					</div> --%>
				</div>
			</div>
			<!-- END PORTLET MAIN -->
		</div>
		<!-- END BEGIN PROFILE SIDEBAR -->
		<!-- BEGIN PROFILE CONTENT -->
		<div class="profile-content">
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light"  style="min-height: 800px;">
						<!-- <div class="portlet-title tabbable-line">
							<div class="caption caption-md">
								<i class="icon-globe theme-font hide"></i> <span class="caption-subject font-blue-madison bold uppercase">Profile
									Account</span>
							</div>
						</div> -->
						<div class="portlet-title">
					        <div class="caption">
					            <i class="icon-settings font-dark"></i>
					            <span class="caption-subject font-dark sbold uppercase">로그정보</span>
					        </div>
					    </div>
						<div class="portlet-body">
							<table id="user" class="table table-bordered table-striped" style="table-layout: fixed">
		                     <tbody>
		                         <tr>
		                             <th style="width:15%;text-align: center;">일시 </th>
		                             <c:if test="${allLogInq.proc_date ne null and allLogInq.proc_time ne null}">
										<td style="width:35%;text-align: center;">
											<fmt:parseDate value="${allLogInq.proc_date}" pattern="yyyyMMdd" var="proc_date" /> 
											<fmt:formatDate value="${proc_date}" pattern="YY-MM-dd" />
											<fmt:parseDate value="${allLogInq.proc_time}" pattern="HHmmss" var="proc_time" /> 
											<fmt:formatDate value="${proc_time}" pattern="HH:mm:ss" />
										</td>
									</c:if>
									<th style="width:15%;text-align: center;"> 시스템명 </th>
		                             <td style="width:35%;text-align: center;">
		                                 <ctl:nullCv nullCheck="${allLogInq.system_name}"/>
		                             </td>
		                         </tr>
	                             <tr>
		                             <th style="text-align: center;"> 정보주체 </th>
		                             <td style="text-align: center;">
		                                 <%-- <ctl:nullCv nullCheck="${allLogInq.result_type}"/> --%>
		                                 <c:set var="prev" value=""/>
										<c:set var="cnt1" value="1"/>
										<c:set var="total" value="0"/>
										<c:forEach items="${allLogInq.result_type}">
											<c:set var="total" value="${total + 1 }"/>
										</c:forEach>
										<%-- <c:forTokens items="${allLogInq.result_type_sort}" delims="," var="item" varStatus="status">
											<c:choose>
												<c:when test="${prev ne item}">
													<c:if test="${status.index != 0 }">
														<c:forEach items="${CACHE_RESULT_TYPE}" var="i">
															<c:if test="${i.key == prev}">
																<img src="${rootPath }/resources/image/icon/resultType/${i.key}.png" title="${i.value }"/><c:if test="${cnt1 != 1 }">&nbsp;(${cnt1 })</c:if>
															</c:if>
														</c:forEach>					
													</c:if>
													<c:set var="prev" value="${item }"/>
													<c:set var="cnt1" value="1"/>
												</c:when>
												<c:otherwise>
													<c:set var="cnt1" value="${cnt1 + 1 }"/>
												</c:otherwise>
											</c:choose>
											<c:if test="${status.count ==  total}">
												<c:forEach items="${CACHE_RESULT_TYPE}" var="i">
													<c:if test="${i.key == prev}">
														<img src="${rootPath }/resources/image/icon/resultType/${i.key}.png" title="${i.value }"/><c:if test="${cnt1 != 1 }">&nbsp;(${cnt1 })</c:if>
													</c:if>
												</c:forEach>		
											</c:if>
											<c:forEach items="${CACHE_RESULT_TYPE}" var="i" varStatus="status">
												<c:if test="${i.key == item}">
													<img src="${rootPath }/resources/image/icon/resultType/${i.key}.png" title="${i.value }"/>
												</c:if>
											</c:forEach>
										</c:forTokens>	 --%>
										<%-- <c:set var="preValue" value="-1"/>
										<c:forTokens items="${allLogInq.result_type_sort}" delims="," var="item" varStatus="status">
											<c:forEach items="${CACHE_RESULT_TYPE}" var="i">
												<c:if test="${i.key == item && i.value != preValue}">
													<img src="${rootPath }/resources/image/icon/resultType/${i.key}.png" title="${i.value }"/>
													<c:set var="preValue" value="${i.value }"/>
												</c:if>
											</c:forEach>	
										</c:forTokens>	 --%>
										<c:forEach items="${allLogInq.result_type}" var="item" varStatus="status">
											<c:choose>
												<c:when test="${prev eq item}">	<!-- 이전값과 같을 때 -->
													<c:set var="cnt1" value="${cnt1 + 1 }"/>
													<c:if test="${status.count == total }">${cnt1 }</c:if>
												</c:when>
												<c:otherwise> 					<!-- 이전값과 다를 때 -->
													<c:if test="${status.count != 1}">${cnt1 }</c:if>
													<c:forEach items="${CACHE_RESULT_TYPE}" var="i">
														<c:if test="${i.key == item }">
															<img src="${rootPath }/resources/image/icon/resultType/${i.key}.png" title="${i.value }"/>
														</c:if>
													</c:forEach>
													
													<c:set var="prev" value="${item }"/>
													<c:set var="cnt1" value="1"/>
													<c:if test="${status.count == total }">${cnt1 }</c:if>
												</c:otherwise>
											</c:choose>
										</c:forEach>
		                             </td>
		                             <th style="text-align: center;"> 수행업무 </th>
		                             <td style="text-align: center;">
		                                 <c:forEach items="${CACHE_REQ_TYPE}" var="i" varStatus="status">
											<c:if test="${i.key == allLogInq.req_type}">
												<ctl:nullCv nullCheck="${i.value}"/>
											</c:if>
										</c:forEach>
		                             </td>
		                         </tr>
		                         <c:choose>
			                         <c:when test="${allLogInq.req_sql ne null }">
			                         	<tr>
			                         		<th style="text-align: center;"> SQL 정보 </th>
			                         		<td colspan="3" style="text-align: left;"><TEXTAREA rows="4" style="width: 100%;" readonly="readonly">${allLogInq.req_sql}</TEXTAREA></td>
			                         	</tr>
			                         	<tr>
			                         		<th style="text-align: center;"> SQL 응답 </th>
			                         		<td colspan="3" style="text-align: left;"><TEXTAREA rows="4" style="width: 100%;" readonly="readonly">${allLogInq.res_sql}</TEXTAREA></td>
			                         	</tr>
			                         </c:when>
			                         <c:otherwise>
			                         	<c:choose>
			                         		<c:when test="${log_sql_yn=='Y' }">
			                         			<tr><th style="text-align: center;"> DB주체 건수 </th><td style="text-align: center;">${logSqlResultCt } 건</td>
			                         			<c:if test="${scrn_name_view != 3}">
			                         			<th style="text-align: center;"> 메뉴명 </th><td style="text-align: center;"><ctl:nullCv nullCheck="${allLogInq.scrn_name}" /></td>
			                         			</c:if>
			                         			<c:if test="${scrn_name_view == 3}"><td colspan="2"></c:if></tr>
			                         		</c:when>
			                         		<c:otherwise>
			                         			<c:if test="${scrn_name_view != 3}">
			                         			<tr><th style="text-align: center;"> 메뉴명 </th><td colspan="3" style="text-align: center;"><ctl:nullCv nullCheck="${allLogInq.scrn_name}" /></td></tr>
			                         			</c:if>
			                         		</c:otherwise>
			                         	</c:choose>
				                         
				                         <tr>
				                         	<th style="text-align: center; vertical-align: middle;"> 접근 경로 </th>
											 <c:choose>
												<c:when test="${allLogInq.req_url eq null}">
													<td colspan="3" style="text-align: center;">-</td>
												</c:when>
												<c:otherwise>
													<td colspan="3" style="text-align: left;">
														<div style="overflow-y: auto; word-break: break-all; max-height: 100px;"><c:out value="${allLogInq.req_url}"/></div>
													</td>
												</c:otherwise>
											 </c:choose>
				                         </tr>
		                         	</c:otherwise>
		                         </c:choose>
		                         <c:if test="${biz_log_body_req eq 'Y'}">
		                         	<tr>
			                         	<th style="text-align: center; vertical-align: middle;"> 요청 본문 </th>
										 <c:choose>
											<c:when test="${detail_body.req_body eq null}">
												<td colspan="3" style="text-align: center;">-</td>
											</c:when>
											<c:otherwise>
												<td colspan="3" style="text-align: left;"><TEXTAREA rows="4" style="width: 100%;" readonly="readonly">${detail_body.req_body}</TEXTAREA></td>												
											</c:otherwise>
										 </c:choose>
			                         </tr>
			                     </c:if>
			                     <c:if test="${biz_log_body_res eq 'Y'}">
			                         <tr>
			                         	<th style="text-align: center; vertical-align: middle;"> 응답 본문 </th>
										 <c:choose>
											<c:when test="${detail_body.resp_body eq null}">
												<td colspan="3" style="text-align: center;">-</td>
											</c:when>
											<c:otherwise>
												<td colspan="3" style="text-align: left;"><TEXTAREA rows="4" style="width: 100%;" readonly="readonly">${detail_body.resp_body}</TEXTAREA></td>												
											</c:otherwise>
										 </c:choose>
			                         </tr>
		                         </c:if>
		                         
		                         <c:if test="${biz_log_sql eq 'Y'}">
		                         	<tr>
			                         	<th rowspan="${querylistSize+1 }" style="text-align: center; vertical-align: middle;"> 쿼리 </th>
										 <c:choose>
											<c:when test="${detail_query.query_id eq null}">
												<td colspan="3" style="text-align: center;">-</td>
											</c:when>
											<c:otherwise>
												<%-- <td colspan="3" style="text-align: left;"><TEXTAREA rows="4" style="width: 100%;" readonly="readonly">${detail_query.query_id}</TEXTAREA></td>	 --%>
												<%-- <td colspan="3" style="text-align: left;">
													<select size="3" style="width: 100%;" readonly="readonly">
													
													<c:set var="enter" value=""/>		
													<c:set var="querysplit" value="${fn:split(detail_query.query_id,enter)}"/>
													<c:forEach var="queryone" items="${querysplit}">
														<option style="padding: 8px;">${queryone }</option>
													</c:forEach>
													
													</select>
													<c:set var="enter" value=""/>		
													<c:set var="querysplit" value="${fn:split(detail_query.query_id,enter)}"/>
													<c:forEach var="queryone" items="${querysplit}">
														${queryone }<br/>
													</c:forEach>
													
												</td> --%>
												<%-- <c:set var="enter" value=""/>		
													<c:set var="querysplit" value="${fn:split(detail_query.query_id,enter)}"/>
													<c:forEach var="queryone" items="${querysplit}">
														<tr><td colspan="3" style="text-align: left;">${queryone }</td></tr>
														<tr><td colspan="3" style="text-align: left;">asd</td></tr>
														<tr><td colspan="3" style="text-align: left;">asd</td></tr>
														<tr><td colspan="3" style="text-align: left;">asd</td></tr>
													</c:forEach> --%>
													<!-- <tr><td colspan="3" style="text-align: left; word-break:break-all;" wrap="hard"> -->
												<c:forEach items="${querylist2 }" var="query" >
													<tr><td colspan="3" style="text-align: left; width:200px; height:50px;word-break:break-all;" >
													<div style="overflow:auto; height:40px; width:100%">${query }
													</div>
													</td>
													</tr>
												</c:forEach>
												
												<%-- <td colspan="3" rowspan="${querylistSize+1 }" >
												<div style="overflow:scroll;>
												<table>
												<tr>
												<td>dddsa</td>
												<td>dda</td>
												<td>dda</td></tr>
												</table>
												</div>
												</td> --%>
											</c:otherwise>
										 </c:choose>
			                         </tr>
		                         </c:if>
		                         
	                             <c:if test="${ui_type eq 'K'}">
		                         <tr>
		                             <th style="width:15%;text-align: center;"> GRID 정보 </th>
										<td style="width:35%;text-align: center;">
											<ctl:nullCv nullCheck="${allLogInq.grid_id}"/>
										</td>
									<th style="width:15%;text-align: center;"> RD 정보 </th>
		                             <td style="width:35%;text-align: center;">
		                                 <ctl:nullCv nullCheck="${allLogInq.rd_name}"/>
		                                 <c:if test="${allLogInq.rd_id ne '' and allLogInq.rd_id ne null}">(${allLogInq.rd_id })</c:if>
		                             </td>
		                         </tr>
		                         <tr>
		                         	<th style="text-align: center;"> 쿼리정보 </th>
									 <c:choose>
										<c:when test="${allLogInq.request_data eq null}">
											<td colspan="3" style="text-align: center;">-</td>
										</c:when>
										<c:otherwise>
											<td colspan="3" style="text-align: left;">${allLogInq.request_data}</td>												
										</c:otherwise>
									 </c:choose>
		                         </tr>
								</c:if>
		                     </tbody>
		                 </table>
						</div>
						
						<c:if test="${!empty allLogInqFileList }">
						<div class="portlet-title">
					        <div class="caption">
					            <i class="icon-settings font-dark"></i>
					            <span class="caption-subject font-dark sbold uppercase">파일정보</span>
					        </div>
					    </div>
					    <div class="portlet-body">
						    <div class="row">
						    	<div class="col-md-12">
						    		<table id="user" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer">
						    			<tbody>
					                         <tr>
					                         	 <th style="width:10%;text-align: center;">No. </th>
					                             <th style="width:60%;text-align: center;">문서명</th>
					                             <th style="width:15%;text-align: center;">문서유형</th>
					                             <th style="width:15%;text-align: center;">구분</th>
					                         </tr>
			                         		<c:forEach items="${allLogInqFileList }" var="file" varStatus="status">
					                         	<tr>
					                         		<td align="center">${status.count }</td>
					                         		<td align="left" style="margin-left: 10px">
					                         			<c:choose>
					                         				<c:when test="${sbiz_log_filedownload eq 'Y'}">
					                         					<a onclick="fileDownload('${file.file_path}', '${file.file_name}')">${file.file_name }</a>
					                         				</c:when>
					                         				<c:otherwise>
					                         					${file.file_name }
					                         				</c:otherwise>
					                         			</c:choose>
					                         		</td>
					                         		<td align="center">${file.file_ext }</td>
					                         		<td align="center">
					                         			<c:choose>
					                         				<c:when test="${file.file_direction eq 'UP' }">업로드</c:when>
					                         				<c:otherwise>다운로드</c:otherwise>
					                         			</c:choose>
					                         		</td>
					                         	</tr>
					                         </c:forEach>
					                    </tbody>
						    		</table>
						    	</div>
						    </div>
					    </div>
					    </c:if>
					    
					    <c:if test="${!empty allLogInqApprovalList }">
						<div class="portlet-title">
					        <div class="caption">
					            <i class="icon-settings font-dark"></i>
					            <span class="caption-subject font-dark sbold uppercase">결재라인정보</span>
					        </div>
					    </div>
					    <div class="portlet-body">
						    <div class="row">
						    	<div class="col-md-12">
						    		<table id="user" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer">
						    			<tbody>
					                         <tr>
					                         	 <th style="width:5%;text-align: center;">No. </th>
					                             <th style="width:15%;text-align: center;">결재일자</th>
					                             <th style="width:10%;text-align: center;">소속</th>
					                             <th style="width:10%;text-align: center;">사용자ID</th>
					                             <th style="width:10%;text-align: center;">사용자명</th>
					                             <th style="width:10%;text-align: center;">결재레벨</th>
					                             <th style="width:10%;text-align: center;">결재유형</th>
					                             <th style="width:30%;text-align: center;">결재결과</th>
					                         </tr> 
			                         		<c:forEach items="${allLogInqApprovalList }" var="approval" varStatus="status">
					                         	<tr>
					                         		<td align="center">${status.count }</td>
					                         		<td align="center" style="margin-left: 10px">
					                         		<c:choose>
					                         			<c:when test="${approval.approval_date != 'null' and approval.approval_date != ''}">
							                         		${approval.approval_date }
					                         			</c:when>
					                         			<c:otherwise>
					                         				-
					                         			</c:otherwise>
					                         		</c:choose>
					                         		</td>
					                         		<td align="center">${approval.approval_dept_name }</td>
					                         		<td align="center">${approval.approval_user_id }</td>
					                         		<td align="center">${approval.approval_user_name }</td>
					                         		<td align="center">${approval.approval_level }</td>
					                         		<td align="center">${approval.approval_type }</td>
					                         		<td align="center">${approval.approval_result }</td>
					                         	</tr>
					                         </c:forEach>
					                    </tbody>
						    		</table>
						    	</div>
						    </div>
					    </div>
					    </c:if>
	<c:choose>
		<c:when test="${log_sql_yn=='Y' }">
			<div class="portlet-body">
			<div class="tabbable tabbable-custom">
			    <ul class="nav nav-tabs">
			    	<li <c:if test="${tab_flag == 1 }">class="active"</c:if>>
			            <a href="#tab1" data-toggle="tab"> 요약정보 </a>
			        </li>
			        <li <c:if test="${tab_flag == 2 }">class="active"</c:if>>
			            <a href="#tab2" data-toggle="tab"> 참조정보 </a>
			        </li>
				</ul>
			
				<div class="tab-content">
					<div class="tab-pane <c:if test="${tab_flag == 1 }">active</c:if>" id="tab1">
						<%@include file="/WEB-INF/views/psm/search/allLogInqDetail_privacyInfo.jsp"%>
					</div>
					<div class="tab-pane <c:if test="${tab_flag == 2 }">active</c:if>" id="tab2">
						<%@include file="/WEB-INF/views/psm/search/allLogInqDetail_sqlInfo.jsp"%>
					</div>
				</div>
			</div>
		</div>
		</c:when>
		<c:otherwise>
			<%@include file="/WEB-INF/views/psm/search/allLogInqDetail_privacyInfo.jsp"%>
		</c:otherwise>
	</c:choose>		    
	    
					    
						
				         
				         <form id="listForm" method="POST">
							<!-- 메뉴 관련 input 시작 -->
							<input type="hidden" name="main_menu_id" value="${search.main_menu_id }" />
							<input type="hidden" name="sub_menu_id" value="${search.sub_menu_id }" />
							<input type="hidden" name="current_menu_id" value="${currentMenuId }" />
							<!-- 메뉴 관련 input 끝 -->
							
							<!-- 페이지 번호 -->
							<input type="hidden" name="page_num" value="${search.page_num }" />
							<input type="hidden" name="allLogInqDetail_page_num" value="${search.allLogInqDetail.page_num }" />
							<input type="hidden" name="extrtCondbyInqDetail_page_num" value="${search.extrtCondbyInqDetail.page_num }" />
							<input type="hidden" name="empDetailInqDetail_page_num" value="${search.empDetailInqDetail.page_num }" />
							
							<!-- 검색조건 관련 input 시작 -->
							<input type="hidden" name="search_from" value="${search.search_from }" />
							<input type="hidden" name="search_to" value="${search.search_to }" />
							<input type="hidden" name="emp_user_id" value="${search.emp_user_id }" />
							<input type="hidden" name="privacy" value="${search.privacy }" />
							<input type="hidden" name="daySelect" value="${search.daySelect }" />
							<input type="hidden" name="user_ip" value="${search.user_ip }" />
							<input type="hidden" name="privacyType" value="${search.privacyType }" />
							<input type="hidden" name="system_seq" value="${search.system_seq }" />
							<input type="hidden" name="req_url" value="${search.req_url }" />
							<c:if test="${search.isSearch eq 'Y' }">
							<input type="hidden" name="emp_user_name" value="${search.emp_user_name}" />
							</c:if>
							<input type="hidden" name="dept_name" value="${search.dept_name}" />
							<input type="hidden" name="dept_id" value="" />
							<input type="hidden" name="rule_cd" value="${search.rule_cd }" />
							<input type="hidden" name="mapping_id" value="${search.mapping_id }" />
							<input type="hidden" name="isSearch" value="${search.isSearch }" />
							<input type="hidden" name="req_type" value="${search.req_type }" />
							<!-- 검색조건 관련 input 끝 -->
							
							<!-- 상세 검색 조건 input 시작 -->
							<input type="hidden" name="dng_val" value="${search.dng_val }"/>
							<input type="hidden" name="detailLogSeq" value="${search.detailLogSeq }"/>
							<input type="hidden" name="detailProcDate" value="${search.detailProcDate }"/>
							<input type="hidden" name="detailProcTime" value="${search.proc_time }"/>
							<input type="hidden" name="bbs_id" value="${search.bbs_id }"/>
							<input type="hidden" name="detailOccrDt" value="${search.detailOccrDt}"/>
							<input type="hidden" name="detailEmpCd" value="${search.detailEmpCd }"/>
							<input type="hidden" name="detailStartDay" value="${search.detailStartDay }"/>
							<input type="hidden" name="detailEndDay" value="${search.detailEndDay }"/>
							<input type="hidden" name="detailEmpDetailSeq" value="${search.detailEmpDetailSeq}"/>
							<input type="hidden" name="misdetect_pattern" value=""/>
							<input type="hidden" name="privacy_type" value=""/>
							<input type="hidden" name="biz_log_result_seq" value="${search.biz_log_result_seq }"/>
							<input type="hidden" name="privacy_seq" value="${search.privacy_seq }"/>
							<input type="hidden" name="result_content" value="${search.result_content }"/>
							<input type="hidden" name="result_type" value="${search.result_type}"/>
							<input type="hidden" name="user_name" value="${search.emp_user_name}"/>
							<input type="hidden" name="sort_flag" value="${search.sort_flag}"/>
							<input type="hidden" name="userId" value =""/>
							<input type="hidden" name="detailResultType" value="${search.detailResultType }"/>
							<!-- 상세 검색 조건 input 끝 -->
							
							<!-- 로그상세 대상 프로필 정보 -->
							<input type="hidden" name="log_emp_user_id" value="${allLogInq.emp_user_name}"/>
							<input type="hidden" name="log_dept_name" value="${allLogInq.dept_name}"/>
							<input type="hidden" name="log_system_name" value="${allLogInq.system_name}"/>
							<input type="hidden" name="log_proc_date" value="${allLogInq.proc_date}"/>
							<input type="hidden" name="log_proc_time" value="${allLogInq.proc_time}"/>
							
							<input type="hidden" name="logSqlPageNum" value="${search.logSqlPageNum}"/>
							<input type="hidden" name="logSqlResultPageNum" value="${search.logSqlResultPageNum}"/>
							<input type="hidden" name="tab_flag"/>
						</form>
						
			             <form id="privacyForm" method="POST">
							<input type="hidden" name="privacy" value="" />
							<input type="hidden" name="privacyType" value="" />
							<input type="hidden" name="system_seq" value="${allLogInq.system_seq}" />
							<input type="hidden" name="emp_user_id" value="${allLogInq.emp_user_id}" />
						</form>
						<form id="logSqlForm" method="post">
							<input type="hidden" name="log_seq" value="${allLogInq.log_seq }" />
							<input type="hidden" name="log_sql_seq" value="${allLogSql.log_sql_seq }" />
							<input type="hidden" name="proc_date" value="${allLogSql.proc_datetime }" />
						</form>
				         
				         <div class="row">
				         	<div class="col-md-12" align="right">
				         		<!-- 버튼 영역 -->
								<div class="option_btn right" style="padding-right:10px;">
									<p class="right">
<%-- 										<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="showBizLog80Popup('${allLogInq.log_seq}')"><i class="fa fa-search"></i>80컬럼보기</a> --%>
										<c:if test="${masterflag eq 'Y' }">
											<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="fnExtrtEmpDetailInfo('${allLogInq.emp_user_id}', '${allLogInq.user_ip }', '${allLogInq.proc_date}', '${allLogInq.system_seq}')"><i class="fa fa-user"></i> 업무패턴보기</a>
										</c:if>
										<c:if test="${search.bbs_id eq null or empty search.bbs_id}">
											<a class="btn btn-sm grey-mint btn-outline sbold uppercase" onclick="javascript:goList();"><i class="fa fa-list"></i> 목록</a>
										</c:if>
										<c:if test="${search.bbs_id eq 'extrtCondbyInq' or search.bbs_id eq 'empDetailInq'}">
											<a class="btn btn-sm grey-mint btn-outline sbold uppercase" onclick="moveExtrtCondbyInqDetail();"><i class="fa fa-list"></i> 목록</a>
										</c:if>
										<c:if test="${search.bbs_id eq 'callingDemand'}">
											<a class="btn btn-sm grey-mint btn-outline sbold uppercase" onclick="moveExtrtCondbyInqDetail();"><i class="fa fa-list"></i> 목록</a>
										</c:if>
									</p>
								</div>
				         	</div>
				         </div>
				     </div>
					</div>
				</div>
			</div>
		</div>
		<!-- END PROFILE CONTENT -->
	</div>
</div>

</div>
<!-- END CONTENT BODY -->

	<form id="menuSearchForm" method="POST">
		<input type="hidden" name="main_menu_id" value="${search.main_menu_id }"/>
		<input type="hidden" name="sub_menu_id" value="${search.sub_menu_id }" />
		<input type="hidden" name="system_seq" value="${search.system_seq }" />
		<input type="hidden" name="emp_user_id" value ="${search.emp_user_id}"/>
		<input type="hidden" name="search_from" value ="${search.search_from}"/>
		<input type="hidden" name="search_to" value ="${search.search_to}"/>
		<input type="hidden" name="emp_user_name" value ="${search.emp_user_name}"/>
		<input type="hidden" name="user_ip" value ="${search.user_ip}"/>
		<input type="hidden" name="daySelect" value ="${search.daySelect}"/>
	</form>


<script type="text/javascript">

	var allLogInqConfig = {
		"listUrl":"${rootPath}/allLogInq/list.html"
		,"detailUrl":"${rootPath}/allLogInq/detail.html"
		,"addUrl":"${rootPath}/allLogInq/add.html"
		,"removeUrl":"${rootPath}/allLogInq/remove.html"
		,"loginPage" : "${rootPath}/loginView.html"
		,"deptDetailChartUrl" : "${rootPath}/extrtCondbyInq/deptDetailChart.html"
		,"goMisdetectUrl" : "${rootPath}/misdetect/list.html"
		,"downloadUrl" : "${rootPath}/allLogInq/downloadDetail.html"
		,"downloadCSVUrl" : "${rootPath}/allLogInq/downloadDetailCSV.html"
		,"privacyUrl" : "${rootPath}/allLogInq/findPrivacyInfo.html"
		,"logSqlExcel" : "${rootPath}/allLogInq/logSqlDownloadExcel.html"
		,"logSqlCss" : "${rootPath}/allLogInq/logSqlDownloadCsv.html"
	};
	var extrtCondbyInqConfig = {
		"detailChartUrl":"${rootPath}/extrtCondbyInq/detailChart.html",
		"detailUrl" : "${rootPath}/extrtCondbyInq/detail.html"
	};
	
</script>


