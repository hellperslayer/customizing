<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>

<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/search/onrLogInqList.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>

<style>
#loading {
 width: 100%;   
 height: 100%;   
 top: 0px;
 left: 0px;
 position: fixed;   
 display: block;   
 opacity: 0.7;   
 background-color: #fff;   
 z-index: 99;   
 text-align: center; }  
  
#loading-image {   
 position: absolute;   
 top: 50%;   
 left: 50%;  
 z-index: 100; } 
</style>

<script type="text/javascript">
$(document).ready(function() {
	$('#loading').show();
});

$(window).load(function() {   
	$('#loading').hide();
});
</script>

<div id="loading"><img id="loading-image" width="30px;" src="${rootPath}/resources/image/loading3.gif" alt="Loading..." /></div>


<h1 class="page-title">
	${currentMenuName}
	<!-- <small>Log-list</small> -->
</h1>
<div class="portlet light portlet-fit portlet-datatable bordered">
	<%-- <div class="portlet-title">
		<div class="caption">
			<i class="icon-settings font-dark"></i> <span
				class="caption-subject font-dark sbold uppercase">접속기록 리스트</span>
		</div>
		<div class="actions">
			<div class="btn-group">
				<a class="btn red btn-outline btn-circle"
					onclick="excelAllLogInqList('${search.total_count}')"
					data-toggle="dropdown"> <i class="fa fa-share"></i> <span
					class="hidden-xs"> 엑셀 </span>
				</a>
			</div>
		</div>
	</div> --%>
	<div class="portlet-body">
		<div class="table-container">

			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
				<!-- <div class="row">
		            <div class="col-md-8 col-sm-12">
		            	<div class="dataTables_info" id="datatable_ajax_2_info" role="status" aria-live="polite">Showing 22 to 30 of 178 entries</div>
		            </div>
		            
	            </div> -->
				<!-- 				<div class="dataTables_scroll" style="position: relative;"> -->
				<div>
					<div class="col-md-6" style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
						<div class="portlet box grey-salt  ">
                            <div class="portlet-title" style="background-color: #2B3643;">
                                <div class="caption">
                                    <i class="fa fa-search"></i>검색 & 엑셀 </div>
	                            <div class="tools">
					                 <a href="javascript:;" class="collapse"> </a>
					            </div>
                            </div>
                            <div class="portlet-body form">
                                <div class="form-body" style="padding-left:10px; padding-right:10px;">
                                    <div class="form-group">
                                        <form id="listForm" method="POST" class="mt-repeater form-horizontal">
                                            <div data-repeater-list="group-a">
                                                <div data-repeater-item class="mt-repeater-item">
                                                    <!-- jQuery Repeater Container -->
                                                    <div class="mt-repeater-input">
                                                        <label class="control-label">기간</label>
                                                        <br/>
                                                        <div class="input-group input-medium date-picker input-daterange"
															data-date="10/11/2012" data-date-format="yyyy-mm-dd">
															<input type="text" class="form-control" id="search_fr"
																name="search_from" value="${search.search_fromWithHyphen}"> 
															<span class="input-group-addon"> &sim; </span> 
															<input type="text" class="form-control" id="search_to" 
																name="search_to" value="${search.search_toWithHyphen}">
														</div> 
													</div>
													<div class="mt-repeater-input">
                                                        <input type="checkbox" id="searchTime" name="searchTime" onclick="searhTime(this)" <c:if test="${search.searchTime eq true }">checked</c:if> /> 
                                                        <label class="control-label">시간</label> 
                                                        <br/>
                                                        <div class="input-group input-xsmall input-daterange">
                                                        	<select style="width: 80px" class="form-control" id="start_h" name="start_h" <c:if test="${search.searchTime ne true }">disabled</c:if>>
	                                                        	<c:forEach var="item" varStatus="i" begin="0" end="23" step="1">
	                                                        		<option value="${item}" 
	                                                        		<c:if test="${item == (search.start_h) }">selected="selected"</c:if>>${item }시</option>
	                                                        	</c:forEach>
                                                        	</select>
                                                       		<span class="input-group-addon input-xsmall"> &sim; </span> 
	                                                        <select style="width: 80px" class="form-control" id="end_h" name="end_h" <c:if test="${search.searchTime ne true }">disabled</c:if>>
	                                                        	<c:forEach var="item" varStatus="i" begin="1" end="24" step="1">
	                                                        		<option value="${item}" 
	                                                        		<c:if test="${item == (search.end_h) }">selected="selected"</c:if>>${item }시</option>
	                                                        	</c:forEach>
	                                                        </select>
                                                        </div>
													</div>
                                                    <div class="mt-repeater-input">
                                                        <label class="control-label">기간선택</label>
                                                        <br/>
                                                        <select class="form-control"
															id="daySelect" name="daySelect"
															onclick="javascript:initDaySelect();">
															<option value="" class="daySelect_first"
																<c:if test='${search.daySelect eq ""}'>selected="selected"</c:if>>기간선택</option>
															<option value="Day"
																<c:if test='${search.daySelect eq "Day"}'>selected="selected"</c:if>>오늘</option>
															<option value="WeekDay"
																<c:if test='${search.daySelect eq "WeekDay"}'>selected="selected"</c:if>>일주일</option>
															<%-- <option value="MonthDay"
																<c:if test='${search.daySelect  eq "MonthDay"}'>selected="selected"</c:if>>전달
																1일부터 ~ 오늘</option>
															<option value="YearDay"
																<c:if test='${search.daySelect  eq "YearDay"}'>selected="selected"</c:if>>올해
																1월1일부터 ~ 오늘</option>
															<option value="TotalMonthDay"
																<c:if test='${search.daySelect eq "TotalMonthDay"}'>selected="selected"</c:if>>전달
																전체</option> --%>
														</select></div>
													<div class="mt-repeater-input">
                                                        <label class="control-label">시스템</label>
                                                        <br/>
                                                        <select name="system_seq"
															class="form-control">
															<option value=""
																${search.system_seq == '' ? 'selected="selected"' : ''}>
																----- 전 체 -----</option>
															<c:if test="${empty systemMasterList}">
																<option>시스템 없음</option>
															</c:if>
															<c:if test="${!empty systemMasterList}">
																<c:forEach items="${systemMasterList}" var="i"
																	varStatus="z">
																	<option value="${i.system_seq}"
																		${i.system_seq==search.system_seq ? "selected=selected" : "" }>${ i.system_name}</option>
																</c:forEach>
															</c:if>
														</select>
													</div>
                                                    <div class="mt-repeater-input">
                                                        <label class="control-label">개인정보유형</label>
                                                        <br/>
                                                        <select name="privacyType"
															class="form-control">
															<option value=""
																${search.privacyType == '' ? 'selected="selected"' : ''}>
																----- 전 체 -----</option>
															<c:if test="${empty CACHE_RESULT_TYPE}">
																<option>데이터 없음</option>
															</c:if>
															<c:if test="${!empty CACHE_RESULT_TYPE}">
																<c:forEach items="${CACHE_RESULT_TYPE}" var="i"
																	varStatus="z">
																	<option value="${i.key}"
																		${i.key==search.privacyType ? "selected=selected" : "" }>${ i.value}</option>
																</c:forEach>
															</c:if>
														</select>
														<div class="" style="padding-left: 0px;">
		                                                    <div class="mt-checkbox-inline">
		                                                        <label class="mt-checkbox mt-checkbox-outline" style="margin-bottom: 0px;">
		                                                            <input type="checkbox" name="check_file"
																	${search.check_file == 'on' ? 'checked="checked"' : ''}
																	style="float: none;" />비정형데이터
		                                                            <span></span>
		                                                        </label>
		                                                    </div>
			                                            </div>
                                                    </div>
                                                    <div class="mt-repeater-input">
                                                        <label class="control-label">사용자ID</label>
                                                        <br/>
                                                        <input type="text" class="form-control" name="emp_user_id" value="${search.emp_user_id}" />
                                                    </div>
                                                    <div class="mt-repeater-input">
                                                        <label class="control-label">사용자명</label>
                                                        <br/>
                                                        <input type="text" class="form-control" name="emp_user_name" value="${search.emp_user_name}" />
                                                    </div>
                                                    <div class="mt-repeater-input">
                                                        <label class="control-label">사용자IP</label>
                                                        <br/>
                                                        <input type="text" class="form-control" name="user_ip" value="${search.user_ip}" />
                                                    </div>
                                                    <div class="mt-repeater-input">
                                                        <label class="control-label">소속</label>
                                                        <br/>
                                                        <input type="text" class="form-control" name="dept_name" value="${search.dept_name}" />
                                                    </div>
                                                    <div class="mt-repeater-input">
                                                        <label class="control-label">접속유형</label>
                                                        <br/>
                                                        <select name="mapping_id"
															class="form-control">
															<option value="code1"
																${search.mapping_id == 'code1' ? 'selected="selected"' : ''}>ID
																로그인</option>
															<option value="code2"
																${search.mapping_id == null ? 'selected="selected"' : ''}>시스템
																로그인</option>
														</select>
                                                    </div>
                                                    <div class="mt-repeater-input">
                                                   	 	<label class="control-label">수행업무</label>
                                                        <br/>
                                                        <select name="req_type"
															class="form-control">
															<option value=""
																${search.req_type == '' ? 'selected="selected"' : ''}>
																----- 전 체 -----</option>
															<c:if test="${empty CACHE_REQ_TYPE}">
																<option>데이터 없음</option>
															</c:if>
															<c:if test="${!empty CACHE_REQ_TYPE}">
																<c:forEach items="${CACHE_REQ_TYPE}" var="i"
																	varStatus="z">
																	<option value="${i.key}"
																		${i.key==search.req_type ? "selected=selected" : "" }>${ i.value}</option>
																</c:forEach>
															</c:if>
														</select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div align="right">
	                                            <button type="reset"
													class="btn btn-sm red-mint btn-outline sbold uppercase"
													onclick="resetOptions(onrLogInqConfig['listUrl'])">
													<i class="fa fa-remove"></i> 초기화
												</button>
												<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
													onclick="moveOnrLogInqList()">
													<i class="fa fa-search"></i> 검색
												</button>&nbsp;&nbsp;
												<%-- <a class="btn red btn-outline btn-circle"
													onclick="excelAllLogInqList('${search.total_count}')"> 
													<i class="fa fa-share"></i> <span class="hidden-xs"> 엑셀 </span>
												</a> --%>
												<%-- <a onclick="excelAllLogInqList('${search.total_count}')"><img src="${rootPath}/resources/image/icon/XLS_3.png"></a> --%>
												<div class="btn-group">
													<a data-toggle="dropdown"> <img src="${rootPath}/resources/image/icon/XLS_3.png">
													</a>
													<ul class="dropdown-menu pull-right">
														<li><a onclick="excelAllLogInqList('${search.total_count}')"> EXCEL </a></li>
														<li><a onclick="csvAllLogInqList()"> CSV </a></li>
													</ul>
												</div>
											</div>
											
											<input type="hidden" name="detailLogSeq" value="" /> <input
												type="hidden" name="detailProcDate" value="" /> <input
												type="hidden" name="main_menu_id"
												value="${paramBean.main_menu_id }" /> <input type="hidden"
												name="sub_menu_id" value="${paramBean.sub_menu_id }" /> <input
												type="hidden" name="current_menu_id" value="${currentMenuId}" />
											<%-- <input type="hidden" name="system_seq" value="${paramBean.system_seq }" /> --%> 
											<input type="hidden" name="page_num" value="${search.page_num}" /> 
											<input type="hidden" name="menuNum" id="menuNum" value="${menuNum}" />
											<input type="hidden" name="menuCh" id="menuCh" value="${menuCh}" />
											<input type="hidden" name="menuId" id="menuId" value="${paramBean.sub_menu_id}" /> 
											<input type="hidden" name="menutitle" id="menutitle" value="${menuCh}${menuNum}" />
											<input type="hidden" name="dept_id" value="" /> 
											<input type="hidden" name="user_id" value="" />
											<input type="hidden" name="user_name" value="" />
											<input type="hidden" name="isSearch" value="${search.isSearch }" />
											<input type="hidden" name = "detailProcTime"  id = "detailProcTime" value=""/>
											<input type="hidden" name = "result_type"  id = "result_type" value=""/>
											<input type="hidden" name = "sort_flag"  id = "sort_flag" value="${search.sort_flag}"/>
											<input type="hidden" name = "detailOccrDt"  id = "detailOccrDt" value=""/>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>
				</div>
				<div>
					<table style="border-top: 1px solid #e7ecf1"
						class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
						style="position: absolute; top: 0px; left: 0px; width: 100%;">

						<thead>
						<c:choose>
						<c:when test="${scrn_name_view == 1 }">
							<tr role="row" class="heading" style="background-color: #c0bebe;">
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									일시</th>
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									소속</th>
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									사용자ID</th>
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center; padding: 0em;">
									사용자명</th>
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									사용자IP</th>
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									시스템명</th>
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									개인정보유형
									<img id ="sort_result_type" class="desc_btn" onclick="javascript:sendDesc(1)"
										src="${rootPath}/resources/image/common/desc_btn.png" alt="내림차순" title="내림차순" /></th>
								<th width="5%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center; padding: 0em;">
									수행업무</th>
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center; padding: 0em;">
									메뉴명</th>	
								<th width="15%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									접근 경로</th>
							</tr>
						</c:when>
						<c:when test="${scrn_name_view == 2 }">
							<tr role="row" class="heading" style="background-color: #c0bebe;">
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									일시</th>
								<th width="8%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									소속</th>
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									사용자ID</th>
								<th width="9%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center; padding: 0em;">
									사용자명</th>
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									사용자IP</th>
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									시스템명</th>
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									개인정보유형
									<img id ="sort_result_type" class="desc_btn" onclick="javascript:sendDesc(1)"
										src="${rootPath}/resources/image/common/desc_btn.png" alt="내림차순" title="내림차순" /></th>
								<th width="8%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center; padding: 0em;">
									수행업무</th>
								<th width="19%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									접근 경로</th>
							</tr>
						</c:when>
						<c:otherwise>
							<tr role="row" class="heading" style="background-color: #c0bebe;">
								<th width="13%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									일시</th>
								<th width="13%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									소속</th>
								<th width="13%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									사용자ID</th>
								<th width="13%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center; padding: 0em;">
									사용자명</th>
								<th width="13%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									사용자IP</th>
								<th width="13%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									시스템명</th>
								<th width="16%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									개인정보유형
									<img id ="sort_result_type" class="desc_btn" onclick="javascript:sendDesc(1)"
										src="${rootPath}/resources/image/common/desc_btn.png" alt="내림차순" title="내림차순" /></th>
								<th width="6%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center; padding: 0em;">
									수행업무</th>
							</tr>
						</c:otherwise>
						</c:choose>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${empty onrLogInqList.allLogInq}">
									<tr>
										<td colspan="10" align="center">데이터가 없습니다.</td>
									</tr>
								</c:when>
								<c:otherwise>
									<c:set value="${onrLogInqList.page_total_count}" var="count" />
									<c:forEach items="${onrLogInqList.allLogInq}" var="onrLogInq"
										varStatus="status1">
										<tr style='cursor: pointer;'
											onclick="javascript:fnOnrLogInqDetail('${onrLogInq.log_seq}', '${onrLogInq.proc_date}');">

											<c:if test="${onrLogInq.proc_date ne null and onrLogInq.proc_time ne null}">
												<td style="text-align: center;">
													<fmt:parseDate value="${onrLogInq.proc_date}" pattern="yyyyMMdd" var="proc_date" /> 
													<fmt:formatDate value="${proc_date}" pattern="yy-MM-dd" /> 
													<fmt:parseDate value="${onrLogInq.proc_time}" pattern="HHmmss" var="proc_time" /> 
													<fmt:formatDate value="${proc_time}" pattern="HH:mm:ss" />
												</td>
											</c:if>
											<c:if test="${onrLogInq.proc_date eq null or onrLogInq.proc_time eq null}">
												<td style="text-align: center;">-</td>
											</c:if>
											<td style="text-align: center;"><ctl:nullCv nullCheck="${onrLogInq.dept_name}" /></td>
											<td style="text-align: center;"><ctl:nullCv nullCheck="${onrLogInq.emp_user_id}" /></td>
											<td style="text-align: center;"><ctl:nullCv nullCheck="${onrLogInq.emp_user_name}" /></td>
											<td style="text-align: center;"><ctl:nullCv nullCheck="${onrLogInq.user_ip}" /></td>
											<td style="text-align: center;"><ctl:nullCv nullCheck="${onrLogInq.system_name}" /></td>

											<c:choose>
											<c:when test="${result_type_view eq 'N' }">
											<!-- result_type 잘라서뿌리기 -->
											<c:if test="${onrLogInq.result_type ne null}">
												<c:set var="result_type_split"
													value="${fn:split(onrLogInq.result_type, ',')}" />
												<c:set var="result_type_string" value="" />
												<c:forEach items="${result_type_split}"
													var="result_type_split_value">
													<c:forEach items="${CACHE_RESULT_TYPE}" var="i"
														varStatus="status">
														<c:if test="${i.key == result_type_split_value}">
															<c:set var="result_type_string"
																value="${result_type_string} ${i.value}" />
														</c:if>
													</c:forEach>
												</c:forEach>
												<c:choose>
													<c:when test="${fn:length(result_type_string) > 8}">
														<td title="${result_type_string}">
															${fn:split(result_type_string, " ")[0]} 외
															${fn:length(fn:split(result_type_string, " ")) - 1}개의 개인정보</td>
													</c:when>
													<c:otherwise>
														<td><ctl:nullCv nullCheck="${result_type_string}" />
														</td>
													</c:otherwise>
												</c:choose>
											</c:if>
											<c:if test="${onrLogInq.result_type eq null}"><td>-</td></c:if>
											</c:when>
											<c:when test="${result_type_view eq 'Y' }">
											<c:if test="${onrLogInq.result_type ne null}">
											 
											<td <c:if test="${onrLogInq.check_exc eq '1' }">bgcolor='yellow' </c:if>>
											<c:set var="prev" value=""/>
											<c:set var="cnt1" value="1"/>
											<c:set var="total" value="0"/>
											<c:forEach items="${onrLogInq.result_type}">
												<c:set var="total" value="${total + 1 }"/>
											</c:forEach>
											
											<c:forEach items="${onrLogInq.result_type}" var="item" varStatus="status">
												<c:choose>
													<c:when test="${prev eq item}">	<!-- 이전값과 같을 때 -->
														<c:set var="cnt1" value="${cnt1 + 1 }"/>
														<c:if test="${status.count == total }">${cnt1 }</c:if>
													</c:when>
													<c:otherwise> 					<!-- 이전값과 다를 때 -->
														<c:if test="${status.count != 1}">${cnt1 }</c:if>
														<c:forEach items="${CACHE_RESULT_TYPE}" var="i">
															<c:if test="${i.key == item }">
																<img src="${rootPath }/resources/image/icon/resultType/${i.key}.png" title="${i.value }"/>
															</c:if>
														</c:forEach>
														
														<c:set var="prev" value="${item }"/>
														<c:set var="cnt1" value="1"/>
														<c:if test="${status.count == total }">${cnt1 }</c:if>
													</c:otherwise>
												</c:choose>
											</c:forEach>
											</td>
											</c:if>
											<c:if test="${onrLogInq.result_type eq null}">
												<td>-</td>
											</c:if>
											</c:when>
											</c:choose>

											<td style="text-align: center;"><c:forEach
													items="${CACHE_REQ_TYPE}" var="i" varStatus="status">
													<c:if test="${i.key == onrLogInq.req_type}">
														<ctl:nullCv nullCheck="${i.value}" />
													</c:if>
												</c:forEach></td>
											
											<c:choose>
											<c:when test="${scrn_name_view == 1 }">
												<c:choose>
													<c:when test="${onrLogInq.scrn_name eq null || onrLogInq.scrn_name eq '' || onrLogInq.scrn_name eq 'null' }">
														<td style="text-align: center;">-</td>
													</c:when>
													<c:otherwise>
														<td style="text-align: left;">${onrLogInq.scrn_name}</td>
													</c:otherwise>
												</c:choose>
												<c:choose>
													<c:when test="${onrLogInq.req_url eq null}">
														<td style="text-align: center;">-</td>
													</c:when>
													<c:otherwise>
														<c:choose>
															<c:when test="${fn:length(onrLogInq.req_url) > 35}">
																<td style="text-align: left">
																	<c:out value="${fn:substring(onrLogInq.req_url, 0, 35)}..."/>
																</td>
															</c:when>
															<c:otherwise>
																<td style="text-align: left">
																	${onrLogInq.req_url}
																</td>
															</c:otherwise>
														</c:choose>
													</c:otherwise>
												</c:choose>
											</c:when>
											<c:when test="${scrn_name_view == 2 }">
											<c:if test="${onrLogInq.scrn_name eq null || onrLogInq.scrn_name eq '' || onrLogInq.scrn_name eq 'null'}">
												<c:if test="${onrLogInq.req_url ne null}">
													<c:choose>
														<c:when test="${fn:length(onrLogInq.req_url) > 35}">
															<td style="text-align: left">
																<c:out value="${fn:substring(onrLogInq.req_url, 0, 35)}..."/></td>
														</c:when>
														<c:otherwise>
															<td style="text-align: left">
															<ctl:nullCv nullCheck="${onrLogInq.req_url}" /></td>
														</c:otherwise>
													</c:choose>
												</c:if>

												<c:if test="${onrLogInq.req_url eq null}">
													<td>-</td>
												</c:if>
											</c:if>
											<c:if 
												test="${onrLogInq.scrn_name ne null && onrLogInq.scrn_name ne '' && onrLogInq.scrn_name ne 'null'}">
												<td >${onrLogInq.scrn_name}</td>
											</c:if>
											</c:when>
										</c:choose>
										</tr>
										<c:set var="count" value="${count - 1 }" />
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
				</div>

				<form id="menuSearchForm" method="POST">
					<input type="hidden" name="main_menu_id"
						value="${paramBean.main_menu_id }" /> <input type="hidden"
						name="sub_menu_id" value="${paramBean.sub_menu_id }" /> <input
						type="hidden" name="system_seq" value="${paramBean.system_seq }" />
					<input type="hidden" name="emp_user_id"
						value="${search.emp_user_id}" /> <input type="hidden"
						name="search_from" value="${search.search_from}" /> <input
						type="hidden" name="search_to" value="${search.search_to}" /> 
					<input type="hidden" name="user_ip" value="${search.user_ip}" /> <input
						type="hidden" name="daySelect" value="${search.daySelect}" />
					<input type="hidden" name="emp_user_name" value="${search.emp_user_name }" />

				</form>

				<div class="row" style="padding: 10px;">
					<!-- <div class="col-md-8 col-sm-12">
			            <div class="dataTables_info">Showing 22 to 30 of 178 entries
			            </div>
		            </div>
		            <div class="col-md-4 col-sm-12"></div> -->
					<!-- 페이징 영역 -->
					<c:if test="${search.total_count > 0}">
						<div id="pagingframe" align="center">
							<p>
								<ctl:paginator currentPage="${search.page_num}"
									rowBlockCount="${search.size}"
									totalRowCount="${search.total_count}" />
							</p>
						</div>
					</c:if>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	/* $('#search_fr, #search_to').datepicker({
	 format: "yyyy-mm-dd",
	 language: "kr",
	 todayHighlight: true
	 });

	 $('#search_fr').datepicker()
	 .on('changeDate', function(ev){
	 var date1 = $('#search_fr').val();
	 $('#search_to').datepicker('setStartDate', date1);
	 });
	 $('#search_to').datepicker()
	 .on('changeDate', function(ev){
	 var date2 = $('#search_to').val();
	 $('#search_fr').datepicker('setEndDate', date2);
	 });  */
	 
	var onrLogInqConfig = {
		"reqUrl" : "${rootPath}/reqLogInq/list.html",
		"listUrl" : "${rootPath}/onrLogInq/list.html",
		"detailUrl" : "${rootPath}/onrLogInq/detail.html",
		"downloadUrl" : "${rootPath}/onrLogInq/download.html",
		"downloadCSVUrl" : "${rootPath}/onrLogInq/downloadCSV.html",
		"deptDetailChartUrl" : "${rootPath}/extrtCondbyInq/deptDetailChart.html"
	};
</script>