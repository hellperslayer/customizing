<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="Preview page of Metronic Admin Theme #1 for dark mega menu option" name="description" />
<meta content="" name="author" />

<div class="tab-pane <c:if test="${tabFlag == 'tab5' }">active</c:if>" id="tab5">	  
	<div class="row">
		<div class="mt-element-step" style="margin:20px;">
			<div class="row step-line">
				<table class="table table-striped table-bordered table-hover order-column">
					<colgroup>
						<col width="50%" />
						<col width="25%" />
						<col width="25%" />
					</colgroup>
					<thead>
						<tr>
							<th scope="col" style="text-align: center;">추출조건명</th>
							<th scope="col" style="text-align: center;">개인누적 평균건수</th>
							<th scope="col" style="text-align: center;">임계치</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${empty avgList}">
								<tr>
									<td colspan="3" align="center">데이터가 없습니다.</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach items="${avgList}" var="avg" varStatus="status">
									<tr>
										<td style="text-align: left; padding-left: 20px;">${avg.rule_nm }</td>
										<td style="text-align: center;">${avg.avg_val } 건</td>
										<c:choose>
											<c:when test="${avg.use_yn eq 'Y' }">
												<td style="text-align: center;"><b>${avg.limit_val }</b> (${avg.prct_int }%)</td>
											</c:when>
											<c:otherwise>
												<td style="text-align: center;">${avg.limit_cnt }</td>
											</c:otherwise>
										</c:choose>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>
	</div>
</div>