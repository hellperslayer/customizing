<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<c:set var="currentMenuId" value="${index_id}" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />


<script
	src="${rootPath}/resources/js/psm/search/summonDelegation.js"
	type="text/javascript" charset="UTF-8"></script>
<script
	src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js"
	type="text/javascript"></script>	

<style type="text/css">
.input_date {
	background-color: #eef1f5;
	padding: 0.3em 0.5em;
	border: 1px solid #e4e4e4;
	height: 30px;
	font-size: 12px;
	color: #555;
}

</style>

<h1 class="page-title">${currentMenuName}</h1>
<div class="portlet light portlet-fit portlet-datatable bordered" style="margin-top: 70px;">
	<div class="portlet-body" style="display:grid;">
		<div class="table-container">
		<form id="addSummonDelegate" method="post">
			<div class="col-md-4" style=" margin-top: 10px; padding-left: 0px; padding-right: 0px;">
				<div class="portlet box grey-salt  ">
	                <div class="portlet-title" style="background-color: #2B3643;">
                    	<div class="caption">
                    		소명 대상부서
                    	</div>	
	                </div>
	                <div id="searchBar" class="portlet-body form">
	                    <div class="form-body" style="padding-left:10px; padding-right:10px;">
	                        <div class="form-group">
	                        	<div class="row">
	                        		<div class="col-md-6">
										<label class="control-label">부서명</label> 
										<select name="dept_id" class="form-control" onchange="selectDept('dept_id',this)">
											<option value="nodata">----- 전 체 -----</option>
											<c:if test="${empty departmentList}">
												<option>등록 부서 없음</option>
											</c:if>
											<c:if test="${!empty departmentList}">
												<c:forEach items="${departmentList}" var="i" varStatus="z">
													<option value="${i.dept_id}" ${i.dept_id==search.dept_id ? "selected=selected" : "" }>${ i.dept_name}</option>
												</c:forEach>
											</c:if>
										</select>
										<div style="height: 250px; overflow: auto; border: none"></div>
									</div>
	                        		<div class="col-md-6">
										<label class="control-label">부서ID</label> 
										<div id="dept_id" class="form-control">
										</div>
										<div style="height: 250px; overflow: auto; border: none"></div>
									</div>
	                        	</div>
	                        </div>
	                    </div>
	                </div>
	            </div>
			</div>
			<div class="col-md-4" style=" margin-top: 10px; padding-left: 20px; padding-right: 0px;">
				<div class="portlet box grey-salt  ">
	                <div class="portlet-title" style="background-color: #2B3643;">
	                   <div class="caption">
                    		소명 위임자
                    	</div>	
	                </div>
	                <div id="searchBar" class="portlet-body form">
	                    <div class="form-body" style="padding-left:10px; padding-right:10px;">
	                        <div class="form-group">
	                        	<div class="row">
	                        		<div class="col-md-6">
										<label class="control-label">시스템</label> 
										<select name="summon_system_seq" class="form-control" onchange="findEmpUser('summon_id',this)">
											<option value="nodata">----- 전 체 -----</option>
											<c:if test="${empty systemMasterList}">
												<option>시스템 없음</option>
											</c:if>
											<c:if test="${!empty systemMasterList}">
												<c:forEach items="${systemMasterList}" var="i" varStatus="z">
													<option value="${i.system_seq}" ${i.system_seq==search.system_seq ? "selected=selected" : "" }>${ i.system_name}</option>
												</c:forEach>
											</c:if>
										</select>
										<div style="height: 250px; overflow: auto; border: none"></div>
									</div>
	                        		<div class="col-md-6">
										<label class="control-label">직원명</label> 
										<select id="summon_id" name="summon_id" class="form-control">
											<option value="nodata">----- 선 택 -----</option>
										</select>
									</div>
	                        	</div>
	                        </div>
	                    </div>
	                </div>
	            </div>
			</div>
			<div class="col-md-3" style=" margin-top: 10px; padding-left: 20px; padding-right: 0px;">
				<div class="portlet box grey-salt  ">
	                <div class="portlet-title" style="background-color: #2B3643;">
	                   <div class="caption">
                    		기간
                    	</div>	
	                </div>
	                <div id="searchBar" class="portlet-body form">
	                    <div class="form-body" style="padding-left:10px; padding-right:10px;">
	                        <div class="form-group">
	                        	<div class="row">
	                        		<div class="col-md-12">
										<label class="control-label">기간</label>
										<div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
											<input type="text" class="form-control" id="delegate_ft" name="delegation_from" value="${parameters.delegation_from}"> 
											<span class="input-group-addon"> &sim; </span> 
											<input type="text" class="form-control" id="delegate_to" name="delegation_to" value="${parameters.delegation_to}">
										</div>
										<div style="height: 250px; overflow: auto; border: none"></div>
									</div>
	                        	</div>
	                        </div>
	                    </div>
	                </div>
	            </div>
			</div>
			<div class="col-md-1"
				style="margin-top: 10px; padding-left: 0px; padding-right: 0px;">
				<div class="portlet box" style="text-align: center">
					<div style="height: 50px; overflow: auto; border: none"></div>
						<button type="button"
							class="btn btn-sm blue btn-outline sbold uppercase"
							onclick="addSummonDelegate()">
							<i class="fa fa-check"></i> 적용
						</button>
	                
				</div>
			</div>
		</form>
		<form id="searchForm" method="post" action="">
			<div class="col-md-12" style=" margin-top: 10px; padding-left: 0px; padding-right: 0px;">
				<div class="portlet box grey-salt  ">
	                <div class="portlet-title" style="background-color: #2B3643;">
                    	<div class="caption">
                    		위임목록
                    	</div>	
	                </div>
	                <div id="searchBar" class="portlet-body form">
	                    <div class="form-body" style="padding-left:10px; padding-right:10px;">
	                        <div class="form-group">
	                        	<div class="row">
	                        		<div class="col-md-2">
										<label class="control-label">부서명</label> 
										<select name="dele_dept_id" class="form-control" onchange="selectDept('dele_dept_id',this)">
											<option value="nodata">----- 전 체 -----</option>
											<c:if test="${empty departmentList}">
												<option>등록 부서 없음</option>
											</c:if>
											<c:if test="${!empty departmentList}">
												<c:forEach items="${departmentList}" var="i" varStatus="z">
													<option value="${i.dept_id}" ${i.dept_id==parameters.dele_dept_id ? "selected=selected" : "" }>${ i.dept_name}</option>
												</c:forEach>
											</c:if>
										</select>
									</div>
	                        		<div class="col-md-2">
										<label class="control-label">부서ID</label> 
										<div id="dele_dept_id" name="dele_emp_user_id" class="form-control">
										</div>
									</div>
	                        		<div class="col-md-2">
										<label class="control-label">시스템</label> 
										<select name="dele_summon_system_seq" class="form-control" onchange="findEmpUser('dele_summon_id',this)">
											<option value="nodata">----- 전 체 -----</option>
											<c:if test="${empty systemMasterList}">
												<option>시스템 없음</option>
											</c:if>
											<c:if test="${!empty systemMasterList}">
												<c:forEach items="${systemMasterList}" var="i" varStatus="z">
													<option value="${i.system_seq}" ${i.system_seq==parameters.dele_summon_system_seq ? "selected=selected" : "" }>${ i.system_name}</option>
												</c:forEach>
											</c:if>
										</select>
									</div>
	                        		<div class="col-md-2">
										<label class="control-label">직원명</label> 
										<select id="dele_summon_id" name="dele_summon_id" class="form-control">
											<option value="nodata">----- 선 택 -----</option>
										</select>
									</div>
									<div class="col-md-2">
										<label class="control-label">기간</label>
										<div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
											<input type="text" class="form-control" id="delegate_search_fr" name="delegate_search_fr" value="${parameters.delegate_search_fr}" > 
											<span class="input-group-addon"> &sim; </span> 
											<input type="text" class="form-control" id="delegate_search_to" name="delegate_search_to" value="${parameters.delegate_search_to}" >
										</div>
									</div>
									<div class="col-md-2">
										<label class="control-label">검색</label>
										<div class="box" style="text-align: center">
											<button type="button"
												class="btn btn-sm blue btn-outline sbold uppercase"
												onclick="searchList()">
												<i class="fa fa-check"></i> 검색
											</button>
										</div>
									</div>
	                        	</div>
	                        	<div class="row" style="padding: 0px 15px">
	                        		<table style="border-top: 1px solid #e7ecf1"
										class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
										style="position: absolute; top: 0px; left: 0px; width: 100%;">
										<thead>
											<tr role="row" class="heading" style="background-color: #c0bebe;">
												<th width="16.5%"
												style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
												부서명
												</th>
												<th width="16.5%"
												style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
												부서ID
												</th>
												<th width="16.5%"
												style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
												시스템
												</th>
												<th width="16.5%"
												style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
												직원명
												</th>
												<th width="17%"
												style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
												기간
												</th>
												<th width="17%"
												style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
												삭제
												</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="item" items="${delegateList }">
												<tr>
													<td align="center">${item.dept_name }</td>
													<td align="center">${item.dept_id }</td>
													<td align="center">${item.summon_system_name }</td>
													<td align="center">${item.summon_name }</td>
													<td align="center">
														<fmt:parseDate var="dateString" value="${item.delegation_from }" pattern="yyyyMMdd" />
														<fmt:formatDate value="${dateString}" pattern="yyyy-MM-dd" /> ~ 
														<fmt:parseDate var="dateString" value="${item.delegation_to }" pattern="yyyyMMdd" />
														<fmt:formatDate value="${dateString}" pattern="yyyy-MM-dd" />
													</td>
													<td style="text-align: center;padding: 2px">
														<button type="button" class="btn btn-sm red-mint btn-outline sbold uppercase" onclick="deleteSummonDelegate('${item.summon_seq}')">
															<i class="fa fa-close"></i> 삭제
														</button>
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
	                        	</div>
	                        </div>
	                    </div>
	                </div>
	            </div>
			</div>
			
			<div class="col-md-12" style=" margin-top: 10px; padding-left: 0px; padding-right: 0px;">
				<div class="portlet box grey-salt  ">
	                <div class="portlet-title" style="background-color: #2B3643;">
                    	<div class="caption">
                    		위임이력
                    	</div>	
	                </div>
	                <div id="searchBar" class="portlet-body form">
	                    <div class="form-body" style="padding-left:10px; padding-right:10px;">
	                        <div class="form-group">
	                        	<div class="row">
	                        		<div class="col-md-2">
										<label class="control-label">부서명</label> 
										<select name="delehist_dept_id" class="form-control" onchange="selectDept('delehist_dept_id',this)">
											<option value="nodata">----- 전 체 -----</option>
											<c:if test="${empty departmentList}">
												<option>등록 부서 없음</option>
											</c:if>
											<c:if test="${!empty departmentList}">
												<c:forEach items="${departmentList}" var="i" varStatus="z">
													<option value="${i.dept_id}" ${i.dept_id==parameters.delehist_dept_id ? "selected=selected" : "" }>${ i.dept_name}</option>
												</c:forEach>
											</c:if>
										</select>
									</div>
	                        		<div class="col-md-2">
										<label class="control-label">부서ID</label> 
										<div id="delehist_dept_id" name=delehist_emp_user_id class="form-control">
										</div>
									</div>
	                        		<div class="col-md-2">
										<label class="control-label">시스템</label> 
										<select name="delehist_summon_system_seq" class="form-control" onchange="findEmpUser('delehist_summon_id',this)">
											<option value="nodata">----- 전 체 -----</option>
											<c:if test="${empty systemMasterList}">
												<option>시스템 없음</option>
											</c:if>
											<c:if test="${!empty systemMasterList}">
												<c:forEach items="${systemMasterList}" var="i" varStatus="z">
													<option value="${i.system_seq}" ${i.system_seq==parameters.delehist_summon_system_seq ? "selected=selected" : "" }>${ i.system_name}</option>
												</c:forEach>
											</c:if>
										</select>
									</div>
	                        		<div class="col-md-2">
										<label class="control-label">직원명</label> 
										<select id="delehist_summon_id" name="delehist_summon_id" class="form-control">
											<option value="nodata">----- 선 택 -----</option>
										</select>
									</div>
									<div class="col-md-2">
										<label class="control-label">기간</label>
										<div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
											<input type="text" class="form-control" id="search_hist_fr" name="delegate_hist_search_fr" value="${parameters.delegate_hist_search_fr}"  > 
											<span class="input-group-addon"> &sim; </span> 
											<input type="text" class="form-control" id="search_hist_to" name="delegate_hist_search_to" value="${parameters.delegate_hist_search_to}"  >
										</div>
									</div>
									<div class="col-md-2">
										<label class="control-label">검색</label>
										<div class="box" style="text-align: center">
											<button type="button"
												class="btn btn-sm blue btn-outline sbold uppercase"
												onclick="searchList()">
												<i class="fa fa-check"></i> 검색
											</button>
										</div>
									</div>
	                        	</div>
	                        	<div class="row" style="padding: 0px 15px">
	                        		<table style="border-top: 1px solid #e7ecf1"
										class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
										style="position: absolute; top: 0px; left: 0px; width: 100%;">
										<thead>
											<tr role="row" class="heading" style="background-color: #c0bebe;">
												<th width="16.5%"
												style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
												부서명
												</th>
												<th width="16.5%"
												style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
												부서ID
												</th>
												<th width="16.5%"
												style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
												시스템
												</th>
												<th width="16.5%"
												style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
												직원명
												</th>
												<th width="17%"
												style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
												기간
												</th>
												<th width="17%"
												style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
												삭제
												</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach var="item" items="${delegateHistList }">
												<tr>
													<td align="center">${item.dept_name }</td>
													<td align="center">${item.dept_id }</td>
													<td align="center">${item.summon_system_name }</td>
													<td align="center">${item.summon_name }</td>
													<td align="center">
														<fmt:parseDate var="dateString" value="${item.delegation_date }" pattern="yyyyMMdd" />
														<fmt:formatDate value="${dateString}" pattern="yyyy-MM-dd" />
													</td>
													<td style="text-align: center">
														<c:choose>
															<c:when test="${item.delegation_act eq 'grant' }">
																권한부여
															</c:when>
															<c:otherwise>
																권한취소
															</c:otherwise>
														</c:choose>
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
	                        	</div>
	                        </div>
	                    </div>
	                </div>
	            </div>
			</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	var summonDelegation = {
		"listUrl" : "${rootPath}/extrtCondbyInq/summonDelegateDept.html"
		,"addUrl" : "${rootPath}/extrtCondbyInq/summonDelegateDeptAdd.html"
		,"deleteUrl" : "${rootPath}/extrtCondbyInq/summonDelegateDeptDelete.html"
	};
	
	function findEmpUser(viewListTag, selectBox){
		$.ajax({
			type: 'POST',
			url: rootPath + '/extrtCondbyInq/findEmpUserList.html',
			data:{
				system_seq : $(selectBox).val()
			},
			dataType:'JSON',
			success: function(data) {
				var htmlSource = '<option value="nodata">----- 선 택 -----</option>';
				data.forEach(function(obj,idx){
					htmlSource += '<option value="'+obj.emp_user_id+'">'+obj.emp_user_name+'</option>';
				});
				$('#'+viewListTag).html(htmlSource);
			}
		});
	}
	
	function addSummonDelegate(){
		$.ajax({
			type: 'POST',
			url: summonDelegation["addUrl"],
			data:$('#addSummonDelegate').serialize(),
			dataType:'JSON',
			success: function(data) {
				if(data=='success'){
					alert('소명 위임 성공');
					location.reload();
				}else{
					alert('오류');
				}
			}
		});
	}
	
	function deleteSummonDelegate(summon_seq){
		$.ajax({
			type: 'POST',
			url: summonDelegation["deleteUrl"],
			data:{
				summon_seq : summon_seq				
			},
			dataType:'JSON',
			success: function(data) {
				if(data=='success'){
					alert('소명 위임 취소');
					location.reload();
				}else if(data=='null'){
					alert("값이 비어있습니다.");
				}else{
					alert('오류');
				}
			}
		});
	}
	
	function searchList(){
		$('#searchForm').attr('action',summonDelegation["listUrl"]);
		$('#searchForm').submit();
	}
	
	function selectDept(viewDivTag, selectBox){
		$('#'+viewDivTag).text($(selectBox).val());
	}
	
	$(document).ready(function(){
		$('#delegate_ft, #delegate_to').val(new Date().convertDateToString());
	});
</script>