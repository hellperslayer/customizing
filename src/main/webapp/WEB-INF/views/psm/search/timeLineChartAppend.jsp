<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html>

											<c:forEach var="al" items="${allLogInq}" varStatus="st">
											<c:set var="classAdd" value="${st.count%2==1 ? 'border-left-before-green-turquoise' : 'border-right-before-green-turquoise'}"/>
											<c:choose>
												<c:when test="${al.desc_div == 'connect' }">
													<li class="mt-item">
														<div class="mt-timeline-icon" style="width: 50px; height: 50px; text-align: center; background: rgb(38, 194, 129);">
															<img src="${rootPath}/resources/image/psm/search/normal.png" style="width: 26px; height: 35px; margin: 7.5px;">
														</div>
														<div class="mt-timeline-content">
															<div class="mt-content-container border-green-turquoise ${classAdd}"
																style="padding: 15px 30px 5px;">
																<div class="mt-title">
																	<h3 class="mt-content-title">접속기록</h3>
																</div>
																<div class="mt-author" style="margin: 10px 0px 0px;">
																	<div class="mt-avatar">
																		<img class="img-circle" src="${rootPath}/resources/assets/layouts/layout/img/profile_user.png">
																	</div>
																	<div class="mt-author-name">
																		<a class="font-blue-madison" style="cursor: default;">${al.emp_user_name }(${al.emp_user_id })</a>
																	</div>
																	<div class="mt-author-notes font-grey-mint">
																		<fmt:parseDate value="${al.proc_date}" pattern="yyyyMMdd" var="proc_date" /> 
																		<fmt:formatDate value="${proc_date}" pattern="yyyy-MM-dd" /> 
																		<fmt:parseDate value="${al.proc_time}" pattern="HHmmss" var="proc_time" /> 
																		<fmt:formatDate value="${proc_time}" pattern="HH:mm:ss" />
																	</div>
																</div>
																<div class="mt-content border-green-turquoise" style="padding: 10px 0px 0px;">
																	<p>
																		<b> 접속한 시스템명 : </b>${al.system_name} &emsp;&emsp;&emsp;<b> 수행업무 : </b>${al.req_context }
																		<br> <b>정보주체식별정보 : </b> 
																		<c:if test="${al.result_type ne null}">
																		<c:set var="prev" value=""/>
																		<c:set var="cnt1" value="1"/>
																		<c:set var="total" value="0"/>
																		<c:forEach items="${al.result_type}">
																			<c:set var="total" value="${total + 1 }"/>
																		</c:forEach>
																		<c:forEach items="${al.result_type}" var="item" varStatus="status">
																			<c:choose>
																				<c:when test="${prev eq item}">	<!-- 이전값과 같을 때 -->
																					<c:set var="cnt1" value="${cnt1 + 1 }"/>
																					<c:if test="${status.count == total }">${cnt1 }</c:if>
																				</c:when>
																				<c:otherwise> 					<!-- 이전값과 다를 때 -->
																					<c:if test="${status.count != 1}">${cnt1 }</c:if>
																					<c:forEach items="${CACHE_RESULT_TYPE}" var="i">
																						<c:if test="${i.key == item }">
																							<img src="${rootPath }/resources/image/icon/resultType/${i.key}.png" title="${i.value }"/>
																						</c:if>
																					</c:forEach>
																					<c:set var="prev" value="${item }"/>
																					<c:set var="cnt1" value="1"/>
																					<c:if test="${status.count == total }">${cnt1 }</c:if>
																				</c:otherwise>
																			</c:choose>
																		</c:forEach>
																		</c:if>
																		<c:if test="${al.result_type eq null}">
																		</c:if>
																	</p>
																</div>
															</div>
														</div>
													</li>
												</c:when>
												<c:when test="${al.desc_div == 'download' }">
													<li class="mt-item">
														<div class="mt-timeline-icon" style="width: 50px; height: 50px; text-align: center; background: rgb(38, 194, 129);">
															<img src="${rootPath}/resources/image/psm/search/normal.png" style="width: 26px; height: 35px; margin: 7.5px;">
														</div>
														<div class="mt-timeline-content">
															<div class="mt-content-container border-green-turquoise ${classAdd}"
																style="padding: 15px 30px 5px;">
																<div class="mt-title">
																	<h3 class="mt-content-title">다운로드접속기록</h3>
																</div>
																<div class="mt-author" style="margin: 10px 0px 0px;">
																	<div class="mt-avatar">
																		<img class="img-circle" src="${rootPath}/resources/assets/layouts/layout/img/profile_user.png">
																	</div>
																	<div class="mt-author-name">
																		<a class="font-blue-madison" style="cursor: default;">${al.emp_user_name }(${al.emp_user_id })</a>
																	</div>
																	<div class="mt-author-notes font-grey-mint">
																		<fmt:parseDate value="${al.proc_date}" pattern="yyyyMMdd" var="proc_date" /> 
																		<fmt:formatDate value="${proc_date}" pattern="yyyy-MM-dd" /> 
																		<fmt:parseDate value="${al.proc_time}" pattern="HHmmss" var="proc_time" /> 
																		<fmt:formatDate value="${proc_time}" pattern="HH:mm:ss" />
																	</div>
																</div>
																<div class="mt-content border-green-turquoise" style="padding: 10px 0px 0px;">
																	<p>
																		<b> 접속한 시스템명 : </b>${al.system_name} &emsp;&emsp;&emsp;<b> 수행업무 : </b>${al.req_context }
																		<br> <b>개인정보유형 : </b> 
																		<c:if test="${al.result_type ne null}">
																		<c:set var="prev" value=""/>
																		<c:set var="cnt1" value="1"/>
																		<c:set var="total" value="0"/>
																		<c:forEach items="${al.result_type}">
																			<c:set var="total" value="${total + 1 }"/>
																		</c:forEach>
																		<c:forEach items="${al.result_type}" var="item" varStatus="status">
																			<c:choose>
																				<c:when test="${prev eq item}">	<!-- 이전값과 같을 때 -->
																					<c:set var="cnt1" value="${cnt1 + 1 }"/>
																					<c:if test="${status.count == total }">${cnt1 }</c:if>
																				</c:when>
																				<c:otherwise> 					<!-- 이전값과 다를 때 -->
																					<c:if test="${status.count != 1}">${cnt1 }</c:if>
																					<c:forEach items="${CACHE_RESULT_TYPE}" var="i">
																						<c:if test="${i.key == item }">
																							<img src="${rootPath }/resources/image/icon/resultType/${i.key}.png" title="${i.value }"/>
																						</c:if>
																					</c:forEach>
																					<c:set var="prev" value="${item }"/>
																					<c:set var="cnt1" value="1"/>
																					<c:if test="${status.count == total }">${cnt1 }</c:if>
																				</c:otherwise>
																			</c:choose>
																		</c:forEach>
																		</c:if>
																		<c:if test="${al.result_type eq null}">
																		</c:if>
																	</p>
																</div>
															</div>
														</div>
													</li>
												</c:when>
												<c:when test="${al.desc_div == 'ref' }">
													<li class="mt-item">
														<div class="mt-timeline-icon" style="width: 50px; height: 50px; text-align: center; background: rgb(38, 194, 129);">
															<img src="${rootPath}/resources/image/psm/search/normal.png" style="width: 26px; height: 35px; margin: 7.5px;">
														</div>
														<div class="mt-timeline-content">
															<div class="mt-content-container border-green-turquoise ${classAdd}"
																style="padding: 15px 30px 5px;">
																<div class="mt-title">
																	<h3 class="mt-content-title">접근기록</h3>
																</div>
																<div class="mt-author" style="margin: 10px 0px 0px;">
																	<div class="mt-avatar">
																		<img class="img-circle" src="${rootPath}/resources/assets/layouts/layout/img/profile_user.png">
																	</div>
																	<div class="mt-author-name">
																		<a class="font-blue-madison" style="cursor: default;">${al.emp_user_name }(${al.emp_user_id })</a>
																	</div>
																	<div class="mt-author-notes font-grey-mint">
																		<fmt:parseDate value="${al.proc_date}" pattern="yyyyMMdd" var="proc_date" /> 
																		<fmt:formatDate value="${proc_date}" pattern="yyyy-MM-dd" /> 
																		<fmt:parseDate value="${al.proc_time}" pattern="HHmmss" var="proc_time" /> 
																		<fmt:formatDate value="${proc_time}" pattern="HH:mm:ss" />
																	</div>
																</div>
																<div class="mt-content border-green-turquoise" style="padding: 10px 0px 0px;">
																	<p>
																		<b> 접속한 시스템명 : </b>${al.system_name}
																		<br> <b>접속 IP : </b>${al.result_type} &emsp;&emsp;&emsp;<b> 접속 경로 : </b>${al.req_context }
																	</p>
																</div>
															</div>
														</div>
													</li>
												</c:when>
												<c:when test="${al.desc_div == 'senario' }">
												<c:set var="classAddSenario" value="${st.count%2==1 ? 'border-left-before-red' : 'border-right-before-red'}"/>
													<li class="mt-item">
														<div class="mt-timeline-icon" style="width: 50px; height: 50px; text-align: center; background: rgb(231,80,90);">
															<img src="${rootPath}/resources/image/psm/search/normal.png" style="width: 26px; height: 35px; margin: 7.5px;">
														</div>
														<div class="mt-timeline-content">
															<div class="mt-content-container border-red ${classAddSenario}"
																style="padding: 15px 30px 5px;">
																<div class="mt-title">
																	<h3 class="mt-content-title">비정상위험</h3>
																</div>
																<div class="mt-author" style="margin: 10px 0px 0px;">
																	<div class="mt-avatar">
																		<img class="img-circle" src="${rootPath}/resources/assets/layouts/layout/img/profile_user.png">
																	</div>
																	<div class="mt-author-name">
																		<a class="font-blue-madison" style="cursor: default;">${al.emp_user_name }(${al.emp_user_id })</a>
																	</div>
																	<div class="mt-author-notes font-grey-mint">
																		<fmt:parseDate value="${al.proc_date}" pattern="yyyyMMdd" var="proc_date" /> 
																		<fmt:formatDate value="${proc_date}" pattern="yyyy-MM-dd" /> 
																	</div>
																</div>
																<div class="mt-content border-red-turquoise" style="padding: 10px 0px 0px;">
																	<p>
																		<b> 접속한 시스템명 : </b>${al.system_name}
																		<br> <b>상세 시나리오명 : </b>${al.result_type} 
																	</p>
																</div>
															</div>
														</div>
													</li>
												</c:when>
											</c:choose>
											</c:forEach>
										