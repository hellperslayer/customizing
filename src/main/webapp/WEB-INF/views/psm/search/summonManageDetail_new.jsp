<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/search/summonList.js"
	type="text/javascript" charset="UTF-8"></script>
<script
	src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js"
	type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function() {
	$("#expect_dt").datepicker();
	$("#expect_dt").click(function() {
	    // datepicker 오픈 전의 CSS를 지정
	    var position   = $(".datepicker").css("position");
	    var top          = $(".datepicker").css("top");
	    var left          = $(".datepicker").css("left");
	    var display     = $(".datepicker").css("display");

	    // datepicker 오픈 시 자동으로 변경되는 CSS 제거
	    $(".datepicker").css("z-index", "");

	    // 자동 지정되었던 CSS를 다시 지정
	    $(".datepicker").attr("style", "z-index:9999 !important");

	    // 저장했던 CSS 다시 지정
	    $(".datepicker").css("position", position);
	    $(".datepicker").css("top", top);
	    $(".datepicker").css("left", left);
	    $(".datepicker").css("display", display);
	});
});

</script>
<h1 class="page-title">${currentMenuName}</h1>


	<div class="portlet light portlet-fit portlet-datatable bordered">

		<div class="portlet-body">
			<div class="table-container">

				<form id="listForm" method="POST">
				
				
				<div class="caption" style="padding-bottom:5px;">
                <i class="icon-settings font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase">추출조건정보</span>
            </div>
            <div style="padding-bottom:20px;">
                <table class="table table-striped table-bordered order-column">
                    <tbody>
                    	<tr>
                            <th width="10%" style="text-align: center; vertical-align: middle;">발생년월일</th>
                            <td width="40%" style="text-align: center; vertical-align: middle;">${extrtCondbyInq.occr_dt}</td>
                            <th width="10%" style="text-align: center; vertical-align: middle;">시나리오명</th>
                            <td width="40%" style="text-align: center; vertical-align: middle;">${extrtCondbyInq.scen_name}</td>
                        </tr>
                        <tr>
                            <th style="text-align: center; vertical-align: middle;">상세시나리오명</th>
                            <td colspan="3" style="text-align: left; vertical-align: middle;">${extrtCondbyInq.rule_nm}</td>
                        </tr>
                        <tr>
                            <th style="text-align: center; vertical-align: middle;">취급자</th>
                            <td style="text-align: center; vertical-align: middle;">${extrtCondbyInq.emp_user_name}(${extrtCondbyInq.emp_user_id})</td>
                            <th style="text-align: center; vertical-align: middle;">소속</th>
                            <td style="text-align: center; vertical-align: middle;">${extrtCondbyInq.dept_name}(${extrtCondbyInq.dept_id})</td>
                        </tr>
                        <tr>
                            <th style="text-align: center; vertical-align: middle;">발생시스템</th>
                            <td style="text-align: center; vertical-align: middle;"><ctl:nullCv nullCheck="${extrtCondbyInq.system_name}"/></td>
                            <th style="text-align: center; vertical-align: middle;">임계치</th>
                            <td style="text-align: center; vertical-align: middle;">${extrtCondbyInq.limit_cnt}</td>
                        </tr>
                        <tr>
                            <th style="text-align: center; vertical-align: middle;">로그구분</th>
                            <td style="text-align: center; vertical-align: middle;">
                            	<c:choose>
                            		<c:when test="${extrtCondbyInq.log_delimiter == 'BA'}">접속기록조회</c:when>
                            		<c:when test="${extrtCondbyInq.log_delimiter == 'DB'}">DB접근조회</c:when>
                            		<c:when test="${extrtCondbyInq.log_delimiter == 'DN'}">다운로드로그</c:when>
                            	</c:choose>
                            </td>
                            <th style="text-align: center; vertical-align: middle;">처리기준</th>
                            <td style="text-align: center; vertical-align: middle;">
                            	<c:choose>
                            		<c:when test="${extrtCondbyInq.rule_view_type == 'M'}">처리량</c:when>
                            		<c:when test="${extrtCondbyInq.rule_view_type == 'R'}">이용량</c:when>
                            	</c:choose>
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: center; vertical-align: middle;">위험도</th>
                            <td style="text-align: center; vertical-align: middle;">${extrtCondbyInq.dng_val}</td>
                            <th style="text-align: center; vertical-align: middle;">
                            	<c:choose>
                            		<c:when test="${ui_type =='DGB'}">
                            			개인정보처리건
									</c:when>
									<c:otherwise>
										비정상걸린횟수
									</c:otherwise>
                            	</c:choose>
                            	
                            </th>
                            <td style="text-align: center; vertical-align: middle;">${extrtCondbyInq.rule_cnt}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
            <div class="caption" style="padding-bottom:5px;">
                <i class="icon-settings font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase">로그정보</span>
            </div>
            
             <div>
                <table class="table table-striped table-bordered table-hover order-column">
                    <thead>
                        <tr>
                        	<th width="5%" style="text-align: center; vertical-align: middle;">번호</th>
                        	<th width="15%" style="text-align: center; vertical-align: middle;">일시</th>
                        	<c:if test="${extrtCondbyInq.log_delimiter == 'DN'}">
                        		<th width="10%" style="text-align: center; vertical-align: middle;">파일명</th>
                        	</c:if>
                        	<th width="10%" style="text-align: center; vertical-align: middle;">시스템</th>
                        	<c:if test="${extrtCondbyInq.log_delimiter != 'DN'}">
                        	<c:choose>
                        		<c:when test="${extrtCondbyInq.rule_view_type == 'R'}">
                        			<th width="15%" style="text-align: center; vertical-align: middle;">개인정보유형</th>
                        		</c:when>
                        		<c:otherwise>
                        			<th width="15%" style="text-align: center; vertical-align: middle;">개인정보유형</th>
                        		</c:otherwise>
                        	</c:choose>
                        	</c:if>
<%--                         	<c:if test="${extrtCondbyInq.rule_view_type == 'R'}"> --%>
<%--                         	<th width="10%" style="text-align: center; vertical-align: middle;">개인정보유형</th></c:if> --%>
                        	<!-- <th width="10%" style="text-align: center; vertical-align: middle;">시간</th> -->
                        	<th width="10%" style="text-align: center; vertical-align: middle;">IP</th>
                        	<c:if test="${extrtCondbyInq.log_delimiter != 'DN'}">
                        	<th width="5%" style="text-align: center; vertical-align: middle;">접근 행위</th>
                        	<th width="10%" style="text-align: center; vertical-align: middle;">접근 경로</th>
                        	</c:if>
                        	<c:if test="${extrtCondbyInq.rule_result_type == 'N'}">
                        	<th width="10%" style="text-align: center; vertical-align: middle;">취급자ID</th></c:if>
                        	<th width="10%" style="text-align: center; vertical-align: middle;">상세정보</th>
                        </tr>
                    </thead>
                    <tbody>
                    <c:set value="${(search.page_num-1) * search.size + 1}" var="no"/>
                    <c:forEach items="${detailList }" var="dl" varStatus="st">
                        <tr>
                        	<td style="text-align: center; vertical-align: middle;">${no}</td>
                        	<td style="text-align: center; vertical-align: middle;">
                        		<fmt:parseDate value="${dl.occr_dt }" pattern="yyyyMMdd" var="d"/>
                        		<fmt:formatDate value="${d }" pattern="yyyy-MM-dd" var="dateVal"/>
                        		<fmt:parseDate value="${dl.proc_time }" pattern="HHmmss" var="t"/>
                        		<fmt:formatDate value="${t }" pattern="HH:mm:ss" var="timeVal"/>
                        		${dateVal } ${timeVal }
                        	</td>
                        	<c:if test="${extrtCondbyInq.log_delimiter == 'DN'}">
                        		<td style="text-align: center; vertical-align: middle;">${dl.file_name}</td>
                        	</c:if>
                        	<td style="text-align: center; vertical-align: middle;">${dl.system_name}</td>
                        	<c:if test="${extrtCondbyInq.log_delimiter != 'DN'}">
                        	<c:choose>
                        		<c:when test="${extrtCondbyInq.rule_view_type == 'R'}">
                        			<td style="text-align: center; vertical-align: middle;">${dl.result_content }</td>
                        		</c:when>
                        		<c:otherwise>
                        			<%-- <td style="text-align: center; vertical-align: middle;">${dl.result_type }</td> --%>
                        			<td>
                        			<c:if test="${dl.result_type ne null}">
												<c:set var="result_type_split"
													value="${fn:split(dl.result_type, ',')}" />
												<c:set var="result_type_string" value="" />
												<c:set var="info_cnt" value="1"/>
												<c:set var="pre" value=""/>
												
												<c:forEach items="${result_type_split}"
													var="result_type_split_value" varStatus="num">
													<c:choose>
													<c:when test="${pre ne result_type_split_value }">
														<!--  pre값이 공백이아니고 result_type_split_value과 다를경우 갯수를 표시하고
															  pre = result_type_split_value 
															  info_cnt은 1로초기화	 -->
														<c:if test="${pre ne ''}">
															${info_cnt}
															<c:set var="info_cnt" value="1"/>
														</c:if>
														<c:set var="pre" value="${result_type_split_value}"/>
														
													</c:when>
													<c:otherwise>
														<c:set var="info_cnt" value="${info_cnt+1}"/>
													</c:otherwise>
													</c:choose>
													<c:forEach items="${CACHE_RESULT_TYPE}" var="i"
														varStatus="status">
														<%-- <c:set var="result_type_string" value="${result_type_string} ${i.value}" /> --%>
														<c:if test="${i.key == pre && info_cnt == 1}">
															<img src="${rootPath }/resources/image/icon/resultType/${pre}.png" title="${CACHE_RESULT_TYPE[i.key] }"/>
														</c:if>
													</c:forEach>
													<!-- result_type_split_value 마지막 값은 비교할 다음값이 없으므로 체크해서 갯수 표시     -->
													 <c:if test="${fn:length(result_type_split) == num.index+1}">
													 	${info_cnt}
													 </c:if>
												</c:forEach>
												<%--
												<c:choose>
													 <c:when test="${fn:length(result_type_string) > 8}">
														<td title="${result_type_string}">
															 ${fn:split(result_type_string, " ")[0]} 외
															${fn:length(fn:split(result_type_string, " ")) - 1}개의 개인정보</td> 
															
													</c:when> 
													<c:otherwise>
														<td><ctl:nullCv nullCheck="${result_type_string}" />
														</td>
													</c:otherwise>
												</c:choose>
												--%>
											</c:if>
											</td>
                        			
                        		</c:otherwise>
                        	</c:choose>
                        	</c:if>
<%--                         	<c:if test="${extrtCondbyInq.rule_view_type == 'R'}"> --%>
<%--                         	<td style="text-align: center; vertical-align: middle;">${dl.result_content }</td></c:if> --%>
                        	<%-- <td style="text-align: center; vertical-align: middle;">${timeVal }</td> --%>
                        	<td style="text-align: center; vertical-align: middle;">${dl.ip }</td>
                        	<c:if test="${extrtCondbyInq.log_delimiter != 'DN'}">
                        	<td style="text-align: center; vertical-align: middle;">
                        		<c:forEach items="${CACHE_REQ_TYPE}" var="i" varStatus="status">
									<c:if test="${i.key == dl.req_type}">
										<ctl:nullCv nullCheck="${i.value}" />
									</c:if>
								</c:forEach>
                        	</td>
                        	<c:choose>
                        		<c:when test="${fn:length(dl.req_url)>35}">
                        			<td title="${dl.req_url} style="text-align: center; vertical-align: middle;">
                        				<c:out value="${fn:substring(dl.req_url,0,34)}">
                        				</c:out>...
                        			</td>
                        			</c:when>
                        		<c:otherwise>
                        			<td style="text-align: center; vertical-align: middle;">
                        				${dl.req_url}
                        		</c:otherwise>
                        	</c:choose>
                        	</c:if>
                        	<c:if test="${extrtCondbyInq.rule_result_type == 'N'}">
                        		<td style="text-align: center; vertical-align: middle;">${dl.emp_user_id }</td>'
                        	</c:if>
                        	<td style="text-align: center; vertical-align: middle;">
                        		<button type="button" style="padding-top: 2px;padding-bottom: 2px; padding-right: 8px;padding-left: 8px;" class="btn blue mt-ladda-btn ladda-button btn-outline btn-circle"
                                onclick="detailResultLogView('${dl.log_seq}','${dl.occr_dt}')">
                                    <span class="ladda-label" style="font-size: 12px;">상세보기</span>
                                <span class="ladda-spinner"></span></button>
                        	</td>
                        </tr>
                        <c:set value="${no+1}" var="no"/>
                    </c:forEach>
                    </tbody>
                </table>
				</div>
            
          <%--   <div>
                <table class="table table-striped table-bordered table-hover order-column">
                    <thead>
                        <tr>
                        	<th width="10%" style="text-align: center; vertical-align: middle;">번호</th>
                        	<th width="10%" style="text-align: center; vertical-align: middle;">일시</th>
                        	<c:if test="${extrtCondbyInq.rule_view_type == 'R'}">
                        	<th width="10%" style="text-align: center; vertical-align: middle;">개인정보유형</th></c:if>
                        	<th width="10%" style="text-align: center; vertical-align: middle;">시간</th>
                        	<th width="10%" style="text-align: center; vertical-align: middle;">IP</th>
                        	<c:if test="${extrtCondbyInq.rule_result_type == 'N'}">
                        	<th width="10%" style="text-align: center; vertical-align: middle;">취급자ID</th></c:if>
                        	<th width="10%" style="text-align: center; vertical-align: middle;">상세정보</th>
                        </tr>
                    </thead>
                    <tbody>
                    <c:set value="${(search.page_num-1) * search.size + 1}" var="no"/>
                    <c:forEach items="${detailList }" var="dl" varStatus="st">
                        <tr>
                        	<td style="text-align: center; vertical-align: middle;">${no}</td>
                        	<td style="text-align: center; vertical-align: middle;">
                        		<fmt:parseDate value="${dl.occr_dt }" pattern="yyyyMMdd" var="d"/>
                        		<fmt:formatDate value="${d }" pattern="yyyy-MM-dd" var="dateVal"/>
                        		<fmt:parseDate value="${dl.proc_time }" pattern="HHmmss" var="t"/>
                        		<fmt:formatDate value="${t }" pattern="HH:mm:ss" var="timeVal"/>
                        		${dateVal } ${timeVal }
                        	</td>
                        	<c:if test="${extrtCondbyInq.rule_view_type == 'R'}">
                        	<td style="text-align: center; vertical-align: middle;">${dl.result_content }</td></c:if>
                        	<td style="text-align: center; vertical-align: middle;">${timeVal }</td>
                        	<td style="text-align: center; vertical-align: middle;">${dl.ip }</td>
                        	<c:if test="${extrtCondbyInq.rule_result_type == 'N'}">
                        	<td style="text-align: center; vertical-align: middle;">${dl.emp_user_id }</td></c:if>
                        	<td style="text-align: center; vertical-align: middle;">
                        		<button type="button" style="padding-top: 2px;padding-bottom: 2px; padding-right: 8px;padding-left: 8px;" class="btn blue mt-ladda-btn ladda-button btn-outline btn-circle"
                                onclick="detailResultLogView('${dl.log_seq}','${dl.occr_dt}')">
                                    <span class="ladda-label" style="font-size: 12px;">상세보기</span>
                                <span class="ladda-spinner"></span></button>
                        	</td>
                        </tr>
                        <c:set value="${no+1}" var="no"/>
                    </c:forEach>
                    </tbody>
                </table>
				</div> --%>
				
				<!-- 페이징 영역 -->
                <c:if test="${search.total_count > 0}">
                    <div id="pagingframe" align="center">
                        <p><ctl:paginator currentPage="${search.page_num}" rowBlockCount="${search.size}" totalRowCount="${search.total_count}" pageName="summonDetail" /></p>
                    </div>
                </c:if>

				<div class="caption" style="padding-bottom: 5px;">
					<i class="icon-settings font-dark"></i> <span
						class="caption-subject font-dark sbold uppercase">소명권고 이력</span>
				</div>
				<div style="padding-bottom: 20px;">
					<table
						class="table table-striped table-bordered table-hover order-column">
						<colgroup>
							<col width="15%" />
							<col width="15%" />
							<col width="15%" />
							<col width="40%" />
							<col width="15%" />
						</colgroup>
						<thead>
							<tr>
								<th scope="col" style="text-align: center;">일시</th>
								<th scope="col" style="text-align: center;">담당자</th>
								<!-- <th scope="col" style="text-align: center;">대상자</th> -->
								<th scope="col" style="text-align: center;">행위</th>
								<th scope="col" style="text-align: center;">내용</th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${empty summon_history}">
									<tr>
										<td colspan="4" align="center">데이터가 없습니다.</td>
									</tr>
								</c:when>
								<c:otherwise>
									<c:forEach items="${summon_history }" var="memo">
										<tr>
											<td style="text-align: center;"><ctl:nullCv nullCheck="${memo.datetime}" /></td>
											<td style="text-align: center;">${memo.user_name }(${memo.user_id })</td>
											<!--<td style="text-align: center;">${memo.target_name }(${memo.target_id })</td> -->
											<td style="text-align: center;">
												<c:choose>
													<c:when test="${memo.status eq '10'}">소명요청</c:when>
													<c:when test="${memo.status eq '20'}">재소명</c:when>
													<c:when test="${memo.status eq '30'}">소명응답</c:when>
													<c:when test="${memo.status eq '40'}">${summon_cfm_yn eq 'Y' ? '부서판정' : '판정'}</c:when>
													<c:when test="${memo.status eq '60'}">최종판정</c:when>
												</c:choose>
											</td>
											<td style="text-align: center;">${memo.msg}</td>
										</tr>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
				</div>
				
				<!-- 메뉴 관련 input 시작 -->
				<input type="hidden" name="main_menu_id" value="${search.main_menu_id }" />
				<input type="hidden" name="sub_menu_id" value="${search.sub_menu_id }" />
				<input type="hidden" name="current_menu_id" value="${currentMenuId }" />
				<!-- 메뉴 관련 input 끝 -->
				
				<!-- 페이지 번호 -->
				<input type="hidden" name="page_num" value="${search.page_num }" />
				<input type="hidden" name="cur_num" value="${search.cur_num }" />
				<input type="hidden" name="empDetailInqDetail_page_num" value="${search.empDetailInqDetail.page_num }" />
				<input type="hidden" name="extrtCondbyInqDetail_page_num" value="${search.extrtCondbyInqDetail.page_num }" />
				
				<!-- 검색조건 관련 input 시작 -->
				<input type="hidden" name="summon_code" value="${search.summon_code}" />
				<input type="hidden" name="search_from" value="${search.search_from }" />
				<input type="hidden" name="search_to" value="${search.search_to }" />
				<input type="hidden" name="emp_user_id" value="${search.emp_user_id }" />
				<input type="hidden" name="emp_user_name" value="${search.emp_user_name}" />
				<input type="hidden" name="dept_name" value="${search.dept_name}" />
				<input type="hidden" name="rule_cd" value="${search.rule_cd }" />
				<input type="hidden" name="daySelect" value="${search.daySelect }" />
				<input type="hidden" name="dateType" value="${search.dateType}" />
				<!-- 검색조건 관련 input 끝 -->
				
				<!-- 상세조건 input시작 -->
				<input type="hidden" name="dng_val" value="${search.dng_val }" />
				<input type="hidden" name="detailProcTime" value=""/>
				<input type="hidden" name="detailStartDay" value="${search.detailStartDay }"/>
				<input type="hidden" name="detailEndDay" value="${search.detailEndDay }"/>
				<input type="hidden" name="bbs_id" value="${search.bbs_id }"/>
				<input type="hidden" name="detailOccrDt" value="${search.detailOccrDt}"/>
				<input type="hidden" name="detailEmpCd" value="${search.detailEmpCd }"/>
				<input type="hidden" name="detailEmpDetailSeq" value="${search.detailEmpDetailSeq}"/>
				<input type="hidden" id="check_type" name="check_type" value="${search.check_type}" />
				<input type="hidden" id="sort_flag" name="sort_flag" value="${search.sort_flag}" />
				<input type="hidden" id="rootPath" name="rootPath" value="${rootPath}" />
				<input type="hidden" name="user_name" value="" />
				<input type="hidden" name="result_type" value="" />
				<input type="hidden" name="desc_status" value="${search.desc_status}" />
				<input type="hidden" name="desc_result" value="${search.desc_result}" />
				<input type="hidden" name="detect_div_search" value="${search.detect_div_search}" />
						
				
				<!-- 상세조건 input끝 -->
				<input type="hidden" id="logType" value="${logType}" />
				<input type="hidden" name="log_delimiter" value="${extrtCondbyInq.log_delimiter }" />
				
				<!-- 소명요청 -->
				<input type="hidden" name="emp_detail_seq" value="${extrtCondbyInq.emp_detail_seq}"/>
				
				
				</form>
				
				<form id="resultForm" method="post">
				
				<div class="row">
					<c:choose>
						<c:when test="${search.detail_summon_status eq 10 || search.detail_summon_status eq 20 }">
							<div class="col-lg-6 col-xs-12 col-sm-12">
								<div class="caption" style="padding-bottom: 5px;">
									<i class="icon-settings font-dark"></i> <span
										class="caption-subject font-dark sbold uppercase">답변정보</span>
								</div>
								<div>
									<table class="table table-bordered order-column">
										<tr>
											<th width="20%" style="text-align: center;">소명응답자</th>
											<td width="30%" style="text-align: center;">-</td>
											<th width="20%" style="text-align: center;">답변일자</th>
											<td width="30%" style="text-align: center;">-</td>
										</tr>
										<tr>
											<th width="20%" style="text-align: center; vertical-align: middle;">소명내용</th>
											<td width="80%" colspan="3">
												<textarea style="width: 100%; height: 150px;" readonly="readonly"></textarea>
											</td>
										</tr>
										<!-- 
										<tr>
											<th width="20%" style="text-align: center; vertical-align: middle;">첨부파일</th>
											<td width="80%" colspan="3" style="text-align: center;"></td>
										</tr>
										 -->
									</table>
								</div>
							</div>
						</c:when>
						<c:otherwise>
							<div class="col-lg-6 col-xs-12 col-sm-12">
								<div class="caption" style="padding-bottom: 5px;">
									<i class="icon-settings font-dark"></i> <spanclass="caption-subject font-dark sbold uppercase">답변정보 </span>
								</div>
								<div>
									<table class="table table-bordered order-column">
										<tr>
											<th width="20%" style="text-align: center;">소명응답자</th>
											<td width="30%" style="text-align: center;">${summon_response.res_user_name}(${summon_response.res_user_id})</td>
											<th width="20%" style="text-align: center;">답변일자</th>
											<td width="30%" style="text-align: center;"><ctl:nullCv nullCheck="${summon_response.datetime}"/></td>
										</tr>
										<tr>
											<th width="20%" style="text-align: center; vertical-align: middle;">소명내용</th>
											<td width="80%" colspan="3">
												<textarea style="width: 100%; height: 150px;" readonly="readonly">${summon_response.responseMsg }</textarea>													
											</td>
										</tr>
										<%-- 
										<tr>
											<th width="20%" style="text-align: center; vertical-align: middle;">첨부파일</th>
											<td width="80%" colspan="3" style="text-align: center;">
												<c:choose>
													<c:when test="${empty raw.file_path1 }">
														-
													</c:when>
													<c:otherwise>
														<a onclick="fileDownload('${raw.file_path1 }', '${raw.file_path2 }')">${raw.file_path1 }</a>
													</c:otherwise>
												</c:choose>
											</td>
										</tr>
										 --%>
									</table>
								</div>
							</div>
						</c:otherwise>
					</c:choose>
					
							<div class="col-lg-6 col-xs-12 col-sm-12">
								<div class="caption" style="padding-bottom: 5px;">
									<i class="icon-settings font-dark"></i> <spanclass="caption-subject font-dark sbold uppercase">판정정보 </span>
								</div>		
								
								<table class="table table-bordered order-column">
										<tr>
											<th width="20%" style="text-align: center;">판정자</th>
											<td width="30%" style="text-align: center;">
												<c:if test="${search.detail_summon_status eq 10 || search.detail_summon_status eq 20 }">-</c:if>
												<c:if test="${search.detail_summon_status eq 30}">${userSession.admin_user_id}</c:if>
												<c:if test="${search.detail_summon_status eq 40}">${summon_decision.decision_user_name}(${summon_decision.decision_user_id })</c:if>
												<c:if test="${search.detail_summon_status eq 60}">슈퍼관리자</c:if>
											</td>
											<th width="20%" style="text-align: center;">판정일</th>
											<td width="30%" style="text-align: center;">
												<c:if test="${search.detail_summon_status eq 10 || search.detail_summon_status eq 20 || search.detail_summon_status eq 30 }">-</c:if>
												<c:if test="${search.detail_summon_status eq 40 || search.detail_summon_status eq 60}">${summon_decision.datetime }</c:if>
											</td>
										</tr>
										<tr>
											<th style="text-align: center; vertical-align: middle;">판정의견</th>
											<c:if test="${search.detail_summon_status eq 10 || search.summon_status eq 20 }">	
												<td colspan="3"><textarea name="msg" style="width: 100%; height: 150px;" readonly="readonly"></textarea></td>
											</c:if>
											<c:if test="${search.detail_summon_status eq 30}">
												<td colspan="3"><textarea name="msg" style="width: 100%; height: 150px;"></textarea></td>
											</c:if>
											<c:if test="${search.detail_summon_status eq 40 }">
												<td colspan="3"><textarea name="msg" style="width: 100%; height: 150px;"" readonly="readonly">${summon_decision.msg}</textarea></td>
											</c:if>
											<c:if test="${search.detail_summon_status eq 60 }">
												<td colspan="3"><textarea name="msg" style="width: 100%; height: 150px;" readonly="readonly">${summon_decision.msg}</textarea></td>
											</c:if>
										</tr>
										<%-- <c:if test="${search.detail_summon_status eq 30}"> --%>
										<tr>
											<th style="text-align: center; vertical-align: middle;">
											${summon_cfm_yn eq 'Y' ? '부서판정' : '판정'}
											</th>
											
											<td colspan="3">	
												<c:choose>	
													<c:when test="${search.detail_summon_status eq 10}">
														소명 요청중입니다.
													</c:when>
													<c:when test="${search.detail_summon_status eq 20}">
														재소명 요청중입니다.
													</c:when>
													<c:when test="${search.detail_summon_status eq 30}">
														<select class="form-control input-small" name="decision_status" style="display: inline-block;margin-right: 10px;" onchange="showAbuseTr()">
														<option value="">판정하세요</option>
														<option value="20">재소명</option>
														<option value="30">적정</option>
														<option value="40">부적정</option>
													</select>
													</c:when>
													<c:when test="${search.detail_summon_status eq 40 || search.detail_summon_status eq 60}">
														<c:if test="${search.detail_decision_status eq 30}">
															적정	
														</c:if>
														<c:if test="${search.detail_decision_status eq 40}">
															부적정
														</c:if>
													</c:when> 
												</c:choose>
											</td>
										</tr>
										<%-- </c:if> --%>
										<%-- <c:choose>
											<c:when test="${search.detail_summon_status eq 40}"> --%>
											<c:if test="${summon_cfm_yn eq 'Y'}">
												<tr>
													<th style="text-align: center; vertical-align: middle;">최종판정</th>
													<td colspan="3">
													<c:choose>
														<c:when test="${search.detail_summon_status eq 10}">
															소명 요청중입니다.
														</c:when>
														<c:when test="${search.detail_summon_status eq 20}">
															재소명 요청중입니다.
														</c:when>
														<c:when test="${search.detail_summon_status eq 30}">
															부서 승인중입니다.
														</c:when>
														<c:when test="${search.detail_summon_status eq 40 && search.user_auth eq 'AUTH00006'}">
															<select class="form-control input-small" name="decision_status_second" style="display: inline-block;margin-right: 10px;" onchange="showAbuseTr()">
																<option value="">판정하세요</option>
																<!-- <option value="20">재소명</option> -->
																<option value="30">적정</option>
																<option value="40">부적정</option>
															</select>
														</c:when>
														<c:when test="${search.detail_summon_status eq 60}">
														<c:if test="${search.detail_decision_status_second eq 30}">
															적정	
														</c:if>
														<c:if test="${search.detail_decision_status_second eq 40}">
															부적정
														</c:if>
														</c:when>
													</c:choose>
													</td>
												</tr>
											</c:if>
												<%-- </c:when>
												<c:otherwise> --%>
												
												<%-- </c:otherwise>
											</c:choose>  --%>
									</table>
							</div>
				</div>
				
				<input type="hidden" name="detail_emp_user_id" value="${extrtCondbyInq.emp_user_id }"/>
				<input type="hidden" name="detail_system_seq" value="${extrtCondbyInq.system_seq }"/>
				<input type="hidden" name="admin_user_id" value="${userSession.admin_user_id}"/>
				<input type="hidden" name="summon_status" value=""/>
				
				<input type="hidden" name="status" value=""/>
				<input type="hidden" name="emp_detail_seq" value="${search.emp_detail_seq }"/>
				
				
				
				</form>
				<div align="right">
					<c:if test="${search.detail_summon_status eq 30}">
						<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="addSummonResult(${search.emp_detail_seq})"><i class="fa fa-check"></i> ${summon_cfm_yn eq 'Y' ? '부서판정' : '판정'}</a>
					</c:if>
					<!--  DGB 전용 최종판정  -->
					<c:if test="${summon_cfm_yn eq 'Y'}">	
					<c:if test="${search.detail_summon_status eq 40 && search.user_auth eq 'AUTH00006'}">	
						<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="addSummonResultConfirm(${search.emp_detail_seq})"><i class="fa fa-check"></i> 최종판정</a>
					</c:if>
					</c:if>
					<!-- 
					<c:if test="${status.desc_status.symbol eq '60'}">
						<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="addSummonResult2(${search.desc_seq})"><i class="fa fa-check"></i> 2차판정</a>
					</c:if>
					<c:if test="${status.desc_status.symbol ne '80' && status.desc_status.symbol ne '90' }">
						<a class="btn btn-sm red btn-outline sbold uppercase" onclick="removeSummonResult(${search.desc_seq})"><i class="fa fa-remove"></i>초기화</a>
					</c:if>
					 -->
					<a class="btn btn-sm dark btn-outline sbold uppercase" onclick="goSummonManageList(${search.cur_num});"><i class="fa fa-list"></i> 목록</a>
				</div>
			</div>
		</div>
	</div>

<form id="moveForm" method="post">
<input type="hidden" name="detailProcDate" value=""/>
<input type="hidden" name="detailLogSeq" value=""/>
</form>

<script type="text/javascript">
	var summonManageDetailConfig = {
		"listUrl" : "${rootPath}/extrtCondbyInq/summonManageDetail.html",
		"addResultSummonResultUrl" : "${rootPath}/extrtCondbyInq/addSummonResult.html",
		"reSummonResultUrl" : "${rootPath}/extrtCondbyInq/summonre.html",
		"loginPage" : "${rootPath}/loginView.html",
		"summonManageListUrl" : "${rootPath}/extrtCondbyInq/summonManageList.html",
		"goEmpDetailInqListUrl" : "${rootPath}/empDetailInq/summonDetail.html",
		"goCallingDemandListUrl" : "${rootPath}/callingDemand/list.html",
		"detailUrl" : "${rootPath}/extrtCondbyInq/detail.html",
		"detailChartUrl" : "${rootPath}/extrtCondbyInq/detailChart.html",
		"removeSummonResultUrl" : "${rootPath}/extrtCondbyInq/removeSummonResult.html",
		"updateSummonResultUrl" : "${rootPath}/extrtCondbyInq/updateSummonResult.html",
		"addReResultSummonResultUrl" : "${rootPath}/extrtCondbyInq/addReSummonResult.html",
		//최종판정
		"ResultConfirmSummonResultUrl" : "${rootPath}/extrtCondbyInq/SummonResultConfirm.html"
	};

	var summonManageConfig= {
			"listUrl" : "${rootPath}/extrtCondbyInq/summonManageList.html"
			,"detailUrl" : "${rootPath}/extrtCondbyInq/summonManageDetail.html"
	}
	
 	var allLogInqDetailConfig = {
        "detailUrl":"${rootPath}/allLogInq/detail.html"
        ,"downloadDetailUrl":"${rootPath}/downloadLogInq/detail.html"
    };
	
	$("#table1").dataTable( {
		scrollY: 310,
		deferRender: false,
		scroller: true,
		searching: false,
		info: false,
		ordering: false
	});
</script>