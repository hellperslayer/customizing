<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="userAddFlag" value="F" />
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />
<script src="${rootPath}/resources/js/common/jquery.form.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/search/thresholdSetting_dept.js" type="text/javascript" charset="UTF-8"></script>

<h1 class="page-title">${currentMenuName}</h1>
<div class="portlet light portlet-fit portlet-datatable bordered">
	
		<div class="portlet-body">
		<div class="table-container">

			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">

				<div class="col-md-6"
					style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
					<div class="portlet box grey-salt  ">
                          <div class="portlet-title" style="background-color: #2B3643;">
                              <div class="caption">
                                  <i class="fa fa-search"></i>검색</div>
                              <div class="tools">
				                 <a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
				             </div>
                          </div>
                          <div id="searchBar" class="portlet-body form" >
                              <div class="form-body" style="padding-left:10px; padding-right:10px;">
                                  <div class="form-group">
                                      <form id="empUserListForm" method="POST" class="mt-repeater form-horizontal">
                                          <div data-repeater-list="group-a">
                                              <div data-repeater-item class="mt-repeater-item">
                                                  <!-- jQuery Repeater Container -->
                                                  <div class="mt-repeater-input">
                                                      <label class="control-label">시나리오</label>
                                                      <br/>
                                                      <input type="text" class="form-control input-medium" name="rule_nm" value="${search.rule_nm}" />
                                                   </div>
                                                   <%-- <div class="mt-repeater-input">
                                                      <label class="control-label">시스템명</label>
                                                      <br/>
                                                      <select name="system_seq" class="form-control input-medium">
														<option value=""
															<c:if test="${search.system_seq == null}">selected="selected"</c:if>>
															----- 전체 -----</option>
														<c:forEach items="${systemMasterList}" var="systemMasterList">
															<option value="${systemMasterList.system_seq}"
																<c:if test="${systemMasterList.system_seq == search.system_seq}">selected="selected"</c:if>>${systemMasterList.system_name}</option>
														</c:forEach>
													</select>
                                                  </div> --%>
                                              </div>
                                          </div>
                                          <div align="right">
                                           <button type="reset"
											class="btn btn-sm red-mint btn-outline sbold uppercase"
											onclick="resetOptions(empUserListConfig['listUrl'])">
											<i class="fa fa-remove"></i> <font>초기화
										</button>
										<button type="button" class="btn btn-sm blue btn-outline sbold uppercase" onclick="searchEmpUserList()">
											<i class="fa fa-search"></i> 검색
										</button>&nbsp;&nbsp;
									</div>
									
                                      </form>
                                  </div>
                              </div>
                          </div>
                      </div>
				</div>
				
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-6">
							<div class="btn-group">
								<button class="btn btn-sm blue btn-outline sbold uppercase" onclick="moveDetail()">
									<i class="fa fa-plus"></i> 등록 
								</button>
							</div>
						</div>
					</div>
				</div>
					
				
				<table class="table table-striped table-bordered table-hover order-column" id="datatable_ajax_2" aria-describedby="datatable_ajax_2_info" role="grid">
					<thead>
						<tr role="row" class="heading">
							<th width="20%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">번호</th>
							<th width="30%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">시나리오명</th>
							<th width="50%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">상세시나리오명</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${empty ruleList}">
								<tr>
									<td colspan="3" align="center">데이터가 없습니다.</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach items="${ruleList}" var="rl" varStatus="status">
									<tr style="cursor: pointer;" onclick="moveDetail('${rl.rule_seq}')">
										<td style="text-align: center;">${status.count}</td>
										<td style="text-align: center;">${rl.scen_name}</td>
										<td style="text-align: center;">${rl.rule_nm}</td>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
				<div class="dataTables_processing DTS_Loading"
					style="display: none;">Please wait ...</div>
				<c:if test="${search.total_count > 0}">
					<div class="page left" id="pagingframe" align="center">
						<p>
							<ctl:paginator currentPage="${search.page_num}" rowBlockCount="${search.size}"
								totalRowCount="${search.total_count}" />
						</p>
					</div>
				</c:if>
			</div>
		</div>

	</div>
</div>
<form action="thresholdSettingDetail_dept.html" method="POST" id="moveForm">
<input type="hidden" name="rule_seq">
</form>
<script type="text/javascript">
	var empUserListConfig = {
		"listUrl" : "${rootPath}/extrtBaseSetup/thresholdSetting_dept.html"
	};
</script>