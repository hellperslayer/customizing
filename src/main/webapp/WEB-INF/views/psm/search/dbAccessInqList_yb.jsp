<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>

<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/search/dbAccessInqList.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>


<h1 class="page-title">
	${currentMenuName}
	<!-- <small>Log-list</small> -->
</h1>
<div class="portlet light portlet-fit portlet-datatable bordered">
	<%-- <div class="portlet-title">
		<div class="caption">
			<i class="icon-settings font-dark"></i> <span
				class="caption-subject font-dark sbold uppercase">접속기록 리스트</span>
		</div>
		<div class="actions">
			<div class="btn-group">
				<a class="btn red btn-outline btn-circle"
					onclick="excelAllLogInqList('${search.total_count}')"
					data-toggle="dropdown"> <i class="fa fa-share"></i> <span
					class="hidden-xs"> 엑셀 </span>
				</a>
			</div>
		</div>
	</div> --%>
	<div class="portlet-body">
		<div class="table-container">

			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
				<div>
					<div class="col-md-6"
						style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
						
						<div class="portlet box grey-salt  ">
                            <div class="portlet-title" style="background-color: #2B3643;">
                                <div class="caption">
                                    <i class="fa fa-search"></i>검색 & 엑셀 </div>
                                <div class="tools">
	                            	<a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
					            </div>
                            </div>
                            <div id="searchBar" class="portlet-body form">
                                <div class="form-body" style="padding-left:10px; padding-right:10px;">
                                    <div class="form-group">
                                        <form id="listForm" method="POST" class="mt-repeater form-horizontal">
                                            <div data-repeater-list="group-a">
                                                <div data-repeater-item class="mt-repeater-item">
                                                    <!-- jQuery Repeater Container -->
                                                    <div class="mt-repeater-input">
                                                        <label class="control-label">실행시간</label>
                                                        <br/>
                                                        <div class="input-group input-medium date-picker input-daterange"
															data-date="10/11/2012" data-date-format="yyyy-mm-dd">
															<input type="text" class="form-control" id="search_fr"
																name="search_from" value="${search.search_fromWithHyphen}"> 
															<span class="input-group-addon"> &sim; </span> 
															<input type="text" class="form-control" id="search_to" 
																name="search_to" value="${search.search_toWithHyphen}">
														</div> 
													</div>
                                                    <div class="mt-repeater-input">
                                                        <label class="control-label">기간선택</label>
                                                        <br/>
                                                        <select class="form-control input-medium"
															id="daySelect" name="daySelect"
															onclick="javascript:initDaySelect();">
															<option value="" class="daySelect_first"
																<c:if test='${search.daySelect eq ""}'>selected="selected"</c:if>>기간선택</option>
															<option value="Day"
																<c:if test='${search.daySelect eq "Day"}'>selected="selected"</c:if>>오늘</option>
															<option value="WeekDay"
																<c:if test='${search.daySelect eq "WeekDay"}'>selected="selected"</c:if>>일주일</option>
															<%-- <option value="MonthDay"
																<c:if test='${search.daySelect  eq "MonthDay"}'>selected="selected"</c:if>>전달
																1일부터 ~ 오늘</option>
															<option value="YearDay"
																<c:if test='${search.daySelect  eq "YearDay"}'>selected="selected"</c:if>>올해
																1월1일부터 ~ 오늘</option>
															<option value="TotalMonthDay"
																<c:if test='${search.daySelect eq "TotalMonthDay"}'>selected="selected"</c:if>>전달
																전체</option> --%>
														</select>
													</div>
													<div class="mt-repeater-input">
                                                        <label class="control-label">사용자ID</label>
                                                        <br/>
                                                        <input type="text" class="form-control" name="emp_user_id" value="${search.emp_user_id}" />
                                                    </div>
                                                    <div class="mt-repeater-input">
                                                        <label class="control-label">사용자명</label>
                                                        <br/>
                                                        <input type="text" class="form-control" name="emp_user_name" value="${search.emp_user_name}" />
                                                    </div>
                                                    <div class="mt-repeater-input">
                                                        <label class="control-label">소속</label>
                                                        <br/>
                                                        <input type="text" class="form-control" name="dept_name" value="${search.dept_name}" />
                                                    </div>
                                                    <div class="mt-repeater-input">
                                                        <label class="control-label">사용자IP</label>
                                                        <br/>
                                                        <input type="text" class="form-control" name="user_ip" value="${search.user_ip}" />
                                                    </div>
                                                    <%-- <div class="mt-repeater-input">
                                                        <label class="control-label">개인정보유형</label>
                                                        <br/>
                                                        <select name="privacy"
															class="form-control">
															<option value=""
																${search.privacy == '' ? 'selected="selected"' : ''}>
																----- 전 체 -----</option>
															<c:if test="${empty CACHE_RESULT_TYPE}">
																<option>데이터 없음</option>
															</c:if>
															<c:if test="${!empty CACHE_RESULT_TYPE}">
																<c:forEach items="${CACHE_RESULT_TYPE}" var="i"
																	varStatus="z">
																	<option value="${i.key}"
																		${i.key==search.privacy ? "selected=selected" : "" }>${ i.value}</option>
																</c:forEach>
															</c:if>
														</select>
                                                    </div> --%>
                                                    <div class="mt-repeater-input">
                                                        <label class="control-label">시스템</label>
                                                        <br/>
                                                        <select name="system_seq"
															class="form-control">
															<option value=""
																${search.system_seq == '' ? 'selected="selected"' : ''}>
																----- 전 체 -----</option>
															<c:if test="${empty systemMasterList}">
																<option>시스템 없음</option>
															</c:if>
															<c:if test="${!empty systemMasterList}">
																<c:forEach items="${systemMasterList}" var="i"
																	varStatus="z">
																	<option value="${i.system_seq}"
																		${i.system_seq==search.system_seq ? "selected=selected" : "" }>${ i.system_name}</option>
																</c:forEach>
															</c:if>
														</select>
													</div>
                                                </div>
                                            </div>
                                            <div align="right">
	                                            <button type="reset"
													class="btn btn-sm red-mint btn-outline sbold uppercase"
													onclick="resetOptions(dbAccessInqConfig['listUrl'])">
													<i class="fa fa-remove"></i> <font>초기화
												</button>
												<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
													onclick="movedbAccessInqList()">
													<i class="fa fa-search"></i> 검색
												</button>&nbsp;&nbsp;
												<%-- <a class="btn red btn-outline btn-circle"
													onclick="excelDbAccessInqList('${search.total_count}')"> 
													<i class="fa fa-share"></i> <span class="hidden-xs"> 엑셀 </span>
												</a> --%>
												<a onclick="excelDbAccessInqList('${search.total_count}')"><img src="${rootPath}/resources/image/icon/XLS_3.png"></a>
											</div>
											
											<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }"/>
											<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }"/>
											<input type="hidden" name="page_num" value="${search.page_num}" />
											<input type="hidden" name="detailLogSeq" value=""/>
											<input type="hidden" name="detailLogSqlSeq" value=""/>
											<input type="hidden" name="detailProcDate" value=""/>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>
				</div>
				<div>
					<table style="border-top: 1px solid #e7ecf1"
						class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
						style="position: absolute; top: 0px; left: 0px; width: 100%;">

						<thead>
							<tr role="row" class="heading" style="background-color: #c0bebe;">
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									실행시간</th>
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									소속</th>
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									사용자ID</th>
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center; padding: 0em;">
									사용자명</th>
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									사용자IP</th>
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									시스템명</th>
								<!-- <th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									개인정보유형</th> -->
								<th width="40%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									COMMAND</th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${empty dbAccessInqList.dbAccessInq}">
									<tr>
										<td colspan="8" align="center">데이터가 없습니다.</td>
									</tr>
								</c:when>
								<c:otherwise>
									<c:set value="${dbAccessInqList.page_total_count}" var="count"/>
									<c:forEach items="${dbAccessInqList.dbAccessInq}" var="dbAccessInq" varStatus="status">
										<tr style='cursor: pointer;' onclick="javascript:fndbAccessInqDetail('${dbAccessInq.log_seq}','${dbAccessInq.proc_date}');">
											<c:if test="${dbAccessInq.proc_date ne null and dbAccessInq.proc_time ne null}">
												<td style="text-align: center;">
													<fmt:parseDate value="${dbAccessInq.proc_date}" pattern="yyyyMMdd" var="proc_date" /> 
													<fmt:formatDate value="${proc_date}" pattern="YY-MM-dd" />
													<fmt:parseDate value="${dbAccessInq.proc_time}" pattern="HHmmss" var="proc_time" /> 
													<fmt:formatDate value="${proc_time}" pattern="HH:mm:ss" />
												</td>
											</c:if>
											<c:if test="${dbAccessInq.proc_date eq null or dbAccessInq.proc_time eq null}">
												<td style="text-align: center;">-</td>
											</c:if>
											<td style="text-align: center;"><ctl:nullCv nullCheck="${dbAccessInq.dept_name}" /></td>
											<td style="text-align: center;"><ctl:nullCv nullCheck="${dbAccessInq.emp_user_id}" /></td>
											<td style="text-align: center;"><ctl:nullCv nullCheck="${dbAccessInq.emp_user_name}" /></td>
											<td style="text-align: center;"><ctl:nullCv nullCheck="${dbAccessInq.user_ip}" /></td>
											<td style="text-align: center;"><ctl:nullCv nullCheck="${dbAccessInq.system_name}" /></td>
											<%-- <td>
												<c:set var="prev" value=""/>
												<c:set var="cnt1" value="1"/>
												<c:set var="total" value="0"/>
												<c:forEach items="${dbAccessInq.result_type}">
													<c:set var="total" value="${total + 1 }"/>
												</c:forEach>
												
												<c:forEach items="${dbAccessInq.result_type}" var="item" varStatus="status">
													<c:choose>
														<c:when test="${prev eq item}">	<!-- 이전값과 같을 때 -->
															<c:set var="cnt1" value="${cnt1 + 1 }"/>
															<c:if test="${status.count == total }">${cnt1 }</c:if>
														</c:when>
														<c:otherwise> 					<!-- 이전값과 다를 때 -->
															<c:if test="${status.count != 1}">${cnt1 }</c:if>
															<c:forEach items="${CACHE_RESULT_TYPE}" var="i">
																<c:if test="${i.key == item }">
																	<img src="${rootPath }/resources/image/icon/resultType/${i.key}.png" title="${i.value }"/>
																</c:if>
															</c:forEach>
															
															<c:set var="prev" value="${item }"/>
															<c:set var="cnt1" value="1"/>
															<c:if test="${status.count == total }">${cnt1 }</c:if>
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</td> --%>
											<td style="text-align: left;">
												<c:choose>
													<c:when test="${fn:length(dbAccessInq.req_context) > 50}">
														${fn:substring(dbAccessInq.req_context, 0, 50)}...
													</c:when>
													<c:otherwise>
														<ctl:nullCv nullCheck="${dbAccessInq.req_context}" />
													</c:otherwise>
												</c:choose>
											</td>
										</tr>
										<c:set var="count" value="${count - 1 }" />
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
				</div>

				<div class="row" style="padding: 10px;">
					<!-- 페이징 영역 -->
					<c:if test="${search.total_count > 0}">
						<div id="pagingframe" align="center">
							<p><ctl:paginator currentPage="${search.page_num}" rowBlockCount="${search.size}" totalRowCount="${search.total_count}" /></p>
						</div>
					</c:if>
				</div>
			</div>
		</div>
	</div>
</div>

<form id="menuSearchForm" method="POST">
	<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" /> 
	<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" /> 
	<input type="hidden" name="system_seq" value="${paramBean.system_seq }" />
</form>
				
<script type="text/javascript">

	var dbAccessInqConfig = {
		"listUrl" : "${rootPath}/dbAccessInq/list.html",
		"detailUrl" : "${rootPath}/dbAccessInq/detail.html",
		"downloadUrl" : "${rootPath}/dbAccessInq/download.html"
	};
</script>