<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>
<div class="portlet-title">
       <div class="caption" style="width:50%; padding-top:9px; float: left;">
       	<i class="icon-settings font-dark"></i>
           <span class="caption-subject font-dark sbold uppercase">정보주체 정보</span>
       </div>
       <div style="text-align: right; float: right;">
	</div>
</div>
					    <div class="portlet-body" style="padding-top: 0px;">
				         <div class="row">
				             <div class="col-md-12">
				                 <table id="user" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer">
				                     <tbody>
		                     			<tr>
				                         	<th style="width:10%;text-align: center;">No. </th>
				                            <th style="width:30%;text-align: center;">정보주체1 </th>
				                            <th style="width:30%;text-align: center;">정보주체2</th>
				                         </tr>

				                         
				                         <c:choose>
											<c:when test="${empty sqlPrivacyResult}">
												<tr>
									        		<td colspan="3" align="center">데이터가 없습니다.</td>
									        	</tr>
											</c:when>
											<c:otherwise>
												<c:forEach items="${sqlPrivacyResult}" var="sqlPrivacy"  varStatus="status">
													<tr>
														<td align="center"><ctl:nullCv nullCheck="${sqlPrivacy.sql_group_idx}"/></td>
														<td align="center"><ctl:nullCv nullCheck="${sqlPrivacy.privacy_order1}"/></td>
														<td align="center"><ctl:nullCv nullCheck="${sqlPrivacy.privacy_order2}"/></td>
													</tr>
												<c:set var="count" value="${count - 1 }"/>
												</c:forEach>
											</c:otherwise>
										</c:choose>
				                     </tbody>
				                 </table>
				             </div>
				             
				         </div>
				         <div class="row">
				         	<div class="col-md-12" align="center">
				         		 <!-- 페이징 영역 -->
								<c:if test="${search.sqlPrivacyDetail.total_count > 0}">
									<div id="pagingframe">
										<p><ctl:paginator currentPage="${search.sqlPrivacyDetail.page_num}" rowBlockCount="${search.sqlPrivacyDetail.size}" totalRowCount="${search.sqlPrivacyDetail.total_count}" pageName="sqlPrivacyDetail"/></p>
									</div>
								</c:if>
				         	</div>
				         </div>
				         </div>