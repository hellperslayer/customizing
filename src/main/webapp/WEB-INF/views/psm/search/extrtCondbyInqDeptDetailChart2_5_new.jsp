<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="Preview page of Metronic Admin Theme #1 for dark mega menu option" name="description" />
<meta content="" name="author" />
<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>
<script>



//검색
function onSearch() {
	var fDate = document.getElementById("search_from").value;
	var tDate = document.getElementById("search_to").value;
	if(tDate<fDate){
		alert("검색 기간을 확인하세요");
		return;
	}
	getChart11();
	getChart12();
	getChart13();
	getChart14();
}

</script>

<div id="submenu5" style="display:none;">	  
	<div class="portlet box grey-salt" style="margin-bottom: 10px;">
		<div class="portlet-title" style="background-color: #2B3643;">
			<div class="caption">
				<img src="${rootPath}/resources/image/icon/search.png"> 검색
			</div>
			<div class="tools">
				<a href="javascript:;" class="collapse"></a>
			</div>
		</div>
		<!-- portlet-body S -->
		<div class="portlet-body form" style="display: block">
			<form id="listForm" method="POST" class="form-body">
				<div data-repeater-list="group-a">
					<div data-repeater-item class="mt-repeater-item">
						<div class="mt-repeater-input">
							<div class="row">
								<div class="col-md-12">														
									<div class="col-md-9">
									<br>
									<label class="control-label">일시</label><br>
										<div
											class="input-group input-medium date-picker input-daterange"
											data-date="10/11/2012" data-date-format="yyyy-mm-dd">
											<input type="text" class="form-control" id="search_from"
												name="search_from"
												value="${search.search_fromWithHyphen}"> <span
												class="input-group-addon"> &sim; </span> <input type="text"
												class="form-control" id="search_to" name="search_to"
												value="${search.search_toWithHyphen}">
										</div>
										<br>
									</div>
									<div class="col-md-3" align="right"
										style="padding-right: 20px;">
										<br><br><br>
										<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
											onclick="onSearch()">
											<i class="fa fa-check"></i> 검색
										</button>&nbsp;&nbsp;
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
		<!-- portlet-boyd E -->
	</div>
	<div class="m-grid m-grid-responsive-md m-grid-demo">
		<div class="m-grid-row" style="height:412px">
			<div class="m-grid-col m-grid-col-middle m-grid-col-center" style="padding: 10px; height: 380px;">
				<div class="portlet light bordered" style="margin-bottom:2px;">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-bar-chart font-green-haze"></i>
								<span class="caption-subject bold uppercase font-red-haze">부서가 처리하는 전체적인 개인정보 유형</span>
						</div>
					</div>
					<div class="poltlet-body">
						<div id="extrt_amchart_11" class="chart"></div>
					</div>
				</div>
			</div>	
			<div class="m-grid-col m-grid-col-middle m-grid-col-center" style="padding: 10px; height: 380px;">
				<div class="portlet light bordered" style="margin-bottom:2px;">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-bar-chart font-green-haze"></i>
							<span class="caption-subject bold uppercase font-red-haze">시스템별 고유식별정보 이용 현황</span>
						</div>
					</div>
					<div class="poltlet-body">
						<div id="extrt_amchart_12" class="chart"></div>
					</div>
				</div>
			</div>	
		</div>
		<div class="m-grid-row" style="height:412px">
			<div class="m-grid-col m-grid-col-middle m-grid-col-center" style="padding: 10px; height: 380px;">
				<div class="portlet light bordered" style="margin-bottom:2px;">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-bar-chart font-green-haze"></i>
							<span class="caption-subject bold uppercase font-red-haze">시스템별 처리량</span>
						</div>
					</div>
					<div class="poltlet-body">
						<div id="extrt_amchart_13" class="chart"></div>
					</div>
				</div>
			</div>	
			<div class="m-grid-col m-grid-col-middle m-grid-col-center" style="padding: 10px; height: 380px;">
				<div class="portlet light bordered" style="margin-bottom:2px;">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-bar-chart font-green-haze"></i>
							<span class="caption-subject bold uppercase font-red-haze">업무유형별 결과</span>
						</div>
					</div>
					<div class="poltlet-body">
						<div id="extrt_amchart_14" class="chart"></div>
					</div>
				</div>
			</div>	
		</div>
	</div>
</div>