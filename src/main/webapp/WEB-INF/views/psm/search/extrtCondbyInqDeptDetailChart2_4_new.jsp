<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="Preview page of Metronic Admin Theme #1 for dark mega menu option" name="description" />
<meta content="" name="author" />

<div id="submenu4" style="display:none;">     
<!--    <div class="row"> -->
<!--        <div class="portlet light bordered"> -->
            <div class="row margin-top-20">
                <div class="m-grid m-grid-responsive-md m-grid-demo">
                    <div class="m-grid-row" style="height:412px">
                        <div class="m-grid-col m-grid-col-middle m-grid-col-center" style="padding: 10px; height: 400px;">
                            <div class="portlet light bordered" style="margin-bottom:2px;">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-bar-chart font-green-haze"></i>
                                        <span class="caption-subject bold uppercase font-red-haze">
                                        시간대별 개인정보 처리 패턴 
                                        </span>
                                        <span class="caption-subject bold uppercase font-dark">
                                          <%-- (전일 총 처리 건수는 <fmt:formatNumber value="${chart10Tot }" pattern="#,###" /> 건 입니다.) --%>
                                          (최근 30일 동안의 총 처리 건수는 <fmt:formatNumber value="${chart10Tot }" pattern="#,###" /> 건 입니다.)
                                        </span>
                                    </div>
                                </div>
                                <div class="poltlet-body">
                                    <div id="extrt_amchart_10" class="chart"></div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
<!--        </div> -->
<!--    </div> -->
</div>