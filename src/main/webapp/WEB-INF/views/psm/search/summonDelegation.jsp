<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<c:set var="currentMenuId" value="${index_id}" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />


<script
	src="${rootPath}/resources/js/psm/search/summonDelegation.js"
	type="text/javascript" charset="UTF-8"></script>
  <%-- <script
	src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js"
	type="text/javascript"></script>   --%>
	<script	src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>

<style type="text/css">
	.input_date {
		background-color: #eef1f5;
		padding: 0.3em 0.5em;
		border: 1px solid #e4e4e4;
		height: 30px;
		font-size: 12px;
		color: #555;
	}
	.fade.in {
	    opacity: 0;
	}
</style>

<h1 class="page-title">${currentMenuName}</h1>
<div class="portlet light portlet-fit portlet-datatable bordered">
	<div class="portlet-body">
		<div class="table-container">

			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">

				<div class="dataTables_scroll" style="position: relative;">
					<!-- 개인정보 예외 정책 등록 -->
					<div class="caption"  style="padding-bottom: 5px;">
						<i class="icon-settings font-dark"></i> <span
							class="caption-subject font-dark sbold uppercase">소명 판정자 위임</span>
					</div>
					 <form id="misForm" method="POST" class="mt-repeater form-horizontal">
					<div>
						<table style="border-top: 1px solid #e7ecf1"
							class="table table-bordered" id="datatable_ajax_2"
							aria-describedby="datatable_ajax_2_info" role="grid"
							style="position: absolute; top: 0px; left: 0px; width: 100%;">
							<thead>
								<th width="25%"
									style="border-bottom: 1px solid #e7ecf1; text-align: center;">
									판정자</th>
								<th width="25%"
									style="border-bottom: 1px solid #e7ecf1; text-align: center;">
									위임자</th>
								<th width="35%"
									style="border-bottom: 1px solid #e7ecf1; text-align: center;">
									위임기간</th>
								<th width="15%"
									style="border-bottom: 1px solid #e7ecf1; text-align: center;">
									추가</th>
							</thead>
							<tbody>
								<tr>
									<td align="center"><input type="text" name="admin_user"
										id="admin_user" class="form-control input-medium" value=${userSession.admin_user_name} readonly></td>
									<td align="center">
										<select class="form-control input-medium" name="emp_user" id="emp_user">
											<option value=""> -------- 선 택 -------- </option>
											<c:forEach items="${extrtCondbyInqList.extrtCondbyInq}" var="extrtCondbyInq" varStatus="status">
												<option value="${extrtCondbyInq.emp_user_id}"
													<c:if test="${extrtCondbyInq.emp_user_id == search.emp_user_id }">selected="selected"</c:if>>${extrtCondbyInq.emp_user_name}
												</option>
											</c:forEach>
										</select>
									</td>
									<td align="center">
										<div class="input-group input-medium date-picker input-daterange"
											data-date="10/11/2012" data-date-format="yyyy-mm-dd">
											<input type="text" class="form-control" id="search_from" 
												name="search_from" value="${search.search_fromWithHyphen}"> 
												<span class="input-group-addon"> &sim; </span> 
											<input type="text" class="form-control" id="search_to" 
												name="search_to" value="${search.search_toWithHyphen}">
										</div> 
									</td>
									<td align="center">
										<button type="button"
											class="btn btn-sm blue btn-outline sbold uppercase"
											onclick="javascript:summonUserAdd()">
											<i class="fa fa-plus"></i> 위임
										</button>
									</td>
								</tr>
							</tbody>
						</table>
						<input type="hidden" name="dept_id" id="dept_id" value="${search.dept_id}" />
						<input type="hidden" name="auth_ids" id="auth_ids" value="${search.auth_ids}"/>	
					</div>
					</form>
					
					<div class="caption" style="padding-bottom: 5px;">
						<i class="icon-settings font-dark"></i> <span
							class="caption-subject font-dark sbold uppercase"> 소명 판정자 위임 현황</span>
					</div>
						<div>
							<table style="border-top: 1px solid #e7ecf1"
								class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
								id="datatable_ajax_2" aria-describedby="datatable_ajax_2_info"
								role="grid"
								style="position: absolute; top: 0px; left: 0px; width: 100%;">
								<thead>
									<tr role="row" class="heading"
										style="background-color: #c0bebe;">
										<th width="25%"
											style="border-bottom: 1px solid #e7ecf1; text-align: center;">
											판정자</th>
										<th width="25%"
											style="border-bottom: 1px solid #e7ecf1; text-align: center;">
											위임자</th>
										<th width="35%"
											style="border-bottom: 1px solid #e7ecf1; text-align: center;">
											위임기간</th>
										<th width="15%"
											style="border-bottom: 1px solid #e7ecf1; text-align: center;">
											취소</th>
									</tr>
								</thead>
								<tbody>
								<c:choose>
									<c:when test="${empty delegationList.delegation}">
										<tr>
											<td colspan="4" align="center">데이터가 없습니다.</td>
										</tr>
									</c:when>
									<c:otherwise>
										<c:forEach items="${delegationList.delegation}" var="delegation" varStatus="status">
											<tr>
												<td style="border-bottom: 1px solid #e7ecf1; text-align: center;">${delegation.approval_name }</td>
												<td style="border-bottom: 1px solid #e7ecf1; text-align: center;">${delegation.delegation_name }</td>
												<td style="border-bottom: 1px solid #e7ecf1; text-align: center;">${delegation.delegation_date }</td>
												<td style="text-align: center">
													<c:choose>
														<c:when test="${delegation.cancel_yn == 'N' }">
															<p class="btn btn-xs red btn-outline filter-cancel"
																onclick="javascript:summonUserRemove('${delegation.delegation_seq }', '${delegation.delegation_id }')">취소</p>	
														</c:when>
														<c:otherwise>
															취소완료
														</c:otherwise>
													</c:choose>
												</td>
											</tr>
										</c:forEach>
									</c:otherwise>
								</c:choose>
								</tbody>
							</table>
						</div>
					<div class="dataTables_processing DTS_Loading"
						style="display: none;">Please wait ...</div>
				</div>
				<div class="row" style="padding: 10px;">
					<!-- 페이징 영역 -->
					<c:if test="${delegationList.page_total_count > 0}">
						<div align="center" id="pagingframe">
							<p>
								<ctl:paginator currentPage="${paramBean.page_cur_num}"
									rowBlockCount="${paramBean.page_size}"
									totalRowCount="${delegationList.page_total_count}" />
							</p>
						</div>
					</c:if>
				</div>
			</div>

		</div>
	</div>
</div>

<script type="text/javascript">
	var summonDelegation = {
		"listUrl" : "${rootPath}/extrtCondbyInq/summmonDelegation.html"
	};
</script>