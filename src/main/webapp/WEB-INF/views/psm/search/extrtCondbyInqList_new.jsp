<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>

<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/search/extrtCondbyInqList.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>

<style>
#loading {
 width: 100%;   
 height: 100%;   
 top: 0px;
 left: 0px;
 position: fixed;   
 display: block;   
 opacity: 0.7;   
 background-color: #fff;   
 z-index: 99;   
 text-align: center; }  
  
#loading-image {   
 position: absolute;   
 top: 50%;   
 left: 50%;  
 z-index: 100; } 
</style>

<script type="text/javascript">
$(document).ready(function() {
	$('#loading').show();
});

$(window).load(function() {   
	$('#loading').hide();
});
</script>

<div id="loading"><img id="loading-image" width="30px;" src="${rootPath}/resources/image/loading3.gif" alt="Loading..." /></div>


<h1 class="page-title">
    ${currentMenuName} 
</h1>
<div class="portlet light portlet-fit portlet-datatable bordered">
    
    <div class="portlet-body">
        <div class="table-container">
            
            <div id="datatable_ajax_2_wrapper" class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
                
            <div class="dataTables_scroll" style="position: relative;">
                <div class="row">
                   <div class="col-md-6" style="width:100%; margin-top:10px; padding-left:0px; padding-right:0px;">
                      
                     <div class="portlet box grey-salt  ">
                           <div class="portlet-title" style="background-color: #2B3643;">
                               <div class="caption">
                                   <i class="fa fa-search"></i>검색 & 엑셀 </div>
                               <div class="tools">
                                    <a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
                               </div>
                           </div>
                           <div class="portlet-body form" id="searchBar">
                               <div class="form-body" style="padding-left:10px; padding-right:10px;">
                                   <div class="form-group">
                                       <form id="listForm" method="POST" class="mt-repeater form-horizontal">
                                           <div data-repeater-list="group-a">
                                               <div class="row">
                                                   <!-- jQuery Repeater Container -->
                                                   <div class="col-md-2">
                                                        <label class="control-label">기간</label>
                                                        <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
                                                            <input type="text" class="form-control" id="search_fr" name="search_from" value="${search.search_fromWithHyphen}"> 
                                                            <span class="input-group-addon"> &sim; </span> 
                                                            <input type="text" class="form-control" id="search_to" name="search_to" value="${search.search_toWithHyphen}">
                                                        </div>
                                                    </div>
                                                <c:if test="${ use_adverse eq 'Y' }">
                                                    <div class="col-md-2">
                                                       <label class="control-label">부적정 사용자 검색</label>
                                                       <br/>
                                                       <select class="form-control" id="use_adverse" name="use_adverse" <%-- onclick="javascript:initAdverseSelect();" --%> >
                                                            <option value="" class="daySelect_first"
                                                                <c:if test='${search.use_adverse eq ""}'>selected="selected"</c:if>>선택</option>
                                                            <option value="20"
                                                                <c:if test='${search.use_adverse eq "20"}'>selected="selected"</c:if>>부적정 사용자</option>
                                                                
                                                        </select>
                                                    </div>
                                                </c:if>
                                                    <div class="col-md-2">
                                                       <label class="control-label">시나리오</label>
                                                       <br/>
                                                       <select id="scen_seq" name="scen_seq" class="form-control" onchange="selScenario()">
                                                            <option value=""> ----- 선 택 ----- </option>
                                                            <c:forEach  items="${scenarioList}" var="scenarioList">
                                                                <option value="${scenarioList.scen_seq}" <c:if test="${scenarioList.scen_seq == search.scen_seq}">selected="selected"</c:if>>${scenarioList.scen_name}</option>
                                                            </c:forEach>
                                                            
                                                        </select>
                                                   </div>
                                                   <div class="col-md-2">
                                                       <label class="control-label">상세시나리오</label>
                                                       <br/>
                                                       <select id="rule_cd" name="rule_cd" class="form-control">
                                                            <option value=""> ----- 선 택 ----- </option>
                                                            <c:forEach  items="${ruleTblList}" var="ruleTblList">
                                                                <option value="${ruleTblList.rule_seq}" <c:if test="${ruleTblList.rule_seq == search.rule_cd}">selected="selected"</c:if>>${ruleTblList.rule_nm}</option>
                                                            </c:forEach>
                                                            <%-- <c:forEach items="${scenarioList }" var="scenarioList">
                                                                <option value="${scenarioList.scen_seq}" <c:if test="${scenarioList.scen_seq == search.scen_seq}">selected="selected"</c:if>>${scenarioList.scen_name}</option>
                                                            </c:forEach> --%>
                                                        </select>
                                                   </div>
                                                   <div class="col-md-2">
                                                        <label class="control-label">부서ID</label>
                                                        <br/>
                                                        <input type="text" class="form-control" name="dept_id" value="${search.dept_id}" />
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="control-label">부서명</label>
                                                        <br/>
                                                        <input type="text" class="form-control" name="dept_name" value="${search.dept_name}" />
                                                    </div>
                                                    <div class="col-md-2">
                                                       <label class="control-label">취급자ID</label>
                                                       <br/>
                                                       <input type="text" class="form-control" name="emp_user_id" value="${search.emp_user_id}" />
                                                       <input type="hidden" name="user_ip" value=""/>
                                                   </div>
                                                </div>
                                                <div class="row">
                                                	<div class="col-md-2">
                                                       <label class="control-label">취급자명</label>
                                                       <br/>
                                                       <input type="text" class="form-control" name="emp_user_name" value="${search.emp_user_name}" />
                                                   </div>
                                                        <div class="col-md-2">
                                                            <label class="control-label">시스템</label>
                                                            <br/>
                                                            <select name="system_seq"
                                                                class="form-control">
                                                                <option value=""
                                                                    ${search.system_seq == '' ? 'selected="selected"' : ''}>
                                                                    ----- 전 체 -----</option>
                                                                <c:if test="${empty systemMasterList}">
                                                                    <option>시스템 없음</option>
                                                                </c:if>
                                                                <c:if test="${!empty systemMasterList}">
                                                                    <c:forEach items="${systemMasterList}" var="i" varStatus="z">
                                                                        <option value="${i.system_seq}"
                                                                            ${i.system_seq==search.system_seq ? "selected=selected" : "" }>${ i.system_name}</option>
                                                                    </c:forEach>
                                                                </c:if>
                                                            </select>
                                                        </div>
                                               </div>
                                           </div>
                                           <hr/>
                                           <div align="right">
                                            <button type="reset" class="btn btn-sm red-mint btn-outline sbold uppercase" onclick="resetOptions(extrtCondbyInqConfig['listUrl'])">
                                                <i class="fa fa-remove"></i> <font>초기화
                                            </button>
                                            <button type="button" class="btn btn-sm blue btn-outline sbold uppercase" onclick="moveextrtCondbyInqList()">
                                                <i class="fa fa-search"></i> 검색
                                            </button>&nbsp;&nbsp;
											<!-- 20210312 hbjang 명지전문대 비정상위험분석 엑셀 커스터마이징 -->
                                           	<c:if test="${ui_type eq 'MJJ' }">
                                           		<div class="btn-group">
													<a data-toggle="dropdown"> <img src="${rootPath}/resources/image/icon/XLS_3.png">
													</a>
													<ul class="dropdown-menu pull-right">
														<li><a onclick="excelExtrtCondbyInqList('${search.total_count}')"> 리스트 </a></li>
														<li><a onclick="excelExtrtReport('${search.total_count}')"> 비정상위험 데이터 </a></li>
													</ul>
												</div>
                                           	</c:if>
                                           	<c:if test="${ui_type ne 'MJJ' }">
                                           		<a onclick="excelExtrtCondbyInqList('${search.total_count}')"><img src="${rootPath}/resources/image/icon/XLS_3.png"></a>	
                                           	</c:if>
                                        </div>
                                        <input type="hidden" name="main_menu_id" value="${search.main_menu_id }"/>
                                        <input type="hidden" name="sub_menu_id" value="${search.sub_menu_id }"/>
                                        <input type="hidden" name="current_menu_id" value="${currentMenuId}" />
                                        <input type="hidden" name="page_num" value="${search.page_num}" />
                                        <input type="hidden" name="detailEmpCd" value=""/>
                                       
                                        <input type="hidden" name="detailEmpDetailSeq" value=""/>
                                        <input type="hidden" name="isSearch" value="${search.isSearch }"/>
                                        <input type="hidden" name="userId" value=""/>
                                        
                                        <div id = "chart_extract">
                                       </form>
                                   </div>
                               </div>
                           </div>
                       </div>
                 </div>
             </div>     
             
             <div class="">    
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="6%" style="text-align: center;">일시</th>
                            <th width="9%" style="text-align: center;">비정상위험 시나리오</th>
                            <th width="22%" style="text-align: center;">비정상위험 상세시나리오</th>
                            <th width="7%" style="text-align: center;">시스템</th>
                            <th width="7%" style="text-align: center;">소속</th>
                            <th width="7%" style="text-align: center;">취급자ID</th>
                            <th width="7%" style="text-align: center;">취급자명</th>
                            <th width="5%" style="text-align: center;">위험도</th>
                            <th width="5%" style="text-align: center;">처리기준</th>
                            <th width="4%" style="text-align: center;">임계치</th>
                            <th width="5%" style="text-align: center;">횟수</th>
                            <th width="10%" style="text-align: center;">상세정보</th>
                          	<c:if test="${ui_type eq 'MJJ' }">
                          		<th width="5%" style="text-align: center;">보고서</th>
                          	</c:if>
                        </tr>
                    </thead>
                    <tbody>
                        <c:choose>
                        <c:when test="${empty extrtCondbyInqList.extrtCondbyInq}">
                            <tr>
                                <td colspan="12" align="center">데이터가 없습니다.</td>
                            </tr>
                        </c:when>
                        <c:otherwise>
                            <c:set value="${extrtCondbyInqList.page_total_count}" var="count"/>
                            <c:forEach items="${extrtCondbyInqList.extrtCondbyInq}" var="extrtCondbyInq" varStatus="status">
                                <tr>
                                    <c:set var="occr_dt" value="${extrtCondbyInq.occr_dt}"/>
                                    <td style="text-align: center;">${fn:substring(occr_dt, 0, 4)}-${fn:substring(occr_dt, 4, 6)}-${fn:substring(occr_dt, 6, fn:length(occr_dt))}&nbsp;
                                    </td>
                                    <td>${extrtCondbyInq.scen_name }</td>
                                    <td style ='padding-left:20px;' >
                                        <ctl:nullCv nullCheck="${extrtCondbyInq.rule_nm }"/>
                                    </td>
                                    <td style="text-align: center;">
                                        <ctl:nullCv nullCheck="${extrtCondbyInq.system_name }"/>
                                    </td>
                                    <c:choose>
                                    	<c:when test="${extrtCondbyInq.dept_name=='DEPT99999' }">
                                    		<td style="text-align: center;">
		                                       <ctl:nullCv nullCheck="${extrtCondbyInq.dept_name }"/>
		                                    </td>
                                    	</c:when>
                                    	<c:otherwise>
                                    		<td style="cursor: pointer; text-align: center;" onclick="fnExtrtDeptDetailInfo3('${extrtCondbyInq.dept_id }', '${extrtCondbyInq.occr_dt}')"  title="부서별 비정상위험분석 상세차트 페이지로 이동합니다.">
		                                        <ctl:nullCv nullCheck="${extrtCondbyInq.dept_name }"/>
		                                    </td>
                                    	</c:otherwise>
                                    </c:choose>
                                    <td style="text-align: center;"><ctl:nullCv nullCheck="${extrtCondbyInq.emp_user_id }"/>
                                        <input type="hidden" name="empDetailList[${status.index}].emp_user_id" value="${extrtCondbyInq.emp_user_id}" />
                                        <input type="hidden" name="empDetailList[${status.index}].emp_user_name" value="${extrtCondbyInq.emp_user_name}" />
                                    </td>                                   
                                    <td style="text-align: center;"><ctl:nullCv nullCheck="${extrtCondbyInq.emp_user_name }"/></td>
                                    <td style="text-align: center;">${extrtCondbyInq.dng_val }</td>
                                    <td style="text-align: center;">
                                    	<c:choose>
                                    		<c:when test="${extrtCondbyInq.rule_view_type =='R'}">이용량</c:when>
                                    		<c:otherwise>처리량</c:otherwise>
                                    	</c:choose>
                                    </td>
                                    <td style="text-align: center;">${extrtCondbyInq.limit_cnt }</td>
                                    <td style="text-align: center;">${extrtCondbyInq.rule_cnt }</td>
                                    <td align="center">
                                    <c:choose>
                                        <c:when test="${(extrtCondbyInq.emp_user_id ne null && extrtCondbyInq.emp_user_id ne '') && extrtCondbyInq.dept_name ne '-' }">
                                        
                                        	<button type="button" style="padding-top: 2px;padding-bottom: 2px; padding-right: 8px;padding-left: 8px;" class="btn blue mt-ladda-btn ladda-button btn-outline btn-circle"
                                        	onclick="scenarioDetail('${extrtCondbyInq.occr_dt}','${extrtCondbyInq.emp_user_id}','${extrtCondbyInq.emp_detail_seq}','${extrtCondbyInq.rule_cd}')"
                                        	data-style="slide-up" data-spinner-color="#333">
                                                <span class="ladda-label" style="font-size: 12px;">상세보기</span>
                                            <span class="ladda-spinner"></span></button>
                                        	<c:if test="${extrtCondbyInq.dept_name!='DEPT99999' and extrtCondbyInq.dept_name!='공통'}">
                                            <button type="button" style="padding-top: 2px;padding-bottom: 2px; padding-right: 8px;padding-left: 8px;" class="btn blue mt-ladda-btn ladda-button btn-outline btn-circle"
                                            onclick="fnExtrtEmpDetailInfo('${extrtCondbyInq.emp_user_id }', '${extrtCondbyInq.occr_dt}')" data-style="slide-up" data-spinner-color="#333">
                                                <span class="ladda-label" style="font-size: 12px;">업무패턴보기</span>
                                            <span class="ladda-spinner"></span></button>
                                            </c:if>
                                        </c:when>
                                        <c:otherwise>-</c:otherwise>
                                    </c:choose>
                                        <input type="hidden" name="empDetailList[${status.index}].emp_dept_id" value="${extrtCondbyInq.dept_id}" /> 
                                        <input type="hidden" name="empDetailList[${status.index}].emp_dept_name" value="${extrtCondbyInq.dept_name}" />
                                    </td>
                                    <c:if test="${ui_type eq 'MJJ' }">
                                    <td align="center">
                                    	<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
                                    		 onclick="showExtrtReport(${extrtCondbyInq.emp_detail_seq}, ${extrtCondbyInq.occr_dt})">보고서</button>
                                    </td>
                                    </c:if>
                                <c:set var="count" value="${count - 1 }"/>
                            </c:forEach>
                        </c:otherwise>
                        </c:choose>
                    </tbody>
                </table>
            </div>
             </div>
                 <div class="row" style="padding: 10px;">
                    <!-- <div class="col-md-8 col-sm-12">
                        <div class="dataTables_info">Showing 22 to 30 of 178 entries
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12"></div> -->
                    <!-- 페이징 영역 -->
                    <c:if test="${search.total_count > 0}">
                        <div id="pagingframe" align="center">
                            <p>
                                <ctl:paginator currentPage="${search.page_num}"
                                    rowBlockCount="${search.size}"
                                    totalRowCount="${search.total_count}" />
                            </p>
                        </div>
                    </c:if>
                </div>
            </div>
            
        </div>
    </div>   
</div>
<form id="moveForm" method="post">
<input type="hidden" name="occr_dt" value=""/>
<input type="hidden" name="emp_user_id" value=""/>
<input type="hidden" name="emp_detail_seq" value=""/>
 <input type="hidden" name="detailOccrDt" value=""/>
<input type="hidden" name="rule_cd" value=""/>
</form>

<%-- <form id="eventForm" method="POST" action="${rootPath}/extrtCondbyInq/list.html">
    <input type="hidden" name="search_from"/>
    <input type="hidden" name="search_to"/>
    <input type="hidden" name="dept_name"/>
    <input type="hidden" name="emp_user_id"/>
</form> --%>

<script type="text/javascript">

    var extrtCondbyInqConfig = {
        "listUrl":"${rootPath}/extrtCondbyInq/list.html"
        ,"detailUrl":"${rootPath}/extrtCondbyInq/detail.html"
        ,"downloadUrl" :"${rootPath}/extrtCondbyInq/download.html"
        ,"menu_id":"${currentMenuId}"
        ,"loginPage" : "${rootPath}/loginView.html"
        ,"detailChartUrl" : "${rootPath}/extrtCondbyInq/detailChart.html"
        ,"deptDetailChartUrl" : "${rootPath}/extrtCondbyInq/deptDetailChart.html"
        ,"extrtReportUrl" : "${rootPath}/extrtCondbyInq/showExtrtReport.html"
        ,"extrtExcelUrl" : "${rootPath}/extrtCondbyInq/extrtExcelDownload.html"
    };

    var menu_id = extrtCondbyInqConfig["menu_id"];
    var log_message = "SUCCESS";
    var log_action = "SELECT";
    
</script>