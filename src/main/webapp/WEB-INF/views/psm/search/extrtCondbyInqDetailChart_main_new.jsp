<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/dashboard/highcharts.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/exporting.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/globalize.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/knockout-3.0.0.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/dx.chartjs.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/zoomingData.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/dashboard/vivagraph.js" type="text/javascript" charset="UTF-8"></script>


<script src="${rootPath}/resources/js/psm/search/extrtCondbyInqList.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/search/extrtCondbyInqDetailChart.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/search/extrtCondbyInqTimeline.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>





<!-- BEGIN THEME GLOBAL STYLES -->
<link href="${rootPath}/resources/css/plugins.min.css" rel="stylesheet" type="text/css">
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<%-- <link href="${rootPath}/resources/css/layout.min.css" rel="stylesheet" type="text/css"> --%>
<link href="${rootPath}/resources/css/custom.min.css" rel="stylesheet" type="text/css">
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="http://localhost:8080/PSM-3.0.2/abnormal/favicon.ico"> 

<script src="${rootPath}/resources/js/jquery.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/bootstrap.min.js" type="text/javascript"></script>

<%-- <script src="${rootPath}/resources/js.cookie.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/jquery.blockui.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/bootstrap-switch.min.js" type="text/javascript"></script> --%>

<!-- END CORE PLUGINS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="${rootPath}/resources/js/layout.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/demo.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/quick-sidebar.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/quick-nav.min.js" type="text/javascript"></script>
<link href="${rootPath}/resources/css/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color">

<script src="${rootPath}/resources/js/datatable.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/datatables.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/datatables.bootstrap.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/menu-util.js" type="text/javascript"></script>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
 

<script type="text/javascript" src="${rootPath}/resources/js/fabric.min.js" async=""></script>
<script type="text/javascript" src="${rootPath}/resources/js/FileSaver.min.js" async=""></script>

<link href="${rootPath}/resources/css/profile.min.css" rel="stylesheet" type="text/css">
<style>
.tabbable-custom>.nav-tabs>li.active{
border-top: 3px solid #578ebe;
}
</style>

<style>
#loading {
 width: 100%;   
 height: 100%;   
 top: 0px;
 left: 0px;
 position: fixed;   
 display: block;   
 opacity: 0.7;   
 background-color: #fff;   
 z-index: 99;   
 text-align: center; }  
  
#loading-image {   
 position: absolute;   
 top: 50%;   
 left: 50%;  
 z-index: 100; } 
</style>

<script type="text/javascript">
$(window).load(function() {    
	$('#loading').hide();
});
</script>

<div id="loading"><img id="loading-image" width="30px;" src="${rootPath}/resources/image/loading3.gif" alt="Loading..." /></div>

<script type="text/javascript">
function onSubmenu(id){
	document.getElementById("submenu1").style.display="none";
	document.getElementById("submenu2").style.display="none";
	document.getElementById("submenu3").style.display="none";
	document.getElementById("submenu4").style.display="none";
	document.getElementById("submenu5").style.display="none";
	document.getElementById("submenu"+id).style.display="block";

	document.getElementById("sub1").style.fontWeight="normal";
	document.getElementById("sub2").style.fontWeight="normal";
	document.getElementById("sub3").style.fontWeight="normal";
	document.getElementById("sub4").style.fontWeight="normal";
	document.getElementById("sub5").style.fontWeight="normal";
	document.getElementById("sub"+id).style.fontWeight="bold";
	
	setTab(2, id);
}
</script>

<h1 class="page-title"> ${currentMenuName}
    <!--bold font-blue-hoki <small>basic bootstrap tables with various options and styles</small> -->
</h1>

<div class="contents left">
 		
<form id="listForm" method="POST">

<div class="row" style="background:#eef1f5; padding-top: 20px; padding-bottom: 20px">
	<input type="hidden" id="use_detailchart" value="${use_detailchart }"/>
	<input type="hidden" id="dept_id" name="dept_id" value="${userInfo.dept_id}">
	<input type="hidden" id="search_from_chart" name="search_from_chart" value="${search.search_from}">
	<input type="hidden" id="search_to_chart" name="search_to_chart" value="${search.search_to}">
	<div class="col-md-12">
		<!-- BEGIN PROFILE SIDEBAR -->
		<div class="profile-sidebar">
			<!-- PORTLET MAIN -->
			<div class="portlet light profile-sidebar-portlet "style=height:147%">
				<!-- SIDEBAR USERPIC -->
				<div class="profile-userpic" style="margin-bottom: -5px; ">
					<img src="${rootPath}/resources/image/common/profile_user2.png" class="img-responsive" alt="profile">
				</div>
				<!-- END SIDEBAR USERPIC -->
				<!-- SIDEBAR USER TITLE -->
				<div class="profile-usertitle">
					<div class="profile-usertitle-name"style="margin-bottom: 10px; ">${userInfo.emp_user_name}</div>
					<%-- <div class="profile-usertitle-job"style="margin-bottom: 15px; ">(${data1.emp_user_id})</div> --%>
				</div>
				<!-- END SIDEBAR USER TITLE -->
				<!-- SIDEBAR BUTTONS -->
			</div>
			<!-- END PORTLET MAIN -->
			<!-- PORTLET MAIN -->
			<div class="portlet light " style="margin-top: -10px;">
				<!-- STAT -->
				<div class="row">
					<h4 class="profile-desc-title" style="padding-bottom: 10px;">프로필</h4>
					<div class="margin-top-20 profile-desc-link"
						style="margin-top: -20px !important;">
						<table class="table table-hover table-light">

							<tbody>
								<tr>
									<td><b>소속</b> : <a
										onclick="fnExtrtDeptDetailChart('${userInfo.dept_id }', '${search.search_to }')">${userInfo.dept_name}</a>
									</td>
								</tr>
								<tr>
									<td><b>IP</b> : ${userInfo.user_ip}</td>
								</tr>
								<tr>
									<td><b>ID</b> : ${userInfo.emp_user_id}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="row list-separated profile-stat">
				  <h4 class="profile-desc-title">업무 시스템</h4>
					<div class="tab-context">
						<div  class="clearfix">
							<c:forEach items="${data2}" var="system" varStatus="status">
								<c:if test="${system.system_name != null}">
								<a class="btn btn-xs blue" style="cursor:default; margin-top: 2px; margin-bottom: 2px;">
										${system.system_name}
										<i class="fa fa-laptop"></i>
			                       	 </a>
								</c:if>								
							</c:forEach>
							
	                    </div>
                    </div>
				
				</div>
				<!-- END STAT -->
				<div class="row" style="margin-top: -15px !important;">					
					<br><br><div class="margin-top-20 profile-desc-link" style="margin-top: 2px !important;t">
						
						<c:if test="${data1.dng_grade == '심각' }"><i class="fa fa-globe"></i><span style="font-size: 15px;"><b style="color: red;">심각 단계</b>는 <b><br>위험도 높은 비정상 개인정보 처리가 다수 발생되어 전사 차원의 매우 심각한 수준의 손실이나 해당 업무의 실패 혹은 경쟁적 지위를 상실할 수 있는 단계입니다.</b></span> </c:if>
						<c:if test="${data1.dng_grade == '경계' }"><i class="fa fa-globe"></i><span style="font-size: 15px;"><b style="color: orange;">경계 단계</b>는 <b><br>다수의 비정상 개인정보 처리가 발생되어 매우 심각한 손실이나 해당 업무가 어려워질 수 있는 정도로 업무에 중대한 영향이 발생할 수 있는 단계입니다.</b></span></c:if>
						<c:if test="${data1.dng_grade == '주의' }"><i class="fa fa-globe"></i><span style="font-size: 15px;"><b style="color: yellow;">주의 단계</b>는 <b><br>여러 항목의 비정상 개인정보 처리가 발생되어 손실이나 해당 업무에 부정적인 영향을 줄 수 있는 정도의 문제가 발생할 수 있는 단계입니다. </b></span></c:if>
						<c:if test="${data1.dng_grade == '관심' }"><i class="fa fa-globe"></i><span style="font-size: 15px;"><b style="color: blue;">관심 단계</b>는 <b><br>일부 비정상 개인정보 처리가 발생되어 해당영역 또는 해당서비스에 작은 영향을 줄 수 있을 정도의 문제가 발생할 수 있는 단계입니다. </b></span></c:if>
						<c:if test="${data1.dng_grade == '정상' }"><i class="fa fa-globe"></i><span style="font-size: 15px;"><b style="color: green;">정상 단계</b>는 <b><br>업무 수행 상 허용된 범위 내에 개인정보 처리가 발생되어 업무적으로 영향이 거의 없는 정도로 파급효과가 미약한 단계입니다.</b></span></c:if>
					</div>
				</div>
			</div>
			<!-- END PORTLET MAIN -->
		</div>
		<!-- END BEGIN PROFILE SIDEBAR -->
		<!-- BEGIN PROFILE CONTENT -->
		<div class="profile-content">
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light ">
						<div class="portlet-title tabbable-line">
							<div class="caption caption-md">
								<i class="icon-globe theme-font hide"></i> <span class="caption-subject font-blue-madison bold uppercase">업무패턴 정보</span>
							</div>
						</div>
						<div class="portlet-body">
							<div class="tabbable tabbable-custom">
							    <ul class="nav nav-tabs">
							    	<li <c:if test="${tabFlag == 'tab1' }">class="active"</c:if>>
							            <a href="#tab1" data-toggle="tab" onclick="setTab(1)"> 위험현황 </a>
							        </li>
							        <li <c:if test="${tabFlag == 'tab2' }">class="active"</c:if>>
							            <a href="#tab2" data-toggle="tab" onclick="setTab(2,1)"> 개인정보 취급패턴 </a>
							        </li>   
							        <li <c:if test="${tabFlag == 'tab3' }">class="active"</c:if>>
							            <a href="#tab3" data-toggle="tab" onclick="setTab(3)"> 시스템접근 타임라인 </a>
							        </li>
<%-- 
							        <li <c:if test="${tabFlag == 'tab4' }">class="active"</c:if>>
							            <a href="#tab4" data-toggle="tab"> 소명 처리 현황 </a>
							        </li>   
 --%>
 									<%-- <li <c:if test="${tabFlag == 'tab5' }">class="active"</c:if>>
							            <a href="#tab5" data-toggle="tab"> 개인누적 평균건수 </a>
							        </li> --%>
							    </ul>
								<input type="hidden" id="tabId" value="${tabFlag }" />
							   
								<!-- END HEADER MENU -->		
								<!-- BEGIN PAGE BASE CONTENT -->
								<div class="tab-content">
											
									<!-- <form id="dashForm" method="POST"> -->
									<!-- 위험현황 항목 영역 -->
									<%@include file="/WEB-INF/views/psm/search/extrtCondbyInqDetailChart1_new.jsp"%>
								    
								    <!-- 개인정보 취급패턴 항목 영역 -->
								    <%@include file="/WEB-INF/views/psm/search/extrtCondbyInqDetailChart2_new.jsp"%>
								    
								    <!-- 시스템접근 타임라인 항목 영역 -->
								    <%@include file="/WEB-INF/views/psm/search/extrtCondbyInqDetailChart3_new.jsp"%>
								    
								    <!-- 소명 처리 현황 항목 영역 -->
<%-- 								    <%@include file="/WEB-INF/views/psm/search/extrtCondbyInqDetailChart4_new.jsp"%> --%>
								    
								    <!-- 평균건수 항목 영역 -->
<%-- 								    <%@include file="/WEB-INF/views/psm/search/extrtCondbyInqDetailChart5_new.jsp"%> --%>
								    <!-- </form> -->
								    
								</div>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END PROFILE CONTENT -->
	</div>
</div>
</form>
</div>

<form id="detailchartForm" method="POST">
	<input type="hidden" name="dept_id" />
	<input type="hidden" name="detailOccrDt" />
</form>
