<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="MENU00048" />
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<link href="${rootPath}/resources/css/common/jquery-ui.dynatree.css" rel="stylesheet" type="text/css">
<script src="${rootPath }/resources/js/common/jquery-ui-1.9.0.custom.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/jquery.dynatree.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath }/resources/js/common/jquery.cookie.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/system_management/departmentList.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/search/empDetailInqList.js" type="text/javascript" charset="UTF-8"></script>


<form id="listForm" method="POST">
	
	<div class="contents left">
		<div id="labelDiv">
			<label class="action_label"><span class="option_name">${currentMenuName}</span></label>
		</div>
		
		<div class="detail_option">· 직원정보</div>
		<table class="table left">
			<colgroup>
				<col width="40px"/>				
				<col width="40px"/>				
				<col width="40px"/>				
				<col width="40px"/>				
			</colgroup>
			<thead>
				<tr>
					<th scope="col">사용자명</th>
					<th scope="col">부서</th>
					<th scope="col">사번</th>
					<th scope="col">사용자IP</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty empUser}">
						<tr>
			        		<td colspan="8" align="center">데이터가 없습니다.</td>
			        	</tr>
					</c:when>
					<c:otherwise>
						<tr>
							<td><ctl:nullCv nullCheck="${empUser.emp_user_name}"/></td>
							<td><ctl:nullCv nullCheck="${empUser.dept_name}"/></td>
							<td><ctl:nullCv nullCheck="${empUser.emp_user_id}"/></td>
							<td><ctl:nullCv nullCheck="${empUser.ip}"/></td>
						</tr>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
		
		<div class="detail_option">· 직원별추출조건</div>
		<table class="table left">
			<colgroup>
				<col width="40px"/>				
				<col width="120px"/>				
				<col width="40px"/>				
			</colgroup>
			<thead>
				<tr>
					<th scope="col">발생일자</th>
					<th scope="col">추출조건</th>
					<th scope="col">위험도</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty empDetailInq}">
						<tr>
			        		<td colspan="8" align="center">데이터가 없습니다.</td>
			        	</tr>
					</c:when>
					<c:otherwise>
						<c:set value="${pageInfo.total_count}" var="count"/>
						<c:forEach items="${empDetailInq}" var="empDetailInq" varStatus="status">
							<tr style ='cursor: pointer;' onclick="javascript:fnExtrtCondbyDetailList('${empDetailInq.occr_dt}','${empUser.emp_user_id}','${empDetailInq.emp_detail_seq}');" <c:if test="${status.count % 2 == 0 }"> class='tr_gray'</c:if>>
								<c:if test="${empDetailInq.occr_dt ne null}">
									<td>
										<fmt:parseDate value="${empDetailInq.occr_dt}" pattern="yyyyMMdd" var="occr_dt" /> 
										<fmt:formatDate value="${occr_dt}" pattern="YY-MM-dd" />
									</td>
								</c:if>
								<c:if test="${empDetailInq.occr_dt eq null}"><td>-</td></c:if>
								
								<td><ctl:nullCv nullCheck="${empDetailInq.rule_nm}"/></td>
								
								<c:if test="${empDetailInq.dng_val ne null}">
									<td>
										<fmt:formatNumber value="${empDetailInq.dng_val }" type="number" />
									</td>
								</c:if>
								<c:if test="${empDetailInq.dng_val eq null}"><td>-</td></c:if>
							</tr>
							<c:set var="count" value="${count - 1 }"/>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
		
		<!-- 메뉴 관련 input 시작 -->
		<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" />
		<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
		<input type="hidden" name="current_menu_id" value="${currentMenuId }" />
		<!-- 메뉴 관련 input 끝 -->
		
		<!-- 페이지 번호 -->
		<input type="hidden" name="page_num" value="${search.page_num }" />
		<input type="hidden" name="allLogInqDetail_page_num" value="${search.allLogInqDetail.page_num }" />
		<input type="hidden" name="empDetailInqDetail_page_num" value="${search.empDetailInqDetail.page_num }" />
		
		<!-- 위험도별 조회 검색조건 관련 input 시작 -->
		<input type="hidden" name="search_from" value="${search.search_from }" />
		<input type="hidden" name="search_to" value="${search.search_to }" />
		<input type="hidden" name="daySelect" value="${search.daySelect }" />
		<input type="hidden" name="emp_user_id" value="${search.emp_user_id}" />
		<input type="hidden" name="emp_user_name" value="${search.emp_user_name}" />
		<input type="hidden" name="dng_val" value="${search.dng_val }" />
		<input type="hidden" name="dept_name" value="${search.dept_name}" />
		<!-- 위험도별 조회 검색조건 관련 input 끝 -->
		
		<!-- Detail조건 input시작 -->
		<input type="hidden" name="detailEmpCd" value="${search.detailEmpCd }"/>
		<input type="hidden" name="detailStartDay" value="${search.detailStartDay }"/>
		<input type="hidden" name="detailEndDay" value="${search.detailEndDay }"/>
		<input type="hidden" name="detailOccrDt" value="${search.detailOccrDt }"/>
		<input type="hidden" name="detailEmpDetailSeq" value="${search.detailEmpDetailSeq }"/>
		<input type="hidden" name="bbs_id" value="${search.bbs_id }"/>
		<!-- Detail조건 input끝 -->
		
		<!-- 페이징 영역 -->
		<c:if test="${search.empDetailInqDetail.total_count > 0}">
			<div class="page left" id="pagingframe">
				<p><ctl:paginator currentPage="${search.empDetailInqDetail.page_num}" rowBlockCount="${search.empDetailInqDetail.size}" totalRowCount="${search.empDetailInqDetail.total_count}" pageName="empDetailInqDetail"/></p>
			</div>
		</c:if>
		
		<!-- 버튼 영역 -->
		<div class="option_btn right">
			<p class="right">
				<a class="btn gray" onclick="javascript:goList();">목록</a>
			</p>
		</div>
	</div>
</form>

<script type="text/javascript">

	var empDetailInqConfig = {
		"listUrl":"${rootPath}/empDetailInq/list.html"
		,"detailUrl":"${rootPath}/empDetailInq/detail.html"
	};
	
	var extrtCondbyInqDetailConfig = {
		"detailUrl":"${rootPath}/extrtCondbyInq/detail.html"
	};

</script>
