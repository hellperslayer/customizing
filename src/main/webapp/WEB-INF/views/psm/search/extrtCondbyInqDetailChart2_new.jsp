<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="Preview page of Metronic Admin Theme #1 for dark mega menu option" name="description" />
<meta content="" name="author" />

<div class="tab-pane <c:if test="${tabFlag == 'tab2' }">active</c:if>" id="tab2">	  
	<div class="row">
		<div class="mt-element-step" style="margin:20px;">
			<div class="row step-line">
				<div class="submenu">
					<a href="#" onclick="javascript:onSubmenu('1');">
			    		<span id="sub1" style="font-weight:bold">
			    			개인정보 취급량
			    		</span>
					</a>
		    		&nbsp;|&nbsp;
					<a href="#" onclick="javascript:onSubmenu('2');">
						<span id="sub2">
							월별 누적량 비교
						</span>
					</a>
		    		&nbsp;|&nbsp;
					<a href="#" onclick="javascript:onSubmenu('3');">
						<span id="sub3">
							요일별 패턴
						</span>
					</a>
		    		&nbsp;|&nbsp;
					<a href="#" onclick="javascript:onSubmenu('4');">
						<span id="sub4">
							24 시간 패턴
						</span>
					</a>
		    		&nbsp;|&nbsp;
					<a href="#" onclick="javascript:onSubmenu('5');">
						<span id="sub5">
							취급 유형
						</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="portlet light">
		<div class="portlet-title">
			<div class="submenu-content">
						
				<!-- <form id="dashForm" method="POST"> -->
				<!-- 개인정보 취급량 항목 영역 -->
				<%@include file="/WEB-INF/views/psm/search/extrtCondbyInqDetailChart2_1_new.jsp"%>
			    
			    <!-- 월별 누적량 비교 항목 영역 -->
			    <%@include file="/WEB-INF/views/psm/search/extrtCondbyInqDetailChart2_2_new.jsp"%>
			    
			    <!-- 일주일 패턴 항목 영역 -->
			    <%@include file="/WEB-INF/views/psm/search/extrtCondbyInqDetailChart2_3_new.jsp"%>
			    
			    <!-- 24 시간 패턴 항목 영역 -->
			    <%@include file="/WEB-INF/views/psm/search/extrtCondbyInqDetailChart2_4_new.jsp"%>
			    
			    <!-- 취급 유형 항목 영역 -->
			    <%@include file="/WEB-INF/views/psm/search/extrtCondbyInqDetailChart2_5_new.jsp"%>
			    <!-- </form> -->
			    
			</div>	
		</div>
	</div>
</div>