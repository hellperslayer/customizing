<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/search/reqLogInqList.js" type="text/javascript" charset="UTF-8"></script>
<link href="${rootPath}/resources/css/profile.min.css" rel="stylesheet" type="text/css">


<h1 class="page-title"> ${currentMenuName}
    <!--bold font-blue-hoki <small>basic bootstrap tables with various options and styles</small> -->
</h1>

<div class="contents left">
 		

<div class="row" style="background:#eef1f5; padding-top: 20px; padding-bottom: 20px">
	<input type="hidden" id="emp_user_id" name="emp_user_id" value="${reqLogInq.emp_user_id}">
	<div class="col-md-12">
		<!-- BEGIN PROFILE SIDEBAR -->
		<div class="profile-sidebar">
			<!-- PORTLET MAIN -->
			<div class="portlet light profile-sidebar-portlet "style=height:147%">
				<!-- SIDEBAR USERPIC -->
				<div class="profile-userpic" style="margin-bottom: -5px; ">
					<img src="${rootPath}/resources/image/common/profile_user2.png" class="img-responsive" alt="profile">
				</div>
				<!-- END SIDEBAR USERPIC -->
				<!-- SIDEBAR USER TITLE -->
				<div class="profile-usertitle">
					<div class="profile-usertitle-name"style="margin-bottom: 10px; "><ctl:nullCv nullCheck="${reqLogInq.emp_user_name}"/></div>
					<%-- <div class="profile-usertitle-job"style="margin-bottom: 15px; ">(${data1.emp_user_id})</div> --%>
				</div>
				<!-- END SIDEBAR USER TITLE -->
				<!-- SIDEBAR BUTTONS -->
			</div>
			<!-- END PORTLET MAIN -->
			<!-- PORTLET MAIN -->
			<div class="portlet light" style="height: 770px;">
				<!-- STAT -->
				<div class="row">
					<h4 class="profile-desc-title" style="padding-bottom: 10px; ">프로필</h4>
					<div class="margin-top-20 profile-desc-link" style="margin-top: -20px !important;">
						<table class="table table-hover table-light">
	                        <tbody>
	                            <tr>
	                                <td> <b>소속</b> : <ctl:nullCv nullCheck="${reqLogInq.dept_name}"/> </td>
	                            </tr>
	                            <tr>
	                                <td> <b>IP</b> : ${reqLogInq.user_ip}</td>
	                            </tr> 
	                            <tr>
	                            	<td> <b>ID</b> : ${reqLogInq.emp_user_id} </td>
	                            </tr>
	                        </tbody>
                        </table>
					</div>
				</div>
				<div class="row list-separated profile-stat">
				  <h4 class="profile-desc-title">업무 시스템</h4>
					<div class="tab-context">
						<div  class="clearfix">
							<c:forEach items="${systemList}" var="system" varStatus="status">
								<c:if test="${system.system_name != null}">
									<a style="cursor:default" class="btn btn-xs blue"
										<%-- <c:if test="${status.count % 4 == 0 }"> class="btn btn-xs red"</c:if>
										<c:if test="${status.count % 4 == 1 }"> class="btn btn-xs yellow"</c:if>
										<c:if test="${status.count % 4 == 2 }"> class="btn btn-xs green"</c:if>
										<c:if test="${status.count % 4 == 3 }"> class="btn btn-xs blue"</c:if>	 --%>						
										style="margin-top: 5px; margin-bottom: 5px;">${system.system_name}
										<i class="fa fa-laptop"></i>
			                       	 </a>
								</c:if>								
							</c:forEach>
	                    </div>
                    </div>
				</div>
				<!-- END STAT -->
				<div class="row" style="margin-top: -15px !important;">					
					<%-- <br><br><div class="margin-top-20 profile-desc-link" style="margin-top: 2px !important;t">
						
						<c:if test="${data1.dng_grade == '심각' }"><i class="fa fa-globe"></i><span style="font-size: 15px;"><b style="color: red;">심각 단계</b>는 <b><br>위험도 높은 비정상 개인정보 처리가 다수 발생되어 전사 차원의 매우 심각한 수준의 손실이나 해당 업무의 실패 혹은 경쟁적 지위를 상실할 수 있는 단계입니다.</b></span> </c:if>
						<c:if test="${data1.dng_grade == '경계' }"><i class="fa fa-globe"></i><span style="font-size: 15px;"><b style="color: orange;">경계 단계</b>는 <b><br>다수의 비정상 개인정보 처리가 발생되어 매우 심각한 손실이나 해당 업무가 어려워질 수 있는 정도로 업무에 중대한 영향이 발생할 수 있는 단계입니다.</b></span></c:if>
						<c:if test="${data1.dng_grade == '주의' }"><i class="fa fa-globe"></i><span style="font-size: 15px;"><b style="color: yellow;">주의 단계</b>는 <b><br>여러 항목의 비정상 개인정보 처리가 발생되어 손실이나 해당 업무에 부정적인 영향을 줄 수 있는 정도의 문제가 발생할 수 있는 단계입니다. </b></span></c:if>
						<c:if test="${data1.dng_grade == '관심' }"><i class="fa fa-globe"></i><span style="font-size: 15px;"><b style="color: blue;">관심 단계</b>는 <b><br>일부 비정상 개인정보 처리가 발생되어 해당영역 또는 해당서비스에 작은 영향을 줄 수 있을 정도의 문제가 발생할 수 있는 단계입니다. </b></span></c:if>
						<c:if test="${data1.dng_grade == '정상' }"><i class="fa fa-globe"></i><span style="font-size: 15px;"><b style="color: green;">정상 단계</b>는 <b><br>업무 수행 상 허용된 범위 내에 개인정보 처리가 발생되어 업무적으로 영향이 거의 없는 정도로 파급효과가 미약한 단계입니다.</b></span></c:if>
					</div> --%>
				</div>
			</div>
			<!-- END PORTLET MAIN -->
		</div>
		<!-- END BEGIN PROFILE SIDEBAR -->
		<!-- BEGIN PROFILE CONTENT -->
		<div class="profile-content">
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light" style="height: 1020px;">
						<!-- <div class="portlet-title tabbable-line">
							<div class="caption caption-md">
								<i class="icon-globe theme-font hide"></i> <span class="caption-subject font-blue-madison bold uppercase">Profile
									Account</span>
							</div>
						</div> -->
						<div class="portlet-title">
					        <div class="caption">
					            <i class="icon-settings font-dark"></i>
					            <span class="caption-subject font-dark sbold uppercase">접근 이력</span>
					        </div>
					    </div>
						
						<div class="portlet-body">
							<div class="row">
								<div class="col-md-12">
									<table id="user"
										class="table table-striped table-bordered table-hover table-checkable dataTable no-footer">
										<tbody>
											<tr>
												<th style="text-align: center;width: 10%">No. </th>
												<th style="text-align: center;width: 10%">일시</th>
												<th style="text-align: center;width: 15%">사용자IP</th>
												<th style="text-align: center;width: 20%">시스템명</th>
												<th style="text-align: center;width: 45%">접근경로</th>
											</tr>
				
											<c:choose>
												<c:when test="${empty reqLogInqDetailList}">
													<tr>
														<td colspan="5" align="center">데이터가 없습니다.</td>
													</tr>
												</c:when>
												<c:otherwise>
													<c:set value="${search.reqLogInqDetail.total_count}" var="count" />
													<c:forEach items="${reqLogInqDetailList}"
														var="item" varStatus="status">
														<tr>
															<td align="center">${count - search.reqLogInqDetail.page_num * 20 + 20 }</td>
															<c:if
																test="${item.proc_date ne null and item.proc_time ne null}">
																<td width="14%" style="text-align: center;"><fmt:parseDate value="${item.proc_date}"
																		pattern="yyyyMMdd" var="proc_date" /> <fmt:formatDate
																		value="${proc_date}" pattern="YY-MM-dd" /> <fmt:parseDate
																		value="${item.proc_time}" pattern="HHmmss"
																		var="proc_time" /> <fmt:formatDate value="${proc_time}"
																		pattern="HH:mm:ss" /></td>
															</c:if>
															<c:if
																test="${item.proc_date eq null or item.proc_time eq null}">
																<td>-</td>
															</c:if>
															<td align="center"><ctl:nullCv
																	nullCheck="${item.user_ip}" /></td>
															<td align="center"><ctl:nullCv
																	nullCheck="${item.system_name}" /></td>
															<c:if test="${item.req_url ne null}">
																<c:choose>
																	<c:when test="${fn:length(item.req_url) > 70}">
																		<td style="text-align: left"
																			title="${item.req_url}">
																			${fn:substring(item.req_url, 0, 70)}...</td>
																	</c:when>
																	<c:otherwise>
																		<td style="text-align: left"><ctl:nullCv
																				nullCheck="${item.req_url}" /></td>
																	</c:otherwise>
																</c:choose>
															</c:if>
	
															<c:if test="${item.req_url eq null}">
																<td>-</td>
															</c:if>
														</tr>
														<c:set var="count" value="${count - 1 }" />
													</c:forEach>
												</c:otherwise>
											</c:choose>
										</tbody>
									</table>
									
									<form id="listForm" method="POST">
									<!-- 메뉴 관련 input 시작 -->
									<input type="hidden" name="main_menu_id" value="${search.main_menu_id }" />
									<input type="hidden" name="sub_menu_id" value="${search.sub_menu_id }" />
									<input type="hidden" name="current_menu_id" value="${currentMenuId }" />
									<!-- 메뉴 관련 input 끝 -->
									
									<!-- 페이지 번호 -->
									<input type="hidden" name="page_num" value="${search.page_num }" />
									<input type="hidden" name="reqLogInqDetail_page_num" value="${search.reqLogInqDetail.page_num }" />
									
									<!-- 검색조건 관련 input 시작 -->
									<input type="hidden" name="search_from" value="${search.search_from }" />
									<input type="hidden" name="search_to" value="${search.search_to }" />
									<input type="hidden" name="emp_user_id" value="${search.emp_user_id }" />
									<input type="hidden" name="daySelect" value="${search.daySelect }" />
									<input type="hidden" name="user_ip" value="${search.user_ip }" />
									<input type="hidden" name="system_seq" value="${search.system_seq }" />
									<input type="hidden" name="req_url" value="${search.req_url }" />
									<input type="hidden" name="emp_user_name" value="${search.emp_user_name}" />
									<input type="hidden" name="dept_name" value="${search.dept_name}" />
									<input type="hidden" name="reqLogInqDetail_user_id" value="${search.reqLogInqDetail_user_id}" />
									<input type="hidden" name="detailLogSeq" value="${search.detailLogSeq}" />
									<input type="hidden" name="isSearch" value="${search.isSearch}" />
									</form>
								</div>
				
							</div>
							<div class="row">
								<div class="col-md-12" align="center">
									<!-- 페이징 영역 -->
									<c:if test="${search.reqLogInqDetail.total_count > 0}">
										<div id="pagingframe" align="center">
											<p>
												<ctl:paginator currentPage="${search.reqLogInqDetail.page_num}"
													rowBlockCount="${search.reqLogInqDetail.size}"
													totalRowCount="${search.reqLogInqDetail.total_count}"
													pageName="reqLogInqDetail" />
											</p>
										</div>
									</c:if>
								</div>
							</div>
				
							<div class="row">
								<div class="col-md-12" align="right">
									<!-- 버튼 영역 -->
									<div class="option_btn right" style="padding-right: 10px;">
										<p class="right">
											<c:if test="${masterflag eq 'Y' }">
												<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="fnExtrtEmpDetailInfo('${reqLogInq.emp_user_name}', '${reqLogInq.emp_user_id }', '${reqLogInq.user_ip}')"><i class="fa fa-user"></i> 업무패턴보기</a>
											</c:if>
											<c:if test="${search.bbs_id eq null or empty search.bbs_id}">
												<a class="btn btn-sm grey-mint btn-outline sbold uppercase"
													onclick="javascript:goList();"><i class="fa fa-list"></i>
													목록</a>
											</c:if>
										</p>
									</div>
								</div>
							</div>
						</div>
						
					    
					</div>
				</div>
			</div>
		</div>
		<!-- END PROFILE CONTENT -->
	</div>
</div>

</div>
<!-- END CONTENT BODY -->

<script type="text/javascript">
	var reqLogInqConfig = {
		"listUrl":"${rootPath}/reqLogInq/list.html"
		,"detailUrl":"${rootPath}/reqLogInq/detail.html"
	};
	var extrtCondbyInqConfig = {
		"detailChartUrl":"${rootPath}/extrtCondbyInq/detailChart.html"
	};
	
</script>


