<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>

<c:set var="currentMenuId" value="${index_id }" />
<%-- <c:set var="currentMenuId" value="MENU00495" /> --%>
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/search/summonList.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>
<div id="loading"><img id="loading-image" width="30px;" src="${rootPath}/resources/image/loading3.gif" alt="Loading..." /></div>
<link rel="stylesheet" href="${rootPath}/resources/css/summonList.css" type="text/css" media="all">
<h1 class="page-title">
	${currentMenuName} 
</h1>
<div class="portlet light portlet-fit portlet-datatable bordered">
    <div class="portlet-body">
        <div class="table-container">
            
            <div id="datatable_ajax_2_wrapper" class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
	            
            <div class="dataTables_scroll" style="position: relative;">
	            <div class="row">
                   <div class="col-md-6" style="width:100%; margin-top:0px; padding-left:0px; padding-right:0px;">
                     <div class="portlet box grey-salt  ">
                           <div class="portlet-title" style="background-color: #2B3643;">
                               <div class="caption">
                                   <i class="fa fa-search"></i>검색</div>
                           </div>
                           <div class="portlet-body form" >
                               <div class="form-body" style="padding-left:10px; padding-right:10px;">
                                   <div class="form-group">
                                       <form id="listForm" method="POST" class="mt-repeater form-horizontal">
                                           <div data-repeater-list="group-a">
                                               <div class="row">
                                                   <!-- jQuery Repeater Container -->
                                                   <div class="col-md-2">
                                                       <label class="control-label">기간</label>
                                                       <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
															<input type="text" class="form-control" id="search_fr" name="search_from" value="${search.search_fromWithHyphen}"> 
																<span class="input-group-addon"> &sim; </span> 
															<input type="text" class="form-control" id="search_to" name="search_to" value="${search.search_toWithHyphen}">
														</div> 
													</div>
                                                   <%-- <div class="mt-repeater-input">
                                                       <label class="control-label">기간선택</label>
                                                       <br/>
                                                       <select class="form-control"
															id="daySelect" name="daySelect"
															onclick="javascript:initDaySelect();">
															<option value="" class="daySelect_first"
																<c:if test='${search.daySelect eq ""}'>selected="selected"</c:if>>기간선택</option>
															<option value="Day"
																<c:if test='${search.daySelect eq "Day"}'>selected="selected"</c:if>>오늘</option>
															<option value="WeekDay"
																<c:if test='${search.daySelect eq "WeekDay"}'>selected="selected"</c:if>>일주일</option>
														</select></div> --%>
													<div class="col-md-2">
                                                        <label class="control-label">시스템</label>
                                                        <div style="width: 100%; position: relative;">
	                                                        <div class="form-control " id="system_seq" name="dept_name" onclick="systemCheckDisplay()">전 체</div>
	                                                        <div class="form-control " id="system_check" style="position: absolute; width: inherit; display: none;" name="dept_name" value="${search.dept_name}" />
	                                                        	<label class="system_check" style="display: block; border-bottom: 1px solid lightgray"><input id="sysallcheck" type="checkbox" onchange="systemAllcheck()" checked="checked"> 전 체</label>
	                                                        	<c:forEach items="${systemMasterList}" var="system">
	                                                        		<label class="system_check" style="display: block;"><input class="syscheck" name="syscheck" type="checkbox" value="${system.system_seq }" onchange="systemAllcheckCount()" checked="checked"> <span>${system.system_name }</span></label>
	                                                        	</c:forEach>
	                                                        </div>
                                                        </div>
                                                    </div>
													<div class="col-md-2">
                                                        <label class="control-label">소속</label>
                                                        <input type="text" class="form-control" name="dept_name" value="${search.dept_name}" />
                                                    </div>
													<div class="col-md-2">
                                                       <label class="control-label">사용자ID</label>
                                                       <input type="text" class="form-control" name="emp_user_id" value="${search.emp_user_id}" />
                                                   </div>
                                                   <div class="col-md-2">
                                                       <label class="control-label">사용자명</label>
                                                       <input type="text" class="form-control" name="emp_user_name" value="${search.emp_user_name}" />
                                                   </div>
                                                   <div class="col-md-2">
                                                       <label class="control-label">시나리오</label>
                                                       <select id="scen_seq" name="scen_seq" class="form-control" onchange="selScenario()">
															<option value=""> ----- 선 택 ----- </option>
															<c:forEach	items="${scenarioList}" var="scenarioList">
																<option value="${scenarioList.scen_seq}" <c:if test="${scenarioList.scen_seq == search.scen_seq}">selected="selected"</c:if>>${scenarioList.scen_name}</option>
															</c:forEach>
															
														</select>
                                                   </div>
                                                   <div class="col-md-2">
                                                       <label class="control-label">상세시나리오</label>
                                                       <select id="rule_cd" name="rule_cd" class="form-control">
															<option value=""> ----- 선 택 ----- </option>
															<c:forEach	items="${ruleTblList}" var="ruleTblList">
																<option value="${ruleTblList.rule_seq}" <c:if test="${ruleTblList.rule_seq == search.rule_cd}">selected="selected"</c:if>>${ruleTblList.rule_nm}</option>
															</c:forEach>
														</select>
                                                   </div>
                                               </div>
                                           </div>
                                           <hr/>
                                           <div align="right">
                                           <!--  <button type="button"
												class="btn btn-sm blue btn-outline sbold uppercase"
												onclick="popupView()">
												<i class="fa fa-check"></i> <font>일괄처리
											</button> -->
                                            <button type="reset"
												class="btn btn-sm red-mint btn-outline sbold uppercase"
												onclick="resetOptions(summonConfig['listUrl'])">
												<i class="fa fa-remove"></i> <font>초기화
											</button>
											<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
												onclick="movesummonList()">
												<i class="fa fa-search"></i> 검색
											</button>&nbsp;&nbsp;
											<div class="btn-group">
												<a href="javascript:;" data-toggle="dropdown"><img src="${rootPath}/resources/image/icon/XLS_3.png"></a>
													<ul class="dropdown-menu pull-right">
														<!-- <li><a data-toggle="modal" data-target="#myModal">업로드</a></li> -->
														<li><a onclick="excelSummonList()"> 다운로드 </a></li>
													</ul>
											</div>
										</div>
										
										<input type="hidden" name="main_menu_id" value="${search.main_menu_id }"/>
										<input type="hidden" name="sub_menu_id" value="${search.sub_menu_id }"/>
										<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
										<input type="hidden" name="page_num" value="${search.page_num}" />
										<input type="hidden" name="cur_num" value="${search.page_num}"/>
										<input type="hidden" name="detailEmpCd" value=""/>
										<input type="hidden" name="detailOccrDt" value=""/>
										<input type="hidden" name="detailEmpDetailSeq" value=""/>
										<input type="hidden" name="emp_detail_seq" value=""/>
										<input type="hidden" name="sort_flag" id="sort_flag" value="${search.sort_flag}"/>
										<input type="hidden" id="rootPath" name="rootPath" value="${rootPath}" />
										<input type="hidden" name="system_seq" value="${search.system_seq}" />
										
										<div id = "chart_extract">
										</div>
                                       </form>
                                   </div>
                               </div>
                           </div>
                       </div>
                 </div>
             </div>     
             
             <div class="row">    
             	<table class="table table-striped table-bordered table-hover order-column">
                	<colgroup>
						<%-- <col width="3%" /> --%>
						<col width="10%" />
						<col width="5%" />
						<col width="5%" />
						<col width="20%" />
						<col width="10%" />
						<col width="10%" />
						<col width="10%" />
						<col width="10%" />
						<col width="10%"/>
						<col width="10%"/>
					</colgroup>
					<thead>
						<tr>
							<!-- <th scope="col" style="text-align: center;"><input type="checkbox" id="allCheck" onchange="allCheck(this)"></th> -->
							<th scope="col" style="text-align: center; vertical-align: middle;">일시</th>
							<th scope="col" style="text-align: center; vertical-align: middle;">시스템</th>
							<th scope="col" style="text-align: center; vertical-align: middle;">비정상위험 시나리오</th>
							<th scope="col" style="text-align: center; vertical-align: middle;">비정상위험 상세시나리오</th>
							<th scope="col" style="text-align: center; vertical-align: middle;">소속</th>
							<th scope="col" style="text-align: center; vertical-align: middle;">취급자ID</th>
							<th scope="col" style="text-align: center; vertical-align: middle;">취급자명</th>
							<c:choose>
								<c:when test="${ui_type == 'DGB'}">
									<th scope="col" style="text-align: center; vertical-align: middle;">과다처리건</th>
									<th scope="col" style="text-align: center; vertical-align: middle;">평균</th>
									<th scope="col" style="text-align: center; vertical-align: middle;">개인정보처리건</th>
								</c:when>
								<c:otherwise>
									<th scope="col" style="text-align: center; vertical-align: middle;">비정상처리횟수</th>
								</c:otherwise>
							</c:choose>
							
							
							
						</tr>
					</thead>
	                <tbody>
	                	<c:choose>
						<c:when test="${empty summonList.extrtCondbyInq}">
							<tr>
								<c:choose>
									<c:when test="${ui_type =='DGB' }">
										<td colspan="10" align="center">데이터가 없습니다.</td>
									</c:when>
									<c:otherwise>
										<td colspan="8" align="center">데이터가 없습니다.</td>
									</c:otherwise>
								</c:choose>
				        		
				        	</tr>
						</c:when>
						<c:otherwise>
							<c:set value="${summonList.page_total_count}" var="count"/>
							<c:forEach items="${summonList.extrtCondbyInq}" var="summon" varStatus="status">
								<tr style="cursor: pointer; <c:if test="${summon.is_check eq 'Y'}">color:#ada9a9;</c:if>" onclick="fnExtrtDetailLog('${summon.occr_dt}','${summon.emp_user_id}','${summon.emp_detail_seq}')">
									<%-- <td style="text-align: center;" onclick="event.cancelBubble=true"><input type="checkbox" class="checkList" name="checkList" value="${summon.emp_detail_seq}" onchange="allCheckCount()"></td> --%>
									<c:if test="${summon.occr_dt != null}">
										<c:set var="occr_dt" value="${summon.occr_dt}"/>
										<td align="center">${fn:substring(occr_dt, 0, 4)}-${fn:substring(occr_dt, 4, 6)}-${fn:substring(occr_dt, 6, fn:length(occr_dt))}&nbsp;
										</td>
									</c:if>
									<td align="center">${summon.system_name }</td>
<%-- 									<c:if test="${summon.occr_dt == null}"><td>-</td></c:if> --%>
<%-- 									<td><ctl:nullCv nullCheck="${summon.status.name}"/>	</td> --%>
									
									<td align="center">
										${summon.scen_name }
									</td>
									<c:choose>
										<c:when test="${fn:length(summon.rule_nm)>25}">
											<td title="${summon.rule_nm}" align="center">
												<c:out value="${fn:substring(summon.rule_nm,0,25)}">
												</c:out>..
											</td>
										</c:when>
										<c:otherwise>
											<td style ='cursor: pointer;' align="center">${summon.rule_nm }</td>	
										</c:otherwise>
									</c:choose>
									
									<c:choose>
										<c:when test="${summon.emp_user_id eq 'xempty' }">
											<td align="center">다수사용자</td>
											<td align="center">다수사용자
												<input type="hidden" name="empDetailList[${status.index}].emp_user_id" value="${summon.emp_user_id}" />
												<input type="hidden" name="empDetailList[${status.index}].emp_user_name" value="${summon.emp_user_name}" />
											</td>									
											<td>다수사용자</td>
										</c:when>
										<c:otherwise>
											<td align="center"><ctl:nullCv nullCheck="${summon.dept_name }"/></td>
											<td align="center"><ctl:nullCv nullCheck="${summon.emp_user_id }"/>
												<input type="hidden" name="empDetailList[${status.index}].emp_user_id" value="${summon.emp_user_id}" />
												<input type="hidden" name="empDetailList[${status.index}].emp_user_name" value="${summon.emp_user_name}" />
											</td>									
											<td><ctl:nullCv nullCheck="${summon.emp_user_name }"/></td>
										</c:otherwise>
									</c:choose>
									
									<c:choose>
										<c:when test="${ui_type == 'DGB'}">
											<td align="center">${summon.rule_cnt2}</td>
											<td align="center">${summon.avg_cnt}</td>
											<td align="center">${summon.rule_cnt}</td>
										</c:when>
										<c:otherwise>
											
											<td align="center">${summon.rule_cnt}</td>
										</c:otherwise>
									</c:choose>
									
										<%-- <input type="hidden" name="empDetailList[${status.index}].rule_cnt" value="${summon.rule_cnt}" /> --%>
								</tr>
								<c:set var="count" value="${count - 1 }"/>
							</c:forEach>
						</c:otherwise>
						</c:choose>
	                </tbody>
	            </table>
            </div>
             <div class="dataTables_processing DTS_Loading" style="display: none;">Please wait ...</div>
            </div>
	             <div class="row" style="padding: 10px;">
		            <!-- 페이징 영역 -->
					<c:if test="${search.total_count > 0}">
						<div id="pagingframe" align="center">
							<p>
								<ctl:paginator currentPage="${search.page_num}"
									rowBlockCount="${search.size}"
									totalRowCount="${search.total_count}" />
							</p>
						</div>
					</c:if>
		        </div>
            </div>
            
        </div>
    </div>   
</div>
<div class="popup_wrap" id="descriptPopup" style="display: none">
	<div class="popup">
        <div class="popup_header" style="background: #2B3643">
            <h1 class="align_l">일괄 소명처리</h1>
            <a class="btn_close align_r" onclick="popupClose()" title="창닫기"></a>
        </div> <!-- e:popup_header -->
        <div class="popup_contents">
            <p class="popup_comment">선택된 소명건을 일괄 처리 합니다.</p>
            <div class="table-box-wrap">
                <div class="table-box">
                   	<textarea id="description" rows="12" cols="65" style="resize: none;border:none;">소명요청 : </textarea>
                </div> <!-- e:table-box -->
            </div> <!-- e:table-box-wrap -->
            <div class="popup_btn align_r">
				<select id="desc_status">
					<option value="20">소명요청</option>
					<option value="90">취소처리</option>
				</select>
                <button class="btn btn_m btn_gray" type="button" onclick="popupClose()">취소</button>
                <button class="btn btn_m" type="button" onclick="batchSummon()" style="background: #2B3643; color:white">완료</button>
            </div> 
        </div>  <!-- e:popup_contents -->
    </div>  <!-- e:popup -->
</div> <!-- e:popup_wrap -->
<script type="text/javascript">

	var summonConfig = {
		"listUrl":"${rootPath}/extrtCondbyInq/summonList.html"
		,"detailUrl":"${rootPath}/extrtCondbyInq/summonDetail.html"
		,"downloadUrl" :"${rootPath}/extrtCondbyInq/summonListExcel.html"
		,"menu_id":"${currentMenuId}"
		,"menu_name":"${currentMenuName}"
		,"loginPage" : "${rootPath}/loginView.html"
		,"detailChartUrl" : "${rootPath}/summon/detailChart.html"
		
	};
	
	summonManageConfig = {
		"listUrl":"${rootPath}/extrtCondbyInq/summonList.html"
	}

	var menu_id = summonConfig["menu_id"];
	var menu_name = summonConfig["menu_name"];
	var log_message = "SUCCESS";
	var log_action = "SELECT";
	
</script>