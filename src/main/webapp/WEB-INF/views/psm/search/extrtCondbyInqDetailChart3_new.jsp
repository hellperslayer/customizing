<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin Theme #1 for dark mega menu option"
	name="description" />
<meta content="" name="author" />
<style>
#loadingT {
	width: 92%;
	height: 62%;
	bottom: 7%;
	right: 4%;
	position: absolute;
	display: block;
	opacity: 0.7;
	background-color: #fff;
	z-index: 99;
	text-align: center;
}

#loading-image {
	position: absolute;
	top: 50%;
	left: 50%;
	z-index: 100;
}
</style>
<div class="tab-pane <c:if test="${tabFlag == 'tab3' }">active</c:if>" id="tab3">
	<div class="row">
		<div class="mt-element-step" style="margin:20px;">
			<div class="row step-line">				
			</div>
			<div class="portlet-body">
				<div class="portlet box grey-salt">
					<div class="portlet-title" style="background-color: #2B3643;">
						<div class="caption">
							<img src="${rootPath}/resources/image/icon/search.png"> 검색
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse"></a>
						</div>
					</div>
					<!-- portlet-body S -->
					<div class="portlet-body form" style="display: block">
						<div class="form-body"
							style="padding-left: 10px; padding-right: 10px;">
							<div class="form-group">
								<form id="timeListForm" method="POST" class="mt-repeater form-horizontal">
									<div data-repeater-list="group-a">
										<div data-repeater-item class="mt-repeater-item">
											<div class="mt-repeater-input">
												<label class="control-label">일시</label> <br />
												<div class="input-group input-medium date-picker input-daterange" data-date-format="yyyy-mm-dd">
													<input type="text" class="form-control" id="search_fromT" name="search_from" value="${search.search_fromWithHyphen}"> <span class="input-group-addon"> &sim; </span>
														<input type="text" class="form-control" id="search_toT" name="search_to" value="${search.search_toWithHyphen}">
												</div>
											</div>
											<div class="mt-repeater-input">
												<label class="control-label">시나리오</label> <br />
												<select id="rule_cd" name="rule_cd" class="ticket-assign form-control input-xlarge">
													<option value="" selected="selected">전체</option>
													<c:forEach items="${rule_list}" var="rule" varStatus="status">
														<option value="${rule.rule_cd}">${rule.rule_nm}</option>
													</c:forEach>
												</select>
											</div>
											<div class="mt-repeater-input">
												<label class="control-label">시스템</label>
												<select id="system_seq" name="system_seq" class="ticket-assign form-control input-small">
													<option value="" selected="selected">전체</option>
													<c:forEach items="${SystemMasterList}" var="SystemMaster" varStatus="status">
														<option value="${SystemMaster.system_seq}">${SystemMaster.system_name}</option>
													</c:forEach>
												</select>
											</div>
										</div>
										<br>
										<div align="right">
											<button type="reset" 
												class="btn btn-sm red-mint btn-outline sbold uppercase"
												onclick="resetTimeline()">
												<i class="fa fa-remove"></i> 취소
											</button>
											<button type="button" 
												class="btn btn-sm blue btn-outline sbold uppercase"
												onclick="findTimeline1()">
												<i class="fa fa-search"></i> 검색
											</button>
											&nbsp;&nbsp;
										</div>
									</div>
									<input type="hidden" id="emp_user_id" name="emp_user_id" value="${userInfo.emp_user_id}" />
								</form>
							</div>
						</div>
					</div>
					<!-- portlet-boyd E -->
				</div>
				<div class="m-grid m-grid-responsive-md m-grid-demo">
					<div class="m-grid-row" style="height:412px">
						<div class="m-grid-col m-grid-col-middle m-grid-col-center" style="padding: 10px; height: 400px;">
							<div class="portlet light bordered" style="margin-bottom:2px;">
								<div class="poltlet-body">
										<div id="timeline_frame" style="height: 640px; overflow-y: auto;" class="mt-timeline-2">
										
										</div>
									<%-- <div id="loadingT">
										<img id="loading-image" width="30px;"
											src="${rootPath}/resources/image/loading3.gif" alt="Loading..." />
									</div> --%>
								</div>
							</div>
						</div>				
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form id="allLogInqForm" method="post">
	<input type="hidden" name="detailLogSeq" />
	<input type="hidden" name="detailProcDate" />
</form>