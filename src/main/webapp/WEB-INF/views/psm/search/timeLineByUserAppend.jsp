<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html>
<ul class="mt-container" style="padding: 0px 20px 0px 0px;" id="timelineUl">
											<c:forEach var="al" items="${allLogInq}" varStatus="st">
											<c:set var="classAdd" value="${st.count%2==1 ? 'border-left-before-green-turquoise' : 'border-right-before-green-turquoise'}"/>
												<c:set var="classAddSenario" value="${st.count%2==1 ? 'border-left-before-red' : 'border-right-before-red'}"/>
													<li class="mt-item">
														<div class="mt-timeline-icon" style="width: 50px; height: 50px; text-align: center; background: rgb(231,80,90);">
															<img src="${rootPath}/resources/image/psm/search/normal.png" style="width: 26px; height: 35px; margin: 7.5px;">
														</div>
														<div class="mt-timeline-content">
															<div class="mt-content-container border-red ${classAddSenario}"
																style="padding: 15px 30px 5px;">
																<div class="mt-title">
																	<h3 class="mt-content-title">비정상위험</h3>
																</div>
																<div class="mt-author" style="margin: 10px 0px 0px;">
																	<div class="mt-avatar">
																		<img class="img-circle" src="${rootPath}/resources/assets/layouts/layout/img/profile_user.png">
																	</div>
																	<div class="mt-author-name">
																		<a class="font-blue-madison" style="cursor: default;">${al.emp_user_name }(${al.emp_user_id })</a>
																	</div>
																	<div class="mt-author-notes font-grey-mint">
																		<fmt:parseDate value="${al.proc_date}" pattern="yyyyMMdd" var="proc_date" /> 
																		<fmt:formatDate value="${proc_date}" pattern="yyyy-MM-dd" /> 
																	</div>
																</div>
																<div class="mt-content border-red-turquoise" style="padding: 10px 0px 0px;">
																	<p>
																		<b> 접속한 시스템명 : </b>${al.system_name}
																		<br> <b>상세 시나리오명 : </b>${al.result_type} 
																	</p>
																</div>
															</div>
														</div>
													</li>
											</c:forEach>
										</ul>
										<div class="mt-timeline-line border-grey-steel" style="height:${timeLineLength}px;" id="lineBar"></div>