<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/search/extrtCondbyInqList.js" type="text/javascript" charset="UTF-8"></script>


<h1 class="page-title">
    ${currentMenuName} 
</h1>

<form id="listForm" method="POST">
<div class="portlet light portlet-fit portlet-datatable bordered">

    <div class="portlet-body">
        <div class="table-container">
             <div class="caption" style="padding-bottom:5px;">
                <i class="icon-settings font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase">추출조건정보</span>
            </div>
            <div style="padding-bottom:20px;">
                <table class="table table-striped table-bordered order-column">
                    <tbody>
                    	<tr>
                            <th width="10%" style="text-align: center; vertical-align: middle;">발생년월일</th>
                            <td width="40%" style="text-align: center; vertical-align: middle;">${extrtCondbyInq.occr_dt}</td>
                            <th width="10%" style="text-align: center; vertical-align: middle;">시나리오명</th>
                            <td width="40%" style="text-align: center; vertical-align: middle;">${extrtCondbyInq.scen_name}</td>
                        </tr>
                        <tr>
                            <th style="text-align: center; vertical-align: middle;">상세시나리오명</th>
                            <td colspan="3" style="text-align: left; vertical-align: middle;">${extrtCondbyInq.rule_nm}</td>
                        </tr>
                        <tr>
                            <th style="text-align: center; vertical-align: middle;">취급자</th>
                            <td style="text-align: center; vertical-align: middle;">${extrtCondbyInq.emp_user_name}(${extrtCondbyInq.emp_user_id})</td>
                            <th style="text-align: center; vertical-align: middle;">소속</th>
                            <td style="text-align: center; vertical-align: middle;">${extrtCondbyInq.dept_name}(${extrtCondbyInq.dept_id})</td>
                        </tr>
                        <tr>
                            <th style="text-align: center; vertical-align: middle;">발생시스템</th>
                            <td style="text-align: center; vertical-align: middle;"><ctl:nullCv nullCheck="${extrtCondbyInq.system_name}"/></td>
                            <th style="text-align: center; vertical-align: middle;">임계치</th>
                            <td style="text-align: center; vertical-align: middle;">${extrtCondbyInq.limit_cnt}</td>
                        </tr>
                        <tr>
                            <th style="text-align: center; vertical-align: middle;">로그구분</th>
                            <td style="text-align: center; vertical-align: middle;">
                            	<c:choose>
                            		<c:when test="${extrtCondbyInq.log_delimiter == 'BA'}">접속기록조회</c:when>
                            		<c:when test="${extrtCondbyInq.log_delimiter == 'DB'}">DB접근조회</c:when>
                            		<c:when test="${extrtCondbyInq.log_delimiter == 'DN'}">다운로드로그</c:when>
                            	</c:choose>
                            </td>
                            <th style="text-align: center; vertical-align: middle;">처리기준</th>
                            <td style="text-align: center; vertical-align: middle;">
                            	<c:choose>
                            		<c:when test="${extrtCondbyInq.rule_view_type == 'M'}">처리량</c:when>
                            		<c:when test="${extrtCondbyInq.rule_view_type == 'R'}">이용량</c:when>
                            	</c:choose>
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: center; vertical-align: middle;">위험도</th>
                            <td style="text-align: center; vertical-align: middle;">${extrtCondbyInq.dng_val}</td>
                            <th style="text-align: center; vertical-align: middle;">비정상걸린횟수</th>
                            <td style="text-align: center; vertical-align: middle;">${extrtCondbyInq.rule_cnt}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
         
            
            <div class="caption" style="padding-bottom:5px;">
                <i class="icon-settings font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase">로그정보</span>
             </div>
            <div>
                <table class="table table-striped table-bordered table-hover order-column">
                    <thead>
                        <tr>
                        	<th width="10%" style="text-align: center; vertical-align: middle;">번호</th>
                        	<th width="10%" style="text-align: center; vertical-align: middle;">일시</th>
                        	<c:if test="${extrtCondbyInq.log_delimiter == 'DN'}">
                        		<th width="10%" style="text-align: center; vertical-align: middle;">파일명</th>
                        	</c:if>
                        	<c:if test="${extrtCondbyInq.rule_view_type == 'R'}">
                        		<th width="10%" style="text-align: center; vertical-align: middle;">개인정보유형</th>
                        	</c:if>
                        	<th width="10%" style="text-align: center; vertical-align: middle;">시간</th>
                        	<th width="10%" style="text-align: center; vertical-align: middle;">IP</th>
                        	<c:if test="${extrtCondbyInq.rule_result_type == 'N'}">
                        		<th width="10%" style="text-align: center; vertical-align: middle;">취급자ID</th>
                        	</c:if>
                        	<th width="10%" style="text-align: center; vertical-align: middle;">상세정보</th>
                        </tr>
                    </thead>
                    <tbody>
                    <c:set value="${(search.page_num-1) * search.size + 1}" var="no"/>
                    <c:forEach items="${detailList }" var="dl" varStatus="st">
                        <tr>
                        	<td style="text-align: center; vertical-align: middle;">${no}</td>
                        	<td style="text-align: center; vertical-align: middle;">
                        		<fmt:parseDate value="${dl.occr_dt }" pattern="yyyyMMdd" var="d"/>
                        		<fmt:formatDate value="${d }" pattern="yyyy-MM-dd" var="dateVal"/>
                        		<fmt:parseDate value="${dl.proc_time }" pattern="HHmmss" var="t"/>
                        		<fmt:formatDate value="${t }" pattern="HH:mm:ss" var="timeVal"/>
                        		${dateVal } ${timeVal }
                        	</td>
                        	<c:if test="${extrtCondbyInq.log_delimiter == 'DN'}">
                        		<td style="text-align: center; vertical-align: middle;">${dl.file_name}</td>
                        	</c:if>
                        	<c:if test="${extrtCondbyInq.rule_view_type == 'R'}">
                        	<td style="text-align: center; vertical-align: middle;">${dl.result_content }</td></c:if>
                        	<td style="text-align: center; vertical-align: middle;">${timeVal }</td>
                        	<td style="text-align: center; vertical-align: middle;">${dl.ip }</td>
                        	<c:if test="${extrtCondbyInq.rule_result_type == 'N'}">
                        	<td style="text-align: center; vertical-align: middle;">${dl.emp_user_id }</td></c:if>
                        	<td style="text-align: center; vertical-align: middle;">
                        		<button type="button" style="padding-top: 2px;padding-bottom: 2px; padding-right: 8px;padding-left: 8px;" class="btn blue mt-ladda-btn ladda-button btn-outline btn-circle"
                                onclick="detailResultLogView('${dl.log_seq}','${dl.occr_dt}')">
                                    <span class="ladda-label" style="font-size: 12px;">상세보기</span>
                                <span class="ladda-spinner"></span></button>
                        	</td>
                        </tr>
                        <c:set value="${no+1}" var="no"/>
                    </c:forEach>
                    </tbody>
                </table>
                
                <!-- 메뉴 관련 input 시작 -->
                <input type="hidden" name="main_menu_id" value="${search.main_menu_id }" />
                <input type="hidden" name="sub_menu_id" value="${search.sub_menu_id }" />
                <input type="hidden" name="current_menu_id" value="${currentMenuId }" />
                <!-- 메뉴 관련 input 끝 -->
                
                <!-- 페이지 번호 -->
                <input type="hidden" name="page_num" value="${search.page_num }" />
                
                <!-- 검색조건 관련 input 시작 -->
                <input type="hidden" name="search_from" value="${search.search_from }" />
                <input type="hidden" name="search_to" value="${search.search_to }" />
                <input type="hidden" name="emp_detail_seq" value="${search.emp_detail_seq }" />
                <input type="hidden" name="emp_user_id" value="${search.emp_user_id }" />
                <input type="hidden" name="emp_user_name" value="${search.emp_user_name}" />
                <input type="hidden" name="dept_name" value="${search.dept_name}" />
                <input type="hidden" name="rule_cd" value="${search.rule_cd }" />
                <input type="hidden" name="daySelect" value="${search.daySelect }" />
                <!-- 검색조건 관련 input 끝 -->
                
                <input type="hidden" name="log_delimiter" value="${extrtCondbyInq.log_delimiter }" />
                
                <!-- 페이징 영역 -->
                <c:if test="${search.total_count > 0}">
                    <div id="pagingframe" align="center">
                        <p><ctl:paginator currentPage="${search.page_num}" rowBlockCount="${search.size}" totalRowCount="${search.total_count}"/></p>
                    </div>
                </c:if>
            </div>
            
        </div>
    </div>
</div>
</form>
<form id="moveForm" method="post">
<input type="hidden" name="detailProcDate" value=""/>
<input type="hidden" name="detailLogSeq" value=""/>
</form>
<script type="text/javascript">

    var extrtCondbyInqConfig = {
        "listUrl":"${rootPath}/extrtCondbyInq/detail.html"
        ,"goExtrtCondbyInqListUrl":"${rootPath}/extrtCondbyInq/list.html"
        ,"goEmpDetailInqListUrl":"${rootPath}/empDetailInq/detail.html"
        ,"goCallingDemandListUrl":"${rootPath}/callingDemand/list.html"
        ,"detailUrl":"${rootPath}/extrtCondbyInq/detail.html"
        ,"detailChartUrl" :"${rootPath}/extrtCondbyInq/detailChart.html"
        ,"saveFollowUpUrl" :"${rootPath}/extrtCondbyInq/saveFollowUp.html" 
        ,"loginPage" : "${rootPath}/loginView.html"
    };
        
    var allLogInqDetailConfig = {
        "detailUrl":"${rootPath}/allLogInq/detail.html"
        ,"downloadDetailUrl":"${rootPath}/downloadLogInq/detail.html"
    };
    
</script>