<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>

<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<%@include file="../../common/include/config.jsp"%>

<div style="background-color:#ffffff;height: 100%;width:10000px;">
<!-- <div class="row" style="padding-top: 10px; padding-bottom: 20px;overflow-x:auto; overflow-y:auto;width: 100%; height: 100%;"> -->
	<table id="user" class="table table-striped table-bordered table-hover dataTable no-footer" border="1" style="margin-top:0 !important;margin-bottom:0 !important;">
		 <tbody>
			 <tr>
			 	<th style="text-align:center;min-width:40px">발생일자</th>
				<th style="text-align:center;min-width:40px">발생시간</th>
				<th style="text-align:center;min-width:70px">개인정보취급자코드</th>
				<th style="text-align:center;min-width:70px">개인정보취급자명</th>
				<th style="text-align:center;min-width:40px">사용자ID</th>
				<th style="text-align:center;min-width:40px">사용자IP주소</th>
				<th style="text-align:center;min-width:40px">소속부서코드</th>
				<th style="text-align:center;min-width:70px">소속부서명</th>
				<th style="text-align:center;min-width:40px">소속지사코드</th>
				<th style="text-align:center;min-width:70px">소속지사명</th>
				<th style="text-align:center;min-width:40px">소속지역본부코드</th>
				<th style="text-align:center;min-width:70px">소속지역본부명</th>
				<th style="text-align:center;min-width:40px">소속구분</th>
				<th style="text-align:center;min-width:40px">접근지사코드</th>
				<th style="text-align:center;min-width:70px">접근지사명</th>
				<th style="text-align:center;min-width:40px">접근지역본부코드</th>
				<th style="text-align:center;min-width:70px">접근지역본부명</th>
				<th style="text-align:center;min-width:40px">지사코드</th>
				<th style="text-align:center;min-width:70px">지사명</th>
				<th style="text-align:center;min-width:40px">부서코드</th>
				<th style="text-align:center;min-width:70px">부서명</th>
				<th style="text-align:center;min-width:40px">소속기관코드</th>
				<th style="text-align:center;min-width:70px">소속기관명</th>
				<th style="text-align:center;min-width:40px">조회자차량코드</th>
				<th style="text-align:center;min-width:40px">조회자장소코드</th>
				<th style="text-align:center;min-width:40px">조회기관구분</th>
				<th style="text-align:center;min-width:40px">정보주체식별키</th>
				<th style="text-align:center;min-width:70px">정보주체명</th>
				<th style="text-align:center;min-width:40px">조회자료건수</th>
				<th style="text-align:center;min-width:40px">조회조건</th>
				<th style="text-align:center;min-width:40px">조회순번</th>
				<th style="text-align:center;min-width:40px">증번호</th>
				<th style="text-align:center;min-width:40px">24종업무로그</th>
				<th style="text-align:center;min-width:70px">개인정보조회사유코드</th>
				<th style="text-align:center;min-width:40px">사업장기호</th>
				<th style="text-align:center;min-width:70px">사업장명</th>
				<th style="text-align:center;min-width:40px">피보험자식별키</th>
				<th style="text-align:center;min-width:70px">피보험자명</th>
				<th style="text-align:center;min-width:40px">대상자연계조건</th>
				<th style="text-align:center;min-width:40px">개인정보조회항목Column명</th>
				<th style="text-align:center;min-width:40px">개인정보내용</th>
				<th style="text-align:center;min-width:100px">조회사유</th>
				<th style="text-align:center;min-width:40px">요양기관코드</th>
				<th style="text-align:center;min-width:70px">요양기관명</th>
				<th style="text-align:center;min-width:40px">접수번호</th>
				<th style="text-align:center;min-width:40px">접수년도</th>
				<th style="text-align:center;min-width:70px">출력화면명</th>
				<th style="text-align:center;min-width:40px">업무코드</th>
				<th style="text-align:center;min-width:70px">업무명</th>
				<th style="text-align:center;min-width:70px">업무상세내용</th>
				<th style="text-align:center;min-width:40px">조회DB</th>
				<th style="text-align:center;min-width:40px">환자코드</th>
				<th style="text-align:center;min-width:40px">환자명</th>
				<th style="text-align:center;min-width:40px">대상혈액번호</th>
				<th style="text-align:center;min-width:50px">대상자적격여부</th>
				<th style="text-align:center;min-width:50px">버튼처리구분</th>
				<th style="text-align:center;min-width:40px">화면ID</th>
				<th style="text-align:center;min-width:70px">화면명</th>
				<th style="text-align:center;min-width:40px">프로그램ID</th>
				<th style="text-align:center;min-width:70px">프로그램명</th>
				<th style="text-align:center;min-width:70px">요청URL</th>
				<th style="text-align:center;min-width:40px">조회프로그램구분</th>
				<th style="text-align:center;min-width:40px">가입자소속코드</th>
				<th style="text-align:center;min-width:70px">가입자소속명</th>
				<th style="text-align:center;min-width:40px">가입자지역본부코드</th>
				<th style="text-align:center;min-width:70px">가입자지역본부명</th>
				<th style="text-align:center;min-width:40px">관할지밖여부</th>
				<th style="text-align:center;min-width:40px">사업장소속코드</th>
				<th style="text-align:center;min-width:70px">사업장소속명</th>
				<th style="text-align:center;min-width:40px">사업장지역본부코드</th>
				<th style="text-align:center;min-width:70px">사업장지역본부명</th>
				<th style="text-align:center;min-width:40px">피보험자소속코드</th>
				<th style="text-align:center;min-width:70px">피보험자소속명</th>
				<th style="text-align:center;min-width:40px">피보험자지역본부코드</th>
				<th style="text-align:center;min-width:70px">피보험자지역본부명</th>
				<th style="text-align:center;min-width:40px">감사번호</th>
				<th style="text-align:center;min-width:40px">권한등급</th>
				<th style="text-align:center;min-width:40px">현재상태</th>
				<th style="text-align:center;min-width:40px">일련번호</th>
				<th style="text-align:center;min-width:40px">시스템코드</th>
				<th style="text-align:center;min-width:40px">기관코드</th>
				<th style="text-align:center;min-width:40px">발생일시</th>
			 </tr>
			 <c:choose>
			 	<c:when test="${fn:length(bizLog80) eq 0}">
			 		<tr>
			 			<td colspan="84"></td>
			 		</tr>
			 	</c:when>
			 	<c:otherwise>
			 		<c:forEach items="${bizLog80 }" var="bizLog">
					 	<tr>
							<td style="white-space:pre">${bizLog.proc_date}</td>
							<td style="white-space:pre">${bizLog.proc_time}</td>
							<td style="white-space:pre">${bizLog.emp_cd}</td>
							<td style="white-space:pre">${bizLog.emp_nm}</td>
							<td style="white-space:pre">${bizLog.user_id}</td>
							<td style="white-space:pre">${bizLog.user_ip}</td>
							<td style="white-space:pre">${bizLog.dept_cd}</td>
							<td style="white-space:pre">${bizLog.dept_nm}</td>
							<td style="white-space:pre">${bizLog.org_cd}</td>
							<td style="white-space:pre">${bizLog.org_nm}</td>
							<td style="white-space:pre">${bizLog.hq_cd}</td>
							<td style="white-space:pre">${bizLog.hq_nm}</td>
							<td style="white-space:pre">${bizLog.posit_gu}</td>
							<td style="white-space:pre">${bizLog.acc_org_cd}</td>
							<td style="white-space:pre">${bizLog.acc_org_nm}</td>
							<td style="white-space:pre">${bizLog.acc_hq_cd}</td>
							<td style="white-space:pre">${bizLog.acc_hq_nm}</td>
							<td style="white-space:pre">${bizLog.bran_cd}</td>
							<td style="white-space:pre">${bizLog.bran_nm}</td>
							<td style="white-space:pre">${bizLog.nh_dept_cd}</td>
							<td style="white-space:pre">${bizLog.nh_dept_nm}</td>
							<td style="white-space:pre">${bizLog.cd_org_cd}</td>
							<td style="white-space:pre">${bizLog.cd_org_nm}</td>
							<td style="white-space:pre">${bizLog.car_cd}</td>
							<td style="white-space:pre">${bizLog.site_cd}</td>
							<td style="white-space:pre">${bizLog.member_div}</td>
							<td style="white-space:pre">${bizLog.join_ssn}</td>
							<td style="white-space:pre">${bizLog.ssn_name}</td>
							<td style="white-space:pre">${bizLog.sear_cont}</td>
							<td style="white-space:pre">${bizLog.sear_val}</td>
							<td style="white-space:pre">${bizLog.inq_seq}</td>
							<td style="white-space:pre">${bizLog.cert_num}</td>
							<td style="white-space:pre">${bizLog.sear_log24}</td>
							<td style="white-space:pre">${bizLog.per_inf_cd}</td>
							<td style="white-space:pre">${bizLog.esta_sym}</td>
							<td style="white-space:pre">${bizLog.firm_nm}</td>
							<td style="white-space:pre">${bizLog.assu_ssn}</td>
							<td style="white-space:pre">${bizLog.assu_nm}</td>
							<td style="white-space:pre">${bizLog.contact_condition}</td>
							<td style="white-space:pre">${bizLog.psnl_inf_qry_hed_colnm}</td>
							<td style="white-space:pre">${bizLog.psnl_inf_cnts}</td>
							<td style="white-space:pre">${bizLog.inq_reason}</td>
							<td style="white-space:pre">${bizLog.ykiho_cd}</td>
							<td style="white-space:pre">${bizLog.ykiho_nm}</td>
							<td style="white-space:pre">${bizLog.recv_no}</td>
							<td style="white-space:pre">${bizLog.recv_yyyy}</td>
							<td style="white-space:pre">${bizLog.output_pgm_id}</td>
							<td style="white-space:pre">${bizLog.busi_cd}</td>
							<td style="white-space:pre">${bizLog.busi_nm}</td>
							<td style="white-space:pre">${bizLog.busi_dtl_contn}</td>
							<td style="white-space:pre">${bizLog.inq_db}</td>
							<td style="white-space:pre">${bizLog.patient_cd}</td>
							<td style="white-space:pre">${bizLog.patient_nm}</td>
							<td style="white-space:pre">${bizLog.bloodno}</td>
							<td style="white-space:pre">${bizLog.result}</td>
							<td style="white-space:pre">${bizLog.button_cd}</td>
							<td style="white-space:pre">${bizLog.scrn_id}</td>
							<td style="white-space:pre">${bizLog.scrn_nm}</td>
							<td style="white-space:pre">${bizLog.prg_id}</td>
							<td style="white-space:pre">${bizLog.prg_nm}</td>
							<td style="white-space:pre">${bizLog.req_url}</td>
							<td style="white-space:pre">${bizLog.vcls}</td>
							<td style="white-space:pre">${bizLog.ssn_org_cd}</td>
							<td style="white-space:pre">${bizLog.ssn_org_nm}</td>
							<td style="white-space:pre">${bizLog.ssn_hq_cd}</td>
							<td style="white-space:pre">${bizLog.ssn_hq_nm}</td>
							<td style="white-space:pre">${bizLog.juri_out}</td>
							<td style="white-space:pre">${bizLog.firm_cd}</td>
							<td style="white-space:pre">${bizLog.firm_cd_nm}</td>
							<td style="white-space:pre">${bizLog.firm_hq_cd}</td>
							<td style="white-space:pre">${bizLog.firm_hq_nm}</td>
							<td style="white-space:pre">${bizLog.assu_org_cd}</td>
							<td style="white-space:pre">${bizLog.assu_org_nm}</td>
							<td style="white-space:pre">${bizLog.assu_hq_cd}</td>
							<td style="white-space:pre">${bizLog.assu_hq_nm}</td>
							<td style="white-space:pre">${bizLog.audit_no}</td>
							<td style="white-space:pre">${bizLog.grade}</td>
							<td style="white-space:pre">${bizLog.status}</td>
							<td style="white-space:pre">${bizLog.log_seq}</td>
							<td style="white-space:pre">${bizLog.sys_cd}</td>
							<td style="white-space:pre">${bizLog.log_agency_cd}</td>
							<td style="white-space:pre">${bizLog.full_proc_date}</td>
						 </tr>
					 </c:forEach>
			 	</c:otherwise>
			 </c:choose>
		 </tbody>
	 </table>
	<!-- END PROFILE CONTENT -->
<!-- </div> -->
</div>