<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>

<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/search/abuseInfoList.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>


<h1 class="page-title">
	${currentMenuName} 
</h1>
<div class="portlet light portlet-fit portlet-datatable bordered">
    
    <div class="portlet-body">
        <div class="table-container">
            
            <div id="datatable_ajax_2_wrapper" class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
	            
            <div class="dataTables_scroll" style="position: relative;">
	            <div class="row">
                   <div class="col-md-6" style="width:100%; margin-top:10px; padding-left:0px; padding-right:0px;">
                      
                     <div class="portlet box grey-salt  ">
                           <div class="portlet-title" style="background-color: #2B3643;">
                               <div class="caption">
                                   <i class="fa fa-search"></i>검색 & 엑셀</div>
                               <div class="tools">
				                 	<a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
			             	   </div>
                           </div>
                           <div class="portlet-body form" id="searchBar">
                               <div class="form-body" style="padding-left:10px; padding-right:10px;">
                                   <div class="form-group">
                                       <form id="listForm" method="POST" class="mt-repeater form-horizontal">
                                           <div data-repeater-list="group-a">
                                               <div class="row">
                                                   <!-- jQuery Repeater Container -->
                                                   <div class="col-md-2">
                                                       <label class="control-label">기간</label>
                                                       <div class="input-group date-picker input-daterange"
															data-date="10/11/2012" data-date-format="yyyy-mm-dd">
															<input type="text" class="form-control" id="search_fr"
																name="search_from" value="${search.search_fromWithHyphen}"> 
																<span class="input-group-addon"> &sim; </span> 
															<input type="text" class="form-control" id="search_to" 
																name="search_to" value="${search.search_toWithHyphen}">
														</div> 
													</div>
<%--                                                    <div class="mt-repeater-input">
                                                       <label class="control-label">기간선택</label>
                                                       <br/>
                                                       <select class="form-control"
															id="daySelect" name="daySelect"
															onclick="javascript:initDaySelect();">
															<option value="" class="daySelect_first"
																<c:if test='${search.daySelect eq ""}'>selected="selected"</c:if>>기간선택</option>
															<option value="Day"
																<c:if test='${search.daySelect eq "Day"}'>selected="selected"</c:if>>오늘</option>
															<option value="WeekDay"
																<c:if test='${search.daySelect eq "WeekDay"}'>selected="selected"</c:if>>일주일</option>
															<option value="MonthDay"
																<c:if test='${search.daySelect  eq "MonthDay"}'>selected="selected"</c:if>>전달
																1일부터 ~ 오늘</option>
															<option value="YearDay"
																<c:if test='${search.daySelect  eq "YearDay"}'>selected="selected"</c:if>>올해
																1월1일부터 ~ 오늘</option>
															<option value="TotalMonthDay"
																<c:if test='${search.daySelect eq "TotalMonthDay"}'>selected="selected"</c:if>>전달
																전체</option>
														</select></div> --%>
													<c:if test="${ use_systemSeq eq 'Y' }">
														<div class="col-md-2">
	                                                        <label class="control-label">시스템</label>
	                                                        <select name="system_seq" class="form-control">
																<option value=""
																	${search.system_seq == '' ? 'selected="selected"' : ''}>
																	----- 전 체 -----</option>
																<c:if test="${empty systemMasterList}">
																	<option>시스템 없음</option>
																</c:if>
																<c:if test="${!empty systemMasterList}">
																	<c:forEach items="${systemMasterList}" var="i"
																		varStatus="z">
																		<option value="${i.system_seq}"
																			${i.system_seq==search.system_seq ? "selected=selected" : "" }>${ i.system_name}</option>
																	</c:forEach>
																</c:if>
															</select>
														</div>
													</c:if>	
													<%-- <div class="mt-repeater-input">
                                                       <label class="control-label">사용자ID</label>
                                                       <br/>
                                                       <input type="text" class="form-control" name="emp_user_id" value="${search.emp_user_id}" />
                                                       <input type="hidden" name="user_ip" value=""/>
                                                   </div>
                                                   <div class="mt-repeater-input">
                                                       <label class="control-label">사용자명</label>
                                                       <br/>
                                                       <input type="text" class="form-control" name="emp_user_name" value="${search.emp_user_name}" />
                                                   </div> --%>
                                                   <div class="col-md-2">
                                                        <label class="control-label">소속</label>
                                                        <input type="text" class="form-control" name="dept_name" value="${search.dept_name}" />
                                                        <input type="hidden" name="dept_id" value=""/>
                                                    </div>
  <%--                                                  <div class="mt-repeater-input">
                                                       <label class="control-label">시나리오</label>
                                                       <br/>
                                                       <select id="scen_seq" name="scen_seq" class="form-control" onchange="selScenario()">
															<option value=""> ----- 선 택 ----- </option>
															<c:forEach	items="${scenarioList}" var="scenarioList">
																<option value="${scenarioList.scen_seq}" <c:if test="${scenarioList.scen_seq == search.scen_seq}">selected="selected"</c:if>>${scenarioList.scen_name}</option>
															</c:forEach>
															
														</select>
                                                   </div> --%>
                                                   <div class="col-md-2">
                                                       <label class="control-label">상세시나리오</label>
                                                       <select id="rule_cd" name="rule_cd" class="form-control">
															<option value=""> ----- 선 택 ----- </option>
															<c:forEach	items="${ruleTblList}" var="ruleTblList">
																<option value="${ruleTblList.rule_seq}" <c:if test="${ruleTblList.rule_seq == search.rule_cd}">selected="selected"</c:if>>${ruleTblList.rule_nm}</option>
															</c:forEach>
															<c:forEach items="${scenarioList }" var="scenarioList">
																<option value="${scenarioList.scen_seq}" <c:if test="${scenarioList.scen_seq == search.scen_seq}">selected="selected"</c:if>>${scenarioList.scen_name}</option>
															</c:forEach>
														</select>
                                                   </div> 
                                               </div>
                                           </div>
                                           <hr/>
                                           <div align="right">
                                            <button type="reset"
												class="btn btn-sm red-mint btn-outline sbold uppercase"
												onclick="resetOptions(abuseInfoConfig['listUrl'])">
												<i class="fa fa-remove"></i> <font>초기화
											</button>
											<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
												onclick="moveAbuseInfoList()">
												<i class="fa fa-search"></i> 검색
											</button>&nbsp;&nbsp;
											<%-- <a class="btn red btn-outline btn-circle"
												onclick="excelAllLogInqList('${search.total_count}')"> 
												<i class="fa fa-share"></i> <span class="hidden-xs"> 엑셀 </span>
											</a> --%>
											<a onclick="excelAbuseInfoList('${search.total_count}')"><img src="${rootPath}/resources/image/icon/XLS_3.png"></a>
										</div>
										
										<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }"/>
										<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }"/>
										<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
										<input type="hidden" name="page_num" value="${search.page_num}" />
										<input type="hidden" name="detailEmpCd" value=""/>
										<input type="hidden" name="detailOccrDt" value=""/>
										<input type="hidden" name="detailEmpDetailSeq" value=""/>
										<input type="hidden" name="isSearch" value="${paramBean.isSearch }"/>
										<input type="hidden" name="desc_seq" value=""/>
										
                                       </form>
                                   </div>
                               </div>
                           </div>
                       </div>
                 </div>
             </div>     
             
             <div class="">    
             	<table class="table table-striped table-bordered table-hover">
                	<colgroup>
						<col width="10%" />
						<col width="10%" />
						<col width="10%" />
						<col width="10%" />
						<col width="10%" />
						<col width="10%" />
						<col width="10%" />
						<col width="10%" />
						<col width="10%" />
						<col width="10%" />
					</colgroup>
					<thead>
						<tr>
							<th scope="col" style="text-align: center;">발생일</th>
							<th scope="col" style="text-align: center;">상세시나리오</th>
							<th scope="col" style="text-align: center;">소명요청일</th>
							<th scope="col" style="text-align: center;">소속</th>
							<th scope="col" style="text-align: center;">소명요청자</th>
							<th scope="col" style="text-align: center;">소명답변자</th>
							<th scope="col" style="text-align: center;">소명판정자</th>
							<th scope="col" style="text-align: center;">오남용목적</th>
							<th scope="col" style="text-align: center;">오남용유형</th>
							<th scope="col" style="text-align: center;">과실정도</th>
						</tr>
					</thead>
	                <tbody>
	                	<c:choose>
						<c:when test="${empty abuseList.extrtCondbyInq}">
							<tr>
				        		<td colspan="10" align="center">데이터가 없습니다.</td>
				        	</tr>
						</c:when>
						<c:otherwise>
							<c:set value="${abuseList.page_total_count}" var="count"/>
							<c:forEach items="${abuseList.extrtCondbyInq}" var="abuseInfo" varStatus="status">
								<tr style ='cursor: pointer;' onclick="fnAbuseInfo('${abuseInfo.desc_seq}')">
									<c:set var="occr_dt" value="${abuseInfo.occr_dt}"/>
									<td style="text-align: center;">${fn:substring(occr_dt, 0, 4)}-${fn:substring(occr_dt, 4, 6)}-${fn:substring(occr_dt, 6, fn:length(occr_dt))}&nbsp;</td>
									<td>${abuseInfo.rule_nm }</td>
									<c:set var="request_dt" value="${abuseInfo.request_dt}"/>
									<td style="text-align: center;">${fn:substring(request_dt, 0, 4)}-${fn:substring(request_dt, 4, 6)}-${fn:substring(request_dt, 6, fn:length(request_dt))}&nbsp;</td>
									<td>${abuseInfo.dept_name }</td>
									<td>${abuseInfo.request_user_name }</td>
									<td>${abuseInfo.request_target_user_name }</td>
									<td>${abuseInfo.result_user_name }</td>
									<td>${abuseInfo.abuse_purpose }</td>
									<td>${abuseInfo.abuse_type }</td>
									<td>${abuseInfo.fault_degree }</td>
								<c:set var="count" value="${count - 1 }"/>
							</c:forEach>
						</c:otherwise>
						</c:choose>
	                </tbody>
	            </table>
            </div>
             </div>
	             <div class="row" style="padding: 10px;">
		            <!-- 페이징 영역 -->
					<c:if test="${search.total_count > 0}">
						<div id="pagingframe" align="center">
							<p>
								<ctl:paginator currentPage="${search.page_num}"
									rowBlockCount="${search.size}"
									totalRowCount="${search.total_count}" />
							</p>
						</div>
					</c:if>
		        </div>
            </div>
            
        </div>
    </div>   
</div>

<%-- <form id="eventForm" method="POST" action="${rootPath}/extrtCondbyInq/list.html">
	<input type="hidden" name="search_from"/>
	<input type="hidden" name="search_to"/>
	<input type="hidden" name="dept_name"/>
	<input type="hidden" name="emp_user_id"/>
</form> --%>

<script type="text/javascript">

	var abuseInfoConfig = {
		"listUrl":"${rootPath}/extrtCondbyInq/abuseInfoList.html"
		,"detailUrl":"${rootPath}/extrtCondbyInq/abuseInfoDetail.html"
		,"downloadUrl" :"${rootPath}/extrtCondbyInq/abuseInfoDownload.html"
		,"menu_id":"${currentMenuId}"
		,"loginPage" : "${rootPath}/loginView.html"
		
	};

	var menu_id = abuseInfoConfig["menu_id"];
	var log_message = "SUCCESS";
	var log_action = "SELECT";
	
</script>