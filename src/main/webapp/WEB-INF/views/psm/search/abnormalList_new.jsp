<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />
<c:set var="now" value="<%=new java.util.Date()%>" />
<c:set var="Year"><fmt:formatDate value="${now}" pattern="yyyy" /></c:set>
<fmt:parseDate var="dateString" value="${install_date.code_name}" pattern="yyyy-MM-dd" />
<c:set var="installYear"><fmt:formatDate value="${dateString}" pattern="yyyy" /></c:set>
<c:set var="mulYear" value="${Year-installYear }"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->

<style>
.amcharts-axis-title {
	cursor: pointer;
}
</style>
<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/search/abnormal.js" type="text/javascript"></script>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>
<div class="row">
	<div class="col-md-12">
		<div id="searchBar" class="portlet-body form">
			<div class="form-body" style="padding-left: 10px; padding-right: 10px;">
				<div class="form-group">
					<form id="searchForm" method="POST">
						<div class="row" align="center">
						<ul style="list-style: none;text-align: center;padding-left: 0px;display: inline-flex;">
							<li><a onclick="searchData('today', 'N')" id="todayA" style="font-size:18px; margin:0 20px; letter-spacing:-.5px;font-weight: bold;
								<c:if test="${search.searchType!='today'}">color:#a8b0b6;</c:if>">전일</a></li>
							<li><a onclick="searchData('day', 'N')" id="dayA" style="font-size:18px; margin:0 20px; letter-spacing:-.5px;font-weight: bold;
								<c:if test="${search.searchType!='day'}">color:#a8b0b6;</c:if>">일간</a></li>
							<li><a onclick="searchData('month', 'N')" id="monthA" style="font-size:18px; margin:0 20px; letter-spacing:-.5px;font-weight: bold;
								<c:if test="${search.searchType!='month'}">color:#a8b0b6;</c:if>">월간</a></li>
						</ul></div>
						<div id="day" style="
							<c:choose>
								<c:when test="${search.searchType=='day'}">display: flex;</c:when>
								<c:otherwise>display:none;</c:otherwise>
							</c:choose>
							width: 800px; margin: 0 auto;">
									<input type="text" class="form-control date-picker" id="search_fr" name="search_from" value="${search.search_fromWithHyphen }" data-date-format="yyyy-mm-dd"/><span>~</span>
									<input type="text" class="form-control date-picker" id="search_to" name="search_to" value="${search.search_toWithHyphen }" data-date-format="yyyy-mm-dd"/>
								<select class="form-control" id="day_system_seq" style="margin-left: 20px">
									<option value="">시스템 전체</option>
									<c:forEach var="sysCode" items="${systemList}">
										<option value="${sysCode.system_seq}"
											<c:if test='${sysCode.system_seq eq search.system_seq}'>selected</c:if>>${sysCode.system_name}</option>
									</c:forEach>
								</select>
							<button class="btn" onclick="searchData('day', 'Y')" style="margin-left: 20px">검색</button>
						</div>
						<div id="month" style="
							<c:choose>
								<c:when test="${search.searchType=='month'}">display: flex;</c:when>
								<c:otherwise>display:none;</c:otherwise>
							</c:choose>
							width: 800px; margin: 0 auto;">
								<select class="form-control" id="year" name="year">
									<c:forEach var="mul" begin="0" end="${mulYear }" step="1">
                        				<option value="${Year-mul }" <c:if test='${search.year eq Year-mul}'>selected</c:if>>${Year-mul }년</option>
                        			</c:forEach>
								</select>
								<select class="form-control" id="month" style="margin-left: 20px" name="month">
									<c:forEach var="i" begin="1" end="12" step="1">
										<option value="${i<10 ? '0':'' }${i }" <c:if test='${search.month eq i}'>selected</c:if>>${i }월</option>
									</c:forEach>
								</select>
								<select class="form-control" id="month_system_seq" style="margin-left: 20px">
									<option value="">시스템 전체</option>
									<c:forEach var="sysCode" items="${systemList}">
										<option value="${sysCode.system_seq}" <c:if test='${sysCode.system_seq eq search.system_seq}'>selected</c:if>>${sysCode.system_name}</option>
									</c:forEach>
								</select>
							<button class="btn" onclick="searchData('month', 'Y')" style="margin-left: 20px">검색</button>

						</div>
						<input type="hidden" name="searchType" value="${search.searchType }">
						<input type="hidden" name="system_seq">
						<input type="hidden" name="isSearch">
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- 그래프 영역 -->
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="portlet light ">
			<form id="countForm" method="POST">
                          <input type="hidden" name="search_to" value=""/>
                          <input type="hidden" name="search_from" value=""/>
                          <input type="hidden" name="isMonth" value=""/>
                          <input type="hidden" name="scen_seq" value=""/>
                          <input type="hidden" name="main_menu_id" />
            </form>
			
			<div class="row" style="margin-left:200px;">
				<c:choose>
					<c:when test="${search.searchType=='today' }">
						<c:forEach items="${topPanal}" var="tp">
						<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
							<a class="dashboard-stat dashboard-stat-v2 ${tp.panalType}" style="cursor: default;">
								<div class="visual">
									<i class="fa fa-comments"></i>
								</div>
								<div class="details">
									<div class="number" style="font-size: 16px;">
										<span data-counter="counterup" data-value="" style="font-size: 30px; text-decoration: underline; cursor: pointer;" onclick="goExtrtCondbyList1('1', ${tp.scen_seq});">
											${tp.day_cnt}</span> (전일)
										&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp; <span data-counter="counterup" data-value="" style="font-size: 30px; text-decoration: underline; cursor: pointer;"
											onclick="goExtrtCondbyList1('2', ${tp.scen_seq});">
											${tp.month_cnt}</span> (당월)
									</div>
									<div class="desc">${tp.scen_name }</div>
								</div>
							</a>
						</div>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<c:forEach items="${topPanal}" var="tp">
						<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
							<a class="dashboard-stat dashboard-stat-v2 ${tp.panalType}" style="cursor: default;">
								<div class="visual">
									<i class="fa fa-comments"></i>
								</div>
								<div class="details">
									<div class="number" style="font-size: 16px;">
										<span data-counter="counterup" data-value="" style="font-size: 30px; text-decoration: underline; cursor: pointer;" onclick="goExtrtCondbyList1('', ${tp.scen_seq});">
											${tp.day_cnt}</span> 
									</div>
									<div class="desc">${tp.scen_name }</div>
								</div>
							</a>
						</div>
						</c:forEach>
					</c:otherwise>
				</c:choose>
                      
             </div>
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
		<div class="portlet light " style="margin-bottom: 0px;">
			<div class="portlet-title">
				<div class="caption ">
					<i class="icon-settings font-dark"></i> <span
						class="caption-subject font-dark bold uppercase">오남용 의심행위(월별)</span>
				</div>
			</div>
			<div class="portlet-body">
				<div id="dashboard_amchart_2" class="chart"></div>
			</div>
		</div>
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-settings font-dark"></i> 
					<span class="caption-subject bold uppercase font-dark">부서별 오남용의심 TOP5</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="table-container">
						<div id="datatable_ajax_2_wrapper" class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
							<div class="dataTables_scroll" style="position: relative;">
                            	<div id="donutchart_0" style="width: 150px; height: 150px; float: left; display: none; cursor: pointer;" ></div>
                            	<div id="donutchart_1" style="width: 150px; height: 150px; float: left; display: none; cursor: pointer;" ></div>
                            	<div id="donutchart_2" style="width: 150px; height: 150px; float: left; display: none; cursor: pointer;" ></div>
                            	<div id="donutchart_3" style="width: 150px; height: 150px; float: left; display: none; cursor: pointer;" ></div>
                            	<div id="donutchart_4" style="width: 150px; height: 150px; float: left; display: none; cursor: pointer;" ></div>
                            	<div id="donutchart_temp" style="width: 150px; height: 150px; display: none;">데이터가 없습니다.</div>
							</div>
							<div class="dataTables_scroll" style="position: relative;">
								<div id="donutText_0" class="dashboard-stat dashboard-stat-v2" style="width: 150px; height: 30px; float: left; display: none;" ></div>
                            	<div id="donutText_1" class="dashboard-stat dashboard-stat-v2" style="width: 150px; height: 30px; float: left; display: none;"></div>
                            	<div id="donutText_2" class="dashboard-stat dashboard-stat-v2" style="width: 150px; height: 30px; float: left; display: none;" ></div>
                            	<div id="donutText_3" class="dashboard-stat dashboard-stat-v2" style="width: 150px; height: 30px; float: left; display: none;"></div>
                            	<div id="donutText_4" class="dashboard-stat dashboard-stat-v2" style="width: 150px; height: 30px; float: left; display: none;"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			<div class="portlet light" style="margin-bottom: 0px;">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-settings font-dark"></i> <span
							class="caption-subject bold uppercase font-dark">오남용 의심행위(일별)
						</span>
					</div>
				</div>
				<div class="portlet-body">
					<div id="dashboard_amchart_1" class="chart"></div>
				</div>
			</div>
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-settings font-dark"></i>
					<span class="caption-subject bold uppercase font-dark">개인별 오남용의심 TOP5</span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="table-container">
						<div id="datatable_ajax_2_wrapper" class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
							<div class="dataTables_scroll" style="position: relative;">
								<div id="donutchartP_0" style="width: 150px; height: 150px; float: left; display: none; cursor: pointer;" ></div>
                            	<div id="donutchartP_1" style="width: 150px; height: 150px; float: left; display: none; cursor: pointer;" ></div>
                            	<div id="donutchartP_2" style="width: 150px; height: 150px; float: left; display: none; cursor: pointer;" ></div>
                            	<div id="donutchartP_3" style="width: 150px; height: 150px; float: left; display: none; cursor: pointer;" ></div>
                            	<div id="donutchartP_4" style="width: 150px; height: 150px; float: left; display: none; cursor: pointer;" ></div>
                            	<div id="donutchartP_temp" style="width: 150px; height: 150px; display: none;">데이터가 없습니다.</div>
							</div>
							<div class="dataTables_scroll" style="position: relative;">
								<div id="donutPText_0" class="dashboard-stat dashboard-stat-v2" style="width: 150px; height: 30px; float: left; display: none;" ></div>
                            	<div id="donutPText_1" class="dashboard-stat dashboard-stat-v2" style="width: 150px; height: 30px; float: left; display: none;"></div>
                            	<div id="donutPText_2" class="dashboard-stat dashboard-stat-v2" style="width: 150px; height: 30px; float: left; display: none;" ></div>
                            	<div id="donutPText_3" class="dashboard-stat dashboard-stat-v2" style="width: 150px; height: 30px; float: left; display: none;"></div>
                            	<div id="donutPText_4" class="dashboard-stat dashboard-stat-v2" style="width: 150px; height: 30px; float: left; display: none;"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	
<form id="detailForm" method="POST">
<input type="hidden" name="detailOccrDt">
<input type="hidden" name="userId">
<input type="hidden" name="dept_id">
</form>
<script type="text/javascript">
	var extrtCondbyInqConfig = {
		"listUrl" : "${rootPath}/extrtCondbyInq/list.html",
		"detailUrl" : "${rootPath}/extrtCondbyInq/detail.html",
		"downloadUrl" : "${rootPath}/extrtCondbyInq/download.html",
		"menu_id" : "${currentMenuId}",
		"loginPage" : "${rootPath}/loginView.html",
		"detailChartUrl" : "${rootPath}/extrtCondbyInq/detailChart.html",
		"deptDetailChartUrl" : "${rootPath}/extrtCondbyInq/deptDetailChart.html",
		"dashboard" : "${rootPath}/extrtCondbyInq/abnormalList.html"
	};

	var menu_id = extrtCondbyInqConfig["menu_id"];
	var log_message = "SUCCESS";
	var log_action = "SELECT";
</script>