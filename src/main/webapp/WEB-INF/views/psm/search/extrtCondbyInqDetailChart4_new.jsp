<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="Preview page of Metronic Admin Theme #1 for dark mega menu option" name="description" />
<meta content="" name="author" />

<div class="tab-pane <c:if test="${tabFlag == 'tab4' }">active</c:if>" id="tab4">	  
	<div class="row margin-top-20" style="padding-top:140px;">
		<div class="m-grid m-grid-responsive-md m-grid-demo">
			<div class="m-grid-row" style="height:412px">
				<div class="m-grid-col m-grid-col-middle m-grid-col-center" style="padding: 10px; height: 400px;">
					<div class="portlet light bordered" style="margin-bottom:2px;">
						<div class="portlet-title">
							<div class="caption">
								<span class="caption-subject bold uppercase font-dark">
								소명 처리 현황
								</span>
							</div>
						</div>
						<div class="poltlet-body">
							<div id="extrt_amchart_16" class="chart"></div>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>