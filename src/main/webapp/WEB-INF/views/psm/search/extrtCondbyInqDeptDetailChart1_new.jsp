<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="Preview page of Metronic Admin Theme #1 for dark mega menu option" name="description" />
<meta content="" name="author" />

<div class="tab-pane <c:if test="${tabFlag == 'tab1' }">active</c:if>" id="tab1">	  
	<div class="row">
		<div class="mt-element-step">
			<div class="row step-line">
<%-- 			
				<div class="col-md-2 mt-step-col last error">
					<div class="mt-step-number bg-white">
						<c:if test="${data1.dng_grade == '심각' }">5</c:if>
						<c:if test="${data1.dng_grade == '경계' }">4</c:if>
						<c:if test="${data1.dng_grade == '주의' }">3</c:if>
						<c:if test="${data1.dng_grade == '관심' }">2</c:if>
						<c:if test="${data1.dng_grade == '정상' }">1</c:if>
					</div>
					<div class="mt-step-title uppercase font-grey-cascade">${data1.dng_grade}</div>
				</div>
 --%>				
				<div class="margin-top-40" >
					<div class="col-md-12">
						<div class="col-md-2" align="center">
						<c:if test="${data1.dng_grade == '심각' }"><img alt="위험단계1" src="${rootPath}/resources/image/psm/search/risk1.png"></c:if>
						<c:if test="${data1.dng_grade == '경계' }"><img alt="위험단계2" src="${rootPath}/resources/image/psm/search/risk2.png"></c:if>
						<c:if test="${data1.dng_grade == '주의' }"><img alt="위험단계3" src="${rootPath}/resources/image/psm/search/risk3.png"></c:if>
						<c:if test="${data1.dng_grade == '관심' }"><img alt="위험단계4" src="${rootPath}/resources/image/psm/search/risk4.png"></c:if>
						<c:if test="${data1.dng_grade == '정상' }"><img alt="위험단계5" src="${rootPath}/resources/image/psm/search/risk5.png"></c:if>
						</div>
						<div class="col-md-10">
							<div style=" margin-top: 5px; margin-bottom: 0px;">
								<span style="font-size:19px;"> ${data1.dept_name} 는 오남용 위험 요인 
								<c:if test="${data1.dng_grade == '심각' }"><b><font style="color:#d91e18">${data1.dng_grade} 단계의</font></b></c:if>
								<c:if test="${data1.dng_grade == '경계' }"><b><font style="color:#e87e04">${data1.dng_grade} 단계의</font></b></c:if>
								<c:if test="${data1.dng_grade == '주의' }"><b><font style="color:#f7ca18">${data1.dng_grade} 단계의</font></b></c:if>
								<c:if test="${data1.dng_grade == '관심' }"><b><font style="color:#3598dc">${data1.dng_grade} 단계의</font></b></c:if>
								<c:if test="${data1.dng_grade == '정상' }"><b><font style="color:#26c281">${data1.dng_grade} 단계의</font></b></c:if>
								 위험현황 입니다.</span>
								<span style="font-size:19px; "><br> 
								<c:if test="${data1.dng_grade == '심각' }"><b><font style="color:#d91e18">오남용 위험요인</font></b></c:if>
								<c:if test="${data1.dng_grade == '경계' }"><b><font style="color:#e87e04">오남용 위험요인</font></b></c:if>
								<c:if test="${data1.dng_grade == '주의' }"><b><font style="color:#f7ca18">오남용 위험요인</font></b></c:if>
								<c:if test="${data1.dng_grade == '관심' }"><b><font style="color:#3598dc">오남용 위험요인</font></b></c:if>
								<c:if test="${data1.dng_grade == '정상' }"><b><font style="color:#26c281">오남용 위험요인</font></b></c:if>을 확인하세요.</span>
							</div>
						</div>
					</div>
<!-- 					<div style=" margin-top: 5px; margin-bottom: 5px;">
					</div> -->
				</div>
			</div>                                            
		</div>                         
	</div>
	<div class="portlet light">
		<div class="poltlet-body">
			<div class="row margin-top-20" style="padding-top:20px;">
				<div class="m-grid m-grid-responsive-md m-grid-demo">
					<div style="text-align: left;"><span class="caption-subject font-blue-madison uppercase">
						<%-- <c:set var="occr_dt" value="${search.detailOccrDt}"/>
						${fn:substring(occr_dt, 0, 4)}-${fn:substring(occr_dt, 4, 6)}-${fn:substring(occr_dt, 6, fn:length(occr_dt))} 기준 --%>
						<c:choose>
							<c:when test="${empty search.detailOccrDt }">
								<c:set var="date1" value="${search.search_from}"/>
								<c:set var="date2" value="${search.search_to}"/>
								${fn:substring(date1, 0, 4)}.${fn:substring(date1, 4, 6)}.${fn:substring(date1, 6, fn:length(date1))} ~
								${fn:substring(date2, 0, 4)}.${fn:substring(date2, 4, 6)}.${fn:substring(date2, 6, fn:length(date2))}
							</c:when>
							<c:otherwise>
								<c:set var="occr_dt" value="${search.detailOccrDt}"/>
								${fn:substring(occr_dt, 0, 4)}.${fn:substring(occr_dt, 4, 6)}.${fn:substring(occr_dt, 6, fn:length(occr_dt))} 기준
							</c:otherwise>
						</c:choose>
					</span></div>
					<div class="m-grid-row" style="height:412px">
						<div class="m-grid-col m-grid-col-middle m-grid-col-center" style="padding: 10px; height: 400px;">
							<div class="portlet light bordered" style="margin-bottom:2px;">
								<div class="portlet-title">
									<div class="caption">
										<i class="icon-bar-chart font-green-haze"></i>
										<span class="caption-subject bold uppercase font-red-haze">개인정보과다처리</span>
									</div>
								</div>
								<div class="poltlet-body">
									<div id="extrt_amchart_5" class="chart"></div>
								</div>
							</div>
						</div>	
						<div class="m-grid-col m-grid-col-middle m-grid-col-center" style="padding: 10px; height: 380px;">
							<div class="portlet light bordered" style="margin-bottom:2px;">
								<div class="portlet-title">
									<div class="caption">
										<i class="icon-bar-chart font-green-haze"></i>
											<span class="caption-subject bold uppercase font-red-haze">비정상접근</span>
									</div>
								</div>
								<div class="poltlet-body">
									<div id="extrt_amchart_3" class="chart"></div>
								</div>
							</div>
						</div>	
						<!-- <div class="m-grid-col m-grid-col-middle m-grid-col-center" style="padding: 10px; height: 400px;">
							<div class="portlet light bordered" style="margin-bottom:2px;">
								<div class="portlet-title">
									<div class="caption">
										<i class="icon-bar-chart font-green-haze"></i>
										<span class="caption-subject bold uppercase font-red-haze">비정상접근</span>
									</div>
								</div>
								<div class="poltlet-body">
									<div id="extrt_amchart_1" class="chart"></div>
								</div>
							</div>
						</div> -->
						<div class="m-grid-col m-grid-col-middle m-grid-col-center" style="padding: 10px; height: 400px;">
							<div class="portlet light bordered" style="margin-bottom:2px;">
								<div class="portlet-title">
									<div class="caption">
										<i class="icon-bar-chart font-green-haze"></i>
										<span class="caption-subject bold uppercase font-red-haze">특정인 처리</span>
									</div>
								</div>
								<div class="poltlet-body">
									<div id="extrt_amchart_2" class="chart"></div>
								</div>
							</div>
						</div>
		
						<div class="m-grid-col m-grid-col-middle m-grid-col-center" style="padding: 10px; height: 400px;">
							<div class="portlet light bordered" style="margin-bottom:2px;">
								<div class="portlet-title">
									<div class="caption">
										<i class="icon-bar-chart font-green-haze"></i>
										<span class="caption-subject bold uppercase font-red-haze">특정시간대 처리</span>
									</div>
								</div>
								<div class="poltlet-body">
									<div id="extrt_amchart_4" class="chart"></div>
								</div>
							</div>
						</div>
						
						<div class="m-grid-col m-grid-col-middle m-grid-col-center" style="padding: 10px; height: 400px;">
							<div class="portlet light bordered" style="margin-bottom:2px;">
								<div class="portlet-title">
									<div class="caption">
										<i class="icon-bar-chart font-green-haze"></i>
										<span class="caption-subject bold uppercase font-red-haze">다운로드 의심행위</span>
									</div>
								</div>
								<div class="poltlet-body">
									<div id="extrt_amchart_D6" class="chart"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>