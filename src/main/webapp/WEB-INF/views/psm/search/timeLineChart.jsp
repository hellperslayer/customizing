<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>

<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />
  
<script src="${rootPath}/resources/js/psm/dashboard/highcharts.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/exporting.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/globalize.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/knockout-3.0.0.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/dx.chartjs.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/zoomingData.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/dashboard/vivagraph.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/search/timeLineChart.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>
<style>
#loading {
 width: 100%;   
 height: 100%;   
 top: 0px;
 left: 0px;
 position: fixed;   
 display: block;   
 opacity: 0.7;   
 background-color: #fff;   
 z-index: 99;   
 text-align: center; }  
  
#loading-image {   
 position: absolute;   
 top: 50%;   
 left: 50%;  
 z-index: 100; } 
</style>

<script type="text/javascript">
$(document).ready(function() {
	$('#loading').show();
});

$(window).load(function() {   
	$('#loading').hide();
});
</script>

<div id="loading"><img id="loading-image" width="30px;" src="${rootPath}/resources/image/loading3.gif" alt="Loading..." /></div>


<h1 class="page-title">
	${currentMenuName}
</h1>
<div class="portlet light portlet-fit portlet-datatable bordered">
	<div class="portlet-body">
		<div class="table-container">

			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
				<div>
					<div class="col-md-12" style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
						<div class="portlet box grey-salt  ">
                            <div class="portlet-title" style="background-color: #2B3643;">
                                <div class="caption">
                                    <i class="fa fa-search"></i>검색 & 엑셀 </div>
	                            <div class="tools">
	                            	<a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
					            </div>
                            </div>
                            <div id="searchBar" class="portlet-body form">
                                <div class="form-body" style="padding-left:10px; padding-right:10px;">
                                    <div class="form-group">
                                        <form id="listForm" method="POST" class="mt-repeater form-horizontal">
                                            <div data-repeater-list="group-a">
                                            	<div data-repeater-item class="mt-repeater-item" style="border-bottom: 0px; padding-bottom: 0px; margin-bottom: 0px;">
                                                    <div class="mt-repeater-input">
														<label class="control-label">기간</label> <br />
                                                        <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
															<input type="text" class="form-control" id="search_fr" name="search_from" value="${search.search_fromWithHyphen}"> 
															<span class="input-group-addon"> &sim; </span> 
															<input type="text" class="form-control" id="search_to" name="search_to" value="${search.search_toWithHyphen}">
														</div> 
													</div>
													<div class="mt-repeater-input">
                                                        <label class="control-label">대상</label>
                                                        <br/>
                                                        <select id="data1" name="data1" class="form-control" onchange="dataChange()" value="${search.data1 }">
															<option value="" ${search.data1 == '' ? 'selected="selected"' : ''}>----- 전 체 -----</option>
															<option value="connect" ${search.data1 == 'connect' ? 'selected="selected"' : ''}>접속기록</option>
															<option value="download" ${search.data1 == 'download' ? 'selected="selected"' : ''}>다운로드접속기록</option>
															<option value="ref" ${search.data1 == 'ref' ? 'selected="selected"' : ''}>접근이력</option>
															<option value="senario" ${search.data1 == 'senario' ? 'selected="selected"' : ''}>비정상위험</option>
														</select>
													</div>
													<div class="mt-repeater-input">
                                                        <label class="control-label">시스템</label>
                                                        <br/>
                                                        <select name="system_seq" class="form-control">
															<option value="" ${search.system_seq == '' ? 'selected="selected"' : ''}>----- 전 체 -----</option>
															<c:if test="${empty systemMasterList}"><option>시스템 없음</option></c:if>
															<c:if test="${!empty systemMasterList}">
																<c:forEach items="${systemMasterList}" var="i" varStatus="z">
																	<option value="${i.system_seq}" ${i.system_seq==search.system_seq ? "selected=selected" : "" }>${ i.system_name}</option>
																</c:forEach>
															</c:if>
														</select>
													</div>
												</div>
                                                <div data-repeater-item class="mt-repeater-item">
													<div class="mt-repeater-input">
                                                        <label class="control-label">취급자ID</label>
                                                        <br/>
                                                        <input type="text" class="form-control" id="emp_user_id" name="emp_user_id" value="${search.emp_user_id}" />
                                                    </div>
                                                   <div class="mt-repeater-input">
                                                        <label class="control-label">취급자명</label>
                                                        <br/>
                                                        <input type="text" class="form-control" id="user_name" name="emp_user_name" value="${search.emp_user_name}" />
                                                    </div>
                                                    <div class="mt-repeater-input" id="senario1" style="display: none;">
														<label class="control-label">시나리오</label> <br />
														<select id="scen_seq" name="scen_seq" class="form-control" onchange="selScenario()">
                                                            <option value=""> ----- 전 체 ----- </option>
                                                            <c:forEach  items="${scenarioList}" var="scenarioList">
                                                                <option value="${scenarioList.scen_seq}" <c:if test="${scenarioList.scen_seq == search.scen_seq}">selected="selected"</c:if>>
                                                                	${scenarioList.scen_name}
                                                                </option>
                                                            </c:forEach>
                                                        </select>
													</div>
													<div class="mt-repeater-input" id="senario2" style="display: none;">
														<label class="control-label">상세시나리오</label> <br />
														<select id="rule_cd" name="rule_cd" class="ticket-assign form-control">
															<option value="" selected="selected"> ----- 전 체 ----- </option>
															<c:forEach items="${rule_list}" var="rule" varStatus="status">
																<option value="${rule.rule_cd}" <c:if test="${rule.rule_cd == search.rule_cd}">selected="selected"</c:if>>
																	${rule.rule_nm}
																</option>
															</c:forEach>
														</select>
													</div>
												</div>
												</div>
											<div align="right">
												<button type="button" class="btn btn-sm red-mint btn-outline sbold uppercase" onclick="timeLineReset()">
													<i class="fa fa-remove"></i> 초기화
												</button>
												<button type="button" class="btn btn-sm blue btn-outline sbold uppercase" onclick="timeLineSearch()">
													<i class="fa fa-search"></i> 검색
												</button>
											</div>
											
											<input type="hidden" id="total_count" name="total_count" value="${search.total_count}" />
											<input type="hidden" id="size" name="size" value="${search.size}" />
											<input type="hidden" id="page_num" name="page_num" value="${search.page_num}" />
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>
				</div>
				<c:if test="${empty allLogInq }">
					<div class="m-grid m-grid-responsive-md m-grid-demo">
						<div class="m-grid-row" style="height:80px">
							<div class="m-grid-col m-grid-col-middle m-grid-col-center" style="padding: 10px; height: 70px;">
								<div class="portlet light bordered" style="margin-bottom:2px;">
									<div class="poltlet-body">
										<div id="timeline_frame" style="height: 20px;" class="mt-timeline-2">
										데이터가 없습니다.
										</div>
									</div>
								</div>
							</div>				
						</div>
					</div>
				</c:if>
				<c:if test="${!empty allLogInq }">
					<div class="m-grid m-grid-responsive-md m-grid-demo">
						<div class="m-grid-row" style="height:412px">
							<div class="m-grid-col m-grid-col-middle m-grid-col-center" style="padding: 10px; height: 400px;">
								<div class="portlet light bordered" style="margin-bottom:2px;">
									<div class="poltlet-body">
										<div id="timeline_frame" style="height: 640px; overflow-y: auto;" class="mt-timeline-2">
											<ul class="mt-container" style="padding: 0px 20px 0px 0px;" id="timelineUl">
												<c:forEach var="al" items="${allLogInq}" varStatus="st">
												<c:set var="classAdd" value="${st.count%2==1 ? 'border-left-before-green-turquoise' : 'border-right-before-green-turquoise'}"/>
												<c:choose>
													<c:when test="${al.desc_div == 'connect' }"> <!-- 접속기록 -->
														<li class="mt-item">
															<div class="mt-timeline-icon" style="width: 50px; height: 50px; text-align: center; background: rgb(38, 194, 129);">
																<img src="${rootPath}/resources/image/psm/search/normal.png" style="width: 26px; height: 35px; margin: 7.5px;">
															</div>
															<div class="mt-timeline-content">
																<div class="mt-content-container border-green-turquoise ${classAdd}"
																	style="padding: 15px 30px 5px;">
																	<div class="mt-title">
																		<h3 class="mt-content-title">접속기록</h3>
																	</div>
																	<div class="mt-author" style="margin: 10px 0px 0px;">
																		<div class="mt-avatar">
																			<img class="img-circle" src="${rootPath}/resources/assets/layouts/layout/img/profile_user.png">
																		</div>
																		<div class="mt-author-name">
																			<a class="font-blue-madison" style="cursor: default;">${al.emp_user_name }(${al.emp_user_id })</a>
																		</div>
																		<div class="mt-author-notes font-grey-mint">
																			<fmt:parseDate value="${al.proc_date}" pattern="yyyyMMdd" var="proc_date" /> 
																			<fmt:formatDate value="${proc_date}" pattern="yyyy-MM-dd" /> 
																			<fmt:parseDate value="${al.proc_time}" pattern="HHmmss" var="proc_time" /> 
																			<fmt:formatDate value="${proc_time}" pattern="HH:mm:ss" />
																		</div>
																	</div>
																	<div class="mt-content border-green-turquoise" style="padding: 10px 0px 0px;">
																		<p>
																			<b> 접속한 시스템명 : </b>${al.system_name} &emsp;&emsp;&emsp;<b> 수행업무 : </b>${al.req_context }
																			<br> <b>정보주체식별정보 : </b> 
																			<c:if test="${al.result_type ne null}">
																			<c:set var="prev" value=""/>
																			<c:set var="cnt1" value="1"/>
																			<c:set var="total" value="0"/>
																			<c:forEach items="${al.result_type}">
																				<c:set var="total" value="${total + 1 }"/>
																			</c:forEach>
																			<c:forEach items="${al.result_type}" var="item" varStatus="status">
																				<c:choose>
																					<c:when test="${prev eq item}">	<!-- 이전값과 같을 때 -->
																						<c:set var="cnt1" value="${cnt1 + 1 }"/>
																						<c:if test="${status.count == total }">${cnt1 }</c:if>
																					</c:when>
																					<c:otherwise> 					<!-- 이전값과 다를 때 -->
																						<c:if test="${status.count != 1}">${cnt1 }</c:if>
																						<c:forEach items="${CACHE_RESULT_TYPE}" var="i">
																							<c:if test="${i.key == item }">
																								<img src="${rootPath }/resources/image/icon/resultType/${i.key}.png" title="${i.value }"/>
																							</c:if>
																						</c:forEach>
																						<c:set var="prev" value="${item }"/>
																						<c:set var="cnt1" value="1"/>
																						<c:if test="${status.count == total }">${cnt1 }</c:if>
																					</c:otherwise>
																				</c:choose>
																			</c:forEach>
																			</c:if>
																			<c:if test="${al.result_type eq null}">
																			</c:if>
																		</p>
																	</div>
																</div>
															</div>
														</li>
													</c:when>
													<c:when test="${al.desc_div == 'download' }"> <!-- 다운로드기록 -->
														<li class="mt-item">
															<div class="mt-timeline-icon" style="width: 50px; height: 50px; text-align: center; background: rgb(38, 194, 129);">
																<img src="${rootPath}/resources/image/psm/search/normal.png" style="width: 26px; height: 35px; margin: 7.5px;">
															</div>
															<div class="mt-timeline-content">
																<div class="mt-content-container border-green-turquoise ${classAdd}"
																	style="padding: 15px 30px 5px;">
																	<div class="mt-title">
																		<h3 class="mt-content-title">다운로드접속기록</h3>
																	</div>
																	<div class="mt-author" style="margin: 10px 0px 0px;">
																		<div class="mt-avatar">
																			<img class="img-circle" src="${rootPath}/resources/assets/layouts/layout/img/profile_user.png">
																		</div>
																		<div class="mt-author-name">
																			<a class="font-blue-madison" style="cursor: default;">${al.emp_user_name }(${al.emp_user_id })</a>
																		</div>
																		<div class="mt-author-notes font-grey-mint">
																			<fmt:parseDate value="${al.proc_date}" pattern="yyyyMMdd" var="proc_date" /> 
																			<fmt:formatDate value="${proc_date}" pattern="yyyy-MM-dd" /> 
																			<fmt:parseDate value="${al.proc_time}" pattern="HHmmss" var="proc_time" /> 
																			<fmt:formatDate value="${proc_time}" pattern="HH:mm:ss" />
																		</div>
																	</div>
																	<div class="mt-content border-green-turquoise" style="padding: 10px 0px 0px;">
																		<p>
																			<b> 접속한 시스템명 : </b>${al.system_name} &emsp;&emsp;&emsp;<b> 수행업무 : </b>${al.req_context }
																			<br> <b>개인정보유형 : </b> 
																			<c:if test="${al.result_type ne null}">
																			<c:set var="prev" value=""/>
																			<c:set var="cnt1" value="1"/>
																			<c:set var="total" value="0"/>
																			<c:forEach items="${al.result_type}">
																				<c:set var="total" value="${total + 1 }"/>
																			</c:forEach>
																			<c:forEach items="${al.result_type}" var="item" varStatus="status">
																				<c:choose>
																					<c:when test="${prev eq item}">	<!-- 이전값과 같을 때 -->
																						<c:set var="cnt1" value="${cnt1 + 1 }"/>
																						<c:if test="${status.count == total }">${cnt1 }</c:if>
																					</c:when>
																					<c:otherwise> 					<!-- 이전값과 다를 때 -->
																						<c:if test="${status.count != 1}">${cnt1 }</c:if>
																						<c:forEach items="${CACHE_RESULT_TYPE}" var="i">
																							<c:if test="${i.key == item }">
																								<img src="${rootPath }/resources/image/icon/resultType/${i.key}.png" title="${i.value }"/>
																							</c:if>
																						</c:forEach>
																						<c:set var="prev" value="${item }"/>
																						<c:set var="cnt1" value="1"/>
																						<c:if test="${status.count == total }">${cnt1 }</c:if>
																					</c:otherwise>
																				</c:choose>
																			</c:forEach>
																			</c:if>
																			<c:if test="${al.result_type eq null}">
																			</c:if>
																		</p>
																	</div>
																</div>
															</div>
														</li>
													</c:when>
													<c:when test="${al.desc_div == 'ref' }"> <!-- 접근기록 -->
														<li class="mt-item">
															<div class="mt-timeline-icon" style="width: 50px; height: 50px; text-align: center; background: rgb(38, 194, 129);">
																<img src="${rootPath}/resources/image/psm/search/normal.png" style="width: 26px; height: 35px; margin: 7.5px;">
															</div>
															<div class="mt-timeline-content">
																<div class="mt-content-container border-green-turquoise ${classAdd}"
																	style="padding: 15px 30px 5px;">
																	<div class="mt-title">
																		<h3 class="mt-content-title">접근기록</h3>
																	</div>
																	<div class="mt-author" style="margin: 10px 0px 0px;">
																		<div class="mt-avatar">
																			<img class="img-circle" src="${rootPath}/resources/assets/layouts/layout/img/profile_user.png">
																		</div>
																		<div class="mt-author-name">
																			<a class="font-blue-madison" style="cursor: default;">${al.emp_user_name }(${al.emp_user_id })</a>
																		</div>
																		<div class="mt-author-notes font-grey-mint">
																			<fmt:parseDate value="${al.proc_date}" pattern="yyyyMMdd" var="proc_date" /> 
																			<fmt:formatDate value="${proc_date}" pattern="yyyy-MM-dd" /> 
																			<fmt:parseDate value="${al.proc_time}" pattern="HHmmss" var="proc_time" /> 
																			<fmt:formatDate value="${proc_time}" pattern="HH:mm:ss" />
																		</div>
																	</div>
																	<div class="mt-content border-green-turquoise" style="padding: 10px 0px 0px;">
																		<p>
																			<b> 접속한 시스템명 : </b>${al.system_name}
																			<br> <b>접속 IP : </b>${al.result_type} &emsp;&emsp;&emsp;<b> 접속 경로 : </b>${al.req_context }
																		</p>
																	</div>
																</div>
															</div>
														</li>
													</c:when>
													<c:when test="${al.desc_div == 'senario' }"> <!-- 비정상위험 -->
													<c:set var="classAddSenario" value="${st.count%2==1 ? 'border-left-before-red' : 'border-right-before-red'}"/>
														<li class="mt-item">
															<div class="mt-timeline-icon" style="width: 50px; height: 50px; text-align: center; background: rgb(231,80,90);">
																<img src="${rootPath}/resources/image/psm/search/normal.png" style="width: 26px; height: 35px; margin: 7.5px;">
															</div>
															<div class="mt-timeline-content">
																<div class="mt-content-container border-red ${classAddSenario}"
																	style="padding: 15px 30px 5px;">
																	<div class="mt-title">
																		<h3 class="mt-content-title">비정상위험</h3>
																	</div>
																	<div class="mt-author" style="margin: 10px 0px 0px;">
																		<div class="mt-avatar">
																			<img class="img-circle" src="${rootPath}/resources/assets/layouts/layout/img/profile_user.png">
																		</div>
																		<div class="mt-author-name">
																			<a class="font-blue-madison" style="cursor: default;">${al.emp_user_name }(${al.emp_user_id })</a>
																		</div>
																		<div class="mt-author-notes font-grey-mint">
																			<fmt:parseDate value="${al.proc_date}" pattern="yyyyMMdd" var="proc_date" /> 
																			<fmt:formatDate value="${proc_date}" pattern="yyyy-MM-dd" /> 
																		</div>
																	</div>
																	<div class="mt-content border-red-turquoise" style="padding: 10px 0px 0px;">
																		<p>
																			<b> 접속한 시스템명 : </b>${al.system_name}
																			<br> <b>상세 시나리오명 : </b>${al.result_type} 
																		</p>
																	</div>
																</div>
															</div>
														</li>
													</c:when>
												</c:choose>
												</c:forEach>
											</ul>
											<div class="mt-timeline-line border-grey-steel" style="height:${timeLineLength}px;" id="lineBar"></div>
											<c:if test="${search.total_count > search.size }">
											<button type="button" class="btn btn-sm blue btn-outline sbold uppercase" onclick="appendData()" id="appendButton">더보기</button></c:if>
										</div>
										<div id="loadingT" style="display:none">
											<img id="loading-image" width="30px;" src="${rootPath}/resources/image/loading3.gif" alt="Loading..." />
										</div>
										</div>
								</div>
							</div>				
						</div>
					</div>
				</c:if>
			</div>
		</div>
	</div>
</div>

<form id="allLogInqForm" method="post">
	<input type="hidden" name="detailLogSeq" />
	<input type="hidden" name="detailProcDate" />
</form>
<script type="text/javascript">
	var selectConfig = {
		"listUrl" : "${rootPath}/extrtCondbyInq/findTimeLineChart.html",
		"detailUrl" : "${rootPath}/allLogInq/detail.html"
	};
</script>