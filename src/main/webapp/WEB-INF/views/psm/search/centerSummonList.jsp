<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/search/summonList.js"
	type="text/javascript" charset="UTF-8"></script>
<script
	src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js"
	type="text/javascript"></script>


<h1 class="page-title">${currentMenuName}</h1>
<div class="portlet light portlet-fit portlet-datatable bordered">
	<div class="portlet-body">
		<div class="table-container">
			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
				<div class="dataTables_scroll" style="position: relative;">
					<div class="row">
						<div class="col-md-6"
							style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
							<div class="portlet box grey-salt  ">
								<div class="portlet-title" style="background-color: #2B3643;">
									<div class="caption">
										<i class="fa fa-search"></i>검색 & 엑셀
									</div>
								</div>
								<div class="portlet-body form">
									<div class="form-body"
										style="padding-left: 10px; padding-right: 10px;">
										<div class="form-group">
											<form id="listForm" method="POST"
												class="mt-repeater form-horizontal">
												<div data-repeater-list="group-a">
													<div data-repeater-item class="mt-repeater-item">
														<!-- jQuery Repeater Container -->
														<div class="mt-repeater-input">
															<label class="control-label">기간</label> <br />
															<div
																class="input-group input-medium date-picker input-daterange"
																data-date="10/11/2012" data-date-format="yyyy-mm-dd">
																<input type="text" class="form-control" id="search_fr"
																	name="search_from"
																	value="${search.search_fromWithHyphen}"> <span
																	class="input-group-addon"> &sim; </span> <input
																	type="text" class="form-control" id="search_to"
																	name="search_to" value="${search.search_toWithHyphen}">
															</div>
														</div>
														<div class="mt-repeater-input">
	                                                        <label class="control-label">시스템</label>
	                                                        <br/>
	                                                        <select name="system_seq"
																class="form-control">
																<option value=""
																	${search.system_seq == '' ? 'selected="selected"' : ''}>
																	----- 전 체 -----</option>
																<c:if test="${empty systemMasterList}">
																	<option>시스템 없음</option>
																</c:if>
																<c:if test="${!empty systemMasterList}">
																	<c:forEach items="${systemMasterList}" var="i"
																		varStatus="z">
																		<option value="${i.system_seq}"
																			${i.system_seq==search.system_seq ? "selected=selected" : "" }>${ i.system_name}</option>
																	</c:forEach>
																</c:if>
															</select>
														</div>
														<div class="mt-repeater-input">
															<label class="control-label">소속</label> <br /> <input
																type="text" class="form-control" name="dept_name"
																value="${search.dept_name}" />
														</div>
														<div class="mt-repeater-input">
															<label class="control-label">사용자ID</label> <br /> <input
																type="text" class="form-control" name="emp_user_id"
																value="${search.emp_user_id}" />
														</div>
														<div class="mt-repeater-input">
															<label class="control-label">사용자명</label> <br /> <input
																type="text" class="form-control" name="emp_user_name"
																value="${search.emp_user_name}" />
														</div>
													</div>
												</div>
												<div align="right">
													<button type="reset"
														class="btn btn-sm red-mint btn-outline sbold uppercase"
														onclick="resetOptions(centerSummonConfig['listUrl'])">
														<i class="fa fa-remove"></i> <font>초기화 
													</button>
													<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
														onclick="movecenterSummonList()">
														<i class="fa fa-search"></i> 검색
													</button>
													&nbsp;&nbsp;
													<%-- <a class="btn red btn-outline btn-circle"
													onclick="excelAllLogInqList('${search.total_count}')"> 
													<i class="fa fa-share"></i> <span class="hidden-xs"> 엑셀 </span>
												</a> --%>
													<a onclick="excelcenterSummonList()"><img src="${rootPath}/resources/image/icon/XLS_3.png"></a>
												</div>

												<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" />
												<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
												<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
												<input type="hidden" name="page_num" value="${search.page_num}" />
												<input type="hidden" name="menuNum" id="menuNum" value="${menuNum}" />
												<input type="hidden" name="menuCh" id="menuCh" value="${menuCh}" />
												<input type="hidden" name="menuId" id="menuId" value="${paramBean.sub_menu_id}" />
												<input type="hidden" name="menutitle" id="menutitle" value="${menuCh}${menuNum}" />
												<input type="hidden" name="detailProcTime" id="detailProcTime" value="" />
												<input type="hidden" name="desc_seq" value="" />
												<input type="hidden" name="isSearch" value="${paramBean.isSearch }" />
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<table
							class="table table-striped table-bordered table-hover order-column">
							<colgroup>
								<col width="10%" />
								<col width="10%" />
								<col width="20%" />
								<col width="15%" />
								<col width="15%" />
								<col width="10%" />
								<col width="10%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col" style="text-align: center;">수집일</th>
									<th scope="col" style="text-align: center;">요청일</th>
									<th scope="col" style="text-align: center;">소속</th>
									<th scope="col" style="text-align: center;">시스템</th>
									<th scope="col" style="text-align: center;">사용자ID</th>
									<th scope="col" style="text-align: center;">사용자명</th>
									<th scope="col" style="text-align: center;">결재등록</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${empty centerSummonList}">
										<tr>
											<td colspan="7" align="center">데이터가 없습니다.</td>
										</tr>
									</c:when>
									<c:otherwise>
										<c:forEach items="${centerSummonList}" var="summon" varStatus="status">
											<tr>
												<c:if test="${summon.collect_dt != null}">
													<c:set var="collect_dt" value="${summon.collect_dt}" />
													<td style="text-align: center;">${fn:substring(collect_dt, 0, 4)}-${fn:substring(collect_dt, 4, 6)}-${fn:substring(collect_dt, 6, fn:length(collect_dt))}&nbsp;
													</td>
												</c:if>
												<c:if test="${summon.collect_dt == null}">
													<td style="text-align: center;">-</td>
												</c:if>
												<!-- 요청일 -->
												<c:if test="${summon.request_dt != null}">
													<c:set var="request_dt" value="${summon.request_dt}" />
													<td style="text-align: center;">${fn:substring(request_dt, 0, 4)}-${fn:substring(request_dt, 4, 6)}-${fn:substring(request_dt, 6, fn:length(request_dt))}&nbsp;
													</td>
												</c:if>
												<c:if test="${summon.request_dt == null}">
													<td style="text-align: center;">-</td>
												</c:if>
												<td style="text-align: center;"><ctl:nullCv
														nullCheck="${summon.dept_name }" /></td>
												<td style="text-align: center;"><ctl:nullCv
														nullCheck="${summon.system_name }" /></td>
												<td style="text-align: center;"><ctl:nullCv
														nullCheck="${summon.request_target_user_id }" /></td>
												<td style="text-align: center;"><ctl:nullCv
														nullCheck="${summon.request_target_user_name }" /></td>
												<td style="text-align: center;">
													<button type="button" class="btn btn-sm blue btn-outline sbold uppercase" onclick="apprUpload('<ctl:nullCv nullCheck="${summon.desc_seq }" />')">
														<i class="fa fa-file"></i>결재등록
													</button>
												</td>
											</tr>
										</c:forEach>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>
					</div>
					<div class="dataTables_processing DTS_Loading"
						style="display: none;">Please wait ...</div>
				</div>
				<div class="row" style="padding: 10px;">
					<!-- <div class="col-md-8 col-sm-12">
			            <div class="dataTables_info">Showing 22 to 30 of 178 entries
			            </div>
		            </div>
		            <div class="col-md-4 col-sm-12"></div> -->
					<!-- 페이징 영역 -->
					<c:if test="${search.total_count > 0}">
						<div id="pagingframe" align="center">
							<p>
								<ctl:paginator currentPage="${search.page_num}"
									rowBlockCount="${search.size}"
									totalRowCount="${search.total_count}" />
							</p>
						</div>
					</c:if>
				</div>
			</div>

		</div>
	</div>
</div>
<form id="apprForm" name="apprForm" method="post" target="ProtoType">
</form>
<script type="text/javascript">
	var centerSummonConfig = {
		"listUrl" : "${rootPath}/extrtCondbyInq/centerSummonList.html",
		"detailUrl" : "${rootPath}/extrtCondbyInq/centerSummonDetail.html",
		"downloadUrl" :"${rootPath}/extrtCondbyInq/centerSummonListExcel.html",
		"menu_id" : "${currentMenuId}",
		"menu_name" : "${currentMenuName}",
		"loginPage" : "${rootPath}/loginView.html"

	};

	var menu_id = centerSummonConfig["menu_id"];
	var menu_name = centerSummonConfig["menu_name"];
	var log_message = "SUCCESS";
	var log_action = "SELECT";
</script>