<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="portlet-title">
       <div class="caption" style="width:50%; padding-top:9px; float: left;">
       	<i class="icon-settings font-dark"></i>
           <span class="caption-subject font-dark sbold uppercase">개인정보</span>
       </div>
       <div style="text-align: right; float: right;">
		<div class="btn-group">
			<a data-toggle="dropdown"> <img src="${rootPath}/resources/image/icon/XLS_3.png">
			</a>
			<ul class="dropdown-menu pull-right">
				<li><a onclick="excelAllLogInqDetail('${search.total_count}')"> EXCEL </a></li>
				<li><a onclick="csvAllLogInqDetail()"> CSV </a></li>
			</ul>
		</div>
		<select style="border: 1px solid #c2cad8; padding: 6px 12px; font-size: 14px; color: #555;" onchange="searchResultType(this.value)">
			<option value="" ${search.detailResultType == '' ? 'selected="selected"' : ''}>----- 전 체 -----</option>
			<c:if test="${empty resultTypeList}">
				<option>데이터 없음</option>
			</c:if>
			<c:if test="${!empty resultTypeList}">
				<c:forEach items="${resultTypeList}" var="i" varStatus="z">
					<option value="${i.result_type}" ${i.result_type==search.detailResultType ? "selected=selected" : "" }>${i.privacy_desc}</option>
				</c:forEach>
			</c:if>
		</select>
	</div>
</div>
					    <div class="portlet-body" style="padding-top: 0px;">
					    <c:choose>
							<c:when test="${ui_type eq 'M' }">
								<div class="row" style="margin-bottom: 20px;">
									<div class="col-md-12">
										<table class="table table-striped table-bordered table-hover table-checkable dataTable no-footer">
											<tbody>
												<tr>
													<th style="width:10%;text-align: center;">No. </th>
						                            <th style="width:20%;text-align: center;">정보주체자 </th>
						                            <th style="width:50%;text-align: center;">개인정보유형</th>
												 	<th style="width:10%;text-align: center;">개인정보수</th>
												 	<th style="width:10%;text-align: center;">상세</th>
												</tr>
												<c:choose>
													<c:when test="${empty allLogInqDetailList}">
										        		<tr><td colspan="4" align="center">데이터가 없습니다.</td></tr>
													</c:when>
													<c:otherwise>
														<c:forEach items="${allLogInqDetailList }" var="ali" varStatus="vs">
															<tr>
																<td align="center">${vs.count }</td>
																<td align="center">${ali.result_owner }</td>
																<td align="center">
																	<c:set var="pr" value=""/>
																	<c:set var="cnt2" value="1"/>
																	<c:set var="tot" value="0"/>
																	<c:forEach items="${ali.result_type}">
																		<c:set var="tot" value="${tot + 1 }"/>
																	</c:forEach>
																	<c:forEach items="${ali.result_type}" var="rt" varStatus="status1">
																		<c:choose>
																			<c:when test="${pr eq rt}">	<!-- 이전값과 같을 때 -->
																				<c:set var="cnt2" value="${cnt2 + 1 }"/>
																				<c:if test="${status1.count == tot }">${cnt2 }</c:if>
																			</c:when>
																			<c:otherwise> 					<!-- 이전값과 다를 때 -->
																				<c:if test="${status1.count != 1}">${cnt2 }</c:if>
																				<c:forEach items="${CACHE_RESULT_TYPE}" var="i">
																					<c:if test="${i.key == rt }">
																						<img src="${rootPath }/resources/image/icon/resultType/${i.key}.png" title="${i.value }"/>
																					</c:if>
																				</c:forEach>
																				
																				<c:set var="pr" value="${rt }"/>
																				<c:set var="cnt2" value="1"/>
																				<c:if test="${status1.count == tot }">${cnt2 }</c:if>
																			</c:otherwise>
																		</c:choose>
																	</c:forEach>
																</td>
																<td align="center">${ali.cnt }</td>
																<td align="center">
																<button type="button" style="padding-top: 2px;padding-bottom: 2px; padding-right: 8px;padding-left: 8px;" class="btn blue mt-ladda-btn ladda-button btn-outline btn-circle" 
																onclick="showResultType('${allLogInq.log_seq}','${allLogInq.proc_date}','${ali.result_owner}','${allLogInq.system_seq}', '${vs.index}')"" data-style="slide-up" data-spinner-color="#333">
							 										<span class="ladda-label" style="font-size: 12px;">상세보기</span>
							                                    <span class="ladda-spinner"></span></button>
		                                    					</td>
															</tr>
															<tr id="subResultType_${vs.index}" style="display: none;">
																<td colspan="5">
																	<div style="overflow: auto;">
																	<table width="100%" id="subResultTable_${vs.index}" class="table table-striped table-bordered table-hover">
												                        <thead>
												                            <tr>
												                                <th style="text-align: center;"> No. </th>
												                                <th style="text-align: center;"> 개인정보유형  </th>
												                                <th style="text-align: center;"> 개인정보내용 </th>
												                            </tr>
												                        </thead>
												                        <tbody>
												                        </tbody>
												                    </table>
												                    </div>
																</td>
															</tr>
														</c:forEach>
													</c:otherwise>
												</c:choose>
											</tbody>
										</table>
									</div>
								</div>
								
							</c:when>
							<c:otherwise>
				         <div class="row">
				             <div class="col-md-12">
				                 <table id="user" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer">
				                     <tbody>
				                         <c:choose>
				                     		<c:when test="${(mode_info_viewer eq 'Y') }">
				                     			<tr>
						                         	<th style="width:10%;text-align: center;">No. </th>
						                            <th style="width:30%;text-align: center;">개인정보유형 </th>
						                            <th style="width:30%;text-align: center;">개인정보내용</th>
												 	<th style="width:15%;text-align: center;">정보주체확인</th>
					                            	<th style="width:15%;text-align: center;">탐지 예외 처리 </th>
						                         </tr>
				                     		</c:when>
				                     		<c:when test="${result_owner_falg eq 'Y' }">
				                     			<tr>
						                         	<th style="width:10%;text-align: center;">No. </th>
						                            <th style="width:30%;text-align: center;">개인정보유형 </th>
						                            <th style="width:30%;text-align: center;">개인정보내용</th>
												 	<th style="width:15%;text-align: center;">정보주체확인</th>
					                            	<th style="width:15%;text-align: center;">탐지 예외 처리 </th>
						                         </tr>
				                     		</c:when>
				                     		<c:otherwise>
				                     			<tr>
						                         	<th style="width:10%;text-align: center;">No. </th>
						                            <th style="width:30%;text-align: center;">개인정보유형 </th>
						                            <th style="width:30%;text-align: center;">개인정보내용</th>
					                            	<th style="width:30%;text-align: center;">탐지 예외 처리 </th>
						                         </tr>
				                     		</c:otherwise>
				                     	</c:choose>
				                         
				                         <c:choose>
											<c:when test="${empty allLogInqDetailList}">
												<tr>
									        		<td colspan="8" align="center">데이터가 없습니다.</td>
									        	</tr>
											</c:when>
											<c:otherwise>
												<c:set value="${search.allLogInqDetail.total_count}" var="count"/>
												<c:forEach items="${allLogInqDetailList}" var="allLogInqDetailList"  varStatus="status">
													<tr>
														<td align="center">${count - search.allLogInqDetail.page_num * search.allLogInqDetail.size + search.allLogInqDetail.size }</td>
														<td align="center"<c:if test="${allLogInqDetailList.check_exc eq 1}">style="color:lightgray;"</c:if>>
															<ctl:nullCv nullCheck="${allLogInqDetailList.result_type}"/>
															<c:if test="${biz_log_file eq 'Y' }">
															<c:choose>
																<c:when test="${allLogInqDetailList.detection_src eq '103' }"><i class="icon-doc" style="vertical-align: middle;"></i></c:when>
																<c:otherwise><i class="icon-screen-desktop" style="vertical-align: middle;"></i></c:otherwise>
															</c:choose>
															</c:if>
														</td>
														<td align="center"<c:if test="${allLogInqDetailList.check_exc eq 1}">style="color:lightgray;"</c:if>>
															<ctl:nullCv nullCheck="${allLogInqDetailList.result_content_masking}"/>
															<c:if test="${(ui_type eq 'B') && (allLogInqDetailList.result_type eq '환자번호') && (!empty allLogInqDetailList.emp_user_name)}">
																(<ctl:nullCv nullCheck="${allLogInqDetailList.emp_user_name}"/>)
															</c:if>
															<c:if test="${(ui_type eq 'B') && (allLogInqDetailList.result_type eq '영상번호') && (!empty allLogInqDetailList.emp_user_name)}">
																(<ctl:nullCv nullCheck="${allLogInqDetailList.emp_user_name}"/>)
															</c:if>
														</td>
														<c:if test="${result_owner_falg eq 'Y' }">
															<td align="center">
																<ctl:nullCv nullCheck="${allLogInqDetailList.result_owner}"/>
															</td>
														</c:if> 															
														<c:if test="${(mode_info_viewer eq 'Y') }">
															<td align="center">
																<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="showPrivacyInfo('${allLogInqDetailList.result_content_masking}','${allLogInqDetailList.privacy_seq}' )" style="cursor: pointer;">
																	<i class="fa fa-check"></i>확인
																</a>
															</td>
														</c:if>
														<c:choose>
														<c:when test="${insert_biz_log_result eq 'N' }">
															<td align="center">-</td>
														</c:when>
														<c:otherwise>
														<c:choose>
															<c:when test="${allLogInqDetailList.check_exc ne 1}">
																<td align="center">
																<p class="btn btn-xs green-haze" onclick="javascript:deletePrivacy(${allLogInqDetailList.biz_log_result_seq},${allLogInqDetailList.privacy_seq},'${allLogInqDetailList2[status.index].result_content}')">
																<i class="fa fa-remove"></i>&nbsp;&nbsp;&nbsp;제외</p></td>
															</c:when>
															<c:otherwise>
																<!-- 제외됨 -->
																<%-- <td align="center" style="color:lightgray; cursor: pointer;" onclick="javascript:goMisdetect(${allLogInqDetailList.biz_log_result_seq},${allLogInqDetailList.privacy_seq},'${allLogInqDetailList.result_content}')">
																<p class="btn btn-xs green-haze" onclick="javascript:goMisdetect()">
																	<i class="fa fa-check"></i>&nbsp;&nbsp;&nbsp;제외됨</p>
																</td> --%>
																<!-- 복원 -->
																<td align="center">
																<p class="btn btn-xs red" onclick="javascript:restorePrivacy(${allLogInqDetailList.biz_log_result_seq},${allLogInqDetailList.privacy_seq},'${allLogInqDetailList2[status.index].result_content}')">
																	<i class="fa fa-check"></i>&nbsp;&nbsp;&nbsp;복원</p>
																</td>
															</c:otherwise>
														</c:choose>
														</c:otherwise>
														</c:choose>
													</tr>
												<c:set var="count" value="${count - 1 }"/>
												</c:forEach>
											</c:otherwise>
										</c:choose>
				                     </tbody>
				                 </table>
				             </div>
				             
				         </div>
				         <div class="row">
				         	<div class="col-md-12" align="center">
				         		 <!-- 페이징 영역 -->
								<c:if test="${search.allLogInqDetail.total_count > 0}">
									<div id="pagingframe">
										<p><ctl:paginator currentPage="${search.allLogInqDetail.page_num}" rowBlockCount="${search.allLogInqDetail.size}" totalRowCount="${search.allLogInqDetail.total_count}" pageName="allLogInqDetail"/></p>
									</div>
								</c:if>
				         	</div>
				         </div>
				         </c:otherwise>
				         </c:choose>
				         </div>