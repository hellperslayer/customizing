<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>

<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<link href="${rootPath}/resources/css/profile.min.css" rel="stylesheet" type="text/css">


<h1 class="page-title" style="text-align: center;"> ${currentMenuName}
	<!--bold font-blue-hoki <small>basic bootstrap tables with various options and styles</small> -->
</h1>

 		

<div class="row" style="padding-top: 10px; padding-bottom: 20px">
	<table id="user" class="table table-bordered table-striped" border="1" style="border-collapse: collapse;width: 480px;height: 100px;">
		 <tbody>
			 <tr>
				<th style="width:30%;text-align: center;">개인정보주체</th>
				<td style="width:70%;text-align: center;">${paramBean.name }</td>
			 </tr>
			 <tr>
				<th style="width:30%;text-align: center;">조회개인정보</th>
				<td style="width:70%;text-align: center;">${paramBean.privacy }</td>
			 </tr>
		 </tbody>
	 </table>
	<!-- END PROFILE CONTENT -->
</div>
