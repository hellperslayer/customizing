<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>

<c:set var="currentMenuId" value="${index_id }" />
<%-- <c:set var="currentMenuId" value="MENU00495" /> --%>
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />
<c:set var="now" value="<%=new java.util.Date()%>" />
<c:set var="Year">
		<fmt:formatDate value="${now}" pattern="yyyy" />
</c:set>
<fmt:parseDate var="dateString" value="${install_date.code_name}" pattern="yyyy-MM-dd" />
<c:set var="installYear">
	<fmt:formatDate value="${dateString}" pattern="yyyy" />
</c:set>
<c:set var="mulYear" value="${Year-installYear }"/>


<script src="${rootPath}/resources/js/psm/search/summonProcess.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>


<h1 class="page-title">
	${currentMenuName} 
</h1>
<div class="portlet light portlet-fit portlet-datatable bordered">
    <div class="portlet-body">
        <div class="table-container">
            <div id="datatable_ajax_2_wrapper" class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
            <div class="dataTables_scroll" style="position: relative;">
	            <div class="row">
                   <div class="col-md-6" style="width:100%; margin-top:0px; padding-left:0px; padding-right:0px;">
                     <div class="portlet box grey-salt  ">
                           <div class="portlet-title" style="background-color: #2B3643;">
                               <div class="caption">
                                   <i class="fa fa-search"></i>검색</div>
                           </div>
                           <div class="portlet-body form" >
                               <div class="form-body" style="padding-left:10px; padding-right:10px;">
                                   <div class="form-group">
                                       <form id="listForm" method="POST" class="mt-repeater form-horizontal">
                                           <div data-repeater-list="group-a">
                                               <div class="row">
                                                   <!-- jQuery Repeater Container -->.
                                                   <div class="col-md-2">
                                                       <label class="control-label">년</label>
                                                       <select id="year" name="year" class="form-control">
															<c:forEach var="mul" begin="0" end="${mulYear }" step="1">
                        										<option value="${Year-mul}" <c:if test="${Year-mul eq search.year}">selected</c:if>>${Year-mul }년</option>
                        									</c:forEach>
														</select>
                                                   </div>
                                                   <div class="col-md-2">
                                                       <label class="control-label">월</label>
                                                       <select id="month" name="month" class="form-control">
															<c:forEach var="i" begin="1" end="12" step="1">
																<option value="${i }" <c:if test="${ i eq search.month }">selected="selected"</c:if>>${i }월</option>
															</c:forEach>
														</select>
                                                   </div>
                                                   <div class="col-md-2">
                                                        <label class="control-label">소속</label>
                                                        <input type="text" class="form-control" name="dept_name" value="${search.dept_name}" />
                                                    </div>
                                                   <div class="col-md-4">
                                                       <label class="control-label">추출조건상세</label>
                                                       <select name="rule_cd" class="form-control">
															<option value=""> ----- 선 택 ----- </option>
															<c:forEach	items="${ruleTblList}" var="ruleTblList">
																<option value="${ruleTblList.rule_seq}" <c:if test="${ruleTblList.rule_seq == search.rule_cd}">selected="selected"</c:if>>${ruleTblList.rule_nm}</option>
															</c:forEach>
														</select>
                                                   </div>
                                               </div>
                                           </div>
                                           <hr/>
                                           <div align="right">
                                            <button type="reset"
												class="btn btn-sm red-mint btn-outline sbold uppercase"
												onclick="resetOptions(summonConfig['listUrl'])">
												<i class="fa fa-remove"></i> <font>초기화
											</button>
											<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
												onclick="movesummonList()">
												<i class="fa fa-search"></i> 검색
											</button>&nbsp;&nbsp;
											<%-- <a class="btn red btn-outline btn-circle"
												onclick="excelAllLogInqList('${search.total_count}')"> 
												<i class="fa fa-share"></i> <span class="hidden-xs"> 엑셀 </span>
											</a> --%>
											<a onclick="excelSummonList('${search.total_count}')"><img src="${rootPath}/resources/image/icon/XLS_3.png"></a>
										</div>
										
										<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }"/>
										<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }"/>
										<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
										<input type="hidden" name="page_num" value="${search.page_num}" />
										<input type="hidden" name="detailEmpCd" value=""/>
										<input type="hidden" name="detailOccrDt" value=""/>
										<input type="hidden" name="detailEmpDetailSeq" value=""/>
										<input type="hidden" name="sort_flag" id="sort_flag" value="${search.sort_flag}"/>
										<input type="hidden" id="rootPath" name="rootPath" value="${rootPath}" />
										
										<div id = "chart_extract">
										</div>
                                       </form>
                                   </div>
                               </div>
                           </div>
                       </div>
                 </div>
             </div>     
             
             <div class="row">    
             	<table class="table table-striped table-bordered table-hover order-column">
                	<colgroup>
						<col width="10%" />
						<col width="15%" />
						<col width="25%" />
						<col width="10%" />
						<col width="10%" />
						<col width="10%" />
						<col width="10%" />
					</colgroup>
					<thead>
						<tr>
							<th scope="col" style="text-align: center;">소속</th>
							<th scope="col" style="text-align: center;">추출조건명</th>
							<th scope="col" style="text-align: center;">추출조건상세</th>
							<th scope="col" style="text-align: center;">소명요청 건</th>
							<th scope="col" style="text-align: center;">판정대기 건</th>
							<th scope="col" style="text-align: center;">재요청 건</th>
							<th scope="col" style="text-align: center;">${summon_cfm_yn eq 'Y' ? '부서판정건' : '판정완료 건'}</th>
							<c:if test="${summon_cfm_yn eq 'Y' }">
								<th scope="col" style="text-align: center;">최종판정 건</th>
							</c:if>							
						</tr>
					</thead>
	                <tbody>
	                	<c:choose>
						<c:when test="${empty extrtCondbyInqList.extrtCondbyInq}">
							<tr>
				        		<td colspan="7" align="center">데이터가 없습니다.</td>
				        	</tr>
						</c:when>
						<c:otherwise>
							<c:set value="${extrtCondbyInqList.page_total_count}" var="count"/>
							<c:forEach items="${extrtCondbyInqList.extrtCondbyInq}" var="summon" varStatus="status">
								<tr>
									<td style="padding-left:20px;"><ctl:nullCv nullCheck="${summon.dept_name }"/>
									<td style="padding-left:20px;">${summon.scen_name }</td>
									<td style="padding-left:20px;">${summon.rule_nm }</td>
									<td style="text-align: right;"><ctl:nullCv nullCheck="${summon.s_nm2 }"/>
									<td style="text-align: right;"><ctl:nullCv nullCheck="${summon.snm_4 }"/>	
									<td style="text-align: right;"><ctl:nullCv nullCheck="${summon.snm_5 }"/>
									<td style="text-align: right;"><ctl:nullCv nullCheck="${summon.snm_8 }"/>
									<td style="text-align: right;"><ctl:nullCv nullCheck="${summon.snm_10 }"/>
								</tr>
								<c:set var="count" value="${count - 1 }"/>
							</c:forEach>
						</c:otherwise>
						</c:choose>
	                </tbody>
	            </table>
            </div>
             <div class="dataTables_processing DTS_Loading" style="display: none;">Please wait ...</div>
            </div>
	             <div class="row" style="padding: 10px;">
		            <!-- 페이징 영역 -->
					<c:if test="${search.total_count > 0}">
						<div id="pagingframe" align="center">
							<p>
								<ctl:paginator currentPage="${search.page_num}"
									rowBlockCount="${search.size}"
									totalRowCount="${search.total_count}" />
							</p>
						</div>
					</c:if>
		        </div>
            </div>
            
        </div>
    </div>   
</div>
<script type="text/javascript">

	var summonConfig = {
		"listUrl":"${rootPath}/extrtCondbyInq/summonProcessby.html"
		,"detailUrl":"${rootPath}/extrtCondbyInq/summonDetail.html"
		,"downloadUrl" :"${rootPath}/extrtCondbyInq/summonProcessbyDownload.html"
		,"menu_id":"${currentMenuId}"
		,"menu_name":"${currentMenuName}"
		,"loginPage" : "${rootPath}/loginView.html"
		,"detailChartUrl" : "${rootPath}/summon/detailChart.html"
		
	};
	
	summonManageConfig = {
		"listUrl":"${rootPath}/extrtCondbyInq/summonList.html"
	}

	var menu_id = summonConfig["menu_id"];
	var menu_name = summonConfig["menu_name"];
	var log_message = "SUCCESS";
	var log_action = "SELECT";
	
</script>