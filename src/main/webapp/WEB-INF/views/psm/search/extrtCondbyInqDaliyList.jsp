<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/search/extrtCondbyInqStaticList.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>
<style>
.mjpoint {
	padding: 0px;
	margin: 0px;
	vertical-align: middle;
	text-align: right;
}

.mjpointZero {
	padding: 0px;
	margin: 0px;
	vertical-align: middle;
	text-align: right;
}
</style>
<h1 class="page-title">${currentMenuName}</h1>
<div class="portlet light portlet-fit portlet-datatable bordered">
	<div class="portlet-body">
		<div class="table-container">
			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
				<div class="dataTables_scroll" style="position: relative;">
					<div class="row">
						<div class="col-md-6"
							style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
							<div class="portlet box grey-salt  ">
								<div class="portlet-title" style="background-color: #2B3643;">
									<div class="caption">
										<i class="fa fa-search"></i>검색 & 엑셀
									</div>
									<div class="tools">
										<a id="searchBar_icon" href="javascript:;" class="collapse"></a>
									</div>
								</div>
								<div class="portlet-body form" id="searchBar">
									<div class="form-body"
										style="padding-left: 10px; padding-right: 10px;">
										<div class="form-group">
											<form id="listForm" method="POST"
												class="mt-repeater form-horizontal">
												<div data-repeater-list="group-a">
													<div class="row">
														<div class="col-md-2">
															<label class="control-label">기간</label>
															<div class="input-group date-picker input-daterange"
																data-date="10/11/2012" data-date-format="yyyy-mm-dd">
																<input type="text" class="form-control" id="search_fr" name="search_from" value="${search.search_fromWithHyphen}"> <span
																	class="input-group-addon"> &sim; </span> 
																	<input type="text" class="form-control" id="search_to" name="search_to" value="${search.search_toWithHyphen}">
															</div>
														</div>
														<div class="col-md-2">
															<label class="control-label">시스템</label> <br /> <select
																name="system_seq" class="form-control">
																<option value="" ${search.system_seq == '' ? 'selected="selected"' : ''}> ----- 전 체 -----</option>
																<c:if test="${empty systemMasterList}">
																	<option>시스템 없음</option>
																</c:if>
																<c:if test="${!empty systemMasterList}">
																	<c:forEach items="${systemMasterList}" var="i" varStatus="z">
																		<option value="${i.system_seq}" ${i.system_seq==search.system_seq ? "selected=selected" : "" }>${ i.system_name}</option>
																	</c:forEach>
																</c:if>
															</select>
														</div>
													</div>
												</div>
												<hr />
												<div align="right">
													<button type="reset" class="btn btn-sm red-mint btn-outline sbold uppercase" onclick="resetOptions(selectConfig['listUrl'])">
														<i class="fa fa-remove"></i> <font>초기화 
													</button>
													<button type="button" class="btn btn-sm blue btn-outline sbold uppercase" onclick="moveextrtCondbyInqList()">
														<i class="fa fa-search"></i> 검색
													</button>
													&nbsp;&nbsp;
													<a onclick="excelExtrtCondbyInqList('${search.total_count}')"><img src="${rootPath}/resources/image/icon/XLS_3.png"></a>
												</div>
												<input type="hidden" name="main_menu_id" value="${search.main_menu_id }" />
												<input type="hidden" name="sub_menu_id" value="${search.sub_menu_id }" />
												<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
												<input type="hidden" name="page_num" value="${search.page_num}" />
												<input type="hidden" name="detailEmpCd" value="" />
												<input type="hidden" name="detailOccrDt" value="" />
												<input type="hidden" name="detailEmpDetailSeq" value="" />
												<input type="hidden" name="isSearch" value="${search.isSearch }" />
												<input type="hidden" name="userId" value="" />
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<table style="border-top: 1px solid #e7ecf1;" class="table table-striped table-bordered table-checkable dataTable no-footer" role="grid">
							<thead>
								<tr><th width="15%" style="text-align: center; vertical-align: middle;">일시</th>
								<c:forEach items="${scenarioList}" var="sl">
									<th width="15%" style="text-align: center; vertical-align: middle;">${sl.scen_name}</th></c:forEach>
								<th width="10%" style="text-align: center; vertical-align: middle;">합계</th></tr>
							</thead>
							<tbody>
							<c:choose>
								<c:when test="${empty extrtCondbyInqList }">
								<tr><td colspan="7">데이터가 없습니다.</td></tr>
								</c:when>
								<c:otherwise>
								<c:forEach items="${extrtCondbyInqList}" var="el">
								<tr>
								<td>
									<fmt:parseDate value="${el.proc_date }" pattern="yyyyMMdd" var="d"/>
									<fmt:formatDate value="${d }" pattern="yyyy-MM-dd"/>
								</td>
								<td <statistics:compare max="${max.scenario1}" value="${el.scenario1}"/>>
									<c:if test="${el.scenario1 != 0}"><p class="mjpoint" style="cursor: pointer;" onclick="moveExtrtListByDate('${el.proc_date}', '1000')"></c:if>
									<c:if test="${el.scenario1 == 0}"><p class="mjpointZero"></c:if><fmt:formatNumber value="${el.scenario1}" type="number" /></p>
								</td>
								<td <statistics:compare max="${max.scenario2}" value="${el.scenario2}"/>>
									<c:if test="${el.scenario2 != 0}"><p class="mjpoint" style="cursor: pointer;" onclick="moveExtrtListByDate('${el.proc_date}', '2000')"></c:if>
									<c:if test="${el.scenario2 == 0}"><p class="mjpointZero"></c:if><fmt:formatNumber value="${el.scenario2}" type="number" /></p>
								</td>
								<td <statistics:compare max="${max.scenario3}" value="${el.scenario3}"/>>
									<c:if test="${el.scenario3 != 0}"><p class="mjpoint" style="cursor: pointer;" onclick="moveExtrtListByDate('${el.proc_date}', '3000')"></c:if>
									<c:if test="${el.scenario3 == 0}"><p class="mjpointZero"></c:if><fmt:formatNumber value="${el.scenario3}" type="number" /></p>
								</td>
								<td <statistics:compare max="${max.scenario4}" value="${el.scenario4}"/>>
									<c:if test="${el.scenario4 != 0}"><p class="mjpoint" style="cursor: pointer;" onclick="moveExtrtListByDate('${el.proc_date}', '4000')"></c:if>
									<c:if test="${el.scenario4 == 0}"><p class="mjpointZero"></c:if><fmt:formatNumber value="${el.scenario4}" type="number" /></p>
								</td>
								<td <statistics:compare max="${max.scenario5}" value="${el.scenario5}"/>>
									<c:if test="${el.scenario5 != 0}"><p class="mjpoint" style="cursor: pointer;" onclick="moveExtrtListByDate('${el.proc_date}', '5000')"></c:if>
									<c:if test="${el.scenario5 == 0}"><p class="mjpointZero"></c:if><fmt:formatNumber value="${el.scenario5}" type="number" /></p>
								</td>
								<td <statistics:compare max="${max.totalPrivCount}" value="${el.totalPrivCount}"/>>
									<c:if test="${el.totalPrivCount != 0}"><p class="mjpoint" style="cursor: pointer;" onclick="moveExtrtListByDate('${el.proc_date}', '')"></c:if>
									<c:if test="${el.totalPrivCount == 0}"><p class="mjpointZero"></c:if><fmt:formatNumber value="${el.totalPrivCount}" type="number" /></p>
								</td>
								</tr>
								</c:forEach>
								<tr>
								<td>합계</td>
								<td>
									<c:if test="${sum.scenario1 != 0}"><p class="mjpoint"></c:if>
									<c:if test="${sum.scenario1 == 0}"><p class="mjpointZero"></c:if><fmt:formatNumber value="${sum.scenario1}" type="number" /></p>
								</td>
								<td>
									<c:if test="${sum.scenario2 != 0}"><p class="mjpoint"></c:if>
									<c:if test="${sum.scenario2 == 0}"><p class="mjpointZero"></c:if><fmt:formatNumber value="${sum.scenario2}" type="number" /></p>
								</td>
								<td>
									<c:if test="${sum.scenario3 != 0}"><p class="mjpoint"></c:if>
									<c:if test="${sum.scenario3 == 0}"><p class="mjpointZero"></c:if><fmt:formatNumber value="${sum.scenario3}" type="number" /></p>
								</td>
								<td>
									<c:if test="${sum.scenario4 != 0}"><p class="mjpoint"></c:if>
									<c:if test="${sum.scenario4 == 0}"><p class="mjpointZero"></c:if><fmt:formatNumber value="${sum.scenario4}" type="number" /></p>
								</td>
								<td>
									<c:if test="${sum.scenario5 != 0}"><p class="mjpoint"></c:if>
									<c:if test="${sum.scenario5 == 0}"><p class="mjpointZero"></c:if><fmt:formatNumber value="${sum.scenario5}" type="number" /></p>
								</td>
								<td>
									<c:if test="${sum.totalPrivCount != 0}"><p class="mjpoint"></c:if>
									<c:if test="${sum.totalPrivCount == 0}"><p class="mjpointZero"></c:if><fmt:formatNumber value="${sum.totalPrivCount}" type="number" /></p>
								</td>
								</tr>
								</c:otherwise>
							</c:choose>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<form id="moveForm" method="post">
	<input type="hidden" name="search_from" value="" />
	<input type="hidden" name="search_to" value="" />
	<input type="hidden" name="emp_user_id" value="" />
	<input type="hidden" name="dept_id" value="" />
	<input type="hidden" name="system_seq" value="" />
	<input type="hidden" name="scen_seq" value="" />
</form>

<script type="text/javascript">
	var selectConfig = {
		"listUrl" : "${rootPath}/extrtCondbyInq/extrtCondbyInqDaliyList.html",
		"detailUrl" : "${rootPath}/extrtCondbyInq/list.html",
		"downloadUrl" : "${rootPath}/extrtCondbyInq/extrtCondbyInqDaliyDownload.html"
	};
</script>