<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/search/summonList.js" type="text/javascript" charset="UTF-8"></script>


<h1 class="page-title">
	${currentMenuName} 
</h1>

<form id="listForm" method="POST">
<div class="portlet light portlet-fit portlet-datatable bordered">

	<div class="portlet-body">
		<div class="table-container">
		
			<div class="caption" style="padding-bottom:5px;">
                <i class="icon-settings font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase">추출조건정보</span>
            </div>
            <div style="padding-bottom:20px;">
                <table class="table table-striped table-bordered order-column">
                    <tbody>
                    	<tr>
                            <th width="10%" style="text-align: center; vertical-align: middle;">발생년월일</th>
                            <td width="40%" style="text-align: center; vertical-align: middle;">${extrtCondbyInq.occr_dt}</td>
                            <th width="10%" style="text-align: center; vertical-align: middle;">시나리오명</th>
                            <td width="40%" style="text-align: center; vertical-align: middle;">${extrtCondbyInq.scen_name}</td>
                        </tr>
                        <tr>
                            <th style="text-align: center; vertical-align: middle;">상세시나리오명</th>
                            <td colspan="3" style="text-align: left; vertical-align: middle;">${extrtCondbyInq.rule_nm}</td>
                        </tr>
                        <tr>
                            <th style="text-align: center; vertical-align: middle;">취급자</th>
                            <td style="text-align: center; vertical-align: middle;">${extrtCondbyInq.emp_user_name}(${extrtCondbyInq.emp_user_id})</td>
                            <th style="text-align: center; vertical-align: middle;">소속</th>
                            <td style="text-align: center; vertical-align: middle;">${extrtCondbyInq.dept_name}(${extrtCondbyInq.dept_id})</td>
                        </tr>
                        <tr>
                            <th style="text-align: center; vertical-align: middle;">발생시스템</th>
                            <td style="text-align: center; vertical-align: middle;"><ctl:nullCv nullCheck="${extrtCondbyInq.system_name}"/></td>
                            <th style="text-align: center; vertical-align: middle;">임계치</th>
                            <td style="text-align: center; vertical-align: middle;">${extrtCondbyInq.limit_cnt}</td>
                        </tr>
                        <tr>
                            <th style="text-align: center; vertical-align: middle;">로그구분</th>
                            <td style="text-align: center; vertical-align: middle;">
                            	<c:choose>
                            		<c:when test="${extrtCondbyInq.log_delimiter == 'BA'}">접속기록조회</c:when>
                            		<c:when test="${extrtCondbyInq.log_delimiter == 'DB'}">DB접근조회</c:when>
                            		<c:when test="${extrtCondbyInq.log_delimiter == 'DN'}">다운로드로그</c:when>
                            	</c:choose>
                            </td>
                            <th style="text-align: center; vertical-align: middle;">처리기준</th>
                            <td style="text-align: center; vertical-align: middle;">
                            	<c:choose>
                            		<c:when test="${extrtCondbyInq.rule_view_type == 'M'}">처리량</c:when>
                            		<c:when test="${extrtCondbyInq.rule_view_type == 'R'}">이용량</c:when>
                            	</c:choose>
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: center; vertical-align: middle;">위험도</th>
                            <td style="text-align: center; vertical-align: middle;">${extrtCondbyInq.dng_val}</td>
                            <c:choose>
                            <c:when test="${ui_type == 'DGB'}">
	                            <th style="text-align: center; vertical-align: middle;">개인정보 처리 건</th>
	                            <td style="text-align: center; vertical-align: middle;">${extrtCondbyInq.rule_cnt}</td>
                            </c:when>
                            <c:otherwise>
                            	<th style="text-align: center; vertical-align: middle;">비정상걸린횟수</th>
	                            <td style="text-align: center; vertical-align: middle;">${extrtCondbyInq.rule_cnt}</td>
                            </c:otherwise>
                            </c:choose>
                            
                        </tr>
                    </tbody>
                </table>
            </div>
            
            <div class="btn-group" style="float:right;">
				<a href="javascript:;" data-toggle="dropdown"><img src="${rootPath}/resources/image/icon/XLS_3.png"></a>
					<ul class="dropdown-menu pull-right">
					<!-- <li><a data-toggle="modal" data-target="#myModal">업로드</a></li> -->
						<li><a onclick="excelSummonDetailUpLoad('${extrtCondbyInq.emp_user_name}','${extrtCondbyInq.emp_user_id}')"> 다운로드 </a></li>
					</ul>
			</div>
            
            
            <div class="caption" style="padding-bottom:5px;">
                <i class="icon-settings font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase">로그정보</span>
                <c:if test="${extrtCondbyInq.rule_view_type == 'M'}">
                <div class="log_filter" style="float:right; margin-right:5%;">
	            	<label for="log_filter">로그 필터(개인정보유형 갯수)</label>
	                <select name="log_filter" id="log_filter">
	                	<option value="">---전체 조회---</option>
	                	<option value="5" <c:if test="${search.log_filter eq '5'}">selected</c:if>>5건이상</option>
	                	<option value="1000" <c:if test="${search.log_filter eq '1000'}">selected</c:if>>1000건이상</option>
	                	<option value="5000" <c:if test="${search.log_filter eq '5000'}">selected</c:if>>5000건이상</option>
	                </select>
	                <button type="button" style="padding-top: 2px;padding-bottom: 2px; padding-right: 8px;padding-left: 8px;" class="btn blue mt-ladda-btn ladda-button btn-outline btn-circle" onclick="LogFilterSearch()">
	                	검색
	                </button>
                </div>
                </c:if>
            </div>
            <div>
                <table class="table table-striped table-bordered table-hover order-column">
                    <thead>
                        <tr>
                        	<th width="5%" style="text-align: center; vertical-align: middle;">
                        		번호
                        	</th>
                        	<th width="15%" style="text-align: center; vertical-align: middle;">일시</th>
                        	<c:if test="${extrtCondbyInq.log_delimiter == 'DN'}">
                        		<th width="10%" style="text-align: center; vertical-align: middle;">파일명</th>
                        	</c:if>
                        	<th width="10%" style="text-align: center; vertical-align: middle;">IP</th>
                        	<th width="10%" style="text-align: center; vertical-align: middle;">시스템</th>
                        	<c:if test="${extrtCondbyInq.log_delimiter != 'DN'}">
	                        	<c:choose>
	                        		<c:when test="${extrtCondbyInq.rule_view_type == 'R'}">
	                        			<th width="15%" style="text-align: center; vertical-align: middle;">개인정보유형</th>
	                        		</c:when>
	                        		<c:otherwise>
	                        			<th width="15%" style="text-align: center; vertical-align: middle;">개인정보유형</th>
	                        		</c:otherwise>
	                        	</c:choose>
	<%--                         	<c:if test="${extrtCondbyInq.rule_view_type == 'R'}"> --%>
	<%--                         	<th width="10%" style="text-align: center; vertical-align: middle;">개인정보유형</th></c:if> --%>
	                        	<!-- <th width="10%" style="text-align: center; vertical-align: middle;">시간</th> -->
	                        	<th width="5%" style="text-align: center; vertical-align: middle;">접근 행위</th>
	                        	<th width="10%" style="text-align: center; vertical-align: middle;">접근 경로</th>
                        	</c:if>
                        	<c:if test="${extrtCondbyInq.rule_result_type == 'N'}">
                        		<th width="10%" style="text-align: center; vertical-align: middle;">취급자ID</th>
                        	</c:if>
                        	<th width="10%" style="text-align: center; vertical-align: middle;">상세정보</th>
                        </tr>
                    </thead>
                    <tbody>
                    <c:set value="${(search.page_num-1) * search.size + 1}" var="no"/>
                    <c:choose>
                   	<c:when test="${empty detailList }">
                   		<td colspan="8" style="text-align:center;">
                   			해당 데이터가 없습니다.
                   		</td>
                   	</c:when>
                   	<c:otherwise>
                    <c:forEach items="${detailList }" var="dl" varStatus="st">
                        <tr>
                        	<td style="text-align: center; vertical-align: middle;">${no}</td>
                        	<td style="text-align: center; vertical-align: middle;">
                        		<fmt:parseDate value="${dl.occr_dt }" pattern="yyyyMMdd" var="d"/>
                        		<fmt:formatDate value="${d }" pattern="yyyy-MM-dd" var="dateVal"/>
                        		<fmt:parseDate value="${dl.proc_time }" pattern="HHmmss" var="t"/>
                        		<fmt:formatDate value="${t }" pattern="HH:mm:ss" var="timeVal"/>
                        		${dateVal } ${timeVal }
                        	</td>
                        	<c:if test="${extrtCondbyInq.log_delimiter == 'DN'}">
                        		<td style="text-align: center; vertical-align: middle;">${dl.file_name}</td>
                        	</c:if>
                        	<td style="text-align: center; vertical-align: middle;">${dl.ip }</td>
                        	<td style="text-align: center; vertical-align: middle;">${dl.system_name}</td>
                        	<c:if test="${extrtCondbyInq.log_delimiter != 'DN'}">
                        	<c:choose>
                        		<c:when test="${extrtCondbyInq.rule_view_type == 'R'}">
                        			<td style="text-align: center; vertical-align: middle;">${dl.result_content }</td>
                        		</c:when>
                        		<c:otherwise>
                        			<%-- <td style="text-align: center; vertical-align: middle;">${dl.result_type }</td> --%>
                        			<td>
                        			<c:if test="${dl.result_type ne null}">
												<c:set var="result_type_split"
													value="${fn:split(dl.result_type, ',')}" />
												<c:set var="result_type_string" value="" />
												<c:set var="info_cnt" value="1"/>
												<c:set var="pre" value=""/>
												
												<c:forEach items="${result_type_split}"
													var="result_type_split_value" varStatus="num">
													<c:choose>
													<c:when test="${pre ne result_type_split_value }">
														<!--  pre값이 공백이아니고 result_type_split_value과 다를경우 갯수를 표시하고
															  pre = result_type_split_value 
															  info_cnt은 1로초기화	 -->
														<c:if test="${pre ne ''}">
															${info_cnt}
															<c:set var="info_cnt" value="1"/>
														</c:if>
														<c:set var="pre" value="${result_type_split_value}"/>
														
													</c:when>
													<c:otherwise>
														<c:set var="info_cnt" value="${info_cnt+1}"/>
													</c:otherwise>
													</c:choose>
													<c:forEach items="${CACHE_RESULT_TYPE}" var="i"
														varStatus="status">
														<%-- <c:set var="result_type_string" value="${result_type_string} ${i.value}" /> --%>
														<c:if test="${i.key == pre && info_cnt == 1}">
															<img src="${rootPath }/resources/image/icon/resultType/${pre}.png" title="${CACHE_RESULT_TYPE[i.key] }"/>
														</c:if>
													</c:forEach>
													<!-- result_type_split_value 마지막 값은 비교할 다음값이 없으므로 체크해서 갯수 표시     -->
													 <c:if test="${fn:length(result_type_split) == num.index+1}">
													 	${info_cnt}
													 </c:if>
												</c:forEach>
											</c:if>
										</td>
                        		</c:otherwise>
                        	</c:choose>
												<%--
												<c:choose>
													 <c:when test="${fn:length(result_type_string) > 8}">
														<td title="${result_type_string}">
															 ${fn:split(result_type_string, " ")[0]} 외
															${fn:length(fn:split(result_type_string, " ")) - 1}개의 개인정보</td> 
															
													</c:when> 
													<c:otherwise>
														<td><ctl:nullCv nullCheck="${result_type_string}" />
														</td>
													</c:otherwise>
												</c:choose>
												
											
                        	
                         	<c:if test="${extrtCondbyInq.rule_view_type == 'R'}"> 
                        	<td style="text-align: center; vertical-align: middle;">${dl.result_content }</td></c:if> 
                        	 <td style="text-align: center; vertical-align: middle;">${timeVal }</td> --%>
                        	
                        	
                        	<td style="text-align: center; vertical-align: middle;">
                        		<c:forEach items="${CACHE_REQ_TYPE}" var="i" varStatus="status">
									<c:if test="${i.key == dl.req_type}">
										<ctl:nullCv nullCheck="${i.value}" />
									</c:if>
								</c:forEach>
                        	</td>

                        	<c:choose>
                        		<c:when test="${fn:length(dl.req_url)>30}">
                        			<td title="${dl.req_url} style="text-align: center; vertical-align: middle;">
                        				<c:out value="${fn:substring(dl.req_url,0,29)}">
                        				</c:out>...
                        			</td>
                        			</c:when>
                        		<c:otherwise>
                        			<td style="text-align: center; vertical-align: middle;">
                        				${dl.req_url}
                        		</c:otherwise>
                        	</c:choose>
                        	</c:if>
                        	<c:if test="${extrtCondbyInq.rule_result_type == 'N'}">
                        		<td style="text-align: center; vertical-align: middle;">${dl.emp_user_id }</td>
                        	</c:if>
                        	<td style="text-align: center; vertical-align: middle;">
                        		<button type="button" style="padding-top: 2px;padding-bottom: 2px; padding-right: 8px;padding-left: 8px;" class="btn blue mt-ladda-btn ladda-button btn-outline btn-circle"
                                onclick="detailResultLogView('${dl.log_seq}','${dl.occr_dt}')">
                                    <span class="ladda-label" style="font-size: 12px;">상세보기</span>
                                <span class="ladda-spinner"></span></button>
                        	</td>
                        </tr>
                        <c:set value="${no+1}" var="no"/>
                    </c:forEach>
                  	</c:otherwise>
                    </c:choose>
                    </tbody>
                </table>
				</div>
				
				<!-- 페이징 영역 -->
                <c:if test="${search.total_count > 0}">
                    <div id="pagingframe" align="center">
                        <p><ctl:paginator currentPage="${search.page_num}" rowBlockCount="${search.size}" totalRowCount="${search.total_count}" pageName="summonDetail" /></p>
                    </div>
                </c:if>
			
			<c:choose>
				<c:when test="${ui_type == 'Shcd'}"></c:when>
				<c:otherwise>
					<div class="portlet-title">
				 <div class="caption">
					 <i class="icon-settings font-dark"></i>
					 <span class="caption-subject font-dark sbold uppercase">소명요청 내용</span>
				 </div>
			</div>
			 <div class="portlet-body">
			 	<div class="row">
					 <div class="col-md-12">
						 <table id="user" class="table table-bordered table-striped">
							 <tbody>
								<tr>
								 <tr>
									 <td style="width:5%;text-align: left;">내&nbsp;&nbsp;&nbsp;용</td>
								 </tr>
								<tr>
									<td style="vertical-align: middle;" class="form-group form-md-line-input">
									<textarea class="form-control" name="msg" rows="7" cols="40"></textarea>
									<div class="form-control-focus"></div>
								</td>
								</tr>
							 </tbody>
						 </table>
					 </div>
				 </div>
			 </div>
				</c:otherwise>
			</c:choose>				
			
			
			
			<!-- 버튼 영역 -->
			<div align="right">
				<p>
					<c:if test="${summon_alarm_type==2}">
						<label for="sms_label">
							sms 발송여부
						</label>
						<input type="checkbox" name="sms_check" id="sms_label">
					</c:if>
					<c:if test="${search.bbs_id eq 'summon' or search.bbs_id eq 'extrtCondbyInq' or empty search.bbs_id}">
						<a class="btn btn-sm dark btn-outline sbold uppercase" onclick="goSummonList(${search.cur_num});"><i class="fa fa-list"></i> 목록</a>
					</c:if>
					<c:if test="${search.bbs_id eq 'empDetailInq'}">
						<a class="btn btn-sm dark btn-outline sbold uppercase" onclick="goEmpDetailInqList();"><i class="fa fa-list"></i> 목록</a>
					</c:if>
					<c:if test="${search.bbs_id eq 'callingDemand'}">
						<a class="btn btn-sm dark btn-outline sbold uppercase" onclick="goCallingDemandList();"><i class="fa fa-list"></i> 목록</a>
					</c:if>
					<c:if test="${summon_alarm_type==1 && summon_alarm_yn=='Y'}">
					<input type="hidden" name="email_check_val" id="email_check_val"/>
						<button class="btn btn-sm btn-default btn-outline sbold uppercase" onclick="summonEmailAlramUnused();" type="button">
							 <i class="fa fa-square" id="checkFont"></i>
							소명알림 미사용 적용
						</button>
					</c:if>
					<button type="button" class="btn btn-sm red-mint btn-outline sbold uppercase" onclick="summonRemove()"><i class="fa fa-remove"></i>&nbsp;삭제</button>
					<button type="button" class="btn btn-sm blue btn-outline sbold uppercase" onclick="addSummon()"><i class="fa fa-check"></i> 소명요청</button>
				</p>
			</div>
				
				<!-- 메뉴 관련 input 시작 -->
				<input type="hidden" name="main_menu_id" value="${search.main_menu_id }" />
				<input type="hidden" name="sub_menu_id" value="${search.sub_menu_id }" />
				<input type="hidden" name="current_menu_id" value="${currentMenuId }" />
				<!-- 메뉴 관련 input 끝 -->
				
				<!-- 페이지 번호 -->
				<input type="hidden" name="page_num" value="${search.page_num }" />
				<input type="hidden" name="cur_num" value="${search.cur_num }" />
				<input type="hidden" name="empDetailInqDetail_page_num" value="${search.empDetailInqDetail.page_num }" />
				<input type="hidden" name="extrtCondbyInqDetail_page_num" value="${search.extrtCondbyInqDetail.page_num }" />
				
				<!-- 검색조건 관련 input 시작 -->
				<input type="hidden" name="search_from" value="${search.search_from }" />
				<input type="hidden" name="search_to" value="${search.search_to }" />
				<input type="hidden" name="emp_user_id" value="${search.emp_user_id }" />
				<input type="hidden" name="emp_user_name" value="${search.emp_user_name}" />
				<input type="hidden" name="insert_user_id" value="${summon.emp_user_id }" />
				<input type="hidden" name="dept_name" value="${search.dept_name}" />
				<input type="hidden" name="rule_cd" value="${search.rule_cd }" />
				<input type="hidden" name="daySelect" value="${search.daySelect }" />
				<input type="hidden" name="admin_user_id" value="${userSession.admin_user_id}" />
				<input type="hidden" name="admin_user_name" value="${userSession.admin_user_name}" />
				<input type="hidden" name="occr_dt" value="${summon.occr_dt}" />
				<input type="hidden" name="rule_nm" value="${summon.rule_nm }" />
				<input type="hidden" name="system_seq" value="${search.system_seq }" />
				<input type="hidden" name="system_name" value="${summon.system_name }" />
				<!-- 검색조건 관련 input 끝 -->
				
				<!-- 상세조건 input시작 -->
				<input type="hidden" name="dng_val" value="${search.dng_val }" />
				<input type="hidden" name="detailProcTime" value=""/>
				<input type="hidden" name="detailStartDay" value="${search.detailStartDay }"/>
				<input type="hidden" name="detailEndDay" value="${search.detailEndDay }"/>
				<input type="hidden" name="bbs_id" value="${search.bbs_id }"/>
				<input type="hidden" id="check_type" name="check_type" value="${search.check_type}" />
				<input type="hidden" id="sort_flag" name="sort_flag" value="${search.sort_flag}" />
				<input type="hidden" id="rootPath" name="rootPath" value="${rootPath}" />
				<input type="hidden" name="logSeqs" value="" />
				<input type="hidden" name="seq" value="" />
				<input type="hidden" name="user_name" value="" />
				<input type="hidden" name="result_type" value="" />
				<input type="hidden" id="scen_seq" name="scen_seq" value="${search.scen_seq}" />
				<input type="hidden" name="rule_cd_temp" value="${summon.rule_cd}" />
				<input type="hidden" name="userId" value="" />
				<input type="hidden" name="log_delimiter" value="${extrtCondbyInq.log_delimiter }" />
				<!-- 상세조건 input끝 -->
				
				<!-- 소명 요청 -->
				<input type="hidden" name="detailOccrDt" value="${search.detailOccrDt}"/>
				<input type="hidden" name="detail_emp_user_id" value="${search.detailEmpCd }"/>
				<input type="hidden" name="detail_system_seq" value="${extrtCondbyInq.system_seq }"/>
				<input type="hidden" name="emp_detail_seq" id="emp_detail_seq" value="${search.emp_detail_seq}"/>
				<input type="hidden" name="detailEmpDetailSeq" value="${search.emp_detail_seq}"/>
				
		</div>
	</div>
</div>
</form>
<form id="apprForm" name="apprForm" method="post" target="ProtoType">
</form>

<form id="moveForm" method="post">
<input type="hidden" name="detailProcDate" value=""/>
<input type="hidden" name="detailLogSeq" value=""/>
</form>

<script type="text/javascript">
	var ui_type = '${ui_type}';


	var summonConfig = {
		"listUrl":"${rootPath}/extrtCondbyInq/summonDetail.html"
		,"goSummonListUrl":"${rootPath}/extrtCondbyInq/summonList.html"
		,"goEmpDetailInqListUrl":"${rootPath}/empDetailInq/summonDetail.html"
		,"goCallingDemandListUrl":"${rootPath}/callingDemand/list.html"
		,"detailUrl":"${rootPath}/extrtCondbyInq/detail.html"
		,"detailChartUrl" :"${rootPath}/extrtCondbyInq/detailChart.html"
		,"addsummonUrl":"${rootPath}/extrtCondbyInq/summonadd.html"
		,"downloadUrl" :"${rootPath}/extrtCondbyInq/summonDetailExcel.html"
	};
		
	var allLogInqDetailConfig = {
        "detailUrl":"${rootPath}/allLogInq/detail.html"
        ,"downloadDetailUrl":"${rootPath}/downloadLogInq/detail.html"
    };
	
	var summonManageConfig = {
		"listUrl":"${rootPath}/extrtCondbyInq/summonList.html"
		,"detailUrl" : "${rootPath}/extrtCondbyInq/summonDetail.html"
	}
	
	$("#table1").dataTable( {
		scrollY: 310,
		//"scrollX": screen.width * 0.92,
		//"scrollCollapse": true,
		deferRender: false,
		scroller: true,
		searching: false,
		info: false,
		ordering: false
	});
	
	
</script>