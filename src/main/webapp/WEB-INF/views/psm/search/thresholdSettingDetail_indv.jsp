<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="extrtBaseSetupAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/search/thresholdSetting_indv.js"
	type="text/javascript" charset="UTF-8"></script>
<h1 class="page-title">${currentMenuName}</h1>
<!-- modal -->
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog" style="position: relative ;top: 30%; left: 0;">
		<!-- Modal content -->
		<div class="modal-content" style="width: 70%; margin-left:15%; margin-right:15%;">
			<div class="modal-header" style="background-color: #32c5d2;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">
					<b style="font-size: 13pt;">임계치일괄적용</b>
				</h4>
			</div>
			<div class="modal-body" style="background-color: #F9FFFF;">
					<div>
						<input type="number" name="thresholdAll" class="form-control">
					</div>
			</div>
			<div class="modal-footer">
				<a class="btn btn-sm blue" onclick="thresholdAll()" data-dismiss="modal">적용 </a>
				<a class="btn btn-sm red" href="#" data-dismiss="modal">닫기 </a>
			</div>
		</div>
	</div>
</div>

<div class="portlet light portlet-fit bordered">
	<div class="portlet-body">
		<!-- BEGIN FORM-->
		<form id="detailForm" action="" method="POST" class="form-horizontal form-bordered form-row-stripped">
		<table class="table table-bordered table-striped">
			<input type="hidden" name="current_menu_id" id="current_menu_id" value="${index_id }" />
			<th width="20%" style="text-align: center; vertical-align: middle;">시나리오명</th>
			<td width="80%">
			<c:choose>
				<c:when test="${parameters.rule_seq == null || parameters.rule_seq ==''}">
					<select name="rule_seq" class="form-control">
					<c:forEach items="${ruleList }" var="rl">
						<option value="${rl.rule_seq}">${rl.scen_name} - ${rl.rule_nm }</option>
					</c:forEach>
					</select>
				</c:when>
				<c:otherwise>
					${ruleList[0].scen_name } - ${ruleList[0].rule_nm }
					<input type="hidden" name="rule_seq" value="${ruleList[0].rule_seq }">
				</c:otherwise>
			</c:choose>
			</td>
		</table>
		
		<!-- 버튼 영역 -->
		<div class="form-actions">
			<div class="row">
				<div class="col-md-offset-2 col-md-10" align="right" style="padding-right: 30px;">
					<button type="button" class="btn btn-sm grey-mint btn-outline sbold uppercase"
						onclick="moveList();">
						<i class="fa fa-list"></i> &nbsp;목록
					</button>
					<a data-toggle="modal" data-target="#myModal" class="btn btn-sm blue btn-outline sbold uppercase">임계치일괄적용</a>
					<c:choose>
						<c:when test="${parameters.rule_seq == null || parameters.rule_seq ==''}">
							<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="addThresholdUser('add')"><i class="fa fa-check"></i>&nbsp;저장</a>
						</c:when>
						<c:otherwise>
							<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="addThresholdUser('update')"><i class="fa fa-check"></i>&nbsp;저장</a>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
			<table class="table table-bordered table-striped" style="margin-top:20px;">
				<thead>
					<th style="width: 30%; text-align: center;">취급자명</th>
					<th style="width: 30%; text-align: center;">취급자ID</th>
					<th style="width: 20%; text-align: center;">시스템</th>
					<th style="width: 20%; text-align: center;">임계치</th>
				</thead>
				<tbody>
					<c:forEach items="${thresholdList }" var="tl" varStatus="st">
					<tr>
						<td style="text-align: center; vertical-align: middle;">${tl.emp_user_name }</td>
						<td style="text-align: center; vertical-align: middle;">${tl.emp_user_id }
							<input type="hidden" name="emp_user_id" value="${tl.emp_user_id }" id="emp_user_id${st.index }">
						</td>
						<td style="text-align: center; vertical-align: middle;">${tl.system_name }</td>
							<input type="hidden" name="system_seq" value="${tl.system_seq }" id="system_seq${st.index }">
						<td style="text-align: left; vertical-align: middle;">
							<c:choose>
								<c:when test="${tl.threshold == 0}"><input type="number" name="threshold" value="" id="threshold${st.index }"></c:when>
								<c:otherwise>
									<input type="number" style="width: 65%;" name="threshold" value="${tl.threshold }" id="threshold${st.index }">
									<a class="btn btn-sm red btn-outline sbold uppercase" onclick="deleteThresholdUser('${tl.emp_user_id }', '${tl.system_seq }')">삭제</a>
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
					</c:forEach>
				</tbody>
			</table>
		</form>
	</div>
</div>
<script type="text/javascript">
	var selectConfig = {
		"listUrl" : "${rootPath}/extrtBaseSetup/thresholdSetting_indv.html"
	};
</script>