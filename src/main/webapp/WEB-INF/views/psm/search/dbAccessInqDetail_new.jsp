<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/search/dbAccessInqList.js" type="text/javascript" charset="UTF-8"></script>

<h1 class="page-title">
	${currentMenuName} 
</h1>
<div class="row">
	
<div class="portlet light portlet-fit bordered">
	
     <div class="portlet-title">
         <div class="caption">
             <i class="icon-settings font-dark"></i>
             <span class="caption-subject font-dark sbold uppercase">로그정보</span>
         </div>
     </div>
     <div class="portlet-body">
         <div class="row">
             <div class="col-md-12">
                 <table id="user" class="table table-bordered table-striped">
                     <tbody>
                         <tr>
                             <th style="width:15%;text-align: center;">일시 </th>
                             <c:if test="${dbAccessInq.proc_date ne null and dbAccessInq.proc_time ne null}">
								<td style="width:35%;text-align: center;">
									<fmt:parseDate value="${dbAccessInq.proc_date}" pattern="yyyyMMdd" var="proc_date" /> 
									<fmt:formatDate value="${proc_date}" pattern="YY-MM-dd" />
									<fmt:parseDate value="${dbAccessInq.proc_time}" pattern="HHmmss" var="proc_time" /> 
									<fmt:formatDate value="${proc_time}" pattern="HH:mm:ss" />
								</td>
							</c:if>
							<c:if test="${dbAccessInq.proc_date eq null or dbAccessInq.proc_time eq null}"><td style="width:35%">-</td></c:if>
                             <th style="width:15%;text-align: center;"> 사용자ID </th>
                             <td style="width:35%;text-align: center;">
                                 <ctl:nullCv nullCheck="${dbAccessInq.user_id}"/>
                             </td>
                         </tr>
                         <tr>
                             <th style="text-align: center;"> 사용자명 </th>
                             <td style="text-align: center;">
                                 <ctl:nullCv nullCheck="${dbAccessInq.emp_user_name}"/>
                             </td>
                             <th style="text-align: center;"> 소속 </th>
                             <td style="text-align: center;">
                                 <ctl:nullCv nullCheck="${dbAccessInq.dept_name}"/>
                             </td>
                         </tr>
                         <tr>
                       		<th style="text-align: center;"> 사용자IP </th>
                             <td style="text-align: center;">
                                 <ctl:nullCv nullCheck="${dbAccessInq.user_ip}"/>
                             </td>
                             <th style="text-align: center;"> 시스템명 </th>
                             <td style="text-align: center;">
                                 <ctl:nullCv nullCheck="${dbAccessInq.system_name}"/>
                             </td>
                         </tr>
                     </tbody>
                 </table>
             </div>
         </div>
     </div>
     <div class="portlet-title">
         <div class="caption">
             <i class="icon-settings font-dark"></i>
             <span class="caption-subject font-dark sbold uppercase">SQL정보</span>
         </div>
     </div>
     <div class="portlet-body">
         <div class="row">
             <div class="col-md-12">
                 <table id="user" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer">
                     <tbody>
                         <tr>
                         	 <th style="width:30%;text-align: center;">발생일시 </th>
                             <th style="width:70%;text-align: center;">REQUEST SQL </th>
                         </tr>
                         
                         <c:choose>
							<c:when test="${empty dbAccessInqSqlList}">
								<tr>
					        		<td colspan="2" align="center">데이터가 없습니다.</td>
					        	</tr>
							</c:when>
							<c:otherwise>
								<c:forEach items="${dbAccessInqSqlList}" var="dbAccessInqSqlList"  varStatus="status">
									<tr>
										<c:if test="${dbAccessInqSqlList.proc_date ne null and dbAccessInqSqlList.proc_time ne null}">
											<td>
												<fmt:parseDate value="${dbAccessInqSqlList.proc_date}" pattern="yyyyMMdd" var="proc_date" /> 
												<fmt:formatDate value="${proc_date}" pattern="YY-MM-dd" />
												<fmt:parseDate value="${dbAccessInqSqlList.proc_time}" pattern="HHmmss" var="proc_time" /> 
												<fmt:formatDate value="${proc_time}" pattern="HH:mm:ss" />
											</td>
										</c:if>
										<c:if test="${dbAccessInqSqlList.proc_date eq null or dbAccessInqSqlList.proc_time eq null}"><td>-</td></c:if>
										<td><ctl:nullCv nullCheck="${dbAccessInqSqlList.reqsql}"/></td>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
                     </tbody>
                 </table>
                 
                 <form id="listForm" method="POST">
					<!-- 메뉴 관련 input 시작 -->
					<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id}" />
					<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id}" />
					<input type="hidden" name="current_menu_id" value="${currentMenuId }" />
					<!-- 메뉴 관련 input 끝 -->
					
					<!-- 페이지 번호 -->
					<input type="hidden" name="page_num" value="${search.page_num}" />
					
					<!-- 검색조건 관련 input 시작 -->
					<input type="hidden" name="search_from" value="${search.search_from}" />
					<input type="hidden" name="search_to" value="${search.search_to}" />
					<input type="hidden" name="daySelect" value="${search.daySelect}" />
				</form>
             </div>
             
         </div>
     </div>
	<!-- 
	<div class="portlet-title">
         <div class="caption">
             <i class="icon-settings font-dark"></i>
             <span class="caption-subject font-dark sbold uppercase">개인정보</span>
         </div>
     </div>
      -->
     <div class="portlet-body">
         <div class="row">
             <div class="col-md-12">
             	<%-- 
                 <table id="user" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer">
                     <tbody>
                         <tr>
                         	 <th style="width:10%;text-align: center;">NO. </th>
                             <th style="width:40%;text-align: center;">개인정보유형 </th>
                             <th style="width:50%;text-align: center;">개인정보내용 </th>
                         </tr>
                         
                         <c:choose>
							<c:when test="${empty dbAccessInqResultList}">
								<tr>
					        		<td colspan="3" align="center">데이터가 없습니다.</td>
					        	</tr>
							</c:when>
							<c:otherwise>
								<c:forEach items="${dbAccessInqResultList}" var="dbAccessInqResultList"  varStatus="status">
									<tr>
										<td style="text-align: center;">${status.count }</td>
										<td style="text-align: center;"><ctl:nullCv nullCheck="${dbAccessInqResultList.result_type}"/></td>
										<td style="text-align: center;"><ctl:nullCv nullCheck="${dbAccessInqResultList.result_content}"/></td>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
                     </tbody>
                 </table>
                  --%>
                 <form id="listForm" method="POST">
					<!-- 메뉴 관련 input 시작 -->
					<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id}" />
					<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id}" />
					<input type="hidden" name="current_menu_id" value="${currentMenuId }" />
					<!-- 메뉴 관련 input 끝 -->
					
					<!-- 페이지 번호 -->
					<input type="hidden" name="page_num" value="${search.page_num}" />
					
					<!-- 검색조건 관련 input 시작 -->
					<input type="hidden" name="search_from" value="${search.search_from}" />
					<input type="hidden" name="search_to" value="${search.search_to}" />
					<input type="hidden" name="daySelect" value="${search.daySelect}" />
				</form>
             </div>
             
         </div>
     </div>
     
	<div class="row">
		<div class="col-md-12" align="right">
			<!-- 버튼 영역 -->
			<div class="option_btn right" style="padding-right: 10px;">
				<p class="right">
					<a class="btn btn-sm dark btn-outline sbold uppercase"
						onclick="javascript:goList();"><i class="fa fa-list"></i> 목록</a>
				</p>
			</div>
		</div>
	</div>

	</div>
</div>

<script type="text/javascript">

var dbAccessInqConfig = {
		"listUrl":"${rootPath}/dbAccessInq/list.html"
	};
</script>
