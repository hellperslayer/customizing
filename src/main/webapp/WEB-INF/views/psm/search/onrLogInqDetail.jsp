<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/search/onrLogInqList.js" type="text/javascript" charset="UTF-8"></script>
<link href="${rootPath}/resources/css/profile.min.css" rel="stylesheet" type="text/css">


<h1 class="page-title"> ${currentMenuName}
    <!--bold font-blue-hoki <small>basic bootstrap tables with various options and styles</small> -->
</h1>

<div class="contents left">
 		

<div class="row" style="background:#eef1f5; padding-top: 20px; padding-bottom: 20px">
	<input type="hidden" id="emp_user_id" name="emp_user_id" value="${allLogInq.emp_user_id}">
	<div class="col-md-12">
		<!-- BEGIN PROFILE SIDEBAR -->
		<div class="profile-sidebar">
			<!-- PORTLET MAIN -->
			<div class="portlet light profile-sidebar-portlet "style=height:147%">
				<!-- SIDEBAR USERPIC -->
				<div class="profile-userpic" style="margin-bottom: -5px; ">
					<img src="${rootPath}/resources/image/common/profile_user2.png" class="img-responsive" alt="profile">
				</div>
				<!-- END SIDEBAR USERPIC -->
				<!-- SIDEBAR USER TITLE -->
				<div class="profile-usertitle">
					<div class="profile-usertitle-name"style="margin-bottom: 10px; ">${onrLogInq.emp_user_name}</div>
					<%-- <div class="profile-usertitle-job"style="margin-bottom: 15px; ">(${data1.emp_user_id})</div> --%>
				</div>
				<!-- END SIDEBAR USER TITLE -->
				<!-- SIDEBAR BUTTONS -->
			</div>
			<!-- END PORTLET MAIN -->
			<!-- PORTLET MAIN -->
			<div class="portlet light" style="height: 550px;">
				<!-- STAT -->
				<div class="row">
					<h4 class="profile-desc-title" style="padding-bottom: 10px; ">프로필</h4>
					<div class="margin-top-20 profile-desc-link" style="margin-top: -20px !important;">
						<table class="table table-hover table-light">
	                        <tbody>
	                            <tr>
	                                <td> <b>소속</b> :
	                                <c:choose>
	                                	<c:when test="${masterflag eq 'Y' }"> 
	                                	<a onclick="fnExtrtDeptDetailInfo('${onrLogInq.dept_id }', '${onrLogInq.proc_date }')">${onrLogInq.dept_name}</a>
	                                	</c:when>
	                                	<c:otherwise>
	                                	${onrLogInq.dept_name}
	                                	</c:otherwise>
	                                </c:choose>
	                                </td>
	                            </tr>
	                            <tr>
	                                <td> <b>IP</b> : ${onrLogInq.user_ip}</td>
	                            </tr> 
	                            <tr>
	                            	<td> <b>ID</b> : ${onrLogInq.emp_user_id} </td>
	                            </tr>
	                        </tbody>
                        </table>
					</div>
				</div>
				<div class="row list-separated profile-stat">
				  <h4 class="profile-desc-title">업무 시스템</h4>
					<div class="tab-context">
						<div  class="clearfix">
							<c:forEach items="${data2}" var="system" varStatus="status">
								<c:if test="${system.system_name != null}">
									<a style="cursor:default" class="btn btn-xs blue"
										<%-- <c:if test="${status.count % 4 == 0 }"> class="btn btn-xs red"</c:if>
										<c:if test="${status.count % 4 == 1 }"> class="btn btn-xs yellow"</c:if>
										<c:if test="${status.count % 4 == 2 }"> class="btn btn-xs green"</c:if>
										<c:if test="${status.count % 4 == 3 }"> class="btn btn-xs blue"</c:if>			 --%>				
										style="margin-top: 5px; margin-bottom: 5px;">${system.system_name}
										<i class="fa fa-laptop"></i>
			                       	 </a>
								</c:if>								
							</c:forEach>
	                    </div>
                    </div>
				</div>
				<!-- END STAT -->
				<div class="row" style="margin-top: -15px !important;">					
					<%-- <br><br><div class="margin-top-20 profile-desc-link" style="margin-top: 2px !important;t">
						
						<c:if test="${data1.dng_grade == '심각' }"><i class="fa fa-globe"></i><span style="font-size: 15px;"><b style="color: red;">심각 단계</b>는 <b><br>위험도 높은 비정상 개인정보 처리가 다수 발생되어 전사 차원의 매우 심각한 수준의 손실이나 해당 업무의 실패 혹은 경쟁적 지위를 상실할 수 있는 단계입니다.</b></span> </c:if>
						<c:if test="${data1.dng_grade == '경계' }"><i class="fa fa-globe"></i><span style="font-size: 15px;"><b style="color: orange;">경계 단계</b>는 <b><br>다수의 비정상 개인정보 처리가 발생되어 매우 심각한 손실이나 해당 업무가 어려워질 수 있는 정도로 업무에 중대한 영향이 발생할 수 있는 단계입니다.</b></span></c:if>
						<c:if test="${data1.dng_grade == '주의' }"><i class="fa fa-globe"></i><span style="font-size: 15px;"><b style="color: yellow;">주의 단계</b>는 <b><br>여러 항목의 비정상 개인정보 처리가 발생되어 손실이나 해당 업무에 부정적인 영향을 줄 수 있는 정도의 문제가 발생할 수 있는 단계입니다. </b></span></c:if>
						<c:if test="${data1.dng_grade == '관심' }"><i class="fa fa-globe"></i><span style="font-size: 15px;"><b style="color: blue;">관심 단계</b>는 <b><br>일부 비정상 개인정보 처리가 발생되어 해당영역 또는 해당서비스에 작은 영향을 줄 수 있을 정도의 문제가 발생할 수 있는 단계입니다. </b></span></c:if>
						<c:if test="${data1.dng_grade == '정상' }"><i class="fa fa-globe"></i><span style="font-size: 15px;"><b style="color: green;">정상 단계</b>는 <b><br>업무 수행 상 허용된 범위 내에 개인정보 처리가 발생되어 업무적으로 영향이 거의 없는 정도로 파급효과가 미약한 단계입니다.</b></span></c:if>
					</div> --%>
				</div>
			</div>
			<!-- END PORTLET MAIN -->
		</div>
		<!-- END BEGIN PROFILE SIDEBAR -->
		<!-- BEGIN PROFILE CONTENT -->
		<div class="profile-content">
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light"  style="min-height: 800px;">
						<!-- <div class="portlet-title tabbable-line">
							<div class="caption caption-md">
								<i class="icon-globe theme-font hide"></i> <span class="caption-subject font-blue-madison bold uppercase">Profile
									Account</span>
							</div>
						</div> -->
						<div class="portlet-title">
					        <div class="caption">
					            <i class="icon-settings font-dark"></i>
					            <span class="caption-subject font-dark sbold uppercase">로그정보</span>
					        </div>
					    </div>
						<div class="portlet-body">
							<table id="user" class="table table-bordered table-striped">
		                     <tbody>
		                         <tr>
		                             <th style="width:15%;text-align: center;">일시 </th>
		                             <c:if test="${onrLogInq.proc_date ne null and onrLogInq.proc_time ne null}">
										<td style="width:35%;text-align: center;">
											<fmt:parseDate value="${onrLogInq.proc_date}" pattern="yyyyMMdd" var="proc_date" /> 
											<fmt:formatDate value="${proc_date}" pattern="YY-MM-dd" />
											<fmt:parseDate value="${onrLogInq.proc_time}" pattern="HHmmss" var="proc_time" /> 
											<fmt:formatDate value="${proc_time}" pattern="HH:mm:ss" />
										</td>
									</c:if>
									<th style="width:15%;text-align: center;"> 시스템명 </th>
		                             <td style="width:35%;text-align: center;">
		                                 <ctl:nullCv nullCheck="${onrLogInq.system_name}"/>
		                             </td>
		                         </tr>
	                             <tr>
		                             <th style="text-align: center;"> 개인정보유형 </th>
		                             <td style="text-align: center;">
		                                 <%-- <ctl:nullCv nullCheck="${allLogInq.result_type}"/> --%>
		                                 <c:set var="prev" value=""/>
										<c:set var="cnt1" value="1"/>
										<c:set var="total" value="0"/>
										<c:forEach items="${onrLogInq.result_type}">
											<c:set var="total" value="${total + 1 }"/>
										</c:forEach>
										<c:forEach items="${onrLogInq.result_type}" var="item" varStatus="status">
											<c:choose>
												<c:when test="${prev eq item}">	<!-- 이전값과 같을 때 -->
													<c:set var="cnt1" value="${cnt1 + 1 }"/>
													<c:if test="${status.count == total }">${cnt1 }</c:if>
												</c:when>
												<c:otherwise> 					<!-- 이전값과 다를 때 -->
													<c:if test="${status.count != 1}">${cnt1 }</c:if>
													<c:forEach items="${CACHE_RESULT_TYPE}" var="i">
														<c:if test="${i.key == item }">
															<img src="${rootPath }/resources/image/icon/resultType/${i.key}.png" title="${i.value }"/>
														</c:if>
													</c:forEach>
													
													<c:set var="prev" value="${item }"/>
													<c:set var="cnt1" value="1"/>
													<c:if test="${status.count == total }">${cnt1 }</c:if>
												</c:otherwise>
											</c:choose>
										</c:forEach>
		                             </td>
		                             <th style="text-align: center;"> 수행업무 </th>
		                             <td style="text-align: center;">
		                                 <c:forEach items="${CACHE_REQ_TYPE}" var="i" varStatus="status">
											<c:if test="${i.key == onrLogInq.req_type}">
												<ctl:nullCv nullCheck="${i.value}"/>
											</c:if>
										</c:forEach>
		                             </td>
		                         </tr>
		                         <tr>
		                         	<th style="text-align: center;vertical-align: middle;"> 접근 경로 </th>
		                         	<c:choose>
										<c:when test="${onrLogInq.req_url eq null}">
											<td colspan="3" style="text-align: center;">-</td>
										</c:when>
										<c:otherwise>
											<td colspan="3" style="text-align: left;"><TEXTAREA rows="3" style="width: 100%;" readonly="readonly">${onrLogInq.req_url}</TEXTAREA></td>												
										</c:otherwise>
									 </c:choose>
		                         </tr> 
		                         <tr>
		                         	<th style="text-align: center;"> 결제라인 </th>
		                         	<td colspan="3" style="text-align: center;">-</td>
		                         </tr> 
		                         <tr>
		                         	<th style="text-align: center;"> 메뉴 </th>
		                         	<td colspan="3" style="text-align: center;">-</td>
		                         </tr>   
		                         <!-- <tr>
		                         	<th style="text-align: center;"> 문서명 </th>
		                         	<td colspan="3" style="text-align: center;"></td>
		                         </tr>   
		                         <tr>
		                         	<th style="text-align: center;"> 문서유형 </th>
		                         	<td colspan="3" style="text-align: center;"></td>
		                         </tr>  -->     
		                     </tbody>
		                 </table>
						</div>
						
						<div class="portlet-title">
					        <div class="caption">
					            <i class="icon-settings font-dark"></i>
					            <span class="caption-subject font-dark sbold uppercase">파일정보</span>
					        </div>
					    </div>
					    <div class="portlet-body">
						    <div class="row">
						    	<div class="col-md-12">
						    		<table id="user" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer">
						    			<tbody>
					                         <tr>
					                         	 <th style="width:10%;text-align: center;">No. </th>
					                             <th style="width:60%;text-align: center;">문서명</th>
					                             <th style="width:15%;text-align: center;">문서유형</th>
					                             <th style="width:15%;text-align: center;">구분</th>
					                         </tr>
					                         <c:choose>
					                         	<c:when test="${empty onrLogInqFileList}">
					                         		<td colspan="4" align="center">데이터가 없습니다.</td>
					                         	</c:when>
					                         	<c:otherwise>
					                         		<c:forEach items="${onrLogInqFileList }" var="file" varStatus="status">
							                         	<tr>
							                         		<td align="center">${status.count }</td>
							                         		<td align="left" style="margin-left: 10px">${file.file_name }</td>
							                         		<td align="center">${file.file_ext }</td>
							                         		<td align="center">
							                         			<c:choose>
							                         				<c:when test="${file.file_direction eq 'UP' }">업로드</c:when>
							                         				<c:otherwise>다운로드</c:otherwise>
							                         			</c:choose>
							                         		</td>
							                         	</tr>
							                         </c:forEach>
					                         	</c:otherwise>
					                         </c:choose>
					                    </tbody>
						    		</table>
						    	</div>
						    </div>
					    </div>
					    
					    <div class="portlet-title">
					        <div class="caption">
					            <i class="icon-settings font-dark"></i>
					            <span class="caption-subject font-dark sbold uppercase">개인정보</span>
					        </div>
					    </div>
					    
					    <div class="portlet-body">
				         <div class="row">
				             <div class="col-md-12">
				                 <table id="user" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer">
				                     <tbody>
				                         <tr>
				                         	 <th style="width:10%;text-align: center;">No. </th>
				                             <th style="width:30%;text-align: center;">개인정보유형 </th>
				                             <th style="width:30%;text-align: center;">개인정보내용</th>
				                             <th style="width:30%;text-align: center;">탐지 예외 처리 </th>
				                         </tr>
				                         
				                         <c:choose>
											<c:when test="${empty onrLogInqDetailList}">
												<tr>
									        		<td colspan="8" align="center">데이터가 없습니다.</td>
									        	</tr>
											</c:when>
											<c:otherwise>
												<c:set value="${search.allLogInqDetail.total_count}" var="count"/>
												<c:forEach items="${onrLogInqDetailList}" var="onrLogInqDetailList"  varStatus="status">
													<tr>
														<td align="center">${count - search.allLogInqDetail.page_num * search.allLogInqDetail.size + search.allLogInqDetail.size }</td>
														<td align="center"<c:if test="${onrLogInqDetailList.check_exc eq 1}">style="color:lightgray;"</c:if>>
															<ctl:nullCv nullCheck="${onrLogInqDetailList.result_type}"/>&nbsp;
															<c:choose>
																<c:when test="${onrLogInqDetailList.detection_src eq '103' }"><i class="icon-doc" style="vertical-align: middle;"></i></c:when>
																<c:otherwise><i class="icon-screen-desktop" style="vertical-align: middle;"></i></c:otherwise>
															</c:choose>
														</td>
														<td align="center"<c:if test="${onrLogInqDetailList.check_exc eq 1}">style="color:lightgray;"</c:if>><ctl:nullCv nullCheck="${onrLogInqDetailList.result_content_masking}"/></td>
														<c:choose>
															<c:when test="${onrLogInqDetailList.check_exc ne 1}">
																<td align="center">
																<p class="btn btn-xs green-haze" onclick="javascript:deletePrivacy(${onrLogInqDetailList.biz_log_result_seq},${onrLogInqDetailList.privacy_seq},'${onrLogInqDetailList.result_content}')">
																<i class="fa fa-remove"></i>&nbsp;&nbsp;&nbsp;제외</p></td>
															</c:when>
															<c:otherwise>
																<!-- 복원 -->
																<td align="center">
																<p class="btn btn-xs red" onclick="javascript:restorePrivacy(${onrLogInqDetailList.biz_log_result_seq},${onrLogInqDetailList.privacy_seq},'${onrLogInqDetailList.result_content}')">
																	<i class="fa fa-check"></i>&nbsp;&nbsp;&nbsp;복원</p>
																</td>
															</c:otherwise>
														</c:choose>
													</tr>
												<c:set var="count" value="${count - 1 }"/>
												</c:forEach>
												
											</c:otherwise>
										</c:choose>
				                     </tbody>
				                 </table>
				                 
				                 <form id="listForm" method="POST">
								<!-- 메뉴 관련 input 시작 -->
								<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" />
								<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
								<input type="hidden" name="current_menu_id" value="${currentMenuId }" />
								<!-- 메뉴 관련 input 끝 -->
								
								<!-- 페이지 번호 -->
								<input type="hidden" name="page_num" value="${search.page_num }" />
								<input type="hidden" name="onrLogInqDetail_page_num" value="${search.allLogInqDetail.page_num }" />
								<input type="hidden" name="extrtCondbyInqDetail_page_num" value="${search.extrtCondbyInqDetail.page_num }" />
								<input type="hidden" name="empDetailInqDetail_page_num" value="${search.empDetailInqDetail.page_num }" />
								
								<!-- 검색조건 관련 input 시작 -->
								<input type="hidden" name="search_from" value="${search.search_from }" />
								<input type="hidden" name="search_to" value="${search.search_to }" />
								<input type="hidden" name="emp_user_id" value="${search.emp_user_id }" />
								<input type="hidden" name="privacy" value="${search.privacy }" />
								<input type="hidden" name="daySelect" value="${search.daySelect }" />
								<input type="hidden" name="user_ip" value="${search.user_ip }" />
								<input type="hidden" name="privacyType" value="${search.privacyType }" />
								<input type="hidden" name="system_seq" value="${search.system_seq }" />
								<input type="hidden" name="req_url" value="${search.req_url }" />
								<c:if test="${search.isSearch eq 'Y' }">
								<input type="hidden" name="emp_user_name" value="${search.emp_user_name}" />
								</c:if>
								<input type="hidden" name="dept_name" value="${search.dept_name}" />
								<input type="hidden" name="dept_id" value="" />
								<input type="hidden" name="rule_cd" value="${search.rule_cd }" />
								<input type="hidden" name="mapping_id" value="${search.mapping_id }" />
								<input type="hidden" name="isSearch" value="${search.isSearch }" />
								<!-- 검색조건 관련 input 끝 -->
								
								<!-- 상세 검색 조건 input 시작 -->
								<input type="hidden" name="dng_val" value="${search.dng_val }"/>
								<input type="hidden" name="detailLogSeq" value="${search.detailLogSeq }"/>
								<input type="hidden" name="detailProcDate" value="${search.detailProcDate }"/>
								<input type="hidden" name="detailProcTime" value="${search.proc_time }"/>
								<input type="hidden" name="bbs_id" value="${search.bbs_id }"/>
								<input type="hidden" name="detailOccrDt" value="${search.detailOccrDt}"/>
								<input type="hidden" name="detailEmpCd" value="${search.detailEmpCd }"/>
								<input type="hidden" name="detailStartDay" value="${search.detailStartDay }"/>
								<input type="hidden" name="detailEndDay" value="${search.detailEndDay }"/>
								<input type="hidden" name="detailEmpDetailSeq" value="${search.detailEmpDetailSeq}"/>
								<input type="hidden" name="misdetect_pattern" value=""/>
								<input type="hidden" name="privacy_type" value=""/>
								<input type="hidden" name="biz_log_result_seq" value="${search.biz_log_result_seq }"/>
								<input type="hidden" name="privacy_seq" value="${search.privacy_seq }"/>
								<input type="hidden" name="result_content" value="${search.result_content }"/>
								<input type="hidden" name="result_type" value="${search.result_type}"/>
								<input type="hidden" name="user_name" value="${search.emp_user_name}"/>
								<input type="hidden" name="sort_flag" value="${search.sort_flag}"/>
								<input type="hidden" name="userId" value =""/>
								<!-- 상세 검색 조건 input 끝 -->
							</form>
				             </div>
				             
				         </div>
				         <div class="row">
				         	<div class="col-md-12" align="center">
				         		 <!-- 페이징 영역 -->
								<c:if test="${search.allLogInqDetail.total_count > 0}">
									<div id="pagingframe">
										<p><ctl:paginator currentPage="${search.allLogInqDetail.page_num}" rowBlockCount="${search.allLogInqDetail.size}" totalRowCount="${search.allLogInqDetail.total_count}" pageName="onrLogInqDetail"/></p>
									</div>
								</c:if>
				         	</div>
				         </div>
				         
				         <div class="row">
				         	<div class="col-md-12" align="right">
				         		<!-- 버튼 영역 -->
								<div class="option_btn right" style="padding-right:10px;">
									<p class="right">
										<c:if test="${masterflag eq 'Y' }">
											<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="fnExtrtEmpDetailInfo('${onrLogInq.emp_user_id}', '${onrLogInq.user_ip }', '${onrLogInq.proc_date}', '${onrLogInq.system_seq}')"><i class="fa fa-user"></i> 업무패턴보기</a>
										</c:if>
										<c:if test="${search.bbs_id eq null or empty search.bbs_id}">
											<a class="btn btn-sm grey-mint btn-outline sbold uppercase" onclick="javascript:goList();"><i class="fa fa-list"></i> 목록</a>
										</c:if>
										<c:if test="${search.bbs_id eq 'extrtCondbyInq' or search.bbs_id eq 'empDetailInq'}">
											<a class="btn btn-sm grey-mint btn-outline sbold uppercase" onclick="moveExtrtCondbyInqDetail();"><i class="fa fa-list"></i> 목록</a>
										</c:if>
										<c:if test="${search.bbs_id eq 'callingDemand'}">
											<a class="btn btn-sm grey-mint btn-outline sbold uppercase" onclick="moveExtrtCondbyInqDetail();"><i class="fa fa-list"></i> 목록</a>
										</c:if>
									</p>
								</div>
				         	</div>
				         </div>
				     </div>
					</div>
				</div>
			</div>
		</div>
		<!-- END PROFILE CONTENT -->
	</div>
</div>

</div>
<!-- END CONTENT BODY -->

	<form id="menuSearchForm" method="POST">
		<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }"/>
		<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
		<input type="hidden" name="system_seq" value="${paramBean.system_seq }" />
		<input type="hidden" name="emp_user_id" value ="${search.emp_user_id}"/>
		<input type="hidden" name="search_from" value ="${search.search_from}"/>
		<input type="hidden" name="search_to" value ="${search.search_to}"/>
		<input type="hidden" name="emp_user_name" value ="${search.emp_user_name}"/>
		<input type="hidden" name="user_ip" value ="${search.user_ip}"/>
		<input type="hidden" name="daySelect" value ="${search.daySelect}"/>
	</form>

<script type="text/javascript">

	var onrLogInqConfig = {
		"listUrl":"${rootPath}/onrLogInq/list.html"
		,"detailUrl":"${rootPath}/onrLogInq/detail.html"
		,"addUrl":"${rootPath}/allLogInq/add.html"
		,"removeUrl":"${rootPath}/allLogInq/remove.html"
		,"loginPage" : "${rootPath}/loginView.html"
		,"deptDetailChartUrl" : "${rootPath}/extrtCondbyInq/deptDetailChart.html"
		,"goMisdetectUrl" : "${rootPath}/misdetect/list.html"
	};
	var extrtCondbyInqConfig = {
		"detailChartUrl":"${rootPath}/extrtCondbyInq/detailChart.html",
		"detailUrl" : "${rootPath}/extrtCondbyInq/detail.html"
	};
	
</script>


