<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>

<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/search/readInfoList.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>

<style>
.datepicker {
	top:130px;
}
</style>


<h1 class="page-title">
	${currentMenuName}
	<!-- <small>Log-list</small> -->
</h1>
<div class="portlet light portlet-fit portlet-datatable bordered">
	
	<div class="portlet-body">
		<div class="table-container">

			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
				
				<div>
					<div class="col-md-6"
						style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
						
						<div class="portlet box grey-salt  ">
                            <div class="portlet-title" style="background-color: #2B3643;">
                                <div class="caption">
                                    <i class="fa fa-search"></i>검색</div>
                                <div class="tools">
	                            	<a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
					            </div>
                            </div>
                            <div id="searchBar" class="portlet-body form" <%-- <c:if test="${search.isSearch != 'Y' }">style="display: none;"</c:if> --%>>
                                <div class="form-body" style="padding-left:10px; padding-right:10px;">
                                    <div class="form-group">
                                        <form id="listForm" method="POST" class="mt-repeater form-horizontal">
                                            <div data-repeater-list="group-a">
                                                <div class="row">
                                                    <!-- jQuery Repeater Container -->
                                                    <div class="col-md-2">
													    <label class="control-label">기간선택</label><br/>
													    <select class="form-control" id="daySelect" name="daySelect" onclick="javascript:initDaySelect();">
															<option value="" class="daySelect_first" <c:if test='${search.daySelect eq ""}'>selected="selected"</c:if>>직접입력</option>
															<option value="Day" <c:if test='${search.daySelect eq "Day"}'>selected="selected"</c:if>>오늘</option>
															<option value="WeekDay" <c:if test='${search.daySelect eq "WeekDay"}'>selected="selected"</c:if>>일주일</option>
														</select>
													</div>
                                                    <div class="col-md-2">
                                                        <label class="control-label">기간</label>
                                                        <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
															<input type="text" class="form-control" id="search_fr" name="search_from" value="${search.search_fromWithHyphen}"> 
															<span class="input-group-addon"> &sim; </span> 
															<input type="text" class="form-control" id="search_to" name="search_to" value="${search.search_toWithHyphen}">
														</div>
													</div>
													<div class="col-md-2">
													<label class="control-label">접속유형</label> 
													<select name="mapping_id" class="form-control">
														<option value="code1" ${search.mapping_id == 'code1' ? 'selected="selected"' : ''}>ID 로그인</option>
														<option value="code2" ${search.mapping_id == null ? 'selected="selected"' : ''}>시스템 로그인</option>
													</select>
												</div>
												<div class="col-md-2">
													<label class="control-label">시스템</label> 
													<select name="system_seq" class="form-control">
														<option value="" ${search.system_seq == '' ? 'selected="selected"' : ''}>----- 전 체 -----</option>
														<c:if test="${empty systemMasterList}">
															<option>시스템 없음</option>
														</c:if>
														<c:if test="${!empty systemMasterList}">
															<c:forEach items="${systemMasterList}" var="i" varStatus="z">
																<option value="${i.system_seq}" ${i.system_seq==search.system_seq ? "selected=selected" : "" }>${ i.system_name}</option>
															</c:forEach>
														</c:if>
													</select>
												</div>
												<div class="col-md-2">
                                                   <label class="control-label">개인정보내용</label>
                                                   <br/>
                                                  <input class="form-control" name="privacy" value="${search.privacy}" />
												</div>
                                                </div>
                                            </div>
                                            <hr/>
                                            <div align="left">
                                            	<div style="color: red; float: left;">※ 대량의 데이터에서 조회 하기 때문에 시간이 다소 소요될 수 있습니다.</div>
                                            </div>
                                            <div align="right">
                                            	<button type="reset"
													class="btn btn-sm red-mint btn-outline sbold uppercase"
													onclick="resetOptions(readInfoConfig['listUrl'])">
													<i class="fa fa-remove"></i> <font>초기화
												</button>
												<button class="btn btn-sm blue btn-outline sbold uppercase"
													onclick="moveallLogInqList()">
													<i class="fa fa-search"></i> 검색
												</button>&nbsp;&nbsp;
												<%-- <a class="btn red btn-outline btn-circle"
													onclick="excelAllLogInqList('${search.total_count}')"> 
													<i class="fa fa-share"></i> <span class="hidden-xs"> 엑셀 </span>
												</a> --%>
												<%-- <a onclick="excelAllLogInqList('${search.total_count}')"><img src="${rootPath}/resources/image/icon/XLS_3.png"></a> --%>
											</div>
                                            <!-- <div align="left" style="float: left;">
                                            	<button type="button" class="btn btn-sm green btn-outline sbold uppercase"
													onclick="showResultTypeWindow()">
													<i class="fa fa-search"></i> <font>개인정보유형
												</button>
                                            </div> -->
											
											<input type="hidden" name="detailLogSeq" value="" /> 
											 
											<input type="hidden" name="main_menu_id" value="${search.main_menu_id }" /> 
											<input type="hidden" name="sub_menu_id" value="${search.sub_menu_id }" /> 
											<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
											<%-- <input type="hidden" name="system_seq" value="${search.system_seq }" /> --%> 
											<input type="hidden" name="page_num" value="${search.page_num}" /> 
											<input type="hidden" name="menuNum" id="menuNum" value="${menuNum}" />
											<input type="hidden" name="menuCh" id="menuCh" value="${menuCh}" />
											<input type="hidden" name="menuId" id="menuId" value="${search.sub_menu_id}" /> 
											<input type="hidden" name="menutitle" id="menutitle" value="${menuCh}${menuNum}" />
											<input type="hidden" name="dept_id" id="dept_id" value="${search.dept_id}" /> 
											<input type="hidden" name="user_id" value="" />
											<input type="hidden" name="isSearch" value="${search.isSearch }" />
											<input type="hidden" name="result_type"  id="result_type" value=""/>
											<input type="hidden" name="isSearch" value="${search.isSearch }" />
											
											<input type="hidden" name="detailProcDate" value="" />
											<input type="hidden" name="detailProcTime" value=""/>
											<input type="hidden" name="detailUserName" value=""/>
											<input type="hidden" name="detailResultType" value=""/>
											<input type="hidden" name="bbs_id" value=""/>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>
				</div>
				<div>
					<table style="border-top: 1px solid #e7ecf1"
						class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
						style="position: absolute; top: 0px; left: 0px; width: 100%;">

						<thead>
							<tr role="row" class="heading" style="background-color: #c0bebe;">
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									일시</th>
								<th width="8%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									소속</th>
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									취급자ID</th>
								<th width="9%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center; padding: 0em;">
									취급자명</th>
								<th width="10%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									접속IP</th>
								<th width="8%"
									style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
									수행업무</th>	
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${empty allLogInqList.allLogInq}">
									<tr>
										<td colspan="10" align="center">데이터가 없습니다.</td>
									</tr>
								</c:when>
								<c:otherwise>
									<c:set value="${allLogInqList.page_total_count}" var="count" />
									<c:forEach items="${allLogInqList.allLogInq}" var="allLogInq"
										varStatus="status1">
										<tr style='cursor: pointer;'
											<c:if test="${status1.count % 2 == 0 }"> class='tr_gray'</c:if>
											onclick="javascript:fnAllLogInqDetail('${allLogInq.log_seq}', '${allLogInq.proc_date}');">

											<c:if
												test="${allLogInq.proc_date ne null and allLogInq.proc_time ne null}">
												<td width="9%" style="text-align: center;"><fmt:parseDate
														value="${allLogInq.proc_date}" pattern="yyyyMMdd"
														var="proc_date" /> <fmt:formatDate value="${proc_date}"
														pattern="YY-MM-dd" /> <fmt:parseDate
														value="${allLogInq.proc_time}" pattern="HHmmss"
														var="proc_time" /> <fmt:formatDate value="${proc_time}"
														pattern="HH:mm:ss" /></td>
											</c:if>
											<c:if
												test="${allLogInq.proc_date eq null or allLogInq.proc_time eq null}">
												<td width="9%" style="text-align: center;">-</td>
											</c:if>
											<td width="9%" style="text-align: center;"><ctl:nullCv
													nullCheck="${allLogInq.dept_name}" /></td>
											<td width="5%" style="text-align: center;"><ctl:nullCv
													nullCheck="${allLogInq.emp_user_id}" /></td>
											<td width="6%" style="text-align: center;"><ctl:nullCv
													nullCheck="${allLogInq.emp_user_name}" /></td>
											<td width="7%" style="text-align: center;"><ctl:nullCv
													nullCheck="${allLogInq.user_ip}" /></td>
											<td style="text-align: center;">
												<c:forEach items="${CACHE_REQ_TYPE}" var="i" varStatus="status">
													<c:if test="${i.key == allLogInq.req_type}">
														<ctl:nullCv nullCheck="${i.value}" />
													</c:if>
												</c:forEach>
											</td>
										</tr>
										<c:set var="count" value="${count - 1 }" />
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
				</div>

				<form id="menuSearchForm" method="POST">
					<input type="hidden" name="main_menu_id" value="${search.main_menu_id }" /> 
					<input type="hidden" name="sub_menu_id" value="${search.sub_menu_id }" /> 
					<input type="hidden" name="system_seq" value="${search.system_seq }" />
					<input type="hidden" name="emp_user_id" value="${search.emp_user_id}" /> 
					<input type="hidden" name="search_from" value="${search.search_from}" /> 
					<input type="hidden" name="search_to" value="${search.search_to}" /> 
					<input type="hidden" name="emp_user_name" value="${search.emp_user_name}" />
					<input type="hidden" name="user_ip" value="${search.user_ip}" /> 
					<input type="hidden" name="daySelect" value="${search.daySelect}" />
				</form>

				<div class="row" style="padding: 10px;">
					<!-- <div class="col-md-8 col-sm-12">
			            <div class="dataTables_info">Showing 22 to 30 of 178 entries
			            </div>
		            </div>
		            <div class="col-md-4 col-sm-12"></div> -->
					<!-- 페이징 영역 -->
					<c:if test="${search.total_count > 0}">
						<div id="pagingframe" align="center">
							<p>
								<ctl:paginator currentPage="${search.page_num}"
									rowBlockCount="${search.size}"
									totalRowCount="${search.total_count}" />
							</p>
						</div>
					</c:if>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	/* $('#search_fr, #search_to').datepicker({
	 format: "yyyy-mm-dd",
	 language: "kr",
	 todayHighlight: true
	 });

	 $('#search_fr').datepicker()
	 .on('changeDate', function(ev){
	 var date1 = $('#search_fr').val();
	 $('#search_to').datepicker('setStartDate', date1);
	 });
	 $('#search_to').datepicker()
	 .on('changeDate', function(ev){
	 var date2 = $('#search_to').val();
	 $('#search_fr').datepicker('setEndDate', date2);
	 });  */

	 //$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-3d");
	 
	var readInfoConfig = {
		"listUrl" : "${rootPath}/allLogInq/readInfoList.html",
		"detailUrl" : "${rootPath}/allLogInq/detail.html"
	};
</script>