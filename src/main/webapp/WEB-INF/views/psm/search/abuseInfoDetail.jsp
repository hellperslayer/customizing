<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/search/abuseInfoList.js" type="text/javascript" charset="UTF-8"></script>
<h1 class="page-title">
	${currentMenuName}
</h1>
<div class="portlet light portlet-fit bordered">
	<div class="portlet-body">
		<!-- BEGIN FORM-->
		<form id="abuseDetailForm" action="" method="POST"
			class="form-horizontal form-bordered form-row-stripped">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<th style="width: 15%; text-align: center;"><label class="control-label">성명 </label></th>
						<td style="width: 35%; vertical-align: middle;" class="form-group form-md-line-input">${abuseInfo.request_target_user_name }</td>
						<th style="width: 15%; text-align: center;"><label class="control-label">추출기준명</label></th>
						<td style="width: 35%;" class="form-group form-md-line-input">${abuseInfo.rule_nm }</td>
					</tr>
					<tr>
						<th style="width: 15%; text-align: center;"><label class="control-label">발생일 </label></th>
						<td style="width: 35%; vertical-align: middle;">
							<c:set var="request_dt" value="${abuseInfo.request_dt}"/>
							${fn:substring(request_dt, 0, 4)}-${fn:substring(request_dt, 4, 6)}-${fn:substring(request_dt, 6, fn:length(request_dt))}
						</td>
						<th style="width: 15%; text-align: center;"><label class="control-label">판정일 </label></th>
						<td style="width: 35%; vertical-align: middle;">
							<c:set var="result_dt" value="${abuseInfo.result_dt}"/>
							${fn:substring(result_dt, 0, 4)}-${fn:substring(result_dt, 4, 6)}-${fn:substring(result_dt, 6, fn:length(result_dt))}
						</td>
					</tr>
					<tr>
						<th style="width: 15%; text-align: center;"><label class="control-label">소명요청사유 </label></th>
						<td style="vertical-align: middle;" class="form-group form-md-line-input" colspan="3">
							<textarea class="form-control" rows="5" cols="40" readonly="readonly">${abuseInfo.request_body}</textarea>
						</td>
					</tr>
					<tr>
						<th style="width: 15%; text-align: center;"><label class="control-label">소명판정사유 </label></th>
						<td style="vertical-align: middle;" class="form-group form-md-line-input" colspan="3">
							<textarea class="form-control" rows="5" cols="40" readonly="readonly">${abuseInfo.result_body}</textarea>
						</td>
					</tr>
					<tr>
						<th style="width: 15%; text-align: center;"><label class="control-label">위반내역</label></th>
						<td style="vertical-align: middle;" class="form-group form-md-line-input" colspan="3">
							<textarea class="form-control" name="abuse_content" rows="5" cols="40">${abuseInfo.abuse_content}</textarea>
						</td>
					</tr>
					<tr>
						<th style="width: 15%; text-align: center;"><label class="control-label">행정처분 처리일자</label></th>
						<td style="width: 35%; vertical-align: middle;" class="form-group form-md-line-input">
							<input class="form-control" name="administrative_dispo_day" value="${abuseInfo.administrative_dispo_day }"/>
						</td>
						<th style="width: 15%; text-align: center;"><label class="control-label">행정처분 유형</label></th>
						<td style="width: 35%; vertical-align: middle;" class="form-group form-md-line-input">
							<select class="form-control" name="administrative_dispo_type">
								<option value="">선택하세요</option>
								<c:forEach var="administrativeDispo" items="${administrativeDispoType }">
								<option value="${administrativeDispo.code_id }" <c:if test="${abuseInfo.administrative_dispo_type eq administrativeDispo.code_id }">selected="selected"</c:if>>${administrativeDispo.code_name }</option>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<th style="width: 15%; text-align: center;"><label class="control-label">형사처분 처리일자</label></th>
						<td style="width: 35%; vertical-align: middle;" class="form-group form-md-line-input">
							<input class="form-control" name="criminal_dispo_day" value="${abuseInfo.criminal_dispo_day }"/>
						</td>
						<th style="width: 15%; text-align: center;"><label class="control-label">형사처분 유형</label></th>
						<td style="width: 35%; vertical-align: middle;" class="form-group form-md-line-input">
							<select class="form-control" name="criminal_dispo_type">
								<option value="">선택하세요</option>
								<c:forEach var="criminalDispo" items="${criminalDispoType }">
								<option value="${criminalDispo.code_id }" <c:if test="${abuseInfo.administrative_dispo_type eq criminalDispo.code_id }">selected="selected"</c:if>>${criminalDispo.code_name }</option>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<th style="width: 15%; text-align: center;"><label class="control-label">기타처분 처리일자</label></th>
						<td style="width: 35%; vertical-align: middle;" class="form-group form-md-line-input">
							<input class="form-control" name="etc_dispo_day" value="${abuseInfo.etc_dispo_day }"/>
						</td>
						<th style="width: 15%; text-align: center;"><label class="control-label">기타처분 유형</label></th>
						<td style="width: 35%; vertical-align: middle;" class="form-group form-md-line-input">
							<input class="form-control" name="etc_dispo_type" value="${abuseInfo.etc_dispo_type }"/>
						</td>
					</tr>
				</tbody>
			</table>



			<!-- 메뉴 관련 input 시작 -->
			<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" />
			<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
			<input type="hidden" name="current_menu_id" value="${currentMenuId }" />
			<!-- 메뉴 관련 input 끝 -->
			
			<!-- 페이지 번호 -->
			<input type="hidden" name="page_num" value="${search.page_num }" />
			<input type="hidden" name="empDetailInqDetail_page_num" value="${search.empDetailInqDetail.page_num }" />
			<input type="hidden" name="extrtCondbyInqDetail_page_num" value="${search.extrtCondbyInqDetail.page_num }" />
			
			<!-- 검색조건 관련 input 시작 -->
			<input type="hidden" name="search_from" value="${search.search_from }" />
			<input type="hidden" name="search_to" value="${search.search_to }" />
			<input type="hidden" name="emp_user_id" value="${search.emp_user_id }" />
			<input type="hidden" name="emp_user_name" value="${search.emp_user_name}" />
			<input type="hidden" name="dept_name" value="${search.dept_name}" />
			<%-- <input type="hidden" name="rule_cd" value="${search.rule_cd }" /> --%>
			<input type="hidden" name="daySelect" value="${search.daySelect }" />
			<!-- 검색조건 관련 input 끝 -->
			
			<input type="hidden" name="desc_seq" value="${abuseInfo.desc_seq }" />
		</form>
		<!-- END FORM-->

		<!-- 버튼 영역 -->
		<div class="form-actions">
			<div class="row">
				<div class="col-md-offset-2 col-md-10" align="right"
					style="padding-right: 30px;">
					<button type="button" class="btn btn-sm grey-mint btn-outline sbold uppercase"
						onclick="javascript:goAbuseInfoList();">
						<i class="fa fa-list"></i> &nbsp;목록
					</button>
					<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="saveAbuseInfo()"><i
						class="fa fa-check"></i>&nbsp;저장</a>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var abuseInfoConfig = {
		"listUrl":"${rootPath}/extrtCondbyInq/abuseInfoList.html"
		,"detailUrl":"${rootPath}/extrtCondbyInq/abuseInfoDetail.html"
		,"saveUrl":"${rootPath}/extrtCondbyInq/saveAbuseInfo.html"
		,"menu_id":"${currentMenuId}"
		,"loginPage" : "${rootPath}/loginView.html"
		
	};
</script>