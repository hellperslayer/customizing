<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>

<HEAD>
<META NAME="Generator" CONTENT="Hancom HWP 8.5.8.1535">
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=euc-kr">
<TITLE>본 문서는 U</TITLE>
<STYLE type="text/css">
<!--
p.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle1
	{style-name:"그림제목"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:9.0pt; font-family:휴먼명조; letter-spacing:0; font-weight:"bold"; font-style:"normal"; color:#000000;}
li.HStyle1
	{style-name:"그림제목"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:9.0pt; font-family:휴먼명조; letter-spacing:0; font-weight:"bold"; font-style:"normal"; color:#000000;}
div.HStyle1
	{style-name:"그림제목"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:9.0pt; font-family:휴먼명조; letter-spacing:0; font-weight:"bold"; font-style:"normal"; color:#000000;}
p.HStyle2
	{style-name:"본문"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:11.0pt; font-family:휴먼명조; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle2
	{style-name:"본문"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:11.0pt; font-family:휴먼명조; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle2
	{style-name:"본문"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:11.0pt; font-family:휴먼명조; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle3
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle3
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle3
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle4
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle4
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle4
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle5
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle5
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle5
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle6
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle6
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle6
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle7
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle7
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle7
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle8
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle8
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle8
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle9
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle9
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle9
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle10
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle10
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle10
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle11
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle11
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle11
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle12
	{style-name:"각주"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:160%; font-size:9.0pt; font-family:휴먼명조; letter-spacing:0; font-weight:"bold"; font-style:"normal"; color:#000000;}
li.HStyle12
	{style-name:"각주"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:160%; font-size:9.0pt; font-family:휴먼명조; letter-spacing:0; font-weight:"bold"; font-style:"normal"; color:#000000;}
div.HStyle12
	{style-name:"각주"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:160%; font-size:9.0pt; font-family:휴먼명조; letter-spacing:0; font-weight:"bold"; font-style:"normal"; color:#000000;}
p.HStyle13
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle13
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle13
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle14
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle14
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle14
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle15
	{style-name:"MS바탕글"; margin-top:3.0pt; margin-bottom:2.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:10.0pt; font-family:굴림체; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle15
	{style-name:"MS바탕글"; margin-top:3.0pt; margin-bottom:2.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:10.0pt; font-family:굴림체; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle15
	{style-name:"MS바탕글"; margin-top:3.0pt; margin-bottom:2.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:10.0pt; font-family:굴림체; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
-->
</STYLE>
</HEAD>

<BODY background="..\..\..\AppData\Local\Temp\Hnc\BinData\EMB00000a683513.bmp">

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<DIV STYLE=''>

<P CLASS=HStyle0 STYLE='margin-left:38.0pt;text-align:center;text-indent:-38.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><IMG src="${rootPath}/resources/image/help/master/PICC6E0.gif" alt="" width="556" height="199" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:38.0pt;text-align:center;text-indent:-38.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>&nbsp;</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:38.0pt;text-align:center;text-indent:-38.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:38.0pt;text-align:center;text-indent:-38.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

</DIV>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'><IMG src="${rootPath}/resources/image/help/master/PICC6E1.png" alt="그림입니다.
원본 그림의 이름: 이지서티_[가로형].jpg
원본 그림의 크기: 가로 1000pixel, 세로 173pixel
사진 찍은 날짜: 2015년 11월 09일 오후 2:23" width="296" height="49" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<TR>
	<TD valign="middle" style='width:559;height:71;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>본 문서에 포함되어 있는 모든 내용의 저작권은 ㈜이지서티에 있으며</SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>대외비 문서로 분류됩니다. 따라서 본 문서에 포함된 어떠한 내용과</SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>기본적인 개념도 제3자에게 공개될 수 없음을 알려드립니다.</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'><BR></SPAN></P>

<DIV STYLE=''>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-6%'><BR></SPAN></P>

</DIV>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-6%'>&nbsp; </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>본 문서는 UBI SAFER-PSM v3.0의 제품 설치 시 (주)이지서티로부터 본 제품의 안전한 운용을 위해 지침에 따라 교육 받은 보안 관리자(최상위 관리자)에게 주어지는 문서입니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>&nbsp;&nbsp;또한 UBI SAFER-PSM v3.0의 제품에 포함된 모든 프로그램과 자료 파일, 사용 설명서, 문서 내용 등은 저작권법과 컴퓨터 프로그램 보호법에 의하여 보호받고 있습니다. </SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>&nbsp;&nbsp;UBI SAFER-PSM v3.0제품에 포함된 모든 프로그램과 자료, 문서 내용 등은 어떤 목적으로도 변형하거나 재가공하여 재판매할 수 없습니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>&nbsp;&nbsp;이 설명서의 내용은 ㈜이지서티의 사전 예고 없이 변경될 수 있습니다. </SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>&nbsp;&nbsp;㈜이지서티의 로고는 대한민국과 다른 여러 나라에 등록이 된 ㈜이지서티의 상표 입니다. ㈜이지서티의 서면 동의 없이 상업적 목적을 위하여 EASYCERTI 로고를 사용할 경우 상표권 침해와 불공정 경쟁행위가 됩니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>&nbsp;&nbsp;이 설명서의 정보가 정확하도록 ㈜이지서티는 모든 노력을 기울였습니다. ㈜이지서티는 인쇄 오류나 오타 및 오기에 대해서는 책임을 지지 않습니다. </SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>&nbsp;</SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>&nbsp;&nbsp;이 기능 설명서에서 언급하는 모든 회사명과 제품명은 각 개발사의 등록 상표이거나 상표이며, 시스템에 적용되어 있는 데이터는 사용자의 이해를 돕기 위한 가상의 데이터를 활용한 것입니다. </SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>&nbsp;제품에 이상이 있는 경우 아래의 연락처로 연락 주십시오.</SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'><BR></SPAN></P>

<DIV STYLE=''>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<TR>
	<TD valign="middle" style='width:356;height:99;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:left;line-height:130%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:130%'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tel: +82-2-865-5577</SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:left;line-height:130%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:130%'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fax: +82-2-6942-9999</SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:left;line-height:130%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:130%'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Homepage : </SPAN><A HREF="http://www.easycerti.com" TARGET="_self"><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:130%'>http://www.easycerti.com</SPAN></A></P>
	<P CLASS=HStyle0 STYLE='text-align:left;line-height:130%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:130%'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E-mail: </SPAN><A HREF="mailto:easycerti@easycerti.com"><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:130%'>easycerti@easycerti.com</SPAN></A></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-6%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-6%;line-height:180%'>Copyrightⓒ EASYCERTI. All rights reserved.</SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>&lt;차&nbsp;&nbsp; 례&gt;</SPAN></P>

</DIV>

<P CLASS=HStyle0 STYLE='margin-left:13.7pt;text-indent:-13.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>들어가는 글<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------------------&nbsp;&nbsp;</SPAN>1</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:13.7pt;text-indent:-13.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>1. 개요<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--------------------------------------------------------------------&nbsp;&nbsp;</SPAN>2</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>1.1. UBI SAFER-PSM이란?<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------&nbsp;&nbsp;</SPAN>2</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>1.2. UBI SAFER-PSM 구축 배경<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;-----------------------------------------------&nbsp;&nbsp;</SPAN>2</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>1.3. UBI SAFER-PSM의 목적<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;----------------------------------------------------&nbsp;&nbsp;</SPAN>4</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>1.4. UBI SAFER-PSM의 목표<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;----------------------------------------------------&nbsp;&nbsp;</SPAN>4</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>1.5. 기대효과<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;--------------------------------------------------------------------&nbsp;&nbsp;</SPAN>4</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:13.7pt;text-indent:-13.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>2. UBI SAFER-PSM 주요기능<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--------------------------------------------&nbsp;&nbsp;</SPAN>5</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>2.1. 비정상행위 모니터링 및 위험분석 기능<SPAN style='HWP-TAB:1;'>&nbsp;------------------------------------&nbsp;&nbsp;</SPAN>5</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>2.2. 접속기록 및 접근이력 관리 기능<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;-----------------------------------------&nbsp;&nbsp;</SPAN>5</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>2.3. 통계 및 보고 기능<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;----------------------------------------------------------&nbsp;&nbsp;</SPAN>5</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>2.4. 정책설정 및 환경설정 기능<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;----------------------------------------------&nbsp;&nbsp;</SPAN>5</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:13.7pt;text-indent:-13.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>3. 설치<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;---------------------------------------------------------------------&nbsp;&nbsp;</SPAN>6</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>3.1. 구성 내역<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-------------------------------------------------------------------&nbsp;&nbsp;</SPAN>6</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>3.2. 설치 구성<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-------------------------------------------------------------------&nbsp;&nbsp;</SPAN>6</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>3.3. 시스템 권장사양<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------------&nbsp;&nbsp;</SPAN>7</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>3.4. 관리자 프로그램 설치<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------&nbsp;&nbsp;</SPAN>7</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:13.7pt;text-indent:-13.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>4. 로그인<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;---------------------------------------------------------------------&nbsp;&nbsp;</SPAN>8</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:13.7pt;text-indent:-13.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>5. 개인정보 접속기록조회<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;---------------------------------------------------&nbsp;&nbsp;</SPAN>9</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>5.1. 접속기록조회<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;--------------------------------------------------------------&nbsp;&nbsp;</SPAN>9</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>5.1.1. 접속기록 조건검색<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;----------------------------------------------------&nbsp;&nbsp;</SPAN>10</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>5.1.2. 접속기록 로그 상세<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;---------------------------------------------------&nbsp;&nbsp;</SPAN>11</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>5.1.3. 엑셀다운로드<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;---------------------------------------------------------&nbsp;&nbsp;</SPAN>12</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>5.2. 백업이력 조회<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;-------------------------------------------------------------&nbsp;&nbsp;</SPAN>12</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>5.2.1. 백업이력 조건검색<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;----------------------------------------------------&nbsp;&nbsp;</SPAN>13</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>5.2.2. 엑셀다운로드<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;---------------------------------------------------------&nbsp;&nbsp;</SPAN>13</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:13.7pt;text-indent:-13.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>6. 시스템 접근이력조회<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-----------------------------------------------------&nbsp;&nbsp;</SPAN>14</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>6.1. 접근이력조회<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;--------------------------------------------------------------&nbsp;&nbsp;</SPAN>14</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>6.1.1. 접근이력 조건검색<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;----------------------------------------------------&nbsp;&nbsp;</SPAN>14</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>6.1.2. 접근이력 로그 상세<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;---------------------------------------------------&nbsp;&nbsp;</SPAN>15</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:13.7pt;text-indent:-13.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>7. 모니터링<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------------------&nbsp;&nbsp;</SPAN>16</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>7.1. 대시보드<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;------------------------------------------------------------------&nbsp;&nbsp;</SPAN>16</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>7.2. 토폴로지<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;------------------------------------------------------------------&nbsp;&nbsp;</SPAN>17</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>7.2.1. 토폴로지 조건검색<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;----------------------------------------------------&nbsp;&nbsp;</SPAN>17</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>7.2.2. 토폴로지 시스템 접속기록조회<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;--------------------------------------&nbsp;&nbsp;</SPAN>18</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:13.7pt;text-indent:-13.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>8. 빅데이터분석<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-------------------------------------------------------------&nbsp;&nbsp;</SPAN>19</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.1. 비정상위험분석<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------------&nbsp;&nbsp;</SPAN>19</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.1.1. 오·남용의심지수<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-------------------------------------------------------&nbsp;&nbsp;</SPAN>19</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.1.2. 오·남용의심행위자<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;----------------------------------------------------&nbsp;&nbsp;</SPAN>19</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.1.3. 오남용의심행위자증감량<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;----------------------------------------------&nbsp;&nbsp;</SPAN>20</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.2. 비정상 위험분석 리스트<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;---------------------------------------------------&nbsp;&nbsp;</SPAN>20</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.2.1. 비정상행위 위험분석 리스트<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------&nbsp;&nbsp;</SPAN>20</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:30.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.2.1.1. 비정상위험분석상세<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-----------------------------------------------&nbsp;&nbsp;</SPAN>20</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.2.2. 비정상위험분석 상세차트<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;---------------------------------------------&nbsp;&nbsp;</SPAN>21</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:30.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.2.2.1. 프로필<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-------------------------------------------------------------&nbsp;&nbsp;</SPAN>21</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:30.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.2.2.2. 위험현황<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;----------------------------------------------------------&nbsp;&nbsp;</SPAN>22</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:30.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.2.2.3. 업무 패턴 정보<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;----------------------------------------------------&nbsp;&nbsp;</SPAN>22</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.3. 비정상 위험 시나리오<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-----------------------------------------------------&nbsp;&nbsp;</SPAN>25</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.3.1. 시나리오 시뮬레이션 조건검색<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;---------------------------------------&nbsp;&nbsp;</SPAN>25</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.3.2. 시나리오 리스트<SPAN style='HWP-TAB:1;'>&nbsp;-------------------------------------------------------&nbsp;&nbsp;</SPAN>25</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:30.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.3.2.1. 시나리오 상세<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;----------------------------------------------------&nbsp;&nbsp;</SPAN>26</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.3.3. 시나리오 조건 검색<SPAN style='HWP-TAB:1;'>&nbsp;----------------------------------------------------&nbsp;&nbsp;</SPAN>27</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.3.4. 시나리오 엑셀다운로드<SPAN style='HWP-TAB:1;'>&nbsp;------------------------------------------------&nbsp;&nbsp;</SPAN>27</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:13.7pt;text-indent:-13.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>9. 개인정보 인텔리젼스 차트<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;----------------------------------------------&nbsp;&nbsp;</SPAN>28</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>9.1. 접속기록<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-------------------------------------------------------------------&nbsp;&nbsp;</SPAN>28</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>9.1.1. 개인정보 처리현황<SPAN style='HWP-TAB:1;'>&nbsp;-----------------------------------------------------&nbsp;&nbsp;</SPAN>28</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>9.1.2. 개인정보 처리 부서 TOP 10<SPAN style='HWP-TAB:1;'>&nbsp;-------------------------------------------&nbsp;&nbsp;</SPAN>28</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>9.1.3. 개인정보 취급자 TOP10<SPAN style='HWP-TAB:1;'>&nbsp;-----------------------------------------------&nbsp;&nbsp;</SPAN>29</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>9.1.4. 시스템별 처리현황<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;----------------------------------------------------&nbsp;&nbsp;</SPAN>29</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>9.1.5. 개인정보 유형별 현황<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-------------------------------------------------&nbsp;&nbsp;</SPAN>30</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>9.1.6. 개인정보 취급 유형별 현황<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-------------------------------------------&nbsp;&nbsp;</SPAN>30</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>9.1.7. CPU, MEMORY, HDD 사용량<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-----------------------------------------&nbsp;&nbsp;</SPAN>31</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>9.2. 위험분석<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-------------------------------------------------------------------&nbsp;&nbsp;</SPAN>31</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>9.2.1. 비정상 위험부서 TOP10<SPAN style='HWP-TAB:1;'>&nbsp;-----------------------------------------------&nbsp;&nbsp;</SPAN>31</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>9.2.2. 비정상 위험 유형별 현황<SPAN style='HWP-TAB:1;'>&nbsp;----------------------------------------------&nbsp;&nbsp;</SPAN>32</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:13.7pt;text-indent:-13.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>10. 통계 및 보고<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------------&nbsp;&nbsp;</SPAN>33</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>10.1. 기간별유형통계<SPAN style='HWP-TAB:1;'>&nbsp;-----------------------------------------------------------&nbsp;&nbsp;</SPAN>33</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>10.1.1. 기간별유형통계 조건검색<SPAN style='HWP-TAB:1;'>---------------------------------------------&nbsp;&nbsp;</SPAN>33</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>10.2. 시스템별유형통계<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;-------------------------------------------------------&nbsp;&nbsp;</SPAN>34</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>10.2.1. 시스템별유형통계 조건검색<SPAN style='HWP-TAB:1;'>&nbsp;------------------------------------------&nbsp;&nbsp;</SPAN>34</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>10.3. 소속별유형통계<SPAN style='HWP-TAB:1;'>------------------------------------------------------------&nbsp;&nbsp;</SPAN>35</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>10.3.1. 소속별유형통계 조건검색<SPAN style='HWP-TAB:1;'>---------------------------------------------&nbsp;&nbsp;</SPAN>36</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>10.4. 개인별유형통계<SPAN style='HWP-TAB:1;'>------------------------------------------------------------&nbsp;&nbsp;</SPAN>36</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>10.4.1. 개인별유형통계 조건검색<SPAN style='HWP-TAB:1;'>---------------------------------------------&nbsp;&nbsp;</SPAN>37</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>10.5. 점검보고서<SPAN style='HWP-TAB:1;'>&nbsp;----------------------------------------------------------------&nbsp;&nbsp;</SPAN>38</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>10.5.1. 접속기록보고서 생성<SPAN style='HWP-TAB:1;'>--------------------------------------------------&nbsp;&nbsp;</SPAN>38</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:13.7pt;text-indent:-13.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>11. 정책설정관리<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;---------------------------------------------------------&nbsp;&nbsp;</SPAN>40</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:7.0pt;'><SPAN STYLE='font-family:"맑은 고딕"'>11.1. 개인정보설정<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;-------------------------------------------------------------&nbsp;&nbsp;</SPAN>40</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>11.1.1. 개인정보설정 상세<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;--------------------------------------------------&nbsp;&nbsp;</SPAN>40</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>11.2. 위험도 기준설정<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;----------------------------------------------------------&nbsp;&nbsp;</SPAN>41</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>11.3. 예외처리DB<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;---------------------------------------------------------------&nbsp;&nbsp;</SPAN>41</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>11.3.1. 예외처리DB 검색<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-----------------------------------------------------&nbsp;&nbsp;</SPAN>42</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>11.3.2. 예외처리DB 등록<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-----------------------------------------------------&nbsp;&nbsp;</SPAN>42</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>11.3.3. 예외처리DB 조회<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-----------------------------------------------------&nbsp;&nbsp;</SPAN>42</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>11.3.4. 예외처리DB 삭제<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-----------------------------------------------------&nbsp;&nbsp;</SPAN>42</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:13.7pt;text-indent:-13.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>12. 환경설정<SPAN style='HWP-TAB:1;'>&nbsp;-----------------------------------------------------------------&nbsp;&nbsp;</SPAN>43</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.1. 메뉴관리<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------------------&nbsp;&nbsp;</SPAN>43</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.2. 권한관리<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------------------&nbsp;&nbsp;</SPAN>44</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.2.1. 권한관리 기능<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;--------------------------------------------------------&nbsp;&nbsp;</SPAN>44</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.2.2. 엑셀다운로드<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;---------------------------------------------------------&nbsp;&nbsp;</SPAN>45</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.3. 조직관리<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------------------&nbsp;&nbsp;</SPAN>45</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.4. 사용자관리<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------------------&nbsp;&nbsp;</SPAN>46</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.4.1. 사용자관리 조건검색<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;---------------------------------------------------&nbsp;&nbsp;</SPAN>46</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.4.2. 사용자관리상세<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;---------------------------------------------------------&nbsp;&nbsp;</SPAN>47</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:30.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.4.2.1. 사용자상세 내용 수정<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;--------------------------------------------&nbsp;&nbsp;</SPAN>47</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:30.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.4.2.2. 사용자상세 내용 삭제<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;----------------------------------------------&nbsp;&nbsp;</SPAN>48</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.4.3. 사용자관리 신규 등록<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;--------------------------------------------------&nbsp;&nbsp;</SPAN>48</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.4.4. 엑셀업로드<SPAN style='HWP-TAB:1;'>-------------------------------------------------------------&nbsp;&nbsp;</SPAN>49</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.4.5. 엑셀다운로드<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;-------------------------------------------------------&nbsp;&nbsp;</SPAN>50</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.5. 관리자관리<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;--------------------------------------------------------------&nbsp;&nbsp;</SPAN>50</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.5.1. 관리자관리 조건검색<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;----------------------------------------------&nbsp;&nbsp;</SPAN>50</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.5.2. 관리자관리 상세<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;---------------------------------------------------&nbsp;&nbsp;</SPAN>51</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:30.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.5.2.1. 관리자상세 내용 수정<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-----------------------------------------&nbsp;&nbsp;</SPAN>52</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:30.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.5.2.2. 관리자상세 내용 삭제<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-----------------------------------------&nbsp;&nbsp;</SPAN>52</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.5.3. 관리자관리 신규 등록<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;-----------------------------------------------&nbsp;&nbsp;</SPAN>53</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.5.4. 엑셀다운로드<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;--------------------------------------------------------&nbsp;&nbsp;</SPAN>54</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.6. 감사이력<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------------------&nbsp;&nbsp;</SPAN>54</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.6.1. 감사이력 조건검색<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;---------------------------------------------------&nbsp;&nbsp;</SPAN>55</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.6.2. 엑셀다운로드<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;--------------------------------------------------------&nbsp;&nbsp;</SPAN>55</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.7. 시스템관리<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;---------------------------------------------------------------&nbsp;&nbsp;</SPAN>56</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.8. 접속허용 IP<SPAN style='HWP-TAB:1;'>&nbsp;----------------------------------------------------------------&nbsp;&nbsp;</SPAN>56</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.8.1. 접속허용IP 생성<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------&nbsp;&nbsp;</SPAN>56</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.8.2. 접속허용IP 삭제 및 비활성화<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;----------------------------------------&nbsp;&nbsp;</SPAN>57</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:13.7pt;text-indent:-13.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>13. 기술지원 문의<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-----------------------------------------------------------&nbsp;&nbsp;</SPAN>58</SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>들어가는 글</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>&nbsp;본 설명서는 (주)이지서티 개인정보종합관리시스템 UBI SAFER-PSM 3.0의 효율적인 운용을 위하여 제공되는 UBI SAFER-PSM v3.0 관리 프로그램의 설치 및 운용 설명서입니다.</SPAN></P>

<DIV STYLE=''>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

</DIV>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-4%;line-height:180%'>UBI SAFER-PSM v3.0은 개인정보보호법에 따라 개인정보 접속기록을 관리하고 개인정보 취급자의 접근권한 및 접근통제 현황을 모니터링하여 개인정보의 유출 및 비정상행위를 방지하는 내부통제 솔루션입니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>UBI SAFER-PSM</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> v3.0 관리 프로그램은 운용자에게 익숙한 Windows환경의 Internet Explorer User Interface로 제공되어 편리하게 이용하실 수 있습니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle3 STYLE='line-height:180%;'>1. <SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>개요</SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>1.1. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>UBI SAFER-PSM이란?</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>UBI SAFER-PSM은 개</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>인정보 유출 및 오·남용 방지를 위한 내부통제 솔루션으로</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>, 개인정보에 대한 접속기록 보관 및 점검 기능을 제공하여 기관의 개인정보처리업무 현황에 대한 관리·감독을 수행할 수 있습니다. 또한, 개인정보보호법 및 관련법규와 감독기관의 규정 및 지침에서 요구하는 개인정보 접속기록에 대한 안전성 확보 관련사항을 준수합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#0000ff;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>1.2. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>UBI SAFER-PSM 구축 배경</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>최근 계속해서 발생하는 개인정보 침해사고로 인하여 국민의 불안감이 증가하고, 피해 사례가 증가함에 따라 개인정보를 처리하는 공공·민간 기관의 개인정보보호 활동에 대한 필요성과 책임의식이 증대되고 있습니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:2%;line-height:180%'>이에 따라, 개인정보보호법 시행(11.9.30) 및 과태료·과징금 부과 등 안전한</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:1%;line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'>개인정보보호를 위해 처벌규정을 강화하는 방향으로 계속해서 개정되고 있습니다.</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:1%;line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-10%;line-height:180%'>또한, 행정자치부 등 관리감독 기관에서 개인정보보호 실태점검, 관리수준 진단을 수행할 때 </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-8%;line-height:180%'>개인정보 접속기록 관리여부를 점검하도록 명시하고 있어 개인정보접속기록에 대한 체계적인 관리체계의 필요성이 증대되고 있는 상황입니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<CAPTION align="top">
<P CLASS=HStyle2 STYLE='margin-left:0.0pt;text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&lt;참고자료 1&gt;</SPAN>
</P>
</CAPTION><TR>
	<TD valign="middle" style='width:562;height:350;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle2 STYLE='line-height:125%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-1%;font-weight:"bold";line-height:125%'>개인정보보호법 제 29조(안전조치의무) </SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-top:5.0pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>&nbsp;개인정보처리자는 개인정보가 분실·도난·유출·위조·변조 또는 훼손되지 아니하도록 내부 관리계획 수립, 접속기록 보관 등 </SPAN><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";font-weight:"bold";text-decoration:"underline";color:#0000ff;line-height:125%'>대통령령으로 정하는 바에 따라 안전성 확보에 필요한 기술적·관리적 및 물리적 조치</SPAN><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>를 하여야 한다. </SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-top:5.0pt;line-height:125%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ff0000;line-height:125%'>&nbsp;</SPAN></P>
	<P CLASS=HStyle2 STYLE='line-height:125%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-1%;font-weight:"bold";line-height:125%'>개인정보보호법 제 31조(개인정보보호책임자의 지정) </SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:21.9pt;margin-top:5.0pt;text-indent:-21.9pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>&nbsp;&nbsp;① 개인정보처리자는 개인정보의 처리에 관한 업무를 총괄해서 책임질 개인정보 보호책임자를 지정하여야 한다.</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:24.2pt;text-indent:-24.2pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>&nbsp;&nbsp;② </SPAN><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";letter-spacing:-5%;line-height:125%'>개인정보 보호책임자는 다음 각 호의 업무를 수행한다.</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:27.0pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>&nbsp;&nbsp;1. 개인정보 보호 계획의 수립 및 시행</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:27.0pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>&nbsp;&nbsp;2. 개인정보 처리 실태 및 관행의 정기적인 조사 및 개선</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:27.0pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>&nbsp;&nbsp;3. 개인정보 처리와 관련한 불만의 처리 및 피해 구제</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:27.0pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'> </SPAN><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";font-weight:"bold";line-height:125%'> </SPAN><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";font-weight:"bold";text-decoration:"underline";color:#0000ff;line-height:125%'>4. 개인정보 유출 및 오용·남용 방지를 위한 내부통제시스템의 구축</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:27.0pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>&nbsp;&nbsp;5. 개인정보 보호 교육 계획의 수립 및 시행</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:27.0pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>&nbsp;&nbsp;6. 개인정보파일의 보호 및 관리·감독</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:27.0pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>&nbsp; </SPAN><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";font-weight:"bold";text-decoration:"underline";color:#0000ff;line-height:125%'>7. 그 밖에 개인정보의 적절한 처리를 위하여 대통령령으로 정한 업무</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:12.0pt;text-indent:-12.0pt;line-height:125%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ff0000;line-height:125%'>&nbsp;</SPAN></P>
	<P CLASS=HStyle2 STYLE='line-height:125%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-1%;font-weight:"bold";line-height:125%'>개인정보보호법 제 34조의2(과징금의 부과 등) </SPAN></P>
	<P CLASS=HStyle2 STYLE='margin-left:22.8pt;text-indent:-22.8pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>&nbsp;&nbsp;① 행정자치부장관은 개인정보처리자가 처리하는 주민등록번호가 분실·도난·유출·위조·변조 또는 훼손된 경우에는 </SPAN><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";font-weight:"bold";text-decoration:"underline";color:#0000ff;line-height:125%'>5억원 이하의 과징금을 부과·징수</SPAN><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>할 수 있다. 다만, 주민등록번호가 분실·도난·유출·위조·변조 또는 훼손되지 아니하도록 개인정보처리자가 제24조제3항에 따른 </SPAN><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";font-weight:"bold";text-decoration:"underline";color:#0000ff;line-height:125%'>안전성 확보에 필요한 조치를 다한 경우에는 그러하지 아니하다.</SPAN><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'> </SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<CAPTION align="top">
<P CLASS=HStyle2 STYLE='margin-left:0.0pt;text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&lt;참고자료 2&gt;</SPAN>
</P>
</CAPTION><TR>
	<TD valign="middle" style='width:562;height:228;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle2 STYLE='line-height:125%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-1%;font-weight:"bold";line-height:125%'>개인정보보호법 시행령 </SPAN><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";font-weight:"bold";line-height:125%'>제30조(개인정보의 안전성 확보 조치)</SPAN><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'> </SPAN></P>
	<P CLASS=HStyle2 STYLE='line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>&nbsp;① 개인정보처리자는 법 제29조에 따라 다음 각 호의 안전성 확보 조치를 하여야 한다.</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:23.0pt;text-indent:-1.0pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>&nbsp;&nbsp;1. 개인정보의 안전한 처리를 위한 내부 관리계획의 수립·시행</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:23.0pt;text-indent:-1.0pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>&nbsp;&nbsp;2. 개인정보에 대한 접근 통제 및 접근 권한의 제한 조치</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:44.6pt;text-indent:-22.6pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>&nbsp;&nbsp;3. </SPAN><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:125%'>개인정보를 안전하게 저장·전송할 수 있는 암호화 기술의 적용 또는 이에 상응하는 조치</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:44.4pt;text-indent:-22.4pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>&nbsp; </SPAN><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";font-weight:"bold";text-decoration:"underline";color:#0000ff;line-height:125%'>4. </SPAN><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";letter-spacing:-7%;font-weight:"bold";text-decoration:"underline";color:#0000ff;line-height:125%'>개인정보 침해사고 발생에 대응하기 위한 접속기록의 보관 및 위조·변조 방지를 위한 조치</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:23.0pt;text-indent:-1.0pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>&nbsp;&nbsp;5. 개인정보에 대한 보안프로그램의 설치 및 갱신</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:44.8pt;text-indent:-22.8pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>&nbsp;&nbsp;6. </SPAN><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:125%'>개인정보의 안전한 보관을 위한 보관시설의 마련 또는 잠금장치의 설치 등 물리적 조치</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:24.7pt;text-indent:-24.7pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>&nbsp;&nbsp;② 행정자치부장관은 개인정보처리자가 제1항에 따른 안전성 확보 조치를 하도록 시스템을 구축하는 등 필요한 지원을 할 수 있다. </SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:12.0pt;text-indent:-12.0pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>&nbsp;&nbsp;③ </SPAN><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:125%'>제1항에 따른 안전성 확보 조치에 관한 세부 기준은 행정자치부장관이 정하여 고시한다. </SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle0 STYLE='line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";color:#0000ff'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<CAPTION align="top">
<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-19.9pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&lt;참고자료 3&gt;</SPAN>
</P>
</CAPTION><TR>
	<TD valign="middle" style='width:562;height:182;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle2 STYLE='line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";font-weight:"bold";line-height:125%'>개인정보의 안전성 확보조치 기준 제8조(접속기록의 보관 및 점검) </SPAN></P>
	<P CLASS=HStyle2 STYLE='margin-left:22.2pt;text-indent:-22.2pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";background-color:#ffffff;line-height:125%'> </SPAN><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>① 개인정보처리자는 개인정보취급자가</SPAN><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";font-weight:"bold";line-height:125%'> </SPAN><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";font-weight:"bold";text-decoration:"underline";color:#0000ff;line-height:125%'>개인정보처리시스템에 접속한 기록을 6개월 이상 보관&#8228;관리</SPAN><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>하여야 한다.</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:22.0pt;text-indent:-22.0pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>&nbsp;&nbsp;② </SPAN><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:125%'>개인정보처리자는 개인정보의 분실&#8228;도난&#8228;유출&#8228;위조&#8228;변조 또는 훼손 등에 대응하기 위하여</SPAN><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'> </SPAN><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";font-weight:"bold";text-decoration:"underline";color:#0000ff;line-height:125%'>개인정보처리시스템의 접속기록 등을 반기별로 1회 이상 점검</SPAN><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>하여야 한다. </SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:22.4pt;text-indent:-22.4pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>&nbsp;&nbsp;③ 개인정보처리자는 개인정보취급자의 접속기록이 위&#8228;변조 및 도난, 분실되지 않도록 해당 </SPAN><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";font-weight:"bold";text-decoration:"underline";color:#0000ff;line-height:125%'>접속기록을 안전하게 보관</SPAN><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>하여야 한다.</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";color:#0000ff;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<CAPTION align="top">
<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-19.9pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&lt;참고자료 4&gt;</SPAN>
</P>
</CAPTION><TR>
	<TD valign="middle" style='width:562;height:203;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle2 STYLE='line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";font-weight:"bold";text-decoration:"underline";color:#0000ff;line-height:125%'>공공기관 개인정보보호 관리수준 진단 항목</SPAN></P>
	<P CLASS=HStyle2 STYLE='line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";font-weight:"bold";line-height:125%'>&nbsp;</SPAN></P>
	<P CLASS=HStyle2 STYLE='line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";font-weight:"bold";line-height:125%'>침해대책 수립 및 이행분야의 진단항목 中 </SPAN></P>
	<P CLASS=HStyle2 STYLE='line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>개인정보처리시스템의 접속기록에 대한 점검 및 후속 조치를 이행여부 </SPAN></P>
	<P CLASS=HStyle2 STYLE='margin-left:22.2pt;text-indent:-22.2pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:125%'>&lt;제출 증빙 자료&gt;</SPAN></P>
	<P CLASS=HStyle2 STYLE='margin-left:22.2pt;text-indent:-22.2pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:125%'>운영하고 있는 개인정보처리시스템에 대해 아래의 사항에 대한 증빙 실적을 제출하여야 함</SPAN></P>
	<P CLASS=HStyle2 STYLE='margin-left:22.2pt;text-indent:-22.2pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>① 접속기록 6개월 이상 보관·관리</SPAN></P>
	<P CLASS=HStyle2 STYLE='margin-left:22.2pt;text-indent:-22.2pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>② 이용자 식별 인증 정보(일시, 컴퓨터, IP 주소, ID 등), 서비스 이용정보 포함</SPAN></P>
	<P CLASS=HStyle2 STYLE='margin-left:22.2pt;text-indent:-22.2pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>③ 접속기록 점검 실적(반기별 1회 이상)</SPAN></P>
	<P CLASS=HStyle2 STYLE='margin-left:22.2pt;text-indent:-22.2pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>④ 접속기록 점검 실적 결과에 따른 후속조치 이행 실적</SPAN></P>
	<P CLASS=HStyle2 STYLE='margin-left:22.2pt;text-indent:-22.2pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>&nbsp;</SPAN></P>
	<P CLASS=HStyle2 STYLE='margin-left:22.2pt;text-indent:-22.2pt;line-height:125%;'><SPAN STYLE='font-size:8.7pt;font-family:"맑은 고딕";line-height:125%'>&lt;&nbsp;최근 5년간 공공기관 개인정보보호 관리수준 진단 지표 내용 발췌(2013년 ~ 2017년) &gt;</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";color:#0000ff;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";color:#0000ff;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";color:#0000ff;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>1.3. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>UBI SAFER-PSM의 목적</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>&nbsp;개인정보취급자의 개인정보 접속기록 보관 및 점검을 위한 기능을 제공하여 기관이 보유한 개인정보를 안전하게 보관 및 관리·감독할 수 있도록 지원합니다. 또한, 개인정보 유출 및 오·남용을 방지하고, 비인가된 개인정보 처리, 대량의 개인정보 유출 등의 비정상행위를 예방하고자 하는데 목적이 있습니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-bottom:4.0pt;text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>1.4. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>UBI SAFER-PSM의 목표</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>개인정보보호법, 국가정보보안지침 등 관련 법규 및 지침 준수</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>개인정보 접속기록에 대한 주기적 현황 관리</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>개인정보 접속기록에 대한 위·변조 및 도난, 분실 방지</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>기관의 개인정보 유출 및 오·남용 방지</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:12.0pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";letter-spacing:-2%;color:#0000ff;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>1.5. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>기대효과</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>UBI SAFER-PSM을 구축하여 기관의 개인정보취급자의 개인정보 처리업무에 대한 현황을 파악하고 정기적인 관리·감독 가능</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>주기적/상시적으로 발생하는 개인정보 관리·감독 및 평가 대응 시 접속기록 보관 및 점검 자료 반영</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>개인정보 취급자로 하여금 개인정보 처리 업무 시 개인정보보호법을 준수할 수 있도록 인식제고 효과</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:375;height:32;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>기대효과</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#cedeef"  style='width:187;height:32;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>구분</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:375;height:57;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:12.0pt;text-align:left;text-indent:0.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>기관의 개인정보취급자의 개인정보 처리업무에 대한 현황을 파악하고 정기적인 관리·감독 가능</SPAN></P>
	</TD>
	<TD valign="middle" style='width:187;height:57;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:160%'>개인정보보호 책임자</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:375;height:57;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:12.0pt;text-align:left;text-indent:0.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>주기적/상시적으로 발생하는 개인정보 관리·감독 및 평가 대응 시 접속기록 보관 및 점검 자료 반영</SPAN></P>
	</TD>
	<TD valign="middle" style='width:187;height:57;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:160%'>개인정보보호 담당자</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:375;height:57;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:12.0pt;text-align:left;text-indent:0.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'>개인정보 취급자로 하여금 개인정보 처리 업무 시 개인정보보호법을 준수할 수 있도록 </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>인식제고 효과</SPAN></P>
	</TD>
	<TD valign="middle" style='width:187;height:57;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:160%'>개인정보 취급자 / </SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:160%'>업무 담당자</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle0 STYLE='line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='margin-left:10.0pt;line-height:180%;'>2. <SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>UBI SAFER-PSM 주요기능</SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>2.1. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>비정상행위 모니터링 및 위험분석 기능</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>개인정보 접속기록 현황 모니터링 기능</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>비정상행위 기간별 추이분석 및 현황 모니터링 기능&nbsp;&nbsp; </SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>개인별 비정상행위 위험도 파악 및 오남용의심행위자 현황 파악 기능</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>비정상행위 시나리오 추출조건 관리 기능</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:12.0pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";color:#0000ff;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>2.2. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>접속기록 및 접근이력 관리 기능</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>개인정보 접속기록 보관 및 조회 기능</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>개인정보 접속기록 백업 보관 및 위·변조 방지 기능</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>시스템 접근이력 보관 및 조회 기능</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";color:#0000ff;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>2.3. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>통계 및 보고 기능</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>기간별 통계 조회 및 엑셀 다운로드 기능</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>시스템별 통계 조회 및 엑셀 다운로드 기능</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>소속별 통계 조회 및 엑셀 다운로드 기능</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>개인별 통계 조회 및 엑셀 다운로드 기능</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>개인정보 접속기록 점검보고서 생성 및 출력 기능</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";color:#0000ff;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>2.4. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>정책설정 및 환경설정 기능</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>개인정보유형 설정 기능</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>위험도기준 설정 기능</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>개인정보 예외처리 DB설정 기능</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>관리자관리 및 권한관리 기능</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>조직 및 사용자관리 기능</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>감사이력 조회 및 엑셀 다운로드 기능</SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle3 STYLE='line-height:180%;'>3. <SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>설치</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:10.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;UBI SAFER-PSM v3.0의 관리 프로그램은 Internet Explorer(이하 I.E로 명칭)가 설치되어 있고, 인터넷이 가능한 PC이며, 관리자 계정 및 비밀번호를 보유하고 있는 관리자에 한하여 사용할 수 있습니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>3.1. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>구성 내역</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>&nbsp;개인정보종합관리시스템 </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>UBI SAFER-PSM</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'> v3.0 구축 시 제공되는 항목은 다음과</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> 같습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>UBI SAFER-PSM v3.0 장비</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>Serial Cable</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>장비 Power Cable</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'>● <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>UBI SAFER-PSM 제품매뉴얼</SPAN></P>

<DIV STYLE=''>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'><BR></SPAN></P>

</DIV>

<P CLASS=HStyle2 STYLE='margin-left:25.6pt;text-indent:-15.6pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>※ UBI SAFER-PSM v3.0의 관리 프로그램은 웹 기반으로 기본적으로 장비 내에 설치되어 구축됩니다. 사용 방법은 4장부터 참고하시기 바랍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>3.2. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>설치 구성</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>UBI SAFER-PSM v3.0은 기본적으로 개인정보가 들어 있는 공통 기반 시스템인</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>웹 서버가 존재하는 서버팜(Server Farm) 바로 앞단에 설치하는 것을 권장합니다.</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:3%;line-height:180%'>설치는 ㈜이지서티의 전문 엔지니어에 의해 설치되며, 기본적으로 설치를 </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>권장하는 네트워크 구성도는 다음과 같습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICC701.png" alt="그림입니다.
원본 그림의 이름: CLP0000179c23bb.bmp
원본 그림의 크기: 가로 676pixel, 세로 261pixel" width="565" height="241" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>UBI SAFER-PSM v3.0은 웹서버로 향하는 HTTP, HTTPS 데이터 내 개인정보를</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> 탐지하는 시스템으로 네트워크상의 보호하고자하는 웹서버 바로 앞단, 방화벽 또는 웹방화벽 등의 보안장비 뒤에 위치시켜 설치하는 것을 권장합니다. UBI SAFER-PSM v3.0의 모듈 설치는 기본적으로 (주)이지서티의 엔지니어를 통하여 설치 배포됩니다. 다음은 관리 프로그램의 사용을 위해서 필요한 사항입니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<CAPTION align="bottom">
<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>[표 1-1 설치 구성]</SPAN>
</P>
</CAPTION><TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:161;height:42;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>System 권장사양</SPAN></P>
	</TD>
	<TD valign="middle" style='width:372;height:42;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>관리 프로그램의 원활한 운용을 위하여 필요한 기본 Hardware 사양</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:161;height:42;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>UBI SAFER-PSM</SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>관리 프로그램</SPAN></P>
	</TD>
	<TD valign="middle" style='width:372;height:42;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>웹 버전(Internet Explorer 지원)</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>3.3. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>시스템 권장사양</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;UBI SAFER-PSM v3.0 관리 프로그램의 원활한 운용을 위해 PC의 권장사양은 아래와 같습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<CAPTION align="bottom">
<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>[표 3-1 관리자 PC 시스템 권장사양]</SPAN>
</P>
</CAPTION><TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:119;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:2.0pt;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>CPU</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:2.0pt;'><SPAN STYLE='font-family:"맑은 고딕"'>Pentium3 866Mhz 이상</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:119;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:2.0pt;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>Memory</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:2.0pt;'><SPAN STYLE='font-family:"맑은 고딕"'>1GB 이상</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:119;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:2.0pt;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>HDD</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:2.0pt;'><SPAN STYLE='font-family:"맑은 고딕"'>10GB 이상</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:119;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:2.0pt;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>Ethernet</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:2.0pt;'><SPAN STYLE='font-family:"맑은 고딕"'>10/1000M Integrated Controller</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:119;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:2.0pt;text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>Internet Explorer</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:2.0pt;'><SPAN STYLE='font-family:"맑은 고딕"'>I E 버전 9.0 이상</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>3.4. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>관리자 프로그램 설치</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:1%;line-height:180%'>UBI SAFER-PSM v3.0의 관리 프로그램은 IE가 설치되어있는 PC면 어디서든 사용 가능하며</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>, UBI SAFER-PSM 모듈은 ㈜이지서티에서 납품 시 설치되어 출고 되므로 사이트 관리자의 별도 설치는 필요치 않습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle3 STYLE='line-height:180%;'>4. <SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>로그인</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:10.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;UBI SAFER-PSM v3.0관리를 위하여 Internet Explorer를 실행 후, Internet </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>Explorer 주소 창에 UBI SAFER-PSM v3.0 관리자 페이지 IP를 입력합니다. 관리자</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:3%;line-height:180%'>페이지 주소가 만약 192.168.0.130일 경우, 다음과 같이 입력합니다. (Port는 </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>업체별로 다르게 설정되어 있으므로 Tomcat Web Port 확인 필요)</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;▶ http://192.168.0.130:8080/psm</SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-10.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-9%;line-height:180%'>위의 주소를 입력하면 UBI SAFER-PSM v3.0 관리자 페이지 로그인 화면이 </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-11%;line-height:180%'>출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICC741.png" alt="그림입니다.
원본 그림의 이름: CLP0000185c0001.bmp
원본 그림의 크기: 가로 1061pixel, 세로 766pixel" width="567" height="410" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>* 잘못된 아이디 입력 시 : “입력하신 ID는 존재하지 않습니다.”</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>* </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-13%;line-height:180%'>잘못된 비밀번호 입력 시 : “비밀번호가 틀렸습니다. </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-15%;line-height:180%'>남은 로그인 가능횟수는</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-13%;line-height:180%'> X회입니다.”</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:10.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-7%;line-height:180%'>&nbsp;관리자 사용권한은 최상위 관리자와 일반 관리자로 나뉩니다. 최상위 관리자의</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-5%;line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-10%;line-height:180%'>경우 전체 권한을 가지므로 관리 프로그램 대분류 메뉴의 전체를 사용할 수 있으나,</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-5%;line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>일반 관리자는 정책관리 기능이 없고 환경설정의 몇 가지 기능에는 접근이 </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-5%;line-height:180%'>불가능합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-5%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:10.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;또한, 최상위 관리자는 단 한명만 존재하며, 일반관리자는 최상위 관리자에 의해 관리되어지고 프로그램의 대분류 메뉴의 환경설정에서 관리되어집니다.</SPAN></P>

<P CLASS=HStyle3 STYLE='line-height:180%;'>5. <SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>개인정보 접속기록조회</SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>5.1. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>접속기록조회</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:2%;line-height:180%'>접속기록조회 화면에서는 개인정보처리시스템에 접속하여 개인정보를 처리한</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:3%;line-height:180%'> 이력들을</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> 조회할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICC7AF.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff00008.bmp
원본 그림의 크기: 가로 1920pixel, 세로 1014pixel" width="567" height="300" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>일시</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>사용자가 개인정보를 처리한 일시</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>소속</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>개인정보를 처리한 사용자의 소속명</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>사용자ID</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>개인정보를 처리한 사용자의 ID</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>개인정보를 처리한 사용자의 이름</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>사용자IP</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>개인정보를 처리한 사용자의 IP</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>시스템</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>사용자가 개인정보를 처리한 시스템명</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:42;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>개인정보<BR>유형</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:42;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>사용자가 처리한 개인정보유형 정보(ex. 주민등록번호, 핸드폰번호)</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:42;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>수행업무</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:42;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>사용자가 접근한 개인정보가 들어있는 화면에서의 행위<BR>(ex. 조회, 등록, 수정, 삭제, 다운로드, 출력)</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>접근 경로</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>사용자가 접근한 개인정보가 들어있는 화면 주소</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:3%;line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:2%;line-height:180%'>UBI SAFER-PSM 로고 옆 왼쪽 상단의 <IMG src="${rootPath}/resources/image/help/master/PICC81D.png" alt="그림입니다.
원본 그림의 이름: CLP0000242c0001.bmp
원본 그림의 크기: 가로 35pixel, 세로 35pixel" width="19" height="19" vspace="0" hspace="0" border="0">버튼을 클릭하면 해당 메뉴에 대한 메뉴명이 표시되며, 표시된 메뉴명을 클릭하면 해당 페이지로 이동합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:3%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:3%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:3%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICC9B4.gif" alt="묶음 개체입니다." width="564" height="280" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>5.1.1. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>접속기록 조건검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-4%;line-height:180%'>접속기록조회 화면에서의 검색 조건은 기본적으로 일시, 기간선택, 개인</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-8%;line-height:180%'>정보</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-10%;line-height:180%'>유형, 사용자ID, 사용자명, 사용자IP, 소속, 접속유형, 수행업무가 있으며, 해당 조건을 입력 후</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-6%;line-height:180%'> 검색</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-4%;line-height:180%'>버튼을 클릭 시 검색조건에 해당하는 개인정보 접속기록 로그가 출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICC9C4.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff0000d.bmp
원본 그림의 크기: 가로 1793pixel, 세로 365pixel" width="567" height="115" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>일시</SPAN></P>
	</TD>
	<TD valign="middle" style='width:477;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>일시를 설정하여 해당 기간에 개인정보를 처리한 이력을 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>기간선택</SPAN></P>
	</TD>
	<TD valign="middle" style='width:477;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:4%'>오늘, 일주일, 전달 1일부터 금일, 올해 1월1일부터 금일, 전달 전체의 </SPAN><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:1%'>기</SPAN><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:2%'>간(날짜)을 설정하면 앞에 조건에 해당하는 기간이 바뀌면서 검색 시</SPAN><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:5%'> 해당 날짜의 개인정보를 처리한 사용자의 이력을 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:64;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>개인정보<BR>유형</SPAN></P>
	</TD>
	<TD valign="middle" style='width:477;height:64;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:2%'>주민등록번호, 운전면허번호, 여권번호, 신용카드번호, 건강보험번호, </SPAN><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:5%'>전화번호, 이메일, 휴대폰번호, 계좌번호, 외국인등록번호 등의 특정 개인정보유형을 선택하여 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>사용자ID</SPAN></P>
	</TD>
	<TD valign="middle" style='width:477;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>개인정보를 처리한 사용자의 ID로 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:477;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>개인정보를 처리한 사용자의 이름으로 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>사용자IP</SPAN></P>
	</TD>
	<TD valign="middle" style='width:477;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>개인정보를 처리한 사용자의 IP로 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>소속</SPAN></P>
	</TD>
	<TD valign="middle" style='width:477;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>사용자의 소속명으로 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>접속유형</SPAN></P>
	</TD>
	<TD valign="middle" style='width:477;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>접속유형을 선택하여 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>수행업무</SPAN></P>
	</TD>
	<TD valign="middle" style='width:477;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>개인정보처리시스템에 접근한 행위에 대해 선택하여 조회</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>5.1.2. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>접속기록 로그 상세</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp;접속기록조회 화면에서 각각의 로그정보를 클릭하게 되면 해당 로그에 대한 세부정보가 나타나게 됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICCA23.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff00013.bmp
원본 그림의 크기: 가로 1646pixel, 세로 898pixel" width="567" height="309" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp;아래 그림과 같이 개인정보를 처리한 로그에 대한 세부정보가 표시가 되며 </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>개인정보를 처리한 사용자의 정보 및 개인정보</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>유형, 개인정보 내용 등을 볼 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICCAB0.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff00011.bmp
원본 그림의 크기: 가로 1665pixel, 세로 734pixel" width="567" height="250" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp;또한, <IMG src="${rootPath}/resources/image/help/master/PICCB1F.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff00012.bmp
원본 그림의 크기: 가로 115pixel, 세로 39pixel" width="57" height="19" vspace="0" hspace="0" border="0"> 버튼을 클릭하시면 개인의 개인정보 처리 업무 패턴을 상세하게 확인하실 수 있습니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>5.1.3. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>엑셀다운로드</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp;접속기록조회 화면에서 다운로드 버튼을 클릭 시 접속기록조회 화면에서 검색한 로그를 EXCEL로 다운로드하는 기능입니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICCB3F.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff00014.bmp
원본 그림의 크기: 가로 1633pixel, 세로 432pixel" width="567" height="150" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:36.4pt;text-indent:-16.4pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";color:#0000ff;line-height:180%'>&nbsp; </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp; </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-4%;line-height:180%'>.xls 형태로 저장되며 ‘일시’, ‘소속’, ‘사용자ID’, ‘사용자명’, ‘사용자IP’, ‘시스템’,</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> ‘개인정보유형’, ‘수행업무’, ‘접근경로’의 정보를 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";color:#0000ff;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICCB8E.png" alt="그림입니다.
원본 그림의 이름: CLP000016400002.bmp
원본 그림의 크기: 가로 1353pixel, 세로 421pixel" width="567" height="176" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>5.2. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>백업이력 조회</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:2%;line-height:180%'>접속기록조회 내 백업이력조회 </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>메뉴는 매일 압축하여 보관하는 개인정보 접속기록 로그에 대한 백업일시 및 경로 등을 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";color:#0000ff;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICCBCD.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff00016.bmp
원본 그림의 크기: 가로 1908pixel, 세로 622pixel" width="567" height="185" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";color:#0000ff;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>5.2.1. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>백업이력 조건검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'>백업이력 조회 화면에서는 일시를 이용하여 검색을 할 수 있으며, 해당</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'> 조건을</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-6%;line-height:180%'>입력 후 검색 버튼을 클릭 시, 검색 조건에 해당하는 백업이력 로그가 출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";color:#0000ff;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICCC2C.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff00017.bmp
원본 그림의 크기: 가로 1850pixel, 세로 645pixel" width="567" height="198" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#0000ff;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#0000ff;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>5.2.2. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>엑셀다운로드</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>백업이력 조회 화면에서 다운로드 버튼을 클릭하게 되면 백업이력 조회 화면</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>에서 출력한 로그들을 EXCEL로 다운받을 수 있는 기능입니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";color:#0000ff;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICCC8A.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff00018.bmp
원본 그림의 크기: 가로 1850pixel, 세로 645pixel" width="567" height="198" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";color:#0000ff;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:36.4pt;text-indent:-16.4pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";color:#0000ff;line-height:180%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><SPAN style='HWP-TAB:1;'>&nbsp;</SPAN>.xls 형태로 저장되며 ‘일시’, ‘로그’, ‘파일경로&amp;파일명’, ‘파일크기’, ‘대상서버’의 정보를 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";color:#0000ff;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICCCD9.png" alt="그림입니다.
원본 그림의 이름: CLP000010d00006.bmp
원본 그림의 크기: 가로 1051pixel, 세로 183pixel" width="562" height="98" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle3 STYLE='line-height:180%;'>6. <SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>시스템 접근이력조회</SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>6.1. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>접근이력조회</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:3%;line-height:180%'>접근이력조회 화면에서는 시스템에 접근한 이력들을</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> 조회할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICCD19.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff0001a.bmp
원본 그림의 크기: 가로 1896pixel, 세로 1001pixel" width="567" height="299" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>일시</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>사용자가 시스템에 접근한 일시</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>소속</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>시스템에 접근한 사용자의 소속명</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>사용자ID</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>시스템에 접근한 사용자의 ID</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>시스템에 접근한 사용자의 이름</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>사용자IP</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>시스템에 접근한 사용자의 IP</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>시스템</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>사용자가 접근한 시스템명</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>접근경로</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>사용자가 접근한 시스템 화면 주소</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>6.1.1. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>접근이력 조건검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;접근이력조회 화면에서의 검색 조건은 기본적으로 일시, 기간선택, 접근경로, </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-6%;line-height:180%'>사용자ID, 사용자명, 사용자IP, 소속이 있으며, 해당 조건을 입력 후</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'> 검색</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>버튼을 클릭 시 검색조건에 해당하는 시스템 접근이력이 출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICCD97.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff0001b.bmp
원본 그림의 크기: 가로 1833pixel, 세로 411pixel" width="567" height="127" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>일시</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>일시를 설정하여 해당 날짜에 시스템에 접근한 이력을 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>기간선택</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>오늘, 일주일, 전달 1일부터 금일, 올해 1월1일부터 금일, 전달 전체의 기간(날짜)을 설정하면 앞에 조건에 해당하는 기간이 바뀌면서 검색 시</SPAN><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:2%'> 해당 날짜의 시스템 접근 이력을 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:35;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>접근경로</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:35;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>사용자가 접근한 시스템 화면 주소로 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>사용자ID</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>시스템에 접근한 사용자의 ID로 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>시스템에 접근한 사용자의 이름으로 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>사용자IP</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>시스템에 접근한 사용자의 IP로 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>소속</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>사용자의 소속명으로 조회</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>6.1.2. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>접근이력 로그 상세</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;접근이력조회 </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>화면에서 각각의 로그정보를 클릭하게 되면 해당 로그에 대한</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> 세부정보가 나타나게 됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICCDE6.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff0001c.bmp
원본 그림의 크기: 가로 1840pixel, 세로 692pixel" width="567" height="192" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:36.4pt;text-indent:-1.4pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;아래 그림과 같이 시스템에 접근한 사용자정보와 해당 사용자의 시스템 접근이력에 대해</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'> 볼 수</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICCE54.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff0001d.bmp
원본 그림의 크기: 가로 1901pixel, 세로 850pixel" width="567" height="254" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle3 STYLE='line-height:180%;'>7. <SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>모니터링</SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>7.1. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>대시보드</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>모니터링 대시보드에서는 실시간 개인정보 처리에 대한 시스템별 로그정보 및</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> 서버 정보를 조회할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICCEC2.png" alt="그림입니다.
원본 그림의 이름: CLP000016b40014.bmp
원본 그림의 크기: 가로 1060pixel, 세로 639pixel" width="567" height="342" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:25.5pt;text-indent:-5.5pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;위 그림과 같이 대시보드 화면에는 차트와 리스트로 구분되어 있으며 시스템정보를 통하여 CPU, MEMORY, HDD 사용량을 알 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:127;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>개인정보접속기록건수</SPAN></P>
	</TD>
	<TD valign="middle" style='width:406;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-14%'>5초 간격으로 실시간 로그 수집현황을 그래프로 출력(시스템별 조회 가능)</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:127;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>실시간 개인정보 접속기록 현황</SPAN></P>
	</TD>
	<TD valign="middle" style='width:406;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-1%'>최근에 수집한 개인정보 접속기록 로그정보에 대해 해당 시스템</SPAN><SPAN STYLE='font-family:"맑은 고딕"'>에 사용자 정보를 출력</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:127;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>시스템별 접속기록</SPAN><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-14%;font-weight:"bold"'> TOP 10</SPAN></P>
	</TD>
	<TD valign="middle" style='width:406;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>시스템별로 로그 수집 상위 10개에 대한 현황을 그래프로 출력 </SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:127;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-16%;font-weight:"bold"'>개인정보 유형별</SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-9%;font-weight:"bold"'>접속기록 현황</SPAN></P>
	</TD>
	<TD valign="middle" style='width:406;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>수집된 개인정보유형별 로그 %로 표시(시스템별 조회 가능)</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:127;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>비정상행위 추이분석</SPAN></P>
	</TD>
	<TD valign="middle" style='width:406;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-1%'>비정상행위로 의심되고 있는 로그 분석 현황</SPAN><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-8%'>(시스템별 조회 가능)</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:127;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>소속별 로그 수집 현황</SPAN></P>
	</TD>
	<TD valign="middle" style='width:406;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>소속별 개인정보 접속기록에 대한 로그 수와 그 안에 개인정보 총 개수를 표시</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:127;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>비정상행위별 분석현황</SPAN></P>
	</TD>
	<TD valign="middle" style='width:406;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>비정상행위 추출조건별 분석 현황(시스템별 조회 가능)</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:127;height:17;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-4%;font-weight:"bold"'>시스템정보</SPAN></P>
	</TD>
	<TD valign="middle" style='width:406;height:17;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>수집 서버의 CPU, MEMORY, HDD 사용량을 %로 표시</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>7.2. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>토폴로지</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-4%;line-height:180%'>&nbsp;모니터링 토폴로지에서는 해당 시스템에 사용자가 실시간 개인정보 처리를 하게</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> 되면 각 시스템에 사용자가 붙으면서 계속 뻗어나가는 부분을 보여주는 기능을 하고 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICCF11.png" alt="그림입니다.
원본 그림의 이름: CLP000016b40017.bmp
원본 그림의 크기: 가로 1832pixel, 세로 782pixel" width="567" height="242" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>7.2.1. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>토폴로지 조건검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>모니터링의 토폴로지 화면에서의 검색 조건은 기본적으로 실시간 모니터링, </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-8%;line-height:180%'>날짜, 시간, IP대역, 개인정보건수 상위 N명, Refresh가 있으며, 해당 조건을 입력 후</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'> 검색버튼을 클릭 시, 검색조건에 해당하는 토폴로지 형태의 모양이 출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>실시간 모니터링</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>실시간으로 토폴로지를 출력할지 선택 여부(체크)</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>날짜</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>해당 기간에 토폴로지 검색 시 입력(실시간 모니터링 X) </SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>시간</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>해당 시간에 토폴로지 검색 시 입력(실시간 모니터링 X)</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>IP대역</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>특정 IP대역의 토폴로지 검색 시 입력(실시간 모니터링 X)</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>개인정보 건수 상위 N명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>개인정보건수 상위 N명으로 제한 여부 선택(체크), N = 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 선택 가능(실시간 모니터링 X)</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>Refresh</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>실시간 토폴로지 출력을 5, 10, 20, 30, 60초 단위로 조회 가능</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>7.2.2. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>토폴로지 시스템 접속기록조회</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:1%;line-height:180%'>토폴로지 화면에서 해당 시스템이나 시스템에 붙어 있는 사용자의 정보를 </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>클릭하게 되면 해당하는 개인정보 접속기록리스트를 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICCF70.png" alt="그림입니다.
원본 그림의 이름: CLP000016b40018.bmp
원본 그림의 크기: 가로 916pixel, 세로 391pixel" width="567" height="242" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICCFBF.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff0001e.bmp
원본 그림의 크기: 가로 1901pixel, 세로 991pixel" width="567" height="296" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle3 STYLE='line-height:180%;'>8. <SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>빅데이터분석</SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>8.1. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>비정상위험분석</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:1%;line-height:180%'>&nbsp;비정상위험분석</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>에서는 비정상행위로 의심되는 개인정보 처리 업무를 수행한 </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>오·남용의심행위자에 대한 정보</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>를 조회할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICD04C.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff0001f.bmp
원본 그림의 크기: 가로 1901pixel, 세로 1008pixel" width="567" height="307" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>8.1.1. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>오·남용의심지수</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;오</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>·</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>남용의심지수</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>에서는 5단계의 위험도 단계(심각, 경계, 주의, 관심, 정상)에 해당하는 오·남용의심행위자에 대한 현황을 확인할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICD0BA.png" alt="그림입니다.
원본 그림의 이름: CLP000016b4001b.bmp
원본 그림의 크기: 가로 1745pixel, 세로 204pixel" width="567" height="66" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>8.1.2. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>오·남용의심행위자</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICD0EA.png" alt="그림입니다.
원본 그림의 이름: CLP000016b4001e.bmp
원본 그림의 크기: 가로 1690pixel, 세로 378pixel" width="567" height="104" vspace="0" hspace="0" border="0"> </SPAN><br><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-6%;line-height:180%'>오</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>·</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-6%;line-height:180%'>남용의심행위자</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-7%;line-height:180%'>에서는 금월의 오</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>·</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-7%;line-height:180%'>남용의심행위자에 대해 일별로 확인할 수 있습니다. 해당 날짜의 오</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>·</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-7%;line-height:180%'>남용의심행위자</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-9%;line-height:180%'> 클릭 시 비정상위험분석리스트로 이동합니다.</SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>8.1.3. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>오남용의심행위자증감량</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp;오남용의심행위자에서는 금년의 오남용의심행위자 증감량을 월별로 확인할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICD129.png" alt="그림입니다.
원본 그림의 이름: CLP00001b440003.bmp
원본 그림의 크기: 가로 1727pixel, 세로 409pixel" width="567" height="137" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-4%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle6 STYLE='margin-left:20.0pt;line-height:180%;'>8.2. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>비정상 위험분석 리스트</SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>8.2.1. <SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>비정상행위 위험분석 리스트</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>비정상행위 위험분석 리스트</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>에서는 해당 날짜의 오남용의심행위자</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-4%;line-height:180%'> 리스트를</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-4%;line-height:180%'>확인할 수 있습니다. 리스트 중 조회하고 싶은 사용자을 클릭 시 비정상위험</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-5%;line-height:180%'>분석</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-4%;line-height:180%'> 상세페이지로 이동합니다. (<IMG src="${rootPath}/resources/image/help/master/PICD14A.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff00021.bmp
원본 그림의 크기: 가로 61pixel, 세로 25pixel" width="47" height="19" vspace="0" hspace="0" border="0"> 버튼을 클릭하면 업무패턴정보로 바로</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'> 이동합니다. )</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICD179.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff00020.bmp
원본 그림의 크기: 가로 1897pixel, 세로 915pixel" width="567" height="274" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle6 STYLE='line-height:180%;'>8.2.1.1. <SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>비정상위험분석상세</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>비정상위험분석상세에서는 비정상행위 추출조건정보와 해당 업무를 수행한 사용자의 정보와 개인정보 접속기록 리스트를 확인할 수 있습니다. 직원정보의 사용자명을 클릭 시 비정상위험분석상세(업무패턴 정보) 페이지로 이동합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICD1D8.png" alt="그림입니다.
원본 그림의 이름: CLP000016b40020.bmp
원본 그림의 크기: 가로 964pixel, 세로 296pixel" width="567" height="159" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>8.2.2. <SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>비정상위험분석 상세차트</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'>
<IMG src="${rootPath}/resources/image/help/master/PICD34F.gif" alt="묶음 개체입니다." width="568" height="251" vspace="0" hspace="0" border="0"> </SPAN><br><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-5%;line-height:180%'>비정상위험분석 상세차트에서는 개인이 해당되는 비정상행위 위험단계와 여러</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'> 비정상행위 추출조건에 대한 개인의 현황을 확인할 수 있습니다. 또한, 업무패턴 정보 화면을 통해 개인의 개인정보 취급패턴을 다양한 방식으로 확인할 수 있습니다. </SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle6 STYLE='line-height:180%;'>8.2.2.1. <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>프로필</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'>
<IMG src="${rootPath}/resources/image/help/master/PICD370.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff00025.bmp
원본 그림의 크기: 가로 1548pixel, 세로 750pixel" width="567" height="180" vspace="0" hspace="0" border="0">&nbsp;&nbsp;&nbsp;<br>비정상위험분석 상세차트에서는 개인의 소속, IP, ID의 정보와 주로 개인정보업무를 처리하는 시스템명이 표시되며 개인의 개인정보처리업무 현황에 따른 위험도단계와 설명을 확인할 수 있습니다. </SPAN></P>

<P CLASS=HStyle6 STYLE='line-height:180%;'>8.2.2.2. <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>위험현황</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:50.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'>&nbsp;비정상위험분석 상세차트에서는 5개의 카테고리의 비정상행위(고유식별정보 과다사용, 비정상접근, 과다처리, 특정인처리, 특정시간대처리)에 대한 개인의 처리현황과 소속의 평균을 비교하여 보여줍니다. </SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:50.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:50.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:50.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:50.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:50.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:50.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:50.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:50.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:50.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:50.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'>&nbsp;</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'>
<IMG src="${rootPath}/resources/image/help/master/PICD4A9.gif" alt="묶음 개체입니다." width="568" height="251" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle6 STYLE='line-height:180%;'>8.2.2.3. <SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>업무 패턴 정보</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:50.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'>&nbsp;개인정보 취급패턴 정보에서는 개인정보 취급량, 월별 누적량 비교, 요일별 패턴, 24시간 패턴, 취급유형 정보를 확인할 수 있습니다. </SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:50.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'>개인정보 취급량은 전달의 개인정보 처리량 대비 3개월 동안의 취급량을 비교할 수 있도록 차트를 제공합니다. 주중평균과 주말평균을 값을 제공하여 평소 업무량이 많을 때와 적을 때를 비교할 수 있도록 하였습니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'>
<IMG src="${rootPath}/resources/image/help/master/PICD4E8.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff00026.bmp
원본 그림의 크기: 가로 1528pixel, 세로 883pixel" width="567" height="314" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:50.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'>월별 누적량 비교는 지난달부터 6개월 동안의 개인정보 취급량을 달별로 비교해 볼 수 있도록 차트를 제공합니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICD556.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff00028.bmp
원본 그림의 크기: 가로 1547pixel, 세로 587pixel" width="567" height="195" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:50.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:50.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'>요일별 패턴은 지난주 현황과 3개월 동안의 해당 요일의 평균값을 비교해 볼 수 있습니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICD5A5.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff00029.bmp
원본 그림의 크기: 가로 1552pixel, 세로 630pixel" width="567" height="195" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:50.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:50.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'>24시간 패턴은 전일 24시간 기준과 3개월 동안 평균값을 비교할 수 있도록 차트를 제공합니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICD604.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff0002a.bmp
원본 그림의 크기: 가로 1551pixel, 세로 659pixel" width="567" height="192" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:50.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:50.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:50.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-4%;line-height:180%'>취급 유형은 개인이 처리하는 개인정보의 유형과 시스템별 처리량, 시스템별</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'> 고유식별정보 처리현황, 업무 유형에 대한 차트를 제공합니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICD682.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff0002c.bmp
원본 그림의 크기: 가로 1520pixel, 세로 965pixel" width="567" height="360" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>8.3. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>비정상 위험 시나리오</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-8%;line-height:180%'>&nbsp;시나리오 시뮬레이션에서는 비정상행위로 의심되는 처리업무에 대한 추출조건 정보가 출력됩니다. 5가지의 비정상행위 카테고리로 이루어진 (고유식별 과다사용, 비정상접근, 과다처리, 특정인 처리, 특정시간대 처리) 추출조건명을 클릭하면 해당 카테고리에 포함되어 있는 세부 추출조건이 표시됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICD6F0.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff0002d.bmp
원본 그림의 크기: 가로 1363pixel, 세로 581pixel" width="567" height="242" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>8.3.1. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>시나리오 시뮬레이션 조건검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;시나리오 시뮬레이션 화면에서의 검색 조건은 기본적으로 시나리오명이</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'> 있으며, 해당 시나리오명을 입력 후 검색버튼을 클릭 시,</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> 검색조건에 해당하는 시나리오가 출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICD720.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff0002f.bmp
원본 그림의 크기: 가로 1843pixel, 세로 297pixel" width="567" height="91" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>8.3.2. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>시나리오 리스트</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-8%;line-height:180%'>&nbsp;시나리오 리스트에서는 비정상행위로 의심되는 시나리오에 대한 상세정보 조회 및 사용여부를 설정할 수 있습니다. 상세시나리오명을 클릭하면 시나리오 상세페이지가 출력됩니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICD75F.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff0002e.bmp
원본 그림의 크기: 가로 1346pixel, 세로 591pixel" width="567" height="249" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:140%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>상세</SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>시나리오명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>비정상행위로 의심되는 시나리오명</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>임계치</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>비정상행위를 판별하기 위한 기준이 되는 경계 값</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>사용여부</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>시나리오 추출조건의 사용여부 </SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>실시간</SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>추출여부</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>시나리오 추출조건의 실시간추출여부</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-align:center;text-indent:-30.0pt;line-height:140%;'></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-8%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-8%;line-height:180%'>&nbsp;상세 시나리오 추가는 기관의 개인정보 처리업무의 특성과 환경에 맞게 ㈜이지서티 방문 엔지니어에 의해 관리됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle6 STYLE='margin-left:30.0pt;line-height:180%;'>8.3.2.1. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>시나리오 상세</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-8%;line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-9%;line-height:180%'>시나리오 상세 페이지에서는 시나리오에 대한 상세정보 조회와 임계치, 위험지수 설정 및 시나리오 사용여부를 설정할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-9%;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICD78F.png" alt="그림입니다.
원본 그림의 이름: 시나리오 상세.PNG
원본 그림의 크기: 가로 1365pixel, 세로 576pixel" width="567" height="174" vspace="0" hspace="0" border="0"></SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-8%;line-height:180%'>&nbsp;</SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>8.3.3. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>시나리오 조건 검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;시나리오 리스트 화면에서의 검색 조건은 기본적으로 사용여부, 실시간추출</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-8%;line-height:180%'>여부가 있으며, 해당 조건을 입력 후 검색버튼을 클릭 시, 검색조건에 해당하는 시나리오가 출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:140%;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:140%'><IMG src="${rootPath}/resources/image/help/master/PICD81C.png" alt="그림입니다.
원본 그림의 이름: CLP00001640000d.bmp
원본 그림의 크기: 가로 1832pixel, 세로 264pixel" width="567" height="82" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-align:center;text-indent:-30.0pt;line-height:140%;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:140%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:140%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>사용여부</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>시나리오 추출조건의 사용여부 설정하여 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>실시간</SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>추출여부</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>시나리오 추출조건의 실시간추출여부 설정하여 조회 </SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-align:center;text-indent:-30.0pt;line-height:140%;'></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-align:center;text-indent:-30.0pt;line-height:140%;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:140%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>8.3.4. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>시나리오 엑셀다운로드</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-8%;line-height:180%'>시나리오 리스트 화면에서 엑셀다운로드 버튼을 클릭하게 되면 시나리오 리스트에 나타난 내용에 대해 EXCEL로 다운받을 수 있는 기능입니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:10.8pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICD84C.png" alt="그림입니다.
원본 그림의 이름: CLP000016440001.bmp
원본 그림의 크기: 가로 1343pixel, 세로 507pixel" width="567" height="195" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:10.8pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>&nbsp;.xls 형태로 저장되며 “상세시나리오명”, “임계치”, “사용여부”, “실시간추출여부”의 정보를 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";color:#ff0000;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICD87C.png" alt="그림입니다.
원본 그림의 이름: CLP000016400010.bmp
원본 그림의 크기: 가로 975pixel, 세로 146pixel" width="567" height="85" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle3 STYLE='line-height:180%;'>9. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>개인정보 인텔리젼스 차트</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:2%;line-height:180%'>개인정보 인텔리젼스 차트는 관리자가 주요 개인정보처리시스템의 개인정보 </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>접속기록 현황과 비정상 위험 현황에 대해 다양한 방면으로 분석하여 보여줄 수 있도록 도식화 되어있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>9.1. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>접속기록</SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>9.1.1. <SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>개인정보 처리현황</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:41.9pt;text-indent:-21.9pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;개인정보 처리 현황 차트는 시스템별로 개인정보 처리 건수를 비교해 볼 수 있습니다. </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-10%;line-height:180%'>또한 기간검색을 통해 기간별로 개인정보 처리현황을 살펴볼 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICD8AC.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff00031.bmp
원본 그림의 크기: 가로 1916pixel, 세로 982pixel" width="567" height="257" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>9.1.2. <SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>개인정보 처리 부서 TOP 10</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;개인정보 처리 부서 TOP 10은 개인정보를 가장 많이 처리한 소속 10군데에 대한 개인정보검출 총 건수를 표현합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICD929.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff00032.bmp
원본 그림의 크기: 가로 1857pixel, 세로 720pixel" width="567" height="240" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>9.1.3. <SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>개인정보 취급자 TOP10</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;text-indent:-15.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;개인정보 취급자 TOP10은 개인정보를 가장 많이 처리한 사용자 10명에 대한 개인정보검출 총 건수를 표현합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICD998.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff00033.bmp
원본 그림의 크기: 가로 1856pixel, 세로 771pixel" width="567" height="258" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>9.1.4. <SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>시스템별 처리현황</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:41.9pt;text-indent:-21.9pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;시스템별 건수는 개인정보 유형별 개인정보검출 건수를 표현합니다. 특정 시스템의 현황을 제외하고 싶은 경우, 그래프 오른쪽에 위치한 시스템명을 클릭하면 해당 시스템이 제외되어 그래프에 표시됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICD9F6.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff00034.bmp
원본 그림의 크기: 가로 1860pixel, 세로 766pixel" width="567" height="254" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>9.1.5. <SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>개인정보 유형별 현황</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:41.9pt;text-indent:-21.9pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'>개인정보 유형별 현황은 개인정보 유형별 총 검출된 건수와 비율을 표현합니다</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICDA64.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff00035.bmp
원본 그림의 크기: 가로 1863pixel, 세로 767pixel" width="567" height="234" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>9.1.6. <SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>개인정보 취급 유형별 현황</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:41.9pt;text-indent:-21.9pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;개인정보 취급 유형별 현황은 개인정보 처리 업무에 대해 수집, 삭제, 수정 등을 비롯한 개인정보보호법에서 명시하고 있는 개인정보 처리 유형에 대해 그 비율을 볼 수 있도록 차트를 제공합니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICDAC3.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff00036.bmp
원본 그림의 크기: 가로 1854pixel, 세로 764pixel" width="567" height="234" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>9.1.7. <SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>CPU, MEMORY, HDD 사용량</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:41.9pt;text-indent:-21.9pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CPU, MEMORY, HDD 사용량은 현재 구동중인 서버의 간략한 정보내용을 그래프로 표현하여 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICDB31.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff00037.bmp
원본 그림의 크기: 가로 1841pixel, 세로 712pixel" width="567" height="219" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:20.0pt;line-height:180%;'>9.2. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>위험분석</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-indent:5.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>위험분석</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>에서는 비정상 위험부서 TOP10 과 비정상 위험 유형별 현황에 대해 차트를 제공하고 있습니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-indent:5.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>9.2.1. <SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>비정상 위험부서 TOP10</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;부서별로 위험부서 10개 현황에 대해 차트를 제공하고 있습니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICDB90.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff00038.bmp
원본 그림의 크기: 가로 1845pixel, 세로 723pixel" width="567" height="222" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>9.2.2. <SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>비정상 위험 유형별 현황</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>비정상 위험 유형별로 현황을 파악할 수 있도록 차트를 제공하고 있습니다.</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICDBEE.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff00039.bmp
원본 그림의 크기: 가로 1856pixel, 세로 762pixel" width="567" height="233" vspace="0" hspace="0" border="0">&nbsp;</SPAN></P>

<P CLASS=HStyle3 STYLE='line-height:180%;'>10. <SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>통계 및 보고</SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>10.1. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>기간별유형통계</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";color:#ff0000;line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-8%;line-height:180%'>기간별유형통계 화면에서는 설정한 기간에 대해 일별 처리된 개인정보 유형에 대한 통계를 조회할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:145%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:145%'><IMG src="${rootPath}/resources/image/help/master/PICDC4D.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff0003a.bmp
원본 그림의 크기: 가로 1911pixel, 세로 567pixel" width="567" height="168" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:25.5pt;text-indent:-5.5pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:25.5pt;text-indent:-5.5pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;기간별 유형통계를 </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'>조회했을 때 출력되는 데이터의 내용은 다음과 같습니다. ■부분은 기간</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>별 가장 많이 처리된 개인정보유형을 알려줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:130%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>날짜</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='line-height:130%;'><SPAN STYLE='font-family:"맑은 고딕"'>기간을 기준으로 개인정보가 처리된 접속기록들의 날짜를 출력</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:130%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>개인정보<BR>유형</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='line-height:130%;'><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-1%'>기간을 기준으로 각각의 개인정보유형인 주민등록번호, 외국인등록번호,</SPAN><SPAN STYLE='font-family:"맑은 고딕"'> </SPAN><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-1%'>운전면허번호, 여권번호, 신용카드번호, 건강보험번호, 전화번호, 이메일,</SPAN><SPAN STYLE='font-family:"맑은 고딕"'> 휴대폰번호, 계좌번호가 처리된 이력들의 건수를 출력 </SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='margin-left:0.0pt;text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:80%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:80%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>10.1.1. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>기간별유형통계 조건검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:1%;line-height:180%'>기간별 유형통계 조회 화면에서의 검색 조건은 기본적으로 일시, 기간선택</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-5%;line-height:180%'>(일별, 월별, 년별)이 있으며, 해당 조건을 입력 후 검색버튼을 클릭 시, 검색조건에</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> 해당하는 기간별유형통계가 출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICDC8C.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff0003d.bmp
원본 그림의 크기: 가로 1806pixel, 세로 190pixel" width="567" height="60" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:130%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";line-height:130%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:130%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>일시</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='line-height:130%;'><SPAN STYLE='font-family:"맑은 고딕"'>기간별 통계를 내기 위한 개인정보 처리 일시 설정</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:130%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>일별</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='line-height:130%;'><SPAN STYLE='font-family:"맑은 고딕"'>당월 기준으로 1일 ～ 말일까지 일별 개인정보유형통계 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:130%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>월별</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='line-height:130%;'><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-1%'>올해 기준으로 1월 1일 ～ 해당년도 말일(12월 31일)까지 월별 개인정보</SPAN><SPAN STYLE='font-family:"맑은 고딕"'>유형통계 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:130%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>년별</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='line-height:130%;'><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-4%'>가장 초기에 처리된 연도의 이전년도부터 금년도까지의 각 연도의 기간별</SPAN><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-3%'> 개인정보유형통계 검색</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>10.2. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>시스템별유형통계</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;시스템</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-8%;line-height:180%'>별 유형통계 화면에서는 설정한 기간에 대해 시스템별 처리된 개인정보 유형에 대한 통계를 조회할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICDCCC.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff0003e.bmp
원본 그림의 크기: 가로 1892pixel, 세로 875pixel" width="567" height="262" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;위 그림에서 보면 시스템별유형통계를 조회했을 때 출력되는 데이터의 내용입니다. ■부분은 시스템별 가장 많이 처리된 개인정보유형을 알려줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>날짜</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>시스템을 기준으로 개인정보가 처리된 이력들의 날짜를 출력</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>시스템명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>시스템을 기준으로 개인정보가 처리된 이력들의 시스템을 출력</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:71;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>개인정보</SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>유형</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:71;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-4%'>시스템을 기준으로 각각의 개인정보유형인 주민등록번호, 외국인등록번호,</SPAN><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-2%'> 운전면허번호, 여권번호, 신용카드번호, 건강보험번호, 전화번호, 이메일, 휴대폰번호, 계좌번호의 처리된 이력들의 건수를 출력</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>10.2.1. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>시스템별유형통계 조건검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp;시스템별유형통계 조회 화면에서의 검색 조건은 기본적으로 일시, 시스템명, 기간선택</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'>(일별, 월별, 년별)이 있으며, 해당 조건을 입력 후 검색버튼을 클릭 시,</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> 검색조건에 해당하는 시스템별유형통계가 출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICDD2A.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff0003f.bmp
원본 그림의 크기: 가로 1803pixel, 세로 279pixel" width="567" height="88" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>일시</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>시스템별 통계를 내기 위한 개인정보 처리 일시 설정</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>시스템명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>PSM에 등록되어 있는 시스템을 설정하여 통계 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>일별</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>당월 기준으로 1일 ～ 말일까지 시스템별 개인정보유형통계 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:53;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>월별</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:53;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-2%'>올해 기준으로 1월 1일 ～ 해당년도 말일(12월 31일)까지 시스템별 개인정보유형통계 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:53;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>년별</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:53;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-18%'>가장 초기에 처리된 연도의 이전년도부터 금년도까지의 각 연도의 시스템별 개인정보유형통계 검색</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>10.3. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>소속별유형통계</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;소속</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-8%;line-height:180%'>별유형통계 화면에서는 설정한 기간에 대해 소속별 처리된 개인정보 유형에 대한 통계를 조회할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICDD6A.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff00040.bmp
원본 그림의 크기: 가로 1832pixel, 세로 785pixel" width="567" height="243" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:0.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-4%;line-height:180%'>위 그림에서 보면 소속별유형통계를 조회했을 때 출력되는 데이터의 내용입니다.</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> ■부분은 소속별 가장 많이 처리된 개인정보유형을 알려줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>날짜</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>소속을 기준으로 개인정보가 처리된 이력들의 날짜를 출력</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>소속명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>소속을 기준으로 개인정보가 처리된 이력들의 소속을 출력</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:71;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>개인정보</SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>유형</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:71;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-1%'>소속을 기준으로 각각의 개인정보유형인 주민등록번호, 외국인등록번호, 운전면허번호, 여권번호, 신용카드번호, 건강보험번호, 전화번호, 이메일,</SPAN><SPAN STYLE='font-family:"맑은 고딕"'> 휴대폰번호, 계좌번호의 처리된 이력들의 건수를 출력 </SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='text-align:left;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:left;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:left;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>10.3.1. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>소속별유형통계 조건검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp;소속별유형통계 조회 화면에서의 검색 조건은 기본적으로 일시, 소속명, 기간선택(일별, 월별, 년별)이 있으며, 해당 조건을 입력 후 검색버튼을 클릭 시, 검색조건에 해당하는 소속별유형통계가 출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICDDC8.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff00041.bmp
원본 그림의 크기: 가로 1796pixel, 세로 279pixel" width="567" height="88" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:90%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:90%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>일시</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>소속별 통계를 내기 위한 개인정보 처리 일시 설정</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>소속명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>PSM에 등록되어 있는 소속명을 설정하여 통계 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>일별</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>당월 기준으로 1일 ~ 말일까지 소속별 개인정보유형통계 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:53;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>월별</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:53;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-3%'>올해 기준으로 1월 1일 ~ 해당년도 말일(12월 31일)까지 소속별 개인정보</SPAN><SPAN STYLE='font-family:"맑은 고딕"'>유형통계 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:53;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>년별</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:53;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-4%'>가장 초기에 처리된 연도의 이전년도부터 금년도까지의 각 연도의</SPAN><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-1%'> 소속별</SPAN><SPAN STYLE='font-family:"맑은 고딕"'> 개인정보유형통계 검색</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='line-height:125%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:125%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>10.4. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>개인별유형통계</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;개인별유형통계 화면에서는 설정한 기간에 대해 개인별 처리된 개인정보 유형에 대한 통계를 조회할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICDE08.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff00042.bmp
원본 그림의 크기: 가로 1827pixel, 세로 797pixel" width="567" height="247" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:150%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";line-height:150%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>위 그림에서 보면 개인별유형통계를 조회했을 때 출력되는 데이터의 내용은 다음과 같습니다. ■부분은 개인별 가장 많이 처리된 개인정보유형을 알려줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>날짜</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>개인을 기준으로 개인정보가 처리된 이력들의 날짜를 출력</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>개인을 기준으로 개인정보가 처리된 이력들의 사용자 이름을 출력</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:71;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>개인정보</SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>유형</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:71;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-1%'>개인을 기준으로 각각의 개인정보유형인 주민등록번호, 외국인등록번호,</SPAN><SPAN STYLE='font-family:"맑은 고딕"'> </SPAN><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-1%'>운전면허번호, 여권번호, 신용카드번호, 건강보험번호, 전화번호, 이메일,</SPAN><SPAN STYLE='font-family:"맑은 고딕"'> 휴대폰번호, 계좌번호의 처리된 이력들의 건수를 출력 </SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>10.4.1. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>개인별유형통계 조건검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp;개인별유형통계 조회 화면에서의 검색 조건은 기본적으로 일시, 사용자명, 기간선택(일별, 월별, 년별)이 있으며, 해당 조건을 입력 후 검색버튼을 클릭 시, 검색조건에 해당하는 개인별유형통계가 출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICDE57.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff00043.bmp
원본 그림의 크기: 가로 1801pixel, 세로 281pixel" width="567" height="89" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>일시</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>개인별 통계를 내기 위한 개인정보 처리 일시 설정</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>사용자 이름을 입력하여 통계 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>일별</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>당월 기준으로 1일 ~ 말일까지 개인별 개인정보유형통계 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:53;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>월별</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:53;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>올해 기준으로 1월 1일 ~ 해당년도 말일(12월 31일)까지 개인별 개인정보유형통계 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:53;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>년별</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:53;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-4%'>가장 초기에 처리된 연도의 이전년도부터 금년도까지의 각 연도의</SPAN><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-1%'> 개인별</SPAN><SPAN STYLE='font-family:"맑은 고딕"'> 개인정보유형통계 검색</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>10.5. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>점검보고서</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-5%;line-height:180%'>접속기록보고서에서는 관리자가 주요 개인정보처리시스템의 개인정보 접속기록을</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> 점검할 수 있도록 보고서를 생성할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>10.5.1. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>접속기록보고서 생성</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;접속기록보고서는 생성하고자 하는 보고서 분류와 기간을 선택하고 대상 시스템을 선택 후 보고서 생성 버튼을 눌러 생성할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICDE87.png" alt="그림입니다.
원본 그림의 이름: CLP00002178000b.bmp
원본 그림의 크기: 가로 955pixel, 세로 444pixel" width="555" height="249" vspace="0" hspace="0" border="0"><br>&nbsp;보고서 생성 클릭 시 팝업창이 생성되며 아래와 같은 개인정보 접속기록 보고서가 나타납니다. 출력이 필요한 경우 오른쪽 상단의 출력 버튼을 눌러 PC와 연결된 프린터를 통해서 출력이 가능합니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICDEB6.png" alt="그림입니다.
원본 그림의 이름: CLP00002178000c.bmp
원본 그림의 크기: 가로 827pixel, 세로 821pixel" width="567" height="569" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle3 STYLE='line-height:180%;'>11. <SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>정책설정관리</SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>11.1. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>개인정보설정</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;개인정보설정에서는 개인정보유형 리스트가 출력됩니다. 개인정보설정이 필요한 개인정보유형을 클릭하면 개인정보설정 상세페이지가 출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICDF05.png" alt="그림입니다.
원본 그림의 이름: CLP00002178000f.bmp
원본 그림의 크기: 가로 951pixel, 세로 438pixel" width="567" height="245" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>11.1.1. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>개인정보설정 상세</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.4pt;text-indent:-5.5pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;개인정보설정상세에서는 개인정보유형에 대한 수집 사용여부를 설정할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:15.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-family:"맑은 고딕";color:#7f7fff'><IMG src="${rootPath}/resources/image/help/master/PICDF26.png" alt="그림입니다.
원본 그림의 이름: CLP00001c147aba.bmp
원본 그림의 크기: 가로 958pixel, 세로 230pixel" width="567" height="123" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>11.2. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>위험도 기준설정</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;위험도기준설정에서는 비정상행위 위험도에 대한 5단계에 대한 위험지수를 설정할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICDF65.png" alt="그림입니다.
원본 그림의 이름: CLP00001ff00044.bmp
원본 그림의 크기: 가로 1610pixel, 세로 939pixel" width="567" height="331" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>11.3. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>예외처리DB</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-6%;line-height:180%'>개인정보접속기록에 생성된 로그를 기준으로 관리자가 판단 시 해당 개인정보가</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-4%;line-height:180%'>실제 개인정보가 아니라고 판단되는 경우 지속적으로 검출되는 것을 차단하기 위해 예외처리DB로 등록하여</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'> 추후에 개인정보로 검출되지 않도록 설정할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICDFD3.png" alt="그림입니다.
원본 그림의 이름: CLP000021780011.bmp
원본 그림의 크기: 가로 1905pixel, 세로 889pixel" width="563" height="247" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>11.3.1. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>예외처리DB 검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp;예외처리</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-7%;line-height:180%'>DB에 등록되어 있는 개인정보를 조회하기 위한 화면입니다.(예외처리DB 메뉴 상단)</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> 조회하고자 하는 개인정보유형 및 해당 개인정보 입력 후 검색 버튼으로 조회할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE032.png" alt="그림입니다.
원본 그림의 이름: CLP000021780012.bmp
원본 그림의 크기: 가로 1799pixel, 세로 273pixel" width="568" height="90" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>11.3.2. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>예외처리DB 등록</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp;예외처리</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-5%;line-height:180%'>DB에 개인정보를 등록하고자 할 때 사용하는 화면입니다.(예외처리DB 메뉴 중단)</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>등록하고자 하는 개인정보유형을 선택하고 해당 개인정보 입력 후 추가 버튼으로</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> 등록할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE081.png" alt="그림입니다.
원본 그림의 이름: CLP000008d40003.bmp
원본 그림의 크기: 가로 1775pixel, 세로 146pixel" width="567" height="52" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>11.3.3. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>예외처리DB 조회</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp;예외처리</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-9%;line-height:180%'>DB에 등록되어 있는 개인정보를 확인할 수 있는 화면입니다.(예외처리DB 메뉴 하단)</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> 상단 검색을 통해 조회 시 해당 조건에 맞는 개인정보를 조회할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE0B1.png" alt="그림입니다.
원본 그림의 이름: CLP000008d40005.bmp
원본 그림의 크기: 가로 1769pixel, 세로 259pixel" width="567" height="83" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>11.3.4. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>예외처리DB 삭제</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp;예외처리</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-9%;line-height:180%'>DB에 등록되어 있는 개인정보를 삭제할 수 있는 화면입니다.(예외처리DB 메뉴 하단) </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-4%;line-height:180%'>예외처리DB에 잘못 등록되었거나 해당 개인정보에 대해 다시 검색을 원할 경우 삭제를</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> 통해 다시 검색할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE0F0.png" alt="그림입니다.
원본 그림의 이름: CLP000008d40006.bmp
원본 그림의 크기: 가로 1770pixel, 세로 265pixel" width="567" height="85" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle3 STYLE='line-height:180%;'>12. <SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>환경설정</SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>12.1. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>메뉴관리</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;메뉴관리 페이지를 들어가면 관리자 메뉴 정보 확인 및 수정이 가능합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE13F.png" alt="그림입니다.
원본 그림의 이름: CLP000021780013.bmp
원본 그림의 크기: 가로 1917pixel, 세로 807pixel" width="565" height="222" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;메뉴 관리의 세부 항목 내역은 다음과 같습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;① 사용자 메뉴명을 입력합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;② 하위 디렉토리를 설정합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;③ 정렬 순서를 설정합니다.(좌측으로부터 정렬)</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;④ 부모메뉴명을 선택하여 메뉴 위치를 설정합니다.(ROOT = 최상위 메뉴)</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;⑤ 사용 여부를 설정합니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;⑥ 메뉴설명을 입력할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;⑦ 권한대상자 중에서 해당 메뉴에 대한 권한 설정을 할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>12.2. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>권한관리</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;권한관리 페이지를 들어가면 관리자 계정 사용여부 확인 및 권한 설정이 가능합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE1CC.png" alt="그림입니다.
원본 그림의 이름: CLP000021780014.bmp
원본 그림의 크기: 가로 1920pixel, 세로 782pixel" width="567" height="231" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>12.2.1. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>권한관리 기능</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:1%;line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>권한관리 계정 중 하나를 클릭하면 계정 사용여부 및 권한 설정(관리자명을</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:1%;line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>입력)이 가능합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE23B.png" alt="그림입니다.
원본 그림의 이름: CLP000021780015.bmp
원본 그림의 크기: 가로 1920pixel, 세로 396pixel" width="574" height="102" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>12.2.2. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>엑셀다운로드</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;권한관리 화면에서 엑셀다운로드 버튼을 클릭하게 되면 권한관리리스트에 나타난 내용에 대해 EXCEL로 다운받을 수 있는 기능입니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE27A.png" alt="그림입니다.
원본 그림의 이름: CLP000021780017.bmp
원본 그림의 크기: 가로 954pixel, 세로 390pixel" width="553" height="213" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:10.8pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:15.0pt;line-height:180%;'><SPAN STYLE='font-size:10.8pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>.xls 형태로 저장되며 “권한ID”, “권한명”, &quot;사용여부”의 정보를 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE2AA.png" alt="그림입니다.
원본 그림의 이름: CLP000012f0004a.bmp
원본 그림의 크기: 가로 670pixel, 세로 251pixel" width="567" height="202" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>12.3. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>조직관리</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:1%;line-height:180%'>환경설정의 조직관리를 들어가면 해당 기관에 등록되어 있는 소속 정보를 </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>확인할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE2CA.png" alt="그림입니다.
원본 그림의 이름: CLP000010d00019.bmp
원본 그림의 크기: 가로 1376pixel, 세로 427pixel" width="562" height="134" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>12.4. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>사용자관리</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;환경설정의 사용자관리 페이지를 들어가면 해당 기관에 등록되어있는 사용자들의 정보를 확인할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE309.png" alt="그림입니다.
원본 그림의 이름: CLP000021780019.bmp
원본 그림의 크기: 가로 1902pixel, 세로 777pixel" width="563" height="217" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>12.4.1. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>사용자관리 조건검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp;사용자관리 화면에서의 검색 조건은 기본적으로 사용자명, 사용자ID, 상태, 소속명, </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:1%;line-height:180%'>하위소속포함, 대상서버가 있으며, 해당 조건을 입력 후 검색버튼을 클릭 시, </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>검색조건에 해당하는 사용자 정보에 대한 이력이 출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE378.png" alt="그림입니다.
원본 그림의 이름: CLP00002178001a.bmp
원본 그림의 크기: 가로 1811pixel, 세로 315pixel" width="567" height="99" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:140%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:140%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>해당 업체에 등록되어 있는 사용자명으로 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>사용자ID</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>해당 업체에 등록되어 있는 사용자ID로 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>상태</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>재직, 퇴직, 휴직, 휴가에 대한 사용자의 상태로 검색 </SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>소속</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>해당 업체에 등록되어 있는 소속으로 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:50;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>하위소속<BR>포함</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:50;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>하위 소속까지 소속 항목에 보이도록 설정(체크여부)</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>대상서버</SPAN></P>
	</TD>
	<TD valign="middle" style='width:451;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>해당 업체에 등록되어 있는 시스템명으로 검색</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>12.4.2. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>사용자관리상세</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;사용자 관리 화면에서 각각의 사용자정보를 클릭하게 되면 클릭한 사용자에 대한 세부정보가 나타나게 됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE398.png" alt="그림입니다.
원본 그림의 이름: CLP00002178001b.bmp
원본 그림의 크기: 가로 942pixel, 세로 395pixel" width="554" height="224" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:35.4pt;text-indent:-5.5pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;아래 그림에서 보면 사용자 관리 페이지에서 클릭한 사용자에 대한 세부 정보를 볼 수 있으며 사용자명, 소속명, 사용자ID, 시스템명, 상태, 사용자IP, E-MAIL, 핸드폰번호, 등록일시, 수정일시와 같은 내용들을 볼 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";color:#0000ff;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE3C8.png" alt="그림입니다.
원본 그림의 이름: CLP000010d0001a.bmp
원본 그림의 크기: 가로 1445pixel, 세로 329pixel" width="554" height="120" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle6 STYLE='line-height:180%;'>12.4.2.1. <SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>사용자상세 내용 수정</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:36.4pt;text-indent:-16.4pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-5%;line-height:180%'>사용자상세 화면에서 해당 사용자 정보에 대한 내용을 수정할 때 수정 버튼을 누르면</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-5%;line-height:180%'>수정됩니다. 이때, 수정할 수 있는 내용으로는 사용자명, 소속명, 상태, 사용자IP가 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE3E8.png" alt="그림입니다.
원본 그림의 이름: CLP000010d0001b.bmp
원본 그림의 크기: 가로 1430pixel, 세로 328pixel" width="554" height="119" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle6 STYLE='line-height:180%;'>12.4.2.2. <SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>사용자상세 내용 삭제</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:41.9pt;text-indent:-21.9pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-5%;line-height:180%'>사용자상세 화면에서 해당 사용자 정보에 대한 내용을 삭제할 때 삭제 버튼을 누르면</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> 사용자 정보가 삭제되는 것을 확인할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE418.png" alt="그림입니다.
원본 그림의 이름: CLP000010d0001c.bmp
원본 그림의 크기: 가로 1440pixel, 세로 331pixel" width="554" height="118" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>12.4.3. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>사용자관리 신규 등록</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-12%;line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-7%;line-height:180%'>사용자관리 상세 화면에서 사용자을 등록하여 개인정보 탐지에 있어 사용자 정보를 자세히</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:2%;line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-9%;line-height:180%'>확</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-8%;line-height:180%'>인하고 싶을 때 신규 버튼을 클릭하면 사용자 신규 등록 화면이 나오게 됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE447.png" alt="그림입니다.
원본 그림의 이름: CLP00002178001c.bmp
원본 그림의 크기: 가로 949pixel, 세로 389pixel" width="554" height="221" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-12%;line-height:180%'>&nbsp;사용자 신규 등록 화면에서 사용자명, 소속명, 사용자ID, 상태, 사용자IP를 입력하여 사용자정보를 추가해줄 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE468.png" alt="그림입니다.
원본 그림의 이름: CLP000010d0001d.bmp
원본 그림의 크기: 가로 1431pixel, 세로 362pixel" width="548" height="130" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>12.4.4. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>엑셀업로드</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp;사용자관리 화면에서 엑셀버튼을 클릭하게 되면 업로드 기능을 사용할 수 있습니다. 해당 기능을 통해 엑셀로 작성한 사용자리스트를 한 번에 등록할 수 있습니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE497.png" alt="그림입니다.
원본 그림의 이름: CLP000016400003.bmp
원본 그림의 크기: 가로 1829pixel, 세로 307pixel" width="567" height="95" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp;업로드 버튼을 선택하면 다음과 같은 팝업창이 출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE4C7.png" alt="그림입니다.
원본 그림의 이름: CLP000016400005.bmp
원본 그림의 크기: 가로 540pixel, 세로 207pixel" width="461" height="177" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'>해당 팝업창의 양식다운로드 버튼을 클릭하면 사용자 리스트를 작성할 엑셀 양식을 다운받을 수 있습니다. 사용자관리 업로드 양식은 다음과 같으며, “소속”, “사용자명”, “사용자ID”, “사용자IP”, “상태”, “E-MAIL”, “핸드폰번호” 정보를 입력할 수 있습니다. 필수 입력항목은 “소속”, “사용자명”, “사용자ID”, “상태”이며 “상태”는 재직, 퇴직, 휴직, 휴가 중 하나를 선택하여 입력하거나 공백으로 처리 가능합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE4E7.png" alt="그림입니다.
원본 그림의 이름: CLP000016400008.bmp
원본 그림의 크기: 가로 1729pixel, 세로 458pixel" width="567" height="150" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;해당 엑셀파일에 사용자정보를 입력하여 엑셀업로드 버튼을 누르면 사용자관리 리스트에 등록됩니다.</SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>12.4.5. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>엑셀다운로드</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp;사용자관리 화면에서 엑셀 버튼을 클릭하게 되면 다운로드 기능을 사용할 수 있습니다. 사용자관리 화면에서 출력한 로그들을 EXCEL로 다운받을 수 있는 기능입니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE517.png" alt="그림입니다.
원본 그림의 이름: CLP000016400004.bmp
원본 그림의 크기: 가로 1829pixel, 세로 307pixel" width="563" height="95" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;.xls 형태로 저장되며 “소속”, “사용자명”, &quot;사용자ID”, &quot;시스템명”, &quot;사용자IP”, &quot;상태”의 정보를 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE547.png" alt="그림입니다.
원본 그림의 이름: CLP000010d0001f.bmp
원본 그림의 크기: 가로 1440pixel, 세로 389pixel" width="567" height="153" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>12.5. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>관리자관리</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>환경설정의 관리자관리 페이지를 들어가면 PSM Management 로그인 관리자의</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> 정보를 확인할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE586.png" alt="그림입니다.
원본 그림의 이름: CLP00000cb80001.bmp
원본 그림의 크기: 가로 1349pixel, 세로 513pixel" width="552" height="200" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>12.5.1. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>관리자관리 조건검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;관리자관리 화면에서의 검색 조건은 기본적으로 관리자ID, 관리자명, 소속이 있으며, 해당 조건을 입력 후 검색버튼을 클릭 시, 검색조건에 해당하는 관리자 정보에 대한 이력이 출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE5C6.png" alt="그림입니다.
원본 그림의 이름: CLP000021780023.bmp
원본 그림의 크기: 가로 1816pixel, 세로 279pixel" width="567" height="87" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:104;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>관리자ID</SPAN></P>
	</TD>
	<TD valign="middle" style='width:429;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>해당 업체에 등록되어 있는 관리자ID로 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:104;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>관리자명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:429;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>해당 업체에 등록되어 있는 관리자명으로 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:104;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>소속</SPAN></P>
	</TD>
	<TD valign="middle" style='width:429;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>해당 업체에 등록되어 있는 소속으로 검색 </SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>12.5.2. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>관리자관리 상세</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>&nbsp;관리자관리 화면에서 각각의 관리자명을 클릭하게 되면 클릭한 관리자에 대한</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> 세부정보가 나타나게 됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE5E6.png" alt="그림입니다.
원본 그림의 이름: CLP00000cb80003.bmp
원본 그림의 크기: 가로 673pixel, 세로 258pixel" width="553" height="202" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>아래 그림에서 보면 관리자관리 페이지에서 조회한 로그에 대한 세부 정보를 볼 수 있으며 관리자ID, 관리자명, 비밀번호, 소속명, 권한명, 연락처, E-MAIL, 관리자설명, 등록일시, 수정일시와 같은 내용들을 볼 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE625.png" alt="그림입니다.
원본 그림의 이름: CLP000021780025.bmp
원본 그림의 크기: 가로 1918pixel, 세로 811pixel" width="553" height="204" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle6 STYLE='line-height:180%;'>12.5.2.1. <SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>관리자상세 내용 수정</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>관리자상세 화면에서 해당 관리자 정보에 대한 내용을 수정할 때 수정 버튼을</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-4%;line-height:180%'>누르면 수정됩니다.</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'> 이때, 수정할 수 있는 내용으로는 관리자명, 비밀번호, 소속명,</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> 권한명, 연락처, E-MAIL, 관리자설명이 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE674.png" alt="그림입니다.
원본 그림의 이름: CLP000021780027.bmp
원본 그림의 크기: 가로 948pixel, 세로 405pixel" width="553" height="228" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle6 STYLE='line-height:180%;'>12.5.2.2. <SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>관리자상세 내용 삭제</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>관리자상세 화면에서 해당 관리자 정보에 대한 내용을 삭제할 때 삭제 버튼을</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> 누르면 관리자 정보가 삭제되는 것을 확인할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE6A4.png" alt="그림입니다.
원본 그림의 이름: CLP000021780028.bmp
원본 그림의 크기: 가로 953pixel, 세로 406pixel" width="553" height="227" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>12.5.3. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>관리자관리 신규 등록</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-5%;line-height:180%'>관리자관리 화면에서 관리자를 등록하여 PSM Management Tool 관리에 필요한</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-8%;line-height:180%'>권한을 주고 싶을 때 신규 버튼을 클릭하면 관리자상세 등록 화면이 나오게 됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-5%;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE6C4.png" alt="그림입니다.
원본 그림의 이름: CLP00002178002d.bmp
원본 그림의 크기: 가로 959pixel, 세로 378pixel" width="554" height="208" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'><SPAN style='HWP-TAB:1;'>&nbsp;</SPAN> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-4%;line-height:180%'>관리자상세 등록 화면에서 관리자ID, 관리자명, 비밀번호, 비밀번호확인, 소속명, 권한명, 연락처, E-MAIL, 알림여부, 관리자설명을 입력하여 사용자정보를 추가해줄 수 있습니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE713.png" alt="그림입니다.
원본 그림의 이름: CLP00002178002a.bmp
원본 그림의 크기: 가로 1917pixel, 세로 771pixel" width="554" height="212" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>12.5.4. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>엑셀다운로드</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'><SPAN style='HWP-TAB:1;'>&nbsp;</SPAN>&nbsp;관리자관리 화면에서 엑셀다운로드 버튼을 클릭하게 되면 관리자관리 화면에서</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> 출력한 로그들을 EXCEL로 다운받을 수 있는 기능입니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE762.png" alt="그림입니다.
원본 그림의 이름: CLP00000cb80004.bmp
원본 그림의 크기: 가로 673pixel, 세로 256pixel" width="552" height="202" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.9pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-12%;line-height:180%'>.xls 형태로 저장되며 “관리자ID”, “관리자명”, &quot;소속”, &quot;E-MAIL”, &quot;권한명“</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'>의 정보를 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";color:#ff0000;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE783.png" alt="그림입니다.
원본 그림의 이름: CLP00000cb80005.bmp
원본 그림의 크기: 가로 1051pixel, 세로 199pixel" width="567" height="108" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>12.6. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>감사이력</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;환경설정의 감사이력 페이지는 관리자가 UBI SAFER-PSM에 접근하여 어떠한 행위를 하였는지 확인할 수 있는 메뉴입니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE7B2.png" alt="그림입니다.
원본 그림의 이름: CLP000010d00020.bmp
원본 그림의 크기: 가로 1442pixel, 세로 537pixel" width="556" height="201" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>12.6.1. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>감사이력 조건검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;감사이력 화면에서의 검색 조건은 일시, 관리자ID, 관리자명이 있으며, 해당 </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:1%;line-height:180%'>조건을 선택 및 입력 후 검색버튼 클릭 시 검색조건에 해당하는 감사이력이 </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE7E2.png" alt="그림입니다.
원본 그림의 이름: CLP000010d00021.bmp
원본 그림의 크기: 가로 1380pixel, 세로 207pixel" width="567" height="85" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:104;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>일시</SPAN></P>
	</TD>
	<TD valign="middle" style='width:429;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>해당 일시의 이력 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:104;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>관리자ID</SPAN></P>
	</TD>
	<TD valign="middle" style='width:429;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>해당 업체에 등록되어 있는 관리자ID로 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:104;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>관리자명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:429;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"맑은 고딕"'>해당 업체에 등록되어 있는 관리자명으로 검색</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>12.6.2. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>엑셀다운로드</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;감사이력 화면에서 엑셀다운로드 버튼을 클릭하게 되면 감사이력에 나타난 내용에 대해 EXCEL로 다운받을 수 있는 기능입니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE802.png" alt="그림입니다.
원본 그림의 이름: CLP000010d00022.bmp
원본 그림의 크기: 가로 1386pixel, 세로 212pixel" width="567" height="87" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-4%;line-height:180%'>&nbsp;.xls 형태로 저장되며 “관리자ID”, “관리자명”, “IP”, “일시”, “분류”, “로그메세지”, “로그종류”의 정보를 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE823.png" alt="그림입니다.
원본 그림의 이름: CLP000021780031.bmp
원본 그림의 크기: 가로 1470pixel, 세로 335pixel" width="567" height="128" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>12.7. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>시스템관리</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:10.9pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:10.9pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'>시스템관리 화면에서 각각의 개인정보기록을 검출할 시스템별 설정에 관한 정보</SPAN><SPAN STYLE='font-size:10.9pt;font-family:"맑은 고딕";line-height:180%'> 확인 및 수정이 가능합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:10.9pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE852.png" alt="그림입니다.
원본 그림의 이름: CLP000008d40027.bmp
원본 그림의 크기: 가로 1878pixel, 세로 511pixel" width="554" height="144" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:0.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;세부 항목 내역은 다음과 같습니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:5.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;① 시스템명을 입력합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:5.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;② 정렬 순서를 설정합니다.(상단으로부터 정렬)</SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:5.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;③ 시스템 타입을 설정합니다.(default: BIZ_LOG)</SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:5.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;④ URL주소 및 하위 디렉토리를 설정합니다. (http:// [해당IP] : 포트번호/)</SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:15.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:5.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>12.8. <SPAN STYLE='font-size:16.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>접속허용 IP</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:10.9pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:10.9pt;font-family:"맑은 고딕";letter-spacing:-4%;line-height:180%'>접속허용 IP 화면에서는 관리자 페이지로 접속하는 IP주소를 제한할 수 있습니다.</SPAN><SPAN STYLE='font-size:10.9pt;font-family:"맑은 고딕";line-height:180%'> (※해당 기능이 활성화되면, 허용 IP를 제외한 모든 주소가 접근이 불가능하게 되오니 주의 바랍니다.)</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:10.9pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE8A1.png" alt="그림입니다.
원본 그림의 이름: CLP000008d4002d.bmp
원본 그림의 크기: 가로 1877pixel, 세로 419pixel" width="553" height="109" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>12.8.1. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>접속허용IP 생성</SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;IP대역관리 화면에서 “신규”버튼을 클릭합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE8E1.png" alt="그림입니다.
원본 그림의 이름: CLP000008d4002c.bmp
원본 그림의 크기: 가로 1880pixel, 세로 415pixel" width="567" height="81" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-5%;line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>접속을 허용할 IP를 입력하고 “사용”을 선택한 후 “등록” 버튼을 클릭함으로</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> 허용된 IP만 접속에 가능하게 설정합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE911.png" alt="그림입니다.
원본 그림의 이름: CLP000008d40037.bmp
원본 그림의 크기: 가로 1894pixel, 세로 474pixel" width="567" height="124" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>허용된 IP이외의 주소에서 접근하면 아래와 같은 접근거부 페이지가 나옵니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE950.png" alt="그림입니다.
원본 그림의 이름: CLP000008d4002f.bmp
원본 그림의 크기: 가로 1116pixel, 세로 215pixel" width="575" height="109" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>12.8.2. <SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>접속허용IP 삭제 및 비활성화</SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;접속이 허용된 IP로 접속하여 해당 IP를 클릭합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE970.png" alt="그림입니다.
원본 그림의 이름: CLP000008d40035.bmp
원본 그림의 크기: 가로 1896pixel, 세로 478pixel" width="558" height="125" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>해당 IP 사용을 “미사용” 또는 삭제를 클릭하여 해당 기능을 비활성화합니다.</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'> </SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PICE9B0.png" alt="그림입니다.
원본 그림의 이름: CLP000008d40031.bmp
원본 그림의 크기: 가로 1897pixel, 세로 536pixel" width="550" height="143" vspace="0" hspace="0" border="0">&nbsp;</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle3 STYLE='line-height:180%;'>13. <SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>기술지원 문의</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:10.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-5%;line-height:180%'> </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-8%;line-height:180%'>온라인상이나 설명서로도 문제를 해결할 수 없을 시에는 (주)이지서티의 기술지원</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'> 센터로 연락하시기 바랍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-10.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&#8226; 전화번호 : (02) 865 - 5577</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&#8226; 팩스번호 : (02) 6942 - 9999</SPAN></P>

</BODY>
</HTML>
