<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>

<HEAD>
<META NAME="Generator" CONTENT="Hancom HWP 9.0.0.562">
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=euc-kr">
<TITLE>UBI SAFER-PSM 관리자 메뉴얼</TITLE>
<STYLE type="text/css">
<!--
p.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle1
	{style-name:"그림제목"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:9.0pt; font-family:휴먼명조; letter-spacing:0; font-weight:"bold"; font-style:"normal"; color:#000000;}
li.HStyle1
	{style-name:"그림제목"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:9.0pt; font-family:휴먼명조; letter-spacing:0; font-weight:"bold"; font-style:"normal"; color:#000000;}
div.HStyle1
	{style-name:"그림제목"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:9.0pt; font-family:휴먼명조; letter-spacing:0; font-weight:"bold"; font-style:"normal"; color:#000000;}
p.HStyle2
	{style-name:"본문"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:11.0pt; font-family:휴먼명조; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle2
	{style-name:"본문"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:11.0pt; font-family:휴먼명조; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle2
	{style-name:"본문"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:11.0pt; font-family:휴먼명조; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle3
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle3
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle3
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle4
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle4
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle4
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle5
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle5
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle5
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle6
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle6
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle6
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle7
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle7
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle7
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle8
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle8
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle8
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle9
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle9
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle9
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle10
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle10
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle10
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle11
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle11
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle11
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle12
	{style-name:"각주"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:160%; font-size:9.0pt; font-family:휴먼명조; letter-spacing:0; font-weight:"bold"; font-style:"normal"; color:#000000;}
li.HStyle12
	{style-name:"각주"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:160%; font-size:9.0pt; font-family:휴먼명조; letter-spacing:0; font-weight:"bold"; font-style:"normal"; color:#000000;}
div.HStyle12
	{style-name:"각주"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:160%; font-size:9.0pt; font-family:휴먼명조; letter-spacing:0; font-weight:"bold"; font-style:"normal"; color:#000000;}
p.HStyle13
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle13
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle13
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle14
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle14
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle14
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle15
	{style-name:"MS바탕글"; margin-top:3.0pt; margin-bottom:2.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:10.0pt; font-family:굴림체; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle15
	{style-name:"MS바탕글"; margin-top:3.0pt; margin-bottom:2.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:10.0pt; font-family:굴림체; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle15
	{style-name:"MS바탕글"; margin-top:3.0pt; margin-bottom:2.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:10.0pt; font-family:굴림체; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle16
	{style-name:"내용"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:11.0pt; font-family:돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle16
	{style-name:"내용"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:11.0pt; font-family:돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle16
	{style-name:"내용"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:11.0pt; font-family:돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle17
	{style-name:"그림 설명 스타일"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:160%; font-size:11.0pt; font-family:돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle17
	{style-name:"그림 설명 스타일"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:160%; font-size:11.0pt; font-family:돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle17
	{style-name:"그림 설명 스타일"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:160%; font-size:11.0pt; font-family:돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle18
	{style-name:"표 설명 스타일"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:11.0pt; font-family:돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle18
	{style-name:"표 설명 스타일"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:11.0pt; font-family:돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle18
	{style-name:"표 설명 스타일"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:11.0pt; font-family:돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
-->
</STYLE>
</HEAD>

<BODY>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<DIV STYLE=''>

<P CLASS=HStyle0 STYLE='margin-left:38.0pt;text-align:center;text-indent:-38.0pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC2B46.gif" alt="" width="449" height="192" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:38.0pt;text-align:center;text-indent:-38.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>&nbsp;</SPAN></P>

</DIV>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<DIV STYLE=''>

<P CLASS=HStyle0 STYLE='margin-left:38.0pt;text-align:center;text-indent:-38.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:38.0pt;text-align:center;text-indent:-38.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>&nbsp;</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:38.0pt;text-align:center;text-indent:-38.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:38.0pt;text-align:center;text-indent:-38.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

</DIV>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC2B47.png" alt="그림입니다.
원본 그림의 이름: 이지서티_[가로형].jpg
원본 그림의 크기: 가로 1000pixel, 세로 173pixel
사진 찍은 날짜: 2015년 11월 09일 오후 2:23" width="296" height="49" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<DIV STYLE=''>

<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>설명서 소개</SPAN></P>

<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:160%'>본 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:160%'>UBI SAFER-PSM V3.0 관리자매뉴얼은 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:160%'>제품 설치 시 (주)이지서티로부터 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-14%;line-height:160%'>본 제품의 안전한 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-16%;line-height:160%'>운용을 위해 지침에 따라 교육 받은 보안 관리자(최상위 관리자)가</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:160%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-7%;line-height:160%'>UBI SAFER-PSM V3.0에서 제공하는</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:160%'> 보안기능을 활용하는 방법을 소개합니다. </SPAN></P>

<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>저작권</SPAN></P>

<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-6%;line-height:160%'>본 설명서에 대한 저작권과 지적 소유권은 (주)이지서티가 가지고 있으며, </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-7%;line-height:160%'>이 권리는 대한민국의</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:160%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-7%;line-height:160%'>저작권법과 국제 저작권 조약에 의하여 보호됩니다.</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-6%;line-height:160%'> 따라서 (주)이지서티의 사전 서면 동의 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:160%'>없이</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:2%;line-height:160%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:160%'>본 설명서의 일부 혹은 전체 내용을 무단 복사, 복제, 출판하는 것은 저작권법에</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:160%'> 저촉됩니다.</SPAN></P>

<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>내용변경</SPAN></P>

<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:160%'>본 설명서의 내용은 추후 제품의 기능 향상 및 (주)이지서티가 정한 목적에</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'> 따라 사전 예고 없이 변경될 수 있습니다. </SPAN></P>

<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>등록상표</SPAN></P>

<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:160%'>㈜이지서티의 로고는 대한민국과 다른 여러 나라에 등록이 된 ㈜이지서티의</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:2%;line-height:160%'> 상표입니다. </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:160%'>㈜이지서티의 서면 동의 없이 상업적 목적을 위하여 EASYCERTI 로고를 사용할 경우 상표권 침해와 불공정 경쟁행위가 됩니다.</SPAN></P>

<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>본 설명서의 정보가 정확하도록 ㈜이지서티는 모든 노력을 기울였습니다. ㈜이지서티는 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:160%'>인쇄 오류나 오타 및 오기에 대해서</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:160%'>는 책임을 지지 않습니다. </SPAN></P>

</DIV>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:180%'>이 설명서에서 언급하는 모든 제품명은 자사의 등록 상표</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>이며,</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:5%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>시스템에 적용되어있는 데이터는 사용자의 이해를 돕기 위한 가상의 데이터를 활용한</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:180%'> 것입니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<DIV STYLE=''>

<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%;'>제품에 이상이 있는 경우 아래의 연락처로 연락 주십시오.</SPAN></P>
<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<TR>
	<TD valign="middle" style='width:356;height:99;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>우편번호 : 08377</SPAN></P>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>주소 : 서울시 구로구 디지털로33길 48 9층 </SPAN></P>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>전화 : +82-2-865-5577</SPAN></P>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>팩스 : +82-2-6942-9999</SPAN></P>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>홈페이지 : </SPAN><A HREF="http://www.easycerti.com" TARGET="_self"><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";color:#000000;line-height:160%'>http://www.easycerti.com</SPAN></A></P>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>고객만족센터</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:20.0pt;text-align:left;text-indent:-5.9pt;line-height:130%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:130%'>Tel: +82-70-7784-8233</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:20.0pt;text-align:left;text-indent:-5.9pt;line-height:130%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:130%'>E-mail: </SPAN><A HREF="mailto:easycerti@easycerti.com"><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";text-decoration:"none";color:#000000;line-height:130%'>support@easycerti.com</SPAN></A></P>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>
	</TD>
</TR>
</TABLE>
<P style="text-align: center;" CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%;'>Copyright ⓒ 2017 EASYCERTI. All rights reserved.</SPAN></P>

</DIV>

<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>&lt;목</SPAN><SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'> 차&gt;</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:13.7pt;text-indent:-13.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>들어가는 글<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------------------&nbsp;&nbsp;</SPAN>1</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:13.7pt;text-indent:-13.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>1. 개요<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--------------------------------------------------------------------&nbsp;&nbsp;</SPAN>2</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>1.1. UBI SAFER-PSM이란?<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------&nbsp;&nbsp;</SPAN>2</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>1.2. UBI SAFER-PSM 구축 배경<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;-----------------------------------------------&nbsp;&nbsp;</SPAN>2</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>1.3. UBI SAFER-PSM의 목적<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;----------------------------------------------------&nbsp;&nbsp;</SPAN>5</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>1.4. UBI SAFER-PSM의 목표<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;----------------------------------------------------&nbsp;&nbsp;</SPAN>5</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>1.5. 기대효과<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;--------------------------------------------------------------------&nbsp;&nbsp;</SPAN>5</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:13.7pt;text-indent:-13.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>2. UBI SAFER-PSM 주요특징<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--------------------------------------------&nbsp;&nbsp;</SPAN>6</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>2.1. UBI SAFER-PSM 주요 기능<SPAN style='HWP-TAB:1;'>&nbsp;--------------------------------------------------&nbsp;&nbsp;</SPAN>6</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>2.1.1. 비정상행위 모니터링 및 위험분석 기능<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;------------------------------&nbsp;&nbsp;</SPAN>6</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>2.1.2. 접속기록 및 접근이력 관리 기능<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;--------------------------------------&nbsp;&nbsp;</SPAN>6</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>2.1.3. 통계 및 보고 기능<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;-----------------------------------------------------&nbsp;&nbsp;</SPAN>6</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>2.1.4. 정책설정 및 환경설정 기능<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;-------------------------------------------&nbsp;&nbsp;</SPAN>6</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>2.2. UBI SAFER-PSM 특장점<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;---------------------------------------------------&nbsp;&nbsp;</SPAN>7</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>2.2.1. 법규/지침 준수<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;--------------------------------------------------------&nbsp;&nbsp;</SPAN>7</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>2.2.2. 공인인증기관을 통한 로그 검출 정확도 보장<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;------------------------&nbsp;&nbsp;</SPAN>7</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>2.2.3. 실시간 처리(In-Memory 기술)<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;----------------------------------------&nbsp;&nbsp;</SPAN>7</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>2.2.4. 이상징후 모니터링 기술<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;----------------------------------------------&nbsp;&nbsp;</SPAN>7</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>2.2.5. 기존 시스템 및 네트워크 환경 변화 없는 솔루션 구축<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;--------------&nbsp;&nbsp;</SPAN>7</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>2.2.6. 기 운영 중인 시스템과의 호환성<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;-------------------------------------&nbsp;&nbsp;</SPAN>7</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>2.2.7. 다양한 접속기록 생성 기술<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;-------------------------------------------&nbsp;&nbsp;</SPAN>8</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:30.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>2.2.7.1. AGENT방식<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;--------------------------------------------------------&nbsp;&nbsp;</SPAN>8</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:30.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>2.2.7.2. AGENTLESS방식<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;---------------------------------------------------&nbsp;&nbsp;</SPAN>9</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:13.7pt;text-indent:-13.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>3. 설치<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;---------------------------------------------------------------------&nbsp;&nbsp;</SPAN>10</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>3.1. 구성 내역<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-------------------------------------------------------------------&nbsp;&nbsp;</SPAN>10</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>3.2. 설치 구성<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-------------------------------------------------------------------&nbsp;&nbsp;</SPAN>11</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>3.3. 시스템 권장사양<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------------&nbsp;&nbsp;</SPAN>12</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>3.4. 관리자 프로그램 설치<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------&nbsp;&nbsp;</SPAN>12</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:13.7pt;text-indent:-13.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>4. UBI SAFER-PSM 시작하기<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------&nbsp;&nbsp;</SPAN>13</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>4.1. 시스템 로그인<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;---------------------------------------------------------------&nbsp;&nbsp;</SPAN>13</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>4.2. 시스템 로그아웃<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------------&nbsp;&nbsp;</SPAN>17</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:13.7pt;text-indent:-13.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>5. 개인정보 접속기록조회<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;---------------------------------------------------&nbsp;&nbsp;</SPAN>19</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>5.1. 접속기록조회<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;--------------------------------------------------------------&nbsp;&nbsp;</SPAN>19</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>5.1.1. 접속기록 조건검색<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;----------------------------------------------------&nbsp;&nbsp;</SPAN>20</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>5.1.2. 접속기록 로그 상세<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;---------------------------------------------------&nbsp;&nbsp;</SPAN>21</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>5.1.3. 엑셀다운로드<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;---------------------------------------------------------&nbsp;&nbsp;</SPAN>22</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>5.2. 백업이력 조회<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;-------------------------------------------------------------&nbsp;&nbsp;</SPAN>23</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>5.2.1. 백업이력 조건검색<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;----------------------------------------------------&nbsp;&nbsp;</SPAN>23</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>5.2.2. 엑셀다운로드<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;---------------------------------------------------------&nbsp;&nbsp;</SPAN>23</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>5.2.3. 위변조 확인<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;-----------------------------------------------------------&nbsp;&nbsp;</SPAN>24</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>5.3. 정보주체자 조회<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;------------------------------------------------------------&nbsp;&nbsp;</SPAN>25</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>5.3.1. 정보주체자 조건검색<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;---------------------------------------------------&nbsp;&nbsp;</SPAN>25</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>5.3.2. 정보주체자 로그 상세<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;-------------------------------------------------&nbsp;&nbsp;</SPAN>26</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>5.4. 다운로드 로그조회<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;---------------------------------------------------------&nbsp;&nbsp;</SPAN>27</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>5.4.1. 다운로드 로그 조건검색<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;-----------------------------------------------&nbsp;&nbsp;</SPAN>27</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>5.4.2. 다운로드 로그조회 상세<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;----------------------------------------------&nbsp;&nbsp;</SPAN>28</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:13.7pt;text-indent:-13.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>6. 시스템 접근이력조회<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-----------------------------------------------------&nbsp;&nbsp;</SPAN>30</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>6.1. 접근이력조회<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;--------------------------------------------------------------&nbsp;&nbsp;</SPAN>30</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>6.1.1. 접근이력 조건검색<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;----------------------------------------------------&nbsp;&nbsp;</SPAN>31</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>6.1.2. 접근이력 로그 상세<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;---------------------------------------------------&nbsp;&nbsp;</SPAN>32</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>6.1.3. 엑셀다운로드<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;----------------------------------------------------------&nbsp;&nbsp;</SPAN>33</SPAN></P>


<P CLASS=HStyle0 STYLE='margin-left:13.7pt;text-indent:-13.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>7. 모니터링<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------------------&nbsp;&nbsp;</SPAN>34</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>7.1. 대시보드<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;------------------------------------------------------------------&nbsp;&nbsp;</SPAN>34</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>7.1.1. 시스템별 접속기록 TOP10<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;--------------------------------------------&nbsp;&nbsp;</SPAN>35</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>7.1.2. 개인정보 접속기록 TOP10<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;--------------------------------------------&nbsp;&nbsp;</SPAN>35</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>7.1.3. 비정상행위 추이분석<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;-------------------------------------------------&nbsp;&nbsp;</SPAN>36</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>7.1.4. 개인정보 유형별 접속기록 현황<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;--------------------------------------&nbsp;&nbsp;</SPAN>36</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>7.1.5. 비정상행위별 분석현황<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;-----------------------------------------------&nbsp;&nbsp;</SPAN>37</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>7.1.6. 소속별 로그 수집 현황<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;------------------------------------------------&nbsp;&nbsp;</SPAN>37</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>7.1.7. 시스템 정보<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;-----------------------------------------------------------&nbsp;&nbsp;</SPAN>38</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>7.2. 토폴로지<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;------------------------------------------------------------------&nbsp;&nbsp;</SPAN>39</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>7.2.1. 토폴로지 조건검색<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;----------------------------------------------------&nbsp;&nbsp;</SPAN>39</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>7.2.2. 토폴로지 시스템 접속기록조회<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;--------------------------------------&nbsp;&nbsp;</SPAN>40</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:13.7pt;text-indent:-13.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>8. 빅데이터분석<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-------------------------------------------------------------&nbsp;&nbsp;</SPAN>41</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.1. 비정상위험분석<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------------&nbsp;&nbsp;</SPAN>41</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.1.1. 오·남용의심지수<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-------------------------------------------------------&nbsp;&nbsp;</SPAN>41</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.1.2. 오·남용의심행위<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;-------------------------------------------------------&nbsp;&nbsp;</SPAN>42</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.1.3. 오남용의심행위자증감<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-------------------------------------------------&nbsp;&nbsp;</SPAN>42</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.1.4. 부서별 TOP10 LIST<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-----------------------------------------------------&nbsp;&nbsp;</SPAN>43</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:30.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.1.4.1. 부서별 비정상위험분석 상세차트<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;----------------------------------&nbsp;&nbsp;</SPAN>43</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.1.5. 개인별 TOP10 LIST<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------&nbsp;&nbsp;</SPAN>44</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:30.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.1.5.1. 비정상위험분석 상세차트<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-------------------------------------------&nbsp;&nbsp;</SPAN>44</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.2. 비정상 위험분석 리스트<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-----------------------------------------------------&nbsp;&nbsp;</SPAN>45</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.2.1. 비정상행위 위험분석 리스트 화면<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;--------------------------------------&nbsp;&nbsp;</SPAN>45</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:30.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.2.1.1. 비정상위험분석상세<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------&nbsp;&nbsp;</SPAN>46</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:40.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.2.1.1.1. 비정상위험분석 상세차트<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;--------------------------------------&nbsp;&nbsp;</SPAN>47</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:40.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.2.1.1.2. 전체로그상세<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;---------------------------------------------------&nbsp;&nbsp;</SPAN>47</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:30.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.2.1.2. 부서별 비정상위험분석 상세차트<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;----------------------------------&nbsp;&nbsp;</SPAN>48</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:40.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.2.1.2.1. 부서별 위험현황<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------&nbsp;&nbsp;</SPAN>48</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:40.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.2.1.2.2. 부서별 개인정보 취급량<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;----------------------------------------&nbsp;&nbsp;</SPAN>48</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:40.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.2.1.2.3. 부서별 월별 누적량 비교<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;---------------------------------------&nbsp;&nbsp;</SPAN>49</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:40.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.2.1.2.4. 부서별 요일별 패턴<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;--------------------------------------------&nbsp;&nbsp;</SPAN>49</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:40.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.2.1.2.5. 부서별 24시간 패턴<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;--------------------------------------------&nbsp;&nbsp;</SPAN>50</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:40.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.2.1.2.6. 부서별 취급유형<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-----------------------------------------------&nbsp;&nbsp;</SPAN>50</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:30.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.2.1.3. 개인별 비정상위험분석 상세차트<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;---------------------------------&nbsp;&nbsp;</SPAN>51</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:40.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.2.1.3.1. 개인별 위험현황<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-----------------------------------------------&nbsp;&nbsp;</SPAN>51</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:40.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.2.1.3.2. 개인별 개인정보 취급량<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;---------------------------------------&nbsp;&nbsp;</SPAN>51</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:40.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.2.1.3.3. 개인별 월별 누적량 비교<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;--------------------------------------&nbsp;&nbsp;</SPAN>52</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:40.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.2.1.3.4. 개인별 요일별 패턴<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-------------------------------------------&nbsp;&nbsp;</SPAN>52</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:40.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.2.1.3.5. 개인별 24시간 패턴<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-------------------------------------------&nbsp;&nbsp;</SPAN>53</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:40.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.2.1.3.6. 개인별 취급유형<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;----------------------------------------------&nbsp;&nbsp;</SPAN>53</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:40.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.2.1.3.7. 개인별 타임라인<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;----------------------------------------------&nbsp;&nbsp;</SPAN>54</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.3. 비정상 위험 시나리오<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-----------------------------------------------------&nbsp;&nbsp;</SPAN>55</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.3.1. 비정상위험 시나리오 조건검색<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;---------------------------------------&nbsp;&nbsp;</SPAN>55</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.3.2. 시나리오 리스트<SPAN style='HWP-TAB:1;'>&nbsp;-------------------------------------------------------&nbsp;&nbsp;</SPAN>56</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:30.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.3.2.1. 시나리오 상세<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;----------------------------------------------------&nbsp;&nbsp;</SPAN>56</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.3.3. 시나리오 조건 검색<SPAN style='HWP-TAB:1;'>&nbsp;----------------------------------------------------&nbsp;&nbsp;</SPAN>57</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>8.3.4. 시나리오 엑셀다운로드<SPAN style='HWP-TAB:1;'>&nbsp;------------------------------------------------&nbsp;&nbsp;</SPAN>57</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:13.7pt;text-indent:-13.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>9. 인텔리젼스 차트<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;---------------------------------------------------------&nbsp;&nbsp;</SPAN>58</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>9.1. 개인정보 처리 현황<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;--------------------------------------------------------&nbsp;&nbsp;</SPAN>58</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>9.2. 시스템별 처리 현황<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;--------------------------------------------------------&nbsp;&nbsp;</SPAN>58</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>9.3. 개인정보 처리부서 TOP 10<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------&nbsp;&nbsp;</SPAN>59</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>9.4. 개인정보 취급자 TOP10<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;---------------------------------------------------&nbsp;&nbsp;</SPAN>59</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>9.5. 개인정보 유형별 현황<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------&nbsp;&nbsp;</SPAN>60</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>9.6. 개인정보 취급 유형별 현황<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------&nbsp;&nbsp;</SPAN>60</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>9.7. 비정상 위험부서 TOP10<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;---------------------------------------------------&nbsp;&nbsp;</SPAN>61</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>9.8. 비정상 위험 유형별 현황<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;--------------------------------------------------&nbsp;&nbsp;</SPAN>61</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>9.9. CPU, MEMORY, HDD 사용량<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;---------------------------------------------&nbsp;&nbsp;</SPAN>62</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:13.7pt;text-indent:-13.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>10. 통계 및 보고<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------------&nbsp;&nbsp;</SPAN>63</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>10.1. 기간별유형통계<SPAN style='HWP-TAB:1;'>&nbsp;-----------------------------------------------------------&nbsp;&nbsp;</SPAN>63</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>10.1.1. 기간별유형통계 조건검색<SPAN style='HWP-TAB:1;'>---------------------------------------------&nbsp;&nbsp;</SPAN>64</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>10.2. 시스템별유형통계<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;-------------------------------------------------------&nbsp;&nbsp;</SPAN>65</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>10.2.1. 시스템별유형통계 조건검색<SPAN style='HWP-TAB:1;'>&nbsp;------------------------------------------&nbsp;&nbsp;</SPAN>66</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>10.3. 소속별유형통계<SPAN style='HWP-TAB:1;'>------------------------------------------------------------&nbsp;&nbsp;</SPAN>67</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>10.3.1. 소속별유형통계 조건검색<SPAN style='HWP-TAB:1;'>---------------------------------------------&nbsp;&nbsp;</SPAN>68</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>10.4. 개인별유형통계<SPAN style='HWP-TAB:1;'>------------------------------------------------------------&nbsp;&nbsp;</SPAN>69</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>10.4.1. 개인별유형통계 조건검색<SPAN style='HWP-TAB:1;'>---------------------------------------------&nbsp;&nbsp;</SPAN>70</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>10.5. 점검보고서<SPAN style='HWP-TAB:1;'>&nbsp;----------------------------------------------------------------&nbsp;&nbsp;</SPAN>71</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>10.5.1. 접속기록보고서 생성<SPAN style='HWP-TAB:1;'>--------------------------------------------------&nbsp;&nbsp;</SPAN>71</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>10.6. 점검보고서 관리<SPAN style='HWP-TAB:1;'>&nbsp;-----------------------------------------------------------&nbsp;&nbsp;</SPAN>73</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:13.7pt;text-indent:-13.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>11. 정책설정관리<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;---------------------------------------------------------&nbsp;&nbsp;</SPAN>74</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:7.0pt;'><SPAN STYLE='font-family:"맑은 고딕"'>11.1. 개인정보설정<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;-------------------------------------------------------------&nbsp;&nbsp;</SPAN>74</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>11.1.1. 개인정보설정 상세<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;--------------------------------------------------&nbsp;&nbsp;</SPAN>74</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>11.2. 위험도 기준설정<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;----------------------------------------------------------&nbsp;&nbsp;</SPAN>75</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>11.3. 예외처리DB<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;---------------------------------------------------------------&nbsp;&nbsp;</SPAN>76</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>11.3.1. 예외처리DB 검색<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-----------------------------------------------------&nbsp;&nbsp;</SPAN>76</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>11.3.2. 예외처리DB 등록<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-----------------------------------------------------&nbsp;&nbsp;</SPAN>76</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>11.3.3. 예외처리DB 조회<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-----------------------------------------------------&nbsp;&nbsp;</SPAN>77</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>11.3.4. 예외처리DB 삭제<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-----------------------------------------------------&nbsp;&nbsp;</SPAN>77</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:13.7pt;text-indent:-13.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>12. 환경설정<SPAN style='HWP-TAB:1;'>&nbsp;-----------------------------------------------------------------&nbsp;&nbsp;</SPAN>78</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.1. 메뉴관리<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------------------&nbsp;&nbsp;</SPAN>78</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.2. 권한관리<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------------------&nbsp;&nbsp;</SPAN>79</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.2.1. 권한관리 기능<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;--------------------------------------------------------&nbsp;&nbsp;</SPAN>79</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.3. 조직관리<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------------------&nbsp;&nbsp;</SPAN>80</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.4. 사원관리<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------------------&nbsp;&nbsp;</SPAN>80</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.4.1. 사원관리 조건검색<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;---------------------------------------------------&nbsp;&nbsp;</SPAN>81</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.4.2. 사원관리상세<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;---------------------------------------------------------&nbsp;&nbsp;</SPAN>82</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:30.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.4.2.1. 사원상세 내용 수정<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;--------------------------------------------&nbsp;&nbsp;</SPAN>83</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:30.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.4.2.2. 사원상세 내용 삭제<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;----------------------------------------------&nbsp;&nbsp;</SPAN>83</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.4.3. 사용자관리 신규 등록<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------&nbsp;&nbsp;</SPAN>84</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.4.4. 엑셀업로드<SPAN style='HWP-TAB:1;'>-------------------------------------------------------------&nbsp;&nbsp;</SPAN>85</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.4.5. 엑셀다운로드<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;-------------------------------------------------------&nbsp;&nbsp;</SPAN>86</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.5. 관리자관리<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;--------------------------------------------------------------&nbsp;&nbsp;</SPAN>87</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.5.1. 관리자관리 조건검색<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;----------------------------------------------&nbsp;&nbsp;</SPAN>87</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.5.2. 관리자관리 상세<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;---------------------------------------------------&nbsp;&nbsp;</SPAN>88</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:30.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.5.2.1. 관리자상세 내용 수정<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-----------------------------------------&nbsp;&nbsp;</SPAN>89</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:30.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.5.2.2. 관리자상세 내용 삭제<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-----------------------------------------&nbsp;&nbsp;</SPAN>89</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:30.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.5.2.3. 관리자 비밀번호 변경<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-----------------------------------------&nbsp;&nbsp;</SPAN>90</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.5.3. 관리자관리 신규 등록<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;-----------------------------------------------&nbsp;&nbsp;</SPAN>91</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.5.4. 엑셀다운로드<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;--------------------------------------------------------&nbsp;&nbsp;</SPAN>93</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.6. 감사이력<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------------------&nbsp;&nbsp;</SPAN>94</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.6.1. 감사이력 조건검색<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;---------------------------------------------------&nbsp;&nbsp;</SPAN>94</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.6.2. 엑셀다운로드<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;--------------------------------------------------------&nbsp;&nbsp;</SPAN>95</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.7. 시스템관리<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;---------------------------------------------------------------&nbsp;&nbsp;</SPAN>96</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.8. 접속허용 IP<SPAN style='HWP-TAB:1;'>&nbsp;----------------------------------------------------------------&nbsp;&nbsp;</SPAN>97</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.8.1. 접속허용IP 생성<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;------------------------------------------------------&nbsp;&nbsp;</SPAN>97</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.8.2. 접속허용IP 삭제 및 비활성화<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;----------------------------------------&nbsp;&nbsp;</SPAN>98</SPAN></P>

<P CLASS=HStyle0 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>12.9. 고객관리<SPAN style='HWP-TAB:1;'>&nbsp;-------------------------------------------------------------------&nbsp;&nbsp;</SPAN>98</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:13.7pt;text-indent:-13.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>13. 기술지원 문의<SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;-----------------------------------------------------------&nbsp;&nbsp;</SPAN>58</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%; text-align: center;'><SPAN STYLE='font-size:14.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold;";line-height:180%'>들어가는 글</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'> </SPAN><SPAN STYLE='font-size:8.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:4%;line-height:180%'>본 설명서는 (주)이지서</SPAN><SPAN STYLE='font-size:8.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:4%;line-height:180%'>티 UBI SAFER-PSM V3.0의 효율적인 운용을 위하여 제공되는 </SPAN><SPAN STYLE='font-size:8.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>UBI SAFER-PSM V3.0 관리자 매뉴얼입니다.</SPAN></P>

<DIV STYLE=''>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

</DIV>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-12%;line-height:180%'>UBI SAFER-PSM V3.0은 개인정보보호법에 따라 개인정보 접속기록을 관리하고</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:11%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>개인정보 취급자의 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:5%;line-height:180%'>업무 처리 현황을 모니터링하여 개인정보의 유출 및 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>비정상행위를 방지하는 개인정보 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>종합관리 솔루션입니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:8.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>UBI SAFER-PSM</SPAN><SPAN STYLE='font-size:8.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'> V3.0 관리 프로그램은 운용자에게 익숙한 Windows환경의 </SPAN><SPAN STYLE='font-size:8.7pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>Internet Explorer User Interface로 제공되어 편리하게 이용할 수 있습니다.</SPAN></P>

<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle3 STYLE='line-height:180%;'>1. <SPAN STYLE='font-size:14.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>개요</SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>1.1. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>UBI SAFER-PSM이란?</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:2%;line-height:180%'>&nbsp;UBI SAFER-PSM은 개</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:2%;line-height:180%'>인정보 유출 및 오·남용 방지를 위한 내부통제 솔루션으로, </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>개인정보에 대한 접속기록 보관 및 점검 기능을 제공하여 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>기관의 개인정보 처리업무 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>현황에 대한 관리·감독을 수행할 수 있습니다.</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:2%;line-height:180%'>또한, 개인정보보호법 및 관련법규와 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'>감독기관의 규정 및 지침에서 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-9%;line-height:180%'>요구하는 개인정보 접속기록에 대한 안전성 확보 관련사항을</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'> 준수합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>1.2. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>UBI SAFER-PSM 구축 배경</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'>최근 계속해서 발생하는 개인정보 침해사고로 인하여 국민의 불안감이 증가하고, 피해 사례가 증가함에 따라 개인정보를 처리하는 공공·민간 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:180%'>기관의 개인정보보호 활동에 대한 필요성과 책임의식이 증대되고 있습니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;이에 따라, 개인정보보호법 시행(11.9.30) 및 과태료·과징금 부과 등 안전한</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>개인정보</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:3%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-9%;line-height:180%'>보호를 위해 처벌규정을 강화하는 방향으로 계속해서 개정되고 있습니다.</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-16%;line-height:180%'>또한, 행정자치부 등</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-10%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-11%;line-height:180%'>관리감독 기관에서 개인정보보호 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>실태점검, 관리수준 진단을 수행할 때 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>개인정보 접속기록 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>관리여부를 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-12%;line-height:180%'>점검하도록 명시하고 있어 개인정보접속기록에 대한 체계적인 관리체계의 필요성이</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:180%'> 증대되고 있습니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<CAPTION align="top">
<P CLASS=HStyle2 STYLE='margin-left:0.0pt;text-align:center;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&lt;참고자료 1&gt;</SPAN>
</P>
</CAPTION><TR>
	<TD valign="middle" style='width:494;height:326;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:125%;'><SPAN STYLE='font-size:8.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;font-weight:"bold";line-height:125%'>개인정보보호법 제 29조(안전조치의무) </SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-top:5.0pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'> </SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:125%'>개인정보처리자는 개인정보가 분실·도난·유출·위조·변조 또는 훼손되지 아니하도록 내부 관리계획 수립,</SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'> 접속기록 보관 등 </SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";text-decoration:"underline";color:#0000ff;line-height:125%'>대통령령으로 정하는 바에 따라 안전성 확보에 필요한 기술적·관리적 및 물리적 조치</SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>를 하여야 한다. </SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-top:5.0pt;line-height:125%;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ff0000;line-height:125%'>&nbsp;</SPAN></P>
	<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:125%;'><SPAN STYLE='font-size:8.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;font-weight:"bold";line-height:125%'>개인정보보호법 제 31조(개인정보보호책임자의 지정) </SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:20.7pt;margin-top:5.0pt;text-indent:-20.7pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>&nbsp;&nbsp;① </SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:125%'>개인정보처리자는 개인정보의 처리에 관한 업무를 총괄해서 책임질 개인정보 보호책임자를 </SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>지정하여야 한다.</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:24.2pt;text-indent:-24.2pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>&nbsp;&nbsp;② </SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:125%'>개인정보 보호책임자는 다음 각 호의 업무를 수행한다.</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:27.0pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>&nbsp;&nbsp;1. 개인정보 보호 계획의 수립 및 시행</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:27.0pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>&nbsp;&nbsp;2. 개인정보 처리 실태 및 관행의 정기적인 조사 및 개선</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:27.0pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>&nbsp;&nbsp;3. 개인정보 처리와 관련한 불만의 처리 및 피해 구제</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:27.0pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'> </SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:125%'> </SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";text-decoration:"underline";color:#0000ff;line-height:125%'>4. 개인정보 유출 및 오용·남용 방지를 위한 내부통제시스템의 구축</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:27.0pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>&nbsp;&nbsp;5. 개인정보 보호 교육 계획의 수립 및 시행</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:27.0pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>&nbsp;&nbsp;6. 개인정보파일의 보호 및 관리·감독</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:27.0pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>&nbsp; </SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";text-decoration:"underline";color:#0000ff;line-height:125%'>7. 그 밖에 개인정보의 적절한 처리를 위하여 대통령령으로 정한 업무</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:12.0pt;text-indent:-12.0pt;line-height:125%;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ff0000;line-height:125%'>&nbsp;</SPAN></P>
	<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:125%;'><SPAN STYLE='font-size:8.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;font-weight:"bold";line-height:125%'>개인정보보호법 제 34조의2(과징금의 부과 등) </SPAN></P>
	<P CLASS=HStyle2 STYLE='margin-left:20.3pt;text-indent:-20.3pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>&nbsp;&nbsp;① </SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:2%;line-height:125%'>행정자치부장관은 개인정보처리자가 처리하는 주민등록번호가 분실·도난·유출·위조·변조 또는</SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:125%'> 훼손된 경우에는 </SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;font-weight:"bold";text-decoration:"underline";color:#0000ff;line-height:125%'>5억원 이하의 과징금을 부과·징수</SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:125%'>할 수 있다. 다만, 주민등록번호가 분실·도난·</SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;font-weight:"bold";line-height:125%'>유출·위조·변조 또는 훼손되지 아니하도록 개인정보처리자가 제24조제3항에 따른 </SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;font-weight:"bold";text-decoration:"underline";color:#0000ff;line-height:125%'>안전성 확보에</SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;font-weight:"bold";text-decoration:"underline";color:#0000ff;line-height:125%'> 필요한 조치를 다한 경우에는 그러하지 아니하다.</SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:125%'> </SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<CAPTION align="top">
<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-19.9pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&lt;참고자료 2&gt;</SPAN>
</P>
</CAPTION><TR>
	<TD valign="middle" style='width:494;height:186;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:125%;'><SPAN STYLE='font-size:8.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;font-weight:"bold";line-height:125%'>개인정보보호법 시행령 </SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:125%'>제30조(개인정보의 안전성 확보 조치)</SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'> </SPAN></P>
	<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>&nbsp;① 개인정보처리자는 법 제29조에 따라 다음 각 호의 안전성 확보 조치를 하여야 한다.</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:23.0pt;text-indent:-1.0pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>&nbsp;&nbsp;1. 개인정보의 안전한 처리를 위한 내부 관리계획의 수립·시행</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:23.0pt;text-indent:-1.0pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>&nbsp;&nbsp;2. 개인정보에 대한 접근 통제 및 접근 권한의 제한 조치</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:44.6pt;text-indent:-22.6pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>&nbsp;&nbsp;3. </SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:125%'>개인정보를 안전하게 저장·전송할 수 있는 암호화 기술의 적용 또는 이에 상응하는 조치</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:44.4pt;text-indent:-22.4pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>&nbsp; </SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";text-decoration:"underline";color:#0000ff;line-height:125%'>4. </SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-7%;font-weight:"bold";text-decoration:"underline";color:#0000ff;line-height:125%'>개인정보 침해사고 발생에 대응하기 위한 접속기록의 보관 및 위조·변조 방지를 위한 조치</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:23.0pt;text-indent:-1.0pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>&nbsp;&nbsp;5. 개인정보에 대한 보안프로그램의 설치 및 갱신</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:44.8pt;text-indent:-22.8pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>&nbsp;&nbsp;6. </SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:125%'>개인정보의 안전한 보관을 위한 보관시설의 마련 또는 잠금장치의 설치 등 물리적 조치</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:19.6pt;text-indent:-19.6pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>&nbsp;&nbsp;② </SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:125%'>행정자치부장관은 개인정보처리자가 제1항에 따른 안전성 확보 조치를 하도록 시스템을 구축하는</SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'> 등 필요한 지원을 할 수 있다. </SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:12.0pt;text-indent:-12.0pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>&nbsp;&nbsp;③ </SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";line-height:125%'>제1항에 따른 안전성 확보 조치에 관한 세부 기준은 행정자치부장관이 정하여 고시한다. </SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle0 STYLE='line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<CAPTION align="top">
<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-19.9pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&lt;참고자료 3&gt;</SPAN>
</P>
</CAPTION><TR>
	<TD valign="middle" style='width:494;height:185;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:125%'>개인정보의 안전성 확보조치 기준 제8조(접속기록의 보관 및 점검) </SPAN></P>
	<P CLASS=HStyle2 STYLE='margin-left:19.5pt;text-indent:-19.5pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'> </SPAN><SPAN STYLE='font-size:8.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";background-color:#ffffff;line-height:125%'> </SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>① </SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:125%'>개인정보처리자는 개인정보취급자가</SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;font-weight:"bold";line-height:125%'> </SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;font-weight:"bold";text-decoration:"underline";color:#0000ff;line-height:125%'>개인정보처리시스템에 접속한 기록을 1년 이상 보관&#8228;관리</SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>하여야 한다. 다만, 5만명 이상의 정보주체에 관하여 개인정보를 처리하거나, 고유식별정보 또는 민감정보를 처리하는 개인정보처리 시스템의 경우에는 </SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";text-decoration:"underline";color:#0000ff;line-height:125%'>2년 이상 보관</SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;font-weight:"bold";text-decoration:"underline";color:#0000ff;line-height:125%'>&#8228;</SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";text-decoration:"underline";color:#0000ff;line-height:125%'>관리</SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>하여야 한다.<BR></SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:19.8pt;text-indent:-19.8pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>&nbsp;&nbsp;② </SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:125%'>개인정보처리자는 개인정보의 오&#8228;남용, 분실&#8228;도난&#8228;유출&#8228;위조&#8228;변조 또는 훼손 등에 대응하기 위하여</SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:125%'> </SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;font-weight:"bold";text-decoration:"underline";color:#0000ff;line-height:125%'>개인정보처리시스템의</SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";text-decoration:"underline";color:#0000ff;line-height:125%'> 접속기록 등을 월 1회 이상 점검</SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>하여야 한다. 특히 개인정보를 다운로드한 것이 발견되었을 경우에는 내부관리 계획으로 정하는 바에 따라 그 사유를 반드시 확인하여야 한다.<BR></SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:19.6pt;text-indent:-19.6pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>&nbsp;&nbsp;③ </SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:125%'>개인정보처리자는 개인정보취급자의 접속기록이 위&#8228;변조 및 도난, 분실되지 않도록 해당 </SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";text-decoration:"underline";color:#0000ff;line-height:125%'>접속기록을</SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";text-decoration:"underline";color:#0000ff;line-height:125%'> 안전하게 보관</SPAN><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>하여야 한다.</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<CAPTION align="top">
<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-19.9pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&lt;참고자료 4&gt;</SPAN>
</P>
</CAPTION><TR>
	<TD valign="middle" style='width:494;height:203;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";text-decoration:"underline";color:#0000ff;line-height:125%'>공공기관 개인정보보호 관리수준 진단 항목</SPAN></P>
	<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"맑은 고딕";font-weight:"bold";line-height:125%'>&nbsp;</SPAN></P>
	<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:125%'>침해대책 수립 및 이행분야의 진단항목 中 </SPAN></P>
	<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>개인정보처리시스템의 접속기록에 대한 점검 및 후속 조치를 이행여부 </SPAN></P>
	<P CLASS=HStyle2 STYLE='margin-left:22.2pt;text-indent:-22.2pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:125%'>&nbsp;</SPAN></P>
	<P CLASS=HStyle2 STYLE='margin-left:22.2pt;text-indent:-22.2pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:125%'>&lt;제출 증빙 자료&gt;</SPAN></P>
	<P CLASS=HStyle2 STYLE='margin-left:22.2pt;text-indent:-22.2pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:125%'>운영하고 있는 개인정보처리시스템에 대해 아래의 사항에 대한 증빙 실적을 제출하여야 함.</SPAN></P>
	<P CLASS=HStyle2 STYLE='margin-left:22.2pt;text-indent:-22.2pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:125%'>&nbsp;</SPAN></P>
	<P CLASS=HStyle2 STYLE='margin-left:22.2pt;text-indent:-22.2pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>&nbsp;&nbsp;① 접속기록 6개월 이상 보관·관리</SPAN></P>
	<P CLASS=HStyle2 STYLE='margin-left:22.2pt;text-indent:-22.2pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>&nbsp;&nbsp;② 이용자 식별 인증 정보(일시, 컴퓨터, IP 주소, ID 등), 서비스 이용정보 포함</SPAN></P>
	<P CLASS=HStyle2 STYLE='margin-left:22.2pt;text-indent:-22.2pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>&nbsp;&nbsp;③ 접속기록 점검 실적(반기별 1회 이상)</SPAN></P>
	<P CLASS=HStyle2 STYLE='margin-left:22.2pt;text-indent:-22.2pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";line-height:125%'>&nbsp;&nbsp;④ 접속기록 점검 실적 결과에 따른 후속조치 이행 실적</SPAN></P>
	<P CLASS=HStyle2 STYLE='margin-left:22.2pt;text-indent:-22.2pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"맑은 고딕";line-height:125%'>&nbsp;</SPAN></P>
	<P CLASS=HStyle2 STYLE='margin-left:22.2pt;text-indent:-22.2pt;line-height:125%;'><SPAN STYLE='font-size:7.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:125%'>&nbsp;&nbsp;※ 최근 5년간 공공기관 개인정보보호 관리수준 진단 지표 내용 발췌(2013년 ~ 2017년) </SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>1.3. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>UBI SAFER-PSM의 목적</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-20%;line-height:180%'>개인정보취급자의 개인정보 접속기록 보관 및 점검을 위한 기능을 제공하여 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-15%;line-height:180%'>기관이 보유한 개인정보를</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-12%;line-height:180%'> 안전하게 보관 및 관리·감독할 수 있도록 지원합니다.</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-9%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-17%;line-height:180%'>또한, 개인정보 유출 및 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-10%;line-height:180%'>오·남용을 방지하고, 비인가 된 개인정보 처리, </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-12%;line-height:180%'>대량의 개인정보 유출 등의 비정상행위를 예방하고자</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-9%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-12%;line-height:180%'>하는데 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-10%;line-height:180%'>목적이 있습니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-bottom:4.0pt;text-indent:20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>1.4. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>UBI SAFER-PSM의 목표</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>개인정보보호법, 국가정보보안지침 등 관련 법규 및 지침을 준수합니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>개인정보 접속기록에 대한 주기적 현황 관리가 가능합니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>개인정보 접속기록에 대한 위·변조 및 도난, 분실을 방지합니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>기관의 개인정보 유출 및 오·남용을 방지합니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:12.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>1.5. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>기대효과</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>UBI SAFER-PSM을 구축하여 기관의 개인정보취급자의 개인정보 처리</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>업무에 대한 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>현황을 파악하고 정기적인 관리·감독이 가능합니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>주기적/상시적으로 발생하는 개인정보 관리·감독 및 평가 대응 시 접속기록 보관 및 점검에 대한 증빙 자료로 제출이 가능합니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-6%;line-height:180%'>개인정보 취급자로 하여금 개인정보 처리 업무 시 개인정보보호법을</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'> 준수할 수 있도록</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'> 인식제고 효과를 기대할 수 있습니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<CAPTION align="top">
<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>[표 1-1] 업무 대상자별 기대효과</SPAN>
</P>
</CAPTION><TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:165;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>업무 대상자</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#cedeef"  style='width:329;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>기대효과</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:165;height:45;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>개인정보보호 책임자</SPAN></P>
	</TD>
	<TD valign="middle" style='width:329;height:45;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:12.0pt;text-align:left;text-indent:0.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>기관의 개인정보취급자의 개인정보 처리업무에 대한 현황을 파악하고 정기적인 관리·감독 가능</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:165;height:45;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>개인정보보호 담당자</SPAN></P>
	</TD>
	<TD valign="middle" style='width:329;height:45;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:12.0pt;text-align:left;text-indent:0.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>주기적/상시적으로 발생하는 개인정보 관리·감독 및 평가 대응 시 접속기록 보관 및 점검 자료 산출</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:165;height:45;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>개인정보 취급자 / </SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>업무 담당자</SPAN></P>
	</TD>
	<TD valign="middle" style='width:329;height:45;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:12.0pt;text-align:left;text-indent:0.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>개인정보 취급자로 하여금 개인정보 처리 업무 시 개인정보보호법을 준수할 수 있도록 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>인식제고 효과</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle0 STYLE='line-height:180%;'></P>

<P CLASS=HStyle4 STYLE='margin-left:10.0pt;line-height:180%;'>2. <SPAN STYLE='font-size:14.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>UBI SAFER-PSM 주요 특징</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:20.0pt;line-height:180%;'>2.1. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>UBI SAFER-PSM 주요 기능</SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>2.1.1. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>비정상행위 모니터링 및 위험분석 기능</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>개인정보 접속기록 현황 모니터링 기능을 제공합니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>비정상행위 기간별 추이분석 및 현황 모니터링 기능을 제공합니다.&nbsp;&nbsp; </SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>개인별 비정상행위 위험도 파악 및 오남용의심행위자 현황 파악 기능을 제공합니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>비정상행위 시나리오 추출조건 관리 기능</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>을 제공합니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:12.0pt;line-height:180%;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5>2.1.2. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>접속기록 및 접근이력 관리 기능</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:29.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>개인정보 접속기록 보관 및 조회 기능</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>을 제공합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:29.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>개인정보 접속기록 백업 보관 및 위·변조 방지 기능</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>을 제공합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:29.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>시스템 접근이력 보관 및 조회 기능</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>을&nbsp;&nbsp; 제공합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5>2.1.3. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>통계 및 보고 기능</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:29.1pt;text-indent:9.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>기간별 통계 조회 및 엑셀 다운로드 기능</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>을 제공합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:29.1pt;text-indent:9.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>시스템별 통계 조회 및 엑셀 다운로드 기능</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>을 제공합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:29.1pt;text-indent:9.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>소속별 통계 조회 및 엑셀 다운로드 기능</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>을 제공합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:29.1pt;text-indent:9.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>개인별 통계 조회 및 엑셀 다운로드 기능</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>을 제공합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:29.1pt;text-indent:9.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>개인정보 접속기록 점검보고서 생성 및 출력 기능</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>을 제공합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5>2.1.4. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>정책설정 및 환경설정 기능</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>개인정보유형 설정 기능</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>을 제공합니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>위험도기준 설정 기능</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>을 제공합니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>개인정보 예외처리 DB설정 기능</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>을 제공합니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>관리자관리 및 권한관리 기능</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>을 제공합니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>조직 및 사원관리 기능</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>을 제공합니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>감사이력 조회 및 엑셀 다운로드 기능</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>을 제공합니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4>2.2. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>UBI SAFER-PSM 특장점</SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>2.2.1. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;font-weight:"bold";line-height:180%'>법규/지침 준수</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;text-align:left;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'>개인정보보호법, 국가정보보안지침과 같은 법규 및 지침에 명시되어 있는 개인정보</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 처리에 대한 접속기록 6개월 이상 보관 및 점검기능을 제공해주고, 접근이력 추가 기록에 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>따른 사고발생시 행위추적 기능, 개인정보 접속기록 위·변조 방지 및 개인정보 오·남용 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>분석 등으로 침해사고 발생 시 신속한 대응이 가능합니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>2.2.2. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;font-weight:"bold";line-height:180%'>공인인증기관을 통한 로그 검출 정확도 보장</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;text-align:left;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;공인인증기관의 성능시험을 통해 접속기록에 대한 유실/ 과탐/ 오탐 없이 정확한 개인정보 접속기록 생성. 및 수집에 대한 정확도를 검증받았습니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>2.2.3. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;font-weight:"bold";line-height:180%'>실시간 처리(In-Memory) 기술</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;text-align:left;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:7%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>데이터의 로드를 최소화하여 메모리상에서 개인정보를 고속으로 탐지하는 기능을 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-7%;line-height:180%'>지원하며, 디스크가 아닌 메인 메모리에 모든 데이터를 저장하고 처리하기 때문에 데이터 검색과 접근이 디스크 기반 기술보다 평균 약 100~1,000배 빠르며, 데이터 조회 및 처리 속도의 대폭 감소와 시스템 확장성 확보, 디스크 공간의 절감 효과를 보장합니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>2.2.4. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;font-weight:"bold";line-height:180%'>이상징후 모니터링 기술</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;text-align:left;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;개인정보 모니터링 시스템 및 방법에 대한 보유 특허를 통해 이상징후 추출조건 생성방식을 마련하고, 추출조건에 대한 지표 마련 및 위험도에 대한 정량적 근거를 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-9%;line-height:180%'>마련하였습니다. 단순 로그건수부터 통계적 기법, 시나리오 기법 등의 이상징후 추출조건의</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 고도화 수행경험을 통해 이상징후 모니터링 관리 및 운영을 보장합니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5>2.2.5. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;font-weight:"bold";line-height:160%'>기존 시스템 및 네트워크 환경 변화 없는 솔루션 구축</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;text-align:left;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:3%;line-height:180%'>UBI SAFER-PSM V3.0은 기존 시스템 및 네트워크 환경의 변경 없이 구축이</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'> 가능합니다. </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-7%;line-height:180%'>업무처리 시스템 운영 환경에 최적화된 다양한 연동 방식을 지원합니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";letter-spacing:-2%;font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>2.2.6. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>기 운영 중인 시스템과의 호환성</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;text-align:left;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;UBI SAFER-PSM V3.0은 고객사의 기 운영 중인 시스템에 어떠한 영향도 주지 않는 호환성을 보장합니다.</SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>2.2.7. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>다양한 접속기록 생성 기술</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;text-align:left;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;㈜이지서티는 접속기록을 생성하는 다양한 원천기술을 보유하고 있으며, 빅데이터 기반의 접속기록 생성 원천 기술을 보유하고 있습니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle6 STYLE='line-height:180%;'>2.2.7.1. <SPAN STYLE='font-family:"나눔명조,한컴돋움";font-weight:"bold"'>AGENT방식</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:39.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>BCI 방식</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:39.1pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC2B67.png" alt="그림입니다.
원본 그림의 이름: CLP00001f0c0005.bmp
원본 그림의 크기: 가로 425pixel, 세로 94pixel" width="378" height="95" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:39.1pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>BCI 방식은</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;font-weight:"bold";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>Byte code 직접 조작을 통해 원하는 정보와 수행되는 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>각각의 트랜잭션이 수행하는 SQL쿼리까지 포착 가능합니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:39.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>FILTER방식</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;text-align:center;text-indent:-29.1pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC2B87.png" alt="그림입니다." width="424" height="96" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:39.1pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>FILTER방식은</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;font-weight:"bold";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>업무 WAS에서 요청/응답 신호만 포착하는 단순하고 위험도가 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>낮은 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>방식이며, 시스템 의존성이 낮아 안정적이며 기간 단축에 유리합니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:39.1pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:39.1pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:39.1pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:39.1pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:39.1pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:39.1pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:39.1pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle6 STYLE='line-height:180%;'>2.2.7.2. <SPAN STYLE='font-family:"나눔명조,한컴돋움";font-weight:"bold"'>AGENTLESS방식</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:39.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>CONTENTS FILTERING방식</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:4.1pt;text-align:center;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC2B98.png" alt="그림입니다.
원본 그림의 이름: CLP00001f0c0007.bmp
원본 그림의 크기: 가로 425pixel, 세로 94pixel" width="378" height="94" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:4.1pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:39.1pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:3%;line-height:180%'>CONTENTS FILTERING방식은 CONTENTS FILTERING</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:3%;line-height:180%'> 자체를 수집분석 서버에서 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>구동하며, 기</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:180%'>존 업무 WAS에서는 별도의 추가내역 없이 HTTP 요청/응답 신호 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>포착합니다.</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:180%'> SSL 통신으로 주고받은 HTTP 요청/응답 신호를 암호화된 상태로 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>포착하며, SSL을 통한 HTTP 메시지 암호문의 복호화는 불가능합니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:4.1pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:39.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>CLOUD FILTERING방식</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:4.1pt;text-align:center;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC2B99.png" alt="그림입니다.
원본 그림의 이름: CLP00001cb40007.bmp
원본 그림의 크기: 가로 383pixel, 세로 98pixel" width="378" height="94" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:39.1pt;text-align:left;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;클라우드 환경에 설치된 개인정보 처리 시스템의 접속로그 생성이 가능하며, 시스템 확대 시 유연한 정책설정을 통한 접속기록 시스템 연계가 가능합니다. </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>다양한 시스템의 접속기록 생성에 따른 Hypervisor의 부하가 발생할 수 있습니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:13.5pt;text-align:left;text-indent:-13.5pt;line-height:100%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:100%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:13.5pt;text-align:left;text-indent:-13.5pt;line-height:100%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:100%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle3 STYLE='line-height:180%;'>3. <SPAN STYLE='font-size:14.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>설치</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:10.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:180%'>UBI SAFER-PSM V3.0의 관리 프로그램은 Internet Explorer(이하 IE로 명칭)</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>가</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 설치되어 있고, 인터넷이 가능한 PC이며, 관리자 계정 및 비밀번호를 보유하고 있는 관리자에 한하여 사용할 수 있습니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>3.1. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>구성 내역</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-7%;line-height:180%'>개인정보종합관리시스템 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-7%;line-height:180%'>UBI SAFER-PSM</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-7%;line-height:180%'> V3.0 구축 시 제공되는 항목은</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'> 다음과</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'> 같습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>UBI SAFER-PSM V3.0 장비</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>Serial Cable</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>장비 Power Cable</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>UBI SAFER-PSM 제품매뉴얼</SPAN></P>

<DIV STYLE=''>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

</DIV>

<P CLASS=HStyle2 STYLE='margin-left:22.4pt;text-indent:-12.5pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>※ </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>UBI SAFER-PSM V3.0의 관리 프로그램은 웹 기반으로 기본적으로 장비 내에 설치되어 구축됩니다. 사용 방법은 4장부터 참고하시기 바랍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<DIV STYLE=''>

<P CLASS=HStyle4 STYLE='line-height:180%;'>3.2. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>설치 구성</SPAN></P>

</DIV>

<P CLASS=HStyle0 STYLE='margin-left:39.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>Agentless 방식</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:39.1pt;line-height:180%;'><SPAN STYLE='font-size:3.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC2BC9.png" alt="그림입니다.
원본 그림의 이름: 설치구성도(3)-03.jpg
원본 그림의 크기: 가로 2080pixel, 세로 796pixel" width="500" height="191" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-6%;line-height:180%'>UBI SAFER-PSM V3.0&nbsp; Agentless방식은 기본적으로 개인정보가 들어 있는 공통 기반 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-13%;line-height:180%'>시스템인</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-12%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-14%;line-height:180%'>웹 서버가 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-11%;line-height:180%'>존재하는 서버팜(Server Farm) 바로 앞단에 설치하는 것을</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'> 권장합니다.</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'> </SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:39.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>Agent 방식</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:3.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC2C85.png" alt="그림입니다.
원본 그림의 이름: 설치구성도(3)-02.jpg
원본 그림의 크기: 가로 2080pixel, 세로 796pixel" width="500" height="191" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<CAPTION align="top">
<P CLASS=HStyle2 STYLE='margin-left:0.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-6%;line-height:180%'>UBI SAFER-PSM V3.0 Agent방식은 기본적으로 대상시스템과 통신이 가능한 동일 서버 구간에</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-11%;line-height:180%'> 설치하는 것을</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'> 권장합니다.</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'> </SPAN>
</P>

<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'><BR></SPAN>
</P>

<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>[표 3-1 설치 구성]</SPAN>
</P>
</CAPTION><TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:142;height:42;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:20.0pt;text-align:center;text-indent:-20.0pt;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>System 권장사양</SPAN></P>
	</TD>
	<TD valign="middle" style='width:346;height:42;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:20.0pt;text-indent:-20.0pt;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-10%;line-height:160%'>관리 프로그램의 원활한 운용을 위하여 필요한 기본 Hardware 사양</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:142;height:42;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:20.0pt;text-align:center;text-indent:-20.0pt;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>UBI SAFER-PSM</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:20.0pt;text-align:center;text-indent:-20.0pt;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>관리 프로그램</SPAN></P>
	</TD>
	<TD valign="middle" style='width:346;height:42;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:20.0pt;text-indent:-20.0pt;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>웹 버전(Internet Explorer 9.0 지원)</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle0 STYLE='line-height:180%;'></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>3.3. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>시스템 권장사양</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>UBI SAFER-PSM V3.0 관리 프로그램의 원활한 운용을 위해 PC의 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>권장사양은 아래와</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 같습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<CAPTION align="top">
<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>[표 3-2 관리자 PC 시스템 권장사양]</SPAN>
</P>
</CAPTION><TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:119;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:2.0pt;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>CPU</SPAN></P>
	</TD>
	<TD valign="middle" style='width:376;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:2.0pt;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>Core 2 Duo 이상</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:119;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:2.0pt;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>Memory</SPAN></P>
	</TD>
	<TD valign="middle" style='width:376;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:2.0pt;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>4GB 이상</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:119;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:2.0pt;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>HDD</SPAN></P>
	</TD>
	<TD valign="middle" style='width:376;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:2.0pt;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>10GB 이상</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:119;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:2.0pt;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>Ethernet</SPAN></P>
	</TD>
	<TD valign="middle" style='width:376;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:2.0pt;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>10/1000M Integrated Controller</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:119;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:2.0pt;text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>Internet Explorer</SPAN></P>
	</TD>
	<TD valign="middle" style='width:376;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:2.0pt;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>IE 버전 9.0 이상</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>3.4. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>관리자 프로그램 설치</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>UBI SAFER-PSM V3.0의 관리 프로그램은 IE가 설치되어있는 PC인 경우 어디서든 사용 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'>가능하며</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-6%;line-height:180%'>, UBI SAFER-PSM 모듈은 ㈜이지서티에서 납품 시 설치되어 출고되므로 사이트 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-7%;line-height:180%'>관리자의 별도 설치는 필요치 않습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='text-indent:-30.0pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='text-indent:-30.0pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='text-indent:-30.0pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle3>4. <SPAN STYLE='font-size:14.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>UBI SAFER-PSM 시작하기</SPAN></P>

<P CLASS=HStyle4>4.1. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>시스템 로그인</SPAN></P>

<P CLASS=HStyle16 STYLE='text-indent:29.1pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>다음의 순서에 따라 관리자 PC에서 로그인 하십시오. </SPAN></P>

<P CLASS=HStyle16 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</SPAN></SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>1. 웹 브라우저를 띄웁니다.</SPAN></P>

<P CLASS=HStyle16 STYLE='margin-left:51.4pt;text-indent:-51.4pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</SPAN></SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>2. </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-10%;line-height:180%'>주소 입력 창에 https://if_addr:port_number와 같이 PSM이 설치된 시스템의 IP 주소와</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'> 포트 번호를 입력합니다.</SPAN></P>

<P CLASS=HStyle16 STYLE='margin-left:56.0pt;text-indent:-56.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3. 로그인 화면이 출력됩니다.</SPAN></P>

<P CLASS=HStyle16 STYLE='margin-left:56.0pt;text-indent:-56.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle16 STYLE='margin-left:56.0pt;text-align:center;text-indent:-56.0pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC2D42.png" alt="그림입니다.
원본 그림의 이름: CLP00001ed00003.bmp
원본 그림의 크기: 가로 1120pixel, 세로 673pixel" width="499" height="268" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle15 STYLE='margin-left:39.1pt;text-indent:-39.1pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</SPAN></P>

<P CLASS=HStyle15 STYLE='margin-left:39.1pt;text-indent:-39.1pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4. 사용자 인증을 위해 USER ID와 PASSWORD를 입력합니다.&nbsp;&nbsp;&nbsp; </SPAN></P>

<P CLASS=HStyle15 STYLE='text-indent:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>5. 로그인을 클릭합니다.</SPAN></P>

<P CLASS=HStyle15 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>6. </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-6%;line-height:180%'>USER ID와 PASSWORD 정보가 올바르지 않을 경우, 오류 메시지가</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 출력됩니다. </SPAN></P>

<P CLASS=HStyle15 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>7. 시스템에 성공적으로 로그인되면 초기 웹 페이지가 나타납니다.</SPAN></P>

<P CLASS=HStyle15 STYLE='margin-left:39.1pt;text-indent:-39.1pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;</SPAN></P>

<P CLASS=HStyle15 STYLE='margin-left:54.2pt;text-indent:-54.2pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle15 STYLE='text-indent:64.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;ID를 입력하지 않고 로그인을 시도한 경우</SPAN></P>

<P CLASS=HStyle15 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:100%'><IMG src="${rootPath}/resources/image/help/master/PIC2D81.png" alt="그림입니다.
원본 그림의 이름: CLP00001ed00006.bmp
원본 그림의 크기: 가로 763pixel, 세로 657pixel" width="378" height="283" vspace="0" hspace="0" border="0">&nbsp;</SPAN></P>

<P CLASS=HStyle15 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:100%'><BR></SPAN></P>

<P CLASS=HStyle15 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:100%'><BR></SPAN></P>

<P CLASS=HStyle15 STYLE='text-indent:64.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;PASSWORD를 입력하지 않고 로그인을 시도한 경우</SPAN></P>

<P CLASS=HStyle15 STYLE='text-align:center;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC2DC1.png" alt="그림입니다.
원본 그림의 이름: CLP00001ed00005.bmp
원본 그림의 크기: 가로 789pixel, 세로 632pixel" width="378" height="283" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle15 STYLE='text-indent:64.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>ID나&nbsp; PASSWORD가 틀린 경우</SPAN></P>

<P CLASS=HStyle15 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:100%'><IMG src="${rootPath}/resources/image/help/master/PIC2E00.png" alt="그림입니다.
원본 그림의 이름: CLP00001ed00007.bmp
원본 그림의 크기: 가로 738pixel, 세로 654pixel" width="378" height="283" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle15 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:100%'><BR></SPAN></P>

<P CLASS=HStyle15 STYLE='line-height:50%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:50%'><BR></SPAN></P>

<P CLASS=HStyle15 STYLE='text-indent:64.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;ID나&nbsp; PASSWORD가 5회 이상 틀린 경우</SPAN></P>

<P CLASS=HStyle15 STYLE='text-align:center;line-height:5%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:5%'><BR></SPAN></P>

<P CLASS=HStyle15 STYLE='text-align:center;line-height:5%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:5%'><IMG src="${rootPath}/resources/image/help/master/PIC2E30.png" alt="그림입니다.
원본 그림의 이름: CLP00001ed00008.bmp
원본 그림의 크기: 가로 743pixel, 세로 660pixel" width="378" height="283" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle15 STYLE='text-align:center;line-height:5%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:5%'><BR></SPAN></P>

<P CLASS=HStyle15 STYLE='text-align:center;line-height:5%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:5%'><BR></SPAN></P>

<P CLASS=HStyle15 STYLE='text-align:center;line-height:5%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:5%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;</SPAN></SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&#8680;</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-13%;line-height:180%'>로그인 실패횟수가 5회 이상이며, 10분간 계정 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-9%;line-height:180%'>잠김이 발생될 경우 발생합니다.</SPAN></P>

<P CLASS=HStyle15 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:100%'><BR></SPAN></P>

<P CLASS=HStyle15 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:100%'><BR></SPAN></P>

<P CLASS=HStyle15 STYLE='text-indent:64.1pt;line-height:180%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;허용되지 않은 IP로 접속한 경우</SPAN></P>

<P CLASS=HStyle15 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:100%'><IMG src="${rootPath}/resources/image/help/master/PIC2E60.png" alt="그림입니다.
원본 그림의 이름: CLP000008d4002f.bmp
원본 그림의 크기: 가로 1116pixel, 세로 215pixel" width="499" height="95" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle15 STYLE='margin-left:60.4pt;text-indent:-15.4pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-8%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle15 STYLE='margin-left:23.3pt;text-indent:-23.3pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC2E71.png" alt="" width="31" height="31" vspace="0" hspace="0" border="0"></SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:4%;line-height:180%'>초기 로그인할 수 있는 USER ID와 PASSWORD는 사이트마다 다르게 설정될 수</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 있습니다.</SPAN></P>

<P CLASS=HStyle15 STYLE='margin-left:23.3pt;text-indent:-23.3pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC2E81.png" alt="" width="31" height="31" vspace="0" hspace="0" border="0"></SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:2%;line-height:180%'>사용할 수 있는 USER ID와 PASSWORD는 관리자 설정에서 추가할 수 있습니다. </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>관리자 설정에 대한 자세한 이용 방법은 본 설명서 12.5절을 참고하시기 바랍니다.</SPAN></P>

<P CLASS=HStyle15 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-8%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4>4.2. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>시스템 로그아웃</SPAN></P>

<P CLASS=HStyle16><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;다음의 순서에 따라 관리자 PC에서 로그아웃 하십시오. </SPAN></P>

<P CLASS=HStyle15 STYLE='text-indent:39.1pt;line-height:160%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>1. 웹 브라우저 우측 상단의 로그아웃 버튼을 클릭합니다.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </SPAN></P>

<P CLASS=HStyle15 STYLE='line-height:160%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'><IMG src="${rootPath}/resources/image/help/master/PIC2EB1.png" alt="그림입니다.
원본 그림의 이름: CLP000006a80017.bmp
원본 그림의 크기: 가로 1902pixel, 세로 974pixel" width="499" height="143" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle15 STYLE='line-height:105%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:105%'>&nbsp;</SPAN></P>

<P CLASS=HStyle15 STYLE='line-height:105%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:105%'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UBI SAFER-PSM V3.0 초기 로그인 화면으로 전환됩니다.</SPAN></P>

<P CLASS=HStyle15 STYLE='text-align:center;line-height:105%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:105%'><IMG src="${rootPath}/resources/image/help/master/PIC2F20.png" alt="그림입니다.
원본 그림의 이름: CLP00001ed00003.bmp
원본 그림의 크기: 가로 1120pixel, 세로 673pixel" width="378" height="283" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle15 STYLE='line-height:105%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:105%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;</SPAN></SPAN></P>

<P CLASS=HStyle4 STYLE='margin-left:0.0pt;margin-top:3.0pt;margin-bottom:2.0pt;text-indent:39.1pt;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>2. 세션 만료 시 자동 로그아웃</SPAN></P>

<P CLASS=HStyle15 STYLE='margin-left:44.6pt;text-indent:-5.5pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>인가된 관리자의 비활동 시간이 10분을 초과할 경우, 아래와 같은</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 메시지가 뜨며, 세션이 종료됩니다. 관리자는 초기 로그인 화면을 통해 재로그인 할 수 있습니다.</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</SPAN></SPAN></P>

<P CLASS=HStyle15 STYLE='margin-left:38.4pt;text-indent:-38.4pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC2F5F.png" alt="그림입니다.
원본 그림의 이름: CLP0000047c004f.bmp
원본 그림의 크기: 가로 1564pixel, 세로 213pixel" width="499" height="68" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle15></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<CAPTION align="top">
<P CLASS=HStyle15><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:100%'>[표 4-1] 관련 오류/확인 메세지</SPAN>
</P>
</CAPTION><TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:250;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>내용</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#cedeef"  style='width:243;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>설명</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:250;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>필수항목을 모두 확인해주세요.</SPAN></P>
	</TD>
	<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:4%;line-height:160%'>USER ID를 입력하지 않았을 경우 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>발생합니다.</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:250;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>필수항목을 모두 확인해주세요.</SPAN></P>
	</TD>
	<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:5%;line-height:160%'>PASSWORD를 입력하지 않았을 경우 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>발생합니다.</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:250;height:46;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>입력 값을 확인해주세요.</SPAN></P>
	</TD>
	<TD valign="middle" style='width:243;height:46;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:160%'>USER ID는 존재하나, PASSWORD가 정확하지 않은 경우 발생합니다.</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:250;height:57;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>로그인 가능횟수를 초과하셨습니다. </SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-9%;line-height:160%'>일정 시간 동안은 로그인 할 수 없습니다.</SPAN></P>
	</TD>
	<TD valign="middle" style='width:243;height:57;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-13%;line-height:160%'>로그인 실패횟수가 5회 이상이며, 10분간 계정 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-9%;line-height:160%'>잠김이 발생될 경우 발생합니다.</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle15></P>

<P CLASS=HStyle15><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:100%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle3 STYLE='line-height:180%;'>5. <SPAN STYLE='font-size:14.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>개인정보 접속기록조회</SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>5.1. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>접속기록조회</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:2%;line-height:180%'>접속기록</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:5%;line-height:180%'>조회 화면에서는 관리대상시스템에서 수집한 로그 중</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 개인정보를 처리한 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:3%;line-height:180%'>접속기록이력들을</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 조회할 수 있습니다. 5W1H를 기반으로 개인정보 처리이력을 수집 및 저장하여 When(일시), Who(소속, 사원ID, 사원명, 접속IP), Where(시스템), What</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'>(개인정보유형), Why(접근행위), How(접근경로)에 대한 정보를 자세히 조회할 수 있으며, </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>해당 개인정보 접속기록조회 메뉴를 통해 개인정보보호법 및 안전성확보조치기준에서</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 요구하는 ‘접속기록의 보관 및 점검’ 조항을 만족합니다.&nbsp; </SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;text-align:center;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC2FAE.png" alt="그림입니다.
원본 그림의 이름: CLP000006a0001e.bmp
원본 그림의 크기: 가로 1902pixel, 세로 964pixel" width="499" height="253" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;text-align:center;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>일시</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>사원이 개인정보를 처리한 일시</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>소속</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>개인정보를 처리한 사용자의 소속명</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>사용자ID</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>개인정보를 처리한 사용자의 ID</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>개인정보를 처리한 사용자의 이름</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>사용자IP</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>개인정보를 처리한 접속 IP</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>시스템명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>사용자가 개인정보를 처리한 시스템명</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:42;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>개인정보<BR>유형</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:42;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>사용자가 처리한 개인정보유형 정보(ex. 주민등록번호, 핸드폰번호)</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:42;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>수행업무</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:42;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>사용자가 접근한 개인정보가 들어있는 화면에서의 행위<BR>(ex. 조회, 등록, 수정, 삭제, 다운로드, 출력)</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>접근 경로</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>사용자가 접근한 개인정보가 들어있는 화면 주소</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>5.1.1. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>접속기록 조건검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:29.0pt;text-indent:-9.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;접속기록</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>조회 화면에서의 검색 조건은 기본적으로 일시, 기간선택, 시스템, 개인</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:180%'>정보</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-7%;line-height:180%'>유형, 사원ID, 사원명, 접속IP, 소속, 접속유형, 접근행위가 있으며, 조건을 입력 후</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-6%;line-height:180%'>검색</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>버튼을 클릭 시 검색조건에 해당하는 개인정보 접속기록이력이 출력됩니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:28.6pt;text-indent:-8.6pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>&nbsp;&nbsp;&nbsp;이를 통해 원하는 기간의 개인정보 접속기록이나 특정 개인정보 유형을 처리한 사원, 소속별 개인정보 접속기록, 특정 사원이 처리한 개인정보 접속기록 등 다양한 조건별 검색이 가능합니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC301D.png" alt="그림입니다.
원본 그림의 이름: CLP000006a0000b.bmp
원본 그림의 크기: 가로 1792pixel, 세로 265pixel" width="499" height="74" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:105%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:105%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:27;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>기간</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:27;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>기간을 설정하여 해당 기간에 개인정보를 처리한 이력을 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:39;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>시간</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:39;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>체크박스를 통해 해당 조건을 추가/제외 할 수 있으며, 해당 시간동안 개인정보를 처리한 이력을 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:39;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>기간선택</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:39;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>오늘, 일주일, 전월, 올해 1월1일부터 현재까지의 조건에 해당하는 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:160%'>개인정보를 처리한 사용자의 이력을 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>시스템</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>개인정보를 처리한 시스템으로 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>개인정보<BR>유형</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:160%'>주민등록번호, 운전면허번호, 여권번호, 신용카드번호, 건강보험번호,</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:160%'>전화번호, 이메일, 휴대폰번호, 계좌번호, 외국인등록번호 등의 특정</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'> 개인정보유형을 선택하여 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>사용자ID</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>개인정보를 처리한 사용자의 ID로 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>개인정보를 처리한 사용자의 이름으로 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>사용자IP</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>개인정보를 처리한 접속 IP로 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>소속</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>사용자의 소속명으로 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>접속유형</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>접속유형을 선택하여 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>수행업무</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>개인정보처리시스템에서 수행한 업무 대해 선택하여 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>접근 경로</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>개인정보를 처리한 URL로 검색</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>5.1.2. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>접속기록 로그 상세</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:29.0pt;text-indent:-9.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>접속기록조회 화면에서 각각의 접속기록정보를 클릭하게 되면 해당</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 로그에 대한 세부정보를 조회할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-31.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC307B.png" alt="그림입니다.
원본 그림의 이름: CLP000006a0001f.bmp
원본 그림의 크기: 가로 1904pixel, 세로 963pixel" width="499" height="253" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:29.0pt;text-indent:-9.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-6%;line-height:180%'>아래 그림과 같이 개인정보를 처리한 로그에 대한 세부정보가 표시가</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'> 되며 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>개인정보를 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>처리한 사용자의 정보 및 개인정보</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'>유형, 개인정보</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'> 내용</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 등을 볼 수 있습니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:29.0pt;text-indent:-9.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;이를 통해 해당 사용자가 처리한 개인정보 유형 뿐 만 아니라 실제 처리한 개인정보 내용에 대해서도 조회가 가능합니다.&nbsp; </SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%; text-align: center'><IMG src="${rootPath}/resources/image/help/master/PIC3109.png" alt="그림입니다.
원본 그림의 이름: CLP000006a00020.bmp
원본 그림의 크기: 가로 1920pixel, 세로 823pixel" width="499" height="214" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>5.1.3. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>엑셀다운로드</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:29.0pt;text-indent:-9.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'>개인정보 접속기록조회 화면에서 다운로드 버튼을 클릭 시 개인정보</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:4%;line-height:180%'>접속기록조회 화면에서 검색한 로그를 EXCEL로 다운로드할 수</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:180%'> 있습니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:29.0pt;text-indent:-9.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:180%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>이를 통해 기관/기업의 담당자는 개인정보 접속기록에 대해 엑셀 파일형식으로 저장 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:180%'>및 보관하여 기관/기업의 개인정보 접속기록 모니터링 및 점검 시 증적자료로 활용할 수 있습니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:29.0pt;text-indent:-9.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:1%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%; text-align: center'><IMG src="${rootPath}/resources/image/help/master/PIC3168.png" alt="그림입니다.
원본 그림의 이름: CLP000022cc0009.bmp
원본 그림의 크기: 가로 1787pixel, 세로 199pixel" width="499" height="52" vspace="0" hspace="0" border="0">&nbsp;</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:33.4pt;text-indent:-13.4pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-6%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-10%;line-height:180%'>.xls 형태로 저장되며 ‘일시’, ‘소속’, ‘사용자ID’, ‘사용자명’, ‘사용자IP’, ‘시스템명’,</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> ‘접근경로’의 정보를 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:33.4pt;text-indent:-13.4pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%; text-align: center'><IMG src="${rootPath}/resources/image/help/master/PIC3198.png" alt="그림입니다.
원본 그림의 이름: CLP000006a0000e.bmp
원본 그림의 크기: 가로 1471pixel, 세로 637pixel" width="499" height="216" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>5.2. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>백업이력 조회</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>개인정보 로그</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:180%'>조회 내 백업이력조회 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>메뉴는 매일 압축하여 보관하는</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 개인정보 접속로그에 대한 백업일시 및 로그, 위변조 확인 등을 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC31F6.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c0005.bmp
원본 그림의 크기: 가로 1919pixel, 세로 522pixel" width="499" height="136" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>5.2.1. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>백업이력 조건검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>백업이력 조회 화면에서는 일시를 이용하여 검색 할 수 있습니다. </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>조건을 입력 후 검색 버튼을 클릭 시, 검색 조건에 해당하는 백업이력 로그가 출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC3246.png" alt="그림입니다." width="499" height="40" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>5.2.2. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>엑셀다운로드</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>백업이력 조회 화면에서 다운로드 버튼을 클릭하게 되면 백업이력 조회 화면</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>에서 출력한 로그들을 EXCEL로 다운받을 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%; text-align: center'><IMG src="${rootPath}/resources/image/help/master/PIC3256.png" alt="그림입니다.
원본 그림의 이름: CLP000022cc000a.bmp
원본 그림의 크기: 가로 1803pixel, 세로 120pixel" width="499" height="38" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:36.4pt;text-indent:-16.4pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;</SPAN></SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>.xls 형태로 저장되며 ‘일시’, ‘분류’, ‘로그’, ‘파일크기’의 정보를 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:36.4pt;text-indent:-36.4pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC3267.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c0004.bmp
원본 그림의 크기: 가로 841pixel, 세로 176pixel" width="499" height="105" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:36.4pt;text-indent:-36.4pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:36.4pt;text-indent:-36.4pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:36.4pt;text-indent:-36.4pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>5.2.3. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>위변조 확인</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>백업이력 조회 화면에서 위변조 확인 버튼을 클릭하면 해당 백업 파일에 대한 위변조 및 누락 여부를 확인 할 수 있습니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC3297.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c0007.bmp
원본 그림의 크기: 가로 1921pixel, 세로 590pixel" width="499" height="153" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>5.3. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>정보주체자 조회</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;정보주체가 조회</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>메뉴는 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:5%;line-height:180%'>관리대상시스템에서 수집한 로그 중</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 특정 개인 정보에 대한 사용 주체자의 정보 일체를 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC32F5.png" alt="그림입니다.
원본 그림의 이름: CLP000006a00021.bmp
원본 그림의 크기: 가로 1905pixel, 세로 587pixel" width="499" height="154" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>5.3.1. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>정보주체자 조건검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;정보주체자</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'> 조회 화면에서는 일시와 사용된 개인정보를 이용하여 검색 할 수 있습니다. </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>조건을 입력 후 검색 버튼을 클릭 시, 검색한 개인정보의 사용일시, 사용자의 소속, ID, 이름, 접속한 IP와 해당 개인정보에 대한 수행 업무가 출력 됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC3364.png" alt="그림입니다.
원본 그림의 이름: CLP000006a00002.bmp
원본 그림의 크기: 가로 1780pixel, 세로 231pixel" width="499" height="65" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>일시</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>일시를 설정하여 해당 기간에 개인정보를 처리한 이력을 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:52;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>기간선택</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:52;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>오늘, 일주일 조건에 해당하는 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:160%'>개인정보를 처리한 이력을 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:67;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>개인정보<BR>내용</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:67;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:160%'>주민등록번호, 운전면허번호, 여권번호, 신용카드번호, 건강보험번호,</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:160%'>전화번호, 이메일, 휴대폰번호, 계좌번호, 외국인등록번호 등의 특정</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'> 개인정보를 입력하여 입력한 내용에 대한 사용 내역 검색</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle0 STYLE='line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>5.3.2. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>정보주체자 로그 상세</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:29.0pt;text-indent:-9.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;정보주체자 조회</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'> 화면에서 로그 정보를 클릭하게 되면 해당</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 로그에 대한 세부정보를 조회할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:29.0pt;text-indent:-9.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC33C3.png" alt="그림입니다.
원본 그림의 이름: CLP000006a00022.bmp
원본 그림의 크기: 가로 1919pixel, 세로 814pixel" width="499" height="212" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>5.4. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>다운로드 로그조회</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;다운로드 로그조회</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>메뉴는 관리대상시스템에서 다운로드를 시도 한 일시와 다운로드 시도한 정보주체자에 대한 정보, 그리고 관리자가 설정한 해당 경로에 대한 다운로드 대표 사유 내용을 확인 할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC3450.png" alt="그림입니다.
원본 그림의 이름: CLP000006a00023.bmp
원본 그림의 크기: 가로 1905pixel, 세로 820pixel" width="499" height="215" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>5.4.1. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>다운로드 로그 조건검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;다운로드 로그</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>조회 화면에서의 검색 조건은 기간, 시간, 기간선택, 시스템, 접속유형, 사용자ID, 사용자명, 사용자IP, 소속, 접근 경로, 파일명에 대한 정보가 있으며, 검색버튼 클릭 시 검색 조건에 해당하는 다운로드 로그가 출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC34BF.png" alt="그림입니다.
원본 그림의 이름: CLP000006a00005.bmp
원본 그림의 크기: 가로 1789pixel, 세로 287pixel" width="499" height="80" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>기간</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>기간을 설정하여 해당 기간에 다운로드한 이력을 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:41;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>시간</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:41;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>체크박스를 통해 해당 조건을 추가/제외 할 수 있으며, 해당 시간동안 다운로드 한 이력을 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>기간선택</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>오늘, 일주일 조건에 해당하는 다운로드</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:160%'> 이력을 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>시스템</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>다운로드를 시도한 시스템으로 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>접속유형</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>접속유형을 선택하여 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>사용자ID</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>다운로드를 시도한 사용자의 ID로 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>다운로드를 시도한 사용자의 이름으로 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>사용자IP</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>다운로드를 시도한 IP로 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>소속</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>다운로드를 시도한 사용자의 소속으로 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>접근경로</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>다운로드를 시도한 URL로 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>파일명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>다운로드한 파일의 이름으로 조회</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>5.4.2. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>다운로드 로그조회 상세</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:29.0pt;text-indent:-9.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;다운로드 로그조회</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'> 화면에서 로그 정보를 클릭하게 되면 해당</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 로그에 대한 세부정보를 조회할 수 있습니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC352D.png" alt="그림입니다.
원본 그림의 이름: CLP000006a00024.bmp
원본 그림의 크기: 가로 1905pixel, 세로 820pixel" width="499" height="215" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:29.0pt;text-indent:-9.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:29.0pt;text-indent:-9.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><SPAN style='HWP-TAB:1;'>&nbsp;</SPAN>&nbsp;아래 화면과 같이 로그의 세부 정보에 추가로 접근 URL까지 확인 할 수 있으며, 다운로드 사유에 대한 수정을 할 수 있습니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC35BB.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c0008.bmp
원본 그림의 크기: 가로 1904pixel, 세로 649pixel" width="499" height="170" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC3639.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c0009.bmp
원본 그림의 크기: 가로 1905pixel, 세로 643pixel" width="499" height="169" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle3 STYLE='line-height:180%;'>6. <SPAN STYLE='font-size:14.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>시스템 접근이력조회</SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>6.1. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>접근이력조회</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:4%;line-height:180%'>접근이력 조회에서는 개인정보를 처리하기 위해 시스템에 접근한 이력</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:3%;line-height:180%'> 뿐</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'> 아니라 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>시스템에 접근한 모든 이력에 대해 조회할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-4%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC36C6.png" alt="그림입니다.
원본 그림의 이름: CLP000006a00027.bmp
원본 그림의 크기: 가로 1904pixel, 세로 667pixel" width="499" height="175" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;text-align:left;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:24;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>일시</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:24;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>사용자가 시스템에 접근한 일시</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:24;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>소속</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:24;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:5%;line-height:160%'>시스템에 접근한 사용자의 소속명</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:24;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>사용자ID</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:24;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>시스템에 접근한 사용자의 ID</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:24;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:24;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>시스템에 접근한 사용자의 이름</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:24;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>사용자IP</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:24;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>시스템에 접근한 사용자의 IP</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:24;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>시스템명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:24;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>사용자가 접근한 시스템명</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:82;height:24;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>접근경로</SPAN></P>
	</TD>
	<TD valign="middle" style='width:413;height:24;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>사용자가 접근한 시스템 화면 주소</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='margin-left:0.0pt;text-align:left;line-height:180%;'></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>6.1.1. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>접근이력 조건검색</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;접근이력조회 화면에서의 검색 조건은 기본적으로 일시, 기간선택, 시스템, 접근경로, 사용자ID, 사용자명, 접속IP, </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-7%;line-height:180%'>소속이 있으며, </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-10%;line-height:180%'>조건을 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-7%;line-height:180%'>입력 후</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 검</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'>색버튼을 클릭 시 검색조건에 해당하는 시스템 접근이력이 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>출력됩니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC3715.png" alt="그림입니다.
원본 그림의 이름: CLP000006a80009.bmp
원본 그림의 크기: 가로 1774pixel, 세로 228pixel" width="499" height="72" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:76;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>기간</SPAN></P>
	</TD>
	<TD valign="middle" style='width:419;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>기간를 설정하여 해당 날짜에 시스템에 접근한 이력을 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:76;height:48;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>기간선택</SPAN></P>
	</TD>
	<TD valign="middle" style='width:419;height:48;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:5%;line-height:160%'>오늘, 일주일 조건에 해당하는 시스템 접근 이력을 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:76;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>접근경로</SPAN></P>
	</TD>
	<TD valign="middle" style='width:419;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:5%;line-height:160%'>사용자가 접근한 시스템 화면 주소로 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:76;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>시스템</SPAN></P>
	</TD>
	<TD valign="middle" style='width:419;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:5%;line-height:160%'>지정한 시스템의 접근 이력만 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:76;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>접속유형</SPAN></P>
	</TD>
	<TD valign="middle" style='width:419;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>접속유형을 선택하여 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:76;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>사용자ID</SPAN></P>
	</TD>
	<TD valign="middle" style='width:419;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>시스템에 접근한 사용자의 ID로 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:76;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:419;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>시스템에 접근한 사용자의 이름으로 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:76;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>사용자IP</SPAN></P>
	</TD>
	<TD valign="middle" style='width:419;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>시스템에 접근한 사용자의 IP로 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:76;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>소속</SPAN></P>
	</TD>
	<TD valign="middle" style='width:419;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>사용자의 소속명으로 조회</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%;'></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>6.1.2. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>접근이력 로그 상세</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-6%;line-height:180%'>접근이력조회 화면에서 각각의 로그정보를 클릭하게 되면 해당 로그에</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 대한 세부정보가 나타나게 됩니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC3755.png" alt="그림입니다.
원본 그림의 이름: CLP000006a00028.bmp
원본 그림의 크기: 가로 1905pixel, 세로 667pixel" width="499" height="175" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.1pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;아래 그림과 같이 시스템에 접근한 사용자의 정보와 시스템 접근이력에 대해 볼 수 있습니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC37B4.png" alt="그림입니다.
원본 그림의 이름: CLP000006a00029.bmp
원본 그림의 크기: 가로 1903pixel, 세로 899pixel" width="499" height="236" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>6.1.3. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>엑셀다운로드</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>&nbsp; </SPAN><SPAN STYLE='font-size:9.2pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>&nbsp;시스템 접근이력조회 화면에서 다운로드 버튼을 클릭하게 되면 시스템 접근이력조회 화면</SPAN><SPAN STYLE='font-size:9.2pt;font-family:"나눔명조,한컴돋움";line-height:180%'>에서 출력한 로그들을 EXCEL로 다운받을 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.2pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-31.0pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC3803.png" alt="그림입니다.
원본 그림의 이름: CLP000006a8000f.bmp
원본 그림의 크기: 가로 1774pixel, 세로 228pixel" width="499" height="60" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:36.4pt;text-indent:-16.4pt;line-height:180%;'><SPAN STYLE='font-size:8.5pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:8.5pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>.xls 형태로 저장되며 ‘일시’, ‘소속’, ‘사용자ID’, ‘사용자명’, ‘사용자IP’, ‘시스템명’,</SPAN><SPAN STYLE='font-size:8.5pt;font-family:"나눔명조,한컴돋움";line-height:180%'> ‘접근경로’의 정보를 보여줍니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC3833.png" alt="그림입니다.
원본 그림의 이름: CLP000006a8000e.bmp
원본 그림의 크기: 가로 1470pixel, 세로 290pixel" width="499" height="106" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle3 STYLE='line-height:180%;'>7. <SPAN STYLE='font-size:14.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>모니터링</SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>7.1. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>대시보드</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:19.2pt;text-indent:-19.2pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;모니터링 대시보드에서는 실시간 개인정보 처리에 대한 시스템별 로그정보 및</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 서버 정보를 조회할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:19.2pt;text-indent:-19.2pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC3882.png" alt="그림입니다.
원본 그림의 이름: CLP000006a0002a.bmp
원본 그림의 크기: 가로 1904pixel, 세로 969pixel" width="499" height="254" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:16.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:16.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;위 그림과 같이 대시보드 화면은 개인정보 접속기록의 다양한 차트와 리스트로 구성되어 모니터링할 수 있으며, 시스템정보를 통하여 CPU, MEMORY, HDD 사용량을 알 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:5.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:5.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:5.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:5.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>7.1.1. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>시스템별 접속기록 TOP10</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:180%'>시스템별로 개인정보 접속기록 수집 상위 10개에 대한 현황을 실시간 그래프로 보여줍니다.</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-9%;line-height:180%'> 현재 처리가 가장 많이 이루어지고 있는 시스템과 접속기록 건수를 확인할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-9%;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC38D1.png" alt="그림입니다.
원본 그림의 이름: CLP000006a0002e.bmp
원본 그림의 크기: 가로 908pixel, 세로 263pixel" width="499" height="145" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:30.0pt;line-height:180%;'>7.1.2. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>개인정보 접속기록 TOP10</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>&nbsp;&nbsp;&nbsp;실시간으로 수집된 개인정보 접속기록 상위 10명의 현황에 대해 일시, 소속, ID, 사용자명, 접속IP, 시스템명 정보를 테이블 형식으로 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC38F1.png" alt="그림입니다.
원본 그림의 이름: CLP000006a0002f.bmp
원본 그림의 크기: 가로 1033pixel, 세로 393pixel" width="499" height="190" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>7.1.3. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>비정상행위 추이분석</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.8pt;text-indent:-29.8pt;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:7%;line-height:160%'>&nbsp;&nbsp;&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-6%;line-height:160%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-10%;line-height:160%'>비정상행위로 의심되는 개인정보 처리행위에 대한 일별 추이를 그래프로 보여줍니다. </SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:29.8pt;text-indent:-29.8pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-10%;line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;text-align:center;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC3921.png" alt="그림입니다.
원본 그림의 이름: CLP000006a00030.bmp
원본 그림의 크기: 가로 1032pixel, 세로 394pixel" width="499" height="191" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;text-align:center;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>7.1.4. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>개인정보 유형별 접속기록 현황</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:27.4pt;text-indent:-27.4pt;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;수집된 전체 개인정보 접속기록 중 각각의 개인정보 유형이 차지하고 있는 비율을 원형그래프를 통해 보여줍니다. </SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:27.4pt;text-indent:-27.4pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;text-align:center;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC3941.png" alt="그림입니다.
원본 그림의 이름: CLP000006a00031.bmp
원본 그림의 크기: 가로 1034pixel, 세로 395pixel" width="499" height="191" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;text-align:center;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;text-align:center;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>7.1.5. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>비정상행위별 분석현황</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:160%'>&nbsp;6가지의 비정상행위 시나리오</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-11%;line-height:160%'>(고유식별정보 과다처리, 비정상접근, 식별정보 과다처리,</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:160%'> 특정인 처리, 특정시간대 처리, 다운로드 사유확인)에 대한 현황을 그래프를 통해 시각화하여 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'><IMG src="${rootPath}/resources/image/help/master/PIC3971.png" alt="그림입니다.
원본 그림의 이름: CLP000006a00032.bmp
원본 그림의 크기: 가로 1035pixel, 세로 393pixel" width="473" height="180" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>7.1.6. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>소속별 로그 수집 현황</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-indent:-30.0pt;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:160%'>소속별 개인정보 접속기록 현황에 대해 순위, 로그 수, 개인정보 수를</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'> 테이블 형식으로 보여줍니다. 이를 통해 현재 가장 개인정보 처리가 많은 소속을 확인할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-indent:-30.0pt;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC3991.png" alt="그림입니다.
원본 그림의 이름: CLP000006a00034.bmp
원본 그림의 크기: 가로 942pixel, 세로 398pixel" width="499" height="211" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>7.1.7. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>시스템 정보</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:30.0pt;text-indent:-30.0pt;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:2%;line-height:160%'>수집 서버의 CPU, MEMORY, HDD 사용량을 그래프로 표시하여 현재 사용량과</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'> 남은 용량을 시각적으로 확인할 수 있습니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:30.0pt;text-indent:-30.0pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC39C1.png" alt="그림입니다.
원본 그림의 이름: CLP000006a00035.bmp
원본 그림의 크기: 가로 905pixel, 세로 382pixel" width="499" height="211" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>7.2. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>토폴로지</SPAN></P>

<P CLASS=HStyle2><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:160%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:160%'>토폴로지에서는 관리대상시스템에서 사용자가 개인정보 처리를 하게</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:160%'> 되면 실시간으로 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>각 시스템의 사용자와 처리정보를 보여주는 화면을 제공합니다. 실시간 모니터링 뿐 아니라 상단의 검색조건을 통해 이전의 개인정보 접속기록 처리정보에 대한 조회도 가능합니다.</SPAN></P>

<P CLASS=HStyle2><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%; text-align: center'><IMG src="${rootPath}/resources/image/help/master/PIC3A20.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c000a.bmp
원본 그림의 크기: 가로 1920pixel, 세로 915pixel" width="499" height="238" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>7.2.1. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>토폴로지 조건검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:160%'>모니터링의 토폴로지 화면에서의 검색 조건은 기본적으로 실시간 모니터링, </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:160%'>날짜, 시간, IP대역, 개인정보건수 상위 N명, Refresh가 있으며, 해당 조건을 입력 후</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:160%'> 검색버튼을 클릭 시, 검색조건에 해당하는 토폴로지 형태의 모양이 출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:11.0pt;text-indent:-11.0pt;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:42;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>실시간 모니터링</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:42;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>실시간으로 토폴로지를 출력할지 선택 여부(체크)</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>날짜</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>해당 기간에 토폴로지 검색 시 입력(실시간 모니터링 X) </SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>시간</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>해당 시간에 토폴로지 검색 시 입력(실시간 모니터링 X)</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:35;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>개인정보</SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>유형</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:35;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>특정 개인정보 처리 내역에 대한 토플로지 검색 시 입력</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>IP대역</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>특정 IP대역의 토폴로지 검색 시 입력</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>개인정보 건수 상위 N명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>개인정보건수 상위 N명으로 제한 여부 선택(체크), N = 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 선택 가능(실시간 모니터링 X)</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>Refresh</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>실시간 토폴로지 출력을 5, 10, 20, 30, 60초 단위로 조회 가능</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='margin-left:11.0pt;text-indent:-11.0pt;line-height:180%;'></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>7.2.2. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>토폴로지 시스템 접속기록조회</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:5%;line-height:180%'>토폴로지 화면에서 시스템이나 시스템에 연결되어 있는 사용자의</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:180%'> 정보를 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>클릭하게 되면 로그이력을 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%; text-align: center'><IMG src="${rootPath}/resources/image/help/master/PIC3ACD.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c000b.bmp
원본 그림의 크기: 가로 1921pixel, 세로 915pixel" width="499" height="238" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>토폴로지 화면에서 조회하고 싶은 시스템명을 클릭한 결과 아래 그림과</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 같이 시스템의 세부 접속기록 정보를 확인할 수 있는 개인정보 접속기록 페이지로 이동됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%; text-align: center'><IMG src="${rootPath}/resources/image/help/master/PIC3B89.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c000c.bmp
원본 그림의 크기: 가로 1904pixel, 세로 747pixel" width="499" height="196" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle3 STYLE='line-height:180%;'>8. <SPAN STYLE='font-size:14.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>빅데이터분석</SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>8.1. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>비정상위험분석</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>비정상위험분석</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-6%;line-height:180%'>에서는 관리대상시스템에서 수집한 로그 중 비정상행위로</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'> 의심되는 개인정보 처리 업무를 수행한 오남용의심행위자에 대한 정보</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>를 조회할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC3C17.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c000e.bmp
원본 그림의 크기: 가로 1888pixel, 세로 938pixel" width="499" height="248" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:130%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:130%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>8.1.1. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>오남용의심지수</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;오남용의심지수</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>에서는 개인정보 처리에 대한 로그를 분석하여 오남용 의심행위현황에 대한 개인별(금일), 부서별(금일), 기간별(일주일), 고유 식별정보 과다사용에 대한 현황을 확인할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-align:center;text-indent:-30.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC3C85.png" alt="그림입니다." width="499" height="53" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:50%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:50%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>8.1.2. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>오남용 의심행위</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-10%;line-height:180%'>오남용의심행위에서는 금월의 오남용의심행위자에 대해 일별로 확인할 수 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-9%;line-height:180%'>있습니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-align:center;text-indent:-30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC3CB5.png" alt="그림입니다." width="378" height="151" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-6%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>오남용의심행위 그래프에서 특정 날짜 클릭 시 해당하는 날짜의</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-6%;line-height:180%'> 비정상위험분석리스트로 이동합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-6%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-indent:-30.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-6%;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC3DB0.gif" alt="묶음 개체입니다." width="499" height="175" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-indent:-30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-6%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>8.1.3. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>오남용 의심행위 증감</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-align:left;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;오남용 의심행위 증감에서는 금년의 오남용 의심행위 증감량을 월별로 확인할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-align:left;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:4.1pt;text-align:center;text-indent:-4.1pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC3DC1.png" alt="그림입니다." width="378" height="151" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>8.1.4. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>부서별 TOP10 LIST</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>부서별 TOP10 LIST에서는 개인정보 오남용 의심행위가 많이 발생한 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>상위 10개부서 정보를 확인할 수 있습니다. 상세보기 버튼을 클릭 시</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 부서별 비정상위험분석 상세차트로 이동합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC3DE1.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c000f.bmp
원본 그림의 크기: 가로 869pixel, 세로 288pixel" width="499" height="166" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle6 STYLE='line-height:180%;'>8.1.4.1. <SPAN STYLE='font-family:"나눔명조,한컴돋움";font-weight:"bold"'>부서별 비정상위험분석 상세차트</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;부서별 비정상위험분석 상세차트에서는 부서별 개인정보 오남용으로 의심되는 비정상행위의 현황을 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-31.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC3E8E.gif" alt="묶음 개체입니다." width="499" height="223" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>8.1.5. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>개인별 TOP10 LIST</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;개인별 TOP10 LIST에서는 개인정보 오남용 의심행위가 많은 상위 10명의 사용자 정보를 확인할 수 있습니다. 상세보기 버튼을 클릭 시 개인별 비정상위험분석 상세차트로 이동합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-align:center;text-indent:-31.0pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC3E9F.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c0010.bmp
원본 그림의 크기: 가로 867pixel, 세로 286pixel" width="499" height="165" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle6 STYLE='line-height:180%;'>8.1.5.1. <SPAN STYLE='font-family:"나눔명조,한컴돋움";font-weight:"bold"'>비정상위험분석 상세차트</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:2%;line-height:180%'>비정상위험분석 상세차트에서는 부서별 개인정보 오남용으로 의심되는</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 개인의 비정상행위 현황을 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;text-indent:-40.0pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC3F3C.gif" alt="묶음 개체입니다." width="499" height="223" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>8.2. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>비정상위험분석 리스트</SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>8.2.1. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>비정상위험분석 리스트 화면</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:2%;line-height:180%'>비정상위험분석 리스트 화면</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:180%'>에서는 오남용의심행위자</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'> 리스트를 확인할 수</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:8%;line-height:180%'> 있습니다.</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>리스트 중 조회하고 싶은 정보를 클릭 시 상세페이지로 이동합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-1%;font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;text-indent:-40.0pt;line-height:180%;text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC3F6C.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c0012.bmp
원본 그림의 크기: 가로 1904pixel, 세로 735pixel" width="499" height="193" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;text-indent:-40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'>조회 가능한 정보는 다음과 같습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;text-indent:-5.8pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>① 상세조건을 클릭 시 비정상위험분석 상세 페이지로 이동합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:25.5pt;text-indent:8.7pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>② </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>소속을 클릭 시 부서별 비정상위험분석 상세차트로 이동합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.9pt;text-indent:-16.8pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>③ 프로필 상세보기 클릭 시 개인별 비정상위험분석 상세차트로 이동합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle6 STYLE='line-height:180%;'>8.2.1.1. <SPAN STYLE='font-family:"나눔명조,한컴돋움";font-weight:"bold"'>비정상위험분석상세</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-6%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-9%;line-height:180%'>비정상위험분석 리스트에서 추출조건을 누를 경우 비정상위험분석상세</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-6%;line-height:180%'> 페이지로 이동합니다. </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>해당되는 비정상행위 로그 이력을 보여주며, 리스트 중 조회하고 싶은 정보를 클릭 시 상세페이지로 이동합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-4%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;text-indent:-40.0pt;line-height:180%; text-align: center; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC3FDA.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c0013.bmp
원본 그림의 크기: 가로 1921pixel, 세로 710pixel" width="499" height="185" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'>&nbsp;조회 가능한 정보는 다음과 같습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>① </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-7%;line-height:180%'>직원정보를 클릭 시 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:180%'>개인별 비정상위험분석 상세차트로 이동합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:25.5pt;text-indent:14.5pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>② 로그정보를 클릭 시 전체로그 상세페이지로 이동합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:25.5pt;text-indent:14.5pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle7 STYLE='text-indent:0.9pt;line-height:180%;'>8.2.1.1.1. <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;font-weight:"bold";line-height:180%'>비정상위험분석 상세차트</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.0pt;text-indent:0.9pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>&nbsp;비정상위험분석상세의 직원정보를 클릭 시 아래와 같이 개인별 비정상위험분석 상세차트를 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.0pt;text-indent:0.9pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.0pt;text-indent:-50.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC4058.gif" alt="묶음 개체입니다." width="499" height="221" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:25.5pt;text-indent:-25.5pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle7 STYLE='line-height:180%;'>8.2.1.1.2. <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>전체로그상세</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.0pt;line-height:165%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:165%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-7%;line-height:165%'>비정상위험분석상세의 로그정보에서 조회하고 싶은 내역을 클릭 시</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:165%'> 아래와 같이 해당로그에 포함되어 있는 전체로그상세 페이지를 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.0pt;line-height:165%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:165%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.0pt;text-indent:-50.0pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC40C6.gif" alt="묶음 개체입니다." width="499" height="204" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:50%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:50%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.0pt;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:160%'>&nbsp;하단의 업무패턴보기를 클릭 시 개인별 비정상행위분석상세 페이지로 이동합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle6 STYLE='line-height:180%;'>8.2.1.2. <SPAN STYLE='font-family:"나눔명조,한컴돋움";font-weight:"bold"'>부서별 비정상위험분석 상세차트</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;비정상위험분석 상세에서 소속을 누를 경우 부서별 비정상위험분석 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>상세차트로 이동합니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle7 STYLE='line-height:180%;'>8.2.1.2.1. <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;font-weight:"bold";line-height:180%'>부서별 위험현황</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>&nbsp;부서별 위험현황에서는 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>부서별 개인정보 오남용으로 의심되는 5개의 카테고리의 비정상행위 현황을 그래프로 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.0pt;text-indent:-50.0pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC4116.gif" alt="묶음 개체입니다." width="499" height="205" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.0pt;text-indent:-50.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle7 STYLE='line-height:180%;'>8.2.1.2.2. <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>부서별 개인정보 취급량</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-10.9pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:180%'>부서별 개인정보 취급량에서는 일별 개인정보 취급량을 그래프로</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-9%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-11%;line-height:180%'>보여주고, 부서별 시스템 처리현황 및 처리건수를 그래프로 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-50.8pt;line-height:180%; text-align: center; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-9%;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC4220.gif" alt="묶음 개체입니다." width="499" height="263" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle7 STYLE='line-height:180%;'>8.2.1.2.3. <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>부서별 월별 누적량 비교</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:49.1pt;text-indent:-9.2pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:2%;line-height:180%'>부서별 월별 누적량 비교에서는 해당 부서의 월별 개인정보 취급량을</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'> 그래프로 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-50.8pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC42DD.gif" alt="묶음 개체입니다." width="499" height="220" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-10.9pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle7 STYLE='line-height:180%;'>8.2.1.2.4. <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>부서별 요일별 패턴</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-10.9pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:6%;line-height:180%'>&nbsp;부서별 요일별 패턴에서는 해당 부서의 요일별 개인정보 취급량을</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'> 그래프로 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-50.8pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC43A9.gif" alt="묶음 개체입니다." width="499" height="225" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-50.8pt;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-50.8pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle7 STYLE='line-height:180%;'>8.2.1.2.5. <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>부서별 24시간 패턴</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-10.9pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'>부서별 24시간 패턴에서는 해당 부서의 24시간 개인정보 취급량(전일 기준)과</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'> 3개월 평균을 비교하여 그래프로 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-50.8pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC44A4.gif" alt="묶음 개체입니다." width="499" height="211" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-50.8pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle7 STYLE='line-height:180%;'>8.2.1.2.6. <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>부서별 취급유형</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-10.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:160%'>&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:160%'>&nbsp;부서별 취급유형에서는 해당 부서가 처리하는 전체적인 개인정보 유형</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:160%'>, </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:160%'>시스템별 고유식별정보 처리현황, 시스템별 처리량, 업무유형별</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:160%'> 결과에 대한 그래프를 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-50.8pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC45BE.gif" alt="묶음 개체입니다." width="499" height="263" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-50.8pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle6 STYLE='line-height:180%;'>8.2.1.3. <SPAN STYLE='font-family:"나눔명조,한컴돋움";font-weight:"bold"'>개인별 비정상위험분석 상세차트</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>&nbsp;비정상위험분석 상세에서 프로필 상세보기를 누를 경우 개인별 비정상위험분석 상세차트로 이동합니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle7 STYLE='line-height:180%;'>8.2.1.3.1. <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>개인별 위험현황</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-10.9pt;line-height:175%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:175%'>&nbsp;&nbsp;&nbsp;개인별 위험현황에서는 개인</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:175%'>별 개인정보 오남용으로 의심되는 5개의 카테고리의 비정상행위 현황을 그래프로 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-50.8pt;line-height:125%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:125%'><IMG src="${rootPath}/resources/image/help/master/PIC460D.gif" alt="묶음 개체입니다." width="499" height="201" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-50.8pt;line-height:125%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:125%'><BR></SPAN></P>

<P CLASS=HStyle7 STYLE='line-height:180%;'>8.2.1.3.2. <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>개인별 개인정보 취급량</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-10.9pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-6%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-14%;line-height:180%'>개인별 개인정보 취급량에서는 개인의 일별 개인정보 취급량을 그래프로</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-7%;line-height:180%'> 보여주고, </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>시스템 처리현황 및 처리건수를 그래프로 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-50.8pt;line-height:180%; text-align: center; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC46D9.gif" alt="묶음 개체입니다." width="499" height="280" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle7 STYLE='line-height:180%;'>8.2.1.3.3. <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>개인별 월별 누적량 비교</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-10.9pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>&nbsp;개인별 월별 누적량 비교에서는 개인의 월별 개인정보 취급량을</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'> 그래프로 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-50.8pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC4757.gif" alt="묶음 개체입니다." width="499" height="221" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-50.8pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle7 STYLE='line-height:180%;'>8.2.1.3.4. <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>개인별 요일별 패턴</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:49.1pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>&nbsp;개인별 요일별 패턴에서는 개인의 요일별 개인정보 취급량을 그래프로 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-50.8pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC47D5.gif" alt="묶음 개체입니다." width="499" height="226" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-10.9pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-10.9pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-10.9pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle7 STYLE='line-height:180%;'>8.2.1.3.5. <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>개인별 24시간 패턴</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-10.9pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:2%;line-height:180%'>개인별 24시간 패턴에서는 개인의 24시간 개인정보 취급량(전일 기준)</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>과 3개월 평균을 비교하여 그래프로 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;text-indent:-40.0pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC4882.gif" alt="묶음 개체입니다." width="499" height="216" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;text-indent:-40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle7 STYLE='line-height:180%;'>8.2.1.3.6. <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>개인별 취급유형</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-10.9pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>개인별 취급유형에서는 개인이 처리하는 전체적인 개인정보 유형, </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>시스템별 고유식별정보 처리현황, 시스템별 처리량, 업무유형별 결과에 대한 그래프를 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-50.8pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC495E.gif" alt="묶음 개체입니다." width="499" height="262" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-10.9pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle7 STYLE='line-height:180%;'>8.2.1.3.7. <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>개인별 타임라인</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-10.9pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>&nbsp;&nbsp;&nbsp;개인별 타임라인에서는 개인의 업무처리 이력을 가장 최근부터 타임라인 형식으로 보여줍니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-50.8pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC4A2A.gif" alt="묶음 개체입니다." width="499" height="255" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:50.8pt;text-indent:-50.8pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>8.3. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>비정상위험 시나리오</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:180%'>&nbsp;비정상위험 시나리오에서는 비정상행위로 의심되는 처리업무에 대한 추출조건 정보가 출력됩니다. 6</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:4%;line-height:180%'>가지의 비정상행위 카테고리로 이루어진</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>(고유식별정보 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-7%;line-height:180%'>과다처리, 비정상 접근, 식별정보 과다처리, 특정인 처리, 특정시간대 처리, 다운로드 사유확인) </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-11%;line-height:180%'>추출조건명을 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>클릭하면 해당 카테고리에 포함되어 있는 세부 추출조건이 표시됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%; text-align: center; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-4%;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC4A5A.png" alt="그림입니다.
원본 그림의 이름: CLP00003bd40002.bmp
원본 그림의 크기: 가로 1920pixel, 세로 647pixel" width="499" height="168" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:135%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:135%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>8.3.1. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>비정상위험 시나리오 조건검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>비정상위험 시나리오 조건검색 화면에서의 검색 조건은 기본적으로</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>시나리오명이</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'> 있으며, 해당 시나리오명을 입력 후 검색버튼을 클릭 시,</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 검색조건에 해당하는 시나리오가 출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-align:center;text-indent:-30.0pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC4A8A.png" alt="그림입니다.
원본 그림의 이름: CLP0000047c0021.bmp
원본 그림의 크기: 가로 1785pixel, 세로 150pixel" width="499" height="47" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-align:center;text-indent:-30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-align:center;text-indent:-30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-align:center;text-indent:-30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-align:center;text-indent:-30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-align:center;text-indent:-30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-align:center;text-indent:-30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-align:center;text-indent:-30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-align:center;text-indent:-30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-align:center;text-indent:-30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>8.3.2. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>시나리오 리스트</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-10%;line-height:180%'>시나리오 리스트에서는 비정상행위로 의심되는 시나리오에 대한 상세정보 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:180%'>조회 및 사용여부를 설정할 수 있습니다. </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>상세시나리오명을 클릭하면 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:180%'>시나리오 상세페이지가 출력됩니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-8%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-indent:-30.0pt;line-height:180%; text-align: center; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC4AF8.gif" alt="묶음 개체입니다." width="499" height="141" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-align:center;text-indent:-30.0pt;line-height:110%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:35;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>상세</SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>시나리오명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:35;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>비정상행위로 의심되는 시나리오 명</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>임계치</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>비정상행위를 판별하기 위한 기준이 되는 경계 값</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>사용여부</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>시나리오 추출조건의 사용여부 </SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>실시간</SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>추출여부</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>시나리오 추출조건의 실시간추출여부</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-align:center;text-indent:-30.0pt;line-height:110%;'></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-8%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:180%'>&nbsp;상세 시나리오 추가는 기관의 개인정보 처리업무의 특성과 환경에 맞게 ㈜이지서티 방문 엔지니어에 의해 관리됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle6 STYLE='line-height:180%;'>8.3.2.1. <SPAN STYLE='font-family:"나눔명조,한컴돋움";font-weight:"bold"'>시나리오 상세</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-9%;line-height:180%'>시나리오 상세 페이지에서는 시나리오에 대한 상세정보 조회와 임계치, 위험지수 설정 및 시나리오 사용여부를 설정할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;text-indent:-40.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-9%;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC4B38.gif" alt="묶음 개체입니다." width="499" height="190" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>8.3.3. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>시나리오 조건 검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:180%'>시나리오 리스트 화면에서의 검색 조건은 기본적으로 사용여부, </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:2%;line-height:180%'>실시간추출</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-6%;line-height:180%'>여부가 있습니다. 조건을 입력 후 검색버튼을 클릭 시, </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:180%'>시나리오가 출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-8%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-align:center;text-indent:-30.0pt;line-height:140%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:140%'><IMG src="${rootPath}/resources/image/help/master/PIC4B39.png" alt="그림입니다.
원본 그림의 이름: CLP0000047c0026.bmp
원본 그림의 크기: 가로 1785pixel, 세로 158pixel" width="499" height="41" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-align:center;text-indent:-30.0pt;line-height:140%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:140%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-align:center;text-indent:-30.0pt;line-height:140%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>사용여부</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>시나리오 추출조건의 사용여부 설정하여 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:35;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>실시간</SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>추출여부</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:35;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>시나리오 추출조건의 실시간추출여부 설정하여 조회 </SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-align:center;text-indent:-30.0pt;line-height:140%;'></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-align:center;text-indent:-30.0pt;line-height:140%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:140%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>8.3.4. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>시나리오 엑셀다운로드</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-9%;line-height:180%'>시나리오 리스트 화면에서 엑셀다운로드 버튼을 클릭하게 되면 시나리오 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:180%'>리스트에 나타난 내용에 대해 EXCEL로 다운받을 수 있는 기능입니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-8%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-indent:-30.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC4B97.gif" alt="묶음 개체입니다." width="499" height="193" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:8.8pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:8.8pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>&nbsp;.xls 형태로 저장되며 “상세시나리오명”, “임계치”, “사용여부”, “실시간추출여부”의 정보를 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC4B98.png" alt="그림입니다.
원본 그림의 이름: CLP0000047c0028.bmp
원본 그림의 크기: 가로 840pixel, 세로 112pixel" width="499" height="68" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle3 STYLE='line-height:180%;'>9. <SPAN STYLE='font-size:14.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>인텔리젼스 차트</SPAN></P>

<P CLASS=HStyle4>9.1. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>개인정보 처리 현황</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-14%;line-height:180%'>개인정보 처리 현황은</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-13%;line-height:180%'> 각 시스템에서 처리된 모든 개인정보 건수가 표현이 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-12%;line-height:180%'>됩니다. 특정 시스템별 개인정보 처리 현황을 제외하고 싶은 경우, 그래프 상단에 위치한 시스템명을 클릭하면 해당 시스템이 제외되어 그래프에 표시됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-12%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-indent:-30.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC4C84.gif" alt="묶음 개체입니다." width="499" height="229" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4>9.2. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>시스템별 처리 현황</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:21.9pt;text-indent:-21.9pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:180%'>시스템별 처리 현황은 시스템 내에서 개인정보 유형별 개인정보검출 건수를 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-7%;line-height:180%'>표현합니다. </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>특정 시스템의 현황을 제외하고 싶은 경우, 그래프 오른쪽에 위치한</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-7%;line-height:180%'> 시스템명을 클릭하면 해당 시스템이 제외되어 그래프에 표시됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:21.9pt;text-indent:-21.9pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-7%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;line-height:180%; text-align: center; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-7%;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC4D40.gif" alt="묶음 개체입니다." width="499" height="221" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle4>9.3. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>개인정보 처리부서 TOP 10</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>개인정보 처리부서 TOP10은 개인정보를 가장 많이 처리한 부서 10개에</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 대한 개인정보검출 건수를 표현합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC4DBE.gif" alt="묶음 개체입니다." width="499" height="222" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>9.4. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>개인정보 취급자 TOP10</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:2%;line-height:180%'>개인정보 취급자 TOP10은 개인정보를 가장 많이 취급한 10명의 사용자에 대한</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 개인정보검출 총 건수를 표현합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC4E4C.gif" alt="묶음 개체입니다." width="499" height="232" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>9.5. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>개인정보 유형별 현황</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:41.9pt;text-indent:-21.9pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;개인정보 유형별 현황은 각각의 유형별 총 검출된 건수를 표현합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:41.9pt;text-indent:-21.9pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:41.9pt;text-indent:-41.9pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC4F37.gif" alt="묶음 개체입니다." width="499" height="235" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>9.6. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>개인정보 취급 유형별 현황</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;개인정보 취급 유형별 현황은 각각의 취급 유형별 총 검출된 건수를 표현합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:41.9pt;text-indent:-41.9pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC4FE4.gif" alt="묶음 개체입니다." width="499" height="225" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>9.7. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>비정상 위험부서 TOP10</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>비정상 위험부서 TOP10에서는 관리대상시스템으로부터 수집한 로그 중 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>개인정보처리 업무 시 오남용으로 의심되는 비정상행위 건수가 많이 발생한 상위 10개의 부서를 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC5072.gif" alt="묶음 개체입니다." width="499" height="227" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>9.8. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>비정상 위험 유형별 현황</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>비정상 위험 유형별 현황은 비정상행위 유형별 발생 건수를 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-5%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%; text-align: center'><IMG src="${rootPath}/resources/image/help/master/PIC511F.gif" alt="묶음 개체입니다." width="499" height="242" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle4>9.9. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>CPU, MEMORY, HDD 사용량</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;CPU, MEMORY, HDD 사용량은 현재 구동중인 서버의 간략한 정보 내용을 그래프로 표현하여 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC517D.gif" alt="묶음 개체입니다." width="499" height="242" vspace="0" hspace="0" border="0"></SPAN></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle3 STYLE='line-height:180%;'>10. <SPAN STYLE='font-size:14.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>통계 및 보고</SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>10.1. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>기간별유형통계</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'>기간별유형통계 화면에서는 설정한 기간에 대해 일별 처리된 개인정보</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:180%'> 유형에 대한 통계를 조회할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:1%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%; text-align: center'><IMG src="${rootPath}/resources/image/help/master/PIC51AD.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c0014.bmp
원본 그림의 크기: 가로 1921pixel, 세로 692pixel" width="499" height="180" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:25.5pt;text-indent:-5.5pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-6%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:25.5pt;text-indent:-5.5pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-6%;line-height:180%'>&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:180%'>기간별유형통계를 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-11%;line-height:180%'>조회했을 때 출력되는 데이터의 내용은 다음과 같습니다.</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'> ■부분은 기간</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>별 가장 많이 처리된 개인정보유형을 알려줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;text-align:center;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:130%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:130%'>날짜</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>기간을 기준으로 개인정보가 처리된 로그의 날짜를 출력</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:71;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:130%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:130%'>개인정보<BR>유형</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:71;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:160%'>기간을 기준으로 각각의 개인정보유형인 주민등록번호, 외국인등록번호,</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:160%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:160%'>운전면허번호, 여권번호, 신용카드번호, 건강보험번호, 전화번호, 이메일,</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:160%'> 휴대폰번호, 계좌번호가 처리된 로그 이력 건수를 출력 </SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='margin-left:0.0pt;text-align:center;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:80%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:80%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>10.1.1. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>기간별유형통계 조건검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:180%'>기간별유형통계 조회 화면에서의 검색 조건은 기본적으로 기간, 기간선택</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>(일별, 월별, 년별)이 있으며, 조건을 입력 후 검색버튼을 클릭 시, 검색조건에</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 해당하는 기간별유형통계가 출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:130%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:130%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:130%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:130%'><IMG src="${rootPath}/resources/image/help/master/PIC51ED.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c001c.bmp
원본 그림의 크기: 가로 1806pixel, 세로 223pixel" width="499" height="62" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:130%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:130%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>기간</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>기간별 통계를 내기 위한 개인정보 처리 기간 설정</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>일별</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>당월 기준으로 1일 ～ 말일까지 일별 개인정보유형통계 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:42;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>월별</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:42;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:160%'>올해 기준으로 1월 1일 ～ 해당년도 말일(12월 31일)까지 월별 개인정보</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>유형통계 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:42;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>년별</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:42;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:160%'>최초에 설치된 연도부터 금년도까지의 각 연도의 기간별</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:160%'> 개인정보유형통계 검색</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>10.2. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>시스템별유형통계</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>시스템</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-12%;line-height:180%'>별유형통계 화면에서는 시스템별 처리된 개인정보 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:180%'>유형에 대한 통계를 조회할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%; text-align: center'><IMG src="${rootPath}/resources/image/help/master/PIC524C.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c0016.bmp
원본 그림의 크기: 가로 1920pixel, 세로 887pixel" width="499" height="231" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'>위 그림에서 보면 시스템별유형통계를 조회했을 때 출력되는 데이터의 내용입니다.</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:3%;line-height:180%'>■부분은 시스템별 가장 많이 처리된 개인정보유형을 알려줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>날짜</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>시스템을 기준으로 개인정보가 처리된 로그의 날짜를 출력</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>시스템명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>시스템을 기준으로 개인정보가 처리된 로그의 시스템을 출력</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:67;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>개인정보</SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>유형</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:67;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-6%;line-height:160%'>시스템을 기준으로 각각의 개인정보유형인 주민등록번호, 외국인등록번호,</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:160%'> 운전면허번호, 여권번호, 신용카드번호, 건강보험번호, 전화번호, 이메일, 휴대폰번호, 계좌번호의 처리된 로그 이력 건수를 출력</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>10.2.1. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>시스템별유형통계 조건검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'>시스템별유형통계 조회 화면에서의 검색 조건은 기본적으로 일시, 시스템명</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-6%;line-height:180%'>이 있으며, 해당 조건을 입력 후</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-12%;line-height:180%'>검색버튼을 클릭 시,</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-9%;line-height:180%'> 검색조건에 해당하는 시스템별유형통계가 출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-9%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%; text-align: center'><IMG src="${rootPath}/resources/image/help/master/PIC52D9.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c0017.bmp
원본 그림의 크기: 가로 1806pixel, 세로 223pixel" width="499" height="62" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>기간</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>시스템별 통계를 내기 위한 개인정보 처리 기간 설정</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>시스템명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>등록되어 있는 시스템을 설정하여 통계 조회</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>10.3. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>소속별유형통계</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;소속</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:180%'>별유형통계 화면에서는 설정한 기간에 대해 소속별 처리된 개인정보 유형에 대한 통계를 조회할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-8%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC5338.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c0018.bmp
원본 그림의 크기: 가로 1921pixel, 세로 777pixel" width="499" height="202" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:0.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-7%;line-height:180%'>위 그림에서 보면 소속별유형통계를 조회했을 때 출력되는 데이터의 내용입니다.</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'> ■부분은 소속별 가장 많이 처리된 개인정보유형을 알려줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>날짜</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>소속을 기준으로 개인정보가 처리된 로그의 날짜를 출력</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>소속명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>소속을 기준으로 개인정보가 처리된 로그의 소속을 출력</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:71;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>개인정보</SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>유형</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:71;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:160%'>소속을 기준으로 각각의 개인정보유형인 주민등록번호, 외국인등록번호, 운전면허번호, 여권번호, 신용카드번호, 건강보험번호, 전화번호, 이메일,</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:160%'> 휴대폰번호, 계좌번호의 처리된 로그 이력 건수를 출력 </SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>10.3.1. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>소속별유형통계 조건검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:12%;line-height:180%'>소속별유형통계 조회 화면에서의 검색 조건은 기본적으로 일시, </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:17%;line-height:180%'>소속</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:5%;line-height:180%'>이 있으며, 조건을 입력 후</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-12%;line-height:180%'>검색버튼을</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 클릭 시, 검색조건에 해당하는 소속별유형통계가 출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC5387.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c0019.bmp
원본 그림의 크기: 가로 1805pixel, 세로 224pixel" width="499" height="62" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;line-height:90%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:90%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>기간</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>소속별 통계를 내기 위한 개인정보 처리 기간 설정</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>소속</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>등록되어 있는 소속명을 설정하여 통계 조회</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='line-height:125%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:125%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:125%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:125%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:125%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:125%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:125%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:125%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:125%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:125%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:125%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:125%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:125%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:125%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:125%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:125%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:125%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:125%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:125%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:125%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:125%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:125%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:125%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:125%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:125%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:125%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:125%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:125%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:125%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:125%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:125%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:125%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>10.4. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>개인별유형통계</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-6%;line-height:180%'>&nbsp;개인별유형통계 화면에서는 설정한 기간에 대해 개인별 처리된 개인정보 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>유형에 대한 통계를 조회할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:20.0pt;text-indent:-20.0pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC53D6.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c001a.bmp
원본 그림의 크기: 가로 1921pixel, 세로 852pixel" width="499" height="221" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>위 그림에서 보면 개인별유형통계를 조회했을 때 출력되는 데이터의 내용은 다음과 같습니다. ■부분은 개인별 가장 많이 처리된 개인정보유형을 알려줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>날짜</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>개인을 기준으로 개인정보가 처리된 로그의 날짜를 출력</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>사원명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>개인을 기준으로 개인정보가 처리된 로그의 사원 이름을 출력</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:71;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>개인정보</SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>유형</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:71;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:160%'>개인을 기준으로 각각의 개인정보유형인 주민등록번호, 외국인등록번호,</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:160%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:160%'>운전면허번호, 여권번호, 신용카드번호, 건강보험번호, 전화번호, 이메일,</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:160%'> 휴대폰번호, 계좌번호의 처리된 로그 이력 건수를 출력</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'> </SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>10.4.1. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>개인별유형통계 조건검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:180%'>&nbsp;개인별유형통계 조회 화면에서의 검색 조건은 기본적으로 일시, 사용자명, 사용자ID가</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 있으며, 해당 조건을 입력 후 검색버튼을 클릭 시, 검색조건에 해당하는 개인별유형통계가 출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC5435.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c001b.bmp
원본 그림의 크기: 가로 1805pixel, 세로 223pixel" width="499" height="62" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>기간</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>개인별 통계를 내기 위한 개인정보 처리 기간 설정</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>사용자ID</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>사용자의 ID를 입력하여 통계 조회</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>사용자의 이름을 입력하여 통계 조회</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>10.5. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>점검보고서</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>점검</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-9%;line-height:180%'>보고서에서는 관리자가</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'> 기간을 선택하여 로그 현황에 대한 보고서를 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>생성할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>10.5.1. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>접속기록보고서 생성</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>접속기록보고서는 생성하고자 하는 보고서 분류와 기간을 선택하고 대상 시스템을 선택 후 보고서 생성 버튼을 눌러 생성할 수 있습니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-4%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:31.0pt;text-indent:-31.0pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC54C3.gif" alt="묶음 개체입니다." width="499" height="229" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>&nbsp;보고서 생성 클릭 시 팝업창이 생성되며 아래와 같은 점검보고서가 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>나타납니다. 출력이 필요한 경우 오른쪽 상단의 출력 버튼을 눌러 PC와 연결된 프린터를 통해서 출력이 가능합니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;필요에 따라 출력이 아닌 별도의 doc 파일로 저장 역시 가능하며, 이는 팝업된 보고서&nbsp; 오른쪽 상단 생성 버튼을 눌러 현재 생성된 문서를 저장할 수 있으며, 점검보고서 관리 페이지에서 다운로드 할 수 있습니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;text-indent:-20.0pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC54D3.png" alt="그림입니다.
원본 그림의 이름: CLP000006a00015.bmp
원본 그림의 크기: 가로 785pixel, 세로 833pixel" width="499" height="530" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>점검보고서에는 개인정보 접속기록 현황 및 비정상행위 현황에 대한 다양한 그래프와 분석정보가 포함되어 있으며, 이를 통해 각 기관/기업의 개인정보보호담당자는 월별 개인정보 접속기록을 점검할 수 있고, 점검 내역에 대한 담당자의 의견을 기입할 수 있는 의견란을 제공하여 점검사항에 대한 담당자의 의견을 추가하여 점검보고서를 출력할 수 있습니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>10.6. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>점검보고서 관리</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>점검</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-9%;line-height:180%'>보고서에서 생성, 저장한 파일을 관리할 수 있는 페이지 이며, 점검보고서를 doc파일로 다운로드 받을 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC5522.png" alt="그림입니다.
원본 그림의 이름: CLP000006a00018.bmp
원본 그림의 크기: 가로 1920pixel, 세로 462pixel" width="499" height="120" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>No</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>문서 번호</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:43;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>보고서 생성일</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:43;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>점검보고서에서 점검보고서를 생성한 일시</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>작성자</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>점검보고서에서 점검보고서를 생성한 사용자</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>보고서 제목</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>점검보고서 제목 다운로드 기록이 있는 보고서는 회색으로 표시</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:39;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>다운로드 일자</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:39;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:160%'>마지막으로 점검보고서를 다운로드 한 일시</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:81;height:39;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>다운로드 횟수</SPAN></P>
	</TD>
	<TD valign="middle" style='width:414;height:39;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:160%'>다운로드를 받은 횟수</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='margin-left:0.0pt;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC5562.png" alt="그림입니다.
원본 그림의 이름: CLP000006a0001a.bmp
원본 그림의 크기: 가로 1920pixel, 세로 425pixel" width="499" height="111" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle3 STYLE='line-height:180%;'>11. <SPAN STYLE='font-size:14.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>정책설정관리</SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>11.1. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>개인정보설정</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>개인정보설정에서는 개인정보유형 리스트가 출력됩니다. 개인정보설정이</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>필요한 개인정보유형을 클릭하면 개인정보설정 상세페이지가 출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-5%;line-height:180%'>&nbsp;</SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%; text-align: center'><IMG src="${rootPath}/resources/image/help/master/PIC55B1.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c001e.bmp
원본 그림의 크기: 가로 1920pixel, 세로 812pixel" width="499" height="211" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>11.1.1. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>개인정보설정 상세</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.4pt;text-indent:-5.5pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'>개인정보설정상세에서는 개인정보유형에 대한 수집 사용여부와 개인정보 마스킹 사용 여부를 설정 할 수 있습니다. 마스킹 사용 시 설정 후 수집되는 개인정보의 일부가 감추어집니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.4pt;text-indent:-5.5pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC55F0.png" alt="그림입니다.
원본 그림의 이름: CLP000006a00038.bmp
원본 그림의 크기: 가로 1331pixel, 세로 344pixel" width="499" height="129" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>11.2. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>위험도 기준설정</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;위험도기준설정에서는 비정상행위 위험도에 대한 5단계에 대한 위험지수를 설정할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC5778.gif" alt="묶음 개체입니다." width="499" height="253" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>11.3. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>예외처리DB</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>개인정보 로그조회 화면에 생성된 로그를 기준으로 관리자가 판단 시 해당 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-6%;line-height:180%'>개인정보가</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>실제 개인정보가 아니라고 판단되는 경우 지속적으로 검출되는 것을 차단하기 위해 예외처리DB로 등록하여</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'> 추후에 개인정보로 검출되지 않도록 설정할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC5844.gif" alt="묶음 개체입니다." width="499" height="226" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>11.3.1. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>예외처리DB 검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;예외처리</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-7%;line-height:180%'>DB에 등록되어 있는 개인정보를 조회하기 위한 화면입니다.(예외처리DB 메뉴 상단)</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 조회하고자 하는 개인정보유형 및 개인정보 입력 후 검색 버튼으로 조회할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle3 STYLE='margin-left:20.0pt;text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC5855.png" alt="그림입니다." width="499" height="57" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>11.3.2. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>예외처리DB 등록</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:2%;line-height:180%'>&nbsp;예외처리</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'>DB에 개인정보를 등록하고자 할 때 사용하는 화면입니다.</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>(예외처리DB 메뉴 중단)</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>등록하고자 하는 개인정보유형을 선택하고 개인정보 입력 후 추가 버튼으로</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 등록할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%; text-align: center'><IMG src="${rootPath}/resources/image/help/master/PIC5875.png" alt="그림입니다." width="499" height="37" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>11.3.3. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>예외처리DB 조회</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;예외처리</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-9%;line-height:180%'>DB에 등록되어 있는 개인정보를 확인할 수 있는 화면입니다.(예외처리DB 메뉴 하단)</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 상단 검색을 통해 조회 시 해당 조건에 맞는 개인정보를 조회할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC5895.png" alt="그림입니다.
원본 그림의 이름: CLP00001bfc0003.bmp
원본 그림의 크기: 가로 1673pixel, 세로 294pixel" width="499" height="84" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>11.3.4. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>예외처리DB 삭제</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:33.6pt;text-indent:-13.6pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;예외처리</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-9%;line-height:180%'>DB에 등록되어 있는 개인정보를 삭제할 수 있는 화면입니다.</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-6%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-15%;line-height:180%'>(예외처리DB 메뉴 하단) </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-10%;line-height:180%'>예외처리DB에 잘못 등록되었거나 해당 개인정보에 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-7%;line-height:180%'>대해 다시 검색을 원할 경우 삭제를</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'> 통해 다시 검색할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:33.6pt;text-indent:-13.6pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:33.6pt;text-indent:-33.6pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC58C5.png" alt="그림입니다.
원본 그림의 이름: CLP00001bfc0004.bmp
원본 그림의 크기: 가로 1691pixel, 세로 299pixel" width="499" height="86" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle3 STYLE='line-height:180%;'>12. <SPAN STYLE='font-size:14.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>환경설정</SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>12.1. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>메뉴관리</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-9%;line-height:180%'>메뉴관리 페이지를 들어가면 관리자 메뉴 정보 확인 및 수정이 가능합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC5972.gif" alt="묶음 개체입니다." width="499" height="222" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;메뉴관리의 세부 항목 내역은 다음과 같습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;① 사용자 메뉴명을 입력합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;② 하위 디렉토리를 설정합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;③ 정렬 순서를 설정합니다.(좌측으로부터 정렬)</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;④ </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:180%'>부모메뉴명을 선택하여 메뉴 위치를 설정합니다.(ROOT = 최상위 메뉴)</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;⑤ 사용 여부를 설정합니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;⑥ 메뉴설명을 입력할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;⑦ </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>권한대상자 중에서 해당 메뉴에 대한 권한 설정을 할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>12.2. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>권한관리</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-6%;line-height:180%'>권한관리 페이지를 들어가면 관리자 계정 사용여부 확인 및 권한 설정이</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 가능합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC59F0.gif" alt="묶음 개체입니다." width="499" height="197" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>12.2.1. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>권한관리 기능</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:180%'>&nbsp;권한관리 계정 중 하나를 클릭하면 계정 사용여부 및 권한 설정</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>(관리자명을</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>입력)이 가능합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-31.0pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC5A3F.gif" alt="묶음 개체입니다." width="499" height="137" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>12.3. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>조직관리</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:180%'>환경설정의 조직관리를 들어가면 해당 기관에 등록되어 있는 소속 정보를 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>확인할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC5A5F.png" alt="그림입니다.
원본 그림의 이름: CLP000006a0003c.bmp
원본 그림의 크기: 가로 1917pixel, 세로 510pixel" width="499" height="133" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>12.4. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>사원관리</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;환경설정의 사원관리 페이지를 들어가면 해당 기관에 등록되어있는 사원들의 정보를 확인할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC5AAF.png" alt="그림입니다.
원본 그림의 이름: CLP000006a0003d.bmp
원본 그림의 크기: 가로 1896pixel, 세로 912pixel" width="499" height="240" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>12.4.1. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>사원관리 조건검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>사원관리 화면에서의 검색 조건은 기본적으로 사용자명, 사용자ID, 상태,</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 소속, </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:180%'>시스템명이 있습니다. </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:3%;line-height:180%'>조건을 입력 후 검색버튼을 클릭 시, </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:2%;line-height:180%'>검색조건에 해당하는 사원 정보에 대한 이력이</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC5B0D.png" alt="그림입니다.
원본 그림의 이름: CLP000006a00039.bmp
원본 그림의 크기: 가로 1752pixel, 세로 166pixel" width="495" height="46" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:140%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:140%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:76;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:419;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>고객사에 등록되어 있는 사용자명으로 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:76;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>사용자ID</SPAN></P>
	</TD>
	<TD valign="middle" style='width:419;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>고객사에 등록되어 있는 사용자ID로 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:76;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>상태</SPAN></P>
	</TD>
	<TD valign="middle" style='width:419;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>재직, 퇴직, 휴직, 휴가에 대한 사원의 상태로 검색 </SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:76;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>소속</SPAN></P>
	</TD>
	<TD valign="middle" style='width:419;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>고객사에 등록되어 있는 소속으로 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:76;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>시스템명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:419;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>고객사에 등록되어 있는 시스템명으로 검색</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>12.4.2. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>사원관리상세</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>사원 관리 화면에서 각각의 사원정보를 클릭하게 되면 클릭한 사원에</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 대한 세부정보가 나타나게 됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-31.0pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC5B5C.png" alt="그림입니다.
원본 그림의 이름: CLP000006a0003a.bmp
원본 그림의 크기: 가로 1898pixel, 세로 940pixel" width="499" height="247" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:35.4pt;text-indent:-5.5pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:35.4pt;text-indent:-5.5pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;아래 그림에서 보면 사원 관리 페이지에서 클릭한 사원에 대한 세부 정보를 볼 수 있으며 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:2%;line-height:180%'>사원명, 소속명, 사원ID, 시스템명, 상태, IP, E-MAIL, 핸드폰번호, 등록일시, 수정일시와 같은 내용들을</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 볼 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:35.4pt;text-indent:-5.5pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:35.4pt;text-indent:-35.4pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC5C48.gif" alt="묶음 개체입니다." width="499" height="131" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:35.4pt;text-indent:-5.5pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:35.4pt;text-indent:-5.5pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:35.4pt;text-indent:-5.5pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:35.4pt;text-indent:-5.5pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:35.4pt;text-indent:-5.5pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle6 STYLE='line-height:180%;'>12.4.2.1. <SPAN STYLE='font-family:"나눔명조,한컴돋움";font-weight:"bold"'>사원상세 내용 수정</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.8pt;text-indent:-20.8pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;사원상세 화면에서 해당 사원 정보에 대한 내용을 수정할 때 수정 버튼을 누르면</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>수정됩니다. 이때, 수정할 수 있는 내용으로는 사원명, 소속명, 상태, 사원IP가 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.8pt;text-indent:-20.8pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-5%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:36.4pt;text-indent:-36.4pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC5CC6.gif" alt="묶음 개체입니다." width="499" height="130" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:36.4pt;text-indent:-16.4pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle6 STYLE='line-height:180%;'>12.4.2.2. <SPAN STYLE='font-family:"나눔명조,한컴돋움";font-weight:"bold"'>사원상세 내용 삭제</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>&nbsp;사원상세 화면에서 해당 사원 정보에 대한 내용을 삭제할 때 삭제 버튼을 누르면</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 사원 정보가 삭제되는 것을 확인할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;text-indent:-40.0pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC5D25.gif" alt="묶음 개체입니다." width="499" height="130" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>12.4.3. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>사원관리 신규 등록</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-12%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-7%;line-height:180%'>사원관리 상세 화면에서 사원을 등록하여 개인정보 탐지에 있어 사원 정보를 자세히</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:2%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-9%;line-height:180%'>확</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:180%'>인하고 싶을 때 신규 버튼을 클릭하면 사원 신규 등록 화면이 나오게 됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-8%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%; text-align: center'><IMG src="${rootPath}/resources/image/help/master/PIC5DB2.gif" alt="묶음 개체입니다." width="499" height="222" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-12%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-12%;line-height:180%'>&nbsp;사원 신규 등록 화면에서 사원명, 소속명, 사원ID, 상태, IP 등을 입력하여 사원정보를 추가 할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%; text-align: center'><IMG src="${rootPath}/resources/image/help/master/PIC5DD2.png" alt="그림입니다.
원본 그림의 이름: CLP000006a0003b.bmp
원본 그림의 크기: 가로 1920pixel, 세로 506pixel" width="499" height="132" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>12.4.4. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>엑셀업로드</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'>사원관리 화면에서 엑셀버튼을 클릭하게 되면 업로드 기능을 사용할 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>수 있습니다. </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>해당 기능을 통해 엑셀로 작성한 사원리스트를 한 번에</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 등록할 수 있습니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%; text-align: center'><IMG src="${rootPath}/resources/image/help/master/PIC5E02.png" alt="그림입니다.
원본 그림의 이름: CLP00001ed00014.bmp
원본 그림의 크기: 가로 1774pixel, 세로 324pixel" width="499" height="91" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;업로드 버튼을 선택하면 다음과 같은 팝업창이 출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC5E13.png" alt="그림입니다.
원본 그림의 이름: CLP000016400005.bmp
원본 그림의 크기: 가로 540pixel, 세로 207pixel" width="454" height="170" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:160%'>해당 팝업창의 양식다운로드 버튼을 클릭하면 사원 리스트를 작성할 엑셀 양식을 다운받을 수 있습니다. 사원관리 업로드 양식은 다음과 같으며, “소속”, “사원명”, “사원ID”, “사원IP”, “상태”, “E-MAIL”, “핸드폰번호” 정보를 입력할 수 있습니다. 필수 입력항목은 “소속”, “사원명”, “사원ID”, “상태”이며 “상태”는 재직, 퇴직, 휴직, 휴가 중 하나를 선택하여 입력하거나 공백으로 처리 가능합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;text-indent:-30.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC5E33.png" alt="그림입니다.
원본 그림의 이름: CLP0000047c0036.bmp
원본 그림의 크기: 가로 1729pixel, 세로 278pixel" width="499" height="81" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:35.4pt;text-indent:-5.5pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;엑셀파일에 사원정보를 입력하여 엑셀업로드 버튼을 누르면 사원관리 리스트에 등록됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:35.4pt;text-indent:-5.5pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>12.4.5. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>엑셀다운로드</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;사원관리 화면에서 엑셀 버튼을 클릭하게 되면 다운로드 기능을 사용할 수 있습니다. 사원관리 화면에서 출력한 로그들을 EXCEL로 다운받을 수 있는 기능입니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%; text-align: center'><IMG src="${rootPath}/resources/image/help/master/PIC5E63.png" alt="그림입니다.
원본 그림의 이름: CLP00001ed00015.bmp
원본 그림의 크기: 가로 1775pixel, 세로 325pixel" width="499" height="91" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;.xls 형태로 저장되며 “소속”, “사용자명”, &quot;사용자ID”, &quot;시스템명”, &quot;IP”, “상태”의 정보를 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%; text-align: center'><IMG src="${rootPath}/resources/image/help/master/PIC5E83.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c001d.bmp
원본 그림의 크기: 가로 1261pixel, 세로 222pixel" width="499" height="88" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>12.5. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>관리자관리</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>환경설정의 관리자관리 페이지를 들어가면 PSM Management 로그인 관리자의</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 정보를 확인할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%; text-align: center'><IMG src="${rootPath}/resources/image/help/master/PIC5F21.gif" alt="묶음 개체입니다." width="499" height="176" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>12.5.1. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>관리자관리 조건검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>관리자관리 화면에서의 검색 조건은 기본적으로 관리자ID, 관리자명, </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>소속이 있으며, </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>해당 조건을 입력 후 검색버튼을 클릭 시, 검색조건에 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>해당하는 관리자 정보에 대한 이력이 출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%; text-align: center'><IMG src="${rootPath}/resources/image/help/master/PIC5F22.png" alt="그림입니다." width="499" height="51" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:104;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>관리자ID</SPAN></P>
	</TD>
	<TD valign="middle" style='width:391;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>등록되어 있는 관리자ID로 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:104;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>관리자명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:391;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>등록되어 있는 관리자명으로 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:104;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>소속</SPAN></P>
	</TD>
	<TD valign="middle" style='width:391;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>등록되어 있는 소속으로 검색 </SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>12.5.2. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>관리자관리 상세</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:2%;line-height:180%'>관리자관리 화면에서 각각의 관리자명을 클릭하게 되면 클릭한 관리자에</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'> 대한</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 세부정보가 나타나게 됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC5FA0.gif" alt="묶음 개체입니다." width="499" height="177" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>아래 그림에서 보면 관리자관리 페이지에서 클릭한 로그에 대한 세부 정보를 볼 수 있으며</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>관리자ID, 관리자명, 소속명, 권한명, 연락처, E-MAIL, 관리자설명, 등록일시, 수정일시와 같은 내용 등을 볼 수 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%; text-align: center'><IMG src="${rootPath}/resources/image/help/master/PIC5FC0.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c001f.bmp
원본 그림의 크기: 가로 1920pixel, 세로 694pixel" width="499" height="181" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle6 STYLE='line-height:180%;'>12.5.2.1. <SPAN STYLE='font-family:"나눔명조,한컴돋움";font-weight:"bold"'>관리자상세 내용 수정</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>관리자상세 화면에서 해당 관리자 정보에 대한 내용을 수정할 때 수정 버튼을</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>누르면 수정됩니다.</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'> 이때, 수정할 수 있는 내용으로는 관리자명, 소속명,</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 권한명, 연락처, E-MAIL, 관리자설명이 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC600F.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c0020.bmp
원본 그림의 크기: 가로 1920pixel, 세로 694pixel" width="499" height="181" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle6 STYLE='line-height:180%;'>12.5.2.2. <SPAN STYLE='font-family:"나눔명조,한컴돋움";font-weight:"bold"'>관리자상세 내용 삭제</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-9%;line-height:180%'>관리자상세 화면에서 해당 관리자 정보에 대한 내용을 삭제할 때 삭제 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-6%;line-height:180%'>버튼을</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'> 누르면 관리자 정보가 삭제되는 것을 확인할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%; text-align: center'><IMG src="${rootPath}/resources/image/help/master/PIC605E.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c0021.bmp
원본 그림의 크기: 가로 1920pixel, 세로 694pixel" width="499" height="181" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"나눔명조,한컴돋움";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"나눔명조,한컴돋움";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"나눔명조,한컴돋움";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"나눔명조,한컴돋움";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"나눔명조,한컴돋움";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"나눔명조,한컴돋움";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"나눔명조,한컴돋움";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle6 STYLE='line-height:180%;'>12.5.2.3. <SPAN STYLE='font-family:"나눔명조,한컴돋움";font-weight:"bold"'>관리자 비밀번호 변경</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>관리자 비밀번호 변경은 우측상단 관리자 이름칸을 누르면 비밀번호 변경창이 나타나며 현재 비밀번호를 입력 후 새 비밀번호를 입력하면 변경할 수 있습니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%; text-align: center;'><SPAN STYLE='font-family:"나눔명조,한컴돋움";font-weight:"bold"'><IMG src="${rootPath}/resources/image/help/master/PIC60BD.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c0027.bmp
원본 그림의 크기: 가로 1903pixel, 세로 710pixel" width="499" height="186" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-family:"나눔명조,한컴돋움";font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%; text-align: center;'><SPAN STYLE='font-family:"나눔명조,한컴돋움";font-weight:"bold"'><IMG src="${rootPath}/resources/image/help/master/PIC6189.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c0026.bmp
원본 그림의 크기: 가로 1903pixel, 세로 680pixel" width="499" height="178" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>12.5.3. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>관리자관리 신규 등록</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-9%;line-height:180%'>관리자관리 화면에서 관리자를 등록하여 PSM Management Tool 관리에</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-9%;line-height:180%'>필요한</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-7%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-12%;line-height:180%'>권한을 주고 싶을 때 신규 버튼을 클릭하면 관리자상세 등록 화면이</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-8%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-11%;line-height:180%'>나오게 됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-11%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-31.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-11%;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC6226.gif" alt="묶음 개체입니다." width="499" height="168" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-31.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'><SPAN style='HWP-TAB:1;'>&nbsp;</SPAN></SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-7%;line-height:180%'>관리자상세 등록 화면에서 관리자ID, 관리자명, </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>소속명, 권한명, 연락처, E-MAIL, 관리자설명을 입력하여 관리자를 추가 할</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'> 수 있습니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>최초 비밀번호는 관리자 아이디와 동일하며, 비밀번호 변경 후 이용 가능합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-4%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%; text-align: center'><IMG src="${rootPath}/resources/image/help/master/PIC6246.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c0029.bmp
원본 그림의 크기: 가로 1921pixel, 세로 790pixel" width="499" height="205" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><SPAN style='HWP-TAB:1;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</SPAN></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'><SPAN style='HWP-TAB:1;'>&nbsp;</SPAN></SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'>&nbsp;관리자 신규 등록 후 최초 로그인이 다음과 같은 안내 창이 나타나며, 최초 비밀번호 (관리자 아이디와 동일)를 입력 후 새 비밀번호를 변경해야 이용 가능합니다.</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'> </SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC62A5.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c0028.bmp
원본 그림의 크기: 가로 1920pixel, 세로 676pixel" width="499" height="176" vspace="0" hspace="0" border="0"></SPAN></P>


<P CLASS=HStyle5 STYLE='line-height:180%;'>12.5.4. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>엑셀다운로드</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'><SPAN style='HWP-TAB:1;'>&nbsp;</SPAN></SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>관리자관리 화면에서 엑셀다운로드 버튼을 클릭하게 되면 관리자관리</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'> 화면에서</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'> 출력한 로그들을 EXCEL로 다운받을 수 있는 기능입니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%; text-align: center'><IMG src="${rootPath}/resources/image/help/master/PIC6323.gif" alt="묶음 개체입니다." width="499" height="171" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:40.9pt;text-indent:-11.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-13%;line-height:180%'>.xls 형태로 저장되며 “관리자ID”, “관리자명”, &quot;소속”, &quot;E-MAIL”, &quot;권한명“</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'>의 정보를 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%; text-align: center'><IMG src="${rootPath}/resources/image/help/master/PIC6334.png" alt="그림입니다.
원본 그림의 이름: CLP0000047c0040.bmp
원본 그림의 크기: 가로 1053pixel, 세로 153pixel" width="499" height="72" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>12.6. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>감사이력</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>환경설정의 감사이력 페이지는 관리자가 UBI SAFER-PSM에 접근하여 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>어떠한 행위를 하였는지 확인할 수 있는 메뉴입니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%; text-align: center'><IMG src="${rootPath}/resources/image/help/master/PIC6373.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c002b.bmp
원본 그림의 크기: 가로 1920pixel, 세로 794pixel" width="499" height="207" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>12.6.1. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>감사이력 조건검색</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:4%;line-height:180%'>&nbsp;감사이력 화면에서의 검색 조건은 일시, 관리자ID, 관리자명이 있으며,</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>해당 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>조건을 선택 및 입력 후 검색버튼 클릭 시 검색조건에</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:1%;line-height:180%'> 해당하는 감사이력이 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>출력됩니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC63A3.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c002c.bmp
원본 그림의 크기: 가로 1805pixel, 세로 224pixel" width="499" height="62" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:40%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:40%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:104;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>기간</SPAN></P>
	</TD>
	<TD valign="middle" style='width:391;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>해당하는 기간동안의 감사이력 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:104;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>관리자ID</SPAN></P>
	</TD>
	<TD valign="middle" style='width:391;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>관리자ID로 감사이력 검색</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#cedeef"  style='width:104;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:160%'>관리자명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:391;height:30;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:160%'>관리자명으로 감사이력 검색</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>12.6.2. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>엑셀다운로드</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>감사이력 화면에서 엑셀다운로드 버튼을 클릭하게 되면 감사이력에 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>나타난 내용에 대해 EXCEL로 다운받을 수 있는 기능입니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-1.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-31.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC6431.gif" alt="묶음 개체입니다." width="499" height="200" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:31.0pt;text-indent:-31.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-4%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>&nbsp;.xls 형태로 저장되며 “관리자ID”, “관리자명”, “IP”, “일시”, “사건유형 및 로그메세지”, “분류”의 정보를 보여줍니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC6441.png" alt="그림입니다.
원본 그림의 이름: CLP00001ed00020.bmp
원본 그림의 크기: 가로 1471pixel, 세로 302pixel" width="499" height="103" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>12.7. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>시스템관리</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:8.9pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:8.9pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'>시스템관리 화면에서 각각의 개인정보기록을 검출할 시스템별 설정에 관한 정보</SPAN><SPAN STYLE='font-size:8.9pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 확인 및 수정이 가능합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:8.9pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:8.9pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC6471.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c002d.bmp
원본 그림의 크기: 가로 1920pixel, 세로 408pixel" width="499" height="106" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:0.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;세부 항목 내역은 다음과 같습니다. </SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:5.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;① 시스템명을 입력합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:5.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;② 정렬 순서를 설정합니다.(상단으로부터 정렬)</SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:5.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;③ 로그 유형을 설정합니다.(default: BIZ_LOG)</SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:5.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;④ </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-9%;line-height:180%'>URL주소 및 하위 디렉토리를 설정합니다. (http:// [해당IP] : 포트번호/)</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>12.8. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>접속허용 IP</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:8.9pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:8.9pt;font-family:"나눔명조,한컴돋움";letter-spacing:-4%;line-height:180%'>접속허용 IP 화면에서는 관리자 페이지로 접속하는 IP주소를 제한할 수 있습니다.</SPAN><SPAN STYLE='font-size:8.9pt;font-family:"나눔명조,한컴돋움";line-height:180%'> (※ 해당 기능이 활성화되면, 허용 IP를 제외한 모든 주소가 접근이 불가능하게 되오니 주의 바랍니다.)</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:8.9pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC64A1.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c002e.bmp
원본 그림의 크기: 가로 1921pixel, 세로 322pixel" width="499" height="84" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-align:center;text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>12.8.1. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>접속허용IP 생성</SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IP대역관리 화면에서 “신규”버튼을 클릭합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%; text-align: center'><IMG src="${rootPath}/resources/image/help/master/PIC64E1.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c002f.bmp
원본 그림의 크기: 가로 1921pixel, 세로 322pixel" width="499" height="84" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>&nbsp;</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;접속을 허용할 IP를 입력하고 “사용”을 선택한 후 “등록” 버튼을 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>클릭함으로</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> 허용된 IP만 접속에 가능하게 설정합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC653F.png" alt="그림입니다.
원본 그림의 이름: CLP000006a0003e.bmp
원본 그림의 크기: 가로 1919pixel, 세로 436pixel" width="499" height="113" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>허용된 IP이외의 주소에서 접근하면 아래와 같은 접근거부 페이지가 나옵니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%; text-align: center'><IMG src="${rootPath}/resources/image/help/master/PIC657F.png" alt="그림입니다.
원본 그림의 이름: CLP000008d4002f.bmp
원본 그림의 크기: 가로 1116pixel, 세로 215pixel" width="499" height="98" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle5 STYLE='line-height:180%;'>12.8.2. <SPAN STYLE='font-size:11.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>접속허용IP 삭제 및 비활성화</SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'>&nbsp;접속이 허용된 IP로 접속하여 해당 IP를 클릭합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:10.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC65AF.png" alt="그림입니다.
원본 그림의 이름: CLP000006a0003f.bmp
원본 그림의 크기: 가로 1919pixel, 세로 442pixel" width="499" height="115" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-13%;line-height:180%'>해당 IP 사용을 “미사용” 또는 삭제를 클릭하여 해당 기능을 비활성화합니다.</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:30.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-13%;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC65DF.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c0031.bmp
원본 그림의 크기: 가로 1920pixel, 세로 326pixel" width="499" height="85" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:10.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle4 STYLE='line-height:180%;'>12.9. <SPAN STYLE='font-size:12.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>고객관리</SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-1%;line-height:180%'>&nbsp;고객관리에서는 기관/기업의 로고이미지를 UBI SAFER-PSM 로그인 </SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-2%;line-height:180%'>페이지에 등록할 수 있는 기능을 제공합니다.</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:180%'> </SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-20.0pt;line-height:180%; text-align: center;text-align: center;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'><IMG src="${rootPath}/resources/image/help/master/PIC662E.png" alt="그림입니다.
원본 그림의 이름: CLP0000443c0032.bmp
원본 그림의 크기: 가로 1920pixel, 세로 574pixel" width="499" height="149" vspace="0" hspace="0" border="0"></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle3 STYLE='line-height:180%;'>13. <SPAN STYLE='font-size:14.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:180%'>기술지원 문의</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:10.0pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-5%;line-height:180%'>&nbsp;온라인상이나 설명서로도 문제를 해결할 수 없을 시에는 (주)이지서티의 아래의 연락처로</SPAN><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";letter-spacing:-3%;line-height:180%'> 연락하시기 바랍니다.</SPAN></P>
<P CLASS=HStyle0 STYLE='text-align:left;line-height:130%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:130%'><BR></SPAN></P>
		
<DIV STYLE=''>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
	<TR>
		<TD valign="middle" style='width:236;height:99;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='margin-left:20.0pt;text-align:left;text-indent:-5.9pt;line-height:130%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";font-weight:"bold";line-height:130%'>㈜ 이지서티 고객만족센터</SPAN></P>
		<P CLASS=HStyle0 STYLE='margin-left:20.0pt;text-align:left;text-indent:-5.9pt;line-height:130%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:130%'>Tel: +82-70-7784-8233</SPAN></P>
		<P CLASS=HStyle0 STYLE='margin-left:20.0pt;text-align:left;text-indent:-5.9pt;line-height:130%;'>● <SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:130%'>E-mail: </SPAN><A HREF="mailto:easycerti@easycerti.com"><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";text-decoration:"none";color:#000000;line-height:130%'>support@easycerti.com</SPAN></A></P>
		</TD>
	</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-indent:-5.9pt;line-height:180%;'></P>

</DIV>

<P CLASS=HStyle2 STYLE='text-indent:-5.9pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-5.9pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-5.9pt;line-height:180%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='text-indent:-5.9pt;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; margin: 0 auto;'>
<TR>
	<TD valign="middle" style='width:503;height:63;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='line-height:130%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:130%'>&nbsp;</SPAN></P>
	<P CLASS=HStyle0 STYLE='line-height:130%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:130%'>본 설명서는 주식회사 이지서티가 고객 서비스를 제공하기 위해 제작한 것 입니다. 본 설명서 내용의 무단전재 및 재배포를 금하며 저작권의 소유는 주식회사 이지서티에 있습니다.</SPAN></P>
	<P CLASS=HStyle0 STYLE='line-height:130%;'><SPAN STYLE='font-size:9.0pt;font-family:"나눔명조,한컴돋움";line-height:130%'>&nbsp;</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle2 STYLE='text-indent:-5.9pt;line-height:180%;'></P>

</BODY>

</HTML>
