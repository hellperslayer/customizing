<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/dashboard/highcharts.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/exporting.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/globalize.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/knockout-3.0.0.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/dx.chartjs.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/zoomingData.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/dashboard/vivagraph.js" type="text/javascript" charset="UTF-8"></script>


<script src="${rootPath}/resources/js/psm/search/extrtCondbyInqList.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/search/extrtCondbyInqDeptDetailChart.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>


<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="${rootPath}/resources/css/css.css" rel="stylesheet" type="text/css">
<link href="${rootPath}/resources/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="${rootPath}/resources/css/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="${rootPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="${rootPath}/resources/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->

<link href="${rootPath}/resources/css/datatables.min.css" rel="stylesheet" type="text/css">
<link href="${rootPath}/resources/css/datatables.bootstrap.css" rel="stylesheet" type="text/css">
<link href="${rootPath}/resources/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css">

<!-- BEGIN THEME GLOBAL STYLES -->
<link href="${rootPath}/resources/css/plugins.min.css" rel="stylesheet" type="text/css">
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<%-- <link href="${rootPath}/resources/css/layout.min.css" rel="stylesheet" type="text/css"> --%>
<link href="${rootPath}/resources/css/custom.min.css" rel="stylesheet" type="text/css">
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="http://localhost:8080/PSM-3.0.2/abnormal/favicon.ico"> 

<script src="${rootPath}/resources/js/jquery.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/bootstrap.min.js" type="text/javascript"></script>

<!-- END CORE PLUGINS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="${rootPath}/resources/js/layout.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/demo.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/quick-sidebar.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/quick-nav.min.js" type="text/javascript"></script>
<link href="${rootPath}/resources/css/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color">

<script src="${rootPath}/resources/js/datatable.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/datatables.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/datatables.bootstrap.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/menu-util.js" type="text/javascript"></script>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
 

<script type="text/javascript" src="${rootPath}/resources/js/fabric.min.js" async=""></script>
<script type="text/javascript" src="${rootPath}/resources/js/FileSaver.min.js" async=""></script>

<link href="${rootPath}/resources/css/profile.min.css" rel="stylesheet" type="text/css">

<style>
.tabbable-custom>.nav-tabs>li.active{
border-top: 3px solid #578ebe;
}
</style>

<style>
#loading {
 width: 100%;   
 height: 100%;   
 top: 0px;
 left: 0px;
 position: fixed;   
 display: block;   
 opacity: 0.7;   
 background-color: #fff;   
 z-index: 99;   
 text-align: center; }  
  
#loading-image {   
 position: absolute;   
 top: 50%;   
 left: 50%;  
 z-index: 100; } 
</style>

<script type="text/javascript">
$(window).load(function() {    
	//$('#loading').show();
});
</script>

<div id="loading"><img id="loading-image" width="30px;" src="${rootPath}/resources/image/loading3.gif" alt="Loading..." /></div>

<script src="${rootPath}/resources/js/pie.js"></script>
<script type="text/javascript">
function onSubmenu(id){
	document.getElementById("submenu1").style.display="none";
	document.getElementById("submenu2").style.display="none";
	document.getElementById("submenu3").style.display="none";
	document.getElementById("submenu4").style.display="none";
	document.getElementById("submenu5").style.display="none";
	document.getElementById("submenu"+id).style.display="block";

	document.getElementById("sub1").style.fontWeight="normal";
	document.getElementById("sub2").style.fontWeight="normal";
	document.getElementById("sub3").style.fontWeight="normal";
	document.getElementById("sub4").style.fontWeight="normal";
	document.getElementById("sub5").style.fontWeight="normal";
	document.getElementById("sub"+id).style.fontWeight="bold";
	
	setTab(2, id);
}

function onGo(){
	var dept = document.getElementById("selDept");
	var deptId = dept.value;
	var deptName =dept.options[dept.selectedIndex].text;
	
	if(deptId == ""){
		alert("부서를 선택해 주세요");
	}

	fnExtrtDeptDetailInfo2(deptName, deptId);
}
</script>

<h1 class="page-title"> ${currentMenuName}
    <!--bold font-blue-hoki <small>basic bootstrap tables with various options and styles</small> -->
</h1>

<div class="contents left">
 		
<form id="listForm" method="POST">
<input type="hidden" id="use_detailchart" value="${use_detailchart }"/>
<div class="row" style="background:#eef1f5; padding-top: 20px; padding-bottom: 20px">
	<input type="hidden" id="dept_id" name="dept_id" value="${search.dept_id}">
	<input type="hidden" id="emp_user_id" name="emp_user_id" value="">
	<input type="hidden" id="userId" name="userId" value="">
	<input type="hidden" id="emp_user_name" name="emp_user_name" value="">
	<input type="hidden" id="search_from" name="search_from_chart" value="${search.search_from}">
	<input type="hidden" id="search_to" name="search_to_chart" value="${search.search_to}">
	<input type="hidden" id="detailOccrDt" name="detailOccrDt" value="">
	<div class="col-md-12">
		<!-- BEGIN PROFILE SIDEBAR -->
		<div class="profile-sidebar">
			<!-- SIDEBAR DEPARTMENT TITLE -->
			<div class="panel panel-info">
				<div class="panel-heading">
					<h4 class="profile-desc-title">부서명</h4>
				</div>
				<div class="profile-usertitle-name" style="margin: 20px 10px; ">${data1.dept_name}
					<c:forEach items="${data5}" var="dept" varStatus="status">
						<c:if test="${dept.dept_id == data1.dept_id }">
							<button type="button" title="전일 기준 순위" class="btn btn-circle blue" style="cursor:default;
							<c:if test="${status.index+1 < 6 }">
								 background-color: #e12330; border-color: #dc1e2b;
							</c:if>
							 " disabled>${status.index+1 }</button>
						</c:if>								
					</c:forEach>
				</div>
			</div>
			<!-- END SIDEBAR DEPARTMENT TITLE -->
			<!-- STAT -->
			<div class="note note-info">
				<h4 class="profile-desc-title">주요 개인정보 취급자 <font style="font-size:8pt">(기준: 전일)</font></h4>
				<div class="tab-context" style="margin-top:20px; ">
					<c:forEach items="${data3}" var="user" varStatus="status">
						<c:if test="${user.emp_user_name != null}">
						<c:if test="${status.index < 5}">
							<a class="btn btn-circle red-mint btn-outline sbold" style="cursor:default;" onclick="fnExtrtEmpDetailInfo2('${search.search_to}','${user.emp_user_id}','${data1.dept_id}')">
							${user.emp_user_name}
							</a>
						</c:if>								
						</c:if>								
					</c:forEach>
					<c:if test="${over > 0}">
					 외 ${over }명
					</c:if>
				</div>
			</div>
			<div class="note note-info">
				<h4 class="profile-desc-title">업무 시스템</h4>
				<div class="tab-context" style="margin-top:20px; ">
					<div class="clearfix">
						<c:forEach items="${data2}" var="system" varStatus="status">
							<c:if test="${system.system_name != null}">
								<a class="btn btn-xs blue" style="cursor:default; margin-top: 2px; margin-bottom: 2px;">
									${system.system_name}
									<i class="fa fa-laptop"></i>
		                       	 </a>
							</c:if>								
						</c:forEach>
                    </div>
				</div>
			</div>
			<div class="note note-info">					
				<h4 class="profile-desc-title">위험 부서 TOP 5 <font style="font-size:8pt">(기준: 전일)</font></h4>
				<div class="tab-context" style="margin-top:20px; ">
					<div class="clearfix">
						<c:forEach items="${data5}" var="dept" varStatus="status">
							<c:if test="${status.index+1 < 6 }">
								<h5><a onclick="fnExtrtDeptDetailInfo2('${dept.dept_name}', '${dept.dept_id }')" style="text-decoration:none">
								<span class="badge badge-danger" style="background-color:red; ">${status.index+1 }</span> ${dept.dept_name}</a></h5>
							</c:if>								
						</c:forEach>
                    </div>
<%-- 					<div class="clearfix" style="margin-top:10px; ">
						<div class="input-group">
						<span class="input-group-btn btn-right">
						<select class="form-control input-small input-inline" id="selDept" name="selDept">
							<option value="">그 외 부서</option>
							<c:forEach items="${data4}" var="dept" varStatus="status">
								<c:if test="${status.index+1 > 5 }">
									<option value="${dept.dept_id }">${dept.dept_name}</option>
								</c:if>								
							</c:forEach>
						</select>
						<button class="btn blue-madison" type="button" onclick="onGo()">Go!</button>
						</span>
						</div>
                    </div> --%>
				</div>
			</div>
			<!-- END STAT -->
		</div>
		<!-- END BEGIN PROFILE SIDEBAR -->
		<!-- BEGIN PROFILE CONTENT -->
		<div class="profile-content">
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light ">
						<div class="portlet-title tabbable-line">
							<div class="caption caption-md">
								<i class="icon-globe theme-font hide"></i> <span class="caption-subject font-blue-madison bold uppercase">업무패턴 정보</span>
							</div>
						</div>
						<div class="portlet-body">
							<div class="tabbable tabbable-custom">
							    <ul class="nav nav-tabs">
							    	<li <c:if test="${tabFlag == 'tab1' }">class="active"</c:if>>
							            <a href="#tab1" data-toggle="tab" onclick="setTab(1)"> 위험현황 </a>
							        </li>
							        <li <c:if test="${tabFlag == 'tab2' }">class="active"</c:if>>
							            <a href="#tab2" data-toggle="tab" onclick="setTab(2,1)"> 개인정보 취급패턴 </a>
							        </li>   
<%-- 
							        <li <c:if test="${tabFlag == 'tab3' }">class="active"</c:if>>
							            <a href="#tab3" data-toggle="tab"> 시스템접근 타임라인 </a>
							        </li>   
							        <li <c:if test="${tabFlag == 'tab4' }">class="active"</c:if>>
							            <a href="#tab4" data-toggle="tab"> 소명 처리 현황 </a>
							        </li>   
 --%>
							    </ul>
								<input type="hidden" id="tabId" value="${tabFlag }" />
							   
								<!-- END HEADER MENU -->		
								<!-- BEGIN PAGE BASE CONTENT -->
								<div class="tab-content">
											
									<!-- <form id="dashForm" method="POST"> -->
									<!-- 위험현황 항목 영역 -->
									<%@include file="/WEB-INF/views/psm/search/extrtCondbyInqDeptDetailChart1_new.jsp"%>
								    
								    <!-- 개인정보 취급패턴 항목 영역 -->
								    <%@include file="/WEB-INF/views/psm/search/extrtCondbyInqDeptDetailChart2_new.jsp"%>
								    
<%-- 
								    <!-- 시스템접근 타임라인 항목 영역 -->
								    <%@include file="/WEB-INF/views/psm/search/extrtCondbyInqDetailChart3_new.jsp"%>
								    
								    <!-- 소명 처리 현황 항목 영역 -->
								    <%@include file="/WEB-INF/views/psm/search/extrtCondbyInqDetailChart4_new.jsp"%>
								    <!-- </form> -->
 --%>
								    
								</div>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END PROFILE CONTENT -->
	</div>
</div>
</form>
</div>
