<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/search/summonList.js"
	type="text/javascript" charset="UTF-8"></script>
<script
	src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js"
	type="text/javascript"></script>

<h1 class="page-title">${currentMenuName}</h1>
<div class="portlet light portlet-fit portlet-datatable bordered">

	<!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog" style="position: relative ;top: 30%; left: 0;">

			<!-- Modal content -->
			<div class="modal-content">
				<div class="modal-header" style="background-color: #32c5d2;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">
						<b style="font-size: 13pt;">엑셀 업로드</b>
					</h4>
				</div>
				<div class="modal-body" style="background-color: #F9FFFF;">
					<form action="upload.html" method="post" id="fileForm" name="fileForm" enctype="multipart/form-data">
						<div>
							<table width="100%">
								<tr style="height: 40px">
									<td width="70%" style="vertical-align: middle;" align="center">
										<!--다중파일 <input multiple="multiple" type="file" name="file">	 -->
										<input class="fileUpLoad" type="file" id="file" name="file">
									</td>
									<td>
										<img style="cursor: pointer;" name="submup" onclick="excelSummonUpLoad()" 
										src="${rootPath}/resources/image/common/btn_exupload.gif" alt="엑셀업로드" title="엑셀업로드" />
									</td>
								</tr>
								<tr style="height: 40px">
									<td width="70%" style="vertical-align: middle;">
										<span class="downexam">&nbsp;&nbsp;&nbsp;※다운받은 양식으로 업로드하셔야 정보가 등록됩니다.</span>
									</td>
									<td>
										<img class="btn_excel exceldown" onclick="exDown()" style="cursor: pointer;"
										src="${rootPath}/resources/image/common/formUp.png" alt="양식다운로드" title="양식다운로드" />
									</td>
								</tr>
							</table>
						</div>
						<input type="hidden" id="result" name="result" value="false" />
					</form>
				</div>
				<div class="modal-footer">
					<a class="btn btn-sm red" href="#" data-dismiss="modal"> <i
						class="fa fa-remove"></i> 닫기
					</a>
				</div>
			</div>
		</div>
	</div>


	<div class="portlet-body">
		<div class="table-container">
			<div id="datatable_ajax_2_wrapper" class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
				<div class="dataTables_scroll" style="position: relative;">
					
					<!-- 검색  옵션-->
					<div class="row">
						<div class="col-md-6" style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
							<div class="portlet box grey-salt  ">
								<div class="portlet-title" style="background-color: #2B3643;">
									<div class="caption">
										<i class="fa fa-search"></i>검색 & 엑셀
									</div>
								</div>
								<div class="portlet-body form">
									<div class="form-body" style="padding-left: 10px; padding-right: 10px;">
										<div class="form-group">
											<form id="listForm" method="POST" class="mt-repeater form-horizontal">
												<div data-repeater-list="group-a">
													<div data-repeater-item class="row">
														<!-- jQuery Repeater Container -->
														<%-- <div class="col-md-2">
															<label class="control-label">소명코드</label>
															<input type="text" class="form-control" name="summon_code" value="${search.summon_code }" />
														</div> --%>
														<%-- <div class="col-md-2">
															<label class="control-label">기간별</label>
															<select	name="dateType" class="form-control">
																 <option value="1"
																	<c:if test="${search.dateType eq '1' }">selected="selected"</c:if>>수집일</option>
																<option value="2"
																	<c:if test="${search.dateType eq '2' }">selected="selected"</c:if>>요청일</option>
																<option value="3"
																	<c:if test="${search.dateType eq '3' }">selected="selected"</c:if>>답변일</option> 
																
															</select>
														</div> --%>
														<div class="col-md-2">
															<label class="control-label">기간</label>
															<div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
																<input type="text" class="form-control" id="search_fr" name="search_from" value="${search.search_fromWithHyphen}"> 
																<span class="input-group-addon"> &sim; </span>
																<input type="text" class="form-control" id="search_to" name="search_to" value="${search.search_toWithHyphen}">
															</div>
														</div> 
														<div class="col-md-2">
															<label class="control-label">진행상태</label>
															<select name="summon_status" class="form-control">
																<option value="">----- 전 체 -----</option>
																<option value="10" <c:if test="${search.summon_status == '10'}">selected="selected"</c:if>>소명요청</option>
																<option value="20" <c:if test="${search.summon_status == '20'}">selected="selected"</c:if>>재소명</option>
																<option value="30" <c:if test="${search.summon_status == '30'}">selected="selected"</c:if>>소명응답</option>
																<option value="40" <c:if test="${search.summon_status == '40'}">selected="selected"</c:if>>${summon_cfm_yn eq 'Y' ? '부서승인완료' : '판정완료'}</option>
																<!--  최종판정자 사용 여부 (DGB전용  -->
																<c:if test="${summon_cfm_yn eq 'Y'}">
																<option value="60" <c:if test="${search.summon_status == '60'}">selected="selected"</c:if>>최종승인완료</option>
																</c:if>
															</select>
														</div>
														<div class="col-md-2">
															<label class="control-label">${summon_cfm_yn eq 'Y' ? '승인구분':'판정구분'}</label>

															<select	name="decision_status" class="form-control">
																<option value="">-----전 체 -----</option>
																<option value="10"<c:if test="${search.decision_status eq '10' }">selected="selected"</c:if>>미판정</option>
																<option value="20"<c:if test="${search.decision_status eq '20' }">selected="selected"</c:if>>재소명</option>
																<option value="30"<c:if test="${search.decision_status eq '30' }">selected="selected"</c:if>>적정</option>
																<option value="40"<c:if test="${search.decision_status eq '40' }">selected="selected"</c:if>>부적정</option>
															</select>
														</div>
														<div class="col-md-2">
	                                                        <label class="control-label">시스템명</label>
	                                                        <select name="system_seq" class="form-control">
																<option value="" ${search.system_seq == '' ? 'selected="selected"' : ''}>----- 전 체 -----</option>
																<c:if test="${empty systemMasterList}">
																	<option>시스템 없음</option>
																</c:if>
																<c:if test="${!empty systemMasterList}">
																	<c:forEach items="${systemMasterList}" var="i" varStatus="z">
																		<option value="${i.system_seq}"	${i.system_seq==search.system_seq ? "selected=selected" : "" }>${ i.system_name}</option>
																	</c:forEach>
																</c:if>
															</select>
														</div>
														<!-- 
														<div class="col-md-2">
															<label class="control-label">재소명요청내역</label>
															<select name="resummon_yn" class="form-control">
																<option value="">-----전 체 -----</option>
																<option value="Y"
																	<c:if test="${search.resummon_yn eq 'Y' }">selected="selected"</c:if>>있음</option>
																<option value="N"
																	<c:if test="${search.resummon_yn eq 'N' }">selected="selected"</c:if>>없음</option>
															</select>
														</div>
														 -->
													</div>
													<div class="row">
														<%-- 추출조건 주석 처리
														<div class = "col-md-3">
															<div class="col-md-4" style="padding: 0px;"> 추출조건:</div>
															<div class="col-md-8" align="left" style="padding: 0px;">
															<select name="rule_cd" class="ticket-assign form-control input-small">
																<option value=""> ----- 선 택 ----- </option>
																<c:forEach	items="${ruleTblList}" var="ruleTblList">
																	<option value="${ruleTblList.rule_seq}" <c:if test="${ruleTblList.rule_seq == search.rule_cd}">selected="selected"</c:if>>${ruleTblList.rule_nm}</option>
																</c:forEach>
															</select>
															</div>
														</div> --%>
														<%-- <div class="mt-repeater-input">
                                                        <label class="control-label">키워드</label>
                                                        <br/>
                                                        <input class="form-control" name="keyword" value="" />
                                                    </div> --%>
                                                    
														<%-- 	소명 요청자 주석처리													
														<div class="mt-repeater-input">
															<label class="control-label">소명요청자</label> <br /> <input
																type="text" class="form-control" name="summon_req_emp"
																value="${search.summon_req_emp}" />
														</div> --%>
<%-- 														<div class="mt-repeater-input">
															<label class="control-label">판정자</label> <br /> <input
																type="text" class="form-control" name="deci_nm"
																value="${search.deci_nm}" />
														</div> --%>
														<div class="col-md-2">
															<label class="control-label">소속</label>
															<input type="text" class="form-control" name="dept_name" value="${search.dept_name}" />
														</div>
														<div class="col-md-2">
															<label class="control-label">취급자ID</label>
															<input type="text" class="form-control" name="emp_user_id" value="${search.emp_user_id}" />
														</div>
														<div class="col-md-2">
															<label class="control-label">취급자명</label>
															<input type="text" class="form-control" name="emp_user_name" value="${search.emp_user_name}" />
														</div>
														
													</div>
												</div>
												<hr/>
												<div align="right">
													<button type="reset"
														class="btn btn-sm red-mint btn-outline sbold uppercase"
														onclick="resetOptions(summonManageConfig['listUrl'])">
														<i class="fa fa-remove"></i> <font>초기화 
													</button>
													<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
														onclick="moveSummonManageList()">
														<i class="fa fa-search"></i> 검색
													</button>
													<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
														onclick="findReportSummon()">
														<i class="fa fa-check"></i> 보고서생성
													</button>
													&nbsp;&nbsp;
													<%-- <a class="btn red btn-outline btn-circle"
													onclick="excelAllLogInqList('${search.total_count}')"> 
													<i class="fa fa-share"></i> <span class="hidden-xs"> 엑셀 </span>
												</a> --%>
													<div class="btn-group">
														<a href="javascript:;" data-toggle="dropdown"><img src="${rootPath}/resources/image/icon/XLS_3.png"></a>
														<ul class="dropdown-menu pull-right">
															<!-- <li><a data-toggle="modal" data-target="#myModal">업로드</a></li> -->
															<li><a onclick="excelSummonManageList()"> 다운로드 </a></li>
														</ul>
													</div>
												</div>

												<input type="hidden" name="detailLogSeq" value="" />
												<input type="hidden" name="detailProcDate" value="" />
												<input type="hidden" name="main_menu_id" value="${search.main_menu_id }" />
												<input type="hidden" name="sub_menu_id" value="${search.sub_menu_id }" />
												<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
												<%-- <input type="hidden" name="system_seq" value="${search.system_seq }" /> --%>
												<input type="hidden" name="page_num" value="${search.page_num}" />
												<input type="hidden" name="cur_num" value="${search.page_num}"/>
												<input type="hidden" name="menuNum" id="menuNum" value="${menuNum}" />
												<input type="hidden" name="menuCh" id="menuCh" value="${menuCh}" />
												<input type="hidden" name="menuId" id="menuId" value="${search.sub_menu_id}" />
												<input type="hidden" name="menutitle" id="menutitle" value="${menuCh}${menuNum}" />
												<input type="hidden" name="dept_id" id="dept_id" value="${search.dept_id}" />
												<input type="hidden" name="user_id" value="" />
												<input type="hidden" name="detailProcTime" id="detailProcTime" value="" />
												<input type="hidden" name="result_type" id="result_type" value="" />
												<input type="hidden" name="detailEmpCd" value="" />
												<input type="hidden" name="detailOccrDt" value="" />
												<input type="hidden" name="isSearch" value="${search.isSearch }" />
												<input type="hidden" name="user_name" value=""/>
	 											<input type="hidden" name="detect_div" value=""/>
												<input type="hidden" name="occr_dt" value="" />
												<!-- <input type="hidden" name="resummon_yn" value=""/>
												<input type="hidden" name="system_seq" value=""/>
												<input type="hidden" name="dateType" value=""/> -->
												<!--  -->
												<input type="hidden" name="detail_summon_status" value=""/>
												<input type="hidden" name="detail_decision_status" value=""/>
											 	<input type="hidden" name="detail_decision_status_second" value=""/> 
												<input type="hidden" name="emp_detail_seq" value=""/>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<!-- 리스트 -->
					<div class="row">
						<table class="table table-striped table-bordered table-hover order-column">
							<thead>
								<tr>
									<!-- <th width="5%" style="text-align: center;">소명코드</th> -->
									<!-- <th width="5%" style="text-align: center;">수집일</th>
									<th width="5%"  style="text-align: center;">요청일</th>
									<th width="5%"  style="text-align: center;">답변일</th> -->
									<th width="5%"  style="text-align: center;">일시</th>
									<th width="5%"  style="text-align: center;">소속</th>
									<th width="10%"  style="text-align: center;">취급자ID</th>
									<th width="10%"  style="text-align: center;">취급자명</th>
									<th width="5%" style="text-align:center;">시스템명</th>
									<th width="35%"  style="text-align: center;">응답내용</th>
									<th width="5%"  style="text-align: center;">승인내용</th>	
									<th width="10%"  style="text-align: center;">${summon_cfm_yn eq 'Y' ? '승인구분':'판정구분'}</th>
									<th width="5%"  style="text-align: center;">진행상태</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${empty summonList.extrtCondbyInq}">
										<tr>
											<td colspan="11" align="center">데이터가 없습니다.</td>
										</tr>
									</c:when>
									<c:otherwise>
										<c:set value="${summonList.page_total_count}" var="count" />
										<c:forEach items="${summonList.extrtCondbyInq}" var="summon" varStatus="status">
											<tr	style="cursor: pointer;" onclick="fnSummonManageDetail('${summon.occr_dt}','${summon.emp_user_id}','${summon.emp_detail_seq}','${summon.emp_user_name}','${summon.summon_status}','${summon.decision_status}','${summon.decision_status_second}')">
												<%-- <td style="text-align: center;"><ctl:nullCv	nullCheck="${summon.emp_detail_seq }" /></td> --%>
												<!-- 수집일 -->
												<c:if test="${summon.occr_dt != null}">
													<c:set var="occr_dt" value="${summon.occr_dt}" />
													<td style="text-align: center;">${fn:substring(occr_dt, 0, 4)}-${fn:substring(occr_dt, 4, 6)}-${fn:substring(occr_dt, 6, fn:length(occr_dt))}&nbsp;
													</td>
												</c:if>
												<c:if test="${summon.occr_dt == null}">
													<td style="text-align: center;">-</td>
												</c:if>
												<%-- <!-- 요청일 -->
												<c:if test="${summon.req_dt != null}">
													<c:set var="req_dt" value="${summon.req_dt}" />
													<td style="text-align: center;">${fn:substring(req_dt, 0, 4)}-${fn:substring(req_dt, 4, 6)}-${fn:substring(req_dt, 6, fn:length(req_dt))}&nbsp;
													</td>
												</c:if>
												<c:if test="${summon.req_dt == null}">
													<td style="text-align: center;">-</td>
												</c:if>
												<!-- 답변일 -->
												<c:if test="${summon.res_dt != null}">
													<c:set var="res_dt" value="${summon.res_dt}" />
													<td style="text-align: center;">${fn:substring(res_dt, 0, 4)}-${fn:substring(res_dt, 4, 6)}-${fn:substring(res_dt, 6, fn:length(res_dt))}&nbsp;
													</td>
												</c:if>
												<c:if test="${summon.res_dt == null}">
													<td style="text-align: center;">-</td>
												</c:if> --%>
												<td style="text-align: center;"><ctl:nullCv	nullCheck="${summon.dept_name }" /></td>
												<td style="text-align: center;"><ctl:nullCv	nullCheck="${summon.emp_user_id }" /></td>
												<td style="text-align: center;"><ctl:nullCv	nullCheck="${summon.emp_user_name }" /></td>
												<td style="text-align: center;"><ctl:nullCv	nullCheck="${summon.system_name}" /></td>
												<c:choose>
													<c:when test="${fn:length(summon.msg_reply) > 40}">
														<td title="${summon.msg_reply}" style="text-align: center;">
															<c:out value="${fn:substring(summon.msg_reply,0,39)}">
															</c:out>...
														</td>
													</c:when>
													<c:otherwise>
														<td style="text-align: center;">
															<ctl:nullCv nullCheck="${summon.msg_reply }" />
														</td>
													</c:otherwise>
												</c:choose>
													
												
												<td style="text-align: center;"><ctl:nullCv nullCheck="${summon.msg_decision}" /></td>
												<td style="text-align: center;">
													<c:choose>
														<c:when test="${summon.decision_status eq '10' }">미판정</c:when>
														<c:when test="${summon.decision_status eq '20' }">재소명</c:when>
														<c:when test="${summon.decision_status eq '30' and summon.decision_status_second == '10'}">적정</c:when>
														<c:when test="${summon.decision_status eq '40' and summon.decision_status_second == '10'}">부적정</c:when>
														<c:when test="${summon.decision_status_second == '30'}">적정(최종)</c:when>
														<c:when test="${summon.decision_status_second == '40'}">부적정(최종)</c:when>
													</c:choose>
												</td>
												<td style="text-align: center;">
													<c:choose>
														<c:when test="${summon.summon_status eq '10' }">승인요청</c:when>
														<c:when test="${summon.summon_status eq '20' }">재소명</c:when>
														<c:when test="${summon.summon_status eq '30' }">소명응답</c:when>
														<c:when test="${summon.summon_status eq '40' }">${summon_cfm_yn eq 'Y'? '부서승인완료' : '판정완료' }</c:when>
														<c:when test="${summon.summon_status eq '60' && summon_cfm_yn eq 'Y'}">최종승인완료</c:when>
														
													</c:choose>
												</td>
											</tr>
												<c:set var="count" value="${count - 1 }" />
										</c:forEach>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>
					</div>
					<div class="row" style="padding: 10px;">
		            <!-- 페이징 영역 -->
					<c:if test="${search.total_count > 0}">
						<div id="pagingframe" align="center">
							<p>
								<ctl:paginator currentPage="${search.page_num}"
									rowBlockCount="${search.size}"
									totalRowCount="${search.total_count}" />
							</p>
						</div>
					</c:if>
		        </div>


<!-- 엑셀양식 다운로드 -->
<form action="exdownload.html" method="post" id="exdown" name="exdown"
	enctype="multipart/form-data"></form>

<script type="text/javascript">
	var summonManageConfig = {
		"listUrl" : "${rootPath}/extrtCondbyInq/summonManageList.html",
		"detailUrl" : "${rootPath}/extrtCondbyInq/summonManageDetail.html",
		"downloadUrl" :"${rootPath}/extrtCondbyInq/summonManageListExcel.html",
		"menu_id" : "${currentMenuId}",
		"menu_name" : "${currentMenuName}",
		"loginPage" : "${rootPath}/loginView.html",
		"repot_summon" : "${rootPath}/report/report_summon.html"

	};

	var menu_id = summonManageConfig["menu_id"];
	var menu_name = summonManageConfig["menu_name"];
	var log_message = "SUCCESS";
	var log_action = "SELECT";
</script>