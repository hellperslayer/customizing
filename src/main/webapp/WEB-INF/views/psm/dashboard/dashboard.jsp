<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<c:set var="currentMenuId" value="MENU00030" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<link rel="shortcut icon" type="image/x-icon" href="${rootPath}/resources/image/common/favicon_H2.ico" />
<link rel="stylesheet" type="text/css" href="${rootPath}/resources/css/common/fontawesome/font-awesome.css" />
<meta http-equiv="X-UA-Compatible" content="IE=9" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" charset="UTF-8">
	rootPath = '${rootPath}';
	contextPath = '${pageContext.servletContext.contextPath}';
</script>
<link rel="stylesheet" href="${rootPath}/resources/css/common/jquery-ui.css" />
<!-- 시군구 : 0 , 아카데미 : 1 /> -->
<c:if test="${chng_layout eq 0}">
	<link rel="stylesheet" type="text/css" href="${rootPath}/resources/css/common/style_gun.css" />
</c:if>
<c:if test="${chng_layout eq 1}">
	<link rel="stylesheet" type="text/css" href="${rootPath}/resources/css/common/style.css" />
</c:if>

<link rel="stylesheet" type="text/css" href="${rootPath}/resources/css/common/reset.css" />
<link rel="stylesheet" type="text/css" href="${rootPath}/resources/dashboard/css/reset.css" />
<script src="${rootPath}/resources/js/common/jquery-1.8.2.min.js" type="text/javascript" charset="UTF-8"></script> 
<script src="${rootPath}/resources/js/common/jquery-ui-1.9.0.custom.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/jquery.ui.datepicker-ko.js" type="text/javascript" charset="UTF-8"></script>

<!-- 시군구 : 0 , 아카데미 : 1  -->
<%-- <c:if test="${chng_layout eq 0}"> --%>
<%-- 	<script src="${rootPath}/resources/js/common/menu-util_gun.js" type="text/javascript" charset="UTF-8"></script> --%>
<%-- </c:if> --%>
<%-- <c:if test="${chng_layout eq 1}"> --%>
<%-- 	<script src="${rootPath}/resources/js/common/menu-util.js" type="text/javascript" charset="UTF-8"></script> --%>
<%-- </c:if> --%>

<script src="${rootPath}/resources/js/layout/layout.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/PageNavigater.js" type="text/javascript" charset="UTF-8"></script>

<style>
	#graph { float: left; width: 100%; height: 700px; overflow: hidden; } 
</style>
<script src="${rootPath}/resources/js/psm/dashboard/highcharts.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/exporting.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/globalize.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/knockout-3.0.0.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/dx.chartjs.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/zoomingData.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/dashboard.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/dashboard/vivagraph.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/dashboard/topology.js" type="text/javascript" charset="UTF-8"></script>

<form id="dashboardForm" method="POST">
	<input type="hidden" name="main_menu_id" value="${mainMenuId }" /> <input
		type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
	<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
	<input type="hidden" name="page_num" />
</form>

<!-- <div class="tab_dash left"> -->
<!-- 	<span id="tab1" -->
<%-- 		class="tab <c:if test="${tabFlag == 'tab1' }">select</c:if>">대시보드</span> --%>
<!-- 	<span id="tab2" -->
<%-- 		class="tab <c:if test="${tabFlag == 'tab2' }">select</c:if>">토폴로지</span> --%>
<!-- 	<div class="dashlogo"> -->
<!-- 		<img -->
<%-- 			src="${pageContext.servletContext.contextPath}/resources/image/login/logo3.png" /> --%>
<!-- 	</div> -->
<!-- </div> -->


<input type="hidden" id="tabId" value="${tabFlag }" />

<%-- <h1 class="page-title">
	<c:if test="${tabFlag == 'tab1' }">
		대시보드 <small>Dashboard</small>
	</c:if>
	<c:if test="${tabFlag == 'tab2' }">
		토폴로지 <small>Topology</small>
	</c:if>
</h1> --%>

<!-- 대시보드 항목 영역 -->
<%-- <div class="row">
	<div class="col-md-12">
		<div class="dashboard" id="dashContent">
			<!-- 시군구 : 0 , 아카데미 : 1  -->
			<c:if test="${dashboard eq 0}">
				<%@include file="/WEB-INF/views/psm/dashboard/dashboard_sub_gun.jsp"%>
			</c:if>
			<c:if test="${dashboard eq 1}">
				<%@include file="/WEB-INF/views/psm/dashboard/dashboard_sub_gun.jsp"%>
			</c:if>
		</div>
	</div>
</div> --%>


<!-- 토폴로지 항목 영역 -->
<div class="row" style="background-color: #444">
	<div class="col-md-12">
		<div class="topology none" id="topoContent">
			<%@include file="/WEB-INF/views/psm/dashboard/topology_n.jsp"%>
		</div>
	</div>
</div>
<!-- 전체로그조회 이동 Form -->
<form id="allLogInqListForm" action="${rootPath }/allLogInq/list.html" method="POST">
	<input type="hidden" name="main_menu_id" value="MENU00040" /> 
	<input type="hidden" name="sub_menu_id" value="MENU00041" /> 
	<input type="hidden" name="search_to" /> 
	<input type="hidden" name="search_from" /> 
	<input type="hidden" name="starthm" /> 
	<input type="hidden" name="endhm" /> 
	<input type="hidden" name="system_seq" />
	<input type="hidden" name="sub_menu_id" /> 
	<input type="hidden" name="emp_user_id" />
	<input type="hidden" name="user_ip" />
	<input type="hidden" name="privacyType" />
</form>

<script type="text/javascript">
	var dashboardConfig = {
		"dashboardUrl" : "${rootPath}/dashboard/dashboard.html",
		"detailUrl" : "${rootPath}/dashboard/detail.html"
	};
	
</script>