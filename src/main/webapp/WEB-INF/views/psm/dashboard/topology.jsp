<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<style>
	#searchBar {
		background: none;
	}
</style>

<!-- 토폴로지 검색조건 -->
<div class="option_tp left">
	<form action="" id="topologyForm" method="post">
		<span id ="searchBar">
			<span class="option_list">
		  		<label class="no_img_label">
		  			<input type="checkbox" id="realtime" name="realtime" <c:if test="${paramBean.realtime == 'on'  }">checked="checked"</c:if> value="${paramBean.realtime }">
		  			<i>&nbsp;실시간 모니터링</i>
		    	</label>
	    	</span>
	    	
	    	<span id="searchDate">
		    	<span class="option_list margin_tp">
			    	<i>날짜 : </i>
			    	<input type="text" class="calender input_cal_tp" id="start_date" name="start_date" value="${paramBean.start_date}">
				</span>
				
				<span class="option_list margin_tp">
					<i>시간 : </i>
					<select id="starth" name="starth">
						<c:forEach var="item"  varStatus="i" begin="0" end="23" step="1">
							<c:if test="${paramBean.realtime == 'on'}">
								<c:if test="${paramBean.startm < 20}">
									<option value="${item}" <c:if test="${item == (paramBean.starth - 1) }">selected="selected"</c:if>   > ${item}</option>
								</c:if>
								<c:if test="${paramBean.startm >= 20}">
									<option value="${item}" <c:if test="${item == paramBean.starth }">selected="selected"</c:if>   > ${item}</option>
								</c:if>
							</c:if>
							<c:if test="${paramBean.realtime != 'on'}">
								<option value="${item}" <c:if test="${item == paramBean.starth }">selected="selected"</c:if>   > ${item}</option>
							</c:if>
						</c:forEach>
					</select>
					<select id="startm" name="startm">
						<c:forEach var="item"  varStatus="i" begin="0" end="59" step="1">
							<c:if test="${paramBean.realtime == 'on'}">
								<c:if test="${paramBean.startm < 20}"> 
									<option value="${item}" <c:if test="${item == (paramBean.startm + 40) }">selected="selected"</c:if> <c:if test="${item == (paramBean.startm+40) }">selected="selected"</c:if> >${item}</option>
								</c:if>
								<c:if test="${paramBean.startm >= 20}"> 
									<option value="${item}" <c:if test="${item == (paramBean.startm - 20) }">selected="selected"</c:if> <c:if test="${ item== (paramBean.startm-20) }">selected="selected"</c:if> >${item}</option>
								</c:if>
							</c:if>
							<c:if test="${paramBean.realtime != 'on' }">
								<option value="${item}" <c:if test="${item == paramBean.startm}">selected="selected"</c:if> <c:if test="${ item == paramBean.startm}">selected="selected"</c:if> >${item}</option>
							</c:if>
						</c:forEach>
					</select>
					<i class="sim">&sim;</i>
					<select id="endh" name="endh">
						<c:forEach var="item"  varStatus="i" begin="0" end="23" step="1">
							<option value="${item}" <c:if test="${item == paramBean.endh }">selected="selected"</c:if> >${item}</option>
						</c:forEach>
					</select>
					<select id="endm" name="endm">
						<c:forEach var="item"  varStatus="i" begin="0" end="59" step="1">
							<option value="${item}" <c:if test="${item == paramBean.endm }">selected="selected"</c:if>>${item}</option>
						</c:forEach>
					</select>
				</span>
			</span>
			
			<span class="option_list margin_tp">	
				<i>IP대역 : </i>
				<input type="text" id="search_ip" name="search_ip" style="width: 120px" value="${paramBean.search_ip == null? '' : paramBean.search_ip }">
			</span>	
	    	
			<span class="option_list margin_tp">
				<label class="no_img_label">
					<input type="checkbox" id="isTop_n" name="isTop_n" <c:if test="${paramBean.isTop_n == 'on'  }">checked="checked"</c:if> value="${paramBean.isTop_n }"> 
					<i>&nbsp;개인정보건수 상위 N명 :</i>
				</label>
				<select id="top_n" name="top_n">
					<option value="10" <c:if test="${paramBean.top_n==null || paramBean.top_n==10 }">selected="selected"</c:if>>10</option>
					<option value="20" <c:if test="${paramBean.top_n==20 }">selected="selected"</c:if>>20</option>
					<option value="30" <c:if test="${paramBean.top_n==30 }">selected="selected"</c:if>>30</option>
					<option value="40" <c:if test="${paramBean.top_n==40 }">selected="selected"</c:if>>40</option>
					<option value="50" <c:if test="${paramBean.top_n==50 }">selected="selected"</c:if>>50</option>
					<option value="60" <c:if test="${paramBean.top_n==60 }">selected="selected"</c:if>>60</option>
					<option value="70" <c:if test="${paramBean.top_n==70 }">selected="selected"</c:if>>80</option>
					<option value="80" <c:if test="${paramBean.top_n==80 }">selected="selected"</c:if>>80</option>
					<option value="90" <c:if test="${paramBean.top_n==90 }">selected="selected"</c:if>>90</option>
					<option value="100" <c:if test="${paramBean.top_n==100 }">selected="selected"</c:if>>100</option>
				</select>
			</span>
			
			<span class="option_list margin_tp">
		    	<i>Refresh : </i>
		    	<select id="refreshBox" onchange="refresh()" style="width: 60px">
		    		<option value="300000">선택</option>
		    		<option value="5000">5</option>
		    		<option value="10000">10</option>
		    		<option value="20000">20</option>
		    		<option value="30000">30</option>
		    		<option value="60000">60</option>
		    	</select>
		    	<i>(단위/초)</i>
			</span> 
					
			<input type="hidden" id="paramBean" name ="paramBean" value="${paramBean.realtime }">
			<input type="hidden" id="toggleCd" name ="toggleCd" value="${paramBean.toggleCd == null? '2' : paramBean.toggleCd }">
				
			<!-- '조회' 버튼 -->
			<a class="btn small blue right" id="topo_search">조회</a>
		
		</span> <!-- // searchBar End -->
	</form> <!-- // topologyForm End -->
</div>

<!-- 토폴로지 그려지는 부분 -->	
<div id="graph"></div>


