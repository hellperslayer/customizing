<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>
<c:set var="rootPath" value="${pageContext.servletContext.contextPath}" scope="application"/>
<link rel="stylesheet" type="text/css" href="${rootPath}/resources/css/common/eight_dash.css" />

<!-- <meta http-equiv="X-UA-Compatible" content="IE=8" /> -->
	  <meta charset="utf-8" />
	   <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #1 for dark mega menu option" name="description" />
        <meta content="" name="author" />
    
    <script type="text/javascript" charset="UTF-8">
  		rootPath = '${rootPath}';
  		contextPath = '${pageContext.servletContext.contextPath}';
 	</script>
    
    
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="${rootPath}/resources/css/font-awesome2.min.css" rel="stylesheet" type="text/css">
<link href="${rootPath}/resources/css/simple-line-icons2.min.css" rel="stylesheet" type="text/css">
<link href="${rootPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="${rootPath}/resources/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->

<link href="${rootPath}/resources/css/datatables.min.css" rel="stylesheet" type="text/css">
<link href="${rootPath}/resources/css/datatables.bootstrap.css" rel="stylesheet" type="text/css">
<link href="${rootPath}/resources/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css">

<!-- BEGIN THEME GLOBAL STYLES -->
<link href="${rootPath}/resources/css/components_new.min.css" rel="stylesheet" id="style_components" type="text/css">
<link href="${rootPath}/resources/css/plugins.min.css" rel="stylesheet" type="text/css">
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="${rootPath}/resources/css/layout.min.css" rel="stylesheet" type="text/css">
<link href="${rootPath}/resources/css/custom.min.css" rel="stylesheet" type="text/css">
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="http://localhost:8080/PSM-3.0.2/abnormal/favicon.ico"> 

<script src="${rootPath}/resources/js/jquery.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/js.cookie.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/jquery.blockui.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/bootstrap-switch.min.js" type="text/javascript"></script>

<!-- AMCHART SCRIPT START-->
<script src="${rootPath}/resources/js/amcharts.js"></script>
<script src="${rootPath}/resources/js/serial.js"></script>
<script src="${rootPath}/resources/js/export.min.js"></script>
<script src="${rootPath}/resources/js/amstock.js"></script>
<script src="${rootPath}/resources/js/xy.js"></script>
<script src="${rootPath}/resources/js/radar.js"></script>
<script src="${rootPath}/resources/js/light.js"></script>
<script src="${rootPath}/resources/js/pie.js"></script>
<script src="${rootPath}/resources/js/gantt.js"></script>
<script src="${rootPath}/resources/js/gauge.js"></script>

<link rel="stylesheet" href="${rootPath}/resources/css/export.css" type="text/css" media="all">

<!-- AMCHART SCRIPT END -->

<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="${rootPath}/resources/js/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="${rootPath}/resources/js/layout.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/demo.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/quick-sidebar.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/quick-nav.min.js" type="text/javascript"></script>

<script src="${rootPath}/resources/js/datatable.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/datatables.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/datatables.bootstrap.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/menu-util.js" type="text/javascript"></script>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<script type="text/javascript" src="${rootPath}/resources/js/fabric.min.js" async=""></script>
<script type="text/javascript" src="${rootPath}/resources/js/FileSaver.min.js" async=""></script>
<%-- <link href="${rootPath}/resources/css/profile.min.css" rel="stylesheet" type="text/css"> --%>


<!-- <link rel="stylesheet" href="/psm/resources/css/psm/dashboard/dashboardChart.css" type="text/css" media="all" /> -->
<script src="${rootPath}/resources/js/dashboard/charts.js?ver=2"></script>
	
<body>
		<div class="wrapper">
			
			 
			
				<div class="container_dash left">
					






<style>
	#graph { float: left; width: 100%; height: 700px; overflow: hidden; }
</style>

	
<!-- 대시보드 항목 영역 S -->
<div class="" id="dashContent">

<!-- 시군구 : 0 , 아카데미 : 1  -->
<link rel="stylesheet" type="text/css" href="${rootPath}/resources/css/common/eight_dash.css" />



               <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content" style="min-height: 818px;">
                        <!-- BEGIN PAGE HEADER-->
                       
                        <!-- BEGIN PAGE BAR -->
                       






<style>
.page-content{
/* background-image: url('${rootPath}/resources/image/layout/bg-dashboard.png'); */
background-color: #0f0f1e;
}
</style>


<!--
<style type="text/css">
    body,td,th {
	font-family: "굴림", Gulim, Arial, Helvetica, sans-serif;
}
body {
	background-image: url(image/layout/bg-dashboard.png);
}
</style>
-->


    <!--
    <style type="text/css">
    body,td,th {
	font-family: "굴림", Gulim, Arial, Helvetica, sans-serif;
}
body {
	background-image: url(image/layout/bg-dashboard.png);
}
    </style>
	-->


<%-- <script src="${rootPath}/resources/js/profile.min.js" type="text/javascript"></script> --%>





<!-- 대시보드 S-->
<div class="portlet light bordered form-fit" >
<!-- 대시보드 real S-->
<div class="portlet-body">
	<div class="tabbable tabbable-tabdrop">
    	<!-- BEGIN HEADER MENU 
	    <ul class="nav nav-tabs">
	    	<li class="active">
	            <a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#tab1" data-toggle="tab"> 접속기록 </a>
	        </li>
	        <li>
	            <a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#tab2" data-toggle="tab"> 위험분석 </a>
	        </li>
	        <li>
	            <a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#tab3" data-toggle="tab"> 토플로지 </a>
	        </li>     
	    </ul>
		<!-- END HEADER MENU -->		
		<!-- BEGIN PAGE BASE CONTENT -->
		<div class="tab-content">
					
			<div class="tab-pane active" id="tab1">	  
			<div class="row">
				<div class="col-xs-12 col-sm-12">
		            <div class="portlet light bordered" style="margin-top:-8px;">
		               
						<div class="portlet-title" style="height:30px; min-height:10px; margin-top:-10px;">
							<div class="caption ">
		                    	<i class="icon-bar-chart font-green-haze2" style="font-size:15px;"></i>
		                        <span class="caption-subject bold uppercase font-green-haze2" style="font-size:15px;">개인정보 처리시스템별 처리 현황</span>
		                    </div>
		                </div>
						
		                <div class="portlet-body">
		                    <div id="dashboard_amchart_1" class="chart" style="height:234px; border:0px solid #000;"></div>
		                </div>
		            </div>
		        </div>       
				<div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">
		            <div class="portlet light bordered">
		                <div class="portlet-title" style="height:30px; min-height:10px; margin-top:-10px;">
		                    <div class="caption">
		                    	<i class="icon-bar-chart font-green-haze2" style="font-size:15px"></i>
		                        <span class="caption-subject bold uppercase font-green-haze2" style="font-size:15px">실시간 개인정보 처리 현황</span>
		                        <!-- <span class="caption-helper font-dark">distance stats...</span> -->
		                    </div>
		                    
		                </div>
		                <div class="portlet-body" style="position:relative; top:-8px; left:0px;">
							<div id="dashboard_amchart_2" class="chart" style="overflow: visible; text-align: left;">
							<!--
							-->
							</div>
		                </div>
		            </div>
		        </div>
				<div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">
		            <div class="portlet light bordered">
		                <div class="portlet-title" style="height:30px; min-height:10px; margin-top:-10px;">
		                    <div class="caption ">
		                    	<i class="icon-bar-chart font-green-haze2" style="font-size:15px"></i>
		                        <span class="caption-subject bold uppercase font-green-haze2" style="font-size:15px">시간대별 개인정보 처리 로그 건수</span>
		                        <!-- <span class="caption-helper">distance stats...</span> -->
		                    </div>
		                    
		                </div>
		                <div class="portlet-body" style="position:relative; top:-8px; left:0px;">
		                    <div id="dashboard_amchart_17" class="chart"></div>
		                </div>
		            </div>
		        </div> 
				<div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">
		            <div class="portlet light bordered">
		                <div class="portlet-title" style="height:30px; min-height:10px; margin-top:-10px;">
		                    <div class="caption ">
		                    	<i class="icon-bar-chart font-green-haze2" style="font-size:15px"></i>
		                        <span class="caption-subject bold uppercase font-green-haze2" style="font-size:15px">최근로그 수집</span>
		                        <!-- <span class="caption-helper">distance stats...</span> -->
		                    </div>
		                    <!-- <div class="actions">
		                        <a href="#" class="btn btn-circle green btn-outline btn-sm">
		                            <i class="fa fa-pencil"></i> Export </a>
		                        <a href="#" class="btn btn-circle green btn-outline btn-sm">
		                            <i class="fa fa-print"></i> Print </a>
		                    </div> -->
		                </div>
		                <!--
						<div class="portlet-body" style="position:relative; top:-8px; left:0px;">
		                    <div id="dashboard_amchart_2" class="chart"></div>
		                </div>
						-->
						
<style type="text/css">
.tableClass {
	background: #0f0f1e;
	color:white;
}
table {
	 border-collapse: collapse;
}
table, th, tr, td {
	border: 1px solid #575761;
	height: 40px;
}
th {
	color:#C5583C;
}
tr:hover{background-color:#173137} 
</style>
						<!--portlet-body S-->
						<div class="portlet-body" style="position:relative; top:-10px; left:0px; height:269px; opacity:1">
							<div class="row">
							<div class="dataTables_scroll" style="width:100%; clear:both;">
								<div class="dataTables_scrollBody" style="overflow-y: auto; overflow-x:hidden; height:268px; width: 100%;">
									<!-- <table id="table_log1" style="border-top: 2px solid #459aef;" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer" role="grid"> -->
									<table id="table_log1" class="tableClass">
										<thead>
											<tr role="row">
												<th width="20%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; font-size:12px;">
													시스템</th>
												<th width="15%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; font-size:12px;">
													생성일시</th>
												<th width="15%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; font-size:12px;">
													부서</th>
												<th width="15%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; font-size:12px;">
													ID</th>
												<th width="15%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; font-size:12px;">
													사용자명</th>
												<th width="20%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; font-size:12px;">
													사용자IP</th>
											</tr>
										</thead>
										<tbody>
											<!-- <tr style="text-align: center;">
												<td width="20%" style="height:10px; font-size:12px">보조사업관리</td>
												<td width="15%" style="height:10px; font-size:12px">2017-02-15</td>
												<td width="10%" style="height:10px; font-size:12px">부속실</td>
												<td width="10%" style="height:10px; font-size:12px">ehchoi</td>
												<td width="15%" style="height:10px; font-size:12px">홍길동</td>
												<td width="30%" style="height:10px; font-size:12px">192.168.0.211</td>
											</tr>
											<tr style="text-align: center;">
												<td style="height:10px; font-size:12px">보조사업관리</td>
												<td style="height:10px; font-size:12px">2017-02-15</td>
												<td style="height:10px; font-size:12px">부속실</td>
												<td style="height:10px; font-size:12px">ehchoi</td>
												<td style="height:10px; font-size:12px">홍길동</td>
												<td style="height:10px; font-size:12px">192.168.0.211</td>
											</tr> -->
										</tbody>
									</table>
									<!-- <div style="position: relative; top: 0px; left: 0px; width: 1px; height: 8366px;"></div> -->
								</div>
								<div class="dataTables_processing DTS_Loading" style="display: none;">Please wait ...</div>
							</div>
						</div>			
		                </div>
						<!--portlet-body E-->
		            </div>
		        </div> 
				<div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">
		            <div class="portlet light bordered">
		                <div class="portlet-title" style="height:30px; min-height:10px; margin-top:-10px;">
		                    <div class="caption">
		                    	<i class="icon-bar-chart font-green-haze2" style="font-size:15px"></i>
		                        <span class="caption-subject bold uppercase font-green-haze2" style="font-size:15px">실시간 개인정보 취급자 현황</span>
		                        <!-- <span class="caption-helper font-dark">distance stats...</span> -->
		                    </div>
		                    
		                </div>
		                <div class="portlet-body" style="position:relative; top:-8px; left:0px;">
							<div id="dashboard_amchart_4" class="chart" style="overflow: visible; text-align: left;">
							<!--
							<div class="amcharts-main-div" style="position: relative; width: 100%; height: 100%;"><div class="amcharts-chart-div" style="overflow: hidden; position: relative; text-align: left; width: 851px; height: 252px; padding: 0px; cursor: default;"><svg version="1.1" style="position: absolute; width: 546px; height: 252px; top: 0px; left: 0px;"><desc>JavaScript chart by amCharts 3.20.20</desc><g><path cs="100,100" d="M0.5,0.5 L850.5,0.5 L850.5,251.5 L0.5,251.5 Z" fill="#FFFFFF" stroke="#000000" fill-opacity="0" stroke-width="1" stroke-opacity="0" class="amcharts-bg"></path><path cs="100,100" d="M0.5,0.5 L773.5,0.5 L773.5,199.5 L0.5,199.5 L0.5,0.5 Z" fill="#FFFFFF" stroke="#000000" fill-opacity="0" stroke-width="1" stroke-opacity="0" class="amcharts-plot-area" transform="translate(57,32)"></path></g><g><g class="amcharts-category-axis" transform="translate(57,32)"><g><rect x="0.5" y="0.5" width="70" height="199" rx="0" ry="0" stroke-width="0" fill="#000000" stroke="#000000" fill-opacity="0.05" stroke-opacity="0.05" transform="translate(0,0)" class="amcharts-axis-fill"></rect></g><g><rect x="0.5" y="0.5" width="70" height="199" rx="0" ry="0" stroke-width="0" fill="#000000" stroke="#000000" fill-opacity="0.05" stroke-opacity="0.05" transform="translate(141,0)" class="amcharts-axis-fill"></rect></g><g><rect x="0.5" y="0.5" width="70" height="199" rx="0" ry="0" stroke-width="0" fill="#000000" stroke="#000000" fill-opacity="0.05" stroke-opacity="0.05" transform="translate(281,0)" class="amcharts-axis-fill"></rect></g><g><rect x="0.5" y="0.5" width="70" height="199" rx="0" ry="0" stroke-width="0" fill="#000000" stroke="#000000" fill-opacity="0.05" stroke-opacity="0.05" transform="translate(422,0)" class="amcharts-axis-fill"></rect></g><g><rect x="0.5" y="0.5" width="71" height="199" rx="0" ry="0" stroke-width="0" fill="#000000" stroke="#000000" fill-opacity="0.05" stroke-opacity="0.05" transform="translate(562,0)" class="amcharts-axis-fill"></rect></g><g><rect x="0.5" y="0.5" width="70" height="199" rx="0" ry="0" stroke-width="0" fill="#000000" stroke="#000000" fill-opacity="0.05" stroke-opacity="0.05" transform="translate(703,0)" class="amcharts-axis-fill"></rect></g></g><g class="amcharts-value-axis value-axis-valueAxisAuto0_1488455767339" transform="translate(57,32)" visibility="visible"><g><path cs="100,100" d="M0.5,0.5 L0.5,0.5 L773.5,0.5" fill="none" stroke-width="1" stroke-dasharray="5" stroke-opacity="0.1" stroke="#000000" class="amcharts-axis-grid"></path></g><g><path cs="100,100" d="M0.5,40.5 L0.5,40.5 L773.5,40.5" fill="none" stroke-width="1" stroke-dasharray="5" stroke-opacity="0.1" stroke="#000000" class="amcharts-axis-grid"></path></g><g><path cs="100,100" d="M0.5,80.5 L0.5,80.5 L773.5,80.5" fill="none" stroke-width="1" stroke-dasharray="5" stroke-opacity="0.1" stroke="#000000" class="amcharts-axis-grid"></path></g><g><path cs="100,100" d="M0.5,119.5 L0.5,119.5 L773.5,119.5" fill="none" stroke-width="1" stroke-dasharray="5" stroke-opacity="0.1" stroke="#000000" class="amcharts-axis-grid"></path></g><g><path cs="100,100" d="M0.5,159.5 L0.5,159.5 L773.5,159.5" fill="none" stroke-width="1" stroke-dasharray="5" stroke-opacity="0.1" stroke="#000000" class="amcharts-axis-grid"></path></g><g><path cs="100,100" d="M0.5,199.5 L0.5,199.5 L773.5,199.5" fill="none" stroke-width="1" stroke-dasharray="5" stroke-opacity="0.1" stroke="#000000" class="amcharts-axis-grid"></path></g></g></g><g transform="translate(57,32)" clip-path="url(#AmChartsEl-40)"><g visibility="hidden"></g></g><g></g><g></g><g></g><g><g transform="translate(57,32)" class="amcharts-graph-line amcharts-graph-graphAuto0_1488455767340"><g></g></g><g transform="translate(57,32)" class="amcharts-graph-line amcharts-graph-graphAuto1_1488455767340" opacity="1" visibility="visible"><g></g><g clip-path="url(#AmChartsEl-42)"><path cs="100,100" d="M35.5,159.7 L105.5,40.3 L176.5,80.1 L246.5,119.9 L316.5,0.5 L386.5,40.3 L457.5,40.3 L527.5,0.5 L597.5,159.7 L668.5,80.1 L738.5,40.3 M0,0 L0,0" fill="none" stroke-width="1" stroke-opacity="0.9" stroke="#fdd400" stroke-linejoin="round" class="amcharts-graph-stroke"></path></g><clippath id="AmChartsEl-42"><rect x="0" y="0" width="773" height="199" rx="0" ry="0" stroke-width="0"></rect></clippath><g></g></g><g transform="translate(57,32)" class="amcharts-graph-line amcharts-graph-graphAuto2_1488455767340" opacity="1" visibility="visible"><g></g><g clip-path="url(#AmChartsEl-43)"><path cs="100,100" d="M35.5,80.1 L105.5,199.5 L176.5,0.5 L246.5,0.5 L316.5,40.3 L386.5,0.5 L457.5,80.1 L527.5,159.7 L597.5,40.3 L668.5,199.5 L738.5,119.9 M0,0 L0,0" fill="none" stroke-width="1" stroke-opacity="0.9" stroke="#84b761" stroke-linejoin="round" class="amcharts-graph-stroke"></path></g><clippath id="AmChartsEl-43"><rect x="0" y="0" width="773" height="199" rx="0" ry="0" stroke-width="0"></rect></clippath><g></g></g></g><g></g><g><g class="amcharts-category-axis"><path cs="100,100" d="M0.5,0.5 L773.5,0.5" fill="none" stroke-width="1" stroke-opacity="0" stroke="#000000" transform="translate(57,32)" class="amcharts-axis-line"></path></g><g class="amcharts-value-axis value-axis-valueAxisAuto0_1488455767339"><path cs="100,100" d="M0.5,0.5 L0.5,199.5" fill="none" stroke-width="1" stroke-opacity="0" stroke="#000000" transform="translate(57,32)" class="amcharts-axis-line" visibility="visible"></path></g></g><g><g transform="translate(57,32)" clip-path="url(#AmChartsEl-41)" style="pointer-events: none;"><path cs="100,100" d="M0.5,0.5 L0.5,0.5 L0.5,199.5" fill="none" stroke-width="1" stroke-opacity="0" stroke="#000000" class="amcharts-cursor-line amcharts-cursor-line-vertical" visibility="hidden" transform="translate(386,0)"></path><path cs="100,100" d="M0.5,0.5 L773.5,0.5 L773.5,0.5" fill="none" stroke-width="1" stroke="#000000" class="amcharts-cursor-line amcharts-cursor-line-horizontal" visibility="hidden" transform="translate(0,26)"></path></g><clippath id="AmChartsEl-41"><rect x="0" y="0" width="773" height="199" rx="0" ry="0" stroke-width="0"></rect></clippath></g><g></g><g><g transform="translate(57,32)" class="amcharts-graph-line amcharts-graph-graphAuto0_1488455767340"></g><g transform="translate(57,32)" class="amcharts-graph-line amcharts-graph-graphAuto1_1488455767340" opacity="1" visibility="visible"><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(35,159)" aria-label="Germany 1930 5" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(105,40)" aria-label="Germany 1934 2" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(176,80)" aria-label="Germany 1938 3" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(246,119) scale(1)" aria-label="Germany 1950 4" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(316,0) scale(1)" aria-label="Germany 1954 1" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(386,40) scale(1)" aria-label="Germany 1958 2" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(457,40) scale(1)" aria-label="Germany 1962 2" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(527,0) scale(1)" aria-label="Germany 1966 1" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(597,159) scale(1)" aria-label="Germany 1970 5" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(668,80) scale(1)" aria-label="Germany 1974 3" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(738,40) scale(1)" aria-label="Germany 1978 2" class="amcharts-graph-bullet"></circle></g><g transform="translate(57,32)" class="amcharts-graph-line amcharts-graph-graphAuto2_1488455767340" opacity="1" visibility="visible"><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(35,80)" aria-label="United Kingdom 1930 3" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(105,199)" aria-label="United Kingdom 1934 6" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(176,0)" aria-label="United Kingdom 1938 1" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(246,0) scale(1)" aria-label="United Kingdom 1950 1" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(316,40) scale(1)" aria-label="United Kingdom 1954 2" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(386,0) scale(1)" aria-label="United Kingdom 1958 1" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(457,80) scale(1)" aria-label="United Kingdom 1962 3" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(527,159) scale(1)" aria-label="United Kingdom 1966 5" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(597,40) scale(1)" aria-label="United Kingdom 1970 2" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(668,199) scale(1)" aria-label="United Kingdom 1974 6" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(738,119) scale(1)" aria-label="United Kingdom 1978 4" class="amcharts-graph-bullet"></circle></g></g><g><g></g></g><g><g class="amcharts-category-axis" transform="translate(57,32)" visibility="visible"><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(35.13636363636363,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1930</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(105.13636363636363,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1934</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(176.13636363636363,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1938</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(246.13636363636363,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1950</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(316.1363636363636,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1954</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(386.1363636363636,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1958</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(457.1363636363636,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1962</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(527.1363636363636,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1966</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(597.1363636363636,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1970</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(668.1363636363636,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1974</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(738.1363636363636,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1978</tspan></text></g><g class="amcharts-value-axis value-axis-valueAxisAuto0_1488455767339" transform="translate(57,32)" visibility="visible"><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" transform="translate(-12,-1)" class="amcharts-axis-label"><tspan y="6" x="0">1</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" transform="translate(-12,39)" class="amcharts-axis-label"><tspan y="6" x="0">2</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" transform="translate(-12,79)" class="amcharts-axis-label"><tspan y="6" x="0">3</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" transform="translate(-12,118)" class="amcharts-axis-label"><tspan y="6" x="0">4</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" transform="translate(-12,158)" class="amcharts-axis-label"><tspan y="6" x="0">5</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" transform="translate(-12,198)" class="amcharts-axis-label"><tspan y="6" x="0">6</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="12px" opacity="1" font-weight="bold" text-anchor="middle" class="amcharts-axis-title" transform="translate(-36,100) rotate(-90)"><tspan y="6" x="0">Place taken</tspan></text></g></g><g></g><g transform="translate(57,32)"></g><g></g><g></g><clippath id="AmChartsEl-40"><rect x="-1" y="-1" width="775" height="201" rx="0" ry="0" stroke-width="0"></rect></clippath></svg><a href="http://www.amcharts.com/javascript-charts/" title="JavaScript charts" style="position: absolute; text-decoration: none; color: rgb(0, 0, 0); font-family: Verdana; font-size: 11px; opacity: 0.7; display: block; left: 62px; top: 37px;"></a></div><div class="amChartsLegend amcharts-legend-div" style="overflow: hidden; position: relative; text-align: left; width: 851px; height: 48px; cursor: default;"><svg version="1.1" style="position: absolute; width: 851px; height: 48px;"><desc>JavaScript chart by amCharts 3.20.20</desc><g transform="translate(57,0)"><path cs="100,100" d="M0.5,0.5 L773.5,0.5 L773.5,37.5 L0.5,37.5 Z" fill="#FFFFFF" stroke="#000000" fill-opacity="0" stroke-width="1" stroke-opacity="0" class="amcharts-legend-bg"></path><g transform="translate(0,11)"><g cursor="pointer" class="amcharts-legend-item-graphAuto0_1488455767340" aria-label="Italy" transform="translate(0,0)"><g class="amcharts-graph-line amcharts-graph-graphAuto0_1488455767340 amcharts-legend-marker"><path cs="100,100" d="M0.5,8.5 L32.5,8.5" fill="none" stroke-width="1" stroke-opacity="0.9" stroke="#AAB3B3" class="amcharts-graph-stroke"></path><circle r="4" cx="0" cy="0" fill="#AAB3B3" stroke="#AAB3B3" fill-opacity="1" stroke-width="2" stroke-opacity="0" class="amcharts-graph-bullet" transform="translate(17,8)"></circle></g><text y="6" fill="#AAB3B3" font-family="Verdana" font-size="11px" opacity="1" text-anchor="start" class="amcharts-legend-label" transform="translate(37,7)"><tspan y="6" x="0">Italy</tspan></text><text y="6" fill="#AAB3B3" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" class="amcharts-legend-value" transform="translate(182,7)"> </text><rect x="32" y="0" width="150" height="18" rx="0" ry="0" stroke-width="0" stroke="none" fill="#fff" fill-opacity="0.005"></rect></g><g cursor="pointer" class="amcharts-legend-item-graphAuto1_1488455767340" aria-label="Germany" transform="translate(197,0)"><g class="amcharts-graph-line amcharts-graph-graphAuto1_1488455767340 amcharts-legend-marker"><path cs="100,100" d="M0.5,8.5 L32.5,8.5" fill="none" stroke-width="1" stroke-opacity="0.9" stroke="#fdd400" class="amcharts-graph-stroke"></path><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#000000" fill-opacity="1" stroke-width="2" stroke-opacity="0" class="amcharts-graph-bullet" transform="translate(17,8)"></circle></g><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="start" class="amcharts-legend-label" transform="translate(37,7)"><tspan y="6" x="0">Germany</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" class="amcharts-legend-value" transform="translate(182,7)"> </text><rect x="32" y="0" width="150" height="18" rx="0" ry="0" stroke-width="0" stroke="none" fill="#fff" fill-opacity="0.005"></rect></g><g cursor="pointer" class="amcharts-legend-item-graphAuto2_1488455767340" aria-label="United Kingdom" transform="translate(393,0)"><g class="amcharts-graph-line amcharts-graph-graphAuto2_1488455767340 amcharts-legend-marker"><path cs="100,100" d="M0.5,8.5 L32.5,8.5" fill="none" stroke-width="1" stroke-opacity="0.9" stroke="#84b761" class="amcharts-graph-stroke"></path><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#000000" fill-opacity="1" stroke-width="2" stroke-opacity="0" class="amcharts-graph-bullet" transform="translate(17,8)"></circle></g><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="start" class="amcharts-legend-label" transform="translate(37,7)"><tspan y="6" x="0">United Kingdom</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" class="amcharts-legend-value" transform="translate(182,7)"> </text><rect x="32" y="0" width="150" height="18" rx="0" ry="0" stroke-width="0" stroke="none" fill="#fff" fill-opacity="0.005"></rect></g></g></g></svg></div><div class="amcharts-export-menu amcharts-export-menu-bottom-right amExportButton"><ul><li class="export-main"><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>menu.label.undefined</span></a><ul><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>Download as ...</span></a><ul><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>PNG</span></a></li><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>JPG</span></a></li><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>SVG</span></a></li><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>PDF</span></a></li></ul></li><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>Save as ...</span></a><ul><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>CSV</span></a></li><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>XLSX</span></a></li><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>JSON</span></a></li></ul></li><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>Annotate ...</span></a></li><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>Print</span></a></li></ul></li></ul></div></div>
							-->
							</div>
		                </div>
		            </div>
		        </div>
				<div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">
		            <div class="portlet light bordered">
		                <div class="portlet-title" style="height:30px; min-height:10px; margin-top:-10px;">
		                    <div class="caption ">
		                    	<i class="icon-bar-chart font-green-haze2" style="font-size:15px"></i>
		                        <span class="caption-subject bold uppercase font-green-haze2" style="font-size:15px">시간대별 개인정보 취급자 건수</span>
		                        <!-- <span class="caption-helper">distance stats...</span> -->
		                    </div>
		                    <!-- <div class="actions">
		                        <a href="#" class="btn btn-circle green btn-outline btn-sm">
		                            <i class="fa fa-pencil"></i> Export </a>
		                        <a href="#" class="btn btn-circle green btn-outline btn-sm">
		                            <i class="fa fa-print"></i> Print </a>
		                    </div> -->
		                </div>
		                <div class="portlet-body" style="position:relative; top:-8px; left:0px;">
		                    <div id="dashboard_amchart_18" class="chart"></div>
		                </div>
		            </div>
		        </div> 
				<div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">
		            <div class="portlet light bordered">
		                <div class="portlet-title" style="height:30px; min-height:10px; margin-top:-10px;">
		                    <div class="caption ">
		                    	<i class="icon-bar-chart font-green-haze2 style="font-size:15px"></i>
		                        <span class="caption-subject bold uppercase font-green-haze2" style="font-size:15px">개인별 로그</span>
		                        <!-- <span class="caption-helper">distance stats...</span> -->
		                    </div>
		                    <!-- <div class="actions">
		                        <a href="#" class="btn btn-circle green btn-outline btn-sm">
		                            <i class="fa fa-pencil"></i> Export </a>
		                        <a href="#" class="btn btn-circle green btn-outline btn-sm">
		                            <i class="fa fa-print"></i> Print </a>
		                    </div> -->
		                </div>
		                <!--
						<div class="portlet-body" style="position:relative; top:-8px; left:0px;">
		                    <div id="dashboard_amchart_2" class="chart"></div>
		                </div>
						-->
						<!--portlet-body S-->
						<div class="portlet-body" style="position:relative; top:-10px; left:0px; height:268px; opacity:1">
							<div class="row">
							<div class="dataTables_scroll" style="width:100%; clear:both;">
								<div class="dataTables_scrollBody" style="overflow-y: auto; overflow-x:hidden; height:268px; width: 100%;">
									<!-- <table id="table_log2" style="border-top: 2px solid #459aef;" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer" role="grid"> -->
									<table id="table_log2" class="tableClass">
										<thead>
											<tr role="row"> 
												<th width="50%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; height:10px; font-size:12px;">
													성명</th>
												<th width="25%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; height:10px; font-size:12px;">
													로그수</th>
												<th width="25%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; height:10px; font-size:12px;">
													개인정보수</th>
											</tr>
										</thead>
										<tbody>
											<!-- <tr style="text-align: center;">
												<td width="20%" style="height:10px; font-size:12px">보조사업관리</td>
												<td width="15%" style="height:10px; font-size:12px">2017-02-15</td>
												<td width="10%" style="height:10px; font-size:12px">부속실</td>
											</tr> -->
										</tbody>
									</table>
									<!-- <div style="position: relative; top: 0px; left: 0px; width: 1px; height: 8366px;"></div> -->
								</div>
								<div class="dataTables_processing DTS_Loading" style="display: none;">Please wait ...</div>
							</div>
							</div>			
		                </div>
						<!--portlet-body E-->
		            </div>
		        </div>
				 <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">
		            <div class="portlet light bordered">
		                <div class="portlet-title" style="height:30px; min-height:10px; margin-top:-10px;">
		                    <div class="caption ">
		                    	<i class="icon-bar-chart font-green-haze2" style="font-size:15px"></i>
		                        <span class="caption-subject bold uppercase font-green-haze2" style="font-size:15px">부서별 로그</span>
		                        <!-- <span class="caption-helper">distance stats...</span> -->
		                    </div>
		                    <!-- <div class="actions">
		                        <a href="#" class="btn btn-circle green btn-outline btn-sm">
		                            <i class="fa fa-pencil"></i> Export </a>
		                        <a href="#" class="btn btn-circle green btn-outline btn-sm">
		                            <i class="fa fa-print"></i> Print </a>
		                    </div> -->
		                </div>
						<!--
		                <div class="portlet-body" style="position:relative; top:-8px; left:0px;">
		                    <div id="dashboard_amchart_2" class="chart"></div>
		                </div>
						-->
						<!--portlet-body S-->
						<div class="portlet-body" style="position:relative; top:-10px; left:0px; height:268px; opacity:1">
							<div class="row">
							<div class="dataTables_scroll" style="width:100%; clear:both;">
								<div class="dataTables_scrollBody" style="overflow-y: auto; overflow-x:hidden; height:268px; width: 100%;">
									<!-- <table id="table_log3" style="border-top: 2px solid #459aef;" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer" role="grid"> -->
									<table id="table_log3" class="tableClass">
										<thead>
											<tr role="row"> 
												<th width="20%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; height:10px; font-size:12px;">
													부서명</th>
												<th width="15%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; height:10px; font-size:12px;">
													로그수</th>
												<th width="10%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; height:10px; font-size:12px;">
													개인정보수</th>
											</tr>
										</thead>
										<tbody>
											<!-- <tr style="text-align: center;">
												<td width="20%" style="height:10px; font-size:12px">보조사업관리</td>
												<td width="15%" style="height:10px; font-size:12px">2017-02-15</td>
												<td width="10%" style="height:10px; font-size:12px">부속실</td>
											</tr> -->
										</tbody>
									</table>
									<!-- <div style="position: relative; top: 0px; left: 0px; width: 1px; height: 8366px;"></div> -->
								</div>
								<div class="dataTables_processing DTS_Loading" style="display: none;">Please wait ...</div>
							</div>
							</div>			
		                </div>
						<!--portlet-body E-->
		            </div>
		        </div>
				 <div class="col-lg-8 col-md-8 col-xs-12 col-sm-12">
		            <div class="portlet light bordered">
		                <div class="portlet-title" style="height:30px; min-height:10px; margin-top:-15px;">
		                    <div class="caption ">
		                    	<i class="icon-bar-chart font-green-haze2" style="font-size:15px"></i>
		                        <span class="caption-subject bold uppercase font-green-haze2" style="font-size:15px">부서별 개인정보 처리 현황</span>
		                        <!-- <span class="caption-helper">distance stats...</span> -->
		                    </div>
		                    
		                </div>
		                <div class="portlet-body" style="position:relative; top:-8px; left:0px;">
		                    <div id="dashboard_amchart_3" class="chart"></div>
		                </div>
		            </div>
					<div class="portlet light bordered">
		                <div class="portlet-title" style="height:30px; min-height:10px; margin-top:-10px;">
		                    <div class="caption ">
		                    	<i class="icon-bar-chart font-green-haze2" style="font-size:15px"></i>
		                        <span class="caption-subject bold uppercase font-green-haze2" style="font-size:15px">부서별 개인정보 처리 유형</span>
		                        <!-- <span class="caption-helper">distance stats...</span> -->
		                    </div>
	                    </div>
		                <div class="portlet-body" style="position:relative; top:-8px; left:0px;">
		                    <div id="dashboard_amchart_5" class="chart"></div>
		                </div>
		            </div>
		        </div>
		        <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">
		            <div class="portlet light bordered">
		                <div class="portlet-title" style="height:30px; min-height:10px; margin-top:-10px;">
		                    <div class="caption ">
		                    	<i class="icon-bar-chart font-green-haze2" style="font-size:15px"></i>
		                        <span class="caption-subject bold uppercase font-green-haze2" style="font-size:15px">시스템 별 상태</span>
		                        <!-- <span class="caption-helper">distance stats...</span> -->
		                    </div>
		                   
		                </div>
						
							
						<div class="portlet-body" style="position:relative; top:-8px; left:0px; height:576px">
							<c:set var="testVar">새올행정시스템,주민등록시스템,세외수입시스템,표준지방세정보시스템,재정시스템,건축행정시스템,부동산종합공부시스템,부동산거래관리시스템,지방세가상계좌시스템,인사행정시스템,온나라시스템</c:set>
							<c:forTokens items="${testVar}" delims="," var="value" varStatus="status">
								<div class="chart-block" style="padding:6px">
	  								<font color="#fff" size="4"><c:out value="${value}" /></font> <div id="line${status.index }" style="vertical-align: middle; display: inline-block; width: 40%; height: 40px;"></div> 
	  								<font color="#fff" size="4">6%</font> <div id="column${status.index }" style="vertical-align: middle;display: inline-block; width: 40%; height: 40px;"></div>
								</div>
							</c:forTokens>
		                </div>
						
						
						<!--portlet-body S
						<div class="portlet-body" style="position:relative; top:-10px; left:0px; height:576px;">
							<div class="row">
							<div class="dataTables_scroll" style="width:100%; clear:both;">
								<div class="dataTables_scrollBody" style="overflow-y: auto; overflow-x:hidden; height: 575px; width: 100%;">
									<table style="border-top: 2px solid #459aef;" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer" role="grid">
										<thead>
											<tr role="row" class="heading">
												<th width="20%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; height:10px; font-size:12px;">
													시스템</th>
												<th width="15%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; height:10px; font-size:12px;">
													생성일시</th>
												<th width="10%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; height:10px; font-size:12px;">
													부서</th>
												<th width="10%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; height:10px; font-size:12px;">
													ID</th>
												<th width="15%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; height:10px; font-size:12px;">
													사용자명</th>
												<th width="30%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; height:10px; font-size:12px;">
													사용자IP</th>
											</tr>
										</thead>
										<tbody>
											<tr style="text-align: center;">
												<td width="20%" style="height:10px; font-size:12px">보조사업관리</td>
												<td width="15%" style="height:10px; font-size:12px">2017-02-15</td>
												<td width="10%" style="height:10px; font-size:12px">부속실</td>
												<td width="10%" style="height:10px; font-size:12px">ehchoi</td>
												<td width="15%" style="height:10px; font-size:12px">홍길동</td>
												<td width="30%" style="height:10px; font-size:12px">192.168.0.211</td>
											</tr>
											<tr style="text-align: center;">
												<td style="height:10px; font-size:12px">보조사업관리</td>
												<td style="height:10px; font-size:12px">2017-02-15</td>
												<td style="height:10px; font-size:12px">부속실</td>
												<td style="height:10px; font-size:12px">ehchoi</td>
												<td style="height:10px; font-size:12px">홍길동</td>
												<td style="height:10px; font-size:12px">192.168.0.211</td>
											</tr>
										</tbody>
									</table>
									<div style="position: relative; top: 0px; left: 0px; width: 1px; height: 8366px;"></div>
								</div>
								<div class="dataTables_processing DTS_Loading" style="display: none;">Please wait ...</div>
							</div>
						</div>			
		                </div>
						<!--portlet-body E-->
		            </div>
		        </div>
				<div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">
		            <div class="portlet light bordered">
		                <div class="portlet-title" style="height:30px; min-height:10px; margin-top:-10px;">
		                    <div class="caption">
		                    	<i class="icon-bar-chart font-green-haze2" style="font-size:15px"></i>
		                        <span class="caption-subject bold uppercase font-green-haze2" style="font-size:15px">일주일 추이</span>
		                        <!-- <span class="caption-helper font-dark">distance stats...</span> -->
		                    </div> 
		                    
		                </div>
		                <div class="portlet-body" style="position:relative; top:-8px; left:0px;">
							<div id="dashboard_amchart_9" class="chart" style="overflow: visible; text-align: left;">
							<!--
							<div class="amcharts-main-div" style="position: relative; width: 100%; height: 100%;"><div class="amcharts-chart-div" style="overflow: hidden; position: relative; text-align: left; width: 851px; height: 252px; padding: 0px; cursor: default;"><svg version="1.1" style="position: absolute; width: 546px; height: 252px; top: 0px; left: 0px;"><desc>JavaScript chart by amCharts 3.20.20</desc><g><path cs="100,100" d="M0.5,0.5 L850.5,0.5 L850.5,251.5 L0.5,251.5 Z" fill="#FFFFFF" stroke="#000000" fill-opacity="0" stroke-width="1" stroke-opacity="0" class="amcharts-bg"></path><path cs="100,100" d="M0.5,0.5 L773.5,0.5 L773.5,199.5 L0.5,199.5 L0.5,0.5 Z" fill="#FFFFFF" stroke="#000000" fill-opacity="0" stroke-width="1" stroke-opacity="0" class="amcharts-plot-area" transform="translate(57,32)"></path></g><g><g class="amcharts-category-axis" transform="translate(57,32)"><g><rect x="0.5" y="0.5" width="70" height="199" rx="0" ry="0" stroke-width="0" fill="#000000" stroke="#000000" fill-opacity="0.05" stroke-opacity="0.05" transform="translate(0,0)" class="amcharts-axis-fill"></rect></g><g><rect x="0.5" y="0.5" width="70" height="199" rx="0" ry="0" stroke-width="0" fill="#000000" stroke="#000000" fill-opacity="0.05" stroke-opacity="0.05" transform="translate(141,0)" class="amcharts-axis-fill"></rect></g><g><rect x="0.5" y="0.5" width="70" height="199" rx="0" ry="0" stroke-width="0" fill="#000000" stroke="#000000" fill-opacity="0.05" stroke-opacity="0.05" transform="translate(281,0)" class="amcharts-axis-fill"></rect></g><g><rect x="0.5" y="0.5" width="70" height="199" rx="0" ry="0" stroke-width="0" fill="#000000" stroke="#000000" fill-opacity="0.05" stroke-opacity="0.05" transform="translate(422,0)" class="amcharts-axis-fill"></rect></g><g><rect x="0.5" y="0.5" width="71" height="199" rx="0" ry="0" stroke-width="0" fill="#000000" stroke="#000000" fill-opacity="0.05" stroke-opacity="0.05" transform="translate(562,0)" class="amcharts-axis-fill"></rect></g><g><rect x="0.5" y="0.5" width="70" height="199" rx="0" ry="0" stroke-width="0" fill="#000000" stroke="#000000" fill-opacity="0.05" stroke-opacity="0.05" transform="translate(703,0)" class="amcharts-axis-fill"></rect></g></g><g class="amcharts-value-axis value-axis-valueAxisAuto0_1488455767339" transform="translate(57,32)" visibility="visible"><g><path cs="100,100" d="M0.5,0.5 L0.5,0.5 L773.5,0.5" fill="none" stroke-width="1" stroke-dasharray="5" stroke-opacity="0.1" stroke="#000000" class="amcharts-axis-grid"></path></g><g><path cs="100,100" d="M0.5,40.5 L0.5,40.5 L773.5,40.5" fill="none" stroke-width="1" stroke-dasharray="5" stroke-opacity="0.1" stroke="#000000" class="amcharts-axis-grid"></path></g><g><path cs="100,100" d="M0.5,80.5 L0.5,80.5 L773.5,80.5" fill="none" stroke-width="1" stroke-dasharray="5" stroke-opacity="0.1" stroke="#000000" class="amcharts-axis-grid"></path></g><g><path cs="100,100" d="M0.5,119.5 L0.5,119.5 L773.5,119.5" fill="none" stroke-width="1" stroke-dasharray="5" stroke-opacity="0.1" stroke="#000000" class="amcharts-axis-grid"></path></g><g><path cs="100,100" d="M0.5,159.5 L0.5,159.5 L773.5,159.5" fill="none" stroke-width="1" stroke-dasharray="5" stroke-opacity="0.1" stroke="#000000" class="amcharts-axis-grid"></path></g><g><path cs="100,100" d="M0.5,199.5 L0.5,199.5 L773.5,199.5" fill="none" stroke-width="1" stroke-dasharray="5" stroke-opacity="0.1" stroke="#000000" class="amcharts-axis-grid"></path></g></g></g><g transform="translate(57,32)" clip-path="url(#AmChartsEl-40)"><g visibility="hidden"></g></g><g></g><g></g><g></g><g><g transform="translate(57,32)" class="amcharts-graph-line amcharts-graph-graphAuto0_1488455767340"><g></g></g><g transform="translate(57,32)" class="amcharts-graph-line amcharts-graph-graphAuto1_1488455767340" opacity="1" visibility="visible"><g></g><g clip-path="url(#AmChartsEl-42)"><path cs="100,100" d="M35.5,159.7 L105.5,40.3 L176.5,80.1 L246.5,119.9 L316.5,0.5 L386.5,40.3 L457.5,40.3 L527.5,0.5 L597.5,159.7 L668.5,80.1 L738.5,40.3 M0,0 L0,0" fill="none" stroke-width="1" stroke-opacity="0.9" stroke="#fdd400" stroke-linejoin="round" class="amcharts-graph-stroke"></path></g><clippath id="AmChartsEl-42"><rect x="0" y="0" width="773" height="199" rx="0" ry="0" stroke-width="0"></rect></clippath><g></g></g><g transform="translate(57,32)" class="amcharts-graph-line amcharts-graph-graphAuto2_1488455767340" opacity="1" visibility="visible"><g></g><g clip-path="url(#AmChartsEl-43)"><path cs="100,100" d="M35.5,80.1 L105.5,199.5 L176.5,0.5 L246.5,0.5 L316.5,40.3 L386.5,0.5 L457.5,80.1 L527.5,159.7 L597.5,40.3 L668.5,199.5 L738.5,119.9 M0,0 L0,0" fill="none" stroke-width="1" stroke-opacity="0.9" stroke="#84b761" stroke-linejoin="round" class="amcharts-graph-stroke"></path></g><clippath id="AmChartsEl-43"><rect x="0" y="0" width="773" height="199" rx="0" ry="0" stroke-width="0"></rect></clippath><g></g></g></g><g></g><g><g class="amcharts-category-axis"><path cs="100,100" d="M0.5,0.5 L773.5,0.5" fill="none" stroke-width="1" stroke-opacity="0" stroke="#000000" transform="translate(57,32)" class="amcharts-axis-line"></path></g><g class="amcharts-value-axis value-axis-valueAxisAuto0_1488455767339"><path cs="100,100" d="M0.5,0.5 L0.5,199.5" fill="none" stroke-width="1" stroke-opacity="0" stroke="#000000" transform="translate(57,32)" class="amcharts-axis-line" visibility="visible"></path></g></g><g><g transform="translate(57,32)" clip-path="url(#AmChartsEl-41)" style="pointer-events: none;"><path cs="100,100" d="M0.5,0.5 L0.5,0.5 L0.5,199.5" fill="none" stroke-width="1" stroke-opacity="0" stroke="#000000" class="amcharts-cursor-line amcharts-cursor-line-vertical" visibility="hidden" transform="translate(386,0)"></path><path cs="100,100" d="M0.5,0.5 L773.5,0.5 L773.5,0.5" fill="none" stroke-width="1" stroke="#000000" class="amcharts-cursor-line amcharts-cursor-line-horizontal" visibility="hidden" transform="translate(0,26)"></path></g><clippath id="AmChartsEl-41"><rect x="0" y="0" width="773" height="199" rx="0" ry="0" stroke-width="0"></rect></clippath></g><g></g><g><g transform="translate(57,32)" class="amcharts-graph-line amcharts-graph-graphAuto0_1488455767340"></g><g transform="translate(57,32)" class="amcharts-graph-line amcharts-graph-graphAuto1_1488455767340" opacity="1" visibility="visible"><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(35,159)" aria-label="Germany 1930 5" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(105,40)" aria-label="Germany 1934 2" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(176,80)" aria-label="Germany 1938 3" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(246,119) scale(1)" aria-label="Germany 1950 4" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(316,0) scale(1)" aria-label="Germany 1954 1" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(386,40) scale(1)" aria-label="Germany 1958 2" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(457,40) scale(1)" aria-label="Germany 1962 2" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(527,0) scale(1)" aria-label="Germany 1966 1" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(597,159) scale(1)" aria-label="Germany 1970 5" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(668,80) scale(1)" aria-label="Germany 1974 3" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(738,40) scale(1)" aria-label="Germany 1978 2" class="amcharts-graph-bullet"></circle></g><g transform="translate(57,32)" class="amcharts-graph-line amcharts-graph-graphAuto2_1488455767340" opacity="1" visibility="visible"><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(35,80)" aria-label="United Kingdom 1930 3" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(105,199)" aria-label="United Kingdom 1934 6" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(176,0)" aria-label="United Kingdom 1938 1" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(246,0) scale(1)" aria-label="United Kingdom 1950 1" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(316,40) scale(1)" aria-label="United Kingdom 1954 2" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(386,0) scale(1)" aria-label="United Kingdom 1958 1" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(457,80) scale(1)" aria-label="United Kingdom 1962 3" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(527,159) scale(1)" aria-label="United Kingdom 1966 5" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(597,40) scale(1)" aria-label="United Kingdom 1970 2" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(668,199) scale(1)" aria-label="United Kingdom 1974 6" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(738,119) scale(1)" aria-label="United Kingdom 1978 4" class="amcharts-graph-bullet"></circle></g></g><g><g></g></g><g><g class="amcharts-category-axis" transform="translate(57,32)" visibility="visible"><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(35.13636363636363,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1930</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(105.13636363636363,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1934</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(176.13636363636363,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1938</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(246.13636363636363,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1950</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(316.1363636363636,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1954</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(386.1363636363636,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1958</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(457.1363636363636,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1962</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(527.1363636363636,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1966</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(597.1363636363636,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1970</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(668.1363636363636,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1974</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(738.1363636363636,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1978</tspan></text></g><g class="amcharts-value-axis value-axis-valueAxisAuto0_1488455767339" transform="translate(57,32)" visibility="visible"><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" transform="translate(-12,-1)" class="amcharts-axis-label"><tspan y="6" x="0">1</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" transform="translate(-12,39)" class="amcharts-axis-label"><tspan y="6" x="0">2</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" transform="translate(-12,79)" class="amcharts-axis-label"><tspan y="6" x="0">3</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" transform="translate(-12,118)" class="amcharts-axis-label"><tspan y="6" x="0">4</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" transform="translate(-12,158)" class="amcharts-axis-label"><tspan y="6" x="0">5</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" transform="translate(-12,198)" class="amcharts-axis-label"><tspan y="6" x="0">6</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="12px" opacity="1" font-weight="bold" text-anchor="middle" class="amcharts-axis-title" transform="translate(-36,100) rotate(-90)"><tspan y="6" x="0">Place taken</tspan></text></g></g><g></g><g transform="translate(57,32)"></g><g></g><g></g><clippath id="AmChartsEl-40"><rect x="-1" y="-1" width="775" height="201" rx="0" ry="0" stroke-width="0"></rect></clippath></svg><a href="http://www.amcharts.com/javascript-charts/" title="JavaScript charts" style="position: absolute; text-decoration: none; color: rgb(0, 0, 0); font-family: Verdana; font-size: 11px; opacity: 0.7; display: block; left: 62px; top: 37px;"></a></div><div class="amChartsLegend amcharts-legend-div" style="overflow: hidden; position: relative; text-align: left; width: 851px; height: 48px; cursor: default;"><svg version="1.1" style="position: absolute; width: 851px; height: 48px;"><desc>JavaScript chart by amCharts 3.20.20</desc><g transform="translate(57,0)"><path cs="100,100" d="M0.5,0.5 L773.5,0.5 L773.5,37.5 L0.5,37.5 Z" fill="#FFFFFF" stroke="#000000" fill-opacity="0" stroke-width="1" stroke-opacity="0" class="amcharts-legend-bg"></path><g transform="translate(0,11)"><g cursor="pointer" class="amcharts-legend-item-graphAuto0_1488455767340" aria-label="Italy" transform="translate(0,0)"><g class="amcharts-graph-line amcharts-graph-graphAuto0_1488455767340 amcharts-legend-marker"><path cs="100,100" d="M0.5,8.5 L32.5,8.5" fill="none" stroke-width="1" stroke-opacity="0.9" stroke="#AAB3B3" class="amcharts-graph-stroke"></path><circle r="4" cx="0" cy="0" fill="#AAB3B3" stroke="#AAB3B3" fill-opacity="1" stroke-width="2" stroke-opacity="0" class="amcharts-graph-bullet" transform="translate(17,8)"></circle></g><text y="6" fill="#AAB3B3" font-family="Verdana" font-size="11px" opacity="1" text-anchor="start" class="amcharts-legend-label" transform="translate(37,7)"><tspan y="6" x="0">Italy</tspan></text><text y="6" fill="#AAB3B3" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" class="amcharts-legend-value" transform="translate(182,7)"> </text><rect x="32" y="0" width="150" height="18" rx="0" ry="0" stroke-width="0" stroke="none" fill="#fff" fill-opacity="0.005"></rect></g><g cursor="pointer" class="amcharts-legend-item-graphAuto1_1488455767340" aria-label="Germany" transform="translate(197,0)"><g class="amcharts-graph-line amcharts-graph-graphAuto1_1488455767340 amcharts-legend-marker"><path cs="100,100" d="M0.5,8.5 L32.5,8.5" fill="none" stroke-width="1" stroke-opacity="0.9" stroke="#fdd400" class="amcharts-graph-stroke"></path><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#000000" fill-opacity="1" stroke-width="2" stroke-opacity="0" class="amcharts-graph-bullet" transform="translate(17,8)"></circle></g><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="start" class="amcharts-legend-label" transform="translate(37,7)"><tspan y="6" x="0">Germany</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" class="amcharts-legend-value" transform="translate(182,7)"> </text><rect x="32" y="0" width="150" height="18" rx="0" ry="0" stroke-width="0" stroke="none" fill="#fff" fill-opacity="0.005"></rect></g><g cursor="pointer" class="amcharts-legend-item-graphAuto2_1488455767340" aria-label="United Kingdom" transform="translate(393,0)"><g class="amcharts-graph-line amcharts-graph-graphAuto2_1488455767340 amcharts-legend-marker"><path cs="100,100" d="M0.5,8.5 L32.5,8.5" fill="none" stroke-width="1" stroke-opacity="0.9" stroke="#84b761" class="amcharts-graph-stroke"></path><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#000000" fill-opacity="1" stroke-width="2" stroke-opacity="0" class="amcharts-graph-bullet" transform="translate(17,8)"></circle></g><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="start" class="amcharts-legend-label" transform="translate(37,7)"><tspan y="6" x="0">United Kingdom</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" class="amcharts-legend-value" transform="translate(182,7)"> </text><rect x="32" y="0" width="150" height="18" rx="0" ry="0" stroke-width="0" stroke="none" fill="#fff" fill-opacity="0.005"></rect></g></g></g></svg></div><div class="amcharts-export-menu amcharts-export-menu-bottom-right amExportButton"><ul><li class="export-main"><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>menu.label.undefined</span></a><ul><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>Download as ...</span></a><ul><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>PNG</span></a></li><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>JPG</span></a></li><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>SVG</span></a></li><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>PDF</span></a></li></ul></li><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>Save as ...</span></a><ul><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>CSV</span></a></li><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>XLSX</span></a></li><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>JSON</span></a></li></ul></li><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>Annotate ...</span></a></li><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>Print</span></a></li></ul></li></ul></div></div>
							-->
							</div>
		                </div>
		            </div>
		        </div>
				 <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">
		            <div class="portlet light bordered">
		                <div class="portlet-title" style="height:30px; min-height:10px; margin-top:-10px;">
		                    <div class="caption">
		                    	<i class="icon-bar-chart font-green-haze2" style="font-size:15px"></i>
		                        <span class="caption-subject bold uppercase font-green-haze2" style="font-size:15px">개인정보 유형별 비율</span>
		                        <!-- <span class="caption-helper">distance stats...</span> -->
		                    </div>
		                    
		                </div>
						
						<div class="portlet-body" style="position:relative; top:-8px; left:0px;">
		                    <div id="dashboard_amchart_7" class="chart"></div>
		                </div>
						

						<!--portlet-body S
						<div class="portlet-body" style="position:relative; top:-10px; left:0px; height:268px;">
							<div class="row">
							<div class="dataTables_scroll" style="width:100%; clear:both;">
								<div class="dataTables_scrollBody" style="overflow-y: auto; overflow-x:hidden; height:268px; width: 100%;">
									<table style="border-top: 2px solid #459aef;" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer" role="grid">
										<thead>
											<tr role="row" class="heading"> 
												<th width="20%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; height:10px; font-size:12px;">
													시스템</th>
												<th width="15%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; height:10px; font-size:12px;">
													생성일시</th>
												<th width="10%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; height:10px; font-size:12px;">
													부서</th>
												<th width="10%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; height:10px; font-size:12px;">
													ID</th>
												<th width="15%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; height:10px; font-size:12px;">
													사용자명</th>
												<th width="30%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; height:10px; font-size:12px;">
													사용자IP</th>
											</tr>
										</thead>
										<tbody>
											<tr style="text-align: center;">
												<td width="20%" style="height:10px; font-size:12px">보조사업관리</td>
												<td width="15%" style="height:10px; font-size:12px">2017-02-15</td>
												<td width="10%" style="height:10px; font-size:12px">부속실</td>
												<td width="10%" style="height:10px; font-size:12px">ehchoi</td>
												<td width="15%" style="height:10px; font-size:12px">홍길동</td>
												<td width="30%" style="height:10px; font-size:12px">192.168.0.211</td>
											</tr>
										</tbody>
									</table>
									<div style="position: relative; top: 0px; left: 0px; width: 1px; height: 8366px;"></div>
								</div>
								<div class="dataTables_processing DTS_Loading" style="display: none;">Please wait ...</div>
							</div>
							</div>			
		                </div>
						<!--portlet-body E-->
		            </div>
		        </div>
				 <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">
		            <div class="portlet light bordered">
		                <div class="portlet-title" style="height:30px; min-height:10px; margin-top:-10px;">
		                    <div class="caption ">
		                    	<i class="icon-bar-chart font-green-haze2" style="font-size:15px"></i>
		                        <span class="caption-subject bold uppercase font-green-haze2" style="font-size:15px">주민등록번호 처리현황</span>
		                        <!-- <span class="caption-helper">distance stats...</span> -->
		                    </div>
		                   
		                </div>
		                <div class="portlet-body" style="position:relative; top:-8px; left:0px; height:268px">
		                    <div id="dashboard_amchart_10" class="chart"></div>
		                </div>
		            </div>
		        </div>
		        <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">
		            <div class="portlet light bordered">
		                <div class="portlet-title" style="height:30px; min-height:10px; margin-top:-10px;">
		                    <div class="caption ">
		                    	<i class="icon-bar-chart font-green-haze2" style="font-size:15px"></i>
		                        <span class="caption-subject bold uppercase font-green-haze2" style="font-size:15px">전체 컨텐츠 로그 수집 상태</span>
		                        <!-- <span class="caption-helper">distance stats...</span> -->
		                    </div>
		                    
		                </div>
		                <div class="portlet-body" style="position:relative; top:-8px; left:0px;">
		                    <div id="dashboard_amchart_16" class="chart"></div>
		                </div>
		            </div>
		        </div>
				<div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">
		            <div class="portlet light bordered">
		                <div class="portlet-title" style="height:30px; min-height:10px; margin-top:-10px;">
		                    <div class="caption">
		                    	<i class="icon-bar-chart font-green-haze2" style="font-size:15px"></i>
		                        <span class="caption-subject bold uppercase font-green-haze2" style="font-size:15px">6개월 개인정보 처리 현황</span>
		                        <!-- <span class="caption-helper font-dark">distance stats...</span> -->
		                    </div>
		                    
		                </div>
		                <div class="portlet-body" style="position:relative; top:-8px; left:0px;">
							<div id="dashboard_amchart_13" class="chart" style="overflow: visible; text-align: left;">
							<!--
							<div class="amcharts-main-div" style="position: relative; width: 100%; height: 100%;"><div class="amcharts-chart-div" style="overflow: hidden; position: relative; text-align: left; width: 851px; height: 252px; padding: 0px; cursor: default;"><svg version="1.1" style="position: absolute; width: 546px; height: 252px; top: 0px; left: 0px;"><desc>JavaScript chart by amCharts 3.20.20</desc><g><path cs="100,100" d="M0.5,0.5 L850.5,0.5 L850.5,251.5 L0.5,251.5 Z" fill="#FFFFFF" stroke="#000000" fill-opacity="0" stroke-width="1" stroke-opacity="0" class="amcharts-bg"></path><path cs="100,100" d="M0.5,0.5 L773.5,0.5 L773.5,199.5 L0.5,199.5 L0.5,0.5 Z" fill="#FFFFFF" stroke="#000000" fill-opacity="0" stroke-width="1" stroke-opacity="0" class="amcharts-plot-area" transform="translate(57,32)"></path></g><g><g class="amcharts-category-axis" transform="translate(57,32)"><g><rect x="0.5" y="0.5" width="70" height="199" rx="0" ry="0" stroke-width="0" fill="#000000" stroke="#000000" fill-opacity="0.05" stroke-opacity="0.05" transform="translate(0,0)" class="amcharts-axis-fill"></rect></g><g><rect x="0.5" y="0.5" width="70" height="199" rx="0" ry="0" stroke-width="0" fill="#000000" stroke="#000000" fill-opacity="0.05" stroke-opacity="0.05" transform="translate(141,0)" class="amcharts-axis-fill"></rect></g><g><rect x="0.5" y="0.5" width="70" height="199" rx="0" ry="0" stroke-width="0" fill="#000000" stroke="#000000" fill-opacity="0.05" stroke-opacity="0.05" transform="translate(281,0)" class="amcharts-axis-fill"></rect></g><g><rect x="0.5" y="0.5" width="70" height="199" rx="0" ry="0" stroke-width="0" fill="#000000" stroke="#000000" fill-opacity="0.05" stroke-opacity="0.05" transform="translate(422,0)" class="amcharts-axis-fill"></rect></g><g><rect x="0.5" y="0.5" width="71" height="199" rx="0" ry="0" stroke-width="0" fill="#000000" stroke="#000000" fill-opacity="0.05" stroke-opacity="0.05" transform="translate(562,0)" class="amcharts-axis-fill"></rect></g><g><rect x="0.5" y="0.5" width="70" height="199" rx="0" ry="0" stroke-width="0" fill="#000000" stroke="#000000" fill-opacity="0.05" stroke-opacity="0.05" transform="translate(703,0)" class="amcharts-axis-fill"></rect></g></g><g class="amcharts-value-axis value-axis-valueAxisAuto0_1488455767339" transform="translate(57,32)" visibility="visible"><g><path cs="100,100" d="M0.5,0.5 L0.5,0.5 L773.5,0.5" fill="none" stroke-width="1" stroke-dasharray="5" stroke-opacity="0.1" stroke="#000000" class="amcharts-axis-grid"></path></g><g><path cs="100,100" d="M0.5,40.5 L0.5,40.5 L773.5,40.5" fill="none" stroke-width="1" stroke-dasharray="5" stroke-opacity="0.1" stroke="#000000" class="amcharts-axis-grid"></path></g><g><path cs="100,100" d="M0.5,80.5 L0.5,80.5 L773.5,80.5" fill="none" stroke-width="1" stroke-dasharray="5" stroke-opacity="0.1" stroke="#000000" class="amcharts-axis-grid"></path></g><g><path cs="100,100" d="M0.5,119.5 L0.5,119.5 L773.5,119.5" fill="none" stroke-width="1" stroke-dasharray="5" stroke-opacity="0.1" stroke="#000000" class="amcharts-axis-grid"></path></g><g><path cs="100,100" d="M0.5,159.5 L0.5,159.5 L773.5,159.5" fill="none" stroke-width="1" stroke-dasharray="5" stroke-opacity="0.1" stroke="#000000" class="amcharts-axis-grid"></path></g><g><path cs="100,100" d="M0.5,199.5 L0.5,199.5 L773.5,199.5" fill="none" stroke-width="1" stroke-dasharray="5" stroke-opacity="0.1" stroke="#000000" class="amcharts-axis-grid"></path></g></g></g><g transform="translate(57,32)" clip-path="url(#AmChartsEl-40)"><g visibility="hidden"></g></g><g></g><g></g><g></g><g><g transform="translate(57,32)" class="amcharts-graph-line amcharts-graph-graphAuto0_1488455767340"><g></g></g><g transform="translate(57,32)" class="amcharts-graph-line amcharts-graph-graphAuto1_1488455767340" opacity="1" visibility="visible"><g></g><g clip-path="url(#AmChartsEl-42)"><path cs="100,100" d="M35.5,159.7 L105.5,40.3 L176.5,80.1 L246.5,119.9 L316.5,0.5 L386.5,40.3 L457.5,40.3 L527.5,0.5 L597.5,159.7 L668.5,80.1 L738.5,40.3 M0,0 L0,0" fill="none" stroke-width="1" stroke-opacity="0.9" stroke="#fdd400" stroke-linejoin="round" class="amcharts-graph-stroke"></path></g><clippath id="AmChartsEl-42"><rect x="0" y="0" width="773" height="199" rx="0" ry="0" stroke-width="0"></rect></clippath><g></g></g><g transform="translate(57,32)" class="amcharts-graph-line amcharts-graph-graphAuto2_1488455767340" opacity="1" visibility="visible"><g></g><g clip-path="url(#AmChartsEl-43)"><path cs="100,100" d="M35.5,80.1 L105.5,199.5 L176.5,0.5 L246.5,0.5 L316.5,40.3 L386.5,0.5 L457.5,80.1 L527.5,159.7 L597.5,40.3 L668.5,199.5 L738.5,119.9 M0,0 L0,0" fill="none" stroke-width="1" stroke-opacity="0.9" stroke="#84b761" stroke-linejoin="round" class="amcharts-graph-stroke"></path></g><clippath id="AmChartsEl-43"><rect x="0" y="0" width="773" height="199" rx="0" ry="0" stroke-width="0"></rect></clippath><g></g></g></g><g></g><g><g class="amcharts-category-axis"><path cs="100,100" d="M0.5,0.5 L773.5,0.5" fill="none" stroke-width="1" stroke-opacity="0" stroke="#000000" transform="translate(57,32)" class="amcharts-axis-line"></path></g><g class="amcharts-value-axis value-axis-valueAxisAuto0_1488455767339"><path cs="100,100" d="M0.5,0.5 L0.5,199.5" fill="none" stroke-width="1" stroke-opacity="0" stroke="#000000" transform="translate(57,32)" class="amcharts-axis-line" visibility="visible"></path></g></g><g><g transform="translate(57,32)" clip-path="url(#AmChartsEl-41)" style="pointer-events: none;"><path cs="100,100" d="M0.5,0.5 L0.5,0.5 L0.5,199.5" fill="none" stroke-width="1" stroke-opacity="0" stroke="#000000" class="amcharts-cursor-line amcharts-cursor-line-vertical" visibility="hidden" transform="translate(386,0)"></path><path cs="100,100" d="M0.5,0.5 L773.5,0.5 L773.5,0.5" fill="none" stroke-width="1" stroke="#000000" class="amcharts-cursor-line amcharts-cursor-line-horizontal" visibility="hidden" transform="translate(0,26)"></path></g><clippath id="AmChartsEl-41"><rect x="0" y="0" width="773" height="199" rx="0" ry="0" stroke-width="0"></rect></clippath></g><g></g><g><g transform="translate(57,32)" class="amcharts-graph-line amcharts-graph-graphAuto0_1488455767340"></g><g transform="translate(57,32)" class="amcharts-graph-line amcharts-graph-graphAuto1_1488455767340" opacity="1" visibility="visible"><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(35,159)" aria-label="Germany 1930 5" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(105,40)" aria-label="Germany 1934 2" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(176,80)" aria-label="Germany 1938 3" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(246,119) scale(1)" aria-label="Germany 1950 4" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(316,0) scale(1)" aria-label="Germany 1954 1" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(386,40) scale(1)" aria-label="Germany 1958 2" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(457,40) scale(1)" aria-label="Germany 1962 2" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(527,0) scale(1)" aria-label="Germany 1966 1" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(597,159) scale(1)" aria-label="Germany 1970 5" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(668,80) scale(1)" aria-label="Germany 1974 3" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#fdd400" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(738,40) scale(1)" aria-label="Germany 1978 2" class="amcharts-graph-bullet"></circle></g><g transform="translate(57,32)" class="amcharts-graph-line amcharts-graph-graphAuto2_1488455767340" opacity="1" visibility="visible"><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(35,80)" aria-label="United Kingdom 1930 3" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(105,199)" aria-label="United Kingdom 1934 6" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(176,0)" aria-label="United Kingdom 1938 1" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(246,0) scale(1)" aria-label="United Kingdom 1950 1" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(316,40) scale(1)" aria-label="United Kingdom 1954 2" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(386,0) scale(1)" aria-label="United Kingdom 1958 1" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(457,80) scale(1)" aria-label="United Kingdom 1962 3" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(527,159) scale(1)" aria-label="United Kingdom 1966 5" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(597,40) scale(1)" aria-label="United Kingdom 1970 2" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(668,199) scale(1)" aria-label="United Kingdom 1974 6" class="amcharts-graph-bullet"></circle><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#84b761" fill-opacity="1" stroke-width="2" stroke-opacity="0" transform="translate(738,119) scale(1)" aria-label="United Kingdom 1978 4" class="amcharts-graph-bullet"></circle></g></g><g><g></g></g><g><g class="amcharts-category-axis" transform="translate(57,32)" visibility="visible"><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(35.13636363636363,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1930</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(105.13636363636363,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1934</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(176.13636363636363,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1938</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(246.13636363636363,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1950</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(316.1363636363636,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1954</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(386.1363636363636,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1958</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(457.1363636363636,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1962</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(527.1363636363636,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1966</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(597.1363636363636,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1970</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(668.1363636363636,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1974</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(738.1363636363636,-17.5)" class="amcharts-axis-label"><tspan y="6" x="0">1978</tspan></text></g><g class="amcharts-value-axis value-axis-valueAxisAuto0_1488455767339" transform="translate(57,32)" visibility="visible"><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" transform="translate(-12,-1)" class="amcharts-axis-label"><tspan y="6" x="0">1</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" transform="translate(-12,39)" class="amcharts-axis-label"><tspan y="6" x="0">2</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" transform="translate(-12,79)" class="amcharts-axis-label"><tspan y="6" x="0">3</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" transform="translate(-12,118)" class="amcharts-axis-label"><tspan y="6" x="0">4</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" transform="translate(-12,158)" class="amcharts-axis-label"><tspan y="6" x="0">5</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" transform="translate(-12,198)" class="amcharts-axis-label"><tspan y="6" x="0">6</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="12px" opacity="1" font-weight="bold" text-anchor="middle" class="amcharts-axis-title" transform="translate(-36,100) rotate(-90)"><tspan y="6" x="0">Place taken</tspan></text></g></g><g></g><g transform="translate(57,32)"></g><g></g><g></g><clippath id="AmChartsEl-40"><rect x="-1" y="-1" width="775" height="201" rx="0" ry="0" stroke-width="0"></rect></clippath></svg><a href="http://www.amcharts.com/javascript-charts/" title="JavaScript charts" style="position: absolute; text-decoration: none; color: rgb(0, 0, 0); font-family: Verdana; font-size: 11px; opacity: 0.7; display: block; left: 62px; top: 37px;"></a></div><div class="amChartsLegend amcharts-legend-div" style="overflow: hidden; position: relative; text-align: left; width: 851px; height: 48px; cursor: default;"><svg version="1.1" style="position: absolute; width: 851px; height: 48px;"><desc>JavaScript chart by amCharts 3.20.20</desc><g transform="translate(57,0)"><path cs="100,100" d="M0.5,0.5 L773.5,0.5 L773.5,37.5 L0.5,37.5 Z" fill="#FFFFFF" stroke="#000000" fill-opacity="0" stroke-width="1" stroke-opacity="0" class="amcharts-legend-bg"></path><g transform="translate(0,11)"><g cursor="pointer" class="amcharts-legend-item-graphAuto0_1488455767340" aria-label="Italy" transform="translate(0,0)"><g class="amcharts-graph-line amcharts-graph-graphAuto0_1488455767340 amcharts-legend-marker"><path cs="100,100" d="M0.5,8.5 L32.5,8.5" fill="none" stroke-width="1" stroke-opacity="0.9" stroke="#AAB3B3" class="amcharts-graph-stroke"></path><circle r="4" cx="0" cy="0" fill="#AAB3B3" stroke="#AAB3B3" fill-opacity="1" stroke-width="2" stroke-opacity="0" class="amcharts-graph-bullet" transform="translate(17,8)"></circle></g><text y="6" fill="#AAB3B3" font-family="Verdana" font-size="11px" opacity="1" text-anchor="start" class="amcharts-legend-label" transform="translate(37,7)"><tspan y="6" x="0">Italy</tspan></text><text y="6" fill="#AAB3B3" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" class="amcharts-legend-value" transform="translate(182,7)"> </text><rect x="32" y="0" width="150" height="18" rx="0" ry="0" stroke-width="0" stroke="none" fill="#fff" fill-opacity="0.005"></rect></g><g cursor="pointer" class="amcharts-legend-item-graphAuto1_1488455767340" aria-label="Germany" transform="translate(197,0)"><g class="amcharts-graph-line amcharts-graph-graphAuto1_1488455767340 amcharts-legend-marker"><path cs="100,100" d="M0.5,8.5 L32.5,8.5" fill="none" stroke-width="1" stroke-opacity="0.9" stroke="#fdd400" class="amcharts-graph-stroke"></path><circle r="4" cx="0" cy="0" fill="#fdd400" stroke="#000000" fill-opacity="1" stroke-width="2" stroke-opacity="0" class="amcharts-graph-bullet" transform="translate(17,8)"></circle></g><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="start" class="amcharts-legend-label" transform="translate(37,7)"><tspan y="6" x="0">Germany</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" class="amcharts-legend-value" transform="translate(182,7)"> </text><rect x="32" y="0" width="150" height="18" rx="0" ry="0" stroke-width="0" stroke="none" fill="#fff" fill-opacity="0.005"></rect></g><g cursor="pointer" class="amcharts-legend-item-graphAuto2_1488455767340" aria-label="United Kingdom" transform="translate(393,0)"><g class="amcharts-graph-line amcharts-graph-graphAuto2_1488455767340 amcharts-legend-marker"><path cs="100,100" d="M0.5,8.5 L32.5,8.5" fill="none" stroke-width="1" stroke-opacity="0.9" stroke="#84b761" class="amcharts-graph-stroke"></path><circle r="4" cx="0" cy="0" fill="#84b761" stroke="#000000" fill-opacity="1" stroke-width="2" stroke-opacity="0" class="amcharts-graph-bullet" transform="translate(17,8)"></circle></g><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="start" class="amcharts-legend-label" transform="translate(37,7)"><tspan y="6" x="0">United Kingdom</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" class="amcharts-legend-value" transform="translate(182,7)"> </text><rect x="32" y="0" width="150" height="18" rx="0" ry="0" stroke-width="0" stroke="none" fill="#fff" fill-opacity="0.005"></rect></g></g></g></svg></div><div class="amcharts-export-menu amcharts-export-menu-bottom-right amExportButton"><ul><li class="export-main"><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>menu.label.undefined</span></a><ul><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>Download as ...</span></a><ul><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>PNG</span></a></li><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>JPG</span></a></li><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>SVG</span></a></li><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>PDF</span></a></li></ul></li><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>Save as ...</span></a><ul><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>CSV</span></a></li><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>XLSX</span></a></li><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>JSON</span></a></li></ul></li><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>Annotate ...</span></a></li><li><a href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"><span>Print</span></a></li></ul></li></ul></div></div>
							-->
							</div>
		                </div>
		            </div>
		        </div>
				 <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">
		            <div class="portlet light bordered">
		                <div class="portlet-title" style="height:30px; min-height:10px; margin-top:-10px;">
		                    <div class="caption">
		                    	<i class="icon-bar-chart font-green-haze2" style="font-size:15px"></i>
		                        <span class="caption-subject bold uppercase font-green-haze2" style="font-size:15px">업무 유형별 비율</span>
		                        <!-- <span class="caption-helper">distance stats...</span> -->
		                    </div>
		                    
		                </div>


						
						<div class="portlet-body" style="position:relative; top:-8px; left:0px;">
		                    <div id="dashboard_amchart_8" class="chart"></div>
		                </div>
						

						<!--portlet-body S
						<div class="portlet-body" style="position:relative; top:-10px; left:0px; height:268px;">
							<div class="row">
							<div class="dataTables_scroll" style="width:100%; clear:both;">
								<div class="dataTables_scrollBody" style="overflow-y: auto; overflow-x:hidden; height:268px; width: 100%;">
									<table style="border-top: 2px solid #459aef;" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer" role="grid">
										<thead>
											<tr role="row" class="heading"> 
												<th width="20%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; height:10px; font-size:12px;">
													시스템</th>
												<th width="15%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; height:10px; font-size:12px;">
													생성일시</th>
												<th width="10%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; height:10px; font-size:12px;">
													부서</th>
												<th width="10%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; height:10px; font-size:12px;">
													ID</th>
												<th width="15%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; height:10px; font-size:12px;">
													사용자명</th>
												<th width="30%" style="border-bottom: 1px solid #e7ecf1; text-align: center; padding-left: 0px; padding-right: 0px; height:10px; font-size:12px;">
													사용자IP</th>
											</tr>
										</thead>
										<tbody>
											<tr style="text-align: center;">
												<td width="20%" style="height:10px; font-size:12px">보조사업관리</td>
												<td width="15%" style="height:10px; font-size:12px">2017-02-15</td>
												<td width="10%" style="height:10px; font-size:12px">부속실</td>
												<td width="10%" style="height:10px; font-size:12px">ehchoi</td>
												<td width="15%" style="height:10px; font-size:12px">홍길동</td>
												<td width="30%" style="height:10px; font-size:12px">192.168.0.211</td>
											</tr>
										</tbody>
									</table>
									<div style="position: relative; top: 0px; left: 0px; width: 1px; height: 8366px;"></div>
								</div>
								<div class="dataTables_processing DTS_Loading" style="display: none;">Please wait ...</div>
							</div>
							</div>			
		                </div>
						<!--portlet-body E-->
		            </div>
		        </div>
				 <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">
		            <div class="portlet light bordered">
		                <div class="portlet-title" style="height:30px; min-height:10px; margin-top:-10px;">
		                    <div class="caption ">
		                    	<i class="icon-bar-chart font-green-haze2" style="font-size:15px"></i>
		                        <span class="caption-subject bold uppercase font-green-haze2" style="font-size:15px">PSM 접속 현황</span>
		                        <!-- <span class="caption-helper">distance stats...</span> -->
		                    </div>
		                   
		                </div>
		                <div class="portlet-body" style="position:relative; top:-8px; left:0px; height:268px">
		                    <div id="dashboard_amchart_15" class="chart"></div>
		                </div>
		            </div>
		        </div>
				<div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">
		            <div class="portlet light bordered">
		                <div class="portlet-title" style="height:30px; min-height:10px; margin-top:-10px;">
		                    <div class="caption ">
		                    	<i class="icon-bar-chart font-green-haze2" style="font-size:15px"></i>
		                        <span class="caption-subject bold uppercase font-green-haze2" style="font-size:15px">HDD, MEMORY, CPU</span>
		                        <!-- <span class="caption-helper">distance stats...</span> -->
		                    </div>
		                    <!-- <div class="actions">
		                        <a href="#" class="btn btn-circle green btn-outline btn-sm">
		                            <i class="fa fa-pencil"></i> Export </a>
		                        <a href="#" class="btn btn-circle green btn-outline btn-sm">
		                            <i class="fa fa-print"></i> Print </a>
		                    </div> -->
		                </div>
		                <div class="portlet-body" style="position:relative; top:-8px; left:0px;">
		                    <div id="dashboard_amchart_19" class="chart"></div>
		                </div>
		            </div>
		        </div>
				 <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">
		            <div class="portlet light bordered">
		                <div class="portlet-title" style="height:30px; min-height:10px; margin-top:-10px;">
		                    <div class="caption ">
		                    	<i class="icon-bar-chart font-green-haze2" style="font-size:15px"></i>
		                        <span class="caption-subject bold uppercase font-green-haze2" style="font-size:15px">수집서버현황(설치일~현재)</span>
		                        <!-- <span class="caption-helper">distance stats...</span> -->
		                    </div>
		                    <!-- <div class="actions">
		                        <a href="#" class="btn btn-circle green btn-outline btn-sm">
		                            <i class="fa fa-pencil"></i> Export </a>
		                        <a href="#" class="btn btn-circle green btn-outline btn-sm">
		                            <i class="fa fa-print"></i> Print </a>
		                    </div> -->
		                </div>
		                <div class="portlet-body" style="position:relative; top:-8px; left:0px;">
		                    <div id="dashboard_amchart_20" class="chart"></div>
		                </div>
		            </div>
		        </div>
		       
		      
		        
		        
		       
		   
		    
		</div>	
	</div>
</div>

</div>

                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
                
                
                <!-- BEGIN QUICK SIDEBAR -->
                



</div>
            
            
			
			
			

			
			
<!-- BEGIN FOOTER
<div class="page-footer">
	<div class="page-footer-inner">
		Copyright ©2017 <a target="_blank" href="http://www.easycerti.com/" title="EASYCERTI"><SPAN style="color:#95cde7">EASYCERTI</SPAN></a>. All rights reserved.
	</div>
	<div class="scroll-to-top" style="display: none;">
		<i class="icon-arrow-up"></i>
	</div>
</div> 
<!-- END FOOTER -->

        

</div>
<!-- 대시보드 E-->
       
<div class="quick-nav-overlay"></div>
</body>
</html>