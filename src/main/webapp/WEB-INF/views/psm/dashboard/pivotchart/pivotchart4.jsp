<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />
<h1 class="page-title">${currentMenuName }</h1>

<div class="portlet light portlet-fit portlet-datatable bordered">
	<div class="portlet-body">
		<div class="table-container">
			<div id="datatable_ajax_2_wrapper" class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
				<div>
					<div class="col-md-6" style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
						<div class="portlet box grey-salsa">	
							<div class="portlet-title" style="background-color: #2B3643;">
								<div class="caption">
								<i class="fa fa-search"></i> 
									검색
								</div>
								<div class="tools">
	                            	<a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
					            </div>
							</div>
							<div class="portlet-body form">
                                <div class="form-body" style="padding-left:10px; padding-right:10px;">
                                    <div class="form-group">
                                    	<form id="listForm" method="POST" class="mt-repeater form-horizontal">
                                    		<div data-repeater-list="group-a">
                                               	<div class="row">
                                               		<div class="col-md-3">
                                               			<label class="control-label">기간</label>
                                                        <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
															<input type="text" class="form-control" id="search_fr" name="search_from" value="${paramBean.search_from}"> 
															<span class="input-group-addon"> &sim; </span> 
															<input type="text" class="form-control" id="search_to" name="search_to" value="${paramBean.search_to}">
														</div>
                                               		</div>
                                               		<div class="col-md-3">
                                               			<label class="control-label">시스템명</label>
												   		<br/>
													   	<select name="system_seq" id="system_seq" class="form-control">
															<option value="" ${paramBean.system_seq == '' ? 'selected="selected"' : ''}>
															----- 전 체 -----</option>
															<c:if test="${empty systemList}">
															<option>시스템 데이터 없습니다.</option>
															</c:if>
															<c:if test="${!empty systemList}">
															<c:forEach items="${systemList}" var="i" varStatus="z">
															<option value="${i.system_seq}" onclick="setColorIndex(${z.index })"
																${i.system_seq==paramBean.system_seq ? "selected=selected" : "" }>
																${ i.system_name}</option>
															</c:forEach>
															</c:if>
														</select>
                                               		</div>
															
														<div class="col-md-3">
															<label class="control-label">조회대상</label><br/>
															<select id="searchType" name="searchType" class="ticket-assign form-control">
																<option value="01">접속기록조회</option>
																<option value="02" <c:if test='${searchType eq "02"}'>selected="selected"</c:if>>시스템로그조회</option>
																<option value="03" <c:if test='${searchType eq "03"}'>selected="selected"</c:if>>다운로드 로그조회</option>
															</select>
														</div>
                                               	</div>
                                            </div>
                                            <hr/>
                                            <div align="right">
                                            	<button class="btn btn-sm blue btn-outline sbold uppercase" type="button" onclick="setchart4()">
													<i class="fa fa-search"></i> 검색
												</button>	
                                            </div>
                                            
                                            <input type="hidden" name="privacyType" />
                                            <input type="hidden" name="main_menu_id" />
                                    	</form>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
				</div>
				<div id="chart4" style="width: 100%; height: 500px;">
					
				</div>
			</div>
		</div>
	</div>
</div>

<style>
.amcharts-pie-slice {
  transform: scale(1);
  transform-origin: 50% 50%;
  transition-duration: 0.3s;
  transition: all .3s ease-out;
  -webkit-transition: all .3s ease-out;
  -moz-transition: all .3s ease-out;
  -o-transition: all .3s ease-out;
  cursor: pointer;
  box-shadow: 0 0 30px 0 #000;
}

.amcharts-pie-slice:hover {
  transform: scale(1.1);
  filter: url(#shadow);
}							
</style>

<script type="text/javascript">

window.onload = function() {
	setchart4();
}

function setchart4 () {
	var startdate = $('#search_fr').val();
	var enddate = $('#search_to').val();
	var system_seq = $('#system_seq').val();
	var searchType = $('#searchType').val();
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/new_dashboard_chart4.html',
		data: { 
			"startdate": startdate,
			"enddate": enddate,
			"system_seq" : system_seq,
			"searchType": searchType
		},
		success: function(data) {
			drawchart4(data);
		},
		beforeSend:function() {
			$('#loading').show();
		},
		complete:function() {
			$('#loading').hide();
		}
	});
}


function drawchart4 (data) {

	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"privacy_seq": jsonData[i].privacy_seq,"privacy_desc": jsonData[i].privacy_desc,"nPrivCount": jsonData[i].nPrivCount };
		dataProvider.push(value);
	}
	
	
	var chart4 = AmCharts.makeChart( "chart4", {
		  "type": "pie",
		  "theme": "light",
		  "dataProvider": dataProvider,
		  "titleField": "privacy_desc",
		  "valueField": "nPrivCount",
		  "descriptionField": "privacy_seq",
		  "labelRadius": 5,
		  "balloonText": "",
		  "legend":{
		   	"position":"right",
		    "marginRight":100,
		    "autoMargins":false,
		  },
		  "radius": "40%",
		  "innerRadius": "35%",
		  "labelText": "[[title]]: [[percents]]% ([[value]])",
		  "export": {
		    "enabled": false
		  }
		} );
	
	/* chart4.addListener("clickSlice", handleClick); */
}

function handleClick(event) {
	var startdate = $('#search_fr').val();
	var enddate = $('#search_to').val();
	
	$('input[name=search_from]').attr("value", startdate);
	$('input[name=search_to]').attr("value", enddate);
	$('input[name=privacyType]').attr("value", event.dataItem.description);
	$('input[name=main_menu_id]').val('MENU00040');
	
	$("#listForm").attr("action", "${rootPath}/allLogInq/list.html");
	$("#listForm").submit();
}
// setchart4();
</script>