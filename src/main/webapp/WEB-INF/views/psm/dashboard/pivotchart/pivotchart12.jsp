<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>

<c:set var="currentName" value="비정상 위험 유형별 현황"/>
<h1 class="page-title">${currentName }</h1>

<div class="portlet light portlet-fit portlet-datatable bordered">
	<div class="portlet-body">
		<div class="table-container">
			<div id="datatable_ajax_2_wrapper" class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
				<div>
					<div class="col-md-6" style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
						<div class="portlet box grey-salsa">	
							<div class="portlet-title" style="background-color: #2B3643;">
								<div class="caption">
								<i class="fa fa-search"></i> 
									검색
								</div>
							</div>
							<div class="portlet-body form">
                                <div class="form-body" style="padding-left:10px; padding-right:10px;">
                                    <div class="form-group">
                                    	<form id="listForm" method="POST" class="mt-repeater form-horizontal">
                                    		<div class="row">
                                            	<div class="col-md-3">
                                               		<label class="control-label">기간</label>
                                                    <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
														<input type="text" class="form-control" id="search_fr" name="search_from" value="${paramBean.search_from}"> 
														<span class="input-group-addon"> &sim; </span> 
														<input type="text" class="form-control" id="search_to" name="search_to" value="${paramBean.search_to}">
													</div>
                                               	</div>
                                               	<div class="col-md-3">
                                               		<label class="control-label">시스템명</label>
												   	<select name="system_seq" id="system_seq" class="form-control">
														<option value="" ${paramBean.system_seq == '' ? 'selected="selected"' : ''}>
														----- 전 체 -----</option>
														<c:if test="${empty systemList}">
														<option>시스템 데이터 없습니다.</option>
														</c:if>
														<c:if test="${!empty systemList}">
														<c:forEach items="${systemList}" var="i" varStatus="z">
														<option value="${i.system_seq}" onclick="setColorIndex(${z.index })"
															${i.system_seq==paramBean.system_seq ? "selected=selected" : "" }>
															${ i.system_name}</option>
														</c:forEach>
														</c:if>
													</select>
                                               	</div>
                                            </div>
                                            <hr/>
                                            <div align="right">
                                            	<button class="btn btn-sm blue btn-outline sbold uppercase" type="button" onclick="setchart12()">
													<i class="fa fa-search"></i> 검색
												</button>	
                                            </div>
                                    	</form>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
				</div>
				<div id="chart12" style="width: 100%; height: 500px;">
					
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
window.onload = function() {
	setchart12();
}
function setchart12 () {
	var startdate = $('#search_fr').val();
	var enddate = $('#search_to').val();
	var system_seq = $('#system_seq').val();
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/new_dashboard_chart12.html',
		data: { 
			"startdate" : startdate,
			"enddate" : enddate,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart12(data);
		},
		beforeSend:function() {
			$('#loading').show();
		},
		complete:function() {
			$('#loading').hide();
		}
	});
}

function drawchart12 (data) {
	
	var dataProvider = new Array();
	var color = ["#FF0F00", "#FF6600", "#FF9E01", "#FCD202", "#F8FF01"];
	
	var jsonData = JSON.parse(data);
	
	for (var i = 0; i < jsonData.length; i++) {
		var t = i%5;
		
		var value = {"scen_name": jsonData[i].scen_name,"nPrivCount": jsonData[i].nPrivCount,"color": color[t]};
		dataProvider.push(value);
	}
	
	/*dashboard_amchart_12*/
	var chart12 = AmCharts.makeChart("chart12", {
		  "type": "serial",
		  "theme": "light",
		  "marginRight": 70,
		  "dataProvider": dataProvider,
		  "valueAxes": [{
		    "axisAlpha": 0,
		    "position": "left",
		    "title": "추출건수"
		  }],
		  "startDuration": 1,
		  "graphs": [{
		    "balloonText": "[[category]]: [[value]]건",
		    "fillColorsField": "color",
		    "fillAlphas": 0.5,
		    "lineAlpha": 0.2,
		    "type": "column",
		    "valueField": "nPrivCount",
		    "labelText": "[[value]]"
		  }],
		  "chartCursor": {
		    "categoryBalloonEnabled": false,
		    "cursorAlpha": 0,
		    "zoomable": false
		  },
		  "categoryField": "scen_name",
		  "categoryAxis": {
		    "gridPosition": "start"
		    //,"labelRotation": 45
		  },
		  "export": {
		    "enabled": false
		  }

		});
}
</script>