<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />
<h1 class="page-title">${currentMenuName }</h1>

<div class="portlet light portlet-fit portlet-datatable bordered">
	<div class="portlet-body">
		<div class="table-container">
			<div id="datatable_ajax_2_wrapper" class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
				<div>
					<div class="col-md-6" style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
						<div class="portlet box grey-salsa">	
							<div class="portlet-title" style="background-color: #2B3643;">
								<div class="caption">
								<i class="fa fa-search"></i> 
									검색
								</div>
								<div class="tools">
	                            	<a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
					            </div>
							</div>
							<div class="portlet-body form">
                                <div class="form-body" style="padding-left:10px; padding-right:10px;">
                                    <div class="form-group">
                                    	<form id="listForm" method="POST" class="mt-repeater form-horizontal">
                                    		<div data-repeater-list="group-a">
                                               	<div class="row">
                                               		<div class="col-md-3">
                                               			<label class="control-label">날짜</label>
                                                        <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
															<input type="text" class="form-control" id="search_to" name="search_to" value="${today}">
														</div>
                                               		</div>
                                               		
                                               	<div class="col-md-3">
                                               			<label class="control-label">서버명</label>
												   		<br/>
													   	<select name="server_seq" id="server_seq" class="form-control input-medium">
													   		<c:forEach var="se" items="${serverList}">
															<option value="${se.server_seq }" ${paramBean.server_seq==se.server_seq ? "selected=selected" : "" }>${se.server_name }</option>
													   		</c:forEach>
														</select>
                                               		</div> 
                                               	</div>
                                            </div>
                                            <hr/>
                                            <div align="right">
                                            	<button class="btn btn-sm blue btn-outline sbold uppercase" type="button" onclick="setchart9()">
													<i class="fa fa-search"></i> 검색
												</button>	
                                            </div>
                                    	</form>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
				</div>
				<div id="chart9" style="width: 100%; height: 500px; cursor: pointer;">
					
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
window.onload = function() {
	setchart9();
}

function setchart9 () {
	var enddate = $('#search_to').val();
	var server_seq = $('#server_seq').val();
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/new_dashboard_chart9.html',
		data: { 
			"enddate": enddate,
			"server_seq": server_seq
		},
		success: function(data) {
			drawchart9(data);
		},
		beforeSend:function() {
			$('#loading').show();
		},
		complete:function() {
			$('#loading').hide();
		}
	});
}


function drawchart9 (data) {
	
	var dataProvider = new Array();
	var graphs = new Array();
	
	var jsonData = JSON.parse(data);
	
	var  valueTemp="";
	var  nameTemp="";
	
	for (var i = 0; i < jsonData.length-1; i++) {
		dataProvider.push(jsonData[i]);
		
	}
	
	if ( jsonData.length > 0) {
		var graphsTemp = jsonData[jsonData.length-1];
		
		for (var j = 0; j < graphsTemp.length; j++) {
			graphs.push(graphsTemp[j]);
		}
	}
	
	
	/*dashboard_amchart_9*/
	var chart9 = AmCharts.makeChart("chart9", {
		"type": "serial",
		"theme": "light",
	    "titles": [{
	        "text": "",
	        "size": 15
	    }],
	    "legend": {
	        "align": "center",
	        "equalWidths": false,
	        "periodValueText": "total: [[value.sum]]",
	        "valueAlign": "left",
	        "valueText": "[[value]] ([[percents]]%)",
	        "valueWidth": 100
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "stackType": "100%",
	        "gridAlpha": 0.07,
	        "position": "left",
	        "title": "percent"
	    }],
	    "graphs": graphs,
	    "plotAreaBorderAlpha": 0,
	    "marginLeft": 0,
	    "marginBottom": 0,
	    "chartCursor": {
	        "cursorAlpha": 0,
	        "zoomable": false
	    },
	    "categoryField": "year",
	    "categoryAxis": {
	        "startOnAxis": true,
	        "axisColor": "#DADADA",
	        "gridAlpha": 0.07
	    },
	    "export": {
	    	"enabled": false
	     }
	});

	console.log("a");
}
//setchart9();
</script>