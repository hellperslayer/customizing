<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />
<h1 class="page-title">${currentMenuName }</h1>

<div class="portlet light portlet-fit portlet-datatable bordered">
	<div class="portlet-body">
		<div class="table-container">
			<div id="datatable_ajax_2_wrapper" class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
				<div>
					<div class="col-md-6" style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
						<div class="portlet box grey-salsa">	
							<div class="portlet-title" style="background-color: #2B3643;">
								<div class="caption">
								<i class="fa fa-search"></i> 
									검색
								</div>
								<div class="tools">
	                            	<a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
					            </div>
							</div>
							<div class="portlet-body form">
                                <div class="form-body" style="padding-left:10px; padding-right:10px;">
                                    <div class="form-group">
                                    	<form id="listForm" method="POST" class="mt-repeater form-horizontal">
                                    		<div data-repeater-list="group-a">
                                               	<div class="row">
                                               		<div class="col-md-3">
                                               			<label class="control-label">기간</label>
                                                        <br/>
                                                        <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
															<input type="text" class="form-control" id="search_fr" name="search_from" value="${paramBean.search_from}"> 
															<span class="input-group-addon"> &sim; </span> 
															<input type="text" class="form-control" id="search_to" name="search_to" value="${paramBean.search_to}">
														</div>
                                               		</div>
                                               		<div class="col-md-3">
                                               			<label class="control-label">시스템명</label>
												   		<br/>
													   	<select name="system_seq" id="system_seq" class="form-control">
															<option value="" ${paramBean.system_seq == '' ? 'selected="selected"' : ''}>
															----- 전 체 -----</option>
															<c:if test="${empty systemList}">
															<option>시스템 데이터 없습니다.</option>
															</c:if>
															<c:if test="${!empty systemList}">
															<c:forEach items="${systemList}" var="i" varStatus="z">
															<option value="${i.system_seq}" onclick="setColorIndex(${z.index })"
																${i.system_seq==paramBean.system_seq ? "selected=selected" : "" }>
																${ i.system_name}</option>
															</c:forEach>
															</c:if>
														</select>
                                               		</div>
														<div class="col-md-3">
															<label class="control-label">조회대상</label><br/>
															<select id="searchType" name="searchType" class="ticket-assign form-control">
																<option value="01">접속기록조회</option>
																<option value="02" <c:if test='${search.searchType eq "02"}'>selected="selected"</c:if>>시스템로그조회</option>
																<option value="03" <c:if test='${search.searchType eq "03"}'>selected="selected"</c:if>>다운로드 로그조회</option>
															</select>
														</div>
													<div class="col-md-3">
														<label class="control-label">처리량/이용량</label><br/>
														<select id="logCtType" name="logCtType" class="ticket-assign form-control">
															<option value="logCt" <c:if test='${search.logCtType eq "logCt"}'>selected="selected"</c:if>>처리량</option>
															<option value="totalCt" <c:if test='${search.logCtType eq "totalCt"}'>selected="selected"</c:if>>이용량</option>
														</select>
													</div>
                                               	</div>
                                            </div>
                                            <hr/>
                                            <div align="right">
                                            	<button class="btn btn-sm blue btn-outline sbold uppercase" type="button" onclick="chart2Search()">
													<i class="fa fa-search"></i> 검색
												</button>	
                                            </div>
                                            
                                            <input type="hidden" name="emp_user_id" />
                                            <input type="hidden" name="main_menu_id" />
                                            <input type="hidden" name="page_num" id="page_num" value="${search.page_num }"/>
                                    	</form>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
				</div>
				<div id="chart2" style="width: 100%; height: 500px;">
					
				</div>
				<c:if test="${search.total_count > 0}">
						<div class="page left" id="pagingframe" align="center">
							<p>
								<ctl:paginator currentPage="${search.page_num}"
									rowBlockCount="${search.size}"
									totalRowCount="${search.total_count}" />
							</p>
						</div>
					</c:if>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

window.onload = function() {
	setchart2();
}

function setchart2 () {
	var search_from = $('#search_fr').val();
	console.log(search_from);
	var search_to = $('#search_to').val();
	console.log(search_to);
	var system_seq = $('#system_seq').val();
	console.log(system_seq);
	var searchType = $('#searchType').val();
	console.log(searchType);
	var logCtType = $('#logCtType').val();
	console.log(logCtType);
	var page_num = $('#page_num').val();
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/new_dashboard_chart2.html',
		data: { 
			"search_from": search_from,
			"search_to": search_to,
			"system_seq" : system_seq,
			"searchType": searchType,
			"logCtType" : logCtType,
			"page_num" : page_num
		},
		success: function(data) {
			drawchart2(data);
		},
		beforeSend:function() {
			$('#loading').show();
		},
		complete:function() {
			$('#loading').hide();
		}
	});
}
function drawchart2 (data) {
	
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"emp_user_name": jsonData[i].emp_user_name,"emp_user_id": jsonData[i].emp_user_id,"nPrivCount": jsonData[i].nPrivCount };
		dataProvider.push(value);
	}
	
	var chart2 = AmCharts.makeChart("chart2", {
		"type": "serial",
	     "theme": "light",
		"categoryField": "emp_user_name",
		"rotate": true,
		"startDuration": 1,
		"categoryAxis": {
			"gridPosition": "start",
			"position": "left"
		},
		"trendLines": [],
		"graphs": [ 
			{
				"balloonText": "[[category]]: [[value]]건",
				"fillAlphas": 0.6,
				"id": "AmGraph-1",
				"lineAlpha": 0.2,
				"title": "개인정보검출(건)",
				"type": "column",
				"valueField": "nPrivCount",
				"labelText": "[[value]]",
				"showHandOnHover" : true
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"position": "top",
				"axisAlpha": 0
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider": dataProvider,
	    "export": {
	    	"enabled": false
	     }

	});
			
	
	
	/* chart2.addListener("clickGraphItem", handleClick); */

}

function handleClick(event) {
	
	var startdate = $('#search_fr').val();
	var enddate = $('#search_to').val();
	
	$('input[name=search_from]').attr("value", startdate);
	$('input[name=search_to]').attr("value", enddate);
	$('input[name=emp_user_id]').attr("value", event.item.description);
	$('input[name=main_menu_id]').val('MENU00040');
	$("#listForm").attr("action", "${rootPath}/allLogInq/list.html");
	$("#listForm").submit();
}
//setchart2();


function goPage(num){
	var url = rootPath + "/pivotchart/pivotchart.html?type=2";
	$("#listForm").attr("action",url);
	$("#listForm input[name=page_num]").val(num);
	$("#listForm").submit();
}

function chart2Search() {
	var url = rootPath + '/pivotchart/pivotchart.html?type=2';
	$("input[name=page_num]").val(1);
	$("#listForm").attr("action", url);
	$("#listForm").submit();
}


</script>