<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>

<c:set var="currentName" value="개인정보 처리량 예측"/>
<h1 class="page-title">${currentName }</h1>

<div class="portlet light portlet-fit portlet-datatable bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-settings font-dark"></i> <span
				class="caption-subject font-dark sbold uppercase">${currentName} 차트</span>
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-container">
			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
				<div class="dataTables_scroll" style="position: relative;">
					<div>
						<div class="col-md-6"
							style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
							<div class="portlet box grey-salsa">
								<div class="portlet-title" style="background-color: #32c5d2;">
									<div class="caption">
									<i class="fa fa-gift"></i> 
										검색
									</div>
									<div class="tools">
										<a href="javascript:;" class="expand"></a>
									</div>
								</div>


								<!-- portlet-body S -->
								<form id="listForm" method="POST" class="portlet-body" style="display:none;">
									<div class="portlet-body">
										<div class="table-container">
											<div id="datatable_ajax_2_wrapper"
												class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
												<div class="row" style="padding-bottom: 5px;">
												<div class="col-md-4">
													<div class="col-md-3">◎ 기간:</div>
													<div class="col-md-9" style="padding: 0px;">
														<span class="date date-picker margin-bottom-5 input-small"
															data-date-format="yyyy-mm-dd"> <input type="text"
															id="search_fr" style="width: 80px;" class="input_date"
															readonly="" name="search_from" placeholder="From"
															value="${paramBean.search_from}"> <span
															class="">
																<button class="btn btn-sm default" type="button">
																	<i class="fa fa-calendar"></i>
																</button>
														</span>
														</span> <i class="sim">&sim;</i> <span
															class="date date-picker input-small"
															data-date-format="yyyy-mm-dd"> <input type="text"
															id="search_to" style="width: 80px;" class="input_date"
															readonly="" name="search_to" placeholder="To"
															value="${paramBean.search_to}"> <span
															class="">
																<button class="btn btn-sm default" type="button">
																	<i class="fa fa-calendar"></i>
																</button>
														</span>
														</span>
													</div>
												</div>
												
												<div class="col-md-8" align="right">
													<button class="btn btn-sm green table-group-action-submit"
														onclick="moveallLogInqList()">
														<i class="fa fa-check"></i> 검색
													</button>
												</div>
												</div>
												
											</div>
											<!-- portlet-boyd E -->

										</div>
									</div>
								</form>
								<!-- portlet-boyd E -->
							</div>
						</div>
					</div>
					<div id="chart13" style="width: 100%; height: 500px;">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
function setchart13 () {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/new_dashboard_chart13.html',
		data: { 
			"startdate" : startdate,
			"enddate" : enddate
		},
		success: function(data) {
			drawchart13(data);
		}
	});
}

function drawchart13 (data) {
	var dataProvider = new Array();
		
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"proc_date": jsonData[i].proc_date,"nPrivCount": jsonData[i].nPrivCount};
		dataProvider.push(value);
	}
	
	
	/*dashboard_amchart_5*/
	var chart5 = AmCharts.makeChart("chart13", {
	    "type": "serial",
	    "theme": "light",
	    "marginRight":10,
	    "legend": {
	        "equalWidths": false,
	        "periodValueText": "",
	        "position": "right",
	        "valueAlign": "left",
	        "valueWidth": 50
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "stackType": "regular",
	        "gridAlpha": 0.2,
	        "position": "left",
	        "title": "개인정보검출(건)"
	    }],
	    "graphs": [{
	    	"balloonText": "<b>[[value]]</b></span>",
	    	"fillAlphas": 0.6,
	        "lineAlpha": 0.2,
	        "title": "개인정보검출",
	        "valueField": "nPrivCount",
	        "labelText": "[[value]]"
	    }],
	    "plotAreaBorderAlpha": 0,
	    "marginTop": 10,
	    "marginLeft": 0,
	    "marginBottom": 0,
	    "chartScrollbar": {},
	    "chartCursor": {
	        "cursorAlpha": 0
	    },
	    "categoryField": "proc_date",
	    "categoryAxis": {
	        "startOnAxis": true,
	        "axisColor": "#DADADA",
	        "gridAlpha": 0.07,
	        "title": "시간",
	        "guides": [{
	            category: "2001",
	            toCategory: "2003",
	            lineColor: "#CC0000",
	            lineAlpha: 1,
	            fillAlpha: 0.2,
	            fillColor: "#CC0000",
	            dashLength: 2,
	            inside: true,
	            labelRotation: 90,
	            label: "fines for speeding increased"
	        }, {
	            category: "2007",
	            lineColor: "#CC0000",
	            lineAlpha: 1,
	            dashLength: 2,
	            inside: true,
	            labelRotation: 90,
	            label: "motorcycle fee introduced"
	        }]
	    },
	    "export": {
	    	"enabled": false
	     }
	});
}
setchart13();
</script>