<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />
<c:set var="currentName" value="기간별 비정상위험분석 현황"/>
<h1 class="page-title">${currentName }</h1>

<div class="portlet light portlet-fit portlet-datatable bordered">
	<%-- <div class="portlet-title">
		<div class="caption">
			<i class="icon-settings font-dark"></i> <span
				class="caption-subject font-dark sbold uppercase">${currentName } 차트</span>
		</div>
	</div> --%>
	<div class="portlet-body">
		<div class="table-container">
			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
				<div class="dataTables_scroll" style="position: relative;">
					<div>
						<div class="col-md-6"
							style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
							<div class="portlet box grey-salsa">
								<div class="portlet-title" style="background-color: #2B3643;">
									<div class="caption">
									<i class="fa fa-search"></i> 
										검색
									</div>
									<div class="tools">
										<a href="javascript:;" class="collapse"></a>
									</div>
								</div>


								<!-- portlet-body S -->
								<div class="portlet-body form" >
                               <div class="form-body" style="padding-left:10px; padding-right:10px;">
                                   <div class="form-group">
                                       <form id="listForm" method="POST" class="mt-repeater form-horizontal">
                                           <div data-repeater-list="group-a">
                                               <div data-repeater-item class="mt-repeater-item">
                                                   <!-- jQuery Repeater Container -->
                                                   <div class="mt-repeater-input" style="padding-left:20px;">
                                                       <label class="control-label">일시</label>
                                                       <br/>
                                                       <div
															class="input-group input-medium date-picker input-daterange"
															data-date="10/11/2012" data-date-format="yyyy-mm-dd">
															<input type="text" class="form-control" id="search_fr"
																name="search_from"
																value="${paramBean.search_from}"> <span
																class="input-group-addon"> &sim; </span> <input type="text"
																class="form-control" id="search_to" name="search_to"
																value="${paramBean.search_to}">
														</div>
													</div>
                                                   
													<div class="mt-repeater-input">
                                                       <label class="control-label">사용자ID</label>
                                                       <br/>
                                                       <input type="text" class="form-control input-medium" name="emp_user_id" id="emp_user_id" value="${paramBean.emp_user_id}" />
                                                   </div>
                                                   <div class="mt-repeater-input">
                                                       <label class="control-label">사용자명</label>
                                                       <br/>
                                                       <input type="text" class="form-control input-medium" name="emp_user_name" id="emp_user_name" value="${paramBean.emp_user_name}" />
                                                   </div>
                                               </div>
                                           </div>
                                           <div align="right">
                                            <a class="btn btn-sm blue btn-outline sbold uppercase" onclick="setchart14()"><i class="fa fa-check"></i> 검색</a>
										</div>
                                       </form>
                                   </div>
                               </div>
                           </div>
								<!-- portlet-boyd E -->
							</div>
						</div>
					</div>
					<div id="chart14" style="width: 100%; height: 600px;">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
var url = "${rootPath}/pivotchart/pivotchart.html?type=14";

function setchart14 () {
	var startdate = $('#search_fr').val();
	var enddate = $('#search_to').val();
	var emp_user_id = $('#emp_user_id').val();
	var emp_user_name = $('#emp_user_name').val();
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/new_dashboard_chart14.html',
		data: { 
			"startdate" : startdate,
			"enddate" : enddate,
			"emp_user_id" : emp_user_id,
			"emp_user_name" : emp_user_name
		},
		success: function(data) {
			drawchart14(data);
		},
		beforeSend:function() {
			$('#loading').show();
		},
		complete:function() {
			$('#loading').hide();
		}
	});
}

function drawchart14 (data) {
	
	var dataProvider = new Array();
	var graphs = new Array();
	
	var jsonData = JSON.parse(data);
	
	var  valueTemp="";
	var  nameTemp="";
	
	for (var i = 0; i < jsonData.length; i++) {
		dataProvider.push(jsonData[i].dataProvider);
		
	}
	
	if ( jsonData.length > 0) {
		var graphsTemp = jsonData[0].graphs;
		
		for (var j = 0; j < graphsTemp.length; j++) {
			graphs.push(graphsTemp[j]);
		}
	}
	
	/*dashboard_amchart_1*/
	var chart1 = AmCharts.makeChart("chart14", {
	    "type": "serial",
	    "theme": "light",
	    "legend": {
	        "useGraphSettings": true,
	        "position": "top"
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "integersOnly": true,
	        "reversed": false,
	        "axisAlpha": 0,
	        "dashLength": 5,
	        "gridCount": 10,
	        "position": "left"
	        //,"title": "개인정보검출(건)"
	    }],
	    "startDuration": 0,
	    "sequencedAnimation": false,
	    "startEffect": "easeOutSine",
	    "graphs": graphs,
	    "chartCursor": {
	        "cursorAlpha": 0,
	        "zoomable": false
	    },
	    "categoryField": "occr_dt",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "fillAlpha": 0.05,
	        "fillColor": "#000000",
	        "gridAlpha": 0,
	        "position": "bottom"
	    },
	    "export": {
	    	"enabled": false,
	        "position": "bottom-right"
	     }
	});
}
//setchart14();
</script>