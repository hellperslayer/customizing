<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>

<c:set var="currentName" value="시스템별 현재 사용자 수"/>
<h1 class="page-title">${currentName }</h1>

<div class="portlet light portlet-fit portlet-datatable bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-settings font-dark"></i> <span
				class="caption-subject font-dark sbold uppercase">${currentName} 차트</span>
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-container">
			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
				<div class="dataTables_scroll" style="position: relative;">
					<div>
						<div class="col-md-6"
							style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
							<div class="portlet box grey-salsa">
								<div class="portlet-title" style="background-color: #32c5d2;">
									<div class="caption">
									<i class="fa fa-gift"></i> 
										검색
									</div>
									<div class="tools">
										<a href="javascript:;" class="expand"></a>
									</div>
								</div>


								<!-- portlet-body S -->
								<form id="listForm" method="POST" class="portlet-body" style="display:none;">
									<div class="portlet-body">
										<div class="table-container">
											<div id="datatable_ajax_2_wrapper"
												class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
												<div class="row" style="padding-bottom: 5px;">
												<div class="col-md-4">
													<div class="col-md-3">◎ 기간:</div>
													<div class="col-md-9" style="padding: 0px;">
														<span class="date date-picker margin-bottom-5 input-small"
															data-date-format="yyyy-mm-dd"> <input type="text"
															id="search_fr" style="width: 80px;" class="input_date"
															readonly="" name="search_from" placeholder="From"
															value="${paramBean.search_from}"> <span
															class="">
																<button class="btn btn-sm default" type="button">
																	<i class="fa fa-calendar"></i>
																</button>
														</span>
														</span> <i class="sim">&sim;</i> <span
															class="date date-picker input-small"
															data-date-format="yyyy-mm-dd"> <input type="text"
															id="search_to" style="width: 80px;" class="input_date"
															readonly="" name="search_to" placeholder="To"
															value="${paramBean.search_to}"> <span
															class="">
																<button class="btn btn-sm default" type="button">
																	<i class="fa fa-calendar"></i>
																</button>
														</span>
														</span>
													</div>
												</div>
												
												<div class="col-md-8" align="right">
													<button class="btn btn-sm green table-group-action-submit"
														onclick="moveallLogInqList()">
														<i class="fa fa-check"></i> 검색
													</button>
												</div>
												</div>
												
											</div>
											<!-- portlet-boyd E -->

										</div>
									</div>
								</form>
								<!-- portlet-boyd E -->
							</div>
						</div>
					</div>
					<div id="chart7" style="width: 100%; height: 500px;">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
function setchart7 () {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/new_dashboard_chart7.html',
		data: { 
			"startdate": startdate,
			"enddate": enddate
		},
		success: function(data) {
			drawchart7(data);
		}
	});
}

function drawchart7 (data) {
	
	var dataProvider = new Array();
	var graphs = new Array();
	
	var jsonData = JSON.parse(data);
	
	var  valueTemp="";
	var  nameTemp="";
	
	for (var i = 0; i < jsonData.length-1; i++) {
		dataProvider.push(jsonData[i]);
		
	}
	
	if ( jsonData.length > 0) {
		var graphsTemp = jsonData[jsonData.length-1];
		
		for (var j = 0; j < graphsTemp.length; j++) {
			graphs.push(graphsTemp[j]);
		}
	}
	
	/*dashboard_amchart_7*/
	var chart7 = AmCharts.makeChart("chart7", {
	    "type": "xy",
	    "theme": "light",
	    "marginRight": 80,
	    "startDuration": 1.5,
	    "trendLines": [],
	    "balloon": {
	        "adjustBorderColor": false,
	        "shadowAlpha": 0,
	        "fixedPosition":true
	    },
	    "graphs": graphs,
	    "valueAxes": [{
	        "id": "ValueAxis-1",
	        "axisAlpha": 0
	    }, {
	        "id": "ValueAxis-2",
	        "axisAlpha": 0,
	        "position": "bottom"
	    }],
	    "allLabels": [],
	    "titles": [],
	    "dataProvider": dataProvider,

	    "export": {
	        "enabled": false
	    },

	    "chartScrollbar": {
	        "offset": 15,
	        "scrollbarHeight": 5
	    },

	    "chartCursor":{
	       "pan":true,
	       "cursorAlpha":0,
	       "valueLineAlpha":0
	    }
	});
}
setchart7();
</script>