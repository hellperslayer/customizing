<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />
<h1 class="page-title">${currentMenuName }</h1>

<div class="portlet light portlet-fit portlet-datatable bordered">
	<div class="portlet-body">
		<div class="table-container">
			<div id="datatable_ajax_2_wrapper" class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
				<div>
					<div class="col-md-6" style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
						<div class="portlet box grey-salsa">	
							<div class="portlet-title" style="background-color: #2B3643;">
								<div class="caption">
								<i class="fa fa-search"></i> 
									검색
								</div>
								<div class="tools">
	                            	<a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
					            </div>
							</div>
							<div class="portlet-body form">
                                <div class="form-body" style="padding-left:10px; padding-right:10px;">
                                    <div class="form-group">
                                    	<form id="listForm" method="POST" class="mt-repeater form-horizontal">
                                    		<div data-repeater-list="group-a">
                                               	<div class="row">
                                               		<div class="col-md-3">
                                               			<label class="control-label">기간</label>
                                                        <br/>
                                                        <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
															<input type="text" class="form-control" id="search_fr" name="search_from" value="${paramBean.search_from}"> 
															<span class="input-group-addon"> &sim; </span> 
															<input type="text" class="form-control" id="search_to" name="search_to" value="${paramBean.search_to}">
														</div>
                                               		</div>
                                               		<div class="col-md-3">
                                               			<label class="control-label">시스템명</label>
												   		<br/>
													   	<select name="system_seq" id="system_seq" class="form-control">
															<option value="" ${paramBean.system_seq == '' ? 'selected="selected"' : ''}>
															----- 전 체 -----</option>
															<c:if test="${empty systemList}">
															<option>시스템 데이터 없습니다.</option>
															</c:if>
															<c:if test="${!empty systemList}">
															<c:forEach items="${systemList}" var="i" varStatus="z">
															<option value="${i.system_seq}" onclick="setColorIndex(${z.index })"
																${i.system_seq==paramBean.system_seq ? "selected=selected" : "" }>
																${ i.system_name}</option>
															</c:forEach>
															</c:if>
														</select>
                                               		</div>
														<div class="col-md-3">
															<label class="control-label">조회대상</label><br/>
															<select id="searchType" name="searchType" class="ticket-assign form-control">
																<option value="01">접속기록조회</option>
																<option value="02" <c:if test='${searchType eq "02"}'>selected="selected"</c:if>>시스템로그조회</option>
																<option value="03" <c:if test='${searchType eq "03"}'>selected="selected"</c:if>>다운로드 로그조회</option>
															</select>
														</div>
                                               	</div>
                                            </div>
                                            <hr/>
                                            <div align="right">
                                            	<button class="btn btn-sm blue btn-outline sbold uppercase" type="button" onclick="setchart5()">
													<i class="fa fa-search"></i> 검색
												</button>	
                                            </div>
                                            
                                            <input type="hidden" name="req_type" />
                                            <input type="hidden" name="main_menu_id" />
                                    	</form>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
				</div>
				<div id="chart5" style="width:900px; height: 500px; display: inline-block;">
				</div>
				<div id="chartdiv2" style="width:400px; height: 500px; display: inline-block;">
				</div>
			</div>
		</div>
	</div>
</div>

<style>
.amcharts-pie-slice {
  transform: scale(1);
  transform-origin: 50% 50%;
  transition-duration: 0.3s;
  transition: all .3s ease-out;
  -webkit-transition: all .3s ease-out;
  -moz-transition: all .3s ease-out;
  -o-transition: all .3s ease-out;
  cursor: pointer;
  box-shadow: 0 0 30px 0 #000;
}

.amcharts-pie-slice:hover {
  transform: scale(1.1);
  filter: url(#shadow);
}							
</style>

<script type="text/javascript">
window.onload = function() {
	setchart5();
}

function setchart5 () {
	var startdate = $('#search_fr').val();
	var enddate = $('#search_to').val();
	var system_seq = $('#system_seq').val();
	var searchType = $('#searchType').val();
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/new_dashboard_chart5.html',
		data: { 
			"startdate": startdate,
			"enddate": enddate,
			"system_seq" : system_seq,
			"searchType": searchType
		},
		success: function(data) {
			drawchart5(data);
		},
		beforeSend:function() {
			$('#loading').show();
		},
		complete:function() {
			$('#loading').hide();
		}
	});
}
function drawchart5 (data) {
	var jsonData = JSON.parse(data);
	var dataProvider = jsonData["dataProvider"];
	var graphs = jsonData["graphs"];
	var columnChart = AmCharts.makeChart("chart5", {
	    "type": "serial",
		"theme": "light",
		"legend": {
	        "position": "bottom",
			"useGraphSettings": true,
			"markerSize": 10,
			"switchable":true
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.3,
	        "gridAlpha": 0
	    }],
	    "graphs": graphs,
	    "rotate": true,
	    "categoryField": "category",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "gridAlpha": 0,
	        "position": "left"
	    },
	    "export": {
	    	"enabled": false
	     }
	});
	
	// add hover events to sync the charts
	columnChart.addListener("rollOverGraphItem", function(event) {
	  // format new data for the pie chart based on the current category being hovered
	  var data = event.item.dataContext;
	  pieChart.dataProvider = [];
	  console.log(event);
	  // create a slice for each graph in column chart
	  for (var x in columnChart.graphs) {
	    // let's distinguish particular slice by setting it's alpha higher for
	    // graph currently being hovered on
	    var alpha = 0.5;
	    if (event.item.graph.valueField == columnChart.graphs[x].valueField)
	      alpha = 1;
	    pieChart.dataProvider.push({
	      "label": columnChart.graphs[x].title,
	      "value": data[columnChart.graphs[x].valueField],
	      "alpha": alpha
	    });
	  }
	  // update title
	  pieChart.titles[0].text = data[columnChart.categoryField];
	  // update the pie chart
	  pieChart.validateData();
	});

	// add buler event to reset pie chart back to totals
	columnChart.addListener("rollOutGraphItem", function(event) {
	  // get totals data
	  pieChart.dataProvider = getPieChartTotals();
	  // update title
	  pieChart.titles[0].text = "전체 시스템";
	  // update the pie chart
	  pieChart.validateData();
	});
	// function that generates pie chart data out of the column data
	function getPieChartTotals() {
	  // calculate totals
	  var data = {};
	  for (x in columnChart.dataProvider) {
	    var row = columnChart.dataProvider[x];
	    for (y in row) {
	      if (0 == x) {
	        data[y] = row[y];
	      }
	      else {
	    	if(data[y]==null) {
	    		data[y] =0;
	    	}
	        data[y] += row[y];
	      }
	    }
	  }
	  // now build the pie data
	  var dataProvider = [];
	  for (var x in columnChart.graphs) {
		  /* console.log(isNaN(data[columnChart.graphs[x].valueField]));
		  if(isNaN(data[columnChart.graphs[x].valueField])){
			  dataProvider.push({
			      "label": columnChart.graphs[x].title,
			      "value": 0
			    });
		  } else { */
		    dataProvider.push({
		      "label": columnChart.graphs[x].title,
		      "value": Math.round(data[columnChart.graphs[x].valueField] * 100) / 100
		    });
		  /* } */
	  }
	  return dataProvider;
	}

	// build pie chart
	var pieChart = AmCharts.makeChart("chartdiv2", {
	  "type": "pie",
	  "theme": "light",
	  "colors": columnChart.colors,
	  "labelRadius": -50,
	  "labelText": "[[title]]<br>[[percents]]%<br>([[value]]건)",
	  "color": "#fff",
	  "hideLabelsPercent": 5,
	  "titles": [{
	    "text": "전체 시스템",
	    "color": "#000"
	  }],
	  "dataProvider": getPieChartTotals(),
	  "valueField": "value",
	  "titleField": "label",
	  "alphaField": "alpha"
	});
	
}

function drawchart5_a (data) {
	
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	
	for (var i = 0; i < jsonData.length; i++) {
		var req_type;
		if(jsonData[i].req_type == 'RD') req_type = "조회";
		else if(jsonData[i].req_type == 'CR') req_type = "등록";
		else if(jsonData[i].req_type == 'DL') req_type = "삭제";
		else if(jsonData[i].req_type == 'UD') req_type = "수정";
		else if(jsonData[i].req_type == 'DN') req_type = "다운로드";
		else if(jsonData[i].req_type == 'PR') req_type = "출력";
		else if(jsonData[i].req_type == 'CO') req_type = "수집";
		else if(jsonData[i].req_type == 'NE') req_type = "생성";
		else if(jsonData[i].req_type == 'BE') req_type = "연계";
		else if(jsonData[i].req_type == 'IN') req_type = "연동";
		else if(jsonData[i].req_type == 'WR') req_type = "기록";
		else if(jsonData[i].req_type == 'SA') req_type = "저장";
		else if(jsonData[i].req_type == 'SU') req_type = "보유";
		else if(jsonData[i].req_type == 'FI') req_type = "가공";
		else if(jsonData[i].req_type == 'UP') req_type = "편집";
		else if(jsonData[i].req_type == 'SC') req_type = "검색";
		else if(jsonData[i].req_type == 'CT') req_type = "정정";
		else if(jsonData[i].req_type == 'RE') req_type = "복구";
		else if(jsonData[i].req_type == 'US') req_type = "이용";
		else if(jsonData[i].req_type == 'OF') req_type = "제공";
		else if(jsonData[i].req_type == 'OP') req_type = "공개";
		else if(jsonData[i].req_type == 'AN') req_type = "파기";
		else if(jsonData[i].req_type == 'EX') req_type = "실행";
		else if(jsonData[i].req_type == 'SV') req_type = "저장";
		else if(jsonData[i].req_type == 'PRRD') req_type = "출력(RD)";
		else if(jsonData[i].req_type == 'PREX') req_type = "출력(Excel)";
		else req_type = "기타";
		
		var value = {"req_type": req_type, "data1": jsonData[i].req_type, "cnt": jsonData[i].cnt};
		dataProvider.push(value);
	}
	
	var chart5 = AmCharts.makeChart( "chart5", {
	  "type": "pie",
	  "theme": "light",
	  "dataProvider": dataProvider,
	  "titleField": "req_type",
	  "valueField": "cnt",
	  "descriptionField": "data1",
	  "labelRadius": 5,
	  "legend":{
	   	"position":"right",
	    "marginRight":100,
	    "autoMargins":false
	  },
	  "radius": "40%",
	  "innerRadius": "35%",
	  "labelText": "[[title]]",
	  "export": {
	    "enabled": false
	  }
	} );
	
	chart5.addListener("clickSlice", handleClick);
}

function handleClick(event) {
	var startdate = $('#search_fr').val();
	var enddate = $('#search_to').val();
	
	$('input[name=search_from]').attr("value", startdate);
	$('input[name=search_to]').attr("value", enddate);
	$('input[name=req_type]').attr("value", event.dataItem.description);
	$('input[name=main_menu_id]').val('MENU00040');
	
	$("#listForm").attr("action", "${rootPath}/allLogInq/list.html");
	$("#listForm").submit();
}

//setchart5();
</script>