<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />
<h1 class="page-title">${currentMenuName }</h1>

<div class="portlet light portlet-fit portlet-datatable bordered">
	<div class="portlet-body">
		<div class="table-container">
			<div id="datatable_ajax_2_wrapper" class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
				<div>
					<div class="col-md-6" style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
						<div class="portlet box grey-salsa">	
							<div class="portlet-title" style="background-color: #2B3643;">
								<div class="caption">
								<i class="fa fa-search"></i> 
									검색
								</div>
								<div class="tools">
	                            	<a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
					            </div>
							</div>
							<div class="portlet-body form">
                                <div class="form-body" style="padding-left:10px; padding-right:10px;">
                                    <div class="form-group">
                                    	<form id="listForm" method="POST" class="mt-repeater form-horizontal">
                                            <div class="row">
	                                            <div class="col-md-2">
	                                            	<label class="control-label">기간</label>
	                                                <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
														<input type="text" class="form-control" id="search_fr" name="search_from" value="${paramBean.search_from}"> 
														<span class="input-group-addon"> &sim; </span> 
														<input type="text" class="form-control" id="search_to" name="search_to" value="${paramBean.search_to}">
													</div>
	                                            </div>
													
												<div class="col-md-2">
													<label class="control-label">조회대상</label><br/>
													<select id="searchType"name="searchType" class="ticket-assign form-control">
														<option value="01">접속기록조회</option>
														<option value="02">시스템로그조회</option>
														<option value="03">다운로드 로그조회</option>
													</select>
												</div>
											<div class="col-md-2">
												<label class="control-label">처리량/이용량</label><br/>
												<select id="logCtType"name="logCtType" class="ticket-assign form-control">
													<option value="logCt">처리량</option>
													<option value="totalCt">이용량</option>
												</select>
											</div>
                                            </div>
                                            <hr/>
                                            <div align="right">
                                            	<button class="btn btn-sm blue btn-outline sbold uppercase" type="button" onclick="setchart1()">
													<i class="fa fa-search"></i> 검색
												</button>	
                                            </div>
                                    	</form>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
				</div>
				<div id="chart1" style="width: 100%; height: 500px;">
					
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
var url = "${rootPath}/pivotchart/pivotchart.html?type=1";

window.onload = function() {
	 setchart1();
}

function setchart1 () {
	var startdate = $('#search_fr').val();
	var enddate = $('#search_to').val();
	var searchType = $('#searchType').val();
	var logCtType = $('#logCtType').val();
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/new_dashboard_chart1.html',
		data: { 
			"startdate" : startdate,
			"enddate" : enddate,
			"searchType": searchType,
			"logCtType" : logCtType
		},
		success: function(data) {
			drawchart1(data);
		},
		beforeSend:function() {
			$('#loading').show();
		},
		complete:function() {
			$('#loading').hide();
		}
	});
}

function drawchart1 (data) {
	
	var dataProvider = new Array();
	var graphs = new Array();
	
	var jsonData = JSON.parse(data);
	
	var  valueTemp="";
	var  nameTemp="";
	
	for (var i = 0; i < jsonData.length; i++) {
		dataProvider.push(jsonData[i].dataProvider);
		
	}
	
	if ( jsonData.length > 0) {
		var graphsTemp = jsonData[0].graphs;
		
		for (var j = 0; j < graphsTemp.length; j++) {
			graphs.push(graphsTemp[j]);
		}
	}
	
	/*dashboard_amchart_1*/
	var chart1 = AmCharts.makeChart("chart1", {
	    "type": "serial",
	    "theme": "light",
	    "legend": {
	        "useGraphSettings": true,
	        "position": "top"
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "integersOnly": true,
	        "reversed": false,
	        "axisAlpha": 0,
	        "dashLength": 5,
	        "gridCount": 10,
	        "position": "left",
	        "title": "개인정보검출(건)"
	    }],
	    "startDuration": 0,
	    "sequencedAnimation": false,
	    "startEffect": "easeOutSine",
	    "graphs": graphs,
	    "chartCursor": {
	        "cursorAlpha": 0,
	        "zoomable": false
	    },
	    "categoryField": "proc_date",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "fillAlpha": 0.05,
	        "fillColor": "#000000",
	        "gridAlpha": 0,
	        "position": "bottom"
	    },
	    "export": {
	    	"enabled": false,
	        "position": "bottom-right"
	     }
	});
}
//setchart1();
</script>