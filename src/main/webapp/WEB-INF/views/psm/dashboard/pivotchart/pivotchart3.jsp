<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />
<h1 class="page-title">${currentMenuName }</h1>

<div class="portlet light portlet-fit portlet-datatable bordered">
	<div class="portlet-body">
		<div class="table-container">
			<div id="datatable_ajax_2_wrapper" class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
				<div>
					<div class="col-md-6" style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
						<div class="portlet box grey-salsa">	
							<div class="portlet-title" style="background-color: #2B3643;">
								<div class="caption">
								<i class="fa fa-search"></i> 
									검색
								</div>
								<div class="tools">
	                            	<a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
					            </div>
							</div>
							<div class="portlet-body form">
                                <div class="form-body" style="padding-left:10px; padding-right:10px;">
                                    <div class="form-group">
                                    	<form id="listForm" method="POST" class="mt-repeater form-horizontal">
                                    		<div data-repeater-list="group-a">
                                               	<div class="row">
                                               		<div class="col-md-3">
                                               			<label class="control-label">기간</label>
                                                        <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
															<input type="text" class="form-control" id="search_fr" name="search_from" value="${paramBean.search_from}"> 
															<span class="input-group-addon"> &sim; </span> 
															<input type="text" class="form-control" id="search_to" name="search_to" value="${paramBean.search_to}">
														</div>
                                               		</div>
															
														<div class="col-md-3">
															<label class="control-label">조회대상</label><br/>
															<select id="searchType"name="searchType" class="ticket-assign form-control">
																<option value="01">접속기록조회</option>
															<option value="02" <c:if test='${searchType eq "02"}'>selected="selected"</c:if>>시스템로그조회</option>
															<option value="03" <c:if test='${searchType eq "03"}'>selected="selected"</c:if>>다운로드 로그조회</option>
															</select>
														</div>
                                               	</div>
                                            </div>
                                            <hr/>
                                            <div align="right">
                                            	<button class="btn btn-sm blue btn-outline sbold uppercase" type="button" onclick="setchart3()">
													<i class="fa fa-search"></i> 검색
												</button>	
                                            </div>
                                            
                                            <input type="hidden" name="system_seq" />
                                            <input type="hidden" name="main_menu_id" />
                                    	</form>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
				</div>
				<div id="chart3" style="width: 100%; height: 500px;">
					
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
window.onload = function() {
	setchart3();
}

function setchart3 () {
	var startdate = $('#search_fr').val();
	var enddate = $('#search_to').val();
	var searchType = $('#searchType').val();
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/new_dashboard_chart3.html',
		data: { 
			"startdate": startdate,
			"enddate": enddate,
			"searchType": searchType
		},
		success: function(data) {
			drawchart3(data);
		},
		beforeSend:function() {
			$('#loading').show();
		},
		complete:function() {
			$('#loading').hide();
		}
	});
}

function drawchart3(data) {

	var jsonData = JSON.parse(data);
	var dataProvider = jsonData["dataProvider"];
	var graphs = jsonData["graphs"];
	
	var chart8 = AmCharts.makeChart("chart3", {
	    "type": "serial",
		"theme": "light",
		"legend": {
	        "horizontalGap": 10,
	        "maxColumns": 1,
	        "position": "right",
			"useGraphSettings": true,
			"markerSize": 10
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.3,
	        "gridAlpha": 0
	    }],
	    "graphs": graphs,
	    "rotate": true,
	    "categoryField": "category",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "gridAlpha": 0,
	        "position": "left"
	    },
	    "export": {
	    	"enabled": false
	     }
	});
}

function drawchart3_bak (data) {
	
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var name = jsonData[i].system_name;
		if ( name != null && name.length > 5 ) name= name.substring(0,5)+"...";
		
		var value = {"system_seq": jsonData[i].system_seq,"system_name": name,"priv1": jsonData[i].priv1,"priv2": jsonData[i].priv2,"priv3": jsonData[i].priv3,"priv4": jsonData[i].priv4,"priv5": jsonData[i].priv5,"priv6": jsonData[i].priv6,"priv7": jsonData[i].priv7,"priv8": jsonData[i].priv8,"priv9": jsonData[i].priv9,"priv10": jsonData[i].priv10 };
		dataProvider.push(value);
	}
	
	/*dashboard_amchart_3*/
	var chart3 = AmCharts.makeChart("chart3", {
	    "type": "serial",
		"theme": "light",
	    "legend": {
	        "horizontalGap": 10,
	        "maxColumns": 1,
	        "position": "right",
			"useGraphSettings": true,
			"markerSize": 10
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.3,
	        "gridAlpha": 0
	    }],
	    "graphs": [{
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "주민등록번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "priv1",
	        "descriptionField": "system_seq",
	        "labelText": "[[value]]",
	        "showHandOnHover" : true
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "운전면허번호",
	        "type": "column",
			"color": "#000000",
			"descriptionField": "system_seq",
	        "valueField": "priv2",
	        "showHandOnHover" : true
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "여권번호",
	        "type": "column",
			"color": "#000000",
			"descriptionField": "system_seq",
	        "valueField": "priv3",
	        "showHandOnHover" : true
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "신용카드번호",
	        "type": "column",
			"color": "#000000",
			"descriptionField": "system_seq",
	        "valueField": "priv4",
	        "showHandOnHover" : true
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "건강보험번호",
	        "type": "column",
			"color": "#000000",
			"descriptionField": "system_seq",
	        "valueField": "priv5",
	        "showHandOnHover" : true
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "전화번호",
	        "type": "column",
			"color": "#000000",
			"descriptionField": "system_seq",
	        "valueField": "priv6",
	        "showHandOnHover" : true
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "이메일",
	        "type": "column",
			"color": "#000000",
			"descriptionField": "system_seq",
	        "valueField": "priv7",
	        "showHandOnHover" : true
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "휴대전화번호",
	        "type": "column",
			"color": "#000000",
			"descriptionField": "system_seq",
	        "valueField": "priv8",
	        "showHandOnHover" : true
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "계좌번호",
	        "type": "column",
			"color": "#000000",
			"descriptionField": "system_seq",
	        "valueField": "priv9",
	        "showHandOnHover" : true
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "외국인등록번호",
	        "type": "column",
			"color": "#000000",
			"descriptionField": "system_seq",
	        "valueField": "priv10",
	        "showHandOnHover" : true
	    }],
	    "categoryField": "system_name",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "gridAlpha": 0,
	        "position": "left"
	    },
	    "export": {
	    	"enabled": false
	     }

	});
	
	chart3.addListener("clickGraphItem", handleClick);
}

function handleClick(event) {
	
	var startdate = $('#search_fr').val();
	var enddate = $('#search_to').val();
	
	$('input[name=search_from]').attr("value", startdate);
	$('input[name=search_to]').attr("value", enddate);
	$('input[name=system_seq]').attr("value", event.item.description);
	$('input[name=main_menu_id]').val('MENU00040');
	$("#listForm").attr("action", "${rootPath}/allLogInq/list.html");
	$("#listForm").submit();
}
//setchart3();
</script>