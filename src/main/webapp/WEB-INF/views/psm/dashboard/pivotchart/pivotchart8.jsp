<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>

<c:set var="currentName" value="시스템별 개인정보 처리 추이"/>
<h1 class="page-title">${currentName }</h1>

<div class="portlet light portlet-fit portlet-datatable bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-settings font-dark"></i> <span
				class="caption-subject font-dark sbold uppercase">${currentName} 차트</span>
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-container">
			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
				<div class="dataTables_scroll" style="position: relative;">
					<div>
						<div class="col-md-6"
							style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
							<div class="portlet box grey-salsa">
								<div class="portlet-title" style="background-color: #32c5d2;">
									<div class="caption">
									<i class="fa fa-gift"></i> 
										검색
									</div>
									<div class="tools">
										<a href="javascript:;" class="expand"></a>
									</div>
								</div>


								<!-- portlet-body S -->
								<form id="listForm" method="POST" class="portlet-body" style="display:none;">
									<div class="portlet-body">
										<div class="table-container">
											<div id="datatable_ajax_2_wrapper"
												class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
												<div class="row" style="padding-bottom: 5px;">
												<div class="col-md-4">
													<div class="col-md-3">◎ 기간:</div>
													<div class="col-md-9" style="padding: 0px;">
														<span class="date date-picker margin-bottom-5 input-small"
															data-date-format="yyyy-mm-dd"> <input type="text"
															id="search_fr" style="width: 80px;" class="input_date"
															readonly="" name="search_from" placeholder="From"
															value="${paramBean.search_from}"> <span
															class="">
																<button class="btn btn-sm default" type="button">
																	<i class="fa fa-calendar"></i>
																</button>
														</span>
														</span> <i class="sim">&sim;</i> <span
															class="date date-picker input-small"
															data-date-format="yyyy-mm-dd"> <input type="text"
															id="search_to" style="width: 80px;" class="input_date"
															readonly="" name="search_to" placeholder="To"
															value="${paramBean.search_to}"> <span
															class="">
																<button class="btn btn-sm default" type="button">
																	<i class="fa fa-calendar"></i>
																</button>
														</span>
														</span>
													</div>
												</div>
												
												<div class="col-md-8" align="right">
													<button class="btn btn-sm green table-group-action-submit"
														onclick="moveallLogInqList()">
														<i class="fa fa-check"></i> 검색
													</button>
												</div>
												</div>
												
											</div>
											<!-- portlet-boyd E -->

										</div>
									</div>
								</form>
								<!-- portlet-boyd E -->
							</div>
						</div>
					</div>
					<div id="chart8" style="width: 100%; height: 500px;">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
function setchart8 () {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/new_dashboard_chart8.html',
		data: { 
			"startdate": startdate,
			"enddate": enddate
		},
		success: function(data) {
			drawchart8(data);
		}
	});
}

function drawchart8 (data) {
	var dataProvider = new Array();
	var graphs = new Array();
	
	var jsonData = JSON.parse(data);
	
	var  valueTemp="";
	var  nameTemp="";
	
	for (var i = 0; i < jsonData.length; i++) {
		dataProvider.push(jsonData[i].dataProvider);
		
	}
	
	if ( jsonData.length > 0) {
		var graphsTemp = jsonData[0].graphs;
		
		for (var j = 0; j < graphsTemp.length; j++) {
			graphs.push(graphsTemp[j]);
		}
	}
	
	var chart1 = AmCharts.makeChart("chart8", {
	    "type": "serial",
	    "theme": "light",
	    "legend": {
	        "useGraphSettings": true,
	        "position": "top"
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "integersOnly": true,
	        "reversed": false,
	        "axisAlpha": 0,
	        "dashLength": 5,
	        "gridCount": 10,
	        "position": "left",
	        "title": "개인정보검출(건)"
	    }],
	    "startDuration": 0,
	    "graphs": graphs,
	    "chartCursor": {
	        "cursorAlpha": 0,
	        "zoomable": false
	    },
	    "categoryField": "proc_date",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "fillAlpha": 0.05,
	        "fillColor": "#000000",
	        "gridAlpha": 0,
	        "position": "bottom"
	    },
	    "export": {
	    	"enabled": false,
	        "position": "bottom-right"
	     }
	});
}
setchart8();
</script>