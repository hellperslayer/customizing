<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>

<c:set var="currentName" value="개인정보 유형별 현황"/>
<h1 class="page-title">${currentName }</h1>

<div class="portlet light portlet-fit portlet-datatable bordered">
	<%-- <div class="portlet-title">
		<div class="caption">
			<i class="icon-settings font-dark"></i> <span
				class="caption-subject font-dark sbold uppercase">${currentName} 차트</span>
		</div>
	</div> --%>
	<div class="portlet-body">
		<div class="table-container">
			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
				<div class="dataTables_scroll" style="position: relative;">
					<div>
						<div class="col-md-6"
							style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
							<div class="portlet box grey-salsa">	
								<div class="portlet-title" style="background-color: #2B3643;">
									<div class="caption">
									<i class="fa fa-search"></i> 
										검색
									</div>
								</div>


								<!-- portlet-body S -->
								<form id="listForm" method="POST" class="portlet-body" >
									<input type="hidden" name="privacyType" />
<<<<<<< .mine
									<div class="portlet-body">
										<div class="table-container">
											<div id="datatable_ajax_2_wrapper"
												class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
												<div class="row" style="padding-left: 20px;">
												<div class="col-md-12" >
													<!-- <div class="col-md-3">◎ 기간:</div> -->
													<%-- <div class="col-md-9" style="padding: 0px;">
														<span class="date date-picker margin-bottom-5 input-small"
															data-date-format="yyyy-mm-dd"> <input type="text"
															id="search_fr" style="width: 80px;" class="input_date"
															readonly="" name="search_from" placeholder="From"
															value="${paramBean.search_from}"> <span
															class="">
																<button class="btn btn-sm default" type="button">
																	<i class="fa fa-calendar"></i>
																</button>
														</span>
														</span> <i class="sim">&sim;</i> <span
															class="date date-picker input-small"
															data-date-format="yyyy-mm-dd"> <input type="text"
															id="search_to" style="width: 80px;" class="input_date"
															readonly="" name="search_to" placeholder="To"
															value="${paramBean.search_to}"> <span
															class="">
																<button class="btn btn-sm default" type="button">
																	<i class="fa fa-calendar"></i>
																</button>
														</span>
														</span>
													</div> --%>
													<label class="control-label">기간</label><br/>
													<div class="col-md-9" style="padding-left: 0px;">
														<div
															class="input-group input-medium date-picker input-daterange"
															data-date="10/11/2012" data-date-format="yyyy-mm-dd">
															<input type="text" class="form-control" id="search_fr"
																name="search_from"
																value="${paramBean.search_from}"> <span
																class="input-group-addon"> &sim; </span> <input type="text"
																class="form-control" id="search_to" name="search_to"
																value="${paramBean.search_to}">
														</div>
													</div>
													<div class="col-md-3" align="right">
														<!-- <button class="btn btn-sm green table-group-action-submit"
															onclick="moveallLogInqList()">
															<i class="fa fa-check"></i> 검색
														</button> -->
														<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="setchart4()"><i class="fa fa-check"></i> 검색</a>
													</div>	
												</div>
												
												
												</div>
												
											</div>
											<!-- portlet-boyd E -->

||||||| .r88
=======
									<div class="form-group">
<!-- 					 <form id="listForm" method="POST" class="mt-repeater form-horizontal"> -->
<!-- 					     <div data-repeater-list="group-a"> -->
<!-- 					         <div data-repeater-item class="mt-repeater-item"> -->
							<div class="mt-repeater-input">
								<div class="row">
									<div class="col-md-4">
										<label class="control-label">조회일자</label>
								   		<br/>
										<div class="input-group input-medium date-picker input-daterange"
											data-date="10/11/2012" data-date-format="yyyy-mm-dd">
											<input type="text" class="form-control" id="search_fr"
												name="search_from" value="${paramBean.search_from}"> 
											<span class="input-group-addon"> &sim; </span> 
											<input type="text" class="form-control" id="search_to" 
												name="search_to" value="${paramBean.search_to}">
>>>>>>> .r96
										</div>
									</div>
									<div class="col-md-4">
									  <label class="control-label">시스템명</label>
								   		<br/>
									   <select name="system_seq" id="system_seq" class="form-control input-medium">
											<option value="" ${paramBean.system_seq == '' ? 'selected="selected"' : ''}>
											----- 전 체 -----</option>
											<c:if test="${empty systemList}">
											<option>시스템 데이터 없습니다.</option>
											</c:if>
											<c:if test="${!empty systemList}">
											<c:forEach items="${systemList}" var="i" varStatus="z">
											<option value="${i.system_seq}" onclick="setColorIndex(${z.index })"
												${i.system_seq==paramBean.system_seq ? "selected=selected" : "" }>
												${ i.system_name}</option>
											</c:forEach>
											</c:if>
										</select>
									</div>
									<div class="col-md-4" align="right"	style="padding-right: 20px;">
										<br><br>
										<button class="btn btn-sm blue btn-outline sbold uppercase" type="button"
										onclick="setchart4()">
											<i class="fa fa-search"></i> 검색
										</button>&nbsp;&nbsp;														
									</div>
								</div>							 
							</div>
<!-- 					            </div> -->
<!-- 					        </div> -->
        					
<!--                            </form> -->
                       </div>
								</form>
								<!-- portlet-boyd E -->
							</div>
						</div>
					</div>
					<div id="chart4" style="width: 100%; height: 500px; cursor: pointer;">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<style>
.amcharts-pie-slice {
  transform: scale(1);
  transform-origin: 50% 50%;
  transition-duration: 0.3s;
  transition: all .3s ease-out;
  -webkit-transition: all .3s ease-out;
  -moz-transition: all .3s ease-out;
  -o-transition: all .3s ease-out;
  cursor: pointer;
  box-shadow: 0 0 30px 0 #000;
}

.amcharts-pie-slice:hover {
  transform: scale(1.1);
  filter: url(#shadow);
}							
</style>

<script type="text/javascript">
function setchart4 () {
	var startdate = $('#search_fr').val();
	var enddate = $('#search_to').val();
	var system_seq = $('#system_seq').val();
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/new_dashboard_chart4.html',
		data: { 
			"startdate": startdate,
			"enddate": enddate,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart4(data);
		},
		beforeSend:function() {
			$('#loading').show();
		},
		complete:function() {
			$('#loading').hide();
		}
	});
}


function drawchart4 (data) {

	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"privacy_seq": jsonData[i].privacy_seq,"privacy_desc": jsonData[i].privacy_desc,"nPrivCount": jsonData[i].nPrivCount };
		dataProvider.push(value);
	}
	
	
	var chart4 = AmCharts.makeChart( "chart4", {
		  "type": "pie",
		  "theme": "light",
		  "dataProvider": dataProvider,
		  "titleField": "privacy_desc",
		  "valueField": "nPrivCount",
		  "descriptionField": "privacy_seq",
		  "labelRadius": 5,
		  "balloonText": "[[title]]: [[percents]]% ([[value]])",
		  "legend":{
		   	"position":"right",
		    "marginRight":100,
		    "autoMargins":false,
		  },
		  "radius": "40%",
		  "innerRadius": "35%",
		  "labelText": "[[title]]",
		  "export": {
		    "enabled": false
		  }
		} );
	
	chart4.addListener("clickSlice", handleClick);
}

function handleClick(event) {
	var startdate = $('#search_fr').val();
	var enddate = $('#search_to').val();
	
	$('input[name=search_from]').attr("value", startdate);
	$('input[name=search_to]').attr("value", enddate);
	$('input[name=privacyType]').attr("value", event.dataItem.description);
	
	$("#listForm").attr("action", "${rootPath}/allLogInq/list.html");
	$("#listForm").submit();
}
// setchart4();
</script>