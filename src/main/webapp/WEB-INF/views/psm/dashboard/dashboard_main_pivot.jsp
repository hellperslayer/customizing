<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>
<c:set var="currentMenuId" value="MENU00031" />
<ctl:checkMenuAuth menuId="${currentMenuId}"/>

<style>
	#graph { float: left; width: 100%; height: 700px; overflow: hidden; }
</style>

<script src="${rootPath}/resources/js/psm/dashboard/highcharts.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/exporting.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/globalize.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/knockout-3.0.0.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/dx.chartjs.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/zoomingData.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/dashboard_pivot.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/dashboard/vivagraph.js" type="text/javascript" charset="UTF-8"></script>


<form id="dashboardForm" method="POST">
	<input type="hidden" name="main_menu_id" value="${mainMenuId }" />
	<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
	<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
	<input type="hidden" name="page_num" />
</form>
	
	
	<div class="portlet-body">
		<div class="tabbable tabbable-tabdrop">
	    	<!-- BEGIN HEADER MENU -->
		    <ul class="nav nav-tabs">
		    	<div class="tab_dash left">
					<span id="tab1" class="tab <c:if test="${tabFlag == 'tab1' }">select</c:if>">접속기록</span>
					<span id="tab2" class="tab <c:if test="${tabFlag == 'tab2' }">select</c:if>">위험분석</span>
					<div class="dashlogo">
						<img src="${pageContext.servletContext.contextPath}/resources/image/login/logo3.png" />
					</div>
				</div>
		    </ul>
		   </div>
	   </div>
<input type="hidden" id="tabId" value="${tabFlag }" />
	
<!-- 대시보드 항목 영역 -->
<div class="dashboard1" id="dashContent1">
	<%@include file="/WEB-INF/views/psm/dashboard/dashboardChart1.jsp"%>
</div>

<!-- 대시보드 항목 영역 -->
<div class="dashboard2 none" id="dashContent2">
	<%@include file="/WEB-INF/views/psm/dashboard/dashboardChart2.jsp"%>
</div>

<!-- 전체로그조회 이동 Form -->
<form id="allLogInqListForm" action="${rootPath }/allLogInq/list.html" method="POST">
	<input type="hidden" name="main_menu_id" value="MENU00040" />
	<input type="hidden" name="sub_menu_id" value="MENU00041" />
	<input type="hidden" name="search_to" />
	<input type="hidden" name="search_from" />
	<input type="hidden" name="starthm" />
	<input type="hidden" name="endhm" />
	<input type="hidden" name="system_seq" />
	<input type="hidden" name="sub_menu_id" />
	<input type="hidden" name="emp_user_id" />
</form>

<script type="text/javascript">
	
	var dashboardConfig = {
		"dashboardUrl":"${rootPath}/dashboard/dashboard.html"
		,"detailUrl":"${rootPath}/dashboard/detail.html"
	};
 	
</script>