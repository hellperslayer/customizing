<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%-- <c:set var="currentMenuId" value="MENU00030" /> --%>
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />
<script src="${rootPath}/resources/js/psm/dashboard/dashboard2.js" type="text/javascript" charset="UTF-8" ></script>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>
<style>
.chartStyle {
	width: 100%; 
	height: 300px;
}
</style>

<div class="row" style="padding-right: 20px; padding-left: 20px;">
	<h1 class="page-title">
		${currentMenuName}
<!-- 		<small>Dashboard</small> -->
	</h1>
	<div id="dateBtnDiv" align="right">
		<button class="btn btn-m green btn-inline sbold uppercase"  onclick="fnNowClick(1);"><i class="fa fa-search"></i>&nbsp;실시간</button>
		<button class="btn btn-m green btn-outline sbold uppercase" onclick="fnNowClick(2);"><i class="fa fa-search"></i>&nbsp;일&nbsp;&nbsp;간</button>
		<button class="btn btn-m green btn-outline sbold uppercase" onclick="fnNowClick(3);"><i class="fa fa-search"></i>&nbsp;월&nbsp;&nbsp;간</button>
	</div>
	<div id="srchDiv" class="portlet box grey-salt">
		<div class="portlet-title" style="background-color: #2B3643;">
			<div class="caption">
				<i class="fa fa-search"></i>검색
			</div>
			<div class="tools">
				<a href="javascript:;" class="expand"> </a>
			</div>
		</div>
		<div class="portlet-body form">
			<div class="form-body" style="padding-left: 10px; padding-right: 10px;">
				<div class="form-group">
					<form id="listForm" method="POST" class="mt-repeater form-horizontal">
						<div data-repeater-list="group-a">
							<div class="row">
								<div id="dateDiv" class="col-md-2">
									<label class="control-label">기간</label>
									<div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
										<input type="text" class="form-control" id="search_fr" name="search_from" value="${paramBean.search_from}">
										<span class="input-group-addon"> &sim; </span> 
										<input type="text" class="form-control" id="search_to" name="search_to" value="${paramBean.search_to}">
									</div>
								</div>
								<div id="MonthDiv">
									<div class="col-md-2">
										<label class="control-label">년도</label>
										<select id="yearSel" name="yearSel" class="form-control" style="text-align:right;">
										</select>
									</div>
									<div class="col-md-1">
										<label class="control-label">월</label>
										<select id="monthSel" name="monthSel" class="form-control input-xsmall" style="text-align:right;">
											<option value="1">1월</option>
											<option value="2">2월</option>
											<option value="3">3월</option>
											<option value="4">4월</option>
											<option value="5">5월</option>
											<option value="6">6월</option>
											<option value="7">7월</option>
											<option value="8">8월</option>
											<option value="9">9월</option>
											<option value="10">10월</option>
											<option value="11">11월</option>
											<option value="12">12월</option>
										</select>
									</div>
								</div>
								<div class="col-md-2">
									<label class="control-label">시스템명</label>
									<select name="system_seq" id="system_seq" class="form-control">
										<option value="" ${paramBean.system_seq == '' ? 'selected="selected"' : ''}>----- 전 체 -----</option>
										<c:if test="${empty systemList}">
											<option>시스템 데이터 없습니다.</option>
										</c:if>
										<c:if test="${!empty systemList}">
											<c:forEach items="${systemList}" var="i" varStatus="z">
												<option value="${i.system_seq}" onclick="setColorIndex(${z.index })"
													${i.system_seq==paramBean.system_seq ? "selected=selected" : "" }>
													${ i.system_name}
												</option>
											</c:forEach>
										</c:if>
									</select>
								</div>
							</div>
						</div>
						<hr/>
						<div align="right">
							<button class="btn btn-sm blue btn-outline sbold uppercase" type="button" onclick="searchSystemType()">
								<i class="fa fa-search"></i> 검색
							</button>
						</div>
						
						<input type="hidden" name="emp_user_id"/>
		            	<input type="hidden" name="emp_user_name"/>
		            	<input type="hidden" name="dept_name"/>
		            	<input type="hidden" name="user_ip"/>
		            	<input type="hidden" name="detailOccrDt"/>
		            	<input type="hidden" name="detailEmpCd"/>
		            	<input type="hidden" name="detailEmpDetailSeq"/>
		            	<input type="hidden" name="main_menu_id"/>
		            	<input type="hidden" name="privacyType"/>
		            	<input type="hidden" name="scen_seq"/>
	            	
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
	    <div class="col-lg-6 col-xs-12 col-sm-12">
	        <div class="portlet light bordered">
	            <div class="portlet-title">
	                <div class="caption">
	                    <i class="icon-bar-chart font-green-haze"></i><span class="caption-subject bold uppercase font-green-haze">시스템별 접속기록 TOP5</span>
	                    <span style="color:red;font-size: 12px;padding-left: 10px;">※ 그래프 클릭시 해당 시스템으로 검색이 됩니다.</span>
	                </div>
	                <div id="show_all" align="right" style="cursor: pointer; display: none;" onclick="showAllSystemType()">전체 보기</div>
	            </div>
	            <div class="portlet-body">
	                <div id="chart3" class="chartStyle" style="display:none;"></div>
	                <div id="chart3_nodata" class="chartStyle"><td>데이터가 없습니다.</td></div>
	            </div>
	        </div>
	    </div>
	    <div class="col-lg-6 col-xs-12 col-sm-12">
	        <div class="portlet light bordered">
	            <div class="portlet-title">
	                <div class="caption ">
	                    <i class="icon-bar-chart font-green-haze"></i><span class="caption-subject bold uppercase font-green-haze">개인정보 접속기록 TOP5</span>
	                </div>
	            </div>
	            <div class="portlet-body">
	                <div style="height: 300px;">
	                    <table id="chart2" class="table table-striped table-hover">
	                        <thead>
	                            <tr>
	                                <th style="text-align: center;">일시</th>
									<th style="text-align: center;">소속</th>
									<th style="text-align: center;">ID</th>
									<th style="text-align: center;">사용자명</th>
									<%--<th style="text-align: center;">사용자IP</th>--%>
									<th style="text-align: center;">개인정보 수</th>
	                            </tr>
	                        </thead>
	                        <tbody>
							</tbody>
	                    </table>
	                </div>
	            </div>
	        </div>
	    </div>
	    
	</div>
	<div class="row">
		<div class="col-lg-4 col-xs-12 col-sm-12">
	        <div class="portlet light bordered">
	            <div class="portlet-title">
	                <div class="caption ">
	                    <i class="icon-bar-chart font-green-haze"></i><span class="caption-subject bold uppercase font-green-haze">소속별 로그 수집 현황 TOP5</span>
	                </div>
	            </div>
	            <div class="portlet-body">
	            	<div style="height: 300px;">
	                    <table id="chart7" class="table table-striped table-hover">
	                        <thead>
	                            <tr>
	                                <th style="text-align: center;"> 순위 </th>
	                                <th style="text-align: center;"> 소속 </th>
	                                <th style="text-align: center;"> 개인정보 수 </th>
	                                <th style="text-align: center;"> 개인정보 수<p style="font-size:10px; margin:0 0 0 0; paddding:0 0 0 0;">(전년동월)</p></th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                        </tbody>
	                    </table>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="col-lg-4 col-xs-12 col-sm-12">
	        <div class="portlet light bordered">
	            <div class="portlet-title">
	                <div class="caption ">
	                    <i class="icon-bar-chart font-green-haze"></i><span class="caption-subject bold uppercase font-green-haze">개인정보 유형별 접속기록 현황</span>
	                </div>
	            </div>
	            <div class="portlet-body">
	                <div id="chart4" class="chartStyle" style="display:none;"></div>
	                <div id="chart4_nodata" class="chartStyle"><td>데이터가 없습니다.</td></div>
	            </div>
	        </div>
	    </div>
	    <div class="col-lg-4 col-xs-12 col-sm-12">
	        <div class="portlet light bordered">
	            <div class="portlet-title">
	                <div class="caption ">
	                    <i class="icon-bar-chart font-green-haze"></i><span class="caption-subject bold uppercase font-green-haze">시스템 정보</span>
	                </div>
	            </div>
	            <div class="portlet-body">
	                <div id="chart8" class="chartStyle"></div>
	                <div id="chart8_nodata" class="chartStyle"><td>데이터가 없습니다.</td></div>
	            </div>
	        </div>
	    </div>
	</div>
</div>
<div id="datepicDiv"></div>
<script type="text/javascript">
	var dashboardConfig = {
		"listUrl" : "${rootPath}/allLogInq/list.html",
		"extrtCondbyInqListUrl" : "${rootPath}/extrtCondbyInq/list.html",
		"extrtCondbyInqDetailUrl" : "${rootPath}/extrtCondbyInq/detail.html"
	};
	
	$("#chart7").dataTable({
		scrollY: 220,
		deferRender: false,
		scroller: true,
		searching: false,
		info: false,
		ordering: false
	});
	
	$("#chart2").dataTable( {
		scrollY: 220,
		deferRender: false,
		scroller: true,
		searching: false,
		info: false,
		ordering: false
	});
</script>
