<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>
<c:set var="currentMenuId" value="MENU00031" />
<ctl:checkMenuAuth menuId="${currentMenuId}"/>


<script src="${rootPath}/resources/js/dashboard/dashboardCharts.js" type="text/javascript"></script>

<form id="dashboardForm" method="POST">
	<input type="hidden" name="main_menu_id" value="${mainMenuId }" />
	<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
	<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
	<input type="hidden" name="page_num" />
</form>

<!-- <h1 class="page-title">
	피벗차트 <small>PivotChart</small>
</h1> -->

<!-- 대시보드 -->
<div class="portlet light bordered form-fit">

<div class="portlet-body">
	<div class="tabbable tabbable-tabdrop">
    	<!-- BEGIN HEADER MENU -->
	    <ul class="nav nav-tabs">
	    	<li <c:if test="${tabFlag == 'tab1' }">class="active"</c:if>>
	            <a href="#tab1" data-toggle="tab"> 접속기록 </a>
	        </li>
	        <li <c:if test="${tabFlag == 'tab2' }">class="active"</c:if>>
	            <a href="#tab2" data-toggle="tab"> 위험분석 </a>
	        </li>   
	        <%-- <div align="right" style="padding-right: 20px;">
				<img
					src="${rootPath}/resources/image/login/logo3.png" />
			</div> --%>
	    </ul>
	    
	   <input type="hidden" id="tabId" value="${tabFlag }" />
	   
		<!-- END HEADER MENU -->		
		<!-- BEGIN PAGE BASE CONTENT -->
		<div class="tab-content">
					
			<!-- <form id="dashForm" method="POST"> -->
			<!-- 접속기록 항목 영역 -->
			<%@include file="/WEB-INF/views/psm/dashboard/dashboardChart1_new.jsp"%>
		    
		    <!-- 위험분석 항목 영역 -->
		    <%@include file="/WEB-INF/views/psm/dashboard/dashboardChart2_new.jsp"%>
		    <!-- </form> -->
		    
		</div>	
	</div>
</div>

</div>

<!-- 전체로그조회 이동 Form -->
<form id="allLogInqListForm" action="${rootPath }/allLogInq/list.html" method="POST">
	<input type="hidden" name="main_menu_id" value="MENU00040" />
	<input type="hidden" name="sub_menu_id" value="MENU00041" />
	<input type="hidden" name="search_to" />
	<input type="hidden" name="search_from" />
	<input type="hidden" name="starthm" />
	<input type="hidden" name="endhm" />
	<input type="hidden" name="system_seq" />
	<input type="hidden" name="sub_menu_id" />
	<input type="hidden" name="emp_user_id" />
</form>

<script type="text/javascript">
	
	var dashboardConfig = {
		"dashboardUrl":"${rootPath}/dashboard/dashboard.html"
		,"detailUrl":"${rootPath}/dashboard/detail.html"
	};
 	
</script>