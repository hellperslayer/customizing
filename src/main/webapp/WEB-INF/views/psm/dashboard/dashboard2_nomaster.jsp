<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%-- <c:set var="currentMenuId" value="MENU00030" /> --%>
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />
<script src="${rootPath}/resources/js/psm/dashboard/dashboard2.js" type="text/javascript" charset="UTF-8" ></script>

<style>
.chartStyle {
	width: 100%; 
	height: 300px;
}

/* .dialog__trigger, .dialog__action {
  border: 3px solid #333333;
  background: #f1f1f1;
  padding: 15px 20px;
  font-size: 1.1rem;
  text-transform: uppercase;
  display: block;
  -webkit-transition: all 150ms ease-out;
  transition: all 150ms ease-out;
  -webkit-transform: translateY(0px);
          transform: translateY(0px);
}
.dialog__trigger:hover, .dialog__action:hover {
  -webkit-transform: translateY(-5px);
          transform: translateY(-5px);
  -webkit-transition: all 100ms ease-in;
  transition: all 100ms ease-in;
  box-shadow: 0 5px 10px rgba(51, 51, 51, 0.4);
}
.dialog__trigger:focus, .dialog__action:focus {
  outline: 0;
}
.dialog__trigger:active, .dialog__action:active {
  -webkit-transform: translateY(-3px);
          transform: translateY(-3px);
}

.dialog {
  background: #f1f1f1;
  width: 70%;
  position: absolute;
  left: calc(50% - 35%);
  top: 0;
  padding: 30px;
  box-shadow: 0 10px 30px rgba(51, 51, 51, 0.4);
  border: 3px solid red;
  visibility: hidden;
  opacity: 0;
  -webkit-transition: all 180ms ease-in;
  transition: all 180ms ease-in;
}
@media (max-width: 600px) {
  .dialog {
    width: 90%;
    left: calc(50% - 45%);
  }
}
.dialog.dialog--active {
  top: 10%;
  visibility: visible;
  opacity: 1;
  -webkit-transition: all 250ms ease-out;
  transition: all 250ms ease-out;
}
.dialog .dialog__close {
  font-size: 2rem;
  line-height: 2rem;
  position: absolute;
  right: 15px;
  top: 15px;
  cursor: pointer;
  padding: 15px;
  -webkit-transition: color 150ms ease;
  transition: color 150ms ease;
}
.dialog .dialog__close:hover {
  color: #E74C3C;
}
.dialog .dialog__title {
  font-size: 2rem;
  font-family: 'Slabo 27px', serif;
  font-weight: 100;
  margin: 0;
  padding: 0 0 15px 0;
  border-bottom: 2px solid #333333;
}
.dialog .dialog__content {
  font-size: 1.1rem;
  line-height: 2rem;
}
.dialog .dialog__action {
  margin: 0;
  font-size: 1rem;
} */
</style>

<!-- <button class="dialog__trigger">Open Dialog</button> -->

<!-- <div class="dialog" style="z-index: 100">
  <span class="dialog__close">&#x2715;</span>
  <h2 class="dialog__title">비정상행위자 발생!! <a onclick="goExtrtCondByInqList()" class="btn btn-sm blue btn-outline sbold uppercase">확인</a></h2>
</div>   -->

<div class="row" style="padding-right: 20px; padding-left: 20px;">
	<h1 class="page-title"> 대시보드
	    <small>Dashboard</small>
	</h1>
	<div class="portlet box grey-salt  ">
         <div class="portlet-title" style="background-color: #2B3643;">
             <div class="caption">
                 <i class="fa fa-search"></i>검색</div>
             <div class="tools">
                 <a href="javascript:;" class="expand"> </a>
             </div>
         </div>
         <div class="portlet-body form" style="display:none;">
			<div class="form-body" style="padding-left: 10px; padding-right: 10px;">
				<div class="form-group">
					<form id="listForm" method="POST" class="mt-repeater form-horizontal">
						<div data-repeater-list="group-a">
							<div data-repeater-item class="mt-repeater-item">
								<div class="mt-repeater-input">
									<label class="control-label">기간</label> <br />
									<div class="input-group input-medium date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
										<input type="text" class="form-control" id="search_fr" name="search_from" value="${paramBean.search_from}">
										<span class="input-group-addon"> &sim; </span> 
										<input type="text" class="form-control" id="search_to" name="search_to" value="${paramBean.search_to}">
									</div>
								</div>
								<div class="mt-repeater-input">
									<label class="control-label">시스템명</label> <br /> 
									<select name="system_seq" id="system_seq" class="form-control input-medium">
										<option value="" ${paramBean.system_seq == '' ? 'selected="selected"' : ''}>----- 전 체 -----</option>
										<c:if test="${empty systemList}">
											<option>시스템 데이터 없습니다.</option>
										</c:if>
										<c:if test="${!empty systemList}">
											<c:forEach items="${systemList}" var="i" varStatus="z">
												<option value="${i.system_seq}" onclick="setColorIndex(${z.index })"
													${i.system_seq==paramBean.system_seq ? "selected=selected" : "" }>
													${ i.system_name}
												</option>
											</c:forEach>
										</c:if>
									</select>
								</div>
							</div>
						</div>
						<div align="right">
							<button class="btn btn-sm blue btn-outline sbold uppercase"
								type="button" onclick="searchSystemType()">
								<i class="fa fa-search"></i> 검색
							</button>
						</div>
						
						<input type="hidden" name="emp_user_id"/>
		            	<input type="hidden" name="emp_user_name"/>
		            	<input type="hidden" name="dept_name"/>
		            	<input type="hidden" name="user_ip"/>
		            	<input type="hidden" name="detailOccrDt"/>
		            	<input type="hidden" name="detailEmpCd"/>
		            	<input type="hidden" name="detailEmpDetailSeq"/>
		            	<input type="hidden" name="main_menu_id"/>
		            	<input type="hidden" name="privacyType"/>
		            	<input type="hidden" name="scen_seq"/>
	            	
					</form>
				</div>
			</div>
		</div>
	</div>
    <!-- <div id="empDiv" class="row" style="display: none;">
    	<div class="col-lg-12 col-xs-12 col-sm-12">
    		<div class="portlet light" style="border-style: solid; border-color: red;">
    			<div class="portlet-title">
	                <div class="caption">
	                    <span class="caption-subject bold uppercase font-dark">비정상행위자 발생</span>
	                </div>
	            </div>
    			<div class="portlet-body">
    				<table id="chart_emp" class="table table-hover">
                        <thead>
                            <tr>
                                <th style="width:20%; text-align: center;">일시</th>
								<th style="width:20%; text-align: center;">추출조건</th>
								<th style="width:20%; text-align: center;">소속</th>
								<th style="width:20%; text-align: center;">사용자ID</th>
								<th style="width:20%; text-align: center;">사용자명</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
    			</div>
    		</div>
    	</div>
    </div> -->
	<div class="row">
	    <div class="col-lg-6 col-xs-12 col-sm-12">
	        <div class="portlet light bordered">
	            <div class="portlet-title">
	                <div class="caption">
	                    <i class="icon-bar-chart font-green-haze"></i><span class="caption-subject bold uppercase font-green-haze">시스템별 접속기록 TOP10</span>
	                    <span style="color:red;font-size: 12px;padding-left: 10px;">※ 그래프 클릭시 해당 시스템으로 검색이 됩니다.</span>
	                </div>
	                <div id="show_all" align="right" style="cursor: pointer; display: none;" onclick="showAllSystemType()">전체 보기</div>
	            </div>
	            <div class="portlet-body">
	                <div id="chart3" class="chartStyle"></div>
	                <div id="chart3_nodata"><td>데이터가 없습니다.</td></div>
	            </div>
	        </div>
	    </div>
	    <div class="col-lg-6 col-xs-12 col-sm-12">
	        <div class="portlet light bordered">
	            <div class="portlet-title">
	                <div class="caption ">
	                    <i class="icon-bar-chart font-green-haze"></i><span class="caption-subject bold uppercase font-green-haze">개인정보 접속기록 TOP10</span>
	                </div>
	            </div>
	            <form id="listForm" method="POST">
	            	<input type="hidden" name="search_from"/>
	            	<input type="hidden" name="search_to"/> 
	            	<input type="hidden" name="emp_user_id"/>
	            	<input type="hidden" name="emp_user_name"/>
	            	<input type="hidden" name="dept_name"/>
	            	<input type="hidden" name="user_ip"/>
	            	<input type="hidden" name="system_seq"/>
	            	<input type="hidden" name="detailOccrDt"/>
	            	<input type="hidden" name="detailEmpCd"/>
	            	<input type="hidden" name="detailEmpDetailSeq"/>
	            	<input type="hidden" name="main_menu_id"/>
	            </form>
	            <div class="portlet-body">
	                <div style="height: 300px;">
	                    <table id="chart2" class="table table-striped table-hover">
	                        <thead>
	                            <tr>
	                                <th style="text-align: center;">일시</th>
									<th style="text-align: center;">소속</th>
									<th style="text-align: center;">ID</th>
									<th style="text-align: center;">사용자명</th>
									<th style="text-align: center;">사용자IP</th>
									<!-- <th style="text-align: center;">시스템명</th> -->
									<th style="text-align: center;">로그수</th>
	                            </tr>
	                        </thead>
	                        <tbody>
<%-- 								<c:forEach items="${rescentList}" var="rescentList"> --%>
<!-- 									<tr> -->
<%-- 										<fmt:parseDate var="dateString" value="${rescentList.proc_date}" pattern="yyyyMMddHHmmss" /> --%>
<%-- 										<td style="text-align: center;"><fmt:formatDate value="${dateString}" pattern="yyyy-MM-dd HH:mm:ss" /></td> --%>
<%-- 										<td style="text-align: center;">${rescentList.dept_name}</td> --%>
<%-- 										<td style="text-align: center;">${rescentList.emp_user_id}</td> --%>
<%-- 										<td style="text-align: center;">${rescentList.emp_user_name}</td> --%>
<%-- 										<td style="text-align: center;">${rescentList.user_ip}</td> --%>
<%-- 										<td style="text-align: center;">${rescentList.system_name}</td> --%>
<!-- 									</tr> -->
<%-- 								</c:forEach> --%>
							</tbody>
	                    </table>
	                </div>
	            </div>
	        </div>
	    </div>
	    
	</div>
	<!-- <div class="row">
	    <div class="col-lg-4 col-xs-12 col-sm-12">
	        <div class="portlet light ">
	            <div class="portlet-title">
	                <div class="caption">
	                    <span class="caption-subject bold uppercase font-dark">비정상행위 추이분석</span>
	                </div>
	            </div>
	            <div class="portlet-body">
	                <div id="chart5" class="chartStyle"></div>
	                <div id="chart5_nodata"><td>데이터가 없습니다.</td></div>
	            </div>
	        </div>
	    </div>
	    <div class="col-lg-4 col-xs-12 col-sm-12">
	        <div class="portlet light ">
	            <div class="portlet-title">
	                <div class="caption ">
	                    <span class="caption-subject font-dark bold uppercase">개인정보 유형별 접속기록 현황</span>
	                </div>
	            </div>
	            <div class="portlet-body">
	                <div id="chart4" class="chartStyle"></div>
	                <div id="chart4_nodata"><td>데이터가 없습니다.</td></div>
	            </div>
	        </div>
	    </div>
	    <div class="col-lg-4 col-xs-12 col-sm-12">
	        <div class="portlet light ">
	            <div class="portlet-title">
	                <div class="caption">
	                    <span class="caption-subject bold uppercase font-dark">비정상행위별 분석현황</span>
	                </div>
	            </div>
	            <div class="portlet-body">
	                <div id="chart6" class="chartStyle"></div>
	                <div id="chart6_nodata"><td>데이터가 없습니다.</td></div>
	            </div>
	        </div>
	    </div>
	</div> -->
	<div class="row">
	    <div class="col-lg-4 col-xs-12 col-sm-12">
	        <div class="portlet light bordered">
	            <div class="portlet-title">
	                <div class="caption ">
	                    <i class="icon-bar-chart font-green-haze"></i><span class="caption-subject bold uppercase font-green-haze">소속별 로그 수집 현황</span>
	                </div>
	            </div>
	            <div class="portlet-body">
	            	<div style="height: 300px;">
	                    <table id="chart7" class="table table-striped table-hover">
	                        <thead>
	                            <tr>
	                                <th style="text-align: center;"> 순위 </th>
	                                <th style="text-align: center;"> 소속 </th>
	                                <th style="text-align: center;"> 로그 수 </th>
	                                <th style="text-align: center;"> 개인정보 수 </th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                        </tbody>
	                    </table>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="col-lg-4 col-xs-12 col-sm-12">
	        <div class="portlet light bordered">
	            <div class="portlet-title">
	                <div class="caption ">
	                    <i class="icon-bar-chart font-green-haze"></i><span class="caption-subject bold uppercase font-green-haze">개인정보 유형별 접속기록 현황</span>
	                </div>
	            </div>
	            <div class="portlet-body">
	                <div id="chart4" class="chartStyle"></div>
	                <div id="chart4_nodata"><td>데이터가 없습니다.</td></div>
	            </div>
	        </div>
	    </div>
	    <div class="col-lg-4 col-xs-12 col-sm-12">
	        <div class="portlet light bordered">
	            <div class="portlet-title">
	                <div class="caption ">
	                    <i class="icon-bar-chart font-green-haze"></i><span class="caption-subject bold uppercase font-green-haze">시스템 정보</span>
	                </div>
	            </div>
	            <div class="portlet-body">
	                <div id="chart8" class="chartStyle"></div>
	                <div id="chart8_nodata"><td>데이터가 없습니다.</td></div>
	            </div>
	        </div>
	    </div>
	</div>
</div>

<script>
	$("#chart7").dataTable( {
		scrollY: 220,
		deferRender: false,
		scroller: true,
		searching: false,
		info: false,
		ordering: false
	});
	
	$("#chart2").dataTable( {
		scrollY: 220,
		deferRender: false,
		scroller: true,
		searching: false,
		info: false,
		ordering: false
	});
	
	var dashboardConfig = {
		"listUrl" : "${rootPath}/allLogInq/list.html",
		"extrtCondbyInqListUrl" : "${rootPath}/extrtCondbyInq/list.html",
		"extrtCondbyInqDetailUrl" : "${rootPath}/extrtCondbyInq/detail.html"
	};
</script>
