<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="X-UA-Compatible" content="IE=9">
<c:set var="rootPath" value="${pageContext.servletContext.contextPath}" scope="application"/>
<c:set var="currentMenuId" value="MENU00031" />
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<script type="text/javascript" charset="UTF-8">
	  		rootPath = '${rootPath}';
	  		contextPath = '${pageContext.servletContext.contextPath}';
</script>

<style>
	#graph { float: left; width: 100%; height: 700px; overflow: hidden; }
</style>

<link rel="stylesheet" type="text/css" href="${rootPath}/resources/css/common/dash_popup.css" />

<script src="${rootPath}/resources/js/common/jquery-1.8.2.min.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/menu-util.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/ajax-util.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/common-prototype-util.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/spin.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/jquery.form.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/dashboard/highcharts.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/exporting.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/globalize.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/knockout-3.0.0.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/dx.chartjs.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/zoomingData.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/dashboard_pop.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/dashboard/vivagraph.js" type="text/javascript" charset="UTF-8"></script>

<form id="dashboardForm" method="POST">
	<input type="hidden" name="main_menu_id" value="${mainMenuId }" />
	<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
	<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
	<input type="hidden" name="page_num" />
</form>
<input type="hidden" id = "dstype" value="${dstype}"/>
<div class="dashboard" id="dashContent">
<input type="hidden" id = "select_system_seq" name ="select_system_seq" value="${select_system_seq}"/>
<c:if test="${dstype eq 'type0'}">
<div class="chart_01 col100">
	<h2>최근 로그 수집 현황</h2>
	<div id="chart1" class="chart_area">
		<div class="messageDiv" onclick="dashboardPop('type0')"></div>
	</div> <!-- e:chart_area -->
</div>
</c:if>

<c:if test="${dstype eq 'type1'}">

<div class="chart_01 col100" style="display:none;">
	<h2>최근 로그 수집 현황</h2>
	<div id="chart1" class="chart_area">
		<div class="messageDiv" onclick="dashboardPop('type0')"></div>
	</div> <!-- e:chart_area -->
</div>

<div class="chart_02 col100">
	<h2>최근 수집 로그 정보</h2>
	<div id="chart2" class="chart_area" style="padding: 0;">
		<div class="chart_table">
			<table id="bizTbl" border="0" cellspacing="0" cellpadding="0">
				<colgroup>
					<col width="20%" />
					<col width="15%" />
					<col width="10%" />
					<col width="15%" />
					<col width="20%" />
					<col width="10%" />
				</colgroup>
				<thead>
					<tr>
						<th>일시</th>
						<th>소속</th>
						<th>ID</th>
						<th>사용자명</th>
						<th>사용자IP</th>
						<th>시스템명</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${rescentList}" var="rescentList">
						<tr style='background: #f8f8f8;'>
							<fmt:parseDate var="dateString" value="${rescentList.proc_date}" pattern="yyyyMMddHHmmss" />
							<td><fmt:formatDate value="${dateString}" pattern="yyyy-MM-dd hh:mm:ss" /></td>
							<td>${rescentList.dept_name}</td>
							<td>${rescentList.emp_user_id}</td>
							<td>${rescentList.emp_user_name}</td>
							<td>${rescentList.user_ip}</td>
							<td>${rescentList.system_name}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div> <!-- e:chart_area -->
</div> <!-- e:chart_02 -->
</c:if>
<c:if test="${dstype eq 'type2'}">
<div class="chart_03 col100">
	<h2>시스템별 로그 수집 현황</h2>
	<div id="chart3" class="chart_area">
	</div> <!-- e:chart_area -->
</div> <!-- e:chart_03 -->
</c:if>
<c:if test="${dstype eq 'type3'}">
<div class="chart_04 col100">
	<h2>개인정보유형별 로그 수집 현황</h2>
	<div id="chart4" class="chart_area">
	</div> <!-- e:chart_area -->
</div> <!-- e:chart_04 -->
</c:if>

<c:if test="${dstype eq 'type4'}">
<div class="chart_05 col100">
	<h2>추출 로그 추이 분석 현황</h2>
	<div id="chart5" class="chart_area">
	</div> <!-- e:chart_area -->
</div> <!-- e:chart_05 -->
</c:if>

<c:if test="${dstype eq 'type5'}">
<div class="chart_06 col100">
	<h2>추출조건별 추출로그 현황</h2>
	<div id="chart6" class="chart_area">
	</div> <!-- e:chart_area -->
</div> <!-- e:chart_06 -->
</c:if>

<c:if test="${dstype eq 'type6'}">
<div class="chart_07 col100">
	<h2>소속별 로그 수집 현황</h2>
	<div id="chart7" class="chart_area" style="padding: 0;">
		<div class="chart_table">
			<table id="deptTopTbl" border="0" cellspacing="0" cellpadding="0">
				<colgroup>
					<col width="15%" />
					<col width="35%" />
					<col width="25%" />
					<col width="25%" />
				</colgroup>
				<thead>
					<tr>
						<th>순위</th>
						<th>소속</th>
						<th>로그 수</th>
						<th>개인정보 수</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
		</div>
	</div> <!-- e:chart_area -->
</div> <!-- e:chart_07 -->
</c:if>

<c:if test="${dstype eq 'type7'}">
<div class="chart_08 col100">
	<h2>수집 서버 현황</h2>
	<div id="chart8" class="chart_area">
	</div> <!-- e:chart_area -->
</div> <!-- e:chart_08 -->
</c:if>
</div>

<form id="allLogInqListForm" action="${rootPath }/allLogInq/list.html" method="POST">
	<input type="hidden" name="main_menu_id" value="MENU00040" />
	<input type="hidden" name="sub_menu_id" value="MENU00041" />
	<input type="hidden" name="search_to" />
	<input type="hidden" name="search_from" />
	<input type="hidden" name="starthm" />
	<input type="hidden" name="endhm" />
	<input type="hidden" name="system_seq" />
	<input type="hidden" name="sub_menu_id" />
	<input type="hidden" name="emp_user_id" />
</form>

<input type ="hidden" name = "pop_from" value="${paramBean.pop_from}"/>
<input type ="hidden" name = "pop_to" value="${paramBean.pop_to}"/>

<script type="text/javascript">
	
	var dashboardConfig = {
		"dashboardUrl":"${rootPath}/dashboard/dashboard.html"
		,"detailUrl":"${rootPath}/dashboard/detail.html"
	};
 	
</script>

</html>