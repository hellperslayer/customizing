<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>UBI SAFER-PSM</title>
<link href="images/favicon.ico"/>
<link rel="stylesheet" href="${rootPath}/resources/css/databoard/reset.css" type="text/css" media="all">
<link rel="stylesheet" href="${rootPath}/resources/css/databoard/style.css" type="text/css" media="all">
<c:set var="now" value="<%=new java.util.Date()%>" />
<c:set var="nowYear">
		<fmt:formatDate value="${now}" pattern="yyyy" />
</c:set>
<c:set var="nowDate">
		<fmt:formatDate value="${now}" pattern="yyyy-MM-dd" />
</c:set>
<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/databoard/databoard.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>
</head>
<body>
<div class="wrapper">
    <!-- container -->
    <div class="container">
        <div class="dashborad align_c">
            <div class="data_option align_c">
            	<form action="${rootPath}/dashboard/init_dashboardView.html" id="searchForm" method="post">
	            	<ul class="detail_search">
	                	<li><a class="<c:if test="${parameters.periodType eq 'now'}">select</c:if>" onclick="searchData('now')">실시간</a></li>
	                    <li><a class="<c:if test="${parameters.periodType eq 'date'}">select</c:if>" onclick="searchData('date')">일간</a></li>
	                    <li><a class="<c:if test="${parameters.periodType eq 'month'}">select</c:if>" onclick="searchData('month')">월간</a></li>
	                </ul>                
	                <div class="section_detail_srch">
	                	<!-- 실시간 -->
	                    <c:if test="${parameters.periodType eq 'now'}">
		                    <div class="srch_time">
		                    	<p><fmt:formatDate value="${now}" pattern="yyyy년 MM월 dd일 HH:mm:ss" /></p>
		                    	<select class="system" name="system_seq">
		                            <optgroup>
		                           		<option value="all">시스템 전체</option>
		                            	<c:forEach var="sysCode" items="${systemList}">
		                            		<option value="${sysCode.system_seq}" <c:if test='${sysCode.system_seq eq parameters.system_seq}'>selected</c:if>>${sysCode.system_name} <!-- 시스템 --></option>
		                            	</c:forEach>
		                            </optgroup>
		                        </select>
		                        <c:if test="${ui_type eq 'JOOTAEK'}">
		                        <button id ='auto_click' class="btn_search" onclick="searchData('now')">검색</button>
		                        </c:if>
		                        <c:if test="${ui_type ne 'JOOTAEK'}">
		                        <button class="btn_search" onclick="searchData('now')">검색</button>
		                        </c:if>
		                    </div>
	               			<input type="hidden" id="periodType" name="periodType" value="now"/>  
	                    </c:if>
	                    
	                    <!-- 일간 -->
	                    <c:if test="${parameters.periodType eq 'date'}">
		                    <div class="srch_day date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
		                    	<input type="text" class="day search_fr" id="search_from" name="search_from" data-date-format="yyyy-mm-dd" value="${parameters.search_from }"/> ~ <input type="text" class="day search_to" id="search_to" name="search_to" data-date-format="yyyy-mm-dd" value="${parameters.search_to }"/>
		                    	<select class="system" name="system_seq">
		                            <optgroup>
		                           		<option value="all">시스템 전체</option>
		                            	<c:forEach var="sysCode" items="${systemList}">
		                            		<option value="${sysCode.system_seq}" <c:if test='${sysCode.system_seq eq parameters.system_seq}'>selected</c:if>>${sysCode.system_name} <!-- 시스템 --></option>
		                            	</c:forEach>
		                            </optgroup>
		                        </select>
		                        <button class="btn_search" onclick="searchData('date')">검색</button> 
		                    </div> 
	               			<input type="hidden" id="periodType" name="periodType" value="date"/>         
	                    </c:if>
	                    
	                    <!-- 월간 -->
	                    <c:if test="${parameters.periodType eq 'month'}">
		                    <div class="srch_month">
		                        <select class="year" id="year">
		                            <optgroup>
		                            	<c:forEach var="i" begin="${minYear }" end="${nowYear }" step="1">
		                            		<option value="${i }" <c:if test='${parameters.year eq i}'>selected</c:if>>${i }년</option>
		                            	</c:forEach>
		                            </optgroup>
		                        </select>
		                        <select class="month" id="month">
		                            <optgroup>
		                            	<c:forEach var="i" begin="1" end="12" step="1">
		                               		<option value="${i<10 ? '0':'' }${i }" <c:if test='${parameters.month eq i}'>selected</c:if>>${i }월</option>
		                            	</c:forEach>
		                            </optgroup>
		                        </select>   
		                        <select class="system" name="system_seq">
		                            <optgroup>
		                           		<option value="all">시스템 전체</option>
		                            	<c:forEach var="sysCode" items="${systemList}">
		                            		<option value="${sysCode.system_seq}" <c:if test='${sysCode.system_seq eq parameters.system_seq}'>selected</c:if>>${sysCode.system_name} 시스템</option>
		                            	</c:forEach>
		                            </optgroup>
		                        </select>
		                        <input type="hidden" class="search_from" name="search_from"/>
		                        <button class="btn_search" onclick="searchData('month')">검색</button> 
		                    </div>    
	               			<input type="hidden" id="periodType" name="periodType" value="month"/>
	                    </c:if>    
	                </div>
                </form>
            </div> <!-- e:data_option -->
            
            <div class="dashborad-area">
            	<div class="section_data col2">
					<h2>시스템별 이용량 Top5 <span><img src="${rootPath}/resources/image/databoard/ico-info-dash.gif" /> 개인정보취급자가 정보주체의 개인정보를 처리하기 위해 시도한 횟수</span></h2>
                    <div class="chart_area">
                    	<div class="chart1">
                    		<c:if test="${parameters.system_seq ne 'all'}">
                    		<span id="allSystemLink" style="position: absolute; top: 16px; left: 0px; padding: 10px 24px; color:#a9adb1; cursor: pointer;" onclick="allSystemLink()">&lt;전체 시스템&gt;</span>
                        	</c:if>
                        	<span class="info_message">* 그래프 클릭시 해당 시스템으로 검색이 됩니다.</span>
                    		<c:choose>
                    			<c:when test="${!empty systemConnectTop5}">
                    				<%-- <c:forEach var="item" items="${systemConnectTop5}" varStatus="status">
			                    		<div class="donutchart_s">
			                            	<div id="donutchart${status.count }"></div>
			                                <p class="title">${item.system_name }</p>
			                                <p class="data">${item.cnt}</p>
			                            </div>
		                            </c:forEach> --%>
		                            <c:forEach var="item" items="${systemConnectTop5}" varStatus="status">
	                          		<div class="donutchart_s" style="width: 150px; height: 150px;">
		                            	<div id="donutchart_${status.count }" style="width: 150px; height: 150px;" onclick = "listLinkHandler('${item.system_seq}')">
									</div>
										<span class="inner_name">${item.system_name }</span>
										<span class="inner_value"><fmt:formatNumber value="${item.cnt}" pattern="#,###" /></span>
									</div>
		                            </c:forEach>
                    			</c:when>
                    			<c:otherwise>
                    				<div class="donutchart_s" style="width: 100%">
		                                <p class="title" style="width: 100%; margin-top: 52px; font-size: 16px;">로그가 없습니다</p>
		                            </div>
                    			</c:otherwise>
                    		</c:choose>
                        </div>
                    </div>  <!-- e:chart_area -->            
                </div>  <!-- e:section_data -->
                
                <div class="section_data col2">
                <form action=""></form>
					<h2>개인별 개인정보 이용량 Top5 <%-- <span><img src="${rootPath}/resources/image/databoard/ico-condition.gif" /> 2020-03</span> --%></h2>
                    <div class="chart2">
                    	<c:forEach var="i" begin="0" end="4" step="1">
	                    	<div class="block col${i+1 < 3 ? 2:3}">
	                    		<c:choose>
	                    			<c:when test="${empty personalConnectTop5[i]}">
	                    				<span class="ranking">${i+1}</span>
			                            <p class="count"><span>로그 없음</span></p>
			                            <p class="user" id="emp_user_name">
			                            	${personalConnectTop5[i].emp_user_name }
			                            	<span class="user_id" id="emp_user_id">
			                            		${personalConnectTop5[i].emp_user_id }
			                            	</span>
			                            </p>
			                            <span class="dept">
			                            	${empty personalConnectTop5[i].dept_name ? '부서정보없음': personalConnectTop5[i].dept_name}
			                            </span>
	                    			</c:when>
	                    			<c:otherwise>
	                    				<span class="ranking">${i+1}</span>
			                            <a id="personalConnectTop5Button" onclick="moveIndvbyTypeAnals('${personalConnectTop5[i].emp_user_name }','${personalConnectTop5[i].emp_user_id }')">
			                            	<p class="count">
			                            		<c:choose>
			                            			<c:when test="${personalConnectTop5[i].cnt == 0 }">
			                            				<span>로그 없음</span>
			                            			</c:when>
			                            			<c:otherwise>
			                            				<fmt:formatNumber value="${personalConnectTop5[i].cnt }" pattern="#,###" /><span>건</span>
			                            			</c:otherwise>
			                            		</c:choose>
			                            	</p>
			                            	<p class="user" id="emp_user_name">
			                            		${personalConnectTop5[i].emp_user_name }
			                            		<span class="user_id" id="emp_user_id">
			                            			${personalConnectTop5[i].emp_user_id }
			                            		</span>
			                            	</p>
			                            	<span class="dept">
			                            		${empty personalConnectTop5[i].dept_name ? '부서정보없음': personalConnectTop5[i].dept_name}
			                            	</span>
			                           	</a> 
	                    			</c:otherwise>
	                    		</c:choose>
	                        </div>
                        </c:forEach>
                    </div>  <!-- e:chart_area -->            
                </div>  <!-- e:section_data -->
                
                <div class="section_data col3 both">
					<h2>소속별 개인정보 이용 현황</h2>
                    <div class="chart_area">
                    	<div class="chart3">
                        	<ul>
                        		<c:choose>
                        			<c:when test="${!empty deptLogCollect}">
	                        			<c:forEach var="dept" items="${deptLogCollect}" varStatus="status">
			                            	<li>
			                                	<div class="log_data">
			                                    	<span class="ranking">${status.count }.</span>
			                                    	<p class="dept">${dept.dept_name }</p>
			                                    	<p class="count"><span><fmt:formatNumber value="${dept.cnt }" pattern="#,###" /> 건</span> <span class="bf">
			                                    	<c:choose>
			                                    		<c:when test="${parameters.periodType eq 'date' }">
			                                    			<c:choose>
				                                    			<c:when test="${ui_type eq 'Shcd' }">
				                                    				전일동일비교
				                                    			</c:when>
				                                    			<c:otherwise>
					                                    			전년동일비교
				                                    			</c:otherwise>
			                                    			</c:choose>
			                                    		</c:when>
			                                    		<c:otherwise>
			                                    			전월비교 
			                                    		</c:otherwise>
			                                    	</c:choose>
			                                    	${dept.cnt1 }건 (${dept.cnt2<0? '':'+' }${dept.cnt2})</span></p>
			                                    </div>
			                                </li>
		                                </c:forEach>
                        			</c:when>
                        			<c:otherwise>
                        				<li>
		                                	<div class="log_data">
		                                    	<p class="dept" style="width: 100%; text-align: center; margin: 0px">로그가 없습니다</p>
		                                    </div>
		                                </li>
                        			</c:otherwise>
                        		</c:choose>
                            </ul>
                        </div>                    
                    </div>  <!-- e:chart_area -->            
                </div>  <!-- e:section_data -->
                
                <div class="section_data col3">
					<h2>개인정보 유형별 접속기록 현황</h2>
                    <div class="chart_area">
                    	<!-- <div class="chart4">
                        	<div id="donutchart6" style="width: 100%; height: 300px;"></div>
                        </div> -->
                        <div id="chart4" style="width: 100%; height: 400px; top:150px">
						</div>
                    </div>  <!-- e:chart_area -->            
                </div>  <!-- e:section_data -->
                <c:if test="${ui_type eq 'JOOTAEK'}">
      				 <div class="section_data col3">
						<h2>일자별/시스템별 처리 현황</h2>
						<div class="chart_area">
	      				 	<div id="chart_5" style="width: 100%; height: 400px;  top:150px">
	                  			<!--  <div class="chart5">-->
							</div>
						<!-- 인텔리젼스 -일자별/시스템별 처리 현황 -->
							<div class="portlet light portlet-fit portlet-datatable bordered" style="display:none">
								<div class="portlet-body">
									<div class="table-container">
										<div id="datatable_ajax_2_wrapper" class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
											<div>
												<div class="col-md-6" style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;"><!--"display:none"> -->
													<div class="portlet box grey-salsa">	
														<div class="portlet-title" style="background-color:#2B3643;">
															<!--<div class="caption">
															<i class="fa fa-search"></i> 
																검색
															</div>
															 <div class="tools">
								                            	<a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
												            </div> -->
														</div>
														<div class="portlet-body form">
								                                <div class="form-body" style=="padding-left:10px; padding-right:10px;"><!--  "display:none">-->
								                                    <div class="form-group">
								                                    	<form id="listForm" method="POST" class="mt-repeater form-horizontal">
								                                             <div class="row">
									                                            <div class="col-md-2">
									                                            	<label class="control-label">기간</label>
									                                                <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
																						<input type="text" class="form-control" id="search_fr" name="search_from" value="${paramBean.search_from}"> 
																						<span class="input-group-addon"> &sim; </span> 
																						<input type="text" class="form-control" id="search_to" name="search_to" value="${paramBean.search_to}">
																					</div>
									                                            </div>
																					
																				<div class="col-md-2">
																					<label class="control-label">조회대상</label><br/>
																					<select id="searchType"name="searchType" class="ticket-assign form-control">
																						<option value="01">접속기록조회</option>
																						<option value="02">시스템로그조회</option>
																						<option value="03">다운로드 로그조회</option>
																					</select>
																				</div>
																				<div class="col-md-2">
																					<label class="control-label">처리량/이용량</label><br/>
																					<select id="logCtType"name="logCtType" class="ticket-assign form-control">
																						<option value="logCt">처리량</option>
																						<option value="totalCt">이용량</option>
																					</select>
																				</div>
								                                            </div> 
								                                            <hr/>
								                                           <!--  <div align="right">
								                                            	<button class="btn btn-sm blue btn-outline sbold uppercase" type="button" onclick="setchart5()">
																					<i class="fa fa-search"></i> 검색
																				</button>	
								                                            </div> -->
								                                    	</form>
								                                    </div>
								                                </div>
								                            </div> 
														</div>
													</div>
												</div>
											<!--  <div id="chart1"  style="display:none"><!-- "width: 100%; height: 500px;"> -->												
											</div>
										</div>
									</div>
							</div>
						</div>	
					</div>						
					</c:if>
					<!-- 	시스템분석				 -->
					<c:if test="${ui_type ne 'JOOTAEK'}">
     				 <div class="section_data col3">
 						<h2>접속 기록 관리 시스템 정보</h2>	
						<div class="chart_area system_info" style="height: 25px; padding:0px;margin-bottom: 5px;text-align: left"> 
<%-- 						<c:forEach var="system" items="${systemInfo}" varStatus="status"> --%>
<%-- 							<span class="system_info_name --%>
<%-- 								<c:if test="${status.count == 1}">btn_active</c:if> --%>
<%-- 								" onclick="activeSystemInfo('${system.server_seq }',this)">#${system.server_seq } 서버</span> --%>
<%-- 						</c:forEach> --%>
<!-- 					</div> -->
 					<c:choose>
						<c:when test="${empty systemInfo}">
							<div class="chart_area system_info" style="height: 382px;padding: 30px 40px;">
		                    	<h3>분석 시스템 현황</h3>
		                        <div class="chart5" style="padding: 42px 40px;">
		                        	<div>
		                            	<p class="system_name">MEMORY</p>
		                            	<div class="progress">
										  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40"
										  aria-valuemin="0" aria-valuemax="100" style="width:0%; color:black; background-color: #2f8ee2">
										  </div>
										</div>
		                            </div>
		                            <div>
		                            	<p class="system_name">CPU</p>
		                            	<div class="progress">
										  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40"
										  aria-valuemin="0" aria-valuemax="100" style="width:0%; color:black; background-color: #99aaf0">
										  </div>
										</div>
		                            </div>
		                            <div>
		                            	<p class="system_name">HDD</p>
		                            	<div class="progress">
										  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40"
										  aria-valuemin="0" aria-valuemax="100" style="width:0%; color:black; background-color: #72dad1">
										  </div>
										</div>
		                            </div>
		                        </div>
		                    </div>  <!-- e:chart_area -->      
						</c:when>
						<c:otherwise>
							<c:forEach var="system" items="${systemInfo}" varStatus="status">
			                    <div class="chart_area system_info system_chart <c:if test="${status.count != 1}">disable</c:if>" 
			                  	 id="system${system.server_seq }" style="height: 382px;padding: 30px 40px;">
			                    	<h3> <!-- #${system.server_seq }번   -->분석 시스템 현황</h3>
			                        <div class="chart5" style="padding: 42px 40px;">
			                        	<div>
			                            	<p class="system_name">MEMORY</p>
			                            	<div class="progress">
											  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40"
											  aria-valuemin="0" aria-valuemax="100" style="width:${system.cnt2}%; color:black; background-color: #2f8ee2">
											  </div>
											</div>
			                            </div>
			                            <div>
			                            	<p class="system_name">CPU</p>
			                            	<div class="progress">
											  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40"
											  aria-valuemin="0" aria-valuemax="100" style="width:${system.cnt1}%; color:black; background-color: #99aaf0">
											  </div>
											</div>
			                            </div>
			                            <div>
			                            	<p class="system_name">HDD</p>
			                            	<div class="progress">
											  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40"
											  aria-valuemin="0" aria-valuemax="100" style="width:${system.cnt3}%; color:black; background-color: #72dad1">
											  </div>
											</div>
			                            </div>
			                        </div>
			                    </div>  <!-- e:chart_area -->       
		                    </c:forEach>
						</c:otherwise>
					</c:choose>
                </div>  <!-- e:section_data -->  
               </c:if>
                
            </div>  <!-- e:dashborad-area -->
            <c:set var="nothingData1" value="${systemConnectTop5[0].cnt1-systemConnectTop5[0].cnt}"/>
            <c:set var="nothingData2" value="${systemConnectTop5[1].cnt1-systemConnectTop5[1].cnt}"/>
            <c:set var="nothingData3" value="${systemConnectTop5[2].cnt1-systemConnectTop5[2].cnt}"/>
            <c:set var="nothingData4" value="${systemConnectTop5[3].cnt1-systemConnectTop5[3].cnt}"/>
            <c:set var="nothingData5" value="${systemConnectTop5[4].cnt1-systemConnectTop5[4].cnt}"/>
        </div> <!-- e:contents -->
    </div> <!-- e:container -->
</div><!-- e:wrapper -->

<form action="${rootPath}/dashboard/init_dashboardView.html" id="listForm" method="post">
	<input type="hidden" id="link_search_from" name="search_from"/>
	<input type="hidden" id="link_search_to" name="search_to"/>
	<input type="hidden" id="link_periodType" name="periodType"/>
	<input type="hidden" id="link_system_seq" name="system_seq"/>
	<input type="hidden" id="link_emp_user_name" name="emp_user_name"/>
	<input type="hidden" id="link_emp_user_id" name="emp_user_id"/>
	<input type="hidden" id="link_flag" name="flag" value="fromDatabase"/>
</form>


<script type="text/javascript" src="${rootPath}/resources/js/databoard/loader.js"></script>
<script type="text/javascript">

//9분59초마다 실시간 조회버튼 클릭
setInterval(function() {document.getElementById('auto_click').click();},599000);

function moveIndvbyTypeAnals(emp_user_name, emp_user_id){
	var periodType = $('#periodType').val();
	var nowDate = new Date();
	if(periodType == 'date'){
		var search_from = $('#search_from').val();
		var search_to = $('#search_to').val();
	}else if(periodType == 'month'){
		var search_from =$('#year').val()+'-'+$('#month').val()+'-'+'01';
		var search_to = new Date($('#year').val(),$('#month').val(),0).format('yyyy-MM-dd');
		if($('#year').val() == nowDate.getFullYear() && $('#month').val()==(nowDate.getMonth()+1)){
			search_to = new Date($('#year').val(),$('#month').val()-1,nowDate.getDate()-1).format('yyyy-MM-dd');
		}
	}
	
	if(emp_user_name == "" || emp_user_name == null) return false;	
	$('#link_search_from').val(search_from);
	$('#link_search_to').val(search_to);
	
	$('#link_emp_user_name').val(emp_user_name);
	$('#link_emp_user_id').val(emp_user_id);
	
// 	location.href="${rootPath}/statistics/list_indvbyTypeAnals.html?search_from="+search_from+"&search_to="+search_to+"&emp_user_name="+emp_user_name+"&emp_user_id="+emp_user_id;
	$("#listForm").attr("action",databoardConfig["listUrl"]);
	$("#listForm").submit();
}
$(document).ready(function(){
	setchartTotal();
 	setchart1();
	setchart2();
	setchart3();
	setchart4();
 	setchart5();
	setchart_5();

});

  function allSystemLink(){
	  var periodType = $('#periodType').val();
		if(periodType == 'now'){
	        $('#link_search_from').val('${nowDate}');
	        $('#link_search_to').val('${nowDate}');
	        $('#personalConnectTop5Button').unbind('click');
		}else if(periodType == 'date'){
	        $('#link_search_from').val($('#search_from').val());
	        $('#link_search_to').val($('#search_to').val());
		}else if(periodType == 'month'){
			$('#link_search_from').val($('#year').val()+'-'+$('#month').val()+'-'+'01');
	        $('#link_search_to').val(new Date($('#year').val(),$('#month').val(),0).format('yyyy-MM-dd'));
		}
		$('#link_periodType').val(periodType);
    $('#link_system_seq').val('all');
    $('#listForm').submit();
}

function listLinkHandler(system_seq){
		var periodType = $('#periodType').val();
		if(periodType == 'now'){
	        $('#link_search_from').val('${nowDate}');
	        $('#link_search_to').val('${nowDate}');
		}else if(periodType == 'date'){
	        $('#link_search_from').val($('#search_from').val());
	        $('#link_search_to').val($('#search_to').val());
		}else if(periodType == 'month'){
			$('#link_search_from').val($('#year').val()+'-'+$('#month').val());
		}
		$('#link_periodType').val(periodType);
        $('#link_system_seq').val(system_seq);
        $('#listForm').submit();
}

function setchartTotal() {
	var dataProvider = new Array();
	<c:forEach var="item" items="${arr }">
		var value = {
			"privacy_desc" : "${item[0]}",
			"nPrivCount" : ${item[1]}
		};
		dataProvider.push(value);
	</c:forEach>
	drawchartTotal(dataProvider);
}

function setchart1(){
	var dataProvider = new Array();
	var value = {
		"privacy_desc" : "${systemConnectTop5[0].system_name }",
		"nPrivCount" : ${!empty systemConnectTop5[0].cnt ? systemConnectTop5[0].cnt:0}
	};	
	dataProvider.push(value);
	value = {
		"privacy_desc" : "${item.system_name[0] }",
		"nPrivCount" : ${!empty nothingData1?nothingData1:0}
	};
	dataProvider.push(value);
	drawchart1(dataProvider);
}

function setchart2(){
	var dataProvider = new Array();
	var value = {
		"privacy_desc" : "${systemConnectTop5[1].system_name }",
		"nPrivCount" : ${!empty systemConnectTop5[1].cnt ? systemConnectTop5[1].cnt:0}
	};	
	dataProvider.push(value);
	value = {
		"privacy_desc" : "${item.system_name[1] }",
		"nPrivCount" : ${!empty nothingData2?nothingData2:0}
	};
	dataProvider.push(value);
	drawchart2(dataProvider);
}

function setchart3(){
	var dataProvider = new Array();
	var value = {
		"privacy_desc" : "${systemConnectTop5[2].system_name }",
		"nPrivCount" : ${!empty systemConnectTop5[2].cnt ? systemConnectTop5[2].cnt:0}
	};	
	dataProvider.push(value);
	value = {
		"privacy_desc" : "${item.system_name[2] }",
		"nPrivCount" : ${!empty nothingData3?nothingData3:0}
	};
	dataProvider.push(value);
	drawchart3(dataProvider);
}

function setchart4(){
	var dataProvider = new Array();
	var value = {
		"privacy_desc" : "${systemConnectTop5[3].system_name}",
		"nPrivCount" : ${!empty systemConnectTop5[3].cnt ? systemConnectTop5[3].cnt:0}
	};	
	dataProvider.push(value);
	value = {
		"privacy_desc" : "${item.system_name[3] }",
		"nPrivCount" : ${!empty nothingData4?nothingData4:0}
	};
	dataProvider.push(value);
	drawchart4(dataProvider);
}

 function setchart5(){
	var dataProvider = new Array();
	var value = {
		"privacy_desc" : "${systemConnectTop5[4].system_name }",
		"nPrivCount" : ${!empty systemConnectTop5[4].cnt ? systemConnectTop5[4].cnt:0}
	};	
	dataProvider.push(value);
	value = {
		"privacy_desc" : "${item.system_name[4] }",
		"nPrivCount" : ${!empty nothingData5?nothingData5:0}
	};
	dataProvider.push(value);
	drawchart5(dataProvider);
}
 
var databoardConfig = {
		"listUrl":"${rootPath}/statistics/list_indvbyTypeAnals.html"
	};


/* window.onload = function() {
	 setchart5();
} */
//pivotchart1.jsp
function setchart_5 () {
	var startdate = $('#search_fr').val();
	var enddate = $('#search_to').val();
	var searchType = $('#searchType').val();
	var logCtType = $('#logCtType').val();
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/new_dashboard_chart1.html',
		data: { 
			"startdate" : startdate,
			"enddate" : enddate,
			"searchType": searchType,
			"logCtType" : logCtType
		},
		success: function(data) {
			drawchart_5(data);
		},
		beforeSend:function() {
			$('#loading').show();
		},
		complete:function() {
			$('#loading').hide();
		}
	});
}

function drawchart_5 (data) {
	
	var dataProvider = new Array();
	var graphs = new Array();
	
	var jsonData = JSON.parse(data);
	
	var  valueTemp="";
	var  nameTemp="";
	
	for (var i = 0; i < jsonData.length; i++) {
		dataProvider.push(jsonData[i].dataProvider);
		
	}
	
	if ( jsonData.length > 0) {
		var graphsTemp = jsonData[0].graphs;
		
		for (var j = 0; j < graphsTemp.length; j++) {
			graphs.push(graphsTemp[j]);
		}
	}
	
	/*dashboard_amchart_1*/
	var chart_5 = AmCharts.makeChart("chart_5", {
	    "type": "serial",
	    "theme": "light",
	    "legend": {
		    
	        "useGraphSettings": true,
	        "position": "top",
	        "color" : "#ffffff" 
	 		        
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "integersOnly": true,
	        "reversed": false,
	        "axisAlpha": 0,
	        "dashLength": 5,
	        "gridCount": 10,
	        "position": "left",
	        "title": "개인정보검출(건)",
		    "color" : "#ffffff",
		    "titleColor" : "#ffffff"  
	    }],
	    "startDuration": 0,
	    "sequencedAnimation": false,
	    "startEffect": "easeOutSine",
	    "graphs": graphs,
	    "chartCursor": {
	        "cursorAlpha": 0,
	        "zoomable": false
	    },
	    "categoryField": "proc_date",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "fillAlpha": 0.05,
	        "fillColor": "#000000",
	        "gridAlpha": 0,
	        "position": "bottom",
        	"color" : "#ffffff" 
	    },
	    "export": {
	    	"enabled": false,
	        "position": "bottom-right"
	     }
	});
}

</script>

</body>
</html>