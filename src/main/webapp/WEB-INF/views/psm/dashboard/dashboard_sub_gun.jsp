<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<link rel="stylesheet" type="text/css" href="${rootPath}/resources/css/common/eight_dash.css" />


<form id = "dashForm" method="POST">
<input type="hidden" id="select_system_seq" name="select_system_seq" value="all" /> 

<div class="chart_01 col2_2" onclick="dashboardPop('type0')">
	<h2>개인정보 접속기록 건수</h2>
	<div id="chart1" class="chart_area">
		<div class="messageDiv" onclick="dashboardPop('type0')"></div>
	</div> <!-- e:chart_area -->
</div> <!-- e:chart_01 -->
  
<div class="chart_nav" id="dash_menu">
	<ul>
		<li class="nav_all_selected nav_all" id="all"><a>전체 시스템</a></li>
		<c:forEach items="${systemMasterList}" var="system" varStatus="i">
			<li class="nav_sys_noSelected nav_sys" id="${system.system_seq }"><a>${system.system_name }</a></li>
		</c:forEach>
	</ul>
</div> <!-- e:chart_nav -->

<div class="chart_02 col2_2" onclick="dashboardPop('type1')">
	<h2>실시간 개인정보 접속기록 현황</h2>
	<div id="chart2" class="chart_area" style="padding: 0; overflow:auto;">
		<div class="chart_table">
			<table id="bizTbl" border="0" cellspacing="0" cellpadding="0">
				<colgroup>
					<col width="*%" />
					<col width="15%" />
					<col width="10%" />
					<col width="15%" />
					<col width="20%" />
					<col width="10%" />
				</colgroup>
				<thead>
					<tr>
						<th style="text-align: center;">일시</th>
						<th style="text-align: center;">소속</th>
						<th style="text-align: center;">ID</th>
						<th style="text-align: center;">사용자명</th>
						<th style="text-align: center;">사용자IP</th>
						<th style="text-align: center;">시스템명</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${rescentList}" var="rescentList">
						<tr style='background: #f8f8f8;'>
							<fmt:parseDate var="dateString" value="${rescentList.proc_date}" pattern="yyyyMMddHHmmss" />
							<td><fmt:formatDate value="${dateString}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
							<td>${rescentList.dept_name}</td>
							<td>${rescentList.emp_user_id}</td>
							<td>${rescentList.emp_user_name}</td>
							<td>${rescentList.user_ip}</td>
							<td>${rescentList.system_name}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div> <!-- e:chart_area -->
</div> <!-- e:chart_02 -->
<input type="hidden" id="rescentList" name="rescentList" value="${rescentList}" />
<div class="chart_03 col4">	
	<h2 style="float:left;">시스템별 접속기록 TOP10</h2>
 <!-- 	<div class="dash_date dash_date1" id="chart3_css">
	   <input name="search_from" type="text" class="calender search_fr ch3From" value="" onchange="getChart3('');"/>
	   <i class="sim">&sim;</i>
	   <input name="search_to" type="text" class="calender search_to ch3To" value="" onchange="getChart3('');"/>
	</div>  --> 
	<div id="chart3" class="chart_area pointer_ds" onclick="dashboardPop('type2')">
	</div> <!-- e:chart_area -->
</div> <!-- e:chart_03 -->

<div class="chart_04 col4"  >
	<h2 style="float:left;">개인정보 유형별 접속기록 현황</h2>
<%--   	<div class="dash_date dash_date1" id="chart4_css">
	   <input name="search_from" type="text" class="calender search_fr ch4From" value="${paramBean.search_from}" onchange="getChart4('');"/>
	   <i class="sim">&sim;</i>
	   <input name="search_to" type="text" class="calender search_to ch4To" value="${paramBean.search_to}" onchange="getChart4('');"/>
	</div>  --%> 
	<div id="chart4" class="chart_area pointer_ds" onclick="dashboardPop('type3')">
	</div> <!-- e:chart_area -->
</div> <!-- e:chart_04 -->

<div class="chart_05 col4">
	<h2 style="float:left;">비정상행위 추이분석</h2>
<!--  	<div class="dash_date dash_date1" id="chart5_css">
	   <input name="search_from" type="text" class="calender search_fr ch5From"  onchange="getChart5('');"/>
	   <i class="sim">&sim;</i>
	   <input name="search_to" type="text" class="calender search_to ch5To"  onchange="getChart5('');"/>
	</div>   -->
	<div id="chart5" class="chart_area pointer_ds" onclick="dashboardPop('type4')">
	</div> <!-- e:chart_area -->
</div> <!-- e:chart_05 -->


<div class="chart_07 col4" >
	<h2 style="float:left;">소속별 로그 수집 현황</h2>
<!--  	<div class="dash_date dash_date1" id="chart7_css">
	   <input name="search_from" type="text" class="calender search_fr ch7From"onchange="getChart7('');"/>
	   <i class="sim">&sim;</i>
	   <input name="search_to" type="text" class="calender search_to ch7To" onchange="getChart7('');"/>
	</div>  --> 
	<div id="chart7" class="chart_area pointer_ds" style="padding: 0;" onclick="dashboardPop('type6')">
		<div class="chart_table">
			<table id="deptTopTbl" border="0" cellspacing="0" cellpadding="0">
				<colgroup>
					<col width="15%" />
					<col width="35%" />
					<col width="25%" />
					<col width="25%" />
				</colgroup>				
				<thead>
					<tr>
						<th>순위</th>
						<th>소속</th>
						<th>로그 수</th>
						<th>개인정보 수</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
		</div>
	</div> <!-- e:chart_area -->
</div> <!-- e:chart_07 -->

<div class="chart_06 col4">
	<h2 style="float:left;">비정상행위별 분석현황</h2>
<!--  	<div class="dash_date dash_date1" id="chart6_css">
	   <input name="search_from" type="text" class="calender search_fr ch6From" onchange="getChart6('');" />
	   <i class="sim">&sim;</i>
	   <input name="search_to" type="text" class="calender search_to ch6To" onchange="getChart6('');"/>
	</div>  -->
	<div id="chart6" class="chart_area pointer_ds" onclick="dashboardPop('type5')">
	</div> <!-- e:chart_area -->
</div> <!-- e:chart_06 -->

<div class="chart_08 col4" onclick="dashboardPop('type7')">
	<h2>시스템정보</h2>
	<div id="chart8" class="chart_area pointer_ds">
	</div> <!-- e:chart_area -->
</div> <!-- e:chart_08 -->

<input type ="hidden" name = "dstype"/>
<input type ="hidden" name = "pop_from"/>
<input type ="hidden" name = "pop_to"/>
</form>
