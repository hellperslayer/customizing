<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!-- <link rel="stylesheet" type="text/css" -->
<%-- 	href="${rootPath}/resources/css/common/eight_dash.css" /> --%>

<!-- <meta http-equiv="X-UA-Compatible" content="IE=8" /> -->
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin Theme #1 for dark mega menu option"
	name="description" />
<meta content="" name="author" />




<div class="tab-pane <c:if test="${tabFlag == 'tab2' }">active</c:if>" id="tab2">		    	
	<div class="row">  
	    <div class="col-lg-12">
	    	<div class="portlet light bordered">
	            <div class="portlet-title">
	                <div class="caption">
	                    <i class="icon-bar-chart font-green-haze"></i>
	                    <span class="caption-subject bold uppercase font-green-haze">오남용 의심행위자 분석</span>			                        
	                </div>		
	                <div class="actions">		                        
	                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#"> </a>
	                </div>	                    
	            </div>
	            <div class="portlet-body">
	                <div id="dashboard_amchart_10" class="chart"></div>
	            </div>
	        </div>
	    </div>			        
	   </div>
	   <div class="row">
	   	<div class="col-lg-12">
	        <div class="portlet light bordered">
	            <div class="portlet-title">
	                <div class="caption">
	                    <i class="icon-bar-chart font-green-haze"></i>
	                    <span class="caption-subject bold uppercase font-green-haze">소속별 위험도 분석</span>			                        
	                </div>	
	                <div class="actions">		                        
	                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#"> </a>
	                </div>		                    
	            </div>
	            <div class="portlet-body">
	                <div id="dashboard_amchart_11" class="chart"></div>
	            </div>       
	        </div>
	   	</div>
	   </div>
	   <div class="row">		        	
	   	<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
	        <div class="portlet light bordered">
	            <div class="portlet-title">
	                <div class="caption">
	                    <i class="icon-bar-chart font-green-haze"></i>
	                    <span class="caption-subject bold uppercase font-green-haze">유형별 위험도 현황</span>			                        
	                </div>
	                <div class="actions">		                        
	                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#"> </a>
	                </div>			                    
	            </div>
	            <div class="portlet-body">
	                <div id="dashboard_amchart_12" class="chart"></div>
	            </div>
	        </div>
	   	</div>
	   	<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
	        <div class="portlet light bordered">
	            <div class="portlet-title">
	                <div class="caption">
	                    <i class="icon-bar-chart font-green-haze"></i>
	                    <span class="caption-subject bold uppercase font-green-haze">개인정보 처리량 예측</span>			                        
	                </div>	
	                <div class="actions">		                        
	                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#"> </a>
	                </div>		                    
	            </div>
	            <div class="portlet-body">
	                <div id="dashboard_amchart_13" class="chart"></div>
	            </div>
	        </div>
	   	</div>
   </div>
	      
</div>
