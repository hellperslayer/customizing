<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8">
	<title>psm대시보드</title>
	<c:set var="rootPath" value="${pageContext.servletContext.contextPath}" scope="application"/>
	
	<script type="text/javascript" charset="UTF-8">
  		rootPath = '${rootPath}';
 	</script>
 	
	<link href="${rootPath}/resources/dashboard/css/con_style.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
	
	<script src="${rootPath}/resources/js/jquery.min.js" type="text/javascript"></script>
	
	
	<!-- AMCHART SCRIPT START-->
	<script src="${rootPath}/resources/js/amcharts.js"></script>
	<script src="${rootPath}/resources/js/serial.js"></script>
	<script src="${rootPath}/resources/js/export.min.js"></script>
	<script src="${rootPath}/resources/js/amstock.js"></script>
	<script src="${rootPath}/resources/js/light.js"></script>
	<script src="${rootPath}/resources/js/gauge.js"></script>
	<script src="https://www.amcharts.com/lib/3/themes/dark.js"></script>
	
	<link rel="stylesheet" href="${rootPath}/resources/css/export.css" type="text/css" media="all">
	<script type="text/javascript" src="${rootPath}/resources/js/dashboard/dashboard_map.js?ver=2"></script>
	<!-- AMCHART SCRIPT END -->
</head>
<style>
body{
/*  font-size: 8px; */
/*  font-weight: bold; */
/* text-align: center; */
}
</style>


<script type="text/javascript">
/* $(function(){
	var locArr=new Array();
	var contentT=new Array();
	var content=new Array("중구", "서구", "동구", "영도구", "부산진구", "동래구", "남구", "북구", "해운대구", "사하구", "금정구", "강서구", "연제구", "수영구", "사상구", "기장군");
	for(var c=1;c<17;c++){
		locArr.push("#loc"+c);
		//contentT.push("개인정보 처리 위험도 \"관심\"단계");
	}
	var befDot;
	var txtForm=$("<textarea></textarea>").attr({"cols":10,"rows":1,"id":"txt","readonly":"readonly"}).css({"position":"absolute","background":"transparent","border":"none","overflow-y":"hidden","color":"#BBE075","font-weight":"bold","font-family":"Malgun Gothic","font-size":"15px","z-index":"111","text-align":"center"}).text(contentT[0]+"\r\n"+contentT[0]+"\r\n"+contentT[0]+"\r\n"+contentT[0]+"\r\n"+contentT[0]+"\r\n"+contentT[0]);
	var txtCont=$("<textarea></textarea>").attr({"cols":30,"rows":7,"id":"txtCon","readonly":"readonly"}).css({"position":"absolute","background":"transparent","border":"none","overflow-y":"hidden","z-index":"105","font-size":"7px","font-weight":"bold","color":"white","font-family":"Malgun Gothic"}).text(contentT[0]);
	var nowLoc=$(locArr[0]);
	var accentImg=$("#accent").css({"position":"absolute","top":nowLoc.position().top-14.5,"left":nowLoc.position().left-14,"z-index":"90","display":"block"});;
	var infoBox=$("#infoBox").css({"position":"absolute","top":nowLoc.position().top-80,"left":nowLoc.position().left-270,"display":"block","z-index":"100"});
	var i=1;
	$(accentImg).css({"top":nowLoc.position().top-14.5,"left":nowLoc.position().left-14,"z-index":"90","display":"block"});
	$(infoBox).appendTo("#info");
	$("#info").append($(txtForm).css({"top":nowLoc.position().top-59,"left":nowLoc.position().left-85}).text(content[0]));
	$("#info").append($(txtCont).css({"top":nowLoc.position().top-74,"left":nowLoc.position().left-262}).text(contentT[0]+"\r\n"+contentT[0]+"\r\n"+contentT[0]+"\r\n"+contentT[0]+"\r\n"+contentT[0]+"\r\n"+contentT[0]));
	setInterval(function(){
		nowLoc=$(locArr[i]);
		$("#txt").remove();
		//$("#txtCon").remove();
		$(accentImg).css({"top":nowLoc.position().top-14.5,"left":nowLoc.position().left-14,"z-index":"90","display":"block"});
		$(infoBox).css({"position":"absolute","top":nowLoc.position().top-80,"left":nowLoc.position().left-270,"display":"block","z-index":"100"}).appendTo("#info");
		$("#info").append($(txtForm).css({"top":nowLoc.position().top-59,"left":nowLoc.position().left-85}).text(content[i]));
		$("#info").append($(txtCont).css({"top":nowLoc.position().top-74,"left":nowLoc.position().left-262}));
		if(i>=15)
		{
			i=0;
		}else {
			i++;
		}
	}, 5000);
}) */
</script>
<body>

<!-- wrap -->
<div id="wrap">
	<!-- container -->
	<div id="container" class="dashboard">
		<div class="content">
			
				<!--컨텐츠박스 S-->
			<div id="con01">
				<div id="con_date"></div>
				<div id="con_time"></div>
			</div>
			<!--컨텐츠박스 E-->
			

			<!--컨텐츠박스 S2-->
			<div id="con02">
				<div id="con_city">부산광역시</div>
				<div id="con_attention"></div>
					<div id="con_expression">
					</div>
				<%-- <div id="con_map" style="background-image: url('${rootPath}/resources/dashboard/images/map/youngdo.png');"> --%>
				<div id="con_map" style="position: relative;">	
				<div id="info">
				<img width="20px" id="infoBox" src="${rootPath}/resources/dashboard/images/map_text_left.png" style="width: 260px;height: 93px;display: none;">
				</div>
					<img id="accent" src="${rootPath}/resources/dashboard/images/main.png" style="width: 49px;height: 48px;display: none;">
					<img width="20px" id="loc12" src="${rootPath}/resources/dashboard/images/r2.png" style="position:absolute;top: 126px;left: 101px;">
					<img width="20px" id="loc11" src="${rootPath}/resources/dashboard/images/r2.png" style="position:absolute;top: 143px;left: 131px;">
					<img width="20px" id="loc16" src="${rootPath}/resources/dashboard/images/r2.png" style="position:absolute;top: 146px;left: 71px;">
					<img width="20px" id="loc9" src="${rootPath}/resources/dashboard/images/r2.png" style="position:absolute;top: 133px;left: 55px;">
					<img width="20px" id="loc6" src="${rootPath}/resources/dashboard/images/r2.png" style="position:absolute;top: 167px;left: 95px;">
					<img width="20px" id="loc8" src="${rootPath}/resources/dashboard/images/r2.png" style="position:absolute;top: 167px;left: 95px;">
					<img width="20px" id="loc15" src="${rootPath}/resources/dashboard/images/r2.png" style="position:absolute;top: 167px;left: 95px;">
					<img width="20px" id="loc5" src="${rootPath}/resources/dashboard/images/r2.png" style="position:absolute;top: 161px;left: 168px;">
					<img width="20px" id="loc13" src="${rootPath}/resources/dashboard/images/r2.png" style="position:absolute;top: 161px;left: 168px;">
					<img width="20px" id="loc14" src="${rootPath}/resources/dashboard/images/r2.png" style="position:absolute;top: 201px;left: 132px;">
					<img width="20px" id="loc7" src="${rootPath}/resources/dashboard/images/r2.png" style="position:absolute;top: 201px;left: 132px;">
					<img width="20px" id="loc3" src="${rootPath}/resources/dashboard/images/r2.png" style="position:absolute;top: 243px;left: 169px;">
					<img width="20px" id="loc1" src="${rootPath}/resources/dashboard/images/r2.png" style="position:absolute;top: 243px;left: 169px;">
					<img width="20px" id="loc2" src="${rootPath}/resources/dashboard/images/r2.png" style="position:absolute;top: 243px;left: 169px;">
					<img width="20px" id="loc4" src="${rootPath}/resources/dashboard/images/r2.png" style="position:absolute;top: 124px;left: 160px;">
					<img width="20px" id="loc10" src="${rootPath}/resources/dashboard/images/r2.png" style="position:absolute;top: 124px;left: 160px;">
				</div>
			</div>
			<!--컨텐츠박스 E2-->
			
			
			<!--컨텐츠박스 S3-->
			<div id="con03">
				<!--con03_1 S-->
				<div id="con03_1">
					<!--con03_S-->
					<div id="con03_s">
						<!--table-block S-->
						<div class="table-block" style="margin-top:108px; width:95%; padding-left:50px;">								
								<!-- table -->
								<table class="tbl-col" id="bizTbl">
									<colgroup>
										<col style="width:5%">
										<col style="width:15%">
										<col style="width:auto">
									</colgroup>
									<!--
									<thead>
										<tr>
											<th>순위</th>
											<th>이름</th>
											<th>부서</th>
										</tr>
									</thead>
									-->
									<tbody>
										<!--
										<tr>
											<td>1</td>
											<td>김이지</td>
											<td class="ta-l">솔루션개발본부</td>
											<td>100</td>
											<td>123456-1234567</td>
											<td class="ta-l">010-111-2222</td>
											<td class="ta-l">easycerti@naver.com</td>
										</tr>
										-->
									</tbody>
								</table>
								<!-- //table -->

								
						</div>
						<!--table-block E-->
					</div>
					<!--con03_E-->
					<!--con03_s2 S-->
					<div id="con03_s2">
						<div id="con03_box"></div>
						<div id="con03_box2"></div>
					</div>
					<!--con03_s2 E-->
				</div>
				<!--con03_1 E-->
				<!--con03_2 S-->
				<div id="con03_2">
						<!--table-block S-->
						
						<div class="table-block" style="margin-top:108px; width:95%; padding-left:50px;">								
								<div style="float:right"><!-- <img src="../images/more.png" width="32" height="12" border="0" alt=""> --></div>
								<!-- table -->
								<table class="tbl-col" id="bizTbl2">
									<colgroup>
										<col style="width:5%">
										<col style="width:15%">
										<col style="width:15%">
										<col style="width:15%">
										<col style="width:20%">
										<col style="width:15%">
										<col style="width:auto">
									</colgroup>
									<thead>
										<tr>
											<th>순위</th>
											<th>이름</th>
											<th>부서</th>
											<th>합계</th>
											<th>주민번호</th>
											<th>휴대번호</th>
											<th>이메일주소</th>
										</tr>
									</thead>
									<tbody>
										<!--
										<tr>
											<td>1</td>
											<td>김이지</td>
											<td class="ta-l">솔루션개발본부</td>
											<td>100</td>
											<td>123456-1234567</td>
											<td class="ta-l">010-111-2222</td>
											<td class="ta-l">easycerti@naver.com</td>
										</tr>
										-->
										
									</tbody>
								</table>
								<!-- //table -->

								
						</div>
						<!--table-block E-->
				</div>
				<!--con03_2 E-->
			</div>
			<!--컨텐츠박스 E3-->
		
		</div>
		<!-- //content -->
	</div>
	<!-- //container -->
	
		
</div>
<!-- //wrap -->

</body>
</html>

