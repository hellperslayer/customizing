<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="X-UA-Compatible" content="IE=9">
<c:set var="rootPath" value="${pageContext.servletContext.contextPath}" scope="application"/>
<c:set var="currentMenuId" value="MENU00031" />
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<script type="text/javascript" charset="UTF-8">
	  		rootPath = '${rootPath}';
	  		contextPath = '${pageContext.servletContext.contextPath}';
</script>

<style>
	#graph { float: left; width: 100%; height: 700px; overflow: hidden; }
</style>

<link rel="stylesheet" type="text/css" href="${rootPath}/resources/css/common/dash_popup.css" />
<link rel="stylesheet" href="${rootPath}/resources/css/common/jquery-ui.css" />
<script src="${rootPath}/resources/js/common/jquery-1.8.2.min.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/menu-util.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/ajax-util.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/jquery-ui-1.9.0.custom.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/jquery.ui.datepicker-ko.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/common-prototype-util.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/spin.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/jquery.form.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/dashboard/highcharts.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/exporting.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/globalize.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/knockout-3.0.0.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/dx.chartjs.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/zoomingData.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/dashPopList.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>


<form id="dashboardForm" method="POST">
	<input type="hidden" name="main_menu_id" value="${mainMenuId }" />
	<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
	<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
	<input type="hidden" name="page_num" />
</form>

<form id="popListForm" method="POST">
<div class="chart_02 col100">
	<h2>최근 수집 로그 정보</h2>
	<div id="chart2" class="chart_area" style="padding: 0;">
		<div class="chart_table">
			<table id="bizTbl" border="0" cellspacing="0" cellpadding="0">
				<colgroup>
					<col width="5%" />
					<col width="10%" />
					<col width="10%" />
					<col width="15%" />
					<col width="20%" />
					<col width="10%" />
					<col width="10%" />
					<col width="10%" />
				</colgroup>
				<thead>
					<tr>
						<th>ID</th>
						<th>유형</th>
						<th>날짜</th>
						<th>이름</th>
						<th>좌표</th>
						<th>설명</th>
						<th>수정</th>
						<th>삭제</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${items}" var="list" varStatus="status">
						<tr style='background: #f8f8f8;'>
							<td>${list.coord_id}</td>
							<td><select name="coord_type${status.count}">
									<option value ="0" <c:if test="${list.coord_type eq 0}">selected="selected"</c:if>>BMT</option>
									<option value ="1" <c:if test="${list.coord_type eq 1}">selected="selected"</c:if>>시연</option>
								</select>
							</td>
							<td><input type="text" name="coord_date${status.count}" class="calender" value="${list.coord_date}" style="width:100px;"/></td>						
							<td><input type="text" name="coord_name${status.count}" value="${list.coord_name}" style="width:100px;"/></td>
							<td><input type="text" name="coordinate${status.count}" value="${list.coordinate}" /></td>
							<td><input type="text" name="coord_memo${status.count}" value="${list.coord_memo}" style="width:100px;"/></td>
							<td><p onclick ="javascript:updateCoord(${list.coord_id},${status.count})" style="cursor: pointer;">수정</p></td>
							<td><p onclick ="javascript:deleteCoord(${list.coord_id})" style="cursor: pointer;">삭제</p></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div> <!-- e:chart_area -->
</div> <!-- e:chart_02 -->
<input type="hidden" name="coord_date"/>
<input type="hidden" name="coord_name"/>
<input type="hidden" name="coordinate"/>
<input type="hidden" name="coord_type"/>
<input type="hidden" name="coord_memo"/>
<input type="hidden" name="coord_id"/>
</form>

<script type="text/javascript">
	
var dashboardPopConfig = {
		"dashboardUrl":"${rootPath}/dashboard/dashboard.html"
		,"urlList":"${rootPath}/allLogInq/popupList.html"
		,"saveUrl":"${rootPath}/allLogInq/updateGoogle.html"
		,"removeUrl":"${rootPath}/allLogInq/deleteGoogle.html"
	};		
 	
</script>

</html>