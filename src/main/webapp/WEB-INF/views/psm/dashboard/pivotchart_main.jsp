<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>

<c:set var="currentMenuId" value="${index_id}" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>
	
	
<style>
#loading {
 width: 100%;   
 height: 100%;   
 top: 0px;
 left: 0px;
 position: fixed;   
 display: block;   
 opacity: 0.7;   
 background-color: #fff;   
 z-index: 99;   
 text-align: center; }  
  
#loading-image {   
 position: absolute;   
 top: 50%;   
 left: 50%;  
 z-index: 100; } 
</style>

<script type="text/javascript">
$(window).load(function() {    
	$('#loading').hide();
});
</script>

<div id="loading"><img id="loading-image" width="30px;" src="${rootPath}/resources/image/loading3.gif" alt="Loading..." /></div>


<form id="pivotchartForm" method="POST">
	<input type="hidden" name="main_menu_id" value="${mainMenuId }" />
	<input type="hidden" name="sub_menu_id" value="${parambean.sub_menu_id }" />
	<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
</form>

<script>
//var startdate = "${parambean.search_from}";
//var enddate = "${parambean.search_to}";

function moveallLogInqList() {
	$("#listForm").attr("action",url);
	$("#listForm").submit();
}


</script>

<%-- <h1 class="page-title">${currentMenuName}</h1> --%>

<c:choose>
	<c:when test="${type == 1 }"><%@include file="/WEB-INF/views/psm/dashboard/pivotchart/pivotchart1.jsp"%></c:when>
	<c:when test="${type == 2 }"><%@include file="/WEB-INF/views/psm/dashboard/pivotchart/pivotchart2.jsp"%></c:when>
	<c:when test="${type == 3 }"><%@include file="/WEB-INF/views/psm/dashboard/pivotchart/pivotchart3.jsp"%></c:when>
	<c:when test="${type == 4 }"><%@include file="/WEB-INF/views/psm/dashboard/pivotchart/pivotchart4.jsp"%></c:when>
	<c:when test="${type == 5 }"><%@include file="/WEB-INF/views/psm/dashboard/pivotchart/pivotchart5.jsp"%></c:when>
	<c:when test="${type == 6 }"><%@include file="/WEB-INF/views/psm/dashboard/pivotchart/pivotchart6.jsp"%></c:when>
	<c:when test="${type == 7 }"><%@include file="/WEB-INF/views/psm/dashboard/pivotchart/pivotchart7.jsp"%></c:when>
	<c:when test="${type == 8 }"><%@include file="/WEB-INF/views/psm/dashboard/pivotchart/pivotchart8.jsp"%></c:when>
	<c:when test="${type == 9 }"><%@include file="/WEB-INF/views/psm/dashboard/pivotchart/pivotchart9.jsp"%></c:when>
	<c:when test="${type == 10 }"><%@include file="/WEB-INF/views/psm/dashboard/pivotchart/pivotchart10.jsp"%></c:when>
	<c:when test="${type == 11 }"><%@include file="/WEB-INF/views/psm/dashboard/pivotchart/pivotchart11.jsp"%></c:when>
	<c:when test="${type == 12 }"><%@include file="/WEB-INF/views/psm/dashboard/pivotchart/pivotchart12.jsp"%></c:when>
	<c:when test="${type == 13 }"><%@include file="/WEB-INF/views/psm/dashboard/pivotchart/pivotchart13.jsp"%></c:when>
	<c:when test="${type == 14 }"><%@include file="/WEB-INF/views/psm/dashboard/pivotchart/pivotchart14.jsp"%></c:when>
	<c:when test="${type == 15 }"><%@include file="/WEB-INF/views/psm/dashboard/pivotchart/pivotchart15.jsp"%></c:when>
</c:choose>

