<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!-- <link rel="stylesheet" type="text/css" -->
<%-- 	href="${rootPath}/resources/css/common/eight_dash.css" /> --%>

<!-- <meta http-equiv="X-UA-Compatible" content="IE=8" /> -->
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin Theme #1 for dark mega menu option"
	name="description" />
<meta content="" name="author" />


<div class="tab-pane <c:if test="${tabFlag == 'tab1' }">active</c:if>" id="tab1">	  
	<div class="row">
		<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-bar-chart font-green-haze"></i> <span
							class="caption-subject bold uppercase font-green-haze">접속기록건수</span>
					</div>
					<div class="actions">
						<a class="btn btn-circle btn-icon-only btn-default fullscreen"
							href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"
							data-original-title="" title=""> </a>
					</div>
				</div>
				<div class="portlet-body">
					<div id="dashboard_amchart_1" class="chart"></div>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption ">
						<i class="icon-bar-chart font-green-haze"></i> <span
							class="caption-subject bold uppercase font-green-haze">사용자TOP10</span>
					</div>
					<div class="actions">
						<a class="btn btn-circle btn-icon-only btn-default fullscreen"
							href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"
							data-original-title="" title=""> </a>
					</div>
				</div>
				<div class="portlet-body">
					<div id="dashboard_amchart_2" class="chart"></div>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-bar-chart font-green-haze"></i> <span
							class="caption-subject bold uppercase font-green-haze">시스템별
							건수</span>
					</div>
					<div class="actions">
						<a class="btn btn-circle btn-icon-only btn-default fullscreen"
							href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"
							data-original-title="" title=""> </a>
					</div>
				</div>
				<div class="portlet-body">
					<div id="dashboard_amchart_3" class="chart"></div>
				</div>
			</div>
		</div>

		<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-bar-chart font-green-haze"></i> <span
							class="caption-subject bold uppercase font-green-haze">유형별
							건수1</span>
					</div>
					<div class="actions">
						<a class="btn btn-circle btn-icon-only btn-default fullscreen"
							href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"
							data-original-title="" title=""> </a>
					</div>
				</div>
				<div class="portlet-body">
					<div id="dashboard_amchart_4" class="chart"></div>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption ">
						<i class="icon-bar-chart font-green-haze"></i> <span
							class="caption-subject bold uppercase font-green-haze">유형별
							건수2</span>
					</div>
					<div class="actions">
						<a class="btn btn-circle btn-icon-only btn-default fullscreen"
							href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"
							data-original-title="" title=""> </a>
					</div>
				</div>
				<div class="portlet-body">
					<div id="dashboard_amchart_5" class="chart"></div>
				</div>
				<!-- <div>
                  <canvas id="line-chart" data-render="chart-js"></canvas>
              </div> -->

			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-bar-chart font-green-haze"></i> <span
							class="caption-subject bold uppercase font-green-haze">소속별
							TOP5</span>
					</div>
					<div class="actions">
						<a class="btn btn-circle btn-icon-only btn-default fullscreen"
							href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"
							data-original-title="" title=""> </a>
					</div>
				</div>
				<div class="portlet-body">
					<div id="dashboard_amchart_6" class="chart"></div>
				</div>
			</div>
		</div>

		<div class="col-lg-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-bar-chart font-green-haze"></i> <span
							class="caption-subject bold uppercase font-green-haze">시스템
							별 현재 사용자 수</span>
					</div>
					<div class="actions">
						<a class="btn btn-circle btn-icon-only btn-default fullscreen"
							href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"
							data-original-title="" title=""> </a>
					</div>
				</div>
				<div class="portlet-body">
					<div id="dashboard_amchart_7" class="chart"></div>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption ">
						<i class="icon-bar-chart font-green-haze"></i> <span
							class="caption-subject bold uppercase font-green-haze">시스템
							별 개인정보 처리 추이</span>
					</div>
					<div class="actions">
						<a class="btn btn-circle btn-icon-only btn-default fullscreen"
							href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"
							data-original-title="" title=""> </a>
					</div>
				</div>
				<div class="portlet-body">
					<div id="dashboard_amchart_8" class="chart"></div>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-bar-chart font-green-haze"></i> <span
							class="caption-subject bold uppercase font-green-haze">CPU,
							MEMORY, HDD 사용량</span>
					</div>
					<div class="actions">
						<a class="btn btn-circle btn-icon-only btn-default fullscreen"
							href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"
							data-original-title="" title=""> </a>
					</div>
				</div>
				<div class="portlet-body">

					<div id="dashboard_amchart_9" class="chart"></div>
				</div>
			</div>
		</div>

	</div>
</div>
