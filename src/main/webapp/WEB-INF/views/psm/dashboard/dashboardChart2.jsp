<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!-- <link rel="stylesheet" type="text/css" -->
<%-- 	href="${rootPath}/resources/css/common/eight_dash.css" /> --%>

<!-- <meta http-equiv="X-UA-Compatible" content="IE=8" /> -->
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta
	content="Preview page of Metronic Admin Theme #1 for dark mega menu option"
	name="description" />
<meta content="" name="author" />




<!-- BEGIN GLOBAL MANDATORY STYLES -->
<%-- <link href="${rootPath}/resources/css/font-awesome.min.css" --%>
<!-- 	rel="stylesheet" type="text/css"> -->
<%-- <link href="${rootPath}/resources/css/simple-line-icons.min.css" --%>
<!-- 	rel="stylesheet" type="text/css"> -->
<%-- <link href="${rootPath}/resources/css/bootstrap.min.css" --%>
<!-- 	rel="stylesheet" type="text/css"> -->
<%-- <link href="${rootPath}/resources/css/bootstrap-switch.min.css" --%>
<!-- 	rel="stylesheet" type="text/css"> -->
<!-- END GLOBAL MANDATORY STYLES -->

<%-- <link href="${rootPath}/resources/css/datatables.min.css" --%>
<!-- 	rel="stylesheet" type="text/css"> -->
<%-- <link href="${rootPath}/resources/css/datatables.bootstrap.css" --%>
<!-- 	rel="stylesheet" type="text/css"> -->
<%-- <link href="${rootPath}/resources/css/bootstrap-datepicker3.min.css" --%>
<!-- 	rel="stylesheet" type="text/css"> -->

<!-- BEGIN THEME GLOBAL STYLES -->
<%-- <link href="${rootPath}/resources/css/components.min.css" --%>
<!-- 	rel="stylesheet" id="style_components" type="text/css"> -->
<%-- <link href="${rootPath}/resources/css/plugins.min.css" rel="stylesheet" --%>
<!-- 	type="text/css"> -->
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<%-- <link href="${rootPath}/resources/css/layout.min.css" rel="stylesheet" --%>
<!-- 	type="text/css"> -->
<%-- <link href="${rootPath}/resources/css/custom.min.css" rel="stylesheet" --%>
<!-- 	type="text/css"> -->
<!-- END THEME LAYOUT STYLES -->
<!-- <link rel="shortcut icon" -->
<!-- 	href="http://localhost:8080/PSM-3.0.2/abnormal/favicon.ico"> -->

<%-- <script src="${rootPath}/resources/js/jquery.min.js" --%>
<!-- 	type="text/javascript"></script> -->
<%-- <script src="${rootPath}/resources/js/bootstrap.min.js" --%>
<!-- 	type="text/javascript"></script> -->
<%-- <script src="${rootPath}/resources/js/js.cookie.min.js" --%>
<!-- 	type="text/javascript"></script> -->
<%-- <script src="${rootPath}/resources/js/jquery.slimscroll.min.js" --%>
<!-- 	type="text/javascript"></script> -->
<%-- <script src="${rootPath}/resources/js/jquery.blockui.min.js" --%>
<!-- 	type="text/javascript"></script> -->
<%-- <script src="${rootPath}/resources/js/bootstrap-switch.min.js" --%>
<!-- 	type="text/javascript"></script> -->

<!-- AMCHART SCRIPT START -->
<%-- <script src="${rootPath}/resources/js/amcharts.js"></script> --%>
<%-- <script src="${rootPath}/resources/js/serial.js"></script> --%>
<%-- <script src="${rootPath}/resources/js/export.min.js"></script> --%>
<%-- <script src="${rootPath}/resources/js/amstock.js"></script> --%>
<%-- <script src="${rootPath}/resources/js/xy.js"></script> --%>
<%-- <link rel="stylesheet" href="${rootPath}/resources/css/export.css" --%>
<!-- 	type="text/css" media="all"> -->
<%-- <script src="${rootPath}/resources/js/light.js"></script> --%>
<!-- AMCHART SCRIPT END -->

<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<%-- <script src="${rootPath}/resources/js/app.min.js" type="text/javascript"></script> --%>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<%-- <script src="${rootPath}/resources/js/layout.min.js" --%>
<!-- 	type="text/javascript"></script> -->
<%-- <script src="${rootPath}/resources/js/demo.min.js" --%>
<!-- 	type="text/javascript"></script> -->
<%-- <script src="${rootPath}/resources/js/quick-sidebar.min.js" --%>
<!-- 	type="text/javascript"></script> -->
<%-- <script src="${rootPath}/resources/js/quick-nav.min.js" --%>
<!-- 	type="text/javascript"></script> -->

<%-- <script src="${rootPath}/resources/js/datatable.js" --%>
<!-- 	type="text/javascript"></script> -->
<%-- <script src="${rootPath}/resources/js/datatables.min.js" --%>
<!-- 	type="text/javascript"></script> -->
<%-- <script src="${rootPath}/resources/js/datatables.bootstrap.js" --%>
<!-- 	type="text/javascript"></script> -->
<%-- <script src="${rootPath}/resources/js/bootstrap-datepicker.min.js" --%>
<!-- 	type="text/javascript"></script> -->
<%-- <script src="${rootPath}/resources/js/menu-util.js" --%>
<!-- 	type="text/javascript"></script> -->
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<!-- <script type="text/javascript" -->
<%-- 	src="${rootPath}/resources/js/fabric.min.js" async=""></script> --%>
<!-- <script type="text/javascript" -->
<%-- 	src="${rootPath}/resources/js/FileSaver.min.js" async=""></script> --%>
<%-- <link href="${rootPath}/resources/css/profile.min.css" rel="stylesheet" --%>
<!-- 	type="text/css"> -->
<%-- <script src="${rootPath}/resources/js/pie.js"></script> --%>



<form id="dashForm" method="POST">

	<div class="portlet-body">
		<div class="tabbable tabbable-tabdrop">

			<!-- END HEADER MENU -->
			<!-- BEGIN PAGE BASE CONTENT -->
			<div class="tab-content">

				<div class="tab-pane active" id="tab2">
					<div class="row">
						<div class="col-lg-12">
							<div class="portlet light bordered">
								<div class="portlet-title">
									<div class="caption">
										<i class="icon-bar-chart font-green-haze"></i> <span
											class="caption-subject bold uppercase font-green-haze">오남용
											의심행위자 분석</span>
									</div>
									<div class="actions">
										<a class="btn btn-circle btn-icon-only btn-default fullscreen"
											href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"
											data-original-title="" title=""> </a>
									</div>
								</div>
								<div class="portlet-body">
									<div id="dashboard_amchart_10" class="chart"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="portlet light bordered">
								<div class="portlet-title">
									<div class="caption">
										<i class="icon-bar-chart font-green-haze"></i> <span
											class="caption-subject bold uppercase font-green-haze">부서별
											위험도 분석</span>
									</div>
									<div class="actions">
										<a class="btn btn-circle btn-icon-only btn-default fullscreen"
											href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"
											data-original-title="" title=""> </a>
									</div>
								</div>
								<div class="portlet-body">
									<div id="dashboard_amchart_11" class="chart"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
							<div class="portlet light bordered">
								<div class="portlet-title">
									<div class="caption">
										<i class="icon-bar-chart font-green-haze"></i> <span
											class="caption-subject bold uppercase font-green-haze">유형별
											위험도 현황</span>
									</div>
									<div class="actions">
										<a class="btn btn-circle btn-icon-only btn-default fullscreen"
											href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"
											data-original-title="" title=""> </a>
									</div>
								</div>
								<div class="portlet-body">
									<div id="dashboard_amchart_12" class="chart"></div>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
							<div class="portlet light bordered">
								<div class="portlet-title">
									<div class="caption">
										<i class="icon-bar-chart font-green-haze"></i> <span
											class="caption-subject bold uppercase font-green-haze">개인정보
											처리량 예측</span>
									</div>
									<div class="actions">
										<a class="btn btn-circle btn-icon-only btn-default fullscreen"
											href="http://localhost:8080/PSM-3.0.2/dashboard/dashboard_main.psm#"
											data-original-title="" title=""> </a>
									</div>
								</div>
								<div class="portlet-body">
									<div id="dashboard_amchart_13" class="chart"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</form>
