<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<script src="${rootPath}/resources/js/common/ezDatepicker.js"
	type="text/javascript" charset="UTF-8"></script>
<style>
#searchBar {
	background: none;
}
.cont {
	color: black !important;
	height:30px; width: 150px; font-size: 15px;
}
</style>

<style type="text/css" media="screen">
	html, body, svg { width: 100%; height: 100%;}
</style>

<!-- 토폴로지 검색조건 -->
<div class="option_tp left" style="width: 100%;">
	<form action="" id="topologyForm" method="post">
		<span id="searchBar"
			style="width: 100%; float: left; position: relative;">
			<div style="width: 100%;">
				<div style="width: 8%; position: relative; float: left;">
					<span class="option_list"> 
						<label class="no_img_label" style="width: 100%;"> 
						<input type="checkbox" id="realtime" name="realtime"
							<c:if test="${paramBean.realtime == 'on'  }">checked="checked"</c:if>
							value="${paramBean.realtime }"> <i>&nbsp;실시간 모니터링</i>
					</label>
					</span>
				</div>
				<div style="width: 31%; position: relative; float: left;">
					<span id="searchDate" style="width: 100%;"> <span
						class="option_list margin_tp"> <i>날짜 : </i> <input
							type="text" class="calender input_cal_tp cont" id="start_date" style="color: black; height:30px; width: 150px; font-size: 15px;"
							name="start_date" value="${paramBean.start_date}">
					</span> <span class="option_list margin_tp"> <i>시간 : </i> <select class="cont"
							id="starth" name="starth">
								<c:forEach var="item" varStatus="i" begin="0" end="23" step="1">
								<option value="${item}"<c:if test="${item == paramBean.starth }">selected="selected"</c:if>>${item}</option>
									<%-- <c:if test="${paramBean.realtime == 'on'}">
										<c:if test="${paramBean.startm < 20}">
											<option value="${item}"
												<c:if test="${item == (paramBean.starth - 1) }">selected="selected"</c:if>>
												${item}</option>
										</c:if>
										<c:if test="${paramBean.startm >= 20}">
											<option value="${item}"
												<c:if test="${item == paramBean.starth }">selected="selected"</c:if>>
												${item}</option>
										</c:if>
									</c:if>
									<c:if test="${paramBean.realtime != 'on'}">
										<option value="${item}"
											<c:if test="${item == paramBean.starth }">selected="selected"</c:if>>
											${item}</option>
									</c:if> --%>
								</c:forEach>
						</select> <select id="startm" name="startm" class="cont">
								<c:forEach var="item" varStatus="i" begin="0" end="59" step="1">
								<option value="${item}"<c:if test="${item == paramBean.startm }">selected="selected"</c:if>>${item}</option>
									<%-- <c:if test="${paramBean.realtime == 'on'}">
										<c:if test="${paramBean.startm < 20}">
											<option value="${item}"
												<c:if test="${item == (paramBean.startm + 40) }">selected="selected"</c:if>
												<c:if test="${item == (paramBean.startm+40) }">selected="selected"</c:if>>${item}</option>
										</c:if>
										<c:if test="${paramBean.startm >= 20}">
											<option value="${item}"
												<c:if test="${item == (paramBean.startm - 20) }">selected="selected"</c:if>
												<c:if test="${ item== (paramBean.startm-20) }">selected="selected"</c:if>>${item}</option>
										</c:if>
									</c:if>
									<c:if test="${paramBean.realtime != 'on' }">
										<option value="${item}"
											<c:if test="${item == paramBean.startm}">selected="selected"</c:if>
											<c:if test="${ item == paramBean.startm}">selected="selected"</c:if>>${item}</option>
									</c:if> --%>
								</c:forEach>
						</select> <i class="sim">&sim;</i> <select id="endh" name="endh" class="cont">
								<c:forEach var="item" varStatus="i" begin="0" end="23" step="1">
									<option value="${item}"
										<c:if test="${item == paramBean.endh }">selected="selected"</c:if>>${item}</option>
								</c:forEach>
						</select> <select id="endm" name="endm" class="cont">
								<c:forEach var="item" varStatus="i" begin="0" end="59" step="1">
									<option value="${item}"
										<c:if test="${item == paramBean.endm }">selected="selected"</c:if>>${item}</option>
								</c:forEach>
						</select>
					</span>
					</span>
				</div>
				<div style="width: 13%; position: relative; float: left;">
					<span class="option_list margin_tp" style="width: 100%;"> <i>개인정보유형: </i>
					<select name="privacyType" id="privacyType" class="cont" style="width: 120px">
						<option value="" ${paramBean.privacyType == '' ? 'selected="selected"' : ''}>--- 전 체 ---</option>
						<c:if test="${empty CACHE_RESULT_TYPE}">
							<option>데이터 없음</option>
						</c:if>
						<c:if test="${!empty CACHE_RESULT_TYPE}">
							<c:forEach items="${CACHE_RESULT_TYPE}" var="i" varStatus="z">
								<option value="${i.key}" ${i.key==paramBean.privacyType ? "selected=selected" : "" }>${ i.value}</option>
							</c:forEach>
						</c:if>
					</select>
				</div>
				<div style="width: 11%; position: relative; float: left;">
					<span class="option_list margin_tp" style="width: 100%;"> <i>IP대역
							: </i> <input type="text" id="search_ip" name="search_ip" class="cont"
						style="width: 120px"
						value="${paramBean.search_ip == null? '' : paramBean.search_ip }">
					</span>
				</div>
				<div style="width: 16%; position: relative; float: left;">
					<span class="option_list margin_tp" style="width: 100%;"> <label
						class="no_img_label" style="width: 100%;"> <input
							type="checkbox" id="isTop_n" name="isTop_n"
							<c:if test="${paramBean.isTop_n == 'on'  }">checked="checked"</c:if>
							value="${paramBean.isTop_n }"> <i>&nbsp;개인정보건수 상위 N명
								:</i>&nbsp;&nbsp; <select id="top_n" name="top_n" class="cont">
								<option value="5"
									<c:if test="${paramBean.top_n==5 }">selected="selected"</c:if>>5</option>
								<option value="10"
									<c:if test="${paramBean.top_n==null || paramBean.top_n==10 }">selected="selected"</c:if>>10</option>
								<option value="20"
									<c:if test="${paramBean.top_n==20 }">selected="selected"</c:if>>20</option>
								<option value="30"
									<c:if test="${paramBean.top_n==30 }">selected="selected"</c:if>>30</option>
								<option value="40"
									<c:if test="${paramBean.top_n==40 }">selected="selected"</c:if>>40</option>
								<option value="50"
									<c:if test="${paramBean.top_n==50 }">selected="selected"</c:if>>50</option>
								<option value="60"
									<c:if test="${paramBean.top_n==60 }">selected="selected"</c:if>>60</option>
								<option value="70"
									<c:if test="${paramBean.top_n==70 }">selected="selected"</c:if>>80</option>
								<option value="80"
									<c:if test="${paramBean.top_n==80 }">selected="selected"</c:if>>80</option>
								<option value="90"
									<c:if test="${paramBean.top_n==90 }">selected="selected"</c:if>>90</option>
								<option value="100"
									<c:if test="${paramBean.top_n==100 }">selected="selected"</c:if>>100</option>
						</select>

					</label>
					</span>
				</div>

				<div style="width: 15%; position: relative; float: left;">
					<span class="option_list margin_tp"> <i>Refresh : </i> <select
						id="refreshBox" onchange="refresh()" style="width: 60px" class="cont">
							<option value="300000">선택</option>
							<option value="5000">5</option>
							<option value="10000">10</option>
							<option value="20000">20</option>
							<option value="30000">30</option>
							<option value="60000">60</option>
					</select> <i>(단위/초)</i>
					</span>
				</div>


 


<%-- 				<input type="hidden" id="realtime" name="realtime" value="${paramBean.realtime }">  --%>
				<input type="hidden" id="toggleCd" name="toggleCd" value="${paramBean.toggleCd == null? '2' : paramBean.toggleCd }">

				<!-- '조회' 버튼 -->
				<a class="btn btn-sm blue" id="topo_search">조회</a>
			</div>
		</span>
		<!-- // searchBar End -->
	</form>
	<!-- // topologyForm End -->
</div>

<!-- 토폴로지 그려지는 부분 -->
<div id="graph"></div>


