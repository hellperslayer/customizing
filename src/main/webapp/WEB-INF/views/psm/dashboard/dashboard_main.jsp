<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>
<c:set var="currentMenuId" value="MENU00031" />
<ctl:checkMenuAuth menuId="${currentMenuId}"/>

<style>
	#graph { float: left; width: 100%; height: 700px; overflow: hidden; }
</style>

<script src="${rootPath}/resources/js/psm/dashboard/highcharts.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/exporting.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/globalize.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/knockout-3.0.0.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/dx.chartjs.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/zoomingData.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/psm/dashboard/dashboard.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/dashboard/vivagraph.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/dashboard/topology.js" type="text/javascript" charset="UTF-8"></script>

<form id="dashboardForm" method="POST">
	<input type="hidden" name="main_menu_id" value="${mainMenuId }" />
	<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
	<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
	<input type="hidden" name="page_num" />
</form>
	
<div class="tab_dash left">
	<span id="tab1" class="tab <c:if test="${tabFlag == 'tab1' }">select</c:if>">대시보드</span>
	<span id="tab2" class="tab <c:if test="${tabFlag == 'tab2' }">select</c:if>">토폴로지</span>
<!--  <div id="dashboardPeriod">
	<div style="float:left; float: left; margin-left: 20px; text-align: center; margin-top: 8px; font-size: 14px; font-weight: bold;">대시보드 기간: </div> 
	<div style="margin-left:403px;">
		<input name="search_from" type="text" class="calender search_fr allFrom" value="" onchange="getChart3('');"/>
		<i class="sim">&sim;</i>
		<input name="search_to" type="text" class="calender search_to allTo" value="" onchange="getChart3('');"/> 
	</div> 
</div> --> 
<div class="dashlogo">
	<img src="${pageContext.servletContext.contextPath}/resources/image/login/logo3.png" />
</div>
</div>

	
<input type="hidden" id="tabId" value="${tabFlag }" />
	
<!-- 대시보드 항목 영역 -->
<div class="dashboard" id="dashContent">

	<!-- 시군구 : 0 , 아카데미 : 1  -->
	<c:if test="${dashboard eq 0}">
		<%@include file="/WEB-INF/views/psm/dashboard/dashboard_sub_gun.jsp"%>
	</c:if>
	<c:if test="${dashboard eq 1}">
		<%@include file="/WEB-INF/views/psm/dashboard/dashboard_sub_gun.jsp"%>
	</c:if>
</div>

<!-- 토폴로지 항목 영역 -->
<div class="topology none" id="topoContent">
	<%@include file="/WEB-INF/views/psm/dashboard/topology.jsp"%>
</div>

<!-- 전체로그조회 이동 Form -->
<form id="allLogInqListForm" action="${rootPath }/allLogInq/list.html" method="POST">
	<input type="hidden" name="main_menu_id" value="MENU00040" />
	<input type="hidden" name="sub_menu_id" value="MENU00041" />
	<input type="hidden" name="search_to" />
	<input type="hidden" name="search_from" />
	<input type="hidden" name="starthm" />
	<input type="hidden" name="endhm" />
	<input type="hidden" name="system_seq" />
	<input type="hidden" name="sub_menu_id" />
	<input type="hidden" name="emp_user_id" />
	<input type="hidden" name="privacyType" />
</form>

<script type="text/javascript">
	
	var dashboardConfig = {
		"dashboardUrl":"${rootPath}/dashboard/dashboard.html"
		,"detailUrl":"${rootPath}/dashboard/detail.html"
	};
 	
</script>