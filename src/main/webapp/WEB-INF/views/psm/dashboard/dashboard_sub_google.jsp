<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%String hosturl = javax.servlet.http.HttpUtils.getRequestURL(request).toString(); 
hosturl = hosturl.substring(0, hosturl.indexOf("WEB-INF"));
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=9">
<title>Google Earth</title>

<script type="text/javascript">
	var hosturl = '<%=hosturl %>';
	
</script>

<script src="./js/jquery-1.11.2.js"></script>
<script src="${rootPath}/resources/js/psm/dashboard/highcharts.js" type="text/javascript"></script>

<script src="//www.google.com/jsapi?key=AIzaSyDuUT8icFqMbcNgriBcG2c1mdCgFanVv1k"></script>
<script src="${rootPath}/resources/js/psm/dashboard/ez-map-ge-v1.js" type="text/javascript"></script>


	
<script src="./js/ez-map-ge-v1.js" ></script>

</head>
<body>
<div class="option_area left" style="display:none;">
	<div style="margin-bottom:10px;">
	<form id="googleForm" method="POST">
		<select name="coord_type">
			<option value="0">BMT</option>
			<option value="1">설치납품</option>
		</select>
		<table>
		<tr>
		<td style="vertical-align: middle; padding-left:10px"><span style="font-size:17px;font-weight: bold;"> 좌표:</span></td> <td><input type="text" name="coordinate" /></td>
		<td style="vertical-align: middle; padding-left:10px"><span style="font-size:17px;font-weight: bold;"> 좌표명:</span></td><td><input type="text" name="coord_name" /></td>
		<td style="vertical-align: middle; padding-left:10px"><span style="font-size:17px;font-weight: bold;"> 설명:</span></td><td><input type="text" name="coord_memo" /></td>
		<td style="vertical-align: middle; padding-left:10px"><span style="font-size:17px;font-weight: bold;"> 날짜:</span></td><td><input name="coord_date" type="text" class="calender search_dt" style="z-index:9999;"/></td>
		<td style="vertical-align: middle; padding-left:10px"><input type="button" value="추가" id="coord_add"/></td>	
		<td style="vertical-align: middle; padding-left:5px"><input type="button" value="목록보기" onClick="popupOpen()"/></td>	
		</tr>
		</table>
		<input type="hidden" name="coordinate">
		<input type="hidden" name="main_menu_id" value="MENU00040" />
		<input type="hidden" name="sub_menu_id" value="MENU00041" />
	</form>
	</div>
	 </div>
	<div>
			<div class="option_tab" title="open" >
				<p class="close">open</p>
			</div>
			<div class="option_tab" title="close" style="display:none;">
				<p class="open">close</p>
	</div>
	</div>
	<div id="map3d" style="height: 960px; width: 100%;"></div>
	<%--
	<input type="button" value="HOME" onclick="javascript:goHome();">
	<input type="button" value="LOOP_BALLON" onclick="javascript:showBallons();">
	<input type="button" value="SHOW_BALLON" onclick="javascript:showBallon();">
	<input type="button" value="SHOW_LINE" onclick="javascript:showLine();">
	<input type="button" value="LOAD_KML" onclick="javascript:loadKml();">
	 --%>

</body>
<script>
	function popupOpen(){
		 var frm = document.getElementById("googleForm");
		  window.open('', 'viewer', 'height=700px, width=935px, scrollbars=yes'); 
		 frm.action = dashboardConfig['urlList'];
		 frm.target = "viewer";
		 frm.method = "post";
		 frm.submit();  
	}
	
	$("#coord_add").click(function(){

		coord_add();
		
	});
	
	function coord_add(){	
		sendAjaxGoogle("add");
	}
		
	function sendAjaxGoogle(type){
		var $form = $("#googleForm");
		var log_message_title = '';
		var log_action = '';
		var log_message_params = 'auth_name';
		var menu_id = $('input[name=current_menu_id]').val();
		if (type == 'add') {
			log_message_title = '등록';
			log_action = 'INSERT';
		} else if (type == 'save') {
			log_message_title = '수정';
			log_action = 'UPDATE';
		} else if (type == 'remove') {
			log_message_title = '삭제';
			log_action = 'REMOVE';
		}
		var x = $("input[name=x]").val();
		var y = $("input[name=y]").val();
		$("input[name=coordinate]").val();
		$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
		
		sendAjaxPostRequest(dashboardConfig[type + "Url"], $form.serialize(), ajaxAuth_successHandler, ajaxAuth_errorHandler, type);
	}

	// 권한 ajax call - 성공
	function ajaxAuth_successHandler(data, dataType, actionType){
		if(data.hasOwnProperty("statusCode")) {
			var statusCode = data.statusCode;
			if(statusCode == 'SYS006V') {
				location.href = authDetailConfig['loginPage'];
				return false;
			}
			
			if(data.hasOwnProperty("message")) {
				var message = data.message;
				alert(message);
			}
		}else{
			switch(actionType){
				case "add":
					alert("성공적으로 등록되었습니다.");
					break;
				case "save":
					alert("성공적으로 수정되었습니다.");
					break;
				case "remove":
					alert("성공적으로 삭제되었습니다.");
					break;
			}

			moveList();
		}
	}

	// 권한 ajax call - 실패
	function ajaxAuth_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){
		
		log_message += authLogMessage[actionType + "Action"];
		
		switch(actionType){
			case "add":
				alert("권한 등록 실패하였습니다." + textStatus);
				break;
			case "save":
				alert("권한 수정 실패하였습니다." + textStatus);
				break;
			case "remove":
				alert("권한 삭제 실패하였습니다." + textStatus);
				break;
		}
	}
	
	function moveList(){
		
		$("#googleForm").attr("action",dashboardConfig["googleList"]);
		$("#googleForm").submit();
	}
	
	$(document).ready(function() {

		$('.option_tab').click(function() {
			if($(this).attr('title') == 'open') {
				$('.option_area').slideDown(500);
				$(this).attr('style', 'display: none');
				$(this).siblings().attr('style', 'display: block');
			}
			else if($(this).attr('title') == 'close') {
				$('.option_area').slideUp(500);
				$(this).attr('style', 'display: none');
				$(this).siblings().attr('style', 'display: block');
			}		
		});
	});
	
</script>
</html>