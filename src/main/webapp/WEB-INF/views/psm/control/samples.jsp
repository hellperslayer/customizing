<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<c:set var="currentMenuId" value="MENU00101" />
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>

<div class="contents left">
	<!-- 메뉴명 -->
	<h3 class="page_name">${currentMenuName}</h3>
	
	<div id="sampleDiv">
	<form action="/control.ctrl">
		<div style="height: 2px; background-color: #4990d8;"></div>
			<ul style=" margin-top: 20px;">
				<li>이페이지의 용도는 협업을 위해 신규 추가 기능의 사용법 및 모듈을 제공하는데 의의가 있습니다.</li>
				<li>공지시 혹은 필요한 모듈이 있을때 처음으로 이 페이지 먼저 참조하시기 바랍니다.</li>
			</ul>&nbsp;&nbsp;
		
		<!-- 검색영역 -->
		<div id="searchListDiv" style="text-align: left;">
			&nbsp;
			<b>1. 기간 검색</b>
			<br>
			<ul>
				<li>/resources/js/common/ezDatepicker.js 를 참조</li>
				<li>검색 시작 날짜의 input box의 class 에 calendar을 추가한다.</li>
				<li>검색 마지막 날짜의 input box의 class 에 calendar을 추가한다.</li>
			</ul>&nbsp;&nbsp;
			
			<span>			
				<label><span class="option_name">기간</span></label>
				<input name="search_from" type="text" class="calender" />
				<i class="sim">&sim;</i>
				<input name="search_to" type="text" class="calender" />
			</span>
			
			<br />
			<br />
			<br />
			<br />&nbsp;
			
			<b>2. 날짜 검색</b>
			<ul>
				<li>/resources/js/common/ezDatepicker.js 를 참조</li>
				<li>조회 날짜의 input box의 class에 search_dt를 추가한다.</li>
			</ul>&nbsp;&nbsp;
			
			<span>			
				<label><span class="option_name">날짜</span></label>
				<input name="search_from" type="text" class="search_dt calender" />
			</span>
			
			<br />
			<br />
			<br />
			<br />&nbsp;
			
			<b>3. 공통코드 및 그룹코드 캐쉬화</b>
			&nbsp;&nbsp;
			<c:set value="POLICY_GROUP_TYPE" var="sampleGroup"/>
			<c:set value="PGT01" var="sampleCode"/>
			<ul>
				<li>CACHE_GROUP : 그룹 리스트</li>
			</ul>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>ex)</b>
			&nbsp;&nbsp;&#36;&#123;CACHE_GROUP&#125; : ${CACHE_GROUP }
			<br /><br />
			<ul>
				<li>CACHE_GROUP[그룹코드] : 그룹코드명</li>
			</ul>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			
			
			
			<b>ex)</b>
			&nbsp;&nbsp;&#36;&#123;CACHE_GROUP['POLICY_GROUP_TYPE']&#125; : ${CACHE_GROUP[sampleGroup] }
			
			<br /><br />
			<ul>
				<li>CACHE_그룹코드 : 그룹코드에 해당하는 하위 코드 리스트</li>
			</ul>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			
			<b>ex)</b>
			&nbsp;&nbsp;&#36;&#123;CACHE_POLICY_GROUP_TYPE&#125; : ${CACHE_POLICY_GROUP_TYPE }
			<br /><br />
			<ul>
				<li>CACHE_그룹코드[공통코드] : 공통코드명</li>
			</ul>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>ex)</b>
			&nbsp;&nbsp;&#36;&#123;CACHE_POLICY_GROUP_TYPE['PGT01']&#125; : ${CACHE_POLICY_GROUP_TYPE[sampleCode] }
			<br /><br />
			<ul>
				<li>Bean 객체에서의 사용 방법</li>
			</ul>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<b>-. com.easycerti.eframe.control.web.SampleCtrl 참조</b>
			<c:remove var="sampleGroup" />
			<c:remove var="sampleCode" />
			<br /><br /><br />&nbsp;
			
			
			
			
			<b>4-1. selectbox(공통 코드)</b><br />
			<ul>
				<li><b>[3. 공통코드 및 그룹코드 캐쉬화] 와 소스 참조</b></li>
			</ul>&nbsp;&nbsp;&nbsp;&nbsp;
			<span>
				<select name="codeSelect" style="text-align: center;">
					<option>선택</option>
					<c:if test="${!empty CACHE_GROUP}">
						<c:forEach items="${CACHE_GROUP}" var="i" varStatus="z">
							<option value="${i.key}">${i.key} - ${ i.value}</option>
						</c:forEach>
					</c:if>
				</select><br /><br />&nbsp;&nbsp;&nbsp;&nbsp;
				<select name="codeSelect" style="text-align: center;">
					<option>선택</option>
					<c:if test="${!empty CACHE_POLICY_GROUP_TYPE}">
						<c:forEach items="${CACHE_POLICY_GROUP_TYPE}" var="i" varStatus="z">
							<option value="${i.key}">${i.key} - ${ i.value}</option>
						</c:forEach>
					</c:if>
				</select>
			</span>
			<br /><br />
			&nbsp;&nbsp;
			<b>4-2. selectbox(공통 코드 외 DB에서 가져온 데이터 리스트)</b><br />
			<ul>
				<li><b>소스참조</b></li>
			</ul>&nbsp;&nbsp;&nbsp;&nbsp;
			<select name="departmentSelect" style="text-align: center;">
				<option>선택</option>
				<c:if test="${!empty departs}">
					<c:forEach items="${departs}" var="i" varStatus="z">
						<option value="${i.code_id}">${i.code_id} - ${ i.code_name}</option>
					</c:forEach>
				</c:if>
			</select><br /><br />
			&nbsp;&nbsp;
			<b>4-3. selectbox(하드코딩의 재사용)</b><br />
			<ul>
				<li><b>소스 및 application context 참조</b></li>
			</ul>&nbsp;&nbsp;&nbsp;&nbsp;
			<select name="durationSelect" style="text-align: center;">
				<c:if test="${!empty template_durations.options}">
					<c:forEach items="${template_durations.options}" var="i" varStatus="z">
						<option value="${i.key}">${i.key} - ${ i.value}</option>
					</c:forEach>
				</c:if>
			</select><br /><br />
			&nbsp;&nbsp;
			<b>5. Yes(사용), No(미사용)</b><br />
			<ul>
				<li><b>template object</b></li>
			</ul>&nbsp;&nbsp;&nbsp;&nbsp;
			${template_yesNo.options['Y']}
			${template_yesNo.options['N']}<br /><br />
			<ul>
				<li><b>enum</b></li>
			</ul>&nbsp;&nbsp;&nbsp;&nbsp;
			<c:forEach items="${template_options.yesNo}" var="item">
				<input type="radio" name="useApproval" value="${item}" checked="checked"/>${item.name}&nbsp;
			</c:forEach>
			<br /><br />
			&nbsp;&nbsp;
			<b>6. Menu</b><br />
			<ul>
				<li><b>Upper Menu</b></li>
			</ul>&nbsp;&nbsp;&nbsp;&nbsp;
			<br />
			
			<c:forEach items="${CACHE_MENU}" var="upperMenu" varStatus="status">
				<b>${upperMenu}</b>
			</c:forEach>
			<br />
		</div>
		
		<div style="height: 2px; background-color: #e2e2e2; vertical-align: bottom;"></div>
	</form>

	<br /><br /><br /><br />&nbsp;
	
	<b>5. 페이징</b>
	<ul>
		<li>검색조건 폼의 id를 listForm으로 지정</li>
		<li>form 하위에 page_num name의 hidden 파라미터 포함.</li>
		<li>리스트 하단에 "/WEB-INF/views/common/include/pageNavigator.jsp"를 include 한다.</li>
	</ul>&nbsp;&nbsp;
	
	<b>ex)</b>
	<form id="listForm" name="samples" action="samples.html" method="POST">
		<div style="height: 2px; background-color: #4990d8;"></div>
		<!-- 검색영역 -->
		<div class="option_area left" id="searchListDiv">		
			<div class="col2 left">
				<span>
					<label><span class="option_name">기간</span></label>
					<input name="search_from" type="text" class="calender" value="${paramBean.search_from}" />
					<i class="sim">&sim;</i>
					<input name="search_to" type="text" class="calender" value="${paramBean.search_to}" />
				</span>
			</div>
			<div class="col2 left">
				<span>
					<label><span class="option_name">관리자ID</span></label>
					<input type="text" name="admin_user_id" class="input_box" value="${paramBean.admin_user_id }" />
				</span>
				<span>
					<label><span class="option_name">관리자명</span></label>
					<input type="text" name="admin_user_name" class="input_box" value="${paramBean.admin_user_name }" />
				</span>
			</div>
		</div>
		
		<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }"/>
		<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }"/>
		<input type="hidden" name="page_num" />
	</form>
	
	</div>
	
	<!-- 리스트 영역 -->
	<div>
		<table class="table left">
			<colgroup>
				<col width="8%"/>
				<col width="10%"/>
				<col width="10%"/>
				<col width="12%"/>
				<col width="15%"/>
				<col width="15%"/>
				<col width="15%"/>
				<col width="15%"/>
			</colgroup>
			<thead>
				<tr>
					<th scope="col">No.</th>
					<th scope="col">관리자ID</th>
					<th scope="col">관리자명</th>
					<th scope="col">IP</th>
					<th scope="col">로그기록일시</th>
					<th scope="col">분류</th>
					<th scope="col">로그메세지</th>
					<th scope="col">로그종류</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty managerActHistList.managerActHists}">
						<tr>
			        		<td colspan="8" align="center">데이터가 없습니다.</td>
			        	</tr>
					</c:when>
					<c:otherwise>
						<c:set value="${managerActHistList.page_total_count}" var="count"/>
						<c:forEach items="${managerActHistList.managerActHists}" var="managerActHist" varStatus="status">
							<tr <c:if test="${status.count % 2 == 0 }"> class='tr_gray'</c:if>>
								<td>${count - paramBean.page_cur_num * 10 + 10 }</td>
								<td>${managerActHist.admin_user_id }</td>
								<td>${managerActHist.admin_user_name }</td>
								<td>${managerActHist.ip_address }</td>
								<td>
									<fmt:formatDate value="${managerActHist.log_datetime }" pattern="yyyy-MM-dd kk:mm:ss" />
								</td>
								<c:if test="${managerActHist.s_category != null && managerActHist.s_category != '' }">
									<td style="padding-left: 5px;">${managerActHist.l_category } > ${managerActHist.m_category } > ${managerActHist.s_category }</td>
								</c:if>
								<c:if test="${managerActHist.l_category != null && managerActHist.l_category != '' }">
									<c:if test="${managerActHist.s_category == null || managerActHist.s_category == '' }">
										<td style="padding-left: 5px;">${managerActHist.l_category } > ${managerActHist.m_category }</td>
									</c:if>
								</c:if>
								<c:if test="${managerActHist.l_category == null || managerActHist.l_category == '' }">
									<td></td>
								</c:if>
								<td title="${managerActHist.log_message }"><div class='managerActHistList_logMessage_subStr'><nobr>${managerActHist.log_message}</nobr></div></td>
								<td>${managerActHist.log_action}</td>
							</tr>
							<c:set var="count" value="${count - 1 }"/>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
	
	
	<c:if test="${logAuthAdminUserList.page_total_count > 0}">
		<%@include file="/WEB-INF/views/common/include/pageNavigator.jsp"%>
	</c:if>

	<br />

</div>