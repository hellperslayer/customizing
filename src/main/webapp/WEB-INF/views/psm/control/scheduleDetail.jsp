<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>
<c:set var="currentMenuId" value="MENU00102" />
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/control/scheduleDetail.js" type="text/javascript" charset="UTF-8"></script>

<div class="contents left">
	<div id="labelDiv">	
		<label class="action_label"><span class="option_name">${currentMenuName}</span></label>
	</div>
	
	<form id="scheduleDetailForm" method="POST">
		<table class="table_td">
			<tr>
				<th>스케쥴(*)</th>
				<td colSpan="3">
					<select name="month" style="text-align: center;width: 70px;">
						<option value="*">*</option>
					<c:forEach begin="1" end="12" var="i">
						<option value="${i}" ${schedule.month ne '*' && schedule.month eq i?'selected="selected"':''}><c:out value="${i}월" /></option>
					</c:forEach>
					</select>&nbsp;&nbsp;
					<select name="day" style="text-align: center;width: 70px;">
						<option value="*">*</option>
					<c:forEach begin="1" end="31" var="i">
						<option value="${i}" ${schedule.day ne '*' && schedule.day eq i?'selected="selected"':''}><c:out value="${i}일" /></option>
					</c:forEach>
					</select>&nbsp;&nbsp;
					<select name="hour" style="text-align: center;width: 70px;">
						<option value="*">*</option>
					<c:forEach begin="0" end="23" var="i">
						<option value="${i}" ${schedule.hour ne '*' && schedule.hour eq i?'selected="selected"':''}><c:out value="${i}시" /></option>
					</c:forEach>
					</select>&nbsp;&nbsp;
					<select name="minute" style="text-align: center;width: 70px;">
						<option value="*">*</option>
					<c:forEach begin="0" end="59" var="i">
						<option value="${i}" ${schedule.minute ne '*' && schedule.minute eq i?'selected="selected"':''}><c:out value="${i}분" /></option>
					</c:forEach>
					</select>&nbsp;&nbsp;
				</td>
			</tr>
			<tr>
				<th>Job Bean(*)</th>
				<td colSpan="3">
					<select name="beanId">
						<option value=""> ----- 선 택 ----- </option>
				<c:forEach items="${scheduleJobs}" var="schJob">
						<option value="${schJob.key}" ${schJob.value eq schedule.beanId?'selected="selected"':''}>${schJob.value}</option>
				</c:forEach>	
					</select> 
				</td>
			</tr>
			<tr>
				<th>활성화(*)</th>
				<td colSpan="3">
					<select name="enabled">
				<c:forEach items="${template_options.yesNo}" var="yesNo">
						<option value="${yesNo}" ${yesNo eq schedule.enabled?'selected="selected"':''}>${yesNo}</option>
				</c:forEach>	
					</select> 
				</td>
			</tr>
			<tr>
				<th>설명</th>
				<td>
					<textarea name="descr" rows="5" cols="50" style="width: 99%" >${schedule.descr}</textarea>
				</td>
			</tr>
			<c:if test="${not empty schedule}">
				<tr>
					<th>등록일시</th>
					<td><fmt:formatDate value="${schedule.inserted_date }" pattern="yyyy-MM-dd kk:mm:ss"/></td>
					<th>수정일시</th>
					<td><fmt:formatDate value="${schedule.updated_date }" pattern="yyyy-MM-dd kk:mm:ss"/></td>
				</tr>
			</c:if>
		</table>
		
		<input type="hidden" name="schId" value="${empty schedule?'0':schedule.schId}" />
		<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id}" />
		<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id}" />
		<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
		
		<!-- 검색조건 유지 시작 -->
		<input type="hidden" name="beanId_1" value="${search.beanId_1}" />
		<!-- 검색조건 유지 끝 -->
	</form>
	
	<!-- 버튼 영역 -->
	<div class="option_btn right">
		<p class="right">
			<a class="btn gray" onclick="moveScheduleList()">목록</a>
			<c:choose>
				<c:when test="${empty schedule}">
					<a class="btn gray" onclick="addSchedule()">등록</a>
				</c:when>
				<c:otherwise>
					<a class="btn gray" onclick="saveSchedule()">수정</a>
					<a class="btn gray" onclick="removeSchedule()">삭제</a>
				</c:otherwise>
			</c:choose>
		</p>
	</div>
</div>

<script type="text/javascript">
	
	var scheduleConfig = {
		"listUrl":rootPath + "/scheduler/list.html"
		,"addUrl":rootPath + "/scheduler/add.html"
		,"saveUrl":rootPath + "/scheduler/save.html"
		,"removeUrl":rootPath + "/scheduler/remove.html"
		,"loginPage" : "${rootPath}/loginView.html"
	};
	
</script>