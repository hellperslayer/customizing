<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>
<c:set var="currentMenuId" value="MENU00101" />
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/control/scheduleList.js" type="text/javascript" charset="UTF-8"></script>

<!-- body (navigator 하단) -->
<div class="contents left">
	<!-- 메뉴명 -->
	<h3 class="page_name">${currentMenuName}</h3>
	
	<form id="scheduleListForm" method="POST">
		<!-- 검색영역 -->
		<div class="option_area left">
			<div class="optgroup">
				<div class="col3 left">
					<span>
						<label><span class="option_name">Job Bean</span></label>
						<input type="text" name="beanId_1" value="${search.beanId_1}"/>
					</span>
				</div>
				
				<!-- 버튼 영역 -->
				<div class="option_btn left">
					<p class="right">
						<a class="btn gray" onclick="resetOptions(scheduleListConfig['listUrl'])">취소</a>
						<a class="btn blue" onclick="moveScheduleList()">검색</a>
						<!-- <a class="btn excel" onclick="excelAuthList()">엑셀다운로드</a> -->
					</p>
				</div>
			</div>
		</div>
		
		<!-- option_tab -->
		<div>
			<div class="option_tab" title="close" style="display:none;">
				<p class="close">close</p>
			</div>
			<div class="option_tab" title="open">
				<p class="open">open</p>
			</div>
		</div>
		
		<!-- <input type="hidden" name="auth_id"/> -->
		<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" />
		<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
		<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
		<input type="hidden" name="page_num" value="${search.page_num}"/>
	</form>
	
	<!-- 리스트 영역 -->
	<div>
		<p class="left new_bt">
			<a class="btn blue" onclick="moveDetail('')">신규</a>
		</p>
		<table class="table left">
			<colgroup>
				<col width="10%"/>
				<col width="5%"/>
				<col width="5%" />
				<col width="5%" />
				<col width="5%" />
				<col width="20%" />
				<col width="5%" />
				<col width="30%" />
				<col width="15%" />
			</colgroup>
			<thead>
				<tr>
					<th scope="col">스케쥴ID</th>
					<th scope="col">월</th>
					<th scope="col">일</th>
					<th scope="col">시</th>
					<th scope="col">분</th>
					<th scope="col">Job Bean</th>
					<th scope="col">사용</th>
					<th scope="col">설명</th>
					<th scope="col">최종수정일시</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty search.total_count}">
						<tr>
			        		<td colspan="10" align="center">데이터가 없습니다.</td>
			        	</tr>
					</c:when>
					<c:otherwise>
						<c:forEach items="${scheduleList}" var="schedule" varStatus="status">
							<tr <c:if test="${status.count % 2 == 0 }"> class='tr_gray'</c:if>>
								<td>${schedule.schId}</td>
								<td>${schedule.month}</td>
								<td>${schedule.day}</td>
								<td>${schedule.hour}</td>
								<td>${schedule.minute}</td>
								<td class='underlineTd'>
									<a onclick="moveDetail('${schedule.schId}')">${schedule.beanId}</a>
								</td>
								<td>${schedule.enabled.name}</td>
								<td>${schedule.descr}</td>
								<td>${empty schedule.updated_date?schedule.inserted_date:schedule.updated_date}</td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
	
	<!-- 페이징 영역 -->
	<c:if test="${search.total_count > 0}">
		<div class="page left" id="pagingframe">
			<p><ctl:paginator currentPage="${search.page_num}" rowBlockCount="${search.size}" totalRowCount="${search.total_count}" /></p>
		</div>
	</c:if>
</div>

<script type="text/javascript">
	
	var scheduleListConfig = {
		"listUrl":"${rootPath}/scheduler/list.html"
		,"detailUrl":"${rootPath}/scheduler/detail.html"
	};
 	
</script>