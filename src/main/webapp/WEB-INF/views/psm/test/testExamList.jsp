<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="currentMenuId" value="${index_id }" />
<%-- <ctl:checkMenuAuth menuId="${currentMenuId}" /> --%>
<ctl:drawNavMenu menuId="${currentMenuId}" />
<script src="${rootPath}/resources/js/common/jquery.form.js" type="text/javascript" charset="UTF-8"></script>
<%-- <script src="${rootPath}/resources/js/psm/system_management/empUserMngtList.js" type="text/javascript" charset="UTF-8"></script> --%>

<h1 class="page-title">${currentMenuName}</h1>
<div class="portlet light portlet-fit portlet-datatable bordered">
		
	<div class="portlet-body">
		<div class="table-container">

			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">

				<div class="col-md-6" style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
					
					<div class="portlet box grey-salt  ">
                          <div class="portlet-title" style="background-color: #2B3643;">
                              <div class="caption">
                                  <i class="fa fa-search"></i>검색 & 엑셀 </div>
                              <div class="tools">
				                 <a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
				             </div>
                          </div>
						<div id="searchBar" class="portlet-body form">
							<div class="form-body"
								style="padding-left: 10px; padding-right: 10px;">
								<div class="form-group">
									<form id="empUserListForm" method="POST" action="/psm/testExam/list.html"
										class="mt-repeater form-horizontal">
										<div data-repeater-list="group-a">
											<div data-repeater-item class="mt-repeater-item">
												<div class="mt-repeater-input">
													<label class="control-label">사용자명</label> <br /> 
													<input class="form-control" name="emp_user_name" value="${search.emp_user_name}" />
												</div>
												<div class="mt-repeater-input">
													<label class="control-label">사용자ID</label> <br /> 
													<input class="form-control" name="emp_user_id" value="${search.emp_user_id}" />
												</div>
											</div>
										</div>
										<div align="right">
<!-- 											<button type="reset" -->
<!-- 												class="btn btn-sm red-mint btn-outline sbold uppercase" -->
<!-- 												onclick="resetOptions(empUserListConfig['listUrl'])"> -->
<!-- 												<i class="fa fa-remove"></i> <font>초기화  -->
<!-- 											</button> -->
											<button type="button"
												class="btn btn-sm blue btn-outline sbold uppercase"
												onclick="search()">
												<i class="fa fa-search"></i> 검색
											</button>
											&nbsp;&nbsp;
										</div>

										<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id}" /> 
										<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id}" /> 
										<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>

				
				<table class="table table-striped table-bordered table-hover order-column"
					id="datatable_ajax_2" aria-describedby="datatable_ajax_2_info"
					role="grid">

					<thead>
						<tr role="row" class="heading">
							<th width="10%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								No.</th>
							<th width="45%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								사용자명</th>
							<th width="45%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								사용자ID</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${empty testList}">
								<tr>
									<td colspan="3" align="center">데이터가 없습니다.</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach items="${testList}" var="empUser" varStatus="status">
									<tr>
										<td style="text-align: center;">${status.count }</td>
										<td style="text-align: center;"><c:out value="${empUser.emp_user_name}"/></td>
										<td style="text-align: center;"><c:out value="${empUser.emp_user_id}"/></td>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
				
			</div>
		</div>

	</div>
</div>

<div id="testChart" style="height: 300px;">
</div>
<script>

$(document).ready(function() {
	setChart();
	//drawchart();
});

function setChart() {
	$.ajax({
		type: 'POST',
		url: '/psm/testExam/setChart.html',
		success: function(data) {
			drawchart(data.result);
		}
	});
}

function drawchart (data) {
	//var dataProvider = new Array();
	
	
	/* for(var i=0; i<data.length; i++) {
		var value = {"emp_user_id": data[i].emp_user_id,"cnt": data[i].cnt};
		dataProvider.push(value);
	} */
	alert(data);
	var chart = AmCharts.makeChart( "testChart", {
		  "type": "serial",
		  "theme": "light",
		  "dataProvider": data,
		  "valueAxes": [ {
		    "gridColor": "#FFFFFF",
		    "gridAlpha": 0.2,
		    "dashLength": 0
		  } ],
		  "gridAboveGraphs": true,
		  "startDuration": 1,
		  "graphs": [ {
		    "balloonText": "[[category]]: <b>[[value]]</b>",
		    "fillAlphas": 0.8,
		    "lineAlpha": 0.2,
		    "type": "column",
		    "valueField": "cnt"
		  } ],
		  "chartCursor": {
		    "categoryBalloonEnabled": false,
		    "cursorAlpha": 0,
		    "zoomable": false
		  },
		  "categoryField": "emp_user_id",
		  "categoryAxis": {
		    "gridPosition": "start",
		    "gridAlpha": 0,
		    "tickPosition": "start",
		    "tickLength": 20
		  },
		  "export": {
		    "enabled": true
		  }

		} );
}

function search() {
	$("#empUserListForm").submit();
}
</script>