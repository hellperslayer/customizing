<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script src="${rootPath}/resources/js/jquery/jquery.min.js" type="text/javascript" charset="UTF-8"></script>

<script src="${rootPath}/resources/js/amchart/amcharts.js"></script>
<script src="${rootPath}/resources/js/amchart/serial.js"></script>
<script src="${rootPath}/resources/js/amchart/export.min.js"></script>
<script src="${rootPath}/resources/js/amchart/amstock.js"></script>
<script src="${rootPath}/resources/js/amchart/xy.js"></script>
<script src="${rootPath}/resources/js/amchart/radar.js"></script>
<script src="${rootPath}/resources/js/amchart/light.js"></script>
<script src="${rootPath}/resources/js/amchart/pie.js"></script>
<script src="${rootPath}/resources/js/amchart/gantt.js"></script>
<script src="${rootPath}/resources/js/amchart/gauge.js"></script>

<%-- <link rel="stylesheet" href="${rootPath}/resources/css/export.css" type="text/css" media="all"> --%>
<link rel="stylesheet" href="${pageContext.request.scheme }://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/css/export.css" type="text/css" media="all">
<script src="${rootPath}/resources/js/psm/report/report_charts4.js"></script>
<script src="${rootPath}/resources/js/common/ajax-util.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/common-prototype-util.js" type="text/javascript" charset="UTF-8"></script>

<HEAD>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">

<script type="text/javascript">
function printpr()
{
	$("#printButton").hide();
	$("#saveButton").hide();

	//window.print();
	/* var browser = navigator.userAgent.toLowerCase();
    if ( -1 != browser.indexOf('chrome') ){
               window.print();
    }else if ( -1 != browser.indexOf('trident') ){
               try{
                        //참고로 IE 5.5 이상에서만 동작함

                        //웹 브라우저 컨트롤 생성
                        var webBrowser = '<OBJECT ID="previewWeb" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';

                        //웹 페이지에 객체 삽입
                        document.body.insertAdjacentHTML('beforeEnd', webBrowser);

                        //ExexWB 메쏘드 실행 (7 : 미리보기 , 8 : 페이지 설정 , 6 : 인쇄하기(대화상자))
                        previewWeb.ExecWB(7, 1);

                        //객체 해제
                        previewWeb.outerHTML = "";
               }catch (e) {
                        alert("- 도구 > 인터넷 옵션 > 보안 탭 > 신뢰할 수 있는 사이트 선택\n   1. 사이트 버튼 클릭 > 사이트 추가\n   2. 사용자 지정 수준 클릭 > 스크립팅하기 안전하지 않은 것으로 표시된 ActiveX 컨트롤 (사용)으로 체크\n\n※ 위 설정은 프린트 기능을 사용하기 위함임");
               }
              
    } */
    
    //self.close();
    
	var $form = $("#manageHistForm");
	var log_message_params = '';
	var menu_id = '${menu_id}';
	var log_message_title = '고유식별정보 처리현황보고 출력';
	var log_action = 'PRINT';
	var type = 'add';
	var url = '${rootPath}/report/download.html';
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	sendAjaxPostRequest(url, $form.serialize(), ajaxReport_successHandler, ajaxReport_errorHandler, type);
}

function chk1(){
	$('input[id=radio21]').prop('checked', false);
	$('input[id=radio22]').prop('checked', false);
	$('input[id=radio23]').prop('checked', false);
	if($('#radio11').is(":checked")){
		$('input[id=radio21]').removeAttr('disabled');
		$('input[id=radio22]').removeAttr('disabled');
		$('input[id=radio23]').removeAttr('disabled');
	}else if($('#radio12').is(":checked")){
		$('input[id=radio21]').attr('disabled', 'true');
		$('input[id=radio22]').attr('disabled', 'true');
		$('input[id=radio23]').attr('disabled', 'true');
	}
}

rootPath = '${rootPath}';
contextPath = '${pageContext.servletContext.contextPath}';
var start_date = '${start_date}';
var end_date = '${end_date}';
var period_type = '${period_type}';
var system_seq = '${system_seq}';


function ajaxReport_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = "${rootPath}/loginView.html";
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		window.print();
		self.close();
	}
}

// 관리자 ajax call - 실패
function ajaxReport_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	log_message += codeLogMessage[actionType + "Action"];
	
	alert("실패하였습니다." + textStatus);
}
</script>

<form id="manageHistForm" method="POST">
</form>

<STYLE type="text/css">
<!--
p.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle14
	{style-name:"xl65"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:10.0pt; font-family:굴림; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle14
	{style-name:"xl65"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:10.0pt; font-family:굴림; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle14
	{style-name:"xl65"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:10.0pt; font-family:굴림; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle15
	{style-name:"xl67"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; background-color:#e5e5e5; font-size:10.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle15
	{style-name:"xl67"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; background-color:#e5e5e5; font-size:10.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle15
	{style-name:"xl67"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; background-color:#e5e5e5; font-size:10.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
-->
</STYLE>
</HEAD>

<BODY>
<c:set var="startdate" value="${fn:split(start_date, '-') }"/>
<c:set var="enddate" value="${fn:split(end_date, '-') }"/>
<c:set var="date" value="${fn:split(date, '-') }"/>
<c:choose>
	<c:when test="${period_type == 1 }">
		<c:set var="cur" value="${startdate[1]-0 } 월"/>
		<c:set var="pre" value="이전월"/>
	</c:when>
	<c:when test="${period_type == 2 }">
		<c:choose>
		<c:when test="${startdate[1] == '01' }">
		<c:set var="cur" value="1분기"/>
		</c:when>
		<c:when test="${startdate[1] == '04' }">
		<c:set var="cur" value="2분기"/>
		</c:when>
		<c:when test="${startdate[1] == '07' }">
		<c:set var="cur" value="3분기"/>
		</c:when>
		<c:when test="${startdate[1] == '10' }">
		<c:set var="cur" value="4분기"/>
		</c:when>
		</c:choose>
		<c:set var="pre" value="이전 분기"/>
	</c:when>
	<c:when test="${period_type == 3 }">
		<c:choose>
		<c:when test="${startdate[1] == '01' }">
		<c:set var="cur" value="상반기"/>
		</c:when>
		<c:when test="${startdate[1] == '07' }">
		<c:set var="cur" value="하반기"/>
		</c:when>
		</c:choose>
		<c:set var="pre" value="이전 반기"/>
	</c:when>
	<c:when test="${period_type == 4 }">
		<c:set var="cur" value="${startdate[0]-0 } 년"/>
		<c:set var="pre" value="이전년"/>
	</c:when>
	<c:when test="${period_type == 5 }">
		<c:set var="cur" value="기간"/>
		<c:set var="pre" value="이전 기간"/>
	</c:when>
</c:choose>

<div align="right">
	<input type=button id="printButton" name="printButton"onclick="printpr();" value="출력"/>
	<input type=button id="saveButton" name="saveButton"onclick="saveReport();" value="저장"/>
</div>

<!-- <div align="right">
	<input type=button id="printButton" name="printButton"onclick="exportReport();" value="Export to PDF"/>
</div> -->

<div align="center">
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>


<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;' align="center">
<TABLE border="1" cellspacing="0" align="center" cellpadding="0" style='border-collapse:collapse;border:none;' >
<TR>
	<TD valign="middle" style='width:627;height:123;border-left:double #000000 1.4pt;border-right:double #000000 1.4pt;border-top:double #000000 1.4pt;border-bottom:double #000000 1.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:150%;'><SPAN STYLE='font-size:25.0pt;font-family:"돋움";letter-spacing:-4%;font-weight:"bold";line-height:150%'>고유식별정보 처리 현황 보고</SPAN></P>
	</TD>
</TR>
</TABLE>
</div>
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P id="date" CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'><SPAN STYLE='font-size:20.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";line-height:180%'>
<input type="text" value="${date[0] }.${date[1] }.${date[2] }" style="text-align:center;border: 0;font-size:20.0pt;font-family:'한양중고딕,한컴돋움';font-weight:'bold';line-height:180%"/>
</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'>

<c:if test="${use_reportLogo eq 'Y' }">
<div id="logo" align="center">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" style='width:342;height:77;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<c:choose>
		<c:when test="${filename eq null }" >
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:18.0pt;line-height:160%'><img src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}${logo_report_url}"></SPAN></P>
		</c:when>
		<c:otherwise>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:18.0pt;line-height:160%'><img src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/upload/${filename }"></SPAN></P>
		</c:otherwise>
	</c:choose>
	</TD>
</TR>
</TABLE>
</div>
</c:if>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'>
<c:if test="${use_reportLine eq 'Y' }">
<div id="logo2" align="center">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD rowspan="2" valign="middle" style='width:27;height:114;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-top:2.4pt;text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>결</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-top:2.4pt;text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>재</SPAN></P>
	</TD>
	<TD valign="middle" style='width:91;height:35;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-top:2.4pt;text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>담당</SPAN></P>
	</TD>
	<TD valign="middle" style='width:100;height:35;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-top:2.4pt;text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>CPO</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:91;height:79;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-top:2.4pt;text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>&nbsp;</SPAN></P>
	</TD>
	<TD valign="middle" style='width:100;height:79;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-top:2.4pt;text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>&nbsp;</SPAN></P>
	</TD>
</TR>
</TABLE>
</div>
</c:if>

<div style='page-break-before:always'></div>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:100%;'>
<TR>
	<TD valign="middle" bgcolor="#4e9fd7"  style='width:100%;height:44;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;line-height:160%;width:100%;'>Ⅰ. 점검 개요</SPAN></P>
	</TD>
</TR>
</TABLE>





<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;margin-top:8.0pt;line-height:180%;'> <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>1. 점검 목적</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.5pt;text-indent:-21.2pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ </SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-3%;line-height:180%'>개인정보처리시스템에서 처리하는 개인정보 중 고유식별정보 처리현황을 파악하여 대·내외 평가에 대응할 수 있도록 함</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:41.7pt;text-indent:-23.4pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;margin-top:8.0pt;line-height:180%;'> <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>2. 관련 근거</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 행정자치부 고유식별정보 안전성 확보조치 관리실태 조사</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:54.5pt;text-indent:-36.2pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;- </SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>

공공기관 및 5만명 이상 정보주체의 고유식별정보 처리자에 대한 안전성 확보조치 이행여부를 2년마다 조사하도록 개정됨에 따라 관리실태 조사 추진</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보보호법 제24조(고유식별정보의 처리 제한)</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍  개인정보보호법 시행령 제21조(고유식별정보의 안전성 확보 조치)</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:41.7pt;text-indent:-23.4pt;line-height:180%;'>
<div align="center">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" style='width:595;height:195;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:16.8pt;text-indent:-16.6pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'>제24조(고유식별정보의 처리 제한) ① 개인정보처리자는 다음 각 호의 경우를 제외하고는 법령에 따라 개인을 고유하게 구별하기 위하여 부여된 식별정보로서 대통령령으로 정하는 정보(이하 "고유식별정보"라 한다)를 처리할 수 없다.</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:16.8pt;text-indent:-16.6pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'>② 개인정보처리자가 제1항 각 호에 따라 고유식별정보를 처리하는 경우에는 그 고유식별정보가 분실·도난·유출·위조·변조 또는 훼손되지 아니하도록 대통령령으로 정하는 바에 따라 암호화 등 안전성 확보에 필요한 조치를 하여야 한다.  &lt;개정 2015.7.24.&gt;</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:16.8pt;text-indent:-16.6pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'>③ 행정자치부장관은 처리하는 개인정보의 종류·규모, 종업원 수 및 매출액 규모 등을 고려하여 대통령령으로 정하는 기준에 해당하는 개인정보처리자가 제3항에 따라 안전성 확보에 필요한 조치를 하였는지에 관하여 대통령령으로 정하는 바에 따라 정기적으로 조사하여야 한다.  &lt;신설 2016.3.29.&gt;</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:16.8pt;text-indent:-16.6pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'>④ 행정자치부장관은 대통령령으로 정하는 전문기관으로 하여금 제4항에 따른 조사를 수행하게 할 수 있다.</SPAN></P>
	</TD>
</TR>
</TABLE>
</div>

<P CLASS=HStyle0 STYLE='margin-left:41.7pt;text-indent:-23.4pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>3. 점검 범위 </SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 점검 범위</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:54.5pt;text-indent:-36.2pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;- </SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>

${startdate[0] }년 ${startdate[1] }월 ${startdate[2] }일 ~ ${enddate[0] }년 ${enddate[1] }월 ${enddate[2] }일까지의 개인정보 접속기록 로그</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 점검 대상 개인정보 처리시스템</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'>
<div align="center">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:280;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보처리시스템 명칭</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:280;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>URL</SPAN></P>
	</TD>
</TR>
<c:forEach var="sys" items="${systems }" varStatus="status">
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count }</SPAN></P>
		</TD>
		<TD valign="middle" style='width:280;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${sys.system_name }</SPAN></P>
		</TD>
		<TD valign="middle" style='width:280;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${sys.main_url }</SPAN></P>
		</TD>
	</TR>
<c:set var="numSystems" value="${status.count }"/>
</c:forEach>
</TABLE>
</div>
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>4. 점검 일시</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>
❍&nbsp;<input type="text" value="${date[0] }년 ${date[1] }월 ${date[2] }일" style="border:0;text-align:left;font-size:14.0pt;font-family:'한양신명조,한컴돋움';"/>
</SPAN></P>

<div style='page-break-before:always'></div>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:100%;'>
<TR>
	<TD valign="middle" bgcolor="#4e9fd7"  style='width:100%;height:44;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0><SPAN STYLE='width:100%;font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;line-height:160%'>Ⅱ. 종합 결과</SPAN><SPAN STYLE='font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'> </SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>1. 총평</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 고유식별정보 처리 현황</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:61.6pt;text-indent:-43.3pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;- </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>
${startdate[0] }년 ${startdate[1] }월 ${startdate[2] }일</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>부터 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>
${enddate[0] }년 ${enddate[1] }월 ${enddate[2] }일</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>까지 총 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>
${diffMonth }</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>개월 동안 처리한 고유식별정보는 총 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;font-weight:"bold";color:#0000ff;line-height:180%'>
<fmt:formatNumber value="${totCnt }" pattern="#,###" />
</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;line-height:180%'>건이며, 그 중 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";font-weight:"bold";color:#0000ff;line-height:180%'>
${topType }</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>를 가장 많이 처리하고 있음</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:61.6pt;text-indent:-43.3pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;- 각 고유식별정보별 처리건수</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'>
<div align="center">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:96;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:295;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>고유식별정보</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:212;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>건수</SPAN></P>
	</TD>
</TR>
<c:forEach var="report" items="${typeCnt }" varStatus="status">
<TR>
	<TD valign="middle" style='width:96;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:295;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${report.privacy_desc }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:212;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${report.cnt }" pattern="#,###" /></SPAN></P>
	</TD>
</TR>
</c:forEach>

</TABLE>
</div>
</P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'>
<div align="center">
<div id="chart1" style="width:100%; height: 300px; float: left;"></div>
<div align="center"><img id="chartImage0" style="display: none;" src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/chartImages3/chart1.png"></div>
</div>
</P>

<div style='page-break-before:always'></div>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:100%;'>
<TR>
	<TD valign="middle" bgcolor="#4e9fd7"  style='width:100%;height:44;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0><SPAN STYLE='width:100%;font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;line-height:160%'>Ⅲ. 상세 결과</SPAN><SPAN STYLE='font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'> </SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>1. 시스템별 고유식별정보 처리 결과</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 시스템별 결과</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:61.6pt;text-indent:-43.3pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;- 고유식별정보를 가장 많이 처리하는 시스템은 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;font-weight:"bold";color:#0000ff;line-height:180%'>
${topSystem }
</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;line-height:180%'>(으)로 나타남</SPAN>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:center;text-indent:-22.0pt;line-height:180%;'>
<div id="chart2" style="width:100%; height: 300px;"></div>
<div align="center"><img id="chartImage1" style="display: none;" src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/chartImages3/chart2.png"></div>
</P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'>
<div align="center">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#f2f2f2"  style='width:32;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>시스템명</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>주민등록번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>여권번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>운전면허번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>외국인등록번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:36;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>합계</SPAN></P>
	</TD>
</TR>

<c:forEach items="${systemCnt }" var="item" varStatus="status" >
<TR>
	<TD valign="middle" bgcolor="#f2f2f2"  style='width:32;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:130%;'><SPAN STYLE='font-family:"굴림";letter-spacing:-6%;font-weight:"bold"'>${status.count }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:175;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='line-height:130%;'><SPAN STYLE='font-family:"굴림"'>${item.system_name }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.type1 }" pattern="#,###"/></SPAN></P>
	</TD>	
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.type2 }" pattern="#,###"/></SPAN></P>
	</TD>	
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.type3 }" pattern="#,###"/></SPAN></P>
	</TD>	
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.type10 }" pattern="#,###"/></SPAN></P>
	</TD>	
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.cnt }" pattern="#,###"/></SPAN></P>
	</TD>	
</TR>
</c:forEach>
 
</TABLE>
</div>
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'>

<%-- <div style='page-break-before:always'></div>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>2. 소속사별 고유식별정보 처리 결과</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 소속사별 TOP 10</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:61.6pt;text-indent:-43.3pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;- 고유식별정보를 가장 많이 처리하는 부서는 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;font-weight:"bold";color:#0000ff;line-height:180%'>
${topDept }
</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;line-height:180%'>(으)로 나타남</SPAN>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:center;text-indent:-22.0pt;line-height:180%;'>
<div id="chart3" style="width:100%; height: 300px;"></div>
<div align="center"><img id="chartImage2" style="display: none;" src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/chartImages3/chart3.png"></div>
</P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'>
<div align="center">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#f2f2f2"  style='width:32;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>부서명</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>주민등록번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>여권번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>운전면허번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>외국인등록번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:36;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>합계</SPAN></P>
	</TD>
</TR>

<c:forEach items="${deptCnt }" var="item" varStatus="status" >
<TR>
	<TD valign="middle" bgcolor="#f2f2f2"  style='width:32;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:130%;'><SPAN STYLE='font-family:"굴림";letter-spacing:-6%;font-weight:"bold"'>${status.count }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:175;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='line-height:130%;'><SPAN STYLE='font-family:"굴림"'>${item.dept_name }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.type1 }" pattern="#,###"/></SPAN></P>
	</TD>	
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.type2 }" pattern="#,###"/></SPAN></P>
	</TD>	
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.type3 }" pattern="#,###"/></SPAN></P>
	</TD>	
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.type10 }" pattern="#,###"/></SPAN></P>
	</TD>	
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.cnt }" pattern="#,###"/></SPAN></P>
	</TD>	
</TR>
</c:forEach>
 
</TABLE>
</div>
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'>
 --%>
<div style='page-break-before:always'></div>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>2. 개인별 고유식별정보 처리 결과</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인별 TOP 10</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:61.6pt;text-indent:-43.3pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;- 고유식별정보를 가장 많이 처리하는 사용자는 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;font-weight:"bold";color:#0000ff;line-height:180%'>
${topIndv }
</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;line-height:180%'>(으)로 나타남</SPAN>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:center;text-indent:-22.0pt;line-height:180%;'>
<div id="chart4" style="width:100%; height: 300px;"></div>
<div align="center"><img id="chartImage3" style="display: none;" src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/chartImages3/chart4.png"></div>
</P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'>
<div align="center">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#f2f2f2"  style='width:32;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>소속</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>주민등록번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>여권번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>운전면허번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>외국인등록번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:36;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>합계</SPAN></P>
	</TD>
</TR>

<c:forEach items="${indvCnt }" var="item" varStatus="status" >
<TR>
	<TD valign="middle" bgcolor="#f2f2f2"  style='width:32;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:130%;'><SPAN STYLE='font-family:"굴림";letter-spacing:-6%;font-weight:"bold"'>${status.count }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:175;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='line-height:130%;'><SPAN STYLE='font-family:"굴림"'>${item.emp_user_name }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:175;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='line-height:130%;'><SPAN STYLE='font-family:"굴림"'>${item.dept_name }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.type1 }" pattern="#,###"/></SPAN></P>
	</TD>	
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.type2 }" pattern="#,###"/></SPAN></P>
	</TD>	
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.type3 }" pattern="#,###"/></SPAN></P>
	</TD>	
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.type10 }" pattern="#,###"/></SPAN></P>
	</TD>	
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.cnt }" pattern="#,###"/></SPAN></P>
	</TD>	
</TR>
</c:forEach>
 
</TABLE>
</div>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><BR></P>

<div style='page-break-before:always'></div>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'>
<div align="center">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD rowspan="7" valign="middle" bgcolor="#e5e5e5" style='width:150;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>기관 구분</SPAN></P>
	</TD>
	<TD colspan="2" valign="middle" style='width:550;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>
	<input type="radio" id="radio11" name="radio1" onclick="chk1()"><label for="radio11"> 민간기업(5만 명 이상 정보주체의 고유식별정보처리자)</label>
	</SPAN></P>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>
	&nbsp;&nbsp;<input id="radio21" type="radio" name="radio2"><label for="radio21"> 대기업(소속회사·계열사 포함)</label>
	</SPAN></P>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>
	&nbsp;&nbsp;<input id="radio22" type="radio" name="radio2"><label for="radio22"> 중견기업</label>
	</SPAN></P>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>
	&nbsp;&nbsp;<input id="radio23" type="radio" name="radio2"><label for="radio23"> 중소기업</label>
	</SPAN></P>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>
	<input type="radio" id="radio12" name="radio1" onclick="chk1()"><label for="radio12"> 협회, 단체, 기타(5만 명 이상 정보주체의 고유식별정보처리자)</label>
	</SPAN></P>
	</TD>
</TR>
<TR>
	<TD colspan="2" valign="middle" style='width:550;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>
	<input type="radio" id="radio31" name="radio3"><label for="radio31"> <b>유형1</b></label>
	</SPAN></P>
	</TD>
</TR>
<TR>
	<TD colspan="2" valign="middle" style='width:550;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>
	&nbsp;&nbsp;• 1만 명 이상의 정보주체에 관한 개인정보를 보유한 소상공인, 단체, 개인
	</SPAN></P>
	</TD>
</TR>
<TR>
	<TD colspan="2" valign="middle" style='width:550;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>
	<input type="radio" id="radio32" name="radio3"><label for="radio32"> <b>유형2</b></label>
	</SPAN></P>
	</TD>
</TR>
<TR>
	<TD colspan="2" valign="middle" style='width:550;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>
	&nbsp;&nbsp;• 100만 명 미만의 정보주체에 관한 개인정보를 보유한 중소기업<br>
	&nbsp;&nbsp;• 10만 명 미만의 정보주체에 관한 개인정보를 보유한 대기업, 중견기업, 공공기관<br>
	&nbsp;&nbsp;• 1만 명 미만의 정보주체에 관한 개인정보를 보유한 소상공인, 단체, 개인
	</SPAN></P>
	</TD>
</TR>
<TR>
	<TD colspan="2" valign="middle" style='width:550;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>
	<input type="radio" id="radio33" name="radio3"><label for="radio33"> <b>유형3</b></label>
	</SPAN></P>
	</TD>
</TR>
<TR>
	<TD colspan="2" valign="middle" style='width:550;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>
	&nbsp;&nbsp;• 10만 명 미만의 정보주체에 관한 개인정보를 보유한 대기업, 중견기업, 공공기관<br>
	&nbsp;&nbsp;• 100만 명 미만의 정보주체에 관한 개인정보를 보유한 중소기업, 단체
	</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5" style='width:140;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>고유식별정보가 포함된<br>개인정보처리시스템<br>보유여부</SPAN></P>
	</TD>
	<TD colspan="2" valign="middle" style='width:570;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>
	※ 개인정보처리시스템 : 개인정보를 처리할 수 있도록 체계적으로 구성한 데이터베이스 시스템
	</SPAN></P>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>
	<input type="radio" id="radio41" name="radio4"><label for="radio41"> 보유&nbsp;&nbsp;&nbsp; : 등록기관이 직접 운영·관리(외부 위탁운영 포함)하는 개인정보처리시스템이 있을 경우</label>
	</SPAN></P>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>
	<input type="radio" id="radio42" name="radio4"><label for="radio42"> 미보유 : 개인정보처리시스템을 보유하지 않았거나 상위기관 등에서 운영하는 통합전산시스템
	<br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;등을 통하여 개인정보를 처리하는 경우</label>
	</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5" style='width:150;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>업종 구분</SPAN></P>
	</TD>
	<TD colspan="2" valign="middle" style='width:550;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>
	<select>
	<option>(한국표준산업분유의 대분류 22개 중에서 선택)</option>
	<option>농업, 임업 및 어업</option>
	<option>광업</option>
	<option>제조업</option>
	<option>전기, 가스 증기 및 수도사업</option>
	<option>하수.페기물 처리, 원료재생 및 환경복원업</option>
	<option>건설업</option>
	<option>도매 및 소매업</option>
	<option>운수업</option>
	<option>숙박 및 음식점업</option>
	<option>출판, 영상, 방송통신 및 정보서비스업</option>
	<option>금융 및 보험업</option>
	<option>부동산업 및 임대업</option>
	<option>전문, 과학 및 기술 서비스업</option>
	<option>사업시설관리 및 사업지원 서비스업</option>
	<option>공공행정, 국방 및 사회보장 행정</option>
	<option>교육 서비스업</option>
	<option>보건업 및 사회복지 서비스업</option>
	<option>예술, 스포츠 및 여가관련 서비스업</option>
	<option>협회 및 단체, 수리 및 기타 개인 서비스업</option>
	<option>가구 내 고용활동 및 달리 분류되지 않은 자가소비 생산</option>
	<option>국제 및 외국기관</option>
	<option>기타</option>
	</select>
	</SPAN></P>
	</TD>
</TR>
<TR>
	<TD rowspan="4" valign="middle" bgcolor="#e5e5e5" style='width:150;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>기관 정보</SPAN></P>
	</TD>
	<TD valign="middle" style='width:140;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>기관명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:410;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'><input type="text" size="64"></SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:140;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>사업자등록번호</SPAN></P>
	</TD>
	<TD valign="middle" style='width:410;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'><input type="text" size="64"></SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:140;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>대표자명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:410;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'><input type="text" size="64"></SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:140;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>주소</SPAN></P>
	</TD>
	<TD valign="middle" style='width:410;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0 ><SPAN STYLE='font-family:"굴림";font-weight:"bold"'><input type="text" size="64"></SPAN></P>
	</TD>
</TR>
<TR>
	<TD rowspan="4" valign="middle" bgcolor="#e5e5e5" style='width:150;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>담당자 정보</SPAN></P>
	</TD>
	<TD valign="middle" style='width:140;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>부서명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:410;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'><input type="text" size="64"></SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:140;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>이름</SPAN></P>
	</TD>
	<TD valign="middle" style='width:410;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'><input type="text" size="64"></SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:140;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>회사 전화번호</SPAN></P>
	</TD>
	<TD valign="middle" style='width:410;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'><input type="text" size="64"></SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:140;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>이메일</SPAN></P>
	</TD>
	<TD valign="middle" style='width:410;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'><input type="text" size="64"></SPAN></P>
	</TD>
</TR>
<TR>
	<TD rowspan="4" valign="middle" bgcolor="#e5e5e5" style='width:150;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>고유식별정보<br>보유 현황</SPAN></P>
	</TD>
	<TD valign="middle" style='width:140;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>주민등록번호</SPAN></P>
	</TD>
	<TD valign="middle" style='width:410;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>
	<input type="radio" id="radio51" name="radio5"><label for="radio51"> 무</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="radio" id="radio52" name="radio5"><label for="radio52"> 유</label> ( <input type="text" size="10"> 건 )
	</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:140;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>여권번호</SPAN></P>
	</TD>
	<TD valign="middle" style='width:410;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>
	<input type="radio" id="radio61" name="radio6"><label for="radio61"> 무</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="radio" id="radio62" name="radio6"><label for="radio62"> 유</label> ( <input type="text" size="10"> 건 )
	</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:140;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>운전면허번호</SPAN></P>
	</TD>
	<TD valign="middle" style='width:410;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>
	<input type="radio" id="radio71" name="radio7"><label for="radio71"> 무</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="radio" id="radio72" name="radio7"><label for="radio72"> 유</label> ( <input type="text" size="10"> 건 )
	</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:140;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>외국인등록번호</SPAN></P>
	</TD>
	<TD valign="middle" style='width:410;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>
	<input type="radio" id="radio81" name="radio8"><label for="radio81"> 무</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="radio" id="radio82" name="radio8"><label for="radio82"> 유</label> ( <input type="text" size="10"> 건 )
	</SPAN></P>
	</TD>
</TR>
<TR>
	<TD rowspan="${numSystems }" valign="middle" bgcolor="#e5e5e5" style='width:150;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>고유식별정보가 포함된<br>개인정보<br>처리시스템 현황</SPAN></P>
	</TD>
	<c:forEach var="sys" items="${systems }" varStatus="status">
	<c:if test="${status.count > 1 }">
<TR>
	</c:if>
	<TD valign="middle" style='width:140;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${sys.system_name }</SPAN></P>
	</TD>
	<c:if test="${status.count == 1 }">
	<TD valign="middle" style='width:410;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>
	<input type="text" size="64" placeholder="해당 시스템에 대한 용도와 간략한 설명 기재">
	</SPAN></P>
	</TD>
	</c:if>
	<c:if test="${status.count == 2 }">
	<TD valign="middle" style='width:410;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>
	<input type="text" size="64" placeholder="(예) 내부 인사업무시스템으로 임직원 개인정보 처리">
	</SPAN></P>
	</TD>
	</c:if>
	<c:if test="${status.count == 3 }">
	<TD valign="middle" style='width:410;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>
	<input type="text" size="64" placeholder="(예) ㅇㅇㅇㅇ 고객정보시스템으로 서비스 이용 고객의 개인정보 처리">
	</SPAN></P>
	</TD>
	</c:if>
	<c:if test="${status.count == 4 }">
	<TD valign="middle" style='width:410;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>
	<input type="text" size="64" placeholder="(예) 통합전산시스템으로 하위기관 또는 소속회사가 수집·이용하는 ">
	</SPAN></P>
	</TD>
	</c:if>
	<c:if test="${status.count == 5 }">
	<TD valign="middle" style='width:410;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>
	<input type="text" size="64" placeholder="       개인정보를 통합관리">
	</SPAN></P>
	</TD>
	</c:if>
	<c:if test="${status.count > 5 }">
	<TD valign="middle" style='width:410;border:solid #000000 0.4pt;padding:3.4pt 5.1pt 3.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-family:"굴림";font-weight:"bold"'><input type="text" size="64"></SPAN></P>
	</TD>
	</c:if>
</TR>
	</c:forEach>
</TABLE>
</div>
</P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><BR></P>

</BODY>

</HTML>
