<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<script src="${rootPath}/resources/js/jquery/jquery.min.js" type="text/javascript" charset="UTF-8"></script>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>

<HEAD>
<STYLE type="text/css">
<!--
p.HStyle0
	{style-name:"바탕글";}
li.HStyle0
	{style-name:"바탕글";}
div.HStyle0
	{style-name:"바탕글";}
p.HStyle1
	{style-name:"본문";}
li.HStyle1
	{style-name:"본문";}
div.HStyle1
	{style-name:"본문";}
p.HStyle2
	{style-name:"개요 1";}
li.HStyle2
	{style-name:"개요 1";}
div.HStyle2
	{style-name:"개요 1";}
p.HStyle3
	{style-name:"개요 2";}
li.HStyle3
	{style-name:"개요 2";}
div.HStyle3
	{style-name:"개요 2";}
p.HStyle4
	{style-name:"개요 3";}
li.HStyle4
	{style-name:"개요 3";}
div.HStyle4
	{style-name:"개요 3";}
p.HStyle5
	{style-name:"개요 4";}
li.HStyle5
	{style-name:"개요 4";}
div.HStyle5
	{style-name:"개요 4";}
p.HStyle6
	{style-name:"개요 5";}
li.HStyle6
	{style-name:"개요 5";}
div.HStyle6
	{style-name:"개요 5";}
p.HStyle7
	{style-name:"개요 6";}
li.HStyle7
	{style-name:"개요 6";}
div.HStyle7
	{style-name:"개요 6";}
p.HStyle8
	{style-name:"개요 7";}
li.HStyle8
	{style-name:"개요 7";}
div.HStyle8
	{style-name:"개요 7";}
p.HStyle9
	{style-name:"쪽 번호";}
li.HStyle9
	{style-name:"쪽 번호";}
div.HStyle9
	{style-name:"쪽 번호";}
p.HStyle10
	{style-name:"머리말";}
li.HStyle10
	{style-name:"머리말";}
div.HStyle10
	{style-name:"머리말";}
p.HStyle11
	{style-name:"각주";}
li.HStyle11
	{style-name:"각주";}
div.HStyle11
	{style-name:"각주";}
p.HStyle12
	{style-name:"미주";}
li.HStyle12
	{style-name:"미주";}
div.HStyle12
	{style-name:"미주";}
p.HStyle13
	{style-name:"메모";}
li.HStyle13
	{style-name:"메모";}
div.HStyle13
	{style-name:"메모";}
p.HStyle14
	{style-name:"MS바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle14
	{style-name:"MS바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle14
	{style-name:"MS바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
-->
</STYLE>
</HEAD>

<script type="text/javascript">
function printpr()
{
	$("#printButton").hide();
	
	window.print();
	self.close();
}
</script>

<BODY style="width: 763px;">
<div id="printButton" align="right">
	<input type=button onclick="printpr();" value="출력"/>
</div>
<P CLASS=HStyle14 STYLE='text-align:right;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle14 STYLE='text-align:right;'>
<TABLE width="100%">
<TR>
	<TD colspan="3" valign="middle" style='height:38;border-left:solid #000000 1.4pt;border-right:solid #000000 1.4pt;border-top:solid #000000 1.4pt;border-bottom:solid #000000 1.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:900;line-height:160%'>UBI SAFER-PSM (&nbsp;&nbsp; )월 정기점검 보고서</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle14 STYLE='text-align:right;'></P>
<%-- <c:set var="today" value="${fn:split(today, '-') }"/> --%>
<P CLASS=HStyle14 STYLE='text-align:right;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>점검일자 :&nbsp;&nbsp; ${fn:substring(today,0,4) }&nbsp;&nbsp; 년&nbsp;&nbsp;&nbsp; ${fn:substring(today,4,6) }&nbsp; 월&nbsp;&nbsp;&nbsp; ${fn:substring(today,6,8) } 일</SPAN></P>

<P CLASS=HStyle14 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;'>1. 고객정보</SPAN></P>

<P CLASS=HStyle14 STYLE='text-align:left;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>고객사</SPAN></P>
	</TD>
	<TD valign="middle" style='width:199;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>&nbsp;</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:62;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>부서</SPAN></P>
	</TD>
	<TD valign="middle" style='width:149;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>&nbsp;</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:88;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>고객담당자</SPAN></P>
	</TD>
	<TD valign="middle" style='width:141;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>&nbsp;</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>사업명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:199;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>&nbsp;</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:62;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>도입일자</SPAN></P>
	</TD>
	<TD valign="middle" style='width:149;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>&nbsp;</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:88;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>연락처</SPAN></P>
	</TD>
	<TD valign="middle" style='width:141;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>&nbsp;</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>유지보수</SPAN></P>
	</TD>
	<TD valign="middle" style='width:199;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>□ 무상&nbsp; □ 유상&nbsp; □ 유상대기</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:62;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>점검주기</SPAN></P>
	</TD>
	<TD valign="middle" style='width:149;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>&nbsp;</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:88;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>유지보수기간</SPAN></P>
	</TD>
	<TD valign="middle" style='width:141;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>&nbsp;</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle14 STYLE='text-align:left;'></P>

<P CLASS=HStyle14 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle14 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;'>2. 점검 현황</SPAN></P>

<P CLASS=HStyle14 STYLE='text-align:left;line-height:150%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>&nbsp;</SPAN></P>
	</TD>
	<TD colspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:291;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>Software 수량</SPAN></P>
	</TD>
	<TD colspan="3" valign="middle" bgcolor="#e5e5e5"  style='width:321;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>Software 버전</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>Agent</SPAN></P>
	</TD>
	<TD valign="middle" style='width:146;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>라이센스 :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 개 </SPAN></P>
	</TD>
	<TD valign="middle" style='width:146;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>사용권 :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 개 </SPAN></P>
	</TD>
	<TD colspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:104;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>PSM 버전 정보</SPAN></P>
	</TD>
	<TD valign="middle" style='width:217;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>UBI-SAFER PSM v3.0</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>Manager</SPAN></P>
	</TD>
	<TD valign="middle" style='width:146;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>라이센스 :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 개 </SPAN></P>
	</TD>
	<TD valign="middle" style='width:146;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>사용권 :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 개 </SPAN></P>
	</TD>
	<TD colspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:104;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>&nbsp;</SPAN></P>
	</TD>
	<TD valign="middle" style='width:217;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>&nbsp;</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>Master</SPAN></P>
	</TD>
	<TD valign="middle" style='width:146;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>라이센스 :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 개 </SPAN></P>
	</TD>
	<TD valign="middle" style='width:146;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>사용권 :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 개 </SPAN></P>
	</TD>
	<TD colspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:104;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>&nbsp;</SPAN></P>
	</TD>
	<TD valign="middle" style='width:217;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>&nbsp;</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle14 STYLE='text-align:left;line-height:150%;'></P>

<P CLASS=HStyle14 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle14 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;'>3. 상세 점검</SPAN></P>

<c:set var="list_cnt" value="${fn:length(list)+1 }" />
<P CLASS=HStyle14 STYLE='text-align:left;line-height:110%;'>
<TABLE width="100%" border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD rowspan="${list_cnt }" valign="middle" bgcolor="#e5e5e5"  style='width:71;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>Agent</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:40;height:29;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>연번</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:194;;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>시스템 명</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:90;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-2%;font-weight:bold;line-height:160%'>정상 동작여부</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:102;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:110%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-4%;font-weight:bold;line-height:110%'>로그 건수 (금월)<br/>(${fn:substring(frDate,4,6)}.${fn:substring(frDate,6,8)}~${fn:substring(toDate,4,6)}.${fn:substring(toDate,6,8)})</SPAN></P>
<!-- 	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-2%;font-weight:bold;line-height:160%'>(03.16~04.15)</SPAN></P> -->
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:102;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:110%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-4%;font-weight:bold;line-height:110%'>로그 건수 (전월)<br/>(${fn:substring(frDate2,4,6)}.${fn:substring(frDate2,6,8)}~${fn:substring(toDate2,4,6)}.${fn:substring(toDate2,6,8)})</SPAN></P>
<!-- 	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";letter-spacing:-3%;font-weight:bold;line-height:160%'>(02.16~03.15)</SPAN></P> -->
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:113;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>보고서 생성 여부</SPAN></P>
	</TD>
</TR>
<c:forEach items="${list }" var="item" varStatus="status">
<TR>
	<TD valign="middle" style='width:40;height:10;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";line-height:160%'>${status.count }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:194;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";line-height:160%'>${item.system_name }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:90;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";line-height:160%'>&nbsp;</SPAN></P>
	</TD>
	<TD valign="middle" style='width:102;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";line-height:160%'><fmt:formatNumber value="${item.type2 }" type="number" /></SPAN></P>
	</TD>
	<TD valign="middle" style='width:102;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";line-height:160%'><fmt:formatNumber value="${item.type1 }" type="number" /></SPAN></P>
	</TD>
	<TD valign="middle" style='width:113;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";line-height:160%'>&nbsp;</SPAN></P>
	</TD>
</TR>
</c:forEach>

</TABLE>
<P CLASS=HStyle14 STYLE='text-align:left;line-height:110%;'></P>

<P CLASS=HStyle14 STYLE='text-align:left;line-height:110%;margin-top:5px;'>
<TABLE width="100%" border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD rowspan="3" valign="middle" bgcolor="#e5e5e5"  style='width:70;height:59;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>Manager</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:357;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>점검항목</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:70;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>상태</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:214;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>특이사항</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:357;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:left;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>UI 정상 작동 여부 확인</SPAN></P>
	</TD>
	<TD valign="middle" style='width:70;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>&nbsp;</SPAN></P>
	</TD>
	<TD valign="middle" style='width:214;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='margin-right:10.0pt;text-align:right;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>&nbsp;</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:357;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:left;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>백업파일 통계 생성 확인</SPAN></P>
	</TD>
	<TD valign="middle" style='width:70;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>&nbsp;</SPAN></P>
	</TD>
	<TD valign="middle" style='width:214;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='margin-right:10.0pt;text-align:right;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>&nbsp;</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle14 STYLE='text-align:left;line-height:110%;'></P>

<P CLASS=HStyle14 STYLE='text-align:left;line-height:110%;margin-top:5px;'>
<TABLE width="100%" border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD rowspan="6" valign="middle" bgcolor="#e5e5e5"  style='width:70;height:117;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>Master</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:357;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>점검항목</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:70;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>상태</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:214;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>특이사항</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:357;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:left;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>모니터링 각 항목 정상 출력</SPAN></P>
	</TD>
	<TD valign="middle" style='width:70;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>&nbsp;</SPAN></P>
	</TD>
	<TD valign="middle" style='width:214;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='margin-right:10.0pt;text-align:right;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>&nbsp;</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:357;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:left;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>백업이력조회 정상 동작(점검일 이전 일자까지 생성 확인)</SPAN></P>
	</TD>
	<TD valign="middle" style='width:70;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>&nbsp;</SPAN></P>
	</TD>
	<TD valign="middle" style='width:214;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='margin-right:10.0pt;text-align:right;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>&nbsp;</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:357;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:left;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>통계 데이터 정상 출력(점검일 이전 일자까지 생성 확인)</SPAN></P>
	</TD>
	<TD valign="middle" style='width:70;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>&nbsp;</SPAN></P>
	</TD>
	<TD valign="middle" style='width:214;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='margin-right:10.0pt;text-align:right;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>&nbsp;</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:357;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:left;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>접속기록보고서 정상 생성</SPAN></P>
	</TD>
	<TD valign="middle" style='width:70;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>&nbsp;</SPAN></P>
	</TD>
	<TD valign="middle" style='width:214;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='margin-right:10.0pt;text-align:right;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>&nbsp;</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:357;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:left;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>비정상위험분석 로그 생성(전월 기준 생성 로그 건수 확인)</SPAN></P>
	</TD>
	<TD valign="middle" style='width:70;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>&nbsp;</SPAN></P>
	</TD>
	<TD valign="middle" style='width:214;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='margin-right:10.0pt;text-align:right;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>&nbsp;</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle14 STYLE='text-align:left;line-height:110%;'></P>

<P CLASS=HStyle14 STYLE='text-align:left;line-height:110%;margin-top:5px;'>
<TABLE width="100%" border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD rowspan="4" valign="middle" bgcolor="#e5e5e5"  style='width:70;height:78;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>기타</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:357;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>점검항목</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:70;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>상태</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:214;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>특이사항</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:357;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:left;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>인사연동 정상동작</SPAN></P>
	</TD>
	<TD valign="middle" style='width:70;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>&nbsp;</SPAN></P>
	</TD>
	<TD valign="middle" style='width:214;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='margin-right:10.0pt;text-align:right;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>&nbsp;</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:357;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:left;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>예외처리 DB 등록 여부(등록 건수 명시)</SPAN></P>
	</TD>
	<TD valign="middle" style='width:70;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>&nbsp;</SPAN></P>
	</TD>
	<TD valign="middle" style='width:214;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='margin-right:10.0pt;text-align:right;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>&nbsp;</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:357;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:left;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>관리자 감사 이력 (관리자페이지 최종 접속일)</SPAN></P>
	</TD>
	<TD valign="middle" style='width:70;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>&nbsp;</SPAN></P>
	</TD>
	<TD valign="middle" style='width:214;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='margin-right:10.0pt;text-align:right;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>&nbsp;</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle14 STYLE='text-align:left;line-height:150%;'></P>
<P CLASS=HStyle14 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle14 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;'>4. 점검확인</SPAN></P>

<P CLASS=HStyle14 STYLE='text-align:left;line-height:90%;'>
<TABLE width="100%" border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD colspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:364;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>점검자 의견</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:349;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>고객 의견 </SPAN></P>
	</TD>
</TR>
<TR>
	<TD colspan="2" valign="middle" style='width:364;height:59;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0>&nbsp;</P>
	</TD>
	<TD valign="middle" style='width:349;height:59;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:7.6pt;font-family:"굴림";letter-spacing:-5%;font-weight:bold;line-height:160%'>&nbsp;</SPAN></P>
	</TD>
</TR>
<TR>
	<TD rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:112;height:71;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕"'>정기점검</SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕"'>만족도 조사</SPAN></P>
	</TD>
	<TD colspan="2" valign="middle" style='width:594;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:8.6pt;font-family:"맑은 고딕";line-height:160%'>(주)이지서티 담당 엔지니어의 점검에 만족하십니까? &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; □ 만족&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; □ 불만족</SPAN></P>
	</TD>
</TR>
<TR>
	<TD colspan="2" valign="top" style='width:594;height:43;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:8.6pt;font-family:"맑은 고딕";line-height:160%'>불만족하신다면 그 이유는 무엇입니까?</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle14 STYLE='text-align:left;line-height:90%;'></P>

<P CLASS=HStyle14 STYLE='text-align:left;line-height:140%;margin-top:5px;'>
<TABLE width="100%" border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD colspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:240;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>고객사 담당자</SPAN></P>
	</TD>
	<TD colspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:236;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>유지보수 업체</SPAN></P>
	</TD>
	<TD colspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:236;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>점검자</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:118;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>부&nbsp;&nbsp; 서</SPAN></P>
	</TD>
	<TD valign="middle" style='width:122;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";line-height:160%'>&nbsp;</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:118;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>업&nbsp;&nbsp; 체</SPAN></P>
	</TD>
	<TD valign="middle" style='width:118;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";line-height:160%'>&nbsp;</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:114;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>업&nbsp;&nbsp; 체</SPAN></P>
	</TD>
	<TD valign="middle" style='width:122;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";line-height:160%'>(주)이지서티</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:118;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>성&nbsp;&nbsp; 명 </SPAN></P>
	</TD>
	<TD valign="middle" style='width:122;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";line-height:160%'>&nbsp;(인)</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:118;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>성&nbsp;&nbsp; 명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:118;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";line-height:160%'>(인)</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:114;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>성&nbsp;&nbsp; 명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:122;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";line-height:160%'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(인)</SPAN></P>
	</TD>
</TR>
</TABLE>

</BODY>
</HTML>
