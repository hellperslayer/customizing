<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>UBI SAFER-PSM</title>
<link href="images/favicon.ico"/>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>

<body>

<!-- 
	보고서 생성 대상 시스템 팝업 
	화면 정 가운데 띄워주세요.
-->

<div class="popup_wrap">
	<div class="popup">
        <div class="popup_header">
            <h1 class="align_l">보고서 생성 대상 시스템</h1>
            <a class="btn_close align_r" href="#" title="창닫기"></a>
        </div> <!-- e:popup_header -->
        <div class="popup_contents">
            <p class="popup_comment">보고서 생성 대상을 추가 혹은 제외하여 보고서를 생성 할 수 있습니다.</p>
            <div class="table-box-wrap">
                <div class="table-box">
                    <table>
                        <caption>보고서 생성 대상 시스템 리스트</caption>
                        <colgroup>
                            <col width="15%">
                            <col width="15%">
                            <col width="70%">
                        </colgroup>
                        <thead>
                            <tr>
                            	<th scope="col" width="15%"><input type="checkbox" /></th>
                                <th scope="col" width="15%">CODE</th>
                                <th scope="col" width="70%">시스템명</th>
                            </tr>
                        </thead>
                
                        <tbody>
                            <tr>
                                <td><input type="checkbox" /></td>
                                <td class="code_num">02</td>
                                <td class="system_name">아우디폭스</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" /></td>
                                <td class="code_num">03</td>
                                <td class="system_name">주민등록</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" /></td>
                                <td class="code_num">04</td>
                                <td class="system_name">세외수입</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" /></td>
                                <td class="code_num">05</td>
                                <td class="system_name">인사행정</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" /></td>
                                <td class="code_num">06</td>
                                <td class="system_name">부동산거래</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" /></td>
                                <td class="code_num">31</td>
                                <td class="system_name">농촌산업진흥관리</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" /></td>
                                <td class="code_num">35</td>
                                <td class="system_name">대림산업테스트</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" /></td>
                                <td class="code_num">44</td>
                                <td class="system_name">새움터</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" /></td>
                                <td class="code_num">02</td>
                                <td class="system_name">아우디폭스</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" /></td>
                                <td class="code_num">03</td>
                                <td class="system_name">주민등록</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" /></td>
                                <td class="code_num">04</td>
                                <td class="system_name">세외수입</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" /></td>
                                <td class="code_num">05</td>
                                <td class="system_name">인사행정</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" /></td>
                                <td class="code_num">06</td>
                                <td class="system_name">부동산거래</td>
                            </tr>
                        </tbody>
                    </table>
                </div> <!-- e:table-box -->
            </div> <!-- e:table-box-wrap -->
            <div class="popup_btn align_r">
                <button class="btn btn_m btn_gray" type="button">취소</button>
                <button class="btn btn_m btn_blue" type="button">완료</button>
            </div> 
        </div>  <!-- e:popup_contents -->
    </div>  <!-- e:popup -->
</div> <!-- e:popup_wrap -->



</body>
</html>
