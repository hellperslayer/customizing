<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<script src="${rootPath}/resources/js/jquery/jquery.min.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/amchart/amcharts.js"></script>
<script src="${rootPath}/resources/js/amchart/serial.js"></script>
<script src="${rootPath}/resources/js/amchart/export.min.js"></script>
<script src="${rootPath}/resources/js/amchart/amstock.js"></script>
<script src="${rootPath}/resources/js/amchart/xy.js"></script>
<script src="${rootPath}/resources/js/amchart/radar.js"></script>
<script src="${rootPath}/resources/js/amchart/light.js"></script>
<script src="${rootPath}/resources/js/amchart/pie.js"></script>
<script src="${rootPath}/resources/js/amchart/gantt.js"></script>
<script src="${rootPath}/resources/js/amchart/gauge.js"></script>

<link rel="stylesheet" href="${pageContext.request.scheme }://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/css/export.css" type="text/css" media="all">
<script type="text/javascript" charset="UTF-8">
	rootPath = '${rootPath}';
	contextPath = '${pageContext.servletContext.contextPath}';
	var start_date = '${start_date}';
	var end_date = '${end_date}';
	var strReq = '${CACHE_REQ_TYPE}';
	var period_type = '${period_type}';
	var system_seq = '${system_seq}';
	var use_studentId = '${use_studentId}';
	var download_type = '${download_type}';
	var pdfFullPath = '${pdfFullPath}';
	var use_fullscan = '${ui_type}';
	var quarter_type = '${quarter_type}';
	var compare_type = '${compare_type}';
	
	/* 워드보고서 데이터를 넣기 위한 보고서 키값 */
	var report_seq = '${param.report_seq}';
	var is_last = '${param.is_last}';
	</script>

<script src="${rootPath}/resources/js/psm/report/report_charts_01.js"></script>
<script src="${rootPath}/resources/js/common/ajax-util.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/common-prototype-util.js" type="text/javascript" charset="UTF-8"></script>

<HEAD>
<META NAME="Generator" CONTENT="Hancom HWP 8.5.8.1541">
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">

<script type="text/javascript">

var saveCnt = 0;
function exportChartToImg() {
	var charts = {};

/* 	console.log(AmCharts);
	console.log("#"+AmCharts.charts[0].div); */
	
	if(download_type == 'pdf') {
		$("#buttonDiv").css("display","none");
		$(".downloadChartClass").css("height","950px");
		$(".downloadChartClass").css("width","950px");
		$(".downloadClass").css("width","950px");
		for (var x = 0; x < AmCharts.charts.length; x++) {
			$("#"+AmCharts.charts[x].div.id).css("width","950px");
			console.log("#"+AmCharts.charts[x].div.id+" "+$("#"+AmCharts.charts[x].div.id).css);
		}
	} else {
		$(".downloadClass").css("width","580px");
		$(".downloadChartClass").css("width","580px");
		for (var x = 0; x < AmCharts.charts.length; x++) {
			$(AmCharts.charts[x].div).css("width","580px");
		}	
	}
	
 	var lts = setInterval(function() {
		charts = AmCharts.charts;
		for ( var x in charts) {
			var chart = charts[x];
			console.log(chart["export"]);
			chart["export"].capture({}, function() {
				this.toJPG({}, function(data) {
					var chartName = this.setup.chart.div.id;
					$("img[name="+chartName+"]").attr("src", data);
					$("img[name="+chartName+"]").css("display","");
					console.log(chartName);
					$("#"+chartName).css("display","none");
					
					saveCnt++;
					if(saveCnt == AmCharts.charts.length) {
						$("img[name=logoImg]").attr("src",getBase64Image(document.getElementById("logoImg")));
						$("img[name=logoImg]").css("display","");
						$("#logoImg").remove();
						//exportDocs();
					}
				});
			});
			clearInterval(lts);
		}
	}, 500);
}

function getBase64Image(img) {
  var canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0,img.width,img.height);
  var dataURL = canvas.toDataURL("image/png");
  return dataURL; 
}
	
function exportDocs(inter) {
	var date = new Date();
	var year = date.getFullYear();
	var month = new String(date.getMonth() + 1);
	var day = new String(date.getDate());
	var reportType = '${reportType}';
	
	var log_message_title = "";	
	var admin_user_id = "${userSession.admin_user_id}";
	
	var reportdate = "${start_date}".split('-');
	var reportyear = reportdate[0];
	var reportmonth = reportdate[1];
	
	var proc_date = reportyear+reportmonth;
	
	var periodType = '${period_type}';
	var periodTitle = "";
	if(periodType == 1){
		periodTitle = reportmonth + '월';
		
		
	}else if(periodType == 2){
		var part = "";
		if(reportmonth == 1){
			part = "1";
		}else if(reportmonth == 4){
			part = "2";
		}else if(reportmonth == 7){
			part = "3";
		}else if(reportmonth == 10){
			part = "4";
		}
		periodTitle = part+'분기';
		
	}else if(periodType == 3){
		var part = "";
		if(reportmonth == 1){
			part = "상";
		}else if(reportmonth == 7){
			part = "하";
		}
		periodTitle = part+'반기';
	}else if(periodType == 4){
		periodTitle = '연간';
		
	}else if(periodType == 5){
		periodTitle = '기간';
	}
	
	if(reportType == 1) {
		log_message_title = reportyear+'년_'+ periodTitle + '_담당자용_수준진단보고서';
	} else if(reportType == 3){
		log_message_title = reportyear+'년_'+ periodTitle + '_담당자용_시스템별_수준진단보고서';
	}
	log_message_title = reportyear+'년_'+ periodTitle + '_수준진단보고서';
	
	if(month.length == 1){ 
		month = "0" + month; 
	} 
	if(day.length == 1){ 
		day = "0" + day; 
	} 
	var regdate = year + "" + month + "" + day;
	var agent = navigator.userAgent.toLowerCase();
	
	var header = "<html urn:schemas-microsoft-com:vml" +
	 "xmlns:o='urn:schemas-microsoft-com:office:office' "+
     "xmlns:w='urn:schemas-microsoft-com:office:word' "+
     "xmlns='http://www.w3.org/TR/REC-html40'>"+
     "<head><meta charset='utf-8'><title>Export HTML to Word Document with JavaScript</title></head><body>";
	var footer = "</body></html>";
	var sourceHTML = header+document.getElementById("source-html").innerHTML+footer;
	//var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
	var source = 'data:application/msword;charset=utf-8,' + encodeURIComponent(sourceHTML);
	
	/* 이전 점검보고서 관리 페이지와 호환을 위한 변수 */	
	var reportPath = '/report/addReport.html';
	if(download_type == 'word'){
		reportPath = '/report/addWord.html';
	}
	$.ajax({
		type: 'POST',
		url: rootPath + reportPath,
		data: { 
			"source" : source,
			"log_message_title" : log_message_title,
			"admin_user_id" : admin_user_id,
			"report_type" : reportType,
			"proc_date" : proc_date,
			"pdfFullPath" : pdfFullPath,
			"period_type" : periodType,
			"report_seq" : report_seq,	//word 보고서 데이터를 받을 보고서 키값
			"system_seq": system_seq,
			"system_report": "Y"         // 시스템 보고서 flag
		},
		success: function(data) {
			console.log("is last : " + is_last);  
			if(data == 1) {
				self.close();
			} else if(data == 2) {
				if(confirm('기존 보고서가 있습니다. \n\n덮어 쓰시겠습니까?')){
					alert("보고서 재등록 성공");
					self.close();
				}
			} else if(data == 3 && is_last == 1) {
				alert("보고서 등록 성공");
				parent.$('#loading').hide();
			}else if(data == 3 && is_last == 0){
				
			}else{
				alert("보고서 등록 실패");
				parent.$('#loading').hide();
			}
			//parent.$('#loading').hide();
		}
	});
	
	clearInterval(inter);
}

function saveWordReport() {
	parent.$('#loading').show();
	//exportChartToImg();
	//var inter = setInterval(function() {
			exportDocs(1);
	//}, 500)
}

function printpr()
{
	$("#printButton").hide();
	$("#saveButton").hide();
	
	var $form = $("#manageHistForm");
	var log_message_params = '';
	var menu_id = '${menu_id}';
	var log_message_title = '담당자용_수준진단보고 출력';
	var log_action = 'PRINT';
	var type = 'add';
	var url = '${rootPath}/report/download.html';
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	sendAjaxPostRequest(url, $form.serialize(), ajaxReport_successHandler, ajaxReport_errorHandler, type);
}

function printHtml()
{
	 var divContents = $("#source-html").html();
     var printWindow = window.open('', '', 'height=400,width=800');
     printWindow.document.write('<html><head><title>DIV Contents</title>');
     printWindow.document.write('</head><body >');
     printWindow.document.write(divContents);
     printWindow.document.write('</body></html>');
     printWindow.document.close();
     printWindow.print();
	
}


function ajaxReport_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = "${rootPath}/loginView.html";
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		window.print();
		self.close();
	}
}

// 관리자 ajax call - 실패
function ajaxReport_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	log_message += codeLogMessage[actionType + "Action"];
	
	alert("실패하였습니다." + textStatus);
}
</script>

<form id="manageHistForm" method="POST">
</form>

<style>
@media print {
  .amcharts-bg, .amcharts-plot-area, .amcharts-legend-bg, .amcharts-axis-grid {
    display: none;
  }
}

#loading {
 width: 100%;   
 height: 100%;   
 top: 0px;
 left: 0px;
 position: fixed;   
 display: block;   
 opacity: 0.7;   
 background-color: #fff;   
 z-index: 99;   
 text-align: center; }  
  
#loading-image {   
 position: absolute;   
 top: 50%;   
 left: 50%;  
 z-index: 100; } 
</style>


<script type="text/javascript">
$(document).ready(function() { 
	$('#loading').hide();
	//$('#loading').show();	
	if(download_type == 'word'){
		saveWordReport();
	}
});

$(window).load(function() {   
	$('#loading').hide();
});
</script>

<div id="loading"><img id="loading-image" width="30px;" src="${rootPath}/resources/image/loading3.gif" alt="Loading..." /></div>

<STYLE type="text/css">
p.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle7
/* 	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;} */
p.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle14
	{style-name:"xl65"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:12.0pt; font-family:굴림; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle14
	{style-name:"xl65"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:12.0pt; font-family:굴림; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle14
	{style-name:"xl65"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:12.0pt; font-family:굴림; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle15
	{style-name:"xl67"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; background-color:#e5e5e5; font-size:12.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle15
	{style-name:"xl67"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; background-color:#e5e5e5; font-size:12.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle15
	{style-name:"xl67"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; background-color:#e5e5e5; font-size:12.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
</STYLE>
</HEAD>

<BODY>
<c:set var="startdate" value="${fn:split(start_date, '-') }"/>
<c:set var="enddate" value="${fn:split(end_date, '-') }"/>
<c:set var="date" value="${fn:split(date, '-') }"/>
<c:choose>
	<c:when test="${period_type == 1 }">
		<c:set var="period" value="1 개월"/>
		<c:set var="cur" value="${startdate[1]-0 } 월"/>
		<c:choose>
			<c:when test="${compare_type == 1 }"><c:set var="pre" value="이전 월"/></c:when>
			<c:when test="${compare_type == 2 }"><c:set var="pre" value="전년 동월"/></c:when>
		</c:choose>	
		<c:set var="preYear" value="전년 ${cur}"/>
		<c:set var="total" value="누적"/>
	</c:when>
	<c:when test="${period_type == 2 }">
		<c:set var="period" value="3 개월"/>
		<c:choose>
		<c:when test="${startdate[1] == '01' }">
		<c:set var="m1" value="1월"/>
		<c:set var="m2" value="2월"/>
		<c:set var="m3" value="3월"/>
		<c:set var="cur" value="1분기"/>
		</c:when>
		<c:when test="${startdate[1] == '04' }">
		<c:set var="m1" value="4월"/>
		<c:set var="m2" value="5월"/>
		<c:set var="m3" value="6월"/>
		<c:set var="cur" value="2분기"/>
		</c:when>
		<c:when test="${startdate[1] == '07' }">
		<c:set var="m1" value="7월"/>
		<c:set var="m2" value="8월"/>
		<c:set var="m3" value="9월"/>
		<c:set var="cur" value="3분기"/>
		</c:when>
		<c:when test="${startdate[1] == '10' }">
		<c:set var="m1" value="10월"/>
		<c:set var="m2" value="11월"/>
		<c:set var="m3" value="12월"/>
		<c:set var="cur" value="4분기"/>
		</c:when>
		</c:choose>
		
		<c:choose>
		<c:when test="${compare_type == 1 }"><c:set var="pre" value="이전 분기"/></c:when>
		<c:when test="${compare_type == 2 }"><c:set var="pre" value="전년 동분기"/></c:when>
		</c:choose>
		
		<c:set var="preYear" value="전년 ${cur}"/>
		
		<c:set var="total" value="합계"/>
	</c:when>
	<c:when test="${period_type == 3 }">
		<c:choose>
		<c:when test="${startdate[1] == '01' }">
		<c:set var="cur" value="상반기"/>
		</c:when>
		<c:when test="${startdate[1] == '07' }">
		<c:set var="cur" value="하반기"/>
		</c:when>
		</c:choose>
		<c:set var="pre" value="이전 반기"/>
		<c:set var="preYear" value="전년 ${cur}"/>
	</c:when>
	<c:when test="${period_type == 4 }">
		<c:set var="period" value="1 년"/>
		<c:set var="cur" value="금년"/>
		<c:set var="pre" value="전년"/>
		<c:set var="total" value="합계"/>
	</c:when>
	<c:otherwise> 
		<c:set var="cur" value="기간"/>
		<c:set var="pre" value="이전 기간"/>
	</c:otherwise>
</c:choose>
<div id="source-html">
<div align="right" id="buttonDiv">
    <input type=button id="printButton" name="printButton" onclick="printpr();" value="출력"/>
    <input type=button id="saveButton" name="saveButton" onclick="saveWordReport();" value="생성"/>
</div>

<div align="center">
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'></P>
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'>
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'></P>
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<c:if test="${use_reportLine eq 'Y' }">
<div align="right" style="margin-right: 10%;">
${authorize }
</div>
</c:if>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>


<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
    <TD valign="middle" style='width:627;height:142;border-left:double #000000 1.4pt;border-right:double #000000 1.4pt;border-top:double #000000 1.4pt;border-bottom:double #000000 1.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
    <P CLASS=HStyle0 STYLE='text-align:center;line-height:150%;'><SPAN STYLE='font-size:19.0pt;font-family:"돋움";letter-spacing:-4%;font-weight:"bold";font-style:"italic";line-height:150%'>안전한 개인정보 관리를 위한</SPAN></P>
    <P CLASS=HStyle0 STYLE='text-align:center;line-height:150%;'><SPAN STYLE='font-size:25.0pt;font-family:"돋움";letter-spacing:-4%;font-weight:"bold";line-height:150%'>개인정보 접속기록 점검 보고서</SPAN></P>
    </TD>
</TR>
</TABLE>
</div>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'></P>
<c:choose>
    <c:when test="${download_type eq 'pdf' }">
        <P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
        <P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
    </c:when>
    <c:otherwise>
        <c:choose>
            <c:when test="${fn:length(systems) == 1 }">
                <P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
            </c:when>
            <c:otherwise>
                <P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
                <P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
            </c:otherwise>
        </c:choose>
    </c:otherwise>  
</c:choose> 


<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'>
<SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#000000;line-height:180%'>
<c:if test="${fn:length(systems) == 1 }" >
    ${systems[0].system_name }
</c:if>
</SPAN>
</P>


<c:choose>
    <c:when test="${download_type eq 'pdf' }">
        <c:choose>
            <c:when test="${fn:length(systems) == 1 }">
                <P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
                <P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
            </c:when>
            <c:otherwise>
                <P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
                <P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
                <P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
            </c:otherwise>
        </c:choose>
    </c:when>
    <c:otherwise>
        <P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
    </c:otherwise>
</c:choose>


<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'><SPAN STYLE='font-size:20.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";line-height:180%'>
    <c:choose>
        <c:when test="${period_type eq 1 }">
            ${startdate[0] }년 ${startdate[1] }월
        </c:when>
        <c:when test="${period_type eq 2 }">
            ${startdate[0] }년 ${cur }
        </c:when>
        <c:when test="${period_type eq 4 }">
            ${startdate[0] }년
        </c:when>
    </c:choose>
</SPAN></P>


<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'>
<!-- 로고 사용여부 -->
<c:if test="${use_reportLogo eq 'Y' }">
    <c:choose>
        <c:when test="${download_type ne 'pdf' }"> <!-- word -->
            <div align="center" style="margin-top: 5%;">
                <TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
                <TR>
                    <TD valign="middle" style='border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                    <c:choose>
                        <c:when test="${filename eq null }" >
                            <div style=" height: 77px; text-align: center; "> <!-- background-color: #ffffff; -->
                                <img width: auto;" id="logoImg" src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}${logo_report_url}">
                                <img style="height: 77; width: auto;" name="logoImg" style="display: none">
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div style="max-height: 70px; text-align: center;"> <!-- max-width: 300px; -->
                                <img style="height: 70px; width: auto;" id="logoImg" src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/upload/${filename }"> <!-- max-width: 300px; -->
                                <img style="height: 70px; width: auto;" name="logoImg" style="display: none">
                            </div>
                        </c:otherwise>
                    </c:choose>
                    </TD>
                </TR>
                </TABLE>
            </div>
        </c:when>
        <c:otherwise> <!-- pdf -->
            <div align="center" style="margin-top: 30%;">
                <TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
                <TR>
                    <TD valign="middle" style='width:342;height:77;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                    <c:choose>
                        <c:when test="${filename eq null }" >
                            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:18.0pt;line-height:160%'>
                                <img id="logoImg" style=" width: 100%;" src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}${logo_report_url}">
                                <img name="logoImg" style="display: none">
                            </SPAN></P>
                        </c:when>
                        <c:otherwise>
                            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:18.0pt;line-height:160%'>
                                <img id="logoImg" style="height: 100%;"  src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/upload/${filename }">
                                <img name="logoImg" style="display: none">
                            </SPAN></P>
                        </c:otherwise>
                    </c:choose>
                    </TD>
                </TR>
                </TABLE>
            </div>
        </c:otherwise>
    </c:choose>
</c:if>



<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'></P>
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'>
<div style='page-break-before:always'></div><br style="page-break-before: always">
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'>
<TABLE  border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:100%;'>
<TR>
	<TD valign="middle" bgcolor="#4e9fd7"  style='width:100%;height:44;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
		<P CLASS=HStyle0>
			<SPAN STYLE='font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;line-height:160%;width:100%;'>Ⅰ. 점검 개요</SPAN>
		</P>
	</TD>
</TR>
</TABLE>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;margin-top:8.0pt;line-height:180%;'> <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>1. 점검 목적</SPAN></P><br>
<P CLASS=HStyle5 STYLE='margin-left:39.5pt;text-indent:-21.2pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍  개인정보취급자가 개인정보처리시스템에 접속한 기록을 수집.보관하여  비정상 접근에 대한 <br>의심행위 등을 분석하여 개인정보의 오•남용 및 유.노출 사고를 미연에 방지하도록 선제적 대응 및 <br>예방체계를 구축하고자 함</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:41.7pt;text-indent:-23.4pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;margin-top:8.0pt;line-height:180%;'> <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>2. 관련 근거</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보보호법 제29조 (안전조치의무)</SPAN></P>
<div align="center">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
    <TD valign="middle" style='width:650;height:90;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
    <P CLASS=HStyle0 STYLE='margin-left:15.4pt;text-indent:-15.2pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'>개인정보처리자는 개인정보가 분실.도난.유출.위조.변조 또는 훼손되지 아니하도록 내부 관리 계획 수립, 접속기록 보관 등<br> 대통령령으로 정하는 바에 따라 안전성 확보에 필요한 기술적.관리적 및 물리적 조치를 하여야 한다.</SPAN></P><br>
    </TD>
</TR>
</TABLE>
</div>
<br>


<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보보호법 시행령 제30조 (개인정보의 안전성 확보 조치)</SPAN></P>
<div align="center">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
    <TD valign="middle" style='width:650;height:90;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
    <P CLASS=HStyle0 STYLE='margin-left:15.4pt;text-indent:-15.2pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'>① 개인정보처리자는 법 제29조에 따라 다음 각 호의 안전성 확보 조치를 하여야 한다. </SPAN></P>
    <P CLASS=HStyle0 STYLE='margin-left:15.4pt;text-indent:-15.2pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'>
        &nbsp;&nbsp;&nbsp;&nbsp;1. 개인정보의 안전한 처리를 위한 내부 관리계획의 수립.시행 <br>
		2. 개인정보에 대한 접근 통제 및 접근 권한의 제한 조치<br>
		3. 개인정보를 안전하게 저장.전송할 수 있는 암호화 기술의 적용 또는 이에 상응하는 조치<br>
		4. 개인정보 침해사고 발생에 대응하기 위한 접속기록의 보관 및 위조.변조 방지를 위한 조치<br>
		5. 개인정보에 대한 보안프로그램의 설치 및 갱신<br>
		6. 개인정보의 안전한 보관을 위한 보관시설의 마련 또는 잠금장치의 설치 등 물리적 조치</SPAN></P><br>
    <P CLASS=HStyle0 STYLE='margin-left:15.4pt;text-indent:-15.2pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'>② 행정안전부장관은 개인정보처리자가 제1항에 따른 안전성 확보 조치를 하도록 시스템을 구축하는 등 필요한 지원을 할 수 있다</SPAN></P><br>
    <P CLASS=HStyle0 STYLE='margin-left:15.4pt;text-indent:-15.2pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'>③ 제1항에 따른 안전성 확보 조치에 관한 세부 기준은 행정안전부장관이 정하여 고시한다.</SPAN></P><br>
    <!-- <P CLASS=HStyle0 STYLE='margin-left:15.4pt;text-indent:-15.2pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'>④ 안전행정부장관은 개인정보 처리방침의 작성지침을 정하여 개인정보처리자에게 그 준수를 권장할 수 있다.  &lt;개정 2013. 3. 23.&gt;</SPAN></P><br> -->
    </TD>
</TR>
</TABLE>
</div>
<br>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ </SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-3%;line-height:180%'>개인정보의 안전성 확보조치 기준 제8조(접속기록의 보관 및 점검)</SPAN></P>

<div align="center">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" style='width:650;height:195;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:15.4pt;text-indent:-15.2pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'>① 개인정보처리자는 개인정보취급자가 개인정보처리 시스템에 접속한 기록을 1년 이상 보관․관리하여야 한다. 다만, 5만 명 이상의 정보 주체에 관하여<br>개인정보를 처리하거나, 고유식별정보 또는 민감정보를 처리하는 개인 정보처리시스템의 경우에는 2년 이상 보관·관리하여야 한다.</SPAN></P><br>
	<P CLASS=HStyle0 STYLE='margin-left:16.8pt;text-indent:-16.6pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'>② 개인정보처리자는 개인정보의 오·남용, 분실․도난․유출․위조․변조 또는 훼손 등에 대응하기 위하여 개인정보처리시스템의 접속기록 등을 월 1회 이상<br>점검하여야 한다. 특히 개인정보를 다운로드한 것이 발견되었을 경우에는 내부 관리계획으로 정하는 바에 따라 그 사유를 반드시 확인하여야 한다. </SPAN></P><br>
	<P CLASS=HStyle0 STYLE='margin-left:16.8pt;text-indent:-16.6pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'>③ 개인정보처리자는 개인정보취급자의 접속기록이 위․변조 및 도난, 분실되지 않도록 해당 접속기록을 안전하게 보관하여야 한다.
</SPAN></P>
	</TD>
</TR>
</TABLE>
</div>

<P CLASS=HStyle0 STYLE='margin-left:41.7pt;text-indent:-23.4pt;line-height:180%;'></P>
<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>
<c:choose>
    <c:when test="${download_type eq 'pdf' }">
        <div style='page-break-before:always'></div>
    </c:when>
    <c:otherwise>
        <div style='page-break-before:auto'></div>    
    </c:otherwise>
</c:choose>


<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'> <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>3. 점검 범위 </SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 점검 기간</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:54.5pt;text-indent:-36.2pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;- ${startdate[0] }년 ${startdate[1] }월 ${startdate[2] }일 ~ ${enddate[0] }년 ${enddate[1] }월 ${enddate[2] }일까지의 개인정보 접속기록 로그</SPAN></P><br>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 점검 방법</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:54.5pt;text-indent:-36.2pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;- 개인정보 처리시스템의 개인정보 처리량 및 이용량 측정</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:54.5pt;text-indent:-36.2pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;- 시스템 / 소속별 / 유형별 개인정보 처리량 및 이용량 통계 활용</SPAN></P><br><br>

<div style='page-break-before:auto'></div>


<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'></P>
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'>
<div style='page-break-before:always'></div><br style="page-break-before: always">
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'></P>
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'>


<TABLE border="1" cellspacing="0" cellpadding="0" style='width:100%;border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#4e9fd7"  style='width:100%;height:44;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0><SPAN STYLE='width:100%;font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;line-height:160%'>Ⅱ. 개인정보 접속기록 상세 분석 결과</SPAN><SPAN STYLE='font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'> </SPAN></P>
	</TD>
</TR>
</TABLE>

<!-- 1. 시스템별 개인정보 접속기록 처리현황 START -->
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>1. 
<c:if test="${fn:length(systems) == 1 }" >
    ${systems[0].system_name }
</c:if> 시스템 
 개인정보 접속기록 처리량 및 이용량
</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-43.3pt;line-height:180%;'>
<SPAN STYLE='margin-left:40.3pt;font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>- </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>${startdate[0] }년 ${startdate[1] }월 ${startdate[2] }일</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>부터</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>${enddate[0] }년 ${enddate[1] }월 ${enddate[2] }일</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>까지 총 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>${period }</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'> 동안 발생한 개인정보 접속기록 처리량은 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>
<fmt:formatNumber value="${logCnt }" pattern="#,###" /></SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>건을 처리하고 있으며 이용량은 
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'><fmt:formatNumber value="${privacyCnt }" pattern="#,###" /></SPAN>건을 처리하고 있음
</SPAN>
</P>


<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:center;text-indent:-22.0pt;line-height:180%;'></P>
<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>
<c:if test="${fn:length(systems) == 1 }" >
    ${systems[0].system_name }
</c:if> 시스템 접속기록 처리량 및 이용량
</SPAN>
</SPAN></P></br>

<div align="center;" style="margin-left: 27px;">

    <c:choose>
	    <c:when test="${download_type eq 'pdf' }">
	       <p style="margin-right: 5%; text-align: right; font-size: small; margin-top: 0">[단위] : ${wordUnit01 } 건</p>
	    </c:when>
	    <c:otherwise>
	       <p style="text-align: right; font-size: small; margin-top: 0">[단위] : ${wordUnit01 } 건</p>
	    </c:otherwise>
    </c:choose>
    
    <div class="downloadClass">
    <TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
    <TR>
        <TD rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
        <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
        </TD>
        <TD rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:150;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
        <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보처리시스템</SPAN></P>
        </TD>
        
        
        <!-- 개인정보 처리량 header -->
        <c:if test="${period_type == 1 }">
            <TD colspan="2"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 처리량</SPAN></P></TD>
        </c:if>
        
        <c:if test="${period_type == 2 }">
            <TD colspan="4"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 처리량</SPAN></P></TD>
        </c:if>
        
        <c:if test="${period_type == 4 }">
            <TD colspan="5"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 처리량</SPAN></P></TD>
        </c:if>
        
        
        <!-- 개인정보 이용량 -->
        <c:if test="${period_type == 1 }">
            <TD colspan="2"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 이용량</SPAN></P></TD>
        </c:if>
        
        <c:if test="${period_type == 2 }">
            <TD colspan="4"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 이용량</SPAN></P></TD>
        </c:if>
        
        <c:if test="${period_type == 4 }">
            <TD colspan="5"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 이용량</SPAN></P></TD>
        </c:if>
    </TR>
    <TR>
        <!-- 개인정보 처리량 -->
        <c:if test="${period_type == 1 }">
            <TD align="middle" bgcolor="#e5e5e5"  style='width:130;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>당월</SPAN></P>
        </TD>
        </c:if>
        
        <c:if test="${period_type == 2   }">
                <TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${m1}</SPAN></P></TD>
                <TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${m2}</SPAN></P></TD>
                <TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${m3}</SPAN></P></TD>
        </c:if>
    
        <c:if test="${period_type == 4 }">
            <c:forEach var="i" varStatus="status" begin="1" end="4" step="1">
            <TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}분기</SPAN></P></TD>
            </c:forEach>
        </c:if>
        
        <c:if test="${period_type == 1 }">
            <TD align="middle" bgcolor="#e5e5e5"  style='width:130;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${total }</SPAN></P>
	        </TD>
        </c:if>
        <c:if test="${period_type == 2 }">
            <TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${total }</SPAN></P>
            </TD>
        </c:if>
        <c:if test="${period_type == 4 }">
            <TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${total }</SPAN></P>
            </TD>
        </c:if>
        
        
        <!-- 개인정보 이용량 -->       
        <c:if test="${period_type == 1 }">
            <TD align="middle" bgcolor="#e5e5e5"  style='width:130;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>당월</SPAN></P>
        </TD>
        </c:if>
        <c:if test="${period_type == 2   }">
                <TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${m1}</SPAN></P></TD>
                <TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${m2}</SPAN></P></TD>
                <TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${m3}</SPAN></P></TD>
        </c:if>
        <c:if test="${period_type == 4 }">
            <c:forEach var="i" varStatus="status" begin="1" end="4" step="1">
            <TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}분기</SPAN></P></TD>
            </c:forEach>
        </c:if>
        
        <c:if test="${period_type == 1 }">
            <TD align="middle" bgcolor="#e5e5e5"  style='width:130;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${total }</SPAN></P>
            </TD>
        </c:if>
        <c:if test="${period_type == 2 }">
            <TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${total }</SPAN></P>
            </TD>
        </c:if>
        <c:if test="${period_type == 4 }">
            <TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${total }</SPAN></P>
            </TD>
        </c:if>
    </TR>
    <c:set var="sumType1" value="0"/><c:set var="sumType2" value="0"/><c:set var="sumType3" value="0"/><c:set var="sumType4" value="0"/><c:set var="sumType5" value="0"/>
    <c:set var="sumType1_1" value="0"/><c:set var="sumType1_2" value="0"/><c:set var="sumType1_3" value="0"/><c:set var="sumType1_4" value="0"/><c:set var="sumType1_5" value="0"/>
    
    <c:forEach items="${empWordGrid01 }" var="i" varStatus="status">
        <TR>
            <TD valign="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}</SPAN></P>
            </TD>
            <TD valign="middle" style='width:150;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-family:"굴림"'>${i.system_name}</SPAN></P>
            </TD>
            
            
            <c:if test="${period_type == 1 }">
                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1}" pattern="#,###" /></SPAN></P>
                </TD>
                <c:set var="sumType1" value="${sumType1 + i.type1 }"/>
            </c:if>
            
            <c:if test="${period_type == 2 }">
                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1}" pattern="#,###" /></SPAN></P></TD>
                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type2}" pattern="#,###" /></SPAN></P></TD>
                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type3}" pattern="#,###" /></SPAN></P></TD>
                <c:set var="sumType1" value="${sumType1 + i.type1 }"/> <c:set var="sumType2" value="${sumType2 + i.type2 }"/> <c:set var="sumType3" value="${sumType3 + i.type3 }"/>
            </c:if>
            
            <!-- 년간보고서 -->
            <c:if test="${period_type == 4 }">
                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1}" pattern="#,###" /></SPAN></P>
                </TD>
                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type2}" pattern="#,###" /></SPAN></P>
                </TD>
                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type3}" pattern="#,###" /></SPAN></P>
                </TD>
                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type4}" pattern="#,###" /></SPAN></P>
                </TD>
                <c:set var="sumType1" value="${sumType1 + i.type1 }"/><c:set var="sumType2" value="${sumType2 + i.type2 }"/><c:set var="sumType3" value="${sumType3 + i.type3 }"/><c:set var="sumType4" value="${sumType4 + i.type4 }"/>
            </c:if>
            <!-- 년간보고서 -->
            
            <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.logcnt}" pattern="#,###" /></SPAN></P>
            </TD>
            <c:set var="sumType5" value="${sumType5 + i.logcnt }"/>
            
	            <c:if test="${period_type == 1 }">
	                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1_1}" pattern="#,###" /></SPAN></P>
	                </TD>
	                <c:set var="sumType1_1" value="${sumType1_1 + i.type1_1 }"/>
	            </c:if>
	            
	            <c:if test="${period_type == 2 }">
	                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1_1}" pattern="#,###" /></SPAN></P></TD>
	                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1_2}" pattern="#,###" /></SPAN></P></TD>
	                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1_3}" pattern="#,###" /></SPAN></P></TD>
	                <c:set var="sumType1_1" value="${sumType1_1 + i.type1_1 }"/> <c:set var="sumType1_2" value="${sumType1_2 + i.type1_2 }"/> <c:set var="sumType1_3" value="${sumType1_3 + i.type1_3 }"/>
	            </c:if>
	            
	            <!-- 년간보고서 -->
	            <c:if test="${period_type == 4 }">
	                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1_1}" pattern="#,###" /></SPAN></P>
	                </TD>
	                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1_2}" pattern="#,###" /></SPAN></P>
	                </TD>
	                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1_3}" pattern="#,###" /></SPAN></P>
	                </TD>
	                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1_4}" pattern="#,###" /></SPAN></P>
	                </TD>
	                <c:set var="sumType1_1" value="${sumType1_1 + i.type1_1 }"/><c:set var="sumType1_2" value="${sumType1_2 + i.type1_2 }"/><c:set var="sumType1_3" value="${sumType1_3 + i.type1_3 }"/><c:set var="sumType1_4" value="${sumType1_4 + i.type1_4 }"/>
	            </c:if>
	            
	            <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.privacyCnt}" pattern="#,###" /></SPAN></P>
	            </TD>
	            <c:set var="sumType1_5" value="${sumType1_5 + i.privacyCnt }"/>
        </TR>
    </c:forEach>
        <TR>
            <TD colspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:165.70pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>총합계</SPAN></P>
            </TD>
            
            <c:if test="${period_type == 1 }">
                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1}" pattern="#,###" /></SPAN></P></TD>
            </c:if>
            
            <c:if test="${period_type == 2 }">
                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1}" pattern="#,###" /></SPAN></P></TD>
                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType2}" pattern="#,###" /></SPAN></P></TD>
                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType3}" pattern="#,###" /></SPAN></P></TD>
            </c:if>
            
            <c:if test="${period_type == 4 }">
                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1}" pattern="#,###" /></SPAN></P>
                </TD>
                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType2}" pattern="#,###" /></SPAN></P>
                </TD>
                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType3}" pattern="#,###" /></SPAN></P>
                </TD>
                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType4}" pattern="#,###" /></SPAN></P>
                </TD>
            </c:if>
            <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType5}" pattern="#,###" /></SPAN></P>
            </TD>
            
	            <c:if test="${period_type == 1 }">
	                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1_1}" pattern="#,###" /></SPAN></P></TD>
	            </c:if>
	            
	            <c:if test="${period_type == 2 }">
	                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1_1}" pattern="#,###" /></SPAN></P></TD>
	                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1_2}" pattern="#,###" /></SPAN></P></TD>
	                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1_3}" pattern="#,###" /></SPAN></P></TD>
	            </c:if>
	            
	            <c:if test="${period_type == 4 }">
	                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1_1}" pattern="#,###" /></SPAN></P>
	                </TD>
	                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1_2}" pattern="#,###" /></SPAN></P>
	                </TD>
	                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1_3}" pattern="#,###" /></SPAN></P>
	                </TD>
	                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1_4}" pattern="#,###" /></SPAN></P>
	                </TD>
	            </c:if>
	            
	            <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1_5}" pattern="#,###" /></SPAN></P>
	            </TD>
        </TR>
    </TABLE>
    </div>
</div>


<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>
<div style='page-break-before:auto'></div>
<!-- 3. 시스템별 개인정보 접속기록 현황(비교) START -->
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>
2. <c:if test="${fn:length(systems) == 1 }" >
    ${systems[0].system_name }
</c:if> 시스템 개인정보 접속기록 처리량 및 이용량 (비교)
</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-43.3pt;line-height:180%;'>
<SPAN STYLE='margin-left:40.3pt;font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>- </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>${startdate[0] }년 ${startdate[1] }월 ${startdate[2] }일</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>부터</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>${enddate[0] }년 ${enddate[1] }월 ${enddate[2] }일</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>까지 총 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>${period }</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'> 동안 발생한 개인정보 접속기록 처리량은 ${pre } 대비</SPAN>
<c:if test="${compareLogcnt == 0}">
	<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>동일</SPAN>
</c:if>
<c:if test="${compareLogcnt > 0 }">
	<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#ff0000;line-height:180%'>▲ <fmt:formatNumber value="${compareLogcnt}" pattern="#,###" /></SPAN>
</c:if>
<c:if test="${compareLogcnt < 0 }">
	<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>▼ <fmt:formatNumber value="${-(compareLogcnt)}" pattern="#,###" /></SPAN>
</c:if>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>

건이며, 개인정보 이용량은 ${pre } 대비</SPAN>
	<c:if test="${comparePrivacyCnt == 0}">
		<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>동일</SPAN>
	</c:if>
	<c:if test="${comparePrivacyCnt > 0 }">
		<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#ff0000;line-height:180%'>▲ <fmt:formatNumber value="${comparePrivacyCnt}" pattern="#,###" /></SPAN>
	</c:if>
	<c:if test="${comparePrivacyCnt < 0}">
		<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>▼ <fmt:formatNumber value="${-(comparePrivacyCnt)}" pattern="#,###" /></SPAN>
	</c:if>
	<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>건을 처리하고 있음.</SPAN>
</P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>
❍ <c:if test="${fn:length(systems) == 1 }" >
    ${systems[0].system_name }
</c:if> 시스템 접속기록 처리량 및 이용량 (비교) </SPAN></P>

<!-- <div align="center" style="margin-left: 27px;"> -->
<div style="margin-left: 27px;">
    <c:choose>
        <c:when test="${download_type eq 'pdf' }">
           <p style="margin-right: 5%; text-align: right; font-size: small; margin-top: 0">[단위] : ${wordUnit01 } 건</p>
        </c:when>
        <c:otherwise>
           <p style="text-align: right; font-size: small; margin-top: 0">[단위] : ${wordUnit01 } 건</p>
        </c:otherwise>
    </c:choose>
	<div class="downloadClass">
	<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
	<TR>
		<TD rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
		</TD>
		<TD rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:150;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보처리시스템</SPAN></P>
		</TD>
		<TD colspan="3"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 처리량</SPAN></P>
		</TD>
		<TD colspan="3"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 이용량</SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${pre }</SPAN></P>
		</TD>
		<TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${cur }</SPAN></P>
		</TD>
		<TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>증감</SPAN></P>
		</TD>
		
			<TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${pre }</SPAN></P>
			</TD>
			<TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${cur }</SPAN></P>
			</TD>
			<TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>증감</SPAN></P>
			</TD>
	</TR>
	<c:set var="sumType1" value="0"/><c:set var="sumType2" value="0"/><c:set var="sumType3" value="0"/><c:set var="sumType4" value="0"/><c:set var="sumType5" value="0"/><c:set var="sumType6" value="0"/>
	<c:forEach items="${comparePrevCntBySystem}" var="i" varStatus="status">
		<TR>
			<TD valign="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:150;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-family:"굴림"'>${i.system_name}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.prevLogCnt}" pattern="#,###" /></SPAN></P>
			</TD>
			<c:set var="prevLogSum" value="${prevLogSum + i.prevLogCnt }" />
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.nowLogCnt}" pattern="#,###" /></SPAN></P>
			</TD>
			<c:set var="nowLogSum" value="${nowLogSum + i.nowLogCnt }" />
            <c:set var="minus" value="${i.nowLogCnt - i.prevLogCnt }" />
            <c:set var="deptLogGap" value="${deptLogGap + minus }" />
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<c:if test="${i.nowLogCnt- i.prevLogCnt == 0}">
					<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#000000'>-</SPAN></P>
				</c:if>
				<c:if test="${i.nowLogCnt- i.prevLogCnt > 0 }">
					<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>▲ <fmt:formatNumber value="${i.nowLogCnt- i.prevLogCnt}" pattern="#,###" /></SPAN></P>
				</c:if>
				<c:if test="${i.nowLogCnt- i.prevLogCnt < 0}">
					<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#0000ff'>▼ <fmt:formatNumber value="${-(i.nowLogCnt- i.prevLogCnt)}" pattern="#,###" /></SPAN></P>
				</c:if>
			</TD>
			
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.prevPrivacyCnt }" pattern="#,###" /></SPAN></P>
			</TD>
			<c:set var="prevPrivacySum" value="${prevPrivacySum + i.prevPrivacyCnt }" />
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.nowPrivacyCnt}" pattern="#,###" /></SPAN></P>
			</TD>
			<c:set var="nowPrivacySum" value="${nowPrivacySum + i.nowPrivacyCnt}" />
            <c:set var="minus" value="${i.nowPrivacyCnt - i.prevPrivacyCnt }" />
            <c:set var="deptPrivGap" value="${deptPrivGap + minus }" />
           
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				 <c:if test="${i.nowPrivacyCnt - i.prevPrivacyCnt == 0}">  
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#000000'>-</SPAN></P>
                </c:if>
                <c:if test="${i.nowPrivacyCnt - i.prevPrivacyCnt > 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>▲ <fmt:formatNumber value="${i.nowPrivacyCnt - i.prevPrivacyCnt }" pattern="#,###" /></SPAN></P>
                </c:if>
                <c:if test="${i.nowPrivacyCnt - i.prevPrivacyCnt < 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#0000ff'>▼ <fmt:formatNumber value="${-(i.nowPrivacyCnt - i.prevPrivacyCnt )}" pattern="#,###" /></SPAN></P>
                </c:if>
			</TD>
		</TR>
	</c:forEach>
        <TR>
            <TD colspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:400;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>총합계</SPAN></P>            
            <TD valign="middle" style='width:60;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${prevLogSum}" pattern="#,###" /></SPAN></P>
            </TD>
            <TD valign="middle" style='width:60;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${nowLogSum}" pattern="#,###" /></SPAN></P>
            </TD>
            <TD valign="middle" style='width:60;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <c:if test="${deptLogGap == 0}">   
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#000000'>-</SPAN></P>
                </c:if>
                <c:if test="${deptLogGap > 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>▲ <fmt:formatNumber value="${deptLogGap }" pattern="#,###" /></SPAN></P>
                </c:if>
                <c:if test="${deptLogGap < 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#0000ff'>▼ <fmt:formatNumber value="${-deptLogGap }" pattern="#,###" /></SPAN></P>
                </c:if>
            </TD>
            
            
           <TD valign="middle" style='width:60;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
           <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${prevPrivacySum }" pattern="#,###" /></SPAN></P>
           </TD>
           <TD valign="middle" style='width:60;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
           <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${nowPrivacySum}" pattern="#,###" /></SPAN></P>
           </TD>
           <TD valign="middle" style='width:60;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
              
              <c:if test="${deptPrivGap == 0}">   
                   <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#000000'>-</SPAN></P>
               </c:if>
               <c:if test="${deptPrivGap > 0}">
                   <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>▲ <fmt:formatNumber value="${deptPrivGap}" pattern="#,###" /></SPAN></P>
               </c:if>
               <c:if test="${deptPrivGap < 0}">
                   <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#0000ff'>▼ <fmt:formatNumber value="${-deptPrivGap}" pattern="#,###" /></SPAN></P>
               </c:if>     
           </TD>
        </TR>
	</TABLE>
	</div>
</div>
<!-- 3. 시스템별 개인정보 접속기록 현황(비교) END -->



<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>
<div style='page-break-before:auto'></div>
<!-- 2. 소속별 개인정보 접속기록 현황(분기,누적) START -->
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>3. 소속별 개인정보 접속기록 처리량 및 이용량</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-43.3pt;line-height:180%;'>
<SPAN STYLE='margin-left:40.3pt;font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>- </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>${startdate[0] }년 ${startdate[1] }월 ${startdate[2] }일</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>부터</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>${enddate[0] }년 ${enddate[1] }월 ${enddate[2] }일</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>까지 총 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>${period }</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'> 동안 발생한 개인정보 접속기록 처리량은 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>
<fmt:formatNumber value="${logCnt }" pattern="#,###" /></SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>건을 처리하고 있으며 이용량은 
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>
<fmt:formatNumber value="${privacyCnt }" pattern="#,###" /></SPAN>
건을 처리하고 있음</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 소속별 접속기록 처리량 및 이용량
</SPAN></P>
<div style="margin-left: 27px">
    <c:choose>
        <c:when test="${download_type eq 'pdf' }">
           <p style="margin-right: 5%; text-align: right; font-size: small; margin-top: 0">[단위] : ${wordUnit01 } 건</p>
        </c:when>
        <c:otherwise>
           <p style="text-align: right; font-size: small; margin-top: 0">[단위] : ${wordUnit01 } 건</p>
        </c:otherwise>
    </c:choose>
    <div class="downloadClass">
    <TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
    <TR>
        <TD rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
        <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
        </TD>
        <TD rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:150;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
        <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>소속</SPAN></P>
        </TD>
        <c:if test="${period_type == 1 }">
            <TD colspan="2"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 처리량</SPAN></P></TD>
        </c:if>
        
        <c:if test="${period_type == 2 }">
            <TD colspan="4"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 처리량</SPAN></P></TD>
        </c:if>
        
        <c:if test="${period_type == 4 }">
            <TD colspan="5"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 처리량</SPAN></P></TD>
        </c:if>
        
        
        <c:if test="${period_type == 1 }">
            <TD colspan="2"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 이용량</SPAN></P></TD>
        </c:if>
        
        <c:if test="${period_type == 2 }">
            <TD colspan="4"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 이용량</SPAN></P></TD>
        </c:if>
        
        <c:if test="${period_type == 4 }">
            <TD colspan="5"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 이용량</SPAN></P></TD>
        </c:if>
    </TR>
    <TR>
        <c:if test="${period_type == 1 }">
        <TD align="middle" bgcolor="#e5e5e5"  style='width:130;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>당월</SPAN></P>
        </TD>
        </c:if>
        
        <c:if test="${period_type == 2   }">
                <TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${m1}</SPAN></P></TD>
                <TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${m2}</SPAN></P></TD>
                <TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${m3}</SPAN></P></TD>
        </c:if>
    
        <c:if test="${period_type == 4 }">
            <c:forEach var="i" varStatus="status" begin="1" end="4" step="1">
            <TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}분기</SPAN></P></TD>
            </c:forEach>
        </c:if>
        
        <c:if test="${period_type == 1 }">
            <TD align="middle" bgcolor="#e5e5e5"  style='width:130;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${total }</SPAN></P>
            </TD>
        </c:if>
        <c:if test="${period_type == 2 }">
            <TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${total }</SPAN></P>
            </TD>
        </c:if>
        <c:if test="${period_type == 4 }">
            <TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${total }</SPAN></P>
            </TD>
        </c:if>
        
        
        <c:if test="${period_type == 1 }">
            <TD align="middle" bgcolor="#e5e5e5"  style='width:130;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>당월</SPAN></P>
            </TD>
            </c:if>
            
            <c:if test="${period_type == 2   }">
                    <TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${m1}</SPAN></P></TD>
                    <TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${m2}</SPAN></P></TD>
                    <TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${m3}</SPAN></P></TD>
            </c:if>
        
            <c:if test="${period_type == 4 }">
                <c:forEach var="i" varStatus="status" begin="1" end="4" step="1">
                <TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}분기</SPAN></P></TD>
                </c:forEach>
            </c:if>
            
            <c:if test="${period_type == 1 }">
	            <TD align="middle" bgcolor="#e5e5e5"  style='width:130;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	                <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${total }</SPAN></P>
	            </TD>
	        </c:if>
	        <c:if test="${period_type == 2 }">
	            <TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	                <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${total }</SPAN></P>
	            </TD>
	        </c:if>
	        <c:if test="${period_type == 4 }">
	            <TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	                <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${total }</SPAN></P>
	            </TD>
	        </c:if>
    </TR>
    
    <c:set var="sumType1" value="0"/><c:set var="sumType2" value="0"/><c:set var="sumType3" value="0"/><c:set var="sumType4" value="0"/><c:set var="sumType5" value="0"/>
    <c:set var="sumType1_1" value="0"/><c:set var="sumType1_2" value="0"/><c:set var="sumType1_3" value="0"/><c:set var="sumType1_4" value="0"/><c:set var="sumType1_5" value="0"/>
    <c:forEach items="${empWordGrid02 }" var="i" varStatus="status">
        <TR>
            <TD valign="middle" bgcolor="#e5e5e5"  style='width:20;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}</SPAN></P>
            </TD>
            <TD valign="middle" style='width:135;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-family:"굴림"'>
                <c:choose>
                    <c:when test="${(i.dept_name eq '') or (i.dept_name eq 'null') or (i.dept_name eq null)  }">
                        부서명 없음
                    </c:when>
                    <c:otherwise>
                        ${i.dept_name}                        
                    </c:otherwise>
                </c:choose>    
            </SPAN></P>
            </TD>
            <c:if test="${period_type == 1 }">
                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1}" pattern="#,###" /></SPAN></P>
                </TD>
                <c:set var="sumType1" value="${sumType1 + i.type1 }"/>
            </c:if>
            
            <c:if test="${period_type == 2 }">
                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1}" pattern="#,###" /></SPAN></P></TD>
                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type2}" pattern="#,###" /></SPAN></P></TD>
                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type3}" pattern="#,###" /></SPAN></P></TD>
                <c:set var="sumType1" value="${sumType1 + i.type1 }"/> <c:set var="sumType2" value="${sumType2 + i.type2 }"/> <c:set var="sumType3" value="${sumType3 + i.type3 }"/>
            </c:if>
            
            <!-- 년간보고서 -->
            <c:if test="${period_type == 4 }">
                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1}" pattern="#,###" /></SPAN></P>
                </TD>
                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type2}" pattern="#,###" /></SPAN></P>
                </TD>
                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type3}" pattern="#,###" /></SPAN></P>
                </TD>
                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type4}" pattern="#,###" /></SPAN></P>
                </TD>
                <c:set var="sumType1" value="${sumType1 + i.type1 }"/><c:set var="sumType2" value="${sumType2 + i.type2 }"/><c:set var="sumType3" value="${sumType3 + i.type3 }"/><c:set var="sumType4" value="${sumType4 + i.type4 }"/>
            </c:if>
            
            <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.logcnt}" pattern="#,###" /></SPAN></P>
            </TD>
            <c:set var="sumType5" value="${sumType5 + i.logcnt }"/>
            
            <c:if test="${period_type == 1 }">
                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1_1}" pattern="#,###" /></SPAN></P>
                </TD>
                <c:set var="sumType1_1" value="${sumType1_1 + i.type1_1 }"/>
            </c:if>
            
            <c:if test="${period_type == 2 }">
                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1_1}" pattern="#,###" /></SPAN></P></TD>
                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1_2}" pattern="#,###" /></SPAN></P></TD>
                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1_3}" pattern="#,###" /></SPAN></P></TD>
                <c:set var="sumType1_1" value="${sumType1_1 + i.type1_1 }"/> <c:set var="sumType1_2" value="${sumType1_2 + i.type1_2 }"/> <c:set var="sumType1_3" value="${sumType1_3 + i.type1_3 }"/>
            </c:if>
            
            <!-- 년간보고서 -->
            <c:if test="${period_type == 4 }">
                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1_1}" pattern="#,###" /></SPAN></P>
                </TD>
                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1_2}" pattern="#,###" /></SPAN></P>
                </TD>
                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1_3}" pattern="#,###" /></SPAN></P>
                </TD>
                <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1_4}" pattern="#,###" /></SPAN></P>
                </TD>
                <c:set var="sumType1_1" value="${sumType1_1 + i.type1_1 }"/><c:set var="sumType1_2" value="${sumType1_2 + i.type1_2 }"/><c:set var="sumType1_3" value="${sumType1_3 + i.type1_3 }"/><c:set var="sumType1_4" value="${sumType1_4 + i.type1_4 }"/>
            </c:if>
            
            <TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.privacyCnt}" pattern="#,###" /></SPAN></P>
            </TD>
            <c:set var="sumType1_5" value="${sumType1_5 + i.privacyCnt }"/>
        </TR>
    </c:forEach>
        <TR>
            <TD colspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:165.70pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>총합계</SPAN></P>
            </TD>
            
            <c:if test="${period_type == 1 }">
                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1}" pattern="#,###" /></SPAN></P></TD>
            </c:if>
            
            <c:if test="${period_type == 2 }">
                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1}" pattern="#,###" /></SPAN></P></TD>
                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType2}" pattern="#,###" /></SPAN></P></TD>
                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType3}" pattern="#,###" /></SPAN></P></TD>
            </c:if>
            
            <c:if test="${period_type == 4 }">
                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1}" pattern="#,###" /></SPAN></P>
                </TD>
                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType2}" pattern="#,###" /></SPAN></P>
                </TD>
                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType3}" pattern="#,###" /></SPAN></P>
                </TD>
                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType4}" pattern="#,###" /></SPAN></P>
                </TD>
            </c:if>
            <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType5}" pattern="#,###" /></SPAN></P>
            </TD> 
            <c:if test="${period_type == 1 }">
                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1_1}" pattern="#,###" /></SPAN></P></TD>
            </c:if>
            
            <c:if test="${period_type == 2 }">
                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1_1}" pattern="#,###" /></SPAN></P></TD>
                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1_2}" pattern="#,###" /></SPAN></P></TD>
                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1_3}" pattern="#,###" /></SPAN></P></TD>
            </c:if>
            
            <c:if test="${period_type == 4 }">
                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1_1}" pattern="#,###" /></SPAN></P>
                </TD>
                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1_2}" pattern="#,###" /></SPAN></P>
                </TD>
                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1_3}" pattern="#,###" /></SPAN></P>
                </TD>
                <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1_4}" pattern="#,###" /></SPAN></P>
                </TD>
            </c:if>
            <TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1_5}" pattern="#,###" /></SPAN></P>
            </TD>
        </TR>
    </TABLE>
    </div>
</div>


<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:center;text-indent:-22.0pt;line-height:180%;'></P>
<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>
<div style='page-break-before:auto'></div>

<!-- 6. 소속별 개인정보 접속기록 현황(비교) START -->
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>4. 소속별 개인정보 접속기록 처리량 및 이용량 (비교)
</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-43.3pt;line-height:180%;'>
<SPAN STYLE='margin-left:40.3pt;font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>- </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>${startdate[0] }년 ${startdate[1] }월 ${startdate[2] }일</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>부터</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>${enddate[0] }년 ${enddate[1] }월 ${enddate[2] }일</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>까지 총 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>${period }</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'> 동안 발생한 개인정보 접속기록 처리량은 ${pre } 대비</SPAN>
<c:if test="${compareLogcnt == 0}">
	<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>동일</SPAN>
</c:if>
<c:if test="${compareLogcnt > 0 }">
	<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#ff0000;line-height:180%'>▲ <fmt:formatNumber value="${compareLogcnt}" pattern="#,###" /></SPAN>
</c:if>
<c:if test="${compareLogcnt < 0}">
	<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>▼ <fmt:formatNumber value="${-(compareLogcnt)}" pattern="#,###" /></SPAN>
</c:if>

<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>건이며, 개인정보 이용량은 ${pre } 대비</SPAN>
<c:if test="${comparePrivacyCnt == 0}">
	<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>동일</SPAN>
</c:if>
<c:if test="${comparePrivacyCnt > 0 }">
	<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#ff0000;line-height:180%'>▲ <fmt:formatNumber value="${comparePrivacyCnt}" pattern="#,###" /></SPAN>
</c:if>
<c:if test="${comparePrivacyCnt < 0}">
	<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>▼ <fmt:formatNumber value="${-(comparePrivacyCnt)}" pattern="#,###" /></SPAN>
</c:if>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>건을 처리하고 있음.</SPAN>
</P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:center;text-indent:-22.0pt;line-height:180%;'></P>
<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 소속별 접속기록 처리량 및 이용량 (비교)</SPAN></P>
<div style="margin-left: 27px">
    <c:choose>
        <c:when test="${download_type eq 'pdf' }">
           <p style="margin-right: 5%; text-align: right; font-size: small; margin-top: 0">[단위] : ${wordUnit01 } 건</p>
        </c:when>
        <c:otherwise>
           <p style="text-align: right; font-size: small; margin-top: 0">[단위] : ${wordUnit01 } 건</p>
        </c:otherwise>
    </c:choose>
    <div class="downloadClass">
	<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
	<TR>
		<TD rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
		</TD>
		<TD rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:150;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>소속</SPAN></P>
		</TD>
		<TD colspan="3"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 처리량</SPAN></P>
		</TD>
		
		<TD colspan="3"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 이용량</SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${pre }</SPAN></P>
		</TD>
		<TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${cur }</SPAN></P>
		</TD>
		<TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>증감</SPAN></P>
		</TD>
		
		<TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${pre }</SPAN></P>
		</TD>
		<TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${cur }</SPAN></P>
		</TD>
		<TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>증감</SPAN></P>
		</TD>
	</TR>
   	
   	<%-- <c:if test="${fn:length(comparePrevCntByDept) >=11}"><c:set var="size" value="${fn:length(comparePrevCntByDept)}"/></c:if>
   	<c:if test="${fn:length(comparePrevCntByDept) <11 && fn:length(comparePrevCntByDept) >1 }"><c:set var="size" value="${fn:length(comparePrevCntByDept)-2}"/></c:if> --%>
	<%-- <c:forEach items="${comparePrevCntByDept}" var="i" varStatus="status" begin="0" end="${size }" step="1"> --%>
	
	<c:set var="nowLogSum" value="0" /><c:set var="minus" value="0" /><c:set var="prevLogSum" value="0" /><c:set var="deptLogGap" value="0" /><c:set var="deptPrivGap" value="0" />
    <c:set var="prevPrivacySum" value="0" /><c:set var="nowPrivacySum" value="0" />
    
    
    <c:forEach items="${comparePrevCntByDept}" var="i" varStatus="status">
        <c:if test="${(i.nowLogCnt != 0) or (i.prevLogCnt != 0) }">
		<TR>
			<TD valign="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:150;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-family:"굴림"'>
			     <c:choose>
                    <c:when test="${(i.dept_name eq '') or (i.dept_name eq 'null') or (i.dept_name eq null) }">
                        부서명 없음
                    </c:when>
                    <c:otherwise>
                        ${i.dept_name}                        
                    </c:otherwise>
                </c:choose>
			</SPAN></P>
			</TD>
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.prevLogCnt}" pattern="#,###" /></SPAN></P>
			</TD>
			<c:set var="prevLogSum" value="${prevLogSum + i.prevLogCnt }" />
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.nowLogCnt}" pattern="#,###" /></SPAN></P>
			</TD>
			<c:set var="nowLogSum" value="${nowLogSum + i.nowLogCnt }" />
            <c:set var="minus" value="${i.nowLogCnt - i.prevLogCnt }" />
            <c:set var="deptLogGap" value="${deptLogGap + minus }" />
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<c:if test="${i.nowLogCnt - i.prevLogCnt == 0}">
					<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#000000'>-</SPAN></P>
				</c:if>
				<c:if test="${i.nowLogCnt - i.prevLogCnt > 0 }">
					<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>▲ <fmt:formatNumber value="${i.nowLogCnt - i.prevLogCnt}" pattern="#,###" /></SPAN></P>
				</c:if>
				<c:if test="${i.nowLogCnt - i.prevLogCnt < 0}">
					<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#0000ff'>▼ <fmt:formatNumber value="${-(i.nowLogCnt - i.prevLogCnt)}" pattern="#,###" /></SPAN></P>
				</c:if>
			</TD>
			
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.prevPrivacyCnt }" pattern="#,###" /></SPAN></P>
			</TD>
			<c:set var="prevPrivacySum" value="${prevPrivacySum + i.prevPrivacyCnt }" />
				<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.nowPrivacyCnt}" pattern="#,###" /></SPAN></P>
				</TD>
				<c:set var="nowPrivacySum" value="${nowPrivacySum + i.nowPrivacyCnt }" />
				<c:set var="minus" value="${i.nowPrivacyCnt - i.prevPrivacyCnt }" />
				<c:set var="deptPrivGap" value="${deptPrivGap + minus}" />
				<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
					<c:if test="${i.nowPrivacyCnt - i.prevPrivacyCnt == 0}">
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#000000'>-</SPAN></P>
					</c:if>
					<c:if test="${i.nowPrivacyCnt - i.prevPrivacyCnt > 0}">
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>▲ <fmt:formatNumber value="${i.nowPrivacyCnt - i.prevPrivacyCnt}" pattern="#,###" /></SPAN></P>
					</c:if>
					<c:if test="${i.nowPrivacyCnt - i.prevPrivacyCnt < 0}">
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#0000ff'>▼ <fmt:formatNumber value="${-(i.nowPrivacyCnt - i.prevPrivacyCnt)}" pattern="#,###" /></SPAN></P>
					</c:if>
				</TD>
		    </TR>
		</c:if>
	</c:forEach>
	
	
        <TR>
            <TD colspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:400;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>총합계</SPAN></P>            
            <TD valign="middle" style='width:60;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${prevLogSum}" pattern="#,###" /></SPAN></P>
            </TD>
            <TD valign="middle" style='width:60;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${nowLogSum}" pattern="#,###" /></SPAN></P>
            </TD>
            <TD valign="middle" style='width:60;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <%-- <p>${numConv } / ${ compareUnit01_num}</p> --%>
                <c:if test="${deptLogGap == 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#000000'>-</SPAN></P>
                </c:if>
                <c:if test="${deptLogGap > 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>▲ <fmt:formatNumber value="${deptLogGap}" pattern="#,###" /></SPAN></P>
                </c:if>
                <c:if test="${deptLogGap < 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#0000ff'>▼ <fmt:formatNumber value="${-deptLogGap}" pattern="#,###" /></SPAN></P>
                </c:if>
            </TD>
            
            <TD valign="middle" style='width:60;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${prevPrivacySum}" pattern="#,###" /></SPAN></P>
            </TD>
            <TD valign="middle" style='width:60;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${nowPrivacySum}" pattern="#,###" /></SPAN></P>
            </TD>
            <TD valign="middle" style='width:60;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <c:if test="${deptPrivGap == 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#000000'>-</SPAN></P>
                </c:if>
                <c:if test="${deptPrivGap > 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>▲ <fmt:formatNumber value="${deptPrivGap}" pattern="#,###" /></SPAN></P>
                </c:if>
                <c:if test="${deptPrivGap < 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#0000ff'>▼ <fmt:formatNumber value="${-deptPrivGap}" pattern="#,###" /></SPAN></P>
                </c:if>
            </TD>
        </TR>
	</TABLE>
	</div>
</div>





<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>
<div style='page-break-before:auto'></div>
<!-- 7. 개인정보 유형별 접속기록 현황 (당월,누적) START -->
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>5. 개인정보 유형별 접속기록 이용량</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-43.3pt;line-height:180%;'>
<SPAN STYLE='margin-left:40.3pt;font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>- </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>${startdate[0] }년 ${startdate[1] }월 ${startdate[2] }일</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>부터</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>${enddate[0] }년 ${enddate[1] }월 ${enddate[2] }일</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>까지 총 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>${period }</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'> 동안 발생한 개인정보 접속기록 이용량은 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>
<fmt:formatNumber value="${privacyCnt }" pattern="#,###" /></SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>건을 처리하고 있으며 개인정보 유형별 현황은 다음과 같음.</SPAN>
</P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보 유형별 접속기록 이용량</SPAN></P>
<div style="margin-left: 27px">
    <c:choose>
        <c:when test="${download_type eq 'pdf' }">
           <p style="margin-right: 5%; text-align: right; font-size: small; margin-top: 0">[단위] : ${wordUnit01 } 건</p>
        </c:when>
        <c:otherwise>
           <p style="text-align: right; font-size: small; margin-top: 0">[단위] : ${wordUnit01 } 건</p>
        </c:otherwise>
    </c:choose>
    <div class="downloadClass">
	<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
	<TR>
		<TD rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
		</TD>
		<TD rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:150;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보유형</SPAN></P>
		</TD>
		<c:if test="${period_type == 1 }">
			<TD colspan="2"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 이용량</SPAN></P></TD>
		</c:if>
		
		<c:if test="${period_type == 2 }">
			<TD colspan="4"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 이용량</SPAN></P></TD>
		</c:if>
		
		<c:if test="${period_type == 4 }">
			<TD colspan="3"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 이용량</SPAN></P></TD>
		</c:if>
		
	</TR>
	<TR>
		<c:if test="${period_type == 1 }">
			<TD align="middle" bgcolor="#e5e5e5"  style='width:130;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>당월</SPAN></P>
		</TD>
		</c:if>
		
		<c:if test="${period_type == 2   }">
				<TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${m1}</SPAN></P></TD>
				<TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${m2}</SPAN></P></TD>
				<TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${m3}</SPAN></P></TD>
		</c:if>
	
		<c:if test="${period_type == 4 }">
			<TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>상반기</SPAN></P></TD>
			<TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>하반기</SPAN></P></TD>
		</c:if>
		
		<c:if test="${period_type == 1 }">
            <TD align="middle" bgcolor="#e5e5e5"  style='width:130;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${total }</SPAN></P>
            </TD>
        </c:if>
        <c:if test="${period_type == 2 }">
            <TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${total }</SPAN></P>
            </TD>
        </c:if>
        <c:if test="${period_type == 4 }">
            <TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${total }</SPAN></P>
            </TD>
        </c:if>
	</TR>
   	<c:set var="sumType1" value="0"/><c:set var="sumType2" value="0"/><c:set var="sumType3" value="0"/><c:set var="sumType4" value="0"/><c:set var="sumType5" value="0"/>
	<c:forEach items="${privacyUseCntByResultType}" var="i" varStatus="status">
	    <c:if test="${(i.privacyCnt ne '0') }">
		<TR>
			<TD valign="middle" bgcolor="#e5e5e5"  style='width:20;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:135;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-family:"굴림"'>${i.code_name}</SPAN></P>
			</TD>
			<c:if test="${period_type == 1 }">
			
				<TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1}" pattern="#,###" /></SPAN></P>
				</TD>
				<c:set var="sumType1" value="${sumType1 + i.type1 }"/>
			</c:if>
			
			<c:if test="${period_type == 2 }">
				<TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1}" pattern="#,###" /></SPAN></P></TD>
				<TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type2}" pattern="#,###" /></SPAN></P></TD>
				<TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type3}" pattern="#,###" /></SPAN></P></TD>
				<c:set var="sumType1" value="${sumType1 + i.type1 }"/> <c:set var="sumType2" value="${sumType2 + i.type2 }"/> <c:set var="sumType3" value="${sumType3 + i.type3 }"/>
			</c:if>
			
			<!-- 년간보고서 -->
			<c:if test="${period_type == 4 }">
				<TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1}" pattern="#,###" /></SPAN></P>
				</TD>
				<TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type2}" pattern="#,###" /></SPAN></P>
				</TD>
				<c:set var="sumType1" value="${sumType1 + i.type1 }"/><c:set var="sumType2" value="${sumType2 + i.type2 }"/><c:set var="sumType3" value="${sumType3 + i.type3 }"/><c:set var="sumType4" value="${sumType4 + i.type4 }"/>
			</c:if>
			<!-- 년간보고서 -->
			
			<TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.privacyCnt}" pattern="#,###" /></SPAN></P>
			</TD>
            <c:set var="sumType5" value="${sumType5 + i.privacyCnt }"/>
		</TR>
		</c:if>
	</c:forEach>
		<TR>
			<TD colspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:165.70pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>총합계</SPAN></P>
			</TD>
			
			<c:if test="${period_type == 1 }">
				<TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1}" pattern="#,###" /></SPAN></P></TD>
			</c:if>
			
			<c:if test="${period_type == 2 }">
				<TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1}" pattern="#,###" /></SPAN></P></TD>
				<TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType2}" pattern="#,###" /></SPAN></P></TD>
				<TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType3}" pattern="#,###" /></SPAN></P></TD>
			</c:if>
			
			<c:if test="${period_type == 4 }">
				<TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1}" pattern="#,###" /></SPAN></P>
				</TD>
				<TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType2}" pattern="#,###" /></SPAN></P>
				</TD>
			</c:if>
			
			<TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType5}" pattern="#,###" /></SPAN></P></TD>
		</TR>
	</TABLE>
	</div>
</div>




<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>
<div style='page-break-before:auto'></div>
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>6. 개인정보 유형별 접속기록 이용량(비교)</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-43.3pt;line-height:180%;'>
<SPAN STYLE='margin-left:40.3pt;font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>- </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>${startdate[0] }년 ${startdate[1] }월 ${startdate[2] }일</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>부터</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>${enddate[0] }년 ${enddate[1] }월 ${enddate[2] }일</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>까지 총 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>${period }</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'> 동안 발생한 개인정보 접속기록 이용량은 ${pre } 대비</SPAN>
<c:if test="${comparePrivacyCnt == 0}">
	<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>동일</SPAN>
</c:if>
<c:if test="${comparePrivacyCnt > 0 }">
	<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#ff0000;line-height:180%'>▲ <fmt:formatNumber value="${comparePrivacyCnt}" pattern="#,###" /></SPAN>
</c:if>
<c:if test="${comparePrivacyCnt < 0}">
	<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>▼ <fmt:formatNumber value="${-(comparePrivacyCnt)}" pattern="#,###" /></SPAN>
</c:if>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>건을 처리하고 있음.</SPAN>
</P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보 유형별 접속기록 이용량(비교)</SPAN></P>
<div style="margin-left: 27px">
    <c:choose>
        <c:when test="${download_type eq 'pdf' }">
           <p style="margin-right: 5%; text-align: right; font-size: small; margin-top: 0">[단위] : ${wordUnit01 } 건</p>
        </c:when>
        <c:otherwise>
           <p style="text-align: right; font-size: small; margin-top: 0">[단위] : ${wordUnit01 } 건</p>
        </c:otherwise>
    </c:choose>
    <div class="downloadClass">
	<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
	<TR>
		<TD rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
		</TD>
		<TD rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:150;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보유형</SPAN></P>
		</TD>
		<TD colspan="3"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 이용량</SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${pre }</SPAN></P>
		</TD>
		<TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${cur }</SPAN></P>
		</TD>
		<TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>증감</SPAN></P>
		</TD>
	</TR>
	<c:set var="sumType1" value="0"/><c:set var="sumType2" value="0"/><c:set var="sumType3" value="0"/><c:set var="sumType4" value="0"/><c:set var="sumType5" value="0"/><c:set var="sumType6" value="0"/>
   	<c:set var="nowLogSum" value="0" /><c:set var="minus" value="0" /><c:set var="prevLogSum" value="0" /><c:set var="deptLogGap" value="0" /><c:set var="deptPrivGap" value="0" />
    <c:set var="prevPrivacySum" value="0" /><c:set var="nowPrivacySum" value="0" /><c:set var="privacyGap" value="0" />
	<c:forEach items="${comparePrevCntByResultType}" var="i" varStatus="status">
	   <c:if test="${(i.prevPrivacyCnt ne '0') or (i.nowPrivacyCnt ne '0') }">
		<TR>
			<TD valign="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:150;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-family:"굴림"'>${i.code_name}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.prevPrivacyCnt}" pattern="#,###" /></SPAN></P>
			</TD>
			<c:set var="prevPrivacySum" value="${prevPrivacySum + i.prevPrivacyCnt }" />
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.nowPrivacyCnt}" pattern="#,###" /></SPAN></P>
			</TD>
			<c:set var="nowPrivacySum" value="${nowPrivacySum + i.nowPrivacyCnt }" />
			<c:set var="minus" value="${i.nowPrivacyCnt - i.prevPrivacyCnt }" />
			<c:set var="privacyGap" value="${privacyGap + minus }" />
			
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<c:if test="${i.nowPrivacyCnt- i.prevPrivacyCnt == 0}">
					<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#000000'>-</SPAN></P>
				</c:if>
				<c:if test="${i.nowPrivacyCnt- i.prevPrivacyCnt > 0 }">
					<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>▲ <fmt:formatNumber value="${i.nowPrivacyCnt- i.prevPrivacyCnt}" pattern="#,###" /></SPAN></P>
				</c:if>
				<c:if test="${i.nowPrivacyCnt- i.prevPrivacyCnt < 0}">
					<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#0000ff'>▼ <fmt:formatNumber value="${-(i.nowPrivacyCnt- i.prevPrivacyCnt)}" pattern="#,###" /></SPAN></P>
				</c:if>
			</TD>
		</TR>
		</c:if>
	</c:forEach>
		<TR>
			<TD colspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:400;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>총합계</SPAN></P>			
			<TD valign="middle" style='width:60;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${prevPrivacySum }" pattern="#,###" /></SPAN></P>
			</TD>
			<TD valign="middle" style='width:60;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${nowPrivacySum}" pattern="#,###" /></SPAN></P>
			</TD>
			<TD valign="middle" style='width:60;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			    <c:if test="${privacyGap == 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#000000'>-</SPAN></P>
                </c:if>
                <c:if test="${privacyGap > 0 }">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>▲ <fmt:formatNumber value="${privacyGap}" pattern="#,###" /></SPAN></P>
                </c:if>
                <c:if test="${privacyGap < 0 }">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#0000ff'>▼ <fmt:formatNumber value="${-privacyGap}" pattern="#,###" /></SPAN></P>
                </c:if>
			</TD>
		</TR>
	</TABLE>
	</div>
</div>



<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>
<div style='page-break-before:auto'></div>
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>7. 개인정보 접속기록 수행업무별 처리량</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-43.3pt;line-height:180%;'>
<SPAN STYLE='margin-left:40.3pt;font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>- </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>${startdate[0] }년 ${startdate[1] }월 ${startdate[2] }일</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>부터</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>${enddate[0] }년 ${enddate[1] }월 ${enddate[2] }일</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>까지 총 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>${period }</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'> 동안 발생한 개인정보 접속기록 처리량은 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>
<fmt:formatNumber value="${logCnt }" pattern="#,###" /></SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>건을 처리하고 있으며 수행업무별 현황은 다음과 같음.</SPAN>
</P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:center;text-indent:-22.0pt;line-height:180%;'></P>
<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보 접속기록 수행업무별 처리량</SPAN></P>
<div style="margin-left: 27px">
    <c:choose>
        <c:when test="${download_type eq 'pdf' }">
           <p style="margin-right: 5%; text-align: right; font-size: small; margin-top: 0">[단위] : ${wordUnit01 } 건</p>
        </c:when>
        <c:otherwise>
           <p style="text-align: right; font-size: small; margin-top: 0">[단위] : ${wordUnit01 } 건</p>
        </c:otherwise>
    </c:choose>
    <div class="downloadClass">
	<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
	<TR>
		<TD rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
		</TD>
		<TD rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:150;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>수행업무</SPAN></P>
		</TD>
		<c:if test="${period_type == 1 }">
			<TD colspan="2"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 처리량</SPAN></P></TD>
		</c:if>
		
		<c:if test="${period_type == 2 }">
			<TD colspan="4"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 처리량</SPAN></P></TD>
		</c:if>
		
		<c:if test="${period_type == 4 }">
			<TD colspan="5"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 처리량</SPAN></P></TD>
		</c:if>
		
	</TR>
	<TR>
		<c:if test="${period_type == 1 }">
			<TD align="middle" bgcolor="#e5e5e5"  style='width:130;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>당월</SPAN></P>
		</TD>
		</c:if>
		
		<c:if test="${period_type == 2   }">
				<TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${m1}</SPAN></P></TD>
				<TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${m2}</SPAN></P></TD>
				<TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${m3}</SPAN></P></TD>
		</c:if>
	
		<c:if test="${period_type == 4 }">
			<c:forEach var="i" varStatus="status" begin="1" end="4" step="1">
			<TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}분기</SPAN></P></TD>
			</c:forEach>
		</c:if>
		
		<c:if test="${period_type == 1 }">
            <TD align="middle" bgcolor="#e5e5e5"  style='width:130;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${total }</SPAN></P>
            </TD>
        </c:if>
        <c:if test="${period_type == 2 }">
            <TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${total }</SPAN></P>
            </TD>
        </c:if>
        <c:if test="${period_type == 4 }">
            <TD align="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${total }</SPAN></P>
            </TD>
        </c:if>
	</TR>
   	<c:set var="sumType1" value="0"/><c:set var="sumType2" value="0"/><c:set var="sumType3" value="0"/><c:set var="sumType4" value="0"/><c:set var="sumType5" value="0"/>
	<c:forEach items="${privacyProcessCntByReqType}" var="i" varStatus="status">
    <c:if test="${i.logcnt ne '0' or (i.type1 ne '0')}">
		<TR>
			<TD valign="middle" bgcolor="#e5e5e5"  style='width:20;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:135;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-family:"굴림"'>${i.code_name}</SPAN></P>
			</TD>
			<c:if test="${period_type == 1 }">
				<TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1}" pattern="#,###" /></SPAN></P>
				</TD>
				<c:set var="sumType1" value="${sumType1 + i.type1 }"/>
			</c:if>
			
			<c:if test="${period_type == 2 }">
				<TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1}" pattern="#,###" /></SPAN></P></TD>
				<TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type2}" pattern="#,###" /></SPAN></P></TD>
				<TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type3}" pattern="#,###" /></SPAN></P></TD>
				<c:set var="sumType1" value="${sumType1 + i.type1 }"/> <c:set var="sumType2" value="${sumType2 + i.type2 }"/> <c:set var="sumType3" value="${sumType3 + i.type3 }"/>
			</c:if>
			
			<!-- 년간보고서 -->
			<c:if test="${period_type == 4 }">
				<TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1}" pattern="#,###" /></SPAN></P>
				</TD>
				<TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type2}" pattern="#,###" /></SPAN></P>
				</TD>
				<TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type3}" pattern="#,###" /></SPAN></P>
				</TD>
				<TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type4}" pattern="#,###" /></SPAN></P>
				</TD>
				<c:set var="sumType1" value="${sumType1 + i.type1 }"/><c:set var="sumType2" value="${sumType2 + i.type2 }"/><c:set var="sumType3" value="${sumType3 + i.type3 }"/><c:set var="sumType4" value="${sumType4 + i.type4 }"/>
			</c:if>
			<!-- 년간보고서 -->
			
			<TD valign="middle" style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.logcnt}" pattern="#,###" /></SPAN></P>
			</TD>
            <c:set var="sumType5" value="${sumType5 + i.logcnt }"/>
		</TR>
	</c:if>
		
	</c:forEach>
		<TR>
			<TD colspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:165.70pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>총합계</SPAN></P>
			</TD>
			
			<c:if test="${period_type == 1 }">
				<TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1}" pattern="#,###" /></SPAN></P></TD>
			</c:if>
			
			<c:if test="${period_type == 2 }">
				<TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1}" pattern="#,###" /></SPAN></P></TD>
				<TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType2}" pattern="#,###" /></SPAN></P></TD>
				<TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType3}" pattern="#,###" /></SPAN></P></TD>
			</c:if>
			
			<c:if test="${period_type == 4 }">
				<TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType1}" pattern="#,###" /></SPAN></P>
				</TD>
				<TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType2}" pattern="#,###" /></SPAN></P>
				</TD>
				<TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType3}" pattern="#,###" /></SPAN></P>
				</TD>
				<TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType4}" pattern="#,###" /></SPAN></P>
				</TD>
			</c:if>
			
			<TD valign="middle" style='width:42.55pt;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${sumType5}" pattern="#,###" /></SPAN></P>
			</TD>
		</TR>
	</TABLE>
	</div>
	<p style="margin-right: 5%;color: red; text-align: right; font-weight: bold; font-size: small; margin-top: 7px;">※ 개인정보 안전성 확보조치 기준에 의거한 수행 업무</p>
</div>
<!-- <P CLASS=HStyle5 STYLE='margin-left:318.3pt;text-indent:-21.0pt;line-height:180%; margin-top: 7px;'><SPAN STYLE='font-size:11pt; color: red; font-family:"한양신명조,한컴돋움";line-height:180%'>※ 개인정보 안전성 확보조치 기준에 의거한 수행 업무</SPAN> -->

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>
<div style='page-break-before:auto'></div>
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>8. 개인정보 접속기록 수행업무별 처리량(비교)</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-43.3pt;line-height:180%;'>
<SPAN STYLE='margin-left:40.3pt;font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>- </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>${startdate[0] }년 ${startdate[1] }월 ${startdate[2] }일</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>부터</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>${enddate[0] }년 ${enddate[1] }월 ${enddate[2] }일</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>까지 총 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>${period }</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'> 동안 발생한 개인정보 접속기록 처리량은 ${pre } 대비</SPAN>
<c:if test="${compareLogcnt == 0}">
	<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>동일</SPAN>
</c:if>
<c:if test="${compareLogcnt > 0 }">
	<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#ff0000;line-height:180%'>▲ <fmt:formatNumber value="${compareLogcnt}" pattern="#,###" /></SPAN>
</c:if>
<c:if test="${compareLogcnt < 0}">
	<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>▼ <fmt:formatNumber value="${-(compareLogcnt)}" pattern="#,###" /></SPAN>
</c:if>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>건을 처리하고 있음.</SPAN>
</P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보 접속기록 수행업무별 처리량(비교)</SPAN></P>
<div style="margin-left: 27px">
    <c:choose>
        <c:when test="${download_type eq 'pdf' }">
           <p style="margin-right: 5%; text-align: right; font-size: small; margin-top: 0">[단위] : ${wordUnit01 } 건</p>
        </c:when>
        <c:otherwise>
           <p style="text-align: right; font-size: small; margin-top: 0">[단위] : ${wordUnit01 } 건</p>
        </c:otherwise>
    </c:choose>
    <div class="downloadClass">
	<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
	<TR>
		<TD rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
		</TD>
		<TD rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:150;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>수행업무</SPAN></P>
		</TD>
		<TD colspan="3"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 처리량</SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${pre }</SPAN></P>
		</TD>
		<TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${cur }</SPAN></P>
		</TD>
		<TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>증감</SPAN></P>
		</TD>
	</TR>
   	<c:set var="sumType1" value="0"/><c:set var="sumType2" value="0"/><c:set var="sumType3" value="0"/><c:set var="sumType4" value="0"/><c:set var="sumType5" value="0"/><c:set var="sumType6" value="0"/>
    <c:set var="nowLogSum" value="0" /><c:set var="minus" value="0" /><c:set var="prevLogSum" value="0" /><c:set var="deptLogGap" value="0" /><c:set var="deptPrivGap" value="0" />
    <c:set var="prevPrivacySum" value="0" /><c:set var="nowPrivacySum" value="0" /><c:set var="privacyGap" value="0" />    
	<c:forEach items="${comparePrevCntByReqType}" var="i" varStatus="status">
    <%-- <%-- <c:if test="${i.nowLogCnt ne '0'}"> --%>
		<TR>
			<TD valign="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:150;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-family:"굴림"'>${i.code_name}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.prevLogCnt}" pattern="#,###" /></SPAN></P>
			</TD>
			<c:set var="prevLogSum" value="${prevLogSum + i.prevLogCnt }" />
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.nowLogCnt}" pattern="#,###" /></SPAN></P>
			</TD>
			<c:set var="nowLogSum" value="${nowLogSum + i.nowLogCnt }" />
			<c:set var="minus" value="${i.nowLogCnt - i.prevLogCnt }" />
			<c:set var="privacyGap" value="${privacyGap + minus }" />
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<c:if test="${i.nowLogCnt- i.prevLogCnt == 0}">
					<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#000000'>-</SPAN></P>
				</c:if>
				<c:if test="${i.nowLogCnt- i.prevLogCnt > 0 }">
					<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>▲ <fmt:formatNumber value="${i.nowLogCnt- i.prevLogCnt}" pattern="#,###" /></SPAN></P>
				</c:if>
				<c:if test="${i.nowLogCnt- i.prevLogCnt < 0}">
					<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#0000ff'>▼ <fmt:formatNumber value="${-(i.nowLogCnt- i.prevLogCnt)}" pattern="#,###" /></SPAN></P>
				</c:if>
			</TD>
		</TR>
	<%-- </c:if> --%>
	</c:forEach>
		<TR>
			<TD colspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:400;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>총합계</SPAN></P>			
			<TD valign="middle" style='width:60;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${prevLogSum}" pattern="#,###" /></SPAN></P>
			</TD>
			<TD valign="middle" style='width:60;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${nowLogSum}" pattern="#,###" /></SPAN></P>
			</TD>
			<TD valign="middle" style='width:60;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<c:if test="${privacyGap == 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#000000'>-</SPAN></P>
                </c:if>
                <c:if test="${privacyGap > 0 }">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>▲ <fmt:formatNumber value="${privacyGap}" pattern="#,###" /></SPAN></P>
                </c:if>
                <c:if test="${privacyGap < 0 }">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#0000ff'>▼ <fmt:formatNumber value="${-privacyGap}" pattern="#,###" /></SPAN></P>
                </c:if>
			</TD>
		</TR>
	</TABLE>
	</div>
	<p style="margin-right: 5%;color: red; text-align: right; font-weight: bold; font-size: small; margin-top: 7px;">※ 개인정보 안전성 확보조치 기준에 의거한 수행 업무</p>
</div>



<!-- <div style='page-break-before:always'></div> -->
<%-- <P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'></P>
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'>
<div style='page-break-before:always'></div><br style="page-break-before: always">
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'></P>
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'>

<TABLE border="1" cellspacing="0" cellpadding="0" style='width:100%;border-collapse:collapse;border:none;'>
<TR>
    <TD valign="middle" bgcolor="#4e9fd7"  style='width:100%;height:44;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
    <P CLASS=HStyle0><SPAN STYLE='width:100%;font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;line-height:160%'>Ⅲ. 종합 결과 </SPAN><SPAN STYLE='font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'> </SPAN></P>
    </TD>
</TR>
</TABLE>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'></P>

<!-- 1. 시스템별 개인정보 접속기록 처리현황 START -->
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>1. 개인정보 접속기록 점검 결과</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 주요 시스템 별 개인정보 접속기록 TOP 10 </SPAN></P>


<c:if test="${reportOption[0].input_chart == 'true' }">
<c:choose>
	<c:when test="${download_type eq 'pdf' }">
		<div class='downloadChartClass' id="chart_new1" style="width: 750px; height: 670px;"></div>
		<div align="center"><img name="chart_new1" style="width:750px; height: 500px; display: none;"></div>
	</c:when>
	<c:otherwise>
	   <div class='downloadChartClass' id="chart_new1" style="width: 750px; height: 500px;"></div>
        <div align="center"><img name="chart_new1" style="width:750px; height: 500px; display: none;"></div>
	</c:otherwise>
</c:choose>
</c:if>

<div style="margin-left: 27px;">
    <c:choose>
        <c:when test="${download_type eq 'pdf' }">
           <p style="margin-right: 5%; text-align: right; font-size: small; margin-top: 0">[단위] : ${wordUnit01 } 건</p>
        </c:when>
        <c:otherwise>
           <p style="text-align: right; font-size: small; margin-top: 0">[단위] : ${wordUnit01 } 건</p>
        </c:otherwise>
    </c:choose>
    <div class="downloadClass">
    <TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
    <TR>
        <TD rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
        <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
        </TD>
        <TD rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:150;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
        <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보처리시스템</SPAN></P>
        </TD>
        <TD colspan="3"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
        <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 처리량</SPAN></P>
        </TD>
        <TD colspan="3"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
        <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 이용량</SPAN></P>
        </TD>
    </TR>
    <TR>
        <TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${pre }</SPAN></P>
        </TD>
        <TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${cur }</SPAN></P>
        </TD>
        <TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>증감</SPAN></P>
        </TD>
        
       <TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
           <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${pre }</SPAN></P>
       </TD>
       <TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
           <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${cur }</SPAN></P>
       </TD>
       <TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
           <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>증감</SPAN></P>
       </TD>
    </TR>
    <c:set var="sumType1" value="0"/><c:set var="sumType2" value="0"/><c:set var="sumType3" value="0"/><c:set var="sumType4" value="0"/><c:set var="sumType5" value="0"/><c:set var="sumType6" value="0"/>
    
    <c:forEach items="${comparePrevCntBySystem}" var="i" varStatus="status" begin="0" end="9">
        <TR>
            <TD valign="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}</SPAN></P>
            </TD>
            <TD valign="middle" style='width:150;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-family:"굴림"'>${i.system_name}</SPAN></P>
            </TD>
            <TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.prevLogCnt}" pattern="#,###" /></SPAN></P>
            </TD>
            <TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.nowLogCnt}" pattern="#,###" /></SPAN></P>
            </TD>
            <TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <c:if test="${i.nowLogCnt- i.prevLogCnt == 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#000000'>-</SPAN></P>
                </c:if>
                <c:if test="${i.nowLogCnt- i.prevLogCnt > 0 }">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>▲ <fmt:formatNumber value="${i.nowLogCnt- i.prevLogCnt}" pattern="#,###" /></SPAN></P>
                </c:if>
                <c:if test="${i.nowLogCnt- i.prevLogCnt < 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#0000ff'>▼ <fmt:formatNumber value="${-(i.nowLogCnt- i.prevLogCnt)}" pattern="#,###" /></SPAN></P>
                </c:if>
            </TD>
            <c:set var="sumType1" value="${sumType1 + i.nowLogCnt }" />
            <c:set var="sumType2" value="${sumType2 + i.prevLogCnt }" />
            
            <TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.prevPrivacyCnt }" pattern="#,###" /></SPAN></P>
            </TD>
            <TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.nowPrivacyCnt}" pattern="#,###" /></SPAN></P>
            </TD>
            <TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <c:if test="${i.nowPrivacyCnt - i.prevPrivacyCnt == 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#000000'>-</SPAN></P>
                </c:if>
                <c:if test="${i.nowPrivacyCnt - i.prevPrivacyCnt > 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>▲ <fmt:formatNumber value="${i.nowPrivacyCnt - i.prevPrivacyCnt}" pattern="#,###" /></SPAN></P>
                </c:if>
                <c:if test="${i.nowPrivacyCnt - i.prevPrivacyCnt < 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#0000ff'>▼ <fmt:formatNumber value="${-(i.nowPrivacyCnt - i.prevPrivacyCnt)}" pattern="#,###" /></SPAN></P>
                </c:if>
            </TD>
            <c:set var="sumType3" value="${sumType3 + i.nowPrivacyCnt }" />
               <c:set var="sumType4" value="${sumType4 + i.prevPrivacyCnt }" />
        </TR>
    </c:forEach>
    </TABLE>
    </div>
</div>

<c:choose>
	<c:when test="${reportOption[0].input_chart == 'true' }">
	    <div style='page-break-before:always'></div>
	</c:when>
	<c:otherwise>
	    <div style='page-break-before:auto'></div>
	    <c:if test="${download_type eq 'pdf' }">
	        <P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
	        <P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
	        <P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
	    </c:if>
	</c:otherwise>
</c:choose>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt; margin-top: 50px; line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 주요 소속별 개인정보 접속기록 TOP 10 </SPAN></P>

<c:if test="${reportOption[0].input_chart == 'true' }">

<c:choose>
    <c:when test="${download_type eq 'pdf' }">
        <div class='downloadChartClass' id="chart_new2" style="width: 750px; height: 500px;"></div>
        <div align="center"><img name="chart_new2" style="width:750px; height: 500px; display: none;"></div>
    </c:when>
    <c:otherwise>
        <div class='downloadChartClass' id="chart_new2" style="width: 750px; height: 500px;"></div>
        <div align="center"><img name="chart_new2" style="width:750px; height: 500px; display: none;"></div>
    </c:otherwise>
</c:choose>
</c:if>



<div style="margin-left: 27px;">
    <c:choose>
        <c:when test="${download_type eq 'pdf' }">
           <p style="margin-right: 5%; text-align: right; font-size: small; margin-top: 0">[단위] : ${wordUnit01 } 건</p>
        </c:when>
        <c:otherwise>
           <p style="text-align: right; font-size: small; margin-top: 0">[단위] : ${wordUnit01 } 건</p>
        </c:otherwise>
    </c:choose>
    <div class="downloadClass">
    <TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
    <TR>
        <TD rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
        <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
        </TD>
        <TD rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:150;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
        <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>소속</SPAN></P>
        </TD>
        <TD colspan="3"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
        <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 처리량</SPAN></P>
        </TD>
        <TD colspan="3"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
        <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 이용량</SPAN></P>
        </TD>
    </TR>
    <TR>
        <TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${pre }</SPAN></P>
        </TD>
        <TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${cur }</SPAN></P>
        </TD>
        <TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>증감</SPAN></P>
        </TD>
        
        <TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${pre }</SPAN></P>
        </TD>
        <TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${cur }</SPAN></P>
        </TD>
        <TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>증감</SPAN></P>
        </TD>
    </TR>
    
    <c:if test="${fn:length(comparePrevCntByDept) >=11}"><c:set var="size" value="${fn:length(comparePrevCntByDept)}"/></c:if>
    <c:if test="${fn:length(comparePrevCntByDept) <11 && fn:length(comparePrevCntByDept) >1 }"><c:set var="size" value="${fn:length(comparePrevCntByDept)-2}"/></c:if>
    <c:set var="sumType1" value="0"/><c:set var="sumType2" value="0"/><c:set var="sumType3" value="0"/><c:set var="sumType4" value="0"/><c:set var="sumType5" value="0"/><c:set var="sumType6" value="0"/>
    <c:forEach items="${comparePrevCntByDept}" var="i" varStatus="status" begin="0" end="9" step="1">
        <c:if test="${(i.nowLogCnt != 0) or (i.prevLogCnt != 0) }">
        <TR>
            <TD valign="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}</SPAN></P>
            </TD>
            <TD valign="middle" style='width:150;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-family:"굴림"'>
                <c:choose>
                    <c:when test="${(i.dept_name eq '') or (i.dept_name eq 'null') or (i.dept_name eq null)  }">
                        부서명 없음
                    </c:when>
                    <c:otherwise>
                        ${i.dept_name}                        
                    </c:otherwise>
                </c:choose> 
            </SPAN></P>
            </TD>
            <TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.prevLogCnt}" pattern="#,###" /></SPAN></P>
            </TD>
            <TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.nowLogCnt}" pattern="#,###" /></SPAN></P>
            </TD>
            <TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <c:if test="${i.nowLogCnt - i.prevLogCnt == 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#000000'>-</SPAN></P>
                </c:if>
                <c:if test="${i.nowLogCnt - i.prevLogCnt > 0 }">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>▲ <fmt:formatNumber value="${i.nowLogCnt - i.prevLogCnt}" pattern="#,###" /></SPAN></P>
                </c:if>
                <c:if test="${i.nowLogCnt - i.prevLogCnt < 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#0000ff'>▼ <fmt:formatNumber value="${-(i.nowLogCnt - i.prevLogCnt)}" pattern="#,###" /></SPAN></P>
                </c:if>
            </TD>
            <c:set var="sumType1" value="${sumType1 + i.nowLogCnt }" />
            <c:set var="sumType2" value="${sumType2 + i.prevLogCnt }" />
            
            
            <TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.prevPrivacyCnt }" pattern="#,###" /></SPAN></P>
            </TD>
            <TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.nowPrivacyCnt}" pattern="#,###" /></SPAN></P>
            </TD>
            <TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <c:if test="${i.nowPrivacyCnt - i.prevPrivacyCnt == 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#000000'>-</SPAN></P>
                </c:if>
                <c:if test="${i.nowPrivacyCnt - i.prevPrivacyCnt > 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>▲ <fmt:formatNumber value="${i.nowPrivacyCnt - i.prevPrivacyCnt}" pattern="#,###" /></SPAN></P>
                </c:if>
                <c:if test="${i.nowPrivacyCnt - i.prevPrivacyCnt < 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#0000ff'>▼ <fmt:formatNumber value="${-(i.nowPrivacyCnt - i.prevPrivacyCnt)}" pattern="#,###" /></SPAN></P>
                </c:if>
            </TD>
            <c:set var="sumType3" value="${sumType3 + i.nowPrivacyCnt }" />
            <c:set var="sumType4" value="${sumType4 + i.prevPrivacyCnt }" />
        </TR>
        </c:if>
    </c:forEach>
    </TABLE>
    </div>
</div>


<c:if test="${reportOption[0].input_chart == 'true' }">
<div style='page-break-before:always'></div>
</c:if>
<c:if test="${reportOption[0].input_chart == 'false' }">
<div style='page-break-before:always'></div>
</c:if>

<c:if test="${download_type ne 'pdf' }">
    <div style='page-break-before:always'></div>    
</c:if>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt; margin-top: 50px; text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보 유형별 접속기록 이용량 TOP 10</SPAN></P>

<c:if test="${reportOption[0].input_chart == 'true' }">

<c:choose>
    <c:when test="${download_type eq 'pdf' }">
        <div class='downloadChartClass' id="chart_new3" style="width: 750px; height: 670px;"></div>
        <div align="center"><img name="chart_new3" style="width:750px; height: 500px; display: none;"></div>
    </c:when>
    <c:otherwise>
       <div class='downloadChartClass' id="chart_new3" style="width: 750px; height: 500px;"></div>
        <div align="center"><img name="chart_new3" style="width:750px; height: 500px; display: none;"></div>
    </c:otherwise>
</c:choose>
</c:if>

<div style="margin-left: 27px;">
    <c:choose>
        <c:when test="${download_type eq 'pdf' }">
           <p style="margin-right: 5%; text-align: right; font-size: small; margin-top: 0">[단위] : ${wordUnit01 } 건</p>
        </c:when>
        <c:otherwise>
           <p style="text-align: right; font-size: small; margin-top: 0">[단위] : ${wordUnit01 } 건</p>
        </c:otherwise>
    </c:choose>
    <div class="downloadClass">
    <TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
    <TR>
        <TD rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
        <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
        </TD>
        <TD rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:150;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
        <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보유형</SPAN></P>
        </TD>
        <TD colspan="3"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
        <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 이용량</SPAN></P>
        </TD>
    </TR>
    <TR>
        <TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${pre }</SPAN></P>
        </TD>
        <TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${cur }</SPAN></P>
        </TD>
        <TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>증감</SPAN></P>
        </TD>
    </TR>
    
    
    
    <c:set var="sumType1" value="0"/><c:set var="sumType2" value="0"/><c:set var="sumType3" value="0"/><c:set var="sumType4" value="0"/><c:set var="sumType5" value="0"/><c:set var="sumType6" value="0"/>
    <c:set var="nowLogSum" value="0" /><c:set var="minus" value="0" /><c:set var="prevLogSum" value="0" /><c:set var="deptLogGap" value="0" /><c:set var="deptPrivGap" value="0" />
    <c:set var="prevPrivacySum" value="0" /><c:set var="nowPrivacySum" value="0" /><c:set var="privacyGap" value="0" />
    <c:forEach items="${comparePrevCntByResultType}" var="i" varStatus="status"  begin="0" end="9" step="1">
        <c:if test="${(i.prevPrivacyCnt ne '0') or (i.nowPrivacyCnt ne '0') }">
        <TR>
            <TD valign="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}</SPAN></P>
            </TD>
            <TD valign="middle" style='width:150;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-family:"굴림"'>${i.code_name}</SPAN></P>
            </TD>
            <TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.prevPrivacyCnt}" pattern="#,###" /></SPAN></P>
            </TD>
            <c:set var="prevPrivacySum" value="${prevPrivacySum + i.prevPrivacyCnt }" />
            <TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.nowPrivacyCnt}" pattern="#,###" /></SPAN></P>
            </TD>
            <c:set var="nowPrivacySum" value="${nowPrivacySum + i.nowPrivacyCnt }" />
            <c:set var="minus" value="${i.nowPrivacyCnt - i.prevPrivacyCnt }" />
            <c:set var="privacyGap" value="${privacyGap + minus }" />
            
            <TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <c:if test="${i.nowPrivacyCnt- i.prevPrivacyCnt == 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#000000'>-</SPAN></P>
                </c:if>
                <c:if test="${i.nowPrivacyCnt- i.prevPrivacyCnt > 0 }">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>▲ <fmt:formatNumber value="${i.nowPrivacyCnt- i.prevPrivacyCnt}" pattern="#,###" /></SPAN></P>
                </c:if>
                <c:if test="${i.nowPrivacyCnt- i.prevPrivacyCnt < 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#0000ff'>▼ <fmt:formatNumber value="${-(i.nowPrivacyCnt- i.prevPrivacyCnt)}" pattern="#,###" /></SPAN></P>
                </c:if>
            </TD>
        </TR>
        </c:if>
    </c:forEach>
    </TABLE>
    </div>
</div>





<!-- 사용자별 -->
<div style='page-break-before:always'></div>
<P CLASS=HStyle5 STYLE='margin-left:40.3pt; margin-top: 50px; text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보 접속기록 사용자 TOP 20 </SPAN></P>

<c:if test="${reportOption[0].input_chart == 'true' }">
<c:choose>
    <c:when test="${download_type eq 'pdf' }">
        <div class='downloadChartClass' id="chart_new5" style="width: 750px; height: 670px;"></div>
        <div align="center"><img name="chart_new5" style="width:750px; height: 500px; display: none;"></div>
    </c:when>
    <c:otherwise>
       <div class='downloadChartClass' id="chart_new5" style="width: 750px; height: 500px;"></div>
        <div align="center"><img name="chart_new5" style="width:750px; height: 500px; display: none;"></div>
    </c:otherwise>
</c:choose>
</c:if>


<div style="margin-left: 27px;">
    <c:choose>
        <c:when test="${download_type eq 'pdf' }">
           <p style="margin-right: 5%; text-align: right; font-size: small; margin-top: 0">[단위] : ${wordUnit01 } 건</p>
        </c:when>
        <c:otherwise>
           <p style="text-align: right; font-size: small; margin-top: 0">[단위] : ${wordUnit01 } 건</p>
        </c:otherwise>
    </c:choose>
    <div class="downloadClass">
        <TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
    <TR>
        <TD rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
        <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
        </TD>
        <TD rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:150;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
        <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>사용자</SPAN></P>
        </TD>
        <TD colspan="3"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
        <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 처리량</SPAN></P>
        </TD>
        <TD colspan="3"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
        <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 이용량</SPAN></P>
        </TD>
    </TR>
    <TR>
        <TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${pre }</SPAN></P>
        </TD>
        <TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${cur }</SPAN></P>
        </TD>
        <TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>증감</SPAN></P>
        </TD>
        
        <TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${pre }</SPAN></P>
        </TD>
        <TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${cur }</SPAN></P>
        </TD>
        <TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>증감</SPAN></P>
        </TD>
    </TR>
    
    <c:if test="${fn:length(comparePrevCntByUser) >=11}"><c:set var="size" value="${fn:length(comparePrevCntByUser)}"/></c:if>
    <c:if test="${fn:length(comparePrevCntByUser) <11 && fn:length(comparePrevCntByUser) >1 }"><c:set var="size" value="${fn:length(comparePrevCntByUser)-2}"/></c:if>
    <c:set var="sumType1" value="0"/><c:set var="sumType2" value="0"/><c:set var="sumType3" value="0"/><c:set var="sumType4" value="0"/><c:set var="sumType5" value="0"/><c:set var="sumType6" value="0"/>
    <c:forEach items="${comparePrevCntByUser}" var="i" varStatus="status" begin="0" end="19" step="1">
        <c:if test="${(i.nowLogCnt != 0) or (i.prevLogCnt != 0) }">
        <TR>
            <TD valign="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}</SPAN></P>
            </TD>
            <TD valign="middle" style='width:150;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-family:"굴림"'>${i.emp_user_name }</SPAN></P>
            </TD>
            <TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.prevLogCnt}" pattern="#,###" /></SPAN></P>
            </TD>
            <TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.nowLogCnt}" pattern="#,###" /></SPAN></P>
            </TD>
            <TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <c:if test="${i.nowLogCnt - i.prevLogCnt == 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#000000'>-</SPAN></P>
                </c:if>
                <c:if test="${i.nowLogCnt - i.prevLogCnt > 0 }">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>▲ <fmt:formatNumber value="${i.nowLogCnt - i.prevLogCnt}" pattern="#,###" /></SPAN></P>
                </c:if>
                <c:if test="${i.nowLogCnt - i.prevLogCnt < 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#0000ff'>▼ <fmt:formatNumber value="${-(i.nowLogCnt - i.prevLogCnt)}" pattern="#,###" /></SPAN></P>
                </c:if>
            </TD>
            <c:set var="sumType1" value="${sumType1 + i.nowLogCnt }" />
            <c:set var="sumType2" value="${sumType2 + i.prevLogCnt }" />
            
            
            <TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.prevPrivacyCnt }" pattern="#,###" /></SPAN></P>
            </TD>
            <TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.nowPrivacyCnt}" pattern="#,###" /></SPAN></P>
            </TD>
            <TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <c:if test="${i.nowPrivacyCnt - i.prevPrivacyCnt == 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#000000'>-</SPAN></P>
                </c:if>
                <c:if test="${i.nowPrivacyCnt - i.prevPrivacyCnt > 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>▲ <fmt:formatNumber value="${i.nowPrivacyCnt - i.prevPrivacyCnt}" pattern="#,###" /></SPAN></P>
                </c:if>
                <c:if test="${i.nowPrivacyCnt - i.prevPrivacyCnt < 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#0000ff'>▼ <fmt:formatNumber value="${-(i.nowPrivacyCnt - i.prevPrivacyCnt)}" pattern="#,###" /></SPAN></P>
                </c:if>
            </TD>
            <c:set var="sumType3" value="${sumType3 + i.nowPrivacyCnt }" />
            <c:set var="sumType4" value="${sumType4 + i.prevPrivacyCnt }" />
        </TR>
        </c:if>
    </c:forEach>
    </TABLE>
    </div>
</div>




<!-- 확장자별 -->
<c:choose>
<c:when test="${reportOption[0].input_chart == 'true' }">
    <div style='page-break-before:always'></div>
</c:when>
<c:otherwise>
    <div style='page-break-before:auto'></div>
    <c:if test="${download_type eq 'pdf' }">
	    <P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
		<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
		<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
	</c:if>
</c:otherwise>
</c:choose>

<c:if test="${download_type ne 'pdf' }">
    <div style='page-break-before:always'></div>
</c:if>
<P CLASS=HStyle5 STYLE='margin-left:40.3pt; margin-top: 50px; text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보 접속기록 수행 업무별 처리량 TOP 10 </SPAN></P>

<c:if test="${reportOption[0].input_chart == 'true' }">
<c:choose>
    <c:when test="${download_type eq 'pdf' }">
        <div class='downloadChartClass' id="chart_new4" style="width: 750px; height: 670px;"></div>
        <div align="center"><img name="chart_new4" style="width:750px; height: 500px; display: none;"></div>
    </c:when>
    <c:otherwise>
       <div class='downloadChartClass' id="chart_new4" style="width: 750px; height: 500px;"></div>
        <div align="center"><img name="chart_new4" style="width:750px; height: 500px; display: none;"></div>
    </c:otherwise>
</c:choose>
</c:if>

<div style="margin-left: 27px;">
    <c:choose>
        <c:when test="${download_type eq 'pdf' }">
           <p style="margin-right: 5%; text-align: right; font-size: small; margin-top: 0">[단위] : ${wordUnit01 } 건</p>
        </c:when>
        <c:otherwise>
           <p style="text-align: right; font-size: small; margin-top: 0">[단위] : ${wordUnit01 } 건</p>
        </c:otherwise>
    </c:choose>
    <div class="downloadClass">
    <TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
    <TR>
        <TD rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
        <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
        </TD>
        <TD rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:150;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
        <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>수행업무</SPAN></P>
        </TD>
        <TD colspan="3"align="middle" bgcolor="#e5e5e5"  style='width:270;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
        <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 처리량</SPAN></P>
        </TD>
    </TR>
    <TR>
        <TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${pre }</SPAN></P>
        </TD>
        <TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${cur }</SPAN></P>
        </TD>
        <TD align="middle" bgcolor="#e5e5e5"  style='width:10;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>증감</SPAN></P>
        </TD>
    </TR>
    <c:set var="sumType1" value="0"/><c:set var="sumType2" value="0"/><c:set var="sumType3" value="0"/><c:set var="sumType4" value="0"/><c:set var="sumType5" value="0"/><c:set var="sumType6" value="0"/>
    
    <c:forEach items="${comparePrevCntByReqType}" var="i" varStatus="status" begin="0" end="9">
        <TR>
            <TD valign="middle" bgcolor="#e5e5e5"  style='width:42;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}</SPAN></P>
            </TD>
            <TD valign="middle" style='width:150;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-family:"굴림"'>${i.code_name}</SPAN></P>
            </TD>
            <TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.prevLogCnt}" pattern="#,###" /></SPAN></P>
            </TD>
            <TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
            <P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.nowLogCnt}" pattern="#,###" /></SPAN></P>
            </TD>
            <TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                <c:if test="${i.nowLogCnt- i.prevLogCnt == 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#000000'>-</SPAN></P>
                </c:if>
                <c:if test="${i.nowLogCnt- i.prevLogCnt > 0 }">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>▲ <fmt:formatNumber value="${i.nowLogCnt- i.prevLogCnt}" pattern="#,###" /></SPAN></P>
                </c:if>
                <c:if test="${i.nowLogCnt- i.prevLogCnt < 0}">
                    <P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#0000ff'>▼ <fmt:formatNumber value="${-(i.nowLogCnt- i.prevLogCnt)}" pattern="#,###" /></SPAN></P>
                </c:if>
            </TD>
        </TR>
        <c:set var="sumType1" value="${sumType1 + i.nowLogCnt }" />
        <c:set var="sumType2" value="${sumType2 + i.prevLogCnt }" />
        </c:if>
    </c:forEach>
    </TABLE>
    </div>
</div> --%>


<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>
<div style='page-break-before:auto'></div>
<c:if test="${reportOption[0].input_description == 'true' }">
<div style='page-break-before:auto'></div>
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>2. 총평</SPAN></P>
<div align="left" style="margin-left: 10px; margin-top:10px;">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
    <TD valign="middle" style='width:650;height:195;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;'>
    <c:choose>
        <c:when test="${reportOption[0].description ne '' and reportOption[0].description ne null }">
            <P CLASS=HStyle0 STYLE='margin-left:16.8pt;text-indent:-16.6pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ ${reportOption[0].description}</SPAN></P>
        </c:when>
        <c:otherwise>
            <P CLASS=HStyle0 STYLE='margin-left:16.8pt;text-indent:-16.6pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ ${report_desc_str}</SPAN></P>
        </c:otherwise>
    </c:choose>
    </TD>
</TR>
</TABLE>
</div>
</c:if>
 
 
</div>
</BODY>

</HTML>
