<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<script src="${rootPath}/resources/js/jquery/jquery.min.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/report/report_inspection.js" type="text/javascript" charset="UTF-8"></script>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>

<HEAD>
<STYLE type="text/css">
<!--
p.HStyle0
	{style-name:"바탕글";}
li.HStyle0
	{style-name:"바탕글";}
div.HStyle0
	{style-name:"바탕글";}
p.HStyle1
	{style-name:"본문";}
li.HStyle1
	{style-name:"본문";}
div.HStyle1
	{style-name:"본문";}
p.HStyle2
	{style-name:"개요 1";}
li.HStyle2
	{style-name:"개요 1";}
div.HStyle2
	{style-name:"개요 1";}
p.HStyle3
	{style-name:"개요 2";}
li.HStyle3
	{style-name:"개요 2";}
div.HStyle3
	{style-name:"개요 2";}
p.HStyle4
	{style-name:"개요 3";}
li.HStyle4
	{style-name:"개요 3";}
div.HStyle4
	{style-name:"개요 3";}
p.HStyle5
	{style-name:"개요 4";}
li.HStyle5
	{style-name:"개요 4";}
div.HStyle5
	{style-name:"개요 4";}
p.HStyle6
	{style-name:"개요 5";}
li.HStyle6
	{style-name:"개요 5";}
div.HStyle6
	{style-name:"개요 5";}
p.HStyle7
	{style-name:"개요 6";}
li.HStyle7
	{style-name:"개요 6";}
div.HStyle7
	{style-name:"개요 6";}
p.HStyle8
	{style-name:"개요 7";}
li.HStyle8
	{style-name:"개요 7";}
div.HStyle8
	{style-name:"개요 7";}
p.HStyle9
	{style-name:"쪽 번호";}
li.HStyle9
	{style-name:"쪽 번호";}
div.HStyle9
	{style-name:"쪽 번호";}
p.HStyle10
	{style-name:"머리말";}
li.HStyle10
	{style-name:"머리말";}
div.HStyle10
	{style-name:"머리말";}
p.HStyle11
	{style-name:"각주";}
li.HStyle11
	{style-name:"각주";}
div.HStyle11
	{style-name:"각주";}
p.HStyle12
	{style-name:"미주";}
li.HStyle12
	{style-name:"미주";}
div.HStyle12
	{style-name:"미주";}
p.HStyle13
	{style-name:"메모";}
li.HStyle13
	{style-name:"메모";}
div.HStyle13
	{style-name:"메모";}
p.HStyle14
	{style-name:"MS바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle14
	{style-name:"MS바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle14
	{style-name:"MS바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
-->
.saveInput{
	width: 100%;
    border: none;
    text-align: center;
}
.backslash {
  background: url('${rootPath}/resources/image/backslash.png');
  background-size: 100% 100%;
  background-color: rgb(229, 229, 229);
}
.table_td{
	border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt;
}
.table_span{
	font-size:8.0pt;font-family:"맑은 고딕";line-height:160%;
}
.table_p{
	text-align:center;text-indent:2.9pt;
}
.table_head_span{
	font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%;
}

#proc_month1{
background-color: rgba( 255, 255, 255, 0.0 );
    font-size: 12.0pt;
    font-family: "맑은 고딕";
    font-weight: 900;
    line-height: 160%;
    display: inline;
    width: 25px;
    color: black;
}

#proc_month2{
background-color: rgba( 255, 255, 255, 0.0 );
    font-size: 12.0pt;
    font-family: "맑은 고딕";
    font-weight: 900;
    line-height: 160%;
    display: inline;
    width: 25px;
    color: black;
}

#proc_month3{
	background-color: rgba( 255, 255, 255, 0.0 );
	font-size: 25.0pt;
    font-family: "맑은 고딕";
    font-weight: 900;
    line-height: 160%;
    width: 43px;
    color: black;
    display: inline;
}
</STYLE>
</HEAD>

<script type="text/javascript">
function printpr()
{
	$("#printButton").hide();
	
	window.print();
	self.close();
}
var proc_date = '${param.proc_date}';
var search_fr = '${param.search_fr}';
var search_to = '${param.search_to}';
var rootPath = '${rootPath}';
var report_seq = '${param.report_seq}';
</script>

<BODY style="width: 763px;">
<div id="printButton" align="right">
	<c:choose>
		<c:when test="${empty param.report_seq }">
			<input type=button onclick="savereport();" value="저장"/>
		</c:when>
		<c:otherwise>
			<input type=button onclick="savereport('${param.report_seq }');" value="수정"/>
		</c:otherwise>
	</c:choose>
		<input type=button onclick="printpr();" value="출력" />
	</div>
<P CLASS=HStyle14 STYLE='text-align:right;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle14 STYLE='text-align:right;'>
<TABLE width="100%">
<TR>
	<TD colspan="3" valign="middle" style='height:38;border-left:solid #000000 1.4pt;border-right:solid #000000 1.4pt;border-top:solid #000000 1.4pt;border-bottom:solid #000000 1.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:900;line-height:160%'>UBI SAFER-PSM (<input type="text" id="proc_month1" class="saveInput" disabled="disabled" value="${fn:substring(param.proc_date,4,6)}">)월 정기점검 보고서</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle14 STYLE='text-align:right;'></P>
<%-- <c:set var="today" value="${fn:split(today, '-') }"/> --%>
<P CLASS=HStyle14 STYLE='text-align:right;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>점검일자 :&nbsp;&nbsp; ${fn:substring(today,0,4) }&nbsp;&nbsp; 년&nbsp;&nbsp;&nbsp; ${fn:substring(today,4,6) }&nbsp; 월&nbsp;&nbsp;&nbsp; ${fn:substring(today,6,8) } 일</SPAN></P>
<P CLASS=HStyle14 STYLE='text-align:right;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>점검시간 :&nbsp;&nbsp; <input type="text" class="saveInput" id="inspect_fr" name="inspect_fr" style="width: 67px"> ~ <input type="text" class="saveInput" id="inspect_to" name="inspect_to" style="width: 67px"></SPAN></P>
<P CLASS=HStyle14 STYLE='text-align:right;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>소요시간 :&nbsp;&nbsp; <input type="text" class="saveInput" id="use_time" name="use_time" style="width: 150px"></SPAN></P>
<P CLASS=HStyle14 STYLE='text-align:right;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>㈜이지서티<span style="width: 170px; display: inline-block;"></SPAN></SPAN></P>

<P CLASS=HStyle14 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;'>1. 고객정보</SPAN></P>

<P CLASS=HStyle14 STYLE='text-align:left;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>고객사</SPAN></P>
	</TD>
	<TD valign="middle" style='width:199;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt' colspan="4">
		<input type="text" class="saveInput" id="client" name="client">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:62;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>부서</SPAN></P>
	</TD>
	<TD valign="middle" style='width:149;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<input type="text" class="saveInput" id="dept" name="dept">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:88;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>담당</SPAN></P>
	</TD>
	<TD valign="middle" style='width:141;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<input type="text" class="saveInput" id="manager_name" name="manager_name">
	</TD>
	<TD valign="middle" style='width:141;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt' colspan="2">
		<input type="text" class="saveInput" id="manager_phone" name="manager_phone">
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>유지보수</SPAN></P>
	</TD>
	<TD valign="middle" style='width:52px;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>
		<input type="checkbox" name="check_file" class="saveCheckbox" style="float: none;" id="free" name="free"/>
	</SPAN></P>
	</TD>
	<TD valign="middle" style='width:100px;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>무상</SPAN></P>
	</TD>
	<TD valign="middle" style='width:52px;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>
		<input type="checkbox" name="check_file" class="saveCheckbox" style="float: none;" id="paid" name="paid"/>
	</SPAN></P>
	</TD>
	<TD valign="middle" style='width:100px;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>유상</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:62;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>주기</SPAN></P>
	</TD>
	<TD valign="middle" style='width:149;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<input type="text" class="saveInput" id="cycle" name="cycle">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:88;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>기간</SPAN></P>
	</TD>
	<TD valign="middle" style='width:141;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>${param.search_fr } ~ <br>${param.search_to }</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:88;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>점검형태</SPAN></P>
	</TD>
	<TD valign="middle" style='width:141;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<input type="text" class="saveInput" id="checkform" name="checkform">
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle14 STYLE='text-align:left;'></P>

<P CLASS=HStyle14 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle14 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;'>2. 점검 현황</SPAN></P>

<P CLASS=HStyle14 STYLE='text-align:left;line-height:150%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>&nbsp;</SPAN></P>
	</TD>
	<TD colspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:291;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>Software 수량</SPAN></P>
	</TD>
	<TD colspan="3" valign="middle" bgcolor="#e5e5e5"  style='width:321;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>Software 버전</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>Agent</SPAN></P>
	</TD>
	<TD valign="middle" style='width:146;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>라이센스 :	<input type="text" class="saveInput" style="width: 20%" id="agentlicense" name="agentlicense"> 개 </SPAN></P>
	</TD>
	<TD valign="middle" style='width:146;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>사용권 :<input type="text" class="saveInput" style="width: 20%" id="agentuse" name="agentuse">  개 </SPAN></P>
	</TD>
	<TD colspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:104;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>PSM 버전 정보</SPAN></P>
	</TD>
	<TD valign="middle" style='width:217;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>UBI-SAFER PSM v3.0</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>Manager</SPAN></P>
	</TD>
	<TD valign="middle" style='width:146;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>라이센스 :<input type="text" class="saveInput" style="width: 20%" id="managerlicense" name="managerlicense">  개 </SPAN></P>
	</TD>
	<TD valign="middle" style='width:146;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>사용권 :<input type="text" class="saveInput" style="width: 20%" id="manageruse" name="manageruse">  개 </SPAN></P>
	</TD>
	<TD colspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:104;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>Engine ver.</SPAN></P>
	</TD>
	<TD valign="middle" style='width:217;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<input type="text" class="saveInput" id="engineversion" name="engineversion">
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>Master</SPAN></P>
	</TD>
	<TD valign="middle" style='width:146;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>라이센스 :<input type="text" class="saveInput" style="width: 20%" id="masterlicense" name="masterlicense">  개 </SPAN></P>
	</TD>
	<TD valign="middle" style='width:146;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>사용권 :<input type="text" class="saveInput" style="width: 20%" id="masteruse" name="masteruse">  개 </SPAN></P>
	</TD>
	<TD colspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:104;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>UI ver.</SPAN></P>
	</TD>
	<TD valign="middle" style='width:217;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<input type="text" class="saveInput" id="uiversion" name="uiversion">
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle14 STYLE='text-align:left;line-height:150%;'></P>

<P CLASS=HStyle14 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle14 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;'>3. 상세 점검</SPAN></P>

<c:set var="list_cnt" value="${fn:length(list)+1 }" />
<P CLASS=HStyle14 STYLE='text-align:left;line-height:110%;'>

<div id="log_table" class="saveTable">
	<TABLE width="100%" border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
		<TR>
			<TD rowspan="${list_cnt }" valign="middle" bgcolor="#e5e5e5"  style='width:71;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN class='table_head_span'>Agent</SPAN></P>
			</TD>
			<TD valign="middle" class="table_td" bgcolor="#e5e5e5"  style='width:40;height:29;'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN class='table_head_span'>연번</SPAN></P>
			</TD>
			<TD valign="middle" class="table_td" bgcolor="#e5e5e5"  style='width:194;'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN class='table_head_span'>시스템 명</SPAN></P>
			</TD>
			<TD valign="middle" class="table_td" bgcolor="#e5e5e5"  style='width:90;'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN class='table_head_span' STYLE='letter-spacing:-2%;'>정상 동작여부</SPAN></P>
			</TD>
			<TD valign="middle" class="table_td" bgcolor="#e5e5e5"  style='width:102;'>
			<P CLASS=HStyle0 STYLE='text-align:center;line-height:110%;'><SPAN class='table_head_span' STYLE='letter-spacing:-4%;'>처리량 (금월)<br/>(${fn:substring(frDate,4,6)}.${fn:substring(frDate,6,8)}~${fn:substring(toDate,4,6)}.${fn:substring(toDate,6,8)})</SPAN></P>
			</TD>
			<TD valign="middle" class="table_td" bgcolor="#e5e5e5"  style='width:102;'>
			<P CLASS=HStyle0 STYLE='text-align:center;line-height:110%;'><SPAN class='table_head_span' STYLE='letter-spacing:-4%;'>처리량 (전월)<br/>(${fn:substring(frDate2,4,6)}.${fn:substring(frDate2,6,8)}~${fn:substring(toDate2,4,6)}.${fn:substring(toDate2,6,8)})</SPAN></P>
			</TD>
			<TD valign="middle" class="table_td" bgcolor="#e5e5e5"  style='width:113;'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN class='table_head_span'>보고서 생성 여부</SPAN></P>
			</TD>
		</TR>
		<c:forEach items="${list }" var="item" varStatus="status">
		<TR>
			<TD class="table_td" valign="middle" style='width:40;'>
			<P CLASS='HStyle0 table_p'><SPAN class="table_span">${status.count }</SPAN></P>
			</TD>
			<TD class="table_td" valign="middle" style='width:194;'>
			<P CLASS='HStyle0 table_p'><SPAN class="table_span">${item.system_name }</SPAN></P>
			</TD>
			<TD class="table_td" valign="middle" style='width:90;'>
				<input type="text" class="saveInput" id="system_abnormal_${status.count }" name="system_abnormal_${status.count }">
			</TD>
			<TD class="table_td" valign="middle" style='width:102;'>
			<P CLASS='HStyle0 table_p'><SPAN class="table_span"><fmt:formatNumber value="${item.type1 }" type="number" /></SPAN></P>
			</TD>
			<TD class="table_td" valign="middle" style='width:102;'>
			<P CLASS='HStyle0 table_p'><SPAN class="table_span"><fmt:formatNumber value="${item.type2 }" type="number" /></SPAN></P>
			</TD>
			<TD class="table_td" valign="middle" style='width:113;'>
				<input type="text" class="saveInput" id="report_abnormal_${status.count }" name="report_abnormal_${status.count }">
			</TD>
		</TR>
		</c:forEach>
	</TABLE>
</div>


<P CLASS=HStyle14 STYLE='text-align:left;line-height:110%;'></P>

<P CLASS=HStyle14 STYLE='text-align:left;line-height:110%;margin-top:5px;'>
<TABLE width="100%" border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD rowspan="3" valign="middle" bgcolor="#e5e5e5"  style='width:70;height:59;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>Manager</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:357;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>점검항목</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:70;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>상태</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:214;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>특이사항</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:357;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:left;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>UI 정상 작동 여부 확인</SPAN></P>
	</TD>
	<TD valign="middle" style='width:70;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
		<input type="text" class="saveInput" id="checkstatus1" name="checkstatus1">
	</TD>
	<TD valign="middle" style='width:214;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
		<input type="text" class="saveInput" id="checkoption1" name="checkoption1">
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:357;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:left;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>백업파일 통계 생성 확인</SPAN></P>
	</TD>
	<TD valign="middle" style='width:70;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
		<input type="text" class="saveInput" id="checkstatus2" name="checkstatus2">
	</TD>
	<TD valign="middle" style='width:214;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
		<input type="text" class="saveInput" id="checkoption2" name="checkoption2">
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle14 STYLE='text-align:left;line-height:110%;'></P>

<P CLASS=HStyle14 STYLE='text-align:left;line-height:110%;margin-top:5px;'>
<TABLE width="100%" border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD rowspan="6" valign="middle" bgcolor="#e5e5e5"  style='width:70;height:117;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>Master</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:357;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>점검항목</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:70;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>상태</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:214;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>특이사항</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:357;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:left;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>모니터링 각 항목 정상 출력</SPAN></P>
	</TD>
	<TD valign="middle" style='width:70;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
		<input type="text" class="saveInput" id="checkstatus3" name="checkstatus3">
	</TD>
	<TD valign="middle" style='width:214;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
		<input type="text" class="saveInput" id="checkoption3" name="checkoption3">
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:357;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:left;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>백업이력조회 정상 동작(점검일 이전 일자까지 생성 확인)</SPAN></P>
	</TD>
	<TD valign="middle" style='width:70;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
		<input type="text" class="saveInput" id="checkstatus4" name="checkstatus4">
	</TD>
	<TD valign="middle" style='width:214;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
		<input type="text" class="saveInput" id="checkoption4" name="checkoption4">
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:357;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:left;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>통계 데이터 정상 출력(점검일 이전 일자까지 생성 확인)</SPAN></P>
	</TD>
	<TD valign="middle" style='width:70;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
		<input type="text" class="saveInput" id="checkstatus5" name="checkstatus5">
	</TD>
	<TD valign="middle" style='width:214;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
		<input type="text" class="saveInput" id="checkoption5" name="checkoption5">
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:357;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:left;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>접속기록보고서 정상 생성</SPAN></P>
	</TD>
	<TD valign="middle" style='width:70;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
		<input type="text" class="saveInput" id="checkstatus6" name="checkstatus6">
	</TD>
	<TD valign="middle" style='width:214;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
		<input type="text" class="saveInput" id="checkoption6" name="checkoption6">
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:357;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:left;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>비정상위험분석 로그 생성(전월 기준 생성 로그 건수 확인)</SPAN></P>
	</TD>
	<TD valign="middle" style='width:70;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
		<input type="text" class="saveInput" id="checkstatus7" name="checkstatus7">
	</TD>
	<TD valign="middle" style='width:214;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
		<input type="text" class="saveInput" id="checkoption7" name="checkoption7">
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle14 STYLE='text-align:left;line-height:110%;'></P>

<P CLASS=HStyle14 STYLE='text-align:left;line-height:110%;margin-top:5px;'>
<TABLE width="100%" border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD rowspan="4" valign="middle" bgcolor="#e5e5e5"  style='width:70;height:78;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>기타</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:357;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>점검항목</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:70;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>상태</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:214;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>특이사항</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:357;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:left;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>인사연동 정상동작</SPAN></P>
	</TD>
	<TD valign="middle" style='width:70;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
		<input type="text" class="saveInput" id="checkstatus8" name="checkstatus8">
	</TD>
	<TD valign="middle" style='width:214;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
		<input type="text" class="saveInput" id="checkoption8" name="checkoption8">
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:357;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:left;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>예외처리 DB 등록 여부(등록 건수 명시)</SPAN></P>
	</TD>
	<TD valign="middle" style='width:70;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
		<input type="text" class="saveInput" id="checkstatus9" name="checkstatus9">
	</TD>
	<TD valign="middle" style='width:214;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
		<input type="text" class="saveInput" id="checkoption9" name="checkoption9">
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:357;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:left;text-indent:2.9pt;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>관리자 감사 이력 (관리자페이지 최종 접속일)</SPAN></P>
	</TD>
	<TD valign="middle" style='width:70;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
		<input type="text" class="saveInput" id="checkstatus10" name="checkstatus10">
	</TD>
	<TD valign="middle" style='width:214;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
		<input type="text" class="saveInput" id="checkoption10" name="checkoption10">
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle14 STYLE='text-align:left;line-height:150%;'></P>
<P CLASS=HStyle14 STYLE='text-align:left; margin-bottom: 200px'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'><BR></SPAN></P>

<!-- HW 정기점검 보고서 -->

<P CLASS=HStyle14 STYLE='text-align:right;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle14 STYLE='text-align:right;'>
<TABLE width="100%">
<TR>
	<TD colspan="3" valign="middle" style='height:38;border-left:solid #000000 1.4pt;border-right:solid #000000 1.4pt;border-top:solid #000000 1.4pt;border-bottom:solid #000000 1.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:900;line-height:160%'>UBI SAFER-PSM (<input type="text" id="proc_month2" class="saveInput" disabled="disabled" value="${fn:substring(param.proc_date,4,6)}">)월 정기점검 보고서</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle14 STYLE='text-align:right;'></P>
<%-- <c:set var="today" value="${fn:split(today, '-') }"/> --%>
<P CLASS=HStyle14 STYLE='text-align:right;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>점검일자 :&nbsp;&nbsp; ${fn:substring(today,0,4) }&nbsp;&nbsp; 년&nbsp;&nbsp;&nbsp; ${fn:substring(today,4,6) }&nbsp; 월&nbsp;&nbsp;&nbsp; ${fn:substring(today,6,8) } 일</SPAN></P>

<P CLASS=HStyle14 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;'>1. 리소스 점검</SPAN></P>

<P CLASS=HStyle14 STYLE='text-align:left;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD class="backslash" valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 2.1pt 1.4pt 2.1pt' rowspan="2">
	<P CLASS=HStyle0 STYLE='text-align:right; width: 100%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>Manager</SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:left;width: 100%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>구분</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt' colspan="8">
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>사용량</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:120px;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt' rowspan="2">
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>파티션</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:50;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>CPU<br>(%)</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;' colspan="3">
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>Memory<br>(total / used / free+buffers+cached)</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt' colspan="4">
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>Disk (/)<br>(Size / Used / Avail / Use%)</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;' rowspan="9">
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>1</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:50;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;background: none' rowspan="9">
		<table style="border-collapse: collapse; border: none; height: 100%;">
			<tr>
				<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
					<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'><input type="text" class="saveInput" id="CPUinfo_us_1" name="CPUinfo_us_1" style="width: 60%;">%<br>us</SPAN></P>
				</TD>
			</tr>
			<tr>
				<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
					<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'><input type="text" class="saveInput" id="CPUinfo_sy_1" name="CPUinfo_sy_1" style="width: 60%;">%<br>sy</SPAN></P>
				</TD>
			</tr>
		</table>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none' rowspan="9">
		<input type="text" class="saveInput" id="Memory_total_1_1" name="Memory_total_1_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none' rowspan="9">
		<input type="text" class="saveInput" id="Memory_used_1_1" name="Memory_used_1_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none' rowspan="9">
		<input type="text" class="saveInput" id="Memory_free_1_1" name="Memory_free_1_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_size_1_1" name="Disk_size_1_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_used_1_1" name="Disk_used_1_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_avail_1_1" name="Disk_avail_1_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_usePercent_1_1" name="Disk_usePercent_1_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>/</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_size_2_1" name="Disk_size_2_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_used_2_1" name="Disk_used_2_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_avail_2_1" name="Disk_avail_2_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_usePercent_2_1" name="Disk_usePercent_2_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
	<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>/dev</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_size_3_1" name="Disk_size_3_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_used_3_1" name="Disk_used_3_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_avail_3_1" name="Disk_avail_3_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_usePercent_3_1" name="Disk_usePercent_3_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
	<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>/dev/shm</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_size_4_1" name="Disk_size_4_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_used_4_1" name="Disk_used_4_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_avail_4_1" name="Disk_avail_4_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_usePercent_4_1" name="Disk_usePercent_4_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
	<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>/run</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_size_5_1" name="Disk_size_5_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_used_5_1" name="Disk_used_5_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_avail_5_1" name="Disk_avail_5_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_usePercent_5_1" name="Disk_usePercent_5_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
	<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>/sys/fs/cgroup</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_size_6_1" name="Disk_size_6_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_used_6_1" name="Disk_used_6_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_avail_6_1" name="Disk_avail_6_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_usePercent_6_1" name="Disk_usePercent_6_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
	<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>/var/lib/pgsql</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_size_7_1" name="Disk_size_7_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_used_7_1" name="Disk_used_7_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_avail_7_1" name="Disk_avail_7_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_usePercent_7_1" name="Disk_usePercent_7_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
	<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>/home</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_size_8_1" name="Disk_size_8_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_used_8_1" name="Disk_used_8_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_avail_8_1" name="Disk_avail_8_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_usePercent_8_1" name="Disk_usePercent_8_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
	<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>/boot</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_size_9_1" name="Disk_size_9_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_used_9_1" name="Disk_used_9_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_avail_9_1" name="Disk_avail_9_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_usePercent_9_1" name="Disk_usePercent_9_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_partition_9_1" name="Disk_partition_9_1" style="text-align: left">
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>Total</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'></SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'></SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'></SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'></SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_size_10_1" name="Disk_size_10_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_used_10_1" name="Disk_used_10_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_avail_10_1" name="Disk_avail_10_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_usePercent_10_1" name="Disk_usePercent_10_1">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_partition_10_1" name="Disk_partition_10_1" style="text-align: left">
	</TD>
</TR>
</TABLE>

<P CLASS=HStyle14 STYLE='text-align:left;line-height:110%;'>&nbsp;</P>

<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD class="backslash" valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 2.1pt 1.4pt 2.1pt' rowspan="2">
	<P CLASS=HStyle0 STYLE='text-align:right; width: 100%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>Master</SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:left;width: 100%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>구분</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt' colspan="8">
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>사용량</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:120px;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt' rowspan="2">
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>파티션</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:50;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>CPU<br>(%)</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;' colspan="3">
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>Memory<br>(total / used / free+buffers+cached)</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt' colspan="4">
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>Disk (/)<br>(Size / Used / Avail / Use%)</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;' rowspan="9">
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>1</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:50;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;background: none' rowspan="9">
		<table style="border-collapse: collapse; border: none; height: 100%;">
			<tr>
				<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
					<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'><input type="text" class="saveInput" id="CPUinfo_us_2" name="CPUinfo_us_2" style="width: 60%;">%<br>us</SPAN></P>
				</TD>
			</tr>
			<tr>
				<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
					<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'><input type="text" class="saveInput" id="CPUinfo_sy_2" name="CPUinfo_sy_2" style="width: 60%;">%<br>sy</SPAN></P>
				</TD>
			</tr>
		</table>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none' rowspan="9">
		<input type="text" class="saveInput" id="Memory_total_1_2" name="Memory_total_1_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none' rowspan="9">
		<input type="text" class="saveInput" id="Memory_used_1_2" name="Memory_used_1_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none' rowspan="9">
		<input type="text" class="saveInput" id="Memory_free_1_2" name="Memory_free_1_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_size_1_2" name="Disk_size_1_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_used_1_2" name="Disk_used_1_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_avail_1_2" name="Disk_avail_1_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_usePercent_1_2" name="Disk_usePercent_1_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>/</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_size_2_2" name="Disk_size_2_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_used_2_2" name="Disk_used_2_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_avail_2_2" name="Disk_avail_2_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_usePercent_2_2" name="Disk_usePercent_2_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
	<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>/dev</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_size_3_2" name="Disk_size_3_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_used_3_2" name="Disk_used_3_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_avail_3_2" name="Disk_avail_3_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_usePercent_3_2" name="Disk_usePercent_3_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
	<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>/dev/shm</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_size_4_2" name="Disk_size_4_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_used_4_2" name="Disk_used_4_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_avail_4_2" name="Disk_avail_4_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_usePercent_4_2" name="Disk_usePercent_4_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
	<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>/run</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_size_5_2" name="Disk_size_5_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_used_5_2" name="Disk_used_5_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_avail_5_2" name="Disk_avail_5_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_usePercent_5_2" name="Disk_usePercent_5_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
	<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>/sys/fs/cgroup</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_size_6_2" name="Disk_size_6_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_used_6_2" name="Disk_used_6_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_avail_6_2" name="Disk_avail_6_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_usePercent_6_2" name="Disk_usePercent_6_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
	<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>/var/lib/pgsql</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_size_7_2" name="Disk_size_7_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_used_7_2" name="Disk_used_7_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_avail_7_2" name="Disk_avail_7_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_usePercent_7_2" name="Disk_usePercent_7_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
	<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>/home</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_size_8_2" name="Disk_size_8_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_used_8_2" name="Disk_used_8_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_avail_8_2" name="Disk_avail_8_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_usePercent_8_2" name="Disk_usePercent_8_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
	<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>/boot</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_size_9_2" name="Disk_size_9_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_used_9_2" name="Disk_used_9_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_avail_9_2" name="Disk_avail_9_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_usePercent_9_2" name="Disk_usePercent_9_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_partition_9_2" name="Disk_partition_9_2" style="text-align: left">
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>Total</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'></SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'></SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'></SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'></SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_size_10_2" name="Disk_size_10_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_used_10_2" name="Disk_used_10_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_avail_10_2" name="Disk_avail_10_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_usePercent_10_2" name="Disk_usePercent_10_2">
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none'>
		<input type="text" class="saveInput" id="Disk_partition_10_2" name="Disk_partition_10_2" style="text-align: left">
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle14 STYLE='text-align:left;'></P>

<P CLASS=HStyle14 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle14 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;'>3. DB 점검</SPAN></P>

<P CLASS=HStyle14 STYLE='text-align:left;line-height:150%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width: 100%'>
	<TR>
		<TD class="backslash" valign="middle" bgcolor="#e5e5e5"  style='width:20;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 2.1pt 1.4pt 2.1pt' rowspan="2">
			<P CLASS=HStyle0 STYLE='text-align:right; width: 100%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>DB</SPAN></P>
			<P CLASS=HStyle0 STYLE='text-align:left;width: 100%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>구분</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt' colspan="3">
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>DB 사용량</SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>DB명</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>사용률 / 사용량 / 가용 용량</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>비고</SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:20;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>1</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt; background: none'>
			<input type="text" class="saveInput" id="DBname" name="DBname">
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt; background: none'>
			<P CLASS=HStyle0 STYLE='text-align:center;'>
				<SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>
					<input type="text" class="saveInput" id="DBusepercent" name="DBusepercent" style="width: 20%">% /
					<input type="text" class="saveInput" id="DBused" name="DBused" style="width: 20%">/
					<input type="text" class="saveInput" id="DBempty" name="DBempty" style="width: 20%">
				</SPAN>
			</P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt; background: none'>
			<input type="text" class="saveInput" id="DBremake" name="DBremake">
		</TD>
	</TR>
</TABLE>
<P CLASS=HStyle14 STYLE='text-align:left;line-height:150%;'></P>

<P CLASS=HStyle14 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'><BR></SPAN></P>


<P CLASS=HStyle14 STYLE='text-align:left;line-height:110%;'></P>

<P CLASS=HStyle14 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;'>4. 점검확인</SPAN></P>

<P CLASS=HStyle14 STYLE='text-align:left;line-height:90%;'>
<TABLE width="100%" border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD colspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:364;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>점검자 의견</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:349;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>고객 의견 </SPAN></P>
	</TD>
</TR>
<TR>
	<TD colspan="2" valign="middle" style='width:364;height:59;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
		<textarea class="saveTextarea" id="inspector_Opinion" style="width: 100%; height: 100%; border: none; resize: none"></textarea>
	</TD>
	<TD valign="middle" style='width:349;height:59;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
		<textarea class="saveTextarea" id="client_Opinion" style="width: 100%; height: 100%; border: none; resize: none"></textarea>
	</TD>
</TR>
<TR>
	<TD rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:112;height:71;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕"'>정기점검</SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕"'>만족도 조사</SPAN></P>
	</TD>
	<TD colspan="2" valign="middle" style='width:594;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:8.6pt;font-family:"맑은 고딕";line-height:160%'>(주)이지서티 담당 엔지니어의 점검에 만족하십니까? &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; □ 만족&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; □ 불만족</SPAN></P>
	</TD>
</TR>
<TR>
	<TD colspan="2" valign="top" style='width:594;height:43;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:8.6pt;font-family:"맑은 고딕";line-height:160%'>불만족하신다면 그 이유는 무엇입니까?</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle14 STYLE='text-align:left;line-height:90%;'></P>

<P CLASS=HStyle14 STYLE='text-align:left;line-height:140%;margin-top:5px;'>
<TABLE width="100%" border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD colspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:240;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>고객사 담당자</SPAN></P>
	</TD>
	<TD colspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:236;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>유지보수 업체</SPAN></P>
	</TD>
	<TD colspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:236;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>점검자</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:118;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>부&nbsp;&nbsp; 서</SPAN></P>
	</TD>
	<TD valign="middle" style='width:122;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";line-height:160%'>&nbsp;</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:118;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>업&nbsp;&nbsp; 체</SPAN></P>
	</TD>
	<TD valign="middle" style='width:118;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";line-height:160%'>&nbsp;</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:114;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>업&nbsp;&nbsp; 체</SPAN></P>
	</TD>
	<TD valign="middle" style='width:122;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";line-height:160%'>(주)이지서티</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:118;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>성&nbsp;&nbsp; 명 </SPAN></P>
	</TD>
	<TD valign="middle" style='width:122;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";line-height:160%'>&nbsp;(인)</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:118;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>성&nbsp;&nbsp; 명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:118;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";line-height:160%'>(인)</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:114;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>성&nbsp;&nbsp; 명</SPAN></P>
	</TD>
	<TD valign="middle" style='width:122;height:22;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";line-height:160%'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(인)</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle14 STYLE='text-align:right;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle14 STYLE='text-align:right;'>
<TABLE width="100%">
<TR>
	<TD colspan="3" valign="middle" style='height:38;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:25.0pt;font-family:"맑은 고딕";font-weight:900;line-height:160%'><input type="text" id="proc_month3" class="saveInput" disabled="disabled" value="${fn:substring(param.proc_date,4,6)}">월 정기점검 보고서</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle14 STYLE='text-align:right;'></P>

<P CLASS=HStyle14 STYLE='text-align:left;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width: 100%'>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:40;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 2.1pt 1.4pt 2.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center; width: 100%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>구분</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:85%;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px; background: none' colspan="2">
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%;display: flex'>
			<input type="checkbox" class="saveCheckbox" style="float: none;" id="server" name="server"/>서버
			<input type="checkbox" class="saveCheckbox" style="float: none;" id="backup" name="backup"/>저장/백업
			<input type="checkbox" class="saveCheckbox" style="float: none;" id="security" name="security"/>보안
			<input type="checkbox" class="saveCheckbox" style="float: none;" id="network" name="network"/>네트워크
			<input type="checkbox" class="saveCheckbox" style="float: none;" id="software" name="software"/>S/W
			<input type="checkbox" class="saveCheckbox" style="float: none;" id="etc" name="etc"/>기타
			(<input type="text" class="saveInput" id="inspection_date" name="inspection_date" style="width: 20%">)
		</SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:40;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 2.1pt 1.4pt 2.1pt' rowspan="7">
		<P CLASS=HStyle0 STYLE='text-align:center; width: 100%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>제품정보</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>제품명</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:70%;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px; background: none'>
			<input type="text" class="saveInput" id="solution_name" name="solution_name" style="text-align: left">
		</TD>
	</TR>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>업무명</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:120px;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px; background: none'>
			<input type="text" class="saveInput" id="project_name" name="project_name" style="text-align: left">
		</TD>
	</TR>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>수량</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:120px;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px; background: none'>
			<input type="text" class="saveInput" id="solution_count" name="solution_count" style="text-align: left">
		</TD>
	</TR>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>지원형태</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:120px;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px; background: none'>
			<P CLASS=HStyle0 STYLE='text-align:left;'>
				<SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>
					<input type="checkbox" class="saveCheckbox" style="float: none;" id="visit" name="visit"/>방문
					<input type="checkbox" class="saveCheckbox" style="float: none;" id="remote" name="remote"/>원격
					<input type="checkbox" class="saveCheckbox" style="float: none;" id="suport_etc" name="suport_etc"/>기타
					(<input type="text" class="saveInput" id="inspection_date" name="inspection_date" style="width: 30%">)
				</SPAN>
			</P>
		</TD>
	</TR>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>점검주기</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:120px;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px; background: none'>
			<P CLASS=HStyle0 STYLE='text-align:left;'>
				<SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>
					<input type="checkbox" class="saveCheckbox" style="float: none;" id="monthly" name="monthly"/>월간
					<input type="checkbox" class="saveCheckbox" style="float: none;" id="quarter" name="quarter"/>분기
					<input type="checkbox" class="saveCheckbox" style="float: none;" id="half" name="half"/>반기
					<input type="checkbox" class="saveCheckbox" style="float: none;" id="inspect_cyle_etc" name="inspect_cyle_etc"/>기타
					(<input type="text" class="saveInput" id="inspection_date" name="inspection_date" style="width: 30%">)
				</SPAN>
			</P>
		</TD>
	</TR>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>점검장소</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:120px;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px; background: none'>
			<P CLASS=HStyle0 STYLE='text-align:left;'>
				<SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>
					<input type="checkbox" class="saveCheckbox" style="float: none;" id="monthly" name="monthly"/>월간
					<input type="checkbox" class="saveCheckbox" style="float: none;" id="quarter" name="quarter"/>분기
					<input type="checkbox" class="saveCheckbox" style="float: none;" id="half" name="half"/>반기
					<input type="checkbox" class="saveCheckbox" style="float: none;" id="inspect_cyle_etc" name="inspect_cyle_etc"/>기타
					(<input type="text" class="saveInput" id="inspection_date" name="inspection_date" style="width: 30%">)
				</SPAN>
			</P>
		</TD>
	</TR>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>점검일시</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:120px;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px; background: none'>
			<input type="text" class="saveInput" id="inspection_date" name="inspection_date">
		</TD>
	</TR>
</TABLE>

<P CLASS=HStyle14 STYLE='text-align:left;line-height:110%;'>&nbsp;</P>

<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; width: 100%'>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:50;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px' colspan="2">
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>점검결과</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:50;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>특이사항<br>/<br>조치내역</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:380px;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt; background: none' colspan="3">
		<textarea class="saveTextarea" id="inspect_comment" name="inspect_comment" rows="" cols="" style="width: 100%; height: 100%; resize: none;border: none"></textarea>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:50;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>고객의견</P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:75px;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt; background: none' colspan="3">
		<textarea class="saveTextarea" id="client_comment" name="client_comment" rows="" cols="" style="width: 100%; height: 100%; resize: none;border: none"></textarea>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle14 STYLE='text-align:left;'></P>

<P CLASS=HStyle14 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle14 STYLE='text-align:left;line-height:150%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width: 100%'>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:20;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 2.1pt 1.4pt 2.1pt' rowspan="4">
			<P CLASS=HStyle0 STYLE='text-align:center; width: 100%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>확<br>인<br>란</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>구분</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>소속</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>연락처</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>성명</SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>점검자</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none;padding:10px'>
			<input type="text" class="saveInput" id="inspector_belong" name="inspector_belong" style="text-align: center">
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none;padding:10px'>
			<input type="text" class="saveInput" id="inspector_phone" name="inspector_phone" style="text-align: center">
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none;padding:10px'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'><input type="text" class="saveInput" id="inspector_name" name="inspector_name" style="text-align: center; width:80%">(인)</SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;padding:10px'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>유지보수</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none;padding:10px'>
			<input type="text" class="saveInput" id="repair_belong" name="repair_belong" style="text-align: center">
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none;padding:10px'>
			<input type="text" class="saveInput" id="repair_phone" name="repair_phone" style="text-align: center">
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none;padding:10px'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'><input type="text" class="saveInput" id="repair_name" name="repair_name" style="text-align: center; width:80%">(인)</SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;padding:10px'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>담당자</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none;padding:10px'>
			<input type="text" class="saveInput" id="client_belong" name="client_belong" style="text-align: center">
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none;padding:10px'>
			<input type="text" class="saveInput" id="client_phone" name="client_phone" style="text-align: center">
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none;padding:10px'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'><input type="text" class="saveInput" id="client_name" name="client_name" style="text-align: center; width:80%">(인)</SPAN></P>
		</TD>
	</TR>
</TABLE>
<P CLASS=HStyle14 STYLE='text-align:left;line-height:150%;'></P>
<P CLASS=HStyle14 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'><BR></SPAN></P>
<P CLASS=HStyle14 STYLE='text-align:left;line-height:110%;'></P>
<P CLASS=HStyle14 STYLE='text-align:left;line-height:140%;margin-top:5px;'>
<TABLE width="100%" style="margin-top:50px">
<TR>
	<TD colspan="3" valign="middle" style='height:38;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:25.0pt;font-family:"맑은 고딕";font-weight:900;line-height:160%'>점검대상 리스트</SPAN></P>
	</TD>
</TR>
</TABLE>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width: 100%'>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px;width: 45px;'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>순번</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width: 15%;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%;'>제조사</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>장비명</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>업무명</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width: 10%;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%;'>수량</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width: 10%;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%;'>비고</SPAN></P>
		</TD>
	</TR>
	<c:forEach begin="1" end="12" step="1" var="count">
		<TR>
			<TD valign="middle" bgcolor="#e5e5e5"  style='height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>${count }</SPAN></P>
			</TD>
			<TD valign="middle" bgcolor="#e5e5e5"  style='height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px; background: none'>
				<input type="text" class="saveInput" id="company_${count }" name="company_${count }" style="text-align: center">
			</TD>
			<TD valign="middle" bgcolor="#e5e5e5"  style='height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px; background: none'>
				<input type="text" class="saveInput" id="solution_name_${count }" name="solution_name_${count }" style="text-align: center">
			</TD>
			<TD valign="middle" bgcolor="#e5e5e5"  style='height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px; background: none'>
				<input type="text" class="saveInput" id="project_name_${count }" name="project_name_${count }" style="text-align: center">
			</TD>
			<TD valign="middle" bgcolor="#e5e5e5"  style='height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px; background: none'>
				<input type="text" class="saveInput" id="count_${count }" name="count_${count }" style="text-align: center">
			</TD>
			<TD valign="middle" bgcolor="#e5e5e5"  style='height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px; background: none'>
				<input type="text" class="saveInput" id="comment_${count }" name="comment_${count }" style="text-align: center">
			</TD>
		</TR>
	</c:forEach>
	
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px;' colspan="4">
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>총 수량</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px; background: none'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'></SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px; background: none'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'></SPAN></P>
		</TD>
	</TR>
</TABLE>
</BODY>
</HTML>
