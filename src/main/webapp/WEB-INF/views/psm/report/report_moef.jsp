<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script src="${rootPath}/resources/js/jquery/jquery.min.js" type="text/javascript" charset="UTF-8"></script>

<link rel="stylesheet" href="${rootPath}/resources/css/export.css" type="text/css" media="all">

<script>
rootPath = '${rootPath}';
var start_date = '${start_date}';
var end_date = '${end_date}';
var period_type = '${period_type}';
var system_seq = '${system_seq}';


function printpr()
{
	$("#printButton").hide();

	window.print();
	self.close();
}
</script>

<link rel="stylesheet" href="${rootPath}/resources/css/export.css" type="text/css" media="all">

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>

<HEAD>
<META NAME="Generator" CONTENT="Hancom HWP 8.5.8.1555">
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">
<TITLE>접근행위별 접속기록로그 TOP10</TITLE>
<STYLE type="text/css">
<!--
p.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"bold"; font-style:"normal"; color:#000000;}
li.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"bold"; font-style:"normal"; color:#000000;}
div.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"bold"; font-style:"normal"; color:#000000;}
p.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
-->
</STYLE>
</HEAD>


<c:set var="startdate" value="${fn:split(start_date, '-') }"/>
<c:set var="enddate" value="${fn:split(end_date, '-') }"/>
<%-- <c:set var="date" value="${fn:split(date, '-') }"/> --%>
<c:choose>
	<c:when test="${period_type == 1 }">
		<c:set var="cur" value="${startdate[1]-0 } 월"/>
		<c:set var="pre" value="이전월"/>
	</c:when>
	<c:when test="${period_type == 2 }">
		<c:choose>
		<c:when test="${startdate[1] == '01' }">
		<c:set var="cur" value="1분기"/>
		</c:when>
		<c:when test="${startdate[1] == '04' }">
		<c:set var="cur" value="2분기"/>
		</c:when>
		<c:when test="${startdate[1] == '07' }">
		<c:set var="cur" value="3분기"/>
		</c:when>
		<c:when test="${startdate[1] == '10' }">
		<c:set var="cur" value="4분기"/>
		</c:when>
		</c:choose>
		<c:set var="pre" value="이전 분기"/>
	</c:when>
	<c:when test="${period_type == 3 }">
		<c:choose>
		<c:when test="${startdate[1] == '01' }">
		<c:set var="cur" value="상반기"/>
		</c:when>
		<c:when test="${startdate[1] == '07' }">
		<c:set var="cur" value="하반기"/>
		</c:when>
		</c:choose>
		<c:set var="pre" value="이전 반기"/>
	</c:when>
	<c:when test="${period_type == 4 }">
		<c:set var="cur" value="${startdate[0]-0 } 년"/>
		<c:set var="pre" value="이전년"/>
	</c:when>
	<c:when test="${period_type == 5 }">
		<c:set var="cur" value="기간"/>
		<c:set var="pre" value="이전 기간"/>
	</c:when>
</c:choose>

<div align="right">
	<input type=button id="printButton" name="printButton" onclick="printpr();" value="출력"/>
	<!-- <input type=button id="saveButton" name="saveButton" onclick="saveReport();" value="저장"/> -->
</div>

<BODY>
<div align="center">
<P CLASS=HStyle0><BR></P>

<P CLASS=HStyle0><BR></P>

<P CLASS=HStyle0></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#d8d8d8"  style='width:676;height:108;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:30.0pt;font-weight:"bold";line-height:160%'>접근행위별 접속기록로그 TOP10</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle0></P>

<P CLASS=HStyle0><BR></P>

<P CLASS=HStyle0><BR></P>

<P CLASS=HStyle0><BR></P>

<P CLASS=HStyle0><BR></P>

<P CLASS=HStyle0><BR></P>

<P CLASS=HStyle0><BR></P>

<P CLASS=HStyle0><BR></P>

<P CLASS=HStyle0><BR></P>

<P CLASS=HStyle0><BR></P>

<P CLASS=HStyle0><BR></P>

<P CLASS=HStyle0><BR></P>

<P CLASS=HStyle0><BR></P>

<P CLASS=HStyle0><BR></P>

<P CLASS=HStyle0><BR></P>

<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:15.0pt;line-height:160%'>

<input type="text" value="${fn:substring(date,0,4)}년 ${fn:substring(date,4,6)}월 ${fn:substring(date,6,8)}일" style="text-align:center;border: 0;font-size:15.0pt;line-height:160%"/>
</SPAN></P>

<P CLASS=HStyle0><BR></P>

<P CLASS=HStyle0><BR></P>

<P CLASS=HStyle0 STYLE='text-align:center;'><img src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}${logo_report_url}"></P>

<P CLASS=HStyle0><BR></P>

<P CLASS=HStyle0><BR></P>

<P CLASS=HStyle0><BR></P>

<div style='page-break-before:always'></div>

<P CLASS=HStyle0><BR></P>

<P CLASS=HStyle2><b>1. 조회 기간 및 대상 시스템</b></P>

<P CLASS=HStyle3>□ 조회기간 : ${startdate[0] }년 ${startdate[1] }월 ${startdate[2] }일 ~ ${enddate[0] }년 ${enddate[1] }월 ${enddate[2] }일</P>

<P CLASS=HStyle3>□ 대상시스템 : (대상시스템 전체)</P>

<P CLASS=HStyle0></P>

<P CLASS=HStyle0><BR></P>

<P CLASS=HStyle0><BR></P>

<P CLASS=HStyle2><b>2. 접근행위별 개인정보 접근 건수 통계 (사용자)</b></P>
<P CLASS=HStyle3></P>

<P CLASS=HStyle3><SPAN STYLE='font-weight:"bold"'><BR></SPAN></P>
<P CLASS=HStyle3><SPAN STYLE='font-weight:"bold"'>1) 개인정보 조회 TOP 10</SPAN></P>

<P CLASS=HStyle3></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:770px;'>
<TR>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:5%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>순위</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:12%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>식별자(ID/IP)</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:10%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:10%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>소속</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:8%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>조회건수</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:15%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>개인정보유형</SPAN></P>
	</TD>
</TR>

<c:forEach items="${top10Select }" var="item">
<TR>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.rank }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.emp_user_id }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.emp_user_name }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.dept_name }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><fmt:formatNumber value="${item.tot_cnt }" pattern="#,###" /></P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
<%-- 	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.privacy_type }</P> --%>
		<table style="width: 100%; height: 100%">
			<c:forEach items="${item.reportData }" var="data">
				<tr>
					<td valign="middle" style='word-break:break-all;width:60%;'><P CLASS=HStyle0 STYLE='text-align:center;'>${data.privacy_desc }</P></td>
					<td valign="middle" style='word-break:break-all;width:40%;'><P CLASS=HStyle0 STYLE='text-align:center;'><fmt:formatNumber value="${data.cnt }" pattern="#,###" /></P></td>					
				</tr>
			</c:forEach>
		</table>
	</TD>
</TR>
</c:forEach>
</TABLE>
<P CLASS=HStyle3></P>

<P CLASS=HStyle3><SPAN STYLE='font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle3><SPAN STYLE='font-weight:"bold"'>2) 개인정보 등록 TOP 10</SPAN></P>

<P CLASS=HStyle3></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:770px;'>
<TR>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:5%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>순위</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:12%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>식별자(ID/IP)</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:10%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:10%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>소속</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:8%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>조회건수</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:15%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>개인정보유형</SPAN></P>
	</TD>
</TR>

<c:forEach items="${top10Create }" var="item">
<TR>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.rank }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.emp_user_id }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.emp_user_name }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.dept_name }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><fmt:formatNumber value="${item.tot_cnt }" pattern="#,###" /></P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
<%-- 	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.privacy_type }</P> --%>
		<table style="width: 100%; height: 100%">
			<c:forEach items="${item.reportData }" var="data">
				<tr>
					<td valign="middle" style='word-break:break-all;width:60%;'><P CLASS=HStyle0 STYLE='text-align:center;'>${data.privacy_desc }</P></td>
					<td valign="middle" style='word-break:break-all;width:40%;'><P CLASS=HStyle0 STYLE='text-align:center;'><fmt:formatNumber value="${data.cnt }" pattern="#,###" /></P></td>					
				</tr>
			</c:forEach>
		</table>
	</TD>
</TR>
</c:forEach>
</TABLE>
<P CLASS=HStyle3></P>

<P CLASS=HStyle3><SPAN STYLE='font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle3><SPAN STYLE='font-weight:"bold"'>3) 개인정보 수정 TOP 10</SPAN></P>

<P CLASS=HStyle3></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:770px;'>
<TR>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:5%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>순위</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:12%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>식별자(ID/IP)</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:10%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:10%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>소속</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:8%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>조회건수</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:15%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>개인정보유형</SPAN></P>
	</TD>
</TR>

<c:forEach items="${top10Update }" var="item">
<TR>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.rank }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.emp_user_id }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.emp_user_name }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.dept_name }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><fmt:formatNumber value="${item.tot_cnt }" pattern="#,###" /></P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
<%-- 	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.privacy_type }</P> --%>
		<table style="width: 100%; height: 100%">
			<c:forEach items="${item.reportData }" var="data">
				<tr>
					<td valign="middle" style='word-break:break-all;width:60%;'><P CLASS=HStyle0 STYLE='text-align:center;'>${data.privacy_desc }</P></td>
					<td valign="middle" style='word-break:break-all;width:40%;'><P CLASS=HStyle0 STYLE='text-align:center;'><fmt:formatNumber value="${data.cnt }" pattern="#,###" /></P></td>					
				</tr>
			</c:forEach>
		</table>
	</TD>
</TR>
</c:forEach>
</TABLE>
<P CLASS=HStyle3></P>

<P CLASS=HStyle3><SPAN STYLE='font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle3><SPAN STYLE='font-weight:"bold"'>4) 개인정보 삭제 TOP 10</SPAN></P>

<P CLASS=HStyle3></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:770px;'>
<TR>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:5%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>순위</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:12%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>식별자(ID/IP)</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:10%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:10%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>소속</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:8%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>조회건수</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:15%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>개인정보유형</SPAN></P>
	</TD>
</TR>

<c:forEach items="${top10Delete }" var="item">
<TR>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.rank }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.emp_user_id }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.emp_user_name }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.dept_name }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><fmt:formatNumber value="${item.tot_cnt }" pattern="#,###" /></P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
<%-- 	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.privacy_type }</P> --%>
		<table style="width: 100%; height: 100%">
			<c:forEach items="${item.reportData }" var="data">
				<tr>
					<td valign="middle" style='word-break:break-all;width:60%;'><P CLASS=HStyle0 STYLE='text-align:center;'>${data.privacy_desc }</P></td>
					<td valign="middle" style='word-break:break-all;width:40%;'><P CLASS=HStyle0 STYLE='text-align:center;'><fmt:formatNumber value="${data.cnt }" pattern="#,###" /></P></td>					
				</tr>
			</c:forEach>
		</table>
	</TD>
</TR>
</c:forEach>
</TABLE>
<P CLASS=HStyle3></P>

<P CLASS=HStyle3><SPAN STYLE='font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle3><SPAN STYLE='font-weight:"bold"'>5) 개인정보 출력 TOP 10</SPAN></P>

<P CLASS=HStyle3></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:770px;'>
<TR>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:5%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>순위</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:12%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>식별자(ID/IP)</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:10%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:10%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>소속</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:8%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>조회건수</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:15%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>개인정보유형</SPAN></P>
	</TD>
</TR>

<c:forEach items="${top10Print }" var="item">
<TR>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.rank }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.emp_user_id }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.emp_user_name }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.dept_name }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><fmt:formatNumber value="${item.tot_cnt }" pattern="#,###" /></P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
<%-- 	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.privacy_type }</P> --%>
		<table style="width: 100%; height: 100%">
			<c:forEach items="${item.reportData }" var="data">
				<tr>
					<td valign="middle" style='word-break:break-all;width:60%;'><P CLASS=HStyle0 STYLE='text-align:center;'>${data.privacy_desc }</P></td>
					<td valign="middle" style='word-break:break-all;width:40%;'><P CLASS=HStyle0 STYLE='text-align:center;'><fmt:formatNumber value="${data.cnt }" pattern="#,###" /></P></td>					
				</tr>
			</c:forEach>
		</table>
	</TD>
</TR>
</c:forEach>
</TABLE>
<P CLASS=HStyle3></P>

<P CLASS=HStyle3><SPAN STYLE='font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle3><SPAN STYLE='font-weight:"bold"'>6) 개인정보 다운로드 TOP 10</SPAN></P>

<P CLASS=HStyle3></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:770px;'>
<TR>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:5%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>순위</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:12%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>식별자(ID/IP)</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:10%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:10%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>소속</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:8%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>조회건수</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:15%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>개인정보유형</SPAN></P>
	</TD>
</TR>

<c:forEach items="${top10Download }" var="item">
<TR>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.rank }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.emp_user_id }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.emp_user_name }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.dept_name }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><fmt:formatNumber value="${item.tot_cnt }" pattern="#,###" /></P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
<%-- 	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.privacy_type }</P> --%>
		<table style="width: 100%; height: 100%">
			<c:forEach items="${item.reportData }" var="data">
				<tr>
					<td valign="middle" style='word-break:break-all;width:60%;'><P CLASS=HStyle0 STYLE='text-align:center;'>${data.privacy_desc }</P></td>
					<td valign="middle" style='word-break:break-all;width:40%;'><P CLASS=HStyle0 STYLE='text-align:center;'><fmt:formatNumber value="${data.cnt }" pattern="#,###" /></P></td>					
				</tr>
			</c:forEach>
		</table>
	</TD>
</TR>
</c:forEach>
</TABLE>

<P CLASS=HStyle0><BR></P>

<P CLASS=HStyle0><BR></P>

<P CLASS=HStyle0><BR></P>

<div style='page-break-before:always'></div>

<P CLASS=HStyle2><b>3. 접근행위별 개인정보 접근 건수 통계 (운영자)</b></P>
<P CLASS=HStyle3></P>

<P CLASS=HStyle3><SPAN STYLE='font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle3><SPAN STYLE='font-weight:"bold"'>1) 개인정보 조회 TOP 10</SPAN></P>

<P CLASS=HStyle3></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:770px;'>
<TR>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:5%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>순위</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:12%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>식별자(ID/IP)</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:10%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:10%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>소속</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:8%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>조회건수</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:15%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>개인정보유형</SPAN></P>
	</TD>
</TR>

<c:forEach items="${top10Select2 }" var="item">
<TR>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.rank }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.emp_user_id }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.emp_user_name }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.dept_name }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><fmt:formatNumber value="${item.tot_cnt }" pattern="#,###" /></P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
<%-- 	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.privacy_type }</P> --%>
		<table style="width: 100%; height: 100%">
			<c:forEach items="${item.reportData }" var="data">
				<tr>
					<td valign="middle" style='word-break:break-all;width:60%;'><P CLASS=HStyle0 STYLE='text-align:center;'>${data.privacy_desc }</P></td>
					<td valign="middle" style='word-break:break-all;width:40%;'><P CLASS=HStyle0 STYLE='text-align:center;'><fmt:formatNumber value="${data.cnt }" pattern="#,###" /></P></td>					
				</tr>
			</c:forEach>
		</table>
	</TD>
</TR>
</c:forEach>
</TABLE>
<P CLASS=HStyle3></P>

<P CLASS=HStyle3><SPAN STYLE='font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle3><SPAN STYLE='font-weight:"bold"'>2) 개인정보 등록 TOP 10</SPAN></P>

<P CLASS=HStyle3></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:770px;'>
<TR>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:5%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>순위</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:12%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>식별자(ID/IP)</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:10%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:10%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>소속</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:8%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>조회건수</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:15%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>개인정보유형</SPAN></P>
	</TD>
</TR>

<c:forEach items="${top10Create2 }" var="item">
<TR>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.rank }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.emp_user_id }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.emp_user_name }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.dept_name }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><fmt:formatNumber value="${item.tot_cnt }" pattern="#,###" /></P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
<%-- 	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.privacy_type }</P> --%>
		<table style="width: 100%; height: 100%">
			<c:forEach items="${item.reportData }" var="data">
				<tr>
					<td valign="middle" style='word-break:break-all;width:60%;'><P CLASS=HStyle0 STYLE='text-align:center;'>${data.privacy_desc }</P></td>
					<td valign="middle" style='word-break:break-all;width:40%;'><P CLASS=HStyle0 STYLE='text-align:center;'><fmt:formatNumber value="${data.cnt }" pattern="#,###" /></P></td>					
				</tr>
			</c:forEach>
		</table>
	</TD>
</TR>
</c:forEach>
</TABLE>
<P CLASS=HStyle3></P>

<P CLASS=HStyle3><SPAN STYLE='font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle3><SPAN STYLE='font-weight:"bold"'>3) 개인정보 수정 TOP 10</SPAN></P>

<P CLASS=HStyle3></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:770px;'>
<TR>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:5%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>순위</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:12%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>식별자(ID/IP)</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:10%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:10%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>소속</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:8%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>조회건수</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:15%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>개인정보유형</SPAN></P>
	</TD>
</TR>

<c:forEach items="${top10Update2 }" var="item">
<TR>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.rank }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.emp_user_id }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.emp_user_name }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.dept_name }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><fmt:formatNumber value="${item.tot_cnt }" pattern="#,###" /></P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
<%-- 	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.privacy_type }</P> --%>
		<table style="width: 100%; height: 100%">
			<c:forEach items="${item.reportData }" var="data">
				<tr>
					<td valign="middle" style='word-break:break-all;width:60%;'><P CLASS=HStyle0 STYLE='text-align:center;'>${data.privacy_desc }</P></td>
					<td valign="middle" style='word-break:break-all;width:40%;'><P CLASS=HStyle0 STYLE='text-align:center;'><fmt:formatNumber value="${data.cnt }" pattern="#,###" /></P></td>					
				</tr>
			</c:forEach>
		</table>
	</TD>
</TR>
</c:forEach>
</TABLE>
<P CLASS=HStyle3></P>

<P CLASS=HStyle3><SPAN STYLE='font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle3><SPAN STYLE='font-weight:"bold"'>4) 개인정보 삭제 TOP 10</SPAN></P>

<P CLASS=HStyle3></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:770px;'>
<TR>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:5%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>순위</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:12%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>식별자(ID/IP)</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:10%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:10%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>소속</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:8%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>조회건수</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:15%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>개인정보유형</SPAN></P>
	</TD>
</TR>

<c:forEach items="${top10Delete2 }" var="item">
<TR>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.rank }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.emp_user_id }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.emp_user_name }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.dept_name }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><fmt:formatNumber value="${item.tot_cnt }" pattern="#,###" /></P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
<%-- 	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.privacy_type }</P> --%>
		<table style="width: 100%; height: 100%">
			<c:forEach items="${item.reportData }" var="data">
				<tr>
					<td valign="middle" style='word-break:break-all;width:60%;'><P CLASS=HStyle0 STYLE='text-align:center;'>${data.privacy_desc }</P></td>
					<td valign="middle" style='word-break:break-all;width:40%;'><P CLASS=HStyle0 STYLE='text-align:center;'><fmt:formatNumber value="${data.cnt }" pattern="#,###" /></P></td>					
				</tr>
			</c:forEach>
		</table>
	</TD>
</TR>
</c:forEach>
</TABLE>
<P CLASS=HStyle3></P>

<P CLASS=HStyle3><SPAN STYLE='font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle3><SPAN STYLE='font-weight:"bold"'>5) 개인정보 출력 TOP 10</SPAN></P>

<P CLASS=HStyle3></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:770px;'>
<TR>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:5%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>순위</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:12%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>식별자(ID/IP)</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:10%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:10%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>소속</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:8%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>조회건수</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:15%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>개인정보유형</SPAN></P>
	</TD>
</TR>

<c:forEach items="${top10Print2 }" var="item">
<TR>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.rank }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.emp_user_id }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.emp_user_name }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.dept_name }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><fmt:formatNumber value="${item.tot_cnt }" pattern="#,###" /></P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
<%-- 	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.privacy_type }</P> --%>
		<table style="width: 100%; height: 100%">
			<c:forEach items="${item.reportData }" var="data">
				<tr>
					<td valign="middle" style='word-break:break-all;width:60%;'><P CLASS=HStyle0 STYLE='text-align:center;'>${data.privacy_desc }</P></td>
					<td valign="middle" style='word-break:break-all;width:40%;'><P CLASS=HStyle0 STYLE='text-align:center;'><fmt:formatNumber value="${data.cnt }" pattern="#,###" /></P></td>					
				</tr>
			</c:forEach>
		</table>
	</TD>
</TR>
</c:forEach>
</TABLE>
<P CLASS=HStyle3></P>

<P CLASS=HStyle3><SPAN STYLE='font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle3><SPAN STYLE='font-weight:"bold"'>6) 개인정보 다운로드 TOP 10</SPAN></P>

<P CLASS=HStyle3></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:770px;'>
<TR>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:5%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>순위</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:12%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>식별자(ID/IP)</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:10%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:10%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>소속</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:8%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>조회건수</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#8ed3f8"  style='width:15%;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-weight:"bold"'>개인정보유형</SPAN></P>
	</TD>
</TR>

<c:forEach items="${top10Download2 }" var="item">
<TR>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.rank }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.emp_user_id }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.emp_user_name }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.dept_name }</P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><fmt:formatNumber value="${item.tot_cnt }" pattern="#,###" /></P>
	</TD>
	<TD valign="middle" style='word-break:break-all;height:60;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;'>
<%-- 	<P CLASS=HStyle0 STYLE='text-align:center;'>${item.privacy_type }</P> --%>
		<table style="width: 100%; height: 100%">
			<c:forEach items="${item.reportData }" var="data">
				<tr>
					<td valign="middle" style='word-break:break-all;width:60%;'><P CLASS=HStyle0 STYLE='text-align:center;'>${data.privacy_desc }</P></td>
					<td valign="middle" style='word-break:break-all;width:40%;'><P CLASS=HStyle0 STYLE='text-align:center;'><fmt:formatNumber value="${data.cnt }" pattern="#,###" /></P></td>					
				</tr>
			</c:forEach>
		</table>
	</TD>
</TR>
</c:forEach>
</TABLE>

<P CLASS=HStyle3></P>

<P CLASS=HStyle3><SPAN STYLE='font-weight:"bold"'><BR></SPAN></P>

<P CLASS=HStyle0><BR></P>



</div>
</BODY>

</HTML>
