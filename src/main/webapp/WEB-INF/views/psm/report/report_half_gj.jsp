<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script src="${rootPath}/resources/js/jquery/jquery.min.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/amchart/amcharts.js"></script>
<script src="${rootPath}/resources/js/amchart/serial.js"></script>
<script src="${rootPath}/resources/js/amchart/export.min.js"></script>
<script src="${rootPath}/resources/js/amchart/amstock.js"></script>
<script src="${rootPath}/resources/js/amchart/xy.js"></script>
<script src="${rootPath}/resources/js/amchart/radar.js"></script>
<script src="${rootPath}/resources/js/amchart/light.js"></script>
<script src="${rootPath}/resources/js/amchart/pie.js"></script>
<script src="${rootPath}/resources/js/amchart/gantt.js"></script>
<script src="${rootPath}/resources/js/amchart/gauge.js"></script>

<link rel="stylesheet" href="${pageContext.request.scheme }://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/css/export.css" type="text/css" media="all">
<script src="${rootPath}/resources/js/psm/report/report_charts_half.js"></script>
<script src="${rootPath}/resources/js/common/ajax-util.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/common-prototype-util.js" type="text/javascript" charset="UTF-8"></script>

<script type="text/javascript">

	function printpr()
	{
		$("#printButton").hide();
		$("#saveButton").hide();
		
		//window.print();
		//self.close();
		
		var $form = $("#manageHistForm");
		var log_message_params = '';
		var menu_id = '${menu_id}';
		var log_message_title = '반기별_수준진단보고 출력';
		var log_action = 'PRINT';
		var type = 'add';
		var url = '${rootPath}/report/download.html';
		
		$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
		sendAjaxPostRequest(url, $form.serialize(), ajaxReport_successHandler, ajaxReport_errorHandler, type);
	}
	
	
	rootPath = '${rootPath}';
	contextPath = '${pageContext.servletContext.contextPath}';
	var start_date = '${start_date}';
	var end_date = '${end_date}';
	var system_seq = '${system_seq}';
	var period_type = '${period_type}';
	var half_type = '${half_type}';
	var scheme = '${pageContext.request.scheme}';
	var serverName = '${pageContext.request.serverName}';
	var port = '${pageContext.request.serverPort}';

	function ajaxReport_successHandler(data, dataType, actionType){
		if(data.hasOwnProperty("statusCode")) {
			var statusCode = data.statusCode;
			if(statusCode == 'SYS006V') {
				location.href = "${rootPath}/loginView.html";
				return false;
			}
			
			if(data.hasOwnProperty("message")) {
				var message = data.message;
				alert(message);
			}
		}else{
			window.print();
			self.close();
		}
	}

	// 관리자 ajax call - 실패
	function ajaxReport_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

		log_message += codeLogMessage[actionType + "Action"];
		
		alert("실패하였습니다." + textStatus);
	}
</script>

<form id="manageHistForm" method="POST">
</form>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>

<HEAD>
<META NAME="Generator" CONTENT="Hancom HWP 8.5.8.1555">
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">
<TITLE>개인정보 접속기록 점검보고서</TITLE>
<STYLE type="text/css">
<!--
p.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle14
	{style-name:"td"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle14
	{style-name:"td"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle14
	{style-name:"td"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle15
	{style-name:"xl65"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; font-size:10.0pt; font-family:돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle15
	{style-name:"xl65"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; font-size:10.0pt; font-family:돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle15
	{style-name:"xl65"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; font-size:10.0pt; font-family:돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle16
	{style-name:"xl66"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:100%; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle16
	{style-name:"xl66"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:100%; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle16
	{style-name:"xl66"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:100%; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle17
	{style-name:"xl67"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; background-color:#ffff00; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle17
	{style-name:"xl67"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; background-color:#ffff00; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle17
	{style-name:"xl67"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; background-color:#ffff00; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
-->
</STYLE>
</HEAD>

<BODY>

<div align="right">
	<input type=button id="printButton" name="printButton" onclick="printpr();" value="출력"/>
</div>

<P CLASS=HStyle0></P>

<P CLASS=HStyle0><SPAN STYLE='font-size:15.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:26.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:20.0pt;font-family:"맑은 고딕";line-height:180%'>개인정보의 안전한 관리를 위한</SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:26.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>개인정보 접속기록의 주기적 점검</SPAN></P>
<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:26.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>결과보고서</SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:26.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:26.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<c:set var="date" value="${fn:split(date, '-') }"/>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:26.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>
<input type="text" value="${date[0] }.${date[1] }.${date[2] }" style="text-align:center;border: 0;font-size:26.0pt;font-family:'맑은 고딕';font-weight:'bold';line-height:180%"/>
</SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:26.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:26.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='text-align:center;'>

<c:if test="${use_reportLogo eq 'Y' }">
<div align="center">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" style='width:342;height:77;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<c:choose>
		<c:when test="${filename eq null }" >
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:18.0pt;line-height:160%'><img src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}${logo_report_url}"></SPAN></P>
		</c:when>
		<c:otherwise>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:18.0pt;line-height:160%'><img src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/upload/${filename }"></SPAN></P>
		</c:otherwise>
	</c:choose>
	</TD>
</TR>
</TABLE>
</div>
</c:if>
</P>

<div style='page-break-before:always'></div>

<P CLASS=HStyle0 STYLE='text-align:left;line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:100%'>
<TR>
	<TD valign="middle" bgcolor="#00468c"  style='width:45;height:44;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";letter-spacing:18%;font-weight:"bold";color:#ffffff;line-height:180%'>Ⅰ.</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#00468c"  style='width:584;height:44;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;line-height:180%;'>점검 개요</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle0 STYLE='text-align:left;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='text-align:left;line-height:225%;'><SPAN STYLE='font-size:15.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:225%'>1. 점검목적</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>&nbsp;○ </SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>개인정보처리시스템의 접속기록을 점검하여 불법적인 접근 및 비정상 행위 등을 탐지 분석하여 개인정보의 오·남용 사례를 방지하고 안전하게 관리할 수 있도록 관리·감독을 수행함</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:left;line-height:225%;'><SPAN STYLE='font-size:15.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:225%'>2. 관련 근거</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;○ 개인정보보호법 제29조(안전조치의무)</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;○ 개인정보보호법 시행령 제30조(개인정보의 안전성 확보조치)</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;○ 개인정보의 안전성 확보조치 기준 제8조(접속기록의 보관 및 점검)</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<TR>
	<TD valign="middle" bgcolor="#dfeaf5"  style='width:631;height:146;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:15.7pt;text-indent:-15.7pt;line-height:170%;'><SPAN STYLE='font-family:"맑은 고딕"'>① 개인정보처리자는 개인정보취급자가 개인정보처리시스템에 접속한 기록을 6개월 이상 보관·관리하여야한다.</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:15.5pt;text-indent:-15.5pt;line-height:170%;'><SPAN STYLE='font-family:"맑은 고딕"'>② </SPAN><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-1%'>개인정보처리자는 개인정보의 분실·도난·유출·위조·변조 또는 훼손 등에 대응하기 위하여 개인정보</SPAN><SPAN STYLE='font-family:"맑은 고딕"'>처리시스템의 접속기록 등을 반기별로 1회 이상 점검하여야 한다.</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:15.3pt;text-indent:-15.3pt;line-height:170%;'><SPAN STYLE='font-family:"맑은 고딕"'>③ </SPAN><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-3%'>개인정보처리자는 개인정보취급자의 접속기록이 위·변조 및 도난, 분실되지 않도록 해당 접속기록을</SPAN><SPAN STYLE='font-family:"맑은 고딕"'> 안전하게 보관하여야 한다.</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='text-align:left;line-height:225%;'><SPAN STYLE='font-size:15.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:225%'>3. 점검 범위 및 방법</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;○ 점검 범위</SPAN></P>

<c:set var="startdate" value="${fn:split(start_date, '-') }"/>
<c:set var="enddate" value="${fn:split(end_date, '-') }"/>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;- ${startdate[0] }년 ${startdate[1] }월 ${startdate[2] }일 ~ ${enddate[0] }년 ${enddate[1] }월 ${enddate[2] }일까지의 개인정보 접속기록 로그</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;○ 점검 방법</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;- 개인정보 처리시스템 별 개인정보 처리량 관측</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;- 월별 / 분기별 개인정보 처리량 통계 확보</SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:left;line-height:225%;'><SPAN STYLE='font-size:15.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:225%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:left;line-height:225%;'><SPAN STYLE='font-size:15.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:225%'>4. 점검 일시</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>
&nbsp;○ <input type="text" value="${date[0]}년 ${date[1]}월 ${date[2]}일" style="border:0;text-align:left;font-size:12.0pt;font-family:'맑은 고딕';line-height:180%"/>
</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'></P>


<P CLASS=HStyle0 STYLE='text-align:left;line-height:225%;'><SPAN STYLE='font-size:15.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:225%'>5. 점검 대상</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;○ 업무망 개인정보 처리시스템 접속기록 수집</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'></P>
<TABLE align="center" border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:101;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:201;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>개인정보처리시스템</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:250;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>URL</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:87;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>비고</SPAN></P>
	</TD>
</TR>
<c:forEach items="${systems }" var="system" varStatus="status">
<TR>
	<TD valign="middle" style='width:101;height:48;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:160%'>${status.count }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:201;height:48;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:100%'>${system.system_name }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:250;height:48;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:100%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:100%'>${system.main_url }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:87;height:48;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;</SPAN></P>
	</TD>
</TR>
</c:forEach>
</TABLE>
<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'></P>

<div style='page-break-before:always'></div>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:left;line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:100%;'>
<TR>
	<TD valign="middle" bgcolor="#00468c"  style='width:45;height:44;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;line-height:180%'>II.</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#00468c"  style='width:584;height:44;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;line-height:180%'>상세 결과</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle0 STYLE='text-align:left;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:225%'> </SPAN><SPAN STYLE='font-size:15.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:225%'>1. 개인정보 접속기록 현황</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;○ 개인정보처리시스템 별 접속기록 현황</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:23.2pt;text-indent:-23.2pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;- ${startdate[0] }년 ${startdate[1] }월 ${startdate[2] }일 부터 ${enddate[0] }년 ${enddate[1] }월 ${enddate[2] }일 까지 총 ${diffMon } 개월 동안 발생한 개인정보 접속기록(개인정보 이용량)은 <fmt:formatNumber value="${logCnt }" pattern="#,###" /> 건으로, 개인정보 처리시스템별로는 ${reportBySys}시스템에서 가장 많은 개인정보를 처리하고 있음</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-align:center;text-indent:-24.3pt;line-height:180%;'>
<SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>
<div align="center" id="chart1" style="width:100%; height: 450px; float: left;"></div>
</SPAN>
</P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'></P>
<TABLE id="table1" align="center" border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<thead>
<TR>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:302;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>개인정보처리시스템</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:250;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>개인정보 이용량</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:87;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>비고</SPAN></P>
	</TD>
</TR>
</thead>
<tbody>
</tbody>
</TABLE>
<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'></P>

<div align="center">
<P CLASS=HStyle0 STYLE='margin-left:54.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>* 개인정보 이용량은 개인정보취급자가 정보주체의 개인정보를 처리하기 위해 시도한 횟수를 기록한 값</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:54.3pt;text-indent:-24.3pt;line-height:135%;'><SPAN STYLE='font-family:"맑은 고딕"'>&nbsp;(예 : 접속기록 1건 내 5가지 유형의 개인정보가 처리됨 =&gt; 이용량 1건 / 처리량 5건)</SPAN></P>
</div>

<P CLASS=HStyle0 STYLE='margin-top:20.0pt;margin-bottom:20.0pt;text-align:center;line-height:180%;'>

<div style='page-break-before:always'></div>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;○ 월별 접속기록 현황</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:23.2pt;text-indent:-23.2pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;- ${startdate[0] }년 ${startdate[1] }월 ${startdate[2] }일 부터 ${enddate[0] }년 ${enddate[1] }월 ${enddate[2] } 까지 총 ${diffMon } 개월 동안 발생한 개인정보 접속기록(개인정보 이용량)의 평균은 <span id="avg_logcnt"></span> 건으로, <span id="max_month"></span>에 가장 많은 개인정보를 이용하고 있음</SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:left;'>
<SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
<div align="center">
<div id="chart2" style="width:100%; height: 450px; float: left;"></div>
</div>
</SPAN>
</P>

<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle0></P>
<TABLE id="table2" align="center" border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<thead>
<TR>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:192;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>기간</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:224;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>개인정보 이용량</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:220;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>증감량</SPAN></P>
	</TD>
</TR>
</thead>
<tbody>
</tbody>
</TABLE>
<P CLASS=HStyle0 STYLE='text-align:left;'></P>

<div align="center">
<P CLASS=HStyle0 STYLE='margin-left:54.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>* </SPAN><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-6%'>개인정보 이용량은 개인정보취급자가 정보주체의 개인정보를 처리하기 위해 시도한 횟수를 기록한 값입니다.</SPAN></P>
<P CLASS=HStyle0 STYLE='margin-left:54.3pt;text-indent:-24.3pt;line-height:135%;'><SPAN STYLE='font-family:"맑은 고딕"'>&nbsp;(예 : 접속기록 1건 내 5가지 유형의 개인정보가 처리됨 =&gt; 이용량 1건 / 처리량 5건)</SPAN></P>
</div>
<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<div style='page-break-before:always'></div>

<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:15.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:160%'>2. 개인정보 처리 현황</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;○ 시스템별 처리현황</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:23.2pt;text-indent:-23.2pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;- </SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>개인정보의 시스템별 처리량은</SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";letter-spacing:-1%;font-weight:"bold";line-height:180%'> </SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";letter-spacing:-1%;font-weight:"bold";text-decoration:"underline";line-height:180%'><span id="total_cnt"></span>건</SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>이며, </SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";letter-spacing:-1%;font-weight:"bold";text-decoration:"underline";line-height:180%'><span id="max_system"></span>시스템에서 </SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'>의 가장</SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'> 많은(</SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";text-decoration:"underline";line-height:180%'><span id="max_amount"></span>건)</SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'> 개인정보가 처리됨</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:23.2pt;text-indent:-23.2pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:23.2pt;text-indent:-23.2pt;line-height:180%;'>
<SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>
<div align="center" id="chart3" style="width:100%; height: 450px; float: left;"></div>
</SPAN>
</P>

<P CLASS=HStyle0 STYLE='text-align:center;'></P>
<TABLE id="table3" align="center" border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<thead>
<TR>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:101;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:201;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>개인정보처리시스템</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:250;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>개인정보 처리량</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:87;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>비고</SPAN></P>
	</TD>
</TR>
</thead>
<tbody>
</tbody>
</TABLE>
<P CLASS=HStyle0 STYLE='text-align:left;'></P>

<div align="center">
<P CLASS=HStyle0 STYLE='margin-left:54.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>* 개인정보 처리량은 개인정보를 처리하기 위한 접속기록 內 처리된 개인정보 유형들의 수량입니다.</SPAN></P>
<P CLASS=HStyle0 STYLE='margin-left:54.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>&nbsp;(예 : 접속기록 1건 내 5가지 유형의 개인정보가 처리됨 =&gt; 이용량 1건 / 처리량 5건)</SPAN></P>
</div>

<P CLASS=HStyle0 STYLE='margin-top:20.0pt;margin-bottom:20.0pt;text-align:center;line-height:180%;'>

<div style='page-break-before:always'></div>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;○ 월별 처리현황</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:23.2pt;text-indent:-23.2pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;- </SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";letter-spacing:-4%;line-height:180%'>개인정보의 월별 처리량은</SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";letter-spacing:-4%;font-weight:"bold";line-height:180%'> </SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";letter-spacing:-4%;font-weight:"bold";text-decoration:"underline";line-height:180%'>평균 <span id="avg_cnt4"></span>건</SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";letter-spacing:-4%;line-height:180%'>이며, 상반기 중 <span id="max_month4"></span>월에 개인정보 처리량이</SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";letter-spacing:-1%;line-height:180%'> 가장 많은 것으로 나타남</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-align:center;text-indent:-24.3pt;line-height:180%;'>
<SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;
<div align="center" id="chart4" style="width:100%; height: 450px; float: left;"></div>
</SPAN>
</P>

<P CLASS=HStyle0 STYLE='text-align:center;'></P>
<TABLE id="table4" align="center" border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<thead>
<TR>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:123;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>월별</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:197;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>월별 처리량</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:231;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>전월대비 증감량</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:87;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>비고</SPAN></P>
	</TD>
</TR>
</thead>
<tbody>
</tbody>
</TABLE>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-align:center;text-indent:-24.3pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-left:54.3pt;text-indent:-24.3pt;line-height:135%;'><SPAN STYLE='font-family:"맑은 고딕"'>* 개인정보 처리량은 개인정보를 처리하기 위한 접속기록 內 처리된 개인정보 유형의 수량입니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:54.3pt;text-indent:-24.3pt;line-height:135%;'><SPAN STYLE='font-family:"맑은 고딕"'>&nbsp;(예 : 접속기록 1건 내 5가지 유형의 개인정보가 처리됨 =&gt; 이용량 1건 / 처리량 5건)</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:135%;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:135%'><BR></SPAN></P>

<%-- <div style='page-break-before:always'></div>

<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:15.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:160%'>3. 부서별 현황</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:22.9pt;text-indent:-22.9pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;- 개인정보를 가장 취급하는 부서 중 개인정보를 가장 많이 처리하는 부서는 ${max_dept }로 나타남 (${startdate[0] }년 ${startdate[1] }월 ~ ${enddate[0] }년 ${enddate[1] }월)</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-align:center;line-height:180%;'>
<SPAN STYLE='font-size:15.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>
<div align="center" id="chart5" style="width:100%; height: 450px; float: left;"></div>
</SPAN>
</P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-align:center;line-height:180%;'></P>
<TABLE align="center" border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:44;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:141;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>부서</SPAN></P>
	</TD>
	<c:forEach begin="1" end="6" var="item">
		<TD valign="middle" bgcolor="#e6eef7"  style='width:54;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold";'>${6 * (half_type - 1) + item}월</SPAN></P>
		</TD>
	</c:forEach>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:122;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>개인정보 이용량</SPAN></P>
	</TD>
</TR>
<c:forEach items="${table5 }" var="data" varStatus="status">
<TR>
	<TD valign="middle" style='width:44;height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕"'>${status.count }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:141;height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle15><SPAN STYLE='font-family:"맑은 고딕"'>${data.dept_name }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:54;height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle15><SPAN STYLE='font-family:"맑은 고딕";'><fmt:formatNumber value="${data.type1}" pattern="#,###" /></SPAN></P>
	</TD>
	<TD valign="middle" style='width:54;height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle15><SPAN STYLE='font-family:"맑은 고딕";'><fmt:formatNumber value="${data.type2}" pattern="#,###" /></SPAN></P>
	</TD>
	<TD valign="middle" style='width:54;height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle15><SPAN STYLE='font-family:"맑은 고딕";'><fmt:formatNumber value="${data.type3}" pattern="#,###" /></SPAN></P>
	</TD>
	<TD valign="middle" style='width:54;height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle15><SPAN STYLE='font-family:"맑은 고딕";'><fmt:formatNumber value="${data.type4}" pattern="#,###" /></SPAN></P>
	</TD>
	<TD valign="middle" style='width:54;height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle15><SPAN STYLE='font-family:"맑은 고딕";'><fmt:formatNumber value="${data.type5}" pattern="#,###" /></SPAN></P>
	</TD>
	<TD valign="middle" style='width:54;height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle15><SPAN STYLE='font-family:"맑은 고딕";'><fmt:formatNumber value="${data.type6}" pattern="#,###" /></SPAN></P>
	</TD>
	<TD valign="middle" style='width:122;height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle15><SPAN STYLE='font-family:"맑은 고딕"'><fmt:formatNumber value="${data.type99}" pattern="#,###" /></SPAN></P>
	</TD>
</TR>
</c:forEach>
</TABLE>
<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:135%;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:135%'><BR></SPAN></P>
 --%>
<div style='page-break-before:always'></div>

<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:15.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:160%'>3. 분기별 처리 현황</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:23.5pt;text-indent:-23.5pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;- </SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";letter-spacing:-5%;line-height:180%'>처리한 개인정보의</SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";letter-spacing:-5%;font-weight:"bold";line-height:180%'> 분기별 </SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";letter-spacing:-6%;font-weight:"bold";text-decoration:"underline";line-height:180%'>평균 건수는 <span id="avg_cnt6"></span></SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";letter-spacing:-8%;font-weight:"bold";text-decoration:"underline";line-height:180%'>건</SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";letter-spacing:-5%;line-height:180%'>이며, <span id="quarter1"></span> 대비</SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";letter-spacing:-3%;line-height:180%'> <span id="quarter2"></span>에 개인정보 처리량이 <span id="res6"></span></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-align:center;line-height:180%;'>
<SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>
<div align="center" id="chart6" style="width:100%; height: 450px; float: left;"></div>
</SPAN>
</P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-align:center;line-height:180%;'>
<TABLE align="center" id="table6" border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<thead>
<TR>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:186;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>${2 * (half_type - 1) + 1 }분기</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:186;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>${2 * (half_type - 1) + 2 }분기</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:174;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>비고 (증감량)</SPAN></P>
	</TD>
</TR>
</thead>
<tbody>
</tbody>
</TABLE>

<div style='page-break-before:always'></div>


<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-align:center;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:15.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>&nbsp;</SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:left;line-height:180%;'>
<TABLE width="100%" border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#00468c"  style='width:45;height:44;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;line-height:180%'>III.</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#00468c"  style='width:584;height:44;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;line-height:180%'>종합 결과</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle0 STYLE='text-align:left;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:15.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:225%'>1. 개인정보 접속기록 점검 결과</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:19.5pt;text-indent:-19.5pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";letter-spacing:-5%;line-height:180%'>&nbsp;- </SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";letter-spacing:-7%;line-height:180%'>${startdate[0] }년 ${startdate[1] }월 ${startdate[2] }일 ~ ${enddate[0] }년 ${enddate[1] }월 ${enddate[2] }일까지 총 ${diffMon } 개월 동안 발생한 </SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>개인정보 </SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>처리량은 </SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><span id="total_cnt2"></span></SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>건 이고, </SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>개인정보의 </SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>월 평균처리량은 <span id="avg_total"></span>건임</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-align:center;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>[개인정보 전체 처리현황 증빙자료 ${startdate[1] }월]</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-align:center;line-height:180%;'></P>
<TABLE id="uploadtable1" align="center" border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" style='width:635;height:300;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${startdate[1] }월 ${startdate[2] }일자 접속기록 수집화면을 캡쳐해 주세요.</P>
	<div align="center">
	<form id="uploadForm1" enctype="multipart/form-data" method="POST">
    	<input type="file" name="filename" id="filename" accept=".gif, .jpg, .png" />
        <input type="button" value="등록" onclick="uploadImage1()"/>
    </form>
    </div>
    </TD>
</TR>
</TABLE>
<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-align:center;text-indent:-24.3pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-align:center;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>[개인정보 전체 처리현황 증빙자료 ${enddate[1] }월]</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-align:center;line-height:180%;'></P>
<TABLE id="uploadtable2" align="center" border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" style='width:635;height:300;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${enddate[1] }월 ${enddate[2] }일자 접속기록 수집화면을 캡쳐해 주세요.</P>
	<div align="center">
	<form id="uploadForm2" enctype="multipart/form-data" method="POST">
    	<input type="file" name="filename" id="filename" accept=".gif, .jpg, .png" />
        <input type="button" value="등록" onclick="uploadImage2()"/>
    </form>
    </div>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-align:center;text-indent:-24.3pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:15.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:225%'>2. 접속기록 사후처리</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:10.9pt;line-height:180%;'></P>
<TABLE align="center" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" style='width:650;height:200;'>
<!-- 	<P CLASS=HStyle0 STYLE='margin-left:10.7pt;line-height:180%;'><SPAN STYLE='font-family:"문체부 궁체 흘림체";color:#7f7f7f'>&nbsp;</SPAN></P> -->
		<TEXTAREA rows="12" style="width: 100%;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;"></TEXTAREA>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle0 STYLE='margin-left:10.9pt;text-indent:-10.9pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-left:10.7pt;text-indent:-10.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

</BODY>

</HTML>

