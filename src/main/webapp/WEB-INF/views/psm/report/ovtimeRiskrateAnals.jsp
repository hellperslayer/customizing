<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags" %>

<c:set var="currentMenuId" value="MENU00076" />
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/report/report.js" type="text/javascript" charset="UTF-8"></script>

<div class="contents left">
	<!-- 메뉴명 -->
	<h3 class="page_name">${currentMenuName}</h3>

	<form id="listForm" method="POST">
		<!-- 검색영역 -->
		<div class="option_area left">
			<div class="optgroup">
				<div class="col3 left">
					<span>	
						<label><span class="option_name">기간</span></label>
						<input name="start_date" type="text" class="calender search_fr" value="<procdate:convertDate value="${paramBean.search_from}" />" />
						<i class="sim">&sim;</i>
						<input name="end_date" type="text" class="calender search_to" value="<procdate:convertDate value="${paramBean.search_to}" />" />
					</span>
				</div>
				<div class="col3 left">
					<span>	
						<label><span class="option_name">단위기간별</span></label>
						<input type="hidden" name="chk_date_type" value=""/>
						<a class="btnSummary blue" onclick="searchAnals('DAY')">일일</a>
						<a class="btnSummary blue" onclick="searchAnals('WEEK')">주간</a>
						<a class="btnSummary blue" onclick="searchAnals('MON')">월간</a>
						<input type="hidden" name="max_rnum" value="5"/>
					</span>
				</div>
				
				<!-- 버튼 영역 -->
				<div class="option_btn left">
					<p class="right">
						<a class="btn gray" onclick="resetOptions(reportConfig['listUrl'])">취소</a>
						<a class="btn blue" onclick="searchAnals()">검색</a>
					</p>
				</div>
			</div>
		</div>
		
		<!-- option_tab -->
		<div>
			<div class="option_tab" title="close">
				<p class="close">close</p>
			</div>
			<div class="option_tab" title="open" style="display: none;">
				<p class="open">open</p>
			</div>
		</div>
		
		<input type="hidden" name="current_menu_id" value="${currentMenuId}"/>
	
	</form>

	<div>
		<table class="table left">
		</table>
	</div>
</div>

<script type="text/javascript">

	var reportConfig = {
		//"listUrl":"${rootPath}/report/list_ovtimeRiskrateAnals.html"
		"listUrl":"/PSM_REPORT_DEMO_NEW/frameset?__report=report/psm/holi_danger_analysis.rptdesign"
	};
	
</script>
