<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script src="${rootPath}/resources/js/jquery/jquery.min.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/amchart/amcharts.js"></script>
<script src="${rootPath}/resources/js/amchart/serial.js"></script>
<script src="${rootPath}/resources/js/amchart/export.min.js"></script>
<script src="${rootPath}/resources/js/amchart/amstock.js"></script>
<script src="${rootPath}/resources/js/amchart/xy.js"></script>
<script src="${rootPath}/resources/js/amchart/radar.js"></script>
<script src="${rootPath}/resources/js/amchart/light.js"></script>
<script src="${rootPath}/resources/js/amchart/pie.js"></script>
<script src="${rootPath}/resources/js/amchart/gantt.js"></script>
<script src="${rootPath}/resources/js/amchart/gauge.js"></script>

<link rel="stylesheet" href="${pageContext.request.scheme }://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/css/export.css" type="text/css" media="all">
<script src="${rootPath}/resources/js/psm/report/report_charts_half.js"></script>

<script type="text/javascript">

	function printpr()
	{
		$("#printButton").hide();
		$("#saveButton").hide();
		
		window.print();
		self.close();
	}
	
	
	rootPath = '${rootPath}';
	contextPath = '${pageContext.servletContext.contextPath}';
	var start_date = '${start_date}';
	var end_date = '${end_date}';
	var system_seq = '${system_seq}';
	var period_type = '${period_type}';
	var half_type = '${half_type}';
	var scheme = '${pageContext.request.scheme}';
	var serverName = '${pageContext.request.serverName}';
	var port = '${pageContext.request.serverPort}';

</script>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>

<HEAD>
<META NAME="Generator" CONTENT="Hancom HWP 9.0.0.562">
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=euc-kr">
<TITLE>개인정보의 안전한 관리를 위한</TITLE>
<STYLE type="text/css">
<!--
p.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle14
	{style-name:"td"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle14
	{style-name:"td"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle14
	{style-name:"td"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle15
	{style-name:"xl65"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; font-size:10.0pt; font-family:돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle15
	{style-name:"xl65"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; font-size:10.0pt; font-family:돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle15
	{style-name:"xl65"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; font-size:10.0pt; font-family:돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle16
	{style-name:"xl66"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:100%; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle16
	{style-name:"xl66"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:100%; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle16
	{style-name:"xl66"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:100%; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle17
	{style-name:"xl67"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; background-color:#ffff00; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle17
	{style-name:"xl67"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; background-color:#ffff00; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle17
	{style-name:"xl67"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; background-color:#ffff00; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
-->
</STYLE>
</HEAD>

<BODY>
<div id="source-html">
<div align="right">
	<input type=button id="printButton" name="printButton" onclick="printpr();" value="출력"/>
</div>

<P CLASS=HStyle0></P>

<P CLASS=HStyle0><SPAN STYLE='font-size:15.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:26.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:20.0pt;font-family:"맑은 고딕";line-height:180%'>개인정보의 안전한 관리를 위한</SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:26.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>개인정보처리시스템 접근권한 관리대장</SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:23.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>(${systems[0].system_name })</SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:26.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:26.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:26.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>
<input type="text" value="${fn:substring(date,0,4)}.${fn:substring(date,4,6)}.${fn:substring(date,6,8)}" style="text-align:center;border: 0;font-size:26.0pt;font-family:'맑은 고딕';font-weight:'bold';line-height:180%"/>
</SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:26.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:26.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;'>
<c:if test="${use_reportLogo eq 'Y' }">
<div align="center">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" style='width:342;height:77;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<c:choose>
		<c:when test="${filename eq null }" >
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:18.0pt;line-height:160%'><img src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}${logo_report_url}"></SPAN></P>
		</c:when>
		<c:otherwise>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:18.0pt;line-height:160%'><img src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/upload/${filename }"></SPAN></P>
		</c:otherwise>
	</c:choose>
	</TD>
</TR>
</TABLE>
</div>
</c:if>
</P>

<div style='page-break-before:always'></div>

<P CLASS=HStyle0 STYLE='text-align:left;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:100%'>
<TR>
	<TD valign="middle" bgcolor="#00468c"  style='width:45;height:44;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";letter-spacing:18%;font-weight:"bold";color:#ffffff;line-height:180%'>Ⅰ.</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#00468c"  style='width:584;height:44;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;line-height:180%;'>개요</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle0 STYLE='text-align:left;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='text-align:left;line-height:225%;'><SPAN STYLE='font-size:15.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:225%'>1. 목적</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>&nbsp;○ </SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>개인정보처리시스템의 접근권한 관리 정책을 수립하여 이행하고 개인정보처리시스템에 대한 접근권한 부여, 변경, 말소 내역을 보관 및 관리할 수 있도록 관리·감독을 수행함</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:left;line-height:225%;'><SPAN STYLE='font-size:15.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:225%'>2. 관련 근거</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;○ 개인정보보호법 제29조(안전조치의무)</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;○ 개인정보의 안전성 확보조치 기준 제5조(접근권한의 관리)</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<TR>
	<TD valign="middle" bgcolor="#dfeaf5"  style='width:631;height:115;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:15.5pt;text-indent:-15.5pt;line-height:170%;'><SPAN STYLE='font-family:"맑은 고딕"'>② </SPAN><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-1%'>개인정보처리자는 전보 또는 퇴직 등 인사이동이 발생하여 개인정보 취급자가 변경되었을 경우, 지체없이 개인정보처리 시스템의 접근권한을 변경 또는 말소하여야 한다.</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:15.3pt;text-indent:-15.3pt;line-height:170%;'><SPAN STYLE='font-family:"맑은 고딕"'>③ </SPAN><SPAN STYLE='font-family:"맑은 고딕";letter-spacing:-3%'>개인정보처리자는 제1항 및 제2항에 의한 권한 부여, 변경 또는 말소에 대한 내역을 기록하고 그 기록을 최소 3년간 보관하여야 한다.</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:15.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:left;line-height:225%;'><SPAN STYLE='font-size:15.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:225%'>3. 관리 범위 및 방법</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;○ 관리 범위</SPAN></P>

<c:set var="startdate" value="${fn:split(start_date, '-') }"/>
<c:set var="enddate" value="${fn:split(end_date, '-') }"/>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";letter-spacing:-5%;line-height:180%'>
<c:set value="" var="accessDateString"/>
<c:choose>
	<c:when test="${accessDateType eq 2 }"><c:set value="권한부여일자가 " var="accessDateString"/></c:when>
	<c:when test="${accessDateType eq 3 }"><c:set value="권한변경일자가 " var="accessDateString"/></c:when>
	<c:when test="${accessDateType eq 4 }"><c:set value="권한해제일자가 " var="accessDateString"/></c:when>
	<c:otherwise><c:set value="" var="accessDateString"/></c:otherwise>
</c:choose>
- ${accessDateString }${startdate[0] }년 ${startdate[1] }월 ${startdate[2] }일 ~ ${enddate[0] }년 ${enddate[1] }월 ${enddate[2] }일까지의 개인정보처리시스템의 접근권한 내역
</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;○ 관리 방법</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;- 개인정보 처리시스템 별 접근 권한부여일/부여해제일 표기</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:15.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>4. 관리 대상</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;○ 업무망 개인정보 처리시스템 접근권한 내역</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' align="center">
<TR>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:201;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>개인정보처리시스템</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:250;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>URL</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:87;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>비고</SPAN></P>
	</TD>
</TR>
<c:forEach items="${systems }" var="system" varStatus="status">
<TR>
	<TD valign="middle" style='width:201;height:48;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:100%'>${system.system_name }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:250;height:48;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:100%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:100%'>${system.main_url }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:87;height:48;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;</SPAN></P>
	</TD>
</TR>
</c:forEach>
</TABLE>
<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<div style='page-break-before:always'></div>

<P CLASS=HStyle0 STYLE='text-align:left;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:100%;'>
<TR>
	<TD valign="middle" bgcolor="#00468c"  style='width:45;height:44;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;line-height:180%'>II.</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#00468c"  style='width:584;height:44;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;line-height:180%'>관리대장</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle0 STYLE='text-align:left;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>&nbsp;</SPAN></P>

<%-- <P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;○ ${systems[0].system_name }</SPAN></P> --%>

<P CLASS=HStyle0 STYLE='text-align:left;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' width="100%">
<TR>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:5%;height:29;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:10%;height:29;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>소속</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:8%;height:29;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:10%;height:29;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>사용자ID</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:7%;height:29;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>접근권한</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:10%;height:29;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>권한부여 일자</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:12%;height:29;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>신청/등록사유</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:10%;height:29;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>권한변경 일자</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:12%;height:29;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>처리내용</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:12%;height:29;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>변경사유</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:8%;height:29;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>승인자</SPAN></P>
	</TD>
</TR>

<c:choose>
<c:when test="${empty empUserList }">
<TR><TD colspan="11" valign="middle" style='height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";line-height:160%'>데이터가 없습니다.</SPAN></P></TD></TR>
</c:when>
<c:otherwise>
<c:forEach items="${empUserList }" var="access" varStatus="status">
<TR>
	<TD valign="middle" style='height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";line-height:160%'>${status.count }</SPAN></P>
	</TD>
	<TD valign="middle" style='height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";line-height:100%'>${access.dept_name }</SPAN></P>
	</TD>
	<TD valign="middle" style='height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";line-height:100%'>${access.emp_user_name }</SPAN></P>
	</TD>
	<TD valign="middle" style='height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";line-height:100%'>${access.emp_user_id }</SPAN></P>
	</TD>
	<TD valign="middle" style='height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:100%;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";line-height:100%'>
		<c:choose>
			<c:when test="${access.status eq 'W' }">있음</c:when>
<%-- 			<c:when test="${access.status eq 'L' }">없음</c:when> --%>
			<c:otherwise>없음</c:otherwise>
		</c:choose>
	</SPAN></P>
	</TD>
	<TD valign="middle" style='height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:100%;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";line-height:100%'>
		<fmt:parseDate value="${access.access_date}" pattern="yyyyMMdd" var="access_date" /> 
		<fmt:formatDate value="${access_date}" pattern="yyyy-MM-dd" /></SPAN></P>
	</TD>
	<TD valign="middle" style='height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";line-height:180%'>
		<c:choose>
			<c:when test="${fn:length(access.reason) > 15}">
				${fn:substring(access.reason, 0, 15)}...
			</c:when>
			<c:otherwise>
				${access.reason}
			</c:otherwise>
		</c:choose>
	</SPAN></P>
	</TD>
	<TD valign="middle" style='height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:100%;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";line-height:100%'>
		<fmt:parseDate value="${access.change_date}" pattern="yyyyMMdd" var="change_date" /> 
		<fmt:formatDate value="${change_date}" pattern="yyyy-MM-dd" /></SPAN></P>
	</TD>
	<TD valign="middle" style='height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";line-height:180%'>
		<c:choose>
			<c:when test="${fn:length(access.process_content) > 15}">
				${fn:substring(access.process_content, 0, 15)}...
			</c:when>
			<c:otherwise>
				${access.process_content}
			</c:otherwise>
		</c:choose>
	</SPAN></P>
	</TD>
	<TD valign="middle" style='height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";line-height:180%'>
		<c:choose>
			<c:when test="${fn:length(access.update_reason) > 15}">
				${fn:substring(access.update_reason, 0, 15)}...
			</c:when>
			<c:otherwise>
				${access.update_reason}
			</c:otherwise>
		</c:choose>
	</SPAN></P>
	</TD>
	<TD valign="middle" style='height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";line-height:180%'>${access.approver }</SPAN></P>
	</TD>
</TR>
</c:forEach>
</c:otherwise>
</c:choose>
</TABLE>
<P CLASS=HStyle0 STYLE='text-align:left;'></P>

<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>
</div>
</BODY>

</HTML>

