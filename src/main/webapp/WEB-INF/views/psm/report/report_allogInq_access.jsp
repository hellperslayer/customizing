<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script src="${rootPath}/resources/js/jquery/jquery.min.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/amchart/amcharts.js"></script>
<script src="${rootPath}/resources/js/amchart/serial.js"></script>
<script src="${rootPath}/resources/js/amchart/export.min.js"></script>
<script src="${rootPath}/resources/js/amchart/amstock.js"></script>
<script src="${rootPath}/resources/js/amchart/xy.js"></script>
<script src="${rootPath}/resources/js/amchart/radar.js"></script>
<script src="${rootPath}/resources/js/amchart/light.js"></script>
<script src="${rootPath}/resources/js/amchart/pie.js"></script>
<script src="${rootPath}/resources/js/amchart/gantt.js"></script>
<script src="${rootPath}/resources/js/amchart/gauge.js"></script>

<link rel="stylesheet" href="${pageContext.request.scheme }://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/css/export.css" type="text/css" media="all">
<script src="${rootPath}/resources/js/psm/report/report_charts_half.js"></script>

<script type="text/javascript">

	function printpr()
	{
		$("#printButton").hide();
		$("#saveButton").hide();
		
		window.print();
		self.close();
	}
	
	
	rootPath = '${rootPath}';
	contextPath = '${pageContext.servletContext.contextPath}';
	var start_date = '${start_date}';
	var end_date = '${end_date}';
	var system_seq = '${system_seq}';
	var period_type = '${period_type}';
	var half_type = '${half_type}';
	var scheme = '${pageContext.request.scheme}';
	var serverName = '${pageContext.request.serverName}';
	var port = '${pageContext.request.serverPort}';

</script>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>

<HEAD>
<META NAME="Generator" CONTENT="Hancom HWP 9.0.0.562">
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=euc-kr">
<TITLE>개인정보의 안전한 관리를 위한</TITLE>
<STYLE type="text/css">
<!--
p.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle14
	{style-name:"td"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle14
	{style-name:"td"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle14
	{style-name:"td"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle15
	{style-name:"xl65"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; font-size:10.0pt; font-family:돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle15
	{style-name:"xl65"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; font-size:10.0pt; font-family:돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle15
	{style-name:"xl65"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; font-size:10.0pt; font-family:돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle16
	{style-name:"xl66"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:100%; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle16
	{style-name:"xl66"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:100%; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle16
	{style-name:"xl66"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:100%; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle17
	{style-name:"xl67"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; background-color:#ffff00; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle17
	{style-name:"xl67"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; background-color:#ffff00; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle17
	{style-name:"xl67"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; background-color:#ffff00; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
-->
</STYLE>
</HEAD>

<BODY>

<div align="right">
	<input type=button id="printButton" name="printButton" onclick="printpr();" value="출력"/>
</div>

<P CLASS=HStyle0></P>

<P CLASS=HStyle0><SPAN STYLE='font-size:15.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:26.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:26.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>접속기록로그 및 접근권한관리 TOP20</SPAN></P>

<%-- <P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:23.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>(${systems[0].system_name })</SPAN></P> --%>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:26.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:26.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:26.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>
<input type="text" value="${fn:substring(date,0,4)}.${fn:substring(date,4,6)}.${fn:substring(date,6,8)}" style="text-align:center;border: 0;font-size:26.0pt;font-family:'맑은 고딕';font-weight:'bold';line-height:180%"/>
</SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:26.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:26.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:center;'>
<c:if test="${use_reportLogo eq 'Y' }">
<div align="center">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" style='width:342;height:77;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<c:choose>
		<c:when test="${filename eq null }" >
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:18.0pt;line-height:160%'><img src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}${logo_report_url}"></SPAN></P>
		</c:when>
		<c:otherwise>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:18.0pt;line-height:160%'><img src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/upload/${filename }"></SPAN></P>
		</c:otherwise>
	</c:choose>
	</TD>
</TR>
</TABLE>
</div>
</c:if>
</P>

<div style='page-break-before:always'></div>

<P CLASS=HStyle0 STYLE='text-align:left;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:100%'>
<TR>
	<TD valign="middle" bgcolor="#00468c"  style='width:45;height:44;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";letter-spacing:18%;font-weight:"bold";color:#ffffff;line-height:180%'>Ⅰ.</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#00468c"  style='width:584;height:44;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;line-height:180%;'>접속기록 로그</SPAN></P>
	</TD>
</TR>
</TABLE>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:150%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<c:forEach items="${resList }" var="res">

<P CLASS=HStyle5 STYLE='margin-left:25pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:11.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ ${res.system_name }</SPAN></P> 
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' width="100%">
<TR>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:87;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>일시</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:87;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>소속</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:87;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>사용자ID</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:87;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:87;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>사용자IP</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:200;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>개인정보 건수</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:87;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>수행업무</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:87;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>접근 경로</SPAN></P>
	</TD>
</TR>

<c:choose>
<c:when test="${empty res.allLogInq}">
<TR>
	<TD colspan="8" valign="middle" style='width:15%;height:35;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:100%;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";line-height:100%'>데이터가 없습니다.</SPAN></P>
	</TD>
</TR>
</c:when>
<c:otherwise>
<c:forEach items="${res.allLogInq}" var="allLogInq" varStatus="status">
<TR>
	<TD valign="middle" style='width:10%;height:35;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";line-height:100%'>
		<fmt:parseDate value="${allLogInq.proc_date}" pattern="yyyyMMdd" var="proc_date" /> 
		<fmt:formatDate value="${proc_date}" pattern="yy-MM-dd" /> 
		<fmt:parseDate value="${allLogInq.proc_time}" pattern="HHmmss" var="proc_time" /> 
		<fmt:formatDate value="${proc_time}" pattern="HH:mm:ss" /></SPAN></P>
	</TD>
	<TD valign="middle" style='width:20%;height:35;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:100%;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";line-height:100%'>${allLogInq.dept_name }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:10%;height:35;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:100%;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";line-height:100%'>${allLogInq.emp_user_id }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:10%;height:35;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:100%;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";line-height:100%'>${allLogInq.emp_user_name }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:15%;height:35;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:100%;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";line-height:100%'>${allLogInq.user_ip }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:15%;height:35;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:100%;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";line-height:100%'>
	<%-- 	<c:choose>
			<c:when test="${result_type_view eq 'Y' }">
			<c:if test="${allLogInq.result_type ne null}">
			<c:set var="prev" value=""/>
			<c:set var="cnt1" value="1"/>
			<c:set var="total" value="0"/>
			<c:forEach items="${allLogInq.result_type}">
				<c:set var="total" value="${total + 1 }"/>
			</c:forEach>
			
			<c:forEach items="${allLogInq.result_type}" var="item" varStatus="status">
				<c:choose>
					<c:when test="${prev eq item}">	<!-- 이전값과 같을 때 -->
						<c:set var="cnt1" value="${cnt1 + 1 }"/>
						<c:if test="${status.count == total }">${cnt1 }</c:if>
					</c:when>
					<c:otherwise> 					<!-- 이전값과 다를 때 -->
						<c:if test="${status.count != 1}">${cnt1 }</c:if>
						<c:forEach items="${CACHE_RESULT_TYPE}" var="i">
							<c:if test="${i.key == item }">
								<img src="${rootPath }/resources/image/icon/resultType/${i.key}.png" title="${i.value }"/>
							</c:if>
						</c:forEach>
						
						<c:set var="prev" value="${item }"/>
						<c:set var="cnt1" value="1"/>
						<c:if test="${status.count == total }">${cnt1 }</c:if>
					</c:otherwise>
				</c:choose>
			</c:forEach>
			</c:if>
			<c:if test="${allLogInq.result_type eq null}">
				-
			</c:if>
			</c:when>
			</c:choose>
	</SPAN></P> --%>
	${allLogInq.cnt } 건
	</TD>
	<TD valign="middle" style='width:10%;height:35;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:100%;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";line-height:100%'>
	<c:forEach items="${CACHE_REQ_TYPE}" var="i" varStatus="status">
		<c:if test="${i.key == allLogInq.req_type}">
			${i.value}
		</c:if>
	</c:forEach>
	</SPAN></P>
	</TD>
	<TD valign="middle" style='width:10%;height:35;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:100%;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";line-height:100%'>
		<c:choose>
			<c:when test="${allLogInq.req_url eq '' or allLogInq.req_url eq null}">
				-
			</c:when>
			<c:otherwise>
				<c:choose>
					<c:when test="${fn:length(allLogInq.req_url) > 35}">
						<c:out value="${fn:substring(allLogInq.req_url, 0, 35)}..."/>
					</c:when>
					<c:otherwise>
						<c:out value="${allLogInq.req_url}"/>
					</c:otherwise>
				</c:choose>
			</c:otherwise>
		</c:choose>
	</SPAN></P>
	</TD>
</TR>
</c:forEach>
</c:otherwise>
</c:choose>
</TABLE>
<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<div style='page-break-before:always'></div>
</c:forEach>

<P CLASS=HStyle0 STYLE='text-align:left;line-height:180%;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:100%;'>
<TR>
	<TD valign="middle" bgcolor="#00468c"  style='width:45;height:44;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;line-height:180%'>II.</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#00468c"  style='width:584;height:44;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;line-height:180%'>접근권한 관리</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle0 STYLE='text-align:left;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>&nbsp;</SPAN></P>

<%-- <P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;○ ${systems[0].system_name }</SPAN></P> --%>

<P CLASS=HStyle0 STYLE='text-align:left;'></P>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;' width="100%">
<TR>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:10%;height:29;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>소속</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:8%;height:29;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:10%;height:29;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>사용자ID</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:10%;height:29;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>권한부여 일자</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:12%;height:29;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>신청/등록사유</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:10%;height:29;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>권한변경 일자</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:12%;height:29;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>처리내용</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:12%;height:29;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>변경사유</SPAN></P>
	</TD>
</TR>

<c:choose>
<c:when test="${empty empUserList }">
<TR><TD colspan="11" valign="middle" style='height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:8.0pt;font-family:"맑은 고딕";line-height:160%'>데이터가 없습니다.</SPAN></P></TD></TR>
</c:when>
<c:otherwise>
<c:forEach items="${empUserList }" var="access" varStatus="status">
<TR>
	<TD valign="middle" style='height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";line-height:100%'>${access.dept_name }</SPAN></P>
	</TD>
	<TD valign="middle" style='height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";line-height:100%'>${access.emp_user_name }</SPAN></P>
	</TD>
	<TD valign="middle" style='height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";line-height:100%'>${access.emp_user_id }</SPAN></P>
	</TD>
	<TD valign="middle" style='height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:100%;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";line-height:100%'>
		<fmt:parseDate value="${access.access_date}" pattern="yyyyMMdd" var="access_date" /> 
		<fmt:formatDate value="${access_date}" pattern="yyyy-MM-dd" /></SPAN></P>
	</TD>
	<TD valign="middle" style='height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";line-height:180%'>
		<c:choose>
			<c:when test="${fn:length(access.reason) > 15}">
				${fn:substring(access.reason, 0, 15)}...
			</c:when>
			<c:otherwise>
				${access.reason}
			</c:otherwise>
		</c:choose>
	</SPAN></P>
	</TD>
	<TD valign="middle" style='height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:100%;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";line-height:100%'>
		<fmt:parseDate value="${access.update_date}" pattern="yyyyMMddHHmmss" var="change_date" /> 
		<fmt:formatDate value="${change_date}" pattern="yyyy-MM-dd" /></SPAN></P>
	</TD>
	<TD valign="middle" style='height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";line-height:180%'>
		<c:choose>
			<c:when test="${fn:length(access.process_content) > 15}">
				${fn:substring(access.process_content, 0, 15)}...
			</c:when>
			<c:otherwise>
				${access.process_content}
			</c:otherwise>
		</c:choose>
	</SPAN></P>
	</TD>
	<TD valign="middle" style='height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:7.0pt;font-family:"맑은 고딕";line-height:180%'>
		<c:choose>
			<c:when test="${fn:length(access.update_reason) > 15}">
				${fn:substring(access.update_reason, 0, 15)}...
			</c:when>
			<c:otherwise>
				${access.update_reason}
			</c:otherwise>
		</c:choose>
	</SPAN></P>
	</TD>
</TR>
</c:forEach>
</c:otherwise>
</c:choose>
</TABLE>
<P CLASS=HStyle0 STYLE='text-align:left;'></P>

<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:160%'><BR></SPAN></P>

</BODY>

</HTML>

