<%-- <%@ page language="java" contentType="application/vnd.word;charset=UTF-8" pageEncoding="UTF-8"%> --%>
<%-- <%@ page language="java" contentType="application/hwp;charset=UTF-8" pageEncoding="UTF-8"%> --%>

<%-- <%
	response.setHeader("Content-Disposition", "attachment;filename=report.doc");
// 	response.setHeader("Content-Disposition", "attachment;filename=report.hwp");
	response.setHeader("Content-Description", "JSP Generated Data");
// 	response.setContentType("application/vnd.ms-word");
%> --%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<script src="${rootPath}/resources/js/jquery/jquery.min.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/amchart/amcharts.js"></script>
<script src="${rootPath}/resources/js/amchart/serial.js"></script>
<script src="${rootPath}/resources/js/amchart/export.min.js"></script>
<script src="${rootPath}/resources/js/amchart/amstock.js"></script>
<script src="${rootPath}/resources/js/amchart/xy.js"></script>
<script src="${rootPath}/resources/js/amchart/radar.js"></script>
<script src="${rootPath}/resources/js/amchart/light.js"></script>
<script src="${rootPath}/resources/js/amchart/pie.js"></script>
<script src="${rootPath}/resources/js/amchart/gantt.js"></script>
<script src="${rootPath}/resources/js/amchart/gauge.js"></script>

<%-- <link rel="stylesheet" href="${rootPath}/resources/css/export.css" type="text/css" media="all"> --%>
<link rel="stylesheet" href="${pageContext.request.scheme }://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/css/export.css" type="text/css" media="all">

<script type="text/javascript" charset="UTF-8">
	rootPath = '${rootPath}';
	contextPath = '${pageContext.servletContext.contextPath}';
	var start_date = '${start_date}';
	var end_date = '${end_date}';
	var strReq = '${CACHE_REQ_TYPE}';
	var period_type = '${period_type}';
	var system_seq = '${system_seq}';
	var use_studentId = '${use_studentId}';
	var download_type = '${download_type}';
	var pdfFullPath = '${pdfFullPath}';
	var use_fullscan = '${use_fullscan}';
</script>

<script src="${rootPath}/resources/js/psm/report/report_download_charts.js"></script>
<script src="${rootPath}/resources/js/common/ajax-util.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/common-prototype-util.js" type="text/javascript" charset="UTF-8"></script>

<HEAD>
<META NAME="Generator" CONTENT="Hancom HWP 8.5.8.1541"> 
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">

<script type="text/javascript">

var saveCnt = 0;
function exportChartToImg() {
	var charts = {};
	
	for (var x = 0; x < AmCharts.charts.length; x++) {
		$("#"+AmCharts.charts[x].div.id).css("width","75%");
	}
	
 	var lts = setInterval(function() {
		for (var x = 0; x < AmCharts.charts.length; x++) {
			charts[AmCharts.charts[x].div.id] = AmCharts.charts[x];
		}
		for ( var x in charts) {
			var chart = charts[x];
			chart["export"].capture({}, function() {
				this.toJPG({}, function(data) {
					var chartName = this.setup.chart.div.id;
					$("img[name="+chartName+"]").attr("src", data);
					$("img[name="+chartName+"]").css("display","");
					console.log(chartName);
					$("#"+chartName).css("display","none");
					saveCnt++;
					if(saveCnt == AmCharts.charts.length) {
						$("img[name=logoImg]").attr("src",getBase64Image(document.getElementById("logoImg")));
						$("img[name=logoImg]").css("display","");
						$("#logoImg").remove();
						//exportDocs();
						
					}
				});
			});
			clearInterval(lts);
		}
	}, 500);
}

function getBase64Image(img) {
  var canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);
  var dataURL = canvas.toDataURL("image/png");
  return dataURL; 
}
	
function exportDocs(inter) {
	var date = new Date();
	var year = date.getFullYear();
	var month = new String(date.getMonth() + 1);
	var day = new String(date.getDate());
	var admin_user_id = "${userSession.admin_user_id}";
	
	var reportType = '${report_type}';
	
	var reportdate = "${start_date}".split('-');
	var reportyear = reportdate[0];
	var reportmonth = reportdate[1];
	
	var proc_date = reportyear+reportmonth;
	
	var periodType = '${period_type}';
	var periodTitle = "";
	if(periodType == 1){
		periodTitle = reportmonth + '월';
	}else if(periodType == 2){
		var part = "";
		if(reportmonth == 1){
			part = "1";
		}else if(reportmonth == 4){
			part = "2";
		}else if(reportmonth == 7){
			part = "3";
		}else if(reportmonth == 10){
			part = "4";
		}
		periodTitle = part+'분기';
	}else if(periodType == 3){
		var part = "";
		if(reportmonth == 1){
			part = "상";
		}else if(reportmonth == 7){
			part = "하";
		}
		periodTitle = part+'반기';
	}else if(periodType == 4){
		periodTitle = '연간';
	}else if(periodType == 5){
		periodTitle = '기간';
	}
	
	var log_message_title = reportyear+'년_'+ periodTitle + '_개인정보_다운로드_보고';
	
	
	if(month.length == 1){ 
		month = "0" + month; 
	} 
	if(day.length == 1){ 
		day = "0" + day; 
	} 
	var regdate = year + "" + month + "" + day;
	var agent = navigator.userAgent.toLowerCase();
	
	var header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' "+
    "xmlns:w='urn:schemas-microsoft-com:office:word' "+
    "xmlns='http://www.w3.org/TR/REC-html40'>"+
    "<head><meta charset='utf-8'><title>Export HTML to Word Document with JavaScript</title></head><body>";
	var footer = "</body></html>";
	var sourceHTML = header+document.getElementById("source-html").innerHTML+footer;
	var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/addReport.html',
		data: { 
			"source" : source,
			"log_message_title" : log_message_title,
			"admin_user_id" : admin_user_id,
			"report_type" : reportType,
			"proc_date" : proc_date,
			"pdfFullPath" : pdfFullPath,
			"period_type" : periodType
		},
		success: function(data) {
			if(data == 1) {
				alert("보고서 등록 성공");
				self.close();
			} else if(data == 2) {
				if(confirm('기존 보고서가 있습니다. \n\n덮어 쓰시겠습니까?')){
					alert("보고서 재등록 성공");
					self.close();
				}
			} else {
				alert("보고서 등록 실패");
			}
			parent.$('#loading').hide();
		}
	});
	
	clearInterval(inter);
}

function saveWordReport() {
	parent.$('#loading').show();
	exportChartToImg();
	var inter = setInterval(function() {
		if(saveCnt == AmCharts.charts.length){
			exportDocs(inter);
		}	
	}, 500)
}

function printpr()
{
	$("#printButton").hide();
	$("#saveButton").hide();
	
	var $form = $("#manageHistForm");
	var log_message_params = '';
	var menu_id = '${menu_id}';
	var log_message_title = '담당자용_수준진단보고 출력';
	var log_action = 'PRINT';
	var type = 'add';
	var url = '${rootPath}/report/download.html';
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	sendAjaxPostRequest(url, $form.serialize(), ajaxReport_successHandler, ajaxReport_errorHandler, type);
	
}

function printHtml()
{
	 var divContents = $("#source-html").html();
     var printWindow = window.open('', '', 'height=400,width=800');
     printWindow.document.write('<html><head><title>DIV Contents</title>');
     printWindow.document.write('</head><body >');
     printWindow.document.write(divContents);
     printWindow.document.write('</body></html>');
     printWindow.document.close();
     printWindow.print();
	
}


function ajaxReport_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = "${rootPath}/loginView.html";
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		window.print();
		self.close();
	}
}

// 관리자 ajax call - 실패
function ajaxReport_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	log_message += codeLogMessage[actionType + "Action"];
	
	alert("실패하였습니다." + textStatus);
}
</script>

<form id="manageHistForm" method="POST">
</form>

<style>
@media print {
  .amcharts-bg, .amcharts-plot-area, .amcharts-legend-bg, .amcharts-axis-grid {
    display: none;
  }
}

#loading {
 width: 100%;   
 height: 100%;   
 top: 0px;
 left: 0px;
 position: fixed;   
 display: block;   
 opacity: 0.7;   
 background-color: #fff;   
 z-index: 99;   
 text-align: center; }  
  
#loading-image {   
 position: absolute;   
 top: 50%;   
 left: 50%;  
 z-index: 100; } 
</style>


<script type="text/javascript">
$(document).ready(function() {
	$('#loading').show();
});

$(window).load(function() {   
	$('#loading').hide();
});
</script>

<div id="loading"><img id="loading-image" width="30px;" src="${rootPath}/resources/image/loading3.gif" alt="Loading..." /></div>

<STYLE type="text/css">
<!--
p.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle14
	{style-name:"xl65"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:10.0pt; font-family:굴림; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle14
	{style-name:"xl65"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:10.0pt; font-family:굴림; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle14
	{style-name:"xl65"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:10.0pt; font-family:굴림; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle15
	{style-name:"xl67"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; background-color:#e5e5e5; font-size:10.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle15
	{style-name:"xl67"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; background-color:#e5e5e5; font-size:10.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle15
	{style-name:"xl67"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; background-color:#e5e5e5; font-size:10.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
-->
</STYLE>
</HEAD>

<BODY>
<c:set var="startdate" value="${fn:split(start_date, '-') }"/>
<c:set var="enddate" value="${fn:split(end_date, '-') }"/>
<c:set var="date" value="${fn:split(date, '-') }"/>
<c:choose>
	<c:when test="${period_type == 1 }">
		<c:set var="cur" value="${startdate[1]-0 } 월"/>
		<c:set var="pre" value="이전월"/>
	</c:when>
	<c:when test="${period_type == 2 }">
		<c:choose>
		<c:when test="${startdate[1] == '01' }">
		<c:set var="cur" value="1분기"/>
		</c:when>
		<c:when test="${startdate[1] == '04' }">
		<c:set var="cur" value="2분기"/>
		</c:when>
		<c:when test="${startdate[1] == '07' }">
		<c:set var="cur" value="3분기"/>
		</c:when>
		<c:when test="${startdate[1] == '10' }">
		<c:set var="cur" value="4분기"/>
		</c:when>
		</c:choose>
		<c:set var="pre" value="이전 분기"/>
	</c:when>
	<c:when test="${period_type == 3 }">
		<c:choose>
		<c:when test="${startdate[1] == '01' }">
		<c:set var="cur" value="상반기"/>
		</c:when>
		<c:when test="${startdate[1] == '07' }">
		<c:set var="cur" value="하반기"/>
		</c:when>
		</c:choose>
		<c:set var="pre" value="이전 반기"/>
	</c:when>
	<c:when test="${period_type == 4 }">
		<c:set var="cur" value="${startdate[0]-0 } 년"/>
		<c:set var="pre" value="이전년"/>
	</c:when>
	<c:otherwise>
		<c:set var="cur" value="기간"/>
		<c:set var="pre" value="이전 기간"/>
	</c:otherwise>
</c:choose>
<div id="source-html">
<div align="right" id="buttonDiv">
	<input type=button id="printButton" name="printButton" onclick="printpr();" value="출력"/>
	<input type=button id="saveButton" name="saveButton" onclick="saveWordReport();" value="생성"/>
</div>

<div align="center">
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" style='width:627;height:142;border-left:double #000000 1.4pt;border-right:double #000000 1.4pt;border-top:double #000000 1.4pt;border-bottom:double #000000 1.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:150%;'><SPAN STYLE='font-size:19.0pt;font-family:"돋움";letter-spacing:-4%;font-weight:"bold";font-style:"italic";line-height:150%'>안전한 개인정보 관리를 위한</SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:150%;'><SPAN STYLE='font-size:25.0pt;font-family:"돋움";letter-spacing:-4%;font-weight:"bold";line-height:150%'>개인정보 다운로드 점검 결과</SPAN></P>
	</TD>
</TR>
</TABLE>
</div>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'>
<SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#000000;line-height:180%'>
<c:if test="${fn:length(systems) == 1 }" >
	${systems[0].system_name } 시스템
</c:if>
</SPAN>
</P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'><SPAN STYLE='font-size:20.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";line-height:180%'>

<input type="text" value="${date[0] }.${date[1] }.${date[2] }" style="text-align:center;border: 0;font-size:20.0pt;font-family:'한양중고딕,한컴돋움';font-weight:'bold';line-height:180%"/>
</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'>

<c:if test="${use_reportLogo eq 'Y' }">
<div align="center">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" style='width:342;height:77;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<c:choose>
		<c:when test="${filename eq null }" >
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:18.0pt;line-height:160%'>
				<img id="logoImg" src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}${logo_report_url}">
				<img name="logoImg" style="display: none">
			</SPAN></P>
		</c:when>
		<c:otherwise>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:18.0pt;line-height:160%'>
				<img id="logoImg" src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/upload/${filename }">
				<img name="logoImg" style="display: none">
			</SPAN></P>
		</c:otherwise>
	</c:choose>
	</TD>
</TR>
</TABLE>
</div>
</c:if>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'>

<c:if test="${use_reportLine eq 'Y' }">
<div align="center">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD rowspan="2" valign="middle" style='width:27;height:114;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-top:2.4pt;text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>결</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-top:2.4pt;text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>재</SPAN></P>
	</TD>
	<TD valign="middle" style='width:91;height:35;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-top:2.4pt;text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>담당</SPAN></P>
	</TD>
	<TD valign="middle" style='width:100;height:35;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-top:2.4pt;text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>CPO</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:91;height:79;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-top:2.4pt;text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>&nbsp;</SPAN></P>
	</TD>
	<TD valign="middle" style='width:100;height:79;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-top:2.4pt;text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>&nbsp;</SPAN></P>
	</TD>
</TR>
</TABLE>
</div>
</c:if>

<div style='page-break-before:always'></div><br style="page-break-before: always">

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'>
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:100% '>
<TR>
	<TD valign="middle" bgcolor="#4e9fd7"  style='width:100% ;height:44px;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0><SPAN STYLE='width:100% ;font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;line-height:160%'>Ⅰ. 점검 개요</SPAN></P>
	</TD>
</TR>
</TABLE>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;margin-top:8.0pt;line-height:180%;'> <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>1. 점검 목적</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.5pt;text-indent:-21.2pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ </SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-3%;line-height:180%'>개인정보취급자가 개인정보처리시스템에 접근하여 개인정보를 다운로드한 경우, 다운로드 접속기록 점검을 통해 개인정보의 오·남용 및 유출 방지하고 안전하게 관리할 수 있도록 관리·감독을 수행함</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:41.7pt;text-indent:-23.4pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;margin-top:8.0pt;line-height:180%;'> <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>2. 관련 근거</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보보호법 제29조 (안전조치의무)</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보보호법 시행령 제30조 (개인정보의 안전성 확보 조치)</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ </SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-3%;line-height:180%'>개인정보의 안전성 확보조치 기준 제8조(접속기록의 보관 및 점검)</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:41.7pt;text-indent:-23.4pt;line-height:180%;'>

<div align="left" style="margin-left: 10px">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" style='width:650;height:195;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:15.4pt;text-indent:-15.2pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'>① </SPAN><SPAN STYLE='font-family:"돋움";letter-spacing:-1%'>개인정보처리자는 개인정보취급자가 개인정보처리시스템에 접속한 기록을 1년 이상  보관·관리하여야 한다. 다만, 5만명 이상의 정보주체에 관하여 개인정보를 처리하거나, 고유식별정보 또는 민감정보를 처리하는 개인정보처리시스템의 경우에는 2년 이상 보관‧관리하여야 한다.</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:16.8pt;text-indent:-16.6pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'>② 개인정보처리자는 개인정보의 오‧남용, 분실·도난·유출·위조·변조 또는 훼손 등에 대응하기 위하여 개인정보처리시스템의 접속기록 등을 월 1회 이상 점검하여야 한다. 특히 개인정보를 다운로드한 것이 발견되었을 경우에는 내부 관리계획으로 정하는 바에 따라 그 사유를 반드시 확인하여야 한다.</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:16.8pt;text-indent:-16.6pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'>③ 개인정보처리자는 개인정보취급자의 접속기록이 위·변조 및 도난, 분실되지 않도록 해당 접속기록을 안전하게 보관하여야 한다.</SPAN></P>
	</TD>
</TR>
</TABLE>
</div>

<P CLASS=HStyle0 STYLE='margin-left:41.7pt;text-indent:-23.4pt;line-height:180%;'></P>
<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>


<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'> <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>3. 점검 범위 </SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 점검 범위</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:54.5pt;text-indent:-36.2pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;- </SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>

${startdate[0] }년 ${startdate[1] }월 ${startdate[2] }일 ~ ${enddate[0] }년 ${enddate[1] }월 ${enddate[2] }일까지의 개인정보 다운로드 기록 및 사유에 대한 확인 및 관리
</SPAN>
</P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'>
	<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 점검 대상 개인정보 처리시스템</SPAN>
<c:if test="${use_fullscan eq 'Y'}">
	<SPAN STYLE='font-size:10.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>(주요정보주체식별정보는 TOP3만 노출)</SPAN>
</c:if>
</P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'>

<div align="left" style="margin-left: 10px">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<tr>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'>
			<span style='font-family:"굴림";font-weight:"bold"'>번호</span>
		</p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:30%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'>
			<span style='font-family:"굴림";font-weight:"bold"'>개인정보처리시스템 명칭</span>
		</p>
	</td>
	<c:if test="${use_fullscan eq 'Y'}">
	<td valign="middle" bgcolor="#e5e5e5"  style='width:50%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'>
			<span style='font-family:"굴림";font-weight:"bold"'>주요정보주체식별정보(개인정보건수)</span>
		</p>
	</td>
	</c:if>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'>
			<span style='font-family:"굴림";font-weight:"bold"'>취급자 수</span>
		</p>
	</td>
</tr>
<c:forEach var="sys" items="${systemsDownLst}" varStatus="status">
	<tr>
		<td valign="middle" bgcolor="#e5e5e5"  style='width:47;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<span style='font-family:"굴림";font-weight:"bold"'>${status.count}</span>
			</P>
		</td>
		<td valign="middle" style='width:280;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<span style='font-family:"굴림"'>${sys.system_name}</span>
			</p>
		</td>
		<c:if test="${use_fullscan eq 'Y'}">
		<td valign="middle" style='width:280;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<span style='font-family:"굴림"'>${sys.result_type}</span>
			</p>
		</td>
		</c:if>
		<td valign="middle" style='width:280;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<span style='font-family:"굴림"'>${sys.tot_count}</span>
			</p>
		</td>
	</tr>
</c:forEach>
</TABLE>
</div>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'> <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>4. 점검 일시</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>
❍&nbsp;<input type="text" value="${date[0] }년 ${date[1] }월 ${date[2] }일" style="border:0;text-align:left;font-size:14.0pt;font-family:'한양신명조,한컴돋움';"/>
</SPAN>
</P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>
<!-- 
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'> <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>5. 점검 수행자</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%;float: left;'>❍&nbsp; 정보보호부 <input type="text" style="width:500px; height: 25px; border:0; font-size:14.0pt;font-family:'한양신명조,한컴돋움';" /></SPAN>
</P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>
 -->
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'> <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>5. 점검 기준</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보 접속기록 현황 검토</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보처리시스템에 대한 비정상 행위 시도 탐지</SPAN></P>



<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'>
<div style='page-break-before:always'></div><br style="page-break-before: always">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='width:100%;border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#4e9fd7"  style='width:100%;height:44;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0><SPAN STYLE='width:100%;font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;line-height:160%'>Ⅱ. 종합 결과</SPAN><SPAN STYLE='font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'> </SPAN></P>
	</TD>
</TR>
</TABLE>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>1. 시스템별 현황</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:39.5pt;text-indent:-21.2pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 시스템별 개인정보 다운로드 현황 </SPAN></P>







<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-43.3pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;- </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>
${startdate[0] }년 ${startdate[1] }월 ${startdate[2] }일</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>부터</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;color:#0000ff;line-height:180%'> </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>
${enddate[0] }년 ${enddate[1] }월 ${enddate[2] }일</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>까지 총 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>
${diffMonth }</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>개월 동안</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-4%;line-height:180%'> </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;line-height:180%'>개인정보 다운로드 전체 건수는 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;color:#0000ff;line-height:180%'> </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;font-weight:"bold";color:#0000ff;line-height:180%'>
<fmt:formatNumber value="${downCnt}" pattern="#,###" />
</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;line-height:180%'>건으로,</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;color:#0000ff;line-height:180%'> </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;line-height:180%'>시스템별로는</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'> </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";font-weight:"bold";color:#0000ff;line-height:180%'>
${downBySysCntName}</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>의 경우가 개인정보 다운로드 건수가 가장 많은 것으로 나타남</SPAN></P>

<div style='page-break-before:always'></div><br style="page-break-before: always">






<div class='downloadChartClass' id="chart11_1" style="width:750px; height: 285px; float: left; cursor: pointer;"></div>
<img name="chart11_1" style="width:750px; height: 285px; float: left; display: none;" />
<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'></P>

<div align="left" style="margin-left: 10px">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>순위</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:235;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보처리시스템 명칭</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>다운로드 횟수(회)</SPAN></P>
	</TD>
	<c:if test="${use_fullscan eq 'Y' }">
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 건수(건)</SPAN></P>
	</TD>
	</c:if>
</TR>

<c:forEach items="${findDownloadCntBySys}" var="i" varStatus="status">
		<TR>
			<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:235;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${i.system_name}</SPAN></P>
			</TD> 
			<TD valign="middle" style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.cnt}" pattern="#,###" /></SPAN></P>
			</TD>
			<c:if test="${use_fullscan eq 'Y' }">
			<TD valign="middle" style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.cnt2}" pattern="#,###" /></SPAN></P>
			</TD>
			</c:if>
		</TR>
</c:forEach>
</TABLE>
</div>

<div style='page-break-before:always'></div><br style="page-break-before: always">

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>2. 소속별 현황</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:39.5pt;text-indent:-21.2pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 주요 소속별 개인정보 다운로드 현황 </SPAN>
<SPAN STYLE='font-size:10.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>(최대 상위 30개)</SPAN></P>

<div class='downloadChartClass' id="chart11_2" style="width:750px; height: 285px; float: left; cursor: pointer;"></div>
<img name="chart11_2" style="width:750px; height: 285px; float: left; display: none;">

<div align="left" style="margin-left: 10px">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>순위</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:235;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>부서명</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>다운로드 횟수(회)</SPAN></P>
	</TD>
	<c:if test="${use_fullscan eq 'Y' }">
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 건수(건)</SPAN></P>
	</TD>
	</c:if>
</TR>

<c:forEach items="${findDownloadCntByDeptTop30}" var="i" varStatus="status">
		<TR>
			<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:235;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${i.dept_name}</SPAN></P>
			</TD> 
			<TD valign="middle" style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.cnt}" pattern="#,###" /></SPAN></P>
			</TD>
			<c:if test="${use_fullscan eq 'Y' }">
			<TD valign="middle" style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.cnt2}" pattern="#,###" /></SPAN></P>
			</TD>
			</c:if>
		</TR>
</c:forEach>
</TABLE>
</div>

<div style='page-break-before:always'></div><br style="page-break-before: always">
<!-- 유형별 현황 -->
<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>
<c:if test="${use_fullscan eq 'Y' }">
	<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'>
		<SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>3. 개인정보 유형별 현황</SPAN>
	</P>
	<P CLASS=HStyle5 STYLE='margin-left:39.5pt;text-indent:-21.2pt;line-height:180%;'>
		<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 다운로드한 주요 개인정보 유형</SPAN>
		<SPAN STYLE='font-size:10.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>(최대 상위 10개)</SPAN>
	</P>
	<P CLASS=HStyle5 STYLE='margin-left:39.5pt;text-indent:-21.2pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'></SPAN>
	<div class='downloadChartClass' id="chart11_3" style="width:750px; height: 285px; float: left; cursor: pointer;"></div>
	<img name="chart11_3" style="width:750px; height: 285px; float: left; display: none;">
	
	<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>
	<div align="left" style="margin-left: 10px">
	<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>순위</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:235;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 유형</SPAN></P>
		</TD>
		<!-- 
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>다운로드 횟수(회)</SPAN></P>
		</TD>
		 -->
		<c:if test="${use_fullscan eq 'Y' }">
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 건수(건)</SPAN></P>
		</TD>
		</c:if>
	</TR>
	
	<c:forEach items="${findDownloadCntByResultTop10}" var="i" varStatus="status">
			<TR>
				<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}</SPAN></P>
				</TD>
				<TD valign="middle" style='width:235;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${i.privacy_desc}</SPAN></P>
				</TD>
				<!-- 		
				<TD valign="middle" style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.cnt}" pattern="#,###" /></SPAN></P>
				</TD>
				 -->
				<c:if test="${use_fullscan eq 'Y' }">
				<TD valign="middle" style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.cnt2}" pattern="#,###" /></SPAN></P>
				</TD> 
				</c:if>
			</TR>
	</c:forEach>
	</TABLE> 
	</div> 
</c:if>
<c:choose>
	<c:when test="${use_fullscan eq 'Y' }">
	</c:when>
	<c:otherwise>
	</c:otherwise>
</c:choose>
<!-- 개인별 -->
<div style='page-break-before:always'></div><br style="page-break-before: always">
<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'>
<c:choose>
	<c:when test="${use_fullscan eq 'Y' }">
		<SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>4. 개인별 현황</SPAN>
	</c:when>
	<c:otherwise>
		<SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>3. 개인별 현황</SPAN>
	</c:otherwise>
</c:choose>
</P>
<P CLASS=HStyle5 STYLE='margin-left:39.5pt;text-indent:-21.2pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 주요 사용자별 개인정보 다운로드 현황 </SPAN>
<SPAN STYLE='font-size:10.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>(최대 상위 10개)</SPAN></P> 
<div align="left" style="margin-left: 10px">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>순위</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>이름</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:300;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보처리시스템 명칭</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>부서명</SPAN></P>
	</TD>	
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>다운로드 횟수(회)</SPAN></P>
	</TD>
	<c:if test="${use_fullscan eq 'Y' }">
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 건수(건)</SPAN></P>
	</TD>
	</c:if>
</TR>

<c:forEach items="${findDownloadCntByUserTop10}" var="i" varStatus="status">
		<TR>
			<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${i.emp_user_name}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:300;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${i.system_name}<!-- PSM, 새울행정, 주민등록, 세외수입, 이호조, 인사행정, 지방세, 세움터, 부동산거래 --></SPAN></P>
			</TD> 
			<TD valign="middle" style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${i.dept_name}</SPAN></P>
			</TD> 			
			<TD valign="middle" style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.cnt}" pattern="#,###" /></SPAN></P>
			</TD>
			<c:if test="${use_fullscan eq 'Y' }">
			<TD valign="middle" style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.cnt2}" pattern="#,###" /></SPAN></P>
			</TD> 
			</c:if>
		</TR>
</c:forEach>
</TABLE>
</div>

<div style='page-break-before:always'></div><br style="page-break-before: always">
<!-- 다운로드 URL 현황 -->
<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'>
<c:choose>
	<c:when test="${use_fullscan eq 'Y' }">
		<SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>5. 다운로드 URL 현황</SPAN>
	</c:when>
	<c:otherwise>
		<SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>4. 다운로드 URL 현황</SPAN>
	</c:otherwise>
</c:choose>
</P>
<P CLASS=HStyle5 STYLE='margin-left:39.5pt;text-indent:-21.2pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 주요 다운로드 URL 현황 </SPAN>
<SPAN STYLE='font-size:10.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>(최대 상위 5개)</SPAN></P>
<div align="left" style="margin-left: 10px">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>순위</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:330;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>주요 URL</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:125;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>다운로드 횟수(회)</SPAN></P>
	</TD>
	<c:if test="${use_fullscan eq 'Y' }">
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:125;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 건수(건)</SPAN></P>
	</TD>
	</c:if>
</TR>

<c:forEach items="${findDownloadCntByUrlTop5}" var="i" varStatus="status">
		<TR>
			<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:330;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${i.req_url}</SPAN></P>
			</TD> 
			<TD valign="middle" style='width:125;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.cnt}" pattern="#,###" /></SPAN></P>
			</TD> 
			<c:if test="${use_fullscan eq 'Y' }">
			<TD valign="middle" style='width:125;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.cnt2}" pattern="#,###" /></SPAN></P>
			</TD> 
			</c:if>
		</TR>
</c:forEach>
</TABLE>

</div>

<div style='page-break-before:always'></div><br style="page-break-before: always">
<!-- 사유별 개인정보 다운로드 현황 -->
<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'>
<c:choose>
	<c:when test="${use_fullscan eq 'Y' }">
		<SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>6. 사유별 개인정보 다운로드 현황 </SPAN>
	</c:when>
	<c:otherwise>
		<SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>5. 사유별 개인정보 다운로드 현황 </SPAN>
	</c:otherwise>
</c:choose>
</P>
<P CLASS=HStyle5 STYLE='margin-left:39.5pt;text-indent:-21.2pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 사유별 개인정보 다운로드 건수</SPAN>
<SPAN STYLE='font-size:10.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>(최대 상위 10개)</SPAN></P>
<div align="left" style="margin-left: 10px">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>순위</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:235;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>사유</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>다운로드 횟수(회)</SPAN></P>
	</TD>
	<c:if test="${use_fullscan eq 'Y' }">
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 건수(건)</SPAN></P>
	</TD>
	</c:if>
</TR>

<c:forEach items="${findDownloacCntByReasonTop10}" var="i" varStatus="status">
		<TR>
			<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:235;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${i.code_name}</SPAN></P>
			</TD>		
			<TD valign="middle" style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.cnt}" pattern="#,###" /></SPAN></P>
			</TD>
			<c:if test="${use_fullscan eq 'Y' }">
			<TD valign="middle" style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.cnt2}" pattern="#,###" /></SPAN></P>
			</TD> 
			</c:if>
		</TR>
</c:forEach>
</TABLE> 
</div> 
 
<div style='page-break-before:always'></div><br style="page-break-before: always">
<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>

</div>
</BODY>

</HTML>
