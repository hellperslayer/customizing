<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>
<c:set var="currentMenuId" value="${index_id}" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />
<c:set var="now" value="<%=new java.util.Date()%>" />
<c:set var="Year">
		<fmt:formatDate value="${now}" pattern="yyyy" />
</c:set>
<fmt:parseDate var="dateString" value="${install_date.code_name}" pattern="yyyy-MM-dd" />
<c:set var="installYear">
	<fmt:formatDate value="${dateString}" pattern="yyyy" />
</c:set>
<c:set var="mulYear" value="${Year-installYear }"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="images/favicon.ico"/>
<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/js/reportmanage/reportManagement_new.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/common-prototype-util.js" type="text/javascript" charset="UTF-8"></script>
<script>
	var auth = '${userAuth.make_report_auth}';
</script>

<link rel="stylesheet" href="${rootPath}/resources/css/reportmanage/reset.css" type="text/css" media="all">
<link rel="stylesheet" href="${rootPath}/resources/css/reportmanage/style.css" type="text/css" media="all">

<body>
<div id="loading"><img id="loading-image" width="30px;" src="${rootPath}/resources/image/loading3.gif" alt="Loading..." /></div>
<div class="wrapper">
    <input type="hidden" id="current_menu_id" name="current_menu_id" value="${index_id }" />
    <!-- container -->
    <div class="container">
        <div class="contents align_c">
            <div class="top-area">
                <h2>어떤 점검 보고서를 관리 하시겠어요?</h2>
                <ul class="report_type">
                	<c:if test="${empty reportcode }">
                		<li><a class="select report_select" style="width:210px">사용가능한 보고서가 없습니다</a></li>
                	</c:if>
               		<c:forEach var="report" items="${reportcode }" varStatus="status">	
                    	<li><a class="<c:if test='${status.count eq 1 }'>select</c:if> report_select" onclick="reportSelect(this)" value="${report.code_id }">${report.code_name }</a></li>
					</c:forEach>
                </ul>
            </div>
            <div class="report">
                <div class="option-area">
                    <select id="select_year" onchange="selectYear()">
                        <optgroup>
                        	<c:forEach var="mul" begin="0" end="${mulYear }" step="1">
                        		<option>${Year-mul }년</option>
                        	</c:forEach>
                        </optgroup>
                    </select>      
                    <!--
                    	.report-object 클릭시 popup.html 띄워주세요. 
                    -->              
                    <a onclick="systemOptionPopup()" class="report-object align_r">보고서 생성 대상 시스템 <strong id='targetSysCount'>${fn:length(sysAuthIds) }</strong> 건</a>
                </div>  <!-- e:option-area -->
                
                
                <!-- 연간 보고서 -->
                <div class="title">
                	<h3 class="align_l">연간 보고서</h3>
                	<button  class="btn_f_open active_target_4 align_r" data-number="4"></button>
                </div>
                
                <ul class="report_list report_list_y align_l active_target_4">
                	<!--
                		li상황별 li와 버튼에 class 추가 및 삭제됩니다. 상황별UI는 월별 보고서를 참고해주세요.
                        
                        .등록된 데이터X  보고서 생성가능O : li.nodata / 보고서생성버튼 btn.btn_line
                        .등록된 데이터X  보고서 생성불가능X : li.nodata / 보고서생성버튼 btn.btn_line_disable
                        .등록된 데이터O  보고서 다운로드O : li class없음.
                        .등록된 데이터O  보고서 선택O : li.select
                	-->
                    <li class="">
                    	<div class="list-bar">
                            <div class="data">
                                <div class="align_l">
                                    <h4><c class="year">2020</c>년 통합 점검보고서</h4>
                                    <div>ㆍ마지막 업데이트 <span class='lastUpdate'></span></div>
                                    <span class="inpChk">
	                                    <label for="ch1" style="cursor:pointer"><input type="checkbox" id="input_chart_4_1" onchange="reportOption(4,1,this)" checked="checked"/> 차트포함</label>
	                                </span>                                
	                                <span class="inpChk">
	                                    <label for="ch2" style="cursor:pointer"><input type="checkbox" id="input_description_4_1" onchange="reportOption(4,1,this)" checked="checked"/> 총평포함</label>
	                                </span>
	                                <span class="inpChk">
	                                    <button class="btn btn_s btn_desc" type="button" onclick="descPopup(4,1)"><img src="${rootPath}/resources/image/reportmanage/btn-edit.gif" alt="총평 수정하기" title="총평 수정하기" /></button>	
	                                </span>
                                </div>
                                <div class="align_r">
                                    <span class="activeReport">
	                                    <button class="btn btn btn_redo" type="button" onclick="report_delete(4,1)">초기화</button>
	                                    <!-- <button class="btn btn_m btn_gray" type="button">미리보기</button> -->
	                                    <button class="btn btn_m btn_blue" type="button" onclick="select(this,4,1)">다운로드</button>
                                    </span>
                                    <span class="nonActiveReport">
                                    	<button class="btn btn_l btn_line" type="button" onclick="makeReport(4,1)">+ 보고서 생성</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="option-bar">
                            <div class="align_l">
           	                    <form action="report_upload.html" class="inpChk fileBox" method="post" enctype="multipart/form-data">
                                    <label class="btn_s btn_file uploadBtn">파일 업로드</label>
                                    <input type="text" class="fileName" readonly="readonly">
                                    <input type="file" class="uploadBtn file" name="file">
                       				<input type="hidden" id="reportSeq_4_1" class="reportSeq" name="report_seq"/>
                       				<input type="hidden" name="report_type"/>
                                </form> 
                                  	<button for="uploadBtn" class="btn_s btn_t_download uploadBtn" onclick="userReportDownload(4,1)">다운로드</button>    
                            </div>
                            <div class="align_r">
                                <span class="download_info align_l">파일 형식 선택</span>
                                <button class="btn btn_s" type="button" name="excelBtn" onclick="report_excel(4,1)"><img src="${rootPath}/resources/image/reportmanage/btn-xlsx.gif" alt="엑셀다운로드" title="엑셀다운로드" /></button>
                                <button class="btn btn_s" type="button" onclick="report_pdf(4,1)"><img src="${rootPath}/resources/image/reportmanage/btn-pdf.gif" alt="PDF 다운로드" title="PDF 다운로드" /></button>
                                <button class="btn btn_s" type="button" onclick="report_word(4,1)"><img src="${rootPath}/resources/image/reportmanage/btn-doc.gif" alt="워드 다운로드" title="워드 다운로드" /></button>
                            </div>
                        </div>  <!-- e:option-bar -->
                    </li>                   
                </ul>
                
                
                <!-- 분기별 보고서 -->
                <div class="title">
                	<h3 class="align_l">분기별 보고서</h3>
                	<button  class="btn_f_open active_target_2 align_r" data-number="2"></button>
                </div>
                
                <ul class="report_list report_list_q align_l active_target_2">
                   	<c:forEach var="i" begin="1" end="4" step="1">
	                    <li class="">
	                    	<div class="list-bar">
	                            <div class="data">
	                                <div class="align_l">
	                                    <h4><c class="year">2020</c>년 ${i }분기 점검보고서</h4>
	                                    <div>ㆍ마지막 업데이트 <span class='lastUpdate'></span></div>
	                                    <span class="inpChk">
	                                    	<label for="ch1" style="cursor:pointer"><input type="checkbox" id="input_chart_2_${1+(i-1)*3 }" onchange="reportOption(2,${1+(i-1)*3 },this)" checked="checked"/> 차트포함</label>
		                                </span>                                
		                                <span class="inpChk">
		                                    <label for="ch2" style="cursor:pointer"><input type="checkbox" id="input_description_2_${1+(i-1)*3 }" onchange="reportOption(2,${1+(i-1)*3 },this)" checked="checked"/> 총평포함</label>
		                                </span>
		                                <span class="inpChk">
		                                    <button class="btn btn_s btn_desc" type="button" onclick="descPopup(2,${1+(i-1)*3 })"><img src="${rootPath}/resources/image/reportmanage/btn-edit.gif" alt="총평 수정하기" title="총평 수정하기" /></button>	
		                                </span>
	                                </div>
	                                <div class="align_r">
	                                    <span class="activeReport">
		                                    <button class="btn btn btn_redo" type="button" onclick="report_delete(2,${i})">초기화</button>
		                                    <!-- <button class="btn btn_m btn_gray" type="button">미리보기</button> -->
		                                    <button class="btn btn_m btn_blue" type="button" onclick="select(this,2,${i})">다운로드</button>
	                                    </span>
	                                    <span class="nonActiveReport">
	                                    	<button class="btn btn_l btn_line" type="button" onclick="makeReport(2,${i})">+ 보고서 생성</button>
	                                    </span>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="option-bar">
	                            <div class="align_l">
             	                    <form action="report_upload.html" class="inpChk fileBox" method="post" enctype="multipart/form-data">
	                                    <label class="btn_s btn_file uploadBtn">파일 업로드</label>
	                                    <input type="text" class="fileName" readonly="readonly">
	                                    <input type="file" class="uploadBtn file" name="file">
	                       				<input type="hidden" id="reportSeq_2_${i }" class="reportSeq" name="report_seq"/>
	                       				<input type="hidden" name="report_type"/>
	                                </form> 
                                   	<button for="uploadBtn" class="btn_s btn_t_download uploadBtn" onclick="userReportDownload(2,${i})">다운로드</button>    
	                            </div>
	                            <div class="align_r">
	                                <span class="download_info align_l">파일 형식 선택</span>
	                                <button class="btn btn_s" type="button" name="excelBtn" onclick="report_excel(2,${i })"><img src="${rootPath}/resources/image/reportmanage/btn-xlsx.gif" alt="엑셀다운로드" title="엑셀다운로드" /></button>
	                                <button class="btn btn_s" type="button" onclick="report_pdf(2,${i })"><img src="${rootPath}/resources/image/reportmanage/btn-pdf.gif" alt="PDF 다운로드" title="PDF 다운로드" /></button>
	                                <button class="btn btn_s" type="button" onclick="report_word(2,${i })"><img src="${rootPath}/resources/image/reportmanage/btn-doc.gif" alt="워드 다운로드" title="워드 다운로드" /></button>
	                            </div>
	                        </div>  <!-- e:option-bar -->
	                    </li>
                    </c:forEach>
                </ul>                
                
                
                <!-- 월별 보고서 -->
                <div class="title">
                	<h3 class="align_l">월별 보고서</h3>
                	<button  class="btn_f_open active_target_1 align_r active" data-number="1"></button>
                </div>
                
                <ul class="report_list report_list_m align_l active_target_1 active">
                    <!-- 입력 데이터 있음. 비선택-->
                    <c:forEach var="i" begin="1" end="12" step="1">
	                    <li class="">
	                    	<div class="list-bar">
	                            <div class="data">
	                                <div class="align_l">
	                                    <h4><c class="year"></c>년 ${i }월 점검보고서</h4>
	                                    <div>ㆍ마지막 업데이트 <span class='lastUpdate'></span></div>
		                                <span class="inpChk">
		                                    <label for="ch1" style="cursor:pointer"><input type="checkbox" id="input_chart_1_${i }" onchange="reportOption(1,${i },this)" checked="checked"/> 차트포함</label>
		                                </span>                                
		                                <span class="inpChk">
		                                    <label for="ch2" style="cursor:pointer"><input type="checkbox" id="input_description_1_${i }" onchange="reportOption(1,${i },this)" checked="checked"/> 총평포함</label>
		                                </span>
		                                <span class="inpChk">
		                                    <button class="btn btn_s btn_desc" type="button" onclick="descPopup(1,${i })"><img src="${rootPath}/resources/image/reportmanage/btn-edit.gif" alt="총평 수정하기" title="총평 수정하기" /></button>	
		                                </span>
	                                </div>
		                            <div class="align_l2">
		                            	<div>설치 이전 날짜의 보고서는 생성할 수 없습니다.</div>                                        
		                            </div>	
	                                <div class="align_r">
	                                	<span class="activeReport">
		                                    <button class="btn btn btn_redo" type="button" onclick="report_delete(1,${i})">초기화</button>
		                                    <!-- <button class="btn btn_m btn_gray" type="button">미리보기</button> -->
		                                    <button class="btn btn_m btn_blue" type="button" onclick="select(this,1,${i})">다운로드</button>
	                                    </span>
	                                    <span class="nonActiveReport">
	                                    	<button class="btn btn_l btn_line" type="button" onclick="makeMonthReport(1,${i})">+ 보고서 생성</button>
	                                    </span>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="option-bar">
	                            <div class="align_l">
	                               <form action="report_upload.html" class="inpChk fileBox" method="post" enctype="multipart/form-data">
	                                    <label class="btn_s btn_file uploadBtn">파일 업로드</label>
	                                    <input type="text" class="fileName" readonly="readonly">
	                                    <input type="file" class="uploadBtn file" name="file">
	                       				<input type="hidden" id="reportSeq_1_${i }" class="reportSeq" name="report_seq"/>
	                       				<input type="hidden" name="report_type"/>
	                                </form> 
                                   	<button for="uploadBtn" class="btn_s btn_t_download uploadBtn" onclick="userReportDownload(1,${i})">다운로드</button>                                           
	                            </div>
	                            <div class="align_r">
	                                <span class="download_info align_l">파일 형식 선택</span>
	                                <button class="btn btn_s" type="button" name="excelBtn" onclick="report_excel(1,${i })"><img src="${rootPath}/resources/image/reportmanage/btn-xlsx.gif" alt="엑셀다운로드" title="엑셀다운로드" /></button>
	                                <button class="btn btn_s" type="button" onclick="report_pdf(1,${i })"><img src="${rootPath}/resources/image/reportmanage/btn-pdf.gif" alt="PDF 다운로드" title="PDF 다운로드" /></button>
	                                <button class="btn btn_s" type="button" onclick="report_word(1,${i })"><img src="${rootPath}/resources/image/reportmanage/btn-doc.gif" alt="워드 다운로드" title="워드 다운로드" /></button>
	                            </div>
	                        </div>  <!-- e:option-bar -->
                        </li>  
                    </c:forEach>
                    <!-- 입력 데이터 없음. 보고서 생성 가능 -->
                    <!-- <li class="nodata">
                    	<div class="list-bar">
                            <div class="data">
                                <div class="align_l">
                                    <h4>2020년 1월 점검보고서</h4>
                                    <span>ㆍ마지막 업데이트 2020.02.01</span>
                                </div>
                                <div class="align_r">
                                    
                                </div>
                            </div>
                        </div>
                    </li> --> 
                    <%-- <li class="">
                    	<div class="list-bar">
                            <div class="data">
                                <div class="align_l">
                                    <h4>2020년 6월 점검보고서</h4>
                                    <span>ㆍ마지막 업데이트 2020.02.01</span>
                                </div>
                                <div class="align_r">
                                    <button class="btn btn btn_redo" type="button">초기화</button>
                                    <button class="btn btn_m btn_gray" type="button">미리보기</button>
                                    <button class="btn btn_m btn_blue" type="button">다운로드</button>
                                </div>
                            </div>
                        </div>
                        <div class="option-bar">
                            <div class="align_l">
                                <span class="inpChk">
                                    <label for="ch1" style="cursor:pointer"><input type="checkbox" id="ch1" checked="checked" /> 차트포함</label>
                                </span>                                
                                <span class="inpChk">
                                    <label for="ch2" style="cursor:pointer"><input type="checkbox" id="ch2" checked="checked" /> 총평포함</label>
                                </span>
                                <span class="inpChk">
                                    <button class="btn btn_s" type="button"><img src="${rootPath}/resources/image/reportmanage/btn-edit.gif" alt="총평 수정하기" title="총평 수정하기" /></button>	
                                </span>
                                <span class="inpChk fileBox">
                                    <label for="uploadBtn" class="btn_s btn_file uploadBtn">파일 업로드</label>
                                    <input type="text" class="fileName" readonly="readonly">
                                    <input type="file" class="uploadBtn">
                                </span>                                    
                            </div>
                            <div class="align_r">
                                <span class="download_info align_l">파일 형식 선택</span>
                                <button class="btn btn_s" type="button"><img src="${rootPath}/resources/image/reportmanage/btn-xlsx.gif" alt="엑셀다운로드" title="엑셀다운로드" /></button>
                                <button class="btn btn_s" type="button"><img src="${rootPath}/resources/image/reportmanage/btn-pdf.gif" alt="PDF 다운로드" title="PDF 다운로드" /></button>
                                <button class="btn btn_s" type="button"><img src="${rootPath}/resources/image/reportmanage/btn-doc.gif" alt="워드 다운로드" title="워드 다운로드" /></button>
                            </div>
                        </div>  <!-- e:option-bar -->
                    </li> --%>   
                   
                    <!-- 입력 데이터 없음. 보고서 생성 불가능. 클릭됨+메시지 얼럿 띄울경우-->
                    <!-- <li class="nodata">
                    	<div class="list-bar">
                            <div class="data">
                                <div class="align_l">
                                    <h4>2020년 9월 점검보고서</h4>
                                    <span>ㆍ마지막 업데이트 2020.02.01</span>
                                </div>
                                <div class="align_r">
                                    <button class="btn btn_l btn_line_disable" type="button" onclick="alert('점검 가능한 기간이 아닙니다. 보고서를 생성할 수 없습니다.')">+ 보고서 생성</button>
                                </div>
                            </div>
                        </div>
                    </li> -->
                    <!-- 입력 데이터 없음. 보고서 생성 불가능. 버튼 클릭안되게 할 경우-->
                    <!-- <li class="nodata">
                    	<div class="list-bar">
                            <div class="data">
                                <div class="align_l">
                                    <h4>2020년 12월 점검보고서</h4>
                                    <span>ㆍ마지막 업데이트 2020.02.01</span>
                                </div>
                                <div class="align_r">
                                    <button class="btn btn_l btn_line_disable" type="button" disabled="disabled">+ 보고서 생성</button>
                                </div>
                            </div>
                        </div>
                    </li> --> 
                </ul>        
            </div>  <!-- e:report --> 
        </div> <!-- e:contents -->
    </div> <!-- e:container -->
</div><!-- e:wrapper -->

<div class="popup_wrap" id="reportSysPopup" style="display: none">
	<div class="popup">
        <div class="popup_header">
            <h1 class="align_l">보고서 생성 대상 시스템</h1>
            <a class="btn_close align_r" onclick="popupClose()" title="창닫기"></a>
        </div> <!-- e:popup_header -->
        <div class="popup_contents">
            <p class="popup_comment">보고서 생성 대상을 추가 혹은 제외하여 보고서를 생성 할 수 있습니다.</p>
            <div class="table-box-wrap">
                <div class="table-box">
                    <table>
                        <caption>보고서 생성 대상 시스템 리스트</caption>
                        <colgroup>
                            <col width="15%">
                            <col width="15%">
                            <col width="70%">
                        </colgroup>
                        <thead>
                            <tr>
                            	<th scope="col" width="15%"><input type="checkbox" id="sysAllCheck" onclick="AllCheck()"/></th>
                                <th scope="col" width="15%">CODE</th>
                                <th scope="col" width="70%">시스템명</th>
                            </tr>
                        </thead>
                
                        <tbody>
                        	<c:choose>
								<c:when test="${empty sysAuthIds}">
									<tr>
										<td colspan="3" align="center">데이터가 없습니다.</td>
									</tr>
								</c:when>
								<c:otherwise>
									<c:forEach items="${sysAuthIds}" var="sysCode" varStatus="status">
										<tr>
											<td><input type="checkbox" name="reportCheck${status.index}" class="sysCheck" id="sysCheck_${sysCode}" value="${sysCode}" onclick="checkAllCheck();" checked /></td>
											<td class="code_num"><ctl:nullCv nullCheck="${sysCode}"/></td>
											<td class="system_name"><ctl:nullCv nullCheck="${systemMap[sysCode]}"/></td>
										</tr>
									</c:forEach>
								</c:otherwise>
							</c:choose>
                        </tbody>
                    </table>
                </div> <!-- e:table-box -->
            </div> <!-- e:table-box-wrap -->
            <div class="popup_btn align_r">
                <button class="btn btn_m btn_gray" type="button" onclick="popupCancle()">취소</button>
                <button class="btn btn_m btn_blue" type="button" onclick="popupComplete()">완료</button>
            </div> 
        </div>  <!-- e:popup_contents -->
    </div>  <!-- e:popup -->
</div> <!-- e:popup_wrap -->

<div class="popup_wrap" id="descriptPopup" style="display: none">
	<div class="popup">
        <div class="popup_header">
            <h1 class="align_l">총평 수정</h1>
            <a class="btn_close align_r" onclick="popupClose()" title="창닫기"></a>
        </div> <!-- e:popup_header -->
        <div class="popup_contents">
            <p class="popup_comment">보고서 마지막에 입력될 총평 내용을 수정합니다.</p>
            <div class="table-box-wrap">
                <div class="table-box">
                   	<textarea id="description" rows="17" cols="75" style="resize: none;"></textarea>
                </div> <!-- e:table-box -->
            </div> <!-- e:table-box-wrap -->
            <div class="popup_btn align_r">
            	<input type="hidden" id="proc_month"/>
            	<input type="hidden" id="period_type"/>
                <button class="btn btn_m btn_gray" type="button" onclick="descReset()" title="총평 기본 문구로 되돌립니다" >초기화</button>
                <button class="btn btn_m btn_gray" type="button" onclick="popupClose()" title="수정사항을 적용하지 않고 창을 닫습니다" >취소</button>
                <button class="btn btn_m btn_blue" type="button" onclick="descComplete()" title="수정사항을 적용합니다" >완료</button>
            </div> 
        </div>  <!-- e:popup_contents -->
    </div>  <!-- e:popup -->
</div> <!-- e:popup_wrap -->

<c:if test="${empty install_date}">
	<!-- 솔루션 설치일이 code에 등록되어 있지 않은경우 나타남 -->
	<div id = "install_date_modal" class="modal" style="width: 100%; position: fixed; display: flex; align-items: center;">
		<div style="width:310px; height:115px; border: 1px solid black; position: relative; z-index: 11; margin: 0 auto; background: white;box-shadow: rgba(0,0,0,0.6) 0 0 0 99999px">
			<div class="portlet-title" style="background-color: #2B3643;padding:2px">
		        <div class="caption" style="z-index: 12; color: white">
		       	 <i class="fa fa-search"></i>솔루션 설치일 등록</div>
	        </div>
	        <div style="padding:5px">
		        <div class="input-medium date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd" style="margin:0 auto; padding: 5px;display: inline-block">
					<input type="text" class="form-control" id="install_date" name="install_date" value="<fmt:formatDate value="${now}" pattern="yyyy-MM-dd" />" style="display: inline-block;" readonly="readonly">
				</div>
				<button type="button"
					class="btn btn-sm blue btn-outline sbold uppercase"
					onclick="saveInstallDate()" style="margin-top:6px;">
					등록
				</button>
			    <div style="font-size: 12px; color: red">※이 창은 최초 설치 시 나타나는 창입니다</div>
			    <div style="font-size: 12px; color: red">해당 일자의 다음달 부터 보고서를 생성할 수 있습니다.</div>
		    </div>
		</div>
	</div>
</c:if>
<form id="listForm" method="post" style="display: none">
		<input type="hidden" name="report_seq" />
</form>
<form id="excelForm" method="post" style="display: none">
	<input type="hidden" name="start_date" />
	<input type="hidden" name="end_date" />
	<input type="hidden" name="period_type" />
	
	<input type="hidden" name="quarter_type" />
	<input type="hidden" name="title" />
	<input type="hidden" name="system_seq" />
</form>
<!-- <div id="includeReport"></div>보고서 생성 스크립트가 들어가는 페이지 -->
<iframe id="includeReport"></iframe>
<!--
	개발작업하시면서 스크립트 수정하셔도 됩니다.
    .option-bar > .fileBox 파일업로드 > 파일명 불러오는영역 확인해주세요. 
-->
<script type="text/javascript">
	
$('.btn_f_open').on('click', function(){
  var number = $(this).data("number");
  $(".active_target_" + number).toggleClass("active"); 
  return false;
});


var uploadFile = $('.fileBox .uploadBtn');
uploadFile.on('change', function(){
    if(window.FileReader){
        var filename = $(this)[0].files[0].name;
    } else {
        var filename = $(this).val().split('/').pop().split('\\').pop();
    }

    $(this).siblings('.fileName').val(filename);
});
var install_date = '<ctl:code value="INSTALL_DATE" groupId="SOLUTION_MASTER"/>';
var install_month = null;
var report_title = {
		type1 : "${reportMap['1']}",
		type2 : "${reportMap['2']}",
		type3 : "${reportMap['3']}",
		type4 : "${reportMap['4']}",
		type5 : "${reportMap['5']}",
		type9 : "${reportMap['9']}",
		type10 : "${reportMap['10']}",
		type12 : "${reportMap['12']}",
		type13 : "${reportMap['13']}",
		calltitle : function(type){
			report_type = type;
			return this["type"+type];
		}
}
</script>
</body>