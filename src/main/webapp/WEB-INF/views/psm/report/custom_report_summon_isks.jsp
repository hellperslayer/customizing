
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<script src="${rootPath}/resources/js/jquery/jquery.min.js"
	type="text/javascript" charset="UTF-8"></script>

<script src="${rootPath}/resources/js/amchart/amcharts.js"></script>
<script src="${rootPath}/resources/js/amchart/serial.js"></script>
<script src="${rootPath}/resources/js/amchart/export.min.js"></script>
<script src="${rootPath}/resources/js/amchart/amstock.js"></script>
<script src="${rootPath}/resources/js/amchart/xy.js"></script>
<script src="${rootPath}/resources/js/amchart/radar.js"></script>
<script src="${rootPath}/resources/js/amchart/light.js"></script>
<script src="${rootPath}/resources/js/amchart/pie.js"></script>
<script src="${rootPath}/resources/js/amchart/gantt.js"></script>
<script src="${rootPath}/resources/js/amchart/gauge.js"></script>

<%-- <link rel="stylesheet" href="${rootPath}/resources/css/export.css" type="text/css" media="all"> --%>
<link rel="stylesheet"
	href="${pageContext.request.scheme }://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/css/export.css"
	type="text/css" media="all">
<script src="${rootPath}/resources/js/psm/report/report_summoncharts.js"></script>
<%-- <script src="${rootPath}/resources/js/psm/report/bluebird.min.js"></script> --%>
<%-- <script src="${rootPath}/resources/js/psm/report/html2canvas.min.js"></script> --%>
<%-- <script src="${rootPath}/resources/js/psm/report/jspdf.min.js"></script> --%>

<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 15">
<meta name=Originator content="Microsoft Word 15">
<!-- <link rel=File-List href="소명관리%20Report_F.files/filelist.xml">
<link rel=Edit-Time-Data href="소명관리%20Report_F.files/editdata.mso">
<link rel=OLE-Object-Data href="소명관리%20Report_F.files/oledata.mso"> -->
<META NAME="Generator" CONTENT="Hancom HWP 8.5.8.1541">
<link rel=dataStoreItem href="소명관리%20Report_F.files/item0001.xml"
	target="소명관리%20Report_F.files/props002.xml">
<link rel=themeData href="소명관리%20Report_F.files/themedata.thmx">
<link rel=colorSchemeMapping
	href="소명관리%20Report_F.files/colorschememapping.xml">


<script type="text/javascript">

function printpr()
{
	$("#printButton").hide();

	window.print();
    
    self.close();
}

	rootPath = '${rootPath}';
	contextPath = '${pageContext.servletContext.contextPath}';
	var search_from = '${search_from}';
	var search_to = '${search_to}';
</script>

<style>
<!-- /* Font Definitions */
@font-face {
	font-family: Wingdings;
	panose-1: 5 0 0 0 0 0 0 0 0 0;
	mso-font-charset: 2;
	mso-generic-font-family: auto;
	mso-font-pitch: variable;
	mso-font-signature: 0 268435456 0 0 -2147483648 0;
}

@font-face {
	font-family: "Cambria Math";
	panose-1: 2 4 5 3 5 4 6 3 2 4;
	mso-font-charset: 1;
	mso-generic-font-family: roman;
	mso-font-pitch: variable;
	mso-font-signature: 0 0 0 0 0 0;
}

@font-face {
	font-family: "맑은 고딕";
	panose-1: 2 11 5 3 2 0 0 2 0 4;
	mso-font-charset: 129;
	mso-generic-font-family: modern;
	mso-font-pitch: variable;
	mso-font-signature: -1879047505 165117179 18 0 524289 0;
}

@font-face {
	font-family: ¹\00D9\00C5\00C1;
	panose-1: 0 0 0 0 0 0 0 0 0 0;
	mso-font-alt: "Times New Roman";
	mso-font-charset: 0;
	mso-generic-font-family: auto;
	mso-font-format: other;
	mso-font-pitch: auto;
	mso-font-signature: 3 0 0 0 1 0;
}

@font-face {
	font-family: "\@맑은 고딕";
	mso-font-charset: 129;
	mso-generic-font-family: modern;
	mso-font-pitch: variable;
	mso-font-signature: -1879047505 165117179 18 0 524289 0;
}
/* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal {
	mso-style-unhide: no;
	mso-style-qformat: yes;
	mso-style-parent: "";
	margin-top: 0cm;
	margin-right: 0cm;
	margin-bottom: 10.0pt;
	margin-left: 0cm;
	text-align: justify;
	text-justify: inter-ideograph;
	line-height: 115%;
	mso-pagination: none;
	text-autospace: none;
	word-break: break-hangul;
	font-size: 10.0pt;
	mso-bidi-font-size: 11.0pt;
	font-family: "맑은 고딕";
	mso-ascii-font-family: "맑은 고딕";
	mso-ascii-theme-font: minor-latin;
	mso-fareast-font-family: "맑은 고딕";
	mso-fareast-theme-font: minor-fareast;
	mso-hansi-font-family: "맑은 고딕";
	mso-hansi-theme-font: minor-latin;
	mso-bidi-font-family: "Times New Roman";
	mso-bidi-theme-font: minor-bidi;
	mso-font-kerning: 1.0pt;
}

p.MsoFootnoteText, li.MsoFootnoteText, div.MsoFootnoteText {
	mso-style-noshow: yes;
	mso-style-priority: 99;
	mso-style-link: "각주 텍스트 Char";
	margin-top: 0cm;
	margin-right: 0cm;
	margin-bottom: 10.0pt;
	margin-left: 0cm;
	line-height: 115%;
	mso-pagination: none;
	layout-grid-mode: char;
	text-autospace: none;
	word-break: break-hangul;
	font-size: 10.0pt;
	mso-bidi-font-size: 11.0pt;
	font-family: "맑은 고딕";
	mso-ascii-font-family: "맑은 고딕";
	mso-ascii-theme-font: minor-latin;
	mso-fareast-font-family: "맑은 고딕";
	mso-fareast-theme-font: minor-fareast;
	mso-hansi-font-family: "맑은 고딕";
	mso-hansi-theme-font: minor-latin;
	mso-bidi-font-family: "Times New Roman";
	mso-bidi-theme-font: minor-bidi;
	mso-font-kerning: 1.0pt;
}

p.MsoHeader, li.MsoHeader, div.MsoHeader {
	mso-style-priority: 99;
	mso-style-link: "머리글 Char";
	margin-top: 0cm;
	margin-right: 0cm;
	margin-bottom: 10.0pt;
	margin-left: 0cm;
	text-align: justify;
	text-justify: inter-ideograph;
	line-height: 115%;
	mso-pagination: none;
	tab-stops: center 225.65pt right 451.3pt;
	layout-grid-mode: char;
	text-autospace: none;
	word-break: break-hangul;
	font-size: 10.0pt;
	mso-bidi-font-size: 11.0pt;
	font-family: "맑은 고딕";
	mso-ascii-font-family: "맑은 고딕";
	mso-ascii-theme-font: minor-latin;
	mso-fareast-font-family: "맑은 고딕";
	mso-fareast-theme-font: minor-fareast;
	mso-hansi-font-family: "맑은 고딕";
	mso-hansi-theme-font: minor-latin;
	mso-bidi-font-family: "Times New Roman";
	mso-bidi-theme-font: minor-bidi;
	mso-font-kerning: 1.0pt;
}

p.MsoFooter, li.MsoFooter, div.MsoFooter {
	mso-style-priority: 99;
	mso-style-link: "바닥글 Char";
	margin-top: 0cm;
	margin-right: 0cm;
	margin-bottom: 10.0pt;
	margin-left: 0cm;
	text-align: justify;
	text-justify: inter-ideograph;
	line-height: 115%;
	mso-pagination: none;
	tab-stops: center 225.65pt right 451.3pt;
	layout-grid-mode: char;
	text-autospace: none;
	word-break: break-hangul;
	font-size: 10.0pt;
	mso-bidi-font-size: 11.0pt;
	font-family: "맑은 고딕";
	mso-ascii-font-family: "맑은 고딕";
	mso-ascii-theme-font: minor-latin;
	mso-fareast-font-family: "맑은 고딕";
	mso-fareast-theme-font: minor-fareast;
	mso-hansi-font-family: "맑은 고딕";
	mso-hansi-theme-font: minor-latin;
	mso-bidi-font-family: "Times New Roman";
	mso-bidi-theme-font: minor-bidi;
	mso-font-kerning: 1.0pt;
}

span.MsoFootnoteReference {
	mso-style-noshow: yes;
	mso-style-priority: 99;
	vertical-align: super;
}

p.MsoAcetate, li.MsoAcetate, div.MsoAcetate {
	mso-style-noshow: yes;
	mso-style-priority: 99;
	mso-style-link: "풍선 도움말 텍스트 Char";
	margin: 0cm;
	margin-bottom: .0001pt;
	text-align: justify;
	text-justify: inter-ideograph;
	mso-pagination: none;
	text-autospace: none;
	word-break: break-hangul;
	font-size: 9.0pt;
	font-family: "맑은 고딕";
	mso-ascii-font-family: "맑은 고딕";
	mso-ascii-theme-font: major-latin;
	mso-fareast-font-family: "맑은 고딕";
	mso-fareast-theme-font: major-fareast;
	mso-hansi-font-family: "맑은 고딕";
	mso-hansi-theme-font: major-latin;
	mso-bidi-font-family: "Times New Roman";
	mso-bidi-theme-font: major-bidi;
	mso-font-kerning: 1.0pt;
}

p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph {
	mso-style-priority: 34;
	mso-style-unhide: no;
	mso-style-qformat: yes;
	margin-top: 0cm;
	margin-right: 0cm;
	margin-bottom: 10.0pt;
	margin-left: 42.55pt;
	text-align: justify;
	text-justify: inter-ideograph;
	line-height: 115%;
	mso-pagination: none;
	text-autospace: none;
	word-break: break-hangul;
	font-size: 10.0pt;
	mso-bidi-font-size: 11.0pt;
	font-family: "맑은 고딕";
	mso-ascii-font-family: "맑은 고딕";
	mso-ascii-theme-font: minor-latin;
	mso-fareast-font-family: "맑은 고딕";
	mso-fareast-theme-font: minor-fareast;
	mso-hansi-font-family: "맑은 고딕";
	mso-hansi-theme-font: minor-latin;
	mso-bidi-font-family: "Times New Roman";
	mso-bidi-theme-font: minor-bidi;
	mso-font-kerning: 1.0pt;
}

span.Char {
	mso-style-name: "풍선 도움말 텍스트 Char";
	mso-style-noshow: yes;
	mso-style-priority: 99;
	mso-style-unhide: no;
	mso-style-locked: yes;
	mso-style-link: "풍선 도움말 텍스트";
	mso-ansi-font-size: 9.0pt;
	mso-bidi-font-size: 9.0pt;
	font-family: "맑은 고딕";
	mso-ascii-font-family: "맑은 고딕";
	mso-ascii-theme-font: major-latin;
	mso-fareast-font-family: "맑은 고딕";
	mso-fareast-theme-font: major-fareast;
	mso-hansi-font-family: "맑은 고딕";
	mso-hansi-theme-font: major-latin;
	mso-bidi-font-family: "Times New Roman";
	mso-bidi-theme-font: major-bidi;
}

span.Char0 {
	mso-style-name: "머리글 Char";
	mso-style-priority: 99;
	mso-style-unhide: no;
	mso-style-locked: yes;
	mso-style-link: 머리글;
}

span.Char1 {
	mso-style-name: "바닥글 Char";
	mso-style-priority: 99;
	mso-style-unhide: no;
	mso-style-locked: yes;
	mso-style-link: 바닥글;
}

p.s0, li.s0, div.s0 {
	mso-style-name: s0;
	mso-style-unhide: no;
	mso-style-parent: "";
	margin: 0cm;
	margin-bottom: .0001pt;
	mso-pagination: none;
	mso-layout-grid-align: none;
	text-autospace: none;
	font-size: 12.0pt;
	font-family: ¹\00D9\00C5\00C1;
	mso-fareast-font-family: "맑은 고딕";
	mso-bidi-font-family: "Times New Roman";
}

span.DeltaViewInsertion {
	mso-style-name: "DeltaView Insertion";
	mso-style-priority: 99;
	mso-style-unhide: no;
	mso-style-parent: "";
	color: blue;
	text-decoration: underline;
	text-underline: double;
}

span.DeltaViewDeletion {
	mso-style-name: "DeltaView Deletion";
	mso-style-priority: 99;
	mso-style-unhide: no;
	mso-style-parent: "";
	color: red;
	text-decoration: line-through;
}

span.Char2 {
	mso-style-name: "각주 텍스트 Char";
	mso-style-noshow: yes;
	mso-style-priority: 99;
	mso-style-unhide: no;
	mso-style-locked: yes;
	mso-style-link: "각주 텍스트";
}

p.L2Normal, li.L2Normal, div.L2Normal {
	mso-style-name: "L2 Normal";
	mso-style-unhide: no;
	mso-style-qformat: yes;
	mso-style-parent: "목록 단락";
	margin-top: 0cm;
	margin-right: 0cm;
	margin-bottom: 6.0pt;
	margin-left: 35.4pt;
	mso-para-margin-top: 0cm;
	mso-para-margin-right: 0cm;
	mso-para-margin-bottom: 6.0pt;
	mso-para-margin-left: 3.54gd;
	mso-pagination: none;
	text-autospace: none;
	word-break: break-hangul;
	font-size: 10.0pt;
	mso-bidi-font-size: 11.0pt;
	font-family: "맑은 고딕";
	mso-ascii-font-family: "맑은 고딕";
	mso-ascii-theme-font: minor-latin;
	mso-fareast-font-family: "맑은 고딕";
	mso-fareast-theme-font: minor-fareast;
	mso-hansi-font-family: "맑은 고딕";
	mso-hansi-theme-font: minor-latin;
	mso-bidi-font-family: "Times New Roman";
	mso-bidi-theme-font: minor-bidi;
	mso-font-kerning: 1.0pt;
}

.MsoChpDefault {
	mso-style-type: export-only;
	mso-default-props: yes;
	font-family: "맑은 고딕";
	mso-ascii-font-family: "맑은 고딕";
	mso-ascii-theme-font: minor-latin;
	mso-bidi-font-family: "Times New Roman";
	mso-bidi-theme-font: minor-bidi;
}

.MsoPapDefault {
	mso-style-type: export-only;
	margin-bottom: 10.0pt;
	text-align: justify;
	text-justify: inter-ideograph;
	line-height: 115%;
}
/* Page Definitions */
@page {
	mso-page-border-surround-header: no;
	mso-page-border-surround-footer: no;
	mso-footnote-separator: url("소명관리%20Report_F.files/header.htm") fs;
	mso-footnote-continuation-separator:
		url("소명관리%20Report_F.files/header.htm") fcs;
	mso-endnote-separator: url("소명관리%20Report_F.files/header.htm") es;
	mso-endnote-continuation-separator:
		url("소명관리%20Report_F.files/header.htm") ecs;
}

@page WordSection1 {
	size: 595.3pt 841.9pt;
	margin: 36.0pt 36.0pt 36.0pt 36.0pt;
	mso-header-margin: 42.55pt;
	mso-footer-margin: 49.6pt;
	mso-footer: url("소명관리%20Report_F.files/header.htm") f1;
	mso-paper-source: 0;
}

div.WordSection1 {
	page: WordSection1;
}
/* List Definitions */
@
list l0 {
	mso-list-id: 16;
	mso-list-type: hybrid;
	mso-list-template-ids: 1116639482 189571442 67698713 67698715 67698703
		67698713 67698715 67698703 67698713 67698715;
}

@
list l0:level1 {
	mso-level-text: "\(%1\)";
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 32.2pt;
	text-indent: -18.0pt;
	mso-bidi-font-family: "Times New Roman";
}

@
list l0:level2 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 54.2pt;
	text-indent: -20.0pt;
	mso-bidi-font-family: "Times New Roman";
}

@
list l0:level3 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 74.2pt;
	text-indent: -20.0pt;
	mso-bidi-font-family: "Times New Roman";
}

@
list l0:level4 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 94.2pt;
	text-indent: -20.0pt;
	mso-bidi-font-family: "Times New Roman";
}

@
list l0:level5 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 114.2pt;
	text-indent: -20.0pt;
	mso-bidi-font-family: "Times New Roman";
}

@
list l0:level6 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 134.2pt;
	text-indent: -20.0pt;
	mso-bidi-font-family: "Times New Roman";
}

@
list l0:level7 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 154.2pt;
	text-indent: -20.0pt;
	mso-bidi-font-family: "Times New Roman";
}

@
list l0:level8 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 174.2pt;
	text-indent: -20.0pt;
	mso-bidi-font-family: "Times New Roman";
}

@
list l0:level9 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 194.2pt;
	text-indent: -20.0pt;
	mso-bidi-font-family: "Times New Roman";
}

@
list l1 {
	mso-list-id: 17;
	mso-list-type: hybrid;
	mso-list-template-ids: -25632718 -328271330 67698691 67698693 67698689
		67698691 67698693 67698689 67698691 67698693;
}

@
list l1:level1 {
	mso-level-number-format: bullet;
	mso-level-text: \2022;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 40.0pt;
	text-indent: -20.0pt;
	font-family: "Arial", sans-serif;
	mso-bidi-font-family: "Times New Roman";
}

@
list l1:level2 {
	mso-level-number-format: bullet;
	mso-level-text: \F06E;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 60.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l1:level3 {
	mso-level-number-format: bullet;
	mso-level-text: \F075;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 80.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l1:level4 {
	mso-level-number-format: bullet;
	mso-level-text: \F06C;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 100.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l1:level5 {
	mso-level-number-format: bullet;
	mso-level-text: \F06E;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 120.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l1:level6 {
	mso-level-number-format: bullet;
	mso-level-text: \F075;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 140.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l1:level7 {
	mso-level-number-format: bullet;
	mso-level-text: \F06C;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 160.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l1:level8 {
	mso-level-number-format: bullet;
	mso-level-text: \F06E;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 180.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l1:level9 {
	mso-level-number-format: bullet;
	mso-level-text: \F075;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 200.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l2 {
	mso-list-id: 19;
	mso-list-type: hybrid;
	mso-list-template-ids: 1116639482 189571442 67698713 67698715 67698703
		67698713 67698715 67698703 67698713 67698715;
}

@
list l2:level1 {
	mso-level-text: "\(%1\)";
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 32.2pt;
	text-indent: -18.0pt;
	mso-bidi-font-family: "Times New Roman";
}

@
list l2:level2 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 54.2pt;
	text-indent: -20.0pt;
	mso-bidi-font-family: "Times New Roman";
}

@
list l2:level3 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 74.2pt;
	text-indent: -20.0pt;
	mso-bidi-font-family: "Times New Roman";
}

@
list l2:level4 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 94.2pt;
	text-indent: -20.0pt;
	mso-bidi-font-family: "Times New Roman";
}

@
list l2:level5 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 114.2pt;
	text-indent: -20.0pt;
	mso-bidi-font-family: "Times New Roman";
}

@
list l2:level6 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 134.2pt;
	text-indent: -20.0pt;
	mso-bidi-font-family: "Times New Roman";
}

@
list l2:level7 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 154.2pt;
	text-indent: -20.0pt;
	mso-bidi-font-family: "Times New Roman";
}

@
list l2:level8 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 174.2pt;
	text-indent: -20.0pt;
	mso-bidi-font-family: "Times New Roman";
}

@
list l2:level9 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 194.2pt;
	text-indent: -20.0pt;
	mso-bidi-font-family: "Times New Roman";
}

@
list l3 {
	mso-list-id: 41440483;
	mso-list-type: hybrid;
	mso-list-template-ids: -1298904324 1489771454 67698713 67698715 67698703
		67698713 67698715 67698703 67698713 67698715;
}

@
list l3:level1 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 38.0pt;
	text-indent: -18.0pt;
}

@
list l3:level2 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 60.0pt;
	text-indent: -20.0pt;
}

@
list l3:level3 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 80.0pt;
	text-indent: -20.0pt;
}

@
list l3:level4 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 100.0pt;
	text-indent: -20.0pt;
}

@
list l3:level5 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 120.0pt;
	text-indent: -20.0pt;
}

@
list l3:level6 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 140.0pt;
	text-indent: -20.0pt;
}

@
list l3:level7 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 160.0pt;
	text-indent: -20.0pt;
}

@
list l3:level8 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 180.0pt;
	text-indent: -20.0pt;
}

@
list l3:level9 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 200.0pt;
	text-indent: -20.0pt;
}

@
list l4 {
	mso-list-id: 147333747;
	mso-list-type: hybrid;
	mso-list-template-ids: -1681257920 -362661742 67698713 67698715 67698703
		67698713 67698715 67698703 67698713 67698715;
}

@
list l4:level1 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 38.0pt;
	text-indent: -18.0pt;
}

@
list l4:level2 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 60.0pt;
	text-indent: -20.0pt;
}

@
list l4:level3 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 80.0pt;
	text-indent: -20.0pt;
}

@
list l4:level4 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 100.0pt;
	text-indent: -20.0pt;
}

@
list l4:level5 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 120.0pt;
	text-indent: -20.0pt;
}

@
list l4:level6 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 140.0pt;
	text-indent: -20.0pt;
}

@
list l4:level7 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 160.0pt;
	text-indent: -20.0pt;
}

@
list l4:level8 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 180.0pt;
	text-indent: -20.0pt;
}

@
list l4:level9 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 200.0pt;
	text-indent: -20.0pt;
}

@
list l5 {
	mso-list-id: 378747247;
	mso-list-type: hybrid;
	mso-list-template-ids: -1330342106 1352704682 1688885378 1352704682
		756572602 67698691 67698693 67698689 67698691 67698693;
}

@
list l5:level1 {
	mso-level-number-format: bullet;
	mso-level-text: -;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 34.2pt;
	text-indent: -20.0pt;
	font-family: "Arial", sans-serif;
	mso-bidi-font-family: "Times New Roman";
}

@
list l5:level2 {
	mso-level-number-format: decimal-full-width;
	mso-level-text: "\(%2\)";
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 40.0pt;
	text-indent: -20.0pt;
	mso-ansi-language: EN-US;
}

@
list l5:level3 {
	mso-level-number-format: bullet;
	mso-level-text: -;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 60.0pt;
	text-indent: -20.0pt;
	font-family: "Arial", sans-serif;
	mso-bidi-font-family: "Times New Roman";
}

@
list l5:level4 {
	mso-level-number-format: decimal-full-width;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 78.0pt;
	text-indent: -18.0pt;
}

@
list l5:level5 {
	mso-level-number-format: bullet;
	mso-level-text: \F06E;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 100.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l5:level6 {
	mso-level-number-format: bullet;
	mso-level-text: \F075;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 120.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l5:level7 {
	mso-level-number-format: bullet;
	mso-level-text: \F06C;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 140.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l5:level8 {
	mso-level-number-format: bullet;
	mso-level-text: \F06E;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 160.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l5:level9 {
	mso-level-number-format: bullet;
	mso-level-text: \F075;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 180.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l6 {
	mso-list-id: 401636795;
	mso-list-type: hybrid;
	mso-list-template-ids: 689195112 -1230741494 669538152 67698715 67698703
		67698713 67698715 67698703 67698713 67698715;
}

@
list l6:level1 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 38.0pt;
	text-indent: -18.0pt;
}

@
list l6:level2 {
	mso-level-number-format: decimal-enclosed-circle;
	mso-level-text: %2;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 58.0pt;
	text-indent: -18.0pt;
}

@
list l6:level3 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 80.0pt;
	text-indent: -20.0pt;
}

@
list l6:level4 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 100.0pt;
	text-indent: -20.0pt;
}

@
list l6:level5 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 120.0pt;
	text-indent: -20.0pt;
}

@
list l6:level6 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 140.0pt;
	text-indent: -20.0pt;
}

@
list l6:level7 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 160.0pt;
	text-indent: -20.0pt;
}

@
list l6:level8 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 180.0pt;
	text-indent: -20.0pt;
}

@
list l6:level9 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 200.0pt;
	text-indent: -20.0pt;
}

@
list l7 {
	mso-list-id: 504129606;
	mso-list-type: hybrid;
	mso-list-template-ids: -1174098022 812295632 67698713 67698715 67698703
		67698713 67698715 67698703 67698713 67698715;
}

@
list l7:level1 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 25.1pt;
	text-indent: -18.0pt;
}

@
list l7:level2 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 47.1pt;
	text-indent: -20.0pt;
}

@
list l7:level3 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 67.1pt;
	text-indent: -20.0pt;
}

@
list l7:level4 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 87.1pt;
	text-indent: -20.0pt;
}

@
list l7:level5 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 107.1pt;
	text-indent: -20.0pt;
}

@
list l7:level6 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 127.1pt;
	text-indent: -20.0pt;
}

@
list l7:level7 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 147.1pt;
	text-indent: -20.0pt;
}

@
list l7:level8 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 167.1pt;
	text-indent: -20.0pt;
}

@
list l7:level9 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 187.1pt;
	text-indent: -20.0pt;
}

@
list l8 {
	mso-list-id: 681519180;
	mso-list-type: hybrid;
	mso-list-template-ids: 1571169000 922618056 67698713 67698715 67698703
		67698713 67698715 67698703 67698713 67698715;
}

@
list l8:level1 {
	mso-level-text: "%1\)";
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 56.0pt;
	text-indent: -18.0pt;
}

@
list l8:level2 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 78.0pt;
	text-indent: -20.0pt;
}

@
list l8:level3 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 98.0pt;
	text-indent: -20.0pt;
}

@
list l8:level4 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 118.0pt;
	text-indent: -20.0pt;
}

@
list l8:level5 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 138.0pt;
	text-indent: -20.0pt;
}

@
list l8:level6 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 158.0pt;
	text-indent: -20.0pt;
}

@
list l8:level7 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 178.0pt;
	text-indent: -20.0pt;
}

@
list l8:level8 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 198.0pt;
	text-indent: -20.0pt;
}

@
list l8:level9 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 218.0pt;
	text-indent: -20.0pt;
}

@
list l9 {
	mso-list-id: 886331158;
	mso-list-type: hybrid;
	mso-list-template-ids: -300671652 -1423793852 67698713 67698715 67698703
		67698713 67698715 67698703 67698713 67698715;
}

@
list l9:level1 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 38.0pt;
	text-indent: -18.0pt;
}

@
list l9:level2 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 60.0pt;
	text-indent: -20.0pt;
}

@
list l9:level3 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 80.0pt;
	text-indent: -20.0pt;
}

@
list l9:level4 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 100.0pt;
	text-indent: -20.0pt;
}

@
list l9:level5 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 120.0pt;
	text-indent: -20.0pt;
}

@
list l9:level6 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 140.0pt;
	text-indent: -20.0pt;
}

@
list l9:level7 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 160.0pt;
	text-indent: -20.0pt;
}

@
list l9:level8 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 180.0pt;
	text-indent: -20.0pt;
}

@
list l9:level9 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 200.0pt;
	text-indent: -20.0pt;
}

@
list l10 {
	mso-list-id: 1038554751;
	mso-list-type: hybrid;
	mso-list-template-ids: -1666304302 1118572326 67698713 67698715 67698703
		67698713 67698715 67698703 67698713 67698715;
}

@
list l10:level1 {
	mso-level-text: "%1\)";
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 56.0pt;
	text-indent: -18.0pt;
}

@
list l10:level2 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 78.0pt;
	text-indent: -20.0pt;
}

@
list l10:level3 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 98.0pt;
	text-indent: -20.0pt;
}

@
list l10:level4 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 118.0pt;
	text-indent: -20.0pt;
}

@
list l10:level5 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 138.0pt;
	text-indent: -20.0pt;
}

@
list l10:level6 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 158.0pt;
	text-indent: -20.0pt;
}

@
list l10:level7 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 178.0pt;
	text-indent: -20.0pt;
}

@
list l10:level8 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 198.0pt;
	text-indent: -20.0pt;
}

@
list l10:level9 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 218.0pt;
	text-indent: -20.0pt;
}

@
list l11 {
	mso-list-id: 1059094147;
	mso-list-type: hybrid;
	mso-list-template-ids: -476278850 -545598776 67698691 67698693 67698689
		67698691 67698693 67698689 67698691 67698693;
}

@
list l11:level1 {
	mso-level-number-format: bullet;
	mso-level-text: -;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 74.0pt;
	text-indent: -18.0pt;
	font-family: "맑은 고딕";
	mso-bidi-font-family: "Times New Roman";
	mso-bidi-theme-font: minor-bidi;
}

@
list l11:level2 {
	mso-level-number-format: bullet;
	mso-level-text: \F06E;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 96.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l11:level3 {
	mso-level-number-format: bullet;
	mso-level-text: \F075;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 116.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l11:level4 {
	mso-level-number-format: bullet;
	mso-level-text: \F06C;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 136.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l11:level5 {
	mso-level-number-format: bullet;
	mso-level-text: \F06E;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 156.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l11:level6 {
	mso-level-number-format: bullet;
	mso-level-text: \F075;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 176.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l11:level7 {
	mso-level-number-format: bullet;
	mso-level-text: \F06C;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 196.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l11:level8 {
	mso-level-number-format: bullet;
	mso-level-text: \F06E;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 216.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l11:level9 {
	mso-level-number-format: bullet;
	mso-level-text: \F075;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 236.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l12 {
	mso-list-id: 1332903263;
	mso-list-type: hybrid;
	mso-list-template-ids: 1381374228 -1423793852 67698713 67698715 67698703
		67698713 67698715 67698703 67698713 67698715;
}

@
list l12:level1 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 38.0pt;
	text-indent: -18.0pt;
}

@
list l12:level2 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 60.0pt;
	text-indent: -20.0pt;
}

@
list l12:level3 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 80.0pt;
	text-indent: -20.0pt;
}

@
list l12:level4 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 100.0pt;
	text-indent: -20.0pt;
}

@
list l12:level5 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 120.0pt;
	text-indent: -20.0pt;
}

@
list l12:level6 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 140.0pt;
	text-indent: -20.0pt;
}

@
list l12:level7 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 160.0pt;
	text-indent: -20.0pt;
}

@
list l12:level8 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 180.0pt;
	text-indent: -20.0pt;
}

@
list l12:level9 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 200.0pt;
	text-indent: -20.0pt;
}

@
list l13 {
	mso-list-id: 1347826734;
	mso-list-type: hybrid;
	mso-list-template-ids: -315950136 -472598110 67698691 67698693 67698689
		67698691 67698693 67698689 67698691 67698693;
}

@
list l13:level1 {
	mso-level-start-at: 2;
	mso-level-number-format: bullet;
	mso-level-text: ◎;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 56.0pt;
	text-indent: -18.0pt;
	font-family: "맑은 고딕";
	mso-bidi-font-family: "Times New Roman";
	mso-bidi-theme-font: minor-bidi;
}

@
list l13:level2 {
	mso-level-number-format: bullet;
	mso-level-text: \F06E;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 78.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l13:level3 {
	mso-level-number-format: bullet;
	mso-level-text: \F075;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 98.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l13:level4 {
	mso-level-number-format: bullet;
	mso-level-text: \F06C;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 118.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l13:level5 {
	mso-level-number-format: bullet;
	mso-level-text: \F06E;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 138.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l13:level6 {
	mso-level-number-format: bullet;
	mso-level-text: \F075;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 158.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l13:level7 {
	mso-level-number-format: bullet;
	mso-level-text: \F06C;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 178.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l13:level8 {
	mso-level-number-format: bullet;
	mso-level-text: \F06E;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 198.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l13:level9 {
	mso-level-number-format: bullet;
	mso-level-text: \F075;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 218.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l14 {
	mso-list-id: 1368291986;
	mso-list-type: hybrid;
	mso-list-template-ids: -25632718 -328271330 67698691 67698693 67698689
		67698691 67698693 67698689 67698691 67698693;
}

@
list l14:level1 {
	mso-level-number-format: bullet;
	mso-level-text: \2022;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 40.0pt;
	text-indent: -20.0pt;
	font-family: "Arial", sans-serif;
	mso-bidi-font-family: "Times New Roman";
}

@
list l14:level2 {
	mso-level-number-format: bullet;
	mso-level-text: \F06E;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 60.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l14:level3 {
	mso-level-number-format: bullet;
	mso-level-text: \F075;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 80.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l14:level4 {
	mso-level-number-format: bullet;
	mso-level-text: \F06C;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 100.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l14:level5 {
	mso-level-number-format: bullet;
	mso-level-text: \F06E;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 120.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l14:level6 {
	mso-level-number-format: bullet;
	mso-level-text: \F075;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 140.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l14:level7 {
	mso-level-number-format: bullet;
	mso-level-text: \F06C;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 160.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l14:level8 {
	mso-level-number-format: bullet;
	mso-level-text: \F06E;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 180.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l14:level9 {
	mso-level-number-format: bullet;
	mso-level-text: \F075;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 200.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l15 {
	mso-list-id: 1391228301;
	mso-list-type: hybrid;
	mso-list-template-ids: 155514858 67698703 67698713 67698715 67698703
		67698713 67698715 67698703 67698713 67698715;
}

@
list l15:level1 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 20.0pt;
	text-indent: -20.0pt;
}

@
list l15:level2 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 40.0pt;
	text-indent: -20.0pt;
}

@
list l15:level3 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 60.0pt;
	text-indent: -20.0pt;
}

@
list l15:level4 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 80.0pt;
	text-indent: -20.0pt;
}

@
list l15:level5 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 100.0pt;
	text-indent: -20.0pt;
}

@
list l15:level6 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 120.0pt;
	text-indent: -20.0pt;
}

@
list l15:level7 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 140.0pt;
	text-indent: -20.0pt;
}

@
list l15:level8 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 160.0pt;
	text-indent: -20.0pt;
}

@
list l15:level9 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 180.0pt;
	text-indent: -20.0pt;
}

@
list l16 {
	mso-list-id: 1432046403;
	mso-list-type: hybrid;
	mso-list-template-ids: 511057406 1521137162 67698713 67698715 67698703
		67698713 67698715 67698703 67698713 67698715;
}

@
list l16:level1 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 38.25pt;
	text-indent: -18.0pt;
}

@
list l16:level2 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 60.25pt;
	text-indent: -20.0pt;
}

@
list l16:level3 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 80.25pt;
	text-indent: -20.0pt;
}

@
list l16:level4 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 100.25pt;
	text-indent: -20.0pt;
}

@
list l16:level5 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 120.25pt;
	text-indent: -20.0pt;
}

@
list l16:level6 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 140.25pt;
	text-indent: -20.0pt;
}

@
list l16:level7 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 160.25pt;
	text-indent: -20.0pt;
}

@
list l16:level8 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 180.25pt;
	text-indent: -20.0pt;
}

@
list l16:level9 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 200.25pt;
	text-indent: -20.0pt;
}

@
list l17 {
	mso-list-id: 1990287041;
	mso-list-type: hybrid;
	mso-list-template-ids: 1781064784 -1175403622 67698713 67698715 67698703
		67698713 67698715 67698703 67698713 67698715;
}

@
list l17:level1 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 38.0pt;
	text-indent: -18.0pt;
}

@
list l17:level2 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 60.0pt;
	text-indent: -20.0pt;
}

@
list l17:level3 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 80.0pt;
	text-indent: -20.0pt;
}

@
list l17:level4 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 100.0pt;
	text-indent: -20.0pt;
}

@
list l17:level5 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 120.0pt;
	text-indent: -20.0pt;
}

@
list l17:level6 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 140.0pt;
	text-indent: -20.0pt;
}

@
list l17:level7 {
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 160.0pt;
	text-indent: -20.0pt;
}

@
list l17:level8 {
	mso-level-number-format: alpha-upper;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 180.0pt;
	text-indent: -20.0pt;
}

@
list l17:level9 {
	mso-level-number-format: roman-lower;
	mso-level-tab-stop: none;
	mso-level-number-position: right;
	margin-left: 200.0pt;
	text-indent: -20.0pt;
}

@
list l18 {
	mso-list-id: 2131852806;
	mso-list-type: hybrid;
	mso-list-template-ids: 708232122 -1763041610 67698691 67698693 67698689
		67698691 67698693 67698689 67698691 67698693;
}

@
list l18:level1 {
	mso-level-number-format: bullet;
	mso-level-text: \F0B7;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 40.0pt;
	text-indent: -20.0pt;
	mso-ansi-font-size: 11.0pt;
	font-family: Symbol;
	color: windowtext;
}

@
list l18:level2 {
	mso-level-number-format: bullet;
	mso-level-text: \F06E;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 60.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l18:level3 {
	mso-level-number-format: bullet;
	mso-level-text: \F075;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 80.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l18:level4 {
	mso-level-number-format: bullet;
	mso-level-text: \F06C;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 100.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l18:level5 {
	mso-level-number-format: bullet;
	mso-level-text: \F06E;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 120.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l18:level6 {
	mso-level-number-format: bullet;
	mso-level-text: \F075;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 140.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l18:level7 {
	mso-level-number-format: bullet;
	mso-level-text: \F06C;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 160.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l18:level8 {
	mso-level-number-format: bullet;
	mso-level-text: \F06E;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 180.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

@
list l18:level9 {
	mso-level-number-format: bullet;
	mso-level-text: \F075;
	mso-level-tab-stop: none;
	mso-level-number-position: left;
	margin-left: 200.0pt;
	text-indent: -20.0pt;
	font-family: Wingdings;
}

ol {
	margin-bottom: 0cm;
}

ul {
	margin-bottom: 0cm;
}
-->
</style>

</head>

<body lang=KO style='tab-interval: 42.55pt'>

	<div align="right">
		<input type=button id="printButton" name="printButton" onclick="printpr();" value="출력"/>
		<!-- <input type=button id="savePDFButton" name="savePDFButton" onclick="saveReport();" value="저장"/> -->
	</div>

	<div class=WordSection1>

		<p class=MsoNormal align=center
			style='margin-bottom: 0cm; margin-bottom: 20pt; text-align: center; line-height: normal; word-break: keep-all'>
			<b style='mso-bidi-font-weight: normal'><span
				style='font-size: 14.0pt; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial'>개인정보 처리 이력 점검 결과</span></b><b
				style='mso-bidi-font-weight: normal'><span lang=EN-US
				style='font-size: 14.0pt; font-family: "Arial", sans-serif'>
					<o:p></o:p>
			</span></b>
		</p>
		
		<p class='MsoListParagraph'
			style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0cm; margin-left: 38.0pt; margin-bottom: 2pt; text-indent: -18.0pt; line-height: normal; mso-list: l6 level1 lfo9; word-break: keep-all'>
			<b style='mso-bidi-font-weight: normal'><span
				style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>
					1.점검 기간 <span lang=EN-US>:</span>
			</span><span lang=EN-US>${search_from}</span> ~ <span lang=EN-US>${search_to}</span></b>
			</span>
		</p>

		<p class='MsoListParagraph'
			style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0cm; margin-left: 38.0pt; margin-bottom: 2pt; text-indent: -18.0pt; line-height: normal; mso-list: l6 level1 lfo9; word-break: keep-all'>
			<b style='mso-bidi-font-weight: normal'><span
				style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>2.점검
					대상 <span lang=EN-US>:</span>
			</span><span lang=EN-US
				style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>
			</span><span
				style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>개인정보처리
				시스템<span lang=EN-US><o:p></o:p></span>
			</span></b>
		</p>

		<p class='MsoListParagraph'
			style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0cm; margin-left: 38.0pt; margin-bottom: 2pt; text-indent: -18.0pt; line-height: normal; mso-list: l6 level1 lfo9; word-break: keep-all'>
			<![if !supportLists]>
			<span lang=EN-US
				style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast; mso-bidi-font-family: "맑은 고딕"; mso-bidi-theme-font: minor-fareast'><span
				style='mso-list: Ignore'>3.<span
					style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
				</span></span></span>
			<![endif]>
			<b style='mso-bidi-font-weight: normal'><span
				style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>점검 기준<span lang=EN-US></span>
			</span></b><span lang=EN-US
				style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>
			</span>
		</p>
		
		<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 
			style='  margin-left: 38.25pt;  border-collapse: collapse; border: none; mso-border-alt: solid windowtext .5pt; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt'>
			<tr style='mso-yfti-irow: 0; mso-yfti-firstrow: yes'>
				<td width=200 valign=top
					style='width: 350pt; border: solid windowtext 1.0pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
					<p class='MsoListParagraph' align=center
						style='margin: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
						<b><span
							style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>시나리오명<span lang=EN-US><o:p></o:p></span>
						</span></b>
					</p>
				</td>
				<td width=300 valign=top
					style='width: 500pt; border: solid windowtext 1.0pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
					<p class='MsoListParagraph' align=center
						style='margin: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
						<b><span
							style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>시나리오 설명<span
							lang=EN-US><o:p></o:p></span></span></b>
					</p>
				</td>
			</tr>
			<c:forEach items="${orgRuleInfoListIsks }" var="orgRuleInfo">
				<tr style='mso-yfti-irow: 0; mso-yfti-firstrow: yes'>
					<td width=200 valign=top
						style='width: 350pt; border: solid windowtext 1.0pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
						<p class='MsoListParagraph'
							style='margin: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
							<b><span
								style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>${orgRuleInfo.rule_nm }<span lang=EN-US><o:p></o:p></span>
							</span></b>
						</p>
					</td>
					<td width=300 valign=top
						style='width: 500pt; border: solid windowtext 1.0pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
						<p class='MsoListParagraph'
							style='margin: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
							<b><span
								style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>${orgRuleInfo.script_desc}<span
								lang=EN-US><o:p></o:p></span></span></b>
						</p>
					</td>
				</tr>
			</c:forEach>
		</table>
		
	<p class='MsoListParagraph'
			style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0cm; margin-left: 38.0pt; margin-bottom: 2pt; text-indent: -18.0pt; line-height: normal; mso-list: l6 level1 lfo9; word-break: keep-all'>
			<![if !supportLists]>
			<span lang=EN-US
				style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast; mso-bidi-font-family: "맑은 고딕"; mso-bidi-theme-font: minor-fareast'><span
				style='mso-list: Ignore'>4.<span
					style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
				</span></span></span>
			<![endif]>
			<b style='mso-bidi-font-weight: normal'><span
				style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>점검 결과<span lang=EN-US></span>
			</span></b><span lang=EN-US
				style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>
			</span>
		</p>
		
		<p class='MsoListParagraph'
			style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0cm; margin-left: 38.25pt; margin-bottom: 2pt; line-height: normal; word-break: keep-all'>
			<b style='mso-bidi-font-weight: normal'>
				<span lang=EN-US
					style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>
					- 소명 건수 : <span lang=EN-US style='color: blue'>총 <fmt:formatNumber value="${summonReqCnt }" pattern="#,###"/>건</span>
				</span>
				<br>
				<span lang=EN-US
					style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>
					- 소명 대상자 : <span lang=EN-US style='color: blue'>총 <fmt:formatNumber value="${totalExtrtCntIsks.totalCnt }" pattern="#,###"/>명(${totalExtrtCntIsks.cnt_sql })</span>
				</span>
				<br>
				<span lang=EN-US
					style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>
					- 진행현황 : <span lang=EN-US style='color: blue'>완료 : <fmt:formatNumber value="${totalSummonCntIsks.cnt1 }" pattern="#,###"/>건, 진행중 : <fmt:formatNumber value="${totalSummonCntIsks.cnt2 }" pattern="#,###"/>건</span>
				</span>
				<br>
				<span lang=EN-US
					style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>
					- 시스템 및 시나리오별 발생현황 
				</span>
			</b>
			<br>
		</p>
		
		<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
			style='  margin-left: 38.25pt; border-collapse: collapse; border: none; mso-border-alt: solid windowtext .5pt; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt'>
			<tr style='mso-yfti-irow: 0; mso-yfti-firstrow: yes'>
				<td width=200 valign=top
					style='width: 98.5pt; border: solid windowtext 1.0pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
					<p class='MsoListParagraph' align=center
						style='margin: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
						<b><span
							style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>시스템명<span lang=EN-US><o:p></o:p></span>
						</span></b>
					</p>
				</td>
				<c:forEach items="${orgRuleInfoListIsks }" var="orgRuleInfo2">
					<td width=300 valign=top
						style='width: 98.5pt; border: solid windowtext 1.0pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
						<p class='MsoListParagraph' align=center
							style='margin: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
							<b><span
								style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>${orgRuleInfo2.rule_nm }<span
								lang=EN-US><o:p></o:p></span></span></b>
						</p>
					</td>
				</c:forEach>
				
			</tr>
			<c:forEach items="${totalRuleSystemCntListIsks }" var="totalRuleSystemCnt">
				<tr style='mso-yfti-irow: 0; mso-yfti-firstrow: yes'>
					<td width=200 valign=top
						style='width: 98.5pt; border: solid windowtext 1.0pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
						<p class='MsoListParagraph' align=center
							style='margin: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
							<b><span
								style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>${totalRuleSystemCnt.system_name }<span lang=EN-US><o:p></o:p></span>
							</span></b>
						</p>
					</td>
					<td width=300 valign=top style='width: 98.5pt; border: solid windowtext 1.0pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
						<p class='MsoListParagraph' align=center style='margin: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
							<b><span style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>
							${totalRuleSystemCnt.type1}<span lang=EN-US><o:p></o:p></span></span></b>
						</p>
					</td>
					<td width=300 valign=top style='width: 98.5pt; border: solid windowtext 1.0pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
						<p class='MsoListParagraph' align=center style='margin: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
							<b><span style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>
							${totalRuleSystemCnt.type2}<span lang=EN-US><o:p></o:p></span></span></b>
						</p>
					</td>
					<td width=300 valign=top style='width: 98.5pt; border: solid windowtext 1.0pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
						<p class='MsoListParagraph' align=center style='margin: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
							<b><span style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>
							${totalRuleSystemCnt.type3}<span lang=EN-US><o:p></o:p></span></span></b>
						</p>
					</td>
					<td width=300 valign=top style='width: 98.5pt; border: solid windowtext 1.0pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
						<p class='MsoListParagraph' align=center style='margin: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
							<b><span style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>
							${totalRuleSystemCnt.type4}<span lang=EN-US><o:p></o:p></span></span></b>
						</p>
					</td>
					<td width=300 valign=top style='width: 98.5pt; border: solid windowtext 1.0pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
						<p class='MsoListParagraph' align=center style='margin: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
							<b><span style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>
							${totalRuleSystemCnt.type5}<span lang=EN-US><o:p></o:p></span></span></b>
						</p>
					</td>
					<td width=300 valign=top style='width: 98.5pt; border: solid windowtext 1.0pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
						<p class='MsoListParagraph' align=center style='margin: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
							<b><span style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>
							${totalRuleSystemCnt.type6}<span lang=EN-US><o:p></o:p></span></span></b>
						</p>
					</td>
					<td width=300 valign=top style='width: 98.5pt; border: solid windowtext 1.0pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
						<p class='MsoListParagraph' align=center style='margin: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
							<b><span style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>
							${totalRuleSystemCnt.type7}<span lang=EN-US><o:p></o:p></span></span></b>
						</p>
					</td>
					<td width=300 valign=top style='width: 98.5pt; border: solid windowtext 1.0pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
						<p class='MsoListParagraph' align=center style='margin: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
							<b><span style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>
							${totalRuleSystemCnt.type8}<span lang=EN-US><o:p></o:p></span></span></b>
						</p>
					</td>
					<td width=300 valign=top style='width: 98.5pt; border: solid windowtext 1.0pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
						<p class='MsoListParagraph' align=center style='margin: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
							<b><span style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>
							${totalRuleSystemCnt.type9}<span lang=EN-US><o:p></o:p></span></span></b>
						</p>
					</td>
					<td width=300 valign=top style='width: 98.5pt; border: solid windowtext 1.0pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
						<p class='MsoListParagraph' align=center style='margin: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
							<b><span style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>
							${totalRuleSystemCnt.type10}<span lang=EN-US><o:p></o:p></span></span></b>
						</p>
					</td>
				</tr>
			</c:forEach>
		</table>
		
		<p class=MsoNormal
			style='margin-bottom: 0cm; margin-bottom: 2pt; line-height: normal; word-break: keep-all'>
			<span lang=EN-US
				style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'><o:p>&nbsp;</o:p></span>
		</p>

		<p class='MsoListParagraph'
			style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0cm; margin-left: 38.25pt; margin-bottom: 2pt; text-indent: -18.0pt; line-height: normal; mso-list: l16 level1 lfo18; word-break: keep-all'>
			<![if !supportLists]>
			<b style='mso-bidi-font-weight: normal'><span lang=EN-US
				style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast; mso-bidi-font-family: "맑은 고딕"; mso-bidi-theme-font: minor-fareast'><span
					style='mso-list: Ignore'># 별첨<span
						style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
					</span></span></span></b>
		</p>
		
		<p class='MsoListParagraph'
			style='margin-top: 5pt; margin-right: 0cm; margin-bottom: 0cm; margin-left: 38.25pt; margin-bottom: 2pt; text-indent: -18.0pt; line-height: normal; mso-list: l16 level1 lfo18; word-break: keep-all'>
			<![if !supportLists]>
			<b style='mso-bidi-font-weight: normal'><span lang=EN-US
				style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast; mso-bidi-font-family: "맑은 고딕"; mso-bidi-theme-font: minor-fareast'><span
					style='mso-list: Ignore'>1.<span
						style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
					</span></span></span></b>
			<![endif]>
			<b style='mso-bidi-font-weight: normal'><span
				style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>누적 소명현황 TOP5<span lang=EN-US><o:p></o:p></span>
			</span></b>
		</p>
		
		<p class='MsoListParagraph'
			style='margin-top: 5pt; margin-right: 0cm; margin-bottom: 0cm; margin-left: 55.25pt; margin-bottom: 2pt; text-indent: -18.0pt; line-height: normal; mso-list: l16 level1 lfo18; word-break: keep-all; color: red;'>
			<b style='mso-bidi-font-weight: normal'><span
				style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>1) 
				<span lang=EN-US>진행상태 기준<o:p></o:p></span>
			</span></b>
		</p>
		
		<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
			width=652
			style='width: 530.05pt;  margin-left: 38.25pt; margin-top: 3pt; border-collapse: collapse; mso-border-alt: solid windowtext .5pt; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt'>
			<tr style='mso-yfti-irow: 0; mso-yfti-firstrow: yes'>
				<td width=123 
					style='width: 150.15pt; border: solid windowtext 1.0pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
					<p class=MsoNormal align=center
						style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
						<b><span
							style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>부서<span
							lang=EN-US><o:p></o:p></span></span></b>
					</p>
				</td>
				<td width=123
					style='width: 80.15pt; border: solid windowtext 1.0pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
					<p class=MsoNormal align=center
						style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
						<b><span lang=EN-US
							style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>이름<o:p></o:p></span></b>
					</p>
				</td>
				<td width=113
					style='width: 50.15pt; border: solid windowtext 1.0pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
					<p class=MsoNormal align=center
						style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
						<b><span
							style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>총건수<span
							lang=EN-US><o:p></o:p></span></span>
					</p>
				</td>
				<td width=113
					style='width: 50.15pt; border: solid windowtext 1.0pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
					<p class=MsoNormal align=center
						style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
						<b><span
							style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>소명요청<span
							lang=EN-US><o:p></o:p></span></span></b>
					</p>
				</td>
				<td width=180 valign=top
					style='width: 50.15pt; border: solid windowtext 1.0pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
					<p class=MsoNormal align=center
						style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
						<b><span
							style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>소명응답<span
							lang=EN-US><o:p></o:p></span></span></b>
					</p>
				</td>
				<td width=180 valign=top
					style='width: 50.15pt; border: solid windowtext 1.0pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
					<p class=MsoNormal align=center
						style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
						<b><span
							style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>재소명요청<span
							lang=EN-US><o:p></o:p></span></span></b>
					</p>
				</td>
				<td width=180 valign=top
					style='width: 50.15pt; border: solid windowtext 1.0pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
					<p class=MsoNormal align=center
						style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
						<b><span
							style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>판정완료<span
							lang=EN-US><o:p></o:p></span></span></b>
					</p>
				</td>
				<td width=180 valign=top
					style='width: 50.15pt; border: solid windowtext 1.0pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
					<p class=MsoNormal align=center
						style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
						<b><span
							style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>소명취소<span
							lang=EN-US><o:p></o:p></span></span></b>
					</p>
				</td>
				<td width=180 valign=top
					style='width: 100.15pt; border: solid windowtext 1.0pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
					<p class=MsoNormal align=center
						style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
						<b><span
							style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>기타<span
							lang=EN-US><o:p></o:p></span></span></b>
					</p>
				</td>
			</tr>
			<c:choose>
				<c:when test="${empty summonStatusList}">
					<tr>
						<td colspan="9" valign=top
							style='width: 77.95pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
							<p class=MsoNormal align=center
								style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
								<span lang=EN-US
									style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>데이터가 없습니다.<o:p></o:p></span>
							</p>
						</td>
					</tr>
				</c:when>
				<c:otherwise>
					<c:forEach items="${summonStatusList }" var="summonStatusInq" varStatus="status">
						<tr>
							<td width=85 valign=top
								style='width: 150.15pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt; vertical-align: middle;'>
								<p class=MsoNormal align=center
									style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
									<span
										style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>${summonStatusInq.dept_name }<span
										lang=EN-US><o:p></o:p></span></span>
								</p>
							</td>
							<td width=95 valign=top
								style='width: 80.15pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt; vertical-align: middle;'>
								<p class=MsoNormal align=center
									style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
									<span
										style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>${summonStatusInq.emp_user_name }<span
										lang=EN-US><o:p></o:p></span></span>
								</p>
							</td>
							<td width=95 valign=top
								style='width: 50.15pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt; vertical-align: middle;'>
								<p class=MsoNormal align=center
									style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
									<span
										style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>${summonStatusInq.r_sum }건<span
										lang=EN-US><o:p></o:p></span></span>
								</p>
							</td>
							<td width=95 valign=top
								style='width: 50.15pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt; vertical-align: middle;'>
								<p class=MsoNormal align=center
									style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
									<span
										style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>${summonStatusInq.r2 }건<span
										lang=EN-US><o:p></o:p></span></span>
								</p>
							</td>
							<td width=95 valign=top
								style='width: 50.15pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt; vertical-align: middle;'>
								<p class=MsoNormal align=center
									style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
									<span
										style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>${summonStatusInq.r4 }건<span
										lang=EN-US><o:p></o:p></span></span>
								</p>
							</td>
							<td width=95 valign=top
								style='width: 50.15pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt; vertical-align: middle;'>
								<p class=MsoNormal align=center
									style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
									<span
										style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>${summonStatusInq.r5 }건<span
										lang=EN-US><o:p></o:p></span></span>
								</p>
							</td>
							<td width=95 valign=top
								style='width: 50.15pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt; vertical-align: middle;'>
								<p class=MsoNormal align=center
									style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
									<span
										style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>${summonStatusInq.r8 }건<span
										lang=EN-US><o:p></o:p></span></span>
								</p>
							</td>
							<td width=95 valign=top
								style='width: 50.15pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt; vertical-align: middle;'>
								<p class=MsoNormal align=center
									style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
									<span
										style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>${summonStatusInq.r9 }건<span
										lang=EN-US><o:p></o:p></span></span>
								</p>
							</td>
							<td width=95 valign=top
								style='width: 100.15pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt; vertical-align: middle;'>
								<p class=MsoNormal align=center
									style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
									<span
										style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'><span
										lang=EN-US><o:p></o:p></span></span>
								</p>
							</td>
						</tr>
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</table>
		
		<p class='MsoListParagraph'
			style='margin-top: 5pt; margin-right: 0cm; margin-bottom: 0cm; margin-left: 55.25pt; margin-bottom: 2pt; text-indent: -18.0pt; line-height: normal; mso-list: l16 level1 lfo18; word-break: keep-all; color: red;'>
			<b style='mso-bidi-font-weight: normal'><span
				style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>2) 
				<span lang=EN-US>판정결과 기준<o:p></o:p></span>
			</span></b>
		</p>
		
		<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
			width=652
			style='width: 530.05pt;  margin-left: 38.25pt;  margin-top: 3pt; border-collapse: collapse; mso-border-alt: solid windowtext .5pt; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt'>
			<tr style='mso-yfti-irow: 0; mso-yfti-firstrow: yes'>
				<td width=123 
					style='width: 150.15pt; border: solid windowtext 1.0pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
					<p class=MsoNormal align=center
						style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
						<b><span
							style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>부서<span
							lang=EN-US><o:p></o:p></span></span></b>
					</p>
				</td>
				<td width=123
					style='width: 80.15pt; border: solid windowtext 1.0pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
					<p class=MsoNormal align=center
						style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
						<b><span lang=EN-US
							style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>이름<o:p></o:p></span></b>
					</p>
				</td>
				<td width=113
					style='width: 50.15pt; border: solid windowtext 1.0pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
					<p class=MsoNormal align=center
						style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
						<b><span
							style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>총건수<span
							lang=EN-US><o:p></o:p></span></span>
					</p>
				</td>
				<td width=113
					style='width: 50.15pt; border: solid windowtext 1.0pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
					<p class=MsoNormal align=center
						style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
						<b><span
							style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>미판정<span
							lang=EN-US><o:p></o:p></span></span></b>
					</p>
				</td>
				<td width=180 valign=top
					style='width: 50.15pt; border: solid windowtext 1.0pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
					<p class=MsoNormal align=center
						style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
						<b><span
							style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>적정<span
							lang=EN-US><o:p></o:p></span></span></b>
					</p>
				</td>
				<td width=180 valign=top
					style='width: 50.15pt; border: solid windowtext 1.0pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
					<p class=MsoNormal align=center
						style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
						<b><span
							style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>부적정<span
							lang=EN-US><o:p></o:p></span></span></b>
					</p>
				</td>
				<td width=180 valign=top
					style='width: 50.15pt; border: solid windowtext 1.0pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
					<p class=MsoNormal align=center
						style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
						<b><span
							style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>재소명<span
							lang=EN-US><o:p></o:p></span></span></b>
					</p>
				</td>
				<td width=180 valign=top
					style='width: 100.15pt; border: solid windowtext 1.0pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
					<p class=MsoNormal align=center
						style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
						<b><span
							style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>기타<span
							lang=EN-US><o:p></o:p></span></span></b>
					</p>
				</td>
			</tr>
			<c:choose>
				<c:when test="${empty summonResultList}">
					<tr>
						<td colspan="8" valign=top
							style='width: 77.95pt; border: solid windowtext 1.0pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt'>
							<p class=MsoNormal align=center
								style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
								<span lang=EN-US
									style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>데이터가 없습니다.<o:p></o:p></span>
							</p>
						</td>
					</tr>
				</c:when>
				<c:otherwise>
					<c:forEach items="${summonResultList }" var="summonResultInq" varStatus="status">
						<tr>
							<td width=85 valign=top
								style='width: 150.15pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt; vertical-align: middle;'>
								<p class=MsoNormal align=center
									style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
									<span
										style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>${summonResultInq.dept_name }<span
										lang=EN-US><o:p></o:p></span></span>
								</p>
							</td>
							<td width=95 valign=top
								style='width: 80.15pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt; vertical-align: middle;'>
								<p class=MsoNormal align=center
									style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
									<span
										style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>${summonResultInq.emp_user_name }<span
										lang=EN-US><o:p></o:p></span></span>
								</p>
							</td>
							<td width=95 valign=top
								style='width: 50.15pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt; vertical-align: middle;'>
								<p class=MsoNormal align=center
									style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
									<span
										style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>${summonResultInq.rs_sum }건<span
										lang=EN-US><o:p></o:p></span></span>
								</p>
							</td>
							<td width=95 valign=top
								style='width: 50.15pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt; vertical-align: middle;'>
								<p class=MsoNormal align=center
									style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
									<span
										style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>${summonResultInq.rs0 }건<span
										lang=EN-US><o:p></o:p></span></span>
								</p>
							</td>
							<td width=95 valign=top
								style='width: 50.15pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt; vertical-align: middle;'>
								<p class=MsoNormal align=center
									style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
									<span
										style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>${summonResultInq.rs1 }건<span
										lang=EN-US><o:p></o:p></span></span>
								</p>
							</td>
							<td width=95 valign=top
								style='width: 50.15pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4ptl; vertical-align: middle;'>
								<p class=MsoNormal align=center
									style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
									<span
										style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>${summonResultInq.rs2 }건<span
										lang=EN-US><o:p></o:p></span></span>
								</p>
							</td>
							<td width=95 valign=top
								style='width: 50.15pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4ptl; vertical-align: middle;'>
								<p class=MsoNormal align=center
									style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
									<span
										style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>${summonResultInq.rs3 }건<span
										lang=EN-US><o:p></o:p></span></span>
								</p>
							</td>
							<td width=95 valign=top
								style='width: 100.15pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; padding: 0cm 5.4pt 0cm 5.4pt; vertical-align: middle;'>
								<p class=MsoNormal align=center
									style='margin-bottom: 0cm; margin-bottom: .0001pt; text-align: center; line-height: normal; word-break: keep-all'>
									<span
										style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'><span
										lang=EN-US><o:p></o:p></span></span>
								</p>
							</td>
						</tr>
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</table>

		<p class='MsoListParagraph'
			style='margin-top: 5pt; margin-right: 0cm; margin-bottom: 0cm; margin-left: 38.25pt; margin-bottom: 2pt; text-indent: -18.0pt; line-height: normal; mso-list: l16 level1 lfo18; word-break: keep-all'>
			<![if !supportLists]>
			<b style='mso-bidi-font-weight: normal'><span lang=EN-US
				style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast; mso-bidi-font-family: "맑은 고딕"; mso-bidi-theme-font: minor-fareast'><span
					style='mso-list: Ignore'>2.<span
						style='font: 7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
					</span></span></span></b>
			<![endif]>
			<b style='mso-bidi-font-weight: normal'><span
				style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>부서별
					개인 소명 현황<span lang=EN-US><o:p></o:p></span>
			</span></b>
		</p>

		<p class='MsoListParagraph'
			style='margin-top: 5pt; margin-right: 0cm; margin-bottom: 0cm; margin-left: 55.25pt; margin-bottom: 2pt; text-indent: -18.0pt; line-height: normal; mso-list: l16 level1 lfo18; word-break: keep-all; color: red;'>
			<b style='mso-bidi-font-weight: normal'><span
				style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>1) 
				<span lang=EN-US>진행상태 기준<o:p></o:p></span>
			</span></b>
		</p>
		
		<div id="chart1" style="width:100%; height: 300px;"></div>
		<img id="chartImage0" style="display: none;" src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/chartImages/chart1.png">
		
		<p class='MsoListParagraph'
			style='margin-top: 5pt; margin-right: 0cm; margin-bottom: 0cm; margin-left: 55.25pt; margin-bottom: 2pt; text-indent: -18.0pt; line-height: normal; mso-list: l16 level1 lfo18; word-break: keep-all; color: red;'>
			<b style='mso-bidi-font-weight: normal'><span
				style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>2) 
				<span lang=EN-US>판정결과 기준<o:p></o:p></span>
			</span></b>
		</p>
		
		<div id="chart2" style="width:100%; height: 300px;"></div>
		
		<div style='page-break-before:always'></div>
		
		 <p class='MsoListParagraph'
			style='margin-top: 5pt; margin-right: 0cm; margin-bottom: 0cm; margin-left: 38.25pt; margin-bottom: 2pt; text-indent: -18.0pt; line-height: normal; mso-list: l16 level1 lfo18; word-break: keep-all'>
			<b style='mso-bidi-font-weight: normal'><span
				style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>3.개인정보처리 유형별
					소명 현황<span lang=EN-US><o:p></o:p></span>
			</span></b>
		</p>
		
		<div id="chart3" style="width:100%; height: 300px;"></div>

		<p class='MsoListParagraph'
			style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0cm; margin-left: 38.25pt; margin-bottom: 2pt; text-indent: -18.0pt; line-height: normal; mso-list: l16 level1 lfo18; word-break: keep-all'>
			<b style='mso-bidi-font-weight: normal'><span
				style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'>4.개인정보처리
					시스템별 소명 현황<span lang=EN-US><o:p></o:p></span>
			</span></b>
		</p>
		
		<div id="chart4" style="width:100%; height: 285px; float: left;"></div><div style="clear:both;"></div>

		<p class='MsoListParagraph'
			style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0cm; margin-left: 38.25pt; margin-bottom: 2pt; line-height: normal; word-break: keep-all'>
			<span lang=EN-US
				style='mso-bidi-font-size: 10.0pt; mso-ascii-font-family: "맑은 고딕"; mso-ascii-theme-font: minor-fareast; mso-hansi-font-family: "맑은 고딕"; mso-hansi-theme-font: minor-fareast'><o:p>&nbsp;</o:p></span>
		</p>

	</div>

</body>

</html>
