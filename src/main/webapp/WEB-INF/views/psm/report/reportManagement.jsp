<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>

<style>
	.btn-xsm{
		padding: 0 5px;
		font-size: 12px;
   		line-height: 1.5;
	}
</style>
<c:set var="currentMenuId" value="${index_id}" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<c:set var="now" value="<%=new java.util.Date()%>" />
<c:set var="sysYear">
	<fmt:formatDate value="${now}" pattern="yyyy" />
</c:set>
<c:set var="sysMonth">
	<fmt:formatDate value="${now}" pattern="MM" />
</c:set>
<c:choose>
	<c:when test="${empty search.search_from}">
		<c:set var="fromYear">
<%-- 			<fmt:formatDate value="${now}" pattern="yyyy" /> --%>
		</c:set>
		<c:set var="fromMonth">
			<fmt:formatDate value="${now}" pattern="MM" />
		</c:set>
	</c:when>
	<c:otherwise>
		<c:set var="fromYear" value="${fn:substring(search.search_from,0,4) }" />
		<c:set var="fromMonth"
			value="${fn:substring(search.search_from,4,6) }" />
	</c:otherwise>
</c:choose>
<c:choose>
	<c:when test="${empty search.search_to}">
		<c:set var="toYear">
			<fmt:formatDate value="${now}" pattern="yyyy" />
		</c:set>
		<c:set var="toMonth">
			<fmt:formatDate value="${now}" pattern="MM" />
		</c:set>
	</c:when>
	<c:otherwise>
		<c:set var="toYear" value="${fn:substring(search.search_to,0,4) }" />
		<c:set var="toMonth" value="${fn:substring(search.search_to,4,6) }" />
	</c:otherwise>
</c:choose>

<script src="${rootPath}/resources/js/common/ezDatepicker.js"
	type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/report/report.js"
	type="text/javascript" charset="UTF-8"></script>
<script
	src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js"
	type="text/javascript"></script>
	
<h1 class="page-title">${currentMenuName}</h1>

<div class="portlet light portlet-fit portlet-datatable bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-settings font-dark"></i> <span
				class="caption-subject font-dark sbold uppercase">${currentMenuName}
				리스트</span>
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-container">
			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
				<div class="dataTables_scroll" style="position: relative;">
					<div>
						<div class="col-md-6"
							style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
							<div class="portlet box grey-salt">
								<div class="portlet-title" style="background-color: #2B3643;">
									<div class="caption">
										<i class="fa fa-plus"></i> 검색
									</div>
									<!-- <div class="tools">
		                                 <a href="javascript:;" class="collapse"> </a>
		                             </div> -->
								</div>
								<div class="portlet-body form">
									<div class="form-body"
										style="padding-left: 20px; padding-right: 10px;">
										<div class="form-group">
										<form action="" class="mt-repeater form-horizontal">
											<div data-repeater-list="group-a">
											<div data-repeater-item class="mt-repeater-item">
												<!-- jQuery Repeater Container -->
												<div class="mt-repeater-input">
													<div class="col-md-2">
														<label class="control-label">보고서</label>
														<select id="reportType" name="makeType" class="form-control input-medium">
														<c:forEach var="report" items="${reportMap}" varStatus="idx">
															<c:if test="${idx.index < 6}">
																<option value="${report.key }" ${report.key eq search.report_type ? 'selected':'' }>${report.value }</option>
															</c:if>
															<c:if test="${idx.index == 6 and mode_access_auth eq 'Y'}">
																<option value="${report.key }" ${report.key eq search.report_type ? 'selected':'' }>${report.value }</option>
															</c:if>
															<c:if test="${idx.index == 7 and ui_type eq 'M'}">
																<option value="${report.key }" ${report.key eq search.report_type ? 'selected':'' }>${report.value }</option>
															</c:if>
															<c:if test="${idx.index == 8 and ui_type eq 'H' }">
																<option value="${report.key }" ${report.key eq search.report_type ? 'selected':'' }>${report.value }</option>
															</c:if>
															<c:if test="${idx.index == 9}">
																<option value="${report.key }" ${report.key eq search.report_type ? 'selected':'' }>${report.value }</option>
															</c:if>
															<c:if test="${idx.index == 10 and ui_type eq 'Kurly' }">
																<option value="${report.key }" ${report.key eq search.report_type ? 'selected':'' }>${report.value }</option>
															</c:if>
														</c:forEach>
														</select>
													</div>
													<div class="col-md-6" id="selPeriod" style="margin-left:40px;">
														<label class="control-label">기간선택 <input type="checkbox" id="dateCheck" class="custom-control-input" onchange="dateSelect()" style="margin:0px;"  <c:if test="${!empty search.isSearch }">checked</c:if>></label><br />
														<div>
															<div class="input-group input-daterange" style="display: flex;">
																<select id="startyear" name="startyear" class="form-control" style="text-align: right; width: 95px;" onchange="yearReplace()"<c:if test="${empty search.isSearch }">disabled</c:if>>
																	<c:forEach var="i" begin="${sysYear-10 }"
																		end="${sysYear }" step="1">
																		<option value="${i }"
																			<c:if test="${i eq fromYear }">selected</c:if>>${i }년</option>
																	</c:forEach>
																</select>
																<select id="startmonth" name="startmonth" class="form-control" style="text-align: right; width: 80px;" onchange="monthReplace()" <c:if test="${empty search.isSearch }">disabled</c:if>>
																	<c:forEach var="i" begin="1" end="12" step="1">
																		<option value="${i }"
																			<c:if test="${i eq fromMonth }">selected</c:if>>${i }월</option>
																	</c:forEach>
																</select>
																<div>
																	<span class="input-group-addon " style="text-align: center; height: 34px">~</span>
																</div>
																<select id="endyear" name="endyear" class="form-control" style="text-align: right; width: 95px;" onchange="yearReplace()"<c:if test="${empty search.isSearch }">disabled</c:if>>
																	<c:forEach var="i" begin="${sysYear-10 }"
																		end="${sysYear }" step="1">
																		<option value="${i }"
																			<c:if test="${i eq toYear }">selected</c:if>>${i }년</option>
																	</c:forEach>
																</select>
																<select id="endmonth" name="endmonth" class="form-control" style="text-align: right; width: 80px;" onchange="monthReplace()" <c:if test="${empty search.isSearch }">disabled</c:if>>
																	<c:forEach var="i" begin="1" end="12" step="1">
																		<option value="${i }"
																			<c:if test="${i eq toMonth }">selected</c:if>>${i }월</option>
																	</c:forEach>
																</select>
															</div>
														</div>
													</div>
												</div>
												</div>
											</div>
										</form>
										</div>
										<div align="right">
											<a class="btn btn-sm blue btn-outline sbold uppercase"
												onclick="report_Search()"> <i class="fa fa-search"></i>
												검색
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div>
					<table
						class="table table-striped table-bordered table-hover order-column">
						<colgroup>
							<col width="5%" />
							<col width="10%" />
							<col width="10%" />
							<col width="50%" />
							<col width="10%" />
							<col width="10%" />
							<col width="5%" />
						</colgroup>
						<thead>
							<tr>
								<th scope="col" style="text-align: center;">번호</th>
								<th scope="col" style="text-align: center;">보고서 분류</th>
								<th scope="col" style="text-align: center;">작성자</th>
								<th scope="col" style="text-align: center;">보고서 제목</th>
								<th scope="col" style="text-align: center;">최근다운일자</th>
								<th scope="col" style="text-align: center;">다운로드횟수</th>
								<th scope="col" style="text-align: center;">다운로드</th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${empty reportList}">
									<tr>
										<td colspan="7" align="center">데이터가 없습니다.</td>
									</tr>
								</c:when>
								<c:otherwise>
									<c:forEach items="${reportList}" var="list" varStatus="status">
										<fmt:formatDate var="updateTime" value="${list.update_time}"
											pattern="yyyy-MM-dd HH:mm:ss" />
										<fmt:formatDate var="downloadTime"
											value="${list.download_time}" pattern="yyyy-MM-dd HH:mm:ss" />
										<tr>
											<td style="text-align: center;"><ctl:nullCv nullCheck="${list.rownum}" /></td>
											<td style="text-align: center;"><ctl:nullCv nullCheck="${reportMap[list.report_type]}" /></td>
											<td style="text-align: center;"><ctl:nullCv
													nullCheck="${list.update_id}" /></td>
											<c:choose>
												<c:when test="${list.download_flag eq 'N'}">
													<td style="padding-left: 20px;">
													<ctl:nullCv nullCheck="${list.report_title}" /></td>
												</c:when>
												<c:otherwise>
													<td style="padding-left: 20px; color: lightgray;">
													<ctl:nullCv nullCheck="${list.report_title}" /></td>
												</c:otherwise>
											</c:choose>
											<td style="text-align: center;"><ctl:nullCv
													nullCheck="${downloadTime}" /></td>
											<td style="text-align: center;"><ctl:nullCv
													nullCheck="${ list.download_cnt}" /></td>
											<td style="cursor: pointer;">
											<a class="btn btn-xsm blue btn-outline sbold uppercase"
												onclick="report_Download('${list.report_seq}')">
												<i class="fas fa-file-word"></i>
												DOC
											</a>
											<a class="btn btn-xsm red btn-outline sbold uppercase"
												onclick="report_PDF('${list.report_seq}')">
												<i class="fas fa-file-pdf"></i>
												PDF
											</a>
											</td>
										</tr>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>

					<form id="listForm" method="post" style="display: none">
						<input type="hidden" name="current_menu_id" value="${currentMenuId}" /> 
						<input type="hidden" name="page_num">
						<input type="hidden" name="search_from" value="${search.search_from}"> 
						<input type="hidden" name="search_to" value="${search.search_to}"> 
						<input type="hidden" name="report_type" value="${search.report_type}">
						<input type="hidden" name="isSearch" value="${search.isSearch}">
						<input type="hidden" name="report_seq">
					</form>
					
					<div class="row" style="padding: 10px;">
						<!-- 페이징 영역 -->
						<c:if test="${search.total_count > 0}">
							<div id="pagingframe" align="center">
								<p>
									<ctl:paginator currentPage="${search.page_num}"
										rowBlockCount="${search.size}"
										totalRowCount="${search.total_count}" />
								</p>
							</div>
						</c:if>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>