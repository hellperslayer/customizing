<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>

<c:set var="currentMenuId" value="${index_id}" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/common/ezDatepicker.js"
	type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/report/report.js"
	type="text/javascript" charset="UTF-8"></script>
<script
	src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js"
	type="text/javascript"></script>

<script>
	function makeReport() {

		var checkbox = $("input[type=checkbox]");

		var start_date = $("#start_date").val()
		var end_date = $("#end_date").val()

		var count = checkbox.length;
		var colString = "";
		var chcekString = "";
		/* for(var i=1; i<=count; i++){
			
			var idx = '';
			
			if ( i < 10 ) idx = '0'+i;
			else idx = i;
			
			//var checkVal = $("input:checkbox[name=reportCheck" + idx + "]:checked").val();
			var checkVal = $($("input[type=checkbox]")[i-1]).attr('checked');
			
			if(checkVal =='undefined' || checkVal == null){
				checkVal = "0";	
			} else {
				checkVal = $($("input[type=checkbox]")[i-1]).val();
			}

			if (colString.length == 0) {
				colString = checkVal;
				chcekString="0";
			} else {
				colString = colString + "," + checkVal;
				chcekString = chcekString + "," + "0";
			}
		} */
		for (var i = 0; i < count; i++) {

			var checkVal = $(
					"input:checkbox[name=reportCheck" + i + "]:checked").val();

			if (checkVal == 'undefined' || checkVal == null) {
				checkVal = "0";
			}

			if (colString.length == 0) {
				colString = checkVal;
				chcekString = "0";
			} else {
				colString = colString + "," + checkVal;
				chcekString = chcekString + "," + "0";
			}
		}

		if (colString == chcekString) {
			alert("보고서를 확인할 도메인을 선택하세요.");
			return;
		}

		var url = "${rootPath}/report/reportDetail.html";

		url = url + "?system_seq=" + colString;
		url = url + "&start_date=" + start_date;
		url = url + "&end_date=" + end_date;

		window.open(url, 'Popup', 'height=800px, width=800px, scrollbars=yes, left=0, top=0');

		//location.href="${rootPath}/report/reportDetail.html?system_seq=" + colString+"&start_date=" + start_date+"&end_date=" + end_date;
	}

var makeReport_new = function() {
	var url;
	var type = document.getElementById("makeType").value;
	var periodType = document.getElementById("periodType").value;
	var accessDateType = document.getElementById("accessDateType").value;
	var  quarter_type = document.getElementById("quarter").value;

	var year = $("#year").val();
	var month, endyear, endmonth;
	
	
	if(periodType == "1") {
		month = $("#month").val();
		endyear = $("#year").val();
		endmonth = $("#month").val();
	} else if(periodType == "2") {
		var q = document.getElementById("quarter").value;
		endyear = year;

		if(q == "1") {
			month = 1;
			endmonth = 3;
		} else if(q == "2") {
			month = 4;
			endmonth = 6;
		} else if(q == "3") {
			month = 7;
			endmonth = 9;
		} else if(q == "4") {
			month = 10;
			endmonth = 12;
		}
	} else if(periodType == "3") {
		var h = document.getElementById("half").value;
		endyear = year;

		if(h == "1") {
			month = 1;
			endmonth = 6;
		} else if(h == "2") {
			month = 7;
			endmonth = 12;
		}
	} else if(periodType == "4") {
		endyear = year;
		month = 1;
		endmonth = 12;
	} else if(periodType == "5") {
		month = $("#startmonth").val();
		endyear = $("#endyear").val();
		endmonth = $("#endmonth").val();
	}

	var lastDay = ( new Date(endyear, endmonth, 0) ).getDate();
	if (month < 10) {
		month = "0" + month;
	}
	if (endmonth < 10) {
		endmonth = "0" + endmonth;
	}
	var date = year + "-" + month;
	var enddate = endyear + "-" + endmonth;
	var start_date = date + "-01";
	var end_date = enddate + "-" + lastDay;
	var checkbox = $("input[type=checkbox]");
	var count = checkbox.length;
	var colString = "";
	var chcekString = "";
	var res = "";
	var checkBoxArr = $('input:checkbox[name^=reportCheck]:checked');
	var menu_id = $('input[name=current_menu_id]').val();
	
	for(var i = 0; i<checkBoxArr.length; i++) {
		if(res == "") {
			res += checkBoxArr[i].value;
		} else {
			res += "," + checkBoxArr[i].value;
		}
	}
	if(res == "") {
		alert("보고서를 확인할 도메인을 선택하세요.");
		return;
	}
	if(type == "6") {
		var temp = res.split(',');
		if(temp.length > 1) {
			alert("하나의 시스템만 선택해주세요.");
			return;
		}
	} else {
		alert("보고서는 매달1일부터 매달 말일까지 생성됩니다.");
	}
	// report/reportDetail_new.html => base on 1~4 || logic eq 1,3
	if(type == 1) {
		url = "${rootPath}/report/reportEmpLevel.html";
	} else if(type == 2) {
		url = "${rootPath}/report/reportCPO.html";
	} else if(type == 3) {
		url = "${rootPath}/report/reportEmpSystem.html";
	} else if(type == 4) {
		url = "${rootPath}/report/reportIdenUniq.html";
	} else if(type == 5) { 
		url = "${rootPath}/report/reportHalfDate.html";
	} else if(type == 6) {
		url = "${rootPath}/report/reportDetail_access.html";
	} else if(type == 7) {
		url = "${rootPath}/report/reportDetail_moef.html";
	} else if(type == 8) {
		url = "${rootPath}/report/reportDetail_allogInq_access.html";
	} else if(type == 9) {
		url = "${rootPath}/report/reportDetail_download.html";
	} else if(type == 10) {
		url = "${rootPath}/report/reportDetail_authInfo.html";
	} else if(type == 11) {
		url = "${rootPath}/report/reportDetail_combined.html";
	} else if(type == 12) {
		url = "${rootPath}/report/reportDetail_download_new.html";
	} else if(type == 13) {
		url = "${rootPath}/report/reportEmpLevel_new.html";
	}  else if(type == 14){
		url = "${rootPath}/report/reportTotalIntegration.html"
	}else if(type == 15){
        url = "${rootPath}/report/reportDBAccess.html"
    }else if(type == 99){
        url = "${rootPath}/report/system_empReport.html"
    }else if(type == 98){
        url = "${rootPath}/report/system_downReport.html"
    }
	
// 	alert(start_date+"************"+end_date);
	url += "?type="+type + "&period_type="+periodType + "&system_seq="+res + "&start_date="+start_date + "&end_date="+end_date + "&accessDateType="+accessDateType + "&menu_id="+menu_id+"&quarter_type="+quarter_type; /* + "&download_type=pdf" */
	if(type == 5) {
		url += "&half_type="+ document.getElementById("half").value;
	}
	window.open(url, 'Popup', 'height=800px, width=1000px, scrollbars=yes, left=0, top=0');
};
	 
	 function selPeriodType(type) {
		 var type = "periodType"+type
		 document.getElementById("periodType1").style.display = "none";
		 document.getElementById("periodType2").style.display = "none";
		 document.getElementById("periodType3").style.display = "none";
		 document.getElementById("periodType5").style.display = "none";
		 if(type!="4") {
			 document.getElementById(type).style.display = "block";
		 }
	 }
	 

	 function checkOneCheck(a) {
	 	var typeVal = $("#makeType").val();
	 	
	 	if(typeVal == 6) {
		 	$('input[name^=reportCheck]').prop('checked', false);
	 		a.checked = true;
	 	}
	 }
	 
	 function checkAllCheck() {
		 $('#allCheck').prop("checked", $("input:checkbox[name^=reportCheck]:checked").length == ${fn:length(systemList)} ? true : false);
	 }
	 
</script>


<h1 class="page-title">${currentMenuName}</h1>

<div class="portlet light portlet-fit portlet-datatable bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-settings font-dark"></i> <span
				class="caption-subject font-dark sbold uppercase">${currentMenuName} 리스트</span>
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-container">
			<div id="datatable_ajax_2_wrapper" class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
				<div class="dataTables_scroll" style="position: relative;">
					<div>
						<div class="col-md-6" style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
							<div class="portlet box grey-salt">
		                         <div class="portlet-title" style="background-color: #2B3643;">
		                             <div class="caption">
		                                 <i class="fa fa-plus"></i> 생성 </div>
		                         </div>
		                         <div class="portlet-body form">
		                             <div class="form-body" style="padding-left:20px; padding-right:10px;">
			                            <div class="form-group">
	                                     	<form id="listForm" method="POST" class="mt-repeater form-horizontal">
		                                         <div data-repeater-list="group-a">
		                                             <div data-repeater-item class="mt-repeater-item">
														<!-- jQuery Repeater Container -->
														<div class="mt-repeater-input">
															<div class="col-md-4">
		                                                     <label class="control-label">분류</label>
		                                                     <select id="makeType" name="makeType" class="form-control input-medium" onchange="enableCheck(this)">
																<option value="1">담당자용_수준진단보고</option>
																<c:if test="${masterflag eq 'Y' }">
																<c:if test="${userSession.auth_id == 'AUTH00000' || userSession.auth_id == 'AUTH00001'}">
																	<option value="2">CPO_오남용보고</option>
																</c:if>
																</c:if>
																<option value="3">담당자용_시스템별 수준진단보고</option>
																<option value="4">고유식별정보 처리 현황 보고</option>
																<option value="5">반기별_수준진단보고</option>
                                                               	<c:if test="${mode_access_auth eq 'Y' }">
																	<option value="6">접근권한 관리대장</option>
																</c:if>
																<c:if test="${ui_type eq 'M' }">
																	<option value="7">접근행위별 접속기록로그 TOP10</option>
																</c:if>
																<c:if test="${ui_type eq 'H' }">
																	<option value="8">접속기록로그 및 접근권한관리 TOP20</option>
																</c:if>
																<option value="9">개인정보 다운로드_보고</option>
																<c:if test="${ui_type eq 'Kurly' }">
																<option value="10">권한부여 관리 보고서</option>
																</c:if>
																<option value="12">다운로드 보고서_new2</option>
																<option value="13">수준진단보고서_new1</option>
																<option value="14">통합보고서</option>
																
																<option value="99" selected="selected">시스템 수준진단 (test)</option>
																<option value="98" selected="selected">시스템 다운로드 (test)</option>
		                                                     </select> 
															</div>														 
															<div class="col-md-6" id="selPeriod">
														 	<c:set var="date" value="${fn:split(paramBean.search_to,'-')}" />
															<label class="control-label">기간선택</label>
															<div class="row">
																<div id="accessDateTypeDiv" class="col-md-3">
																<select id="accessDateType" name="accessDateType" class="form-control input-small"
																	style="text-align: right; ">
																		<option value="1">전체</option>
																		<option value="2">권한부여일자</option>
																		<option value="3">권한변경일자</option>
																		<option value="4">권한해제일자</option>
																</select>
																</div>
																<div class="col-md-2">
																<select id="periodType" name="periodType" class="form-control"
																	style="text-align: right; " onchange="selPeriodType(this.value)">
																		<option value="1">월별</option>
																		<option value="2">분기별</option>
																		<option value="3">반기별</option>
																		<option value="4">년간</option>
																		<option value="5">기간검색</option>
																</select>
																</div>
																<div class="col-md-2">
																<select id="year" name="year" class="form-control input-small"
																	style="text-align: right; ">
																	<c:forEach var="i" begin="${date[0]-10 }"
																		end="${date[0] }" step="1">
																		<option value="${i }"
																			<c:if test="${i eq date[0] }">selected</c:if>>${i }년</option>
																	</c:forEach>
																</select>
																</div>
																<div id="periodType1" class="col-md-2">
																	<select id="month" name="month"
																		class="form-control input-xsmall"
																		style="text-align: right;">
																		<c:forEach var="i" begin="1" end="12" step="1">
																			<option value="${i }"
																				<c:if test="${i eq date[1] }">selected</c:if>>${i }월</option>
																		</c:forEach>
																	</select>
																</div>
																<div id="periodType2" class="col-md-2" style="display:none">
																	<select id="quarter" name="quarter" class="form-control input-small"
																			style="text-align: right; ">
																			<option value="1">1분기</option>
																			<option value="2">2분기</option>
																			<option value="3">3분기</option>
																			<option value="4">4분기</option>
																	</select>
																</div>
																<div id="periodType3" class="col-md-2" style="display:none">
																	<select id="half" name="half" class="form-control input-small"
																			style="text-align: right; ">
																			<option value="1">상반기</option>
																			<option value="2">하반기</option>
																	</select>
																</div>
																<div id="periodType5" style="display:none">
																	<div class="col-md-2">
																		<select id="startmonth" name="startmonth"
																			class="form-control input-xsmall"
																			style="text-align: right;">
																			<c:forEach var="i" begin="1" end="12" step="1">
																				<option value="${i }"
																					<c:if test="${i eq date[1] }">selected</c:if>>${i }월</option>
																			</c:forEach>
																		</select>
																	</div>
																	<div class="col-md-1">
																	-
																	</div>
																	<div class="col-md-2">
																		<select id="endyear" name="endyear" class="form-control input-small"
																			style="text-align: right; ">
																			<c:forEach var="i" begin="${date[0]-10 }"
																				end="${date[0] }" step="1">
																				<option value="${i }"
																					<c:if test="${i eq date[0] }">selected</c:if>>${i }년</option>
																			</c:forEach>
																		</select>
																	</div>
																	<div class="col-md-2">
																		<select id="endmonth" name="endmonth"
																			class="form-control input-xsmall"
																			style="text-align: right;">
																			<c:forEach var="i" begin="1" end="12" step="1">
																				<option value="${i }"
																					<c:if test="${i eq date[1] }">selected</c:if>>${i }월</option>
																			</c:forEach>
																		</select>
																	</div>
																</div>
															</div>
														</div>
	                                                 </div>
													</div>
	                                             </div>
										
												<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
		                                   </form>
		                               </div>
                                       <div align="right">
                                          	<a class="btn btn-sm blue btn-outline sbold uppercase"
												onclick="makeReport_new()"> <i class="fa fa-check"></i>
												보고서 생성
											</a>
										</div>
	                                 </div>
	                             </div>
							</div>
						</div>
					</div>
				</div>
				<div>
					<table
						class="table table-striped table-bordered table-hover order-column">
						<colgroup>
							<col width="5%" />
							<col width="45%" />
							<col width="45%" />
							<col width="5%" />
						</colgroup>
						<thead>
							<tr>
								<th scope="col" style="text-align: center;">No.</th>
								<th scope="col" style="text-align: center;">시스템명</th>
								<th scope="col" style="text-align: center;">URL</th>
								<th scope="col" style="text-align: center;">
								<input type="checkbox" id="allCheck" onclick="javascript:fnallCheck();" checked /> 전체</th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${empty systemList}">
									<tr>
										<td colspan="4" align="center">데이터가 없습니다.</td>
									</tr>
								</c:when>
								<c:otherwise>
									<c:set value="${search.total_count}" var="count" />
									<c:forEach items="${systemList}" var="list" varStatus="status">
										<tr>
											<td style="text-align: center;"><ctl:nullCv
													nullCheck="${status.count}" /></td>
											<td style="padding-left: 20px;"><ctl:nullCv
													nullCheck="${list.system_name }" /></td>
											<td style="padding-left: 20px;">
												<%-- <ctl:nullCv nullCheck="${list.main_url }" /> --%>
												${list.main_url }
											</td>
											<%-- <td><input type="checkbox" name ="reportCheck${list.system_seq}" id ="reportCheck${list.system_seq}" value= "${list.system_seq}" /></td> --%>
											<td style="text-align: center;"><input type="checkbox"
												name="reportCheck${status.index}"
												id="reportCheck${status.index}" value="${list.system_seq}" onclick="checkOneCheck(this);checkAllCheck()"
												checked /></td>
										</tr>
										<c:set var="count" value="${count - 1 }" />
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<%-- <form id="eventForm" method="POST" action="${rootPath}/allLogInq/list.html">
	<input type="hidden" name="search_from"/>
	<input type="hidden" name="search_to"/>
	<input type="hidden" name="system_seq"/>
	<input type="hidden" name="dept_name"/>
	<input type="hidden" name="privacyType"/>
	<input type="hidden" name="req_type"/>
	<input type="hidden" name="emp_user_id"/>
	<!-- <input type="hidden" name="start_h"/>
	<input type="hidden" name="end_h"/> -->
	<input type="hidden" name="scen_seq"/>
</form> --%>

<script type="text/javascript">
	var reportConfig = {
		//"listUrl":"${rootPath}/report/list_ovtimeRiskrateAnals.html"
		"listUrl" : "/PSM_REPORT_DEMO_NEW/frameset?__report=report/psm/holi_danger_analysis.rptdesign"
	};
	
	var makeType = $('input[id=makeType]').val();
	if(makeType == null) 
		enableCheck(1);
		
	function enableCheck(type) {
		var res = type.value;

		$('input[id=allCheck]').removeAttr('disabled');
		$('input[name^=reportCheck]').removeAttr('disabled');
		
		/* if(res == 3 || res == 5 || res == 8) {
			$('input[id=allCheck]').removeAttr('disabled');
			$('input[name^=reportCheck]').removeAttr('disabled');
		}else if(res == 6) {
			$('input[name^=reportCheck]').removeAttr('disabled');
			allNoCheck();
		}else {
			$('input[id=allCheck]').prop('checked', true);
			$('input[name^=reportCheck]').prop('checked', true);
			$('input[id=allCheck]').attr('disabled', 'true');
			$('input[name^=reportCheck]').attr('disabled', 'true');
		} */
		
		if(res == 2 && ${ui_type != 'Guri'}){
			$("input:checkbox[name^=reportCheck]").prop("checked",true);
			$("input:checkbox[name^=reportCheck]").prop("disabled",true);
			$('#allCheck').prop("disabled",true);
			$('#allCheck').prop("checked",true);
		}else{
			$("input:checkbox[name^=reportCheck]").prop("disabled",false);
			$('#allCheck').prop("disabled",false);
		}
		
		if(res == 5) {
			$("#periodType").val(3);
			$("#periodType").attr('disabled', 'true');
			selPeriodType(3);
		} else {
			$("#periodType").removeAttr('disabled');
		}
		
		if(res == 6) {
			$("#accessDateType").val(1);
			 document.getElementById("accessDateTypeDiv").style.display = "block";
		} else {
			$("#accessDateType").val(1);
			 document.getElementById("accessDateTypeDiv").style.display = "none";
		}
	}
</script>