<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<script src="${rootPath}/resources/js/jquery/jquery.min.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/report/report_inspection.js" type="text/javascript" charset="UTF-8"></script>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>

<HEAD>
<STYLE type="text/css">
<!--
p.HStyle0
	{style-name:"바탕글";}
li.HStyle0
	{style-name:"바탕글";}
div.HStyle0
	{style-name:"바탕글";}
p.HStyle1
	{style-name:"본문";}
li.HStyle1
	{style-name:"본문";}
div.HStyle1
	{style-name:"본문";}
p.HStyle2
	{style-name:"개요 1";}
li.HStyle2
	{style-name:"개요 1";}
div.HStyle2
	{style-name:"개요 1";}
p.HStyle3
	{style-name:"개요 2";}
li.HStyle3
	{style-name:"개요 2";}
div.HStyle3
	{style-name:"개요 2";}
p.HStyle4
	{style-name:"개요 3";}
li.HStyle4
	{style-name:"개요 3";}
div.HStyle4
	{style-name:"개요 3";}
p.HStyle5
	{style-name:"개요 4";}
li.HStyle5
	{style-name:"개요 4";}
div.HStyle5
	{style-name:"개요 4";}
p.HStyle6
	{style-name:"개요 5";}
li.HStyle6
	{style-name:"개요 5";}
div.HStyle6
	{style-name:"개요 5";}
p.HStyle7
	{style-name:"개요 6";}
li.HStyle7
	{style-name:"개요 6";}
div.HStyle7
	{style-name:"개요 6";}
p.HStyle8
	{style-name:"개요 7";}
li.HStyle8
	{style-name:"개요 7";}
div.HStyle8
	{style-name:"개요 7";}
p.HStyle9
	{style-name:"쪽 번호";}
li.HStyle9
	{style-name:"쪽 번호";}
div.HStyle9
	{style-name:"쪽 번호";}
p.HStyle10
	{style-name:"머리말";}
li.HStyle10
	{style-name:"머리말";}
div.HStyle10
	{style-name:"머리말";}
p.HStyle11
	{style-name:"각주";}
li.HStyle11
	{style-name:"각주";}
div.HStyle11
	{style-name:"각주";}
p.HStyle12
	{style-name:"미주";}
li.HStyle12
	{style-name:"미주";}
div.HStyle12
	{style-name:"미주";}
p.HStyle13
	{style-name:"메모";}
li.HStyle13
	{style-name:"메모";}
div.HStyle13
	{style-name:"메모";}
p.HStyle14
	{style-name:"MS바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle14
	{style-name:"MS바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle14
	{style-name:"MS바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
-->
.saveInput{
	width: 100%;
    border: none;
    text-align: center;
}
.backslash {
  background: url('${rootPath}/resources/image/backslash.png');
  background-size: 100% 100%;
  background-color: rgb(229, 229, 229);
}
</STYLE>
</HEAD>

<script type="text/javascript">
function printpr()
{
	$("#printButton").hide();
	
	window.print();
	self.close();
}
var proc_date = '${param.proc_date}';
var rootPath = '${rootPath}';
</script>

<BODY style="width: 763px;">
<div id="printButton" align="right">
	<input type=button onclick="savereport();" value="저장"/>
	<input type=button onclick="printpr();" value="출력"/>
</div>
<P CLASS=HStyle14 STYLE='text-align:right;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle14 STYLE='text-align:right;'>
<TABLE width="100%">
<TR>
	<TD colspan="3" valign="middle" style='height:38;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:25.0pt;font-family:"맑은 고딕";font-weight:900;line-height:160%'>${fn:substring(param.proc_date,4,6)}월 정기점검 보고서</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle14 STYLE='text-align:right;'></P>

<P CLASS=HStyle14 STYLE='text-align:left;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width: 100%'>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:40;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 2.1pt 1.4pt 2.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center; width: 100%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>구분</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:85%;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px; background: none' colspan="2">
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%;display: flex'>
			<input type="checkbox" class="saveCheckbox" style="float: none;" id="server" name="server"/>서버
			<input type="checkbox" class="saveCheckbox" style="float: none;" id="backup" name="backup"/>저장/백업
			<input type="checkbox" class="saveCheckbox" style="float: none;" id="security" name="security"/>보안
			<input type="checkbox" class="saveCheckbox" style="float: none;" id="network" name="network"/>네트워크
			<input type="checkbox" class="saveCheckbox" style="float: none;" id="software" name="software"/>S/W
			<input type="checkbox" class="saveCheckbox" style="float: none;" id="etc" name="etc"/>기타
			(<input type="text" class="saveInput" id="inspection_date" name="inspection_date" style="width: 20%">)
		</SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:40;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 2.1pt 1.4pt 2.1pt' rowspan="7">
		<P CLASS=HStyle0 STYLE='text-align:center; width: 100%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>제품정보</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>제품명</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:70%;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px; background: none'>
			<input type="text" class="saveInput" id="solution_name" name="solution_name" style="text-align: left">
		</TD>
	</TR>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>업무명</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:120px;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px; background: none'>
			<input type="text" class="saveInput" id="project_name" name="project_name" style="text-align: left">
		</TD>
	</TR>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>수량</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:120px;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px; background: none'>
			<input type="text" class="saveInput" id="solution_count" name="solution_count" style="text-align: left">
		</TD>
	</TR>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>지원형태</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:120px;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px; background: none'>
			<P CLASS=HStyle0 STYLE='text-align:left;'>
				<SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>
					<input type="checkbox" class="saveCheckbox" style="float: none;" id="visit" name="visit"/>방문
					<input type="checkbox" class="saveCheckbox" style="float: none;" id="remote" name="remote"/>원격
					<input type="checkbox" class="saveCheckbox" style="float: none;" id="suport_etc" name="suport_etc"/>기타
					(<input type="text" class="saveInput" id="inspection_date" name="inspection_date" style="width: 30%">)
				</SPAN>
			</P>
		</TD>
	</TR>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>점검주기</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:120px;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px; background: none'>
			<P CLASS=HStyle0 STYLE='text-align:left;'>
				<SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>
					<input type="checkbox" class="saveCheckbox" style="float: none;" id="monthly" name="monthly"/>월간
					<input type="checkbox" class="saveCheckbox" style="float: none;" id="quarter" name="quarter"/>분기
					<input type="checkbox" class="saveCheckbox" style="float: none;" id="half" name="half"/>반기
					<input type="checkbox" class="saveCheckbox" style="float: none;" id="inspect_cyle_etc" name="inspect_cyle_etc"/>기타
					(<input type="text" class="saveInput" id="inspection_date" name="inspection_date" style="width: 30%">)
				</SPAN>
			</P>
		</TD>
	</TR>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>점검장소</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:120px;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px; background: none'>
			<P CLASS=HStyle0 STYLE='text-align:left;'>
				<SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>
					<input type="checkbox" class="saveCheckbox" style="float: none;" id="monthly" name="monthly"/>월간
					<input type="checkbox" class="saveCheckbox" style="float: none;" id="quarter" name="quarter"/>분기
					<input type="checkbox" class="saveCheckbox" style="float: none;" id="half" name="half"/>반기
					<input type="checkbox" class="saveCheckbox" style="float: none;" id="inspect_cyle_etc" name="inspect_cyle_etc"/>기타
					(<input type="text" class="saveInput" id="inspection_date" name="inspection_date" style="width: 30%">)
				</SPAN>
			</P>
		</TD>
	</TR>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>점검일시</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:120px;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:9px; background: none'>
			<input type="text" class="saveInput" id="inspection_date" name="inspection_date">
		</TD>
	</TR>
</TABLE>

<P CLASS=HStyle14 STYLE='text-align:left;line-height:110%;'>&nbsp;</P>

<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none; width: 100%'>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:50;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px' colspan="2">
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>점검결과</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:50;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>특이사항<br>/<br>조치내역</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:380px;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt; background: none' colspan="3">
		<textarea class="saveTextarea" id="inspect_comment" name="inspect_comment" rows="" cols="" style="width: 100%; height: 100%; resize: none;border: none"></textarea>
	</TD>
</TR>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:50;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>고객의견</P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:72;height:75px;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt; background: none' colspan="3">
		<textarea class="saveTextarea" id="client_comment" name="client_comment" rows="" cols="" style="width: 100%; height: 100%; resize: none;border: none"></textarea>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle14 STYLE='text-align:left;'></P>

<P CLASS=HStyle14 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'><BR></SPAN></P>

<P CLASS=HStyle14 STYLE='text-align:left;line-height:150%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width: 100%'>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:20;height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 2.1pt 1.4pt 2.1pt' rowspan="4">
			<P CLASS=HStyle0 STYLE='text-align:center; width: 100%;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>확<br>인<br>란</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>구분</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>소속</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>연락처</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>성명</SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>점검자</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none;padding:10px'>
			<input type="text" class="saveInput" id="inspector_belong" name="inspector_belong" style="text-align: center">
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none;padding:10px'>
			<input type="text" class="saveInput" id="inspector_phone" name="inspector_phone" style="text-align: center">
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none;padding:10px'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'><input type="text" class="saveInput" id="inspector_name" name="inspector_name" style="text-align: center; width:80%">(인)</SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;padding:10px'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>유지보수</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none;padding:10px'>
			<input type="text" class="saveInput" id="repair_belong" name="repair_belong" style="text-align: center">
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none;padding:10px'>
			<input type="text" class="saveInput" id="repair_phone" name="repair_phone" style="text-align: center">
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none;padding:10px'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'><input type="text" class="saveInput" id="repair_name" name="repair_name" style="text-align: center; width:80%">(인)</SPAN></P>
		</TD>
	</TR>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;padding:10px'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>담당자</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none;padding:10px'>
			<input type="text" class="saveInput" id="client_belong" name="client_belong" style="text-align: center">
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none;padding:10px'>
			<input type="text" class="saveInput" id="client_phone" name="client_phone" style="text-align: center">
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:99;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;background: none;padding:10px'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'><input type="text" class="saveInput" id="client_name" name="client_name" style="text-align: center; width:80%">(인)</SPAN></P>
		</TD>
	</TR>
</TABLE>
<P CLASS=HStyle14 STYLE='text-align:left;line-height:150%;'></P>
<P CLASS=HStyle14 STYLE='text-align:left;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'><BR></SPAN></P>
<P CLASS=HStyle14 STYLE='text-align:left;line-height:110%;'></P>
<P CLASS=HStyle14 STYLE='text-align:left;line-height:140%;margin-top:5px;'>
<TABLE width="100%" style="margin-top:50px">
<TR>
	<TD colspan="3" valign="middle" style='height:38;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:25.0pt;font-family:"맑은 고딕";font-weight:900;line-height:160%'>점검대상 리스트</SPAN></P>
	</TD>
</TR>
</TABLE>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width: 100%'>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px;width: 45px;'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>순번</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width: 15%;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%;'>제조사</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>장비명</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>업무명</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width: 10%;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%;'>수량</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width: 10%;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%;'>비고</SPAN></P>
		</TD>
	</TR>
	<c:forEach begin="1" end="12" step="1" var="count">
		<TR>
			<TD valign="middle" bgcolor="#e5e5e5"  style='height:21;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>${count }</SPAN></P>
			</TD>
			<TD valign="middle" bgcolor="#e5e5e5"  style='height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px; background: none'>
				<input type="text" class="saveInput" id="company_${count }" name="company_${count }" style="text-align: center">
			</TD>
			<TD valign="middle" bgcolor="#e5e5e5"  style='height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px; background: none'>
				<input type="text" class="saveInput" id="solution_name_${count }" name="solution_name_${count }" style="text-align: center">
			</TD>
			<TD valign="middle" bgcolor="#e5e5e5"  style='height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px; background: none'>
				<input type="text" class="saveInput" id="project_name_${count }" name="project_name_${count }" style="text-align: center">
			</TD>
			<TD valign="middle" bgcolor="#e5e5e5"  style='height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px; background: none'>
				<input type="text" class="saveInput" id="count_${count }" name="count_${count }" style="text-align: center">
			</TD>
			<TD valign="middle" bgcolor="#e5e5e5"  style='height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px; background: none'>
				<input type="text" class="saveInput" id="comment_${count }" name="comment_${count }" style="text-align: center">
			</TD>
		</TR>
	</c:forEach>
	
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px;' colspan="4">
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'>총 수량</SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px; background: none'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'></SPAN></P>
		</TD>
		<TD valign="middle" bgcolor="#e5e5e5"  style='height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:10px; background: none'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:bold;line-height:160%'></SPAN></P>
		</TD>
	</TR>
</TABLE>
</BODY>
</HTML>
