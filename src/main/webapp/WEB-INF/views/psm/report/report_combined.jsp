<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script src="${rootPath}/resources/js/jquery/jquery.min.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/amchart/amcharts.js"></script>
<script src="${rootPath}/resources/js/amchart/serial.js"></script>
<script src="${rootPath}/resources/js/amchart/export.min.js"></script>
<script src="${rootPath}/resources/js/amchart/amstock.js"></script>
<script src="${rootPath}/resources/js/amchart/xy.js"></script>
<script src="${rootPath}/resources/js/amchart/radar.js"></script>
<script src="${rootPath}/resources/js/amchart/light.js"></script>
<script src="${rootPath}/resources/js/amchart/pie.js"></script>
<script src="${rootPath}/resources/js/amchart/gantt.js"></script>
<script src="${rootPath}/resources/js/amchart/gauge.js"></script>

<link rel="stylesheet" href="${pageContext.request.scheme }://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/css/export.css" type="text/css" media="all">

<script type="text/javascript" charset="UTF-8">
	rootPath = '${rootPath}';
	contextPath = '${pageContext.servletContext.contextPath}';
	var start_date = '${start_date}';
	var end_date = '${end_date}';
	var stDate = '${stDate}';
	var edDate = '${edDate}';
	var strReq = '${CACHE_REQ_TYPE}';
	var period_type = '${period_type}';
	var system_seq = '${system_seq}';
	var use_studentId = '${use_studentId}';
	var download_type = '${download_type}';
	var pdfFullPath = '${pdfFullPath}';
</script>

<script src="${rootPath}/resources/js/psm/report/report_combined.js"></script>
<script src="${rootPath}/resources/js/common/ajax-util.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/common-prototype-util.js" type="text/javascript" charset="UTF-8"></script>

<script type="text/javascript">


var saveCnt = 0;
function exportChartToImg() {
	var charts = {};
	
	for (var x = 0; x < AmCharts.charts.length; x++) {
		$("#"+AmCharts.charts[x].div.id).css("width","75%");
	}
	
 	var lts = setInterval(function() {
		for (var x = 0; x < AmCharts.charts.length; x++) {
			charts[AmCharts.charts[x].div.id] = AmCharts.charts[x];
		}
		for ( var x in charts) {
			var chart = charts[x];
			chart["export"].capture({}, function() {
				this.toJPG({}, function(data) {
					var chartName = this.setup.chart.div.id;
					$("img[name="+chartName+"]").attr("src", data);
					$("img[name="+chartName+"]").css("display","");
					console.log(chartName);
					$("#"+chartName).css("display","none");
					saveCnt++;
					if(saveCnt == AmCharts.charts.length) {
						$("img[name=logoImg]").attr("src",getBase64Image(document.getElementById("logoImg")));
						$("img[name=logoImg]").css("display","");
						$("#logoImg").remove();
						//exportDocs();
						
					}
				});
			});
			clearInterval(lts);
		}
	}, 500);
}

function getBase64Image(img) {
  var canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);
  var dataURL = canvas.toDataURL("image/png");
  return dataURL; 
}
	
function exportDocs(inter) {
	var date = new Date();
	var year = date.getFullYear();
	var month = new String(date.getMonth() + 1);
	var day = new String(date.getDate());
	
	var admin_user_id = "${userSession.admin_user_id}";
	
	var reportType = '${report_type}';
	
	var reportdate = "${start_date}".split('-');
	var reportyear = reportdate[0];
	var reportmonth = reportdate[1];
	var reporthalf = reportmonth < 7 ? '상반기':'하반기';
	
	var log_message_title = reportyear+'년_'+reporthalf+'_반기별_수준진단보고서';	
	
	var proc_date = reportyear+reportmonth;
	
	if(month.length == 1){ 
		month = "0" + month; 
	} 
	if(day.length == 1){ 
		day = "0" + day; 
	} 
	var regdate = year + "" + month + "" + day;
	var agent = navigator.userAgent.toLowerCase();
	
	$('.updatetd').html("");
	$('.worddelete').remove();
	
	var header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' "+
    "xmlns:w='urn:schemas-microsoft-com:office:word' "+
    "xmlns='http://www.w3.org/TR/REC-html40'>"+
    "<head><meta charset='utf-8'><title>Export HTML to Word Document with JavaScript</title></head><body>";
	var footer = "</body></html>";
	var sourceHTML = header+document.getElementById("source-html").innerHTML+footer;
	var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/addReport.html',
		data: { 
			"source" : source,
			"log_message_title" : log_message_title,
			"admin_user_id" : admin_user_id,
			"report_type" : reportType,
			"proc_date" : proc_date,
			"pdfFullPath" : pdfFullPath,
			"period_type" : period_type
		},
		success: function(data) {
			if(data == 1) {
				alert("보고서 등록 성공");
				self.close();
			} else if(data == 2) {
				if(confirm('기존 보고서가 있습니다. \n\n덮어 쓰시겠습니까?')){
					alert("보고서 재등록 성공");
					self.close();
				}
			} else {
				alert("보고서 등록 실패");
			}
			$('#loading').hide();
		}
	});

	clearInterval(inter);
	
}

function saveWordReport() {
	$('#loading').show();
	exportChartToImg();
	var inter = setInterval(function() {
		if(saveCnt == AmCharts.charts.length) {
			exportDocs(inter);
		}	
	}, 500)
}

	function printpr()
	{
		$("#printButton").hide();
		$("#saveButton").hide();
		
		//window.print();
		//self.close();
		
		var $form = $("#manageHistForm");
		var log_message_params = '';
		var menu_id = '${menu_id}';
		var log_message_title = '반기별_수준진단보고 출력';
		var log_action = 'PRINT';
		var type = 'add';
		var url = '${rootPath}/report/download.html';
		
		$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
		sendAjaxPostRequest(url, $form.serialize(), ajaxReport_successHandler, ajaxReport_errorHandler, type);
	}
	
	
	rootPath = '${rootPath}';
	contextPath = '${pageContext.servletContext.contextPath}';
	var start_date = '${start_date}';
	var end_date = '${end_date}';
	var system_seq = '${system_seq}';
	var period_type = '${period_type}';
	var half_type = '${half_type}';
	var scheme = '${pageContext.request.scheme}';
	var serverName = '${pageContext.request.serverName}';
	var port = '${pageContext.request.serverPort}';

	function ajaxReport_successHandler(data, dataType, actionType){
		if(data.hasOwnProperty("statusCode")) {
			var statusCode = data.statusCode;
			if(statusCode == 'SYS006V') {
				location.href = "${rootPath}/loginView.html";
				return false;
			}
			
			if(data.hasOwnProperty("message")) {
				var message = data.message;
				alert(message);
			}
		}else{
			window.print();
			self.close();
		}
	}

	// 관리자 ajax call - 실패
	function ajaxReport_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

		log_message += codeLogMessage[actionType + "Action"];
		
		alert("실패하였습니다." + textStatus);
	}
</script>

<form id="manageHistForm" method="POST">
</form>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>

<HEAD>
<META NAME="Generator" CONTENT="Hancom HWP 8.5.8.1555">
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">
<TITLE>개인정보 접속기록 점검보고서</TITLE>
<STYLE type="text/css">
<!--
p.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle14
	{style-name:"td"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle14
	{style-name:"td"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle14
	{style-name:"td"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle15
	{style-name:"xl65"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; font-size:10.0pt; font-family:돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle15
	{style-name:"xl65"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; font-size:10.0pt; font-family:돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle15
	{style-name:"xl65"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; font-size:10.0pt; font-family:돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle16
	{style-name:"xl66"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:100%; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle16
	{style-name:"xl66"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:100%; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle16
	{style-name:"xl66"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:100%; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle17
	{style-name:"xl67"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; background-color:#ffff00; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle17
	{style-name:"xl67"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; background-color:#ffff00; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle17
	{style-name:"xl67"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; background-color:#ffff00; font-size:11.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
-->
</STYLE>
</HEAD>

<BODY>
<c:set var="startdate" value="${fn:split(start_date, '-') }"/>
<c:set var="enddate" value="${fn:split(end_date, '-') }"/>
<c:set var="compareDate" value="${fn:split(compareDate, '-') }"/>
<c:set var="date" value="${fn:split(date, '-') }"/>
<c:choose>
	<c:when test="${period_type == 1 }">
		<c:set var="cur" value="${startdate[1]-0 } 월"/>
		<c:set var="pre" value="이전월"/>
	</c:when>
	<c:when test="${period_type == 2 }">
		<c:choose>
		<c:when test="${startdate[1] == '01' }">
		<c:set var="cur" value="1분기"/>
		</c:when>
		<c:when test="${startdate[1] == '04' }">
		<c:set var="cur" value="2분기"/>
		</c:when>
		<c:when test="${startdate[1] == '07' }">
		<c:set var="cur" value="3분기"/>
		</c:when>
		<c:when test="${startdate[1] == '10' }">
		<c:set var="cur" value="4분기"/>
		</c:when>
		</c:choose>
		<c:set var="pre" value="이전 분기"/>
	</c:when>
	<c:when test="${period_type == 3 }">
		<c:choose>
		<c:when test="${startdate[1] == '01' }">
		<c:set var="cur" value="상반기"/>
		</c:when>
		<c:when test="${startdate[1] == '07' }">
		<c:set var="cur" value="하반기"/>
		</c:when>
		</c:choose>
		<c:set var="pre" value="이전 반기"/>
	</c:when>
	<c:when test="${period_type == 4 }">
		<c:set var="cur" value="${startdate[0]-0 } 년"/>
		<c:set var="pre" value="이전년"/>
	</c:when>
	<c:otherwise>
		<c:set var="cur" value="기간"/>
		<c:set var="pre" value="이전 기간"/>
	</c:otherwise>
</c:choose>
<div id="source-html">
<div align="right" id="buttonDiv">
	<input type=button id="printButton" name="printButton" onclick="printpr();" value="출력"/>
	<input type=button id="saveButton" name="saveButton" onclick="saveWordReport();" value="생성"/>
</div>

<div align="center">
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" style='width:627;height:142;border-left:double #000000 1.4pt;border-right:double #000000 1.4pt;border-top:double #000000 1.4pt;border-bottom:double #000000 1.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:150%;'><SPAN STYLE='font-size:19.0pt;font-family:"돋움";letter-spacing:-4%;font-weight:"bold";font-style:"italic";line-height:150%'>개인정보의 안전한 관리를 위한</SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:150%;'><SPAN STYLE='font-size:25.0pt;font-family:"돋움";letter-spacing:-4%;font-weight:"bold";line-height:150%'>개인정보 접속기록의 주기적 점검 결과</SPAN></P>
	</TD>
</TR>
</TABLE>
</div>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'>
<SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#000000;line-height:180%'>
<c:if test="${fn:length(systems) == 1 }" >
	${systems[0].system_name } 시스템
</c:if>
</SPAN>
</P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<%-- <P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'><SPAN STYLE='font-size:20.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";line-height:180%'>

<input type="text" value="${date[0] }.${date[1] } " style="text-align:center;border: 0;font-size:20.0pt;font-family:'한양중고딕,한컴돋움';font-weight:'bold';line-height:180%"/>
</SPAN></P> --%>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'>

<c:if test="${use_reportLogo eq 'Y' }">
<div align="center">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" style='width:342;height:77;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<c:choose>
		<c:when test="${filename eq null }" >
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:18.0pt;line-height:160%'>
				<img id="logoImg" src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}${logo_report_url}">
				<img name="logoImg" style="display: none">
			</SPAN></P>
		</c:when>
		<c:otherwise>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:18.0pt;line-height:160%'>
				<img id="logoImg" src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/upload/${filename }">
				<img name="logoImg" style="display: none">
			</SPAN></P>
		</c:otherwise>
	</c:choose>
	</TD>
</TR>
</TABLE>
</div>
</c:if>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'>

<c:if test="${use_reportLine eq 'Y' }">
<div align="center">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD rowspan="2" valign="middle" style='width:27;height:114;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-top:2.4pt;text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>결</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-top:2.4pt;text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>재</SPAN></P>
	</TD>
	<TD valign="middle" style='width:91;height:35;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-top:2.4pt;text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>담당</SPAN></P>
	</TD>
	<TD valign="middle" style='width:100;height:35;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-top:2.4pt;text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>CPO</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:91;height:79;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-top:2.4pt;text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>&nbsp;</SPAN></P>
	</TD>
	<TD valign="middle" style='width:100;height:79;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-top:2.4pt;text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>&nbsp;</SPAN></P>
	</TD>
</TR>
</TABLE>
</div>
</c:if>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'>
<div style='page-break-before:always'></div><br style="page-break-before: always">

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'>
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:100% '>
<TR>
	<TD valign="middle" bgcolor="#4e9fd7"  style='width:100% ;height:44px;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0><SPAN STYLE='width:100% ;font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;line-height:160%'>ㅁ   목차</SPAN></P>
	</TD>
</TR>
</TABLE>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;margin-top:8.0pt;line-height:180%;'> <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>Ⅰ. 점검 개요</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>1.	점검 목적</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>2.	관련 근거</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>3.	점검 범위 및 방법</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>4.	점검 일시</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>5.	점검 대상</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:41.7pt;text-indent:-23.4pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>
<P CLASS=HStyle2 STYLE='margin-left:0.0pt;margin-top:8.0pt;line-height:180%;'> <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>Ⅱ. 점검 결과</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>1.	개인정보 접속기록 현황</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>2.	개인정보 처리 현황</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>3.	고유식별정보 처리 현황</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>4.	개인정보 접속기록 점검 결과</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:41.7pt;text-indent:-23.4pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>
<P CLASS=HStyle2 STYLE='margin-left:0.0pt;margin-top:8.0pt;line-height:180%;'> <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>[별쳠]</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>1.	개인정보 접속기록 상세 결과</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>2.	개인정보 처리 상세 결과</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>3.	고유식별정보 처리 상세 결과</SPAN></P>

<div style='page-break-before:always'></div><br style="page-break-before: always">

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'>
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:100% '>
<TR>
	<TD valign="middle" bgcolor="#4e9fd7"  style='width:100% ;height:44px;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0><SPAN STYLE='width:100% ;font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;line-height:160%'>Ⅰ. 점검 개요</SPAN></P>
	</TD>
</TR>
</TABLE>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;margin-top:8.0pt;line-height:180%;'> <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>1. 점검 목적</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.5pt;text-indent:-21.2pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ </SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-3%;line-height:180%'>개인정보처리시스템의 접속기록을 점검하여 불법적인 접근 및 비정상</SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;line-height:180%'>행위 등을 탐지 분석하여 개인정보의 오·남용 사례를 방지하고</SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'> 안전하게 관리할 수 있도록 관리</SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;line-height:180%'>·</SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>감독을 수행함</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:41.7pt;text-indent:-23.4pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>
<P CLASS=HStyle2 STYLE='margin-left:0.0pt;margin-top:8.0pt;line-height:180%;'> <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>2. 관련 근거</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보보호법 제29조 (안전조치의무)</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보보호법 시행령 제30조 (개인정보의 안전성 확보 조치)</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ </SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-3%;line-height:180%'>개인정보의 안전성 확보조치 기준 제8조(접속기록의 보관 및 점검)</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:41.7pt;text-indent:-23.4pt;line-height:180%;'>

<!-- <div align="center">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" style='width:595;height:195;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:15.4pt;text-indent:-15.2pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'>① </SPAN><SPAN STYLE='font-family:"돋움";letter-spacing:-1%'>개인정보처리자는 개인정보취급자가 개인정보처리시스템에 접속한 기록을 6개월 이상 보관·</SPAN><SPAN STYLE='font-family:"돋움"'>관리하여야 한다.</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:16.8pt;text-indent:-16.6pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'>② 개인정보처리자는 개인정보의 분실·도난·유출·위조·변조 또는 훼손 등에 대응하기 위하여 </SPAN><SPAN STYLE='font-family:"돋움";font-weight:"bold";text-decoration:"underline"'>개인정보처리시스템의 접속기록 등을 반기별로 1회 이상 점검하여야 한다.</SPAN><SPAN STYLE='font-family:"돋움"'> </SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:16.8pt;text-indent:-16.6pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'>③ 개인정보처리자는 개인정보취급자의 접속기록이 위·변조 및 도난, 분실되지 않도록 해당 접속기록을 안전하게 보관하여야 한다.</SPAN></P>
	</TD>
</TR>
</TABLE>
</div> -->

<div align="left" style="margin-left: 10px">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" style='width:650;height:195;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:15.4pt;text-indent:-15.2pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'>① </SPAN><SPAN STYLE='font-family:"돋움";letter-spacing:-1%'>개인정보처리자는 개인정보취급자가 개인정보처리시스템에 접속한 기록을 1년 이상  보관·관리하여야 한다. 다만, 5만명 이상의 정보주체에 관하여 개인정보를 처리하거나, 고유식별정보 또는 민감정보를 처리하는 개인정보처리시스템의 경우에는 2년 이상 보관‧관리하여야 한다.</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:16.8pt;text-indent:-16.6pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'>② 개인정보처리자는 개인정보의 오‧남용, 분실·도난·유출·위조·변조 또는 훼손 등에 대응하기 위하여 개인정보처리시스템의 접속기록 등을 월 1회 이상 점검하여야 한다. 특히 개인정보를 다운로드한 것이 발견되었을 경우에는 내부 관리계획으로 정하는 바에 따라 그 사유를 반드시 확인하여야 한다.</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:16.8pt;text-indent:-16.6pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'>③ 개인정보처리자는 개인정보취급자의 접속기록이 위·변조 및 도난, 분실되지 않도록 해당 접속기록을 안전하게 보관하여야 한다.</SPAN></P>
	</TD>
</TR>
</TABLE>
</div>

<P CLASS=HStyle0 STYLE='margin-left:41.7pt;text-indent:-23.4pt;line-height:180%;'></P>
<P CLASS=HStyle5 STYLE='margin-left:41.7pt;text-indent:-23.4pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'> <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>3. 점검 범위 및 방법</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 점검 범위</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:54.5pt;text-indent:-36.2pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;- </SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>

${startdate[0] }년 ${startdate[1] }월 ${startdate[2] }일 ~ ${enddate[0] }년 ${enddate[1] }월 ${enddate[2] }일까지의 개인정보 접속기록 로그
</SPAN>
</P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 점검 방법</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:54.5pt;text-indent:-36.2pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;- </SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>
개인정보 처리시스템 별 개인정보 처리량 관측</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:54.5pt;text-indent:-36.2pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;- </SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>
월별 / 분기별 개인정보 처리량 통계 확보</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:41.7pt;text-indent:-23.4pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'> <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>4. 점검 일시</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>
❍&nbsp;<input type="text" value="${date[0] }년 ${date[1] }월 ${date[2] }일" style="border:0;text-align:left;font-size:14.0pt;font-family:'한양신명조,한컴돋움';"/>
</SPAN>
</P>

<div style='page-break-before:always'></div><br style="page-break-before: always">
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'> <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>5. 점검 대상</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍업무망 개인정보 처리시스템 접속기록 수집</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'>

<div align="left" style="margin-left: 10px">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:47;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:280;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>개인정보처리시스템</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:280;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>URL</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:60;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>비고</SPAN></P>
	</TD>
</TR>
<c:forEach var="sys" items="${systems }" varStatus="status">
	<TR>
		<TD valign="middle" bgcolor="#e6eef7"  style='width:47;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>${status.count }</SPAN></P>
		</TD>
		<TD valign="middle" style='width:280;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${sys.system_name }</SPAN></P>
		</TD>
		<TD valign="middle" style='width:280;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${sys.main_url }</SPAN></P>
		</TD>
		<TD valign="middle" style='width:60;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'></SPAN></P>
		</TD>
	</TR>
</c:forEach>
</TABLE>
</div>
<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>


<P CLASS=HStyle0 STYLE='text-align:left;line-height:180%;'>
<div style='page-break-before:always'></div><br style="page-break-before: always">
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'>
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:100% '>
<TR>
	<TD valign="middle" bgcolor="#4e9fd7"  style='width:100% ;height:44px;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0><SPAN STYLE='width:100% ;font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;line-height:160%'>Ⅱ. 점검 결과</SPAN></P>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle0 STYLE='text-align:left;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:225%'> </SPAN><SPAN STYLE='font-size:15.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:225%'>1. 총평</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;○ 개인정보 접속기록 현황</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:61.6pt;text-indent:-43.3pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;- </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>
${startdate[0] }년 ${startdate[1] }월 ${startdate[2] }일</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>부터</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;color:#0000ff;line-height:180%'> </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>
${enddate[0] }년 ${enddate[1] }월 ${enddate[2] }일</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>까지 총 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>
${diffMon }</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>개월 동안 발생한</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-4%;line-height:180%'> </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;line-height:180%'>개인정보 접속기록 로그 총 횟수는</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;color:#0000ff;line-height:180%'> </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;font-weight:"bold";color:#0000ff;line-height:180%'>
<fmt:formatNumber value="${logCnt }" pattern="#,###" />
</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;line-height:180%'>건으로,</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;color:#0000ff;line-height:180%'> </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;line-height:180%'>개인정보</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-1%;line-height:180%'> 처리시스템별로는</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'> </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";font-weight:"bold";color:#0000ff;line-height:180%'>
${reportBySys }</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";color:#0000ff;line-height:180%'>, </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>소속별로는 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";font-weight:"bold";color:#0000ff;line-height:180%'>
${reportByDept }</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>의 경우가 개인정보 처리량이 가장 많은 것으로 나타남</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-align:center;text-indent:-24.3pt;line-height:180%;'>
<SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>
<div class='downloadChartClass' align="center" id="chart1" style="width:750px; height: 450px; float: left;"></div>
<img name="chart1" style="width:750px; height: 450px; float: left; display: none;">
</SPAN>
</P>

<div align="left" style="margin-left: 10px">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:123;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>개인정보처리시스템</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:123;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>개인정보 이용량</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:123;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>비고</SPAN></P>
	</TD>
</TR>
<c:forEach var="sysList" items="${reportBySysList }" varStatus="status">
	<TR>
		<TD valign="middle" style='width:302;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${sysList.system_name }</SPAN></P>
		</TD>
		<TD valign="middle" style='width:250;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${sysList.cnt }</SPAN></P>
		</TD>
		<TD valign="middle" style='width:87;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'></SPAN></P>
		</TD>
	</TR>
</c:forEach>
	<TR>
		<TD valign="middle" style='width:302;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>합계</SPAN></P>
		</TD>
		<TD valign="middle" style='width:250;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${logCnt }</SPAN></P>
		</TD>
		<TD valign="middle" style='width:87;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'></SPAN></P>
		</TD>
	</TR>
</TABLE>
</div>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'></P>

<div align="center">
<P CLASS=HStyle0 STYLE='margin-left:54.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>* 개인정보 이용량은 개인정보취급자가 정보주체의 개인정보를 처리하기 위해 시도한 횟수를 기록한 값</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:54.3pt;text-indent:-24.3pt;line-height:135%;'><SPAN STYLE='font-family:"맑은 고딕"'>&nbsp;(예 : 접속기록 1건 내 5가지 유형의 개인정보가 처리됨 =&gt; 이용량 1건 / 처리량 5건)</SPAN></P>
</div>

<P CLASS=HStyle0 STYLE='margin-top:20.0pt;margin-bottom:20.0pt;text-align:center;line-height:180%;'>


<div style='page-break-before:always'></div><br style="page-break-before: always">

<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:15.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:160%'>2. 개인정보 처리 현황</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;○ 시스템별 처리현황</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:23.2pt;text-indent:-23.2pt;line-height:180%;'>
<SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;&nbsp;- </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;line-height:180%'>개인정보의 시스템별 처리량은</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;color:#0000ff;line-height:180%'> </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;font-weight:"bold";color:#0000ff;line-height:180%'>
<span id="total_cnt"></span>건</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;line-height:180%'>이며, </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'> </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";font-weight:"bold";color:#0000ff;line-height:180%'><span id="max_system"></span>시스템에서 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>의 가장</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'> 많은</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";font-weight:"bold";color:#0000ff;line-height:180%'>(<span id="max_amount"></span>건)</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'> 개인정보가 처리됨</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:23.2pt;text-indent:-23.2pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:23.2pt;text-indent:-23.2pt;line-height:180%;'>
<SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>
<div class='downloadChartClass' align="center" id="chart3" style="width:750px; height: 450px; float: left;"></div>
<img name="chart3" style="width:750px; height: 450px; float: left; display: none;">
</SPAN>
</P>

<div align="left" style="margin-left: 10px">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:123;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>개인정보처리시스템</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:123;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>개인정보 이용량</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:123;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>비고</SPAN></P>
	</TD>
</TR>
<c:forEach var="sysList" items="${reporProcessBySysList }" varStatus="status">
	<TR>
		<TD valign="middle" style='width:302;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${sysList.system_name }</SPAN></P>
		</TD>
		<TD valign="middle" style='width:250;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${sysList.cnt }</SPAN></P>
		</TD>
		<TD valign="middle" style='width:87;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'></SPAN></P>
		</TD>
	</TR>
</c:forEach>
	<TR>
		<TD valign="middle" style='width:302;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>합계</SPAN></P>
		</TD>
		<TD valign="middle" style='width:250;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${privCnt }</SPAN></P>
		</TD>
		<TD valign="middle" style='width:87;height:20;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'></SPAN></P>
		</TD>
	</TR>
</TABLE>
</div>

<div align="center">
<P CLASS=HStyle0 STYLE='margin-left:54.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>* 개인정보 처리량은 개인정보를 처리하기 위한 접속기록 內 처리된 개인정보 유형들의 수량입니다.</SPAN></P>
<P CLASS=HStyle0 STYLE='margin-left:54.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-family:"맑은 고딕"'>&nbsp;(예 : 접속기록 1건 내 5가지 유형의 개인정보가 처리됨 =&gt; 이용량 1건 / 처리량 5건)</SPAN></P>
</div>

<P CLASS=HStyle0 STYLE='margin-top:20.0pt;margin-bottom:20.0pt;text-align:center;line-height:180%;'>


<div style='page-break-before:always'></div><br style="page-break-before: always">

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;○ 월별 접속기록 현황</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:61.6pt;text-indent:-43.3pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;- </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>개인정보의 월별 처리량은 평균 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;color:#0000ff;line-height:180%'> </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;font-weight:"bold";color:#0000ff;line-height:180%'>
<span id="avg_cnt4"></span>
</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;line-height:180%'>건이며,</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'> </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";font-weight:"bold";color:#0000ff;line-height:180%'>
<span id="max_month4"></span>월</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>에 개인정보 처리량이 가장 많은 것으로 나타남</SPAN>
</P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-align:center;text-indent:-24.3pt;line-height:180%;'>
<SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>&nbsp;
<div class='downloadChartClass' align="center" id="chart4" style="width:750px; height: 450px; float: left;"></div>
<img name="chart4" style="width:750px; height: 450px; float: left; display: none;">
</SPAN>
</P>

<P CLASS=HStyle0 STYLE='text-align:center;'></P>

<div align="left" style="margin-left: 10px">
<TABLE id="table4" border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<thead>
<TR>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:123;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>월별</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:197;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>월별 처리량</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:231;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>전월대비 증감량</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:87;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>비고</SPAN></P>
	</TD>
</TR>
</thead>
<tbody>
</tbody>
</TABLE>
</div>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-align:center;text-indent:-24.3pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-left:54.3pt;text-indent:-24.3pt;line-height:135%;'><SPAN STYLE='font-family:"맑은 고딕"'>* 개인정보 처리량은 개인정보를 처리하기 위한 접속기록 內 처리된 개인정보 유형의 수량입니다.</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:54.3pt;text-indent:-24.3pt;line-height:135%;'><SPAN STYLE='font-family:"맑은 고딕"'>&nbsp;(예 : 접속기록 1건 내 5가지 유형의 개인정보가 처리됨 =&gt; 이용량 1건 / 처리량 5건)</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:135%;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:135%'><BR></SPAN></P>


<div style='page-break-before:always'></div><br style="page-break-before: always">

<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:15.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:160%'>3. 부서별 현황</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:61.6pt;text-indent:-43.3pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;- </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>개인정보를 가장 취급하는 부서 중 개인정보를 가장 많이 처리하는 부서는 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;color:#0000ff;line-height:180%'></SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;font-weight:"bold";color:#0000ff;line-height:180%'>
${max_dept }
</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;line-height:180%'>로 나타남 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'> </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";font-weight:"bold";color:#0000ff;line-height:180%'>
(${compareDate[0] }년 ${compareDate[1] }월 ~ ${enddate[0] }년 ${enddate[1] }월)</SPAN>
</P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-align:center;line-height:180%;'>
<SPAN STYLE='font-size:15.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>
<div class='downloadChartClass' align="center" id="chart5" style="width:750px; height: 450px; float: left;"></div>
<img name="chart5" style="width:750px; height: 450px; float: left; display: none;">
</SPAN>
</P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-align:center;line-height:180%;'></P>

<div align="left" style="margin-left: 10px">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:44;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:141;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>부서</SPAN></P>
	</TD>
	<c:forEach items="${monthList }" var="data" varStatus="status">
		<TD valign="middle" bgcolor="#e6eef7"  style='width:54;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold";'>${data}</SPAN></P>
		</TD>
	</c:forEach>
	<TD valign="middle" bgcolor="#e6eef7"  style='width:122;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>개인정보 이용량</SPAN></P>
	</TD>
</TR>
<c:forEach items="${table5 }" var="data" varStatus="status">
<TR>
	<TD valign="middle" style='width:44;height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕"'>${status.count }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:141;height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle15><SPAN STYLE='font-family:"맑은 고딕"'>${data.dept_name }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:54;height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle15><SPAN STYLE='font-family:"맑은 고딕";'><fmt:formatNumber value="${data.type1}" pattern="#,###" /></SPAN></P>
	</TD>
	<TD valign="middle" style='width:54;height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle15><SPAN STYLE='font-family:"맑은 고딕";'><fmt:formatNumber value="${data.type2}" pattern="#,###" /></SPAN></P>
	</TD>
	<TD valign="middle" style='width:54;height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle15><SPAN STYLE='font-family:"맑은 고딕";'><fmt:formatNumber value="${data.type3}" pattern="#,###" /></SPAN></P>
	</TD>
	<TD valign="middle" style='width:54;height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle15><SPAN STYLE='font-family:"맑은 고딕";'><fmt:formatNumber value="${data.type4}" pattern="#,###" /></SPAN></P>
	</TD>
	<TD valign="middle" style='width:54;height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle15><SPAN STYLE='font-family:"맑은 고딕";'><fmt:formatNumber value="${data.type5}" pattern="#,###" /></SPAN></P>
	</TD>
	<TD valign="middle" style='width:54;height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle15><SPAN STYLE='font-family:"맑은 고딕";'><fmt:formatNumber value="${data.type6}" pattern="#,###" /></SPAN></P>
	</TD>
	<TD valign="middle" style='width:122;height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle15><SPAN STYLE='font-family:"맑은 고딕"'><fmt:formatNumber value="${data.type99}" pattern="#,###" /></SPAN></P>
	</TD>
</TR>
</c:forEach>
</TABLE>
</div>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-indent:-24.3pt;line-height:135%;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:135%'><BR></SPAN></P>


<div style='page-break-before:always'></div><br style="page-break-before: always">

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 고유식별정보 처리 현황</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:61.6pt;text-indent:-43.3pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;- </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>
${startdate[0] }년 ${startdate[1] }월 ${startdate[2] }일</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>부터 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>
${enddate[0] }년 ${enddate[1] }월 ${enddate[2] }일</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>까지 총 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>
${diffMon }</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>개월 동안 처리한 고유식별정보는 총 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;font-weight:"bold";color:#0000ff;line-height:180%'>
<fmt:formatNumber value="${chart7TotalCnt }" pattern="#,###" />
</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;line-height:180%'>건이며, 그 중 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";font-weight:"bold";color:#0000ff;line-height:180%'>
${topType }</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>를 가장 많이 처리하고 있음</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:61.6pt;text-indent:-43.3pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;- 각 고유식별정보별 처리건수</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'>
<div align="left" style="margin-left: 10px">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:96;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:295;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>고유식별정보</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:212;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>건수</SPAN></P>
	</TD>
</TR>
<c:forEach var="report" items="${typeCnt }" varStatus="status">
<TR>
	<TD valign="middle" style='width:96;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:295;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${report.privacy_desc }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:212;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${report.cnt }" pattern="#,###" /></SPAN></P>
	</TD>
</TR>
</c:forEach>

</TABLE>
</div>
</P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'>
<div align="center">
<div class='downloadChartClass' id="chart7" style="width:750px; height: 300px; float: left;"></div>
<img name="chart7" style="width:750; height: 300px; float: left; display: none;">
<%-- <div align="center"><img id="chartImage0" style="display: none;" src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/chartImages3/chart1.png"></div> --%>
</div>	
</P>

<div style='page-break-before:always'></div><br style="page-break-before: always">


<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-align:center;text-indent:-24.3pt;line-height:180%;'><SPAN STYLE='font-size:15.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>&nbsp;</SPAN></P>

<P CLASS=HStyle0 STYLE='text-align:left;line-height:180%;'>
<TABLE width="100%" border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
</TABLE>
<P CLASS=HStyle0 STYLE='text-align:left;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:15.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:225%'>1. 개인정보 접속기록 점검 결과</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:19.5pt;text-indent:-19.5pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";letter-spacing:-5%;line-height:180%'>&nbsp;- </SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";letter-spacing:-7%;line-height:180%'>${compareDate[0] }년 ${compareDate[1] }월 ${compareDate[2] }일 ~ ${enddate[0] }년 ${enddate[1] }월 ${enddate[2] }일까지 총 ${compareDiffMon } 개월 동안 발생한 </SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";letter-spacing:-2%;line-height:180%'>개인정보 </SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>처리량은 </SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'><!-- <span id="total_cnt2"></span> --></SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>건 이고, </SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'>개인정보의 </SPAN><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>월 평균처리량은 <!-- <span id="avg_total"></span> -->건임</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-align:center;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>[개인정보 전체 처리현황 증빙자료 ${compareDate[1] }월]</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-align:center;line-height:180%;'></P>
<TABLE id="uploadtable1" align="center" border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD class="updatetd" valign="middle" style='width:635;height:300;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${compareDate[1] }월 ${compareDate[2] }일자 접속기록 수집화면을 캡쳐해 주세요.</P>
	<div align="center">
	<form id="uploadForm1" enctype="multipart/form-data" method="POST">
    	<input type="file" name="filename" id="filename" accept=".gif, .jpg, .png" />
        <input type="button" value="등록" onclick="uploadImage1()"/>
    </form>
    </div>
    </TD>
</TR>
</TABLE>
<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-align:center;text-indent:-24.3pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-align:center;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:180%'>[개인정보 전체 처리현황 증빙자료 ${enddate[1] }월]</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-align:center;line-height:180%;'></P>
<TABLE id="uploadtable2" align="center" border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD class="updatetd" valign="middle" style='width:635;height:300;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>${enddate[1] }월 ${enddate[2] }일자 접속기록 수집화면을 캡쳐해 주세요.</P>
	<div align="center">
	<form id="uploadForm2" enctype="multipart/form-data" method="POST">
    	<input type="file" name="filename" id="filename" accept=".gif, .jpg, .png" />
        <input type="button" value="등록" onclick="uploadImage2()"/>
    </form>
    </div>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle0 STYLE='margin-left:24.3pt;text-align:center;text-indent:-24.3pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='text-align:left;'><SPAN STYLE='font-size:15.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:225%'>2. 접속기록 사후처리</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:10.9pt;line-height:180%;'></P>
<TABLE align="center" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" style='width:650;height:200;'>
<!-- 	<P CLASS=HStyle0 STYLE='margin-left:10.7pt;line-height:180%;'><SPAN STYLE='font-family:"문체부 궁체 흘림체";color:#7f7f7f'>&nbsp;</SPAN></P> -->
		<TEXTAREA rows="12" style="width: 100%;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;"></TEXTAREA>
	</TD>
</TR>
</TABLE>
<P CLASS=HStyle0 STYLE='margin-left:10.9pt;text-indent:-10.9pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-left:10.7pt;text-indent:-10.7pt;line-height:180%;'><SPAN STYLE='font-size:12.0pt;font-family:"맑은 고딕";line-height:180%'><BR></SPAN></P>
</div>

<div style='page-break-before:always'></div><br style="page-break-before: always">

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'>
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:100%;'>
<TR>
	<TD valign="middle" bgcolor="#4e9fd7"  style='width:100%;height:44;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;width:100%;line-height:160%'>Ⅲ. 상세 결과</SPAN><SPAN STYLE='font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'> </SPAN></P>
	</TD>
</TR>
</TABLE>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>1. 개인정보 접속기록 상세점검 결과</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보 접속기록 전체현황</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;- 처리한 개인정보의 전체 건수는 
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";font-weight:"bold";color:#0000ff;line-height:180%'>
<fmt:formatNumber value="${privCnt }" pattern="#,###" />
</SPAN>
건으로 나타남</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'>
<div class='downloadChartClass' id="chart8" name="chart8" style="width:750px; height: 250px; float: left; cursor: pointer;"></div>
<img name="chart8" style="width:750px; height: 250px; float: left; display: none;">
</P>
<br>
<div align="left" style="margin-left: 10px">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:235;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>날짜</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 건수</SPAN></P>
	</TD>
<!-- 
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>평균</SPAN></P>
	</TD>
 -->
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>증감</SPAN></P>
	</TD>
</TR>

<c:forEach items="${privTypeCount}" var="i" varStatus="status">
		<TR>
			<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:235;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${i.proc_date}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.cnt}" pattern="#,###" /></SPAN></P>
			</TD>
<%-- 			
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${privTypeCountAvg}" pattern="#,###" /></SPAN></P>
			</TD>
 --%>			
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			
			<c:if test="${i.tot_cnt == '-'}">
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#000000'>-</SPAN></P>
			</c:if>
			<c:if test="${i.tot_cnt != '-' && i.bCheckPlus == '1' }">
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>▲ <fmt:formatNumber value="${i.tot_cnt}" pattern="#,###" /></SPAN></P>
			</c:if>
			<c:if test="${i.tot_cnt != '-' && i.bCheckPlus == '0'}">
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#0000ff'>▼ <fmt:formatNumber value="${i.tot_cnt}" pattern="#,###" /></SPAN></P>
			</c:if>
			</TD>
		</TR>
</c:forEach>

</TABLE>
</div>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보 접속기록 <c:if test="${period_type == 1 }">월별, </c:if>시간별 추이 </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>(단위: 처리한 개인정보 건수)</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'>
<c:if test="${period_type == 1 }">
<div class='downloadChartClass' id="chart9_1" style="width:750px; height: 200px; float: left;"></div>
<div align="center"><img name="chart9_1" style="width:750px; height: 200px; float: left; display: none;"></div>
</c:if>
<br><br>
<div class='downloadChartClass' id="chart9_3" style="width:750px; height: 200px; float: left;"></div><div style="clear: both;"></div>
<div align="center"><img name="chart9_3" style="width:750px; height: 200px; float: left; display: none;"></div>
</P>

<div style='page-break-before:always'></div><br style="page-break-before: always">

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보처리시스템별 접속기록 현황</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>(단위: 처리한 개인정보 건수)</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:center;text-indent:-22.0pt;line-height:180%;'>
<div class='downloadChartClass' id="chart10" style="width:750px; height: 280px;"></div>
<div align="center"><img name="chart10" style="width:750px; height: 280px; display: none;"></div>
</P>
<br>


<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:center;text-indent:-22.0pt;line-height:180%;'>

<div align="left" style="margin-left: 10px">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:235;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보처리시스템</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${pre }</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${cur }</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>증감</SPAN></P>
	</TD>
</TR>
<c:forEach items="${listChart4Detail}" var="i" varStatus="status">
		<TR>
			<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:235;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${i.system_name}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type2}" pattern="#,###" /></SPAN></P>
			</TD>
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1}" pattern="#,###" /></SPAN></P>
			</TD>
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			
			<c:if test="${i.type3 == 0}">
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#000000'>-</SPAN></P>
			</c:if>
			<c:if test="${i.type3 != 0 && i.bCheckPlus == '1' }">
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>▲ <fmt:formatNumber value="${i.type3}" pattern="#,###" /></SPAN></P>
			</c:if>
			<c:if test="${i.type3 != 0 && i.bCheckPlus == '0'}">
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#0000ff'>▼ <fmt:formatNumber value="${i.type3}" pattern="#,###" /></SPAN></P>
			</c:if>
			</TD>
		</TR>
</c:forEach>
</TABLE>

</div>

<div style='page-break-before:always'></div><br style="page-break-before: always">

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:center;text-indent:-22.0pt;line-height:180%;'></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 소속별 접속기록 현황 TOP10</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>(단위: 처리한 개인정보 건수)</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:center;text-indent:-22.0pt;line-height:180%;'>
<div class='downloadChartClass' id="chart11" style="width: 750px; height: 280px;"></div>
<div align="center"><img name="chart11" style="width:750px; height: 280px; display: none;"></div>
</P>
<br>


<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'></P>

<div align="left" style="margin-left: 10px">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:280;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>부서명</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:127;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${pre }</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:90;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${cur }</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:90;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>증감</SPAN></P>
	</TD>
</TR>

<c:forEach items="${listChart5Detail}" var="i" varStatus="status">
		<TR>
			<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:235;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${i.dept_name}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type2}" pattern="#,###" /></SPAN></P>
			</TD>
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1}" pattern="#,###" /></SPAN></P>
			</TD>
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			
			<c:if test="${i.type3 == 0}">
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>-</SPAN></P>
			</c:if>
			<c:if test="${i.type3 != 0 && i.bCheckPlus == '1' }">
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>▲ <fmt:formatNumber value="${i.type3}" pattern="#,###" /></SPAN></P>
			</c:if>
			<c:if test="${i.type3 != 0 && i.bCheckPlus == '0'}">
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#0000ff'>▼ <fmt:formatNumber value="${i.type3}" pattern="#,###" /></SPAN></P>
			</c:if>
			</TD>
		</TR>
</c:forEach>
</TABLE>

</div>

<div style='page-break-before:always'></div><br style="page-break-before: always">

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>2. 개인정보 처리 상세</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보 업무 행위별 결과</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>(단위: 처리한 개인정보 건수)</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:center;text-indent:-22.0pt;line-height:180%;'>
<div class='downloadChartClass' id="chart12" style="width:750px; height: 300px; cursor: pointer;"></div>
<div align="center"><img name="chart12" style="width:750px; height: 300px; float: left; display: none;"></div>
</P>
<br>


<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'></P>

<div align="left" style="margin-left: 10px">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:280;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보처리시스템</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:73;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>조회</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:73;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>등록</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:73;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>수정</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:73;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>삭제</SPAN></P>
	</TD>
</TR>

<c:forEach items="${systemCurdCount}" var="i" varStatus="status">
	<c:choose>
	<c:when test="${fn:length(systemCurdCount) == status.count }">
	<TR>
		<TD colspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:327;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>전체</SPAN></P>
		</TD>
		<TD valign="middle" style='width:73;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1 }" pattern="#,###" /></SPAN></P>
		</TD>
		<TD valign="middle" style='width:73;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type2 }" pattern="#,###" /></SPAN></P>
		</TD>
		<TD valign="middle" style='width:73;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type3 }" pattern="#,###" /></SPAN></P>
		</TD>
		<TD valign="middle" style='width:73;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type4 }" pattern="#,###" /></SPAN></P>
		</TD>
	</TR>
	</c:when>
	<c:otherwise>
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}</SPAN></P>
		</TD>
		<TD valign="middle" style='width:280;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${i.system_name}</SPAN></P>
		</TD>
		<TD valign="middle" style='width:73;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1 }" pattern="#,###" /></SPAN></P>
		</TD>
		<TD valign="middle" style='width:73;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type2 }" pattern="#,###" /></SPAN></P>
		</TD>
		<TD valign="middle" style='width:73;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type3 }" pattern="#,###" /></SPAN></P>
		</TD>
		<TD valign="middle" style='width:73;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type4 }" pattern="#,###" /></SPAN></P>
		</TD>
	</TR>
	</c:otherwise>
	</c:choose>
</c:forEach>
</TABLE>

</div>

<div style='page-break-before:always'></div><br style="page-break-before: always">

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보 유형별 결과</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:left;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;- 전체적인 처리 유형</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>(단위: 처리한 개인정보 건수)</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'>
<!-- <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'><IMG src="file:///C:\Users\EHCHOI\Desktop\PIC27F9.png" alt="그림입니다.
원본 그림의 이름: CLP00001c240003.bmp
원본 그림의 크기: 가로 614pixel, 세로 385pixel" width="597" height="351" vspace="0" hspace="0" border="0"></SPAN> -->
<div class='downloadChartClass' id="chart13" style="width: 750px; height: 300px;"></div>
<div align="center"><img name="chart13" style="width:750px; height: 300px; display: none;"></div>
<%-- <div align="center"><img id="chartImage7" style="display: none;" src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/chartImages/chart7.png"></div> --%>
</P>

<!-- <div style='page-break-before:always'></div><br style="page-break-before: always"> -->

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:left;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:left;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;- 시스템별 개인정보처리 유형</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>(단위: 처리한 개인정보 건수)</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:left;text-indent:-22.0pt;line-height:180%;'>
<!-- <SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><IMG src="file:///C:\Users\EHCHOI\Desktop\PIC2809.png" alt="그림입니다.
원본 그림의 이름: CLP00001c240006.bmp
원본 그림의 크기: 가로 616pixel, 세로 401pixel" width="602" height="347" vspace="0" hspace="0" border="0"></SPAN> -->
<div class='downloadChartClass' id="chart14" style="width: 750px; height: 500px;"></div>
<div align="center"><img name="chart14" style="width:750px; height: 300px; display: none;"></div>
<%-- <div align="center"><img id="chartImage8" style="display: none;" src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/chartImages/chart8.png"></div> --%>
</P>



<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:left;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;- 소속별 개인정보처리 유형 TOP10</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>(단위: 처리한 개인정보 건수)</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:left;text-indent:-22.0pt;line-height:180%;'>
<!-- <SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><IMG src="file:///C:\Users\EHCHOI\Desktop\PIC281A.png" alt="그림입니다.
원본 그림의 이름: CLP00001c240004.bmp
원본 그림의 크기: 가로 616pixel, 세로 401pixel" width="601" height="360" vspace="0" hspace="0" border="0"></SPAN> -->
<div class='downloadChartClass' id="chart15" style="width: 750px; height: 500px;"></div>
<div align="center"><img name="chart15" style="width:750px; height: 300px; display: none;"></div>
<%-- <div align="center"><img id="chartImage9" style="display: none;" src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/chartImages/chart9.png"></div> --%>
</P>

<!-- <div style='page-break-before:always'></div><br style="page-break-before: always"> -->

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:left;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인별 결과 TOP10</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>(단위: 처리한 개인정보 건수)</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'>
<!-- <SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><IMG src="file:///C:\Users\EHCHOI\Desktop\PIC281B.png" alt="그림입니다.
원본 그림의 이름: CLP00001c240007.bmp
원본 그림의 크기: 가로 616pixel, 세로 401pixel" width="616" height="351" vspace="0" hspace="0" border="0"></SPAN> -->
<div class='downloadChartClass' id="chart16" style="width: 750px; height: 500px;"></div>
<div align="center"><img name="chart16" style="width:750px; height: 300px; display: none;"></div>
<%-- <div align="center"><img id="chartImage10" style="display: none;" src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/chartImages/chart10.png"></div> --%>
</P>

<div style='page-break-before:always'></div><br style="page-break-before: always">

<!-- 고유식별 상세 -->
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>3. 고유식별정보 처리 상세 결과</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 시스템별 결과</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:61.6pt;text-indent:-43.3pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;- 고유식별정보를 가장 많이 처리하는 시스템은 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;font-weight:"bold";color:#0000ff;line-height:180%'>
${topSystem }
</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;line-height:180%'>(으)로 나타남</SPAN>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:center;text-indent:-22.0pt;line-height:180%;'>
<div class='downloadChartClass' id="chart17" style="width:750px; height: 300px;"></div>
<img name="chart17" style="width:750px; height: 300px; display: none;">
<%-- <div align="center"><img id="chartImage1" style="display: none;" src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/chartImages3/chart2.png"></div> --%>
</P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'>
<div align="left" style="margin-left: 10px">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#f2f2f2"  style='width:32;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>시스템명</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>주민등록번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>여권번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>운전면허번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>외국인등록번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:36;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>합계</SPAN></P>
	</TD>
</TR>

<c:forEach items="${systemCnt }" var="item" varStatus="status" >
<TR>
	<TD valign="middle" bgcolor="#f2f2f2"  style='width:32;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:130%;'><SPAN STYLE='font-family:"굴림";letter-spacing:-6%;font-weight:"bold"'>${status.count }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:175;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='line-height:130%;'><SPAN STYLE='font-family:"굴림"'>${item.system_name }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.type1 }" pattern="#,###"/></SPAN></P>
	</TD>	
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.type2 }" pattern="#,###"/></SPAN></P>
	</TD>	
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.type3 }" pattern="#,###"/></SPAN></P>
	</TD>	
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.type10 }" pattern="#,###"/></SPAN></P>
	</TD>	
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.cnt }" pattern="#,###"/></SPAN></P>
	</TD>	
</TR>
</c:forEach>
 
</TABLE>
</div>
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'>

<div style='page-break-before:always'></div>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>2. 소속사별 고유식별정보 처리 결과</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 소속사별 TOP 10</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:61.6pt;text-indent:-43.3pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;- 고유식별정보를 가장 많이 처리하는 부서는 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;font-weight:"bold";color:#0000ff;line-height:180%'>
${topDept }
</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;line-height:180%'>(으)로 나타남</SPAN>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:center;text-indent:-22.0pt;line-height:180%;'>
<div class='downloadChartClass' id="chart18" style="width:750px; height: 300px;"></div>
<img name="chart18" style="width:750px; height: 300px; display: none;">
<%-- <div align="center"><img id="chartImage2" style="display: none;" src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/chartImages3/chart3.png"></div> --%>
</P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'>
<div align="left" style="margin-left: 10px">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#f2f2f2"  style='width:32;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>부서명</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>주민등록번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>여권번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>운전면허번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>외국인등록번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:36;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>합계</SPAN></P>
	</TD>
</TR>

<c:forEach items="${deptCnt }" var="item" varStatus="status" >
<TR>
	<TD valign="middle" bgcolor="#f2f2f2"  style='width:32;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:130%;'><SPAN STYLE='font-family:"굴림";letter-spacing:-6%;font-weight:"bold"'>${status.count }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:175;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='line-height:130%;'><SPAN STYLE='font-family:"굴림"'>${item.dept_name }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.type1 }" pattern="#,###"/></SPAN></P>
	</TD>	
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.type2 }" pattern="#,###"/></SPAN></P>
	</TD>	
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.type3 }" pattern="#,###"/></SPAN></P>
	</TD>	
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.type10 }" pattern="#,###"/></SPAN></P>
	</TD>	
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.cnt }" pattern="#,###"/></SPAN></P>
	</TD>	
</TR>
</c:forEach>
 
</TABLE>
</div>
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'>

<div style='page-break-before:always'></div>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>3. 개인별 고유식별정보 처리 결과</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인별 TOP 10</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:61.6pt;text-indent:-43.3pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;- 고유식별정보를 가장 많이 처리하는 사용자는 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;font-weight:"bold";color:#0000ff;line-height:180%'>
${topIndv }
</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;line-height:180%'>(으)로 나타남</SPAN>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:center;text-indent:-22.0pt;line-height:180%;'>
<div class='downloadChartClass' id="chart19" style="width:750px; height: 300px;"></div>
<img name="chart19" style="width:750px; height: 300px; display: none;">
<%-- <div align="center"><img id="chartImage3" style="display: none;" src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/chartImages3/chart4.png"></div> --%>
</P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'>
<div align="left" style="margin-left: 10px">
<TABLE class="downloadClass" border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#f2f2f2"  style='width:32;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>사용자명</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>소속</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>주민등록번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>여권번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>운전면허번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:100;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>외국인등록번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:36;height:65;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>합계</SPAN></P>
	</TD>
</TR>

<c:forEach items="${indvCnt }" var="item" varStatus="status" >
<TR>
	<TD valign="middle" bgcolor="#f2f2f2"  style='width:32;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:130%;'><SPAN STYLE='font-family:"굴림";letter-spacing:-6%;font-weight:"bold"'>${status.count }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:175;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='line-height:130%;'><SPAN STYLE='font-family:"굴림"'>${item.emp_user_name }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:175;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='line-height:130%;'><SPAN STYLE='font-family:"굴림"'>${item.dept_name }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.type1 }" pattern="#,###"/></SPAN></P>
	</TD>	
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.type2 }" pattern="#,###"/></SPAN></P>
	</TD>	
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.type3 }" pattern="#,###"/></SPAN></P>
	</TD>	
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.type10 }" pattern="#,###"/></SPAN></P>
	</TD>	
	<TD valign="middle" style='width:36;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${item.cnt }" pattern="#,###"/></SPAN></P>
	</TD>	
</TR>
</c:forEach>
 
</TABLE>
</div>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><BR></P>

<style>
	#loading {
	 width: 100%;   
	 height: 100%;   
	 top: 0px;
	 left: 0px;
	 position: fixed;   
	 display: block;   
	 opacity: 0.7;   
	 background-color: #fff;   
	 z-index: 99;   
	 text-align: center; 
	 }  
	  
	#loading-image {   
	 position: absolute;   
	 top: 50%;   
	 left: 50%;  
	 z-index: 100; 
	 } 
</style>


<script type="text/javascript">
$(document).ready(function() {
	$('#loading').show();
});

$(window).load(function() {   
	$('#loading').hide();
});
</script>

</BODY>

<div id="loading"><img id="loading-image" width="30px;" src="${rootPath}/resources/image/loading3.gif" alt="Loading..." /></div>
</HTML>