<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<script src="${rootPath}/resources/js/jquery/jquery.min.js" type="text/javascript" charset="UTF-8"></script>

<script src="${rootPath}/resources/js/amcharts.js"></script>
<script src="${rootPath}/resources/js/serial.js"></script>
<script src="${rootPath}/resources/js/export.min.js"></script>
<script src="${rootPath}/resources/js/amstock.js"></script>
<script src="${rootPath}/resources/js/xy.js"></script>
<script src="${rootPath}/resources/js/radar.js"></script>
<script src="${rootPath}/resources/js/light.js"></script>
<script src="${rootPath}/resources/js/pie.js"></script>
<script src="${rootPath}/resources/js/gantt.js"></script>
<script src="${rootPath}/resources/js/gauge.js"></script>

<link rel="stylesheet" href="${rootPath}/resources/css/export.css" type="text/css" media="all">
<script src="${rootPath}/resources/js/psm/report/report_charts.js"></script>

<HEAD>
<META NAME="Generator" CONTENT="Hancom HWP 8.5.8.1541">
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">

<script type="text/javascript" charset="UTF-8">
	rootPath = '${rootPath}';
	contextPath = '${pageContext.servletContext.contextPath}';
	var start_date = '${start_date}';
	var end_date = '${end_date}';
	var strReq = '${CACHE_REQ_TYPE}';
	
</script>

<script type="text/javascript">

var saveCnt = 0;
function exportChartToImg() {
	var charts = {};
	
	for (var x = 0; x < AmCharts.charts.length; x++) {
		$("#"+AmCharts.charts[x].div.id).css("width","75%");
	}
	
 	var lts = setInterval(function() {
		for (var x = 0; x < AmCharts.charts.length; x++) {
			charts[AmCharts.charts[x].div.id] = AmCharts.charts[x];
		}
		for ( var x in charts) {
			var chart = charts[x];
			chart["export"].capture({}, function() {
				this.toJPG({}, function(data) {
					var chartName = this.setup.chart.div.id;
					$("img[name="+chartName+"]").attr("src", data);
					$("img[name="+chartName+"]").css("display","");
					console.log(chartName);
					$("#"+chartName).css("display","none");
					saveCnt++;
					if(saveCnt == AmCharts.charts.length) {
						$("img[name=logoImg]").attr("src",getBase64Image(document.getElementById("logoImg")));
						$("img[name=logoImg]").css("display","");
						$("#logoImg").remove();
						//exportDocs();
						
					}
				});
			});
			clearInterval(lts);
		}
	}, 500);
}

function getBase64Image(img) {
  var canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);
  var dataURL = canvas.toDataURL("image/png");
  return dataURL; 
}
	
function exportDocs(inter) {
	var date = new Date();
	var year = date.getFullYear();
	var month = new String(date.getMonth() + 1);
	var day = new String(date.getDate());
	if(month.length == 1){ 
		month = "0" + month; 
	} 
	if(day.length == 1){ 
		day = "0" + day; 
	} 
	var regdate = year + "" + month + "" + day;
	var agent = navigator.userAgent.toLowerCase();
	
	if ((navigator.appName == 'Netscape' && navigator.userAgent.search('Trident') != -1) || (agent.indexOf("msie") != -1) ) {
		saveAsPage(regdate);
	} else {
		exportHTML(regdate);
	}

	clearInterval(inter);
	window.close();
}

function saveWordReport() {
	exportChartToImg();
	var inter = setInterval(function() {
		if(saveCnt == AmCharts.charts.length) {
			exportDocs(inter);
		}	
	}, 500)
}

function printpr()
{
	$("#printButton").hide();

	window.print();
	/* var browser = navigator.userAgent.toLowerCase();
    if ( -1 != browser.indexOf('chrome') ){
               window.print();
    }else if ( -1 != browser.indexOf('trident') ){
               try{
                        //참고로 IE 5.5 이상에서만 동작함

                        //웹 브라우저 컨트롤 생성
                        var webBrowser = '<OBJECT ID="previewWeb" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';

                        //웹 페이지에 객체 삽입
                        document.body.insertAdjacentHTML('beforeEnd', webBrowser);

                        //ExexWB 메쏘드 실행 (7 : 미리보기 , 8 : 페이지 설정 , 6 : 인쇄하기(대화상자))
                        previewWeb.ExecWB(7, 1);

                        //객체 해제
                        previewWeb.outerHTML = "";
               }catch (e) {
                        alert("- 도구 > 인터넷 옵션 > 보안 탭 > 신뢰할 수 있는 사이트 선택\n   1. 사이트 버튼 클릭 > 사이트 추가\n   2. 사용자 지정 수준 클릭 > 스크립팅하기 안전하지 않은 것으로 표시된 ActiveX 컨트롤 (사용)으로 체크\n\n※ 위 설정은 프린트 기능을 사용하기 위함임");
               }
              
    } */
    
    self.close();
}

</script>

<STYLE type="text/css">
<!--
p.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle14
	{style-name:"xl65"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:10.0pt; font-family:굴림; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle14
	{style-name:"xl65"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:10.0pt; font-family:굴림; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle14
	{style-name:"xl65"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:10.0pt; font-family:굴림; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle15
	{style-name:"xl67"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; background-color:#e5e5e5; font-size:10.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle15
	{style-name:"xl67"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; background-color:#e5e5e5; font-size:10.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle15
	{style-name:"xl67"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; background-color:#e5e5e5; font-size:10.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
-->
</STYLE>
</HEAD>

<BODY>
<div id="source-html">
<div align="right">
	<input type=button id="printButton" name="printButton"onclick="printpr();" value="출력"/>
	<input type=button id="saveButton" name="saveButton" onclick="saveWordReport();" value="저장"/>
</div>

<div align="center">
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" style='width:627;height:142;border-left:double #000000 1.4pt;border-right:double #000000 1.4pt;border-top:double #000000 1.4pt;border-bottom:double #000000 1.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:150%;'><SPAN STYLE='font-size:19.0pt;font-family:"돋움";letter-spacing:-4%;font-weight:"bold";font-style:"italic";line-height:150%'>개인정보의 안전한 관리를 위한</SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:150%;'><SPAN STYLE='font-size:25.0pt;font-family:"돋움";letter-spacing:-4%;font-weight:"bold";line-height:150%'>개인정보 접속기록의 주기적 점검 결과</SPAN></P>
	</TD>
</TR>
</TABLE>
</div>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'><SPAN STYLE='font-size:20.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";line-height:180%'>
<c:set var="date" value="${fn:split(start_date, '-') }"/>
${date[0] }.${date[1] }
</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'>

<div align="center">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" style='width:342;height:77;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<c:choose>
		<c:when test="${filename eq null }" >
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:18.0pt;line-height:160%'>
				<img id="logoImg" src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}${logo_report_url}">
				<img name="logoImg" style="display: none">
			</SPAN></P>
		</c:when>
		<c:otherwise>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:18.0pt;line-height:160%'>
				<img id="logoImg" src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/upload/${filename }">
				<img name="logoImg" style="display: none">
			</SPAN></P>
		</c:otherwise>
	</c:choose>
	</TD>
</TR>
</TABLE>
</div>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'>

<div align="center">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD rowspan="2" valign="middle" style='width:27;height:114;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-top:2.4pt;text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>결</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-top:2.4pt;text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>재</SPAN></P>
	</TD>
	<TD valign="middle" style='width:91;height:35;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-top:2.4pt;text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>담당</SPAN></P>
	</TD>
	<TD valign="middle" style='width:100;height:35;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-top:2.4pt;text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>CPO</SPAN></P>
	</TD>
</TR>
<TR>
	<TD valign="middle" style='width:91;height:79;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-top:2.4pt;text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>&nbsp;</SPAN></P>
	</TD>
	<TD valign="middle" style='width:100;height:79;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-top:2.4pt;text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>&nbsp;</SPAN></P>
	</TD>
</TR>
</TABLE>
</div>


<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'>
<div style='page-break-before:always'></div><br style="page-break-before: always">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:100% '>
<TR>
	<TD valign="middle" bgcolor="#4e9fd7"  style='width:100% ;height:44px;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0><SPAN STYLE='width:100% ;font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;line-height:160%'>Ⅰ. 점검 개요</SPAN></P>
	</TD>
</TR>
</TABLE>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;margin-top:8.0pt;line-height:180%;'> <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>1. 점검 목적</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.5pt;text-indent:-21.2pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ </SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-3%;line-height:180%'>개인정보처리시스템의 접속기록을 점검하여 불법적인 접근 및 비정상</SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'> </SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;line-height:180%'>행위 등을 탐지 분석하여 개인정보의 오·남용 사례를 방지하고</SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'> 안전하게 관리할 수 있도록 관리</SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;line-height:180%'>·</SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>감독을 수행함</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:41.7pt;text-indent:-23.4pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;margin-top:8.0pt;line-height:180%;'> <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>2. 관련 근거</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보보호법 제29조 (안전조치의무)</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보보호법 시행령 제30조 (개인정보의 안전성 확보 조치)</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ </SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-3%;line-height:180%'>개인정보의 안전성 확보조치 기준 제8조(접속기록의 보관 및 점검)</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-left:41.7pt;text-indent:-23.4pt;line-height:180%;'>

<!-- <div align="center">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" style='width:595;height:195;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:15.4pt;text-indent:-15.2pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'>① </SPAN><SPAN STYLE='font-family:"돋움";letter-spacing:-1%'>개인정보처리자는 개인정보취급자가 개인정보처리시스템에 접속한 기록을 6개월 이상 보관·</SPAN><SPAN STYLE='font-family:"돋움"'>관리하여야 한다.</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:16.8pt;text-indent:-16.6pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'>② 개인정보처리자는 개인정보의 분실·도난·유출·위조·변조 또는 훼손 등에 대응하기 위하여 </SPAN><SPAN STYLE='font-family:"돋움";font-weight:"bold";text-decoration:"underline"'>개인정보처리시스템의 접속기록 등을 반기별로 1회 이상 점검하여야 한다.</SPAN><SPAN STYLE='font-family:"돋움"'> </SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:16.8pt;text-indent:-16.6pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'>③ 개인정보처리자는 개인정보취급자의 접속기록이 위·변조 및 도난, 분실되지 않도록 해당 접속기록을 안전하게 보관하여야 한다.</SPAN></P>
	</TD>
</TR>
</TABLE>
</div> -->

<div align="center">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" style='width:650;height:195;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='margin-left:15.4pt;text-indent:-15.2pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'>① </SPAN><SPAN STYLE='font-family:"돋움";letter-spacing:-1%'>개인정보처리자는 개인정보취급자가 개인정보처리시스템에 접속한 기록을 1년 이상  보관·관리하여야 한다. 다만, 5만명 이상의 정보주체에 관하여 개인정보를 처리하거나, 고유식별정보 또는 민감정보를 처리하는 개인정보처리시스템의 경우에는 2년 이상 보관‧관리하여야 한다.</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:16.8pt;text-indent:-16.6pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'>② 개인정보처리자는 개인정보의 오‧남용, 분실·도난·유출·위조·변조 또는 훼손 등에 대응하기 위하여 개인정보처리시스템의 접속기록 등을 월 1회 이상 점검하여야 한다. 특히 개인정보를 다운로드한 것이 발견되었을 경우에는 내부 관리계획으로 정하는 바에 따라 그 사유를 반드시 확인하여야 한다.</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:16.8pt;text-indent:-16.6pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'>③ 개인정보처리자는 개인정보취급자의 접속기록이 위·변조 및 도난, 분실되지 않도록 해당 접속기록을 안전하게 보관하여야 한다.</SPAN></P>
	</TD>
</TR>
</TABLE>
</div>

<P CLASS=HStyle0 STYLE='margin-left:41.7pt;text-indent:-23.4pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'> <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>3. 점검 범위 </SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 점검 범위</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:54.5pt;text-indent:-36.2pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;- </SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>
<c:set var="startdate" value="${fn:split(start_date, '-') }"/>
<c:set var="enddate" value="${fn:split(end_date, '-') }"/>

${startdate[0] }년 ${startdate[1] }월 ${startdate[2] }일 ~ ${enddate[0] }년 ${enddate[1] }월 ${enddate[2] }일까지의 개인정보 접속기록 로그
</SPAN>
</P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 점검 대상 개인정보 처리시스템</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'>

<div align="center">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:280;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보처리시스템 명칭</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:280;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>URL</SPAN></P>
	</TD>
</TR>
<c:forEach var="sys" items="${systems }" varStatus="status">
	<TR>
		<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count }</SPAN></P>
		</TD>
		<TD valign="middle" style='width:280;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${sys.system_name }</SPAN></P>
		</TD>
		<TD valign="middle" style='width:280;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${sys.main_url }</SPAN></P>
		</TD>
	</TR>
</c:forEach>
</TABLE>
</div>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'> <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>4. 점검 일시</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>
❍ ${date[0] }년 ${date[1] }월 ${date[2] }일
</SPAN>
</P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>
<!-- 
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'> <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>5. 점검 수행자</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 정보보호부 개인정보보호팀</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>
 -->
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'> <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>5. 점검 방법</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보 접속기록 현황 검토</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보처리시스템에 대한 비정상 행위 시도 탐지</SPAN></P>


<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'>
<div style='page-break-before:always'></div><br style="page-break-before: always">
<TABLE border="1" cellspacing="0" cellpadding="0" style='width:100%;border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#4e9fd7"  style='width:100%;height:44;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0><SPAN STYLE='width:100%;font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;line-height:160%'>Ⅱ. 종합 결과</SPAN><SPAN STYLE='font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'> </SPAN></P>
	</TD>
</TR>
</TABLE>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>1. 총평</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보 접속기록 현황</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:61.6pt;text-indent:-43.3pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;- </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>
${startdate[0] }년 ${startdate[1] }월 ${startdate[2] }일</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>부터</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;color:#0000ff;line-height:180%'> </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>
${enddate[0] }년 ${enddate[1] }월 ${enddate[2] }일</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>까지 총 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;font-weight:"bold";color:#0000ff;line-height:180%'>
${diffMonth }</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>개월 동안 발생한</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-4%;line-height:180%'> </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;line-height:180%'>개인정보 접속기록 로그 총 건수는</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;color:#0000ff;line-height:180%'> </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;font-weight:"bold";color:#0000ff;line-height:180%'>
<fmt:formatNumber value="${logCnt }" pattern="#,###" />
</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;line-height:180%'>건으로,</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;color:#0000ff;line-height:180%'> </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;line-height:180%'>개인정보</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-1%;line-height:180%'> 처리시스템별로는</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'> </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";font-weight:"bold";color:#0000ff;line-height:180%'>
<span id="reportBySys"></span></SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";color:#0000ff;line-height:180%'>, </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>소속별로는 </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";font-weight:"bold";color:#0000ff;line-height:180%'>
<span id="reportByDept"></span></SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>의 경우가 개인정보 처리량이 가장 많은 것으로 나타남</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:61.6pt;text-indent:-43.3pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;- 각 시스템별 및 소속별 개인정보 처리량</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'>
<!-- <SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><IMG src="file:///C:\Users\EHCHOI\Desktop\PIC2793.png" alt="그림입니다.
원본 그림의 이름: CLP00000bd012b8.bmp
원본 그림의 크기: 가로 335pixel, 세로 228pixel" width="273" height="228" vspace="0" hspace="0" border="0">
<P CLASS=HStyle0 STYLE='text-align:center;'>[개인정보 처리시스템별 결과]
</P>
<IMG src="file:///C:\Users\EHCHOI\Desktop\PIC27A4.png" alt="그림입니다.
원본 그림의 이름: CLP00000bd012b8.bmp
원본 그림의 크기: 가로 335pixel, 세로 228pixel" width="251" height="228" vspace="0" hspace="0" border="0">
<P CLASS=HStyle0 STYLE='text-align:center;'>[소속별 결과]
</P>
</SPAN> -->
<div align="center">
<div id="chart1_1" name="chart1_1" style="width:100%; height: 300px; float: left; cursor: pointer;"></div>
<img name="chart1_1" style="width:100%; height: 300px; float: left; display: none;">
<div id="chart1_2" name="chart1_2" style="width:100%; height: 200px; float: left; cursor: pointer;"></div><div style="clear:both;"></div>
<img name="chart1_2" style="width:100%; height: 200px; float: left; display: none;">
</div>
</P>

<P CLASS=HStyle5 STYLE='margin-left:61.6pt;text-indent:-43.3pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:61.6pt;text-indent:-43.3pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;- 개인정보 처리 업무 행위별 결과는 
<c:forEach items="${CACHE_REQ_TYPE}" var="i" varStatus="status">
	<c:if test="${i.key == topReportByReqtype}">
		<ctl:nullCv nullCheck="${i.value}" />
	</c:if>
</c:forEach>
에 대한 접속기록량이 가장 많은 것으로 나타남</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:61.6pt;text-indent:-43.3pt;line-height:180%;'>

<div align="center">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:96;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:295;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>업무행위</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:212;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>건수</SPAN></P>
	</TD>
</TR>
<c:forEach var="report" items="${listByReqtype }" varStatus="status">
<TR>
	<TD valign="middle" style='width:96;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count }</SPAN></P>
	</TD>
	<TD valign="middle" style='width:295;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>
	<c:forEach items="${CACHE_REQ_TYPE}" var="i" varStatus="status">
		<c:if test="${i.key == report.req_type}">
			<ctl:nullCv nullCheck="${i.value}" />
		</c:if>
	</c:forEach>
	</SPAN></P>
	</TD>
	<TD valign="middle" style='width:212;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${report.cnt }" pattern="#,###" /></SPAN></P>
	</TD>
</TR>
</c:forEach>

</TABLE>
</div>

<div style='page-break-before:always'></div><br style="page-break-before: always">

<P CLASS=HStyle5 STYLE='margin-left:61.6pt;text-indent:-43.3pt;line-height:180%;'></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보처리 비정상 행위 의심 사례 검토</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:61.6pt;text-indent:-43.3pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;- 비정상행위 의심</SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-3%;line-height:180%'>건수 총 </SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-3%;font-weight:"bold";color:#0000ff;line-height:180%'><fmt:formatNumber value="${empDetailCnt }" pattern="#,###" /></SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-3%;line-height:180%'>건에 대해 검토함</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:66.7pt;text-indent:-48.4pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-1%;color:#ff0000;line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;․</SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-2%;color:#ff0000;line-height:280%'></SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";color:#ff0000;line-height:180%'> </SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-2%;color:#ff0000;line-height:180%'> </SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:61.6pt;text-indent:-43.3pt;line-height:180%;text-align: center;'>
<textarea rows="5" cols="70" placeholder="내용을 입력하시오" style='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-1%;line-height:180%'></textarea>
</P>

<P CLASS=HStyle5 STYLE='margin-left:66.7pt;text-indent:-48.4pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-1%;color:#ff0000;line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;․</SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-6%;color:#ff0000;line-height:180%'></SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-4%;color:#ff0000;line-height:180%'> </SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>2. 후속조치</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:61.6pt;text-indent:-43.3pt;line-height:180%;text-align: center;'>
<textarea rows="5" cols="70" placeholder="내용을 입력하시오" style='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-1%;line-height:180%'></textarea>
</P>

<P CLASS=HStyle5 STYLE='margin-left:62.3pt;text-indent:-44.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;&nbsp; </SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";color:#ff0000;line-height:300%'></SPAN></P>


<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'>
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:100%;'>
<TR>
	<TD valign="middle" bgcolor="#4e9fd7"  style='width:100%;height:44;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;width:100%;line-height:160%'>Ⅲ. 상세 결과</SPAN><SPAN STYLE='font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'> </SPAN></P>
	</TD>
</TR>
</TABLE>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>1. 개인정보 접속기록 상세점검 결과</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보 접속기록 전체현황</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;&nbsp;&nbsp;- 처리한 개인정보의 전체 건수</SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-3%;line-height:180%'>
는 <fmt:formatNumber value="${privCnt }" pattern="#,###" />건으로 나타남</SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'>
<!-- <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>&nbsp;&nbsp;<IMG src="file:///C:\Users\EHCHOI\Desktop\PIC27A5.png" alt="그림입니다.
원본 그림의 이름: CLP00001c240005.bmp
원본 그림의 크기: 가로 614pixel, 세로 385pixel" width="598" height="385" vspace="0" hspace="0" border="0"></SPAN> -->
<div id="chart2" style="width:100%; height: 400px;"></div>
<img name="chart2" style="width:100%; height: 400px; float: left; display: none;">
</P>

<div align="center">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:235;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>날짜</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보 접속기록</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>평균</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>증감률</SPAN></P>
	</TD>
</TR>

<c:forEach items="${privTypeCount}" var="i" varStatus="status">
		<TR>
			<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:235;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${i.proc_date}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.cnt}" pattern="#,###" /></SPAN></P>
			</TD>
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${privTypeCountAvg}" pattern="#,###" /></SPAN></P>
			</TD>
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			
			<c:if test="${i.tot_cnt == '-'}">
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>-</SPAN></P>
			</c:if>
			<c:if test="${i.tot_cnt != '-' && i.bCheckPlus == '1' }">
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>▲ <fmt:formatNumber value="${i.tot_cnt}" pattern="#,###" /></SPAN></P>
			</c:if>
			<c:if test="${i.tot_cnt != '-' && i.bCheckPlus == '0'}">
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#0000ff'>▼ <fmt:formatNumber value="${i.tot_cnt}" pattern="#,###" /></SPAN></P>
			</c:if>
			</TD>
		</TR>
</c:forEach>

</TABLE>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보 접속기록 월별, 시간별 추이 </SPAN><SPAN STYLE='font-size:11.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>(단위: 처리한 개인정보 건수)</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'>
<!-- <SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><IMG src="file:///C:\Users\EHCHOI\Desktop\PIC27B5.png" alt="그림입니다.
원본 그림의 이름: CLP00001c240009.bmp
원본 그림의 크기: 가로 691pixel, 세로 228pixel" width="416" height="174" vspace="0" hspace="0" border="0">
<P CLASS=HStyle0>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[월별]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; [주별]
</P>
<IMG src="file:///C:\Users\EHCHOI\Desktop\PIC27C6.png" alt="그림입니다.
원본 그림의 이름: CLP00001c24000a.bmp
원본 그림의 크기: 가로 433pixel, 세로 229pixel" width="188" height="174" vspace="0" hspace="0" border="0">
<P CLASS=HStyle0>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[일별]
</P>
</SPAN> -->
<div id="chart3_1" style="width:100%; height: 200px; float: left;"></div>
<%-- <div align="center"><img id="chartImage11" style="display: none;" src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/chartImages/chart3_1.png"></div> --%>
<div align="center"><img name="chart3_1" style="width:100%; height: 200px; float: left; display: none;"></div>
<!-- <div id="chart3_2" style="width:50%; height: 200px; float: left;"></div> -->
<div id="chart3_3" style="width:100%; height: 200px; float: left;"></div><div style="clear: both;"></div>
<div align="center"><img name="chart3_3" style="width:100%; height: 200px; float: left; display: none;"></div>
</P>

<div style='page-break-before:always'></div><br style="page-break-before: always">

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보처리시스템별 접속기록 현황</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>(단위: 처리한 개인정보 건수)</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:center;text-indent:-22.0pt;line-height:180%;'>
<!-- <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'><IMG src="file:///C:\Users\EHCHOI\Desktop\PIC27C7.png" alt="그림입니다.
원본 그림의 이름: CLP00001c24815d.bmp
원본 그림의 크기: 가로 534pixel, 세로 564pixel" width="433" height="355" vspace="0" hspace="0" border="0"></SPAN> -->
<div id="chart4" style="width:100%; height: 500px;"></div>
<div align="center"><img name="chart4" style="width:100%; height: 500px; display: none;"></div>
</P>


<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:center;text-indent:-22.0pt;line-height:180%;'>

<div align="center">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:235;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보처리시스템 명칭</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>이전월</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>금월</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>증감</SPAN></P>
	</TD>
</TR>
<c:forEach items="${listChart4Detail}" var="i" varStatus="status">
		<TR>
			<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:235;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${i.system_name}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type2}" pattern="#,###" /></SPAN></P>
			</TD>
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1}" pattern="#,###" /></SPAN></P>
			</TD>
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			
			<c:if test="${i.type3 == 0}">
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>-</SPAN></P>
			</c:if>
			<c:if test="${i.type3 != 0 && i.bCheckPlus == '1' }">
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>▲ <fmt:formatNumber value="${i.type3}" pattern="#,###" /></SPAN></P>
			</c:if>
			<c:if test="${i.type3 != 0 && i.bCheckPlus == '0'}">
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#0000ff'>▼ <fmt:formatNumber value="${i.type3}" pattern="#,###" /></SPAN></P>
			</c:if>
			</TD>
		</TR>
</c:forEach>
</TABLE>

</div>

<div style='page-break-before:always'></div><br style="page-break-before: always">

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:center;text-indent:-22.0pt;line-height:180%;'></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 소속별 접속기록 현황 TOP10</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>(단위: 처리한 개인정보 건수)</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:center;text-indent:-22.0pt;line-height:180%;'>
<!-- <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'><IMG src="file:///C:\Users\EHCHOI\Desktop\PIC27D7.png" alt="그림입니다.
원본 그림의 이름: CLP00001c240002.bmp
원본 그림의 크기: 가로 481pixel, 세로 288pixel" width="546" height="327" vspace="0" hspace="0" border="0"></SPAN> -->
<div id="chart5" style="width: 100%; height: 400px;"></div>
<div align="center"><img name="chart5" style="width:100%; height: 400px; display: none;"></div>
</P>


<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'></P>

<div align="center">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:280;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>부서명</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:127;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>이전월</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:90;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>금월</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:90;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>증감</SPAN></P>
	</TD>
</TR>

<c:forEach items="${listChart5Detail}" var="i" varStatus="status">
		<TR>
			<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:235;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${i.dept_name}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:131;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type2}" pattern="#,###" /></SPAN></P>
			</TD>
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1}" pattern="#,###" /></SPAN></P>
			</TD>
			<TD valign="middle" style='width:90;height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			
			<c:if test="${i.type3 == 0}">
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>-</SPAN></P>
			</c:if>
			<c:if test="${i.type3 != 0 && i.bCheckPlus == '1' }">
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#ff0000'>▲ <fmt:formatNumber value="${i.type3}" pattern="#,###" /></SPAN></P>
			</c:if>
			<c:if test="${i.type3 != 0 && i.bCheckPlus == '0'}">
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";color:#0000ff'>▼ <fmt:formatNumber value="${i.type3}" pattern="#,###" /></SPAN></P>
			</c:if>
			</TD>
		</TR>
</c:forEach>
</TABLE>

</div>

<div style='page-break-before:always'></div><br style="page-break-before: always">

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보 업무 행위별 결과</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>(단위: 처리한 개인정보 건수)</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:center;text-indent:-22.0pt;line-height:180%;'>
<!-- <SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><IMG src="file:///C:\Users\EHCHOI\Desktop\PIC27E8.png" alt="그림입니다.
원본 그림의 이름: CLP00001c240001.bmp
원본 그림의 크기: 가로 393pixel, 세로 393pixel" width="368" height="368" vspace="0" hspace="0" border="0"></SPAN> -->
<div id="chart6" style="width:100%; height: 400px"></div>
<div align="center"><img name="chart6" style="width:100%; height: 400px; display: none;"></div>
</P>


<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'></P>

<div align="center">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>번호</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:280;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>개인정보처리시스템 명칭</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:73;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>조회</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:73;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>등록</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:73;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>수정</SPAN></P>
	</TD>
	<TD valign="middle" bgcolor="#e5e5e5"  style='width:73;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>삭제</SPAN></P>
	</TD>
</TR>

<c:forEach items="${systemList}" var="i" varStatus="status">
		<TR>
			<TD valign="middle" bgcolor="#e5e5e5"  style='width:47;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>${status.count}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:280;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림"'>${i.system_name}</SPAN></P>
			</TD>
			<TD valign="middle" style='width:73;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type1 }" pattern="#,###" /></SPAN></P>
			</TD>
			<TD valign="middle" style='width:73;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type2 }" pattern="#,###" /></SPAN></P>
			</TD>
			<TD valign="middle" style='width:73;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type3 }" pattern="#,###" /></SPAN></P>
			</TD>
			<TD valign="middle" style='width:73;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${i.type4 }" pattern="#,###" /></SPAN></P>
			</TD>
		</TR>
</c:forEach>
<TR>
	<TD colspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:327;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"굴림";font-weight:"bold"'>전체</SPAN></P>
	</TD>
	<TD valign="middle" style='width:73;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${systemCount1}" pattern="#,###" /></SPAN></P>
	</TD>
	<TD valign="middle" style='width:73;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${systemCount2}" pattern="#,###" /></SPAN></P>
	</TD>
	<TD valign="middle" style='width:73;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${systemCount3}" pattern="#,###" /></SPAN></P>
	</TD>
	<TD valign="middle" style='width:73;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:right;'><SPAN STYLE='font-family:"굴림"'><fmt:formatNumber value="${systemCount4}" pattern="#,###" /></SPAN></P>
	</TD>
</TR>
</TABLE>

</div>

<div style='page-break-before:always'></div><br style="page-break-before: always">

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보 유형별 결과</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:left;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;- 전체적인 처리 유형</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>(단위: 처리한 개인정보 건수)</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'>
<!-- <SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'><IMG src="file:///C:\Users\EHCHOI\Desktop\PIC27F9.png" alt="그림입니다.
원본 그림의 이름: CLP00001c240003.bmp
원본 그림의 크기: 가로 614pixel, 세로 385pixel" width="597" height="351" vspace="0" hspace="0" border="0"></SPAN> -->
<div id="chart7" style="width: 100%; height: 400px;"></div>
<div align="center"><img name="chart7" style="width:100%; height: 400px; display: none;"></div>
</P>


<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:left;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:left;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;- 시스템별 개인정보처리 유형</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>(단위: 처리한 개인정보 건수)</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:left;text-indent:-22.0pt;line-height:180%;'>
<!-- <SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><IMG src="file:///C:\Users\EHCHOI\Desktop\PIC2809.png" alt="그림입니다.
원본 그림의 이름: CLP00001c240006.bmp
원본 그림의 크기: 가로 616pixel, 세로 401pixel" width="602" height="347" vspace="0" hspace="0" border="0"></SPAN> -->
<div id="chart8" style="width: 100%; height: 700px;"></div>
<div align="center"><img name="chart8" style="width:100%; height: 700px; display: none;"></div>
</P>

<div style='page-break-before:always'></div><br style="page-break-before: always">

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:left;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;- 소속별 개인정보처리 유형 TOP10</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>(단위: 처리한 개인정보 건수)</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:left;text-indent:-22.0pt;line-height:180%;'>
<!-- <SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><IMG src="file:///C:\Users\EHCHOI\Desktop\PIC281A.png" alt="그림입니다.
원본 그림의 이름: CLP00001c240004.bmp
원본 그림의 크기: 가로 616pixel, 세로 401pixel" width="601" height="360" vspace="0" hspace="0" border="0"></SPAN> -->
<div id="chart9" style="width: 100%; height: 700px;"></div>
<div align="center"><img name="chart9" style="width:100%; height: 700px; display: none;"></div>
</P>


<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:left;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인별 결과 TOP10</SPAN><SPAN STYLE='font-size:11.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>(단위: 처리한 개인정보 건수)</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'>
<!-- <SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><IMG src="file:///C:\Users\EHCHOI\Desktop\PIC281B.png" alt="그림입니다.
원본 그림의 이름: CLP00001c240007.bmp
원본 그림의 크기: 가로 616pixel, 세로 401pixel" width="616" height="351" vspace="0" hspace="0" border="0"></SPAN> -->
<div id="chart10" style="width: 100%; height: 700px;"></div>
<div align="center"><img name="chart10" style="width:100%; height: 700px; display: none;"></div>
</P>



<div style='page-break-before:always'></div><br style="page-break-before: always">

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:center;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>2. 비정상행위 의심사례 검토 결과</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 비정상 행위 기준별 결과</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:left;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;- 과다사용 : <fmt:formatNumber value="${listEmpDetailCount1}" pattern="#,###" />건</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:left;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;- 특정인 처리 : <fmt:formatNumber value="${listEmpDetailCount2}" pattern="#,###" />건 </SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:left;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;- 비정상 접근 : <fmt:formatNumber value="${listEmpDetailCount3}" pattern="#,###" />건&nbsp;&nbsp; </SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:left;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;- 특정시간대 처리 : <fmt:formatNumber value="${listEmpDetailCount4}" pattern="#,###" />건</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:left;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>&nbsp;- 고유식별정보 과다사용 : <fmt:formatNumber value="${listEmpDetailCount5}" pattern="#,###" />건</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:center;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-align:left;text-indent:-22.0pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인별 결과 TOP10</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'>

<div align="center">
<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" style='width:125;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>이름</P>
	</TD>
	<TD valign="middle" style='width:129;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>소속</P>
	</TD>
	<TD valign="middle" style='width:140;height:28;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P CLASS=HStyle0 STYLE='text-align:center;'>건수</P>
	</TD>
</TR>


<c:forEach items="${listEmpDetail}" var="i" varStatus="status">
		<TR>
			<TD valign="middle" style='width:225;height:32;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'>${i.emp_user_name.substring(0,1)}**</P>
			</TD>
			<TD valign="middle" style='width:229;height:32;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'>${i.dept_name}</P>
			</TD>
			<TD valign="middle" style='width:240;height:32;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><fmt:formatNumber value="${i.cnt}" pattern="#,###" /></P>
			</TD>
		</TR>
</c:forEach>













</TABLE>
</div>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;'></P>

</div>
</BODY>

</HTML>
