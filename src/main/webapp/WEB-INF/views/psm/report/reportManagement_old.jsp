<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>
<c:set var="currentMenuId" value="${index_id}" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />
<c:set var="report_1_4" value="true"/>
<c:set var="report_2_4" value="true"/>
<c:set var="report_3_4" value="false"/>
<c:set var="report_4_4" value="false"/>
<c:set var="all_report" value="true"/>
<c:set var="now" value="<%=new java.util.Date()%>" />

<c:choose>
	<c:when test="${empty search.search_from}">
		<c:set var="Year">
 			<fmt:formatDate value="${now}" pattern="yyyy" />
		</c:set>
		<c:set var="Month">
			<fmt:formatDate value="${now}" pattern="MM" />
		</c:set>
	</c:when>
	<c:otherwise>
		<c:set var="Year" value="${fn:substring(search.search_from,0,4) }" />
		<c:set var="Month" value="${fn:substring(search.search_from,4,6) }" />
	</c:otherwise>
</c:choose>
<link rel="stylesheet" href="${rootPath}/resources/css/reportmanage/reportManagement_new.css" type="text/css" media="all">
<script src="${rootPath}/resources/js/reportmanage/reportManagement_new.js" type="text/javascript" charset="UTF-8"></script>
<script>
var report_type = "${report_type}";
var year = ${Year};
var rootPath = "${rootPath}";
var install_date = '<ctl:code value="INSTALL_DATE" groupId="SOLUTION_MASTER"/>';
var install_month = null;

var report_intro = {
		type1 : "담당자용 수준 진단 보고의 개요가 작성되는 부분 입니다",
		type2 : "CPO 오남용 보고의 개요가 작성되는 부분 입니다",
		type3 : "담당자용 시스템별 수준진단 보고의 개요가 작성되는 부분 입니다",
		type4 : "고유식별정보 처리 현황 보고의 개요가 작성되는 부분 입니다",
		type5 : "반기별 수준진단 보고의 개요가 작성되는 부분 입니다",
		type9 : "개인정보 다운로드 보고의 개요가 작성되는 부분 입니다",
		callintro : function(type){
			report_type = type;
			return this["type"+type];
		}
}

var report_title = {
		type1 : "${reportMap['1']}",
		type2 : "${reportMap['2']}",
		type3 : "${reportMap['3']}",
		type4 : "${reportMap['4']}",
		type5 : "${reportMap['5']}",
		type9 : "${reportMap['9']}",
		type10 : "${reportMap['10']}",
		type12 : "${reportMap['12']}",
		type13 : "${reportMap['13']}",
		calltitle : function(type){
			report_type = type;
			return this["type"+type];
		}
}

var report_page = {
		type1 : rootPath+"/report/reportDetail_new.html",
		type2 : rootPath+"/report/reportCPO.html",
		type3 : rootPath+"/report/reportDetail_new.html",
		type4 : rootPath+"/report/reportDetail_new.html",
		type5 : rootPath+"/report/reportDetail_half.html",
		type9 : rootPath+"/report/reportDetail_download.html",
		type10 : rootPath+"/report/reportDetail_authInfo.html",
		type13 : rootPath+"/report/reportEmpLevel_new.html",
		type12 : rootPath+"/report/reportDetail_download_new.html",
		callpage : function(type){
			report_type = type;
			return this["type"+type];
		}
}

$(document).ready(function(){
	var auth = '${userAuth.make_report_auth}';
	if(auth == 'N'){
		$('.report_new_btn').remove();
		$('.makeAuth').remove();
	}
	if(install_date != '-'){
		install_month = new Date(install_date).format('yyyyMM');
	}else{
		install_month = '999999';
	}
});
</script>


<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/report/report.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>
	
<h1 class="page-title">${currentMenuName}</h1>
<c:if test="${empty install_date}">
	<!-- 솔루션 설치일이 code에 등록되어 있지 않은경우 나타남 -->
	<div id = "install_date_modal" class="modal" style="width: 95%; position: absolute; top: 260px; display: block">
		<div style="width:310px; height:115px; border: 1px solid black; position: relative; z-index: 11; margin: 0 auto; background: white">
			<div class="portlet-title" style="background-color: #2B3643;padding:2px">
		        <div class="caption" style="z-index: 12; color: white">
		       	 <i class="fa fa-search"></i>솔루션 설치일 등록</div>
	        </div>
	        <div style="padding:5px">
		        <div class="input-medium date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd" style="margin:0 auto; padding: 5px;display: inline-block">
					<input type="text" class="form-control" id="install_date" name="install_date" value="<fmt:formatDate value="${now}" pattern="yyyy-MM-dd" />" style="display: inline-block;" readonly="readonly">
				</div>
				<button type="button"
					class="btn btn-sm blue btn-outline sbold uppercase"
					onclick="saveInstallDate()">
					등록
				</button>
			    <div style="font-size: 12px; color: red">※이 창은 최초 설치 시 나타나는 창입니다</div>
			    <div style="font-size: 12px; color: red">해당 일자의 다음달 부터 보고서를 생성할 수 있습니다.</div>
		    </div>
		</div>
	</div>
</c:if>
<div id = "report_option_modal" class="modal" style="width: 95%; position: absolute; top: 260px;">
		<div style="width:500px; height:372px; border: 1px solid black; position: relative; z-index: 11; margin: 0 auto; background: white">
			<div class="portlet-title" style="background-color: #2B3643;padding:2px">
		        <div class="caption" style="z-index: 12; color: white">
		       	 <i class="fa fa-search"></i>보고서 설정
		       	 <i id="option_close" class="fa fa-times-circle" onclick="modalClose(this)" style="float: right;font-size: 16px;padding: 2px;cursor: pointer;"></i>
		       	 </div>
	        </div>
	        <div style="padding:20px">
				<label class=""></label> <label class="control-label"  title="아직 구현중입니다" >
				<input id="input_chart" type="checkbox"/> 차트포함</label>
				<label class=""></label> <label class="control-label" title="아직 구현중입니다" >
				<input id="input_description" type="checkbox" onchange="input_description(this)"/> 총평포함</label>
				<textarea id="report_description" rows="12" cols="62" style="resize: none;" disabled="disabled"></textarea>
				<button type="button"
					class="btn btn-sm blue btn-outline sbold uppercase"
					onclick="saveReportOption()" style="margin-top:8px">
					저장
				</button>
		    </div>
		</div>
	</div>

<div class="portlet light portlet-fit portlet-datatable bordered">
	<!-- 검색조건 -->
	<br/>
	<hr style="border-top: thick solid #509af1;" />
	<div class="portlet-body row" style="display: flex; padding:10px auto 10px auto;">
		<!-- 보고서 생성 대상시스템 -->
		<div class="col-md-3">
			<div class="body_part">
				<div class="portlet box content_subtitle">
					<i class="fas fa-dot-circle"></i>
					<h4>
						<b> 보고서 생성 대상시스템</b>
					</h4>
				</div>
				<div class="portlet box"
					style="border-top: thick solid #509af1; border-bottom: 1.4px solid lightgray; width: 100%; margin: 0 auto; height: 77px"></div>
				<div>
					<table
						class="table table-striped table-bordered table-hover order-column">
						<colgroup>
							<col width="20%" />
							<col width="80%" />
						</colgroup>
						<thead>
							<tr>
								<th scope="col" style="text-align: center;"><input
									type="checkbox" id="allCheck"
									onclick="javascript:fnallCheck();" checked /> 전체</th>
								<th scope="col" style="text-align: center;">시스템명</th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${empty sysAuthIds}">
									<tr>
										<td colspan="2" align="center">데이터가 없습니다.</td>
									</tr>
								</c:when>
								<c:otherwise>
									<c:forEach items="${sysAuthIds}" var="sysCode"
										varStatus="status">
										<tr>
											<td style="text-align: center;"><input type="checkbox"
												name="reportCheck${status.index}" class="reportSys"
												id="reportCheck${status.index}" value="${sysCode}"
												onclick="checkOneCheck(this);checkAllCheck()" checked /></td>
											<td style="padding-left: 20px;"><ctl:nullCv
													nullCheck="${systemMap[sysCode]} (CODE:${sysCode})" /></td>
										</tr>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- 보고서 개요 -->
		<div class="col-md-9">
			<div class="body_part">
				
				<div class="portlet box"
					style="border-bottom: 1.4px solid lightgray; width: 100%; margin: 0;">
					<div id="intro_content" style="height: 50px">
						<div class="portlet-body box"
							style="width: 1000px; border-radius: 5px !important; padding: 15px !important; overflow: auto;">
							<div class="form-body"
								style="padding-left: 10px; padding-right: 10px;">
								<div class="form-group">
									<div data-repeater-list="group-a">
										<div class="row">
											<div class="col-md-2" style="padding:9px">
												<i class="fas fa-dot-circle"></i>
												<h4>
													<b> 보고서</b>
												</h4>
											</div>
											<div class="col-md-2">
												<select
													id="year_select" class="form-control" style="width: 100%"
													onchange="reportYearChange(this)">
													<c:forEach var="i" begin="${Year-10}" end="${Year}"
														step="1">
														<option value="${i }"
															<c:if test="${i eq Year}">selected</c:if>>${i }년</option>
													</c:forEach>
												</select>
											</div>
											<div class="col-md-3">
												<select
													id="report_select" class="form-control" style="width: 100%"
													onchange="reportIntroChange(this)">

													<c:forEach var="report" items="${reportcode }">
														<option value="${report.code_id }">${report.code_name }</option>
													</c:forEach>
												</select>
											</div>
											<div class="col-md-2"></div>
											<div class="col-md-3">
												<c:if test="${userAuth.make_report_auth eq 'N'}">
													<button type="button"
														class="btn btn-sm red btn-outline sbold uppercase"
														style="height: 34px; float: right" title="해당 계정은 보고서 생성 권한이 없습니다. 관리자에게 문의해주세요">
														보고서 생성 불가
													</button>
												</c:if>
												<c:if test="${userAuth.make_report_auth eq 'Y'}">
													<button type="button"
														class="btn btn-sm blue btn-outline sbold uppercase"
														onclick="modalOpen()" style="height: 34px; float: right">
														<i class="fa fa-cog"></i>
														설정
													</button>
												</c:if>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="portlet-body form">
				<div class="form-body">
					<div class="form-group">
						<div data-repeater-list="group-a">
							<div class="portlet box"
								style="width: 1020px; margin: 20px auto; margin-left: 5px;">
								<div class="row"
									style="width: 1020px; margin: 10px auto 0 auto;">
									<div
										style="font-size: medium; font-weight: bold; width: 220px; height: 220px; float: left;">
										<!-- 연간 보고서 -->
										<div
											style="display: inline-block; width: 200px; margin-left: 20px;">
											<label class="control-label"> <i
												class="fas fa-dot-circle"></i>
												<h4>
													<b> 연간 보고서</b>
												</h4>
											</label>
										</div>
										<div class="portlet box" width="200px"
											style="margin: 0 0 0 0; text-align: center; clear: left;">
											<div
												style="font-weight: 600; position: relative; display: inline-block;">
												<img
													src="${rootPath}/resources/image/reportmanage/bg_shelves01.png"
													width="200px" height="210px" />
												<div
													style="position: absolute; width: 200px; top: 8px; left: auto; display: flex; justify-content: space-around;">
													<label> <span
														class="report report_disable report_year"> <span
															class="q_report_font q_report_font_top"><p
																	class="year myclassTemp">${Year}</p> 통합</span> <span
															class="q_report_font q_report_font_bottom">보고서</span> <img
															class="report_img" alt=""
															src="${rootPath}/resources/image/reportmanage/report_img_enable.png" />
															<div class="report_shadow">
																<img class="report_new_btn" style="position: absolute;"
																	alt=""
																	src="${rootPath}/resources/image/reportmanage/btn_new.png"
																	onclick="make_report(4,1,this)" /> <img
																	class="report_loading" style="position: absolute;"
																	alt="" src="${rootPath}/resources/image/loading3.gif" />
																<span class="report_check"> <img
																	class="check_img" alt=""
																	src="${rootPath}/resources/image/reportmanage/img_check.png" />
																</span>
															</div>
													</span> <input class="check_box check_top" type="checkbox"
														disabled="disabled" /> <input class="report_seq"
														type="hidden" value="" /> <input class="userfile_yn"
														type="hidden" value="N" />
													</label>
												</div>
											</div>
										</div>
									</div>
									<div
										style="font-size: medium; font-weight: bold; width: 770px; height: 220px; float: right;">
										<!-- 분기별 보고서 -->
										<div
											style="display: inline-block; width: 770px; margin-left: 30px;">
											<label class="control-label"> <i
												class="fas fa-dot-circle"></i>
												<h4>
													<b> 분기별 보고서</b>
												</h4>
											</label>
										</div>
										<div class="portlet box" width="780px"
											style="margin: 0 0 0 0; text-align: center; clear: right;">
											<div style="position: relative; display: inline-block;">
												<img alt=""
													src="${rootPath}/resources/image/reportmanage/background.jpg"
													width="761px" height="210px" />
												<div
													style="position: absolute; width: 761px; top: 8px; left: 0; display: flex; justify-content: space-around;">
													<c:forEach var="i" begin="1" end="4" step="1">
														<label> <span
															class="report report_disable report_quarter"> <span
																class="q_report_font q_report_font_top"><p
																		class="year myclassTemp">${Year}</p> ${i}분기</span> <span
																class="q_report_font q_report_font_bottom">보고서</span> <img
																class="report_img" alt=""
																src="${rootPath}/resources/image/reportmanage/report_img_enable.png" />
																<div class="report_shadow">
																	<img class="report_new_btn" style="position: absolute;"
																		alt=""
																		src="${rootPath}/resources/image/reportmanage/btn_new.png"
																		onclick="make_report(2,${1+(i-1)*3},this)" /> <img
																		class="report_loading" style="position: absolute;"
																		alt="" src="${rootPath}/resources/image/loading3.gif" />
																	<span class="report_check" style=""> <img
																		class="check_img" alt=""
																		src="${rootPath}/resources/image/reportmanage/img_check.png" />
																	</span>
																</div>
														</span> <input class="check_box check_mid" type="checkbox"
															disabled="disabled" /> <input class="report_seq"
															type="hidden" value="" /> <input class="userfile_yn"
															type="hidden" value="N" />
														</label>
													</c:forEach>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<%-- 			<div id="monthRepo rtBtnDiv">
				<div class="btn_group month_group">
					<label id="monthLabelAtch" style="margin-left:10px;"></label>
					<!-- 월별 보고서 버튼  -->
					<label onclick="report_replace('month')"><img alt="" src="${rootPath}/resources/image/reportmanage/btn_renew.png">재생성</label>
					<label onclick="report_delete('month')"><img alt="" src="${rootPath}/resources/image/reportmanage/btn_del.png">삭제</label>
					<label onclick="report_download('month')"><img alt="" src="${rootPath}/resources/image/reportmanage/btn_download.png">다운로드</label>
					<!-- 업로드 기능 임시 비활성 -->
					<label onclick="report_upload('month')"><img alt="" src="${rootPath}/resources/image/reportmanage/btn_upload.png">업로드</label>
					<label class="userfilebtn" onclick="report_userfile_download('month')"><img alt="" src="${rootPath}/resources/image/reportmanage/btn_download.png">사용자 파일 다운로드</label>
					<input id="month_seq" class="btn_seq" type="hidden" value="" />						
				</div>
			</div> --%>
			<%-- 			<div id="yearReportBtnDiv">
				<div class="btn_group year_group">	
					<label id="yearLabelAtch" style="margin-left:10px;"></label>
					<!-- 연간 보고서 버튼 -->
					<label onclick="report_replace('year')"><img alt="" src="${rootPath}/resources/image/reportmanage/btn_renew.png">재생성</label>
					<label onclick="report_delete('year')"><img alt="" src="${rootPath}/resources/image/reportmanage/btn_del.png">삭제</label>
					<label onclick="report_download('year')"><img alt="" src="${rootPath}/resources/image/reportmanage/btn_download.png">다운로드</label>
					
					<c:if test="${false}">
						<!-- 업로드 기능 임시 비활성 -->
						<label onclick="report_upload('year')"><img alt="" src="${rootPath}/resources/image/reportmanage/btn_upload.png" />업로드</label>
						<label class="userfilebtn" onclick="report_userfile_download('year')"><img alt="" src="${rootPath}/resources/image/reportmanage/btn_download.png" />사용자 파일 다운로드</label>
					</c:if>
					<input id="year_seq" class="btn_seq" type="hidden" value="" />
				</div>
			</div>
			<div id="quarterReportBtnDiv">
				<div class="btn_group quarter_group" style="text-align: left;">
					<label id="quarterLabelAtch" style="margin-left:10px;"></label>
					<!-- 분기별 보고서 버튼  -->
					<label onclick="report_replace('quarter')"><img alt="" src="${rootPath}/resources/image/reportmanage/btn_renew.png">재생성</label>
					<label onclick="report_delete('quarter')"><img alt="" src="${rootPath}/resources/image/reportmanage/btn_del.png">삭제</label>
					<label onclick="report_download('quarter')"><img alt="" src="${rootPath}/resources/image/reportmanage/btn_download.png">다운로드</label>
					<c:if test="${false}">
						<!-- 업로드 기능 임시 비활성 -->
						<label onclick="report_upload('quarter')"><img alt="" src="${rootPath}/resources/image/reportmanage/btn_upload.png">업로드</label>
						<label class="userfilebtn" onclick="report_userfile_download('quarter')"><img alt="" src="${rootPath}/resources/image/reportmanage/btn_download.png">사용자 파일 다운로드</label>
					</c:if>
					<input id="quarter_seq" class="btn_seq" type="hidden" value="" />
				</div>
			</div> --%>
			<div id="monthRepo rtBtnDiv">
				<div class="btn_group month_group">
					<label id="LabelAtch" style="margin-left: 10px;"></label>
					<!-- 월별 보고서 버튼  -->
					<label class='makeAuth' onclick="report_replace('month')"><img alt=""
						src="${rootPath}/resources/image/reportmanage/btn_renew.png">재생성</label>
					<label class='makeAuth' onclick="report_delete('month')"><img alt=""
						src="${rootPath}/resources/image/reportmanage/btn_del.png">삭제</label>
					<label>
					<img alt="" src="${rootPath}/resources/image/reportmanage/btn_download.png">다운로드</label>
					<a class="btn btn-xsm blue btn-outline sbold uppercase" onclick="report_word('${list.report_seq}')">
						<i class="fas fa-file-word"></i>
						DOC
					</a>
					<a class="btn btn-xsm red btn-outline sbold uppercase" onclick="report_pdf('month')">
						<i class="fas fa-file-pdf"></i>
						PDF
					</a>
					<!-- 업로드 기능 임시 비활성 -->
					<label class='makeAuth' onclick="report_upload('month')"><img alt=""
						src="${rootPath}/resources/image/reportmanage/btn_upload.png">업로드</label>
					<input type="text" id="uploadReportName" value="" disabled="disabled">
					<label class="userfilebtn" onclick="report_userfile_download('month')">
						<img alt="" src="${rootPath}/resources/image/reportmanage/btn_download.png">
					</label> 
					<input id="btn_seq" class="btn_seq" type="hidden" value="" />
				</div>
			</div>
			<div class="body_part" style="padding: 13px;">
				<div class="portlet box content_subtitle"
					style="margin: 10px 20px 10px;">
					<i class="fas fa-dot-circle"></i>
					<h4>
						<b> 월별 보고서 </b>
					</h4>
				</div>
				<div class="portlet box"
					style="width: 1020px; margin: 10px 0px; padding: 0 0 0 0;">
					<div
						style="position: relative; width: 1009px; display: inline-block;">
						<img alt=""
							src="${rootPath}/resources/image/reportmanage/background.jpg"
							width="1009px" height="210px" />
						<div
							style="position: absolute; width: 100%; top: 8px; left: 0; display: flex; justify-content: space-around; padding: 0px 15px">
							<c:forEach var="i" begin="1" end="6" step="1">
								<label> <span
									class="report report_disable report_month_t">
										<span class="q_report_font q_report_font_top"><p
												class="year myclassTemp">${Year}</p> ${i}월</span> <span
										class="q_report_font q_report_font_bottom">보고서</span> <img
										class="report_img" alt=""
										src="${rootPath}/resources/image/reportmanage/report_img_enable.png">
										<div class="report_shadow">
											<img class="report_new_btn" style="position: absolute;"
												alt=""
												src="${rootPath}/resources/image/reportmanage/btn_new.png"
												onclick="make_report_month(1,${i},this)"> <img
												class="report_loading" style="position: absolute;" alt=""
												src="${rootPath}/resources/image/loading3.gif"> <span
												class="report_check" style=""> <img class="check_img"
												alt=""
												src="${rootPath}/resources/image/reportmanage/img_check.png">
											</span>
										</div>
								</span> <input class="check_box check_bot" type="checkbox"
									disabled="disabled" /> <input class="report_seq" type="hidden"
									value="" /> <input class="userfile_yn" type="hidden" value="N" />
								</label>
							</c:forEach>
						</div>
					</div>
					<div
						style="position: relative; display: inline-block; margin-top: 20px; width: 1009px%;">
						<img alt=""
							src="${rootPath}/resources/image/reportmanage/background.jpg"
							width="1009px" height="210px" />
						<div
							style="position: absolute; width: 100%; top: 8px; left: 0; display: flex; justify-content: space-around; padding: 0px 15px">
							<c:forEach var="i" begin="7" end="12" step="1">
								<label> <span
									class="report report_disable report_month_t">
										<span class="q_report_font q_report_font_top"><p
												class="year myclassTemp">${Year}</p> ${i}월</span> <span
										class="q_report_font q_report_font_bottom">보고서</span> <img
										class="report_img" alt=""
										src="${rootPath}/resources/image/reportmanage/report_img_enable.png">
										<div class="report_shadow">
											<img class="report_new_btn" style="position: absolute;"
												alt=""
												src="${rootPath}/resources/image/reportmanage/btn_new.png"
												onclick="make_report_month(1,${i},this)"> <img
												class="report_loading" style="position: absolute;" alt=""
												src="${rootPath}/resources/image/loading3.gif"> <span
												class="report_check" style=""> <img class="check_img"
												alt=""
												src="${rootPath}/resources/image/reportmanage/img_check.png">
											</span>
										</div>
								</span> <input class="check_box check_bot" type="checkbox"
									disabled="disabled"> <input class="report_seq"
									type="hidden" value=""> <input class="userfile_yn"
									type="hidden" value="N">
								</label>
							</c:forEach>
						</div>
					</div>
				</div>
			</div>
			<!-- BUTTON -->
		</div>
	</div>
	<form id="listForm" method="post" style="display: none">
		<input type="hidden" name="current_menu_id" value="${currentMenuId}" /> 
		<input type="hidden" name="page_num" />
		<input type="hidden" name="search_from" value="${search.search_from}" /> 
		<input type="hidden" name="search_to" value="${search.search_to}" /> 
		<input type="hidden" name="report_type" value="${search.report_type}" />
		<input type="hidden" name="isSearch" value="${search.isSearch}" />
		<input type="hidden" name="start_date" />
		<input type="hidden" name="end_date" />
		<input type="hidden" name="path" />
		<input type="hidden" name="title" />
		<input type="hidden" name="report_seq" />
	</form>
	<form action="report_upload.html" method="post" id="fileForm" name="fileForm" enctype="multipart/form-data">
		<input type="hidden" name="report_seq" />
		<input type="hidden" name="report_type" />
		<input type="file" id="file" name="file" class="file" />
	</form>
</div>
<div id="test" style="width: 0px; height: 0px; overflow: hidden;"></div>

