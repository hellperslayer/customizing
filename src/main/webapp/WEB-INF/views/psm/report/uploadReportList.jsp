<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html>
<table id="user" class="table table-bordered table-striped" style="box-sizing: border-box;">
	<colgroup>
		<col width="10%" />
		<col width="40%" />
		<col width="20%" />
		<col width="20%" />
		<col width="10%" />
	</colgroup>
	<tbody>
		<c:forEach var="report" items="${reportList }">
		<tr>
			<td style="text-align: center">${report.report_id }</td>
			<td>${report.file_path }</td>
			<td>
				<fmt:formatDate value="${report.upload_date }" pattern="YYYY-MM-dd" />
			</td>
			<td>
				<fmt:formatDate value="${report.delete_date }" pattern="YYYY-MM-dd" />
			</td>
			<td style="text-align: center ; padding: 2px">
				<button class="btn btn-sm red-mint btn-outline sbold uppercase"
					onclick="upLoadReportRemove('${report.report_id}')">
					<i class="fa fa-remove"></i> <font>삭제
				</button>
			</td>
		</tr>
		</c:forEach>
		<c:if test="${fn:length(reportList) == 0 }">
		<tr>	
			<td style="text-align: center" colspan="5">
				업로드된 보고서가 없습니다.
			</td>
		</tr>
		</c:if>
	</tbody>
</table>