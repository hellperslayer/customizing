<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<script src="${rootPath}/resources/js/jquery/jquery.min.js" type="text/javascript" charset="UTF-8"></script>

<script src="${rootPath}/resources/js/amchart/amcharts.js"></script>
<script src="${rootPath}/resources/js/amchart/serial.js"></script>
<script src="${rootPath}/resources/js/amchart/export.min.js"></script>
<script src="${rootPath}/resources/js/amchart/amstock.js"></script>
<script src="${rootPath}/resources/js/amchart/xy.js"></script>
<script src="${rootPath}/resources/js/amchart/radar.js"></script>
<script src="${rootPath}/resources/js/amchart/light.js"></script>
<script src="${rootPath}/resources/js/amchart/pie.js"></script>
<script src="${rootPath}/resources/js/amchart/gantt.js"></script>
<script src="${rootPath}/resources/js/amchart/gauge.js"></script>

<%-- <link rel="stylesheet" href="${rootPath}/resources/css/export.css" type="text/css" media="all"> --%>
<link rel="stylesheet" href="${pageContext.request.scheme }://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/css/export.css" type="text/css" media="all">
<script src="${rootPath}/resources/js/psm/report/report_charts2.js"></script>
<script src="${rootPath}/resources/js/common/ajax-util.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/common-prototype-util.js" type="text/javascript" charset="UTF-8"></script>

<script type="text/javascript" charset="UTF-8">
	rootPath = '${rootPath}';
	contextPath = '${pageContext.servletContext.contextPath}';
	var start_date = '${start_date}';
	var end_date = '${end_date}';
	var strReq = '${CACHE_REQ_TYPE}';
	var period_type = '${period_type}';
	var system_seq = '${system_seq}';
	var use_studentId = '${use_studentId}';
	var download_type = '${download_type}';
	var pdfFullPath = '${pdfFullPath}';
	/* 워드보고서 데이터를 넣기 위한 보고서 키값 */
	var report_seq = '${param.report_seq}';
	var is_last = '${param.is_last}';
</script>

<HEAD>
<META NAME="Generator" CONTENT="Hancom HWP 8.5.8.1541">
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">

<script type="text/javascript">

var saveCnt = 0;
function exportChartToImg() {
	var charts = {};
		if(download_type == 'pdf') {
			$("#buttonDiv").css("display","none");
			$(".downloadChartClass").css("height","950px");
			$(".downloadChartClass").css("width","950px");
			$(".downloadClass").css("width","950px");
			for (var x = 0; x < AmCharts.charts.length; x++) {
				$("#"+AmCharts.charts[x].div.id).css("width","950px");
				console.log("#"+AmCharts.charts[x].div.id+" "+$("#"+AmCharts.charts[x].div.id).css);
			}
		} else {
			$(".downloadClass").css("width","580px");
			$(".downloadChartClass").css("width","580px");
			for (var x = 0; x < AmCharts.charts.length; x++) {
				$(AmCharts.charts[x].div).css("width","580px");
			}	
		}
		
	 	var lts = setInterval(function() {
			charts = AmCharts.charts;
			for ( var x in charts) {
				var chart = charts[x];
				console.log(chart["export"]);
				chart["export"].capture({}, function() {
					this.toJPG({}, function(data) {
						var chartName = this.setup.chart.div.id;
						$("img[name="+chartName+"]").attr("src", data);
						$("img[name="+chartName+"]").css("display","");
						console.log(chartName);
						$("#"+chartName).css("display","none");
						
						saveCnt++;
						if(saveCnt == AmCharts.charts.length) {
							$("img[name=logoImg]").attr("src",getBase64Image(document.getElementById("logoImg")));
							$("img[name=logoImg]").css("display","");
							$("#logoImg").remove();
							//exportDocs();
						}
					});
				});
				clearInterval(lts);
			}
		}, 500);
}

function getBase64Image(img) {
  var canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0,img.width,img.height);
  var dataURL = canvas.toDataURL("image/png");
  return dataURL; 
}
	
function exportDocs(inter) {
	var date = new Date();
	var year = date.getFullYear();
	var month = new String(date.getMonth() + 1);
	var day = new String(date.getDate());
	var admin_user_id = "${userSession.admin_user_id}";

	var reportType = '${reportType}';
	
	var reportdate = "${start_date}".split('-');
	var reportyear = reportdate[0];
	var reportmonth = reportdate[1];
	
	var proc_date = reportyear+reportmonth;

	var periodType = '${period_type}';
	var periodTitle = "";
	if(periodType == 1){
		periodTitle = reportmonth + '월';
	}else if(periodType == 2){
		var part = "";
		if(reportmonth == 1){
			part = "1";
		}else if(reportmonth == 4){
			part = "2";
		}else if(reportmonth == 7){
			part = "3";
		}else if(reportmonth == 10){
			part = "4";
		}
		periodTitle = part+'분기';
	}else if(periodType == 3){
		var part = "";
		if(reportmonth == 1){
			part = "상";
		}else if(reportmonth == 7){
			part = "하";
		}
		periodTitle = part+'반기';
	}else if(periodType == 4){
		periodTitle = '연간';
	}else if(periodType == 5){
		periodTitle = '기간';
	}
	
	var log_message_title = reportyear+'년_'+ periodTitle + '_CPO_오남용보고서';
	
	if(month.length == 1){ 
		month = "0" + month; 
	} 
	if(day.length == 1){ 
		day = "0" + day; 
	} 
	var regdate = year + "" + month + "" + day;
	var agent = navigator.userAgent.toLowerCase();
	
	var header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' "+
    "xmlns:w='urn:schemas-microsoft-com:office:word' "+
    "xmlns='http://www.w3.org/TR/REC-html40'>"+
    "<head><meta charset='utf-8'><title>Export HTML to Word Document with JavaScript</title></head><body>";
	var footer = "</body></html>";
	var sourceHTML = header+document.getElementById("source-html").innerHTML+footer;
// 	var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
	var source = 'data:application/msword;charset=utf-8,' + encodeURIComponent(sourceHTML);
	
	/* 이전 점검보고서 관리 페이지와 호환을 위한 변수 */	
	var reportPath = '/report/addReport.html';
	if(download_type == 'word'){
		reportPath = '/report/addWord.html';
	} 
	
	$.ajax({
		type: 'POST',
		url: rootPath + reportPath,
		data: { 
			"source" : source,
			"log_message_title" : log_message_title,
			"admin_user_id" : admin_user_id,
			"report_type" : reportType,
			"proc_date" : proc_date,
			"pdfFullPath" : pdfFullPath,
			"period_type" : periodType,
			"report_seq" : report_seq,
			"system_seq" : system_seq,
			"system_report" : "Y"        
		},
		success: function(data) {
			if(data == 1) {
                self.close();
            } else if(data == 2) {
                if(confirm('기존 보고서가 있습니다. \n\n덮어 쓰시겠습니까?')){
                    alert("보고서 재등록 성공");
                    self.close();
                }
            } else if(data == 3 && is_last == 1) {
                alert("보고서 등록 성공");
                parent.$('#loading').hide();
            }else if(data == 3 && is_last == 0){
                
            }else{
                alert("보고서 등록 실패");
                parent.$('#loading').hide();
            }
			//parent.$('#loading').hide();
		}
	});
	
	clearInterval(inter);
}

function saveWordReport() {
	parent.$('#loading').show();
	//exportChartToImg();
	//var inter = setInterval(function() {
	//	if(saveCnt == AmCharts.charts.length) {
			exportDocs(1);
	//	}	
	//}, 500)
}

function printpr()
{
	$("#printButton").hide();
	$("#saveButton").hide();
	
	var $form = $("#manageHistForm");
	var log_message_params = '';
	var menu_id = '${menu_id}';
	var log_message_title = 'CPO_오남용보고 출력';
	var log_action = 'PRINT';
	var type = 'add';
	var url = '${rootPath}/report/download.html';
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	sendAjaxPostRequest(url, $form.serialize(), ajaxReport_successHandler, ajaxReport_errorHandler, type);
}

function printHtml()
{
	 var divContents = $("#source-html").html();
     var printWindow = window.open('', '', 'height=400,width=800');
     printWindow.document.write('<html><head><title>DIV Contents</title>');
     printWindow.document.write('</head><body >');
     printWindow.document.write(divContents);
     printWindow.document.write('</body></html>');
     printWindow.document.close();
     printWindow.print();
	
}

rootPath = '${rootPath}';
contextPath = '${pageContext.servletContext.contextPath}';
var start_date = '${start_date}';
var end_date = '${end_date}';
var period_type = '${period_type}';

function ajaxReport_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = "${rootPath}/loginView.html";
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		window.print();
		self.close();
	}
}

// 관리자 ajax call - 실패
function ajaxReport_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	log_message += codeLogMessage[actionType + "Action"];
	
	alert("실패하였습니다." + textStatus);
}
</script>

<form id="manageHistForm" method="POST">
</form>

<STYLE type="text/css">
@media print {
  .amcharts-bg, .amcharts-plot-area, .amcharts-legend-bg, .amcharts-axis-grid {
    display: none;
  }
}

#loading {
	 width: 100%;   
	 height: 100%;   
	 top: 0px;
	 left: 0px;
	 position: fixed;   
	 display: block;   
	 opacity: 0.7;   
	 background-color: #fff;   
	 z-index: 99;   
	 text-align: center; 
 }  
  
#loading-image {   
	 position: absolute;   
	 top: 50%;   
	 left: 50%;  
	 z-index: 100; 
 } 
</style>
<STYLE type="text/css">
p.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle7
/* 	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;} */
p.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:12.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:left; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle14
	{style-name:"xl65"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:12.0pt; font-family:굴림; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle14
	{style-name:"xl65"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:12.0pt; font-family:굴림; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle14
	{style-name:"xl65"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:100%; font-size:12.0pt; font-family:굴림; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle15
	{style-name:"xl67"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; background-color:#e5e5e5; font-size:12.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle15
	{style-name:"xl67"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; background-color:#e5e5e5; font-size:12.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle15
	{style-name:"xl67"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:center; text-indent:0.0pt; line-height:100%; background-color:#e5e5e5; font-size:12.0pt; font-family:맑은 고딕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
</STYLE>
</HEAD>

<script type="text/javascript">
$(document).ready(function() {
	$('#loading').hide();
	if(download_type == 'word'){
		saveWordReport();
	}
});

$(window).load(function() {   
	$('#loading').hide();
});
</script>

<BODY>
<div id="loading"><img id="loading-image" width="30px;" src="${rootPath}/resources/image/loading3.gif" alt="Loading..." /></div>
<c:set var="startdate" value="${fn:split(start_date, '-') }"/>
<c:set var="enddate" value="${fn:split(end_date, '-') }"/>
<c:set var="date" value="${fn:split(date, '-') }"/>
<c:choose>
	<c:when test="${period_type == 2 }">
		<c:choose>
		<c:when test="${startdate[1] == '01' }">
		<c:set var="m1" value="1월"/>
		<c:set var="m2" value="2월"/>
		<c:set var="m3" value="3월"/>
		</c:when>
		<c:when test="${startdate[1] == '04' }">
		<c:set var="m1" value="4월"/>
		<c:set var="m2" value="5월"/>
		<c:set var="m3" value="6월"/>
		</c:when>
		<c:when test="${startdate[1] == '07' }">
		<c:set var="m1" value="7월"/>
		<c:set var="m2" value="8월"/>
		<c:set var="m3" value="9월"/>
		</c:when>
		<c:when test="${startdate[1] == '10' }">
		<c:set var="m1" value="10월"/>
		<c:set var="m2" value="11월"/>
		<c:set var="m3" value="12월"/>
		</c:when>
		</c:choose>
	</c:when>
	<c:when test="${period_type == 4 }">
		<c:set var="q1" value="1분기"/>
		<c:set var="q2" value="2분기"/>
		<c:set var="q3" value="3분기"/>
		<c:set var="q4" value="4분기"/>
	</c:when>
</c:choose>
<div id="source-html">
<div align="right" id="buttonDiv">
	<input type=button id="printButton" name="printButton"onclick="printpr();" value="출력"/>
	<input type=button id="saveButton" name="saveButton" onclick="saveWordReport();" value="생성"/>
</div>

<div align="center">
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'></P>
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'></P>
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'></P>
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<c:if test="${use_reportLine eq 'Y' }"><!-- 결재라인 -->
<div id="logo2" align="right" style="margin-right: 10%;">
${authorize }
</div>
</c:if>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<TABLE border="1" cellspacing="0" align="center" cellpadding="0" style='border-collapse:collapse;border:none;' >
<TR>
	<TD valign="middle" style='width:627;height:123;border-left:double #000000 1.4pt;border-right:double #000000 1.4pt;border-top:double #000000 1.4pt;border-bottom:double #000000 1.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
	<P id="title"  CLASS=HStyle0 STYLE='text-align:center;line-height:150%;'><SPAN STYLE='font-size:19.0pt;font-family:"돋움";letter-spacing:-4%;font-weight:"bold";font-style:"italic";line-height:150%'>개인정보의 안전한 관리를 위한</SPAN></P>
	<P CLASS=HStyle0 STYLE='text-align:center;line-height:150%;'><SPAN STYLE='font-size:25.0pt;font-family:"돋움";letter-spacing:-4%;font-weight:"bold";line-height:150%'>개인정보 오남용 점검보고서</SPAN></P>
	</TD>
</TR>
</TABLE>
</div>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'>
<SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#000000;line-height:180%'>
<c:if test="${fn:length(systems_name) == 1 }" >
	${systems_name[0].system_name }
</c:if>
</SPAN>
</P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>

<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;text-align:center;line-height:180%;'><SPAN STYLE='font-size:20.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";line-height:180%'>
${reportDate }
</SPAN></P>


<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
<!-- 로고 사용여부 -->
<c:if test="${use_reportLogo eq 'Y' }">
    <c:choose>
        <c:when test="${download_type ne 'pdf' }"> <!-- word -->
            <div align="center" style="margin-top: 5%;">
                <TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
                <TR>
                    <TD valign="middle" style='border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                    <c:choose>
                        <c:when test="${filename eq null }" >
                            <div style=" height: 77px; text-align: center; "> <!-- background-color: #ffffff; -->
                                <img style="width: auto;" id="logoImg" src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}${logo_report_url}">
                                <img style="height: 77; width: auto; display: none" name="logoImg">
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div style="max-height: 70px; text-align: center;"> <!-- max-width: 300px; -->
                                <img style="height: 70px; width: auto;" id="logoImg" src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/upload/${filename }"> <!-- max-width: 300px; -->
                                <img style="height: 70px; width: auto;" name="logoImg" style="display: none">
                            </div>
                        </c:otherwise>
                    </c:choose>
                    </TD>
                </TR>
                </TABLE>
            </div>
        </c:when>
        <c:otherwise> <!-- pdf -->
            <div align="center" style="margin-top: 30%;">
                <TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
                <TR>
                    <TD valign="middle" style='width:342;height:77;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
                    <c:choose>
                        <c:when test="${filename eq null }" >
                            <P CLASS="logo" STYLE='text-align:center;'><SPAN STYLE='font-size:18.0pt;line-height:160%'>
                                <img id="logoImg" style="width: 100%;" src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}${logo_report_url}">
                                <img name="logoImg" style="display: none">
                            </SPAN></P>
                        </c:when>
                        <c:otherwise>
                            <P CLASS="logo" STYLE='text-align:center;'><SPAN STYLE='font-size:18.0pt;line-height:160%'>
                                <img id="logoImg" style="height: 100%;" src="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${rootPath}/resources/upload/${filename }">
                                <img name="logoImg" style="display: none">
                            </SPAN></P>
                        </c:otherwise>
                    </c:choose>
                    </TD>
                </TR>
                </TABLE>
            </div>
        </c:otherwise>
    </c:choose>
</c:if>
<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'></P>
<div style='page-break-before:always'></div><br style="page-break-before: always">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:100%;'>
<TR>
	<TD valign="middle" bgcolor="#4e9fd7"  style='width:100%;height:44;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0><SPAN STYLE='font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;line-height:160%;width:100%;'>Ⅰ. 점검 개요</SPAN></P>
	</TD>
</TR>
</TABLE>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;margin-top:8.0pt;margin-bottom: 0px;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>1. 점검 목적</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:20pt;line-height:180%;margin-top: 5px;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-3%;line-height:180%'> 개인정보처리시스템의 접속기록을 점검하여 불법적인 접근 및 비정상</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'> </SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;line-height:180%'>행위 등을 탐지 분석하여 개인정보의 오·남용 사례를 방지하고</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'> 안전하게 관리할 수 있도록 관리</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:3%;line-height:180%'>·</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>감독을 수행함</SPAN></P>

<P CLASS=HStyle2 STYLE='margin-left:0.0pt;margin-top:8.0pt;margin-bottom: 0px;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>2. 관련 근거</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;margin-top: 5px;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보보호법 제29조 (안전조치의무)</SPAN></P>
<div align="left" style="margin-left: 10px; margin-top:5px;">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
    <TD valign="left" style='width:650;height:50;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
    <P CLASS=HStyle0 STYLE='margin-left:15.4pt;line-height:180%;'><SPAN STYLE='font-family:"돋움"'> 개인정보처리자는 개인정보가 분실·도난·유출·변조 또는 훼손되지 아니하도록 내부 관리계획 수립, 접속기록 보관 등 대통령령으로 정하는 바에 따라 안전성 확보에 필요한 기술적·관리적 및 물리적 조치를 하여야 한다.</SPAN></P>
    </TD>
</TR>
</TABLE>
</div>
<br>
<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;margin-top: 5px;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보보호법 시행령 제30조 (개인정보의 안전성 확보 조치)</SPAN></P>
<div align="left" style="margin-left: 10px; margin-top:5px;">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
    <TD valign="middle" style='width:650;height:90;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
    <P CLASS=HStyle0 STYLE='margin-left:15.4pt;text-indent:-15.2pt;line-height:200%;margin-bottom: 5px;margin-top: 5px;'><SPAN STYLE='font-family:"돋움"'>① 개인정보처리자는 다음 각 호의 사항이 포함된 개인정보의 처리 방침(이하 "개인정보 처리방침"이라 한다)을 정하여야 한다. 이 경우 공공기관은 제32조에 따라 등록대상이 되는 개인정보파일에 대하여 개인정보 처리방침을 정한다.</SPAN></P>
    <P CLASS=HStyle0 STYLE='margin-left:15.4pt;line-height:180%;margin-bottom: 5px;margin-top: 5px;'><SPAN STYLE='font-family:"돋움"'>
        1. 개인정보의 처리 목적<br>
		2. 개인정보의 처리 및 보유 기간<br>
		3. 개인정보의 제3자 제공에 관한 사항(해당되는 경우에만 정한다)<br>
		4. 개인정보처리의 위탁에 관한 사항(해당되는 경우에만 정한다)<br>
		5. 정보주체의 권리·의무 및 그 행사방법에 관한 사항<br>
		6. 그 밖에 개인정보의 처리에 관하여 대통령령으로 정한 사항</SPAN></P>
    <P CLASS=HStyle0 STYLE='margin-left:15.4pt;text-indent:-15.2pt;line-height:180%;margin-bottom: 5px;margin-top: 5px;'><SPAN STYLE='font-family:"돋움"'>② 개인정보처리자가 개인정보 처리방침을 수립하거나 변경하는 경우에는 정보주체가 쉽게 확인할 수 있도록 대통령령으로 정하는 방법에 따라 공개하여야 한다.</SPAN></P>
    <P CLASS=HStyle0 STYLE='margin-left:15.4pt;text-indent:-15.2pt;line-height:180%;margin-bottom: 5px;margin-top: 5px;'><SPAN STYLE='font-family:"돋움"'>③ 개인정보 처리방침의 내용과 개인정보처리자와 정보주체 간에 체결한 계약의 내용이 다른 경우에는 정보주체에게 유리한 것을 적용한다.</SPAN></P>
    <P CLASS=HStyle0 STYLE='margin-left:15.4pt;text-indent:-15.2pt;line-height:180%;margin-bottom: 5px;margin-top: 5px;'><SPAN STYLE='font-family:"돋움"'>④ 안전행정부장관은 개인정보 처리방침의 작성지침을 정하여 개인정보처리자에게 그 준수를 권장할 수 있다.  &lt;개정 2013. 3. 23.&gt;</SPAN></P>
    </TD>
</TR>
</TABLE>
</div>
<br>
<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;line-height:180%;margin-top: 5px;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ </SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-3%;line-height:180%'>개인정보의 안전성 확보조치 기준 제8조(접속기록의 보관 및 점검)</SPAN></P>

<div align="left" style="margin-left: 10px; margin-top:5px;">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" style='width:650;height:150;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 3.1pt 1.4pt 3.1pt;'>
	<P CLASS=HStyle0 STYLE='margin-left:15.4pt;text-indent:-15.2pt;line-height:180%;margin-bottom: 5px;margin-top: 5px;'><SPAN STYLE='font-family:"돋움"'>① </SPAN><SPAN STYLE='font-family:"돋움";letter-spacing:-1%'>개인정보처리자는 개인정보취급자가 개인정보처리시스템에 접속한 기록을 1년 이상  보관·관리하여야 한다. 다만, 5만명 이상의 정보주체에 관하여 개인정보를 처리하거나, 고유식별정보 또는 민감정보를 처리하는 개인정보처리시스템의 경우에는 2년 이상 보관‧관리하여야 한다.</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:16.8pt;text-indent:-16.6pt;line-height:180%;margin-bottom: 5px;margin-top: 5px;'><SPAN STYLE='font-family:"돋움"'>② 개인정보처리자는 개인정보의 오‧남용, 분실·도난·유출·위조·변조 또는 훼손 등에 대응하기 위하여 개인정보처리시스템의 접속기록 등을 월 1회 이상 점검하여야 한다. 특히 개인정보를 다운로드한 것이 발견되었을 경우에는 내부 관리계획으로 정하는 바에 따라 그 사유를 반드시 확인하여야 한다.</SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-left:16.8pt;text-indent:-16.6pt;line-height:180%;margin-bottom: 5px;margin-top: 5px;'><SPAN STYLE='font-family:"돋움"'>③ 개인정보처리자는 개인정보취급자의 접속기록이 위·변조 및 도난, 분실되지 않도록 해당 접속기록을 안전하게 보관하여야 한다.</SPAN></P>
	</TD>
</TR>
</TABLE>
</div>

<div style='page-break-before:always'></div><br style="page-break-before: always">
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;margin-bottom: 0px;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>3. 점검 범위 </SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;margin-top: 5px;margin-bottom: 5px;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍  점검 범위</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:40pt;margin-top: 5px;margin-bottom: 5px;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>- </SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>
${startdate[0] }년 ${startdate[1] }월 ${startdate[2] }일 ~ ${enddate[0] }년 ${enddate[1] }월 ${enddate[2] }일까지의 개인정보 비정상접근 접속기록 로그</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:39.3pt;text-indent:-21.0pt;margin-top: 5px;margin-bottom: 5px;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍  점검 방법</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:40pt;margin-top: 5px;margin-bottom: 5px;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>- </SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>
개인정보 접속기록 현황 검토</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:40pt;margin-top: 5px;margin-bottom: 5px;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>- </SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-5%;line-height:180%'>
개인정보처리시스템에 대한 비정상 행위</SPAN></P>



<P CLASS=HStyle0 STYLE='margin-top:8.0pt;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>4. 점검 대상</SPAN></P>
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'></P>
<div align="left" style="margin-left: 10px">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<c:if test="${period_type == 1}">
<tr>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:8%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>번호</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:30%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>개인정보처리시스템</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:31%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>비정상위험 처리량</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:31%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>비정상위험 이용량</span></p>
	</td>
</tr>
</c:if>
<c:if test="${period_type == 2}">
<tr>
	<td rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:5%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>번호</span></p>
	</td>
	<td rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:25%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>개인정보처리시스템</span></p>
	</td>
	<td colspan="4" valign="middle" bgcolor="#e5e5e5"  style='width:35%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>비정상위험 처리량</span></p>
	</td>
	<td colspan="4" valign="middle" bgcolor="#e5e5e5"  style='width:35%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>비정상위험 이용량</span></p>
	</td>
</tr>
<tr>
	<c:forEach begin="1" end="2" step="1">
	<td valign="middle" bgcolor="#e5e5e5"  style='width:8%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>${m1}</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:8%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>${m2}</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:8%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>${m3}</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:11%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>합계</span></p>
	</td>
	</c:forEach>
</tr>
</c:if>
<c:if test="${period_type == 4}">
<tr>
	<td rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:5%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>번호</span></p>
	</td>
	<td rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:25%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>개인정보처리시스템</span></p>
	</td>
	<td colspan="5" valign="middle" bgcolor="#e5e5e5"  style='width:35%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>비정상위험 처리량</span></p>
	</td>
	<td colspan="5" valign="middle" bgcolor="#e5e5e5"  style='width:35%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>비정상위험 이용량</span></p>
	</td>
</tr>
<tr>
	<c:forEach begin="1" end="2" step="1">
	<td valign="middle" bgcolor="#e5e5e5"  style='width:7%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>${q1}</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:7%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>${q2}</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:7%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>${q3}</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:7%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>${q4}</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:7%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>합계</span></p>
	</td>
	</c:forEach>
</tr>
</c:if>
<!-- 시스템 처리량 이용량 -->
<c:forEach var="sys" items="${systems }" varStatus="status">
	<c:choose>
		<c:when test="${status.last}">
	<tr>
		<td colspan="2" valign="middle" bgcolor="#e5e5e5"  style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<span style='font-family:"굴림";font-weight:"bold"'>합계</span>
			</P>
		</td>
		<c:if test="${period_type == 2 || period_type == 4 }">
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sys.cnt0 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sys.cnt1 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sys.cnt2 }" pattern="#,###" /></span>
			</p>
		</td>
		</c:if>
		<c:if test="${period_type == 4 }">
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sys.cnt3 }" pattern="#,###" /></span>
			</p>
		</td>
		</c:if>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sys.totalCnt }" pattern="#,###" /></span>
			</p>
		</td>
		<c:if test="${period_type == 2 || period_type == 4 }">
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sys.logcnt0 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sys.logcnt1 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sys.logcnt2 }" pattern="#,###" /></span>
			</p>
		</td>
		</c:if>
		<c:if test="${period_type == 4 }">
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sys.logcnt3 }" pattern="#,###" /></span>
			</p>
		</td>
		</c:if>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sys.logcnt }" pattern="#,###" /></span>
			</p>
		</td>
	</tr>
		</c:when>
		<c:otherwise>
	<tr>
		<td valign="middle" bgcolor="#e5e5e5"  style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<span style='font-family:"굴림";font-weight:"bold"'>${status.count }</span>
			</P>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<span style='font-family:"굴림"'>${sys.system_name }</span>
			</p>
		</td>
		<c:if test="${period_type == 2 || period_type == 4 }">
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sys.cnt0 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sys.cnt1 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sys.cnt2 }" pattern="#,###" /></span>
			</p>
		</td>
		</c:if>
		<c:if test="${period_type == 4 }">
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sys.cnt3 }" pattern="#,###" /></span>
			</p>
		</td>
		</c:if>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sys.totalCnt }" pattern="#,###" /></span>
			</p>
		</td>
		<c:if test="${period_type == 2 || period_type == 4 }">
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sys.logcnt0 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sys.logcnt1 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sys.logcnt2 }" pattern="#,###" /></span>
			</p>
		</td>
		</c:if>
		<c:if test="${period_type == 4 }">
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sys.logcnt3 }" pattern="#,###" /></span>
			</p>
		</td>
		</c:if>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sys.logcnt }" pattern="#,###" /></span>
			</p>
		</td>
	</tr>
		</c:otherwise>
	</c:choose>
</c:forEach>
</TABLE>
</div>


<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'></P>
<div style='page-break-before:always'></div><br style="page-break-before: always">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:100%;'>
<TR>
	<TD valign="middle" bgcolor="#4e9fd7"  style='width:100%;height:44;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0><SPAN STYLE='width:100%;font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;line-height:160%'>Ⅱ. 비정상위험분석 결과</SPAN><SPAN STYLE='font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'> </SPAN></P>
	</TD>
</TR>
</TABLE>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>1. 
<c:if test="${fn:length(systems_name) == 1 }" >
	${systems_name[0].system_name }
</c:if>
시스템 비정상행위 의심사례 처리건 검토 결과</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;margin-top: 5px;margin-bottom: 5px;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인정보처리 비정상 행위 의심 사례 검토</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:40pt;line-height:180%;margin-top: 5px;margin-bottom: 5px;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>- 비정상행위 의심</SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-3%;line-height:180%'> 처리건수 총 </SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-3%;font-weight:"bold";color:#0000ff;line-height:180%'>
<fmt:formatNumber value="${totalCnt }" pattern="#,###"/></SPAN><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-3%;line-height:180%'>건에 대해 검토함</SPAN></P>

<div align="left" style="margin-left: 10px">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<tr>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:8%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>번호</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:32%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>개인정보처리시스템</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>개인정보과다</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>비정상접근</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>특정인</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>특정시간</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>다운로드</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>합계</span></p>
	</td>
</tr>
<!-- 시스템별 시나리오별 처리량 -->
<c:forEach var="sbs" items="${systemByScenario }" varStatus="status">
	<c:choose>
		<c:when test="${status.last}">
	<tr>
		<td colspan="2" valign="middle" bgcolor="#e5e5e5"  style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<span style='font-family:"굴림";font-weight:"bold"'>합계</span>
			</P>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sbs.scenario1 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sbs.scenario2 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sbs.scenario3 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sbs.scenario4 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sbs.scenario5 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sbs.totalCnt }" pattern="#,###" /></span>
			</p>
		</td>
	</tr>
		</c:when>
		<c:otherwise>
	<tr>
		<td valign="middle" bgcolor="#e5e5e5"  style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<span style='font-family:"굴림";font-weight:"bold"'>${status.count }</span>
			</P>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<span style='font-family:"굴림"'>${sbs.system_name }</span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sbs.scenario1 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sbs.scenario2 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sbs.scenario3 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sbs.scenario4 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sbs.scenario5 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sbs.totalCnt }" pattern="#,###" /></span>
			</p>
		</td>
	</tr>
		</c:otherwise>
	</c:choose>
</c:forEach>
</TABLE>
</div>


<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'></P>
<div style='page-break-before:always'></div><br style="page-break-before: always">   
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>2. 소속별 비정상행위 의심사례 처리건 검토 결과</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;margin-top: 5px;margin-bottom: 5px;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 소속별 비정상 행위 의심 사례 검토</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:40pt;line-height:180%;margin-top: 5px;margin-bottom: 5px;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>- 소속별 결과 TOP5는</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-3%;font-weight:"bold";color:#0000ff;line-height:180%'>${deptNameTop5 }</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";letter-spacing:-3%;line-height:180%'>순으로 집계된다.</SPAN></P>

<div align="left" style="margin-left: 10px">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<tr>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:8%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>번호</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:32%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>소속</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>개인정보과다</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>비정상접근</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>특정인</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>특정시간</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>다운로드</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>합계</span></p>
	</td>
</tr>
<!-- 부서별 시나리오별 처리량 -->
<c:forEach var="dbs" items="${deptByScenario }" varStatus="status">
	<c:choose>
		<c:when test="${status.last}">
	<tr>
		<td colspan="2" valign="middle" bgcolor="#e5e5e5"  style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<span style='font-family:"굴림";font-weight:"bold"'>합계</span>
			</P>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${dbs.scenario1 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${dbs.scenario2 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${dbs.scenario3 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${dbs.scenario4 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${dbs.scenario5 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${dbs.totalCnt }" pattern="#,###" /></span>
			</p>
		</td>
	</tr>
		</c:when>
		<c:otherwise>
	<tr>
		<td valign="middle" bgcolor="#e5e5e5"  style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<span style='font-family:"굴림";font-weight:"bold"'>${status.count }</span>
			</P>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<span style='font-family:"굴림"'>${dbs.dept_name }</span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${dbs.scenario1 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${dbs.scenario2 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${dbs.scenario3 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${dbs.scenario4 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${dbs.scenario5 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${dbs.totalCnt }" pattern="#,###" /></span>
			</p>
		</td>
	</tr>
		</c:otherwise>
	</c:choose>
</c:forEach>
</TABLE>
</div>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'></P>
<div style='page-break-before:always'></div><br style="page-break-before: always">
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>3. 취급자별 비정상행위 의심사례 처리건 검토 결과</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;margin-top: 5px;margin-bottom: 5px;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 개인별 비정상 행위 의심 사례 검토</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:40pt;line-height:180%;margin-top: 5px;margin-bottom: 5px;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>- 개인별 결과 TOP5는</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";font-weight:"bold";color:#0000ff;line-height:180%'>${empUserNameTop5 }</SPAN>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>순으로 집계된다.</SPAN></P>

<div align="left" style="margin-left: 10px">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<tr>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:8%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>번호</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:16%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>소속</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:16%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>취급자</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>개인정보과다</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>비정상접근</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>특정인</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>특정시간</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>다운로드</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>합계</span></p>
	</td>
</tr>
<!-- 개인별 시나리오별 처리량 --> 
<c:forEach var="ubs" items="${userByScenario }" varStatus="status">
	<c:choose>
		<c:when test="${status.last}">
	<tr>
		<td colspan="3" valign="middle" bgcolor="#e5e5e5"  style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<span style='font-family:"굴림";font-weight:"bold"'>합계</span>
			</P>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${ubs.scenario1 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${ubs.scenario2 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${ubs.scenario3 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${ubs.scenario4 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${ubs.scenario5 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${ubs.totalCnt }" pattern="#,###" /></span>
			</p>
		</td>
	</tr>
		</c:when>
		<c:otherwise>
	<tr>
		<td valign="middle" bgcolor="#e5e5e5"  style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<span style='font-family:"굴림";font-weight:"bold"'>${status.count }</span>
			</P>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<span style='font-family:"굴림"'>${ubs.dept_name }</span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<span style='font-family:"굴림"'>${ubs.emp_user_name }</span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${ubs.scenario1 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${ubs.scenario2 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${ubs.scenario3 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${ubs.scenario4 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${ubs.scenario5 }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${ubs.totalCnt }" pattern="#,###" /></span>
			</p>
		</td>
	</tr>
		</c:otherwise>
	</c:choose>
</c:forEach>
</TABLE>
</div>

<%-- <P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'></P>
<div style='page-break-before:always'></div><br style="page-break-before: always">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;width:100%;'>
<TR>
	<TD valign="middle" bgcolor="#4e9fd7"  style='width:100%;height:44;border-left:none;border-right:none;border-top:none;border-bottom:none;padding:1.4pt 1.4pt 1.4pt 1.4pt'>
	<P CLASS=HStyle0><SPAN STYLE='width:100%;font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";color:#ffffff;line-height:160%'>Ⅲ. 종합 결과</SPAN><SPAN STYLE='font-size:19.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'> </SPAN></P>
	</TD>
</TR>
</TABLE>

<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%;'>1. 접속기록 비정상위험 분석 결과</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:20pt;line-height:180%;'>
<SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%;'>개인정보접속기록 주요 비정상위험분석 현황은 다음과 같으며, 향후 개인정보보호 정책을 반영할 경우를 대비하여 개인정보보호 강화활동을 지속적으로 수행할 것</SPAN></P>

<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;margin-bottom:0px;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 주요 시스템별 개인정보 접속기록 비정상위험 TOP5</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:50pt;text-indent:-10pt;line-height:180%;margin-top:0px;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>- 각 시스템별 비정상 위험에 대한 전월 대비 증감 추이 현황을 파악하여, 상위 5개 시스템에 대한 비정상 위험 현황을 알 수 있다.</SPAN></P>
<c:if test="${reportOption[0].input_chart == 'true' }">
<div class='downloadChartClass' id="systemChart" style="width:750px; height:400px;" align="center;"></div>
<img name="systemChart" style="width:750px; height: 300px; float: left; display: none;">
</c:if>

<div align="left" style="margin-left: 10px">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<tr>
	<td rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:8%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>번호</span></p>
	</td>
	<td rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:32%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>개인정보처리시스템</span></p>
	</td>
	<td colspan="3" valign="middle" bgcolor="#e5e5e5"  style='width:30%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>처리량</span></p>
	</td>
	<td colspan="3" valign="middle" bgcolor="#e5e5e5"  style='width:30%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>이용량</span></p>
	</td>
</tr>
<tr>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>${preText}</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>${thisText }</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>증감</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>${preText }</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>${thisText }</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>증감</span></p>
	</td>
</tr>
<!-- 시스템별 처리량 이용량 top5 -->
<c:forEach var="sbs" items="${systemTop5 }" varStatus="status" begin="0" end="4">
	<tr>
		<td valign="middle" bgcolor="#e5e5e5"  style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<span style='font-family:"굴림";font-weight:"bold"'>${status.count }</span>
			</P>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
           <p class="HStyle0" style='text-align:center;'>
               <span style='font-family:"굴림"'>${sbs.system_name }</span>
           </p>
        </td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sbs.compareCnt }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sbs.totalCnt }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<c:choose>
					<c:when test="${sbs.compareCnt < sbs.totalCnt }">
						<span style='font-family:"굴림"; color:#ff0000;'>▲ <fmt:formatNumber value="${sbs.totalCnt- sbs.compareCnt}" pattern="#,###" /></span>
					</c:when>
					<c:when test="${sbs.compareCnt > sbs.totalCnt }">
						<span style='font-family:"굴림"; color:#0000ff;'>▼ <fmt:formatNumber value="${sbs.compareCnt- sbs.totalCnt}" pattern="#,###" /></span>
					</c:when>
					<c:otherwise>-</c:otherwise>
				</c:choose>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sbs.compareLogCnt }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sbs.logcnt }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<c:choose>
					<c:when test="${sbs.compareLogCnt < sbs.logcnt }">
						<span style='font-family:"굴림"; color:#ff0000;'>▲ <fmt:formatNumber value="${sbs.logcnt- sbs.compareLogCnt}" pattern="#,###" /></span>
					</c:when>
					<c:when test="${sbs.compareLogCnt > sbs.logcnt }">
						<span style='font-family:"굴림"; color:#0000ff;'>▼ <fmt:formatNumber value="${sbs.compareLogCnt- sbs.logcnt}" pattern="#,###" /></span>
					</c:when>
					<c:otherwise>-</c:otherwise>
				</c:choose>
			</p>
		</td>
	</tr>
		
</c:forEach>
</TABLE>
</div>




<c:choose>
	<c:when test="${download_type ne 'pdf'}">
	<div style='page-break-before:always'></div><br style="page-break-before: always">
	</c:when>
	<c:when test="${reportOption[0].input_chart == 'true'}">
	<div style='page-break-before:always'></div><br style="page-break-before: always">
	</c:when>
	<c:when test="${reportOption[0].input_chart == 'false'}">
	<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
	</c:when>
</c:choose>

<c:choose>
	<c:when test="${download_type ne 'pdf' }">
<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:130%;margin-bottom:0px;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:130%'>❍ 주요 소속별 개인정보 접속기록 비정상위험 TOP5</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:50pt;text-indent:-10pt;line-height:130%;margin-top:0px;margin-bottom:0px;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:130%'>- 각 소속별 비정상 위험에 대한 전월 대비 증감 추이 현황을 파악하여, 상위 5개 소속에 대한 비정상 위험 현황을 알 수 있다.</SPAN></P>	
	</c:when>
	<c:otherwise>
<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;margin-bottom:0px;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 주요 소속별 개인정보 접속기록 비정상위험 TOP5</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:50pt;text-indent:-10pt;line-height:180%;margin-top:0px;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>- 각 소속별 비정상 위험에 대한 전월 대비 증감 추이 현황을 파악하여, 상위 5개 소속에 대한 비정상 위험 현황을 알 수 있음</SPAN></P>
	</c:otherwise>
</c:choose>

<c:if test="${reportOption[0].input_chart == 'true' }">
<div class='downloadChartClass' id="deptChart" style="width:750px; height:400px;" align="center;"></div>
<img name="deptChart" style="width:750px; height: 300px; float: left; display: none;">
</c:if>

<div align="left" style="margin-left: 10px">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<tr>
	<td rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:8%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>번호</span></p>
	</td>
	<td rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:32%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>소속</span></p>
	</td>
	<td colspan="3" valign="middle" bgcolor="#e5e5e5"  style='width:30%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>처리량</span></p>
	</td>
	<td colspan="3" valign="middle" bgcolor="#e5e5e5"  style='width:30%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>이용량</span></p>
	</td>
</tr>
<tr>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>${preText}</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>${thisText }</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>증감</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>${preText }</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>${thisText }</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>증감</span></p>
	</td>
</tr>
<!-- 부서별 처리량 이용량 top5 -->
<c:forEach var="sbs" items="${deptTop5 }" varStatus="status" begin="0" end="4">
	<tr>
		<td valign="middle" bgcolor="#e5e5e5"  style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<span style='font-family:"굴림";font-weight:"bold"'>${status.count }</span>
			</P>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<span style='font-family:"굴림"'>${sbs.dept_name }</span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sbs.compareCnt }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sbs.totalCnt }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<c:choose>
					<c:when test="${sbs.compareCnt < sbs.totalCnt }">
						<span style='font-family:"굴림"; color:#ff0000;'>▲ <fmt:formatNumber value="${sbs.totalCnt- sbs.compareCnt}" pattern="#,###" /></span>
					</c:when>
					<c:when test="${sbs.compareCnt > sbs.totalCnt }">
						<span style='font-family:"굴림"; color:#0000ff;'>▼ <fmt:formatNumber value="${sbs.compareCnt- sbs.totalCnt}" pattern="#,###" /></span>
					</c:when>
					<c:otherwise>-</c:otherwise>
				</c:choose>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sbs.compareLogCnt }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sbs.logcnt }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<c:choose>
					<c:when test="${sbs.compareLogCnt < sbs.logcnt }">
						<span style='font-family:"굴림"; color:#ff0000;'>▲ <fmt:formatNumber value="${sbs.logcnt- sbs.compareLogCnt}" pattern="#,###" /></span>
					</c:when>
					<c:when test="${sbs.compareLogCnt > sbs.logcnt }">
						<span style='font-family:"굴림"; color:#0000ff;'>▼ <fmt:formatNumber value="${sbs.compareLogCnt- sbs.logcnt}" pattern="#,###" /></span>
					</c:when>
					<c:otherwise>-</c:otherwise>
				</c:choose>
			</p>
		</td>
	</tr>
</c:forEach>
</TABLE>
</div>





<div style='page-break-before:always'></div><br style="page-break-before: always">
<c:choose>
	<c:when test="${download_type ne 'pdf' }">
<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:130%;margin-bottom:0px;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:130%'>❍ 주요 취급자별 개인정보 접속기록 비정상위험 TOP5</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:50pt;text-indent:-10pt;line-height:130%;margin-top:0px;margin-bottom:0px;'><SPAN STYLE='font-size:13.5pt;font-family:"한양신명조,한컴돋움";line-height:130%'>- 각 취급자별 비정상 위험에 대한 전월 대비 증감 추이 현황을 파악하여, 상위 5개 취급자에 대한 비정상 위험 현황을 알 수 있다.</SPAN></P>	
	</c:when>
	<c:otherwise>
<P CLASS=HStyle5 STYLE='margin-left:40.3pt;text-indent:-22.0pt;line-height:180%;margin-bottom:0px;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ 주요 취급자별 개인정보 접속기록 비정상위험 TOP5</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:50pt;text-indent:-10pt;line-height:180%;margin-top:0px'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>- 각 취급자별 비정상 위험에 대한 전월 대비 증감 추이 현황을 파악하여, 상위 5개 취급자에 대한 비정상 위험 현황을 알 수 있다.</SPAN></P>
	</c:otherwise>
</c:choose>
<c:if test="${reportOption[0].input_chart == 'true' }">
<div class='downloadChartClass' id="userChart" style="width:750px; height:400px;" align="center;"></div>
<img name="userChart" style="width:750px; height: 300px; float: left; display: none;">
</c:if>

<div align="left" style="margin-left: 10px">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<tr>
	<td rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:8%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>번호</span></p>
	</td>
	<td rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:16%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>소속</span></p>
	</td>
	<td rowspan="2" valign="middle" bgcolor="#e5e5e5"  style='width:16%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>취급자</span></p>
	</td>
	<td colspan="3" valign="middle" bgcolor="#e5e5e5"  style='width:30%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>처리량</span></p>
	</td>
	<td colspan="3" valign="middle" bgcolor="#e5e5e5"  style='width:30%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>이용량</span></p>
	</td>
</tr>
<tr>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>${preText}</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>${thisText }</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>증감</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>${preText }</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>${thisText }</span></p>
	</td>
	<td valign="middle" bgcolor="#e5e5e5"  style='width:10%;height:40;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
		<p class="HStyle0" style='text-align:center;'><span style='font-family:"굴림";font-weight:"bold"'>증감</span></p>
	</td>
</tr>
<!-- 개인별 처리량 이용량 top5 -->
<c:forEach var="sbs" items="${userTop5 }" varStatus="status" begin="0" end="4">
	<tr>
		<td valign="middle" bgcolor="#e5e5e5"  style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<span style='font-family:"굴림";font-weight:"bold"'>${status.count }</span>
			</P>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<span style='font-family:"굴림"'>${sbs.dept_name }</span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<span style='font-family:"굴림"'>${sbs.emp_user_name }</span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sbs.compareCnt }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sbs.totalCnt }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<c:choose>
					<c:when test="${sbs.compareCnt < sbs.totalCnt }">
						<span style='font-family:"굴림"; color:#ff0000;'>▲ <fmt:formatNumber value="${sbs.totalCnt- sbs.compareCnt}" pattern="#,###" /></span>
					</c:when>
					<c:when test="${sbs.compareCnt > sbs.totalCnt }">
						<span style='font-family:"굴림"; color:#0000ff;'>▼ <fmt:formatNumber value="${sbs.compareCnt- sbs.totalCnt}" pattern="#,###" /></span>
					</c:when>
					<c:otherwise>-</c:otherwise>
				</c:choose>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sbs.compareLogCnt }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:right;'>
				<span style='font-family:"굴림"'><fmt:formatNumber value="${sbs.logcnt }" pattern="#,###" /></span>
			</p>
		</td>
		<td valign="middle" style='height:25;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<p class="HStyle0" style='text-align:center;'>
				<c:choose>
					<c:when test="${sbs.compareLogCnt < sbs.logcnt }">
						<span style='font-family:"굴림"; color:#ff0000;'>▲ <fmt:formatNumber value="${sbs.logcnt- sbs.compareLogCnt}" pattern="#,###" /></span>
					</c:when>
					<c:when test="${sbs.compareLogCnt > sbs.logcnt }">
						<span style='font-family:"굴림"; color:#0000ff;'>▼ <fmt:formatNumber value="${sbs.compareLogCnt- sbs.logcnt}" pattern="#,###" /></span>
					</c:when>
					<c:otherwise>-</c:otherwise>
				</c:choose>
			</p>
		</td>
	</tr>
</c:forEach>
</TABLE>
</div>



<c:choose>
	<c:when test="${download_type ne 'pdf'}">
	<div style='page-break-before:always'></div><br style="page-break-before: always">
	</c:when>
	<c:when test="${reportOption[0].input_chart == 'true'}">
	<div style='page-break-before:always'></div><br style="page-break-before: always">
	</c:when>
	<c:when test="${reportOption[0].input_chart == 'false'}">
	<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
	<P CLASS=HStyle0 STYLE='margin-top:3.0pt;margin-bottom:3.0pt;line-height:180%;'><SPAN STYLE='font-size:18.0pt;font-family:"한양중고딕,한컴돋움";font-weight:"bold";color:#ff0000;line-height:180%'><BR></SPAN></P>
	</c:when>
</c:choose> --%>



<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%;'>2. 개인정보 접속기록 비정상위험 분석을 통한 향후 계획</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:35pt;text-indent:-16pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍  금번 수행한 개인정보 접속기록 비정상위험분석 보고서의 데이터는 향후 월 보고서, 분기 보고서,
 연간 보고서에 반영하여 개인정보 비정상 활용추이를 분석하여 개인정보보호 오남용정책을 수립할 때 반영하도록 한다.</SPAN></P>
<P CLASS=HStyle5 STYLE='margin-left:35pt;text-indent:-16pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍  본 데이터는 개인정보 비정상 접근 및 오남용 의심행위 등을 분석하여 통계분석과 비정상 위험
시나리오 개발에 사용하여 개인정보보호 강화 활동을 지속적으로 수행할 수 있도록 반영하도록 한다.</SPAN></P> 





<c:if test="${reportOption[0].input_description == 'true' }">
<P CLASS=HStyle0 STYLE='margin-top:8.0pt;line-height:180%;'><SPAN STYLE='font-size:17.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:180%'>3. 총평</SPAN></P>
<div align="left" style="margin-left: 10px; margin-top:10px;">
<TABLE class='downloadClass' border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
<TR>
	<TD valign="middle" style='width:650;height:195;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt;'>
	<P CLASS=HStyle0 STYLE='margin-left:16.8pt;text-indent:-16.6pt;line-height:200%;'><SPAN STYLE='font-family:"돋움"'><SPAN STYLE='font-size:14.0pt;font-family:"한양신명조,한컴돋움";line-height:180%'>❍ ${reportOption[0].description}</SPAN></P>
	</TD>
</TR>
</TABLE>
</div>
</c:if>
</div>
</BODY>
</HTML>