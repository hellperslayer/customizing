<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script src="${rootPath}/resources/js/jquery/jquery.min.js" type="text/javascript" charset="UTF-8"></script>

<script	src="${rootPath}/resources/js/highcharts/highcharts.js" type="text/javascript" charset="UTF-8"></script>
<script	src="${rootPath}/resources/js/highcharts/highcharts-more.js" type="text/javascript" charset="UTF-8"></script>

<script type="text/javascript">


	$(document).ready(function() {
		showChart(item);
	});
	
	
	function printpr()
	{
		$("#printButton").hide();
	
		window.print();
		/* var browser = navigator.userAgent.toLowerCase();
        if ( -1 != browser.indexOf('chrome') ){
                   window.print();
        }else if ( -1 != browser.indexOf('trident') ){
                   try{
                            //참고로 IE 5.5 이상에서만 동작함

                            //웹 브라우저 컨트롤 생성
                            var webBrowser = '<OBJECT ID="previewWeb" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';

                            //웹 페이지에 객체 삽입
                            document.body.insertAdjacentHTML('beforeEnd', webBrowser);

                            //ExexWB 메쏘드 실행 (7 : 미리보기 , 8 : 페이지 설정 , 6 : 인쇄하기(대화상자))
                            previewWeb.ExecWB(7, 1);

                            //객체 해제
                            previewWeb.outerHTML = "";
                   }catch (e) {
                            alert("- 도구 > 인터넷 옵션 > 보안 탭 > 신뢰할 수 있는 사이트 선택\n   1. 사이트 버튼 클릭 > 사이트 추가\n   2. 사용자 지정 수준 클릭 > 스크립팅하기 안전하지 않은 것으로 표시된 ActiveX 컨트롤 (사용)으로 체크\n\n※ 위 설정은 프린트 기능을 사용하기 위함임");
                   }
                  
        } */
	    
	    self.close();
	}
	
	function showChart(item) {
		<c:forEach items="${systemList}" var="item">
		
			$('#drawChart3${item.system_seq}').highcharts({
		        chart: {
		            type: 'column'
		        },
		        title: {
		            text: ''
		        },
		        subtitle: {
		            text: ''
		        },
		        xAxis: {
		            categories: [
		                '주민등록번호',
		                '외국인등록번호',
		                '신용카드번호',
		                '여권번호',
		                '운전면허번호',
		                '휴대전화번호',
		                '일반전화번호',
		                '전자우편주소',
		                '건강보험번호',
		                '금융계좌번호',
		                '학번'
		            ]
		        },
		        yAxis: {
		            min: 0,
		            title: {
		                text: '개인정보검출 (건)'
		            },
		            stackLabels: {
		                enabled: true,
		                style: {
		                    fontWeight: 'bold',
		                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
		                }
		            }
		        },
		        legend: {
		            align: 'right',
		            x: -30,
		            verticalAlign: 'top',
		            y: 25,
		            floating: true,
		            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
		            borderColor: '#CCC',
		            borderWidth: 1,
		            shadow: false
		        },
		        tooltip: {
		            headerFormat: '<b>{point.x}</b><br/>',
		            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
		        },
		        plotOptions: {
		            column: {
		                stacking: 'normal'
		            }
		        },credits : { enabled : false },
		        series: [{
		            name: '개인정보 유형',
		            palette : ['#6fc0ff', '#31a0ff', '#0d7ad8', '#2433ff', '#6724ff', '#8440c9', '#b956de', '#f57cf4', '#f57ca4', '#ffa488'],
		            data: [${item.resultType1}, 
		                   ${item.resultType10}, 
		                   ${item.resultType4}, 
		                   ${item.resultType3}, 
		                   ${item.resultType2}, 
		                   ${item.resultType8}, 
		                   ${item.resultType6}, 
		                   ${item.resultType7}, 
		                   ${item.resultType5}, 
		                   ${item.resultType9}, 
		                   ${item.resultType99}]
		        }]
		    });
		</c:forEach>
	}

</script>

<STYLE type="text/css">
<!--
p.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle0
	{style-name:"바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle1
	{style-name:"본문"; margin-left:15.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle2
	{style-name:"개요 1"; margin-left:10.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle3
	{style-name:"개요 2"; margin-left:20.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle4
	{style-name:"개요 3"; margin-left:30.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle5
	{style-name:"개요 4"; margin-left:40.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle6
	{style-name:"개요 5"; margin-left:50.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle7
	{style-name:"개요 6"; margin-left:60.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle8
	{style-name:"개요 7"; margin-left:70.0pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle9
	{style-name:"쪽 번호"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle10
	{style-name:"머리말"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:150%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle11
	{style-name:"각주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle12
	{style-name:"미주"; margin-left:13.1pt; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:-13.1pt; line-height:130%; font-size:9.0pt; font-family:함초롬바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle13
	{style-name:"메모"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:130%; font-size:9.0pt; font-family:함초롬돋움; letter-spacing:-5%; font-weight:"normal"; font-style:"normal"; color:#000000;}
p.HStyle14
	{style-name:"MS바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
li.HStyle14
	{style-name:"MS바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
div.HStyle14
	{style-name:"MS바탕글"; margin-top:0.0pt; margin-bottom:0.0pt; text-align:justify; text-indent:0.0pt; line-height:160%; font-size:10.0pt; font-family:바탕; letter-spacing:0; font-weight:"normal"; font-style:"normal"; color:#000000;}
-->
</STYLE>
	<div align="right">
		<input type=button id="printButton" name="printButton"onclick="printpr();" value="출력"/>
	</div>

	<div align="center">
		<P CLASS=HStyle0 STYLE='text-align:left;line-height:120%;'><SPAN STYLE='font-size:22.0pt;font-family:"HY헤드라인M";color:#ff0000;line-height:120%'><BR></SPAN></P>
		
		<P CLASS=HStyle0 STYLE='text-align:center;line-height:120%;'><SPAN STYLE='font-size:22.0pt;font-family:"HY헤드라인M";color:#ff0000;line-height:120%'><BR></SPAN></P>
		
		<P CLASS=HStyle0 STYLE='text-align:center;line-height:120%;'><SPAN STYLE='font-size:22.0pt;font-family:"HY헤드라인M";color:#ff0000;line-height:120%'><BR></SPAN></P>
		
		<P CLASS=HStyle0 STYLE='text-align:center;line-height:120%;'><SPAN STYLE='font-size:22.0pt;font-family:"HY헤드라인M";color:#ff0000;line-height:120%'><BR></SPAN></P>
		
		<P CLASS=HStyle0 STYLE='text-align:center;line-height:120%;'><SPAN STYLE='font-size:22.0pt;font-family:"HY헤드라인M";color:#ff0000;line-height:120%'><BR></SPAN></P>
		
		<P CLASS=HStyle0 STYLE='text-align:center;line-height:120%;'></P>
		<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
		<TR>
			<TD valign="middle" style='width:635;height:157;border-left:double #000000 1.4pt;border-right:double #000000 1.4pt;border-top:double #000000 1.4pt;border-bottom:double #000000 1.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:26.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>개인정보 접속기록 보고</SPAN></P>
			</TD>
		</TR>
		</TABLE>
		<P CLASS=HStyle0 STYLE='text-align:center;line-height:120%;'></P>
		
		<P CLASS=HStyle0 STYLE='text-align:center;line-height:120%;'><SPAN STYLE='font-family:"HY울릉도M";color:#ff0000'><BR></SPAN></P>
		
		<P CLASS=HStyle0 STYLE='text-align:center;line-height:120%;'><SPAN STYLE='font-family:"HY울릉도M";color:#ff0000'><BR></SPAN></P>
		
		<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-family:"바탕";color:#ff0000'><BR></SPAN></P>
		
		<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-family:"바탕";color:#ff0000'><BR></SPAN></P>
		
		<P CLASS=HStyle14 STYLE='text-align:center;'></P>
		<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
		<TR>
			<TD valign="middle" style='width:123;height:42;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:0.0pt 5.4pt 0.0pt 5.4pt'>
			<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>작업내용</SPAN></P>
			</TD>
			<TD valign="middle" style='width:444;height:42;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:0.0pt 5.4pt 0.0pt 5.4pt'>
			<P CLASS=HStyle0 STYLE='margin-left:132.6pt;margin-top:2.0pt;margin-bottom:2.0pt;text-align:center;text-indent:-132.6pt;line-height:170%;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:170%'>개인정보 접속기록 보고</SPAN></P>
			</TD>
		</TR>
		<TR>
			<TD valign="middle" style='width:123;height:42;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:0.0pt 5.4pt 0.0pt 5.4pt'>
			<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-size:14.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>문서코드</SPAN></P>
			</TD>
			<TD valign="middle" style='width:444;height:42;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:0.0pt 5.4pt 0.0pt 5.4pt'>
			<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕"'>CSR-${date.substring(2,4)}${date.substring(5,7)}${date.substring(8,10)}</SPAN></P>
			</TD>
		</TR>
		</TABLE>
		<P CLASS=HStyle14 STYLE='text-align:center;'></P>
		
		<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-family:"바탕"'><BR></SPAN></P>
		
		<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-family:"바탕"'><BR></SPAN></P>
		
		<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-family:"바탕"'><BR></SPAN></P>
		
		<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-family:"바탕"'><BR></SPAN></P>
		
		<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-family:"바탕"'><BR></SPAN></P>
		
		<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-family:"바탕"'><BR></SPAN></P>
		
		<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-family:"바탕"'><BR></SPAN></P>
		
		<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-size:20.0pt;font-family:"HY견고딕";color:#ff0000;line-height:160%'> </SPAN><SPAN STYLE='font-size:20.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>${date.substring(0,4)}.${date.substring(5,7)}</SPAN></P>
		
		<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-family:"바탕"'><BR></SPAN></P>
		
		<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-family:"바탕"'><BR></SPAN></P>
		
		<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-family:"바탕"'><BR></SPAN></P>
		
		<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-family:"바탕"'><BR></SPAN></P>
		
		<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-family:"바탕"'><BR></SPAN></P>
		
		<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-family:"바탕"'><BR></SPAN></P>
		
		<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-family:"바탕"'><BR></SPAN></P>
		
		<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-family:"바탕"'><IMG src="${pageContext.servletContext.contextPath}/resources/image/report/easycerti.png" alt="" width="204" height="35" vspace="0" hspace="0" border="0"></SPAN></P>
		
		<P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-family:"바탕"'><BR></SPAN></P>
		
		<P CLASS=HStyle0><SPAN STYLE='font-family:"한컴바탕"'><BR></SPAN></P>
	</div>
	
	<div style='page-break-before:always'></div>
	
	<div align="center">
		<P CLASS=HStyle0></P>
	
		<P CLASS=HStyle0><BR></P>
		
		<P CLASS=HStyle0></P>
		<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
		<TR>
			<TD valign="middle" style='width:639;height:98;border-left:none;border-right:none;border-top:solid #000000 1.4pt;border-bottom:solid #000000 1.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='line-height:180%;'><SPAN STYLE='font-size:15.0pt;font-family:"한양중고딕,한컴돋움";letter-spacing:-4%;line-height:180%'>주요 개인정보처리시스템의 개인정보 접속기록을 점검하고 위험도 산정기준을 분류하여 </SPAN><SPAN STYLE='font-size:15.0pt;font-family:"한양중고딕,한컴돋움";letter-spacing:-3%;line-height:180%'>그 결과를 보고드림</SPAN></P>
			</TD>
		</TR>
		</TABLE>
		<P CLASS=HStyle0></P>
		
		<P CLASS=HStyle0><SPAN STYLE='font-size:12.0pt;line-height:160%'><BR></SPAN></P>
		
		<P CLASS=HStyle0><SPAN STYLE='font-size:4.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:160%'><BR></SPAN></P>
		
		<P CLASS=HStyle0><SPAN STYLE='font-size:18.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:160%'>1. 점검 개요</SPAN></P>
		
		<P CLASS=HStyle0><SPAN STYLE='font-family:"휴먼명조";font-weight:"bold"'><BR></SPAN></P>
		
		<P CLASS=HStyle0 STYLE='margin-left:26.3pt;text-indent:-26.3pt;'><SPAN STYLE='font-size:16.0pt;font-family:"휴먼명조";font-weight:"bold";line-height:160%'>□ 점검 목적</SPAN></P>
		
		<P CLASS=HStyle0 STYLE='margin-left:26.3pt;text-indent:-26.3pt;'><SPAN STYLE='font-size:4.0pt;font-family:"휴먼명조";font-weight:"bold";line-height:160%'><BR></SPAN></P>
		
		<P CLASS=HStyle0 STYLE='margin-left:38.1pt;text-indent:-38.1pt;'><SPAN STYLE='font-size:16.0pt;font-family:"휴먼명조";line-height:160%'>&nbsp;&nbsp;&nbsp;- 개인정보처리시스템에 접속한 기록을 상시 점검</SPAN><SPAN STYLE='font-size:16.0pt;font-family:"휴먼명조";letter-spacing:-1%;line-height:160%'>하여 불법적인 접근을 통제하며, 시스템 관리자 및 개인정보</SPAN><SPAN STYLE='font-size:16.0pt;font-family:"휴먼명조";line-height:160%'>취급자들이 개인정보를 오남용하는 사례가 발생하지 않도록 사전에 방지하여 관리·감독의 수준을 향상</SPAN></P>
		
		<P CLASS=HStyle0 STYLE='margin-left:38.1pt;text-indent:-38.1pt;'><SPAN STYLE='font-size:4.0pt;font-family:"휴먼명조";font-weight:"bold";line-height:160%'><BR></SPAN></P>
		
		<P CLASS=HStyle0 STYLE='margin-left:38.1pt;text-indent:-38.1pt;'><SPAN STYLE='font-size:4.0pt;font-family:"휴먼명조";font-weight:"bold";line-height:160%'><BR></SPAN></P>
		
		<P CLASS=HStyle0 STYLE='margin-left:38.1pt;text-indent:-38.1pt;'><SPAN STYLE='font-size:4.0pt;font-family:"휴먼명조";font-weight:"bold";line-height:160%'><BR></SPAN></P>
		
		<P CLASS=HStyle0 STYLE='margin-left:26.3pt;text-indent:-26.3pt;'><SPAN STYLE='font-size:16.0pt;font-family:"휴먼명조";font-weight:"bold";line-height:160%'>□ 관련 근거</SPAN></P>
		
		<P CLASS=HStyle0 STYLE='margin-left:26.3pt;text-indent:-26.3pt;'><SPAN STYLE='font-size:4.0pt;font-family:"휴먼명조";font-weight:"bold";line-height:160%'><BR></SPAN></P>
		
		<P CLASS=HStyle0 STYLE='margin-left:38.1pt;text-indent:-38.1pt;'><SPAN STYLE='font-size:16.0pt;font-family:"휴먼명조";line-height:160%'>&nbsp;&nbsp;&nbsp;- </SPAN><SPAN STYLE='font-size:16.0pt;font-family:"휴먼명조";letter-spacing:-4%;line-height:160%'>개인정보보호법 제29조 (안전조치의무)</SPAN></P>
		
		<P CLASS=HStyle0 STYLE='margin-left:38.1pt;text-indent:-38.1pt;'><SPAN STYLE='font-size:16.0pt;font-family:"휴먼명조";line-height:160%'>&nbsp;&nbsp;&nbsp;- </SPAN><SPAN STYLE='font-size:16.0pt;font-family:"휴먼명조";letter-spacing:-4%;line-height:160%'>개인정보보호법 시행령 제30조 (개인정보의 안전성 확보 조치)</SPAN></P>
		
		<P CLASS=HStyle0 STYLE='margin-left:38.1pt;text-indent:-38.1pt;'><SPAN STYLE='font-size:16.0pt;font-family:"휴먼명조";letter-spacing:-4%;line-height:160%'>&nbsp;&nbsp;&nbsp;- </SPAN><SPAN STYLE='font-size:16.0pt;font-family:"휴먼명조";letter-spacing:-11%;line-height:160%'>개인정보의 안전성 확보조치 기준 제8조 (접속기록의 보관 및 점검)</SPAN></P>
		
		<P CLASS=HStyle0 STYLE='margin-left:38.1pt;text-indent:-38.1pt;'><SPAN STYLE='font-size:4.0pt;font-family:"휴먼명조";font-weight:"bold";line-height:160%'><BR></SPAN></P>
		
		<P CLASS=HStyle0 STYLE='margin-left:38.1pt;text-indent:-38.1pt;'><SPAN STYLE='font-size:4.0pt;font-family:"휴먼명조";font-weight:"bold";line-height:160%'><BR></SPAN></P>
		
		<P CLASS=HStyle0 STYLE='margin-left:38.1pt;text-indent:-38.1pt;'><SPAN STYLE='font-size:4.0pt;font-family:"휴먼명조";font-weight:"bold";line-height:160%'><BR></SPAN></P>
		
		<P CLASS=HStyle0 STYLE='margin-left:26.3pt;text-indent:-26.3pt;'><SPAN STYLE='font-size:16.0pt;font-family:"휴먼명조";font-weight:"bold";line-height:160%'>□ 점검 대상</SPAN></P>
		
		<P CLASS=HStyle0 STYLE='margin-left:26.3pt;text-indent:-26.3pt;'><SPAN STYLE='font-size:4.0pt;font-family:"휴먼명조";font-weight:"bold";line-height:160%'><BR></SPAN></P>
		
		<P CLASS=HStyle0 STYLE='margin-left:26.3pt;text-align:center;text-indent:-26.3pt;'></P>
		<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
		<TR>
			<TD valign="middle" bgcolor="#999999"  style='width:59;height:32;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>번호</SPAN></P>
			</TD>
			<TD valign="middle" bgcolor="#999999"  style='width:206;height:32;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>개인정보처리시스템</SPAN></P>
			</TD>
			<TD valign="middle" bgcolor="#999999"  style='width:165;height:32;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
			<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-family:"맑은 고딕";font-weight:"bold"'>URL</SPAN></P>
			</TD>
		</TR>
		
		
		<c:forEach items="${systemList}" var="list" varStatus="status">
			<TR>
				<TD valign="middle" style='width:59;height:35;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>${status.count}</SPAN></P>
				</TD>
				<TD valign="middle" style='width:206;height:35;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>${list.system_name}</SPAN></P>
				</TD>
				<TD valign="middle" style='width:165;height:35;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>
					<c:if test="${list.main_url == null || list.main_url == '' }">
						-
					</c:if>
					<c:if test="${list.main_url != null && list.main_url != '' }">
						${list.main_url}
					</c:if>
				</SPAN></P>
				</TD>
			</TR>
		</c:forEach>
		</TABLE>
		<P CLASS=HStyle0 STYLE='margin-left:26.3pt;text-align:center;text-indent:-26.3pt;'></P>
		
		<P CLASS=HStyle0><SPAN STYLE='font-family:"한컴바탕"'><BR></SPAN></P>
	</div>

	<div style='page-break-before:always'></div>

	
	<P CLASS=HStyle0><SPAN STYLE='font-size:16.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:160%'><BR></SPAN></P>
	<P CLASS=HStyle0><SPAN STYLE='font-size:18.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:160%'>2. 점검 현황</SPAN></P>
	
	<c:forEach items="${systemList}" var="list" varStatus="status">
		<div align="center">
			
			<P CLASS=HStyle0><SPAN STYLE='font-family:"HY헤드라인M";font-weight:"bold"'><BR></SPAN></P>
			
			<P CLASS=HStyle0 STYLE='margin-left:26.3pt;text-indent:-26.3pt;'><SPAN STYLE='font-size:16.0pt;font-family:"휴먼명조";font-weight:"bold";line-height:160%'>&nbsp;2.${status.count} ${list.system_name}
			
			
			<c:if test="${list.main_url != null && list.main_url != '' }">
					(${list.main_url})
			</c:if>
					
			</SPAN></P>
			
			<P CLASS=HStyle0 STYLE='margin-left:26.3pt;text-indent:-26.3pt;'><SPAN STYLE='font-size:4.0pt;font-family:"휴먼명조";font-weight:"bold";line-height:160%'><BR></SPAN></P>
			
			<P CLASS=HStyle0 STYLE='margin-left:26.3pt;text-indent:-26.3pt;'><SPAN STYLE='font-size:16.0pt;font-family:"휴먼명조";line-height:160%'>□ 기본 현황</SPAN></P>
			
			<P CLASS=HStyle0 STYLE='margin-left:26.3pt;text-indent:-26.3pt;'><SPAN STYLE='font-size:4.0pt;font-family:"휴먼명조";font-weight:"bold";line-height:160%'><BR></SPAN></P>
			
			<P CLASS=HStyle0 STYLE='margin-left:26.3pt;text-align:center;text-indent:-26.3pt;'></P>
			<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
			<TR>
				<TD valign="middle" bgcolor="#999999"  style='width:243;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>구분</SPAN></P>
				</TD>
				<TD valign="middle" bgcolor="#999999"  style='width:332;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>상세내역</SPAN></P>
				</TD>
			</TR>
			<TR>
				<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>대상시스템 명</SPAN></P>
				</TD>
				<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>${list.system_name}</SPAN></P>
				</TD>
			</TR>
			<TR>
				<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>대상시스템 URL</SPAN></P>
				</TD>
				<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>
				<c:if test="${list.main_url == null || list.main_url == '' }">
						-
					</c:if>
					<c:if test="${list.main_url != null && list.main_url != '' }">
						${list.main_url}
					</c:if>
				</SPAN></P>
				</TD>
			</TR>
			<TR>
				<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>기간</SPAN></P>
				</TD>
				<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>${start_date} ~ ${end_date}</SPAN></P>
				</TD>
			</TR>
			<TR>
				<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>수집 / 분석 솔루션</SPAN></P>
				</TD>
				<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>UBI SAFER-PSM</SPAN></P>
				</TD>
			</TR>
			<TR>
				<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>개인정보 검출현황</SPAN></P>
				</TD>
				<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>${list.tot_count} 건</SPAN></P>
				</TD>
			</TR>
			</TABLE>
			
			
			<P CLASS=HStyle0 STYLE='margin-left:26.3pt;text-align:center;text-indent:-26.3pt;'><SPAN STYLE='font-size:16.0pt;font-family:"휴먼명조";font-weight:"bold";line-height:160%'>&nbsp;&nbsp;</SPAN></P>
			
			<P CLASS=HStyle0 STYLE='margin-left:26.3pt;text-indent:-26.3pt;'><SPAN STYLE='font-size:4.0pt;font-family:"휴먼명조";font-weight:"bold";line-height:160%'><BR></SPAN></P>
			
			<P CLASS=HStyle0 STYLE='margin-left:26.3pt;text-indent:-26.3pt;'><SPAN STYLE='font-size:16.0pt;font-family:"휴먼명조";line-height:160%'>□ ${list.system_name} 개인정보 처리 현황</SPAN></P>
			
			<P CLASS=HStyle0 STYLE='margin-left:26.3pt;text-indent:-26.3pt;'><SPAN STYLE='font-size:4.0pt;font-family:"휴먼명조";line-height:160%'><BR></SPAN></P>
			
			<div id="drawChart3${list.system_seq}" class="chart_area">
			
			</div>
				
			<P CLASS=HStyle0 STYLE='margin-left:26.3pt;text-indent:-26.3pt;'><SPAN STYLE='font-size:16.0pt;font-family:"휴먼명조";line-height:160%'><BR></SPAN></P>
			
			<P CLASS=HStyle0><SPAN STYLE='font-family:"한컴바탕"'><BR></SPAN></P>
		</div>
		<div style='page-break-before:always'></div>
	</c:forEach>
	
	
	<div align="center">
		<P CLASS=HStyle0><SPAN STYLE='font-size:18.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:160%'><BR></SPAN></P>

		<P CLASS=HStyle0><SPAN STYLE='font-size:18.0pt;font-family:"HY헤드라인M";font-weight:"bold";line-height:160%'>3. 점검 결과</SPAN></P>
		
		<P CLASS=HStyle0 STYLE='margin-left:26.3pt;text-indent:-26.3pt;'><SPAN STYLE='font-size:6.0pt;font-family:"휴먼명조";font-weight:"bold";line-height:160%'><BR></SPAN></P>
		

		
		
		<P CLASS=HStyle0 STYLE='margin-left:26.3pt;text-indent:-26.3pt;'><SPAN STYLE='font-size:16.0pt;font-family:"휴먼명조";line-height:160%'>□ 전체 개인정보 접속자 Top10</SPAN></P>
		
		<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
			<TR>
				<TD valign="middle" bgcolor="#999999"  style='width:243;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>부서명</SPAN></P>
				</TD>
				<TD valign="middle" bgcolor="#999999"  style='width:332;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>아이디</SPAN></P>
				</TD>
				<TD valign="middle" bgcolor="#999999"  style='width:332;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>이름</SPAN></P>
				</TD>
				<TD valign="middle" bgcolor="#999999"  style='width:332;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
				<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>처리건수</SPAN></P>
				</TD>
				
			</TR>
			
			<c:if test="${systemCnt.top1 != null && systemCnt.top1 != '' }">
				<c:set var="totTop1" value="${fn:split(systemCnt.top1,',')}"></c:set>
					
					<TR>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${totTop1[0] != null && totTop1[0] != '' && totTop1[0] != 'null'}">
								${totTop1[0]}
							</c:if>
							<c:if test="${totTop1[0] == null || totTop1[0] == '' || totTop1[0] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>
							<c:if test="${totTop1[1] != null && totTop1[1] != '' && totTop1[1] != 'null'}">
								${totTop1[1]}
							</c:if>
							<c:if test="${totTop1[1] == null || totTop1[1] == '' || totTop1[1] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>
							<c:if test="${totTop1[2] != null && totTop1[2] != '' && totTop1[2] != 'null'}">
								${totTop1[2]}
							</c:if>
							<c:if test="${totTop1[2] == null || totTop1[2] == '' || totTop1[2] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>${totTop1[3]} 건</SPAN></P>
						</TD>
					</TR>
			</c:if>
			
			<c:if test="${systemCnt.top2 != null && systemCnt.top2 != '' }">
				<c:set var="totTop2" value="${fn:split(systemCnt.top2,',')}"></c:set>
					
					<TR>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
						<c:if test="${totTop2[0] != null && totTop2[0] != '' && totTop2[0] != 'null'}">
								${totTop2[0]}
							</c:if>
							<c:if test="${totTop2[0] == null || totTop2[0] == '' || totTop2[0] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>
						<c:if test="${totTop2[1] != null && totTop2[1] != '' && totTop2[1] != 'null'}">
								${totTop2[1]}
							</c:if>
							<c:if test="${totTop2[1] == null || totTop2[1] == '' || totTop2[1] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>
						<c:if test="${totTop2[2] != null && totTop2[2] != '' && totTop2[2] != 'null'}">
								${totTop2[2]}
							</c:if>
							<c:if test="${totTop2[2] == null || totTop2[2] == '' || totTop2[2] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>${totTop2[3]} 건</SPAN></P>
						</TD>
					</TR>
			</c:if>
			
			<c:if test="${systemCnt.top3 != null && systemCnt.top3 != '' }">
				<c:set var="totTop3" value="${fn:split(systemCnt.top3,',')}"></c:set>
					
					<TR>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
						<c:if test="${totTop3[0] != null && totTop3[0] != '' && totTop3[0] != 'null'}">
								${totTop3[0]}
							</c:if>
							<c:if test="${totTop3[0] == null || totTop3[0] == '' || totTop3[0] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>
						<c:if test="${totTop3[1] != null && totTop3[1] != '' && totTop3[1] != 'null'}">
								${totTop3[1]}
							</c:if>
							<c:if test="${totTop3[1] == null || totTop3[1] == '' || totTop3[1] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>
						<c:if test="${totTop3[2] != null && totTop3[2] != '' && totTop3[2] != 'null'}">
								${totTop3[2]}
							</c:if>
							<c:if test="${totTop3[2] == null || totTop3[2] == '' || totTop3[2] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>${totTop3[3]} 건</SPAN></P>
						</TD>
					</TR>
			</c:if>
			
			<c:if test="${systemCnt.top4 != null && systemCnt.top4 != '' }">
				<c:set var="totTop4" value="${fn:split(systemCnt.top4,',')}"></c:set>
					
					<TR>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
						<c:if test="${totTop4[0] != null && totTop4[0] != '' && totTop4[0] != 'null'}">
								${totTop4[0]}
							</c:if>
							<c:if test="${totTop4[0] == null || totTop4[0] == '' || totTop4[0] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>
						<c:if test="${totTop4[1] != null && totTop4[1] != '' && totTop4[1] != 'null'}">
								${totTop4[1]}
							</c:if>
							<c:if test="${totTop4[1] == null || totTop4[1] == '' || totTop4[1] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>
						<c:if test="${totTop4[2] != null && totTop4[2] != '' && totTop4[2] != 'null'}">
								${totTop4[2]}
							</c:if>
							<c:if test="${totTop4[2] == null || totTop4[2] == '' || totTop4[2] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>${totTop4[3]} 건</SPAN></P>
						</TD>
					</TR>
			</c:if>
			
			<c:if test="${systemCnt.top5 != null && systemCnt.top5 != '' }">
				<c:set var="totTop5" value="${fn:split(systemCnt.top5,',')}"></c:set>
					
					<TR>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
						<c:if test="${totTop5[0] != null && totTop5[0] != '' && totTop5[0] != 'null'}">
								${totTop5[0]}
							</c:if>
							<c:if test="${totTop5[0] == null || totTop5[0] == '' || totTop5[0] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>
						<c:if test="${totTop5[1] != null && totTop5[1] != '' && totTop5[1] != 'null'}">
								${totTop5[1]}
							</c:if>
							<c:if test="${totTop5[1] == null || totTop5[1] == '' || totTop5[1] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>
						<c:if test="${totTop5[2] != null && totTop5[2] != '' && totTop5[2] != 'null'}">
								${totTop5[2]}
							</c:if>
							<c:if test="${totTop5[2] == null || totTop5[2] == '' || totTop5[2] == 'null'}">
								-
							</c:if></SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>${totTop5[3]} 건</SPAN></P>
						</TD>
					</TR>
			</c:if>
			
			<c:if test="${systemCnt.top6 != null && systemCnt.top6 != '' }">
				<c:set var="totTop6" value="${fn:split(systemCnt.top6,',')}"></c:set>
					
					<TR>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
						<c:if test="${totTop6[0] != null && totTop6[0] != '' && totTop6[0] != 'null'}">
								${totTop6[0]}
							</c:if>
							<c:if test="${totTop6[0] == null || totTop6[0] == '' || totTop6[0] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>
						<c:if test="${totTop6[1] != null && totTop6[1] != '' && totTop6[1] != 'null'}">
								${totTop6[1]}
							</c:if>
							<c:if test="${totTop6[1] == null || totTop6[1] == '' || totTop6[1] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>
						<c:if test="${totTop6[2] != null && totTop6[2] != '' && totTop6[2] != 'null'}">
								${totTop6[2]}
							</c:if>
							<c:if test="${totTop6[2] == null || totTop6[2] == '' || totTop6[2] == 'null'}">
								-
							</c:if></SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>${totTop6[3]} 건</SPAN></P>
						</TD>
					</TR>
			</c:if>
			
			<c:if test="${systemCnt.top7 != null && systemCnt.top7 != '' }">
				<c:set var="totTop7" value="${fn:split(systemCnt.top7,',')}"></c:set>
					
					<TR>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
						<c:if test="${totTop7[0] != null && totTop7[0] != '' && totTop7[0] != 'null'}">
								${totTop7[0]}
							</c:if>
							<c:if test="${totTop7[0] == null || totTop7[0] == '' || totTop7[0] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>
						<c:if test="${totTop7[1] != null && totTop7[1] != '' && totTop7[1] != 'null'}">
								${totTop7[1]}
							</c:if>
							<c:if test="${totTop7[1] == null || totTop7[1] == '' || totTop7[1] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>
						<c:if test="${totTop7[2] != null && totTop7[2] != '' && totTop7[2] != 'null'}">
								${totTop7[2]}
							</c:if>
							<c:if test="${totTop7[2] == null || totTop7[2] == '' || totTop7[2] == 'null'}">
								-
							</c:if></SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>${totTop7[3]} 건</SPAN></P>
						</TD>
					</TR>
			</c:if>
			
			<c:if test="${systemCnt.top8 != null && systemCnt.top8 != '' }">
				<c:set var="totTop8" value="${fn:split(systemCnt.top8,',')}"></c:set>
					
					<TR>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
						<c:if test="${totTop8[0] != null && totTop8[0] != '' && totTop8[0] != 'null'}">
								${totTop8[0]}
							</c:if>
							<c:if test="${totTop8[0] == null || totTop8[0] == '' || totTop8[0] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>
						<c:if test="${totTop8[1] != null && totTop8[1] != '' && totTop8[1] != 'null'}">
								${totTop8[1]}
							</c:if>
							<c:if test="${totTop8[1] == null || totTop8[1] == '' || totTop8[1] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>
						<c:if test="${totTop8[2] != null && totTop8[2] != '' && totTop8[2] != 'null'}">
								${totTop8[2]}
							</c:if>
							<c:if test="${totTop8[2] == null || totTop8[2] == '' || totTop8[2] == 'null'}">
								-
							</c:if></SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>${totTop8[3]} 건</SPAN></P>
						</TD>
					</TR>
			</c:if>
			
			<c:if test="${systemCnt.top9 != null && systemCnt.top9 != '' }">
				<c:set var="totTop9" value="${fn:split(systemCnt.top9,',')}"></c:set>
					
					<TR>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
						<c:if test="${totTop9[0] != null && totTop9[0] != '' && totTop9[0] != 'null'}">
								${totTop9[0]}
							</c:if>
							<c:if test="${totTop9[0] == null || totTop9[0] == '' || totTop9[0] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>
						<c:if test="${totTop9[1] != null && totTop9[1] != '' && totTop9[1] != 'null'}">
								${totTop9[1]}
							</c:if>
							<c:if test="${totTop9[1] == null || totTop9[1] == '' || totTop9[1] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>
						<c:if test="${totTop9[2] != null && totTop9[2] != '' && totTop9[2] != 'null'}">
								${totTop9[2]}
							</c:if>
							<c:if test="${totTop9[2] == null || totTop9[2] == '' || totTop9[2] == 'null'}">
								-
							</c:if></SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>${totTop9[3]} 건</SPAN></P>
						</TD>
					</TR>
			</c:if>
			
			<c:if test="${systemCnt.top10 != null && systemCnt.top10 != '' }">
				<c:set var="totTop10" value="${fn:split(systemCnt.top10,',')}"></c:set>
					
					<TR>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
						<c:if test="${totTop10[0] != null && totTop10[0] != '' && totTop10[0] != 'null'}">
								${totTop10[0]}
							</c:if>
							<c:if test="${totTop10[0] == null || totTop10[0] == '' || totTop10[0] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>
						<c:if test="${totTop10[1] != null && totTop10[1] != '' && totTop10[1] != 'null'}">
								${totTop10[1]}
							</c:if>
							<c:if test="${totTop10[1] == null || totTop10[1] == '' || totTop10[1] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>
						<c:if test="${totTop10[2] != null && totTop10[2] != '' && totTop10[2] != 'null'}">
								${totTop10[2]}
							</c:if>
							<c:if test="${totTop10[2] == null || totTop10[2] == '' || totTop10[2] == 'null'}">
								-
							</c:if></SPAN></P>
						</TD>
						<TD valign="middle" style='width:332;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";line-height:160%'>${totTop10[3]} 건</SPAN></P>
						</TD>
					</TR>
			</c:if>
			
		</TABLE>
		<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
		<tr>　　</tr>
		</TABLE>
		<c:forEach items="${systemList}" var="list" varStatus="status">
		
			<P CLASS=HStyle0 STYLE='margin-left:26.3pt;text-indent:-26.3pt;'><SPAN STYLE='font-size:16.0pt;font-family:"휴먼명조";line-height:160%'>□ ${list.system_name} 개인정보 접속자 Top10</SPAN></P>
			
			<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
				<TR>
					<TD valign="middle" bgcolor="#999999"  style='width:243;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
					<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>부서명</SPAN></P>
					</TD>
					<TD valign="middle" bgcolor="#999999"  style='width:332;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
					<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>아이디</SPAN></P>
					</TD>
					<TD valign="middle" bgcolor="#999999"  style='width:332;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
					<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>이름</SPAN></P>
					</TD>
					<TD valign="middle" bgcolor="#999999"  style='width:332;height:36;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
					<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>처리건수</SPAN></P>
					</TD>
				</TR>
				
				<c:set var="top1" value="${fn:split(list.top1,',')}"></c:set>
				
				<c:if test="${list.top1 != null && list.top1 != '' }">
					<TR>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top1[0] != null && top1[0] != '' && top1[0] != 'null'}">
								${top1[0]}
							</c:if>
							<c:if test="${top1[0] == null || top1[0] == '' || top1[0] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top1[1] != null && top1[1] != '' && top1[1] != 'null'}">
								${top1[1]}
							</c:if>
							<c:if test="${top1[1] == null || top1[1] == '' || top1[1] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top1[2] != null && top1[2] != '' && top1[2] != 'null'}">
								${top1[2]}
							</c:if>
							<c:if test="${top1[2] == null || top1[2] == '' || top1[2] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							${top1[3]} 건
						</SPAN></P>
						</TD>
					</TR>
				</c:if>
				
				<c:set var="top2" value="${fn:split(list.top2,',')}"></c:set>
				<c:if test="${list.top2 != null && list.top2 != '' }">
					<TR>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top2[0] != null && top2[0] != '' && top2[0] != 'null'}">
								${top2[0]}
							</c:if>
							<c:if test="${top2[0] == null || top2[0] == '' || top2[0] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top2[1] != null && top2[1] != '' && top2[1] != 'null'}">
								${top2[1]}
							</c:if>
							<c:if test="${top2[1] == null || top2[1] == '' || top2[1] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top2[2] != null && top2[2] != '' && top2[2] != 'null'}">
								${top2[2]}
							</c:if>
							<c:if test="${top2[2] == null || top2[2] == '' || top2[2] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							${top2[3]} 건
						</SPAN></P>
						</TD>
					</TR>
				</c:if>
				
				<c:set var="top3" value="${fn:split(list.top3,',')}"></c:set>
				
				<c:if test="${list.top3 != null && list.top3 != '' }">
					<TR>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top3[0] != null && top3[0] != '' && top3[0] != 'null'}">
								${top3[0]}
							</c:if>
							<c:if test="${top3[0] == null || top3[0] == '' || top3[0] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top3[1] != null && top3[1] != '' && top3[1] != 'null'}">
								${top3[1]}
							</c:if>
							<c:if test="${top3[1] == null || top3[1] == '' || top3[1] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top3[2] != null && top3[2] != '' && top3[2] != 'null'}">
								${top3[2]}
							</c:if>
							<c:if test="${top3[2] == null || top3[2] == '' || top3[2] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							${top3[3]} 건
						</SPAN></P>
						</TD>
					</TR>
				</c:if>
				
				<c:set var="top4" value="${fn:split(list.top4,',')}"></c:set>
				
				<c:if test="${list.top4 != null && list.top4 != '' }">
					<TR>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top4[0] != null && top4[0] != '' && top4[0] != 'null'}">
								${top4[0]}
							</c:if>
							<c:if test="${top4[0] == null || top4[0] == '' || top4[0] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top4[1] != null && top4[1] != '' && top4[1] != 'null'}">
								${top4[1]}
							</c:if>
							<c:if test="${top4[1] == null || top4[1] == '' || top4[1] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top4[2] != null && top4[2] != '' && top4[2] != 'null'}">
								${top4[2]}
							</c:if>
							<c:if test="${top4[2] == null || top4[2] == '' || top4[2] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							${top4[3]} 건
						</SPAN></P>
						</TD>
					</TR>
				</c:if>
				
				<c:set var="top5" value="${fn:split(list.top5,',')}"></c:set>
				
				<c:if test="${list.top5 != null && list.top5 != '' }">
					<TR>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top5[0] != null && top5[0] != '' && top5[0] != 'null'}">
								${top5[0]}
							</c:if>
							<c:if test="${top5[0] == null || top5[0] == '' || top5[0] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top5[1] != null && top5[1] != '' && top5[1] != 'null'}">
								${top5[1]}
							</c:if>
							<c:if test="${top5[1] == null || top5[1] == '' || top5[1] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top5[2] != null && top5[2] != '' && top5[2] != 'null'}">
								${top5[2]}
							</c:if>
							<c:if test="${top5[2] == null || top5[2] == '' || top5[2] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							${top5[3]} 건
						</SPAN></P>
						</TD>
					</TR>
				</c:if>
				
				<c:set var="top6" value="${fn:split(list.top6,',')}"></c:set>
				
				<c:if test="${list.top6 != null && list.top6 != '' }">
					<TR>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top6[0] != null && top6[0] != '' && top6[0] != 'null'}">
								${top6[0]}
							</c:if>
							<c:if test="${top6[0] == null || top6[0] == '' || top6[0] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top6[1] != null && top6[1] != '' && top6[1] != 'null'}">
								${top6[1]}
							</c:if>
							<c:if test="${top6[1] == null || top6[1] == '' || top6[1] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top6[2] != null && top6[2] != '' && top6[2] != 'null'}">
								${top6[2]}
							</c:if>
							<c:if test="${top6[2] == null || top6[2] == '' || top6[2] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							${top6[3]} 건
						</SPAN></P>
						</TD>
					</TR>
				</c:if>
				
				<c:set var="top7" value="${fn:split(list.top7,',')}"></c:set>
				
				<c:if test="${list.top7 != null && list.top7 != '' }">
					<TR>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top7[0] != null && top7[0] != '' && top7[0] != 'null'}">
								${top7[0]}
							</c:if>
							<c:if test="${top7[0] == null || top7[0] == '' || top7[0] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top7[1] != null && top7[1] != '' && top7[1] != 'null'}">
								${top7[1]}
							</c:if>
							<c:if test="${top7[1] == null || top7[1] == '' || top7[1] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top7[2] != null && top7[2] != '' && top7[2] != 'null'}">
								${top7[2]}
							</c:if>
							<c:if test="${top7[2] == null || top7[2] == '' || top7[2] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							${top7[3]} 건
						</SPAN></P>
						</TD>
					</TR>
				</c:if>
				
				<c:set var="top8" value="${fn:split(list.top8,',')}"></c:set>
				
				<c:if test="${list.top8 != null && list.top8 != '' }">
					<TR>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top8[0] != null && top8[0] != '' && top8[0] != 'null'}">
								${top8[0]}
							</c:if>
							<c:if test="${top8[0] == null || top8[0] == '' || top8[0] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top8[1] != null && top8[1] != '' && top8[1] != 'null'}">
								${top8[1]}
							</c:if>
							<c:if test="${top8[1] == null || top8[1] == '' || top8[1] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top8[2] != null && top8[2] != '' && top8[2] != 'null'}">
								${top8[2]}
							</c:if>
							<c:if test="${top8[2] == null || top8[2] == '' || top8[2] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							${top8[3]} 건
						</SPAN></P>
						</TD>
					</TR>
				</c:if>
				
				 <c:set var="top9" value="${fn:split(list.top9,',')}"></c:set>
				
				<c:if test="${list.top9 != null && list.top9 != '' }">
					<TR>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top9[0] != null && top9[0] != '' && top9[0] != 'null'}">
								${top9[0]}
							</c:if>
							<c:if test="${top9[0] == null || top9[0] == '' || top9[0] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top9[1] != null && top9[1] != '' && top9[1] != 'null'}">
								${top9[1]}
							</c:if>
							<c:if test="${top9[1] == null || top9[1] == '' || top9[1] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top9[2] != null && top9[2] != '' && top9[2] != 'null'}">
								${top9[2]}
							</c:if>
							<c:if test="${top9[2] == null || top9[2] == '' || top9[2] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							${top9[3]} 건
						</SPAN></P>
						</TD>
					</TR>
				</c:if>
				
				<c:set var="top10" value="${fn:split(list.top10,',')}"></c:set>
				
				<c:if test="${list.top10 != null && list.top10 != '' }">
					<TR>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top10[0] != null && top10[0] != '' && top10[0] != 'null'}">
								${top10[0]}
							</c:if>
							<c:if test="${top10[0] == null || top10[0] == '' || top10[0] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top10[1] != null && top10[1] != '' && top10[1] != 'null'}">
								${top10[1]}
							</c:if>
							<c:if test="${top10[1] == null || top10[1] == '' || top10[1] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							<c:if test="${top10[2] != null && top10[2] != '' && top10[2] != 'null'}">
								${top10[2]}
							</c:if>
							<c:if test="${top10[2] == null || top10[2] == '' || top10[2] == 'null'}">
								-
							</c:if>
						</SPAN></P>
						</TD>
						<TD valign="middle" style='width:243;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'>
						<P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:9.0pt;font-family:"맑은 고딕";font-weight:"bold";line-height:160%'>
							${top10[3]} 건
						</SPAN></P>
						</TD>
					</TR>
				</c:if>
			</TABLE>
		<TABLE border="1" cellspacing="0" cellpadding="0" style='border-collapse:collapse;border:none;'>
		<tr>　　</tr>
		</TABLE>
		</c:forEach>
		
		
		
		
		
		
		
		
		
		
		
		
	</div>
	<c:set var="systemList" value="${systemList}"></c:set>




