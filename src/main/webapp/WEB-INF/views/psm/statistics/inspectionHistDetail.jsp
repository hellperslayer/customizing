<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>

<c:set var="menuMappSetupAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/statistics/inspection_hist.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>

<h1 class="page-title">${currentMenuName}</h1>
<!-- 그룹코드 신규 -->
<div class="portlet light portlet-fit bordered">
	<div class="portlet-body">
		<!-- BEGIN FORM-->
		<form id="listForm" action="" method="POST" class="form-horizontal form-bordered form-row-stripped">
			<c:if test="${!empty inspection}">
				<input type="hidden" name="seq" value="${inspection.seq }" />
			</c:if>
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<th style="width: 15%; text-align: center;"><label class="control-label">점검월</label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<c:choose>
								<c:when test="${empty inspection.proc_month }">
									<select id="year" name="year" class="form-control input-xsmall" style="text-align: right; float: left;">
										<c:forEach var="i" begin="${fn:substring(now,0,4)-10 }"
											end="${fn:substring(now,0,4) }" step="1">
											<option value="${i }"
												<c:if test="${i eq fn:substring(now,0,4) }">selected</c:if>>${i }년</option>
										</c:forEach>
									</select>
									<select id="month" name="month" class="form-control input-xsmall" style="text-align: right;">
										<c:forEach var="i" begin="1" end="12" step="1">
											<option value="${i }"
												<c:if test="${i eq fn:substring(now,5,7) }">selected</c:if>>${i }월</option>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
									<c:out value="${fn:substring(inspection.proc_month,0,4) }년 ${fn:substring(inspection.proc_month,4,6) }월" />
								</c:otherwise>
							</c:choose>
						</td>
						<th style="width: 15%; text-align: center;"><label class="control-label">점검날짜 </label></th>
						<td style="width: 35%; vertical-align: middle;" class="form-group form-md-line-input">
							<c:choose>
								<c:when test="${empty inspection}">
									<input type="text" name="proc_date" class="form-control" value="${now }" readonly="readonly"/>
								</c:when>
								<c:otherwise>
									<fmt:parseDate value="${inspection.proc_date}" pattern="yyyyMMdd" var="proc_date" /> 
									<fmt:formatDate value="${proc_date}" pattern="yyyy-MM-dd" />
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
					<tr>
						<th style="text-align: center;"><label class="control-label">담당자</label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<input type="text" name="manager" class="form-control" value="${inspection.manager }" />
							<div class="form-control-focus"></div>
						</td>
						<th style="text-align: center;"><label class="control-label">점검자</label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<input type="text" name="inspector" class="form-control" value="${inspection.inspector }" />
							<div class="form-control-focus"></div>
						</td>
					</tr>
					<tr>
						<th style="text-align: center; vertical-align: middle;"><label class="control-label">직전 정기점검</label></th>
						<c:choose>
							<c:when test="${empty inspection}">
								<td><TEXTAREA rows="8" style="width: 100%;" readonly="readonly">${recent_content}</TEXTAREA></td>
							</c:when>
							<c:otherwise>
								<td><TEXTAREA rows="8" style="width: 100%;" readonly="readonly">${prev_content}</TEXTAREA></td>
							</c:otherwise>
						</c:choose>
						<th style="text-align: center; vertical-align: middle;"><label class="control-label">변경사항 및 특이사항</label></th>
						<td><TEXTAREA name="contents" rows="8" style="width: 100%;">${inspection.contents}</TEXTAREA></td>
					</tr>
					<tr>
						<th style="text-align: center; vertical-align: middle;"><label class="control-label">다음 점검일</label></th>
						<td colspan="3" style="vertical-align: middle;">
							<c:choose>
							<c:when test="${empty inspection}">
								<div class="mt-repeater-input">
									<div class="input-group input-medium date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
										<input type="text" class="form-control input-small" id="next_date" name="next_date"
											value="<procdate:convertDate value="${paramBean.search_to}" />">
									</div>
								</div>
							</c:when>
							<c:otherwise>
								<div class="mt-repeater-input">
								<div class="input-group input-medium date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
									<input type="text" class="form-control input-small" id="next_date" name="next_date"
										value="<procdate:convertDate value="${inspection.next_date}" />">
								</div>
							</div>
							</c:otherwise>
							</c:choose>
						</td>
					</tr>
				</tbody>
			</table>


			<!-- 메뉴 관련 input 시작 -->
			<input type="hidden" name="main_menu_id"
				value="${paramBean.main_menu_id }" /> <input type="hidden"
				name="sub_menu_id" value="${paramBean.sub_menu_id }" /> <input
				type="hidden" name="current_menu_id" value="${currentMenuId }" />
			<!-- 메뉴 관련 input 끝 -->

			<!-- 페이지 번호 -->
<%-- 			<input type="hidden" name="page_num" value="${search.page_num }" /> --%>

			<!-- 검색조건 관련 input 시작 -->
			<%-- <input type="hidden" name="system_seq_search"
				value="${search.system_seq_search}" /> <input type="hidden"
				name="url_search" value="${search.url_search }" /> --%>
			<!-- 검색조건 관련 input 끝 -->
		</form>
		<!-- END FORM-->

		<!-- 버튼 영역 -->
		<div class="form-actions">
			<div class="row">
				<div class="col-md-offset-2 col-md-10" align="right"
					style="padding-right: 30px;">
					<button type="button" class="btn btn-sm dark btn-outline sbold uppercase"
						onclick="goInspectionHistList()">
						<i class="fa fa-list"></i> &nbsp;목록
					</button>
					<c:choose>
						<c:when test="${empty inspection}">
							<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="addInspectionHist()"><i
								class="fa fa-check"></i>&nbsp;추가</a>
						</c:when>
						<c:otherwise>
							<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="saveInspectionHist()"><i
								class="fa fa-check"></i>&nbsp;저장</a>
							<!-- <a class="btn btn-sm red-mint btn-outline sbold uppercase" onclick="removemenuMappSetup()"><i
								class="fa fa-remove"></i>&nbsp;삭제</a> -->
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

var inspectionHistConfig = {
		"listUrl" : "${rootPath}/statistics/inspection_hist.html",
		"detailUrl" : "${rootPath}/statistics/inspection_hist_detail.html",
		"downloadUrl" : "${rootPath}/statistics/download.html",
		"addUrl" : "${rootPath}/statistics/addInspectionHist.html",
		"saveUrl" : "${rootPath}/statistics/saveInspectionHist.html",
		"loginPage" : "${rootPath}/loginView.html"
	};
	
</script>