<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>

<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/statistics/logResultList.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>

<style>
#loading {
 width: 100%;   
 height: 100%;   
 top: 0px;
 left: 0px;
 position: fixed;   
 display: block;   
 opacity: 0.7;   
 background-color: #fff;   
 z-index: 99;   
 text-align: center; }  
  
#loading-image {   
 position: absolute;   
 top: 50%;   
 left: 50%;  
 z-index: 100; } 
</style>

<script type="text/javascript">
$(document).ready(function() {
	$('#loading').show();
});

$(window).load(function() {   
	$('#loading').hide();
});
</script>

<div id="loading"><img id="loading-image" width="30px;" src="${rootPath}/resources/image/loading3.gif" alt="Loading..." /></div>


<h1 class="page-title">
	${currentMenuName}
</h1>
<div class="portlet light portlet-fit portlet-datatable bordered">
	<div class="portlet-body">
		<div class="table-container">

			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
				<div>
					<div class="col-md-6" style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
						<div class="portlet box grey-salt  ">
                            <div class="portlet-title" style="background-color: #2B3643;">
                                <div class="caption">
                                    <i class="fa fa-search"></i>검색 & 엑셀 </div>
	                            <div class="tools">
					                 <a href="javascript:;" class="collapse"> </a>
					            </div>
                            </div>
                            <div class="portlet-body form">
                                <div class="form-body" style="padding-left:10px; padding-right:10px;">
                                    <div class="form-group">
                                        <form id="listForm" method="POST" class="mt-repeater form-horizontal">
                                            <div data-repeater-list="group-a">
                                                <div class="row">
                                                    <div class="col-md-2">
                                            		<label class="control-label">기간</label> <br />
													<div class="input-group input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
														<input type="text" class="form-control" readonly="readonly"
															value="<procdate:convertDate value="${search.search_from}" />">
														<span class="input-group-addon"> &sim; </span>
														<input type="text" class="form-control" readonly="readonly"
															value="<procdate:convertDate value="${search.search_to}" />">
													</div>
                                            	</div>
													<div class="col-md-2">
													<input type="checkbox" id="searchTime" name="searchTime" onclick="searhTime(this)" <c:if test="${search.searchTime eq true }">checked</c:if> /> 
                                                        <label class="control-label">시간</label> 
                                                        <div class="input-group input-daterange">
                                                        	<select class="form-control" id="start_h" name="start_h" onchange="setProcTime('start')" <c:if test="${search.searchTime ne true }">disabled</c:if>>
	                                                        	<c:forEach var="item" varStatus="i" begin="0" end="23" step="1">
	                                                        		<option value="${item}"
	                                                        		<c:if test="${item == (search.start_h) }">selected="selected"</c:if>>${item }시</option>
	                                                        	</c:forEach>
                                                        	</select>
                                                       		<span class="input-group-addon"> &sim; </span> 
	                                                        <select class="form-control" id="end_h" name="end_h" onchange="setProcTime('end')" <c:if test="${search.searchTime ne true }">disabled</c:if>>
	                                                        	<c:forEach var="item" varStatus="i" begin="1" end="24" step="1">
	                                                        		<option value="${item}" 
	                                                        		<c:if test="${item == (search.end_h) }">selected="selected"</c:if>>${item }시</option>
	                                                        	</c:forEach>
	                                                        </select>
                                                        </div>
                                            	</div>
													<div class="col-md-2">
                                                        <label class="control-label">시스템</label>
                                                        <br/>
                                                        <select name="system_seq" class="form-control" <c:if test="${shType== 'system' }">disabled="disabled"</c:if>>>
															<option value=""
																${search.system_seq == '' ? 'selected="selected"' : ''}>
																----- 전 체 -----</option>
															<c:if test="${empty systemMasterList}">
																<option>시스템 없음</option>
															</c:if>
															<c:if test="${!empty systemMasterList}">
																<c:forEach items="${systemMasterList}" var="i"
																	varStatus="z">
																	<option value="${i.system_seq}"
																		${i.system_seq==search.system_seq ? "selected=selected" : "" }>${ i.system_name}</option>
																</c:forEach>
															</c:if>
														</select>
													</div>
													<div class="col-md-2" >
													<label class="control-label">소속</label>
                                                    <input type="text" class="form-control" name="dept_name" value="${search.dept_name}" <c:if test="${shType== 'dept' }">readonly="readonly"</c:if>/>
												</div>
												<div class="col-md-2">
													<label class="control-label">사용자ID</label>
                                                    <input type  ="text" class="form-control" name="emp_user_id" value="${search.emp_user_id}" <c:if test="${shType== 'user' }">readonly="readonly"</c:if> />
												</div>
												<div class="col-md-2">
													<label class="control-label">사용자명</label>
                                                    <input type="text" class="form-control" name="emp_user_name" value="${search.emp_user_name}" 
                                                    <c:if test="${shType=='user'}">value="${allLogInq[0].emp_user_name }" readonly="readonly"</c:if>/>
												</div>
												</div>
												<div class="row">
													
                                                    <c:if test="${search.login_auth_id ne 'AUTH20000' }">
                                                    <div class="col-md-2">
                                                        <label class="control-label">사용자IP</label>
                                                        <br/>
                                                        <input type="text" class="form-control" name="user_ip" value="${search.user_ip}" />
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="control-label">접근 경로</label>
                                                        <br/>
                                                        <input type="text" class="form-control" name="req_url" value="${search.req_url}" />
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="control-label">파일명</label>
                                                        <br/>
                                                        <input type="text" class="form-control" name="file_name" value="${search.file_name}" />
                                                    </div>
                                                    </c:if>
                                                </div>
                                            </div>
                                            <hr/>
                                            <div align="right" style="padding-top:5px;">
	                                            <button type="button"
													class="btn btn-sm red-mint btn-outline sbold uppercase"
													onclick="resultReset()">
													<i class="fa fa-remove"></i> 초기화
												</button>
												<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
													onclick="resultSearch()">
													<i class="fa fa-search"></i> 검색
												</button>&nbsp;&nbsp;
												<div class="btn-group">
													<a data-toggle="dropdown"> <img src="${rootPath}/resources/image/icon/XLS_3.png">
													</a>
													<ul class="dropdown-menu pull-right">
														<li><a onclick="downResultExcel()"> EXCEL </a></li>
														<li><a onclick="downResultCsv()"> CSV </a></li>
													</ul>
												</div>
											</div>
											<input type="hidden" name="privacyType" value="${search.privacyType }" />
											<input type="hidden" class="form-control" id="search_fr" name="search_from" value="${search.search_fromWithHyphen}"> 
											<input type="hidden" class="form-control" id="search_to" name="search_to" value="${search.search_toWithHyphen}">
											<input type="hidden" name="shType" value="${shType }" />
												
											<input type="hidden" name="detailLogSeq" value="" /> 
											<input type="hidden" name="detailProcDate" value="" /> 
											<input type="hidden" name="main_menu_id" value="${search.main_menu_id }" /> 
											<input type="hidden" name="sub_menu_id" value="${search.sub_menu_id }" /> 
											<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
											<%-- <input type="hidden" name="system_seq" value="${search.system_seq }" /> --%> 
											<input type="hidden" name="page_num" value="${search.page_num}" /> 
											<input type="hidden" name="menuNum" id="menuNum" value="${menuNum}" />
											<input type="hidden" name="menuCh" id="menuCh" value="${menuCh}" />
											<input type="hidden" name="menuId" id="menuId" value="${search.sub_menu_id}" /> 
											<input type="hidden" name="menutitle" id="menutitle" value="${menuCh}${menuNum}" />
											<input type="hidden" name="dept_id" value="" /> 
											<input type="hidden" name="user_id" value="" />
											<input type="hidden" name="user_name" value="" />
											<input type="hidden" name="isSearch" value="${search.isSearch }" />
											<input type="hidden" name = "detailProcTime"  id = "detailProcTime" value=""/>
											<input type="hidden" name = "result_type"  id = "result_type" value=""/>
											<input type="hidden" name = "sort_flag"  id = "sort_flag" value="${search.sort_flag}"/>
											<input type="hidden" name = "detailOccrDt"  id = "detailOccrDt" value=""/>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>
				</div>
				<div>
					<table style="border-top: 1px solid #e7ecf1"
						class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
						style="position: absolute; top: 0px; left: 0px; width: 100%;">

						<thead>
						<c:choose>
						<c:when test="${scrn_name_view == 1 }">
							<tr role="row" class="heading" style="background-color: #c0bebe;">
								<th width="10%"style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">일시</th>
								<th width="8%" style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">소속</th>
								<th width="8%" style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">사용자ID</th>
								<th width="8%" style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center; padding: 0em;">사용자명</th>
								<th width="8%" style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">사용자IP</th>
								<th width="10%"style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">시스템명</th>
								<th width="6%"style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">파일명</th>
								<c:if test="${use_fullscan eq 'Y' }">
								<th width="10%"style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">${privacyTypeName}</th>
								</c:if>
								<th width="10%"style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center; padding: 0em;">메뉴명</th>	
								<th width="8%"tyle="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">사유</th>
							</tr>
						</c:when>
						<c:when test="${scrn_name_view == 2 }">
							<tr role="row" class="heading" style="background-color: #c0bebe;">
								<th width="10%"style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">일시</th>
								<th width="8%"style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">소속</th>
								<th width="8%"style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">사용자ID</th>
								<th width="8%"style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center; padding: 0em;">사용자명</th>
								<th width="8%"style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">사용자IP</th>
								<th width="10%"style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">시스템명</th>
								<th width="8%"style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">파일명</th>
								<c:if test="${use_fullscan eq 'Y' }">
								<th width="12%"style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">${privacyTypeName}</th>
								</c:if>
								<th width="12%"style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">사유</th>
							</tr>
						</c:when>
						<c:otherwise>
							<tr role="row" class="heading" style="background-color: #c0bebe;">
								<th width="10%"style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">일시</th>
								<th width="10%"style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">소속</th>
								<th width="10%"style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">사용자ID</th>
								<th width="10%"style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center; padding: 0em;">사용자명</th>
								<th width="10%"style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">사용자IP</th>
								<th width="10%"style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">시스템명</th>
								<th width="10%"style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">파일명</th>
								<c:if test="${use_fullscan eq 'Y' }">
								<th width="16%"style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">${privacyTypeName}</th>
								</c:if>								
								<th width="14%"style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">사유</th>
							</tr>
						</c:otherwise>
						</c:choose>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${empty allLogInq}">
									<tr>
										<td colspan="10" align="center">데이터가 없습니다.</td>
									</tr>
								</c:when>
								<c:otherwise>
									<c:forEach items="${allLogInq}" var="downloadLogInq" varStatus="status1">
										<tr>
											<td style="text-align: center;">
												<fmt:parseDate value="${downloadLogInq.proc_date}" pattern="yyyyMMdd" var="proc_date" /> 
												<fmt:formatDate value="${proc_date}" pattern="yy-MM-dd" /> 
												<fmt:parseDate value="${downloadLogInq.proc_time}" pattern="HHmmss" var="proc_time" /> 
												<fmt:formatDate value="${proc_time}" pattern="HH:mm:ss" />
											</td>
											<c:if test="${downloadLogInq.proc_date eq null or downloadLogInq.proc_time eq null}">
												<td style="text-align: center;">-</td>
											</c:if>
											<td style="text-align: center;"><ctl:nullCv nullCheck="${downloadLogInq.dept_name}" /></td>
											<td style="text-align: center;"><ctl:nullCv nullCheck="${downloadLogInq.emp_user_id}" /></td>
											<td style="text-align: center;"><ctl:nullCv nullCheck="${downloadLogInq.emp_user_name}" /></td>
											<td style="text-align: center;"><ctl:nullCv nullCheck="${downloadLogInq.user_ip}" /></td>
											<td style="text-align: center;"><ctl:nullCv nullCheck="${downloadLogInq.system_name}" /></td>
											<c:choose>
												<c:when test="${fn:length(downloadLogInq.file_name) > 15}">
													<td style="text-align: left" title="${downloadLogInq.file_name}"><c:out value="${fn:substring(downloadLogInq.file_name, 0, 15)}..."/></td>
												</c:when>
												<c:otherwise>
													<td style="text-align: left"><ctl:nullCv nullCheck="${downloadLogInq.file_name}" /></td>
												</c:otherwise>
											</c:choose>
											
											<c:if test="${use_fullscan eq 'Y' }">
												<td style="text-align: center;"><ctl:nullCv nullCheck="${downloadLogInq.result_content}" /></td>
											</c:if>
											
											<c:choose>
											<c:when test="${scrn_name_view == 1 }">
												<c:choose>
													<c:when test="${downloadLogInq.scrn_name eq null || downloadLogInq.scrn_name eq '' || downloadLogInq.scrn_name eq 'null' }">
														<td style="text-align: center;">-</td>
													</c:when>
													<c:otherwise>
														<td style="text-align: left;">${downloadLogInq.scrn_name}</td>
													</c:otherwise>
												</c:choose>
											</c:when>
											<c:when test="${scrn_name_view == 2 }">
											<c:if test="${downloadLogInq.scrn_name eq null || downloadLogInq.scrn_name eq '' || downloadLogInq.scrn_name eq 'null'}">
											</c:if>
											<c:if 
												test="${downloadLogInq.scrn_name ne null && downloadLogInq.scrn_name ne '' && downloadLogInq.scrn_name ne 'null'}">
												<td >${downloadLogInq.scrn_name}</td>
											</c:if>
											</c:when>
										</c:choose>
											<c:choose>
												<c:when test="${fn:length(downloadLogInq.reason) > 20}">
													<td style="text-align: left" title="${downloadLogInq.reason}"><c:out value="${fn:substring(downloadLogInq.reason, 0, 20)}..."/></td>
												</c:when>
												<c:otherwise>
													<td style="text-align: left"><ctl:nullCv nullCheck="${downloadLogInq.reason}" /></td>
												</c:otherwise>
											</c:choose>
										</tr>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
				</div>

				<form id="menuSearchForm" method="POST">
					<input type="hidden" name="main_menu_id" value="${search.main_menu_id }" /> 
					<input type="hidden" name="sub_menu_id" value="${search.sub_menu_id }" /> 
					<input type="hidden" name="system_seq" value="${search.system_seq }" />
					<input type="hidden" name="emp_user_id" value="${search.emp_user_id}" /> 
					<input type="hidden" name="search_from" value="${search.search_from}" /> 
					<input type="hidden" name="search_to" value="${search.search_to}" /> 
					<input type="hidden" name="user_ip" value="${search.user_ip}" /> 
					<input type="hidden" name="daySelect" value="${search.daySelect}" />
					<input type="hidden" name="emp_user_name" value="${search.emp_user_name }" />

				</form>

				<div class="row" style="padding: 10px;">
					<!-- 페이징 영역 -->
					<c:if test="${search.total_count > 0}">
						<div id="pagingframe" align="center">
							<p>
								<ctl:paginator currentPage="${search.page_num}" rowBlockCount="${search.size}" totalRowCount="${search.total_count}" />
							</p>
						</div>
					</c:if>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var allLogInqConfig = {
		"listUrl" : "${rootPath}/statistics/downloadLogResultList.html",
		"downloadUrl" : "${rootPath}/statistics/downloadLogResultListExcel.html",
		"downloadCSVUrl" : "${rootPath}/statistics/downloadLogResultListCsv.html"
	};
</script>