<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>

<script type="text/javascript" charset="UTF-8">
	var use_fullscan = '${use_fullscan}';
	var searchType = '${searchType}';
</script>

<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/common/ezDatepicker.js"
	type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/statistics/statistics.js"
	type="text/javascript" charset="UTF-8"></script>
<style>
.mjpoint {
	padding: 0px;
	margin: 0px;
	vertical-align: middle;
	text-align: center;
}

td {
	padding: 0px;
	margin: 0px;
	vertical-align: middle;
	text-align: center;
}

.mjpointZero {
	padding: 0px;
	margin: 0px;
	vertical-align: middle;
	text-align: center;
}

th {
	padding: 0px;
	margin: 0px;
	vertical-align: middle;
	text-align: center;
}
</style>
<h1 class="page-title">${currentMenuName}</h1>
<script
	src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js"
	type="text/javascript"></script>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light portlet-fit portlet-datatable bordered">
			<div class="portlet-body">
				<div class="table-container">
					<div id="datatable_ajax_2_wrapper"
						class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
						<div class="">
							<div class="col-md-12" style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
								<div class="portlet box grey-salt  ">
									<div class="portlet-title" style="background-color: #2B3643;">
										<div class="caption">
											<i class="fa fa-search"></i>검색 & 엑셀
										</div>
										<div class="tools">
											<a id="searchBar_icon" href="" class="collapse"> </a>
										</div>
									</div>
									<div id="searchBar" class="portlet-body form">
										<div class="form-body" style="padding-left: 20px; padding-right: 10px;">
											<div class="form-group">
												<form id="listForm" method="POST" class="mt-repeater form-horizontal">
													<div data-repeater-list="group-a">
														<div data-repeater-item class="mt-repeater-item">
															<!-- jQuery Repeater Container -->
															<div class="mt-repeater-input">
																<label class="control-label">기간</label> <br />
																<div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
																	<input type="text" class="form-control" id="search_fr" name="search_from"
																		value="<procdate:convertDate value="${paramBean.search_from}" />">
																	<span class="input-group-addon"> &sim; </span>
																	<input type="text" class="form-control" id="search_to" name="search_to"
																		value="<procdate:convertDate value="${paramBean.search_to}" />">
																</div>
															</div>
															<div class="mt-repeater-input">
																<label class="control-label">사용자명</label> <br/>
																<input class="form-control" type="text" name="emp_user_name" value="${paramBean.emp_user_name}" />
															</div>
															<div class="mt-repeater-input">
																<label class="control-label">사용자ID</label> <br/>
																<input class="form-control" type="text" name="emp_user_id" value="${paramBean.emp_user_id}" />
															</div>
															<div class="mt-repeater-input">
																<label class="control-label">부서명</label> <br/>
																<input class="form-control" type="text" name="dept_name" value="${paramBean.dept_name}" />
															</div>
															<div class="mt-repeater-input">
																<label class="control-label">부서ID</label> <br/>
																<input class="form-control" type="text" name="dept_id" value="${paramBean.dept_id}" />
															</div>
														</div>
													</div>
													<div align="right">
														<button type="reset"
															class="btn btn-sm red-mint btn-outline sbold uppercase"
															onclick="resetOptions(statisticsListConfig['listUrl'])">
															<i class="fa fa-remove"></i> 초기화
														</button>
														<button type="button"
															class="btn btn-sm blue btn-outline sbold uppercase"
															onclick="moveStatistics()">
															<i class="fa fa-search"></i> 검색
														</button>
														&nbsp;&nbsp; <a onclick="indvmoveExcel()"><img
															src="${rootPath}/resources/image/icon/XLS_3.png"></a>
													</div>

													<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" />
													<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
													<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
													<input type="hidden" name="page_num" />
													<input type="hidden" id="check_type" name="check_type" value="${paramBean.check_type}" />
													<input type="hidden" id="sort_flag" name="sort_flag" value="${paramBean.sort_flag}" />
													<input type="hidden" id="rootPath" name="rootPath" value="${rootPath}" />
													<input type="hidden" id="check_dp" name="check_dp" value="${paramBean.chk_date_type}" />
													<input type="hidden" name="isSearch" value="" />
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div style="color: red; text-align: right;">※ 위험도는 비정상위험 시나리오 별 위험지수의  합계입니다.</div>
						<div class="">
							<table style="border-top: 1px solid #e7ecf1;"
								class="table table-striped table-bordered table-checkable dataTable no-footer"
								role="grid">
								<colgroup>
									<col width="20%" />
									<col width="20%" />
									<col width="20%" />
									<col width="20%" />
									<col width="20%" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col" style="padding: 0px; margin: 0px; vertical-align: middle; text-align: center;">사용자명</th>
										<th scope="col" style="padding: 0px; margin: 0px; vertical-align: middle; text-align: center;">사용자ID</th>
										<th scope="col" style="padding: 0px; margin: 0px; vertical-align: middle; text-align: center;">부서명</th>
										<th scope="col" style="padding: 0px; margin: 0px; vertical-align: middle; text-align: center;">부서ID</th>
										<th scope="col" style="padding: 10px; margin: 10px; vertical-align: middle; text-align: center;">위험도</th>
									</tr>
								</thead>
								<tbody>
									<c:choose>
										<c:when test="${empty statisticsList.statistics}">
											<tr>
												<td colspan="5" align="center">데이터가 없습니다.</td>
											</tr>
										</c:when>
										<c:otherwise>
											<c:forEach items="${statisticsList.statistics}" var="statistics">
												<tr>
													<td style="vertical-align: middle; text-align: center;">${statistics.emp_user_name}</td>
													<td style="vertical-align: middle; text-align: center;">${statistics.emp_user_id}</td>
													<td style="vertical-align: middle; text-align: center;">${statistics.dept_name}</td>
													<td style="vertical-align: middle; text-align: center;">${statistics.dept_id}</td>
													<td style="vertical-align: middle; text-align: center;">${statistics.dng_val}</td>
												</tr>
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
						</div>

						<div class="row" style="padding: 10px;">
							<!-- 페이징 영역 -->
							<c:if test="${paramBean.pageInfo.total_count > 0}">
								<div id="pagingframe" align="center">
									<p>
										<ctl:paginator currentPage="${paramBean.pageInfo.page_num}"
											rowBlockCount="${paramBean.pageInfo.size}"
											totalRowCount="${paramBean.pageInfo.total_count}" />
									</p>
								</div>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">

	var statisticsListConfig = {
		"listUrl":"${rootPath}/statistics/list_indvbyDngValAnals.html",
		"downloadUrl":"${rootPath}/statistics/indvDngValdownload.html"
	};
	
</script>