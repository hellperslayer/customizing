<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/statistics/inspection_hist.js" type="text/javascript" charset="UTF-8"></script>
<h1 class="page-title">${currentMenuName}</h1>
<script
	src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js"
	type="text/javascript"></script>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light portlet-fit portlet-datatable bordered">
			<div class="portlet-body">
				<div class="table-container">
					<div id="datatable_ajax_2_wrapper"
						class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
						<div class="raw">
							<div class="col-md-6"
								style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
								
								<div class="portlet box grey-salt  ">
		                            <div class="portlet-title" style="background-color: #2B3643;">
		                                <div class="caption">
		                                    <i class="fa fa-search"></i>검색 </div>
	                                    <div class="tools">
						                 	<a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
							             </div>
		                            </div>
		                            <div id="searchBar" class="portlet-body form" >
		                                <div class="form-body" style="padding-left:10px; padding-right:10px;">
		                                    <div class="form-group">
		                                        <form id="listForm" method="POST" class="mt-repeater form-horizontal">
		                                            <div data-repeater-list="group-a">
		                                                <div class="row">
		                                                    <!-- jQuery Repeater Container -->
		                                                    <div class="col-md-2">
			                                                    <label class="control-label">기간</label>
		                                                    	<div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
																	<input type="text" class="form-control" id="search_fr"
																		name="search_from"
																		value="<procdate:convertDate value="${paramBean.search_from}" />"> <span
																		class="input-group-addon"> &sim; </span> <input type="text"
																		class="form-control" id="search_to" name="search_to"
																		value="<procdate:convertDate value="${paramBean.search_to}" />">
																</div> 
															</div>
		                                                </div>
		                                            </div>
		                                            <hr/>
		                                            <div align="right">
			                                            <button type="reset"
															class="btn btn-sm red-mint btn-outline sbold uppercase"
															onclick="resetOptions(inspectionHistConfig['listUrl'])">
															<i class="fa fa-remove"></i> <font>초기화
														</button>
														<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
															onclick="searchInspectionHistList()">
															<i class="fa fa-search"></i> 검색
														</button>&nbsp;&nbsp;
														<%-- <div class="btn-group">
															<a onclick="excelMenuMappSetup('${search.total_count}')">
																<img align="right" src="${rootPath}/resources/image/icon/XLS_3.png">
															</a>
														</div> --%>
													</div>
													
													<input type="hidden" name="seq" value="" /> 
													<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" /> 
													<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" /> 
													<input type="hidden" name="current_menu_id" value="${currentMenuId}" /> 
													<input type="hidden" name="page_num" value="${pageInfo.page_num}" />
<%-- 													<input type="hidden" name="isSearch" value="${paramBean.isSearch }" /> --%>
		                                        </form>
		                                    </div>
		                                </div>
		                            </div>
                        		</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-12" align="left">
								<a class="btn btn-sm blue btn-outline sbold uppercase"
									onclick="goInspectionDetail(null)">신규 <i class="fa fa-plus"></i>
								</a>
							</div>
						</div>
						<div class="raw">
							<table style="border-top: 1px solid #e7ecf1;"
								class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
								role="grid">
								<colgroup>
									<col width="20%" />
									<col width="60%" />
									<col width="20%" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col" style="text-align: center;">점검일</th>
										<th scope="col" style="text-align: center;">TITLE</th>
										<th scope="col" style="text-align: center;">점검자</th>
									</tr>
								</thead>
								<tbody>
									<c:choose>
										<c:when test="${empty list}">
											<tr>
												<td colspan="3" align="center">데이터가 없습니다.</td>
											</tr>
										</c:when>
										<c:otherwise>
											<c:set value="${pageInfo.total_count}" var="count" />
											<c:forEach items="${list}" var="item" varStatus="status">
												<tr style='cursor: pointer;' onclick="goInspectionDetail(${item.seq})">
													<td align="center">
														<fmt:parseDate value="${item.proc_date}" pattern="yyyyMMdd" var="proc_date" /> 
														<fmt:formatDate value="${proc_date}" pattern="yyyy-MM-dd" /> 
													</td>
													<td align="center">${fn:substring(item.proc_month,0,4) }년 ${fn:substring(item.proc_month,4,6) }월 정기점검</td>
													<td align="center">${item.inspector }</td>
												</tr>
												<c:set var="count" value="${count - 1 }" />
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
							<!-- 페이징 영역 -->
							<c:if test="${pageInfo.total_count > 0}">
								<div id="pagingframe" align="center">
									<p>
										<ctl:paginator currentPage="${pageInfo.page_num}"
											rowBlockCount="${pageInfo.size}"
											totalRowCount="${pageInfo.total_count}" />
									</p>
								</div>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form action="exdownload.html" method="post" id="exdown" name="exdown"
enctype="multipart/form-data"></form>
<script type="text/javascript">
	var inspectionHistConfig = {
		"listUrl" : "${rootPath}/statistics/inspection_hist.html",
		"detailUrl" : "${rootPath}/statistics/inspection_hist_detail.html",
		"downloadUrl" : "${rootPath}/statistics/download.html"
	};
</script>
