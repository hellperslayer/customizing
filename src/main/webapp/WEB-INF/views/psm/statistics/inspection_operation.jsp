<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="table-container">
	<div id="datatable_ajax_2_wrapper"
		class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
		<div class="raw">
			<div class="col-md-6"
				style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
				
				<div class="portlet box grey-salt  ">
                        <div class="portlet-title" style="background-color: #2B3643;">
                            <div class="caption">
                                <i class="fa fa-search"></i>검색</div>
                            <div class="tools">
                                <a id="searchBar_icon" href="" class="collapse"> </a>
                            </div>
                        </div>
                        <div id="searchBar" class="portlet-body form" >
                            <div class="form-body" style="padding-left:20px; padding-right:10px;">
                                <div class="form-group">
                                    <form id="listForm2" method="POST" class="mt-repeater form-horizontal">
                                    	<input type="hidden" name="tab_flag" value="2"/>
                                        <div data-repeater-list="group-a">
                                        <br>
                                            <div data-repeater-item class="mt-repeater-item">
                                                <!-- jQuery Repeater Container -->
                                                <div class="mt-repeater-input">
                                                    <label class="control-label">기간</label>
                                                    <br/>
                                                    <div class="row">
                                                    <div class="col-md-1">
                                                    <select class="form-control input-small" id="select_year" name="select_year">
                                                    <c:forEach var="idx" begin="0" end="2" >
                                                    	<option value="${sel_year - (2-idx)}" <c:if test="${(sel_year - (2-idx)) == sel_year}">selected</c:if>>${sel_year - (2-idx)} 년</option>
                                                    </c:forEach>
                                                    </select>
                                                    </div>
                                                    <div class="col-md-1">
                                                    <select class="form-control input-small" id="select_month" name="select_month">
                                                    <c:forEach var="month" begin="1" end="12">
                                                    	<option value="${month }" <c:if test="${month == sel_month}">selected</c:if>>${month } 월</option>
                                                    </c:forEach>
                                                    </select>
                                                    </div>
                                                    <!-- <div class="col-md-1">
                                                    <button type="button" class="btn btn-sm  btn-outline sbold uppercase"
														onclick="searchInspection2()">
														<i class="fa fa-search"></i> 검색
													</button>
                                                    </div> -->
                                                    </div>
												</div>
                                            </div>
                                        </div>
                                        <div align="right">
                                         	<!-- <button type="reset"
											class="btn btn-sm red-mint btn-outline sbold uppercase"
											onclick="resetOptions(inspectionConfig['listUrl'])">
											<i class="fa fa-remove"></i> <font>초기화</font>
											</button> -->
											<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
												onclick="searchInspection2()">
												<i class="fa fa-search"></i> 검색
											</button>&nbsp;&nbsp;
										</div>
							
									<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" /> 
									<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" /> 
									<input type="hidden" name="current_menu_id" value="${currentMenuId}" /> 
									<input type="hidden" id="rootPath" name="rootPath" value="${rootPath}" /> 
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
			</div>
		</div>
		<div class="raw">
			<table style="border-top: 1px solid #e7ecf1;" class="table table-striped table-bordered table-checkable no-footer" role="grid">
				<%-- <colgroup>
					<col width="*" />
					<c:forEach begin="1" end="31"><col width="2.5%" /></c:forEach>
				</colgroup> --%>
				<thead>
					<tr>
						<th width="*" style="vertical-align: middle; text-align: center;">시스템</th>
						<c:forEach var="item" begin="1" end="31">
							<th width="2.8%" style="vertical-align: middle; text-align: center;">${item }</th>
						</c:forEach>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${operation }" var="data">
					<tr>
						<td>${data.system_name }</td>
						<td align="right" <c:choose><c:when test="${data.cnt1 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt1 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt2 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt2 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt3 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt3 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt4 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt4 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt5 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt5 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt6 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt6 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt7 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt7 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt8 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt8 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt9 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt9 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt10 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt10 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt11 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt11 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt12 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt12 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt13 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt13 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt14 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt14 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt15 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt15 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt16 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt16 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt17 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt17 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt18 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt18 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt19 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt19 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt20 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt20 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt21 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt21 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt22 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt22 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt23 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt23 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt24 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt24 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt25 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt25 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt26 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt26 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt27 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt27 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt28 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt28 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt29 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt29 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt30 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt30 }</td>
						<td align="right" <c:choose><c:when test="${data.cnt31 > 0 }">bgcolor=""</c:when><c:otherwise>bgcolor="red"</c:otherwise></c:choose>>${data.cnt31 }</td>
					</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>