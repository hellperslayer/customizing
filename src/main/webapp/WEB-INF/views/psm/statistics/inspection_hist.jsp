<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/statistics/inspection_hist.js" type="text/javascript" charset="UTF-8"></script>
<script
	src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js"
	type="text/javascript"></script>
<div class="table-container">
	<div id="datatable_ajax_2_wrapper"
		class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
		<div class="raw">
			<div class="col-md-6"
				style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
				
				<div class="portlet box grey-salt  ">
                          <div class="portlet-title" style="background-color: #2B3643;">
                              <div class="caption">
                                  <i class="fa fa-search"></i>검색 </div>
                                 <div class="tools">
		                 	<a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
			             </div>
                          </div>
                          <div id="searchBar" class="portlet-body form" >
                              <div class="form-body" style="padding-right:10px;padding-left:20px;">
                                  <div class="form-group">
                                      <form id="reportForm" method="POST" class="mt-repeater form-horizontal">
                                          <div data-repeater-list="group-a">
                                              <div class="row">
                                                  <!-- jQuery Repeater Container -->
                                                  <div class="mt-repeater-input" style="padding-top:20px;">
	                                                  <div class="col-md-12">
	                                                   <label class="control-label">기간</label>
													<div class="row">
														<div class="col-md-1">
															<select class="form-control input-small" id="select_year"
																name="select_year">
																<c:forEach var="idx" begin="0" end="2">
																	<option value="${sel_year - (2-idx)}"
																		<c:if test="${(sel_year - (2-idx)) == sel_year}">selected</c:if>>${sel_year - (2-idx)}
																		년</option>
																</c:forEach>
															</select>
														</div>
														<div class="col-md-1">
															<select class="form-control input-small"
																id="hist_month" name="hist_month">
																<option value="all">전체</option>
																<c:forEach var="month" begin="1" end="12">
																	<option value="<fmt:formatNumber minIntegerDigits="2" value="${month}" />"
																		<c:if test="${paramBean.hist_month ne 'all' and month eq paramBean.hist_month}">selected</c:if>>${month }
																		월</option>
																</c:forEach>
															</select>
														</div>
													</div>
												</div>
                                              </div>
                                          </div>
                                          <hr style="margin:15px 0;"/>
                                          <div align="right">
										<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
											onclick="goPage(1)">
											<i class="fa fa-search"></i> 검색
										</button>&nbsp;&nbsp;
										<%-- <div class="btn-group">
											<a onclick="excelMenuMappSetup('${search.total_count}')">
												<img align="right" src="${rootPath}/resources/image/icon/XLS_3.png">
											</a>
										</div> --%>
									</div>
									
									<input type="hidden" name="seq" value="" /> 
									<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" /> 
									<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" /> 
									<input type="hidden" name="current_menu_id" value="${currentMenuId}" /> 
									<input type="hidden" name="page_num" value="${paramBean.page_num}" />
									<input type="hidden" name="select_tab"/>
									<input type="hidden" name="report_seq"/>
                                      </form>
                                  </div>
                              </div>
                          </div>
                 </div>
			</div>
		</div>
		
		<div class="raw">
			<table style="border-top: 1px solid #e7ecf1;"
				class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
				role="grid">
				<colgroup>
					<col width="20%" />
					<col width="40%" />
					<col width="20%" />
					<col width="10%" />
					<col width="10%" />
				</colgroup>
				<thead>
					<tr>
						<th scope="col" style="text-align: center;">점검일</th>
						<th scope="col" style="text-align: center;">TITLE</th>
						<th scope="col" style="text-align: center;">로그수집기간</th>
						<th scope="col" style="text-align: center;">점검자</th>
						<th scope="col" style="text-align: center;">삭제</th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${empty reportlist}">
							<tr>
								<td colspan="5" align="center">데이터가 없습니다.</td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:set value="${pageInfo.total_count}" var="count" />
							<c:forEach items="${reportlist}" var="item" varStatus="status">
								<tr style='cursor: pointer;' onclick="loadReport('${item.report_seq}')">
									<td align="center">${item.update_time }</td>
									<td align="center">${item.report_title }</td>
									<td align="center">${item.search_fr} ~ ${item.search_to}</td>
									<td align="center">${item.update_id }</td>
									<td align="center" style="padding: 3px">
										<button type="button" class="btn btn-sm red-mint btn-outline sbold uppercase" onclick="event.cancelBubble=true;report_delete('${item.report_seq}')">
											<i class="fa fa-remove"></i> <font>삭제
									</td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
			<!-- 페이징 영역 -->
			<c:if test="${reportCount > 0}">
				<div id="pagingframe" align="center">
					<p>
						<ctl:paginator currentPage="${empty paramBean.page_num?1:paramBean.page_num}"
							rowBlockCount="${paramBean.page_size}"
							totalRowCount="${reportCount}" />
					</p>
				</div>
			</c:if>
		</div>
	</div>
</div>
