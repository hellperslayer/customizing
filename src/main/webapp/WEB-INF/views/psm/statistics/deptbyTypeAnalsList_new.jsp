<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>

<script type="text/javascript" charset="UTF-8">
	var use_fullscan = '${use_fullscan}';
	var searchType = '${searchType}';
</script>

<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/common/ezDatepicker.js"
	type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/statistics/statistics.js"
	type="text/javascript" charset="UTF-8"></script>
<style>
.mjpoint {
	padding: 0px;
	margin: 0px;
	vertical-align: middle;
	text-align: right;
}

td {
	padding: 0px;
	margin: 0px;
	vertical-align: middle;
	text-align: center;
}

.mjpointZero {
	padding: 0px;
	margin: 0px;
	vertical-align: middle;
	text-align: right;
}

th {
	padding: 0px;
	margin: 0px;
	vertical-align: middle;
	text-align: center;
}
</style>
<h1 class="page-title">${currentMenuName}</h1>
<script
	src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js"
	type="text/javascript"></script>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light portlet-fit portlet-datatable bordered">
			<%-- <div class="portlet-title">
				<div class="caption">
					<i class="icon-settings font-dark"></i> <span
						class="caption-subject font-dark sbold uppercase">${currentMenuName}
						리스트</span>
				</div>
				<div class="actions">
					<div class="btn-group">
						<a class="btn red btn-outline btn-circle" onclick="deptmoveExcel()"
							data-toggle="dropdown"> <i class="fa fa-share"></i> <span
							class="hidden-xs"> 엑셀 </span>
						</a>
					</div>
				</div>
			</div> --%>
			<div class="portlet-body">
				<div class="table-container">
					<div id="datatable_ajax_2_wrapper"
						class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
						<div class="raw">
							<div class="col-md-6"
								style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
								
								<div class="portlet box grey-salt  ">
			                         <div class="portlet-title" style="background-color: #2B3643;">
			                             <div class="caption">
			                                 <i class="fa fa-search"></i>검색 & 엑셀</div>
			                             <div class="tools">
			                                 <a id="searchBar_icon" href="" class="collapse"> </a>
			                             </div>
			                         </div>
			                         <div id="searchBar" class="portlet-body form" >
			                             <div class="form-body" style="padding-left:20px; padding-right:10px;">
			                                 <div class="form-group">
			                                     <form id="listForm" method="POST" class="mt-repeater form-horizontal">
			                                         <div class="row">
			                                                 <div class="col-md-2">
			                                                     <label class="control-label">기간</label>
			                                                     <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
																	<input type="text" class="form-control" id="search_fr"
																		name="search_from"
																		value="<procdate:convertDate value="${paramBean.search_from}" />"> <span
																		class="input-group-addon"> &sim; </span> <input type="text"
																		class="form-control" id="search_to" name="search_to"
																		value="<procdate:convertDate value="${paramBean.search_to}" />">
																</div> </div>
															<div class="col-md-2">
			                                                     <label class="control-label">소속</label>
			                                                     <br/>
																<input type="text" class="form-control" name="dept_name" value="${paramBean.dept_name}" />
															</div>
															
																<div class="col-md-2">
																	<label class="control-label">조회대상</label><br/>
																	<select id="searchType" name="searchType" class="ticket-assign form-control">
																		<option value="01" >접속기록조회</option>
																		<option value="02" <c:if test='${searchType eq "02"}'>selected="selected"</c:if>>시스템로그조회</option>
																		<option value="03" <c:if test='${searchType eq "03"}'>selected="selected"</c:if>>다운로드 로그조회</option>
																	</select>
																</div>
															<div class="col-md-2">
			                                                     <label class="control-label">시스템명</label>
			                                                     <br/>
			                                                     <select name="system_seq" id="system_seq" class="form-control">
																	<option value=""
																		${paramBean.system_seq == '' ? 'selected="selected"' : ''}>
																		----- 전 체 -----</option>
																	<c:if test="${empty systemList}">
																		<option>시스템 데이터 없습니다.</option>
																	</c:if>
																	<c:if test="${!empty systemList}">
																		<c:forEach items="${systemList}" var="i" varStatus="z">
																			<option value="${i.system_seq}"
																				${i.system_seq==paramBean.system_seq ? "selected=selected" : "" }>
																				${ i.system_name}</option>
																		</c:forEach>
																	</c:if>
																</select> 
															</div>
			                                             </div>
			                                             <hr/>
			                                         <div align="right">
			                                          	<button type="reset"
															class="btn btn-sm red-mint btn-outline sbold uppercase"
															onclick="resetOptions(statisticsListConfig['listUrl'])">
															<i class="fa fa-remove"></i> <font>초기화
														</button>
														<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
															onclick="moveStatistics()">
															<i class="fa fa-search"></i> 검색
														</button>&nbsp;&nbsp;
														<a onclick="deptmoveExcel()"><img src="${rootPath}/resources/image/icon/XLS_3.png"></a>
													</div>
													<input type="hidden" name="main_menu_id"
														value="${paramBean.main_menu_id }" /> <input
														type="hidden" name="sub_menu_id"
														value="${paramBean.sub_menu_id }" /> <input type="hidden"
														name="current_menu_id" value="${currentMenuId}" /> <input
														type="hidden" name="page_num" /> <input type="hidden"
														id="check_type" name="check_type"
														value="${paramBean.check_type}" /> <input type="hidden"
														id="sort_flag" name="sort_flag"
														value="${paramBean.sort_flag}" /> <input type="hidden"
														id="rootPath" name="rootPath" value="${rootPath}" /> <input
														type="hidden" id="check_dp" name="check_dp"
														value="${paramBean.chk_date_type}" />
													<input type="hidden" name="isSearch" value="" />
			                                     </form>
			                                 </div>
			                             </div>
			                         </div>
			                     </div>
							</div>
						</div>
						<div class="raw">
							<table style="border-top: 1px solid #e7ecf1;"
								class="table table-striped table-bordered table-checkable dataTable no-footer"
								role="grid">
								<c:set var="resultType_cnt" value="${100/(3+resultTypes.size())}" />
								<colgroup>
									<col width="8%" />
									<c:forEach items="${resultTypes }">
										<col width="${resultType_cnt }%"/>
									</c:forEach>
									<col />
								</colgroup>
								<thead>
									<tr>
										<th scope="col" rowspan="2"
											style="padding: 0px; margin: 0px; vertical-align: middle; text-align: center;">소속</th>
										<th scope="col" colspan="10"
											style="padding: 10px; margin: 10px; vertical-align: middle; text-align: center;">개인정보유형</th>
									</tr>
									<tr>
										<c:forEach items="${resultTypes }" var="result">
											<th style="padding-top: 5px; padding-bottom: 5px; padding-left:0px; padding-right:0px; margin: 0px; vertical-align: middle; text-align: center;">
											<c:set var="result_id" value="type${result.code_id}"/>
												<p style="padding: 0px; margin: 0px;">
													${result.code_name }<img class="desc_btn" id="${result_id }"
														onclick="javascript:sendDesc('${result_id }', 1)"
														src="${rootPath}/resources/image/common/desc_btn.png"
														alt="내림차순" title="내림차순" />
												</p>
											</th>
										</c:forEach>
										<th width="6%" style="padding-top: 5px; padding-bottom: 5px; margin: 0px; vertical-align: middle; text-align: center;">
											<p style="padding: 0px; margin: 0px;">
												합계<img class="desc_btn" id="type_sum"
													onclick="javascript:sendDesc('type_sum', 1)"
													src="${rootPath}/resources/image/common/desc_btn.png"
													alt="내림차순" title="내림차순" />
											</p>
										</th>
									</tr>
								</thead>
								<tbody>
									<c:choose>
										<c:when test="${empty statisticsList.statistics}">
											<tr>
												<td colspan="${resultTypes.size() + 2}" align="center">데이터가 없습니다.</td>
											</tr>
										</c:when>
										<c:otherwise>
<%-- 											<c:set value="${pageInfo.total_count}" var="count" /> --%>
											<c:forEach items="${statisticsList.statistics}" var="statistics">
												<tr>
													<c:choose>
														<c:when test="${searchType=='02' }"><td style="padding: 0px; margin: 0px; vertical-align: middle; text-align: center;">시스템로그</td></c:when>
														<c:otherwise><td style="padding: 0px; margin: 0px; vertical-align: middle; text-align: center;">${statistics.dept_name}(${statistics.dept_id})</td></c:otherwise>
													</c:choose>
													<c:forEach items="${resultTypes }" var="result" varStatus="status">
														<c:choose>
															<c:when test="${result.result_type_order eq '1' }">
																<c:set var="statistics_type" value="${statistics.type1 }"/>
																<c:set var="statistics_type_max" value="${statisticsList.type1_max }"/>
															</c:when>
															<c:when test="${result.result_type_order eq '2'}">
																<c:set var="statistics_type" value="${statistics.type2 }"/>
																<c:set var="statistics_type_max" value="${statisticsList.type2_max }"/>
															</c:when>
															<c:when test="${result.result_type_order eq '3' }">
																<c:set var="statistics_type" value="${statistics.type3 }"/>
																<c:set var="statistics_type_max" value="${statisticsList.type3_max }"/>
															</c:when>
															<c:when test="${result.result_type_order eq '4' }">
																<c:set var="statistics_type" value="${statistics.type4 }"/>
																<c:set var="statistics_type_max" value="${statisticsList.type4_max }"/>
															</c:when>
															<c:when test="${result.result_type_order eq '5' }">
																<c:set var="statistics_type" value="${statistics.type5 }"/>
																<c:set var="statistics_type_max" value="${statisticsList.type5_max }"/>
															</c:when>
															<c:when test="${result.result_type_order eq '6' }">
																<c:set var="statistics_type" value="${statistics.type6 }"/>
																<c:set var="statistics_type_max" value="${statisticsList.type6_max }"/>
															</c:when>
															<c:when test="${result.result_type_order eq '7' }">
																<c:set var="statistics_type" value="${statistics.type7 }"/>
																<c:set var="statistics_type_max" value="${statisticsList.type7_max }"/>
															</c:when>
															<c:when test="${result.result_type_order eq '8' }">
																<c:set var="statistics_type" value="${statistics.type8 }"/>
																<c:set var="statistics_type_max" value="${statisticsList.type8_max }"/>
															</c:when>
															<c:when test="${result.result_type_order eq '9' }">
																<c:set var="statistics_type" value="${statistics.type9 }"/>
																<c:set var="statistics_type_max" value="${statisticsList.type9_max }"/>
															</c:when>
															<c:when test="${result.result_type_order eq '10' }">
																<c:set var="statistics_type" value="${statistics.type10 }"/>
																<c:set var="statistics_type_max" value="${statisticsList.type10_max }"/>
															</c:when>
															<c:when test="${result.result_type_order eq '11' }">
																<c:set var="statistics_type" value="${statistics.type11 }"/>
																<c:set var="statistics_type_max" value="${statisticsList.type11_max }"/>
															</c:when>
															<c:when test="${result.result_type_order eq '12' }">
																<c:set var="statistics_type" value="${statistics.type12 }"/>
																<c:set var="statistics_type_max" value="${statisticsList.type12_max }"/>
															</c:when>
															<c:when test="${result.result_type_order eq '13' }">
																<c:set var="statistics_type" value="${statistics.type13 }"/>
																<c:set var="statistics_type_max" value="${statisticsList.type13_max }"/>
															</c:when>
															<c:when test="${result.result_type_order eq '14' }">
																<c:set var="statistics_type" value="${statistics.type14 }"/>
																<c:set var="statistics_type_max" value="${statisticsList.type14_max }"/>
															</c:when>
															<c:when test="${result.result_type_order eq '15' }">
																<c:set var="statistics_type" value="${statistics.type15 }"/>
																<c:set var="statistics_type_max" value="${statisticsList.type15_max }"/>
															</c:when>
															<c:when test="${result.result_type_order eq '16' }">
																<c:set var="statistics_type" value="${statistics.type16 }"/>
																<c:set var="statistics_type_max" value="${statisticsList.type16_max }"/>
															</c:when>
															<c:when test="${result.result_type_order eq '17' }">
																<c:set var="statistics_type" value="${statistics.type17 }"/>
																<c:set var="statistics_type_max" value="${statisticsList.type17_max }"/>
															</c:when>
															<c:when test="${result.result_type_order eq '18' }">
																<c:set var="statistics_type" value="${statistics.type18 }"/>
																<c:set var="statistics_type_max" value="${statisticsList.type18_max }"/>
															</c:when>
														</c:choose>
														
														<td <statistics:compare max="${statistics_type_max}" value="${statistics_type}" />>
															<c:if test="${statistics_type !=0}">
																<p class="mjpoint" style=" cursor: pointer;" 
																onclick="moveToAlldept_new2(${result.result_type_order },'${statistics.dept_name}','dept', '${searchType }')">																
															</c:if> 
															<c:if test="${statistics_type ==0}">
																<p class="mjpointZero">
															</c:if> 
															<fmt:formatNumber value="${statistics_type}" type="number" />
															</p>
														</td>
													</c:forEach>
													<td <statistics:compare max="${statisticsList.type_sum_max}" value="${statistics.type_sum}" />>
														<c:if test="${statistics.type_sum !=0}">
															<p style=" cursor: pointer;" class="mjpoint" 
															onclick="moveToAlldept_new2('all','${statistics.dept_name}','dept', '${searchType }')">
														</c:if> 
														<c:if test="${statistics.type_sum ==0}">
															<p class="mjpointZero">
														</c:if> 
														<fmt:formatNumber value="${statistics.type_sum}" type="number" />
														</p>
													</td>
												</tr>
<%-- 												<c:set var="count" value="${count - 1 }" /> --%>
											</c:forEach>
											<tr>
												<td colspan="1">합계</td>
												<c:forEach items="${resultTypes }" var="result">
													<c:choose>
														<c:when test="${result.result_type_order eq '1' }"><td style="text-align: right;"><fmt:formatNumber value="${statisticsSum.type1}" type="number" /></td></c:when>
														<c:when test="${result.result_type_order eq '2' }"><td style="text-align: right;"><fmt:formatNumber value="${statisticsSum.type2}" type="number" /></td></c:when>
														<c:when test="${result.result_type_order eq '3' }"><td style="text-align: right;"><fmt:formatNumber value="${statisticsSum.type3}" type="number" /></td></c:when>
														<c:when test="${result.result_type_order eq '4' }"><td style="text-align: right;"><fmt:formatNumber value="${statisticsSum.type4}" type="number" /></td></c:when>
														<c:when test="${result.result_type_order eq '5' }"><td style="text-align: right;"><fmt:formatNumber value="${statisticsSum.type5}" type="number" /></td></c:when>
														<c:when test="${result.result_type_order eq '6' }"><td style="text-align: right;"><fmt:formatNumber value="${statisticsSum.type6}" type="number" /></td></c:when>
														<c:when test="${result.result_type_order eq '7' }"><td style="text-align: right;"><fmt:formatNumber value="${statisticsSum.type7}" type="number" /></td></c:when>
														<c:when test="${result.result_type_order eq '8' }"><td style="text-align: right;"><fmt:formatNumber value="${statisticsSum.type8}" type="number" /></td></c:when>
														<c:when test="${result.result_type_order eq '9' }"><td style="text-align: right;"><fmt:formatNumber value="${statisticsSum.type9}" type="number" /></td></c:when>
														<c:when test="${result.result_type_order eq '10' }"><td style="text-align: right;"><fmt:formatNumber value="${statisticsSum.type10}" type="number" /></td></c:when>
														<c:when test="${result.result_type_order eq '11' }"><td style="text-align: right;"><fmt:formatNumber value="${statisticsSum.type11}" type="number" /></td></c:when>
														<c:when test="${result.result_type_order eq '12' }"><td style="text-align: right;"><fmt:formatNumber value="${statisticsSum.type12}" type="number" /></td></c:when>
														<c:when test="${result.result_type_order eq '13' }"><td style="text-align: right;"><fmt:formatNumber value="${statisticsSum.type13}" type="number" /></td></c:when>
														<c:when test="${result.result_type_order eq '14' }"><td style="text-align: right;"><fmt:formatNumber value="${statisticsSum.type14}" type="number" /></td></c:when>
														<c:when test="${result.result_type_order eq '15' }"><td style="text-align: right;"><fmt:formatNumber value="${statisticsSum.type15}" type="number" /></td></c:when>
														<c:when test="${result.result_type_order eq '16' }"><td style="text-align: right;"><fmt:formatNumber value="${statisticsSum.type16}" type="number" /></td></c:when>
														<c:when test="${result.result_type_order eq '17' }"><td style="text-align: right;"><fmt:formatNumber value="${statisticsSum.type17}" type="number" /></td></c:when>
														<c:when test="${result.result_type_order eq '18' }"><td style="text-align: right;"><fmt:formatNumber value="${statisticsSum.type18}" type="number" /></td></c:when>
													</c:choose>
												</c:forEach>
												<td style="text-align: right;"><fmt:formatNumber value="${statisticsSum.type_sum}" type="number" /></td>
											</tr>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<form action="" id="deptForm" method="post">
	<input type="hidden" id="search_to" name="search_to"
		value="${paramBean.search_to}"> <input type="hidden"
		id="search_from" name="search_from" value="${paramBean.search_from}">
	<input type="hidden" id="privacyType" name="privacyType" value="">
	<input type="hidden" id="check_times" name="check_times" value="${paramBean.chk_date_type}">
	<input type="hidden" id="dept_id" name="dept_id" value="">
	<input type="hidden" id="dept_name" name="dept_name" value="">
	<input type="hidden" name="system_seq" value="">
	<input type="hidden" id="shType" name="shType" value="">
	<input type="hidden" id="searchType" name="searchType" value="${searchType }">
	<input type="hidden" name="current_menu_id" value="MENU00041" />
	<input type="hidden" name="main_menu_id" value="MENU00040" />
</form>
<script type="text/javascript">

	var statisticsListConfig = {
		"listUrl":"${rootPath}/statistics/list_deptbyTypeAnals.html",
		"allListUrl":"${rootPath}/statistics/connectLogResultList.html",
		"allListUrl2":"${rootPath}/statistics/downloadLogResultList.html",
		"downloadUrl":"${rootPath}/statistics/deptdownload.html"
	};
	
</script>