<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>

<script type="text/javascript" charset="UTF-8">
	var use_fullscan = '${use_fullscan}';
	var searchType = '${searchType}';
</script>

<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/statistics/statistics.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>
<style>
.mjpoint {
	padding: 0px;
	margin: 0px;
	vertical-align: middle;
	text-align: right;
}

td {
	padding: 0px;
	margin: 0px;
	vertical-align: middle;
	text-align: center;
}

.mjpointZero {
	padding: 0px;
	margin: 0px;
	vertical-align: middle;
	text-align: right;
}
</style>
<h1 class="page-title">${currentMenuName}</h1>


<div class="row">
	<div class="col-md-12">
		<div class="portlet light portlet-fit portlet-datatable bordered">
			<div class="portlet-body">
				<div class="table-container">
					<div id="datatable_ajax_2_wrapper"
						class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
						<div class="raw">
							<div class="col-md-6"
								style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
								
								<div class="portlet box grey-salt  ">
			                         <div class="portlet-title" style="background-color: #2B3643;">
			                             <div class="caption">
			                                 <i class="fa fa-search"></i>검색 & 엑셀 </div>
			                             <div class="tools">
			                                 <a id="searchBar_icon" href="" class="collapse"> </a>
			                             </div>
			                         </div>
			                         <div id="searchBar" class="portlet-body form" >
			                             <div class="form-body" style="padding-left:20px; padding-right:10px;">
			                                 <div class="form-group">
			                                     <form id="listForm" method="POST" class="mt-repeater form-horizontal">
			                                         <div class="row">
															<div class="col-md-2">
			                                                     <label class="control-label">기간</label>
			                                                     <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
																	<input type="text" class="form-control" id="search_fr" name="search_from" value="<procdate:convertDate value="${paramBean.search_from}" />">
																	<span class="input-group-addon"> &sim; </span> 
																	<input type="text" class="form-control" id="search_to" name="search_to"	value="<procdate:convertDate value="${paramBean.search_to}" />">
																</div>
															</div>
															
															<div class="col-md-2">
																<label class="control-label">조회대상</label><br/>
																<select id="searchType" name="searchType" class="ticket-assign form-control">
																	<option value="01">접속기록조회</option>
																	<option value="02" <c:if test='${searchType eq "02"}'>selected="selected"</c:if>>시스템로그조회</option>
																	<option value="03" <c:if test='${searchType eq "03"}'>selected="selected"</c:if>>다운로드 로그조회</option>
																</select>
															</div>
			                                             </div>
			                                             <hr/>
			                                         <div align="right">
			                                          	<button type="reset"
															class="btn btn-sm red-mint btn-outline sbold uppercase"
															onclick="resetOptions(statisticsListConfig['listUrl'])">
															<i class="fa fa-remove"></i> <font>초기화
														</button>
														<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
															onclick="moveStatistics()">
															<i class="fa fa-search"></i> 검색
														</button>&nbsp;&nbsp;
														<a onclick="moveExcel()"><img src="${rootPath}/resources/image/icon/XLS_3.png"></a>
													</div>
											
													<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" /> 
													<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" /> 
													<input type="hidden" name="current_menu_id" value="${currentMenuId}" /> 
													<input type="hidden" name="page_num" /> 
													<input type="hidden" id="check_type" name="check_type" value="${paramBean.check_type}" /> 
													<input type="hidden" id="sort_flag" name="sort_flag" value="${paramBean.sort_flag}" /> 
													<input type="hidden" id="rootPath" name="rootPath" value="${rootPath}" /> 
													<input type="hidden" id="check_dp" name="check_dp" value="${paramBean.chk_date_type}" />
													<input type="hidden" name="isSearch" value="${paramBean.isSearch}" />
			                                     </form>
			                                 </div>
			                             </div>
			                         </div>
			                     </div>
							</div>
						</div>
						<div class="row">
							<table style="border-top: 1px solid #e7ecf1;" class="table table-striped table-bordered table-checkable dataTable no-footer" role="grid">
								<thead>
								<tr>
									<td width="7%">기간</td>
									<c:forEach items="${systemSeq }" var="sys">
									<c:forEach items="${systemList}" var="sysName" varStatus="st">
										<c:if test="${sys==sysName.system_seq }"><td>${sysName.system_name }
										<c:set value="a${st.index}" var="sysid"/>
										<img class="desc_btn" id="${sysName.system_seq}" onclick="javascript:sendDesc('${sysid}', 1)" src="${rootPath}/resources/image/common/desc_btn.png" alt="내림차순" title="내림차순" />
										</td></c:if>
									</c:forEach>
									</c:forEach>
									<td>합계</td>
								</tr>
								</thead>
								<tbody>
								<c:forEach items="${statistics }" var="s">
									<tr>
									<td>${s['proc_date'] }</td>
									<c:forEach begin="0" end="${mapCt}" step="1" var="no">
									<c:set var="col" value="a${no}"/>
									<td <statistics:compare max="${maxMap[col]}" value="${s[col]}" /> >
										<c:if test="${s[col] !=0}">
											<p class="mjpoint">																
										</c:if> <c:if test="${s[col] ==0}">
											<p class="mjpointZero">
										</c:if> <fmt:formatNumber value="${s[col]}" type="number" />
										</p>
									</td>
									</c:forEach>
									<td <statistics:compare max="${maxMap['logcnt']}" value="${s['logcnt'] }" /> >
										<c:if test="${s['logcnt'] !=0}">
											<p class="mjpoint">																
										</c:if> <c:if test="${s['logcnt'] ==0}">
											<p class="mjpointZero">
										</c:if> <fmt:formatNumber value="${s['logcnt'] }" type="number" />
										</p>
									</td>
									</tr>
								</c:forEach>
								<tr><td>합계</td>
								<c:set var="sumVal" value="0"/>
								<c:forEach begin="0" end="${mapCt}" step="1" var="no2">
								<td>
								<c:set var="col2" value="a${no2}"/>
								<c:if test="${statisticsSum[col2] !=0}"><p class="mjpoint"><fmt:formatNumber value="${statisticsSum[col2]}" type="number" /></p></c:if>
								<c:if test="${statisticsSum[col2] ==0}"><p class="mjpointZero">0</p></c:if>
								<c:set var="sumVal" value="${sumVal + statisticsSum[col2] }"/>
								</td>
								</c:forEach>
								<td><p class="mjpoint" style=" cursor: pointer;"><fmt:formatNumber value="${sumVal }" type="number" /></p></td>
								</tr>
								</tbody>
							</table>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<form action="" id="perdForm" method="post">
	<input type="hidden" id="search_to" name="search_to" value="${paramBean.search_to}">
	<input type="hidden" id="search_from" name="search_from" value="${paramBean.search_from}">
	<input type="hidden" id="privacyType" name="privacyType" value="">
	<input type="hidden" id="shType" name="shType" value="">
	<input type="hidden" id="check_times" name="check_times" value="${paramBean.chk_date_type}">
	<input type="hidden" name="current_menu_id" value="MENU00041" />
	<input type="hidden" name="main_menu_id" value="MENU00040" />
	<input type="hidden" id="searchType" name="searchType" value="">
</form>
<script type="text/javascript">
 
	var statisticsListConfig = {
		"listUrl":"${rootPath}/statistics/perdbyStatisticsList.html"
		,"downloadUrl":"${rootPath}/statistics/perdSystemdownload.html"
	};
	

	var perdConfig = {
		"listUrl":"${rootPath}/statistics/connectLogResultList.html",
		"listUrl2":"${rootPath}/statistics/downloadLogResultList.html"
	};

</script>