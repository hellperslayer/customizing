<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<c:set var="now" value="<%=new java.util.Date()%>" />
<c:set var="nowDate">
		<fmt:formatDate value="${now}" pattern="yyyy년 MM월" />
</c:set>
<c:set var="nowYear">
		<fmt:formatDate value="${now}" pattern="yyyy" />
</c:set>
<c:set var="nowMonth">
		<fmt:formatDate value="${now}" pattern="MM" />
</c:set>
<div class="table-container">
	<div id="datatable_ajax_2_wrapper"
		class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
		<div class="raw">
			<div class="col-md-6" style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
				<div class="portlet box grey-salt  ">
                        <div class="portlet-title" style="background-color: #2B3643;">
                            <div class="caption">
                                <i class="fa fa-search"></i>검색</div>
                            <div class="tools">
                                <a href="" class="collapse"> </a>
                            </div>
                        </div>
                        <div class="portlet-body form" >
                            <div class="form-body" style="padding-left:20px; padding-right:10px;">
                                <div class="form-group">
                                    <form id="patchForm" method="POST" class="mt-repeater form-horizontal">
                                    	<input type="hidden" name="tab_flag" value="1"/>
                                        <div data-repeater-list="group-a">
                                        <br>
                                            <div data-repeater-item class="mt-repeater-item">
                                                <div class="mt-repeater-input">
                                                    <label class="control-label">기간</label>
                                                    <div class="row">
														<div class="col-md-1">
															<select class="form-control input-small" id="select_year"
																name="select_year">
																<c:forEach var="idx" begin="0" end="2">
																	<option value="${sel_year - (2-idx)}"
																		<c:if test="${(sel_year - (2-idx)) == sel_year}">selected</c:if>>${sel_year - (2-idx)}
																		년</option>
																</c:forEach>
															</select>
														</div>
														<div class="col-md-1">
															<select class="form-control input-small"
																id="patch_month" name="patch_month">
																<option value="all">전체</option>
																<c:forEach var="month" begin="1" end="12">
																	<option value="<fmt:formatNumber minIntegerDigits="2" value="${month}" />"
																		<c:if test="${month == paramBean.patch_month}">selected</c:if>>${month }
																		월</option>
																</c:forEach>
															</select>
														</div>
													</div>
												</div>
                                            </div>
                                        </div>
                                        <div class="row">
                                        <div class="col-sm-6">
                                        </div>
                                        <div class="col-sm-6" align="right">
										<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
											onclick="searchPatchStatus()">
											<i class="fa fa-search"></i> 검색
										</button>&nbsp;&nbsp;
<%-- 										<a onclick="deptmoveExcel()"><img src="${rootPath}/resources/image/icon/XLS_3.png"></a> --%>
										</div>
										</div>
							
									<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" /> 
									<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" /> 
									<input type="hidden" name="current_menu_id" value="${currentMenuId}" /> 
									<input type="hidden" name="select_tab"/>
<%-- 									<input type="hidden" id="rootPath" name="rootPath" value="${rootPath}" />  --%>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
			</div>
		</div>
		<div class="raw">
			<h4>SW업그레이드</h4>
			<table style="border-top: 1px solid #e7ecf1;"
				class="table table-striped table-bordered table-checkable dataTable no-footer"
				role="grid">
				<colgroup>
					<col width="20%" />
					<col width="20%" />
					<col width="20%" />
					<col width="20%" />
					<col width="20%" />
				</colgroup>
				<thead>
					<tr>
						<th scope="col" style="vertical-align: middle; text-align: center;">구분</th>
						<th scope="col" style="vertical-align: middle; text-align: center;">버전</th>
						<th scope="col" style="vertical-align: middle; text-align: center;">작업일</th>
						<th scope="col" style="vertical-align: middle; text-align: center;">관련스크립트</th>
						<th scope="col" style="vertical-align: middle; text-align: center;">주요내용</th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${empty patchHist_sw}">
							<tr>
								<td colspan="5" align="center">데이터가 없습니다.</td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach items="${patchHist_sw}" var="item" >
								<tr>
									<td>${item.patch_type }</td>
									<td>${item.patch_version }</td>
									<td>${item.patch_date }</td>
									<td>${item.script }</td>
									<td>${item.description }</td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
		</div>
		<div class="raw">
			<h4>DATA업그레이드</h4>
			<table style="border-top: 1px solid #e7ecf1;"
				class="table table-striped table-bordered table-checkable dataTable no-footer"
				role="grid">
				<colgroup>
					<col width="20%" />
					<col width="20%" />
					<col width="20%" />
					<col width="20%" />
					<col width="20%" />
				</colgroup>
				<thead>
					<tr>
						<th scope="col" style="vertical-align: middle; text-align: center;">구분</th>
						<th scope="col" style="vertical-align: middle; text-align: center;">버전</th>
						<th scope="col" style="vertical-align: middle; text-align: center;">작업일</th>
						<th scope="col" style="vertical-align: middle; text-align: center;">관련스크립트</th>
						<th scope="col" style="vertical-align: middle; text-align: center;">주요내용</th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${empty patchHist_data}">
							<tr>
								<td colspan="5" align="center">데이터가 없습니다.</td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach items="${patchHist_data}" var="item" >
								<tr>
									<td>${item.patch_type }</td>
									<td>${item.patch_version }</td>
									<td>${item.patch_date }</td>
									<td>${item.script }</td>
									<td>${item.description }</td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
		</div>
		<div class="raw">
			<h4>MAPPING업그레이드</h4>
			<table style="border-top: 1px solid #e7ecf1;"
				class="table table-striped table-bordered table-checkable dataTable no-footer"
				role="grid">
				<colgroup>
					<col width="20%" />
					<col width="20%" />
					<col width="20%" />
					<col width="20%" />
					<col width="20%" />
				</colgroup>
				<thead>
					<tr>
						<th scope="col" style="vertical-align: middle; text-align: center;">구분</th>
						<th scope="col" style="vertical-align: middle; text-align: center;">버전</th>
						<th scope="col" style="vertical-align: middle; text-align: center;">작업일</th>
						<th scope="col" style="vertical-align: middle; text-align: center;">관련스크립트</th>
						<th scope="col" style="vertical-align: middle; text-align: center;">주요내용</th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${empty patchHist_mapping}">
							<tr>
								<td colspan="5" align="center">데이터가 없습니다.</td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach items="${patchHist_mapping}" var="item" >
								<tr>
									<td style="vertical-align: middle; text-align: center;">${item.patch_type }</td>
									<td style="vertical-align: middle; text-align: center;">${item.patch_version }</td>
									<td style="vertical-align: middle; text-align: center;">${item.patch_date }</td>
									<td style="vertical-align: middle; text-align: center;">${item.script }</td>
									<td style="vertical-align: middle; text-align: center;">${item.description }</td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
		</div>
	</div>
</div>