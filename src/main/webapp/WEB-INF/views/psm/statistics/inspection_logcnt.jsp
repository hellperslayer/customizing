<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="now" value="<%=new java.util.Date()%>" />
<c:set var="nowDate">
		<fmt:formatDate value="${now}" pattern="yyyy년 MM월" />
</c:set>
<c:set var="nowYear">
		<fmt:formatDate value="${now}" pattern="yyyy" />
</c:set>
<c:set var="nowMonth">
		<fmt:formatDate value="${now}" pattern="MM" />
</c:set>
<div class="table-container">
	<div id="datatable_ajax_2_wrapper"
		class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
		<div class="raw">
			<div class="col-md-6" style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
				<div class="portlet box grey-salt  ">
                        <div class="portlet-title" style="background-color: #2B3643;">
                            <div class="caption">
                                <i class="fa fa-search"></i>검색</div>
                            <div class="tools">
                                <a href="" class="collapse"> </a>
                            </div>
                        </div>
                        <div class="portlet-body form" >
                            <div class="form-body" style="padding-left:20px; padding-right:10px;">
                                <div class="form-group">
                                    <form id="listForm" method="POST" class="mt-repeater form-horizontal">
                                    	<input type="hidden" name="tab_flag" value="1"/>
                                        <div data-repeater-list="group-a">
                                        <br>
                                            <div data-repeater-item class="mt-repeater-item">
                                                <!-- jQuery Repeater Container -->
                                                <div class="mt-repeater-input">
                                                    <label class="control-label">로그 수집 기간</label>
                                                    <br/>
                                                    <div class="input-group input-medium date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
													<input type="text" class="form-control" id="search_fr"
														name="search_from"
														value="<procdate:convertDate value="${paramBean.search_from}" />"> <span
														class="input-group-addon"> &sim; </span> <input type="text"
														class="form-control" id="search_to" name="search_to"
														value="<procdate:convertDate value="${paramBean.search_to}" />">
													</div> 
												</div>
											 
                                               <%--  <div class="mt-repeater-input">
                                                    <label class="control-label">접속유형</label>
                                                    <br/>
	                                                <select id="check_type" name="check_type" class="form-control input-medium">
														<option value=""
															<c:if test="${paramBean.check_type == '' }">selected</c:if>>ID 로그인</option>
														<option value="system"
															<c:if test="${paramBean.check_type == 'system' }">selected</c:if>>시스템 로그인</option>
													</select>
												</div> --%>
                                                <div class="mt-repeater-input">
                                                    <label class="control-label">보고서 작성 월</label>
                                                    <br/>
	                                                <div class="input-group">
		                                                <select id="proc_year" name="proc_year" class="form-control" style="display: inline; width: 150px">
															<c:forEach begin="0" end="4" step="1" var="year">
																<option value="${nowYear - year }">${nowYear - year }</option>
															</c:forEach>
														</select>
		                                                <select id="proc_month" name="proc_month" class="form-control" style="display: inline; width: 150px">
		                                                	<c:forEach begin="01" end="12" step="1" var="month">
																<option value="${month}" <c:if test="${month eq nowMonth }">selected</c:if> >${month}월</option>
															</c:forEach>
														</select>
													</div>
												</div>
                                            </div>
                                        </div>
                                        <div class="row">
                                        <div class="col-sm-6">
                                        	<button type="button" class="btn btn-sm green btn-outline sbold uppercase"
											onclick="createReport()">
											<i class="fa fa-plus"></i> 
											<font>점검보고서 생성</font>
										</button>
                                        </div>
                                        <div class="col-sm-6" align="right">
                                       	<button type="reset" class="btn btn-sm red-mint btn-outline sbold uppercase"
											onclick="resetOptions(inspectionConfig['listUrl'])">
											<i class="fa fa-remove"></i> <font>초기화</font>
										</button>
										<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
											onclick="searchInspection()">
											<i class="fa fa-search"></i> 검색
										</button>&nbsp;&nbsp;
<%-- 										<a onclick="deptmoveExcel()"><img src="${rootPath}/resources/image/icon/XLS_3.png"></a> --%>
										</div>
										</div>
							
									<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" /> 
									<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" /> 
									<input type="hidden" name="current_menu_id" value="${currentMenuId}" /> 
<%-- 									<input type="hidden" id="rootPath" name="rootPath" value="${rootPath}" />  --%>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
			</div>
		</div>
		<div class="raw">
			<table style="border-top: 1px solid #e7ecf1;"
				class="table table-striped table-bordered table-checkable dataTable no-footer"
				role="grid">
				<colgroup>
					<col width="10%" />
					<col width="70" />
					<col width="20%" />
				</colgroup>
				<thead>
					<tr>
						<th scope="col" style="vertical-align: middle; text-align: center;">시스템코드</th>
						<th scope="col" style="vertical-align: middle; text-align: center;">시스템명</th>
						<th scope="col" style="vertical-align: middle; text-align: center;">처리량</th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${empty inspection}">
							<tr>
								<td colspan="3" align="center">데이터가 없습니다.</td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach items="${inspection}" var="item">
								<tr>
									<td style="vertical-align: middle; text-align: center;">${item.system_seq}</td>
									<td style="vertical-align: middle; text-align: center;">${item.system_name}</td>
									<td style="vertical-align: middle; text-align: center;"><fmt:formatNumber value="${item.type_sum}" type="number" /></td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
		</div>
	</div>
</div>