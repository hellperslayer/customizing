<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="sqlMappSetupAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/setup/sqlMappSetupList.js"
	type="text/javascript" charset="UTF-8"></script>
<h1 class="page-title">${currentMenuName}</h1>
<!-- 그룹코드 신규 -->
<div class="portlet light portlet-fit bordered">
	<div class="portlet-body">
		<!-- BEGIN FORM-->
			<form id="sqlMappSetupDetailForm" action="" method="POST" class="form-horizontal form-bordered form-row-stripped">
			<input type="hidden" name="tag_sql_url_seq" value="${sqlMappSetupDetail.tag_sql_url_seq }" />
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<tr>
							<th style="text-align: center;"><label class="control-label">URL<span
									class="required">*</span></label></th>
							<td colspan="3" class="form-group form-md-line-input" style="text-align: left;" >
								<textarea class="form-control" rows="7" style="width: 100%;" readonly="readonly">${sqlMappSetupDetail.url}</textarea>
							</td>
						</tr>
						<tr>
							<%-- <th style="width: 15%; text-align: center;"><label
								class="control-label">사용여부 </label></th>
							<td style="width: 35%;" class="form-group form-md-line-input">
								<div class="md-radio-inline">
									<div class="md-radio col-md-4">
										<input type="radio" id="checkbox1_1" class="md-radiobtn"
											id="form_control_2" title="사용" value="Y"
											${fn:containsIgnoreCase(sqlMappSetupDetail.use_yn,'Y')?'checked':'' } onclick="return(false);">
										<label for="checkbox1_1"> <span></span> <span
											class="check"></span> <span class="box"></span> 사용
										</label>
									</div>
									<div class="md-radio col-md-4">
										<input type="radio" id="checkbox1_2" class="md-radiobtn"
											id="form_control_2" title="미사용" value="N" 
											${!fn:containsIgnoreCase(sqlMappSetupDetail.use_yn,'Y')?'checked':'' } onclick="return(false);">
										<label for="checkbox1_2"> <span></span> <span
											class="check"></span> <span class="box"></span> 미사용
										</label>
									</div>
								</div>
							</td> --%>
							<th style="width: 15%; text-align: center;"><label 
								class="control-label">시스템명</label></th>
							<td style="vertical-align:middle">${sqlMappSetupDetail.system_name}</td>
						</tr>
					</tr>
				</tbody>
			</table>
			
			<table class="table table-bordered table-striped">
				<colgroup>
					<col width="10%"/>
					<col width="80%"/>
					<col width="10%"/>
				</colgroup>
				<tbody>
					<tr>
						<td align="center">번호</td>
						<td align="center">SQL</td>
						<td align="center">수행업무</td>
					</tr>
					
					<c:forEach items="${sqlMappSetupDetailSql }" var="sqlMappSetupDetailSql" varStatus="status">
					<tr>
						<td style="text-align: center;">${status.count }</td>
						<td colspan="" class="form-group form-md-line-input" style="text-align: left;" >
						<textarea class="form-control" rows="7" style="width: 100%;" readonly="readonly" id="sqlText">${sqlMappSetupDetailSql.sql}</textarea></td>
						<c:choose>
                   			<c:when test="${sqlMappSetupDetailSql.crud_type == 'RD'}"><td align="center">조회</td></c:when>
                   			<c:when test="${sqlMappSetupDetailSql.crud_type == 'RM'}"><td align="center">삭제</td></c:when>
                   			<c:when test="${sqlMappSetupDetailSql.crud_type == 'UD'}"><td align="center">수정</td></c:when>
                   			<c:otherwise><td align="center">생성</td></c:otherwise>
                   		</c:choose>
					</tr>
					</c:forEach>
				</tbody>
			</table>

			<!-- 메뉴 관련 input 시작 -->
			<input type="hidden" name="main_menu_id"
				value="${paramBean.main_menu_id }" /> <input type="hidden"
				name="sub_menu_id" value="${paramBean.sub_menu_id }" /> <input
				type="hidden" name="current_menu_id" value="${currentMenuId }" />
			<!-- 메뉴 관련 input 끝 -->

			<!-- 페이지 번호 -->
			<input type="hidden" name="page_num" value="${search.page_num }" />

			<!-- 검색조건 관련 input 시작 -->
			<input type="hidden" name="system_seq_search"
				value="${search.system_seq_search}" /> <input type="hidden"
				name="url_search" value="${search.url_search }" />
			<!-- 검색조건 관련 input 끝 -->
		</form>
		<!-- END FORM-->

		<!-- 버튼 영역 -->
		<div class="form-actions">
			<div class="row">
				<div class="col-md-offset-2 col-md-10" align="right"
					style="padding-right: 30px;">
					<button type="button" class="btn btn-sm dark btn-outline sbold uppercase"
						onclick="movesqlMappSetupListtoDetail()">
						<i class="fa fa-list"></i> &nbsp;목록
					</button>
							<a class="btn btn-sm red-mint btn-outline sbold uppercase" onclick="removesqlMappSetup()"><i
								class="fa fa-remove"></i>&nbsp;삭제</a>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	var sqlMappSetupConfig = {
		"listUrl":"${rootPath}/sqlMappSetup/list.html"
		,"detailUrl":"${rootPath}/sqlMappSetup/detail.html"
		,"addUrl":"${rootPath}/sqlMappSetup/add.html"
		,"saveUrl":"${rootPath}/sqlMappSetup/save.html"
		,"removeUrl":"${rootPath}/sqlMappSetup/remove.html"
	};
	
</script>