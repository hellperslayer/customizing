<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="extrtBaseSetupAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/setup/extrtBaseSetupDetail_indv.js" type="text/javascript" charset="UTF-8"></script>
<link href="${rootPath}/resources/css/profile.min.css" rel="stylesheet" type="text/css">


<h1 class="page-title"> ${currentMenuName}
    <!--bold font-blue-hoki <small>basic bootstrap tables with various options and styles</small> -->
</h1>

<div class="contents left">
 		

<div class="row" style="background:#eef1f5; padding-top: 20px; padding-bottom: 20px">
	<%-- <input type="hidden" id="emp_user_id" name="emp_user_id" value="${search.emp_user_id}"> --%>
	<div class="col-md-12">
		<!-- BEGIN PROFILE SIDEBAR -->
		<div class="profile-sidebar">
			<!-- PORTLET MAIN -->
			<div class="portlet light profile-sidebar-portlet "style=height:147%">
				<!-- SIDEBAR USERPIC -->
				<div class="profile-userpic" style="margin-bottom: -5px; ">
					<img src="${rootPath}/resources/image/common/profile_user2.png" class="img-responsive" alt="profile">
				</div>
				<!-- END SIDEBAR USERPIC -->
				<!-- SIDEBAR USER TITLE -->
				<div class="profile-usertitle">
					<div class="profile-usertitle-name"style="margin-bottom: 10px; ">${search.emp_user_name}</div>
					<%-- <div class="profile-usertitle-job"style="margin-bottom: 15px; ">(${data1.emp_user_id})</div> --%>
				</div>
				<!-- END SIDEBAR USER TITLE -->
				<!-- SIDEBAR BUTTONS -->
			</div>
			<!-- END PORTLET MAIN -->
			<!-- PORTLET MAIN -->
			<div class="portlet light" style="height: 770px;">
				<!-- STAT -->
				<div class="row">
					<h4 class="profile-desc-title" style="padding-bottom: 10px; ">프로필</h4>
					<div class="margin-top-20 profile-desc-link" style="margin-top: -20px !important;">
						<table class="table table-hover table-light">
	                        <tbody>
	                            <tr>
	                                <td> <b>소속</b> : ${search.dept_name} </td>
	                            </tr>
	                            <c:if test="${search.user_ip ne null}">
	                            <tr>
	                                <td> <b>IP</b> : ${search.user_ip}</td>
	                            </tr> 
	                            </c:if>
	                            <tr>
	                            	<td> <b>ID</b> : ${search.emp_user_id} </td>
	                            </tr>
	                        </tbody>
                        </table>
					</div>
				</div>
				<%-- <div class="row list-separated profile-stat">
				  <h4 class="profile-desc-title">업무 시스템</h4>
					<div class="tab-context">
						<div  class="clearfix">
							<c:forEach items="${data2}" var="system" varStatus="status">
								<c:if test="${system.system_name != null}">
									<a style="cursor:default" class="btn btn-xs blue"
										<c:if test="${status.count % 4 == 0 }"> class="btn btn-xs red"</c:if>
										<c:if test="${status.count % 4 == 1 }"> class="btn btn-xs yellow"</c:if>
										<c:if test="${status.count % 4 == 2 }"> class="btn btn-xs green"</c:if>
										<c:if test="${status.count % 4 == 3 }"> class="btn btn-xs blue"</c:if>							
										style="margin-top: 5px; margin-bottom: 5px;">${system.system_name}
										<i class="fa fa-laptop"></i>
			                       	 </a>
								</c:if>								
							</c:forEach>
	                    </div>
                    </div>
				</div> --%>
				<!-- END STAT -->
				<div class="row" style="margin-top: -15px !important;">					
					<%-- <br><br><div class="margin-top-20 profile-desc-link" style="margin-top: 2px !important;t">
						
						<c:if test="${data1.dng_grade == '심각' }"><i class="fa fa-globe"></i><span style="font-size: 15px;"><b style="color: red;">심각 단계</b>는 <b><br>위험도 높은 비정상 개인정보 처리가 다수 발생되어 전사 차원의 매우 심각한 수준의 손실이나 해당 업무의 실패 혹은 경쟁적 지위를 상실할 수 있는 단계입니다.</b></span> </c:if>
						<c:if test="${data1.dng_grade == '경계' }"><i class="fa fa-globe"></i><span style="font-size: 15px;"><b style="color: orange;">경계 단계</b>는 <b><br>다수의 비정상 개인정보 처리가 발생되어 매우 심각한 손실이나 해당 업무가 어려워질 수 있는 정도로 업무에 중대한 영향이 발생할 수 있는 단계입니다.</b></span></c:if>
						<c:if test="${data1.dng_grade == '주의' }"><i class="fa fa-globe"></i><span style="font-size: 15px;"><b style="color: yellow;">주의 단계</b>는 <b><br>여러 항목의 비정상 개인정보 처리가 발생되어 손실이나 해당 업무에 부정적인 영향을 줄 수 있는 정도의 문제가 발생할 수 있는 단계입니다. </b></span></c:if>
						<c:if test="${data1.dng_grade == '관심' }"><i class="fa fa-globe"></i><span style="font-size: 15px;"><b style="color: blue;">관심 단계</b>는 <b><br>일부 비정상 개인정보 처리가 발생되어 해당영역 또는 해당서비스에 작은 영향을 줄 수 있을 정도의 문제가 발생할 수 있는 단계입니다. </b></span></c:if>
						<c:if test="${data1.dng_grade == '정상' }"><i class="fa fa-globe"></i><span style="font-size: 15px;"><b style="color: green;">정상 단계</b>는 <b><br>업무 수행 상 허용된 범위 내에 개인정보 처리가 발생되어 업무적으로 영향이 거의 없는 정도로 파급효과가 미약한 단계입니다.</b></span></c:if>
					</div> --%>
				</div>
			</div>
			<!-- END PORTLET MAIN -->
		</div>
		<!-- END BEGIN PROFILE SIDEBAR -->
		<!-- BEGIN PROFILE CONTENT -->
		<div class="profile-content">
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light" style="height: 1020px;">
						<!-- <div class="portlet-title tabbable-line">
							<div class="caption caption-md">
								<i class="icon-globe theme-font hide"></i> <span class="caption-subject font-blue-madison bold uppercase">Profile
									Account</span>
							</div>
						</div> -->
						<div class="portlet-title">
					        <div class="caption">
					            <i class="icon-settings font-dark"></i>
					            <span class="caption-subject font-dark sbold uppercase">개인별 위험도 추출조건</span>
					        </div>
					    </div>
						
						<form id="extrtBaseSetupDetailForm" action="" method="POST">
						<div class="portlet-body">
							<div class="row">
								<div class="col-md-12">
									
									<table class="table table-bordered table-striped">
										<thead>
											<tr role="row" class="heading">
												<th width="10%"
													style="border-bottom: 1px solid #e7ecf1; text-align: center;">
													No.</th>
												<th width="50%"
													style="border-bottom: 1px solid #e7ecf1; text-align: center;">
													추출조건명</th>
												<th width="10%"
													style="border-bottom: 1px solid #e7ecf1; text-align: center;">
													평균(건수)</th>
												<th width="10%"
													style="border-bottom: 1px solid #e7ecf1; text-align: center;">
													비율(단위:%)</th>
												<th width="10%"
													style="border-bottom: 1px solid #e7ecf1; text-align: center;">
													결과</th>
												<th width="10%"
													style="border-bottom: 1px solid #e7ecf1; text-align: center;">
													사용여부</th>
												<!-- <th width="10%"
													style="border-bottom: 1px solid #e7ecf1; text-align: center;">
													적용</th>
												<th width="10%"
													style="border-bottom: 1px solid #e7ecf1; text-align: center;">
													해제</th> -->
											</tr>
										</thead>
										<tbody>
											<c:choose>
												<c:when test="${empty extrtBaseSetupDetail}">
													<tr>
														<td colspan="6" align="center">데이터가 없습니다.</td>
													</tr>
												</c:when>
												<c:otherwise>
													<c:set var="totalcnt" value="0"/>
													<c:forEach items="${extrtBaseSetupDetail}" var="extrtBaseSetup" varStatus="status">
														<tr>
															<input type="hidden" name="rule_seq_${status.index }" value="${extrtBaseSetup.rule_seq }"/>
															<td style="text-align: center; vertical-align: middle;">${status.count}</td>
															<td style="text-align: left; vertical-align: middle;padding-left: 20px;"><c:out value="${extrtBaseSetup.rule_nm}"/></td>
															<td style="text-align: center; vertical-align: middle;"><c:out value="${extrtBaseSetup.avg_val}"/>건</td>
															<td style="text-align: center; vertical-align: middle;">
																<input type="text" style="display: inline;" class="form-control input-xsmall" 
																name="prct_val_${status.index }" value="${extrtBaseSetup.prct_int }" onkeydown="onlyNumber()"/>
															</td>
															<td style="text-align: center; vertical-align: middle;">${extrtBaseSetup.dng_val }</td>
															<td style="text-align: center; vertical-align: middle;">
																<select class="form-control input-small" style="display: inline;" name="use_yn_${status.index }">
																	<option value="Y" <c:if test="${extrtBaseSetup.use_yn eq 'Y' }">selected="selected"</c:if>>사용</option>
																	<option value="N" <c:if test="${extrtBaseSetup.use_yn eq 'N' }">selected="selected"</c:if>>미사용</option>
																</select>
															</td>
															<%-- <td style="text-align: center; vertical-align: middle;"><a class="btn btn-sm blue btn-outline sbold uppercase" onclick="setSetup('${search.emp_user_id}', ${extrtBaseSetup.rule_seq}, ${status.index })">적용</a></td>
															<td style="text-align: center; vertical-align: middle;"><a class="btn btn-sm red-mint btn-outline sbold uppercase">해제</a></td> --%>
														</tr>
														<c:set var="totalcnt" value="${totalcnt + 1 }" />
													</c:forEach>
												</c:otherwise>
											</c:choose>
										</tbody>
									</table>
									
									<!-- 메뉴 관련 input 시작 -->
									<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" /> 
									<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" /> 
									<input type="hidden" name="current_menu_id" value="${currentMenuId }" />
									<!-- 메뉴 관련 input 끝 -->
									
									<input type="hidden" name="totalcnt" value="${totalcnt }" />
									<input type="hidden" name="emp_user_id" value="${search.emp_user_id}" />
									</div>
							</div>
				
							<div class="row">
								<div class="col-md-12" align="right">
									<!-- 버튼 영역 -->
									<div class="option_btn right" style="padding-right: 10px;">
										<p class="right">
										
											<a class="btn btn-sm grey-mint btn-outline sbold uppercase"
											onclick="javascript:goList();"><i class="fa fa-list"></i>
											목록</a>
											
											<a class="btn btn-sm blue btn-outline sbold uppercase"
											onclick="javascript:saveDngVal();"><i class="fa fa-check"></i>
											수정</a>
										</p>
									</div>
								</div>
							</div>
						</div>
						</form>
					    
					</div>
				</div>
			</div>
		</div>
		<!-- END PROFILE CONTENT -->
	</div>
</div>

</div>
<!-- END CONTENT BODY -->

<script type="text/javascript">
	var extrtBaseSetupConfig = {
		"listUrl" : "${rootPath}/extrtBaseSetup/indvlist.html",
		"saveUrl" : "${rootPath}/extrtBaseSetup/setindv.html",
		"removeUrl" : "${rootPath}/extrtBaseSetup/remove.html"
	};
</script>


