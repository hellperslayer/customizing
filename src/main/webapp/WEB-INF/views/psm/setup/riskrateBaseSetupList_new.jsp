<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />
<script src="${rootPath}/resources/js/common/ezDatepicker.js"
	type="text/javascript" charset="UTF-8"></script>
<script
	src="${rootPath}/resources/js/psm/setup/riskrateBaseSetupList.js"
	type="text/javascript" charset="UTF-8"></script>
<h1 class="page-title">${currentMenuName}</h1>
<div class="portlet light portlet-fit portlet-datatable bordered">
	<%-- <div class="portlet-title">
		<div class="caption">
			<i class="icon-settings font-dark"></i> <span
				class="caption-subject font-dark sbold uppercase">${currentMenuName}</span>
		</div>
	</div> --%>
	<div class="portlet-body">
		<div class="table-container">
			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper no-footer">
				<div class="portlet box"
					style="border: 1px; border-style: solid; border-color: #cacfd8;">
					<div class="portlet-title" style="background-color: #2B3643;">
						<div class="caption">
							&nbsp;위험도
						</div>
					</div>
					<form id="riskrateBaseSetupForm" method="POST">
						<div class="portlet-body">
							<div class="table-container" align="center">
							<br>
								<table class="table table-striped table-bordered table-hover table-checkable dataTable no-footer" style="width: 95%; border-color: #cacfd8;" role="grid">
									<colgroup>
										<col width="10%" />
										<col width="35%" />
										<col width="15%" />
										<col width="30%" />
										<col width="10%" />
									</colgroup>
									<thead>
										<tr>
											<th scope="col" style="text-align: center;">단계</th>
											<th scope="col" style="text-align: center;">등급기준</th>
											<th scope="col" style="text-align: center;">대응방안</th>
											<th scope="col" style="text-align: center;">내용</th>
											<th scope="col" style="text-align: center;">위험지수</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${riskrateBaseSetup}"
											var="riskrateBaseSetup" varStatus="status">
											<tr
												<c:if test="${status.count % 2 == 0 }"> class='tr_gray'</c:if>>
												<td class="start"
													style="height: 100px; vertical-align: middle; padding-right: 0em; padding-left: 0em; text-align: center;"
													id="dng_nm"><c:choose>
														<c:when test="${riskrateBaseSetup.dng_cd == '1'}">
															<img alt="심각"
																src="${pageContext.servletContext.contextPath}/resources/image/psm/setup/icon_statred.png"
																style="vertical-align: middle;" />
														</c:when>
														<c:when test="${riskrateBaseSetup.dng_cd == '2'}">
															<img alt="경계"
																src="${pageContext.servletContext.contextPath}/resources/image/psm/setup/icon_statorange.png"
																style="vertical-align: middle;" />
														</c:when>
														<c:when test="${riskrateBaseSetup.dng_cd == '3'}">
															<img alt="주의"
																src="${pageContext.servletContext.contextPath}/resources/image/psm/setup/icon_statyellow.png"
																style="vertical-align: middle;" />
														</c:when>
														<c:when test="${riskrateBaseSetup.dng_cd == '4'}">
															<img alt="관심"
																src="${pageContext.servletContext.contextPath}/resources/image/psm/setup/icon_statblue.png"
																style="vertical-align: middle;" />
														</c:when>
														<c:otherwise>
															<img alt="정상"
																src="${pageContext.servletContext.contextPath}/resources/image/psm/setup/icon_statgreen.png"
																style="vertical-align: middle; padding-right: 0em; padding-left: 0em; text-align: center;" />
														</c:otherwise>
													</c:choose> &nbsp; ${riskrateBaseSetup.dng_nm}</td>
												<td style="vertical-align: middle;"><c:choose>
														<c:when test="${riskrateBaseSetup.dng_cd == '1'}">
																위험도 높은 비정상 개인정보 처리가 다수 발생되어 전사 차원의 매우 심각한 수준의 손실이나 해당 업무의 실패 혹은 경쟁적 지위 상실
														</c:when>
														<c:when test="${riskrateBaseSetup.dng_cd == '2'}">
																다수의 비정상 개인정보 처리가 발생되어 매우 심각한 손실이나 해당 업무가 어려워질 수 있는 정도로 업무에 중대한 영향 발생
														</c:when>
														<c:when test="${riskrateBaseSetup.dng_cd == '3'}">
																여러 항목의 비정상 개인정보 처리가 발생되어 손실이나 해당 업무에 부정적인 영향을 줄 수 있는 정도의 문제 발생
														</c:when>
														<c:when test="${riskrateBaseSetup.dng_cd == '4'}">
																일부 비정상 개인정보 처리가 발생되어 해당영역 또는 해당서비스에 작은 영향을 줄 수 있을 정도의 문제 발생
														</c:when>
														<c:otherwise>
																업무 수행 상 허용된 범위 내에 개인정보 처리가 발생되어 업무적으로 영향이 거의 없는 정도로 파급효과가 미약
														</c:otherwise>
													</c:choose></td>
												<td style="vertical-align: middle;"><c:choose>
														<c:when test="${riskrateBaseSetup.dng_cd == '1'}">
																담당자 검토 후 조치 즉시 필요
														</c:when>
														<c:when test="${riskrateBaseSetup.dng_cd == '2'}">
																담당자의 확인 및 감독이 필요
														</c:when>
														<c:when test="${riskrateBaseSetup.dng_cd == '3'}">
																지속적인 모니터링 실시 필요
														</c:when>
														<c:when test="${riskrateBaseSetup.dng_cd == '4'}">
																분기별 모니터링 실시 필요
														</c:when>
														<c:otherwise>
																반기별 모니터링 실시 필요
														</c:otherwise>
													</c:choose></td>
												<td style="vertical-align: middle;"><c:choose>
														<c:when test="${riskrateBaseSetup.dng_cd == '1'}">
																정보의 유출,변조,파괴,서비스 중지로 업무수행 및 기관(기업) 이미지에 매우 심각한 위험
														</c:when>
														<c:when test="${riskrateBaseSetup.dng_cd == '2'}">
																정보의 유출,변조,파괴,서비스 중지로 업무수행과 기관(기업) 이미지에 매우 심각한 위험
														</c:when>
														<c:when test="${riskrateBaseSetup.dng_cd == '3'}">
																업무수행에 중대한 영향은 발생하지 않으나 관리적 조치가 필요한 상태
														</c:when>
														<c:when test="${riskrateBaseSetup.dng_cd == '4'}">
																업무수행에 위험이 발생하나 심각한 영향이 아니며 빠른 시간 내에 복구 가능
														</c:when>
														<c:otherwise>
																위험이 거의 발생하지 않으나 발생 시에도 업무에 미치는 영향이 미미함
														</c:otherwise>
													</c:choose></td>
												<td style="text-align: center; vertical-align: middle;">
													<input type="text" size="3" class="form-control input-xsmall" 
													style="text-align: center; display: inline;"
													maxlength="3" id='dng_val'
													name="dng_val_${riskrateBaseSetup.dng_cd}"
													value="${riskrateBaseSetup.dng_val}"
													onkeydown="onlyNumber()" />
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
							<br>
							<div style="padding-right: 10px;padding-bottom: 10px;">
								<div align="right">
									<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="saveDngVal()"><i
										class="fa fa-check"></i> 저장</a>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- 	</div> -->
<!-- </div> -->
<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
<!-- </div> -->
<script type="text/javascript">
	var riskrateBaseSetupConfig = {
		"saveUrl" : "${rootPath}/riskrateBaseSetup/save.html"
	};
</script>