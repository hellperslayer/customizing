<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/common/ezDatepicker.js"
	type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/setup/sqlMappSetupList.js"
	type="text/javascript" charset="UTF-8"></script>
<h1 class="page-title">${currentMenuName}</h1>
<script
	src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js"
	type="text/javascript"></script>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light portlet-fit portlet-datatable bordered">
			<div class="portlet-body">
				<div class="table-container">
					<div id="datatable_ajax_2_wrapper"
						class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
						<div class="raw">
							<div class="col-md-6"
								style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
								<div class="portlet box grey-salt  ">
		                            <div class="portlet-title" style="background-color: #2B3643;">
		                                <div class="caption">
		                                    <i class="fa fa-search"></i>검색 & 엑셀 </div>
	                                    <div class="tools">
						                 	<a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
							             </div>
		                            </div>
		                            <div id="searchBar" class="portlet-body form" >
		                                <div class="form-body" style="padding-left:10px; padding-right:10px;">
		                                    <div class="form-group">
		                                        <form id="listForm" method="POST" class="mt-repeater form-horizontal">
		                                            <div data-repeater-list="group-a">
		                                                <div class="row">
		                                                    <!-- jQuery Repeater Container -->
		                                                    <div class="col-md-2">
		                                                        <label class="control-label">시스템</label>
		                                                        <select name="system_seq_search" class="form-control">
																	<option value=""
																		<c:if test="${search.system_seq_search == null}">selected="selected"</c:if>>-----전 체-----</option>
																	<c:forEach items="${systemMasterList}" var="systemMasterList">
																		<option value="${systemMasterList.system_seq}"
																			<c:if test="${systemMasterList.system_seq == search.system_seq_search}">selected="selected"</c:if>>${systemMasterList.system_name}(${ systemMasterList.system_seq})</option>
																	</c:forEach>
																</select>
															</div>
		                                                    <div class="col-md-2">
		                                                        <label class="control-label">URL</label>
		                                                        <input class="form-control" type="text" name="url_search" value="${search.url_search}" />
		                                                    </div>
		                                                </div>
		                                            </div>
		                                            <hr/>
		                                            <div align="right">
			                                            <button type="reset" class="btn btn-sm red-mint btn-outline sbold uppercase"
															onclick="resetOptions(sqlMappSetupConfig['listUrl'])">
															<i class="fa fa-remove"></i> <font>초기화
														</button>
														<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
															onclick="movesqlMappSetupList()">
															<i class="fa fa-search"></i> 검색
														</button>&nbsp;&nbsp;
														<%-- <a onclick="excelMenuMappSetup('${search.total_count}')"><img src="${rootPath}/resources/image/icon/XLS_3.png"></a> --%>
														<div class="btn-group">
															<a href="javascript:;" data-toggle="dropdown">
																<img align="right" src="${rootPath}/resources/image/icon/XLS_3.png">
															</a>
															<ul class="dropdown-menu pull-right">
																<li><a data-toggle="modal" data-target="#myModal">업로드</a></li>
																<li><a onclick="excelSqlMappSetup('${search.total_count}')"> 다운로드 </a></li>
															</ul>
														</div>
													</div>
													
													<input type="hidden" name="tag_sql_url_seq" value="0" /> 
													<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" /> 
													<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" /> 
													<input type="hidden" name="current_menu_id" value="${currentMenuId}" /> 
													<input type="hidden" name="page_num" value="${search.page_num}" />
													<input type="hidden" name="isSearch" value="${paramBean.isSearch }" />
		                                        </form>
		                                    </div>
		                                </div>
		                            </div>
                        		</div>
							</div>
						</div>
						
						<div class="modal fade" id="myModal" role="dialog">
							<div class="modal-dialog" style="position: relative ;top: 30%; left: 0;">
								<!-- Modal content -->
								<div class="modal-content">
									<div class="modal-header" style="background-color: #32c5d2;">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">
											<b style="font-size: 13pt;">엑셀 업로드</b>
										</h4>
									</div>
									<div class="modal-body" style="background-color: #F9FFFF;">
										<form action="upload.html" method="post" id="fileForm"
											name="fileForm" enctype="multipart/form-data">
											<div>
												<table width="100%">
													<tr style="height: 40px">
														<td width="70%" style="vertical-align: middle;" align="center">
															<input class="fileUpLoad" type="file" id="file" name="file">
														</td>
														<td><img style="cursor: pointer;" name="submup"
															onclick="excelMappSetupUpload()"
															src="${rootPath}/resources/image/common/btn_exupload.gif"
															alt="엑셀업로드" title="엑셀업로드" /></td>
													</tr>
													<tr style="height: 40px">
														<td width="50%" style="vertical-align: middle;"><span
															class="downexam">&nbsp;&nbsp;&nbsp;※다운받은 양식으로 
															</br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 업로드하셔야 매뉴매핑 설정값이 등록됩니다.</span></td>
														<td><img class="btn_excel exceldown" onclick="exDown()" style="cursor: pointer;"
															src="${rootPath}/resources/image/common/formUp.png"
															alt="양식다운로드" title="양식다운로드" /></td>
													</tr>
												</table>
											</div>
											<input type="hidden" id="result" name="result" value="false" />
										</form>
									</div>
									<div class="modal-footer">
										<a class="btn btn-sm red" href="#" data-dismiss="modal"> <i
											class="fa fa-remove"></i> 닫기
										</a>
									</div>
								</div>
							</div>
						</div>
						
						<div class="raw">
							<table style="border-top: 1px solid #e7ecf1;"
								class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
								role="grid">
								<colgroup>
									<col width="15%" />
									<col width="38%" />
									<col width="20%" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col" style="text-align: center;">시스템</th>
										<th scope="col" style="text-align: center;">URL주소</th>
<!-- 										<th scope="col" style="text-align: center;">사용여부</th> -->
									</tr>
								</thead>
								<tbody>
									<c:choose>
										<c:when test="${empty sqlMappSetupList.sqlMappSetup}">
											<tr>
												<td colspan="4" align="center">데이터가 없습니다.</td>
											</tr>
										</c:when>
										<c:otherwise>
											<c:set value="${sqlMappSetupList.page_total_count}" var="count" />
											<c:forEach items="${sqlMappSetupList.sqlMappSetup}" var="sqlMappSetup" varStatus="status">
												<tr style='cursor: pointer;' onclick="javascript:fnsqlMappSetupDetail('${sqlMappSetup.tag_sql_url_seq}')">
													<td style="text-align: center;"><ctl:nullCv nullCheck="${sqlMappSetup.system_name}" /></td>
													<c:choose>
														<c:when test="${fn:length(sqlMappSetup.url) > 100}">
															<td style="text-align: left; padding-left: 20px;" title="${sqlMappSetup.url}">
																${fn:substring(sqlMappSetup.url, 0, 100)}...
															</td>
														</c:when>
														<c:otherwise>
															<td style="text-align: left; padding-left: 20px;" title="${sqlMappSetup.url}"><ctl:nullCv nullCheck="${sqlMappSetup.url}" /></td>
														</c:otherwise>
													</c:choose>
													<%-- <c:choose>
														<c:when test="${template_yesNo.options[sqlMappSetup.use_yn] == '사용'}">
															<td style="text-align: center; vertical-align: middle;"><span
																class="label label-sm label-success"><ctl:nullCv
																		nullCheck="${sqlMappSetup.use_yn ne null ? template_yesNo.options[sqlMappSetup.use_yn] : '-'}" /></span></td>
														</c:when>
														<c:otherwise>
															<td style="text-align: center; vertical-align: middle;"><span
																class="label label-sm label-warning "><ctl:nullCv
																		nullCheck="${sqlMappSetup.use_yn ne null ? template_yesNo.options[sqlMappSetup.use_yn] : '-'}" /></span></td>
														</c:otherwise>
													</c:choose> --%>
												</tr>
												<c:set var="count" value="${count - 1 }" />
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
							<!-- 페이징 영역 -->
							<c:if test="${search.total_count > 0}">
								<div id="pagingframe" align="center">
									<p>
										<ctl:paginator currentPage="${search.page_num}"
											rowBlockCount="${search.size}"
											totalRowCount="${search.total_count}" />
									</p>
								</div>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form action="exdownload.html" method="post" id="exdown" name="exdown"
enctype="multipart/form-data"></form>
<script type="text/javascript">
	var sqlMappSetupConfig = {
		"listUrl" : "${rootPath}/sqlMappSetup/list.html",
		"detailUrl" : "${rootPath}/sqlMappSetup/detail.html",
		"downloadUrl" : "${rootPath}/sqlMappSetup/download.html"
	};
</script>
