<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/setup/referenceManagerList.js" type="text/javascript" charset="UTF-8"></script>
<h1 class="page-title">${currentMenuName}</h1>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light portlet-fit portlet-datatable bordered">
			<div class="portlet-body">
				<div class="table-container">
					<div id="datatable_ajax_2_wrapper"
						class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
						<div class="raw">
							<div class="col-md-6"
								style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
								
								<div class="portlet box grey-salt  ">
			                         <div class="portlet-title" style="background-color: #2B3643;">
			                             <div class="caption">
			                                 <i class="fa fa-search"></i>검색 & 엑셀</div>
			                              <div class="tools">
							                 <a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
							             </div>
			                         </div>
			                         <div id="searchBar" class="portlet-body form" >
			                             <div class="form-body" style="padding-left:20px; padding-right:10px;">
			                                 <div class="form-group">
			                                     <form id="listForm" method="POST" class="mt-repeater form-horizontal">
			                                         <div data-repeater-list="group-a">
			                                             <div class="row">
			                                                 <div class="col-md-2">
			                                                     <label class="control-label">테이블명</label>
			                                                     <select name="table_name" id="table_name" class="ticket-assign form-control" onchange="selColum()">
			                                                     	<option value="">--선택안함--</option>
																	<c:forEach items="${referenceManagerList}"
																		var="referenceManagerList" varStatus="status">
																		<option value="${referenceManagerList.table_name}"
																			<c:if test='${referenceManagerList.table_name eq search.table_name}'>selected="selected"</c:if>>${referenceManagerList.table_name}${referenceManagerList.kor_tablename}</option>
																	</c:forEach>
																</select>
															</div>
															<div class="col-md-2">
			                                                     <label class="control-label">검색 컬럼</label>
			                                                     <select name="column_name" id="column_name" class="ticket-assign form-control ">
																	<option value="">--선택안함--</option>
																	<c:forEach items="${column_nameList}" var="columnName">
																		<option value="${columnName}" <c:if test="${columnName == search.column_name}">selected="selected"</c:if>>${columnName}</option>
																	</c:forEach>
																</select> 
															</div>
															<div class="col-md-2">
			                                                     <label class="control-label">검색 데이터  </label>
			                                                     <input type="text" name="column_data" id="column_data" class="ticket-assign form-control " style="text-align: left"; value="${search.column_data}" />
															</div>
			                                             </div>
			                                         </div>
			                                         <hr/>
			                                         <div align="right">
		                                          	<button type="reset"
															class="btn btn-sm red-mint btn-outline sbold uppercase"
															onclick="resetOptions(referenceManagerConfig['listUrl'])">
															<i class="fa fa-remove"></i> <font>초기화
														</button> 
														<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
															onclick="javascript:moveColumnDetailList()">
															<i class="fa fa-search"></i> 검색
														</button>&nbsp;&nbsp;
													</div>
											
													<input type="hidden" name="privacy_seq" value="" />
													<input type="hidden" name="item_values" value="" />
													<input type="hidden" name="type" value="" />
													<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" /> 
													<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" /> 
													<input type="hidden" name="current_menu_id" value="${currentMenuId}" /> 
													<input type="hidden" name="page_num" value="${search.page_num}" />
													<input type="hidden" name="isSearch" value="${paramBean.isSearch }"/>
													<input type="hidden" name="detail_search_datas" value="" />
													
			                                     </form>
			                                 </div>
			                             </div>
			                         </div>
			                     </div>
							</div>
						</div>
						<c:if test="${adminUserAuthId eq 'AUTH00000'}">
<!-- 						<div class="row">
							<div class="col-md-12" align="left">
								<a class="btn btn-sm blue btn-outline sbold uppercase"
									onclick="fnReferenceManagerTableInsert('insert')">신규 <i class="fa fa-plus"></i>
								</a>
							</div>
						</div>  -->
						</c:if>
						<div class="raw">
							<table style="border-top: 1px solid #e7ecf1;"
								class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
								role="grid" width="100%">
								
									<c:choose>
										<c:when test="${empty referenceManagerDetailList}">
											<tr>
												<td colspan="4" align="center">데이터가 없습니다.</td>
											</tr>
										</c:when>
										<c:otherwise>
										<%-- 
										<colgroup>
											<!-- 컬럼 명으로 1줄씩 뿌려준다. foreach 로 개수만큼 출력 -->
											<c:forEach items="${referenceManagerDetailList}" var="referenceManagerDetailList" varStatus="status">
												<c:if test="${status.first }">
													<c:forEach var = "item" items="${referenceManagerDetailList }">
														<col width="20%" />
													</c:forEach>
												</c:if>
											</c:forEach>
										</colgroup>
										 --%>
										<tr>
										<c:forEach items="${column_nameList}" var="columnName">
											<th scope="col" style="text-align: center; vertical-align: middle;">${columnName}</th>
										</c:forEach>
										</tr>
										<c:forEach items="${referenceManagerDetailList}" var="dataMap" varStatus="status">
											<tr style='cursor: pointer;' onclick="javascript:fnReferenceManagerDetail('${dataMap['pKeyDatas'] }')">
												<c:forEach items="${column_nameList}" var="columnName">
													<td scope="col" style="text-align: center; vertical-align: middle;">${dataMap[columnName] }</td>
												</c:forEach>
											</c:forEach>		
										</c:otherwise>
									</c:choose>
							</table>
							<!-- 페이징 영역 -->
							<c:if test="${search.total_count > 0}">
								<div class="page left" id="pagingframe" align="center">
									<p>
										<ctl:paginator currentPage="${search.page_num}"
											rowBlockCount="${search.size}"
											totalRowCount="${search.total_count}" />
									</p>
								</div>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var referenceManagerConfig = {
		"listUrl" : "${rootPath}/indvinfoTypeSetup/referenceManagerDetailList.html",
		"detailUrl" : "${rootPath}/indvinfoTypeSetup/referenceManagerDetail.html",
		"downloadUrl" : "${rootPath}/indvinfoTypeSetup/download.html",
		"menu_id" : "${currentMenuId}"
	};

	var menu_id = referenceManagerConfig["menu_id"];
	var log_message = "SUCCESS";
	var log_action = "SELECT";
</script>