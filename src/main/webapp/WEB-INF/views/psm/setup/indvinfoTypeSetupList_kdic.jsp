<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/setup/indvinfoTypeSetupList_kdic.js" type="text/javascript" charset="UTF-8"></script>
<h1 class="page-title">${currentMenuName}</h1>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light portlet-fit portlet-datatable bordered">

			<div class="portlet-body">
				<div class="table-container">
					<div id="datatable_ajax_2_wrapper"
						class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
						<div class="row">
							<div class="col-md-6"
								style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
								
								<div class="portlet box grey-salt  ">
			                         <div class="portlet-title" style="background-color: #2B3643;">
			                             <div class="caption">
			                                 <i class="fa fa-search"></i>검색 </div>
			                              <div class="tools">
							                 <a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
							             </div>
			                         </div>
			                         <div id="searchBar" class="portlet-body form" >
			                             <div class="form-body" style="padding-left:20px; padding-right:10px;">
			                                 <div class="form-group">
			                                     <form id="listForm" method="POST" class="mt-repeater form-horizontal">
			                                         <div data-repeater-list="group-a">
			                                             <div class="row">
			                                                 <div class="col-md-3">
			                                                     <label class="control-label">DB명</label><br>
			                                                     <input type="text" name="db_name" class="ticket-assign form-control">
															</div>
			                                                 <div class="col-md-3">
			                                                     <label class="control-label">테이블명</label><br>
			                                                     <input type="text" name="table_name" class="ticket-assign form-control">
															</div>
			                                             </div>
			                                         </div>
			                                         <hr/>
			                                         <div align="right">
														<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
															onclick="javascript:moveindvinfoTypeSetupList()">
															<i class="fa fa-search"></i> 검색
														</button>&nbsp;&nbsp;
														<%-- <a onclick="excelIndvinfoTypeSetup('${search.total_count}')"><img src="${rootPath}/resources/image/icon/XLS_3.png"></a> --%>
													</div>
											
													<input type="hidden" name="privacy_table_info_seq" value="" /> 
													<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" /> 
													<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" /> 
													<input type="hidden" name="current_menu_id" value="${currentMenuId}" /> 
													<input type="hidden" name="page_num" value="${search.page_num}" />
													<input type="hidden" name="isSearch" value="${paramBean.isSearch }"/>
													<input type="hidden" name="edited" value/>
			                                     </form>
			                                 </div>
			                             </div>
			                         </div>
			                     </div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12" align="left">
								<a class="btn btn-sm blue btn-outline sbold uppercase"
									onclick="fnindvinfoTypeSetupDetail('')">신규 <i class="fa fa-plus"></i></a>
							</div>
						</div>
						<div class="raw">
							<table style="border-top: 1px solid #e7ecf1;"
								class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
								role="grid">
								<colgroup>
									<col width="50%" />
									<col width="50%" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col" style="text-align: center; vertical-align: middle;">DB명</th>
										<th scope="col" style="text-align: center; vertical-align: middle;">테이블명</th>
									</tr>
								</thead>
								<tbody>
									<c:choose>
										<c:when test="${empty privacyTableList}">
											<tr>
												<td colspan="2" align="center">데이터가 없습니다.</td>
											</tr>
										</c:when>
										<c:otherwise>
											<c:forEach items="${privacyTableList}" var="privacyTable" varStatus="status">
												<tr style='cursor: pointer;'
													onclick="javascript:fnindvinfoTypeSetupDetail_kdic('${privacyTable.privacy_table_info_seq}')">
													<td>
														${privacyTable.db_name}
													</td>
													<td style="text-align: center">
														${privacyTable.table_name}
													</td>
												</tr>
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
							<!-- 페이징 영역 -->
							<c:if test="${search.total_count > 0}">
								<div class="page left" id="pagingframe" align="center">
									<p>
										<ctl:paginator currentPage="${search.page_num}"
											rowBlockCount="${search.size}"
											totalRowCount="${search.total_count}" />
									</p>
								</div>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var indvinfoTypeSetupConfig = {
		"listUrl" : "${rootPath}/indvinfoTypeSetup/list_kdic.html",
		"detailUrl" : "${rootPath}/indvinfoTypeSetup/detail_kdic.html",
		"downloadUrl" : "${rootPath}/indvinfoTypeSetup/download.html",
		"menu_id" : "${currentMenuId}"
	};

	var menu_id = indvinfoTypeSetupConfig["menu_id"];
	var log_message = "SUCCESS";
	var log_action = "SELECT";
</script>