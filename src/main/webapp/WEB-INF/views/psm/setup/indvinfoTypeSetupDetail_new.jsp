<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/setup/indvinfoTypeSetupList.js" type="text/javascript" charset="UTF-8"></script>
<h1 class="page-title">
	${currentMenuName}
	<!--bold font-blue-hoki <small>basic bootstrap tables with various options and styles</small> -->
</h1>
<div class="portlet light portlet-fit bordered">
	<div class="portlet-body">
		<!-- BEGIN FORM-->
		<form id="indvinfoTypeSetupDetailForm" action="" method="POST"
			class="form-horizontal form-bordered form-row-stripped">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<th style="width: 15%; text-align: center;"><label class="control-label">번호 </label></th>
						<td style="width: 35%; vertical-align: middle;" class="form-group form-md-line-input">
						<c:choose>
							<c:when test="${indvinfoTypeSetup.privacy_seq ne null}">
								${indvinfoTypeSetup.privacy_seq} 
								<input type="hidden" name="privacy_seq" type="text" value="${indvinfoTypeSetup.privacy_seq}" />
							</c:when> 
							<c:otherwise>
								<input class="form-control" name="privacy_seq" id="privacy_seq" type="text" value="" />
								<div class="form-control-focus"></div>
							</c:otherwise>
						</c:choose>
						</td>
						<th style="width: 15%; text-align: center;"><label class="control-label">개인정보유형명 </label></th>
						<td style="width: 35%;" class="form-group form-md-line-input">
							<input type="text" name="privacy_desc" class="form-control"
							style="text-align: center; vertical-align: middle;"
							value="${indvinfoTypeSetup.privacy_desc }" />
							<div class="form-control-focus"></div>
						</td>
					</tr>
					<c:choose>
					<c:when test="${adminUserAuthId eq 'AUTH00000'}">
					<tr>
						<th style="text-align: center; vertical-align: middle;">
							<label class="control-label">정규식</label>
						</th>
						<td style="vertical-align: middle;" class="form-group form-md-line-input" colspan="3">
							<textarea name="regular_expression" class="form-control" rows="7">${indvinfoTypeSetup.regular_expression}</textarea>
							<div class="form-control-focus"></div>
						</td>
					</tr>
					</c:when>
					<c:otherwise>
					<input type="hidden" name="regular_expression" value="${indvinfoTypeSetup.regular_expression}">
					</c:otherwise>
					</c:choose>
					<tr>
						<th style="text-align: center;"><label class="control-label">사용여부
						</label></th>
						<td class="form-group form-md-line-input"
							style="vertical-align: middle;">
							<div class="md-radio-inline">
								<div class="md-radio">
									<input type="radio" id="checkbox1_1" name="use_flag" value="Y"
										class="md-radiobtn"
										${fn:containsIgnoreCase(indvinfoTypeSetup.use_flag,'Y')?'checked':'' }>
									<label for="checkbox1_1"> <span></span> <span
										class="check"></span> <span class="box"></span> 사용
									</label>
								</div>
								<div class="md-radio">
									<input type="radio" id="checkbox1_2" name="use_flag" value="N"
										class="md-radiobtn"
										${!fn:containsIgnoreCase(indvinfoTypeSetup.use_flag,'Y')?'checked':'' }>
									<label for="checkbox1_2"> <span></span> <span
										class="check"></span> <span class="box"></span> 미사용
									</label>
								</div>
								<c:if test="${indvinfoTypeSetup.privacy_seq ne null }">
									<div class="md-radio">
										<label class="mt-checkbox mt-checkbox-outline" style="margin-bottom: 0px;"> 
											<input type="checkbox" name="check_all" />전체적용<span></span>
										</label>
									</div>
								</c:if>
							</div>
						</td>
						
						<th style="text-align: center;"><label class="control-label">개인정보 마스킹 사용여부
						</label></th>
						<td class="form-group form-md-line-input"
							style="vertical-align: middle;">
							<div class="md-radio-inline">
								<div class="md-radio">
									<input type="radio" id="checkbox2_1" name="privacy_masking" value="Y"
										class="md-radiobtn"
										${fn:containsIgnoreCase(indvinfoTypeSetup.privacy_masking,'Y')?'checked':'' }>
									<label for="checkbox2_1"> <span></span> <span
										class="check"></span> <span class="box"></span> 사용
									</label>
								</div>
								<div class="md-radio">
									<input type="radio" id="checkbox2_2" name="privacy_masking" value="N"
										class="md-radiobtn"
										${!fn:containsIgnoreCase(indvinfoTypeSetup.privacy_masking,'Y')?'checked':'' }>
									<label for="checkbox2_2"> <span></span> <span
										class="check"></span> <span class="box"></span> 미사용
									</label>
								</div>
								<c:if test="${indvinfoTypeSetup.privacy_seq ne null }">
									<div class="md-radio">
										<label class="mt-checkbox mt-checkbox-outline" style="margin-bottom: 0px;"> 
											<input type="checkbox" name="check_all_masking" />전체적용<span></span>
										</label>
									</div>
								</c:if>
							</div>
						</td>
					</tr>
					<tr>
						<th style="text-align: center;"><label class="control-label">개인정보 내용 보기</label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<div class="md-radio-inline">
								<div class="md-radio">
									<input type="radio" id="checkbox3_1" name="use_biz_log_result" value="Y"
										class="md-radiobtn"
										${fn:containsIgnoreCase(indvinfoTypeSetup.use_biz_log_result,'Y')?'checked':'' }>
									<label for="checkbox3_1"> <span></span> <span
										class="check"></span> <span class="box"></span> 사용
									</label>
								</div>
								<div class="md-radio">
									<input type="radio" id="checkbox3_2" name="use_biz_log_result" value="N"
										class="md-radiobtn"
										${!fn:containsIgnoreCase(indvinfoTypeSetup.use_biz_log_result,'Y')?'checked':'' }>
									<label for="checkbox3_2"> <span></span> <span
										class="check"></span> <span class="box"></span> 미사용
									</label>
								</div>
								<c:if test="${indvinfoTypeSetup.privacy_seq ne null }">
									<div class="md-radio">
										<label class="mt-checkbox mt-checkbox-outline" style="margin-bottom: 0px;"> 
											<input type="checkbox" name="check_all_biz_log_result" />전체적용<span></span>
										</label>
									</div>
								</c:if>
							</div>
						</td>
						<c:choose>
							<c:when test="${adminUserAuthId eq 'AUTH00000'}">
								<th style="text-align: center;"><label class="control-label">매핑코드</label></th>
								<td><input type="number" value="${indvinfoTypeSetup.result_type_order }" name="result_type_order" class="form-control"></td>
							</c:when>
							<c:otherwise>
								<td colspan="2">
									<input type="hidden" value="${indvinfoTypeSetup.result_type_order }" name="result_type_order" class="form-control">
								</td>
							</c:otherwise>
						</c:choose>
					</tr>				</tbody>
			</table>



			<!-- 메뉴 관련 input 시작 -->
			<input type="hidden" name="main_menu_id"
				value="${paramBean.main_menu_id }" /> <input type="hidden"
				name="sub_menu_id" value="${paramBean.sub_menu_id }" /> <input
				type="hidden" name="current_menu_id" value="${currentMenuId }" /> <input
				type="hidden" name="system_seq" value="${search.system_seq }" />

			<!-- 메뉴 관련 input 끝 -->

			<!-- 페이지 번호 -->
			<input type="hidden" name="page_num" value="${search.page_num }" />

			<!-- 검색조건 관련 input 시작 -->
			<input type="hidden" name="privacy_desc_search"
				value="${search.privacy_desc_search}" /> <input type="hidden"
				name="use_flag_search" value="${search.use_flag_search }" />
			<input type="hidden" name="isSearch" value="${paramBean.isSearch }" />
			<!-- 검색조건 관련 input 끝 -->
		</form>
		<!-- END FORM-->

		<!-- 버튼 영역 -->
		<div class="form-actions">
			<div class="row">
				<div class="col-md-offset-2 col-md-10" align="right"
					style="padding-right: 30px;">
					<button type="button" class="btn btn-sm grey-mint btn-outline sbold uppercase"
						onclick="javascript:moveindvinfoTypeSetupListtoDetail();">
						<i class="fa fa-list"></i> &nbsp;목록
					</button>

					<c:choose>
						<c:when test="${empty indvinfoTypeSetup}">
							<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="addindvinfoTypeSetup()"><i
								class="fa fa-check"></i>&nbsp;추가</a>
						</c:when>
						<c:otherwise>
							<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="saveindvinfoTypeSetup()"><i
								class="fa fa-check"></i>&nbsp;저장</a>
							<!-- <a class="btn gray" onclick="removeindvinfoTypeSetup()" >삭제</a> -->
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var indvifoTypeSetupConfig = {
		"listUrl" : "${rootPath}/indvinfoTypeSetup/list.html",
		"addUrl" : "${rootPath}/indvinfoTypeSetup/add.html",
		"saveUrl" : "${rootPath}/indvinfoTypeSetup/save.html",
		"removeUrl" : "${rootPath}/indvinfoTypeSetup/remove.html",
		"loginPage" : "${rootPath}/loginView.html"
	};
</script>