<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/setup/ruleSimulate.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>
<style>
#loading {
 width: 100%;   
 height: 100%;   
 top: 0px;
 left: 0px;
 position: fixed;   
 display: block;   
 opacity: 0.7;   
 background-color: #fff;   
 z-index: 99;   
 text-align: center; }  
  
#loading-image {   
 position: absolute;   
 top: 50%;   
 left: 50%;  
 z-index: 100; }
.portlet.light>.portlet-title {
 min-height: 25px;
}
.portlet>.portlet-title {
 margin: 0px;
}

.portlet.light .portlet-body {
 padding: 0px 0px 10px 0px;
}
</style>

<div id="loading"><img id="loading-image" width="30px;" src="${rootPath}/resources/image/loading3.gif" alt="Loading..." /></div>
<h1 class="page-title"> ${currentMenuName}
	<!--bold font-blue-hoki <small>basic bootstrap tables with various options and styles</small> -->
</h1>
<div class="row">
	<div class="col-md-12">
		<div class="profile-content">
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light"style="min-height: 800px;">
						<div class="portlet-title">
							<div class="caption" style="">
								<i class="icon-settings font-dark"></i>
								<span class="caption-subject font-dark sbold uppercase">시나리오</span>
							</div>
						</div>
						<div class="portlet-body" style="width:30%">
							<select class="form-control" id="rule_select" name="rule_select" onchange="findRuleScript()" >
								<option value="" class="daySelect_first" >선택</option>
								<c:forEach items="${ruleList}" var="rule">
									<option value="${rule.rule_seq }">${rule.rule_nm }</option>
								</c:forEach>
							</select> 								
						</div>
						<div class="portlet-title">
							<div class="caption" style="">
								<i class="icon-settings font-dark"></i>
								<span class="caption-subject font-dark sbold uppercase">추출일</span>
							</div>
						</div>
						<div class="portlet-body" style="width:20%">
							<div class="input-group input-medium date-picker input-daterange"
								data-date="10/11/2012" data-date-format="yyyy-mm-dd">
								<input type="text" class="form-control" id="search_fr"
									name="search_from" value="${search.search_fromWithHyphen}"> 
							</div> 						
						</div>
						<div class="portlet-title">
							<div class="caption" style="">
								<i class="icon-settings font-dark"></i>
								<span class="caption-subject font-dark sbold uppercase">임계치</span>
							</div>
						</div>
						<div class="portlet-body" style="width:20%">
							<div class="input-group input-medium">
								<input type="text" class="form-control" id="limit_cnt" value="" />
							</div> 	
							
						</div>
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-settings font-dark"></i>
								<span class="caption-subject font-dark sbold uppercase">추출스크립트</span>
							</div>
						</div>
						<div class="portlet-body">
							<textarea rows="" cols="" id="script" name="script" style="height: 300px;width:100%"></textarea>								
						</div>
						<div style="text-align: right;padding: 10px 0px;">
							<a class="btn btn-sm blue btn-outline sbold uppercase" id="runButton" onclick="runScript()"><i class="fa fa-check"></i>실행</a>
						</div>
						<div id="tempDiv" style="display: none; ">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-settings font-dark"></i>
									<span class="caption-subject font-dark sbold uppercase">결과</span>
								</div>
							</div>
							<div class="portlet-body" style="overflow-x:auto; overflow-y:auto; width: 100%; height: 350px;">
								<table id="tempTable" class="table table-bordered table-striped" >
								<thead>
									<tr>
										<th style="text-align: center;">log_seq</th>
										<th style="text-align: center;">proc_date</th>
										<th style="text-align: center;">proc_time</th>
										<th style="text-align: center;">emp_user_id</th>
										<th style="text-align: center;">emp_user_name</th>
										<th style="text-align: center;">dept_id</th>
										<th style="text-align: center;">dept_name</th>
										<th style="text-align: center;">user_ip</th>
										<th style="text-align: center;">user_id</th>
										<th style="text-align: center;">scrn_id</th>
										<th style="text-align: center;">scrn_name</th>
										<th style="text-align: center;">req_type</th>
										<th style="text-align: center;">system_seq</th>
										<th style="text-align: center;">server_seq</th>
										<th style="text-align: center;">req_context</th>
										<th style="text-align: center;">req_url</th>
										<th style="text-align: center;">req_end_time</th>
										<th style="text-align: center;">result_type</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
								</table>					
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	<!-- END PROFILE CONTENT -->
<!-- END CONTENT BODY -->

<form id="menuSearchForm" method="POST">
	<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }"/>
	<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
	<input type="hidden" name="system_seq" value="${paramBean.system_seq }" />
	<input type="hidden" name="rule_seq" value=""/>
	<input type="hidden" name="page_num" value="${search.page_num}" />
</form>

<script type="text/javascript">

var simulateConfig = {
		"listUrl" : "${rootPath}/extrtCondbyInq/ruleSimulate.html",
		"menu_id" : "${currentMenuId}"
	};

	var menu_id = simulateConfig["menu_id"];
	var log_message = "SUCCESS";
	var log_action = "SELECT";

</script>