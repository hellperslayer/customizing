<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="userAddFlag" value="F" />
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />
<script src="${rootPath}/resources/js/common/jquery.form.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/setup/extrtBaseSetupList_indv.js" type="text/javascript" charset="UTF-8"></script>

<style>
 form-control1{width:100%;height:34px;padding:6px 12px;background-color:#fff;border:1px solid #c2cad8;border-radius:4px;-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,.075);box-shadow:inset 0 1px 1px rgba(0,0,0,.075);-webkit-transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s;-o-transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s;transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s}
</style>

<h1 class="page-title">${currentMenuName}</h1>
<div class="portlet light portlet-fit portlet-datatable bordered">
	<%-- <div class="portlet-title">
		<div class="caption">
			<i class="icon-settings font-dark"></i> <span
				class="caption-subject font-dark sbold uppercase">${currentMenuName}
				리스트</span>
		</div>
		<div class="actions">
			<div class="btn-group">
				<a class="btn red btn-outline btn-circle" href="javascript:;"
					data-toggle="dropdown"> <i class="fa fa-share"></i> <span
					class="hidden-xs"> 엑셀 </span> <i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu pull-right">
					<li>
						<!-- 					<button type="button" data-toggle="modal" data-target="#myModal">업로드</button> -->
						<a data-toggle="modal" data-target="#myModal">업로드</a> <!-- 										<a onclick="excelUpLoadList()">업로드</a> -->
					</li>
					<li><a onclick="excelEmpUserList()"> 다운로드 </a></li>
				</ul>
			</div>
		</div>
	</div> --%>

	<!-- 여기서부터 모달 몸통 -->

	<!-- Trigger the modal with a button -->
	<!--   <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->

	<!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog" style="position: relative ;top: 30%; left: 0;">

			<!-- Modal content -->
			<div class="modal-content">
				<div class="modal-header" style="background-color: #32c5d2;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">
						<b style="font-size: 13pt;">엑셀 업로드</b>
					</h4>
				</div>
				<div class="modal-body" style="background-color: #F9FFFF;">
					<form action="upload.html" method="post" id="fileForm"
						name="fileForm" enctype="multipart/form-data">
						<div>
							<table width="100%">
								<tr style="height: 40px">
									<td width="70%" style="vertical-align: middle;" align="center">
										<!-- 										<input type="submit" value="upload" />  --> <input
										class="fileUpLoad" type="file" id="file" name="file">
									</td>
									<td><img style="" name="submup"
										onclick="excelEmpUserUpLoad()"
										src="${rootPath}/resources/image/common/btn_exupload.gif"
										alt="엑셀업로드" title="엑셀업로드" /></td>
								</tr>
								<tr style="height: 40px">
									<td width="70%" style="vertical-align: middle;"><span
										class="downexam">&nbsp;&nbsp;&nbsp;※다운받은 양식으로 업로드하셔야
											사용자정보가 등록됩니다.</span></td>
									<td><img class="btn_excel exceldown" onclick="exDown()"
										src="${rootPath}/resources/image/common/formUp.png"
										alt="양식다운로드" title="양식다운로드" /></td>
								</tr>
							</table>
						</div>
						<input type="hidden" id="result" name="result" value="false" />
					</form>
				</div>
				<div class="modal-footer">
					<a class="btn btn-sm red" href="#" data-dismiss="modal"> <i
						class="fa fa-remove"></i> 닫기
					</a>
				</div>
			</div>
		</div>
	</div>
	<!-- 여기까지 -->
	<!-- 		<div class="exceluploader" align="center" -->
	<!-- 			style="height: 200px; position: absolute; z-index: 1;"> -->
	<!-- 			<div class="popup_wrap" align="center" -->
	<!-- 				style="display: none; height: 200px;"> -->
	<!-- 				<div class="portlet light portlet-fit portlet-datatable bordered" -->
	<!-- 					style="height: 100%; border: medium;background-color: #F9FFFF;"> -->

	<!-- 					<div class="portlet-title" style="background-color: #32c5d2;"> -->
	<!-- 						<b style="font-size: 13pt;">엑셀 업로드</b> -->
	<!-- 						<div align="right" style="float: right;"> -->
	<!-- 							<a class="btn btn-sm red" href="#" -->
	<%-- 								onclick="closeUpload('${userSession.admin_user_id}')"> <i --%>
	<!-- 								class="fa fa-remove"></i> 닫기 -->
	<!-- 							</a> -->
	<!-- 						</div> -->
	<!-- 					</div> -->
	<!-- 					<div class="portlet-body" style="background-color: #F9FFFF;" > -->

	<!-- 						<form action="upload.html" method="post" id="fileForm" -->
	<!-- 							name="fileForm" enctype="multipart/form-data"> -->
	<!-- 							<div> -->
	<!-- 								<table width="100%"> -->
	<!-- 									<tr> -->
	<!-- 										<td><img style="" name="submup" -->
	<!-- 											onclick="excelEmpUserUpLoad()" -->
	<%-- 											src="${rootPath}/resources/image/common/btn_exupload.gif" --%>
	<!-- 											alt="엑셀업로드" title="엑셀업로드" /></td> -->
	<!-- 										<td width="70%" style="vertical-align: middle;"> -->
	<!-- 											<input type="submit" value="upload" />  -->
	<!-- 											<input -->
	<!-- 											class="fileUpLoad" type="file" id="file" name="file"> -->
	<!-- 										</td> -->
	<!-- 									</tr> -->
	<!-- 								</table> -->
	<!-- 							</div> -->

	<!-- 							<br> -->
	<!-- 							<div> -->
	<!-- 								<img class="btn_excel exceldown" onclick="exDown()" -->
	<%-- 									src="${rootPath}/resources/image/common/formUp.png" alt="양식다운로드" --%>
	<!-- 									title="양식다운로드" /> <span class="downexam">&nbsp;&nbsp;&nbsp;※다운받은 양식으로 -->
	<!-- 									업로드하셔야 사용자정보가 등록됩니다.</span> -->
	<!-- 							</div> -->
	<!-- 							<input type="hidden" id="result" name="result" value="false" /> -->
	<!-- 						</form> -->
	<!-- 					</div> -->
	<!-- 				</div> -->
	<!-- 			</div> -->
	<!-- 		</div> -->

	<div class="portlet-body">
		<div class="table-container">

			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">

				<!-- 				<div class="dataTables_scroll" style="position: relative;"> -->
				<!-- 				<div class="row"> -->
				<div class="col-md-6"
					style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
					<%-- <div class="portlet box grey-salsa">
						<div class="portlet-title" style="background-color: #2B3643;">
							<div class="caption">
								<img src="${rootPath}/resources/image/icon/search.png"> 검색 & 엑셀
							</div>
							<div class="tools">
								<a href="javascript:;" class="expand"></a>
							</div>
							<div style="float: right; padding: 12px 10px 8px;"><b>${textSearch }</b></div>
						</div>
						<!-- portlet-body S -->
						<form id="empUserListForm"
							action="${rootPath}/empUserMngt/list.html" method="POST"
							class="portlet-body" style="display:none;">
							<div class="portlet-body">
								<div class="table-container">
									<div id="datatable_ajax_2_wrapper"
										class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
										<div class="row" style="padding-bottom: 5px;">
											<div class="col-md-4">
												<div class="col-md-5">◎ 사용자명:</div>
												<div class="col-md-7">
													<input class="form-control input-small" type=""
														name="emp_user_name" value="${search.emp_user_name}" />
												</div>
											</div>
											<div class="col-md-4">
												<div class="col-md-5">◎ 사용자ID:</div>
												<div class="col-md-7" align="left">
													<input class="form-control input-small" type=""
														name="emp_user_id_1" value="${search.emp_user_id_1}" />
												</div>
											</div>
											<div class="col-md-4">
												<div class="col-md-5">◎ 상태:</div>
												<div class="col-md-7" align="left">
													<select name="status" id="status"
														class="form-control input-small">
														<option value=""
															${search.status == '' ? 'selected="selected"' : ''}>
															----- 전체 -----</option>
														<c:if test="${empty CACHE_EMP_USER_STATUS}">
															<option>데이터 없음</option>
														</c:if>
														<c:if test="${!empty CACHE_EMP_USER_STATUS}">
															<c:forEach items="${CACHE_EMP_USER_STATUS}" var="i"
																varStatus="z">
																<option value="${i.key}"
																	${i.key==search.status ? "selected=selected" : "" }>${ i.value}</option>
															</c:forEach>
														</c:if>
													</select>
												</div>
											</div>
										</div>
										<div class="row" style="padding-bottom: 5px;">
											<div class="col-md-4">
												<div class="col-md-5">◎ 소속:</div>
												<div class="col-md-7" align="left">
													<select name="dept_id" id="dept_id"
														class="ticket-assign form-control input-small">
														<option value=""
															${search.dept_id == '' ? 'selected="selected"' : ''}>
															----- 전체 -----</option>
														<c:if test="${empty deptList}">
															<option>소속 데이터 없습니다.</option>
														</c:if>
														<c:if test="${!empty deptList}">
															<c:forEach items="${deptList}" var="i" varStatus="z">
																<option value="${i.dept_id}"
																	${i.dept_id==search.dept_id ? "selected=selected" : "" }>${ i.simple_dept_name}</option>
															</c:forEach>
														</c:if>
													</select>
												</div>
												<label style="padding-left: 20px;"> └<input
													class="deptFlagChk" type="checkbox"
													name="deptLowSearchFlag"
													${search.deptLowSearchFlag == 'on' ? 'checked="checked"' : ''}
													style="float: none;" />&nbsp;하위소속포함
												</label>
											</div>

											<div class="col-md-4">
												<div class="col-md-5">◎ 시스템명:</div>
												<div class="col-md-7" align="left">
													<select name="system_seq_1"
														class="ticket-assign form-control input-small">
														<option value=""
															<c:if test="${search.system_seq_1 == null}">selected="selected"</c:if>>
															----- 전체 -----</option>
														<c:forEach items="${systemMasterList}"
															var="systemMasterList">
															<option value="${systemMasterList.system_seq}"
																<c:if test="${systemMasterList.system_seq == search.system_seq_1}">selected="selected"</c:if>>${systemMasterList.system_name}</option>
														</c:forEach>
													</select>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12" align="right"
												style="padding-right: 20px;">
												<button type="button"
													class="btn btn-sm red table-group-action-submit"
													onclick="resetOptions(empUserListConfig['listUrl'])">
													<i class="fa fa-remove"></i> 취소
												</button>
												<button type="button"
													class="btn btn-sm green table-group-action-submit"
													onclick="moveEmpUserList()">
													<i class="fa fa-check"></i> 검색
												</button>&nbsp;&nbsp;
												<div class="btn-group">
													<a href="javascript:;" data-toggle="dropdown">
														<img width="62px;" src="${rootPath}/resources/image/icon/XLS_1.png">
													</a>
													<ul class="dropdown-menu pull-right">
														<li>
															<!-- 					<button type="button" data-toggle="modal" data-target="#myModal">업로드</button> -->
															<a data-toggle="modal" data-target="#myModal">업로드</a> <!-- 										<a onclick="excelUpLoadList()">업로드</a> -->
														</li>
														<li><a onclick="excelEmpUserList()"> 다운로드 </a></li>
													</ul>
												</div>
											</div>
										</div>
										<!-- portlet-boyd E -->
									</div>
								</div>
							</div>
							<input type="hidden" name="main_menu_id"
								value="${paramBean.main_menu_id}" /> <input type="hidden"
								name="sub_menu_id" value="${paramBean.sub_menu_id}" /> <input
								type="hidden" name="current_menu_id" value="${currentMenuId}" />
							<input type="hidden" name="page_num" value="${search.page_num}" />

						</form>
						<!-- portlet-boyd E -->

					</div> --%>
					
					<div class="portlet box grey-salt  ">
                          <div class="portlet-title" style="background-color: #2B3643;">
                              <div class="caption">
                                  <i class="fa fa-search"></i>검색 & 엑셀 </div>
                              <div class="tools">
                                  <a href="" class="collapse"> </a>
                              </div>
                          </div>
                          <div class="portlet-body form" >
                              <div class="form-body" style="padding-left:10px; padding-right:10px;">
                                  <div class="form-group">
                                      <form id="empUserListForm" method="POST" class="mt-repeater form-horizontal">
                                          <div data-repeater-list="group-a">
                                              <div data-repeater-item class="mt-repeater-item">
                                                  <!-- jQuery Repeater Container -->
                                                  <div class="mt-repeater-input">
                                                      <label class="control-label">사용자명</label>
                                                      <br/>
                                                      <input class="form-control"
														name="emp_user_name" value="${search.emp_user_name}" /> </div>
                                                  <div class="mt-repeater-input">
                                                      <label class="control-label">사용자ID</label>
                                                      <br/>
                                                      <input class="form-control"
														name="emp_user_id_1" value="${search.emp_user_id_1}" /></div>
                                                  <div class="mt-repeater-input">
                                                      <label class="control-label">상태</label>
                                                      <br/>
                                                      <select name="status" id="status"
														class="form-control">
														<option value=""
															${search.status == '' ? 'selected="selected"' : ''}>
															----- 전체 -----</option>
														<c:if test="${empty CACHE_EMP_USER_STATUS}">
															<option>데이터 없음</option>
														</c:if>
														<c:if test="${!empty CACHE_EMP_USER_STATUS}">
															<c:forEach items="${CACHE_EMP_USER_STATUS}" var="i"
																varStatus="z">
																<option value="${i.key}"
																	${i.key==search.status ? "selected=selected" : "" }>${ i.value}</option>
															</c:forEach>
														</c:if>
													</select>
                                                  </div>
                                                  <div class="mt-repeater-input">
                                                      <label class="control-label">소속</label>
                                                      <br/>
                                                      <select name="dept_id" id="dept_id"
														class="form-control">
														<option value=""
															${search.dept_id == '' ? 'selected="selected"' : ''}>
															----- 전체 -----</option>
														<c:if test="${empty deptList}">
															<option>소속 데이터 없습니다.</option>
														</c:if>
														<c:if test="${!empty deptList}">
															<c:forEach items="${deptList}" var="i" varStatus="z">
																<option value="${i.dept_id}"
																	${i.dept_id==search.dept_id ? "selected=selected" : "" }>${ i.simple_dept_name}</option>
															</c:forEach>
														</c:if>
													</select>
													<%-- <label style="padding-left: 0px;"> └<input
														class="deptFlagChk" type="checkbox"
														name="deptLowSearchFlag"
														${search.deptLowSearchFlag == 'on' ? 'checked="checked"' : ''}
														style="float: none;" />&nbsp;하위소속포함
													</label> --%>
													<div class="" style="padding-left: 0px;">
	                                                    <div class="mt-checkbox-inline">└
	                                                        <label class="mt-checkbox mt-checkbox-outline" style="margin-bottom: 0px;">
	                                                            <input type="checkbox" name="deptLowSearchFlag"
																${search.deptLowSearchFlag == 'on' ? 'checked="checked"' : ''}
																style="float: none;" />하위소속포함
	                                                            <span></span>
	                                                        </label>
	                                                    </div>
		                                            </div>
                                                  </div>
                                                  <div class="mt-repeater-input">
                                                      <label class="control-label">사스템명</label>
                                                      <br/>
                                                      <select name="system_seq_1"
														class="form-control">
														<option value=""
															<c:if test="${search.system_seq_1 == null}">selected="selected"</c:if>>
															----- 전체 -----</option>
														<c:forEach items="${systemMasterList}"
															var="systemMasterList">
															<option value="${systemMasterList.system_seq}"
																<c:if test="${systemMasterList.system_seq == search.system_seq_1}">selected="selected"</c:if>>${systemMasterList.system_name}</option>
														</c:forEach>
													</select>
                                                  </div>
                                              </div>
                                          </div>
										<div align="row">
											<div class="col-md-6">
												<select class="form-control input-xlarge" style="display: inline;" name="rule_seq">
													<option value=""> ----- 추출조건 선택 (일괄 적용) -----</option>
													<c:forEach var="ruleinfo" items="${ruleinfoList }">
														<option value="${ruleinfo.rule_seq }">${ruleinfo.rule_nm }</option>
													</c:forEach>
												</select>
												<input class="form-control input-xsmall" style="display: inline;" name="prct_val" 
												placeholder="비율"/>
												<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="setPrctAll()"><i class="fa fa-check"></i> 적용</a>
											</div>
											<div class="col-md-6" align="right">
												<button type="reset"
													class="btn btn-sm red-mint btn-outline sbold uppercase"
													onclick="resetOptions(empUserListConfig['listUrl'])">
													<i class="fa fa-remove"></i> <font>취소 
												</button>
												<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
													onclick="searchEmpUserList()">
													<i class="fa fa-search"></i> 검색
												</button>
												&nbsp;&nbsp;
												<%-- <a class="btn red btn-outline btn-circle"
												onclick="excelAllLogInqList('${search.total_count}')"> 
												<i class="fa fa-share"></i> <span class="hidden-xs"> 엑셀 </span>
											</a> --%>
												<div class="btn-group">
													<a href="javascript:;" data-toggle="dropdown"> <img
														src="${rootPath}/resources/image/icon/XLS_3.png">
													</a>
													<ul class="dropdown-menu pull-right">
														<li>
															<!-- 					<button type="button" data-toggle="modal" data-target="#myModal">업로드</button> -->
															<a data-toggle="modal" data-target="#myModal">업로드</a> <!-- 										<a onclick="excelUpLoadList()">업로드</a> -->
														</li>
														<li><a onclick="excelEmpUserList()"> 다운로드 </a></li>
													</ul>
												</div>
											</div>
										</div>

										<input type="hidden" name="main_menu_id"
										value="${paramBean.main_menu_id}" /> <input type="hidden"
										name="sub_menu_id" value="${paramBean.sub_menu_id}" /> <input
										type="hidden" name="current_menu_id" value="${currentMenuId}" />
									<input type="hidden" name="page_num" value="${search.page_num}" />
									<input type="hidden" name="isSearch" value="${paramBean.isSearch }" />
                                      </form>
                                  </div>
                              </div>
                          </div>
                      </div>
				</div>
				<!-- 				</div> -->

				<!-- <div class="table-toolbar">
					<div class="row">
						<div class="col-md-6">
							<div class="btn-group">
								<button class="btn btn-sm blue btn-outline sbold uppercase" onclick="findEmpUserMngtOne()">
									<i class="fa fa-plus"></i> 신규 
								</button>
							</div>
						</div>
					</div>
				</div> -->

				<table
					class="table table-striped table-bordered table-hover order-column"
					id="datatable_ajax_2" aria-describedby="datatable_ajax_2_info"
					role="grid">

					<thead>
						<tr role="row" class="heading">
							<th width="10%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								No.</th>
							<th width="20%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								소속</th>
							<th width="15%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								사용자명</th>
							<th width="15%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								사용자ID</th>
							<th width="15%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								시스템명</th>
							<th width="15%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								사용자IP</th>
							<th width="10%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								상태</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${empty empUserList}">
								<tr>
									<td colspan="7" align="center">데이터가 없습니다.</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:set value="${search.total_count}" var="count" />
								<c:forEach items="${empUserList}" var="empUser"
									varStatus="status">
									<tr
										onclick="findEmpUserMngtOne('${empUser.emp_user_id}', '${empUser.emp_user_name}', '${empUser.dept_name}', '${empUser.ip}');"
										style="cursor: pointer;">
										<td style="text-align: center;">${count - search.page_num * 10  + 10}</td>
										<td style="padding-left: 20px;"><c:out value="${empUser.dept_name}"/></td>
										<td style="text-align: center;"><c:out value="${empUser.emp_user_name}"/></td>
										<td style="padding-left: 20px;"><c:out value="${empUser.emp_user_id}"/></td>
										<td style="text-align: center;"><c:out value="${empUser.system_name}"/></td>
										<td style="text-align: center;"><c:out value="${empUser.ip}"/></td>
										<td style="text-align: center;"><c:out value="${empUser.status_name}"/></td>
									</tr>
									<c:set var="count" value="${count - 1 }" />
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
				<div class="dataTables_processing DTS_Loading"
					style="display: none;">Please wait ...</div>
				<!-- 				</div> -->
				<!-- 페이징 영역 -->
				<c:if test="${search.total_count > 0}">
					<div class="page left" id="pagingframe" align="center">
						<p>
							<ctl:paginator currentPage="${search.page_num}"
								rowBlockCount="${search.size}"
								totalRowCount="${search.total_count}" />
						</p>
					</div>
				</c:if>
			</div>
		</div>

	</div>
</div>
<!-- 엑셀양식 다운로드 -->
<form action="exdownload.html" method="post" id="exdown" name="exdown"
	enctype="multipart/form-data"></form>
<script type="text/javascript">
	var empUserListConfig = {
		"listUrl" : "${rootPath}/extrtBaseSetup/indvlist.html",
		"detailUrl" : "${rootPath}/extrtBaseSetup/indvdetail.html",
		"saveUrl" : "${rootPath}/extrtBaseSetup/setAllindv.html"
	};
</script>