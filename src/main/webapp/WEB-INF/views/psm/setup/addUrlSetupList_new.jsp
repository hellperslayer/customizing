<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags" %>

<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/setup/addUrlSetupList.js" type="text/javascript" charset="UTF-8"></script>

<h1 class="page-title">${currentMenuName}</h1>

<!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog" style="position: relative ;top: 30%; left: 0;">
			<!-- Modal content -->
			<div class="modal-content">
				<div class="modal-header" style="background-color: #32c5d2;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">
						<b style="font-size: 13pt;">엑셀 업로드</b>
					</h4>
				</div>
				<div class="modal-body" style="background-color: #F9FFFF;">
					<form action="upload.html" method="post" id="fileForm" name="fileForm" enctype="multipart/form-data">
						<div>
							<table width="100%">
								<tr style="height: 40px">
									<td width="70%" style="vertical-align: middle;" align="center">
										<input class="fileUpLoad" type="file" id="file" name="file">
									</td>
									<td><img style="cursor: pointer;" name="submup" onclick="excelEmpUserUpLoad()"
										src="${rootPath}/resources/image/common/btn_exupload.gif" alt="엑셀업로드" title="엑셀업로드" /></td>
								</tr>
								<tr style="height: 40px">
									<td width="70%" style="vertical-align: middle;">
										<span class="downexam">&nbsp;&nbsp;&nbsp;※ 다운받은 양식으로 업로드하셔야 등록됩니다.</span></td>
									<td><img class="btn_excel exceldown" onclick="exDown()" style="cursor: pointer;" src="${rootPath}/resources/image/common/formUp.png"
										alt="양식다운로드" title="양식다운로드" /></td>
								</tr>
							</table>
						</div>
						<input type="hidden" id="result" name="result" value="false" />
					</form>
				</div>
				<div class="modal-footer">
					<a class="btn btn-sm red" href="#" data-dismiss="modal"> <i class="fa fa-remove"></i> 닫기 </a>
				</div>
			</div>
		</div>
	</div>

<div class="row">
	<div class="col-md-12">
		<div class="portlet light portlet-fit portlet-datatable bordered">
			<%-- <div class="portlet-title">
				<div class="caption">
					<i class="icon-settings font-dark"></i> <span
						class="caption-subject font-dark sbold uppercase">${currentMenuName}
						리스트</span>
				</div>
				<div class="actions">
					<div class="btn-group">
						<a class="btn red btn-outline btn-circle" href="javascript:;"
							data-toggle="dropdown"> <i class="fa fa-share"></i> <span
							class="hidden-xs"> 다운로드 </span> <i class="fa fa-angle-down"></i>
						</a>
						<ul class="dropdown-menu pull-right">
							<li><a
								onclick="excelAddlUrlSetup('${search.total_count}')"> 엑셀 </a></li>
						</ul>
					</div>
				</div>
			</div> --%>
			<div class="portlet-body">
				<div class="table-container">
					<div id="datatable_ajax_2_wrapper"
						class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
						<div class="raw">
							<div class="col-md-6"
								style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
								<%-- <div class="portlet box grey-salsa">
									<div class="portlet-title" style="background-color: #32c5d2;">
										<div class="caption">
											<img src="${rootPath}/resources/image/icon/search.png"> 검색
										</div>
										<div class="tools">
											<a href="javascript:;" class="collapse"></a>
										</div>
									</div>
									<!-- portlet-body S -->
									<div class="portlet-body">
										<div class="table-container">
											<div id="datatable_ajax_2_wrapper"
												class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
												<form id="listForm" method="POST">
													<div class="row" style="padding-bottom: 15px;">
														<div class = "col-md-3">
															<div class="col-md-5" style="padding-right: 0px;margin-right: 0px;">
															◎ URL 속성:</div>
															<div class="col-md-7" align="left" style="padding-left: 0px;margin-left: 0px;">
																<select name="url_code_search" id="url_code" class="form-control input-small">
																	<option value="" ${search.url_code_search == '' ? 'selected="selected"' : ''}> -----전 체----- </option>
																	<c:if test="${empty CACHE_URL_TYPE}">
																		<option>데이터 없음</option>
																	</c:if>
																	<c:if test="${!empty CACHE_URL_TYPE}">
																		<c:forEach items="${CACHE_URL_TYPE}" var="i" varStatus="z">
																			<option value="${i.key}"${i.key==search.url_code_search ? "selected=selected" : "" } >${ i.value}</option>
																		</c:forEach>
																	</c:if>
																</select>
															</div>
														</div>														
														<div class = "col-md-3">
															<div class="col-md-5" style="padding-right: 0px;margin-right: 0px;">
															◎ URL 명:</div>
															<div class="col-md-7" align="left" style="padding-left: 0px;margin-left: 0px;">
																<input name="except_url_search" type="text" class="form-control input-small" value="${search.except_url_search}"/>
															</div>
														</div>
														<div class = "col-md-3">
															<div class="col-md-5" style="padding-right: 0px;margin-right: 0px;">
															◎URL 설명:</div>
															<div class="col-md-7" align="left" style="padding-left: 0px;margin-left: 0px;">
																<input name="url_desc_search" type="text" class="form-control input-small" value="${search.url_desc_search}"/>
															</div>
														</div>
														<div class = "col-md-3">
															<div class="col-md-5" style="padding-right: 0px;margin-right: 0px;">
															◎사용여부:</div>
															<div class="col-md-7" align="left" style="padding-left: 0px;margin-left: 0px;">
																<select name="use_yn_search" id="use_yn" class="form-control input-small">
																	<option value="">-----전 체-----</option>
																	<option value="Y">사용</option>
																	<option value="N">미사용</option>
																</select>
															</div>
														</div>
													</div>
													<div class="col-md-12"><br></div>
													<div class="row">
														<div class="col-md-12" align="right">
															<button type="reset" class="btn btn-sm red table-group-action-submit"
																onclick="resetOptions(addUrlListConfig['listUrl'])">
																<i class="fa fa-remove"></i> 취소
															</button>
															<button class="btn btn-sm green table-group-action-submit"
																onclick="moveAddlUrlSetup()">
																<i class="fa fa-check"></i> 검색
															</button>
														</div>
													</div>
													
													<input type="hidden" name="seq" value="" />
													<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id}" />
													<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id}"/>
													<input type="hidden" name="current_menu_id" value="${currentMenuId}"/>
													<input type="hidden" name="page_num" value="${search.page_num}"/>
													
												</form>
												
											</div>
										</div>
									</div>
								</div> --%>
								<div class="portlet box grey-salt  ">
		                            <div class="portlet-title" style="background-color: #2B3643;">
		                                <div class="caption">
		                                    <i class="fa fa-search"></i>검색 & 엑셀 </div>
		                                <div class="tools">
							                 <a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
							             </div>
		                            </div>
		                            <div id="searchBar" class="portlet-body form" >
		                                <div class="form-body" style="padding-left:10px; padding-right:10px;">
		                                    <div class="form-group">
		                                        <form id="listForm" method="POST" class="mt-repeater form-horizontal">
		                                            <div data-repeater-list="group-a">
		                                                <div class="row">
		                                                    <!-- jQuery Repeater Container -->
		                                                    <div class="col-md-2">
																<label class="control-label">시스템</label> 
																<select name="system_seq_search" class="form-control">
																	<option value="" ${search.system_seq_search == '' ? 'selected="selected"' : ''}>----- 전 체 -----</option>
																	<c:if test="${empty systemMasterList}">
																		<option>시스템 없음</option>
																	</c:if>
																	<c:if test="${!empty systemMasterList}">
																		<c:forEach items="${systemMasterList}" var="i">
																			<option value="${i.system_seq}" ${i.system_seq==search.system_seq_search ? 'selected="selected"' : "" }>${ i.system_name}</option>
																		</c:forEach>
																	</c:if>
																</select>
															</div>
		                                                    <div class="col-md-2">
		                                                        <label class="control-label">URL 명</label>
		                                                        <input name="except_url_search" type="text" class="form-control" value="${search.except_url_search}"/>
		                                                    </div>
															<div class="col-md-2">
		                                                        <label class="control-label">URL 설명</label>
		                                                        <input name="url_desc_search" type="text" class="form-control" value="${search.url_desc_search}"/>
															</div>
		                                                    <div class="col-md-2">
		                                                        <label class="control-label">사용여부</label>
		                                                        <select name="use_yn_search" id="use_yn" class="form-control">
																	<option value="">----- 전 체 -----</option>
																	<option value="Y">사용</option>
																	<option value="N">미사용</option>
																</select>
		                                                    </div>
		                                                </div>
		                                            </div>
		                                            <hr/>
		                                            <div align="right">
			                                            <button type="reset"
															class="btn btn-sm red-mint btn-outline sbold uppercase"
															onclick="resetOptions(addUrlListConfig['listUrl'])">
															<i class="fa fa-remove"></i> <font>초기화
														</button>
														<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
															onclick="moveAddlUrlSetup()">
															<i class="fa fa-search"></i> 검색
														</button>&nbsp;&nbsp;
														<div class="btn-group">
															<a href="javascript:;" data-toggle="dropdown">
																<img src="${rootPath}/resources/image/icon/XLS_3.png">
															</a>
															<ul class="dropdown-menu pull-right">
																<li>
																	<a data-toggle="modal" data-target="#myModal">업로드</a>
																</li>
																<li><a onclick="excelAddlUrlSetup('${search.total_count}')"> 다운로드 </a></li>
															</ul>
														</div>
													</div>
													
													<input type="hidden" name="seq" value="" />
													<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id}" />
													<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id}"/>
													<input type="hidden" name="current_menu_id" value="${currentMenuId}"/>
													<input type="hidden" name="page_num" value="${search.page_num}"/>
													<input type="hidden" name="isSearch" value="${paramBean.isSearch }" />
		                                        </form>
		                                    </div>
		                                </div>
		                            </div>
	                        	</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12" align="left">
								<a class="btn btn-sm blue btn-outline sbold uppercase"
									onclick="moveDetail('')">신규 <i class="fa fa-plus"></i></a>
							</div>
						</div>
						<div class="raw">
							<table style="border-top: 1px solid #e7ecf1;"
								class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
								role="grid">
								<colgroup>
									<col width="7%"/>
									<col width="15%"/>
									<col width="15%"/>				
									<col width="25%"/>
									<col width="28%"/>
									<col width="10%"/>
								</colgroup>
								<thead>
									<tr>
										<th scope="col" style="text-align: center;">등록번호</th>
										<th scope="col" style="text-align: center;">시스템</th>
										<th scope="col" style="text-align: center;">URL속성</th>
										<th scope="col" style="text-align: center;">URL</th>
										<th scope="col" style="text-align: center;">URL설명</th>
										<th scope="col" style="text-align: center;">사용여부</th>
									</tr>
								</thead>
								<tbody>
									<c:choose>
										<c:when test="${empty addUrlSetupList.addUrlSetup}">
											<tr>
								        		<td colspan="5" align="center">데이터가 없습니다.</td>
								        	</tr>
										</c:when>
										<c:otherwise>
											<c:set value="${search.total_count}" var="count"/>
											<c:forEach items="${addUrlSetupList.addUrlSetup}" var="addUrlSetup" varStatus="status">
												<tr style ='cursor: pointer;' onclick="javascript:moveDetail('${addUrlSetup.seq}');">
<%-- 													<td><ctl:nullCv nullCheck="${addUrlSetup.seq}"/></td> --%>
													<td><ctl:nullCv nullCheck="${addUrlSetup.seq}"/></td>
													<td style="text-align: center; vertical-align: middle;"><ctl:nullCv nullCheck="${addUrlSetup.system_name}"/></td>
													<td><ctl:nullCv nullCheck="${addUrlSetup.url_code_nm}"/></td>
													<td><ctl:nullCv nullCheck="${addUrlSetup.except_url}"/></td>
													<td><ctl:nullCv nullCheck="${addUrlSetup.url_desc}"/></td>
														<c:choose>
														<c:when test="${template_yesNo.options[addUrlSetup.use_yn] == '사용'}">
															<td style="text-align: center; vertical-align: middle;"><span
																class="label label-sm label-success"> 사용 </span></td>
														</c:when>
														<c:otherwise>
															<td style="text-align: center; vertical-align: middle;"><span
																class="label label-sm label-warning ">미사용 </span></td>
														</c:otherwise>
													</c:choose>
												</tr>
												<c:set var="count" value="${count - 1 }"/>
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
							
							<!-- 페이징 영역 -->
							<c:if test="${search.total_count > 0}">
								<div class="row" id="pagingframe" align="center">
									<p>
										<ctl:paginator currentPage="${search.page_num}"
											rowBlockCount="${search.size}"
											totalRowCount="${search.total_count}" />
									</p>
								</div>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- 엑셀양식 다운로드 -->
<form action="exdownload.html" method="post" id="exdown" name="exdown" enctype="multipart/form-data"></form>
<script type="text/javascript">

	$("#use_yn > option[value="+'<c:out value="${ search.use_yn_search }"/>'+"]").attr("selected","selected");

	var addUrlListConfig = {
		"listUrl":"${rootPath}/addUrlSetup/list.html"
		,"detailUrl":"${rootPath}/addUrlSetup/detail.html"
		,"downloadUrl" :"${rootPath}/addUrlSetup/download.html",
		"uploadUrl" : "${rootPath}/addUrlSetup/upload.html"
	};
	
</script>