<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>

<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/setup/holiSetupDetail.js"
	type="text/javascript" charset="UTF-8"></script>

<h1 class="page-title">${currentMenuName}</h1>
<!-- 그룹코드 신규 -->
<div class="portlet light portlet-fit bordered">
	<div class="portlet-body">
		<!-- BEGIN FORM-->
		<form id="holiDetailForm" action="" method="POST"
			class="form-horizontal form-bordered form-row-stripped">
			<table class="table table-bordered table-striped">
				<tbody>
					<c:choose>
						<c:when test="${empty holiSetup}">
							<tr>
								<th style="width: 15%; text-align: center;">
									<label class="control-label">생성년도 </label>
								</th>
								<td style="width: 85%; vertical-align: middle;" class="form-group form-md-line-input">
									<select id="add_year" name="add_year" class="ticket-assign form-control input-small"></select>
								</td>
							</tr>
						</c:when>
						<c:otherwise>
							<tr>
								<th style="width: 15%; text-align: center;">
									<label class="control-label" for="form_control_1">일자</label>
								</th>
								<td style="width: 40%; vertical-align: middle;">
									<fmt:parseDate value="${holiSetup.holi_dt}" pattern="yyyyMMdd" var="holiDt" /> 
									<fmt:formatDate value="${holiDt}" pattern="yyyy-MM-dd" />
								</td>
							</tr>
							<tr>
								<th style="width: 15%; text-align: center;">
									<label class="control-label">휴일명 </label>
								</th>
								<td style="width: 40%; vertical-align: middle;" class="form-group form-md-line-input">
									<input type="text" name="holi_nm" value="${holiSetup.holi_nm}" class="form-control input-medium" />
								</td>
							</tr>
							<tr>
								<th style="width: 15%; text-align: center;">
									<label class="control-label">휴일여부 </label>
								</th>
								<td style="width: 40%; vertical-align: middle;" class="form-group form-md-line-input">
									<select name="holi_yn" id="holi_yn" class="ticket-assign form-control input-medium">
										<option value="N">정상근무</option>
										<option value="Y">휴일</option>
									</select>
								</td>
							</tr>
						</c:otherwise>
					</c:choose>
					<tr>
						<th style="width: 15%; text-align: center;">
							<label class="control-label">근무시간 </label>
						</th>
						<td style="width: 40%;" class="form-group form-md-line-input">
							<input type="text" name="work_start_tm" value="${holiSetup.work_start_tm}" style="ime-mode: disabled;" onkeydown="onlyNumber()" />
							<i class="sim">&sim;</i>
							<input type="text" name="work_end_tm" value="${holiSetup.work_end_tm}" style="ime-mode: disabled;" onkeydown="onlyNumber()" />
							<label style="width: 100%; float: left; padding-top: 5px; color: red; font-weight: bold;">
								근무시간은 24시간으로 입력 예) 0900 ~ 1800 </label>
						</td>
					</tr>
				</tbody>
			</table>
			<!-- 메뉴 관련 input 시작 -->
			<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" />
			<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
			<input type="hidden" name="current_menu_id" value="${currentMenuId }" />
			<!-- 메뉴 관련 input 끝 -->

			<!-- 페이지 번호 -->
			<input type="hidden" name="page_num" value="${search.page_num }" />

			<!-- 검색조건 관련 input 시작 -->
			<input type="hidden" name="search_from"	value="${search.search_from }" />
			<input type="hidden" name="search_to" value="${search.search_to }" />
			<input type="hidden" name="holi_yn_search" value="${search.holi_yn_search }" />
			<input type="hidden" name="holi_dt" value="${holiSetup.holi_dt }" />
			<!-- 검색조건 관련 input 끝 -->
		</form>
		<!-- END FORM-->

		<!-- 버튼 영역 -->
		<div class="form-actions">
			<div class="row">
				<div class="col-md-offset-2 col-md-10" align="right"
					style="padding-right: 30px;">
					<button type="button" class="btn btn-sm grey-mint btn-outline sbold uppercase"
						onclick="moveHoliList();">
						<i class="fa fa-list"></i> &nbsp;목록
					</button>
					<c:choose>
						<c:when test="${empty holiSetup}">
							<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="addHoli()"><i
								class="fa fa-check"></i>&nbsp;등록</a>
						</c:when>
						<c:otherwise>
							<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="saveHoli()"><i
								class="fa fa-check"></i>&nbsp;수정</a>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	$("#holi_yn > option[value="+'<c:out value="${ holiSetup.holi_yn }"/>'+"]").attr("selected","selected");
	
	var holiDetailConfig = {
		"listUrl":"${rootPath}/holiSetup/list.html"
		,"addUrl":"${rootPath}/holiSetup/add.html"
		,"saveUrl":"${rootPath}/holiSetup/save.html"
		,"loginPage" : "${rootPath}/loginView.html"
	};
	
</script>