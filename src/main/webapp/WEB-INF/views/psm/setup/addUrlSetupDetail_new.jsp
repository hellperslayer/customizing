<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>

<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/setup/addUrlSetupDetail.js"
	type="text/javascript" charset="UTF-8"></script>
<h1 class="page-title">${currentMenuName}</h1>
<div class="portlet light portlet-fit bordered">
	<div class="portlet-body">
		<!-- BEGIN FORM-->
		<form id="addUrlDetailForm" action="" method="POST"
			class="form-horizontal form-bordered form-row-stripped">
			<input type="hidden" name="seq" value="${addUrlSetup.seq}" />
			<table class="table table-bordered table-striped">
				<tbody>
					<c:if test="${not empty addUrlSetup}">
						<tr>
							<th style="width: 15%; text-align: center;"><label
								class="control-label">등록번호<span class="required">*</span>
							</label></th>
							<td style="width: 35%; vertical-align: middle;" colspan="3" class="form-group form-md-line-input">${addUrlSetup.seq}</td>
						</tr>
					</c:if>
					<tr>
						<th style="width: 15%; text-align: center;"><label class="control-label">제외대상URL </label></th>
						<td style="width: 35%; vertical-align: middle;" class="form-group form-md-line-input">
							<input type="text" class="form-control" id="form_control_1" name="except_url" value="${addUrlSetup.except_url}" />
							<div class="form-control-focus"></div></td>
						<th style="text-align: center;width: 15%;"><label class="control-label" for="form_control_1">제외대상URL 속성</span>
						</label></th>
						<td style="vertical-align: middle;width: 35%;" class="form-group form-md-line-input">
							<select name="url_code" id="url_code" style="text-align: center;" class="ticket-assign form-control input-small">
								<c:if test="${empty CACHE_URL_TYPE}">
									<option>데이터 없음</option>
								</c:if>
								<c:if test="${!empty CACHE_URL_TYPE}">
									<c:forEach items="${CACHE_URL_TYPE}" var="i" varStatus="z">
										<option value="${i.key}"
											${i.key==addUrlSetup.url_code ? "selected=selected" : "" }>${ i.value}</option>
									</c:forEach>
								</c:if>
						</select></td>
					</tr>
					<tr>
						<th style="text-align: center;vertical-align: middle;"><label class="control-label">사용여부
						</label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<div class="md-radio-inline">
								<div class="md-radio col-md-4">
									<input type="radio" id="checkbox1_1" class="md-radiobtn" id="form_control_3" name="use_yn" value="Y"
										${addUrlSetup.use_yn == 'Y' || addUrlSetup.use_yn == null ? 'checked=checked' : '' }>
									<label for="checkbox1_1"> <span></span> <span class="check"></span> <span class="box"></span> 사용
									</label>
								</div>
								<div class="md-radio col-md-4">
									<input type="radio" id="checkbox1_2" class="md-radiobtn" id="form_control_4" name="use_yn" value="N"
										${addUrlSetup.use_yn == 'N' ? 'checked=checked' : '' }>
									<label for="checkbox1_2"> <span></span> <span class="check"></span> <span class="box"></span> 미사용
									</label>
								</div>
							</div>
						</td>
						<th style="text-align: center;width: 15%;"><label class="control-label">시스템</span>
						</label></th>
						<td style="vertical-align: middle; width: 35%;" class="form-group form-md-line-input">
							<select name="system_seq" class="form-control">
								<c:if test="${empty systemMasterList}">
									<option>시스템 없음</option>
								</c:if>
								<c:if test="${!empty systemMasterList}">
									<c:forEach items="${systemMasterList}" var="i" varStatus="z">
										<option value="${i.system_seq}" ${i.system_seq==addUrlSetup.system_seq ? "selected=selected" : "" }>${ i.system_name}</option>
									</c:forEach>
								</c:if>
							</select>
						</td>
					</tr>
					<tr>
						<th style="text-align: center;vertical-align: middle;"><label class="control-label">제외대상URL설명</label></th>
						<td colspan="3" class="form-group form-md-line-input" style="vertical-align: middle;">
							<textarea class="form-control" name="url_desc">${addUrlSetup.url_desc}</textarea>
							<div class="form-control-focus"></div></td>
					</tr>
				</tbody>
			</table>
			<!-- 메뉴 관련 input 시작 -->
			<input type="hidden" name="main_menu_id"
				value="${paramBean.main_menu_id }" /> <input type="hidden"
				name="sub_menu_id" value="${paramBean.sub_menu_id }" /> <input
				type="hidden" name="current_menu_id" value="${currentMenuId }" />
			<!-- 메뉴 관련 input 끝 -->

			<!-- 페이지 번호 -->
			<input type="hidden" name="page_num" value="${search.page_num }" />

			<!-- 검색조건 관련 input 시작 -->
			<input type="hidden" name="except_url_search"
				value="${search.except_url_search }" /> <input type="hidden"
				name="url_desc_search" value="${search.url_desc_search }" /> <input
				type="hidden" name="use_yn_search" value="${search.use_yn_search }" />
			<input type="hidden" name="url_code_search"
				value="${search.url_code_search }" />
			<!-- 검색조건 관련 input 끝 -->
		</form>
		<!-- END FORM-->

		<!-- 버튼 영역 -->
		<div class="form-actions">
			<div class="row">
				<div class="col-md-offset-2 col-md-10" align="right"
					style="padding-right: 30px;">
					<button type="button" class="btn btn-sm dark btn-outline sbold uppercase"
						onclick="moveAddUrlList();">
						<i class="fa fa-list"></i> &nbsp;목록
					</button>
					<c:choose>
						<c:when test="${empty addUrlSetup}">
							<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="addAddUrl()"><i
								class="fa fa-check"></i>&nbsp;등록</a>
						</c:when>
						<c:otherwise>
							<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="saveAddUrl()"><i
								class="fa fa-check"></i>&nbsp;수정</a>
							<a class="btn btn-sm red-mint btn-outline sbold uppercase" onclick="removeAddUrl()"><i
								class="fa fa-remove"></i>&nbsp;삭제</a>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
</div>

<form id="addUrlListForm" method="POST">
	<input type="hidden" name="main_menu_id"
		value="${paramBean.main_menu_id }" /> <input type="hidden"
		name="sub_menu_id" value="${paramBean.sub_menu_id }" /> <input
		type="hidden" name="current_menu_id" value="${currentMenuId}" /> <input
		type="hidden" name="except_url" value="${paramBean.except_url }" /> <input
		type="hidden" name="url_desc" value="${paramBean.url_desc }" /> <input
		type="hidden" name="use_yn" value="${paramBean.use_yn }" /> <input
		type="hidden" name="url_code" value="${paramBean.url_code }" />
</form>

<script type="text/javascript">
	var addUrlDetailConfig = {
		"listUrl" : "${rootPath}/addUrlSetup/list.html",
		"addUrl" : "${rootPath}/addUrlSetup/add.html",
		"saveUrl" : "${rootPath}/addUrlSetup/save.html",
		"removeUrl" : "${rootPath}/addUrlSetup/remove.html",
		"loginPage" : "${rootPath}/loginView.html"
	};
</script>