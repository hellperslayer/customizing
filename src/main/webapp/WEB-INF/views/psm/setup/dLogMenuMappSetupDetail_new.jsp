<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="dLogMenuMappSetupAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/setup/dLogMenuMappSetupList.js"
	type="text/javascript" charset="UTF-8"></script>
<h1 class="page-title">${currentMenuName}</h1>
<!-- 그룹코드 신규 -->
<div class="portlet light portlet-fit bordered">
	<div class="portlet-body">
		<!-- BEGIN FORM-->
		<form id="dLogMenuMappSetupDetailForm" action="" method="POST" class="form-horizontal form-bordered form-row-stripped">
			<c:if test="${!empty dLogMenuMappSetupDetail}">
				<input type="hidden" name="dlog_tag_seq" value="${dLogMenuMappSetupDetail.dlog_tag_seq }" />
			</c:if>
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<%-- <th style="width: 15%; text-align: center;"><label
							class="control-label">메뉴매핑ID<span class="required">*</span>
						</label></th>
						<td style="width: 35%; vertical-align: middle;"
							class="form-group form-md-line-input">
							<c:choose>
								<c:when test="${empty dLogMenuMappSetupDetail}">
									<input type="text" name="dlog_tag_seq" size="51"
										class="form-control"/>
									<div class="form-control-focus"></div>
								</c:when>
								<c:otherwise>
									<c:out value="${dLogMenuMappSetupDetail.dlog_tag_seq}" />
									<input type="hidden" name="dlog_tag_seq"
										value="${dLogMenuMappSetupDetail.dlog_tag_seq}" />
								</c:otherwise>
							</c:choose>
						</td> --%>
						<th style="width: 15%; text-align: center;"><label class="control-label">메뉴명</label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<input type="text" class="form-control" name="menu_name" size="51" value="${dLogMenuMappSetupDetail.menu_name }" />
							<div class="form-control-focus"></div></td>
							
						<th style="width: 15%; text-align: center;"><label
							class="control-label">사용여부 </label></th>
						<td style="width: 35%;" class="form-group form-md-line-input">
							<div class="md-radio-inline">
								<div class="md-radio col-md-4">
									<input type="radio" id="checkbox1_1" class="md-radiobtn"
										id="form_control_2" title="사용" name="use_yn" value="Y"
										${fn:containsIgnoreCase(dLogMenuMappSetupDetail.use_yn,'Y')?'checked':'' }>
									<label for="checkbox1_1"> <span></span> <span
										class="check"></span> <span class="box"></span> 사용
									</label>
								</div>
								<div class="md-radio col-md-4">
									<input type="radio" id="checkbox1_2" class="md-radiobtn"
										id="form_control_2" title="미사용" name="use_yn" value="N"
										${!fn:containsIgnoreCase(dLogMenuMappSetupDetail.use_yn,'Y')?'checked':'' }>
									<label for="checkbox1_2"> <span></span> <span
										class="check"></span> <span class="box"></span> 미사용
									</label>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<th style="text-align: center;"><label class="control-label">URL<span class="required">*</span></label></th>
						<%-- <td colspan="3" class="form-group form-md-line-input" style="vertical-align: middle;">
							<input type="text" class="form-control" name="url" size="150" value="${dLogMenuMappSetupDetail.url }" />
							<div class="form-control-focus"></div>
						</td> --%>
						<td colspan="3" class="form-group form-md-line-input" style="text-align: left;"><TEXTAREA name="url" class="form-control" rows="5" style="width: 100%;">${dLogMenuMappSetupDetail.url}</TEXTAREA>
						<div class="form-control-focus"></div></td>
					</tr>
					<tr>
						<th style="text-align: center;"><label class="control-label">파라미터</label></th>
						<td colspan="3" class="form-group form-md-line-input" style="vertical-align: middle;">
							<input type="text" class="form-control" name="parameter" value="${dLogMenuMappSetupDetail.parameter }" />
							<div class="form-control-focus"></div>
						</td>
					</tr>
					<tr>
						<th style="width: 15%; text-align: center;"><label class="control-label">시스템</label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<select name="system_seq" id="system_seq" class="ticket-assign form-control">
									<option value=""
										<c:if test="${dLogMenuMappSetupDetail.system_seq == null}">selected="selected"</c:if>>선택</option>
									<c:forEach items="${systemMasterList}" var="systemMasterList">
										<option value="${systemMasterList.system_seq}"
											<c:if test="${systemMasterList.system_seq == dLogMenuMappSetupDetail.system_seq}">selected="selected"</c:if>>${systemMasterList.system_name}(${systemMasterList.system_seq })</option>
									</c:forEach>
							</select>
						</td>
						
						<th style="width: 15%; text-align: center;"><label
                            class="control-label">관리대장 등록여부 </label></th>
                        <td style="width: 35%;" class="form-group form-md-line-input">
                            <div class="md-radio-inline">
                                <div class="md-radio col-md-4">
                                    <input type="radio" id="checkbox2_1" class="md-radiobtn"
                                        id="form_control_3" title="등록" name="register_status" value="Y"
                                        ${fn:containsIgnoreCase(dLogMenuMappSetupDetail.register_status,'Y')?'checked':'' }>
                                    <label for="checkbox2_1"> <span></span> <span
                                        class="check"></span> <span class="box"></span> 등록
                                    </label>
                                </div>
                                <div class="md-radio col-md-4">
                                    <input type="radio" id="checkbox2_2" class="md-radiobtn"
                                        id="form_control_3" title="미등록" name="register_status" value="N"
                                        ${!fn:containsIgnoreCase(dLogMenuMappSetupDetail.register_status,'Y')?'checked':'' }>
                                    <label for="checkbox2_2"> <span></span> <span
                                        class="check"></span> <span class="box"></span> 미등록
                                    </label>
                                </div>
                            </div>
                        </td>
						
					</tr>
					<tr>
						<th style="text-align: center;"><label class="control-label">파일명</label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<input type="text" class="form-control" name="file_name" value="${dLogMenuMappSetupDetail.file_name }" />
							<div class="form-control-focus"></div>
						</td>
						<th style="text-align: center;"><label class="control-label">다운로드 임계치</label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<input type="text" class="form-control" name="threshold" value="${empty dLogMenuMappSetupDetail.threshold ? 0:dLogMenuMappSetupDetail.threshold }" />
							<div class="form-control-focus"></div>
						</td>
					</tr>
					<tr>
						<th style="text-align: center;"><label class="control-label">사유</label></th>
						<td colspan="3" class="form-group form-md-line-input" style="vertical-align: middle;"><TEXTAREA name="reason" class="form-control" rows="5" style="width: 100%;">${dLogMenuMappSetupDetail.reason }</TEXTAREA>
						<div class="form-control-focus"></div></td>
					</tr>
				</tbody>
			</table>


			<!-- 메뉴 관련 input 시작 -->
			<input type="hidden" name="main_menu_id"
				value="${paramBean.main_menu_id }" /> <input type="hidden"
				name="sub_menu_id" value="${paramBean.sub_menu_id }" /> <input
				type="hidden" name="current_menu_id" value="${currentMenuId }" />
			<!-- 메뉴 관련 input 끝 -->

			<!-- 페이지 번호 -->
			<input type="hidden" name="page_num" value="${search.page_num }" />

			<!-- 검색조건 관련 input 시작 -->
			<input type="hidden" name="system_seq_search"
				value="${search.system_seq_search}" /> <input type="hidden"
				name="url_search" value="${search.url_search }" />
			<!-- 검색조건 관련 input 끝 -->
		</form>
		<!-- END FORM-->

		<!-- 버튼 영역 -->
		<div class="form-actions">
			<div class="row">
				<div class="col-md-offset-2 col-md-10" align="right"
					style="padding-right: 30px;">
					<button type="button" class="btn btn-sm dark btn-outline sbold uppercase"
						onclick="movedLogMenuMappSetupListtoDetail()">
						<i class="fa fa-list"></i> &nbsp;목록
					</button>
					<c:choose>
						<c:when test="${empty dLogMenuMappSetupDetail}">
							<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="adddLogMenuMappSetup()"><i
								class="fa fa-check"></i>&nbsp;추가</a>
						</c:when>
						<c:otherwise>
							<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="savedLogMenuMappSetup()"><i
								class="fa fa-check"></i>&nbsp;저장</a>
							<a class="btn btn-sm red-mint btn-outline sbold uppercase" onclick="removedLogMenuMappSetup()"><i
								class="fa fa-remove"></i>&nbsp;삭제</a>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	var dLogMenuMappSetupConfig = {
		"listUrl":"${rootPath}/dLogMenuMappSetup/list.html"
		,"addUrl":"${rootPath}/dLogMenuMappSetup/add.html"
		,"saveUrl":"${rootPath}/dLogMenuMappSetup/save.html"
		,"removeUrl":"${rootPath}/dLogMenuMappSetup/remove.html"
		,"privFindListUrl":"${rootPath}/dLogMenuMappSetup/menuPrivFindList.html"
	};
	
</script>