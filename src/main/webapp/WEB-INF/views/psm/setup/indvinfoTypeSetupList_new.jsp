<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/setup/indvinfoTypeSetupList.js" type="text/javascript" charset="UTF-8"></script>
<h1 class="page-title">${currentMenuName}</h1>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light portlet-fit portlet-datatable bordered">

			<div class="portlet-body">
				<div class="table-container">
					<div id="datatable_ajax_2_wrapper"
						class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
						<div class="row">
							<div class="col-md-6"
								style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
								
								<div class="portlet box grey-salt  ">
			                         <div class="portlet-title" style="background-color: #2B3643;">
			                             <div class="caption">
			                                 <i class="fa fa-search"></i>검색 & 엑셀 </div>
			                              <div class="tools">
							                 <a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
							             </div>
			                         </div>
			                         <div id="searchBar" class="portlet-body form" >
			                             <div class="form-body" style="padding-left:20px; padding-right:10px;">
			                                 <div class="form-group">
			                                     <form id="listForm" method="POST" class="mt-repeater form-horizontal">
			                                         <div data-repeater-list="group-a">
			                                             <div class="row">
			                                                 <!-- jQuery Repeater Container -->
			                                                 <%-- <div class="col-md-2">
			                                                     <label class="control-label">개인정보유형</label>
			                                                     <input class="form-control" type="text"
																	name="privacy_desc_search"
																	value="${search.privacy_desc_search}" /> </div>
															 <div class="col-md-2">
			                                                     <label class="control-label">사용여부</label>
			                                                     <select name="use_flag_search"
																	class="ticket-assign form-control">
																	<option value="">-----전 체-----</option>
																	<option value="Y"
																		<c:if test='${search.use_flag_search eq "Y"}'>selected="selected"</c:if>>사용</option>
																	<option value="N"
																		<c:if test='${search.use_flag_search eq "N"}'>selected="selected"</c:if>>미사용</option>
																</select> 
															</div> --%>
			                                                 <div class="col-md-3">
			                                                     <label class="control-label">시스템명</label><br>
			                                                     <select name="system_seq"
																	class="ticket-assign form-control" onchange="javascript:moveindvinfoTypeSetupList()">
																	<c:forEach items="${SystemMasterList}"
																		var="SystemMaster" varStatus="status">
																		<option value="${SystemMaster.system_seq}"
																			<c:if test='${SystemMaster.system_seq eq search.system_seq}'>selected="selected"</c:if>>${SystemMaster.system_name}</option>
																	</c:forEach>
																</select>
															</div>
			                                             </div>
			                                         </div>
			                                         <hr/>
			                                         <div align="right">
														<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
															onclick="javascript:moveindvinfoTypeSetupList()">
															<i class="fa fa-search"></i> 검색
														</button>&nbsp;&nbsp;
														<a onclick="excelIndvinfoTypeSetup('${search.total_count}')"><img src="${rootPath}/resources/image/icon/XLS_3.png"></a>
													</div>
											
													<input type="hidden" name="privacy_seq" value="" /> <input
														type="hidden" name="main_menu_id"
														value="${paramBean.main_menu_id }" /> <input
														type="hidden" name="sub_menu_id"
														value="${paramBean.sub_menu_id }" /> <input type="hidden"
														name="current_menu_id" value="${currentMenuId}" /> <input
														type="hidden" name="page_num" value="${search.page_num}" />
													<input type="hidden" name="isSearch" value="${paramBean.isSearch }"/>
			                                     </form>
			                                 </div>
			                             </div>
			                         </div>
			                     </div>
							</div>
						</div>
						<c:if test="${indvinfoTypeCnt<19 }">
						<c:if test="${adminUserAuthId eq 'AUTH00000'}">
						<div class="row">
							<div class="col-md-12" align="left">
								<a class="btn btn-sm blue btn-outline sbold uppercase"
									onclick="fnindvinfoTypeSetupDetail('')">신규 <i class="fa fa-plus"></i></a>
							</div>
						</div>
						</c:if></c:if>
						<div class="raw">
							<table style="border-top: 1px solid #e7ecf1;"
								class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
								role="grid">
								<colgroup>
									<col width="5%" />
									<col width="20%" />
									<c:if test="${adminUserAuthId eq 'AUTH00000'}">
									<col width="25%" />
									<col width="5%" />
									</c:if>
									<col width="15%" />
									<col width="15%" />
									<col width="15%" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col" style="text-align: center; vertical-align: middle;">타입</th>
										<th scope="col" style="text-align: center; vertical-align: middle;">개인정보유형</th>
										<c:if test="${adminUserAuthId eq 'AUTH00000'}">
										<th scope="col" style="text-align: center; vertical-align: middle;">정규식</th>
										<th scope="col" style="text-align: center; vertical-align: middle;">매핑코드</th>
										</c:if>
										<th scope="col" style="text-align: center; vertical-align: middle;">사용여부</th>
										<th scope="col" style="text-align: center; vertical-align: middle;">개인정보 마스킹</th>
										<th scope="col" style="text-align: center; vertical-align: middle;">개인정보 내용보기</th>
									</tr>
								</thead>
								<tbody>
									<c:choose>
										<c:when test="${empty indvinfoTypeSetupList.indvinfoTypeSetup}">
											<tr>
												<td colspan="4" align="center">데이터가 없습니다.</td>
											</tr>
										</c:when>
										<c:otherwise>
											<c:set value="${indvinfoTypeSetupList.page_total_count}"
												var="count" />
											<c:forEach items="${indvinfoTypeSetupList.indvinfoTypeSetup}"
												var="indvinfoTypeSetup" varStatus="status">
												<tr style='cursor: pointer;'
													onclick="javascript:fnindvinfoTypeSetupDetail('${indvinfoTypeSetup.privacy_seq}')">
													<td style="text-align: center; vertical-align: middle;"><ctl:nullCv
															nullCheck="${indvinfoTypeSetup.privacy_seq}" />
													</td>
													<td style="text-align: left; vertical-align: middle;">
														<%-- <ctl:nullCv nullCheck="${indvinfoTypeSetup.privacy_desc}" /> --%>
														<c:out value="${indvinfoTypeSetup.privacy_desc}" default="시스템"/>
													</td>
													<c:if test="${adminUserAuthId eq 'AUTH00000'}">
													<td style="text-align: left; vertical-align: middle;">
														<c:choose>
															<c:when test="${fn:length(indvinfoTypeSetup.regular_expression)>49 }">
																<c:out value="${fn:substring(indvinfoTypeSetup.regular_expression,0,48)}"/>...
															</c:when>
															<c:otherwise>
																<c:out value="${indvinfoTypeSetup.regular_expression}"/>
															</c:otherwise>
														</c:choose>
													</td>
													<td style="text-align: center; vertical-align: middle;">
													<c:if test="${indvinfoTypeSetup.result_type_order =='0'}">-</c:if>
													<c:if test="${indvinfoTypeSetup.result_type_order !='0'}">${indvinfoTypeSetup.result_type_order}</c:if>
													</td>
													</c:if>
													<c:choose>
														<c:when
															test="${template_yesNo.options[indvinfoTypeSetup.use_flag] == '사용'}">
															<td style="text-align: center; vertical-align: middle;"><span
																class="label label-sm label-success"> 사용 </span></td>
														</c:when>
														<c:otherwise>
															<td style="text-align: center; vertical-align: middle;"><span
																class="label label-sm label-warning ">미사용 </span></td>
														</c:otherwise>
													</c:choose>
													<c:choose>
														<c:when
															test="${template_yesNo.options[indvinfoTypeSetup.privacy_masking] == '사용'}">
															<td style="text-align: center; vertical-align: middle;"><span
																class="label label-sm label-success"> 사용 </span></td>
														</c:when>
														<c:otherwise>
															<td style="text-align: center; vertical-align: middle;"><span
																class="label label-sm label-warning ">미사용 </span></td>
														</c:otherwise>
													</c:choose>
													<c:choose>
														<c:when
															test="${template_yesNo.options[indvinfoTypeSetup.use_biz_log_result] == '사용'}">
															<td style="text-align: center; vertical-align: middle;"><span
																class="label label-sm label-success"> 사용 </span></td>
														</c:when>
														<c:otherwise>
															<td style="text-align: center; vertical-align: middle;"><span
																class="label label-sm label-warning ">미사용 </span></td>
														</c:otherwise>
													</c:choose>
												</tr>
												<c:set var="count" value="${count - 1 }" />
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
							<!-- 페이징 영역 -->
							<c:if test="${search.total_count > 0}">
								<div class="page left" id="pagingframe" align="center">
									<p>
										<ctl:paginator currentPage="${search.page_num}"
											rowBlockCount="${search.size}"
											totalRowCount="${search.total_count}" />
									</p>
								</div>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var indvinfoTypeSetupConfig = {
		"listUrl" : "${rootPath}/indvinfoTypeSetup/list.html",
		"detailUrl" : "${rootPath}/indvinfoTypeSetup/detail.html",
		"downloadUrl" : "${rootPath}/indvinfoTypeSetup/download.html",
		"menu_id" : "${currentMenuId}"
	};

	var menu_id = indvinfoTypeSetupConfig["menu_id"];
	var log_message = "SUCCESS";
	var log_action = "SELECT";
</script>