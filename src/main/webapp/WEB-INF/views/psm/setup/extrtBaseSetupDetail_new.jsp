<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="extrtBaseSetupAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/setup/extrtBaseSetupList.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>

<h1 class="page-title">${currentMenuName}</h1>
<div class="portlet light portlet-fit bordered">
	<div class="portlet-body">
		<form id="extrtBaseSetupDetailForm" action="" method="POST" class="form-horizontal form-bordered form-row-stripped">
			<c:if test="${search.isSearch!='Y' }">※ 시나리오 상세 설정 변경 시에는 관리자에게 문의해주세요.</c:if>
			<table class="table table-bordered table-striped" style="margin-bottom: 50px;">
				<tbody>
					<c:choose>
					<c:when test="${extrtBaseSetupDetail.rule_seq==0}">
					<tr>
						<th style="width: 15%; text-align: center;">
							<label class="control-label" for="form_control_1">시나리오명<span class="required">*</span></label>
						</th>
						<td style="width: 35%; vertical-align: middle;" class="form-group form-md-line-input"> 
							<select name="scen_seq" class="ticket-assign form-control" onchange="scenseqChange()">
								<c:forEach items="${scenarioList}" var="scenario" varStatus="status">
									<option value="${scenario.scen_seq}" <c:if test="${scenario.scen_seq eq search.scen_seq }">selected</c:if>>${scenario.scen_name}</option>
								</c:forEach>
							</select>
							<div class="form-control-focus"></div>
						</td>
						<th style="width: 15%; text-align: center; "><label class="control-label">상세시나리오코드 <span class="required">*</span></label></th>
						<td style="width: 35%; vertical-align: middle;" class="form-group form-md-line-input">
							<input type="text" class="form-control" name="rule_seq" value="" />
						</td>
					</tr>
					<tr>
						<th style="text-align: center;"><label class="control-label">상세 시나리오명 <span class="required">*</span>
						</label></th>
						<td colspan="3" class="form-group form-md-line-input">
							<input type="text" class="form-control" name="rule_nm" size="51" value="${extrtBaseSetupDetail.rule_nm }" />
							<div class="form-control-focus"></div>
						</td>
					</tr>
					</c:when>
					<c:otherwise>
					<input type="hidden" name="scen_seq" value="${extrtBaseSetupDetail.scen_seq }" />
					<tr>
						<th style="width: 15%; text-align: center;"><label class="control-label" for="form_control_1">시나리오명
						</label></th>
						<td colspan="3" style="width: 85%; vertical-align: middle;" class="form-group form-md-line-input">
							<c:out value="${extrtBaseSetupDetail.scen_name }" />
						</td>
					</tr>
					<tr>
						<th style="width: 15%; text-align: center;">
							<label class="control-label">상세시나리오코드</label>
						</th>
						<td style="width: 35%; vertical-align: middle;" class="form-group form-md-line-input">${extrtBaseSetupDetail.rule_seq }
							<input type="hidden" name="rule_seq" value="${extrtBaseSetupDetail.rule_seq}" />
						</td>
						<th style="width: 15%; text-align: center;"><label class="control-label">사용여부<span class="required">*</span>
						</label></th>
						<td class="form-group form-md-line-input" style="width: 35%; vertical-align: middle;">
							<div class="md-radio-inline">
								<div class="md-radio col-md-4">
									<input type="radio" id="checkbox1_1" class="md-radiobtn" id="form_control_4" name="use_yn" value="Y"
										${fn:containsIgnoreCase(extrtBaseSetupDetail.use_yn,'Y')?'checked':'' }
										${search.isSearch!='Y'?'disabled':'' }>
									<label for="checkbox1_1"> <span></span> <span class="check"></span> <span class="box"></span> 사용
									</label>
								</div>
								<div class="md-radio col-md-4">
									<input type="radio" id="checkbox1_2" class="md-radiobtn" id="form_control_4" name="use_yn" value="N"
										${!fn:containsIgnoreCase(extrtBaseSetupDetail.use_yn,'Y')?'checked':'' }
										${search.isSearch!='Y'?'disabled':'' }>
									<label for="checkbox1_2"> <span></span> <span
										class="check"></span> <span class="box"></span> 미사용
									</label>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<th style="text-align: center;"><label class="control-label">상세 시나리오명  <span class="required">*</span>
						</label></th>
						<td colspan="3" class="form-group form-md-line-input">
							<input type="text" class="form-control" name="rule_nm" size="51" value="${extrtBaseSetupDetail.rule_nm }"
							${search.isSearch!='Y'?'readonly':'' }/>
							<div class="form-control-focus"></div>
						</td>
					</tr>
					</c:otherwise>
					</c:choose>
				<tr>
					<th style="text-align: center;"><label class="control-label">위험도<span class="required">*</span></label></th>
					<td colspan="3" class="form-group form-md-line-input" style="vertical-align: middle;">
						<input type="text" class="form-control" name="dng_val" size="51" value="${extrtBaseSetupDetail.dng_val }" style="width: 50%; float:left;"
						${search.isSearch!='Y'?'readonly':'' }/>
						<input type="hidden" name="dngValRest" value="${100-dngValSum }" ${search.isSearch!='Y'?'readonly':'' } />
						<div>전체 상세시나리오 위험도 합은 ${dngValSum } 입니다. <br/>(설정할 수 있는 위험도는 <c:out value="${100-dngValSum }"/> 이하입니다.)</div>
					</td>
				</tr>
				<tr>
					<c:choose>
						<c:when test="${ui_type == 'Shcd'}">
							<th style="text-align: center;"><label class="control-label">소명 메일 제목</label></th>
						</c:when>
						<c:otherwise>
							<th style="text-align: center;"><label class="control-label">시나리오 설명</label></th>	
						</c:otherwise>
					</c:choose>
					<td colspan="3" class="form-group form-md-line-input" style="vertical-align: middle;">
						<input type="text" class="form-control" id="form_control_5" size="51" name="rule_desc" value="${extrtBaseSetupDetail.rule_desc }"
						${search.isSearch!='Y'?'readonly':'' } ${search.isSearch!='Y'?'readonly':'' }/>
						<div class="form-control-focus"></div></td>
				</tr>
			</tbody>
			</table>
					
				<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<th style="text-align: center; width: 15%;"><label class="control-label" for="form_control_2">주체 </label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle; width: 35%;">
							<div class="md-radio-inline">
								<div class="md-radio col-md-3">
									<input type="radio" id="checkbox5_1" class="md-radiobtn" id="form_control_7" title="접속기록조회" name="log_delimiter"
										value="BA" ${fn:containsIgnoreCase(extrtBaseSetupDetail.log_delimiter,'BA')?'checked':'' } onclick="return(false);"
										${search.isSearch!='Y'?'disabled':'' }>
									<label for="checkbox5_1"> <span></span> <span
										class="check"></span> <span class="box"></span> 접속기록조회
									</label>
								</div>
								<div class="md-radio col-md-3">
									<input type="radio" id="checkbox5_2" class="md-radiobtn" id="form_control_7" title="DB접근조회" name="log_delimiter"
										value="DB" ${fn:containsIgnoreCase(extrtBaseSetupDetail.log_delimiter,'DB')?'checked':'' } onclick="return(false);"
										${search.isSearch!='Y'?'disabled':'' }>
									<label for="checkbox5_2"> <span></span> <span
										class="check"></span> <span class="box"></span> DB접근조회
									</label>
								</div>
								<div class="md-radio col-md-3">
									<input type="radio" id="checkbox5_3" class="md-radiobtn" id="form_control_7" title="다운로드로그" name="log_delimiter"
										value="DN" ${fn:containsIgnoreCase(extrtBaseSetupDetail.log_delimiter,'DN')?'checked':'' } onclick="return(false);"
										${search.isSearch!='Y'?'disabled':'' }>
									<label for="checkbox5_3"> <span></span> <span
										class="check"></span> <span class="box"></span> 다운로드로그
									</label>
								</div>
							</div>
						</td>
						<th style="text-align: center; width: 15%;"><label class="control-label">알람사용여부<span class="required">*</span> </label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle; width: 35%;">
							<div class="md-radio-inline">
								<div class="md-radio col-md-4">
									<input type="radio" id="checkbox4_1" class="md-radiobtn" id="form_control_8" title="사용" name="alarm_yn"
										value="Y" ${fn:containsIgnoreCase(extrtBaseSetupDetail.alarm_yn,'Y')?'checked':'' }
										${search.isSearch!='Y'?'disabled':'' }>
									<label for="checkbox4_1"> <span></span> <span class="check"></span> <span class="box"></span> 사용
									</label>
								</div>
								<div class="md-radio col-md-4">
									<input type="radio" id="checkbox4_2" class="md-radiobtn" id="form_control_8" title="미사용" name="alarm_yn"
										value="N" ${!fn:containsIgnoreCase(extrtBaseSetupDetail.alarm_yn,'Y')?'checked':'' }
										${search.isSearch!='Y'?'disabled':'' }>
									<label for="checkbox4_2"> <span></span> <span
										class="check"></span> <span class="box"></span> 미사용
									</label>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<th style="text-align: center;"><label class="control-label">분석위험기준<span class="required">*</span></label></th>
						<td colspan="3" class="form-group form-md-line-input" style="vertical-align: middle;">
							<input type="hidden" name="limit_type_info" value="${extrtBaseSetupDetail.limit_type }">
							<div class="md-radio-inline">
								<div class="md-radio col-md-1">
									<input type="radio" id="checkbox6_1" class="md-radiobtn" id="form_control_7" name="limit_type" onclick="radioChange(this.value)" value="1"
									${fn:containsIgnoreCase(extrtBaseSetupDetail.limit_type,'1')?'checked':'' }
									${search.isSearch!='Y'?'disabled':'' }/>
									<label for="checkbox6_1"> <span></span> <span class="check"></span> <span class="box"></span> 공통 </label>
								</div>
								<div class="md-radio col-md-1">
									<input type="radio" id="checkbox6_2" class="md-radiobtn" id="form_control_7" name="limit_type" onclick="radioChange(this.value)" value="2" 
									${fn:containsIgnoreCase(extrtBaseSetupDetail.limit_type,'2')?'checked':'' }
									${search.isSearch!='Y'?'disabled':'' }/>
									<label for="checkbox6_2"> <span></span> <span class="check"></span> <span class="box"></span> 개인별 </label>
								</div>
								<div class="md-radio col-md-1">
									<input type="radio" id="checkbox6_3" class="md-radiobtn" id="form_control_7" name="limit_type" onclick="radioChange(this.value)" value="3" 
									${fn:containsIgnoreCase(extrtBaseSetupDetail.limit_type,'3')?'checked':'' }
									${search.isSearch!='Y'?'disabled':'' }/>
									<label for="checkbox6_3"> <span></span> <span class="check"></span> <span class="box"></span> 부서별 </label>
								</div>
								<div class="md-radio col-md-1">
									<input type="radio" id="checkbox6_4" class="md-radiobtn" id="form_control_7" name="limit_type" onclick="radioChange(this.value)" value="4" 
									${fn:containsIgnoreCase(extrtBaseSetupDetail.limit_type,'4')?'checked':'' }
									${search.isSearch!='Y'?'disabled':'' }/>
									<label for="checkbox6_4"> <span></span> <span class="check"></span> <span class="box"></span> 상위N명 </label>
								</div>
								<div class="md-radio col-md-1">
									<input type="radio" id="checkbox6_5" class="md-radiobtn" id="form_control_7" name="limit_type" onclick="radioChange(this.value)" value="5"
									${fn:containsIgnoreCase(extrtBaseSetupDetail.limit_type,'5')?'checked':'' }
									${search.isSearch!='Y'?'disabled':'' }/>
									<label for="checkbox6_5"> <span></span> <span class="check"></span> <span class="box"></span> 상위N% </label>
								</div>
								<div class="md-radio col-md-1">
									<input type="radio" id="checkbox6_6" class="md-radiobtn" id="form_control_7" name="limit_type" onclick="radioChange(this.value)" value="6"
									${fn:containsIgnoreCase(extrtBaseSetupDetail.limit_type,'6')?'checked':'' }
									${search.isSearch!='Y'?'disabled':'' }/>
									<label for="checkbox6_6"> <span></span> <span class="check"></span> <span class="box"></span> 사용안함 </label>
								</div>
							</div>
						</td>
					</tr>
					
					<tr>
						<th style="text-align: center;"><label class="control-label">임계치<span class="required">*</span></label></th>
						<td colspan="3" class="form-group form-md-line-input" style="vertical-align: middle;">
							<div class="col-md-2"><h5>설정값</h5></div>
							<div class="col-md-3">
	   							<input type="text" class="form-control" name="limit_cnt" id="limit_cnt" value="${extrtBaseSetupDetail.limit_cnt }"
	   							${search.isSearch!='Y'?'readonly':'' }/>
	   						</div>
							<div class="col-md-2" id="limit_type_cnt_div" style="${fn:containsIgnoreCase(extrtBaseSetupDetail.limit_type,'4') || fn:containsIgnoreCase(extrtBaseSetupDetail.limit_type,'5')?'display: inline-block;':'display: none;' }">
							<h5>설정값(N)</h5></div>
							<div class="col-md-3" id="limit_type_cnt_div1" style="${fn:containsIgnoreCase(extrtBaseSetupDetail.limit_type,'4') || fn:containsIgnoreCase(extrtBaseSetupDetail.limit_type,'5')?'display: inline-block;':'display: none;' }">
								<input type="text" class="form-control" name="limit_type_cnt" value="${extrtBaseSetupDetail.limit_type_cnt }" ${search.isSearch!='Y'?'readonly':'' }/>
							</div>
						</td>
						
					</tr>
					<tr>
						<th style="text-align: center;"><label class="control-label">분석대상<span class="required">*</span></label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;" colspan="3">
							<div class="md-radio-inline" style="width: 60%;">
								<div class="md-radio col-md-3">
									<input type="radio" id="checkbox7_1" class="md-radiobtn" title="통합" name="indv_yn" onclick="radioChange2(this.value)"
										value="Y" ${fn:containsIgnoreCase(extrtBaseSetupDetail.indv_yn,'Y')?'checked':'' }
										${search.isSearch!='Y'?'disabled':'' }>
									<label for="checkbox7_1"> <span></span> <span
										class="check"></span> <span class="box"></span> 통합
									</label>
								</div>
								<div class="md-radio col-md-3">
									<input type="radio" id="checkbox7_2" class="md-radiobtn" title="시스템별" name="indv_yn" onclick="radioChange2(this.value)"
										value="N" ${fn:containsIgnoreCase(extrtBaseSetupDetail.indv_yn,'N')?'checked':'' }
										${search.isSearch!='Y'?'disabled':'' }>
									<label for="checkbox7_2"> <span></span> <span class="check"></span> <span class="box"></span> 시스템별
									</label>
								</div>
							</div>
							<input type="hidden" name="system_seq" value="${extrtBaseSetupDetail.system_seq }"/>
							<button type="button" class="btn" data-toggle="modal" data-target="#myModal1" id="systemBtn"
							style="${fn:containsIgnoreCase(extrtBaseSetupDetail.indv_yn,'N')?'display: inline-block;':'display: none;' }">시스템 선택</button>
							<input type="text" class="form-control" style="width: 60%; float:right; 
							${fn:containsIgnoreCase(extrtBaseSetupDetail.indv_yn,'N')?'display: inline-block;':'display: none;' }" id="system_name" value="${extrtBaseSetupDetail.system_name }" readonly="readonly"/>
						</td>
					</tr>
					<tr>
						<th style="text-align: center;"><label class="control-label">테이블접근형태<span class="required">*</span></label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<div class="md-radio-inline" style="width: 60%;">
								<div class="md-radio col-md-3">
									<input type="radio" id="checkbox8_1" class="md-radiobtn" title="1:N" name="rule_result_type" value="1" onclick="ruleResultTypeChange(this.value)"
										${fn:containsIgnoreCase(extrtBaseSetupDetail.rule_result_type,'1')?'checked':'' }
										${search.isSearch!='Y'?'disabled':'' }>
									<label for="checkbox8_1"> <span></span> <span class="check"></span> <span class="box"></span> 1:N
									</label>
								</div>
								<div class="md-radio col-md-3">
									<input type="radio" id="checkbox8_2" class="md-radiobtn" title="N:1" name="rule_result_type" value="N" onclick="ruleResultTypeChange(this.value)"
										${fn:containsIgnoreCase(extrtBaseSetupDetail.rule_result_type,'N')?'checked':'' }
										${search.isSearch!='Y'?'disabled':'' }>
									<label for="checkbox8_2"> <span></span> <span class="check"></span> <span class="box"></span> N:1
									</label>
								</div>
							</div>
						</td>
						<th style="text-align: center;"><label class="control-label">원시로그접근기준<span class="required">*</span>
							<i class="icon-question" title="테이블접근형태 N:1 선택 시 처리량 선택 불가"></i>
						</label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<div class="md-radio-inline" style="width: 60%;">
								<div class="md-radio col-md-4">
									<input type="radio" id="checkbox9_1" class="md-radiobtn" title="처리량" name="rule_view_type" value="M"
										${fn:containsIgnoreCase(extrtBaseSetupDetail.rule_view_type,'M')?'checked':'' }
										${search.isSearch!='Y'?'disabled':'' }
										${fn:containsIgnoreCase(extrtBaseSetupDetail.rule_result_type,'N')?'onclick="return(false)"':'' }>
									<label for="checkbox9_1"> <span></span> <span
										class="check"></span> <span class="box"></span> 처리량
									</label>
								</div>
								<div class="md-radio col-md-4">
									<input type="radio" id="checkbox9_2" class="md-radiobtn" title="이용량" name="rule_view_type" value="R"
										${fn:containsIgnoreCase(extrtBaseSetupDetail.rule_view_type,'R')?'checked':'' }
										${search.isSearch!='Y'?'disabled':'' }
										${fn:containsIgnoreCase(extrtBaseSetupDetail.rule_result_type,'N')?'onclick="return(false)"':'' }>
									<label for="checkbox9_2"> <span></span> <span class="check"></span> <span class="box"></span> 이용량
									</label>
								</div>
							</div>
						</td>
					</tr>
					<!--
					- 원시로그접근기준이 이용량인 경우 개인정보유형 저장 유무를 설정할 수 있고, 처리량인 경우에는 개인정보유형을 저장할 수 없으므로 저장 유무 선택 불가능. 
					- 시간, IP는 제약 없이 저장 유무 설정 할 수 있음.
					- 현재 설정하는 부분을 주석처리 한 대신, 이용량이면 개인정보유형을 저장하도록 처리함(관련 코드는 js 파일에 '개인정보유형 임시 처리' 검색 하면 나옴 ).
					- 시간, IP는 'Y'로 저장되도록 함
					<tr>
						<th style="text-align: center;"><label class="control-label">개인정보유형 저장유무<span class="required">*</span></label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<div class="md-radio-inline" >
								<div class="md-radio col-md-2">
									<input type="radio" id="checkbox10_1" class="md-radiobtn" name="result_type_yn" value="Y"
										${fn:containsIgnoreCase(extrtBaseSetupDetail.result_type_yn,'Y')?'checked':'' }
										${search.isSearch!='Y'?'disabled':'' }>
									<label for="checkbox10_1"> <span></span> <span class="check"></span> <span class="box"></span> Y
									</label>
								</div>
								<div class="md-radio col-md-2">
									<input type="radio" id="checkbox10_2" class="md-radiobtn" name="result_type_yn" value="N"
										${fn:containsIgnoreCase(extrtBaseSetupDetail.result_type_yn,'N')?'checked':'' }
										${search.isSearch!='Y'?'disabled':'' }>
									<label for="checkbox10_2"> <span></span> <span class="check"></span> <span class="box"></span> N
									</label>
								</div>
							</div>
						</td>
						<th style="text-align: center;"><label class="control-label">시간 저장유무<span class="required">*</span></label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<div class="md-radio-inline" >
								<div class="md-radio col-md-2">
									<input type="radio" id="checkbox11_1" class="md-radiobtn" name="time_view_yn" value="Y"
										${fn:containsIgnoreCase(extrtBaseSetupDetail.time_view_yn,'Y')?'checked':'' }
										${search.isSearch!='Y'?'disabled':'' }>
									<label for="checkbox11_1"> <span></span> <span class="check"></span> <span class="box"></span> Y
									</label>
								</div>
								<div class="md-radio col-md-2">
									<input type="radio" id="checkbox11_2" class="md-radiobtn" name="time_view_yn" value="N"
										${fn:containsIgnoreCase(extrtBaseSetupDetail.time_view_yn,'N')?'checked':'' }
										${search.isSearch!='Y'?'disabled':'' }>
									<label for="checkbox11_2"> <span></span> <span class="check"></span> <span class="box"></span> N
									</label>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<th style="text-align: center;"><label class="control-label">IP 저장유무<span class="required">*</span></label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<div class="md-radio-inline" >
								<div class="md-radio col-md-2">
									<input type="radio" id="checkbox12_1" class="md-radiobtn" name="ip_yn" value="Y"
										${fn:containsIgnoreCase(extrtBaseSetupDetail.ip_yn,'Y')?'checked':'' }
										${search.isSearch!='Y'?'disabled':'' }>
									<label for="checkbox12_1"> <span></span> <span class="check"></span> <span class="box"></span> Y
									</label>
								</div>
								<div class="md-radio col-md-2">
									<input type="radio" id="checkbox12_2" class="md-radiobtn" name="ip_yn" value="N"
										${fn:containsIgnoreCase(extrtBaseSetupDetail.ip_yn,'N')?'checked':'' }
										${search.isSearch!='Y'?'disabled':'' }>
									<label for="checkbox12_2"> <span></span> <span class="check"></span> <span class="box"></span> N
									</label>
								</div>
							</div>
						</td>
					</tr> -->
					<!-- START 위의 주석 처리한 부분을 UI 에서 설정하도록 한다면 코드 삭제해야함 -->
					<input type="hidden" name="result_type_yn" value="N">
					<input type="hidden" name="time_view_yn" value="Y">
					<input type="hidden" name="ip_yn" value="Y">
					<!-- END 위의 주석 처리한 부분을 UI 에서 설정하도록 한다면 코드 삭제해야함-->
					<c:if test="${extrtBaseSetupDetail.scen_seq == 1000}">
					<tr>
						<th style="text-align: center;"><label class="control-label">개인정보유형</label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;" colspan="3">
							<input type="hidden" name="privacy_seq" value="${extrtBaseSetupDetail.privacy_seq }" />
							<button type="button" class="btn" style="float:left;" data-toggle="modal" data-target="#myModal2">개인정보유형선택</button>
							<input type="text" class="form-control" style="width: 90%; float:right;" id="privacy_desc" value="${extrtBaseSetupDetail.privacy_desc }" readonly="readonly"/>
						</td>
					</tr>
					</c:if>
					<tr>
						<th style="text-align: center; vertical-align: middle;"><label class="control-label">참조값</label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;" colspan="3">
							<input type="hidden" name="ref_val" value="${extrtBaseSetupDetail.ref_val }">
							<table id="reftable" class="table table-striped table-bordered table-checkable dataTable no-footer" style="width: 80%; float: left; text-align: center;">
								<tr><td style="vertical-align: middle; width: 20%;">변수명</td>
									<td style="vertical-align: middle; width: 20%;">변수</td>
									<td style="vertical-align: middle; width: 50%;">설명 <c:if test="${extrtBaseSetupAuthId eq 'AUTH00000'}"><i class="icon-question" title="특수문자 | 와 ,(콤마) 사용 금지"></i></c:if></td>
									<c:if test="${search.isSearch=='Y'}">
									<td style="vertical-align: middle; width: 10%;">
									<button type="button" class="btn" onclick="addRefVal()">추가</button></td></c:if>
								</tr>
							<c:if test="${extrtBaseSetupDetail.ref_val != null && extrtBaseSetupDetail.ref_val!= '' }">
								<c:forEach items="${ref_val }" var="rv">
								<tr><td><input type="text" class="form-control" value="${fn:split(rv,'|')[0]}" ${search.isSearch!='Y'?'readonly':'' }/></td>
								<td><input type="text" class="form-control" value="${fn:split(rv,'|')[1]}" ${search.isSearch!='Y'?'readonly':'' }/></td>
								<td><input type="text" class="form-control" value="${fn:split(rv,'|')[2]}" ${search.isSearch!='Y'?'readonly':'' }/></td>
								<c:if test="${extrtBaseSetupAuthId eq 'AUTH00000'}"><td><button type="button" class="btn" onclick="removeRefVal(this.parentElement)">삭제</button></td></c:if>
								</tr>
								</c:forEach>
							</c:if>
							</table>
						</td>
					</tr>
					<tr>
						<c:choose>
							<c:when test="${ui_type == 'Shcd'}">
								<th style="text-align: center; vertical-align: middle;"><label class="control-label">소명 메일 내용</label></th>							
							</c:when>
							<c:otherwise>
								<th style="text-align: center; vertical-align: middle;"><label class="control-label">시나리오 상세설명</label></th>	
							</c:otherwise>
						</c:choose>
						<td class="form-group form-md-line-input" style="text-align: center; vertical-align: middle;" colspan="3">
							<textarea name="script_desc" class="form-control" rows="5" style="width: 99%" ${search.isSearch!='Y'?'readonly':'' }>${extrtBaseSetupDetail.script_desc }</textarea>
							<div class="form-control-focus"></div></td>
					</tr>
					<c:if test="${search.isSearch=='Y' }">
						<tr>
							<th style="text-align: center; vertical-align: middle;"><label class="control-label">추출스크립트 </label></th>
							<td class="form-group form-md-line-input" style="text-align: center; vertical-align: middle;"
								colspan="3"><textarea name="script" class="form-control"
									rows="20" style="width: 99%">${extrtBaseSetupDetail.script }</textarea>
								<div class="form-control-focus"></div></td>
						</tr>
					</c:if>
				</tbody>
			</table>
			<input type="hidden" id="extrtBaseSetupAuthId" value="${extrtBaseSetupAuthId }">
			<input type="hidden" name="search_from">
			<!-- 메뉴 관련 input 시작 -->
			<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" />
			<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
			<input type="hidden" name="current_menu_id" value="${currentMenuId }" />
			<input type="hidden" name="seq" value="${search.seq }" />
			<!-- 메뉴 관련 input 끝 -->

			<!-- 페이지 번호 -->
			<input type="hidden" name="page_num" value="${search.page_num }" />
			<!-- 검색조건 관련 input 시작 -->
			<input type="hidden" name="use_yn_search" value="${search.use_yn_search}" />
			<input type="hidden" name="is_realtime_extract_search" value="${search.is_realtime_extract_search }" />
			<input type="hidden" name="alarm_yn_search" value="${search.alarm_yn_search}" />
			<input type="hidden" name="isSearch" value="${search.isSearch}" />
			<input type="hidden" name="isSearchDetail" value="${paramBean.isSearchDetail}" />
			<!-- 검색조건 관련 input 끝 -->
		</form>
		<!-- END FORM-->

		<!-- 버튼 영역 -->
		<div class="form-actions">
			<div class="row" >
				<div align="right" style="padding-right: 20px;">
					<button type="button" class="btn btn-sm grey-mint btn-outline sbold uppercase"
						onclick="moveextrtBaseSetupListtoDetail();">
						<i class="fa fa-list"></i> &nbsp;목록
					</button>
				<c:if test="${search.isSearch=='Y' }">
					<c:choose>
						<c:when test="${extrtBaseSetupDetail.rule_seq==0}">
							<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="addextrtBaseSetup()"><i
								class="fa fa-check"></i>&nbsp;추가</a>
						</c:when>
						<c:otherwise>
							<button type="button" class="btn btn-sm blue btn-outline sbold uppercase" onclick="simulationForm()">시뮬레이션</button>
							<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="saveextrtBaseSetup()"><i
								class="fa fa-check"></i>&nbsp;저장</a>
							<a class="btn btn-sm red-mint btn-outline sbold uppercase" onclick="removeextrtBaseSetup()"><i
								class="fa fa-remove"></i>&nbsp;삭제</a>
						</c:otherwise>
					</c:choose>
				</c:if>
				</div>
			</div>
		</div>
		
		<div id="simul" style="display: none; margin-top: 20px;">
			<div class="caption">
				<i class="icon-settings font-dark"></i> <span class="caption-subject bold uppercase font-dark">시뮬레이션
				</span>&nbsp;&nbsp;&nbsp;<span> ※ 저장된 설정값으로 시뮬레이션이 실행됩니다. 설정 값 변경 시 저장 후 실행해주세요.</span>
			</div>
			<table class="table table-bordered" style="width: 30%; margin-top:15px;">
				<th width="30%" style="vertical-align: middle; text-align: center;">추출일</th>
				<td width="60%"><div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
				<input type="text" class="form-control" id="search_fr" autocomplete="off"> 
			</div></td>
				<td width="10%">
					<button class="btn" onclick="simulate()">실행</button>
				</td>
			</table>
			<div id="simulateResult">
				<table id="tempTable" class="table table-bordered table-striped" >
					<thead>
						<tr>
							<th style="text-align: center;">날짜</th>
							<th style="text-align: center;">취급자ID</th>
							<th style="text-align: center;">취급자명</th>
							<th style="text-align: center;">부서ID</th>
							<th style="text-align: center;">부서명</th>
							<th style="text-align: center;">시스템명</th>
							<th style="text-align: center;">임계치</th>
							<th style="text-align: center;">카운트</th>
							<th style="text-align: center;">비고</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
		
	</div>
</div>

<!-- 시스템 -->
<div class="modal fade" id="myModal1" role="dialog">
	<div class="modal-dialog" style="position: relative ;left: 0;">
		<!-- Modal content -->
		<div class="modal-content">
			<div class="modal-header" style="background-color: #32c5d2;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">
					<b style="font-size: 13pt;">대상 시스템</b>
				</h4>
			</div>
			<div class="modal-body" style="background-color: #F9FFFF;">
				<table class="table table-striped table-bordered table-hover table-checkable dataTable no-footer">
					<thead><tr>
						<th style="vertical-align: middle; text-align: center;">선택 <input type="checkbox" id="systemAll" onclick="checkAll1()"></th>
						<th style="vertical-align: middle; text-align: center;">시스템명</th>
					</tr></thead>
					<tbody>
						<c:set value="0" var="no"/>
						<c:forEach items="${systemList }" var="sl">
						<tr>
							<td style="vertical-align: middle; text-align: center;">
								<c:choose>
								<c:when test="${fn:split(extrtBaseSetupDetail.system_seq, ',')[no] == sl.system_seq}">
									<input type="checkbox" id="system_seq" value="${sl.system_seq }" checked="checked">
									<c:if test="${systemSeqLength > no }">
									<c:set value="${no+1 }" var="no"/></c:if>
								</c:when>
								<c:otherwise>
									<input type="checkbox" id="system_seq" value="${sl.system_seq }">
								</c:otherwise>
								</c:choose>
							</td>
							<td style="vertical-align: middle; text-align: center;" onclick="checkSystem(this.parentElement)">${sl.system_name }</td>
						</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<a class="btn btn-sm blue" onclick="systemCheckbox()" data-dismiss="modal"> <i class="fa fa-check"></i> 선택 </a>
			</div>
		</div>
	</div>
</div>
<!-- 개인정보유형 -->
<div class="modal fade" id="myModal2" role="dialog">
	<div class="modal-dialog" style="position: relative ;left: 0;">
		<!-- Modal content -->
		<div class="modal-content">
			<div class="modal-header" style="background-color: #32c5d2;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">
					<b style="font-size: 13pt;">개인정보유형</b>
				</h4>
			</div>
			<div class="modal-body" style="background-color: #F9FFFF;">
				<table class="table table-striped table-bordered table-hover table-checkable dataTable no-footer">
					<thead><tr>
						<th style="vertical-align: middle; text-align: center;">선택 <input type="checkbox" id="privacyAll" onclick="checkAll2()"></th>
						<th style="vertical-align: middle; text-align: center;">개인정보유형</th>
					</tr></thead>
					<tbody>
					<c:set value="0" var="no1"/>
					<c:forEach items="${indvinfoTypeList }" var="il">
					<tr>
						<td style="vertical-align: middle; text-align: center;">
							<c:choose>
							<c:when test="${fn:split(extrtBaseSetupDetail.privacy_seq, ',')[no1] == il.privacy_seq}">
								<input type="checkbox" id="privacy_seq" value="${il.privacy_seq }" checked="checked">
								<c:if test="${indvinfoLength > no1 }">
								<c:set value="${no1+1 }" var="no1"/></c:if>
							</c:when>
							<c:otherwise>
								<input type="checkbox" id="privacy_seq" value="${il.privacy_seq }">
							</c:otherwise>
							</c:choose>
						</td>
						<td style="vertical-align: middle; text-align: center;" onclick="checkIndv(this.parentElement)">${il.privacy_desc }</td>
					</tr>
					</c:forEach>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<a class="btn btn-sm blue" onclick="indivCheckbox()" data-dismiss="modal"> <i class="fa fa-check"></i> 선택 </a>
			</div>
		</div>
	</div>
</div>

<form id="moveForm" method="POST">
<input type="hidden" name="scen_seq"/>
</form>
<form id="listForm" method="POST">
<input type="hidden" name="scen_seq" value="${search.scen_seq}"/>
<input type="hidden" name="indv_yn" value="${search.indv_yn}"/>
<input type="hidden" name="use_yn_search" value="${search.use_yn_search}"/>
<input type="hidden" name="alarm_yn_search" value="${search.alarm_yn_search}"/>
<input type="hidden" name="isSearch" value="${search.isSearch}"/>
</form>
<script type="text/javascript">
	var extrtBaseSetupConfig = {
		"listUrl" : "${rootPath}/extrtBaseSetup/list.html",
		"addUrl" : "${rootPath}/extrtBaseSetup/add.html",
		"saveUrl" : "${rootPath}/extrtBaseSetup/save.html", 
		"removeUrl" : "${rootPath}/extrtBaseSetup/remove.html",
		"saveIndvUrl" : "${rootPath}/extrtBaseSetup/setAllindv.html",
		"detailUrl" : "${rootPath}/extrtBaseSetup/detail.html"
	};
</script>