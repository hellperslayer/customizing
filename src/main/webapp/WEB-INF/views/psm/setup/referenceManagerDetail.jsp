<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/setup/referenceManagerList.js" type="text/javascript" charset="UTF-8"></script>
<h1 class="page-title">
	${currentMenuName}
	<!--bold font-blue-hoki <small>basic bootstrap tables with various options and styles</small> -->
</h1>
<div class="portlet light portlet-fit bordered">
	<div class="portlet-body">
		<!-- BEGIN FORM-->
		<form id="indvinfoTypeSetupDetailForm" action="" method="POST"
			class="form-horizontal form-bordered form-row-stripped">
			<table class="table table-bordered table-striped">
				<tr>
					<c:forEach items="${column_nameList}" var="columnName" varStatus="status">
						<th style="text-align: center; vertical-align: middle;"">
							<label class="control-label">${columnName }</label>
						</th>
					</c:forEach>
				</tr>
				<tr>
					<c:forEach items="${column_nameList}" var="columnName" varStatus="status">
						<td style="vertical-align: middle;" class="form-group form-md-line-input">
							<input type="text" name="input_data_${status.count }" class="form-control" style="text-align: center; vertical-align: middle;" value="${referenceManagerDetail[columnName] }" />
							<div class="form-control-focus"></div>
						</td>
					</c:forEach>
				</tr>
			</table>



			<!-- 메뉴 관련 input 시작 -->
			<input type="hidden" name="main_menu_id"
				value="${paramBean.main_menu_id }" /> <input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" /> 
				<input type="hidden" name="current_menu_id" value="${currentMenuId }" /> 
				<input type="hidden" name="system_seq" value="${search.system_seq }" />
				<input type="hidden" name="table_name" value="${search.table_name }" />
				<input type="hidden" name="item_values" value="" /> 
				<input type="hidden" name="result" value="" /> 

			<!-- 메뉴 관련 input 끝 -->

			<!-- 페이지 번호 -->
			<input type="hidden" name="page_num" value="${search.page_num }" />
			<input type="hidden" name="search_datas" value="${referenceManagerDetail['pKeyDatas'] }" />

			<!-- 검색조건 관련 input 시작 -->
			<input type="hidden" name="privacy_desc_search"
				value="${search.privacy_desc_search}" /> <input type="hidden"
				name="use_flag_search" value="${search.use_flag_search }" />
			<input type="hidden" name="isSearch" value="${paramBean.isSearch }" />
			<!-- 검색조건 관련 input 끝 -->
		</form>
		<!-- END FORM-->

		<!-- 버튼 영역 -->
		<div class="form-actions">
			<div class="row">
				<div class="col-md-offset-2 col-md-10" align="right"
					style="padding-right: 30px;">
					<button type="button" class="btn btn-sm grey-mint btn-outline sbold uppercase"
						onclick="javascript:moveindvinfoTypeSetupListtoDetail();">
						<i class="fa fa-list"></i> &nbsp;목록
					</button>

					<c:choose>
						<c:when test="${empty referenceManagerDetail}">
							<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="addindvinfoTypeSetup()"><i
								class="fa fa-check"></i>&nbsp;추가</a>
						</c:when>
						<c:otherwise>
							<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="saveReferenceTableColumnInfo()">
							<iclass="fa fa-check"></i>&nbsp;저장</a>
							<a class="btn btn-sm red btn-outline sbold uppercase" onclick="removeindvinfoTypeSetup()">
							<iclass="fa fa-check"></i>&nbsp;삭제</a>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var referenceManagerConfig = {
		"listUrl" : "${rootPath}/indvinfoTypeSetup/referenceManagerDetailList.html",
		"addUrl" : "${rootPath}/indvinfoTypeSetup/add.html",
		"saveUrl" : "${rootPath}/indvinfoTypeSetup/referenceManagerDetailsave.html",
		"removeUrl" : "${rootPath}/indvinfoTypeSetup/removeReferenceManagerDetail.html",
		"loginPage" : "${rootPath}/loginView.html"
	};
	
</script>