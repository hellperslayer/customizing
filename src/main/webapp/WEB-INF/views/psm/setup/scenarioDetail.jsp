<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="extrtBaseSetupAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/setup/extrtBaseSetupScenario.js" type="text/javascript" charset="UTF-8"></script>
<h1 class="page-title">${currentMenuName}</h1>
<div class="portlet light portlet-fit bordered">
	<div class="portlet-body">
		<!-- BEGIN FORM-->
		<form id="detailForm" action="" method="POST" class="form-horizontal form-bordered form-row-stripped">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
							<th style="width: 20%; text-align: center;">
								<label class="control-label">시나리오코드<span class="required">*</span></label>
							</th>
							<td style="width: 80%; vertical-align: middle;" class="form-group form-md-line-input"> 
								<input type="text" class="form-control" name="scen_seq" value="${scenarioInfo.scen_seq }" 
								${!empty scenarioInfo?'readonly':'' }/>
								<div class="form-control-focus"></div>
							</td>
					</tr>
					<tr>
							<th style="width: 15%; text-align: center;">
								<label class="control-label">시나리오명<span class="required">*</span></label>
							</th>
							<td style="width: 35%; vertical-align: middle;" class="form-group form-md-line-input"> 
								<input type="text" class="form-control" name="scen_name" value="${scenarioInfo.scen_name }" />
								<div class="form-control-focus"></div>
							</td>
					</tr>
					<tr>
						<th style="text-align: center;"><label class="control-label">복합위험도<span class="required">*</span></label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<input type="number" class="form-control" name="dng_val" value="${scenarioInfo.dng_val }" style="width: 50%; float:left;"/>
							<input type="hidden" name="dngValRest" value="${100-dngValSum }">
							<div>전체 시나리오 위험도 합은 ${dngValSum } 입니다.<br/>(설정할 수 있는 위험도는 <c:out value="${100-dngValSum }"/> 이하입니다.)</div>
						</td>
					</tr>
					<tr>
						<th style="width:20%; text-align: center;"><label class="control-label" for="form_control_2">주체<span class="required">*</span></label></th>
						<td class="form-group form-md-line-input" style="width:80%; vertical-align: middle;">
							<div class="md-radio-inline">
								<div class="md-radio col-md-3">
									<input type="radio" id="checkbox5_1" class="md-radiobtn" id="form_control_7" title="접속기록조회" name="log_delimiter"
										value="BA" ${fn:containsIgnoreCase(scenarioInfo.log_delimiter,'BA') || fn:containsIgnoreCase(scenarioInfo.log_delimiter,'')?'checked':'' }>
									<label for="checkbox5_1"> <span></span> <span
										class="check"></span> <span class="box"></span> 접속기록조회
									</label>
								</div>
								<div class="md-radio col-md-3">
									<input type="radio" id="checkbox5_2" class="md-radiobtn" id="form_control_7" title="DB접근조회" name="log_delimiter"
										value="DB" ${fn:containsIgnoreCase(scenarioInfo.log_delimiter,'DB')?'checked':'' }>
									<label for="checkbox5_2"> <span></span> <span
										class="check"></span> <span class="box"></span> DB접근조회
									</label>
								</div>
								<div class="md-radio col-md-3">
									<input type="radio" id="checkbox5_3" class="md-radiobtn" id="form_control_7" title="다운로드로그" name="log_delimiter"
										value="DN" ${fn:containsIgnoreCase(scenarioInfo.log_delimiter,'DN')?'checked':'' }>
									<label for="checkbox5_3"> <span></span> <span
										class="check"></span> <span class="box"></span> 다운로드로그
									</label>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<th style="text-align: center;"><label class="control-label">분석위험기준<span class="required">*</span></label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<div class="md-radio-inline">
								<div class="md-radio">
									<input type="radio" id="checkbox6_1" class="md-radiobtn" id="form_control_7" name="limit_type" onclick="radioChange(this.value)" value="1" 
									${fn:containsIgnoreCase(scenarioInfo.limit_type,'1')||fn:containsIgnoreCase(scenarioInfo.limit_type,'6')||fn:containsIgnoreCase(scenarioInfo.limit_type,'')?'checked':'' }>
									<label for="checkbox6_1"> <span></span> <span
										class="check"></span> <span class="box"></span> 공통
									</label>
								</div>
								<div class="md-radio">
									<input type="radio" id="checkbox6_2" class="md-radiobtn" id="form_control_7" name="limit_type" onclick="radioChange(this.value)" value="2" 
									${fn:containsIgnoreCase(scenarioInfo.limit_type,'2')?'checked':'' }>
									<label for="checkbox6_2"> <span></span> <span
										class="check"></span> <span class="box"></span> 개인별
									</label>
								</div>
								<div class="md-radio">
									<input type="radio" id="checkbox6_3" class="md-radiobtn" id="form_control_7" name="limit_type" onclick="radioChange(this.value)" value="3" 
									${fn:containsIgnoreCase(scenarioInfo.limit_type,'3')?'checked':'' }>
									<label for="checkbox6_3"> <span></span> <span
										class="check"></span> <span class="box"></span> 부서별
									</label>
								</div>
								<div class="md-radio">
									<input type="radio" id="checkbox6_4" class="md-radiobtn" id="form_control_7" name="limit_type" onclick="radioChange(this.value)" value="4" 
									${fn:containsIgnoreCase(scenarioInfo.limit_type,'4')?'checked':'' }>
									<label for="checkbox6_4"> <span></span> <span
										class="check"></span> <span class="box"></span> 상위N명
									</label>
								</div>
								<div class="md-radio">
									<input type="radio" id="checkbox6_5" class="md-radiobtn" id="form_control_7" name="limit_type" onclick="radioChange(this.value);" value="5"
									${fn:containsIgnoreCase(scenarioInfo.limit_type,'5')?'checked':'' }>
									<label for="checkbox6_5"> <span></span> <span
										class="check"></span> <span class="box"></span> 상위N%
									</label>
								</div>
							</div>
						</td>
					</tr>
					
					<tr>
						<th style="text-align: center;"><label class="control-label">임계치<span class="required">*</span></label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<div class="col-md-2"><h5>설정값</h5></div>
							<div class="col-md-3">
	   							<input type="number" class="form-control" name="limit_cnt" value="${scenarioInfo.limit_cnt }"/></div>
							<div class="col-md-2" id="limit_type_cnt_div" style="${fn:containsIgnoreCase(scenarioInfo.limit_type,'4') || fn:containsIgnoreCase(scenarioInfo.limit_type,'5')?'display: inline-block;':'display: none;' }">
							<h5>설정값(N)</h5></div>
							<div class="col-md-3" id="limit_type_cnt_div1" style="${fn:containsIgnoreCase(scenarioInfo.limit_type,'4') || fn:containsIgnoreCase(scenarioInfo.limit_type,'5')?'display: inline-block;':'display: none;' }">
								<input type="number" class="form-control" name="limit_type_cnt" value="${scenarioInfo.limit_type_cnt }"/></div>
						</td>
					</tr>
					<tr>
						<th style="text-align: center;"><label class="control-label">분석대상<span class="required">*</span></label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<div class="md-radio-inline" style="float: left; width: 40%;">
								<div class="md-radio col-md-3">
									<input type="radio" id="checkbox7_1" class="md-radiobtn" title="통합" name="indv_yn" onclick="radioChange2(this.value)"
										value="Y" ${fn:containsIgnoreCase(scenarioInfo.indv_yn,'Y') || fn:containsIgnoreCase(scenarioInfo.indv_yn,'na') || fn:containsIgnoreCase(scenarioInfo.indv_yn,'')?'checked':'' }>
									<label for="checkbox7_1"> <span></span> <span
										class="check"></span> <span class="box"></span> 통합
									</label>
								</div>
								<div class="md-radio col-md-3">
									<input type="radio" id="checkbox7_2" class="md-radiobtn" title="시스템별" name="indv_yn" onclick="radioChange2(this.value)"
										value="N" ${fn:containsIgnoreCase(scenarioInfo.indv_yn,'N')?'checked':'' }>
									<label for="checkbox7_2"> <span></span> <span class="check"></span> <span class="box"></span> 시스템별
									</label>
								</div>
							</div>
							<input type="hidden" name="system_seq" value="${scenarioInfo.system_seq }"/>
							<button type="button" class="btn" data-toggle="modal" data-target="#myModal1" id="systemBtn"
							style="${fn:containsIgnoreCase(scenarioInfo.indv_yn,'N')?'display: inline-block;':'display: none;' }">시스템 선택</button>
							<input type="text" class="form-control" readonly="readonly" style="width: 50%; float:right; 
							${fn:containsIgnoreCase(scenarioInfo.indv_yn,'N')?'display: inline-block;':'display: none;' }" id="system_name" value="${scenarioInfo.system_name }" />
						</td>
					</tr>
				</tbody>
			</table>

		</form>
		<!-- END FORM-->

		<!-- 버튼 영역 -->
		<div class="form-actions">
			<div class="row">
				<div class="col-md-offset-2 col-md-10" align="right" style="padding-right: 30px;">
					<button type="button" class="btn btn-sm grey-mint btn-outline sbold uppercase"
						onclick="moveList()">
						<i class="fa fa-list"></i> &nbsp;목록
					</button>
					<c:choose>
						<c:when test="${empty scenarioInfo}">
							<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="addextrtBaseSetup()"><i
								class="fa fa-check"></i>&nbsp;추가</a>
						</c:when>
						<c:otherwise>
							<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="saveextrtBaseSetup()"><i
								class="fa fa-check"></i>&nbsp;저장</a>
							<a class="btn btn-sm red-mint btn-outline sbold uppercase" onclick="removeScenario()"><i
								class="fa fa-remove"></i>&nbsp;삭제</a>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="myModal1" role="dialog">
	<div class="modal-dialog" style="position: relative ;left: 0;">
		<!-- Modal content -->
		<div class="modal-content">
			<div class="modal-header" style="background-color: #32c5d2;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">
					<b style="font-size: 13pt;">대상 시스템</b>
				</h4>
			</div>
			<div class="modal-body" style="background-color: #F9FFFF;">
				<table class="table table-striped table-bordered table-hover table-checkable dataTable no-footer">
					<thead><tr>
						<th style="vertical-align: middle; text-align: center;">선택</th>
						<th style="vertical-align: middle; text-align: center;">시스템명</th>
					</tr></thead>
					<tbody>
						<c:set value="0" var="no"/>
						<c:forEach items="${systemList }" var="sl">
						<tr>
							<td style="vertical-align: middle; text-align: center;">
								<c:choose>
								<c:when test="${fn:split(scenarioInfo.system_seq, ',')[no] == sl.system_seq}">
									<input type="checkbox" id="system_seq" value="${sl.system_seq }" checked="checked">
									<c:if test="${systemSeqLength > no }">
									<c:set value="${no+1 }" var="no"/></c:if>
								</c:when>
								<c:otherwise>
									<input type="checkbox" id="system_seq" value="${sl.system_seq }">
								</c:otherwise>
								</c:choose>
							</td>
							<td style="vertical-align: middle; text-align: center;" onclick="checkSystem(this.parentElement)">${sl.system_name }</td>
						</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<a class="btn btn-sm blue" onclick="systemCheckbox()" data-dismiss="modal"> <i class="fa fa-check"></i> 선택 </a>
			</div>
		</div>
	</div>
</div>
<form id="listForm" action="" method="POST" ></form>
<script type="text/javascript">
var scenarioConfig = {
		"scenarioUrl" : "${rootPath}/extrtBaseSetup/scenarioList.html",
		"detailUrl" : "${rootPath}/extrtBaseSetup/list.html",
		"addUrl" : "${rootPath}/extrtBaseSetup/addScenario.html",
		"saveUrl" : "${rootPath}/extrtBaseSetup/saveScenario.html",
		"removeUrl" : "${rootPath}/extrtBaseSetup/removeScenario.html"
	};
</script>