<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/setup/extrtBaseSetupScenario.js" type="text/javascript" charset="UTF-8"></script>
<h1 class="page-title">${currentMenuName}</h1>


<div class="portlet light portlet-fit portlet-datatable bordered">
	<div class="portlet-body">
		<div class="table-container">
			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
				<div>
					<div class="col-md-6"
						style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
						<div class="portlet box grey-salsa">
							<div class="portlet-title" style="background-color: #2B3643;">
								<div class="caption">
									<i class="fa fa-search"></i>
									검색
								</div>
								<div class="tools">
					                 <a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
					             </div>
							</div>


								<div id="searchBar" class="portlet-body" >
								<form id="listForm" method="POST" class="portlet-body" >
									<div class="table-container">
										<div id="datatable_ajax_2_wrapper"
											class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
											
											<div class="row" style="padding-bottom: 0px; padding-left:20px;">
												<div class="col-md-2">
													<label class="control-label">시나리오명</label></br>
													<div class="col-md-9" style="padding: 0px;">
														<input class="form-control input-large" type="text"
															name="scen_name" value="${search.scen_name}" />
													</div>
												</div>
											</div>
											<hr/>
											<div align="right">
												<button type="reset"
													class="btn btn-sm red-mint btn-outline sbold uppercase"
													onclick="resetOptions(scenarioConfig['listUrl'])">
													<i class="fa fa-remove"></i> 초기화
												</button>
												<button class="btn btn-sm blue btn-outline sbold uppercase"
													onclick="moveScenarioList()">
													<i class="fa fa-check"></i> 검색
												</button>
											</div>
										</div>
									</div>
									<input type="hidden" name="scen_seq" value="" /> 
									<input type="hidden" name="seq" value="" />
									<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" /> 
									<input type="hidden" name="sub_menu_id"	value="${paramBean.sub_menu_id }" /> 
									<input type="hidden" name="current_menu_id" value="${currentMenuId}" /> 
									<input type="hidden" name="isSearch" value="${paramBean.isSearch}" />
								</form>
								</div>

						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12" align="left">
						<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="detailScenario()"><i class="fa fa-plus"></i> 신규 </a>
					</div>
				</div>
				<div>
					<table style="border-top: 1px solid #e7ecf1"
						class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
						style="position: absolute; top: 0px; left: 0px; width: 100%;">
						<thead>
							<tr>
								<th width="5%" style="text-align: center;">번호</th>
								<th width="30%" style="text-align: center;">시나리오명</th>
								<th width="15%" style="text-align: center;">주체</th>
								<th width="10%" style="text-align: center;">분석위험기준</th>
								<th width="10%" style="text-align: center;">복합위험도</th>
								<th width="10%" style="text-align: center;">분석대상</th>
								<th width="20%" style="text-align: center;">이동</th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${empty scenarioList}">
									<tr>
										<td colspan="6" align="center">데이터가 없습니다.</td>
									</tr>
								</c:when>
								<c:otherwise>
									<c:forEach items="${scenarioList }" var="scenario" varStatus="status">
										<tr>
											<%-- <td style="text-align: center; vertical-align: middle;">${status.count }</td> --%>
											<td style="text-align: center; vertical-align: middle;">${scenario.scen_seq }</td>
											<td style="vertical-align: middle;">${scenario.scen_name }</td>
											<td style="text-align: center; vertical-align: middle;">
												<c:choose>
													<c:when test="${scenario.log_delimiter =='BA' }">접속기록조회</c:when>
													<c:when test="${scenario.log_delimiter =='DB' }">DB접근조회</c:when>
													<c:when test="${scenario.log_delimiter =='DN' }">다운로드로그</c:when>
												</c:choose>
											</td>
											<td style="text-align: center; vertical-align: middle;">
												<c:choose>
													<c:when test="${scenario.limit_type ==1 }">공통</c:when>
													<c:when test="${scenario.limit_type ==2 }">개인별</c:when>
													<c:when test="${scenario.limit_type ==3 }">부서별</c:when>
													<c:when test="${scenario.limit_type ==4 }">순위</c:when>
													<c:when test="${scenario.limit_type ==5 }">비율</c:when>
												</c:choose>
											</td>
											<td style="text-align: center; vertical-align: middle;">${scenario.dng_val }</td>
											<td style="text-align: center; vertical-align: middle;">
												<c:choose>
													<c:when test="${scenario.indv_yn =='Y' }">통합</c:when>
													<c:when test="${scenario.indv_yn =='N' }">시스템별</c:when>
												</c:choose>
											</td>
											
											<td style="text-align: center;">
												<button class="btn btn-sm blue btn-outline sbold uppercase" onclick="fnextrtBaseSetup(${scenario.scen_seq}, '${scenario.scen_name}')">
													<i class="fa fa-check"></i>상세시나리오
												</button>
												<button class="btn btn-sm blue btn-outline sbold uppercase" onclick="detailScenario(${scenario.scen_seq})">
													<i class="fa fa-check"></i> 마스터정책수정
												</button>
											</td>
										</tr>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</tbody>
					</table>
				</div>
				
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">

var scenarioConfig = {
		"listUrl" : "${rootPath}/extrtBaseSetup/scenarioList.html",
		"detailUrl" : "${rootPath}/extrtBaseSetup/list.html",
		"scenarioDetail" : "${rootPath}/extrtBaseSetup/scenarioDetail.html"
	};

	var menu_id = scenarioConfig["menu_id"];
	var log_message = "SUCCESS";
	var log_action = "SELECT";

</script>