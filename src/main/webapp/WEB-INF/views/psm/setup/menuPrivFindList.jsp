<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>

<%-- <c:set var="adminUserAuthId" value="${userSession.auth_id}" /> --%>
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/setup/menuPrivFindList.js" type="text/javascript" charset="UTF-8"></script>
<link href="${rootPath}/resources/css/profile.min.css" rel="stylesheet" type="text/css">


<h1 class="page-title"> ${currentMenuName}
	<!--bold font-blue-hoki <small>basic bootstrap tables with various options and styles</small> -->
</h1>
<div class="portlet light portlet-fit bordered">
	<div class="portlet-body">
		<!-- BEGIN FORM-->
		<form id="menuPrivFindListForm" action="" method="POST"
			class="form-horizontal form-bordered form-row-stripped">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<th style="width: 15%; text-align: center;"><label class="control-label">대상URL </label></th>
						<c:choose>
							<c:when test="${manuMappDetail.parameter != null }">
								<td style="width: 35%; vertical-align: middle;" class="form-group form-md-line-input" colspan="3">
									${menuMappDetail.url}
								</td>
								<th style="width: 15%; text-align: center;"><label class="control-label">파라미터 </label></th>
								<td style="width: 35%; vertical-align: middle;" class="form-group form-md-line-input" colspan="3">
									${menuMappDetail.parameter}
								</td>
							</c:when>
							<c:otherwise>
								<td style="width: 85%; vertical-align: middle;" class="form-group form-md-line-input" colspan="3">
									${menuMappDetail.url}
								</td>
							</c:otherwise>
						</c:choose>
						
					</tr>
					<tr>
						<th style="width: 15%; text-align: center;"><label class="control-label">개인정보유형 </label></th>
						<td style="width: 35%; vertical-align: middle;" class="form-group form-md-line-input">
							<select id="privacy_type" name="privacy_type" class="form-control">
								<option value="">----- 선택 -----</option>
								<c:if test="${empty CACHE_RESULT_TYPE}">
									<option>데이터 없음</option>
								</c:if>
								<c:if test="${!empty CACHE_RESULT_TYPE}">
									<c:forEach items="${CACHE_RESULT_TYPE}" var="i" varStatus="z">
										<option value="${i.key}">${ i.value}</option>
									</c:forEach>
								</c:if>
							</select>
						</td>
						<th style="width: 15%; text-align: center;"><label class="control-label">그룹번호 </label></th>
						<td style="width: 35%;" class="form-group form-md-line-input">
							<input type="text" name="get_group_num" class="form-control" style="vertical-align: middle;" value="" />
							<div class="form-control-focus"></div>
						</td>
					</tr>
					<tr>
						<th style="text-align: center; vertical-align: middle;">
							<label class="control-label">정규식</label>
						</th>
						<td style="vertical-align: middle;" class="form-group form-md-line-input" colspan="3">
							<textarea name="regular_expression" class="form-control" rows="7"></textarea>
							<div class="form-control-focus"></div>
						</td>
					</tr>
					<tr>
						<th style="text-align: center;"><label class="control-label">사용여부</label></th>
						<td class="form-group form-md-line-input"
							style="vertical-align: middle;">
							<div class="md-radio-inline">
								<div class="md-radio">
									<input type="radio" id="checkbox1_1" name="use_flag" value="Y" class="md-radiobtn" checked="checked">
									<label for="checkbox1_1">
									<span></span> <span class="check"></span> <span class="box"></span> 사용
									</label>
								</div>
								<div class="md-radio">
									<input type="radio" id="checkbox1_2" name="use_flag" value="N" class="md-radiobtn">
									<label for="checkbox1_2"> 
										<span></span> <span class="check"></span> <span class="box"></span> 미사용
									</label>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		
		<!-- END FORM-->
		
		<!-- 버튼 영역 -->
		<div class="form-actions">
			<div class="row">
				<div class="col-md-offset-2 col-md-10" align="right"
					style="padding-right: 30px;">
					<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="addMenuPrivFind()" id="addBtn">
						<i class="fa fa-check"></i>&nbsp;추가</a>
					<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="saveMenuPrivFind()" id="saveBtn" style="display: none;">
						<i class="fa fa-check"></i>&nbsp;저장</a>
					<a class="btn btn-sm red btn-outline sbold uppercase" onclick="clearOption()" id="clearBtn" style="display: none;">
						<i class="fa fa-remove"></i>&nbsp;초기화</a>
				</div>
			</div>
		</div>
		<div class="row" style="padding-top:10px;">
			<div class="col-md-12">
				<table class="table table-striped table-bordered table-hover table-checkable dataTable no-footer">
					<tbody>
						<tr>
							<th style="width:15%;text-align: center;">개인정보유형</th>
							<th style="width:55%;text-align: center;">정규식</th>
							<th style="width:10%;text-align: center;">그룹번호</th>
							<th style="width:10%;text-align: center;">사용여부</th>
							<th style="width:10%;text-align: center;">기타</th>
						</tr>
						
						<c:choose>
							<c:when test="${empty menuPrivFindList}">
								<tr>
									<td colspan="5" align="center">데이터가 없습니다.</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:set value="${search.total_count}" var="count"/>
								<c:forEach items="${menuPrivFindList}" var="menuPrivFind"  varStatus="status">
									<tr>
<%-- 										<td align="center">${count - search.page_num * search.size + search.size }</td> --%>
										<td align="center">${menuPrivFind.privacy_type_str }</td>
										<td align="center"><c:out value="${menuPrivFind.regular_expression }" escapeXml="true"></c:out></td>
										<td align="center">${menuPrivFind.get_group_num }</td>
										<td align="center">
											<c:choose>
												<c:when test="${menuPrivFind.use_flag eq 'Y'}">사용</c:when>
												<c:otherwise>미사용</c:otherwise>
											</c:choose>
											
										</td>
										<td align="center">
											<a class="btn btn-sm blue btn-outline sbold uppercase"
												onclick="updateMenuPrivFindForm('${menuPrivFind.tag_regexp_seq}','${menuPrivFind.privacy_type}','${menuPrivFind.regular_expression}'
													,'${menuPrivFind.get_group_num}','${menuPrivFind.use_flag}')"><i
											class="fa fa-pencil"></i>&nbsp;수정</a>
											<a class="btn btn-sm red-mint btn-outline sbold uppercase" onclick="removeMenuPrivFind('${menuPrivFind.tag_regexp_seq}')"><i
											class="fa fa-remove"></i>&nbsp;삭제</a>
										</td>
									</tr>
								<c:set var="count" value="${count - 1 }"/>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
					
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12" align="center">
				<!-- 페이징 영역 -->
				<c:if test="${search.total_count > 0}">
					<div id="pagingframe">
						<p><ctl:paginator currentPage="${search.page_num}" rowBlockCount="${search.size}" totalRowCount="${search.total_count}" pageName="menuPrivFindList"/></p>
					</div>
				</c:if>
			</div>
		</div>
		
		<!-- 버튼 영역 -->
		<div class="form-actions">
			<div class="row">
				<div class="col-md-offset-2 col-md-10" align="right"
					style="padding-right: 30px;">
					<button type="button" class="btn btn-sm grey-mint btn-outline sbold uppercase"
						onclick="javascript:moveMenuMappDetail();">
						<i class="fa fa-list"></i> &nbsp;목록
					</button>
				</div>
			</div>
		</div>
		<input type="hidden" id="tag_regexp_seq" name="tag_regexp_seq">
		<input type="hidden" name="tag_seq" value ="${menuMappDetail.tag_seq}"/>
		
		</form>
	</div>
</div>

<!-- END CONTENT BODY -->

<form id="detailSearchForm" method="POST">
	<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }"/>
	<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
	<input type="hidden" name="system_seq" value="${paramBean.system_seq }" />
	<input type="hidden" name="page_num" value="${search.page_num}" />
	<input type="hidden" name="tag_seq" value ="${menuMappDetail.tag_seq}"/>
</form>

<script type="text/javascript">

	var menuPrivFindConfig = {
		"detailUrl":"${rootPath}/menuMappSetup/detail.html"
		,"privFindUrl":"${rootPath}/menuMappSetup/menuPrivFindList.html"
		,"addUrl":"${rootPath}/menuMappSetup/addPrivFind.html"
		,"saveUrl":"${rootPath}/menuMappSetup/savePrivFind.html"
		,"removeUrl":"${rootPath}/menuMappSetup/removePrivFind.html"
		,"loginPage" : "${rootPath}/loginView.html"
	};
	
</script>


 