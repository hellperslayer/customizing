<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="menuMappSetupAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/setup/menuMappSetupList.js"
	type="text/javascript" charset="UTF-8"></script>
<h1 class="page-title">${currentMenuName}</h1>
<!-- 그룹코드 신규 -->
<div class="portlet light portlet-fit bordered">
	<div class="portlet-body">
		<!-- BEGIN FORM-->
		<form id="menuMappSetupDetailForm" action="" method="POST" class="form-horizontal form-bordered form-row-stripped">
			<c:if test="${!empty menuMappSetupDetail}">
				<input type="hidden" name="tag_seq" value="${menuMappSetupDetail.tag_seq }" />
			</c:if>
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<%-- <th style="width: 15%; text-align: center;"><label
							class="control-label">메뉴매핑ID<span class="required">*</span>
						</label></th>
						<td style="width: 35%; vertical-align: middle;"
							class="form-group form-md-line-input">
							<c:choose>
								<c:when test="${empty menuMappSetupDetail}">
									<input type="text" name="tag_seq" size="51"
										class="form-control"/>
									<div class="form-control-focus"></div>
								</c:when>
								<c:otherwise>
									<c:out value="${menuMappSetupDetail.tag_seq}" />
									<input type="hidden" name="tag_seq"
										value="${menuMappSetupDetail.tag_seq}" />
								</c:otherwise>
							</c:choose>
						</td> --%>
						<th style="width: 15%; text-align: center;"><label class="control-label">메뉴명</label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<input type="text" class="form-control" name="menu_name" size="51" value="${menuMappSetupDetail.menu_name }" />
							<div class="form-control-focus"></div></td>
						<th style="width: 15%; text-align: center;"><label
							class="control-label">사용여부 </label></th>
						<td style="width: 35%;" class="form-group form-md-line-input">
							<div class="md-radio-inline">
								<div class="md-radio col-md-4">
									<input type="radio" id="checkbox1_1" class="md-radiobtn"
										id="form_control_2" title="사용" name="use_yn" value="Y"
										${fn:containsIgnoreCase(menuMappSetupDetail.use_yn,'Y')?'checked':'' }>
									<label for="checkbox1_1"> <span></span> <span
										class="check"></span> <span class="box"></span> 사용
									</label>
								</div>
								<div class="md-radio col-md-4">
									<input type="radio" id="checkbox1_2" class="md-radiobtn"
										id="form_control_2" title="미사용" name="use_yn" value="N"
										${!fn:containsIgnoreCase(menuMappSetupDetail.use_yn,'Y')?'checked':'' }>
									<label for="checkbox1_2"> <span></span> <span
										class="check"></span> <span class="box"></span> 미사용
									</label>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<th style="text-align: center;"><label class="control-label">URL<span
								class="required">*</span></label></th>
						<%-- <td colspan="3" class="form-group form-md-line-input" style="vertical-align: middle;">
							<input type="text" class="form-control" name="url" size="150" value="${menuMappSetupDetail.url }" />
							<div class="form-control-focus"></div>
						</td> --%>
						<td colspan="3" class="form-group form-md-line-input" style="text-align: left;"><TEXTAREA name="url" class="form-control" rows="7" style="width: 100%;">${menuMappSetupDetail.url}</TEXTAREA></td>
					</tr>
					<tr>
						<th style="text-align: center;"><label class="control-label">파라미터</label></th>
						<td colspan="3" class="form-group form-md-line-input" style="vertical-align: middle;">
							<input type="text" class="form-control" name="parameter" value="${menuMappSetupDetail.parameter }" />
							<div class="form-control-focus"></div>
						</td>
					</tr>
					<%-- <tr>
						<th style="text-align: center;"><label class="control-label">URL_TYPE
						</label></th>
						<td class="form-group form-md-line-input"
							style="vertical-align: middle;"><input type="text"
							class="form-control" id="form_control_5" name="url_type"
							size="51" value="${menuMappSetupDetail.url_type }" />
							<div class="form-control-focus"></div></td>
						<th style="text-align: center;"><label class="control-label">매뉴명</label></th>
						<td class="form-group form-md-line-input"
							style="vertical-align: middle;"><input type="text"
							class="form-control" name="menu_name" size="51"
							value="${menuMappSetupDetail.menu_name }" />
							<div class="form-control-focus"></div></td>
					</tr> --%>
					<tr>
						<th style="text-align: center;"><label class="control-label">수행업무
						</label></th>
						<td class="form-group form-md-line-input"
							style="vertical-align: middle;"><select name="crud_type"
							id="crud_type" class="ticket-assign form-control input-small">
								<c:forEach items="${CACHE_REQ_TYPE}" var="i" varStatus="status">
									<option value="${i.key}"
										<c:if test="${i.key eq menuMappSetupDetail.crud_type}">selected="selected"</c:if>>${i.value}</option>
								</c:forEach>
						</select></td>
						<th style="text-align: center;"><label class="control-label">시스템명</label></th>
						<td class="form-group form-md-line-input"
							style="vertical-align: middle;"><select name="system_seq"
							id="system_seq" class="ticket-assign form-control input-small">
								<option value=""
									<c:if test="${menuMappSetupDetail.system_seq == null}">selected="selected"</c:if>>선택</option>
								<c:forEach items="${systemMasterList}" var="systemMasterList">
									<option value="${systemMasterList.system_seq}"
										<c:if test="${systemMasterList.system_seq == menuMappSetupDetail.system_seq}">selected="selected"</c:if>>${systemMasterList.system_name}(${systemMasterList.system_seq })</option>
								</c:forEach>
						</select></td>
					</tr>
					<tr>
						<th style="text-align: center;"><label class="control-label">강제로그 생성</label></th>
						<td style="width: 35%;" class="form-group form-md-line-input">
							<div class="md-radio-inline">
								<div class="md-radio col-md-4">
									<input type="radio" id="checkbox2_1" class="md-radiobtn"
										id="form_control_2" title="사용" name="forcelog_yn" value="Y"
										${fn:containsIgnoreCase(menuMappSetupDetail.forcelog_yn,'Y')?'checked':'' }>
									<label for="checkbox2_1"> <span></span> <span
										class="check"></span> <span class="box"></span> 사용
									</label>
								</div>
								<div class="md-radio col-md-4">
									<input type="radio" id="checkbox2_2" class="md-radiobtn"
										id="form_control_2" title="미사용" name="forcelog_yn" value="N"
										${!fn:containsIgnoreCase(menuMappSetupDetail.forcelog_yn,'Y')?'checked':'' }>
									<label for="checkbox2_2"> <span></span> <span
										class="check"></span> <span class="box"></span> 미사용
									</label>
								</div>
							</div>
						</td>
						<th style="text-align: center;"><label class="control-label">강제로그 타입</label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<select name="forcelog_privacy_type" id="forcelog_privacy_type" class="ticket-assign form-control input-small">
								<c:forEach items="${CACHE_RESULT_TYPE}" var="i">
									<option value="${i.key}"
										<c:if test="${i.key eq menuMappSetupDetail.forcelog_privacy_type}">selected="selected"</c:if>>${i.value}</option>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<th style="text-align: center;"><label class="control-label">강제로그 내용</label></th>
						<td colspan="3" class="form-group form-md-line-input" style="vertical-align: middle;">
							<input type="text" class="form-control" name="forcelog_desc" value="${menuMappSetupDetail.forcelog_desc }" />
							<div class="form-control-focus"></div>
						</td>
					</tr>
				</tbody>
			</table>


			<!-- 메뉴 관련 input 시작 -->
			<input type="hidden" name="main_menu_id"
				value="${paramBean.main_menu_id }" /> <input type="hidden"
				name="sub_menu_id" value="${paramBean.sub_menu_id }" /> <input
				type="hidden" name="current_menu_id" value="${currentMenuId }" />
			<!-- 메뉴 관련 input 끝 -->

			<!-- 페이지 번호 -->
			<input type="hidden" name="page_num" value="${search.page_num }" />

			<!-- 검색조건 관련 input 시작 -->
			<input type="hidden" name="system_seq_search"
				value="${search.system_seq_search}" /> <input type="hidden"
				name="url_search" value="${search.url_search }" />
			<!-- 검색조건 관련 input 끝 -->
		</form>
		<!-- END FORM-->

		<!-- 버튼 영역 -->
		<div class="form-actions">
			<div class="row">
				<div class="col-md-offset-2 col-md-10" align="right"
					style="padding-right: 30px;">
					<!-- <button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
						onclick="moveMenuPrivFindList()">
						<i class="fa fa-search"></i> &nbsp;개인정보탐지설정
					</button> -->
					<button type="button" class="btn btn-sm dark btn-outline sbold uppercase"
						onclick="movemenuMappSetupListtoDetail()">
						<i class="fa fa-list"></i> &nbsp;목록
					</button>
					<c:choose>
						<c:when test="${empty menuMappSetupDetail}">
							<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="addmenuMappSetup()"><i
								class="fa fa-check"></i>&nbsp;추가</a>
						</c:when>
						<c:otherwise>
							<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="savemenuMappSetup()"><i
								class="fa fa-check"></i>&nbsp;저장</a>
							<a class="btn btn-sm red-mint btn-outline sbold uppercase" onclick="removemenuMappSetup()"><i
								class="fa fa-remove"></i>&nbsp;삭제</a>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	var menuMappSetupConfig = {
		"listUrl":"${rootPath}/menuMappSetup/list.html"
		,"addUrl":"${rootPath}/menuMappSetup/add.html"
		,"saveUrl":"${rootPath}/menuMappSetup/save.html"
		,"removeUrl":"${rootPath}/menuMappSetup/remove.html"
		,"privFindListUrl":"${rootPath}/menuMappSetup/menuPrivFindList.html"
	};
	
</script>