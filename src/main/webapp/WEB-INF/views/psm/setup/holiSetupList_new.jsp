<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script
	src="${rootPath}/resources/js/psm/setup/holiSetupList.js"
	type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/ezDatepicker.js"
	type="text/javascript" charset="UTF-8"></script>
<script
	src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js"
	type="text/javascript"></script>

<style type="text/css">
.input_date {
	background-color: #eef1f5;
	padding: 0.3em 0.5em;
	border: 1px solid #e4e4e4;
	height: 30px;
	font-size: 12px;
	color: #555;
}
</style>

<h1 class="page-title">${currentMenuName}</h1>
<div class="portlet light portlet-fit portlet-datatable bordered">

<!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog" style="position: relative ;top: 30%; left: 0;">
			<!-- Modal content -->
			<div class="modal-content">
				<div class="modal-header" style="background-color: #32c5d2;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">
						<b style="font-size: 13pt;">엑셀 업로드</b>
					</h4>
				</div>
				<form action="upload.html" method="post" id="fileForm" name="fileForm" enctype="multipart/form-data">
				<div class="modal-body" style="background-color: #F9FFFF;">
						<div>
							<table width="100%">
								<tr style="height: 40px">
									<td width="70%" style="vertical-align: middle;" align="center">
										<input class="fileUpLoad" type="file" id="file" name="file">
									</td>
									<td><img style="cursor: pointer;" name="submup" onclick="excelEmpUserUpLoad()"
										src="${rootPath}/resources/image/common/btn_exupload.gif" alt="엑셀업로드" title="엑셀업로드" /></td>
								</tr>
								<tr style="height: 40px">
									<td width="70%" style="vertical-align: middle;">
										<span class="downexam">&nbsp;&nbsp;&nbsp;※ 다운받은 양식으로 업로드하셔야 등록됩니다.</span></td>
									<td><img class="btn_excel exceldown" onclick="exDown()" style="cursor: pointer;" src="${rootPath}/resources/image/common/formUp.png"
										alt="양식다운로드" title="양식다운로드" /></td>
								</tr>
							</table>
						</div>
						<input type="hidden" id="result" name="result" value="false" />
				</div>
				<div class="modal-footer">
					<span class="md-radio-inline" style="float: left; margin-left: 25px;">
						날짜 충돌 처리 : 
						<div class="md-radio">
							<input type="radio" id="checkbox1_1" name="conflict" value="nothing"
								class="md-radiobtn"
								${fn:containsIgnoreCase(indvinfoTypeSetup.use_flag,'Y')?'checked':'' }>
							<label for="checkbox1_1" title="충돌이 발생한 업로드 데이터는 무시되고, 나머지 날짜가 업로드 됩니다."> <span></span> <span
								class="check"></span> <span class="box"></span> 무시
							</label>
						</div>
						<div class="md-radio">
							<input type="radio" id="checkbox1_2" name="conflict" value="conflict"
								class="md-radiobtn"
								${!fn:containsIgnoreCase(indvinfoTypeSetup.use_flag,'Y')?'checked':'' }>
							<label for="checkbox1_2" title="충돌이 발생한 기존 데이터를 덮어씌웁니다. 기존 데이터가 소실될 수 있습니다."> <span></span> <span
								class="check"></span> <span class="box"></span> 덮어쓰기
							</label>
						</div>
						<div class="md-radio">
							<input type="radio" id="checkbox1_3" name="conflict" value="stop"
								class="md-radiobtn"
								${!fn:containsIgnoreCase(indvinfoTypeSetup.use_flag,'Y')?'checked':'' }>
							<label for="checkbox1_3" title="충돌 발생시 업로드를 중단하고, 모든 업로드 데이터 적용사항을 취소합니다."> <span></span> <span
								class="check"></span> <span class="box"></span> 중단
							</label>
						</div>
					</span>
					<a class="btn btn-sm red" href="#" data-dismiss="modal"> <i class="fa fa-remove"></i> 닫기 </a>
				</div>
				</form>
			</div>
		</div>
	</div>


	<div class="portlet-body">
		<div class="table-container">
			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
				<div class="dataTables_scroll" style="position: relative;">
					<div>
						<div class="col-md-6" style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
							
							<div class="portlet box grey-salt  ">
                            <div class="portlet-title" style="background-color: #2B3643;">
                                <div class="caption">
                                    <i class="fa fa-search"></i>검색 & 엑셀</div>
                                <div class="tools">
                                    <a href="" class="collapse"> </a>
                                </div>
                            </div>
                            <div id="searchBar" class="portlet-body form" >
                                <div class="form-body" style="padding-left:20px; padding-right:10px;">
                                    <div class="form-group">
                                        <form id="listForm" method="POST" class="mt-repeater form-horizontal">
                                            <div data-repeater-list="group-a">
                                                <div class="row">
                                                    <!-- jQuery Repeater Container -->
                                                    <div class="col-md-2">
                                                        <label class="control-label">일시</label>
                                                        <div
															class="input-group date-picker input-daterange"
															data-date="10/11/2012" data-date-format="yyyy-mm-dd">
															<input type="text" class="form-control" id="search_fr"
																name="search_from"
																value="${search.search_fromWithHyphen}"> 
															<span class="input-group-addon"> &sim; 
															</span> <input type="text"
																class="form-control" id="search_to" name="search_to"
																value="${search.search_toWithHyphen}">
														</div> 
												 	</div>
													 <div class="col-md-2">
														<label class="control-label">휴일여부</label>
														<select class="form-control" name="holi_yn_search" id="holi_yn" style="text-align: center;">
															<option value=""> ----- 전 체 ----- </option>
															<option value="N">정상근무</option>
															<option value="Y">휴일</option>
														</select>
													</div>
                                                </div>
                                            </div>
                                            <hr/>
                                            <div align="right">
	                                            <button type="reset"
													class="btn btn-sm red-mint btn-outline sbold uppercase"
													onclick="resetOptions(holiListConfig['listUrl'])">
													<i class="fa fa-remove"></i> <font>취소
												</button>
												<button class="btn btn-sm blue btn-outline sbold uppercase"
													onclick="moveHoliSetup()">
													<i class="fa fa-search"></i> 검색
												</button>&nbsp;&nbsp;
												<div class="btn-group">
													<a href="javascript:;" data-toggle="dropdown">
														<img src="${rootPath}/resources/image/icon/XLS_3.png">
													</a>
													<ul class="dropdown-menu pull-right">
														<li>
															<a data-toggle="modal" data-target="#myModal">업로드</a>
														</li>
														<li><a onclick="excelHoliSetup('${search.total_count}')"> 다운로드 </a></li>
													</ul>
												</div>
												
												
												
											</div>
											
											<input type="hidden" name="main_menu_id"
												value="${paramBean.main_menu_id }" /> <input type="hidden"
												name="sub_menu_id" value="${paramBean.sub_menu_id }" /> <input
												type="hidden" name="current_menu_id" value="${currentMenuId}" />
											<input type="hidden" name="page_num" value="${search.page_num}" />
											<input type="hidden" name="isSearch" value="${paramBean.isSearch }" />
											<input type="hidden" name="holi_dt" value="" />
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>
					
					
					<div class="table-toolbar">
						<div class="row">
							<div class="col-md-6">
								<div class="btn-group">
									<button class="btn btn-sm blue btn-outline sbold uppercase" onclick="moveDetail('')">
										<i class="fa fa-plus"></i> 등록 
									</button>
								</div>
							</div>
						</div>
					</div>

						<div class="raw">
							<table style="border-top: 1px solid #e7ecf1;"
								class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
								role="grid">
								<colgroup>
									<col width="20%" />
									<col width="30%" />
									<col width="20%" />
									<col width="30%" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col" style="text-align: center;">날짜</th>
										<th scope="col" style="text-align: center;">휴일명</th>
										<th scope="col" style="text-align: center;">휴일여부</th>
										<th scope="col" style="text-align: center;">근무시간</th>
									</tr>
								</thead>
								<tbody>
									<c:choose>
										<c:when test="${empty holiSetupList.holiSetup}">
											<tr>
												<td colspan="4" align="center">데이터가 없습니다.</td>
											</tr>
										</c:when>
										<c:otherwise>
											<c:set value="${holiSetupList.page_total_count}" var="count" />
											<c:forEach items="${holiSetupList.holiSetup}" var="holiSetup" varStatus="status">
												<tr style='cursor: pointer;' onclick="moveDetail('${holiSetup.holi_dt}');">
													<td style="text-align: center;">
														<fmt:parseDate value="${holiSetup.holi_dt}" pattern="yyyyMMdd" var="holiDt" /> 
														<fmt:formatDate value="${holiDt}" pattern="yyyy-MM-dd" />
													</td>
													<td style="text-align: center;">
														<ctl:nullCv nullCheck="${holiSetup.holi_nm}" /></td>
													<td style="text-align: center;"><c:choose>
														<c:when test="${holiSetup.holi_yn == 'Y'}">
															휴일
														</c:when>
														<c:otherwise>
															정상근무
														</c:otherwise>
														</c:choose></td>
													<td style="text-align: center;"><ctl:nullCv
															nullCheck="${holiSetup.work_start_tm}" /> ~ <ctl:nullCv
															nullCheck="${holiSetup.work_end_tm}" /></td>
												</tr>
												<c:set var="count" value="${count - 1 }" />
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>

							<!-- 페이징 영역 -->
							<c:if test="${search.total_count > 0}">
								<div class="row" id="pagingframe" align="center">
									<p>
										<ctl:paginator currentPage="${search.page_num}"
											rowBlockCount="${search.size}"
											totalRowCount="${search.total_count}" />
									</p>
								</div>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form id="holiDetailForm" method="POST">
	<input type="hidden" name="holi_dt" value="" /> <input type="hidden"
		name="main_menu_id" value="${paramBean.main_menu_id }" /> <input
		type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
	<input type="hidden" name="current_menu_id" value="${currentMenuId}" />

	<input type="hidden" name="search_from" value="" /> <input
		type="hidden" name="search_to" value="" /> <input type="hidden"
		name="holi_yn" value="" /> <input type="hidden" name="cur_num"
		value="${pageInfo.cur_num}" />
</form>
<!-- 엑셀양식 다운로드 -->
<form action="exdownload.html" method="post" id="exdown" name="exdown" enctype="multipart/form-data"></form>
<script type="text/javascript">
	$(
			"#holi_yn > option[value="
					+ '<c:out value="${ search.holi_yn_search }"/>' + "]")
			.attr("selected", "selected");

	var holiListConfig = {
		"listUrl" : "${rootPath}/holiSetup/list.html",
		"detailUrl" : "${rootPath}/holiSetup/detail.html",
		"downloadUrl" : "${rootPath}/holiSetup/download.html",
		"uploadUrl" : "${rootPath}/holiSetup/upload.html"
	};
</script>