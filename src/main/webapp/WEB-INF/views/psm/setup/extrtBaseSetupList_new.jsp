<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/setup/extrtBaseSetupList.js" type="text/javascript" charset="UTF-8"></script>
<h1 class="page-title">${currentMenuName}</h1>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light portlet-fit portlet-datatable bordered">
			<div class="portlet-body">
				<div class="table-container">
					<div id="datatable_ajax_2_wrapper"
						class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
							<div class="col-md-6" style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
								<div class="portlet box grey-salt">
			                         <div class="portlet-title" style="background-color: #2B3643;">
			                             <div class="caption">
			                                 <i class="fa fa-search"></i>검색 & 엑셀</div>
			                             <div class="tools">
							                 <a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
							             </div>
			                         </div>
			                         <div id="searchBar" class="portlet-body form" >
			                             <div class="form-body" style="padding-left:20px; padding-right:10px;">
			                                 <div class="form-group">
			                                     <form id="listForm" method="POST" class="mt-repeater form-horizontal">
			                                         <div data-repeater-list="group-a">
			                                             <div data-repeater-item class="mt-repeater-item">
			                                             	<div class="mt-repeater-input">
			                                                     <label class="control-label">시나리오</label>
			                                                     <select name="scen_seq" class="form-control input-medium">
																	<option value="0">-----전 체-----</option>
																	<c:forEach items="${scenarioList }" var="sl">
																	<option value="${sl.scen_seq}" <c:if test='${search.scen_seq eq sl.scen_seq}'>selected="selected"</c:if>>${sl.scen_name}</option>
																	</c:forEach>
																</select> </div>
															<div class="mt-repeater-input">
			                                                     <label class="control-label">분석대상</label>
			                                                     <select name="indv_yn" class="form-control input-medium">
																	<option value="">-----전 체-----</option>
																	<option value="Y"
																		<c:if test='${search.indv_yn eq "Y"}'>selected="selected"</c:if>>통합</option>
																	<option value="N"
																		<c:if test='${search.indv_yn eq "N"}'>selected="selected"</c:if>>시스템별</option>
																</select></div>
															<c:if test="${search.isSearch=='Y' }">
			                                                 <div class="mt-repeater-input">
			                                                     <label class="control-label">사용여부</label>
			                                                     <select name="use_yn_search" class="form-control input-medium">
																	<option value="">-----전 체-----</option>
																	<option value="Y"
																		<c:if test='${search.use_yn_search eq "Y"}'>selected="selected"</c:if>>사용</option>
																	<option value="N"
																		<c:if test='${search.use_yn_search eq "N"}'>selected="selected"</c:if>>미사용</option>
																</select> </div>
			                                                 </c:if>
															 <div class="mt-repeater-input">
			                                                     <label class="control-label">알람사용여부</label>
			                                                     <select name="alarm_yn_search" class="form-control input-medium">
																	<option value="">-----전 체-----</option>
																	<option value="Y"
																		<c:if test='${search.alarm_yn_search eq "Y"}'>selected="selected"</c:if>>사용</option>
																	<option value="N"
																		<c:if test='${search.alarm_yn_search eq "N"}'>selected="selected"</c:if>>미사용</option>
																</select></div>
			                                             </div>
			                                         </div>
			                                         <div align="right">
			                                          	<button type="reset"
															class="btn btn-sm red-mint btn-outline sbold uppercase"
															onclick="cancelExtrtBaseSetupList()">
															<i class="fa fa-remove"></i> 초기화
														</button>
														<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
															onclick="javascript:moveextrtBaseSetupList()">
															<i class="fa fa-search"></i> 검색
														</button>&nbsp;&nbsp;
														<%-- <a onclick="excelExtrtBaseSetup('${search.total_count}')"><img src="${rootPath}/resources/image/icon/XLS_3.png"></a> --%>
													</div>
											
													<input type="hidden" name="seq" value="${search.seq }" /> 
													<input type="hidden" name="rule_seq" value="" /> 
													<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" /> 
													<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" /> 
													<input type="hidden" name="current_menu_id" value="${currentMenuId}" /> 
													<input type="hidden" name="page_num" value="${search.page_num}" />
													<input type="hidden" name="isSearch" value="${search.isSearch}" />
													<input type="hidden" name="isSearchDetail" value="${paramBean.isSearchDetail}" />
			                                     </form>
			                                 </div>
			                             </div>
			                         </div>
			                     </div>
							</div>
						</div>
						<c:if test="${search.isSearch=='Y' }">
						<div align="left">
							<a class="btn btn-sm blue btn-outline sbold uppercase"
								onclick="fnextrtBaseSetupDetail(null)"><i class="fa fa-plus"></i> 신규 </a>
						</div>
						</c:if>
						<div class="raw">
							<table style="border-top: 1px solid #e7ecf1;" class="table table-bordered table-hover table-checkable dataTable no-footer" role="grid">
								<thead>
									<tr>
										<th width="5%" style="text-align: center;">번호</th>
										<th width="15%" style="text-align: center;">시나리오</th>
										<th width="35%" style="text-align: center;">상세시나리오명</th>
										<th width="7%" style="text-align: center;">분석대상</th>
										<th width="7%" style="text-align: center;">분석위험기준</th>
										<th width="7%" style="text-align: center;">임계치</th>
										<th width="7%" style="text-align: center;">위험도</th>
										<th width="7%" style="text-align: center;">사용여부</th>
										<th width="7%" style="text-align: center;">알람사용여부</th>
									</tr>
								</thead>
								<tbody>
									<c:choose>
										<c:when test="${empty extrtBaseSetupList}">
											<tr>
												<td colspan="8" align="center">데이터가 없습니다.</td>
											</tr>
										</c:when>
										<c:otherwise>
											<c:forEach items="${extrtBaseSetupList}" var="extrtBaseSetup" varStatus="status">
												<tr style='cursor: pointer;' onclick="javascript:fnextrtBaseSetupDetail('${extrtBaseSetup.rule_seq}')">
													<td style="text-align: center;">${extrtBaseSetup.rule_seq}</td>
													<td style="text-align: left;">${extrtBaseSetup.scen_name}</td>
													<td style="text-align: left;">${extrtBaseSetup.rule_nm}</td>
													<c:choose>
														<c:when test="${extrtBaseSetup.indv_yn == 'Y'}">
															<td style="text-align: center; vertical-align: middle;">통합</td>
														</c:when>
														<c:otherwise>
															<td style="text-align: center; vertical-align: middle;">시스템별</td>
														</c:otherwise>
													</c:choose>
													
													<td style="text-align: center;">
														<c:choose>
															<c:when test="${extrtBaseSetup.limit_type ==1 }">공통</c:when>
															<c:when test="${extrtBaseSetup.limit_type ==2 }">개인별</c:when>
															<c:when test="${extrtBaseSetup.limit_type ==3 }">부서별</c:when>
															<c:when test="${extrtBaseSetup.limit_type ==4 }">순위</c:when>
															<c:when test="${extrtBaseSetup.limit_type ==5 }">비율</c:when>
															<c:when test="${extrtBaseSetup.limit_type ==6 }">사용안함</c:when>
														</c:choose>
													
													</td>
													<td style="text-align: center;">${extrtBaseSetup.limit_cnt}</td>
													<td style="text-align: center;">${extrtBaseSetup.dng_val}</td>
													<c:choose>
														<c:when
															test="${template_yesNo.options[extrtBaseSetup.use_yn] == '사용'}">
															<td style="text-align: center; vertical-align: middle;"><span
																class="label label-sm label-success"> 사용 </span></td>
														</c:when>
														<c:otherwise>
															<td style="text-align: center; vertical-align: middle;"><span
																class="label label-sm label-warning ">미사용 </span></td>
														</c:otherwise>
													</c:choose>
													
													<c:choose>
														<c:when
															test="${template_yesNo.options[extrtBaseSetup.alarm_yn] == '사용'}">
															<td style="text-align: center; vertical-align: middle;"><span
																class="label label-sm label-success"> 사용 </span></td>
														</c:when>
														<c:otherwise>
															<td style="text-align: center; vertical-align: middle;"><span
																class="label label-sm label-warning ">미사용 </span></td>
														</c:otherwise>
													</c:choose>
												</tr>
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
							<c:if test="${search.total_count > 0}">
								<div class="page left" id="pagingframe" align="center">
									<p>
										<ctl:paginator currentPage="${search.page_num}" rowBlockCount="${search.size}" totalRowCount="${search.total_count}" />
									</p>
								</div>
							</c:if> 
							
							<c:if test="${search.isSearch=='Y' }">
							<div class="form-actions">
								<div class="row">
									<div class="col-md-offset-2 col-md-10" align="right" style="padding-right: 15px;">
										<button type="button" class="btn btn-sm grey-mint btn-outline sbold uppercase" onclick="movescenarioList();">
											<i class="fa fa-list"></i> &nbsp;목록
										</button>
									</div>
								</div>
							</div>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var extrtBaseSetupConfig = {
		"listUrl" : "${rootPath}/extrtBaseSetup/list.html",
		"detailUrl" : "${rootPath}/extrtBaseSetup/detail.html",
		"downloadUrl" : "${rootPath}/extrtBaseSetup/download.html",
		"menu_id" : "${currentMenuId}",
		"scenarioUrl" : "${rootPath}/extrtBaseSetup/scenarioList.html"
	};

	var menu_id = extrtBaseSetupConfig["menu_id"];
	var log_message = "SUCCESS";
	var log_action = "SELECT";
	
	function openList(list) {
		var elem = $("tr[id^="+list+"]" );
		var dis = elem.css('display');
		if(dis == "none")
			elem.css('display', 'table-row');
		else
			elem.css('display', 'none');
	}
</script>
