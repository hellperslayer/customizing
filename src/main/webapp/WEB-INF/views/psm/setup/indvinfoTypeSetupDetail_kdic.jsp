<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/setup/indvinfoTypeSetupList_kdic.js" type="text/javascript" charset="UTF-8"></script>
<h1 class="page-title">
	${currentMenuName}
	<!--bold font-blue-hoki <small>basic bootstrap tables with various options and styles</small> -->
</h1>
<div class="portlet light portlet-fit bordered">
	<div class="portlet-body">
		<!-- BEGIN FORM-->
		<form id="listForm" action="" method="POST"
			class="form-horizontal form-bordered form-row-stripped">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<th style="text-align: center;">DB명</th>
						<td style="width: 35%; vertical-align: middle;" class="form-group form-md-line-input">
							<c:if test="${paramBean.edited eq 'Y' }">
								<input type="text" class="db_name form-control" value="${privacyTableDetail.db_name}">
							</c:if>
							<c:if test="${paramBean.edited ne 'Y' }">
								${privacyTableDetail.db_name}
							</c:if>
						</td>
						<th style="text-align: center;">테이블명</th>
						<td style="width: 35%;" class="form-group form-md-line-input">
							<c:if test="${paramBean.edited eq 'Y' }">
								<input type="text" class="table_name form-control" value="${privacyTableDetail.table_name}">
							</c:if>
							<c:if test="${paramBean.edited ne 'Y' }">
								${privacyTableDetail.table_name}
							</c:if>
						</td>
					</tr>
				</tbody>
			</table>
			<c:if test="${paramBean.edited eq 'Y' }">
				<div style="text-align: right; margin-bottom: 20px;">
					<button type="button" class="btn btn-sm blue btn-outline sbold uppercase" onclick="addColumn()">
						<i class="fa fa-plus"></i> 컬럼 추가 
					</button>
				</div>
			</c:if>
			
			<table class="table table-bordered table-striped" id="columnData">
				<colgroup>
					<col width="30%" />
					<col width="40%" />
					<col width="30%" />
				</colgroup>
				<thead>
					<tr>
						<th scope="col"
							style="text-align: center; vertical-align: middle;">컬럼명</th>
						<th scope="col"
							style="text-align: center; vertical-align: middle;">개인정보유형</th>
						<th scope="col"
							style="text-align: center; vertical-align: middle;">우선순위</th>
						<c:if test="${paramBean.edited eq 'Y' }">
							<th scope="col"
								style="text-align: center; vertical-align: middle;">삭제</th>
						</c:if>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${empty privacyTableDetailList}">
							<c:if test="${paramBean.edited eq 'Y' }">
								<tr class="column" id="column1">
									<td style="text-align: center;"><input type="text" class="column_name form-control"></td>
									<td style="text-align: center;">
										<select class="privacy_type form-control">
											<c:forEach items="${privacyType }" var="item">
												<option value="${item.key}"
													<c:if test="${item.key eq column.privacy_type}">selected='selected'</c:if>
												>${item.value}</option>
											</c:forEach>
										</select>
									</td>
									<td style="text-align: center;"><input type="text" class="privacy_order form-control"></td>
									<td style="text-align: center;">
										<button type="button" class="btn btn-sm red btn-outline sbold uppercase" onclick="removeColumn(this)">
											<i class="fa fa-close"></i> 삭제
										</button>
									</td>
									<input type="hidden" class="remove" value="N">
								</tr>
							</c:if>
							<c:if test="${paramBean.edited ne 'Y' }">
								<tr>
									<td colspan="3">데이터가 없습니다.</td>
								</tr>
							</c:if>
						</c:when>
						<c:otherwise>
							<c:forEach var="column" items="${privacyTableDetailList}" varStatus="count">
								<c:if test="${paramBean.edited eq 'Y' }">
									<tr class="column" id="column${count.count} ">
										<td style="text-align: center;"><input type="text" class="column_name form-control" value="${column.column_name }"></td>
										<td style="text-align: center;">
											<select class="privacy_type form-control">
												<c:forEach items="${privacyType }" var="item">
													<option value="${item.key}"
														<c:if test="${item.key eq column.privacy_type}">selected='selected'</c:if>
													>${item.value}</option>
												</c:forEach>
											</select>
										</td>
										<td style="text-align: center;"><input type="text" class="privacy_order form-control" value="${column.privacy_order }"></td>
										<c:if test="${paramBean.edited eq 'Y' }">
										<td style="text-align: center;">
											<button type="button" class="btn btn-sm red btn-outline sbold uppercase" onclick="removeColumn(this)">
												<i class="fa fa-close"></i> 삭제
											</button>
										</td>
										</c:if>
										<input type="hidden" class="privacy_column_info_seq" value="${column.privacy_column_info_seq }">
										<input type="hidden" class="remove" value="N">
									</tr>
								</c:if>
								<c:if test="${paramBean.edited ne 'Y' }">
									<tr>
										<td style="text-align: center;">${column.column_name }</td>
										<td style="text-align: center;">${privacyTypeAll[column.privacy_type] }</td>
										<td style="text-align: center;">${column.privacy_order }</td>
									</tr>
								</c:if>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>



			<!-- 메뉴 관련 input 시작 -->
			<input type="hidden" name="main_menu_id"
				value="${paramBean.main_menu_id }" /> <input type="hidden"
				name="sub_menu_id" value="${paramBean.sub_menu_id }" /> 
			<input type="hidden" name="current_menu_id" value="${currentMenuId }" /> 
			<input type="hidden" name="page_num" value="${search.page_num }" />
			<input type="hidden" name="isSearch" value="${paramBean.isSearch }" />
			<input type="hidden" name="edited" value="Y" />
			<input type="hidden" name="privacy_table_info_seq" value="${privacyTableDetail.privacy_table_info_seq}" />
			<!-- 검색조건 관련 input 끝 -->
		</form>
		<!-- END FORM-->

		<!-- 버튼 영역 -->
		<div class="form-actions">
			<div class="row">
				<div class="col-md-offset-2 col-md-10" align="right"
					style="padding-right: 30px;">
					<button type="button" class="btn btn-sm grey-mint btn-outline sbold uppercase"
						onclick="javascript:moveindvinfoTypeSetupListtoDetail();">
						<i class="fa fa-list"></i> &nbsp;목록
					</button>

					<c:choose>
						<c:when test="${empty privacyTableDetail}">
							<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="add_kdic()"><i
								class="fa fa-check"></i>&nbsp;추가</a>
						</c:when>
						<c:when test="${!empty privacyTableDetail}">
							<c:if test="${paramBean.edited eq 'Y' }">
								<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="save_kdic('${privacyTableDetail.privacy_table_info_seq}')"><i
									class="fa fa-check"></i>&nbsp;저장</a>
							</c:if>
							<c:if test="${paramBean.edited ne 'Y' }">
								<td style="text-align: center;">
									<button type="button" class="btn btn-sm red btn-outline sbold uppercase" onclick="removeTable('${privacyTableDetail.privacy_table_info_seq}')">
										<i class="fa fa-close"></i> 삭제
									</button>
								</td>
								<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="fnindvinfoTypeSetupDetail_kdic('${privacyTableDetail.privacy_table_info_seq}')"><i
									class="fa fa-check"></i>&nbsp;수정</a>
							</c:if>
						</c:when>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var indvinfoTypeSetupConfig = {
		"listUrl" : "${rootPath}/indvinfoTypeSetup/list_kdic.html",
		"detailUrl" : "${rootPath}/indvinfoTypeSetup/detail_kdic.html",
		"addUrl" : "${rootPath}/indvinfoTypeSetup/add_kdic.html",
		"saveUrl" : "${rootPath}/indvinfoTypeSetup/save_kdic.html",
		"removeUrl" : "${rootPath}/indvinfoTypeSetup/remove_kdic.html"
	};
</script>