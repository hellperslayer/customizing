<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script
	src="${rootPath}/resources/js/psm/system_management/agentParamMngtList.js"
	type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/ezDatepicker.js"
	type="text/javascript" charset="UTF-8"></script>
<script
	src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js"
	type="text/javascript"></script>

<!-- BEGIN PAGE TITLE-->
<h1 class="page-title">${currentMenuName}</h1>
<!-- END PAGE TITLE-->

<!-- END PAGE HEADER-->
<div class="portlet light portlet-fit portlet-datatable bordered">
	<div class="portlet-body">
		<div class="table-container">
			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
<%-- 
				<div class="col-md-6"
					style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
						<div class="portlet box grey-salt  ">
                          <div class="portlet-title" style="background-color: #2B3643;">
                              <div class="caption">
                                  <i class="fa fa-search"></i>검색 & 엑셀 </div>
                              <div class="tools">
				                 <a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
				             </div>
                          </div>
                          <div id="searchBar" class="portlet-body form" >
                              <div class="form-body" style="padding-left:10px; padding-right:10px;">
                                  <div class="form-group">
                                      <form id="adminUserListForm" method="POST" class="mt-repeater form-horizontal">
                                          <div data-repeater-list="group-a">
                                              <div data-repeater-item class="mt-repeater-item">
                                                  <!-- jQuery Repeater Container -->
                                                  <div class="mt-repeater-input">
                                                      <label class="control-label">관리자ID</label>
                                                      <br/>
                                                      <input type="text" class="form-control input-medium" name="admin_user_id_1" value="${search.admin_user_id_1 }"> 
                                                      <input type="text" class="form-control input-medium" name="admin_user_id_1" value="${search.admin_user_id_1 }"/>
                                                  </div>
                                                  <div class="mt-repeater-input">
                                                      <label class="control-label">관리자명</label>
                                                      <br/>
                                                      <input type="text" class="form-control input-medium" name="admin_user_name" value="${search.admin_user_name }">
                                                      <input type="text" class="form-control input-medium" name="admin_user_name" value="${search.admin_user_name }"/>
                                                  </div>
                                                  <div class="mt-repeater-input">
                                                      <label class="control-label">소속</label>
                                                      <br/>
                                                      <select name="dept_id" id="dept_id"
														class="form-control input-medium"
														style="width: 100%; margin-left: 0px; ">
														<option value=""
															${search.dept_id == '' ? 'selected="selected"' : ''}>
															----- 전 체 -----</option>
														<c:if test="${empty deptList}">
															<option>소속 데이터 없습니다.</option>
														</c:if>
														<c:if test="${!empty deptList}">
															<c:forEach items="${deptList}" var="i" varStatus="z">
																<option value="${i.dept_id}"
																	${i.dept_id==search.dept_id ? "selected=selected" : "" }>${ i.simple_dept_name}</option>
															</c:forEach>
														</c:if>
													</select>
													<div class="" style="padding-left: 0px;">
	                                                    <div class="mt-checkbox-inline">└
	                                                        <label class="mt-checkbox mt-checkbox-outline" style="margin-bottom: 0px;">
	                                                            <input type="checkbox" name="deptLowSearchFlag"
																${search.deptLowSearchFlag == 'on' ? 'checked="checked"' : ''}
																style="float: none;" />하위소속포함
	                                                            <span></span>
	                                                        </label>
	                                                    </div>
		                                            </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div align="right">
                                           <button type="reset"
											class="btn btn-sm red-mint btn-outline sbold uppercase"
											onclick="resetOptions(adminUserListConfig['listUrl'])">
											<i class="fa fa-remove"></i> <font>초기화
										</button>
										<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
											onclick="moveAdminUserList()">
											<i class="fa fa-search"></i> 검색
										</button>&nbsp;&nbsp;
										<a onclick="excelAdminUserList()"><img src="${rootPath}/resources/image/icon/XLS_3.png"></a>										
									</div>
									
									<input type="hidden" name="main_menu_id"
										value="${paramBean.main_menu_id }" /> <input type="hidden"
										name="sub_menu_id" value="${paramBean.sub_menu_id }" /> <input
										type="hidden" name="current_menu_id" value="${currentMenuId}" />
									<input type="hidden" name="page_num" value="${search.page_num}" />
									<input type="hidden" name="isSearch" value="${paramBean.isSearch }" />
                                      </form>
                                  </div>
                              </div>
                          </div>
                      </div>
				</div>
 --%>
				<!-- <div class="table-toolbar">
					<div class="row">
						<div class="col-md-6">
							<div class="btn-group">
								<button id="menuMngt_new" class="btn btn-sm blue btn-outline sbold uppercase"
									onclick="findAdminUserDetail(null)">
									<i class="fa fa-plus"></i> 신규 
								</button>
							</div>
						</div>
					</div>
				</div> -->

				<table style="border-top: 1px solid #e7ecf1"
					class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
					style="position: absolute; top: 0px; left: 0px; width: 100%;">

					<thead>
						<tr role="row" class="heading" style="background-color: #c0bebe;">
							<th width="20%"
								style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">No.</th>
							<th width="80%"
								style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
								에이전트그룹명</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${empty agentParamList}">
								<tr>
									<td colspan="3" align="center">데이터가 없습니다.</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach items="${agentParamList}" var="agnetParam" varStatus="status">
									<tr role="row" class="odd" style="text-align: center; cursor: pointer;"
										onclick="findAgentParamDetail('${agnetParam.agent_name}')">
										<td width="20%" style="text-align: center;">${status.count }</td>
										<td width="80%" style="text-align: center;">${agnetParam.agent_name}</td>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
				<form id="agentParamListForm" method="POST" class="mt-repeater form-horizontal">
					
					
				</form>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var agentParamListConfig = {
		"listUrl" : "${rootPath}/agentParamMngt/list.html",
		"detailUrl" : "${rootPath}/agentParamMngt/detail.html",
		"saveUrl" : "${rootPath}/agentParamMngt/save.html"
	};
</script>
