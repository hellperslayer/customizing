<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>

<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/system_management/empUserMngtDetail.js" type="text/javascript" charset="UTF-8"></script>


<h1 class="page-title">
	<c:choose>
		<c:when test="${!empty empUserDetail}">
			${currentMenuName}
		</c:when>
		<c:otherwise>
			사용자 신규등록
		</c:otherwise>
	</c:choose>
	<!--bold font-blue-hoki <small>basic bootstrap tables with various options and styles</small> -->
</h1>
<div class="portlet light portlet-fit bordered">
	<div class="portlet-body">
		<!-- BEGIN FORM-->
		<form id="empUserDetailForm" method="POST"
			class="form-horizontal form-bordered form-row-stripped">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<th style="width: 15%; text-align: center;">
							<label class="control-label" for="form_control_1">
								사용자명<span class="required">*</span>
							</label>
						</th>
						<td style="width: 35%; vertical-align: middle;" class="form-group form-md-line-input">
							<c:choose>
								<c:when test="${empty empUserDetail}">
									<input type="text" name="emp_user_name" class="form-control" />
								</c:when>
								<c:otherwise>
									<input type="text" name="emp_user_name" class="form-control" value="${empUserDetail.emp_user_name }"/>
									<input type="hidden" name="emp_user_id" value="${empUserDetail.emp_user_id}" />
								</c:otherwise>
							</c:choose>
							<div class="form-control-focus"></div>
						</td>
						<th style="text-align: center;">
							<label class="control-label">소속명<span class="required">*</span></label>
						</th>
						
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<select name="dept_id" class="ticket-assign form-control input-medium">
							<c:choose>
								<c:when test="${empty empUserDetail}">
								    <option value="">소속을 선택해주세요.</option>
								    <optgroup label="-----------------------------------------"></optgroup>
									<c:forEach items="${deptList}" var="deptopt" varStatus="status">
										<option value="${deptopt.dept_id}">${deptopt.dept_name}</option>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<c:forEach items="${deptList}" var="deptopt" varStatus="status">
										<option value="${deptopt.dept_id}" <c:if test="${deptopt.dept_name eq empUserDetail.dept_name}"> selected="selected" </c:if>>${deptopt.dept_name}</option>
									</c:forEach>
									<input type="hidden" name="temp_dept_id" value="${empUserDetail.dept_name }" />
								</c:otherwise>
							</c:choose>	
							</select>
						</td>
					</tr>
					<tr>
						<th style="text-align: center;">
							<label class="control-label">사용자ID<span class="required">*</span></label>
						</th>
						<c:choose>
							<c:when test="${empty empUserDetail }">
								<td style="vertical-align: middle;" class="form-group form-md-line-input">
<!-- 									<input type="text" name="emp_user_id" onchange="onlyEng()" class="form-control"/> -->
										<input type="text" name="emp_user_id" class="form-control"/>
									<div class="form-control-focus"></div>
								</td>
							</c:when>
							<c:otherwise>
								<td style="vertical-align: middle;" class="form-group form-md-line-input">
									 <c:out value="${empUserDetail.emp_user_id }" />
								</td>
							</c:otherwise>
						</c:choose>
						<th style="text-align: center;"><label class="control-label">시스템명<span class="required">*</span></label></th>
						
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<select name="system_seq" class="ticket-assign form-control input-medium">
								<c:choose>
									<c:when test="${empty empUserDetail}">
										<!-- <td style="vertical-align: middle;" class="form-group form-md-line-input">
											&nbsp;-
										</td> -->
										<option value="">시스템을 선택해주세요.</option>
										<optgroup label="-----------------------------------------"></optgroup>
										<c:forEach items="${systemList}" var="systemopt" varStatus="status">
											<option value="${systemopt.system_seq}">${systemopt.system_name}</option>
										</c:forEach>
									</c:when>
									<c:otherwise>
										<%-- <td style="vertical-align: middle;" class="form-group form-md-line-input">
											&nbsp;<c:out value="${empUserDetail.system_name}" />
										</td> --%>
										<c:forEach items="${systemList}" var="systemopt" varStatus="status">
											<option value="${systemopt.system_seq}" 
											<c:if test="${systemopt.system_seq == empUserDetail.system_seq}"> selected="selected" </c:if>>${systemopt.system_name}</option>
										</c:forEach>
										<input type="hidden" name="temp_system_seq" value="${empUserDetail.system_seq }" />
									</c:otherwise>
								</c:choose>
							</select>
							<input type="hidden" name="sys_cd" value="${empUserDetail.system_seq }" />
						</td>
					</tr>
					<tr>
						<th style="text-align: center;"><label class="control-label">상태</label></th>
						<c:choose>
							<c:when test="${empty empUserDetail}">
								<td class="form-group form-md-line-input" style="vertical-align: middle;">
									<select name="status" class="ticket-assign form-control input-medium" >
									    <!-- <option value="X">사용 안함</option> -->
										<option value="W">재직</option>
										<option value="R">퇴직</option>
										<option value="L">휴직</option>
										<!-- <option value="V">휴가</option> -->
<!-- 
										<option value="E">교육</option>
										<option value="D">파견</option>
 -->
									</select>
								</td>
							</c:when>
							<c:otherwise>
								<td class="form-group form-md-line-input" style="vertical-align: middle;">
									<%-- <c:out value="${empUserDetail.status_name}" /> --%>
									<select name="status" class="ticket-assign form-control input-medium" >
										<%-- <option value="X" <c:if test="${empUserDetail.status eq 'X'}"> selected="selected" </c:if>>사용 안함</option> --%>
										<option value="W" <c:if test="${empUserDetail.status eq 'W'}"> selected="selected" </c:if>>재직</option>
										<option value="R" <c:if test="${empUserDetail.status eq 'R'}"> selected="selected" </c:if>>퇴직</option>
										<option value="L" <c:if test="${empUserDetail.status eq 'L'}"> selected="selected" </c:if>>휴직</option>
										<%-- <option value="V" <c:if test="${empUserDetail.status eq 'V'}"> selected="selected" </c:if>>휴가</option>
										<option value="E" <c:if test="${empUserDetail.status eq 'E'}"> selected="selected" </c:if>>교육</option>
										<option value="D" <c:if test="${empUserDetail.status eq 'D'}"> selected="selected" </c:if>>파견</option> --%>
									</select>
								</td>
								<input type="hidden" name="temp_status" value="${empUserDetail.status }" />
							</c:otherwise>
						</c:choose>
						<th style="text-align: center;"><label class="control-label">IP</label></th>
						<c:choose>
							<c:when test="${empty empUserDetail }">
								<td class="form-group form-md-line-input" style="vertical-align: middle;">
									<input type="text" name="ip" class="form-control" />
									<div class="form-control-focus"></div>
								</td>
							</c:when>
							<c:otherwise>
								<td class="form-group form-md-line-input" style="vertical-align: middle;">
									<%-- <c:out value="${empUserDetail.ip }" /> --%>
									<input type="text" name="ip" value="${empUserDetail.ip }" class="form-control" />
									<div class="form-control-focus"></div>
								</td>
							</c:otherwise>
						</c:choose>
					</tr>
					<tr>
						<th style="text-align: center;"><label class="control-label">E-MAIL</label></th>
						<c:choose>
							<c:when test="${empty empUserDetail }">
								<td class="form-group form-md-line-input" style="vertical-align: middle;">
									<input type="text" name="email_address" class="form-control" />
									<div class="form-control-focus"></div>
								</td>
							</c:when>
							<c:otherwise>
								<td class="form-group form-md-line-input" style="vertical-align: middle;">
									<input type="text" name="email_address" value="${empUserDetail.email_address}" class="form-control" />
									<div class="form-control-focus"></div>
								</td>
							</c:otherwise>
						</c:choose>
						<th style="text-align: center;"><label class="control-label">핸드폰 번호</label></th>
						<c:choose>
							<c:when test="${empty empUserDetail }">
								<td class="form-group form-md-line-input" style="vertical-align: middle;">
									<input type="text" name="mobile_number" class="form-control" />
									<div class="form-control-focus"></div>
								</td>
							</c:when>
							<c:otherwise>
								<td class="form-group form-md-line-input" style="vertical-align: middle;">
									<input type="text" name="mobile_number" value="${empUserDetail.mobile_number}" class="form-control" />
									<div class="form-control-focus"></div>
								</td>
							</c:otherwise>
						</c:choose>			
					</tr>
					<tr>
						<th style="text-align: center;"><label class="control-label">사용여부</label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<select name="use_flag" class="ticket-assign form-control input-medium" >
								<option value="Y" <c:if test='${empUserDetail.use_flag == "Y"}'>selected</c:if>>사용</option>
								<option value="N" <c:if test='${empUserDetail.use_flag == "N"}'>selected</c:if>>미사용</option>
							</select>
						</td>
						<th style="text-align: center;"><label class="control-label">내외구분</label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<select name="external_staff" class="ticket-assign form-control input-medium" >
								<option value="0" <c:if test='${empUserDetail.external_staff == 0}'>selected</c:if>>내부직</option>
								<option value="1" <c:if test='${empUserDetail.external_staff == 1}'>selected</c:if>>외부직</option>
							</select>
						</td>
					</tr>
					<tr>
						<th style="text-align: center;"><label class="control-label">소명유형</label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<select name="self_summon" class="ticket-assign form-control input-medium" >
								<option value="Y" <c:if test='${empUserDetail.self_summon == "Y"}'>selected</c:if>>본인</option>
								<option value="N" <c:if test='${empUserDetail.self_summon == "N"}'>selected</c:if>>대리인</option>
							</select>
						</td>
						<th style="text-align: center;"><label class="control-label"></label></th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
						</td>
					</tr>
					<c:if test="${!empty empUserDetail }">
						<tr>
							<th style="text-align: center;">
								<label class="control-label" for="form_control_2">등록일시 </label>
							</th>
							<td class="form-group form-md-line-input" style="vertical-align: middle;">
								<div class="form-control form-control-static">
									<fmt:formatDate value="${empUserDetail.insert_datetime }" pattern="yyyy-MM-dd kk:mm:ss" />
								</div>
							</td>
							<th style="text-align: center;">
								<label class="control-label" for="form_control_1">수정일시 </label>
							</th>
							<td style="vertical-align: middle;" class="form-group form-md-line-input">
								<div class="form-control form-control-static">
									<fmt:formatDate value="${empUserDetail.update_datetime }" pattern="yyyy-MM-dd kk:mm:ss" />
								</div>
							</td>
						</tr>
					</c:if>
				</tbody>
			</table>
		</form>
		
		<c:if test="${!empty empUserDetail }">
			<div align="right">
				<button type="button" class="btn btn-sm green btn-outline sbold uppercase" onclick="initPass('${empUserDetail.emp_user_id}','${empUserDetail.system_seq}')">
					<i class="fa fa-check"></i> 비밀번호 초기화
				</button>
			</div>
		</c:if>
		<br/><br/>
			
		<div class="form-actions">
			<div class="row">
				<div class="col-md-offset-2 col-md-10" align="right"
					style="padding-right: 30px;">
					<button type="button"
						class="btn btn-sm grey-mint btn-outline sbold uppercase"
						onclick="moveEmpUserList()">
						<i class="fa fa-list"></i> 목록
					</button>
					<c:choose>
						<c:when test="${empty empUserDetail}">
							<button type="button"
								class="btn btn-sm blue btn-outline sbold uppercase"
								onclick="addEmpUserMngtOne()">
								<i class="fa fa-check"></i> 등록
							</button>
						</c:when>
						<c:otherwise>
							<button type="button"
								class="btn btn-sm blue btn-outline sbold uppercase"
								onclick="saveEmpUserMngtOne()">
								<i class="fa fa-check"></i> 수정
							</button>
							<button type="button"
								class="btn btn-sm red-mint btn-outline sbold uppercase"
								onclick="removeEmpUser()">
								<i class="fa fa-close"></i> 삭제
							</button>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>

		<form id="empUserListForm" method="POST">
			<!-- 메뉴 관련 input 시작 -->
			<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" />
			<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
			<input type="hidden" name="current_menu_id" value="${currentMenuId }" />
			<!-- 메뉴 관련 input 끝 -->
			
			<!-- 페이지 번호 -->
			<input type="hidden" name="page_num" value="${paramBean.page_num }" />
			
			<!-- 검색조건 관련 input 시작 -->
			<input type="hidden" name="emp_user_id_1" value="${paramBean.emp_user_id_1 }" />
			<input type="hidden" name="emp_user_name" value="${paramBean.emp_user_name }" />
			<input type="hidden" name="dept_id" value="${paramBean.dept_id }" />
			<input type="hidden" name="deptLowSearchFlag" value="${paramBean.deptLowSearchFlag }" />
			<input type="hidden" name="status" value="${paramBean.status }" />
			<input type="hidden" name="isSearch" value="${paramBean.isSearch }" />
			<!-- 검색조건 관련 input 끝 -->
		</form>
		<!-- END FORM-->
	</div>
</div>

<script type="text/javascript">

	var empUserDetailConfig = {
		"empUserListUrl":"${rootPath}/empUserMngt/list.html"
		,"addEmpUserUrl":"${rootPath}/empUserMngt/add.html"
		,"saveEmpUserUrl":"${rootPath}/empUserMngt/save.html"
		,"removeEmpUserUrl":"${rootPath}/empUserMngt/remove.html"
	};
	
</script>

