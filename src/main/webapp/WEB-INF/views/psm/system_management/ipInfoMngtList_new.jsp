<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<c:set var="currentMenuId" value="${index_id}" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />


<script
	src="${rootPath}/resources/js/psm/system_management/ipInfoMngList.js"
	type="text/javascript" charset="UTF-8"></script>

<style type="text/css">
.input_date {
	background-color: #eef1f5;
	padding: 0.3em 0.5em;
	border: 1px solid #e4e4e4;
	height: 30px;
	font-size: 12px;
	color: #555;
}
</style>

<h1 class="page-title">${currentMenuName}</h1>
<div class="portlet light portlet-fit portlet-datatable bordered">
	<div class="portlet-body">
		<div class="table-container">

			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">

				<div class="dataTables_scroll" style="position: relative;">
					<div>
						<div class="col-md-6"
							style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
							<div class="portlet box grey-salt  ">
		                         <div class="portlet-title" style="background-color: #2B3643;">
		                             <div class="caption">
		                                 <i class="fa fa-search"></i>검색 </div>
		                                 <div class="tools">
							                 <a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
							             </div>
		                         </div>
		                         <div id="searchBar" class="portlet-body form" >
		                             <div class="form-body" style="padding-left:20px; padding-right:10px;">
		                                 <div class="form-group">
		                                     <form id="misForm" method="POST" class="mt-repeater form-horizontal">
		                                         <div data-repeater-list="group-a">
		                                             <div data-repeater-item class="mt-repeater-item">
		                                                 <!-- jQuery Repeater Container -->
		                                                 <div class="mt-repeater-input">
		                                                     <label class="control-label">사용자ID</label>
		                                                     <br/>
		                                                     <input name="user_Id" id="user_Id"
		                                                     	value="${paramBean.user_Id}" 
																style="float: none;"
																class="form-control input-medium"/>
														</div>
														 <div class="mt-repeater-input">
		                                                     <label class="control-label">사용자IP</label>
		                                                     <br/>
		                                                     <input type="text" class="form-control input-medium"
																name="login_Ip"
																value="${paramBean.login_Ip}" />
														</div>
		                                                 
		                                             </div>
		                                         </div>
		                                         <div align="right">
		                                          	<button type="reset"
														class="btn btn-sm red-mint btn-outline sbold uppercase"
														onclick="resetOptions(ipInfoListConfig['listUrl'])">
														<i class="fa fa-remove"></i> <font>초기화
													</button>
													<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
														onclick="moveIpInfoList()">
														<i class="fa fa-search"></i> 검색
													</button>
												</div>
												<input type="hidden" name="use_flag" value="" /> <input
													type="hidden" name="misdetect_id" value="" /> <input
													type="hidden" name="main_menu_id"
													value="${paramBean.main_menu_id }" /> <input type="hidden"
													name="sub_menu_id" value="${paramBean.sub_menu_id }" /> <input
													type="hidden" name="current_menu_id" value="${currentMenuId}" />
												<input type="hidden" name="page_num"
													value="${paramBean.page_cur_num}" />
												<input type="hidden" name="isSearch" value="${paramBean.isSearch }"/>
		                                     </form>
		                                 </div>
		                             </div>
		                         </div>
		                     </div>
						</div>
					</div>

					
<!-- 					<form id="addForm" method="POST"> -->
						<!-- 개인정보 예외 정책 등록 -->
						<div class="caption">
							<i class="icon-settings font-dark"></i> <span
								class="caption-subject font-dark sbold uppercase">사용자IP 정보 등록</span>
						</div>
						<div>
							<table style="border-top: 1px solid #e7ecf1"
								class="table table-bordered"
								id="datatable_ajax_2" aria-describedby="datatable_ajax_2_info"
								role="grid"
								style="position: absolute; top: 0px; left: 0px; width: 100%;">
								<thead>
									<th width="33%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">
										사용자ID</th>
									<th width="33%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">
										사용자IP</th>
									<th width="33%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">
										추가</th>
								</thead>
								<tbody>
									<tr>
										<td align="center"><input type="text" name="add_Id"
											id="add_Id" class="form-control input-medium"/></td>
										<td align="center"><input type="text"
											name="add_Ip" id="add_Ip"
											class="form-control input-medium" /></td>
										<td align="center">
											<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
												onclick="javascript:addUserInfo()">
												<i class="fa fa-plus"></i> 추가 
											</button>
										</td>
									</tr>
								</tbody>
							</table>
						</div>

						<!-- 개인정보 예외 정책 관리 -->
						<div class="caption" style="padding-bottom: 15px;">
							<i class="icon-settings font-dark"></i> <span
								class="caption-subject font-dark sbold uppercase">사용자IP 정보 리스트</span>
						</div>
						<!-- <div class="dataTables_scrollBody" style="position: relative; overflow: auto; height: 400px; width: 100%;"> -->
						<div>

							<table style="border-top: 1px solid #e7ecf1"
								class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
								id="datatable_ajax_2" aria-describedby="datatable_ajax_2_info"
								role="grid"
								style="position: absolute; top: 0px; left: 0px; width: 100%;">

								<thead>
									<tr role="row" class="heading"
										style="background-color: #c0bebe;">
										<th width="10%"
											style="border-bottom: 1px solid #e7ecf1; text-align: center;">
											No.</th>
										<th width="30%"
											style="border-bottom: 1px solid #e7ecf1; text-align: center;">
											사용자ID</th>
										<th width="30%"
											style="border-bottom: 1px solid #e7ecf1; text-align: center;">
											사용자IP</th>
										<th width="25%"
											style="border-bottom: 1px solid #e7ecf1; text-align: center;">
											삭제</th>
									</tr>
								</thead>
								<tbody>
									<c:choose>
										<c:when test="${empty ipInfoList.ipInfo}">
											<tr>
												<td colspan="4" align="center">데이터가 없습니다.</td>
											</tr>
										</c:when>
										<c:otherwise>
											<c:set var="count" value="${ipInfoList.page_total_count }" />
											<c:forEach items="${ipInfoList.ipInfo}" var="list"
												varStatus="status">
												<tr>
<%-- 													<td><c:out value="${status.count}"/></td> --%>
													<td>${count - paramBean.page_cur_num * paramBean.page_size + paramBean.page_size }</td>
													<td style="padding-left: 20px;"><c:out value="${list.user_Id}"/></td>
													<td style="padding-left: 20px;"><c:out value="${list.login_Ip}"/></td>
													<td style="text-align: center">
														<p class="btn btn-xs red btn-outline filter-cancel"
															onclick="javascript:deleteUserInfo('${list.user_Id}', '${list.login_Ip}')">삭제</p>
													</td>
												</tr>
												<c:set var="count" value="${count - 1 }" />
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>

						</div>
<!-- 					</form> -->

					<div class="dataTables_processing DTS_Loading"
						style="display: none;">Please wait ...</div>
				</div>
				<div class="row" style="padding: 10px;">
					<!-- 페이징 영역 -->
					<c:if test="${ipInfoList.page_total_count > 0}">
						<div align="center" id="pagingframe">
							<p>
								<ctl:paginator currentPage="${paramBean.page_cur_num}"
									rowBlockCount="${paramBean.page_size}"
									totalRowCount="${ipInfoList.page_total_count}" />
							</p>
						</div>
					</c:if>
				</div>
			</div>

		</div>
	</div>
</div>

<script type="text/javascript">
	var ipInfoListConfig = {
		"listUrl":"${rootPath}/ipInfo/list.html"
	};	
</script>