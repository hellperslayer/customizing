<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/system_management/agentMngtList.js" type="text/javascript" charset="UTF-8"></script>

<h1 class="page-title">${currentMenuName}</h1>
<div class="row">

	<div class="portlet light portlet-fit bordered">
		<div class="portlet-body">
			<div class="row">
				<div class="col-md-12">
					<form id="agentDetailForm" method="POST"
						class="form-horizontal form-bordered form-row-stripped">
						<table id="user" class="table table-bordered table-striped">
							<tbody>
								<tr>
									<c:choose>
										<c:when test="${empty agent}">
											<th style="width: 15%; text-align: center;"><label
												class="control-label" for="agent_seq">에이전트ID<span
													class="required">*</span>
											</label></th>
											<td style="width: 35%; vertical-align: middle;" class="form-group form-md-line-input">
												<input type="text" class="form-control" id="agent_seq"
												name="agent_seq">
												<div class="form-control-focus"></div>
											</td>
										</c:when>
										<c:otherwise>
											<%-- <th style="width: 15%; text-align: center;"><label
												class="control-label" for="form_control_1">권한ID <span
													class="required">*</span>
											</label></th>
											<td style="width: 35%; vertical-align: middle;"
												class="form-group form-md-line-input"><c:out
													value="${authDetail.auth_id }" /> <input type="hidden"
												name="auth_id" value="${authDetail.auth_id }" />
												<div class="form-control-focus"></div></td> --%>
<%-- 											<input type="hidden" name="auth_id" value="${authDetail.auth_id }" /> --%>
											<th style="width: 15%; text-align: center;"><label
												class="control-label" for="agent_seq">에이전트ID <span
													class="required">*</span>
											</label></th>
											<td style="width: 35%; vertical-align: middle;" class="form-group form-md-line-input">
												<input type="text" class="form-control" id="agent_seq" name="agent_seq" value="${agent.agent_seq }" readonly="readonly">
												<div class="form-control-focus"></div>
											</td>
										</c:otherwise>
									</c:choose>
									<th style="width: 15%; text-align: center;">
									<label class="control-label" for="form_control_1">에이전트명 <span class="required">*</span>
									</label>
									</th>
									<td style="width: 35%; vertical-align: middle;" class="form-group form-md-line-input">
										<input type="text" class="form-control" id="agent_name" name="agent_name" value="${agent.agent_name }">
										<div class="form-control-focus"></div>
									</td>
								</tr>
								<tr>
									<th style="text-align: center;"><label
										class="control-label">동작위치(IP) <span class="required">*</span>
									</label></th>
									<td class="form-group form-md-line-input">
										<input type="text" class="form-control" id="ip" name="ip" value="${agent.ip }">
										<div class="form-control-focus"></div>
									</td>
									<th style="text-align: center;"><label
										class="control-label">PORT <span class="required">*</span>
									</label></th>
									<td class="form-group form-md-line-input">
										<input type="text" class="form-control" id="port" name="port" value="${agent.port }">
										<div class="form-control-focus"></div>
									</td>
								</tr>
								
								<tr>
									<th style="text-align: center;"><label
										class="control-label">수집서버 명칭 <span class="required"></span>
									</label></th>
									<td class="form-group form-md-line-input">
										<input type="text" class="form-control" id="manager_name" name="manager_name" value="${agent.manager_name }">
										<div class="form-control-focus"></div>
									</td>
									<th style="text-align: center;"><label
										class="control-label">수집서버 IP <span class="required"></span>
									</label></th>
									<td class="form-group form-md-line-input">
										<input type="text" class="form-control" id="manager_ip" name="manager_ip" value="${agent.manager_ip }">
										<div class="form-control-focus"></div>
									</td>
								</tr>
							
								<tr>
									<th style="text-align: center;">
										<label class="control-label">로그생성방식 선택<span class="required">*</span></label>
									</th>
									
									<td class="form-group form-md-line-input" style="vertical-align: middle;">
										<select name="agent_type" class="ticket-assign form-control input-medium">
										<option value="" ${agent.agent_type == '' ? 'selected="selected"' : ''}>----- 선 택 -----</option>
										<c:choose>
											<c:when test="${empty CACHE_AGENT_TYPE}">
											    <option value="">로그생성방식 선택</option>
											</c:when>
											<c:otherwise>
												<c:forEach items="${CACHE_AGENT_TYPE}" var="i" varStatus="z">
													<option  value="${i.key}" ${i.key==agent.agent_type ? "selected=selected" : "" }>${ i.value}</option>
												</c:forEach>
											</c:otherwise>
										</c:choose>	
										</select>
									</td>
									
									<th style="text-align: center;">
										<label class="control-label">대상시스템<span class="required">*</span></label>
									</th>
									<td class="form-group form-md-line-input" style="vertical-align: middle;">
										<select name="system_seq" class="ticket-assign form-control input-medium">
										<option value="" ${agent.system_seq == '' ? 'selected="selected"' : ''}>----- 선 택 -----</option>
										<c:choose>
											<c:when test="${empty systemMasterList}">
											    <option>시스템 없음</option>
											</c:when>
											<c:otherwise>
												<c:forEach items="${systemMasterList}" var="i" varStatus="z">
													<option  value="${i.system_seq}" ${i.system_seq==agent.system_seq ? "selected=selected" : "" }>${ i.system_name}</option>
												</c:forEach>
											</c:otherwise>
										</c:choose>	
										</select>
									</td>
									
								</tr>
								<tr>
									<th style="text-align: center;"><label class="control-label">설명</span></label></th>
									<td colspan="3" style="vertical-align: middle;" class="form-group form-md-line-input">
										<input type="text" class="form-control" name="description" value="${agent.description }">
										<div class="form-control-focus"></div>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
					<form id="agentListForm" method="POST">
						<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" /> 
						<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" /> 
						<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-offset-2 col-md-10" align="right"
					style="padding-right: 30px;">
					<button type="button"
						class="btn btn-sm grey-mint btn-outline sbold uppercase"
						onclick="moveAgentList()">
						<i class="fa fa-list"></i> 목록
					</button>
					<c:choose>
						<c:when test="${empty agent}">
							<button type="button"
								class="btn btn-sm blue btn-outline sbold uppercase"
								onclick="addAgent()">
								<i class="fa fa-check"></i> 등록
							</button>
						</c:when>
						<c:otherwise>
							<button type="button"
								class="btn btn-sm blue btn-outline sbold uppercase"
								onclick="saveAgent()">
								<i class="fa fa-check"></i> 수정
							</button>
							<button type="button"
								class="btn btn-sm red-mint btn-outline sbold uppercase"
								onclick="removeAgent()">
								<i class="fa fa-close"></i> 삭제
							</button>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var agentListConfig = {
		"listUrl" : "${rootPath}/agentMngt/list.html",
		"addUrl" : "${rootPath}/agentMngt/add.html",
		"saveUrl" : "${rootPath}/agentMngt/save.html",
		"removeUrl" : "${rootPath}/agentMngt/remove.html"
	};
</script>