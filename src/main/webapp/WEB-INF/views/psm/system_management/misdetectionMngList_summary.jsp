<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/system_management/misdetectSummary.js" type="text/javascript" charset="UTF-8"></script>
<h1 class="page-title">${currentMenuName}</h1>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light portlet-fit portlet-datatable bordered">
			<div class="portlet-body">
				<div class="table-container">
					<div id="datatable_ajax_2_wrapper"
						class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
						<div class="raw">
							<div class="col-md-6"
								style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
								
								<div class="portlet box grey-salt  ">
			                         <div class="portlet-title" style="background-color: #2B3643;">
			                             <div class="caption">
			                                 <i class="fa fa-search"></i>검색 & 엑셀 </div>
			                              <div class="tools">
							                 <a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
							             </div>
			                         </div>
			                         <div id="searchBar" class="portlet-body form" >
			                             <div class="form-body" style="padding-left:20px; padding-right:10px;">
			                                 <div class="form-group">
			                                     <form id="listForm" method="POST" class="mt-repeater form-horizontal">
			                                         <div data-repeater-list="group-a">
			                                             <div class="row">
			                                                 <!-- jQuery Repeater Container -->
			                                                 <div class="col-md-2">
			                                                     <label class="control-label">예외유형</label>
			                                                    <select name="privacy_type" id="privacy_type" style="float: none;"	class="form-control">
																	<option value="">전체유형</option>
																	<c:forEach items="${CACHE_RESULT_TYPE}" var="i" varStatus="z">
																		<option value="${i.key}" ${i.key==misdetectDetail.privacy_type ? "selected=selected" : "" }>${ i.value}</option>
																	</c:forEach>
																</select> 
															</div>
			                                                 <div class="col-md-2">
			                                                   	<label class="control-label">예외 개인정보</label>
                                                   				<input type="text" class="form-control" name="search_data" value="" />
															</div>
			                                             </div>
			                                         </div>
			                                         <hr/>
			                                         <div align="right">
														<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
															onclick="moveList(1)">
															<i class="fa fa-search"></i> 검색
														</button>&nbsp;&nbsp;
													</div>
											
													<input type="hidden" name="log_seq" value="" /> 
													<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" /> 
													<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" /> 
													<input type="hidden" name="current_menu_id" value="${currentMenuId}" /> 
													<input type="hidden" name="page_num" value="${parameters.page_num}" />
													<input type="hidden" name="isSearch" value="${paramBean.isSearch }"/>
			                                     </form>
			                                 </div>
			                             </div>
			                         </div>
			                     </div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12" align="left">
								<a class="btn btn-sm blue btn-outline sbold uppercase"
									onclick="misDetectDetail()">신규 <i class="fa fa-plus"></i></a>
							</div>
						</div>
						<div class="raw">
							<table style="border-top: 1px solid #e7ecf1;"
								class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
								role="grid">
								<colgroup>
									<col width="5%" />
									<col width="15%" />
									<col width="10%" />
									<col width="10%" />
									<col width="50%" />
									<col width="10%" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col"
											style="text-align: center; vertical-align: middle;">번호</th>
										<th scope="col"
											style="text-align: center; vertical-align: middle;">타이틀명</th>
										<th scope="col"
											style="text-align: center; vertical-align: middle;">예외DB개인정보유형</th>
										<th scope="col"
											style="text-align: center; vertical-align: middle;">제외유형</th>
										<th scope="col"
											style="text-align: center; vertical-align: middle;">정규식</th>
										<th scope="col"
											style="text-align: center; vertical-align: middle;">사용여부</th>
										
									</tr>
								</thead>
								<tbody>
									<c:choose>
										<c:when test="${empty misdetectList}">
											<tr>
												<td colspan="6" align="center">데이터가 없습니다.</td>
											</tr>
										</c:when>
										<c:otherwise>
											<c:forEach items="${misdetectList}" var="misdetect" varStatus="status">
												<tr style='cursor: pointer;' onclick="misDetectDetail(${misdetect.log_seq })">
													<td style="text-align: center; vertical-align: middle;">
														${misdetect.log_seq }
													</td>
													<td style="text-align: left; vertical-align: middle;">
														${misdetect.result_title }
													</td>
													<td style="text-align: center; vertical-align: middle;">
														<ctl:code value="${misdetect.privacy_type }" groupId="RESULT_TYPE"/> 
													</td>
													<td style="text-align: center; vertical-align: middle;">
														${mistypeMap.get(misdetect.misdetect_type) }
													</td>
													<td style="text-align: center; vertical-align: middle;">
														<input type="text" style="border: none; width: 100%; user-select:none" 
														<c:choose>
															<c:when test="${misdetect.misdetect_type eq '1' }">
																value="${misdetect.range_to } ~ ${misdetect.range_from }"
															</c:when>
															<c:otherwise>
																value="${misdetect.result_content }"
															</c:otherwise>
														</c:choose>
														>
													</td>
													<td style="text-align: center; vertical-align: middle;">
														<c:if test="${misdetect.use_yn eq 'Y'}">
															<span class="label label-sm label-success"> 사용 </span>
														</c:if>
														<c:if test="${misdetect.use_yn eq 'N'}">
															<span class="label label-sm label-warning ">미사용 </span>
														</c:if>
													</td>
												</tr>
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
							<!-- 페이징 영역 -->
							<c:if test="${parameters.total_count > 0}">
								<div class="page left" id="pagingframe" align="center">
									<p>
										<ctl:paginator currentPage="${parameters.page_num}"
											rowBlockCount="${parameters.size}"
											totalRowCount="${parameters.total_count}" />
									</p>
								</div>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var misdetectConfig = {
		"listUrl" : "${rootPath}/misdetect/list_summary.html",
		"detailUrl" : "${rootPath}/misdetect/detail_summary.html",
		"downloadUrl" : "${rootPath}/misdetect/download_summary.html",
		"menu_id" : "${currentMenuId}"
	};
</script>