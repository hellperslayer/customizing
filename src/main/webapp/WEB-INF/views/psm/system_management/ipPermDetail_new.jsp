<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<c:set var="currentMenuId" value="${index_id}" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script
	src="${rootPath}/resources/js/psm/system_management/ipPermDetail.js"
	type="text/javascript" charset="UTF-8"></script>

<h1 class="page-title">${currentMenuName}</h1>
<div class="portlet light portlet-fit bordered">
	<div class="portlet-body">
		<!-- BEGIN FORM-->
		<form id="ipPermDetailForm" method="POST"
			class="form-horizontal form-bordered form-row-stripped">
			<table id="user" class="table table-bordered table-striped">
				<tbody>
					<c:choose>
					<c:when test="${empty ipPermDetail}">
					<tr>
						<th style="width: 30%; text-align: center;"><label
							class="control-label">관리자 ID<span class="required">*</span>
						</label></th>
						<td style="width: 70%; vertical-align: middle;" class="form-group form-md-line-input">
							<select name="admin_id" class="form-control input-medium">
								<option value="">----- 선택 -----</option>
								<c:forEach items="${adminUsers }" var="admin">
									<option value="${admin.admin_user_id }">${admin.admin_user_id }(${admin.admin_user_name })</option>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<th style="width: 30%; text-align: center;"><label
							class="control-label">권한 IP대역<span class="required">*</span>
						</label></th>
						<td style="width: 70%; vertical-align: middle;"
							class="form-group form-md-line-input"><input type="text"
							class="form-control" name="ip_content">
							<div class="form-control-focus"></div></td>
					</tr>
					</c:when>
					<c:otherwise>
					<tr>
						<th style="width: 30%; text-align: center;"><label
							class="control-label">관리자 ID<span class="required">*</span>
						</label></th>
						<td style="width: 70%; vertical-align: middle;"
							class="form-group form-md-line-input"><input type="text"
							class="form-control" name="admin_id"
							value="${ipPermDetail.admin_id}"> 
							<div class="form-control-focus"></div></td>
					</tr>
					<tr>
						<th style="width: 30%; text-align: center;"><label
							class="control-label">권한 IP<span class="required">*</span>
						</label></th>
						<td style="width: 70%; vertical-align: middle;"
							class="form-group form-md-line-input"><input type="text"
							class="form-control" name="ip_content"
							value="${ipPermDetail.ip_content}"> <input
							type="hidden" name="ip_seq" value="${ipPermDetail.ip_seq}" />
							<div class="form-control-focus"></div></td>
					</tr>
					</c:otherwise>
					</c:choose>
					
					<tr>
						<th style="text-align: center;"><label class="control-label">사용여부
								<span class="required">*</span>
						</label></th>
						<td class="form-group form-md-line-input">
							<div class="md-radio-inline">
								<div class="md-radio col-md-4">
									<input type="radio" id="checkbox1_1" name="use_flag" value="Y"
										class="md-radiobtn"
										${fn:containsIgnoreCase(ipPermDetail.use_flag,'Y')?'checked':'' }>
									<label for="checkbox1_1"> <span></span> <span
										class="check"></span> <span class="box"></span> 사용
									</label>
								</div>
								<div class="md-radio col-md-4">
									<input type="radio" id="checkbox1_2" name="use_flag" value="N"
										class="md-radiobtn"
										${!fn:containsIgnoreCase(ipPermDetail.use_flag,'Y')?'checked':'' }>
									<label for="checkbox1_2"> <span></span> <span
										class="check"></span> <span class="box"></span> 미사용
									</label>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
		<form id="ipPermListForm" method="POST">
			<input type="hidden" name="main_menu_id"
				value="${paramBean.main_menu_id }" /> <input type="hidden"
				name="sub_menu_id" value="${paramBean.sub_menu_id }" /> <input
				type="hidden" name="current_menu_id" value="${currentMenuId}" />
		</form>
		<div class="form-actions">
			<div class="row">
				<div class="col-md-offset-2 col-md-10" align="right"
					style="padding-right: 30px;">
					<button type="button"
						class="btn btn-sm grey-mint btn-outline sbold uppercase"
						onclick="moveIpPermList()">
						<i class="fa fa-list"></i> 목록
					</button>
					<c:choose>
						<c:when test="${empty ipPermDetail}">
							<button type="button"
								class="btn btn-sm blue btn-outline sbold uppercase"
								onclick="addIpPerm()">
								<i class="fa fa-check"></i> 등록
							</button>
						</c:when>
						<c:otherwise>
							<button type="button"
								class="btn btn-sm blue btn-outline sbold uppercase"
								onclick="saveIpPerm()">
								<i class="fa fa-check"></i> 수정
							</button>
							<button type="button"
								class="btn btn-sm red-mint btn-outline sbold uppercase"
								onclick="removeIpPerm()">
								<i class="fa fa-close"></i> 삭제
							</button>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
		<!-- END FORM-->
	</div>
</div>
<script type="text/javascript">
	
	var ipPermConfig = {
		"listUrl":"${rootPath}/ipPerm/list.html"
		,"addUrl":rootPath + "/ipPerm/add.html"
		,"saveUrl":rootPath + "/ipPerm/save.html"
		,"removeUrl":rootPath + "/ipPerm/remove.html"
		,"loginPage" : "${rootPath}/loginView.html"
	};
	
</script>

