
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<c:set var="currentMenuId" value="${index_id}" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />
<c:set var="adminUserAuthId" value="${userSession.auth_id}" />

<script src="${rootPath}/resources/js/psm/system_management/alarmMngt.js" type="text/javascript" charset="UTF-8"></script>

<h1 class="page-title">${currentMenuName}</h1>

<form id="reloadForm" method="POST">
	<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" />
	<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
	<input type="hidden" name="current_menu_id" value="${currentMenuId }" />
</form>

<div class="row">
	<div class="col-md-8">
		<form id="optionForm" method="POST">
		<table class="table table-striped table-bordered order-column">
			<c:set var="agentSeqList" value=""/>
			<c:forEach items="${list }" var="item" varStatus="status">
				<tr>
					<input type="hidden" id="${item.agent_seq }_agent_options" value="" /> 
					<input type="hidden" class="agent_seq" value="${item.agent_seq }">
					<td width="20%" style="text-align: center; vertical-align: middle;" rowspan="4" class='optionList1'>${item.option_name }</td>
					<td class="form-group form-md-line-input" style="vertical-align: middle;">
						DBTYPE : <input class="form-control " style="display: inline;width:80%;" type="text" id="${item.agent_seq }_dbtype" value="${item.dbtype }" />
					</td>
				</tr>
				<tr>
					<td class="form-group form-md-line-input" style="vertical-align: middle;">
						JDBC_URL : <input class="form-control " style="display: inline;width:80%;" type="text" id="${item.agent_seq }_jdbc_url" value="${item.jdbc_url }" />
					</td>
				</tr>
				<tr>
					<td class="form-group form-md-line-input" style="vertical-align: middle;">
						ID : <input class="form-control " style="display: inline;width:80%;" type="text" id="${item.agent_seq }_id" value="${item.id }" />
					</td>
				</tr>
				<tr>
					<td class="form-group form-md-line-input" style="vertical-align: middle;">
						PW : <input class="form-control " style="display: inline;width:80%;" type="text" id="${item.agent_seq }_pw" value="${item.pw }" />
					</td>
				</tr>
				<c:if test="${status.count == 1 }"><c:set var="agentSeqList" value="${item.agent_seq}"/></c:if>
				<c:if test="${status.count != 1 }"><c:set var="agentSeqList" value="${agentSeqList },${item.agent_seq}"/></c:if>
				
			</c:forEach>
			<c:forEach items="${list2 }" var="item" varStatus="status">
				<tr>
					<input type="hidden" id="${item.agent_seq }_agent_options" value="" /> 
					<input type="hidden" class="agent_seq" value="${item.agent_seq }">
					<td width="20%" style="text-align: center; vertical-align: middle;" rowspan="4" class='optionList2'>${item.option_name }</td>
					<td class="form-group form-md-line-input" style="vertical-align: middle;">
						사이트명 : <input class="form-control " style="display: inline;width:80%;" type="text" id="${item.agent_seq }_SiteName" value="${item.site_name }" />
					</td>
				</tr>
				<tr>
					<td class="form-group form-md-line-input" style="vertical-align: middle;">
						송신자 Email : <input class="form-control " style="display: inline;width:80%;" type="text" id="${item.agent_seq }_SenderEmail" value="${item.id }" />
					</td>
				</tr>
				<tr>
					<td class="form-group form-md-line-input" style="vertical-align: middle;">
						송신자 PASS : <input class="form-control " style="display: inline;width:80%;" type="password" id="${item.agent_seq }_SenderPass" value="${item.pw }" />
					</td>
				</tr>
				<tr>
					<td class="form-group form-md-line-input" style="vertical-align: middle;">
						수신자 Eamil : <input class="form-control " style="display: inline;width:80%;" type="text" id="${item.agent_seq }_ReceiverEmail" value="${item.to_id }" />
					</td>
				</tr>
				<td></td>
				<td style="float: right"><a style="text-align: right;" onclick="testmail('${item.agent_seq }')" class="btn btn-sm blue btn-outline sbold uppercase"><i class="fa fa-check"></i>테스트메일 발송</a></td>
				<c:if test="${status.count == 1 }"><c:set var="agentSeqList" value="${item.agent_seq}"/></c:if>
				<c:if test="${status.count != 1 }"><c:set var="agentSeqList" value="${agentSeqList },${item.agent_seq}"/></c:if>
				
			</c:forEach>
		</table>
		<input type="hidden" id="tot_agent_seq" name="tot_agent_seq" value="" />
		<input type="hidden" id="tot_agent_options" name="tot_agent_options" value="" />
		</form>
	</div>
	
	<div class="col-md-8" style="text-align: right">
		<a style="text-align: right" onclick="setAlarmMngt()" class="btn btn-sm blue btn-outline sbold uppercase"><i class="fa fa-check"></i> 수정</a>
	</div>
</div>

<input type="hidden" id="listLength" value="${fn:length(list) }" />
<input type="hidden" id="agentSeqList" value="${agentSeqList }" />
