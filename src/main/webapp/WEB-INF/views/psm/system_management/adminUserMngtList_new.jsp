<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script
	src="${rootPath}/resources/js/psm/system_management/adminUserMngtList.js"
	type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/ezDatepicker.js"
	type="text/javascript" charset="UTF-8"></script>
<script
	src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js"
	type="text/javascript"></script>

<!-- BEGIN PAGE TITLE-->
<h1 class="page-title">${currentMenuName}</h1>
<!-- END PAGE TITLE-->

<!-- END PAGE HEADER-->
<div class="portlet light portlet-fit portlet-datatable bordered">
	<%-- <div class="portlet-title">
		<div class="caption">
			<i class="icon-settings font-dark"></i> <span
				class="caption-subject font-dark sbold uppercase">${currentMenuName}
				리스트</span>
		</div>
		<div class="actions">
			<div class="btn-group">
				<a class="btn red btn-outline btn-circle" onclick="excelAdminUserList()"
					data-toggle="dropdown"> <i class="fa fa-share"></i> <span
					class="hidden-xs"> 엑셀 </span>
				</a>
			</div>
		</div>
	</div> --%>
	<div class="portlet-body">
		<div class="table-container">
			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">

				<div class="col-md-6"
					style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
					
						<%-- <div class="portlet box grey-salsa">
							<div class="portlet-title" style="background-color: #2B3643;">
								<div class="caption">
									<img src="${rootPath}/resources/image/icon/search.png"> 검색 & 엑셀
								</div>
								<div class="tools">
									<a href="javascript:;" class="expand"></a>
								</div>
								<div style="float: right; padding: 12px 10px 8px;"><b>${textSearch }</b></div>
							</div>

							<!-- portlet-body S -->

							<div class="portlet-body" style="display:none;">
							<form id="adminUserListForm"
								action="${rootPath}/adminUserMngt/list.html" method="POST">
								<div class="table-container">
									<div id="datatable_ajax_2_wrapper"
										class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
										<div class="row" style="padding-bottom: 5px;">
											<div class="col-md-4">
												<div class="col-md-5">
													◎ 관리자ID :
												</div>
												<div class="col-md-7">
													<input type="text"
														class="form-control form-filter input-sm"
														name="admin_user_id_1" value="${search.admin_user_id_1 }"
														style="width: 100%; margin: 0 auto;">
												</div>
											</div>
											<div class="col-md-4">
												<div class="col-md-5">
													◎ 관리자명:
												</div>
												<div class="col-md-7">
													<input type="text"
														class="form-control form-filter input-sm"
														name="admin_user_name" value="${search.admin_user_name }"
														style="width: 100%; margin: 0 auto;">
												</div>
											</div>
											<div class="col-md-4">
												<div class="col-md-5">
													◎ 소속:
												</div>
												<div class="col-md-7">
													<select name="dept_id" id="dept_id"
														class="ticket-assign form-control input-small"
														style="width: 100%; margin-left: 0px; ">
														<option value=""
															${search.dept_id == '' ? 'selected="selected"' : ''}>
															----- 전 체 -----</option>
														<c:if test="${empty deptList}">
															<option>소속 데이터 없습니다.</option>
														</c:if>
														<c:if test="${!empty deptList}">
															<c:forEach items="${deptList}" var="i" varStatus="z">
																<option value="${i.dept_id}"
																	${i.dept_id==search.dept_id ? "selected=selected" : "" }>${ i.simple_dept_name}</option>
															</c:forEach>
														</c:if>
													</select>
													<label>
														<input class="deptFlagChk" type="checkbox" name="deptLowSearchFlag" ${search.deptLowSearchFlag == 'on' ? 'checked="checked"' : ''} />&nbsp;하위부서포함
													</label>
												</div>
											</div>
										</div>
										<div class="row">
											<br>
											<div class="col-md-12" align="right"
												style="padding-right: 20px;">
												<button class="btn btn-sm red table-group-action-submit"
													onclick="resetOptions(adminUserListConfig['listUrl'])">
													<i class="fa fa-remove"></i> 취소
												</button>
												<button class="btn btn-sm green table-group-action-submit"
													onclick="moveAdminUserList()">
													<i class="fa fa-check"></i> 검색
												</button>&nbsp;&nbsp;
												<!-- <a class="btn red btn-outline btn-circle" onclick="excelAdminUserList()"
													data-toggle="dropdown"> <i class="fa fa-share"></i> <span
													class="hidden-xs"> 엑셀 </span>
												</a> -->
												<a onclick="excelAdminUserList()"><img width="62px;" src="${rootPath}/resources/image/icon/XLS_1.png"></a>
											</div>
										</div>
									</div>
									<!-- portlet-boyd E -->
								</div>
								
								<input type="hidden" name="main_menu_id"
									value="${paramBean.main_menu_id }" /> <input type="hidden"
									name="sub_menu_id" value="${paramBean.sub_menu_id }" /> <input
									type="hidden" name="current_menu_id" value="${currentMenuId}" />
								<input type="hidden" name="page_num" value="${search.page_num}" />
								</form>
							</div>
							
						</div> --%>
						
						<div class="portlet box grey-salt  ">
                          <div class="portlet-title" style="background-color: #2B3643;">
                              <div class="caption">
                                  <i class="fa fa-search"></i>검색 & 엑셀</div>
                              <div class="tools">
				                 <a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
				             </div>
                          </div>
                          <div id="searchBar" class="portlet-body form" >
                              <div class="form-body" style="padding-left:10px; padding-right:10px;">
                                  <div class="form-group">
                                      <form id="adminUserListForm" method="POST" class="mt-repeater form-horizontal">
                                          <div data-repeater-list="group-a">
                                              <div class="row">
                                                  <!-- jQuery Repeater Container -->
                                                  <div class="col-md-2">
                                                      <label class="control-label">관리자ID</label>
                                                      <%-- <input type="text" class="form-control input-medium" name="admin_user_id_1" value="${search.admin_user_id_1 }"> --%> 
                                                      <input type="text" class="form-control" name="admin_user_id_1" value="${search.admin_user_id_1 }"/>
                                                  </div>
                                                  <div class="col-md-2">
                                                      <label class="control-label">관리자명</label>
                                                      <%-- <input type="text" class="form-control input-medium" name="admin_user_name" value="${search.admin_user_name }"> --%>
                                                      <input type="text" class="form-control" name="admin_user_name" value="${search.admin_user_name }"/>
                                                  </div>
                                                  <div class="col-md-2">
                                                      <label class="control-label">소속</label>
                                                      <select name="dept_id" id="dept_id" class="form-control">
														<option value=""
															${search.dept_id == '' ? 'selected="selected"' : ''}>
															----- 전 체 -----</option>
														<c:if test="${empty deptList}">
															<option>소속 데이터 없습니다.</option>
														</c:if>
														<c:if test="${!empty deptList}">
															<c:forEach items="${deptList}" var="i" varStatus="z">
																<option value="${i.dept_id}"
																	${i.dept_id==search.dept_id ? "selected=selected" : "" }>${ i.simple_dept_name}</option>
															</c:forEach>
														</c:if>
													</select>
													<div class="" style="padding-left: 0px;">
	                                                    <div class="mt-checkbox-inline">└
	                                                        <label class="mt-checkbox mt-checkbox-outline" style="margin-bottom: 0px;">
	                                                            <input type="checkbox" name="deptLowSearchFlag"
																${search.deptLowSearchFlag == 'on' ? 'checked="checked"' : ''}
																style="float: none;" />하위소속포함
	                                                            <span></span>
	                                                        </label>
	                                                    </div>
		                                            </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <hr/>
                                          <div align="right">
                                           <button type="reset"
											class="btn btn-sm red-mint btn-outline sbold uppercase"
											onclick="resetOptions(adminUserListConfig['listUrl'])">
											<i class="fa fa-remove"></i> <font>초기화
										</button>
										<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
											onclick="moveAdminUserList()">
											<i class="fa fa-search"></i> 검색
										</button>&nbsp;&nbsp;
										<a onclick="excelAdminUserList()"><img src="${rootPath}/resources/image/icon/XLS_3.png"></a>										
									</div>
									
									<input type="hidden" name="main_menu_id"
										value="${paramBean.main_menu_id }" /> <input type="hidden"
										name="sub_menu_id" value="${paramBean.sub_menu_id }" /> <input
										type="hidden" name="current_menu_id" value="${currentMenuId}" />
									<input type="hidden" name="page_num" value="${search.page_num}" />
									<input type="hidden" name="isSearch" value="${paramBean.isSearch }" />
                                      </form>
                                  </div>
                              </div>
                          </div>
                      </div>
				</div>

				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-6">
							<div class="btn-group">
								<button id="menuMngt_new" class="btn btn-sm blue btn-outline sbold uppercase"
									onclick="findAdminUserDetail(null)">
									<i class="fa fa-plus"></i> 신규 
								</button>
							</div>
						</div>
					</div>
				</div>

				<table style="border-top: 1px solid #e7ecf1"
					class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
					style="position: absolute; top: 0px; left: 0px; width: 100%;">

					<thead>
						<tr role="row" class="heading" style="background-color: #c0bebe;">
							<th width="10%"
								style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">No.</th>
							<th width="8%"
								style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
								관리자ID</th>
							<th width="10%"
								style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
								관리자명</th>
							<th width="9%"
								style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center; padding: 0em;">
								소속</th>
							<th width="*%"
								style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
								E-MAIL</th>
							<th width="10%"
								style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
								권한명</th>
							<!-- <th width="16%"
								style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
								알람여부</th> -->
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${empty adminUserList}">
								<tr>
									<td colspan="8" align="center">데이터가 없습니다.</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:set value="${search.total_count}" var="count" />
								<c:forEach items="${adminUserList}" var="adminUser"
									varStatus="status">
									<tr role="row" class="odd" style="text-align: center; cursor: pointer;"
										onclick="findAdminUserDetail('${adminUser.admin_user_id}')">
										<td width="8%">${count - search.page_num * search.size + search.size }</td>
										<td width="12%"><c:out value="${adminUser.admin_user_id}"/></td>
										<td width="10%"><c:out value="${adminUser.admin_user_name}"/></td>
										<td width="12%"><c:out value="${adminUser.dept_name}"/></td>
										<td width="*%" style="text-align:left;"><c:out value="${adminUser.email_address}"/></td>
										<td width="15%"><c:out value="${adminUser.auth_name}"/></td>
										<%-- <td width="10%"><c:choose>
												<c:when
													test="${template_yesNo.options[adminUser.alarm_flag] == '사용'}">
													<span class="label label-sm label-success"> 사용 </span>
												</c:when>
												<c:otherwise>
													<span class="label label-sm label-warning">미사용</span>
												</c:otherwise>
											</c:choose></td> --%>
									</tr>
									<c:set var="count" value="${count - 1 }" />
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
				<div class="row" style="padding: 10px;">
					<!-- 페이징 영역 -->
					<c:if test="${search.total_count > 0}">
						<div id="pagingframe" align="center">
							<p>
								<ctl:paginator currentPage="${search.page_num}"
									rowBlockCount="${search.size}"
									totalRowCount="${search.total_count}" />
							</p>
						</div>
					</c:if>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var adminUserListConfig = {
		"listUrl" : "${rootPath}/adminUserMngt/list.html",
		"detailUrl" : "${rootPath}/adminUserMngt/detail.html",
		"downloadUrl" : "${rootPath}/adminUserMngt/download.html"
	};
</script>
