<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script
	src="${rootPath}/resources/js/psm/system_management/authMngtDetail.js"
	type="text/javascript" charset="UTF-8"></script>

<h1 class="page-title">${currentMenuName}</h1>
<div class="row">

	<div class="portlet light portlet-fit bordered">
		<div class="portlet-body">
			<div class="row">
				<div class="col-md-12">
					<form id="authDetailForm" method="POST"
						class="form-horizontal form-bordered form-row-stripped">
						<table id="user" class="table table-bordered table-striped">
							<tbody>
								<tr>
									<c:choose>
										<c:when test="${empty authDetail}">
											<th style="width: 15%; text-align: center;"><label
												class="control-label" for="form_control_1">권한명<span
													class="required">*</span>
											</label></th>
											<td style="width: 35%; vertical-align: middle;" colspan="3" class="form-group form-md-line-input">
												<input type="text" class="form-control" id="form_control_2"
												name="auth_name">
												<div class="form-control-focus"></div>
											</td>
										</c:when>
										<c:otherwise>
											<%-- <th style="width: 15%; text-align: center;"><label
												class="control-label" for="form_control_1">권한ID <span
													class="required">*</span>
											</label></th>
											<td style="width: 35%; vertical-align: middle;"
												class="form-group form-md-line-input"><c:out
													value="${authDetail.auth_id }" /> <input type="hidden"
												name="auth_id" value="${authDetail.auth_id }" />
												<div class="form-control-focus"></div></td> --%>
											<input type="hidden" name="auth_id" value="${authDetail.auth_id }" />
											<th style="width: 15%; text-align: center;"><label
												class="control-label" for="form_control_2">권한명 <span
													class="required">*</span>
											</label></th>
											<td colspan="3" style="width: 35%; vertical-align: middle;"
												class="form-group form-md-line-input"><input
												type="text" class="form-control" id="form_control_2"
												name="auth_name" value="${authDetail.auth_name }">
												<div class="form-control-focus"></div></td>
										</c:otherwise>
									</c:choose>
								</tr>
								<tr>
									<th style="text-align: center;"><label
										class="control-label">사용여부 <span class="required">*</span>
									</label></th>
									<td colspan="3" class="form-group form-md-line-input">
										<div class="md-radio-inline">
											<div class="md-radio col-md-3">
												<input type="radio" id="checkbox1_1" name="use_flag"
													value="Y" class="md-radiobtn"
													${fn:containsIgnoreCase(authDetail.use_flag,'Y') ? 'checked' : ''}>
												<label for="checkbox1_1"> <span></span> <span
													class="check"></span> <span class="box"></span> 사용
												</label>
											</div>
											<div class="md-radio col-md-3">
												<input type="radio" id="checkbox1_2" name="use_flag"
													value="N" class="md-radiobtn"
													${!fn:containsIgnoreCase(authDetail.use_flag,'Y') ? 'checked' : ''}>
												<label for="checkbox1_2"> <span></span> <span
													class="check"></span> <span class="box"></span> 미사용
												</label>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<th style="text-align: center;"><label
										class="control-label">권한설명</span>
									</label></th>
									<c:choose>
										<c:when test="${!empty authDetail}">
											<td colspan="3" style="vertical-align: middle;"
												class="form-group form-md-line-input"><input
												type="text" class="form-control" 
												name="comment" value="${authDetail.comment }">
												<div class="form-control-focus"></div></td>
										</c:when>
										<c:otherwise>
											<td style="vertical-align: middle;" colspan="3" class="form-group form-md-line-input">
												<input type="text" class="form-control" 
												name="comment">
												<div class="form-control-focus"></div>
											</td>
										</c:otherwise>
									</c:choose>
								</tr>
								<c:if test="${!empty authDetail }">
									<tr>
										<th style="text-align: center;"><label
											class="control-label">등록일시</label></th>
										<td class="form-group form-md-line-input"
											style="vertical-align: middle;"><fmt:formatDate
												value="${authDetail.insert_datetime }"
												pattern="yyyy-MM-dd kk:mm:ss" /></td>
										<th style="width: 15%; text-align: center;"><label
											class="control-label">수정일시 </label></th>
										<td class="form-group form-md-line-input"
											style="width: 35%; vertical-align: middle;"><fmt:formatDate
												value="${authDetail.update_datetime }"
												pattern="yyyy-MM-dd kk:mm:ss" /></td>
									</tr>
								</c:if>
							</tbody>
						</table>
						<div align="left" style="color:red">※ 권한을 부여할 시스템을 선택해주세요.</div>
						<div class="col-md-12" style="padding-left:0px; padding-right: 0px">
							<table id="user" class="table table-bordered table-striped">
								<tbody>
									<tr>
										<th style="width: 20%; text-align: center;">
											<label class="control-label"><input type="checkbox" id="sys_auth_ids" onchange="allCheck(this)" style="margin-right: 5px">전체선택
										</th>
										<th style="width: 30%; text-align: center;">
											<label class="control-label">시스템코드</label>
										</th>
										<th style="width: 30%; text-align: center;">
											<label class="control-label">시스템이름</label>
										</th>
										<!-- <th style="width: *0%; text-align: center;">
											<label class="control-label"><input type="checkbox" id="sys_report_ids" onchange="allCheck(this)" style="margin-right: 5px">보고서출력여부</label>
										</th> -->
									</tr>
									<c:forEach var="system" items="${systems }" varStatus="status">
									<tr>
										<td style="text-align: center">
											<input type="checkbox" id="sys_auth_ids${status.count }" class="sys_auth_ids" value="${system.system_seq }" onchange="allCheckCount('sys_auth_ids')"
												<c:if test="${!empty sysAuthMap[system.system_seq]}">checked='checked'</c:if>
											>
										</td>
										<td>${system.system_seq }</td>
										<td>${system.system_name }</td>
										<%-- <td style="text-align: center">
											<input type="checkbox" id="sys_report_ids${status.count }" class="sys_report_ids" value="${system.system_seq }" onchange="allCheckCount('sys_report_ids')"
												<c:if test="${!empty sysReportMap[system.system_seq]}">checked='checked'</c:if>
											>
										</td> --%>
									</tr>
									</c:forEach>
									<c:if test="${fn:length(systems) == 0 }">
									<tr>	
										<td style="text-align: center" colspan="4">
											시스템 코드를 확인할 수 없습니다.
										</td>
									</tr>
									</c:if>
									<input type="hidden" name="sys_auth_ids" value>
									<!-- <input type="hidden" name="sys_report_ids" value> -->
								</tbody>
							</table>
						</div>
						<div class="col-md-6">
							<h4><div align="left">권한별 생성가능한 보고서</div></h4>
							<table id="user" class="table table-bordered table-striped">
								<tbody>
									<tr>
										<th style="width: 20%; text-align: center;">
											<label class="control-label"><input type="checkbox" id="make_report_ids" onchange="allCheck(this)" style="margin-right: 5px">전체선택
										</th>
										<th style="width: *0%; text-align: center;">
											<label class="control-label">보고서제목</label>
										</th>
									</tr>
									<!-- 사용할수 있는 보고서 목록은 code 테이블에 있는 정보를 기준으로함 -->
									<c:forEach var="report" items="${reportcode }">
									<tr>
										<td style="text-align: center">
											<input type="checkbox" class="make_report_ids" value="${report.code_id }" onchange="allCheckCount('make_report_ids')"
												<c:if test="${!empty makeReportMap[report.code_id]}">checked='checked'</c:if>
											>
										</td>
										<td>${report.code_name }</td>
									</tr>
									</c:forEach>
									<c:if test="${fn:length(reportcode) == 0 }">
									<tr>	
										<td style="text-align: center" colspan="2">
											보고서 코드를 확인할 수 없습니다.
										</td>
									</tr>
									</c:if>
									<input type="hidden" name="make_report_ids" value>
								</tbody>
							</table>
						</div>
						<div class="col-md-6">
							<h4><div align="left">권한별 보고서 생성 권한조회</div></h4>
							<table id="user" class="table table-bordered table-striped">
								<tbody>
									<tr>
										<th style="width: 20%; text-align: center;">
											<label class="control-label">생성권한</label>
										</th>
										<th style="width: 20%; text-align: center;">
											<label class="control-label">사용자ID</label>
										</th>
										<th style="width: *0%; text-align: center;">
											<label class="control-label">사용자명</label>
										</th>
									</tr>
									<c:forEach var="user" items="${users }">
									<tr onclick="findAdminUserDetail('${user.admin_user_id }')" style="cursor: pointer;">
										<td style="text-align: center">
										<c:choose>
											<c:when test="${user.make_report_auth eq 'Y'}"><div style="color:#3598dc"><i class="fa fa-check"></i></div></c:when>
											<c:otherwise><div style="color:#cf1c28"><i class="fa fa-close"></i></div></c:otherwise>
										</c:choose>
										</td>
										<td>${user.admin_user_id }</td>
										<td>${user.admin_user_name }</td>
									</tr>
									</c:forEach>
									<c:if test="${fn:length(users) == 0 }">
									<tr>	
										<td style="text-align: center" colspan="3">
											해당 권한에 속한 유저가 없습니다
										</td>
									</tr>
									</c:if>
									<input type="hidden" name="report_auth_user" value>
									<input type="hidden" name="not_report_auth_user" value>
									<input type="checkbox" name="sub_batch" id="sub_batch" style="display: none"/>
								</tbody>
							</table>
						</div>
					</form>
					<form id="authListForm" method="POST">
						<input type="hidden" name="main_menu_id"
							value="${paramBean.main_menu_id }" /> <input type="hidden"
							name="sub_menu_id" value="${paramBean.sub_menu_id }" /> <input
							type="hidden" name="current_menu_id" value="${currentMenuId}" />
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-offset-2 col-md-10" align="right"
					style="padding-right: 30px;">
					<button type="button"
						class="btn btn-sm grey-mint btn-outline sbold uppercase"
						onclick="moveAuthList()">
						<i class="fa fa-list"></i> 목록
					</button>
					<c:choose>
						<c:when test="${empty authDetail}">
							<button type="button"
								class="btn btn-sm blue btn-outline sbold uppercase"
								onclick="addAuth()">
								<i class="fa fa-check"></i> 등록
							</button>
						</c:when>
						<c:otherwise>
							<button type="button"
								class="btn btn-sm btn-default btn-outline sbold uppercase"
								id="subBatchBtn" onclick="batchMenu()">
								<i id="checkFont" class="fa fa-square"></i> 일괄적용
							</button>
							<button type="button"
								class="btn btn-sm blue btn-outline sbold uppercase"
								onclick="saveAuth()">
								<i class="fa fa-check"></i> 수정
							</button>
							<button type="button"
								class="btn btn-sm red-mint btn-outline sbold uppercase"
								onclick="removeAuth()">
								<i class="fa fa-close"></i> 삭제
							</button>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
</div>
 <form id="adminUserListForm" method="POST">
 <input type="hidden" name="page_num" value="1">
 </form>
<script type="text/javascript">
	var authDetailConfig = {
		"listUrl" : "${rootPath}/authMngt/list.html",
		"addUrl" : "${rootPath}/authMngt/add.html",
		"saveUrl" : "${rootPath}/authMngt/save.html",
		"removeUrl" : "${rootPath}/authMngt/remove.html",
		"adminDetailUrl" : "${rootPath}/adminUserMngt/detail.html"
	};
</script>