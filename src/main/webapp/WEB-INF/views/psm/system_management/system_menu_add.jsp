<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />
<link rel="stylesheet" href="${rootPath}/resources/assets/global/plugins/jstree/dist/themes/default/style.min.css" />
<script src="${rootPath}/resources/js/common/jquery-ui-1.9.0.custom.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/jquery.cookie.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/system_management/system_menu.js" type="text/javascript" charset="UTF-8"></script>

<h1 class="page-title">${currentMenuName}</h1>
<div class="portlet light portlet-fit bordered">
	<div class="portlet-body">
		<!-- BEGIN FORM-->
		<form id="systemDetailForm2" method="POST" class="form-horizontal form-bordered form-row-stripped">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<th style="width: 25%; text-align: center;">
							<label class="control-label">시스템 코드<span class="required">*</span></label>
						</th>
						<td style="width: 25%; vertical-align: middle;" class="form-group form-md-line-input">
							<input type="text" class="form-control" id="system_seq" name="system_seq">
							<div class="form-control-focus"></div>
						</td>
						<th style="width: 25%; text-align: center;">
							<label class="control-label">시스템명<span class="required">*</span></label>
						</th>
						<td style="width: 25%; vertical-align: middle;" class="form-group form-md-line-input">
							<input type="text" class="form-control" name="system_name">
							<div class="form-control-focus"></div>
						</td>
					</tr>
					<tr>
						<th style="text-align: center;">
							<label class="control-label">시스템 타입<span class="required">*</label>
						</th>
						<td style="vertical-align: middle;" class="form-group form-md-line-input">
							<select class="form-control" id="system_type" name="system_type">
								<option>BIZ_LOG</option>
								<option>DBAC_LOG</option>	
								<option>BIZ_LOG_ONR</option>
							</select>
						</td>
						<th style="text-align: center;">
							<label class="control-label">정렬순서<span class="required">*</label>
						</th>
						<td style="vertical-align: middle;" class="form-group form-md-line-input">
							<input type="text" class="form-control" id="description" name="description" />
							<div class="form-control-focus"></div>
						</td>
					</tr>
					<tr>
						<th style="text-align: center;">
							<label class="control-label">메인 URL</label>
						</th>
						<td style="vertical-align: middle;" class="form-group form-md-line-input">
							<input type="text" class="form-control" id="main_url" name="main_url">
							<div class="form-control-focus"></div>
						</td>
						<th style="text-align: center;">
							<label class="control-label">관리자</label>
						</th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<input type="text" class="form-control" id="administrator" name="administrator">
							<div class="form-control-focus"></div>
						</td>
					</tr>
				</tbody>
			</table>
			
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<th style="width: 25%; text-align: center;">타입</th>
						<th style="width: 25%; text-align: center">개인정보유형</th>
						<th style="width: 25%; text-align: center">사용여부</th>
						<th style="width: 25%; text-align: center">개인정보마스킹</th>
					</tr>
					<c:forEach items="${list }" var="item" varStatus="status">
						<input type="hidden" name="privacy_seq_${status.count }" value="${item.privacy_seq }" />
					<tr>
						<td style="vertical-align: middle; text-align: center;">${item.privacy_seq }</td>
						<td style="vertical-align: middle; text-align: center;">${item.privacy_desc }</td>
						<td style="vertical-align: middle; text-align: center; padding-left: 50px;">
							<div class="md-radio-inline">
								<div class="md-radio col-md-4">
									<input type="radio" id="radio_1_${status.count }" name="radio_${item.privacy_seq }" value="Y" class="md-radiobtn" checked/>
									<label for="radio_1_${status.count }"> <span></span> <span class="check"></span> <span class="box"></span> 사용 
									</label>
								</div>
								<div class="md-radio col-md-4">
									<input type="radio" id="radio_2_${status.count }" name="radio_${item.privacy_seq }" value="N" class="md-radiobtn" />
									<label for="radio_2_${status.count }"> <span></span> <span class="check"></span> <span class="box"></span> 미사용
									</label>
								</div>
							</div>
						</td>
						<td style="vertical-align: middle; text-align: center; padding-left: 50px;">
							<div class="md-radio-inline">
								<div class="md-radio col-md-4">
									<input type="radio" id="radio2_1_${status.count }" name="radio2_${item.privacy_seq }" value="Y" class="md-radiobtn" />
									<label for="radio2_1_${status.count }"> <span></span> <span class="check"></span> <span class="box"></span> 사용
									</label>
								</div>
								<div class="md-radio col-md-4">
									<input type="radio" id="radio2_2_${status.count }" name="radio2_${item.privacy_seq }" value="N" class="md-radiobtn" checked/>
									<label for="radio2_2_${status.count }"> <span></span> <span class="check"></span> <span class="box"></span> 미사용
									</label>
								</div>
							</div>
						</td>
					</tr>
					</c:forEach>
				</tbody>
			</table>
			
			<div class="form-actions">
			<br>
				<div class="row">
					<div class="col-md-offset-2 col-md-10" align="right"
						style="padding-right: 30px;">
						<button type="button"
							class="btn btn-sm blue btn-outline sbold uppercase"
							onclick="addPart()">
							<i class="fa fa-check"></i> 등록
						</button>
						<button type="button"
							class="btn btn-sm red-mint btn-outline sbold uppercase"
							onclick="moveSystemList()">
							<i class="fa fa-close"></i> 취소
						</button>
					</div>
				</div>
			</div>
			<!-- END F -->
		</form>

		
		<!-- END FORM-->
	</div>
</div>

<form id="systemListForm" method="POST">
	<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" />
	<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
	<input type="hidden" name="current_menu_id" value="${currentMenuId }" />
	<input type="hidden" name="sel_system_seq" value="${selSystemSeq }" />
</form>

<script type="text/javascript">

	var systemConfig = {
		"listUrl":"${rootPath}/systemMngt/list.html"
		,"addUrl":"${rootPath}/systemMngt/add.html"
		,"loginPage" : "${rootPath}/loginView.html"
	};
	
</script>