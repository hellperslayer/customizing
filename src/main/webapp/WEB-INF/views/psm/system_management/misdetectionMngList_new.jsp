<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<c:set var="currentMenuId" value="${index_id}" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script
	src="${rootPath}/resources/js/psm/system_management/misdetectionMngList.js"
	type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/ezDatepicker.js"
	type="text/javascript" charset="UTF-8"></script>

<style type="text/css">
.input_date {
	background-color: #eef1f5;
	padding: 0.3em 0.5em;
	border: 1px solid #e4e4e4;
	height: 30px;
	font-size: 12px;
	color: #555;
}
</style>

<h1 class="page-title">${currentMenuName}</h1>
<div class="portlet light portlet-fit portlet-datatable bordered">
	<%-- <div class="portlet-title">
		<div class="caption">
			<i class="icon-settings font-dark"></i> <span
				class="caption-subject font-dark sbold uppercase">${currentMenuName } 리스트</span>
		</div>
	</div> --%>
	<div class="portlet-body">
		<div class="table-container">

			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">

				<div class="dataTables_scroll" style="position: relative;">
					<div>
						<div class="col-md-6"
							style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
							<%-- <div class="portlet box grey-salsa">
								<div class="portlet-title" style="background-color: #2B3643;">
									<div class="caption">
										<img src="${rootPath}/resources/image/icon/search.png"> 검색
									</div>
									<div class="tools">
										<a href="javascript:;" class="expand"></a>
									</div>
									<div style="float: right; padding: 12px 10px 8px;"><b>${textSearch }</b></div>
								</div>


								<!-- portlet-body S -->
								<form id="misForm" method="POST" class="portlet-body" style="display:none;">
									<div class="portlet-body">
										<div class="table-container">
											<div id="datatable_ajax_2_wrapper"
												class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
												<div class="row" style="padding-bottom: 5px;">
													<div class="col-md-4">
														<div class="col-md-5" align="center">
															◎ 개인정보 유형:
														</div>
														<div class="col-md-7" align="left"">
															<select name="privacy_type" id="privacy_type"
																style="float: none;"
																class="ticket-assign form-control input-small">
																<option value="">전체선택</option>
																<c:forEach items="${privList}" var="list"
																	varStatus="status">
																	<option value="${list.privacy_type}"
																		<c:if test="${list.privacy_type eq paramBean.privacy_type}">selected="selected"</c:if>>${list.privacy_desc}</option>
																</c:forEach>
															</select>
														</div>
													</div>

													<div class="col-md-4">
														<div class="col-md-5" align="center">
															◎ 예외 개인정보:
														</div>
														<div class="col-md-7" align="left">
															<input type="text" class="form-control input-small"
																name="misdetect_pattern"
																value="${paramBean.misdetect_pattern}" />
															<div class="form-control-focus"></div>
														</div>
													</div>
												</div>

												<div class="row">
													<div class="col-md-12" align="right"
														style="padding-right: 20px;">
														<button type="reset"
															class="btn btn-sm red table-group-action-submit"
															onclick="resetOptions(misListConfig['listUrl'])">
															<i class="fa fa-remove"></i> 취소
														</button>
														<button class="btn btn-sm green table-group-action-submit"
															onclick="moveList()">
															<i class="fa fa-check"></i> 검색
														</button>
													</div>
												</div>
											</div>
											<!-- portlet-boyd E -->
										</div>
									</div>
									<input type="hidden" name="use_flag" value="" /> <input
										type="hidden" name="misdetect_id" value="" /> <input
										type="hidden" name="main_menu_id"
										value="${paramBean.main_menu_id }" /> <input type="hidden"
										name="sub_menu_id" value="${paramBean.sub_menu_id }" /> <input
										type="hidden" name="current_menu_id" value="${currentMenuId}" />
									<input type="hidden" name="page_num"
										value="${paramBean.page_cur_num}" />
								</form>
								<!-- portlet-boyd E -->

							</div> --%>
							
							<div class="portlet box grey-salt  ">
		                         <div class="portlet-title" style="background-color: #2B3643;">
		                             <div class="caption">
		                                 <i class="fa fa-search"></i>검색 </div>
		                                 <div class="tools">
							                 <a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
							             </div>
		                         </div>
		                         <div id="searchBar" class="portlet-body form" >
		                             <div class="form-body" style="padding-left:20px; padding-right:10px;">
		                                 <div class="form-group">
		                                     <form id="misForm" method="POST" class="mt-repeater form-horizontal">
		                                         <div data-repeater-list="group-a">
		                                             <div class="row">
		                                                 <!-- jQuery Repeater Container -->
		                                                 <div class="col-md-2">
		                                                     <label class="control-label">개인정보유형</label>
		                                                     <select name="privacy_type" id="privacy_type" style="float: none;"	class="form-control">
																<option value="">전체선택</option>
																<c:forEach items="${privList}" var="list"
																	varStatus="status">
																	<option value="${list.privacy_type}"
																		<c:if test="${list.privacy_type eq paramBean.privacy_type}">selected="selected"</c:if>>${list.privacy_desc}</option>
																</c:forEach>
															</select> 
														</div>
														 <div class="col-md-2">
		                                                     <label class="control-label">예외 개인정보</label>
		                                                     <input type="text" class="form-control" name="misdetect_pattern" value="${paramBean.misdetect_pattern}" />
														</div>
		                                             </div>
		                                         </div>
		                                         <hr/>
		                                         <div align="right">
		                                          	<button type="reset" class="btn btn-sm red-mint btn-outline sbold uppercase" onclick="resetOptions(misListConfig['listUrl'])">
														<i class="fa fa-remove"></i> 
														<font>초기화
													</button>
													<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
														onclick="moveMisdetectionList()">
														<i class="fa fa-search"></i> 검색
													</button>
												</div>
										
												<input type="hidden" name="use_flag" value="" /> 
												<input type="hidden" name="misdetect_id" value="" /> 
												<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" /> 
												<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" /> 
												<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
												<input type="hidden" name="page_num" value="${paramBean.page_cur_num}" />
												<input type="hidden" name="isSearch" value="${paramBean.isSearch }"/>
		                                     </form>
		                                 </div>
		                             </div>
		                         </div>
		                     </div>
						</div>
					</div>


					<div class="modal fade" id="myModal" role="dialog">
							<div class="modal-dialog" style="position: relative ;top: 30%; left: 0;">
					
								<!-- Modal content -->
								<div class="modal-content">
									<div class="modal-header" style="background-color: #32c5d2;">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">
											<b style="font-size: 13pt;">엑셀 업로드</b>
										</h4>
									</div>
									<div class="modal-body" style="background-color: #F9FFFF;">
										<form action="upload.html" method="post" id="fileForm"
											name="fileForm" enctype="multipart/form-data">
											<div>
												<table width="100%">
													<tr style="height: 40px">
														<td width="70%" style="vertical-align: middle;" align="center">
															<input class="fileUpLoad" type="file" id="file" name="file">
														</td>
														<td><img style="cursor: pointer;" name="submup"
															onclick="excelmisDetection()"
															src="${rootPath}/resources/image/common/btn_exupload.gif"
															alt="엑셀업로드" title="엑셀업로드" /></td>
													</tr>
													<tr style="height: 40px">
														<td width="50%" style="vertical-align: middle;"><span
															class="downexam">&nbsp;&nbsp;&nbsp;※다운받은 양식으로 
															</br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 업로드하셔야 예외처리DB정보가 등록됩니다.</span></td>
														<td><img class="btn_excel exceldown" onclick="exDown()" style="cursor: pointer;"
															src="${rootPath}/resources/image/common/formUp.png"
															alt="양식다운로드" title="양식다운로드" /></td>
													</tr>
												</table>
											</div>
											<input type="hidden" id="result" name="result" value="false" />
										</form>
									</div>
									<div class="modal-footer">
										<a class="btn btn-sm red" href="#" data-dismiss="modal"> <i
											class="fa fa-remove"></i> 닫기
										</a>
									</div>
								</div>
							</div>
						</div>
					
					<form id="addForm" method="POST">
						<!-- 개인정보 예외 정책 등록 -->
						<div class="caption">
							<i class="icon-settings font-dark"></i> <span
								class="caption-subject font-dark sbold uppercase">예외 개인정보</span>
						</div>
						<div>
							<table style="border-top: 1px solid #e7ecf1"
								class="table table-bordered"
								id="datatable_ajax_2" aria-describedby="datatable_ajax_2_info"
								role="grid"
								style="position: absolute; top: 0px; left: 0px; width: 100%;">
								<thead>
									<th class="33%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">
										개인정보 유형
									</th>
									<th width="33%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">
										예외 개인정보</th>
									<th width="33%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">
										추가</th>
								</thead>
								<tbody>
									<tr>
										<td align="center">
											<select name="privacy_type"	id="privacy_type" class="form-control input-medium">
												<option value="">유형선택</option>
												<c:forEach items="${privList}" var="list" varStatus="status">
													<option value="${list.privacy_type}"
														<c:if test="${list.privacy_type eq paramBean.privacy_type}">selected="selected"</c:if>>${list.privacy_desc}</option>
												</c:forEach>
											</select>
										</td>
										<td align="center"><input type="text"
											name="misdetect_pattern" id="misdetect_pattern"
											class="form-control input-medium" /></td>
										<td align="center">
											<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
												onclick="javascript:addPrivType()">
												<i class="fa fa-plus"></i> 추가 
											</button>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						
						

						<!-- 개인정보 예외 정책 관리 -->
						<div class="caption" style="padding-bottom: 15px;">
							<i class="icon-settings font-dark"></i> <span
								class="caption-subject font-dark sbold uppercase">예외 개인정보 관리</span>
							<%-- <a onclick="excelMisdetectList('${search.total_count}')"><img align="right" src="${rootPath}/resources/image/icon/XLS_3.png"></a> --%>
							<div class="btn-group">
								<a href="javascript:;" data-toggle="dropdown">
									<img align="right" src="${rootPath}/resources/image/icon/XLS_3.png">
								</a>
								<ul class="dropdown-menu pull-right">
									<li>
										<a data-toggle="modal" data-target="#myModal">업로드</a> <!--<a onclick="excelUpLoadList()">업로드</a> -->
									</li>
									<li><a onclick="excelMisdetectList('${search.total_count}')"> 다운로드 </a></li>
								</ul>
							</div>
						</div>
						<!-- <div class="dataTables_scrollBody" style="position: relative; overflow: auto; height: 400px; width: 100%;"> -->
							<table style="border-top: 1px solid #e7ecf1"
								class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
								id="datatable_ajax_2" aria-describedby="datatable_ajax_2_info"
								role="grid"
								style="position: absolute; top: 0px; left: 0px; width: 100%;">

								<thead>
									<tr role="row" class="heading"
										style="background-color: #c0bebe;">
										<th width="10%"
											style="border-bottom: 1px solid #e7ecf1; text-align: center;">
											No.</th>
										<th width="30%"
											style="border-bottom: 1px solid #e7ecf1; text-align: center;">
											개인정보 유형</th>
										<th width="30%"
											style="border-bottom: 1px solid #e7ecf1; text-align: center;">
											예외 개인정보</th>
										<th width="25%"
											style="border-bottom: 1px solid #e7ecf1; text-align: center;">
											삭제</th>
									</tr>
								</thead>
								<tbody>
									<c:choose>
										<c:when test="${empty misdetectList.mis}">
											<tr>
												<td colspan="4" align="center">데이터가 없습니다.</td>
											</tr>
										</c:when>
										<c:otherwise>
											<c:forEach items="${misdetectList.mis}" var="list"
												varStatus="status">
												<tr>
													<td><c:out value="${list.misdetect_id}"/></td>
													<td style="padding-left: 20px;"><c:out value="${list.privacy_desc}"/></td>
													<td style="padding-left: 20px;"><c:out value="${list.misdetect_pattern}"/></td>
													<td style="text-align: center">
														<p class="btn btn-xs red btn-outline filter-cancel"
															onclick="javascript:deletePrivacy(${list.misdetect_id})">삭제</p>
													</td>
												</tr>
												<c:set var="count" value="${count - 1 }" />
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>

						</div>
					</form>

					<div class="dataTables_processing DTS_Loading"
						style="display: none;">Please wait ...</div>
				</div>
				<div class="row" style="padding: 10px;">
					<!-- <div class="col-md-8 col-sm-12">
			            <div class="dataTables_info">Showing 22 to 30 of 178 entries
			            </div>
		            </div>
		            <div class="col-md-4 col-sm-12"></div> -->
					<!-- 페이징 영역 -->
					<c:if test="${misdetectList.page_total_count > 0}">
						<div align="center" id="pagingframe">
							<p>
								<ctl:paginator currentPage="${paramBean.page_cur_num}"
									rowBlockCount="${paramBean.page_size}"
									totalRowCount="${misdetectList.page_total_count}" />
							</p>
						</div>
					</c:if>
				</div>
			</div>

		</div>
	</div>
</div>

<form action="exdownload.html" method="post" id="exdown" name="exdown"
enctype="multipart/form-data"></form>
<script type="text/javascript">
	var misListConfig = {
		"listUrl":"${rootPath}/misdetect/list.html",
		"removeUrl":"${rootPath}/misdetect/remove.html",
		"addUrl":"${rootPath}/misdetect/add.html",
		"addAllUrl":"${rootPath}/misdetect/addAll.html",
		"downloadUrl":"${rootPath}/misdetect/download.html"
	};	
</script>