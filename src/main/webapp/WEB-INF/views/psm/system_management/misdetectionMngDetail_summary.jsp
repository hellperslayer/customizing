<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<%-- <c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" /> --%>

<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/system_management/misdetectSummary.js" type="text/javascript" charset="UTF-8"></script>

<h1 class="page-title">
	${currentMenuName}
	<!--bold font-blue-hoki <small>basic bootstrap tables with various options and styles</small> -->
</h1>
<div class="portlet light portlet-fit bordered">
	<div class="portlet-body">
		<!-- BEGIN FORM-->
		<form id="misdetectDetailForm" action="" method="POST"
			class="form-horizontal form-bordered form-row-stripped">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<th style="width: 15%; text-align: center;"><label class="control-label">개인정보유형 </label></th>
						<td style="width: 35%; vertical-align: middle;" class="form-group form-md-line-input">
						<!-- <input class="form-control" name="privacy_seq" id="privacy_seq" type="text" value="" /> -->
						<select id="privacy_type" name="privacy_type" class="form-control">
						<option value="" ${search.privacyType == '' ? 'selected="selected"' : ''}> ------ 선 택 ------</option>
							<c:if test="${empty CACHE_RESULT_TYPE}">
								<option>데이터 없음</option>
							</c:if>
							<c:if test="${!empty CACHE_RESULT_TYPE}">
								<c:forEach items="${CACHE_RESULT_TYPE}" var="i" varStatus="z">
									<option value="${i.key}" ${i.key==misdetectDetail.privacy_type ? "selected=selected" : "" }>${ i.value}</option>
								</c:forEach>
							</c:if>
						</select>
						<div class="form-control-focus"></div>
						</td>
						<th style="width: 15%; text-align: center;"><label class="control-label">타이틀명 </label></th>
						<td style="width: 35%;" class="form-group form-md-line-input">
							<input type="text" name="result_title" class="form-control" id="result_title"
							style="text-align: center; vertical-align: middle;"
							value="${misdetectDetail.result_title }" />
							<div class="form-control-focus"></div>
						</td>
					</tr>
					<tr>
						<th style="width: 15%; text-align: center;"><label class="control-label">제외유형</label></th>
						<td style="width: 35%; vertical-align: middle;" class="form-group form-md-line-input">
						<select id="misdetect_type" name="misdetect_type" class="form-control" onchange="misdetectSelect()">
							<option value="" > ------ 선 택 ------</option>
							<option value="1" ${misdetectDetail.misdetect_type == '1' ? 'selected="selected"' : ''}>범위</option>
							<option value="2" ${misdetectDetail.misdetect_type == '2' ? 'selected="selected"' : ''}>문자포함</option>
							<option value="3" ${misdetectDetail.misdetect_type == '3' ? 'selected="selected"' : ''}>정규식</option>
						</select>
						<div class="form-control-focus"></div>
						</td>
					</tr>
					<tr>
						<th style="text-align: center; vertical-align: middle;">
							<label class="control-label">제외문자</label>
						</th>
						<td style="vertical-align: middle;" class="form-group form-md-line-input" colspan="3">
							<div id="result_content_text">
								<textarea id="result_content" name="result_content" class="form-control" rows="7">${misdetectDetail.result_content}</textarea>
								<div class="form-control-focus"></div>
							</div>
							<div id="result_content_range">
								<input type="text" id="range_to" name="range_to" class="form-control" style="display: inline; width: 15%" value="${misdetectDetail.range_to }">
								<span>~</span>
								<input type="text" id="range_from" name="range_from" class="form-control" style="display: inline; width: 15%" value="${misdetectDetail.range_from }">
							</div>
						</td>
					</tr>
					
					<tr>
						<th style="text-align: center;"><label class="control-label">사용여부
						</label></th>
						<td class="form-group form-md-line-input"
							style="vertical-align: middle;">
							<div class="md-radio-inline">
								<div class="md-radio">
									<input type="radio" id="checkbox1_1" name="use_flag" value="Y"
										class="md-radiobtn"
										${fn:containsIgnoreCase(misdetectDetail.use_yn,'Y')?'checked':'' }>
									<label for="checkbox1_1"> <span></span> <span
										class="check"></span> <span class="box"></span> 사용
									</label>
								</div>
								<div class="md-radio">
									<input type="radio" id="checkbox1_2" name="use_flag" value="N"
										class="md-radiobtn"
										${!fn:containsIgnoreCase(misdetectDetail.use_yn,'Y')?'checked':'' }>
									<label for="checkbox1_2"> <span></span> <span
										class="check"></span> <span class="box"></span> 미사용
									</label>
								</div>
							</div>
						</td>
						<th style="text-align: center;"><label class="control-label">대상시스템
						</label></th>
						<td class="form-group form-md-line-input"
							style="vertical-align: middle;">
							<div class="md-radio-inline">
								<select name="system_seq" id="system_seq" style="float: none;"	class="form-control">
									<option value="all">전체시스템적용</option>
									<c:forEach var="system" items="${systemList}" varStatus="count">
										<option value="${system.system_seq}" ${system.system_seq eq misdetectDetail.system_seq ? "selected='selected'":"" }>${system.system_name}</option>
									</c:forEach>
								</select> 
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			



			<!-- 메뉴 관련 input 시작 -->
			<input type="hidden" name="main_menu_id"
				value="${paramBean.main_menu_id }" /> <input type="hidden"
				name="sub_menu_id" value="${paramBean.sub_menu_id }" /> <input
				type="hidden" name="current_menu_id" value="${currentMenuId }" /> <input
				type="hidden" name="system_seq" value="${search.system_seq }" />


			<!-- 페이지 번호 -->
			<input type="hidden" name="page_num" value="${paramBean.page_cur_num}" />
			<input type="hidden" name="log_seq" id="log_seq" value="${misdetectDetail.log_seq}">
			<!-- 검색조건 관련 input 끝 -->
		</form>
		<!-- END FORM-->

		<!-- 버튼 영역 -->
		<div class="form-actions">
			<div class="row">
				<div class="col-md-offset-2 col-md-10" align="right"
					style="padding-right: 30px;">
					<button type="button" class="btn btn-sm grey-mint btn-outline sbold uppercase"
						onclick="moveList()">
						<i class="fa fa-list"></i> &nbsp;목록
					</button>

					<c:choose>
						<c:when test="${empty misdetectDetail}">
							<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="addMisdetectSummry()"><i
								class="fa fa-check"></i>&nbsp;추가</a>
						</c:when>
						<c:otherwise>
							<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="addMisdetectSummry()"><i
								class="fa fa-check"></i>&nbsp;저장</a>
							<!-- <a class="btn gray" onclick="removemisdetect()" >삭제</a> -->
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
	<form id="listForm" method="POST">
		<input type="hidden" name="system_seq" id="system_seq" value="${paramBean.system_seq }">
		<input type="hidden" name="page_num" id="page_num" value="${paramBean.page_cur_num}">
	</form>
</div>
<script type="text/javascript">
	var misdetectConfig = {
		"listUrl" : "${rootPath}/misdetect/list_summary.html",
		"addUrl" : "${rootPath}/misdetect/addMisdetectSummry.html",
		"saveUrl" : "${rootPath}/misdetect/save.html",
		"removeUrl" : "${rootPath}/misdetect/remove.html",
		"loginPage" : "${rootPath}/loginView.html"
	};
</script>