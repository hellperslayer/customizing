
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<c:set var="currentMenuId" value="${index_id}" />
<c:set var="selSystemSeq" value="${sel_system_seq}" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />
<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<script type="text/javascript" charset="UTF-8">
	var auth = '${auth}';
</script>
<script src="${rootPath}/resources/js/psm/system_management/optionSetting.js" type="text/javascript" charset="UTF-8"></script>

<h1 class="page-title">${currentMenuName}</h1>

<form id="reloadForm" method="POST">
	<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" />
	<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
	<input type="hidden" name="current_menu_id" value="${currentMenuId }" />
</form>

<div class="row">
	<div class="col-md-10">
		<form id="optionForm" method="POST"> 
		<table class="table table-striped table-bordered order-column">
			
			<c:forEach items="${list }" var="item" varStatus="status">
			<c:set var="flag_name" value="${fn:split(item.flag_name, ',') }"/>
			<tr>
				<c:choose>
					<c:when test="${list[status.index -1].option_type==item.option_type }">
					</c:when>
					<c:otherwise>
						<td rowspan="${option_type_ct[item.option_type] }" width="20%" style="text-align: center; vertical-align: middle;">${item.option_type }</td>
					</c:otherwise>
				</c:choose>
				
				<td width="20%" style="text-align: center; vertical-align: middle;">${item.option_name }</td>
				<td class="form-group form-md-line-input" style="text-align: center; vertical-align: middle;">
					<c:choose>
					<c:when test="${fn:length(flag_name) == 2 }">
					<div class="md-radio-inline">
						<div class="md-radio col-md-4">
							<input type="radio" id="checkbox${status.count }_1" name="${item.option_id }" value="Y"
								class="md-radiobtn" <c:if test="${item.value eq 'Y' }">checked="checked"</c:if>/>
							<label for="checkbox${status.count }_1"> <span></span> <span
								class="check"></span> <span class="box"></span> ${flag_name[0] }
							</label>
						</div>
						<div class="md-radio col-md-4">
							<input type="radio" id="checkbox${status.count }_2" name="${item.option_id }" value="N"
								class="md-radiobtn" <c:if test="${item.value eq 'N' }">checked="checked"</c:if>/>
							<label for="checkbox${status.count }_2"> <span></span> <span
								class="check"></span> <span class="box"></span> ${flag_name[1] }
							</label>
						</div>
						<c:if test="${item.option_id eq 'use_bmt' }">
						<div>
							<label>name :</label><input type="text" id="bmt_name" name="bmt_name" value="${bmt_name }" />
						</div>
						</c:if>
						<%-- <c:if test="${item.option_id eq 'use_dashboard_scenario' }">
							<select class="form-control input-small" style="text-align: center;" name="select_scenario">
							<c:forEach items="${scenarioList }" var="i">
								<option value="${i.scen_seq}"
									<c:if test='${i.scen_seq eq dashboard_scenario.code_id }'>selected="selected"</c:if>>${i.scen_name }
								</option>
							</c:forEach>
							</select>
						</c:if> --%>
					</div>
					</c:when>
					<c:when test="${fn:length(flag_name) == 3 }">
					<div class="md-radio-inline">
						<div class="md-radio col-md-3">
							<input type="radio" id="checkbox${status.count }_1" name="${item.option_id }" value="1"
								class="md-radiobtn" <c:if test="${item.value eq '1' }">checked="checked"</c:if>/>
							<label for="checkbox${status.count }_1"> <span></span> <span
								class="check"></span> <span class="box"></span> ${flag_name[0] }
							</label>
						</div>
						<div class="md-radio col-md-3">
							<input type="radio" id="checkbox${status.count }_2" name="${item.option_id }" value="2"
								class="md-radiobtn" <c:if test="${item.value eq '2' }">checked="checked"</c:if>/>
							<label for="checkbox${status.count }_2"> <span></span> <span
								class="check"></span> <span class="box"></span> ${flag_name[1] }
							</label>
						</div>
						<div class="md-radio col-md-3">
							<input type="radio" id="checkbox${status.count }_3" name="${item.option_id }" value="3"
								class="md-radiobtn" <c:if test="${item.value eq '3' }">checked="checked"</c:if>/>
							<label for="checkbox${status.count }_3"> <span></span> <span
								class="check"></span> <span class="box"></span> ${flag_name[2] }
							</label>
						</div>
					</div>
					</c:when>
					<c:when test="${item.flag_name eq 'PSM' }">
						<div class="md-radio col-md-4">
							<select class="form-control" style="text-align: center;" name="${item.option_id }" value="${item.value }">
							<c:forEach items="${ui_type }" var="i" varStatus="z">
								<option value="${i.code_id}"
									<c:if test='${i.code_id eq item.value }'>selected="selected"</c:if>>${i.code_name }
								</option>
							</c:forEach>
							</select>
						</div>
					</c:when>
					<c:when test="${item.flag_name eq 'menu' }">
						<div class="md-radio col-md-8">
							<select class="form-control" style="text-align: center;" name="${item.option_id }" value="${item.value }">
							<c:forEach items="${init_login_page }" var="i" varStatus="z">
								<option value="${i.menu_url}"
									<c:if test='${i.menu_url eq item.value }'>selected="selected"</c:if>>${i.menu_url }
								</option>
							</c:forEach>
							</select>
						</div>
					</c:when>
					<c:otherwise>
						<div class="md-radio col-md-4">
						<input class="form-control input-xsmall" style="display: inline;" type="text" name="${item.option_id }" value="${item.value }" />${flag_name[0] }
						</div>
					</c:otherwise>
					</c:choose>
				</td>
				<c:if test="${auth eq 'AUTH00000' }">
				<td> 
				<select class="form-control" style="text-align: center;" id="auth" name="admin_auth_${item.option_id }" >
					<option value ="AUTH00000" <c:if test='${item.auth eq "AUTH00000"}'>selected="selected"</c:if>>${authMap["AUTH00000"].auth_name }</option> 
					<option value ="AUTH00001" <c:if test='${item.auth eq "AUTH00001"}'>selected="selected"</c:if>>${authMap["AUTH00001"].auth_name }</option> 
					<option value ="AUTH00003" <c:if test='${item.auth eq "AUTH00003"}'>selected="selected"</c:if>>${authMap["AUTH00003"].auth_name }</option> 
				</select>
				</td>
				</c:if>
				
			</tr>
			</c:forEach>
		</table>
		</form>
	</div>
	
	<div class="col-md-8" style="text-align: right">
		<a style="text-align: right" onclick="setOptionSetting()" class="btn btn-sm blue btn-outline sbold uppercase"><i class="fa fa-check"></i> 수정</a>
	</div>
</div>

