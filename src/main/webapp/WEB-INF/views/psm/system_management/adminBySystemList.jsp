<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/system_management/adminBySystemList.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>

<!-- BEGIN PAGE TITLE-->
<h1 class="page-title">${currentMenuName}</h1>
<!-- END PAGE TITLE-->

<!-- END PAGE HEADER-->
<div class="portlet light portlet-fit portlet-datatable bordered">
	<div class="portlet-body">
		<div class="table-container">
			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">

				<div class="col-md-6"
					style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
					
						<div class="portlet box grey-salt  ">
                          <div class="portlet-title" style="background-color: #2B3643;">
                              <div class="caption">
                                  <i class="fa fa-search"></i>검색 & 엑셀 </div>
                              <div class="tools">
				                 <a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
				             </div>
                          </div>
                          <div id="searchBar" class="portlet-body form" >
                              <div class="form-body" style="padding-left:10px; padding-right:10px;">
                                  <div class="form-group">
                                      <form id="adminUserListForm" method="POST" class="mt-repeater form-horizontal">
                                          <div data-repeater-list="group-a">
                                              <div data-repeater-item class="mt-repeater-item">
                                                  <!-- jQuery Repeater Container -->
                                                  <div class="mt-repeater-input">
                                                      <label class="control-label">관리자ID</label>
                                                      <br/>
                                                      <input type="text"
														class="form-control input-medium"
														name="admin_user_id_1" value="${search.admin_user_id_1 }"> </div>
                                                  <div class="mt-repeater-input">
                                                      <label class="control-label">관리자명</label>
                                                      <br/>
                                                      <input type="text"
														class="form-control input-medium"
														name="admin_user_name" value="${search.admin_user_name }"></div>
                                                  <div class="mt-repeater-input">
                                                      <label class="control-label">시스템</label>
                                                      <br/>
                                                      <select name="system_seq" id="system_seq"
														class="form-control input-medium"
														style="width: 100%; margin-left: 0px; ">
														<option value=""
															${search.system_seq == '' ? 'selected="selected"' : ''}>
															----- 전 체 -----</option>
														<c:if test="${empty sysList}">
															<option>시스템 데이터 없습니다.</option>
														</c:if>
														<c:if test="${!empty sysList}">
															<c:forEach items="${sysList}" var="i" varStatus="z">
																<option value="${i.system_seq}"
																	${i.system_seq==search.system_seq ? "selected=selected" : "" }>${ i.system_name}</option>
															</c:forEach>
														</c:if>
													</select>
                                                  </div>
                                              </div>
                                          </div>
                                          <div align="right">
                                           <button type="reset"
											class="btn btn-sm red-mint btn-outline sbold uppercase"
											onclick="resetOptions(adminBySystemListConfig['listUrl'])">
											<i class="fa fa-remove"></i> <font>초기화
										</button>
										<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
											onclick="moveAdminUserList()">
											<i class="fa fa-search"></i> 검색
										</button>&nbsp;&nbsp;
										<a onclick="excelAdminUserList()"><img src="${rootPath}/resources/image/icon/XLS_3.png"></a>										
									</div>
									
									<input type="hidden" name="main_menu_id"
										value="${paramBean.main_menu_id }" /> <input type="hidden"
										name="sub_menu_id" value="${paramBean.sub_menu_id }" /> <input
										type="hidden" name="current_menu_id" value="${currentMenuId}" />
									<input type="hidden" name="page_num" value="${search.page_num}" />
									<input type="hidden" name="isSearch" value="${paramBean.isSearch }" />
                                      </form>
                                  </div>
                              </div>
                          </div>
                      </div>
				</div>

				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-6">
							<div class="btn-group">
								<button id="menuMngt_new" class="btn btn-sm blue btn-outline sbold uppercase"
									onclick="findAdminUserDetail(null)">
									<i class="fa fa-plus"></i> 신규 
								</button>
							</div>
						</div>
					</div>
				</div>

				<table style="border-top: 1px solid #e7ecf1"
					class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
					style="position: absolute; top: 0px; left: 0px; width: 100%;">

					<thead>
						<tr role="row" class="heading" style="background-color: #c0bebe;">
							<th width="5%"
								style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">No.</th>
							<th width="15%"
								style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center; padding: 0em;">
								시스템</th>
							<th width="10%"
								style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
								관리자ID</th>
							<th width="10%"
								style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
								관리자명</th>
							<th width="25%"
								style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
								연락처</th>
							<th width="25%"
								style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
								E-MAIL</th>
							<th width="10%"
								style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
								권한명</th>
							<!-- <th width="16%"
								style="border-bottom: 1px solid #e7ecf1; vertical-align: middle; text-align: center;">
								알람여부</th> -->
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${empty adminUserList}">
								<tr>
									<td colspan="7" align="center">데이터가 없습니다.</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:set value="${search.total_count}" var="count" />
								<c:forEach items="${adminUserList}" var="adminUser"
									varStatus="status">
									<tr role="row" class="odd" style="text-align: center; cursor: pointer;"
										onclick="findAdminUserDetail('${adminUser.admin_user_id}')">
										<td>${count - search.page_num * search.size + search.size }</td>
										<td><c:out value="${adminUser.system_name }"/></td>
										<td><c:out value="${adminUser.admin_user_id}"/></td>
										<td><c:out value="${adminUser.admin_user_name}"/></td>
										<td>
											<c:choose>
												<c:when test="${empty adminUser.mobile_number}">-</c:when>
												<c:otherwise><c:out value="${adminUser.mobile_number}"/></c:otherwise>
											</c:choose>
										</td>
										<td>
											<c:choose>
												<c:when test="${empty adminUser.email_address}">-</c:when>
												<c:otherwise><c:out value="${adminUser.email_address}"/></c:otherwise>
											</c:choose>
										</td>
										<td><c:out value="${adminUser.auth_name}"/></td>
									</tr>
									<c:set var="count" value="${count - 1 }" />
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
				<div class="row" style="padding: 10px;">
					<!-- 페이징 영역 -->
					<c:if test="${search.total_count > 0}">
						<div id="pagingframe" align="center">
							<p>
								<ctl:paginator currentPage="${search.page_num}"
									rowBlockCount="${search.size}"
									totalRowCount="${search.total_count}" />
							</p>
						</div>
					</c:if>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var adminBySystemListConfig = {
		"listUrl" : "${rootPath}/adminBySystem/list.html",
		"detailUrl" : "${rootPath}/adminBySystem/detail.html",
		"downloadUrl" : "${rootPath}/adminBySystem/download.html"
	};
</script>
