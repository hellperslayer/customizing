<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="menuAddFlag" value="T" />
<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="currentMenuId" value="MENU00012" />
<c:set var="selMenuId" value="${sel_menu_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />


<link href="${rootPath}/resources/css/common/jquery-ui.dynatree.css" rel="stylesheet" type="text/css" />
<script src="${rootPath}/resources/js/common/jquery-ui-1.9.0.custom.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/jquery.dynatree.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/jquery.cookie.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/system_management/menuMngtList.js" type="text/javascript" charset="UTF-8"></script>


<%-- <link rel="stylesheet" href="${rootPath}/resources/assets/global/plugins/jstree/dist/themes/default/style.min.css" />
<script src="${rootPath}/resources/js/common/jquery-ui-1.9.0.custom.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/system_management/menuMngtList.js" type="text/javascript" charset="UTF-8"></script> --%>

<h1 class="page-title">${currentMenuName}</h1>
<!-- END PAGE TITLE-->
<!-- BEGIN PAGE CONTENT-->
<div class="portlet">
	<div class="row">
		<div class="col-md-4">
			<div class="btn-group">
				<c:if test="${adminUserAuthId == 'AUTH00000'}">
					<button id="menuMngt_new" class="btn btn-sm blue btn-outline sbold uppercase"
						onclick="showMenuDetail(null)">
						<i class="fa fa-plus"></i> 신규 
					</button>
				</c:if>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<!-- 메뉴트리 -->
	<div class="col-md-4">
	
		<div id="menuTree" class="portlet light portlet-fit bordered">
			<ul>
				<c:set var="preDepth" value="0" />
				<c:set var="className" value="folder"/>
				<c:forEach items="${hierarchyMenus}" var="menu">
					<c:choose>
						<c:when test="${menu.menu_depth == 0 }">
						</c:when>
						<c:when test="${preDepth < menu.menu_depth}">
							<ul>
						</c:when>
						<c:when test="${preDepth == menu.menu_depth}">
							</li>
						</c:when>
						<c:when test="${preDepth > menu.menu_depth }">
							<c:forEach begin="1" end="${preDepth - menu.menu_depth}" step="1">
								</li>
							</ul>
							</c:forEach>
						</li>
						</c:when>
					</c:choose>
					<li class="${className }" id="${menu.menu_id}"><a href="${menu.menu_id}">${menu.menu_name}</a>
					<c:set var="preDepth" value="${menu.menu_depth}" /> 
				</c:forEach> 
				<c:forEach begin="0" end="${preDepth}" step="1">
					</ul></li>
				</c:forEach>
			</ul>
		</div>
	
		<%-- <div id="tree_1" class="portlet light portlet-fit bordered">
			<ul>
				<c:set var="preDepth" value="0" />
				<c:set var="className" value="folder"/>
				<c:forEach items="${hierarchyMenus}" var="menu">
					<c:choose>
						<c:when test="${menu.menu_depth == 0 }">
						</c:when>
						<c:when test="${preDepth < menu.menu_depth}">
							<ul>
						</c:when>
						<c:when test="${preDepth == menu.menu_depth}">
							</li>
						</c:when>
						<c:when test="${preDepth > menu.menu_depth }">
							<c:forEach begin="1" end="${preDepth - menu.menu_depth}" step="1">
								</li>
							</ul>
							</c:forEach>
						</li>
						</c:when>
					</c:choose>
					<c:choose>
						<c:when test="${menu.menu_depth == 0 }">
							<li class="${className }" id="${menu.menu_id}" data-jstree='{ "opened" : true, "selected": true }'>
						</c:when>
						<c:otherwise>
							<li class="${className }" id="${menu.menu_id}">
						</c:otherwise>
					</c:choose>
					<a href="#" onclick="findMenuDetail('${menu.menu_id}')"><c:out value="${fn:replace(fn:replace(menu.menu_name, '<br>', ' '), '<br/>', ' ')}"/></a>
					<c:set var="preDepth" value="${menu.menu_depth}" /> 
				</c:forEach> 
				<c:forEach begin="0" end="${preDepth}" step="1">
					</ul></li>
				</c:forEach>
			</ul>
		</div> --%>
		
	    
    
        <%-- <div class="portlet light bordered">
	        <div class="portlet-title">
	            <div class="caption">
	                <i class="icon-social-dribbble font-blue-sharp"></i>
	                <span class="caption-subject font-blue-sharp bold uppercase">Default Tree</span>
	            </div>
	        </div>
            <div class="portlet-body">
                <div id="tree_1" class="tree-demo">
                    <ul>
						<c:set var="preDepth" value="0" />
						<c:forEach items="${hierarchyMenus}" var="menu">
							<c:choose>
								<c:when test="${menu.menu_depth == 0 }">
								</c:when>
								<c:when test="${preDepth < menu.menu_depth}">
									<ul>
								</c:when>
								<c:when test="${preDepth == menu.menu_depth}">
									</li>
								</c:when>
								<c:when test="${preDepth > menu.menu_depth }">
									<c:forEach begin="1" end="${preDepth - menu.menu_depth}" step="1">
										</li>
									</ul>
									</c:forEach>
								</li>
								</c:when>
							</c:choose>
							<li id="${menu.menu_id}">
							<a onclick="findMenuDetail('${menu.menu_id}')"><c:out value="${fn:replace(fn:replace(menu.menu_name, '<br>', ' '), '<br/>', ' ')}"/></a>
							<c:set var="preDepth" value="${menu.menu_depth}" /> 
						</c:forEach> 
						<c:forEach begin="0" end="${preDepth}" step="1">
							</ul></li>
						</c:forEach>
					</ul>
                </div>
            </div>
        </div> --%>
	</div>
	
<!--[if lt IE 9]>
<script src="${rootPath}/resources/assets/global/plugins/respond.min.js"></script>
<script src="${rootPath}/resources/assets/global/plugins/excanvas.min.js"></script> 
<script src="${rootPath}/resources/assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="${rootPath}/resources/assets/global/plugins/jstree/dist/jstree.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="${rootPath}/resources/assets/pages/scripts/ui-tree.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

	<!-- 
		2015.04.20
		슈퍼관리자만 메뉴 등록 가능 (AUTH00000 - 이지서티 전용)
	 -->
	<c:choose>
		<c:when test="${adminUserAuthId == 'AUTH00000' }">
			<div class="right" id="menuDetailDiv" style="margin-top: 0px;">
		</c:when>
		<c:otherwise>
			<div class="right" id="menuDetailDiv">
		</c:otherwise>
	</c:choose>

	<!-- 상세 -->
	<div class="col-md-8">
		<div class="portlet light portlet-fit bordered">
			<!-- <div class="portlet-title">
				<div class="caption">
					<span class="caption-subject font-blue-hoki bold uppercase"
						id="menuIdLabel"></span>
				</div>
			</div> -->
			<div class="portlet-body">
				<!-- BEGIN FORM-->
				<form id="menuDetailForm" method="POST" action="#"
					class="form-horizontal form-bordered form-row-stripped">
					<table class="table table-bordered table-striped">
						<tbody>
							<tr>
								<th style="width: 15%; text-align: center;"><label
									class="control-label">메뉴명<span class="required">*</span>
								</label></th>
								<td class="form-group form-md-line-input" style="width: 35%;">
									<input type="text" class="form-control" name="menu_name">
									<div class="form-control-focus"></div>
								</td>
								<th style="width: 15%; text-align: center;"><label
									class="control-label">부모메뉴명 </label></th>
								<td style="vertical-align: middle;"><span id="rootMenuParentNameSpan"><b>최상위메뉴</b></span> <input
									type="hidden" id="parent_menu_id" value="" /> <select
									class="ticket-assign form-control input-small"
									id="parent_menu_id_select">
										<!-- name="parent_menu_id" -->
										<c:forEach items="${hierarchyMenus }" var="menu">
											<option value="${menu.menu_id }">${menu.simple_menu_name }</option>
										</c:forEach>
								</select></td>
							</tr>
							<tr>
								<th style="text-align: center;"><label
									class="control-label">Link URL </label></th>
								<td class="form-group form-md-line-input"><c:choose>
										<c:when test="${adminUserAuthId == 'AUTH00000' }">
											<input type="text" name="menu_url" class="form-control" />
											<div class="form-control-focus"></div>
										</c:when>
										<c:otherwise>
											<div class="form-control form-control-static">
												<span id="menu_url"></span> <input type="hidden"
													name="menu_url" class="form-control" />
											</div>
										</c:otherwise>
									</c:choose></td>
								<th style="text-align: center;"><label
									class="control-label">사용여부 <span class="required">*</span></label></th>
								<td style="vertical-align: middle;">
									<div class="md-radio-inline"
										style="padding: 0px; margin: 0px; vertical-align: middle;text-align: center;">
										<div class="md-radio">
											<input type="radio" id="checkbox1_1" name="use_flag"
												value="Y" class="md-radiobtn" checked> <label
												for="checkbox1_1"> <span></span> <span class="check"></span>
												<span class="box"></span> 사용
											</label>
										</div>
										<div class="md-radio">
											<input type="radio" id="checkbox1_2" name="use_flag"
												value="N" class="md-radiobtn"> <label
												for="checkbox1_2"> <span></span> <span class="check"></span>
												<span class="box"></span> 미사용
											</label>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<th style="text-align: center;"><label
									class="control-label">정렬순서 </label></th>
								<td class="form-group form-md-line-input"><input
									type="text" name="sort_order" class="form-control"
									onkeydown="check_number()" />
									<div class="form-control-focus"></div></td>
								<th style="text-align: center;"><label
									class="control-label">메뉴설명 </label></th>
								<td class="form-group form-md-line-input"><input
									type="text" name="description" class="form-control" />
									<div class="form-control-focus"></div></td>
							</tr>
							<tr>
								<th style="text-align: center;"><label
									class="control-label">등록자 </label></th>
								<td class="form-group form-md-line-input"
									style="text-align: center; vertical-align: middle;"><span
									id="insertUserIdSpan"></span></td>
								<th style="text-align: center;"><label
									class="control-label">등록일시 </label></th>
								<td class="form-group form-md-line-input"
									style="text-align: center; vertical-align: middle;"><span
									id="insertDatetimeSpan"></span></td>
							</tr>
							<tr>
								<th style="text-align: center;"><label
									class="control-label">수정자</label></th>
								<td class="form-group form-md-line-input"
									style="text-align: center; vertical-align: middle;"><span
									id="updateUserIdSpan"></span></td>
								<th style="text-align: center;"><label
									class="control-label">수정일시 </label></th>
								<td class="form-group form-md-line-input"
									style="text-align: center; vertical-align: middle;"><span
									id="updateDatetimeSpan"></span></td>
							</tr>
						</tbody>
					</table>
					<input type="checkbox" name="sub_batch" id="sub_batch" style="display: none"/>
					<input type="hidden" name="auth_ids" />
					<input type="hidden" name="menu_id" />
				</form>
				<!-- END FORM-->

				<div id="authListDiv" class="dataTables_scrollBody"
					style="position: relative; overflow: auto; height: 300px; width: 100%; padding-top: 30px;">
					<table style="border-top: 1px solid #e7ecf1"
						class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
						id="datatable_ajax_2" aria-describedby="datatable_ajax_2_info"
						role="grid"
						style="position: absolute; top: 0px; left: 0px; width: 100%;">
						<thead>
							<tr role="row" class="heading" style="background-color: #c0bebe;">
								<th width="12%"
									style="border-bottom: 1px solid #e7ecf1; text-align: center; vertical-align: center;">
									<input type="checkbox" class="auth_checkbox"
									id="authMenuAllCheck" onclick="allAuthMenuCheck(this)" />
								</th>
								<!-- <th width="32%"
									style="border-bottom: 1px solid #e7ecf1; text-align: center; vertical-align: center;">
									권한ID</th> -->
								<th width="33%"
									style="border-bottom: 1px solid #e7ecf1; text-align: center;">
									권한명</th>
								<th style="border-bottom: 1px solid #e7ecf1; text-align: center;">설명</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${auths}" var="auth" varStatus="status">
								<tr>
									<td style="text-align: center;"><input type="checkbox"
										class="auth_checkbox" id="${auth.auth_id}" name="auth_ids"
										value="${auth.auth_id}" /></td>
									<%-- <td style="text-align: center;"><c:out
											value="${auth.auth_id }" /></td> --%>
									<td style="text-align: center;"><c:out
											value="${auth.auth_name }" /></td>
									<td style="text-align: center;"><c:out
											value="${auth.comment }" /></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>


				<div class="form-actions" align="right" style="padding-right: 10px;">
				<br>
					<div class="row">
						<!-- <div class="btn-set pull-right"> -->
						<button type="button"
							class="btn btn-sm btn-default btn-outline sbold uppercase"
							id="subBatchBtn" onclick="batchMenu()">
							<i id="checkFont" class="fa fa-square"></i> 일괄적용
						</button>
						<button type="button"
							class="btn btn-sm blue btn-outline sbold uppercase"
							id="addMenuBtn" onclick="addMenu()">
							<i class="fa fa-check"></i> 등록
						</button>
						<button type="button"
							class="btn btn-sm blue btn-outline sbold uppercase"
							id="saveMenuBtn" onclick="saveMenu()">
							<i class="fa fa-check"></i> 수정
						</button>

						<c:if test="${adminUserAuthId == 'AUTH00000' }">
							<button type="button"
								class="btn btn-sm red-mint btn-outline sbold uppercase"
								id="removeMenuBtn" onclick="removeMenu()">
								<i class="fa fa-close"></i> 삭제
							</button>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- END PAGE CONTENT-->

<form id="menuListForm" method="POST">
	<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" />
	<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
	<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
	<input type="hidden" name="sel_menu_id" value="${selMenuId }" />
</form>

<script type="text/javascript">
	var menuConfig = {
		"detailUrl" : "${rootPath}/menuMngt/detail.html",
		"addUrl" : "${rootPath}/menuMngt/add.html",
		"saveUrl" : "${rootPath}/menuMngt/save.html",
		"removeUrl" : "${rootPath}/menuMngt/remove.html",
		"loginPage" : "${rootPath}/loginView.html"
	};
</script>