<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script
	src="${rootPath}/resources/js/psm/system_management/authMngtList.js"
	type="text/javascript" charset="UTF-8"></script>

<h1 class="page-title">${currentMenuName}</h1>
<div class="portlet light portlet-fit portlet-datatable bordered">
	<!-- <div class="portlet-title">
		<div class="caption">
			<i class="icon-settings font-dark"></i> <span
				class="caption-subject font-dark sbold uppercase">권한관리 리스트</span>
		</div>
		<div class="actions">
			<div class="btn-group">
				<a class="btn red btn-outline btn-circle" onclick="excelAuthList()"
					data-toggle="dropdown"> <i class="fa fa-share"></i> <span
					class="hidden-xs"> 엑셀 </span>
				</a>
			</div>
		</div>
	</div> -->
	<div class="portlet-body">
		<div class="table-container">

			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">

				<!--             <div class="dataTables_scroll" style="position: relative;"> -->
				<!-- 				<div class="row"> -->
				<div class="col-md-6"
					style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
					<div class="portlet box grey-salsa">
						<div class="portlet-title" style="background-color: #2B3643;">
							<div class="caption">
								<%-- <img src="${rootPath}/resources/image/icon/search.png"> --%><i class="fa fa-search"></i> 검색 & 엑셀
							</div>
							<div class="tools">
				                 <a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
				             </div>
							<%-- <div style="float: right; padding: 12px 10px 8px;"><b>${textSearch }</b></div> --%>
						</div>


						<!-- portlet-body S -->
						
						<div id="searchBar" class="portlet-body">
						<form id="authForm" method="POST">
							<div class="table-container">
								<div id="datatable_ajax_2_wrapper"
									class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
									<div class="row">
										<div class="col-md-2">
											<label class="control-label">권한명</label></br>
											<div class="input-group" style="padding: 0px;">
												<input class="form-control" name="auth_name" value="${paramBean.auth_name }" />
											</div>
										</div>
									</div>
									<hr/>
									<div align="right">
										<button type="reset"
											class="btn btn-sm red-mint btn-outline sbold uppercase"
											onclick="resetOptions(authListConfig['listUrl'])">
											<i class="fa fa-remove"></i> 초기화
										</button>
										<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
											onclick="searchAuthList()">
											<i class="fa fa-check"></i> 검색
										</button>&nbsp;&nbsp;
										<!-- <a class="btn red btn-outline btn-circle"
											onclick="excelAuthList()"> 
											<i class="fa fa-share"></i> <span class="hidden-xs"> 엑셀 </span>
										</a> -->
										<%-- <a onclick="excelAuthList()"><img src="${rootPath}/resources/image/icon/XLS_3.png"></a> --%>
									</div>
								</div>
								<!-- portlet-boyd E -->
							</div>
								<input type="hidden" name="auth_id" /> <input type="hidden"
								name="main_menu_id" value="${paramBean.main_menu_id }" /> <input
								type="hidden" name="sub_menu_id"
								value="${paramBean.sub_menu_id }" /> <input type="hidden"
								name="current_menu_id" value="${currentMenuId}" /> <input
								type="hidden" name="page_num" />
							<input type="hidden" name="isSearch" value="${isSearch }" />

						</form>
						</div>

							
						<!-- portlet-boyd E -->

					</div>
				</div>
				<!-- 				</div> -->

				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-6">
							<div class="btn-group">
								<button type="button" class="btn btn-sm blue btn-outline sbold uppercase" onclick="moveDetail('')">
									<i class="fa fa-plus"></i> 신규 
								</button>
							</div>
						</div>
					</div>
				</div>

				<!--              <div class="dataTables_scrollBody" style="position: relative; overflow: auto; height: 280px; width: 100%;"> -->
				<table
					class="table table-striped table-bordered table-hover order-column"
					id="datatable_ajax_2" aria-describedby="datatable_ajax_2_info"
					role="grid">

					<thead>
						<tr role="row" class="heading">
							<th width="10%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								No.</th>
							<!-- <th width="30%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								권한ID</th> -->
							<th width="30%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								권한명</th>
							<th width="30%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								사용여부</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${empty authList.auths}">
								<tr>
									<td colspan="4" align="center">데이터가 없습니다.</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:set value="${authList.page_total_count}" var="count" />
								<c:forEach items="${authList.auths}" var="auth"
									varStatus="status">
									<tr onclick="moveDetail('${auth.auth_id}')">
										<td style="text-align: center;">${count - paramBean.page_cur_num * 10 + 10 }</td>
										<%-- <td style="text-align: center;"><a
											onclick="moveDetail('${auth.auth_id}')">${auth.auth_id}</a></td> --%>
										<td style="padding-left: 20px; cursor: pointer;"><c:out value="${auth.auth_name}"/></td>
										<c:choose>
											<c:when
												test="${template_yesNo.options[auth.use_flag] == '사용'}">
												<td style="text-align: center; vertical-align: middle;"><span
													class="label label-sm label-success"> 사용 </span></td>
											</c:when>
											<c:otherwise>
												<td style="text-align: center; vertical-align: middle;"><span
													class="label label-sm label-warning ">미사용 </span></td>
											</c:otherwise>
										</c:choose>
									</tr>
									<c:set var="count" value="${count - 1 }" />
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
				<!-- 페이징 영역 -->
				<c:if test="${authList.page_total_count > 0}">
					<div class="page left" id="pagingframe" align="center">
						<p>
							<ctl:paginator currentPage="${paramBean.page_cur_num}"
								rowBlockCount="${paramBean.page_size}"
								totalRowCount="${authList.page_total_count}" />
						</p>
					</div>
				</c:if>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	
	var authListConfig = {
		"listUrl":"${rootPath}/authMngt/list.html"
		,"detailUrl":"${rootPath}/authMngt/detail.html"
		,"downloadUrl":"${rootPath}/authMngt/download.html"
	};
 	
</script>