<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="userAddFlag" value="F" />
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />
<script src="${rootPath}/resources/js/common/jquery.form.js"
	type="text/javascript" charset="UTF-8"></script>
<script
	src="${rootPath}/resources/js/psm/system_management/talarmMngtList.js"
	type="text/javascript" charset="UTF-8"></script>

<h1 class="page-title">${currentMenuName}</h1>
<div class="portlet light portlet-fit portlet-datatable bordered">
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog" style="position: relative ;top: 30%; left: 0;">

			<!-- Modal content -->
			<div class="modal-content">
				<div class="modal-header" style="background-color: #32c5d2;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">
						<b style="font-size: 13pt;">엑셀 업로드</b>
					</h4>
				</div>
				<div class="modal-body" style="background-color: #F9FFFF;">
					<form action="upload.html" method="post" id="fileForm"
						name="fileForm" enctype="multipart/form-data">
						<div>
							<table width="100%">
								<tr style="height: 40px">
									<td width="70%" style="vertical-align: middle;" align="center">
										<!-- 										<input type="submit" value="upload" />  --> <input
										class="fileUpLoad" type="file" id="file" name="file">
									</td>
									<td><img style="" name="submup"
										onclick="excelTalarmUpLoad()"
										src="${rootPath}/resources/image/common/btn_exupload.gif"
										alt="엑셀업로드" title="엑셀업로드" /></td>
								</tr>
								<tr style="height: 40px">
									<td width="70%" style="vertical-align: middle;"><span
										class="downexam">&nbsp;&nbsp;&nbsp;※다운받은 양식으로 업로드하셔야
											사용자정보가 등록됩니다.</span></td>
									<td><img class="btn_excel exceldown" onclick="exDown()"
										src="${rootPath}/resources/image/common/formUp.png"
										alt="양식다운로드" title="양식다운로드" /></td>
								</tr>
							</table>
						</div>
						<input type="hidden" id="result" name="result" value="false" />
					</form>
				</div>
				<div class="modal-footer">
					<a class="btn btn-sm red" href="#" data-dismiss="modal"> <i
						class="fa fa-remove"></i> 닫기
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-container">

			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">

				<!-- 				<div class="dataTables_scroll" style="position: relative;"> -->
				<!-- 				<div class="row"> -->
				<div class="col-md-6"
					style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
									
					<div class="portlet box grey-salt  ">
                          <div class="portlet-title" style="background-color: #2B3643;">
                              <div class="caption">
                                  <i class="fa fa-search"></i>검색 & 엑셀 </div>
                              <div class="tools">
                                  <a href="" class="collapse"> </a>
                              </div>
                          </div>
                          <div class="portlet-body form" >
                              <div class="form-body" style="padding-left:10px; padding-right:10px;">
                                  <div class="form-group">
                                      <form id="talarmListForm" method="POST" class="mt-repeater form-horizontal">
                                          <div data-repeater-list="group-a">
                                              <div data-repeater-item class="mt-repeater-item">
                                                  <!-- jQuery Repeater Container -->
                                                  <div class="mt-repeater-input">
                                                      <label class="control-label">사용자ID</label>
                                                      <br/>
                                                      <input class="form-control"
														name="emp_user_id_s" value="${search.emp_user_id_s}" /> </div>
												   <div class="mt-repeater-input">
                                                      <label class="control-label">사용자명</label>
                                                      <br/>
                                                      <input class="form-control"
														name="emp_user_name" value="${search.emp_user_name}" /> </div>
                                                  <div class="mt-repeater-input">
                                                      <label class="control-label">E-mail</label>
                                                      <br/>
                                                      <input class="form-control"
														name="email_address" value="${search.email_address}" /></div>
                                                  <div class="mt-repeater-input">
                                                      <label class="control-label">전화번호</label>
                                                       <br/>
                                                      <input class="form-control"
														name="mobile_number" value="${search.mobile_number}" /></div>
												  
                                                  </div>
                                              </div>
                                          </div>
                                          <div align="right">
                                           <button type="reset"
											class="btn btn-sm red-mint btn-outline sbold uppercase"
											onclick="resetOptions(talarmListConfig['listUrl'])">
											<i class="fa fa-remove"></i> <font>취소
										</button>
										<button class="btn btn-sm blue btn-outline sbold uppercase"
											onclick="searchTalarmList()">
											<i class="fa fa-search"></i> 검색
										</button>&nbsp;&nbsp;
										<div class="btn-group">
											<a href="javascript:;" data-toggle="dropdown">
												<img src="${rootPath}/resources/image/icon/XLS_3.png">
											</a>
											<ul class="dropdown-menu pull-right">
												<li>
													<a data-toggle="modal" data-target="#myModal">업로드</a> 
												</li>
												<li><a onclick="excelTalarmList()"> 다운로드 </a></li>
											</ul>
										</div>
									</div>
									
									<input type="hidden" name="main_menu_id"
										value="${paramBean.main_menu_id}" /> <input type="hidden"
										name="sub_menu_id" value="${paramBean.sub_menu_id}" /> <input
										type="hidden" name="current_menu_id" value="${currentMenuId}" />
									<input type="hidden" name="page_num" value="${search.page_num}" />
									<input type="hidden" name="isSearch" value="${paramBean.isSearch }" />
                                      </form>
                                  </div>
                              </div>
                          </div>
                      </div>
				</div>
				<table
					class="table table-striped table-bordered table-hover order-column"
					id="datatable_ajax_2" aria-describedby="datatable_ajax_2_info"
					role="grid">

					<thead>
						<tr role="row" class="heading">
							<th width="10%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								No.</th>
							<th width="15%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								사용자ID</th>
							<th width="15%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								사용자명</th>
							<th width="15%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								E-mail</th>
							<th width="15%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								연락처</th>
							<th width="10%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								E-mail 수신여부</th>
							<th width="10%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								전화 수신여부</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${empty talarmList}">
								<tr>
									<td colspan="7" align="center">데이터가 없습니다.</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:set value="${search.total_count}" var="count" />
								<c:forEach items="${talarmList}" var="talarm"
									varStatus="status">
									<tr
										onclick="findTalarmMngtOne('${talarm.emp_user_id}');"
										style="cursor: pointer;">
										<td style="text-align: center;">${count - search.page_num * 10  + 10}</td>
										<td style="padding-left: 20px;"><c:out value="${talarm.emp_user_id}"/></td>
										<td style="padding-left: 20px;"><c:out value="${talarm.emp_user_name}"/></td>
										<td style="text-align: center;"><c:out value="${talarm.email_address}"/></td>
										<td style="padding-left: 20px;"><c:out value="${talarm.mobile_number}"/></td>
										
										<c:choose>
											<c:when
												test="${template_yesNo.options[talarm.useyn_email] == '사용'}">
												<td style="text-align: center; vertical-align: middle;"><span
													class="label label-sm label-success"> 사용 </span></td>
											</c:when>
											<c:otherwise>
												<td style="text-align: center; vertical-align: middle;"><span
													class="label label-sm label-warning ">미사용 </span></td>
											</c:otherwise>
										</c:choose>
										<c:choose>
											<c:when
												test="${template_yesNo.options[talarm.useyn_htel] == '사용'}">
												<td style="text-align: center; vertical-align: middle;"><span
													class="label label-sm label-success"> 사용 </span></td>
											</c:when>
											<c:otherwise>
												<td style="text-align: center; vertical-align: middle;"><span
													class="label label-sm label-warning ">미사용 </span></td>
											</c:otherwise>
										</c:choose>
									</tr>
									<c:set var="count" value="${count - 1 }" />
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
				<div class="dataTables_processing DTS_Loading"
					style="display: none;">Please wait ...</div>
				<!-- 				</div> -->
				<!-- 페이징 영역 -->
				<c:if test="${search.total_count > 0}">
					<div class="page left" id="pagingframe" align="center">
						<p>
							<ctl:paginator currentPage="${search.page_num}"
								rowBlockCount="${search.size}"
								totalRowCount="${search.total_count}" />
						</p>
					</div>
				</c:if>
			</div>
		</div>
	</div>
</div>
<!-- 엑셀양식 다운로드 -->
<form action="exdownload.html" method="post" id="exdown" name="exdown"
	enctype="multipart/form-data"></form>
<script type="text/javascript">
	var talarmListConfig = {
		"listUrl" : "${rootPath}/talarmMngt/list.html",
		"detailUrl" : "${rootPath}/talarmMngt/detail.html",
		"downloadUrl" : "${rootPath}/talarmMngt/download.html",
		"uploadUrl" : "${rootPath}/talarmMngt/upload.html"
	};
</script>