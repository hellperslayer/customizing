<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<c:set var="currentMenuId" value="${index_id }" />
<c:set var="selDeptId" value="${sel_dept_id }" />
<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<link rel="stylesheet" href="${rootPath}/resources/assets/global/plugins/jstree/dist/themes/default/style.min.css" />
<script src="${rootPath }/resources/js/common/jquery-ui-1.9.0.custom.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath }/resources/js/common/jquery.cookie.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath }/resources/assets/global/plugins/jstree/dist/jstree.min.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/system_management/departmentMngtList.js" type="text/javascript" charset="UTF-8"></script>

<h1 class="page-title">${currentMenuName}</h1>
<!-- END PAGE TITLE-->
<!-- BEGIN PAGE CONTENT-->
<c:if test="${adminUserAuthId == 'AUTH00000' }">
<div class="portlet">
	<div class="row">
		<div class="col-md-4">
			<div class="btn-group">
				<c:if test="${adminUserAuthId == 'AUTH00000'}">
					<button type="button" class="btn btn-sm blue btn-outline sbold uppercase" onclick="showDepartmentDetail(null)">
					<i class="fa fa-plus"></i> 신규 
					</button>
				</c:if>
			</div>
		</div>
	</div>
</div>
</c:if>
<div class="row">
	<!-- 메뉴트리 -->
	<div class="col-md-4">
		<div id="tree_123" class="portlet light portlet-fit bordered">
			
		</div>
	</div>
<!--[if lt IE 9]>
<script src="${rootPath}/resources/assets/global/plugins/respond.min.js"></script>
<script src="${rootPath}/resources/assets/global/plugins/excanvas.min.js"></script> 
<script src="${rootPath}/resources/assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="${rootPath}/resources/assets/global/plugins/jstree/dist/jstree.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="${rootPath}/resources/assets/pages/scripts/ui-tree.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

	<!-- 상세 -->
	<div class="col-md-8">
		<div id="departmentDetailDiv" class="portlet light portlet-fit bordered">
			<div class="portlet-body">
				<!-- BEGIN FORM-->
				<form id="departmentDetailForm" method="POST" role="form" action="#"
					class="form-horizontal form-bordered form-row-stripped">
					<table id="user" class="table table-bordered table-striped">
						<tbody>
							<tr>
								<th style="width: 10%; text-align: center;"><label
									class="control-label">소속명<span class="required">*</span>
								</label></th>
								<td class="form-group form-md-line-input"
									style="text-align: center; vertical-align: middle;width: 25%;">
									<input type="text" class="form-control" id="dept_name" name="dept_name"/><div class="form-control-focus"></div>
								</td>
								<th style="width: 10%; text-align: center;"><label
									class="control-label">소속코드<span class="required">*</span>
								</label></th>
								<td class="form-group form-md-line-input"
									style="text-align: center; vertical-align: middle;width: 25%;">
									<input type="text" class="form-control" id="dept_id" name="dept_id" readonly="readonly"/><div class="form-control-focus"></div>
								</td>
								<th style="width: 15%; text-align: center;">
									<label class="control-label">부모소속명</label>
								</th>
								<td>
									<input type="hidden" id="parent_dept_id" name="parent_dept_id"/>
									<input type="text" readonly="readonly" class="form-control" id="parent_dept_name" name="parent_dept_name"/><div class="form-control-focus"></div>
								</td>
							</tr>
						</tbody>
					</table>
					<hr/>
					<input type="hidden" name="dept_id" />
					<input type="hidden" name="use_flag" value="Y" />
				</form>
				<!-- END FORM-->


				<div class="form-actions" align="right" style="padding-right: 10px;">
				<br>
					<div class="row">
						<!-- <div class="btn-set pull-right"> -->
						<button type="button"
							class="btn btn-sm blue btn-outline sbold uppercase"
							id="addDepartmentBtn" onclick="addDepartment()">
							<i class="fa fa-check"></i> 등록
						</button>
						<button type="button"
							class="btn btn-sm blue btn-outline sbold uppercase"
							id="saveDepartmentBtn" onclick="saveDepartment()">
							<i class="fa fa-check"></i> 수정
						</button>
						<c:if test="${adminUserAuthId == 'AUTH00000' }">
							<button type="button"
								class="btn btn-sm red-mint btn-outline sbold uppercase"
								id="removeDepartmentBtn" onclick="removeDepartment()">
								<i class="fa fa-close"></i> 삭제
							</button>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- END PAGE CONTENT-->

<form id="departmentListForm" method="POST">
	<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" />
	<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
	<input type="hidden" name="current_menu_id" value="${currentMenuId }" />
	<input type="hidden" name="sel_dept_id" value="${selDeptId }" />
</form>

<script type="text/javascript">

	var departmentConfig = {
		"detailUrl":"${rootPath}/departmentMngt/detail.html"
		,"addUrl":"${rootPath}/departmentMngt/add.html"
		,"saveUrl":"${rootPath}/departmentMngt/save.html"
		,"removeUrl":"${rootPath}/departmentMngt/remove.html"
		,"loginPage" : "${rootPath}/loginView.html"
	};
	
</script>