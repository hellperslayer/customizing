
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<c:set var="currentMenuId" value="${index_id}" />
<c:set var="selSystemSeq" value="${sel_system_seq}" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />
<c:set var="adminUserAuthId" value="${userSession.auth_id}" />

<link rel="stylesheet" href="${rootPath}/resources/assets/global/plugins/jstree/dist/themes/default/style.min.css" />
<script src="${rootPath }/resources/js/common/jquery-ui-1.9.0.custom.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath }/resources/js/common/jquery.cookie.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/system_management/system_menu.js" type="text/javascript" charset="UTF-8"></script>

<h1 class="page-title">${currentMenuName}</h1>
<div class="portlet">
	<div class="row">
		<div class="col-md-4">
			<div class="btn-group">
					<button type="button" class="btn btn-sm blue btn-outline sbold uppercase" id="addSystemBtn" onclick="addSystem()">
					<i class="fa fa-plus"></i> 신규 
					</button>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-4">
		<div id="tree_1" class="portlet light portlet-fit bordered">
			<ul>
			<li data-jstree='{ "opened" : true }'>시스템
				<ul>
				<c:forEach items="${hierarchySystems}" var="system">
					<c:if test="${system.system_seq eq '01'}">

					</c:if>

					<c:if test="${system.system_seq ne '01'}">

						<li class="folder"><a href="#" onclick="findSystemDetail('${system.system_seq}')">
						<c:out value="${system.system_name}(CODE: ${system.system_seq})"/>
						</a></li>

					</c:if>

				</c:forEach>
				</ul>
			</li>
			</ul>
		</div>
	</div>

<script src="${rootPath}/resources/assets/global/plugins/jstree/dist/jstree.min.js" type="text/javascript"></script>
<script src="${rootPath}/resources/assets/pages/scripts/ui-tree.min.js" type="text/javascript"></script>

	<!-- 상세 -->
	<div class="col-md-8">
		<div id="systemDetailDiv" class="portlet light portlet-fit bordered">
			<div class="portlet-body">
				<!-- BEGIN FORM-->
				<form id="systemDetailForm" method="POST" role="form" action="#"
					class="form-horizontal form-bordered form-row-stripped">
					<table class="table table-bordered table-striped">
						<tbody>
							<tr>
								<th style="width: 15%; text-align: center;"><label
									class="control-label">시스템명<span class="required">*</span>
								</label></th>
								<td style="width: 35%;" class="form-group form-md-line-input">
									<input type="text" class="form-control" id="system_name"
									name="system_name">
									<div class="form-control-focus"></div>
								</td>
								<th style="width: 15%; text-align: center;"><label
									class="control-label">로그 유형<span class="required">*</span>
								</label></th>
								<td class="form-group form-md-line-input"><select class="form-control" id="system_type" name="system_type" />
									<option></option>
									<option>BIZ_LOG</option>
									<option>BIZ_LOG_ONR</option>
									<option>DBAC_LOG</option>	
									<div class="form-control-focus"></div></td>
							</tr>
							<tr id="insertTr">
								<th style="width: 15%; text-align: center;"><label
									class="control-label">정렬순서<span class="required">*</span>
								</label></th>
								<td style="width: 35%;" class="form-group form-md-line-input">
									<input type="text" class="form-control" id="description"
									name="description" />
									<div class="form-control-focus"></div>
								</td>
								<th style="width: 15%; text-align: center;"><label
									class="control-label">메인 URL </label></th>
								<td style="width: 35%;" class="form-group form-md-line-input">
									<input type="text" class="form-control" id="main_url"
									name="main_url" />
									<div class="form-control-focus"></div>
								</td>
							</tr>
							<tr>
								<th style="width: 15%; text-align: center;"><label
									class="control-label">관리자<span class="required"></span>
								</label></th>
								<td style="width: 35%;" class="form-group form-md-line-input">
									<input type="text" class="form-control" id="administrator"
									name="administrator" />
									<div class="form-control-focus"></div>
								</td>
								<th style="width: 15%; text-align: center;"><label
									class="control-label">로그생성방식<span class="required">*</span>
								</label></th>
								<td class="form-group form-md-line-input">
									<select class="form-control" id="system_type" name="system_type" />
										<c:forEach items="${agentType}" var="at">
											<option value="${at['code_id'] }" <%-- <c:if test='${agentType.code_id eq search.system_seq}'>selected="selected"</c:if> --%>>
											${at['code_name'] }</option>
										</c:forEach>
								</td>
							</tr>
							<tr>
								<th style="width: 15%; text-align: center;"><label class="control-label">수집서버IP<span class="required"></span>
								</label></th>
								<td style="width: 35%;" class="form-group form-md-line-input">
									<input type="text" class="form-control" id="administrator" name="administrator" />
									<div class="form-control-focus"></div>
								</td>
								<th style="width: 15%; text-align: center;"><label class="control-label">분석서버IP<span class="required"></span>
								</label></th>
								<td style="width: 35%;" class="form-group form-md-line-input">
									<input type="text" class="form-control" id="administrator" name="administrator" />
									<div class="form-control-focus"></div>
								</td>
							</tr>
							<tr>
								<th style="width: 15%; text-align: center;"><label class="control-label">네트워크 타입<span class="required"></span>
								</label></th>
								<td class="form-group form-md-line-input">
									<div class="md-radio-inline">
										<div class="md-radio">
											<input type="radio" id="raN_1" name="network_type" value="W" class="md-radiobtn"/>
											<label for="raN_1"> <span></span> <span class="check"></span> <span class="box"></span> 업무망 </label>
										</div>
										<div class="md-radio">
											<input type="radio" id="raN_2" name="network_type" value="A" class="md-radiobtn"/>
											<label for="raN_2"> <span></span> <span class="check"></span> <span class="box"></span> 행정망 </label>
										</div>
										<div class="md-radio">
											<input type="radio" id="raN_3" name="network_type" value="E" class="md-radiobtn"/>
											<label for="raN_3"> <span></span> <span class="check"></span> <span class="box"></span> 외부망 </label>
										</div>
									</div>
								</td>
								<th style="width: 15%; text-align: center;"><label class="control-label" style="text-align: center; padding: 0px">다운로드 기준<br>임계치 설정<span class="required"></span>
								</label></th>
								<td style="width: 35%;" class="form-group form-md-line-input">
									<input type="text" class="form-control" id="threshold" name="threshold" style="width: 75%; display: inline"/>
									<button type="button"
										class="btn btn-sm btn-default btn-outline sbold uppercase"
										id="subBatchBtn" onclick="batchMenu()">
										<i id="checkFont" class="fa fa-square"></i> 일괄적용
									</button>
									<input type="checkbox" name="sub_batch" id="sub_batch" style="display: none"/>
								</td>
							</tr>
						</tbody>
					</table>

					<input type="hidden" id="system_seq" name="system_seq" />
					
				</form>
				<!-- 버튼 영역 -->
				<hr/>
				<div class="form-actions" align="right" style="padding-right: 10px;">
					<button type="button"
						class="btn btn-sm blue btn-outline sbold uppercase"
						id="saveSystemBtn" onclick="saveSystem()">
						<i class="fa fa-check"></i> 수정
					</button>
					<button type="button"
						class="btn btn-sm red-mint btn-outline sbold uppercase"
						id="removeSystemBtn" onclick="removeSystem()">
						<i class="fa fa-close"></i> 삭제
					</button>
				</div>
				
			</div>
			
			<div class="portlet-body">
				<form id="identiInfoForm" method="POST" class="form-horizontal form-bordered form-row-stripped">
				<table class="table table-bordered table-striped">
					<tbody>
					<tr>
						<th rowspan="4" width="25%" style="text-align: center; vertical-align: middle;">고유식별정보보유현황</th>
						<th style="width: 25%; text-align: center; vertical-align: middle;"><label class="control-label">주민등록번호<span class="required"></span>
						</label></th>
						<td style="width: 50%;" class="form-group">
							<div class="md-radio-inline" style="float: left;">
								<div class="md-radio">
									<input type="radio" id="ra1_1" name="existence_yn_1" value="Y" class="md-radiobtn" onchange="inputTagCk(1)"/>
									<label for="ra1_1"> <span></span> <span class="check"></span> <span class="box"></span> 유 </label>
								</div>
								<div class="md-radio">
									<input type="radio" id="ra2_1" name="existence_yn_1" value="N" class="md-radiobtn" onchange="inputTagCk(1)"/>
									<label for="ra2_1"> <span></span> <span class="check"></span> <span class="box"></span> 무 </label>
								</div>
							</div>
							<div style="float: right;">
							<span style="display: inline-flex;">(<input type="text" class="form-control" style="text-align: right;" name="information_ct_1" />건)</span>
							</div>
						</td>
					</tr>
					<tr>
						<th style="width: 30%; text-align: center; vertical-align: middle;"><label class="control-label">여권번호<span class="required"></span>
						</label></th>
						<td style="width: 70%;" class="form-group">
							<div class="md-radio-inline" style="float: left;">
								<div class="md-radio">
									<input type="radio" id="ra1_2" name="existence_yn_2" value="Y" class="md-radiobtn" onclick="inputTagCk(2)"/>
									<label for="ra1_2"> <span></span> <span class="check"></span> <span class="box"></span> 유 </label>
								</div>
								<div class="md-radio">
									<input type="radio" id="ra2_2" name="existence_yn_2" value="N" class="md-radiobtn" onclick="inputTagCk(2)"/>
									<label for="ra2_2"> <span></span> <span class="check"></span> <span class="box"></span> 무 </label>
								</div>
							</div>
							<div style="float: right;">
							<span style="display: inline-flex;">(<input type="text" class="form-control" style="text-align: right;" name="information_ct_2" />건)</span>
							</div>
						</td>
					</tr>
					<tr>
						<th style="width: 30%; text-align: center; vertical-align: middle;"><label class="control-label">운전면허번호<span class="required"></span>
						</label></th>
						<td style="width: 70%;" class="form-group">
							<div class="md-radio-inline" style="float: left;">
								<div class="md-radio">
									<input type="radio" id="ra1_3" name="existence_yn_3" value="Y" class="md-radiobtn" onchange="inputTagCk(3)"/>
									<label for="ra1_3"> <span></span> <span class="check"></span> <span class="box"></span> 유 </label>
								</div>
								<div class="md-radio">
									<input type="radio" id="ra2_3" name="existence_yn_3" value="N" class="md-radiobtn" onchange="inputTagCk(3)"/>
									<label for="ra2_3"> <span></span> <span class="check"></span> <span class="box"></span> 무 </label>
								</div>
							</div>
							<div style="float: right;">
							<span style="display: inline-flex;">(<input type="text" class="form-control" style="text-align: right;" name="information_ct_3" />건)</span>
							</div>
						</td>
					</tr>
					<tr>
						<th style="width: 30%; text-align: center; vertical-align: middle;"><label class="control-label">외국인등록번호<span class="required"></span>
						</label></th>
						<td style="width: 70%;" class="form-group">
							<div class="md-radio-inline" style="float: left;">
								<div class="md-radio">
									<input type="radio" id="ra1_4" name="existence_yn_4" value="Y" class="md-radiobtn" onchange="inputTagCk(4)"/>
									<label for="ra1_4"> <span></span> <span class="check"></span> <span class="box"></span> 유 </label>
								</div>
								<div class="md-radio">
									<input type="radio" id="ra2_4" name="existence_yn_4" value="N" class="md-radiobtn" onchange="inputTagCk(4)"/>
									<label for="ra2_4"> <span></span> <span class="check"></span> <span class="box"></span> 무</label>
								</div>
							</div>
							<div style="float: right;">
							<span style="display: inline-flex;">(<input type="text" class="form-control" style="text-align: right;" name="information_ct_4" />건)</span>
							</div>
						</td>
					</tr>
					</tbody>
				</table>
				</form>
				<div class="form-actions" align="right" style="padding-right: 10px;">
					<button type="button" class="btn btn-sm blue btn-outline sbold uppercase" onclick="saveIdentiInfo()">
						<i class="fa fa-check"></i> 저장
					</button>
				</div>
			</div>
		</div>
	</div>
</div>



<!-- END PAGE CONTENT-->

<form id="systemListForm" method="POST">
	<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" />
	<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
	<input type="hidden" name="current_menu_id" value="${currentMenuId }" />
	<input type="hidden" name="sel_system_seq" value="${selSystemSeq }" />
</form>

<script type="text/javascript">

	var systemConfig = {
		"detailUrl":"${rootPath}/systemMngt/detail.html"
		,"addUrl":"${rootPath}/systemMngt/add.html"
		,"saveUrl":"${rootPath}/systemMngt/save.html"
		,"removeUrl":"${rootPath}/systemMngt/remove.html"
		,"loginPage" : "${rootPath}/loginView.html"
		,"addView": "${rootPath}/systemMngt/addView.html"
	};
	
</script>