<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/system_management/codeMngtDetail.js" type="text/javascript" charset="UTF-8"></script>


<!-- 하위코드 상세 -->
<div class="portlet light bordered form-fit">
	<div class="portlet-title">
	    <div class="caption">
	        <!-- <i class="icon-user font-blue-hoki"></i> -->
	        <span class="caption-subject font-blue-hoki bold uppercase">${currentMenuName}</span>
	        <!-- <span class="caption-helper">demo form...</span> -->
	    </div>	    
	</div>
	<div class="portlet-body form">
	    <!-- BEGIN FORM-->
	    <form id="codeDetailForm" method="POST" class="form-horizontal form-bordered form-row-stripped">	    
	        <div class="form-body">	        
	            <div class="form-group form-md-line-input"> <!-- 같은 라인 폼그룹 -->
                    <label class="col-md-2 control-label">그룹코드ID
                    	<span class="required">*</span>
                    </label>
                    <div class="col-md-4">
                        <div class="form-control form-control-static"> <c:out value="${paramBean.group_code_id}" /> </div>	<!-- input 라인 없는 읽기전용 -->
                        <div class="form-control-focus"> </div>
                    </div>
                    <label class="col-md-2 control-label">코드ID
                    	<span class="required">*</span>
                    </label>
                    <div class="col-md-4">
                    	<c:choose>
							<c:when test="${empty codeDetail}">
								<input type="text" class="form-control" name="code_id" placeholder="">
							</c:when>
							<c:otherwise>
								<c:out value="${codeDetail.code_id}" />
								<input type="hidden" class="form-control" name="code_id" value="${codeDetail.code_id}" />
							</c:otherwise>
						</c:choose>
                        <div class="form-control-focus"> </div>
                    </div>
                </div>
                <div class="form-group form-md-line-input"> 
                    <label class="col-md-2 control-label" for="form_control_1">코드명
                    	<span class="required">*</span>
                    </label>
                    <div class="col-md-4">
                    	<input type="text" class="form-control" name="code_name"id="form_control_1" value="${codeDetail.code_name}" />
                        <div class="form-control-focus"> </div>                     
                    </div>
                    <label class="col-md-2 control-label">코드타입
                    	<span class="required">*</span>
                    </label>
                    <div class="col-md-4">
                        <div class="md-radio-inline">
	                        <div class="md-radio">
	                            <input type="radio" id="checkbox1_1" name="code_type" value="NORMAL" class="md-radiobtn" ${codeDetail.code_type == 'NORMAL' ? 'checked' : ''}>
	                            <label for="checkbox1_1">
	                                <span></span>
	                                <span class="check"></span>
	                                <span class="box"></span> 일반 </label>
	                        </div>
	                        <div class="md-radio">
	                            <input type="radio" id="checkbox1_2" name="code_type" value="SYSTEM" class="md-radiobtn" ${codeDetail.code_type != 'NORMAL' ? 'checked' : ''}>
	                            <label for="checkbox1_2">
	                                <span></span>
	                                <span class="check"></span>
	                                <span class="box"></span> 시스템 </label>
	                        </div>	                        
	                    </div>                        
                    </div>
                </div>
                <div class="form-group form-md-line-input"> 
                    <label class="col-md-2 control-label">사용여부
                    	<span class="required">*</span>
                    </label>
                    <div class="col-md-4">
	                    <div class="md-radio-inline">
	                        <div class="md-radio">
	                            <input type="radio" id="checkbox2_1" name="use_flag" value="Y" class="md-radiobtn" ${fn:containsIgnoreCase(codeDetail.use_flag,'Y') ? 'checked' : ''}>
	                            <label for="checkbox2_1">
	                                <span></span>
	                                <span class="check"></span>
	                                <span class="box"></span> 사용 </label>
	                        </div>
	                        <div class="md-radio">
	                            <input type="radio" id="checkbox2_2" name="use_flag" value="N" class="md-radiobtn" ${!fn:containsIgnoreCase(codeDetail.use_flag,'Y') ? 'checked' : ''}>
	                            <label for="checkbox2_2">
	                                <span></span>
	                                <span class="check"></span>
	                                <span class="box"></span> 미사용 </label>
	                        </div>	                        
	                    </div>
	                </div>
                    <label class="col-md-2 control-label" for="form_control_2">정렬순서</label>
                    <div class="col-md-4">
                    	<input type="text" name="sort_order" class="form-control" value="${codeDetail.sort_order }">
                        <div class="form-control-focus"> </div>                       
                    </div>
                </div>
                <div class="form-group form-md-line-input">
	                <label class="col-md-2 control-label" for="form_control_3">코드설명</label>
	                <div class="col-md-10">
	                    <textarea class="form-control" name="description" rows="5" cols="50">${codeDetail.description}</textarea>
	                    <div class="form-control-focus"> </div>
	                </div>
	            </div> 
	            <c:if test="${not empty codeDetail}">
	            <div class="form-group form-md-line-input"> 
                    <label class="col-md-2 control-label">등록일시                    	
                    </label>
                    <div class="col-md-4">                                                
                        <div class="form-control form-control-static"> <fmt:formatDate value="${codeDetail.insert_datetime }" pattern="yyyy-MM-dd kk:mm:ss"/> </div>	
                        <div class="form-control-focus"> </div>
                    </div>
                    <label class="col-md-2 control-label">수정일시                    	
                    </label>
                    <div class="col-md-4">
                        <div class="form-control form-control-static"> <fmt:formatDate value="${codeDetail.update_datetime }" pattern="yyyy-MM-dd kk:mm:ss"/> </div>  
                        <div class="form-control-focus"> </div>
                    </div>
                </div>        
                </c:if>         	            
	        </div> 
	        
	        <input type="hidden" name="group_code_id" value="${paramBean.group_code_id}" />
			<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id}" />
			<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id}" />
			<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
	    </form>
	    <!-- END FORM-->
	    
	    <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-2 col-md-10" align="right" style="padding-right:30px;">
                	<button type="button" class="btn btn-sm dark btn-outline sbold uppercase" onclick="moveGroupCodeList()">
                    	<i class="fa fa-list"></i> 목록</button>
                    <c:choose>
						<c:when test="${empty codeDetail}">
							<button type="button" class="btn btn-sm blue btn-outline sbold uppercase" onclick="addCode()">
		                        <i class="fa fa-check"></i> 등록</button>
						</c:when>
						<c:otherwise>
							<button type="button" class="btn btn-sm blue btn-outline sbold uppercase" onclick="saveCode()">
		                        <i class="fa fa-check"></i> 수정</button>
		                    <button type="button" class="btn btn-sm red-mint btn-outline sbold uppercase" onclick="removeCode()">
		                        <i class="fa fa-close"></i> 삭제</button>    
						</c:otherwise>
					</c:choose>
                </div>
            </div>
        </div>
	</div>
</div>

<script type="text/javascript">
	
	var codeConfig = {
		"listUrl":rootPath + "/groupCodeMngt/list.html"
		,"addUrl":rootPath + "/codeMngt/add.html"
		,"saveUrl":rootPath + "/codeMngt/save.html"
		,"removeUrl":rootPath + "/codeMngt/remove.html"
		,"loginPage" : "${rootPath}/loginView.html"
	};
	
</script>