<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="adminUserId" value="${userSession.admin_user_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/system_management/adminBySystemDetail.js"type="text/javascript" charset="UTF-8"></script>


<!-- 권한관리 신규 -->
<h1 class="page-title">
	${currentMenuName} <c:if test="${empty adminUserDetail}">(신규)</c:if>
</h1>
<div class="portlet light portlet-fit bordered">
	<div class="portlet-body">
		<!-- BEGIN FORM-->
		<form id="adminUserDetailForm" method="POST"
			class="form-horizontal form-bordered form-row-stripped">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<th style="width: 15%; text-align: center;"><label
							class="control-label" for="form_control_1">관리자ID<span
								class="required">*</span>
						</label></th>
						<td style="width: 35%; vertical-align: middle;"
							class="form-group form-md-line-input"><c:choose>
								<c:when test="${empty adminUserDetail}">
									<input type="text" name="admin_user_id" class="form-control" />
									<div class="form-control-focus"></div>
								</c:when>
								<c:otherwise>
									<c:out value="${adminUserDetail.admin_user_id}" />
									<input type="hidden" name="admin_user_id"
										value="${adminUserDetail.admin_user_id}" />
								</c:otherwise>
							</c:choose></td>
						<th style="width: 15%; text-align: center;"><label
							class="control-label" for="form_control_2">관리자명 <span
								class="required">*</span>
						</label></th>
						<td style="width: 35%;" class="form-group form-md-line-input"><input type="text"
							name="admin_user_name" class="form-control"
							value="${adminUserDetail.admin_user_name }" />
							<div class="form-control-focus"></div></td>
					</tr>
					<tr>
						<th style="text-align: center;"><label class="control-label">비밀번호<span
								class="required">*</span></label></th>
						<td class="form-group form-md-line-input"
							style="vertical-align: middle;"><input type="password"
							name="password" class="form-control" />
							<div class="form-control-focus"></div></td>
						<th style="text-align: center;"><label class="control-label">비밀번호확인<span
								class="required">*</span>
						</label></th>
						<td class="form-group form-md-line-input"
							style="vertical-align: middle;"><input type="password"
							name="password_check" class="form-control" />
							<div class="form-control-focus"></div></td>
					</tr>
					<tr>
						<th style="text-align: center;"><label class="control-label">시스템</label></th>
						<td class="form-group form-md-line-input"
							style="vertical-align: middle;"><select name="system_seq"
							class="ticket-assign form-control input-medium">
								<c:forEach items="${systemMaster }" var="system">
									<option value="${system.system_seq }"
										${system.system_seq == adminUserDetail.auth_ids ? 'selected' : ''}><c:out
											value="${system.system_name }" /></option>
								</c:forEach>
						</select></td>
						<th style="text-align: center;"><label class="control-label">권한명
						</label></th>
						<td class="form-group form-md-line-input"
							style="vertical-align: middle;"><c:choose>
								<c:when test="${empty adminUserDetail }">
									<select name="auth_id"
										class="ticket-assign form-control input-medium">
										<c:choose>
											<c:when test="${adminUserAuthId == 'AUTH00000'}">
												<c:forEach items="${auths}" var="auth">
													<option value="${auth.auth_id}" ${auth.auth_id == adminUserDetail.auth_id ? 'selected' : ''}>
														<c:out value="${auth.auth_name }" />
													</option>
												</c:forEach>
											</c:when>
											<c:otherwise>
												<c:forEach items="${auths}" var="auth">
													<c:if test="${auth.auth_id != 'AUTH00000'}">
													<option value="${auth.auth_id}" ${auth.auth_id == adminUserDetail.auth_id ? 'selected' : ''}>
														<c:out value="${auth.auth_name }" />
													</option>
													</c:if>
												</c:forEach>
											</c:otherwise>
										</c:choose>
									</select>
								</c:when>
								<c:otherwise>
									<!--
										2015.04.20 
										관리자 ID가 이지서티 전용 ID (easycerti) 인 경우 슈퍼관리자 권한을 수정할 수 없다.
										수정을 잘못해서 접속하지 못하는 불상사를 막기 위함.
									 -->
									<c:choose>
										<c:when
											test="${adminUserAuthId == 'AUTH00000' && adminUserDetail.auth_id == 'AUTH00000' }">
											<div class="form-control form-control-static">
												<input type="hidden" name="auth_id"
													value="${adminUserDetail.auth_id }" /><b>슈퍼관리자</b>
											</div>
										</c:when>
										<c:otherwise>
											<!--
												2015.04.20 
												관리자 ID가 고객사에 제공하는 기본 ID (admin) 인 경우 관리자 권한을 수정할 수 없다.
												수정을 잘못해서 접속하지 못하는 불상사를 막기 위함.
											 -->
											<c:if test="${adminUserDetail.admin_user_id == 'admin' }">
												<div class="form-control form-control-static">
													<input type="hidden" name="auth_id"
														value="${adminUserDetail.auth_id }" />관리자
												</div>
											</c:if>
											<c:if test="${adminUserDetail.admin_user_id != 'admin' }">
												<select name="auth_id"
													class="ticket-assign form-control input-medium">
													<c:forEach items="${auths }" var="auth">
														<option value="${auth.auth_id}"
															${auth.auth_id == adminUserDetail.auth_id ? 'selected' : ''}><c:out
																value="${auth.auth_name }" /></option>
													</c:forEach>
												</select>
											</c:if>
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose></td>
					</tr>
					<tr>
						<th style="text-align: center;"><label class="control-label">연락처</label></th>
						<td class="form-group form-md-line-input"
							style="vertical-align: middle;"><input type="text"
							class="form-control" name="mobile_number"
							value="${adminUserDetail.mobile_number }">
							<div class="form-control-focus"></div></td>
						<th style="text-align: center;"><label class="control-label">E-MAIL
						</label></th>
						<td class="form-group form-md-line-input"
							style="vertical-align: middle;"><input type="text"
							class="form-control" name="email_address"
							value="${adminUserDetail.email_address }">
							<div class="form-control-focus"></div></td>
					</tr>
					<tr>
						<th style="text-align: center;vertical-align: middle;"><label class="control-label"
							for="form_control_1">관리자설명
						</label></th>
						<td colspan="3" style="vertical-align: middle;"
							class="form-group form-md-line-input"><textarea
								class="form-control" name="description" rows="5" cols="60">${adminUserDetail.description }</textarea>
							<div class="form-control-focus"></div>
						</td>
						<%-- <th style="text-align: center;vertical-align: middle;">
							<label class="control-label"
								for="form_control_2">알림여부 <span class="required">*</span>
							</label>
						</th>
						<td style="vertical-align: middle;">
							<input type="hidden" name="alarm_flag" value="${adminUserDetail.alarm_flag }">
						</td> --%>
					</tr>
					<c:if test="${!empty adminUserDetail }">
						<tr>
							<th style="text-align: center;"><label class="control-label"
								for="form_control_2">등록일시 </label></th>
							<td class="form-group form-md-line-input"
								style="vertical-align: middle;">
								<div class="form-control form-control-static">
									<fmt:formatDate value="${adminUserDetail.insert_datetime }"
										pattern="yyyy-MM-dd kk:mm:ss" />
								</div>
							</td>
							<th style="text-align: center;"><label class="control-label"
								for="form_control_1">수정일시 </label></th>
							<td style="vertical-align: middle;"
								class="form-group form-md-line-input">
								<div class="form-control form-control-static">
									<fmt:formatDate value="${adminUserDetail.update_datetime }"
										pattern="yyyy-MM-dd kk:mm:ss" />
								</div>
							</td>
						</tr>
					</c:if>
				</tbody>
			</table>
			<%-- <div style="padding-top: 50px;">
				<div id="authListDiv" class="dataTables_scrollBody"
					style="position: relative; overflow: auto; height: 230px; width: 100%;">
					<table style="border-top: 1px solid #e7ecf1"
						class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
						id="datatable_ajax_2" aria-describedby="datatable_ajax_2_info"
						role="grid"
						style="position: absolute; top: 0px; left: 0px; width: 100%;">

						<thead>
							<tr role="row" class="heading">
								<th width="50%" style="border-bottom: 1px solid #e7ecf1;">
									<input type="checkbox" class="auth_checkbox"
									id="authMenuAllCheck" onclick="allAuthMenuCheck(this)" />
								</th>
								<th width="50%" style="border-bottom: 1px solid #e7ecf1;">
									시스템명</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${systemMaster}" var="system"
								varStatus="status">
								<tr>
									<c:choose>
										<c:when
											test="${fn:contains(adminUserDetail.auth_ids, system.system_seq)}">
											<td><input type="checkbox" class="auth_checkbox"
												id="${system.system_seq}" name="auth_ids"
												value="${system.system_seq}" checked /></td>
										</c:when>
										<c:otherwise>
											<td><input type="checkbox" class="auth_checkbox"
												id="${system.system_seq}" name="auth_ids"
												value="${system.system_seq}" /></td>
										</c:otherwise>
									</c:choose>
									<td><c:out value="${system.system_name}" /></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>

			</div> --%>

			<div class="form-actions">
			<br>
				<div class="row">
					<div class="col-md-offset-2 col-md-10" align="right"
						style="padding-right: 30px;">
						<button type="button"
							class="btn btn-sm grey-mint btn-outline sbold uppercase"
							onclick="moveAdminUserList()">
							<i class="fa fa-list"></i> 목록
						</button>
						<c:choose>
							<c:when test="${empty adminUserDetail}">
								<button type="button"
									class="btn btn-sm blue btn-outline sbold uppercase"
									onclick="addAdminUser()">
									<i class="fa fa-check"></i> 등록
								</button>
							</c:when>
							<c:otherwise>
								<button type="button"
									class="btn btn-sm blue btn-outline sbold uppercase"
									onclick="saveAdminUser()">
									<i class="fa fa-check"></i> 수정
								</button>
								<c:if test="${adminUserId != adminUserDetail.admin_user_id}">
									<button type="button"
									class="btn btn-sm red-mint btn-outline sbold uppercase"
									onclick="removeAdminUser()">
									<i class="fa fa-close"></i> 삭제
									</button>
								</c:if>
							
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
			<!-- END F -->
		</form>

		<form id="adminUserListForm" method="POST">
			<!-- 메뉴 관련 input 시작 -->
			<input type="hidden" name="main_menu_id"
				value="${paramBean.main_menu_id }" /> <input type="hidden"
				name="sub_menu_id" value="${paramBean.sub_menu_id }" /> <input
				type="hidden" name="current_menu_id" value="${currentMenuId}" />
			<!-- 메뉴 관련 input 끝 -->

			<!-- 페이지 번호 -->
			<input type="hidden" name="page_num" value="${paramBean.page_num }" />

			<!-- 검색조건 관련 input 시작 -->
			<input type="hidden" name="admin_user_id_1"
				value="${paramBean.admin_user_id_1 }" /> <input type="hidden"
				name="admin_user_name" value="${paramBean.admin_user_name }" /> <input
				type="hidden" name="dept_id" value="${paramBean.dept_id }" /> <input
				type="hidden" name="deptLowSearchFlag"
				value="${paramBean.deptLowSearchFlag }" />
			<input type="hidden" name="isSearch" value="${paramBean.isSearch }"/>
			<!-- 검색조건 관련 input 끝 -->
		</form>
		<!-- END FORM-->
	</div>
</div>

<script type="text/javascript">

	var adminBySystemDetailConfig = {
		"listUrl":"${rootPath}/adminBySystem/list.html"
		,"addUrl":"${rootPath}/adminBySystem/add.html"
		,"saveUrl":"${rootPath}/adminBySystem/save.html"
		,"removeUrl":"${rootPath}/adminBySystem/remove.html"
	};
	
</script>

