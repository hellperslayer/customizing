<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/system_management/groupCodeMngtDetail.js" type="text/javascript" charset="UTF-8"></script>
<h1 class="page-title">${currentMenuName}</h1>
<div class="row">

	<div class="portlet light portlet-fit bordered">
		<div class="portlet-body">
			<div class="row">
				<div class="col-md-12">
					<form id="groupCodeDetailForm" method="POST"
						class="form-horizontal form-bordered form-row-stripped">
						<table id="user" class="table table-bordered table-striped">
							<tbody>
								<tr>
									<th style="width: 15%; text-align: center;"><label
										class="control-label" for="form_control_1">그룹코드ID <span
											class="required">*</span>
									</label></th>
									<td style="width: 35%;vertical-align: middle;" class="form-group form-md-line-input"><c:choose>
											<c:when test="${empty groupCodeDetail}">
												<input type="text" class="form-control" placeholder="" name="group_code_id">
												<div class="form-control-focus"></div>
											</c:when>
											<c:otherwise>
												<c:out value="${groupCodeDetail.group_code_id}" />
												<input type="hidden" class="form-control"
													name="group_code_id"
													value="${groupCodeDetail.group_code_id }" />
											</c:otherwise>
										</c:choose></td>
									<th style="width: 15%; text-align: center;"><label
										class="control-label" for="form_control_2">그룹코드명 <span
											class="required">*</span>
									</label></th>
									<td style="width: 35%;" class="form-group form-md-line-input">
									<input type="text" name="group_code_name" class="form-control"
										value="${groupCodeDetail.group_code_name}">
										<div class="form-control-focus"></div></td>
								</tr>
								<tr>
									<th style="text-align: center;"><label
										class="control-label">사용여부 <span class="required">*</span>
									</label></th>
									<td colspan="3" class="form-group form-md-line-input">
										<div class="md-radio-inline">
											<div class="md-radio col-md-4">
												<input type="radio" id="checkbox1_1" name="use_flag"
													value="Y" class="md-radiobtn"
													${fn:containsIgnoreCase(groupCodeDetail.use_flag,'Y') ? 'checked' : ''}>
												<label for="checkbox1_1"> <span></span> <span
													class="check"></span> <span class="box"></span> 사용
												</label>
											</div>
											<div class="md-radio col-md-4">
												<input type="radio" id="checkbox1_2" name="use_flag"
													value="N" class="md-radiobtn"
													${!fn:containsIgnoreCase(groupCodeDetail.use_flag,'Y') ? 'checked' : ''}>
												<label for="checkbox1_2"> <span></span> <span
													class="check"></span> <span class="box"></span> 미사용
												</label>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<th style="text-align: center;"><label
										class="control-label" for="form_control_3">그룹코드 설명</label></th>
									<td  class="form-group form-md-line-input" style="text-align: center;" colspan="3"><textarea
											class="form-control" id="form_control_3" name="description"
											rows="5">${groupCodeDetail.description}</textarea>
										<div class="form-control-focus"></div></td>
								</tr>
								<c:if test="${not empty groupCodeDetail}">
									<tr>
										<th style="text-align: center; vertical-align: middle;"><label class="control-label">등록일시</label></th>
										<td style="vertical-align: middle;" class="form-group form-md-line-input"><fmt:formatDate value="${groupCodeDetail.insert_datetime }" pattern="yyyy-MM-dd kk:mm:ss"/></td>
										<th style="text-align: center; vertical-align: middle;"><label class="control-label">수정일시</label></th>
										<td style="vertical-align: middle;" class="form-group form-md-line-input"><fmt:formatDate value="${groupCodeDetail.update_datetime }" pattern="yyyy-MM-dd kk:mm:ss"/></td>
									</tr>
								</c:if>
							</tbody>
						</table>
						<input type="hidden" name="main_menu_id"
							value="${paramBean.main_menu_id}" /> <input type="hidden"
							name="sub_menu_id" value="${paramBean.sub_menu_id}" /> <input
							type="hidden" name="current_menu_id" value="${currentMenuId}" />
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12" align="right">
					<!-- 버튼 영역 -->
					<div class="col-md-offset-2 col-md-10" align="right"
						style="padding-right: 30px;">
						<a class="btn btn-sm dark btn-outline sbold uppercase"
							onclick="moveGroupCodeList()"><i class="fa fa-list"></i> 목록</a>
						<c:choose>
							<c:when test="${empty groupCodeDetail}">
								<a class="btn btn-sm blue btn-outline sbold uppercase"
									onclick="addGroupCode()"><i class="fa fa-check"></i>&nbsp;등록</a>
							</c:when>
							<c:otherwise>
								<a class="btn btn-sm blue btn-outline sbold uppercase"
									onclick="saveGroupCode()"><i class="fa fa-check"></i>&nbsp;수정</a>
								<a class="btn btn-sm red-mint btn-outline sbold uppercase"
									onclick="removeGroupCode()"><i class="fa fa-remove"></i>&nbsp;삭제</a>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
		</div>



	</div>
</div>

<script type="text/javascript">
	var groupCodeConfig = {
		"listUrl" : rootPath + "/groupCodeMngt/list.html",
		"addUrl" : rootPath + "/groupCodeMngt/add.html",
		"saveUrl" : rootPath + "/groupCodeMngt/save.html",
		"removeUrl" : rootPath + "/groupCodeMngt/remove.html",
		"loginPage" : "${rootPath}/loginView.html"
	};
</script>