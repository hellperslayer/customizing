<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<c:set var="currentMenuId" value="${index_id}" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script
	src="${rootPath}/resources/js/psm/system_management/changeThemeList.js"
	type="text/javascript" charset="UTF-8"></script>


<!-- BEGIN PAGE TITLE-->
<h1 class="page-title">
	${currentMenuName}
	<!--bold font-blue-hoki <small>basic bootstrap tables with various options and styles</small> -->
</h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<div class="row">
	<div class="col-md-6">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light portlet-fit bordered">
			<div class="portlet-body">

				<table
					class="table table-striped table-bordered table-hover order-column">
					<thead>
						<tr>
							<th width="30%" style="text-align: center;">목록</th>
							<th width="70%" style="text-align: center;">선택</th>
						</tr>
					</thead>
					<form id="themeChangeForm" method="POST">
						<input type="hidden" name="main_menu_id"
							value="${paramBean.main_menu_id }" /> <input type="hidden"
							name="sub_menu_id" value="${paramBean.sub_menu_id }" />
					<tbody>
						<tr style="text-align: center;">
							<td style="vertical-align: middle;">테마</td>
							<td class="form-group form-md-line-input"><select
								name="chng_layout" class="ticket-assign form-control">
									<option value="0"
										<c:if test="${chng_layout eq 0}">selected="selected"</c:if>>UBI
										SAFER-PSM 시군구</option>
									<option value="1"
										<c:if test="${chng_layout eq 1}">selected="selected"</c:if>>UBI
										SAFER-PSM 아카데미</option>
							</select></td>
						</tr>
						<tr style="text-align: center;">
							<td style="vertical-align: middle;">대시보드</td>
							<td class="form-group form-md-line-input"><select
								name="dashboard" class="ticket-assign form-control">
									<option value="0"
										<c:if test="${dashboard eq 0}">selected="selected"</c:if>>대시보드
										전체 출력</option>
									<option value="1"
										<c:if test="${dashboard eq 1}">selected="selected"</c:if>>추출
										조건 출력 제외</option>
							</select></td>
						</tr>
					</tbody>
					</form>
				</table>

			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>

</div>
<div class="row" align="right">
	<div class="col-md-6">
		<button type="button"
			class="btn btn-sm green table-group-action-submit"
			onclick="saveTheme()">
			<i class="fa fa-check"></i> 등록
		</button>
	</div>
</div>
<script type="text/javascript">
	
	var changeThemeConfig = {
			"listUrl":"${rootPath}/changeThemeMngt/list.html",
			"saveUrl":"${rootPath}/changeThemeMngt/update.html",
			"loginPage" : "${rootPath}/loginView.html"
	};
	
</script>
