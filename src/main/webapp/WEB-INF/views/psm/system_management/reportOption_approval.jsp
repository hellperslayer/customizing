<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/system_management/reportOption.js" type="text/javascript" charset="UTF-8"></script>

<form id="appForm" method="POST">
	<input type="hidden" name="tab_flag" value="3"/>
	<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
    <input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" /> 
	<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" /> 
</form>

	<div class="caption">
		<i class="icon-settings font-dark"></i> 
		<span class="caption-subject font-dark sbold uppercase">점검보고서 결재라인 등록 </span>
	</div>	
	
       <div class="portlet-body">
            
            <table class="table table-striped table-bordered order-column">
				<tr>
					<td width="45%" style="text-align: center;" colspan="2" >결재자 이름 등록</td>
					<td width="55%" style="text-align: center; white-space: nowrap;">미리보기</td>
				</tr>
				<tr>
					<td width="15%" style="text-align: center;">
					결재칸수
						<select id="countSelect" onchange="AuthorCount()">
							<optgroup label="">
								<option value="0" ${authorizeLine.count eq 0 ? 'selected="selected"':"" }>사용하지 않음</option>
								<option ${authorizeLine.count eq 1 ? 'selected="selected"':"" }>1</option>
								<option ${authorizeLine.count eq 2 ? 'selected="selected"':"" }>2</option>
								<option ${authorizeLine.count eq 3 ? 'selected="selected"':"" }>3</option>
							</optgroup>
						</select>
					</td>
					<td width="30%" style="text-align: center;padding-left: 50px;">
						<table class="table-striped table-bordered order-column">
							<tr>
								<td style="width: 20%">이름</td>
								<td><input id="authorName1" class="authorName" type="text" disabled="disabled" value="${authorizeLine.name1}"></td>
							</tr>
							<tr>
								<td style="width: 20%">이름</td>
								<td><input id="authorName2" class="authorName" type="text" disabled="disabled" value="${authorizeLine.name2}"></td>
							</tr>
							<tr>
								<td style="width: 20%">이름</td>
								<td><input id="authorName3" class="authorName" type="text" disabled="disabled" value="${authorizeLine.name3}"></td>
							</tr>
							<tr>
								<td colspan="2">
									<input type="button" class="btn btn-sm yellow btn-outline sbold uppercase" onclick="AuthorCount()" value="미리보기" />
									<input type="button" class="btn btn-sm blue btn-outline sbold uppercase" onclick="addAuthorizeLine()" value="저장" />
								</td>
							</tr>
						</table>
					</td>
					<td width="55%" style="text-align: left; white-space: nowrap;">
						<div id="html_source" align="center">
							<TABLE border="1" id="preview" cellspacing="0" cellpadding="0" style='border-collapse:collapse; border: 1px solid black'>
								<TR id="authorNameTr">
									<TD rowspan="2" id="authorNameFirst" valign="middle" style='width: 27px; height: 114px; padding: 1.4pt 5.1pt 1.4pt 5.1pt;'>
									<P STYLE=''><SPAN STYLE=''>결</SPAN></P>
									<P STYLE='margin: 0px'><SPAN STYLE=''>재</SPAN></P>
									</TD>
								</TR>
								<TR id="authorMarkTr">
								</TR>
							</TABLE>
						</div>
					</td>
				</tr>
            </table>
        </div>
        <br><br>
        <div>
        <c:if test="${value_status ne null}">
        <div class="caption">
			<i class="icon-settings font-dark"></i> 
			<span class="caption-subject font-dark sbold uppercase">점검보고서 결재라인 설정</span>
		</div> 
		
		<div class="portlet-body">
			<form id="approvalForm" method="POST"> 
			<table style="border-top: 1px solid #e7ecf1"
				class="table table-bordered"
				style="position: absolute; top: 0px; left: 0px; width: 100%;">
				<thead>
				<tr>
					<th width="5%"
						style="border-bottom: 1px solid #e7ecf1; text-align: center;">
						No.</th>
					<th width="20%"
						style="border-bottom: 1px solid #e7ecf1; text-align: center;">
						시스템명</th>
					<th width="20%"
						style="border-bottom: 1px solid #e7ecf1; text-align: center;">
						1차 결재자명</th>
					<th width="20%"
						style="border-bottom: 1px solid #e7ecf1; text-align: center;">
						2차 결재자명</th>
					<th width="20%"
						style="border-bottom: 1px solid #e7ecf1; text-align: center;">
						3차 결재자명</th>
					<th width="15%"
						style="border-bottom: 1px solid #e7ecf1; text-align: center;">
						
						</th>
				</tr>	
				</thead>
				<tbody>
				 	<c:choose>
						<c:when test="${empty reportApproval}">
							<tr>
								<td colspan="6" align="center">데이터가 없습니다.</td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach items="${reportApproval }" var="approval" varStatus="status">
								<tr>
									<td style="text-align: center; vertical-align: middle;">${status.count }</td>
									<td style="text-align: left; vertical-align: middle;">
										<input type="hidden" name="system_seq${status.count }" value="${approval.system_seq}">
										${approval.system_name} (CODE: ${approval.system_seq})
									</td>
									<td style="text-align: center; vertical-align: middle;">
										
										<input type="text" class="form-control" id="first_auth${status.count }" name="first_auth${status.count }" value="${approval.first_auth}">
									</td>
									<td style="text-align: center; vertical-align: middle;">
										<input type="text" class="form-control" id="second_auth${status.count }" name="second_auth${status.count }" value="${approval.second_auth}">
									</td>
									<td style="text-align: center; vertical-align: middle;">
										<input type="text" class="form-control" id="third_auth${status.count }" name="third_auth${status.count }" value="${approval.third_auth}" >
									</td>
									<td style="text-align: center; vertical-align: middle;">			
										<input type="button" class="btn btn-sm blue btn-outline sbold uppercase" onclick="updReportApproval(${approval.system_seq}, ${status.count })" value="저장" />
									</td>
								</tr>
							</c:forEach>

						</c:otherwise>
					</c:choose> 
				</tbody>
			</table>
			</form>
		<div class="col-md-15" style="text-align: center; padding-bottom: 10px;">
			<input type="button" class="btn btn-sm red btn-outline sbold uppercase" onclick="wholeUpdReportApproval()" value="전체저장" />
		</div>
		</div>
		</c:if>
		</div>

<script type="text/javascript">
	
	var reportOptionConfig = {
			"listUrl":"${rootPath}/reportOption/list.html",
			"addUrl":"${rootPath}/reportOption/upload.html",
			
	};
	
	$(document).ready(function(){
		findDefaultDesc();
	});

</script>