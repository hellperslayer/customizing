<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>
<c:set var="userAddFlag" value="F" />
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />
<%-- <script src="${rootPath}/resources/js/common/jquery.form.js" type="text/javascript" charset="UTF-8"></script> --%>
<script src="${rootPath}/resources/js/psm/system_management/accessAuthList.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>
<h1 class="page-title">${currentMenuName}</h1>
<div class="portlet light portlet-fit portlet-datatable bordered">
	

	<!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog" style="position: relative ;top: 30%; left: 0;">

			<!-- Modal content -->
			<div class="modal-content">
				<div class="modal-header" style="background-color: #32c5d2;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">
						<b style="font-size: 13pt;">엑셀 업로드</b>
					</h4>
				</div>
				<div class="modal-body" style="background-color: #F9FFFF;">
					<form action="upload.html" method="post" id="fileForm"
						name="fileForm" enctype="multipart/form-data">
						<div>
							<table width="100%">
								<tr style="height: 40px">
									<td width="70%" style="vertical-align: middle;" align="center">
										<input class="fileUpLoad" type="file" id="file" name="file">
									</td>
									<td><img style="cursor: pointer;" name="submup"
										onclick="excelEmpUserUpLoad()"
										src="${rootPath}/resources/image/common/btn_exupload.gif"
										alt="엑셀업로드" title="엑셀업로드" /></td>
								</tr>
								<tr style="height: 40px">
									<td width="70%" style="vertical-align: middle;"><span
										class="downexam">&nbsp;&nbsp;&nbsp;※ 다운받은 양식으로 업로드하셔야  등록됩니다.</span></td>
									<td><img class="btn_excel exceldown" onclick="exDown()" style="cursor: pointer;"
										src="${rootPath}/resources/image/common/formUp.png"
										alt="양식다운로드" title="양식다운로드" /></td>
								</tr>
							</table>
						</div>
						<input type="hidden" id="result" name="result" value="false" />
					</form>
				</div>
				<div class="modal-footer">
					<a class="btn btn-sm red" href="#" data-dismiss="modal"> <i
						class="fa fa-remove"></i> 닫기
					</a>
				</div>
			</div>
		</div>
	</div>
	

	<div class="portlet-body">
		<div class="table-container">

			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">

				<div class="col-md-6" style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
					
					<div class="portlet box grey-salt  ">
                          <div class="portlet-title" style="background-color: #2B3643;">
                              <div class="caption">
                                  <i class="fa fa-search"></i>검색 & 엑셀</div>
                              <div class="tools">
				                 <a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
				             </div>
                          </div>
                          <div id="searchBar" class="portlet-body form" >
                              <div class="form-body" style="padding-left:10px; padding-right:10px;">
                                  <div class="form-group">
                                      <form id="empUserListForm" method="POST" class="mt-repeater form-horizontal">
                                          <div data-repeater-list="group-a">
                                              <div data-repeater-item class="mt-repeater-item" style="border-bottom: 0px; padding-bottom: 0px; margin-bottom: 0px;">
                                                  <!-- jQuery Repeater Container -->
                                                  <div class="mt-repeater-input">
                                                        <label class="control-label">권한변경일자</label>
                                                        <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
															<input type="text" class="form-control" id="search_fr" name="search_from" value="${search.search_fromWithHyphen}"> 
															<span class="input-group-addon"> &sim; </span> 
															<input type="text" class="form-control" id="search_to" name="search_to" value="${search.search_toWithHyphen}">
														</div> 
												  </div>
												  <div class="mt-repeater-input">
                                                      <label class="control-label">시스템명</label>
                                                      <select name="system_seq" class="form-control">
														<option value=""
															<c:if test="${search.system_seq == null}">selected="selected"</c:if>>
															----- 전체 -----</option>
														<c:forEach items="${systemMasterList}"
															var="systemMasterList">
															<option value="${systemMasterList.system_seq}"
																<c:if test="${systemMasterList.system_seq == search.system_seq}">selected="selected"</c:if>>${systemMasterList.system_name}</option>
														</c:forEach>
													 </select>
                                                  </div>
                                                  <div class="mt-repeater-input">
                                                      <label class="control-label">사용자명</label>
                                                      <input type="text" class="form-control" name="emp_user_name" value="${search.emp_user_name}" />
                                                  </div>
                                                  <div class="mt-repeater-input">
                                                      <label class="control-label">사용자ID</label>
                                                      <input type="text" class="form-control" name="emp_user_id" value="${search.emp_user_id}" />
                                                  </div>
                                                  <div class="mt-repeater-input">
                                                      <label class="control-label">접근권한</label>
                                                      <select name="status" id="status" class="form-control">
														<option value=""
															${search.status == '' ? 'selected="selected"' : ''}>
															----- 전체 -----</option>
														<c:if test="${empty CACHE_EMP_USER_STATUS}">
															<option>데이터 없음</option>
														</c:if>
														<%-- <c:if test="${!empty CACHE_EMP_USER_STATUS}">
															<c:forEach items="${CACHE_EMP_USER_STATUS}" var="i"
																varStatus="z">
																<option value="${i.key}"
																	${i.key==search.status ? "selected=selected" : "" }>${ i.value}</option>
															</c:forEach>
														</c:if> --%>
														<option value="W" <c:if test="${search.status eq 'W' }">selected="selected"</c:if>>있음</option>
														<option value="R" <c:if test="${search.status eq 'R' }">selected="selected"</c:if>>없음</option>
													  </select>
                                                  </div>
                                                  </div>
                                              <div data-repeater-item class="mt-repeater-item">
                                                <div class="mt-repeater-input">
                                                    <label class="control-label">관리자명</label>
                                                    <input type="text" class="form-control" name="approver" value="${search.approver}" />
                                                </div>
                                               	<div class="mt-repeater-input">
                                                    <label class="control-label">관리자ID</label>
                                                    <input type="text" class="form-control" name="approver_id" value="${search.approver_id}" />
                                                </div>
                                                <div class="mt-repeater-input">
                                                    <label class="control-label">관리자IP</label>
                                                    <input type="text" class="form-control" name="approver_ip" value="${search.approver_ip}" />
                                                </div>
                                                  <div class="mt-repeater-input">
                                                      <label class="control-label">자동/수동</label>
                                                      <select name="manual_flag" id="manual_flag" class="form-control">
														<option value=""
															${search.manual_flag == '' ? 'selected="selected"' : ''}>
															----- 전체 -----</option>
														<option value="Y" <c:if test="${search.manual_flag eq 'Y' }">selected="selected"</c:if>>수동관리</option>
														<option value="N" <c:if test="${search.manual_flag eq 'N' }">selected="selected"</c:if>>자동관리</option>
													 </select>
                                                  </div>
                                              </div>
                                          </div>
                                          <div align="right">
                                           <button type="reset"
											class="btn btn-sm red-mint btn-outline sbold uppercase"
											onclick="resetOptions(accessAuthListConfig['listUrl'])">
											<i class="fa fa-remove"></i> <font>초기화
										</button>
										<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
											onclick="searchEmpUserList()">
											<i class="fa fa-search"></i> 검색
										</button>&nbsp;&nbsp;
										<div class="btn-group">
											<a href="javascript:;" data-toggle="dropdown">
												<img src="${rootPath}/resources/image/icon/XLS_3.png">
											</a>
											<ul class="dropdown-menu pull-right">
												<li>
													<a data-toggle="modal" data-target="#myModal">업로드</a>
												</li>
												<li><a onclick="excelEmpUserList()"> 다운로드 </a></li>
											</ul>
										</div>
									</div>
									
									<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id}" /> 
									<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id}" /> 
									<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
									<input type="hidden" name="page_num" value="${search.page_num}" />
									<input type="hidden" name="isSearch" value="${paramBean.isSearch }" />
                                      </form>
                                  </div>
                              </div>
                          </div>
                      </div>
				</div>
				<!-- 				</div> -->

				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-6">
							<div class="btn-group">
								<button type="button" class="btn btn-sm blue btn-outline sbold uppercase" onclick="findAccessAuthDetail(-1)">
									<i class="fa fa-plus"></i> 신규 
								</button>
							</div>
						</div>
					</div>
				</div>

				<table style="border-top: 1px solid #e7ecf1"
						class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
						style="position: absolute; top: 0px; left: 0px; width: 100%;">

					<thead>
						<tr role="row" class="heading">
							<th width="3%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">번호</th>
							<th width="8%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">시스템</th>
							<th width="7%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">사용자명</th>
							<th width="7%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">사용자ID</th>
							<th width="7%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">관리자IP</th>
							<th width="7%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">관리자명</th>
							<th width="7%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">관리자ID</th>
							<th width="7%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">소속</th>
							<th width="7%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">접근권한</th>
							<th width="8%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">권한변경일</th>
							<th width="7%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">처리내용</th>
							<th width="9%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">등록/변경사유</th>
							<th width="7%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">자동/수동</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${empty empUserList}">
								<tr>
									<td colspan="14" align="center">데이터가 없습니다.</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:set value="${search.total_count}" var="count" />
								<c:forEach items="${empUserList}" var="empUser" varStatus="status">
									<tr style="cursor: pointer;" onclick="findAccessAuthDetail('${empUser.seq}')">
										<td style="text-align: center;"><c:out value="${empUser.seq}"/></td>
										<td style="text-align: center;"><c:out value="${empUser.system_name}"/></td>
										<td style="text-align: center;"><c:out value="${empUser.emp_user_name}"/></td>
										<td style="text-align: center;"><c:out value="${empUser.emp_user_id}"/></td>
										<td style="text-align: center;"><c:out value="${empUser.approver_ip}"/></td>
										<td style="text-align: center;"><c:out value="${empUser.approver}"/></td>
										<td style="text-align: center;"><c:out value="${empUser.approver_id}"/></td>
										<td style="text-align: center;"><c:out value="${empUser.dept_name}"/></td>
										<td style="text-align: center;">
											<c:choose>
												<c:when test="${empUser.status eq 'W' }">있음</c:when>
												<c:otherwise>없음</c:otherwise>
											</c:choose>
										</td>
										<td style="text-align: center;">
											<c:choose>
<%-- 												<c:when test="${empty empUser.change_date }"> --%>
												<c:when test="${empty empUser.update_date }">
												-
												</c:when>
												<c:otherwise>
<%-- 													<fmt:parseDate value="${empUser.change_date}" pattern="yyyyMMdd" var="change_date" /> --%>
													<fmt:parseDate value="${empUser.update_date}" pattern="yyyyMMddHHmmss" var="change_date" /> 
													<fmt:formatDate value="${change_date}" pattern="yyyy-MM-dd" />
												</c:otherwise>
											</c:choose>
										</td>
										<td style="text-align: center;">
											<c:choose>
											<c:when test="${fn:length(empUser.process_content) > 17}">
												${fn:substring(empUser.process_content, 0, 17)}...
											</c:when>
											<c:otherwise>
												<ctl:nullCv nullCheck="${empUser.process_content}"/>
											</c:otherwise>
											</c:choose>
										</td>
										<td style="text-align: center;">
											<c:choose>
											<c:when test="${fn:length(empUser.reason) > 17}">
												${fn:substring(empUser.reason, 0, 17)}...
											</c:when>
											<c:otherwise>
												<ctl:nullCv nullCheck="${empUser.reason}"/>
											</c:otherwise>
											</c:choose>
										</td>
										<td style="text-align: center;">
											<c:choose>
												<c:when test="${empUser.manual_flag eq 'Y' }">수동관리</c:when>
												<c:when test="${empUser.manual_flag eq 'N' }">자동관리</c:when>
											</c:choose>
										</td>
									</tr>
									<c:set var="count" value="${count - 1 }" />
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
				<div class="dataTables_processing DTS_Loading"
					style="display: none;">Please wait ...</div>
				<!-- 				</div> -->
				<!-- 페이징 영역 -->
				<c:if test="${search.total_count > 0}">
					<div class="page left" id="pagingframe" align="center">
						<p>
							<ctl:paginator currentPage="${search.page_num}"
								rowBlockCount="${search.size}"
								totalRowCount="${search.total_count}" />
						</p>
					</div>
				</c:if>
			</div>
		</div>

	</div>
</div>

<form id="detailForm" method="POST">
	<input type="hidden" name="seq" /> 
	<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" /> 
	<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
</form>

<!-- 엑셀양식 다운로드 -->
<form action="exdownload.html" method="post" id="exdown" name="exdown"
	enctype="multipart/form-data"></form>
<script type="text/javascript">
	var accessAuthListConfig = {
		"listUrl" : "${rootPath}/accessAuth/list.html",
		"detailUrl" : "${rootPath}/accessAuth/detail.html",
		"downloadUrl" : "${rootPath}/accessAuth/download.html",
		"uploadUrl" : "${rootPath}/accessAuth/upload.html",
		"loginPage" : "${rootPath}/loginView.html"
	};
</script>