<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<c:set var="adminUserId" value="${userSession.admin_user_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/system_management/accessAuthList.js"type="text/javascript" charset="UTF-8"></script>


<!-- 권한관리 신규 -->
<h1 class="page-title">
	${currentMenuName}
</h1>
<div class="portlet light portlet-fit bordered">
	<div class="portlet-body">
		<!-- BEGIN FORM-->
		<form id="AccessAuthDetailForm" method="POST"
			class="form-horizontal form-bordered form-row-stripped">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<th style="text-align: center;">
							<label class="control-label">관리자명<span class="required">*</span></label>
						</th>
						<td style="vertical-align: middle;" class="form-group form-md-line-input">
							<input type="text" class="form-control" name="approver" value="${accessAuth.approver }">
							<div class="form-control-focus"></div>
						</td>
						<th style="text-align: center;">
							<label class="control-label">관리자ID</label>
						</th>
						<td style="vertical-align: middle;" class="form-group form-md-line-input">
							<input type="text" class="form-control" name="approver_id" value="${accessAuth.approver_id }">
							<div class="form-control-focus"></div>
						</td>
					</tr>
					<tr>
						<th style="text-align: center;">
							<label class="control-label">관리자IP</label>
						</th>
						<td style="vertical-align: middle;" class="form-group form-md-line-input">
							<input type="text" class="form-control" name="approver_ip" value="${accessAuth.approver_ip }">
							<div class="form-control-focus"></div>
						</td>
						<th style="text-align: center;">
							<label class="control-label">접근권한<span class="required">*</span></label>
						</th>
						<td style="vertical-align: middle;" class="form-group form-md-line-input">
							<select name="status" class="form-control">
								<option value="W" <c:if test="${accessAuth.status eq 'W' }">selected="selected"</c:if>>있음</option>
								<%-- <option value="L" <c:if test="${accessAuth.status eq 'L' }">selected="selected"</c:if>>휴직</option> --%>
								<option value="R" <c:if test="${accessAuth.status eq 'R' }">selected="selected"</c:if>>없음</option>
							</select>
						</td>
					</tr>
					<tr>
						<th style="width: 15%; text-align: center;">
							<label class="control-label">사용자명<span class="required">*</span></label>
						</th>
						<td style="width: 35%; vertical-align: middle;" class="form-group form-md-line-input">
							<c:choose>
							<c:when test="${!empty accessAuth }">
								<c:out value="${accessAuth.emp_user_name}" />
								<input type="hidden" name="emp_user_name" value="${accessAuth.emp_user_name}" />
								<input type="hidden" name="seq" value="${accessAuth.seq}">
							</c:when>
							<c:otherwise>
								<input type="text" class="form-control" name="emp_user_name">
								<div class="form-control-focus"></div>
							</c:otherwise>
							</c:choose>
						</td>
						<th style="width: 15%; text-align: center;">
							<label class="control-label">시스템<span class="required">*</span></label>
						</th>
						<td style="width: 35%; vertical-align: middle;" class="form-group form-md-line-input">
							<c:choose>
							<c:when test="${!empty accessAuth }">
								<c:out value="${accessAuth.system_name}" />
								<input type="hidden" name="system_seq" value="${accessAuth.system_seq}" />
							</c:when>
							<c:otherwise>
								<select name="system_seq" class="form-control">
								<c:forEach items="${systemList }" var="system">
									<option value="${system.system_seq }">${system.system_name }</option>
								</c:forEach>
								</select>
							</c:otherwise>
							</c:choose>
						</td>
					</tr>
					<tr>
						<th style="text-align: center;">
							<label class="control-label">사용자ID<span class="required">*</span></label>
						</th>
						<td style="vertical-align: middle;" class="form-group form-md-line-input">
							<c:choose>
							<c:when test="${!empty accessAuth }">
								<c:out value="${accessAuth.emp_user_id}" />
								<input type="hidden" name="emp_user_id" value="${accessAuth.emp_user_id}" />
							</c:when>
							<c:otherwise>
								<input type="text" class="form-control" name="emp_user_id">
								<div class="form-control-focus"></div>
							</c:otherwise>
							</c:choose>
						</td>
						<th style="text-align: center;"><label class="control-label">소속명</label></th>
						<td class="form-group form-md-line-input">
							<select name="dept_id" class="ticket-assign form-control input-medium">
								<option value="">소속 없음</option>
								<c:forEach items="${departments }" var="department">
								<option value="${department.dept_id }"
									${department.dept_id == accessAuth.dept_id ? 'selected' : ''}><c:out
										value="${department.simple_dept_name }" /></option>
								</c:forEach>
							</select>
						</td>
					</tr>
						<c:if test="${!empty accessAuth }"><input type="hidden" name="temp_status" value="${accessAuth.status}" /></c:if>

					<c:if test="${!empty accessAuth }"> 
					<%-- <tr>
						<th style="text-align: center;">
							<label class="control-label"">권한부여일자 <c:if test="${!empty accessAuth.change_date }">(권한변경일자)</c:if></label>
						</th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<div class="form-control form-control-static">
								<fmt:parseDate value="${accessAuth.access_date}" pattern="yyyyMMdd" var="access_date" /> 
								<fmt:formatDate value="${access_date}" pattern="yyyy-MM-dd" />
								<c:if test="${!empty accessAuth.change_date }">
									<fmt:parseDate value="${accessAuth.change_date}" pattern="yyyyMMdd" var="change_date" /> 
									&nbsp;(<fmt:formatDate value="${change_date}" pattern="yyyy-MM-dd" />)
							</c:if>
							</div>
						</td>
						<th style="text-align: center;">
							<label class="control-label"">권한해제일자</label>
						</th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<div class="form-control form-control-static">
								<fmt:parseDate value="${accessAuth.expire_date}" pattern="yyyyMMdd" var="expire_date" /> 
								<fmt:formatDate value="${expire_date}" pattern="yyyy-MM-dd" />
							</div>
						</td>
					</tr> --%>
					<%-- <tr>
						<th style="text-align: center;">
							<label class="control-label"">권한변동일자</label>
						</th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<div class="form-control form-control-static">
								<fmt:parseDate value="${accessAuth.update_date}" pattern="yyyyMMddHHmmss" var="update_date" /> 
								<fmt:formatDate value="${update_date}" pattern="yyyy-MM-dd HH:mm:ss" />
							</div>
						</td>
					</tr> --%>
					<input type="hidden" name="access_date" value="${accessAuth.access_date }" />
					<input type="hidden" name="change_date" value="${accessAuth.change_date }" />
					<input type="hidden" name="expire_date" value="${accessAuth.expire_date }" />
					</c:if>
					<tr>
						<th style="text-align: center;">
							<label class="control-label">수동관리여부<span class="required">*</span></label>
						</th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<select name="manual_flag" class="form-control">
								<option value="Y" <c:if test="${accessAuth.manual_flag eq 'Y' }">selected="selected"</c:if>>수동관리</option>
								<option value="N" <c:if test="${accessAuth.manual_flag eq 'N' }">selected="selected"</c:if>>자동관리</option>
							</select>
						</td>
						<th style="text-align: center; vertical-align: middle;">
							<label class="control-label">등록/변경사유</label>
						</th>
						<td style="vertical-align: middle;" class="form-group form-md-line-input">
							<textarea class="form-control" name="reason" rows="2" cols="60">${accessAuth.reason }</textarea>
							<div class="form-control-focus"></div>
						</td>
					</tr>
					<tr>
						<th style="text-align: center; vertical-align: middle;">
							<label class="control-label">처리내용</label>
						</th>
						<td colspan="3" style="vertical-align: middle;" class="form-group form-md-line-input">
							<textarea class="form-control" name="process_content" rows="2" cols="60">${accessAuth.process_content }</textarea>
							<div class="form-control-focus"></div>
						</td>
					</tr>
					<c:if test="${!empty accessAuth }">
					<tr>
						<th style="text-align: center;">
							<label class="control-label"">권한부여일자 </label>
						</th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<div class="form-control form-control-static">
								<fmt:parseDate value="${accessAuth.access_date}" pattern="yyyyMMdd" var="access_date" />
								<fmt:formatDate value="${access_date}" pattern="yyyy-MM-dd" />
							</div>
						</td>
						<th style="text-align: center;">
							<label class="control-label"">권한변경일자</label>
						</th>
						<td class="form-group form-md-line-input" style="vertical-align: middle;">
							<div class="form-control form-control-static">
								<fmt:parseDate value="${accessAuth.change_date}" pattern="yyyyMMdd" var="change_date" /> 
								<fmt:formatDate value="${change_date}" pattern="yyyy-MM-dd" />
							</div>
						</td>
					</tr>
					</c:if>
				</tbody>
			</table>
			
			<div class="form-actions">
			<br>
				<div class="row">
					<div class="col-md-offset-2 col-md-10" align="right"
						style="padding-right: 30px;">
						<button type="button"
							class="btn btn-sm grey-mint btn-outline sbold uppercase"
							onclick="goList()">
							<i class="fa fa-list"></i> 목록
						</button>
						<c:choose>
							<c:when test="${empty accessAuth}">
								<button type="button"
									class="btn btn-sm blue btn-outline sbold uppercase"
									onclick="addAccessAuth()">
									<i class="fa fa-check"></i> 등록
								</button>
							</c:when>
							<c:otherwise>
								<button type="button"
									class="btn btn-sm blue btn-outline sbold uppercase"
									onclick="saveAccessAuth()">
									<i class="fa fa-check"></i> 수정
								</button>
								<c:if test="${adminUserId != adminUserDetail.admin_user_id}">
									<button type="button"
									class="btn btn-sm red-mint btn-outline sbold uppercase"
									onclick="removeAccessAuth()">
									<i class="fa fa-close"></i> 삭제
									</button>
								</c:if>
							
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
			<!-- END F -->
		</form>

		
		<!-- END FORM-->
	</div>
</div>

<form id="menuForm" method="POST">
	<input type="hidden" name="page_num" value="1"/>
	<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" /> 
	<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
	<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
</form>

<script type="text/javascript">

	var accessAuthListConfig = {
		"listUrl":"${rootPath}/accessAuth/list.html"
		,"addUrl":"${rootPath}/accessAuth/add.html"
		,"saveUrl":"${rootPath}/accessAuth/save.html"
		,"removeUrl":"${rootPath}/accessAuth/remove.html"
		,"loginPage" : "${rootPath}/loginView.html"
	};
	
</script>

