<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script
	src="${rootPath}/resources/js/psm/system_management/reportOption.js"
	type="text/javascript" charset="UTF-8"></script>
	

<div class="portlet light portlet-datatable bordered">
	<div class="caption">
		<i class="icon-settings font-dark"></i> <span
			class="caption-subject font-dark sbold uppercase">로고 이미지 등록</span>
	</div>
	<div class="portlet-body">

		<table class="table table-striped table-bordered order-column">
			<form id="reportOptionForm"
				action="${rootPath}/reportOption/upload.html"
				enctype="multipart/form-data" method="POST" accept-charset="UTF-8"
				onsubmit="return check_1()">
				<tr>
					<td width="30%" style="text-align: center; vertical-align: middle;">
						로고 이미지 등록</td>
					<td width="70%"
						style="text-align: left; white-space: nowrap; vertical-align: middle;">
						<input type="file" name="filename" accept=".gif, .jpg, .png"
						style="display: inline;" /> <input type="submit"
						class="btn btn-sm blue btn-outline sbold uppercase" value="등록" />
					</td>
				</tr>
			</form>
		</table>

	</div>

	<div class="caption">
		<i class="icon-settings font-dark"></i> <span
			class="caption-subject font-dark sbold uppercase">로고 이미지 목록</span>
	</div>
	<div>
		<table style="border-top: 1px solid #e7ecf1"
			class="table table-bordered"
			style="position: absolute; top: 0px; left: 0px; width: 100%;">
			<thead>
				<th width="5%"
					style="border-bottom: 1px solid #e7ecf1; text-align: center;">
					No.</th>
				<th width="25%"
					style="border-bottom: 1px solid #e7ecf1; text-align: center;">
					등록일시</th>
				<th width="30%"
					style="border-bottom: 1px solid #e7ecf1; text-align: center;">
					파일 이름</th>
				<th width="10%"
					style="border-bottom: 1px solid #e7ecf1; text-align: center;">
					사용여부</th>
				<th width="10%"
					style="border-bottom: 1px solid #e7ecf1; text-align: center;">
					삭제</th>
				<th width="20%"
					style="border-bottom: 1px solid #e7ecf1; text-align: center;">
					이미지</th>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty image_logo_list}">
						<tr>
							<td colspan="6" align="center">데이터가 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach items="${image_logo_list }" var="image"
							varStatus="status">
							<tr>
								<td style="text-align: center; vertical-align: middle;">${status.count }</td>
								<td style="text-align: center; vertical-align: middle;"><fmt:parseDate
										value="${image.reg_date}" pattern="yyyyMMdd" var="reg_date" />
									<fmt:formatDate value="${reg_date}" pattern="YYYY-MM-dd" /></td>
								<td style="text-align: center; vertical-align: middle;">${image.origin_name }</td>
								<td style="text-align: center; vertical-align: middle;"><c:if
										test="${fn:trim(image.use) eq 'Y'}">
										<span class="btn btn-xs blue btn-outline"
											onclick="useLogoImage(${image.idx}, 1)">사용</span>
									</c:if> <c:if test="${fn:trim(image.use) eq 'N'}">
										<span class="btn btn-xs yellow btn-outline"
											onclick="useLogoImage(${image.idx}, 2)">미사용</span>
									</c:if></td>

								<td style="text-align: center; vertical-align: middle;">
									<p class="btn btn-xs red btn-outline filter-cancel"
										onclick="deleteLogoImage(${image.idx})">삭제</p>
								</td>
								<td style="vertical-align: middle; text-align: center;">
									<img src="${pageContext.servletContext.contextPath}/resources/upload/${image.stored_name}" width="70" height="70">
								</td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>
</div>


<div class="portlet light portlet-datatable bordered">

	<div class="caption">
		<i class="icon-settings font-dark"></i> <span
			class="caption-subject font-dark sbold uppercase">점검보고서 로고 이미지
			등록 </span>
	</div>

	<div class="portlet-body">

		<table class="table table-striped table-bordered order-column">
			<form id="reportOptionForm2"
				action="${rootPath}/reportOption/upload2.html"
				enctype="multipart/form-data" method="POST" accept-charset="UTF-8"
				onsubmit="return check_2()">
				<tr>
					<td width="30%" style="text-align: center; vertical-align: middle;">
						점검보고서 로고 이미지 등록</td>
					<td width="70%"
						style="text-align: left; white-space: nowrap; vertical-align: middle;">
						<input type="file" name="filename2" accept=".gif, .jpg, .png"
						style="display: inline;" /> <input type="submit"
						class="btn btn-sm blue btn-outline sbold uppercase" value="등록" on />
					</td>
				</tr>
			</form>
		</table>

	</div>

	<div class="caption">
		<i class="icon-settings font-dark"></i> <span
			class="caption-subject font-dark sbold uppercase">점검보고서 로고 이미지
			목록</span>
	</div>
	<div>
		<table style="border-top: 1px solid #e7ecf1"
			class="table table-bordered"
			style="position: absolute; top: 0px; left: 0px; width: 100%;">
			<thead>
				<th width="5%"
					style="border-bottom: 1px solid #e7ecf1; text-align: center;">
					No.</th>
				<th width="25%"
					style="border-bottom: 1px solid #e7ecf1; text-align: center;">
					등록일시</th>
				<th width="30%"
					style="border-bottom: 1px solid #e7ecf1; text-align: center;">
					파일 이름</th>
				<th width="10%"
					style="border-bottom: 1px solid #e7ecf1; text-align: center;">
					사용여부</th>
				<th width="10%"
					style="border-bottom: 1px solid #e7ecf1; text-align: center;">
					삭제</th>
				<th width="20%"
					style="border-bottom: 1px solid #e7ecf1; text-align: center;">
					이미지</th>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${empty image_logo_list2}">
						<tr>
							<td colspan="6" align="center">데이터가 없습니다.</td>
						</tr>
					</c:when>
					<c:otherwise>
						<c:forEach items="${image_logo_list2 }" var="image"	varStatus="status">
							<tr>
								<td style="text-align: center; vertical-align: middle;">${status.count }</td>
								<td style="text-align: center; vertical-align: middle;"><fmt:parseDate
										value="${image.reg_date}" pattern="yyyyMMdd" var="reg_date" />
									<fmt:formatDate value="${reg_date}" pattern="YYYY-MM-dd" /></td>
								<td style="text-align: center; vertical-align: middle;">${image.origin_name }</td>
								<td style="text-align: center; vertical-align: middle;"><c:if
										test="${fn:trim(image.use) eq 'Y'}">
										<span class="btn btn-xs blue btn-outline"
											onclick="useLogoImage2(${image.idx}, 1)">사용</span>
									</c:if> <c:if test="${fn:trim(image.use) eq 'N'}">
										<span class="btn btn-xs yellow btn-outline"
											onclick="useLogoImage2(${image.idx}, 2)">미사용</span>
									</c:if></td>
								<td style="text-align: center; vertical-align: middle;">
									<p class="btn btn-xs red btn-outline filter-cancel"
										onclick="deleteLogoImage(${image.idx})">삭제</p>
								</td>
								<td style="vertical-align: middle; text-align: center;">
								<img src="${pageContext.servletContext.contextPath}/resources/upload/${image.stored_name}" width="70" height="70"></td>
							</tr>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
	</div>

</div>

<form id="listForm" method="POST">
	<input type="hidden" name="tab_flag" value="2"/>
	<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
	<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" /> 
	<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
</form>

<script type="text/javascript">
	
	var reportOptionConfig = {
			"listUrl":"${rootPath}/reportOption/list.html",
			"addUrl":"${rootPath}/reportOption/upload.html"
			
	};
	
	$(document).ready(function(){
		findDefaultDesc();
	});
	
</script>
