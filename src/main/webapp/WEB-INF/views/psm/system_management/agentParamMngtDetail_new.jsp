<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="agentParamAuthId" value="${userSession.auth_id}" />
<c:set var="agentParamId" value="${userSession.admin_user_id}" />
<c:set var="currentMenuId" value="${index_id }" />

<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/system_management/agentParamMngtList.js" type="text/javascript" charset="UTF-8"></script>


<!-- 권한관리 신규 -->
<h1 class="page-title">
	${currentMenuName}
</h1>
<div class="portlet light portlet-fit bordered">
	<div class="portlet-body">
		<!-- BEGIN FORM-->
		<form id="agentParamDetailForm" method="POST" class="form-horizontal form-bordered form-row-stripped">
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<th style="width: 20%; text-align: center;">
							<label class="control-label" for="form_control_2">명칭</label>
						</th>
						<th style="width: 50%; text-align: center;">
							<label class="control-label" for="form_control_2">값</label>
						</th>
						<th style="width: 30%; text-align: center;">
							<label class="control-label" for="form_control_2">설명</label>
						</th>
					</tr>
					<c:forEach var="agentParam" items="${agentParamDetail }" varStatus="status">
						<c:choose>
							<c:when test="${agentParam.param_name eq 'alarm.request.htmlbody' || agentParam.param_name eq 'alarm.response.htmlbody'}">
								<tr>
									<td style="text-align: center;vertical-align: middle;">${agentParam.param_name }</td>
									<td style="text-align: center;">
										<textarea style="width: 100%; height: 150px;" id="paramValue${status.count }" wrap="hard"><c:out value="${agentParam.param_value}"/></textarea>
									</td>
									<td style="vertical-align: middle;">${agentParam.param_desc }</td>
								</tr>
							</c:when>
							<c:otherwise>
								<tr>
									<td style="text-align: center;vertical-align: middle;">${agentParam.param_name }</td>
									<td style="text-align: center;"><input class="form-control" type="text" id="paramValue${status.count }"
										 value="${agentParam.param_value}"></td>
									<td style="vertical-align: middle;">${agentParam.param_desc }</td>
								</tr>
							</c:otherwise>
						</c:choose>
						
					</c:forEach>
				</tbody>
			</table>
			<input type="hidden" name="seqArray" value="${seqArray }"/>
			<input type="hidden" id="valueArray" name="valueArray" value=""/>
			<input type="hidden" id="seqLength" name="seqLength" value="${seqLength }"/>
			<div class="form-actions">
			<br>
				<div class="row">
					<div class="col-md-offset-2 col-md-10" align="right"
						style="padding-right: 30px;">
						<button type="button" class="btn btn-sm grey-mint btn-outline sbold uppercase" onclick="moveAgentParamList()">
							<i class="fa fa-list"></i> 목록
						</button>
						<button type="button" class="btn btn-sm blue btn-outline sbold uppercase" onclick="saveAgentParam()">
							<i class="fa fa-check"></i> 수정
						</button>
					</div>
				</div>
			</div>
			<!-- END F -->
		</form>

		<form id="agentParamListForm" method="POST">
			<!-- 메뉴 관련 input 시작 -->
			<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" />
			<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
			<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
			<!-- 메뉴 관련 input 끝 -->

			<!-- 검색조건 관련 input 시작 -->

			<!-- 검색조건 관련 input 끝 -->
		</form>
		<!-- END FORM-->
	</div>
</div>

<script type="text/javascript">
	var agentParamListConfig = {
		"listUrl" : "${rootPath}/agentParamMngt/list.html"
		,"detailUrl" : "${rootPath}/agentParamMngt/detail.html"
		,"saveUrl" : "${rootPath}/agentParamMngt/save.html"
		,"loginPage" : "${rootPath}/loginView.html"
	};
</script>

