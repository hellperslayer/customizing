<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<c:set var="currentMenuId" value="${index_id}" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script
	src="${rootPath}/resources/js/psm/system_management/ipPermList.js"
	type="text/javascript" charset="UTF-8"></script>

<h1 class="page-title">${currentMenuName}</h1>
<div class="portlet light portlet-fit portlet-datatable bordered">
	<!-- <div class="portlet-title">
		<div class="caption">
			<i class="icon-settings font-dark"></i> <span
				class="caption-subject font-dark sbold uppercase">접속허용IP 리스트</span>
		</div>
	</div> -->
	<div class="portlet-body">
		<div class="table-container">

			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-6">
							<div class="btn-group">
								<button type="button" class="btn btn-sm blue btn-outline sbold uppercase" onclick="ipDetail()">
									<i class="fa fa-plus"></i> 신규 
								</button>
							</div>
						</div>
					</div>
				</div>
				<table style="border-top: 1px solid #e7ecf1"
					class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
					id="datatable_ajax_2" aria-describedby="datatable_ajax_2_info"
					role="grid">
					<thead>
						<tr role="row" class="heading">
							<th width="5%" style="border-bottom: 1px solid #e7ecf1;text-align: center;">
								번호</th>
							<th width="30%" style="border-bottom: 1px solid #e7ecf1;text-align: center;">
								관리자 ID</th>
							<th width="50%" style="border-bottom: 1px solid #e7ecf1;text-align: center;">
								IP대역</th>
							<th width="15%" style="border-bottom: 1px solid #e7ecf1;text-align: center;">
								사용여부</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${empty ips}">
								<tr>
									<td colspan="4" align="center">데이터가 없습니다.</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:forEach items="${ips}" var="ips" varStatus="status">
									<tr onclick="ipDetail(${ips.ip_seq});" style="cursor: pointer;">
										<td><c:out value="${ips.ip_seq}"/></td>
										<td style="text-align: center;"><c:out value="${ips.admin_id}"/></td>
										<td><c:out value="${ips.ip_content}"/></td>
										<td style="vertical-align: middle;text-align: center;"><c:choose>
												<c:when test="${ips.use_flag == 'Y'}">
													<span class="label label-sm label-success"> 사용 </span>
												</c:when>
												<c:otherwise>
													<span class="label label-sm label-warning"> 미사용 </span>
												</c:otherwise>
											</c:choose></td>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
				<div class="dataTables_processing DTS_Loading"
					style="display: none;">Please wait ...</div>
				<!-- 페이징 영역 -->
				<c:if test="${search.total_count > 0}">
					<div class="page left" id="pagingframe" align="center">
						<p>
							<ctl:paginator currentPage="${search.page_num}"
								rowBlockCount="${search.size}"
								totalRowCount="${search.total_count}" />
						</p>
					</div>
				</c:if>
			</div>
		</div>
	</div>
</div>
<form id="ipPermForm" method="POST">
	<input type="hidden" name="main_menu_id"
		value="${paramBean.main_menu_id}" /> <input type="hidden"
		name="sub_menu_id" value="${paramBean.sub_menu_id}" /> <input
		type="hidden" name="current_menu_id" value="${currentMenuId}" /> <input
		type="hidden" name="ip_seq" value="" /> <input type="hidden"
		name="page_num" />
</form>
<script type="text/javascript">
	var ipPermConfig = {
		"ipPermListUrl":"${rootPath}/ipPerm/list.html",
		"ipPermDetailUrl":"${rootPath}/ipPerm/detail.html"
	};
	
</script>