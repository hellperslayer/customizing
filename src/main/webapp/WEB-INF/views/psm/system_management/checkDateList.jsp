<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>
<c:set var="currentMenuId" value="${index_id}" />
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/system_management/ipPermDetail.js" type="text/javascript" charset="UTF-8"></script>

<div class="contents left">
	<div id="labelDiv">
		<label class="action_label"><span class="option_name">${currentMenuName}</span></label>
	</div>
	
	<form id="checkDateForm" method="POST">
		
		<table class="time_td">
			<tr >
				<th>유지보수 만료 날짜</th>
				<td>				
					<p style="font-size:15px; font-weight:bold;">${fn:substring(checkDate,0,4)}년 ${fn:substring(checkDate,4,6)}월 ${fn:substring(checkDate,6,8)}일에 만료됩니다.</p>
				</td>
			</tr>			
		</table>
	</form>
	
	<form id="dateForm" method="POST">
		<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id}" />
		<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id}" />
		<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
	</form>
	
	<!-- 버튼 영역 -->
<!-- 	<div class="option_btn right">
		<p class="right">
			<a class="btn gray" onclick="saveIpPerm()">수정</a>
		</p>
	</div> -->
</div>

<script type="text/javascript">
	
	var ipPermConfig = {
		"listUrl":"${rootPath}/ipPerm/list.html"
		,"addUrl":rootPath + "/ipPerm/add.html"
		,"saveUrl":rootPath + "/ipPerm/save.html"
		,"removeUrl":rootPath + "/ipPerm/remove.html"
		,"loginPage" : "${rootPath}/loginView.html"
	};
	
</script>
