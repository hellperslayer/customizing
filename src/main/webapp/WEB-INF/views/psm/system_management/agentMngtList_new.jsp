<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/system_management/agentMngtList.js" type="text/javascript" charset="UTF-8"></script>
 
 
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> ${currentMenuName}
    <!--bold font-blue-hoki <small>basic bootstrap tables with various options and styles</small> -->
</h1>
<!-- END PAGE TITLE-->

<!-- END PAGE HEADER-->

<form id="agentListForm" method="POST">
	<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" />
	<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
	<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
	<input type="hidden" name="page_num" value="${search.page_num}" />
	<input type="hidden" name="agent_seq"/>
</form>
	
<div class="row">
	<div class="col-md-12">
	    <!-- BEGIN EXAMPLE TABLE PORTLET-->
	    <!-- Begin: Demo Datatable 2 -->
	    <div class="portlet light portlet-fit portlet-datatable bordered">
	        <div class="portlet-title">
	            <div class="btn-group">
		            <img src="${rootPath}/resources/image/psm/system_management/status_play.gif" alt="정상" style="width:25px;height:25px;"/>정상 
					<img src="${rootPath}/resources/image/psm/system_management/status_stop.gif" alt="정지" style="width:25px;height:25px;"/> 정지
					<img src="${rootPath}/resources/image/psm/system_management/status_unusual.gif" alt="비정상" style="width:25px;height:25px;"/> 비정상
					<span style="padding-left:25px;">[ 페이지 로딩 시 서버의 에이전트 상태를 체크하여 각 에이전트 상태를 출력합니다. ]  </span>
					<span><i class="btn icon-settings font-dark blue btn-outline" onclick="mailFormYn()"> 메일서버</i></span>
					<span><i class="btn icon-settings font-dark blue btn-outline" onclick="licenseFormYn()"> 라이센스</i></span>  
			    </div>
	        </div>
	        
	        <div class="">
				<div class="col-md-12" align="left">
					<a class="btn btn-sm blue btn-outline sbold uppercase"
						onclick="goAgentDetail(null)">신규 <i class="fa fa-plus"></i>
					</a>
				</div>
			</div>
	        <div class="portlet-body">
                <div class="table-container">
                    
                    <div id="datatable_ajax_2_wrapper" class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
	                    
<!-- 	                    <div class="dataTables_scrollBody" style="position: relative; overflow: auto; height: 320px; width: 100%;"> -->
			                <table style="border-top:1px solid #e7ecf1" class="table table-striped table-bordered table-hover table-checkable dataTable no-footer" id="datatable_ajax_2" aria-describedby="datatable_ajax_2_info" role="grid" style="position: absolute; top: 0px; left: 0px; width: 100%;">
			              		 
			              		 <thead>
			              		 	<tr role="row" class="heading" style="background-color:#c0bebe;"> 
			                            <th width="5%" style="border-bottom:1px solid #e7ecf1;text-align: center;"> 상태 </th>      
			                            <th width="6%" style="border-bottom:1px solid #e7ecf1;text-align: center;"> 에이전트ID </th>                              
			                            <th width="6%" style="border-bottom:1px solid #e7ecf1;text-align: center;"> 에이전트명 </th>
			                            <th width="6%" style="border-bottom:1px solid #e7ecf1;text-align: center;"> 대상시스템 </th>
			                            <th width="6%" style="border-bottom:1px solid #e7ecf1;text-align: center;"> 수집서버명 </th>                              
			                            <th width="6%" style="border-bottom:1px solid #e7ecf1;text-align: center;"> 수집서버IP </th>
			                            <th width="7%" style="border-bottom:1px solid #e7ecf1;text-align: center;"> 동작위치(IP) </th>
			                            <th width="7%" style="border-bottom:1px solid #e7ecf1;text-align: center;"> 접속기록로그 </th>
			                            <th width="7%" style="border-bottom:1px solid #e7ecf1;text-align: center;"> 다운로드로그 </th>
			                            <th width="9%" style="border-bottom:1px solid #e7ecf1;text-align: center;"> 시스템로그</th>
			                            <th width="10%" style="border-bottom:1px solid #e7ecf1;text-align: center;"> 마지막 동작 변경시간 </th>
			                            <th width="10%" style="border-bottom:1px solid #e7ecf1;text-align: center;"> 로그생성방식 </th>
			                            <!-- <th width="10%" style="border-bottom:1px solid #e7ecf1;text-align: center;"> 설명 </th> -->
			                            <th width="10%" style="border-bottom:1px solid #e7ecf1;text-align: center;"> 동작 </th>
			                        </tr>
			              		 </thead>
			              		 <tbody>
			              		 	<c:choose>
										<c:when test="${empty agentList}">
											<tr>
								        		<td colspan="7" align="center">데이터가 없습니다.</td>
								        	</tr>
										</c:when>
										<c:otherwise>
											<c:set value="${search.total_count}" var="count"/> 
											<c:forEach items="${agentList}" var="agent" varStatus="status">
												<tr>
													<td class="imageTd" align="center">
														<c:choose>
															<c:when test="${agent.status == 'false' }">
																<img class="statusImg" title="정지 상태 입니다" src="${rootPath}/resources/image/psm/system_management/status_stop.gif" />
															</c:when>
															<c:when test="${agent.status == 'use' }">
																<img class="statusImg" title="동작 상태 입니다" src="${rootPath}/resources/image/psm/system_management/status_play.gif" />
															</c:when>
															<c:otherwise>
																<img class="statusImg" title="비정상 상태 입니다" src="${rootPath}/resources/image/psm/system_management/status_unusual.gif" />
															</c:otherwise>
														</c:choose>
													</td>
													<td style="text-align: center;">${agent.agent_seq}</td>
													<td style="padding-left: 20px;cursor: pointer;" onclick="goAgentDetail('${agent.agent_seq}')">${agent.agent_name}</td>
													<td style="padding-left: 20px;cursor: pointer;" onclick="goAgentDetail('${agent.agent_seq}')">${agent.system_name}</td>
													<td style="padding-left: 20px;cursor: pointer;" onclick="goAgentDetail('${agent.agent_seq}')">${agent.manager_name}</td>
													<td style="padding-left: 20px;cursor: pointer;" onclick="goAgentDetail('${agent.agent_seq}')">${agent.manager_ip}</td>
													<td style="text-align: center;">${agent.ip}</td>
													<td style="padding-left: 20px;cursor: pointer;" onclick="goAgentDetail('${agent.agent_seq}')">${agent.ba_logcnt}</td>
													<td style="padding-left: 20px;cursor: pointer;" onclick="goAgentDetail('${agent.agent_seq}')">${agent.dn_logcnt}</td>
													<td style="padding-left: 20px;cursor: pointer;" onclick="goAgentDetail('${agent.agent_seq}')">${agent.bs_logcnt}</td>
													<td style="text-align: center;"><fmt:formatDate value="${agent.command_datetime}" pattern="yyyy-MM-dd kk:mm:ss"/></td>
													<%-- <td style="padding-left: 20px;">${agent.description}</td> --%>
													<td style="padding-left: 20px;">
														<c:forEach items="${CACHE_AGENT_TYPE}" var="i" varStatus="status">
															<c:if test="${i.key == agent.agent_type}">
																<ctl:nullCv nullCheck="${i.value}" />
															</c:if>
														</c:forEach>
													</td>
													<td align="center">
														<c:choose>
															<c:when test="${agent.status == 'false' }">
																<input type="image" onclick="actionAgent('${agent.agent_seq }', '${agent.server_seq }', '${agent.ip }', '${agent.port }', '${agent.status }')" src="${rootPath}/resources/image/psm/system_management/btn_start_up.png" title="에이전트 동작 버튼"/>
															</c:when>
															<c:when test="${agent.status == 'use' }">
																<input type="image" onclick="actionAgent('${agent.agent_seq }', '${agent.server_seq }', '${agent.ip }', '${agent.port }', '${agent.status }')" src="${rootPath}/resources/image/psm/system_management/btn_stop_up.png" title="에이전트 정지 버튼"/>
															</c:when>
															<c:otherwise>
																<input type="image" onclick="agentUnusual()" src="${rootPath}/resources/image/psm/system_management/btn_error_up.png" title="에이전트 비정상 상태"/>
															</c:otherwise>
														</c:choose>
													</td>
												</tr>
												<c:set var="count" value="${count - 1 }"/>
											</c:forEach>
										</c:otherwise>
									</c:choose>	
								</tbody>
			            	</table>
			            	
<!-- 			           	 </div> -->
	                <div class="row">
	                    <c:if test="${search.total_count > 0}">
							<div class="page left" id="pagingframe" align="center">
								<p><ctl:paginator currentPage="${search.page_num}" rowBlockCount="${search.size}" totalRowCount="${search.total_count}" /></p>
							</div>
						</c:if>
                    </div>
                    
                    <div class="row">
							<div class="col-md-6" id="mailDiv" style="width: 903px; height: 350px; display:none;">
								<form id="mailForm" method="POST">
								<h4><i class="icon-settings font-dark"></i> 메일서버</h4>
								<table class="table table-striped table-bordered order-column" height="270px">
									<c:forEach items="${alarm_info }" var="item" varStatus="status">
									<tr>
									<input type="hidden" id="agent_options" name="agent_options" value="" />
									<td width="20%"; height="270px"; style="text-align: center; vertical-align: middle;" rowspan="5">${item.option_name }</td>
									<td class="form-group form-md-line-input" style="vertical-align: middle;" >
										DBTYPE   : <input class="form-control " style="display: inline;width:80%;" type="text" id="dbtype" name="dbtype" value="${item.dbtype }" />
									</td>
								</tr>
								<tr>
									<td class="form-group form-md-line-input" style="vertical-align: middle;">
										JDBC_URL : <input class="form-control " style="display: inline;width:80%;" type="text" id="jdbc_url" name="jdbc_url" value="${item.jdbc_url }" />
									</td>
								</tr>
								<tr>
									<td class="form-group form-md-line-input" style="vertical-align: middle;">
										수신Email : <input class="form-control " style="display: inline;width:80%;" type="text" id="id" name="id" value="${item.id }" />
									</td>
								</tr>
								<tr>
									<td class="form-group form-md-line-input" style="vertical-align: middle;">
										PW : <input class="form-control " style="display: inline;width:80%;" type="text" id="pw" name="pw" value="${item.pw }" />
									</td>
								</tr>
								<tr>
									<td class="form-group form-md-line-input" style="vertical-align: middle;">
										PW : <input class="form-control " style="display: inline;width:80%;" type="text" id="to_id" name="to_id" value="${item.to_id }" />
									</td>
								</tr>
								</c:forEach>
								</table>
								</form>
								<div class="col-md-1" style="padding-right: 20px; float:right;" align="right" >
								<a style="text-align: right" onclick="setAlarmMngt_email()" class="btn btn-sm blue btn-outline sbold uppercase pull-right"><i class="fa fa-check"></i> 수정</a>
							</div>
							</div>
							<div class="col-md-6" id="licenseDiv" style="width: 903px; height: 350px; display:none;">
								<h4><i class="icon-settings font-dark"></i> 라이센스</h4>
								<form id="licenseForm" method="POST">
								<table class="table table-striped table-bordered order-column">
									<c:choose>
										<c:when test="${!empty license_hist }">
											<c:forEach items="${license_hist }" var="item" varStatus="status">
												<tr>
												<td width="20%" height="270px" style="text-align: center; vertical-align: middle;rowspan="4"" >히스토리</td>
												<td class="form-group form-md-line-input" width="80%" height=270px style="vertical-align: middle; rowspan="4">
													<textarea id="history" name="history" style="width: 100%; height: 240px; " >${item.history } </textarea>
												</td>
												</tr>
											</c:forEach>											
										</c:when>
										<c:otherwise>
											<tr>
												<td width="20%" height="270px" style="text-align: center; vertical-align: middle;rowspan="4"" >히스토리</td>
												<td class="form-group form-md-line-input" width="80%" height=270px style="vertical-align: middle; rowspan="4">
													<textarea id="history" name="history" style="width: 100%; height: 240px; resize: none" ></textarea>
												</td>
											</tr>
										</c:otherwise>
									</c:choose>
								</table>
								</form>
								<div class="col-md-1" style="padding-right: 20px; float:right;" align="right" >
								<a style="text-align: right" onclick="setLicenCe_Hist()" class="btn btn-sm blue btn-outline sbold uppercase pull-right"><i class="fa fa-check"></i> 수정</a>
								</div>
							</div>
							
							
					</div>
               	</div>
           	</div>
       	</div>
	</div>
    <!-- End: Demo Datatable 2 -->
    
	<!-- END EXAMPLE TABLE PORTLET-->
    </div>  
</div>

<script type="text/javascript">
	
	var agentListConfig = {
		"listUrl":"${rootPath}/agentMngt/list.html",
		"detailUrl":"${rootPath}/agentMngt/detail.html"
	};
 	
</script>
