<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="userAddFlag" value="F" />
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />
<script src="${rootPath}/resources/js/common/jquery.form.js"
	type="text/javascript" charset="UTF-8"></script>
<script
	src="${rootPath}/resources/js/psm/system_management/empUserMngtList.js"
	type="text/javascript" charset="UTF-8"></script>

<h1 class="page-title">${currentMenuName}</h1>
<div class="portlet light portlet-fit portlet-datatable bordered">

	<!-- 여기서부터 모달 몸통 -->

	<!-- Trigger the modal with a button -->
	<!--   <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->

	<!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog" style="position: relative ;top: 30%; left: 0;">

			<!-- Modal content -->
			<div class="modal-content">
				<div class="modal-header" style="background-color: #32c5d2;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">
						<b style="font-size: 13pt;">엑셀 업로드</b>
					</h4>
				</div>
				<div class="modal-body" style="background-color: #F9FFFF;">
					<form action="upload.html" method="post" id="fileForm"
						name="fileForm" enctype="multipart/form-data">
						<div>
							<table width="100%">
								<tr style="height: 40px">
									<td width="70%" style="vertical-align: middle;" align="center">
										<!-- 										<input type="submit" value="upload" />  --> <input
										class="fileUpLoad" type="file" id="file" name="file">
									</td>
									<td><img style="cursor: pointer;" name="submup"
										onclick="excelEmpUserUpLoad()"
										src="${rootPath}/resources/image/common/btn_exupload.gif"
										alt="엑셀업로드" title="엑셀업로드" /></td>
								</tr>
								<tr style="height: 40px">
									<td width="70%" style="vertical-align: middle;"><span
										class="downexam">&nbsp;&nbsp;&nbsp;※다운받은 양식으로 업로드하셔야
											사용자정보가 등록됩니다.</span></td>
									<td><img class="btn_excel exceldown" onclick="exDown()" style="cursor: pointer;"
										src="${rootPath}/resources/image/common/formUp.png"
										alt="양식다운로드" title="양식다운로드" /></td>
								</tr>
							</table>
						</div>
						<input type="hidden" id="result" name="result" value="false" />
					</form>
				</div>
				<div class="modal-footer">
					<a class="btn btn-sm red" href="#" data-dismiss="modal"> <i
						class="fa fa-remove"></i> 닫기
					</a>
				</div>
			</div>
		</div>
	</div>

	<div class="portlet-body">
		<div class="table-container">

			<div id="datatable_ajax_2_wrapper"
				class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">

				<!-- 				<div class="dataTables_scroll" style="position: relative;"> -->
				<!-- 				<div class="row"> -->
				<div class="col-md-6"
					style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
					<div class="portlet box grey-salt  ">
                          <div class="portlet-title" style="background-color: #2B3643;">
                              <div class="caption">
                                  <i class="fa fa-search"></i>검색 & 엑셀</div>
                              <div class="tools">
				                 <a id="searchBar_icon" href="javascript:;" class="collapse"> </a>
				             </div>
                          </div>
                          <div id="searchBar" class="portlet-body form" >
                              <div class="form-body" style="padding-left:10px; padding-right:10px;">
                                  <div class="form-group">
                                      <form id="empUserListForm" method="POST" class="mt-repeater form-horizontal">
                                          <div data-repeater-list="group-a">
                                              <div class="row">
                                                  <!-- jQuery Repeater Container -->
                                                  <div class="col-md-2">
                                                      <label class="control-label">시스템명</label>
                                                      <select name="system_seq_1"
														class="form-control">
														<option value=""
															<c:if test="${search.system_seq_1 == null}">selected="selected"</c:if>>
															----- 전체 -----</option>
														<c:forEach items="${systemMasterList}"
															var="systemMasterList">
															<option value="${systemMasterList.system_seq}"
																<c:if test="${systemMasterList.system_seq == search.system_seq_1}">selected="selected"</c:if>>${systemMasterList.system_name}</option>
														</c:forEach>
													</select>
                                                  </div>
                                                  <div class="col-md-2">
                                                      <label class="control-label">소속</label>
                                                      <input type="text" class="form-control" name="dept_name" value="${search.dept_name}" />
                                                  </div>
                                                  <div class="col-md-2">
                                                      <label class="control-label">사용자ID</label>
                                                      <input class="form-control" name="emp_user_id_1" value="${search.emp_user_id_1}" />
                                                  </div>
                                                  <div class="col-md-2">
                                                      <label class="control-label">사용자명</label>
                                                      <input class="form-control" name="emp_user_name" value="${search.emp_user_name}" />
                                                  </div>
                                                  <div class="col-md-2">
                                                      <label class="control-label">상태</label>
                                                      <select name="status" class="form-control">
														<option value=""
															${search.status == '' ? 'selected="selected"' : ''}>
															----- 전체 -----</option>
														<c:if test="${empty CACHE_EMP_USER_STATUS}">
															<option>데이터 없음</option>
														</c:if>
														<%-- <c:if test="${!empty CACHE_EMP_USER_STATUS}">
															<c:forEach items="${CACHE_EMP_USER_STATUS}" var="i"
																varStatus="z">
																<option value="${i.key}"
																	${i.key==search.status ? "selected=selected" : "" }>${ i.value}</option>
															</c:forEach>
														</c:if> --%>
														<option value="W" <c:if test="${search.status eq 'W' }">selected="selected"</c:if>>재직</option>
														<option value="R" <c:if test="${search.status eq 'R' }">selected="selected"</c:if>>퇴직</option>
														<option value="L" <c:if test="${search.status eq 'L' }">selected="selected"</c:if>>휴직</option>
													</select>
                                                  </div>
                                              </div>
                                          </div>
                                          <hr/>
                                          <div align="right">
											<button type="reset"
												class="btn btn-sm red-mint btn-outline sbold uppercase"
												onclick="resetOptions(empUserListConfig['listUrl'])">
												<i class="fa fa-remove"></i> <font>초기화
											</button>
											<button type="button" class="btn btn-sm blue btn-outline sbold uppercase"
												onclick="searchEmpUserList()">
												<i class="fa fa-search"></i> 검색
											</button>&nbsp;&nbsp;
										<%-- <a class="btn red btn-outline btn-circle"
											onclick="excelAllLogInqList('${search.total_count}')"> 
											<i class="fa fa-share"></i> <span class="hidden-xs"> 엑셀 </span>
										</a> --%>
										<div class="btn-group">
											<a href="javascript:;" data-toggle="dropdown">
												<img src="${rootPath}/resources/image/icon/XLS_3.png">
											</a>
											<ul class="dropdown-menu pull-right">
												<li>
													<!-- 					<button type="button" data-toggle="modal" data-target="#myModal">업로드</button> -->
													<a data-toggle="modal" data-target="#myModal">업로드</a> <!-- 										<a onclick="excelUpLoadList()">업로드</a> -->
												</li>
												<li><a onclick="excelEmpUserList()"> 다운로드 </a></li>
											</ul>
										</div>
									</div>
									
									<input type="hidden" name="main_menu_id"
										value="${paramBean.main_menu_id}" /> <input type="hidden"
										name="sub_menu_id" value="${paramBean.sub_menu_id}" /> <input
										type="hidden" name="current_menu_id" value="${currentMenuId}" />
									<input type="hidden" name="page_num" value="${search.page_num}" />
									<input type="hidden" name="isSearch" value="${paramBean.isSearch }" />
                                      </form>
                                  </div>
                              </div>
                          </div>
                      </div>
				</div>
				<!-- 				</div> -->

				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-6">
							<div class="btn-group">
								<button type="button" class="btn btn-sm blue btn-outline sbold uppercase" onclick="findEmpUserMngtOne()">
									<i class="fa fa-plus"></i> 신규 
								</button>
							</div>
						</div>
					</div>
				</div>

				<table
					class="table table-striped table-bordered table-hover order-column"
					id="datatable_ajax_2" aria-describedby="datatable_ajax_2_info"
					role="grid">

					<thead>
						<tr role="row" class="heading">
							<th width="10%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								No.</th>
							<th width="15"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								소속</th>
							<th width="10%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								사용자ID</th>
							<th width="15%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								사용자명</th>
							<th width="15%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								시스템명</th>
							<th width="15%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								사용자IP</th>
							<th width="10%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								상태</th>
							<th width="10%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								내외구분</th>
							<th width="10%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								소명유형</th>
							<th width="10%"
								style="border-bottom: 1px solid #e7ecf1; text-align: center;">
								사용여부</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${empty empUserList}">
								<tr>
									<td colspan="7" align="center">데이터가 없습니다.</td>
								</tr>
							</c:when>
							<c:otherwise>
								<c:set value="${search.total_count}" var="count" />
								<c:forEach items="${empUserList}" var="empUser"
									varStatus="status">
									<tr
										onclick="findEmpUserMngtOne('${empUser.emp_user_id}', '${empUser.system_seq }');"
										style="cursor: pointer;">
										<td style="text-align: center;">${count - search.page_num * search.size + search.size}</td>
										<td style="padding-left: 20px;"><c:out value="${empUser.dept_name}"/></td>
										<td style="padding-left: 20px;"><c:out value="${empUser.emp_user_id}"/></td>
										<td style="text-align: center;"><c:out value="${empUser.emp_user_name}"/></td>
										<td style="text-align: center;"><c:out value="${empUser.system_name}"/></td>
										<td style="text-align: center;"><c:out value="${empUser.ip}"/></td>
										<td style="text-align: center;"><c:out value="${empUser.status_name}"/></td>
										<td style="text-align: center;">
										<c:if test='${empUser.external_staff == 0}'><c:out value="내부직"/></c:if>
										<c:if test='${empUser.external_staff == 1}'><c:out value="외부직"/></c:if>
										</td>
										<td style="text-align: center;">
										<c:if test='${empUser.self_summon == "Y"}'><c:out value="본인"/></c:if>
										<c:if test='${empUser.self_summon == "N"}'><c:out value="대리인"/></c:if>
										</td>
										<c:choose>
											<c:when
												test="${empUser.use_flag == 'Y'}">
												<td style="text-align: center;vertical-align: middle;"><span
													class="label label-sm label-success"> 사용 </span></td>
											</c:when>
											<c:otherwise>
												<td style="text-align: center;vertical-align: middle;"><span
													class="label label-sm label-warning ">미사용 </span></td>
											</c:otherwise>
										</c:choose>
									</tr>
									<c:set var="count" value="${count - 1 }" />
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
				<div class="dataTables_processing DTS_Loading"
					style="display: none;">Please wait ...</div>
				<!-- 				</div> -->
				<!-- 페이징 영역 -->
				<c:if test="${search.total_count > 0}">
					<div class="page left" id="pagingframe" align="center">
						<p>
							<ctl:paginator currentPage="${search.page_num}"
								rowBlockCount="${search.size}"
								totalRowCount="${search.total_count}" />
						</p>
					</div>
				</c:if>
			</div>
		</div>

	</div>
</div>
<!-- 엑셀양식 다운로드 -->
<form action="exdownload.html" method="post" id="exdown" name="exdown"
	enctype="multipart/form-data"></form>
<script type="text/javascript">
	var empUserListConfig = {
		"listUrl" : "${rootPath}/empUserMngt/list.html",
		"detailUrl" : "${rootPath}/empUserMngt/detail.html",
		"downloadUrl" : "${rootPath}/empUserMngt/download.html",
		"uploadUrl" : "${rootPath}/empUserMngt/upload.html"
	};
</script>