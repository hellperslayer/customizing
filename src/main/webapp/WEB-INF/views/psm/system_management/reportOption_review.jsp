<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/system_management/reportOption.js" type="text/javascript" charset="UTF-8"></script>
 

	<div class="caption">
		<i class="icon-settings font-dark"></i> 
		<span class="caption-subject font-dark sbold uppercase">점검보고서 총평 기본 문구 등록</span>
	</div>	
	
       <div class="portlet-body">
            
            <table class="table table-striped table-bordered order-column">
				<tr>
					<td width="30%" style="text-align: center;" >보고서 선택</td>
					<td width="70%" style="text-align: center; white-space: nowrap;">문구 작성</td>
				</tr>
				<tr>
					<td style="text-align: center; vertical-align: middle; padding-bottom: 120px;">
					<select id="report_code" onchange="findDefaultDesc()" class="form-control">
							<optgroup label="">
								<c:forEach var="report" items="${reportCode }">
									<option value="${report.code_id }">${report.code_name }</option>
								</c:forEach>
							</optgroup>
						</select>
					</td>
					<td style="text-align: center;">
						<textarea id="defaultdesc" rows="7" style="resize: none; width: 93%; float: left;" class="form-control"></textarea>
						<input type="button" style="float: right" class="btn btn-sm blue btn-outline sbold uppercase" onclick="addDefaultDesc()" value="저장" />
					</td>
				</tr>
            </table>
        </div>

<form id="listForm" method="POST">
	<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
    <input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" /> 
	<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" /> 
</form>

<script type="text/javascript">
	
	var reportOptionConfig = {
			"listUrl":"${rootPath}/reportOption/list.html",
			"addUrl":"${rootPath}/reportOption/upload.html",

			
	};
	
	$(document).ready(function(){
		findDefaultDesc();
	});
	
</script>
