<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="statistics" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>

<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/system_management/reportOption.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>

<style>
.tabbable-custom>.nav-tabs>li.active{
border-top: 3px solid #578ebe;
}
</style>

<h1 class="page-title" style="margin-bottom: 0px;"></h1><%--  ${currentMenuName} --%>
<span style="float: right;font-size: 15px;color: gray;">adminVer. ${version }</span>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light portlet-fit portlet-datatable bordered">
			
			<div class="portlet-body">
				<div class="tabbable tabbable-custom">
				    <ul class="nav nav-tabs">
				    	<li <c:if test="${tab_flag == 1 }">class="active"</c:if>>
				            <a href="#tab1" data-toggle="tab"> 일반사항 </a>
				        </li>
				        <li <c:if test="${tab_flag == 2 }">class="active"</c:if>>
				            <a href="#tab2" data-toggle="tab"> 로고관리 </a>
				        </li>
				        <li <c:if test="${tab_flag == 3 }">class="active"</c:if>>
				            <a href="#tab3" data-toggle="tab"> 결재라인관리 </a>
				        </li>
				        <li <c:if test="${tab_flag == 4 }">class="active"</c:if>>
				            <a href="#tab4" data-toggle="tab"> 총평관리 </a>
				        </li>
					</ul>
				
					<div class="tab-content">
						<div class="tab-pane <c:if test="${tab_flag == 1 }">active</c:if>" id="tab1">  
							<%@include file="/WEB-INF/views/psm/system_management/reportOption_general.jsp"%>
						</div>
						<div class="tab-pane <c:if test="${tab_flag == 2 }">active</c:if>" id="tab2">
							<%@include file="/WEB-INF/views/psm/system_management/reportOption_logo.jsp"%>
						</div>
						<div class="tab-pane <c:if test="${tab_flag == 3 }">active</c:if>" id="tab3">
							<%@include file="/WEB-INF/views/psm/system_management/reportOption_approval.jsp"%>
						</div>
						<div class="tab-pane <c:if test="${tab_flag == 4 }">active</c:if>" id="tab4">
							<%@include file="/WEB-INF/views/psm/system_management/reportOption_review.jsp"%> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	var reportOptionConfig = {
		"listUrl":"${rootPath}/reportOption/list.html",
	};

</script>