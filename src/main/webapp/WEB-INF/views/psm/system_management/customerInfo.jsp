<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/system_management/customerInfo.js" type="text/javascript" charset="UTF-8"></script>
 
 
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title">${currentMenuName}</h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<div class="row">
	<div class="col-md-12">
	    <!-- BEGIN EXAMPLE TABLE PORTLET-->
	    
	    <div class="portlet light portlet-datatable bordered">
	    	<div class="caption">
				<i class="icon-settings font-dark"></i> 
				<span class="caption-subject font-dark sbold uppercase">로고 이미지 등록</span>
			</div>
    	    <div class="portlet-body">
	            
	            <table class="table table-striped table-bordered order-column">
	            <form id="customerInfoForm" action="${rootPath}/customerInfo/upload.html" enctype="multipart/form-data" method="POST" onsubmit="return check()">
                    <tr>	                        
                        <td width="30%" style="text-align: center; vertical-align: middle;"> 로고 이미지 등록 </td>
                        <td width="70%" style="text-align: left;white-space: nowrap; vertical-align: middle;"> 
	                        <input type="file" name="filename" accept=".gif, .jpg, .png" style="display: inline;"/>
	                        <!-- &nbsp;<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="addFile()"><i class="fa fa-check"></i> 등록</a> -->
	                        <input type="submit" class="btn btn-sm blue btn-outline sbold uppercase" value="등록" />
                        </td>
                    </tr>
	                <!-- <tr>
	                	<td width="30%" style="text-align: center; "> 라이센스 등록 </td>
                        <td width="70%" style="text-align: left; "> <input type="text" name="license"/>&nbsp;<a class="btn btn-sm green" onclick="">등록</a></td>
	                </tr> -->
	                
                </form>
	            </table>
	            
	        </div>
	        
	        <div class="caption">
				<i class="icon-settings font-dark"></i> 
				<span class="caption-subject font-dark sbold uppercase">로고 이미지 목록</span>
			</div>
			<div>
				<table style="border-top: 1px solid #e7ecf1"
					class="table table-bordered" style="position: absolute; top: 0px; left: 0px; width: 100%;">
					<thead>
						<th width="5%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">
							No.</th>
						<th width="25%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">
							등록일시</th>
						<th width="30%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">
							파일 이름</th>
						<th width="10%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">
							사용여부</th>
						<th width="10%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">
							삭제</th>
						<th width="20%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">
							이미지</th>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${empty image_logo_list}">
								<tr><td colspan="6" align="center">데이터가 없습니다.</td></tr>
							</c:when>
							<c:otherwise>
								<c:forEach items="${image_logo_list }" var="image" varStatus="status">
									<tr>
										<td style="text-align: center; vertical-align: middle;">${status.count }</td>
										<td style="text-align: center; vertical-align: middle;">
											<fmt:parseDate value="${image.reg_date}" pattern="yyyyMMdd" var="reg_date" /> 
											<fmt:formatDate value="${reg_date}" pattern="YYYY-MM-dd" />
										</td>
										<td style="text-align: center; vertical-align: middle;">${image.origin_name }</td>
										<td style="text-align: center; vertical-align: middle;">
										<%-- <c:choose>
											<c:when test="${image.use eq 'Y'} ">
												<span class="btn btn-xs blue btn-outline" onclick="useLogoImage(${image.idx}, 1)">사용</span>
											</c:when>
											<c:otherwise>
												<span class="btn btn-xs yellow btn-outline" onclick="useLogoImage(${image.idx}, 2)">미사용</span>
											</c:otherwise>
										</c:choose> --%>
											<c:if test="${fn:trim(image.use) eq 'Y'}">
												<span class="btn btn-xs blue btn-outline" onclick="useLogoImage(${image.idx}, 1)">사용</span>
											</c:if>
											<c:if test="${fn:trim(image.use) eq 'N'}">
												<span class="btn btn-xs yellow btn-outline" onclick="useLogoImage(${image.idx}, 2)">미사용</span>
											</c:if>											
										</td>
										<td style="text-align: center; vertical-align: middle;">
											<p class="btn btn-xs red btn-outline filter-cancel"
												onclick="deleteLogoImage(${image.idx})">삭제</p>
										</td>
										<td style="vertical-align: middle; text-align: center;">
											<img src="${pageContext.servletContext.contextPath}/resources/upload/${image.stored_name}" width="70" height="70">
										</td>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>
	    </div>
	        
	    
		<div class="portlet light portlet-datatable bordered">
		
			<div class="caption">
				<i class="icon-settings font-dark"></i> 
				<span class="caption-subject font-dark sbold uppercase">점검보고서 로고 이미지 등록 </span>
			</div>	
			
	        <div class="portlet-body">
		            
		            <table class="table table-striped table-bordered order-column">
		            <form id="customerInfoForm2" action="${rootPath}/customerInfo/upload2.html" enctype="multipart/form-data" method="POST" accept-charset="UTF-8" onsubmit="return check2()">
	                    <tr>	                        
	                        <td width="30%" style="text-align: center; vertical-align: middle;"> 점검보고서 로고 이미지 등록 </td>
	                        <td width="70%" style="text-align: left;white-space: nowrap; vertical-align: middle;"> 
		                        <input type="file" name="filename2" accept=".gif, .jpg, .png" style="display: inline;"/>
		                        <!-- &nbsp;<a class="btn btn-sm blue btn-outline sbold uppercase" onclick="addFile()"><i class="fa fa-check"></i> 등록</a> -->
		                        <input type="submit" class="btn btn-sm blue btn-outline sbold uppercase" value="등록" on/>
	                        </td>
	                    </tr>
		                <!-- <tr>
		                	<td width="30%" style="text-align: center; "> 라이센스 등록 </td>
	                        <td width="70%" style="text-align: left; "> <input type="text" name="license"/>&nbsp;<a class="btn btn-sm green" onclick="">등록</a></td>
		                </tr> -->
	                </form>
		            </table>
		            
		        </div>
	        
	        <div class="caption">
				<i class="icon-settings font-dark"></i> 
				<span class="caption-subject font-dark sbold uppercase">점검보고서 로고 이미지 목록</span>
			</div>
			<div>
				<table style="border-top: 1px solid #e7ecf1"
					class="table table-bordered" style="position: absolute; top: 0px; left: 0px; width: 100%;">
					<thead>
						<th width="5%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">
							No.</th>
						<th width="25%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">
							등록일시</th>
						<th width="30%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">
							파일 이름</th>
						<th width="10%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">
							사용여부</th>
						<th width="10%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">
							삭제</th>
						<th width="20%" style="border-bottom: 1px solid #e7ecf1; text-align: center;">
							이미지</th>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${empty image_logo_list2}">
								<tr><td colspan="6" align="center">데이터가 없습니다.</td></tr>
							</c:when>
							<c:otherwise>
								<c:forEach items="${image_logo_list2 }" var="image" varStatus="status">
									<tr>
										<td style="text-align: center; vertical-align: middle;">${status.count }</td>
										<td style="text-align: center; vertical-align: middle;">
											<fmt:parseDate value="${image.reg_date}" pattern="yyyyMMdd" var="reg_date" /> 
											<fmt:formatDate value="${reg_date}" pattern="YYYY-MM-dd" />
										</td>
										<td style="text-align: center; vertical-align: middle;">${image.origin_name }</td>
										<td style="text-align: center; vertical-align: middle;">
											<c:if test="${fn:trim(image.use) eq 'Y'}">
												<span 
												class="btn btn-xs blue btn-outline" onclick="useLogoImage2(${image.idx}, 1)">사용</span>
											</c:if>
											<c:if test="${fn:trim(image.use) eq 'N'}">
												<span class="btn btn-xs yellow btn-outline" onclick="useLogoImage2(${image.idx}, 2)">미사용</span>
											</c:if>
										</td>
										<td style="text-align: center; vertical-align: middle;">
											<p class="btn btn-xs red btn-outline filter-cancel"
												onclick="deleteLogoImage(${image.idx})">삭제</p>
										</td>
										<td style="vertical-align: middle; text-align: center;">
											<img src="${pageContext.servletContext.contextPath}/resources/upload/${image.stored_name}" width="70" height="70">
										</td>
									</tr>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>
			
			
	    <!-- END EXAMPLE TABLE PORTLET-->
    </div>
    
    <div class="portlet light portlet-datatable bordered">
		
	<div class="caption">
		<i class="icon-settings font-dark"></i> 
		<span class="caption-subject font-dark sbold uppercase">점검보고서 결재라인 등록 </span>
	</div>	
	
       <div class="portlet-body">
            
            <table class="table table-striped table-bordered order-column">
				<tr>
					<td width="45%" style="text-align: center;" colspan="2" >결재자 이름 등록</td>
					<td width="55%" style="text-align: center; white-space: nowrap;">미리보기</td>
				</tr>
				<tr>
					<td width="15%" style="text-align: center;">
					결재칸수
						<select id="countSelect" onchange="AuthorCount()">
							<optgroup label="">
								<option value="0" ${authorizeLine.count eq 0 ? 'selected="selected"':"" }>사용하지 않음</option>
								<option ${authorizeLine.count eq 1 ? 'selected="selected"':"" }>1</option>
								<option ${authorizeLine.count eq 2 ? 'selected="selected"':"" }>2</option>
								<option ${authorizeLine.count eq 3 ? 'selected="selected"':"" }>3</option>
							</optgroup>
						</select>
					</td>
					<td width="30%" style="text-align: center;">
						<table class="table-striped table-bordered order-column">
							<tr>
								<td style="width: 20%">이름</td>
								<td><input id="authorName1" class="authorName" type="text" disabled="disabled" value="${authorizeLine.name1}"></td>
							</tr>
							<tr>
								<td style="width: 20%">이름</td>
								<td><input id="authorName2" class="authorName" type="text" disabled="disabled" value="${authorizeLine.name2}"></td>
							</tr>
							<tr>
								<td style="width: 20%">이름</td>
								<td><input id="authorName3" class="authorName" type="text" disabled="disabled" value="${authorizeLine.name3}"></td>
							</tr>
							<tr>
								<td colspan="2">
									<input type="button" class="btn btn-sm yellow btn-outline sbold uppercase" onclick="AuthorCount()" value="미리보기" />
									<input type="button" class="btn btn-sm blue btn-outline sbold uppercase" onclick="addAuthorizeLine()" value="저장" />
								</td>
							</tr>
						</table>
					</td>
					<td width="55%" style="text-align: left; white-space: nowrap;">
						<div id="html_source" align="center">
							<TABLE border="1" id="preview" cellspacing="0" cellpadding="0" style='border-collapse:collapse; border: 1px solid black'>
								<TR id="authorNameTr">
									<TD rowspan="2" id="authorNameFirst" valign="middle" style='width: 27px; height: 114px; padding: 1.4pt 5.1pt 1.4pt 5.1pt;'>
									<P STYLE=''><SPAN STYLE=''>결</SPAN></P>
									<P STYLE='margin: 0px'><SPAN STYLE=''>재</SPAN></P>
									</TD>
								</TR>
								<TR id="authorMarkTr">
								</TR>
							</TABLE>
						</div>
					</td>
				</tr>
            </table>
        </div>
	</div>
    <div class="portlet light portlet-datatable bordered">
		
	<div class="caption">
		<i class="icon-settings font-dark"></i> 
		<span class="caption-subject font-dark sbold uppercase">점검보고서 총평 기본 문구 등록</span>
	</div>	
	
       <div class="portlet-body">
            
            <table class="table table-striped table-bordered order-column">
				<tr>
					<td width="30%" style="text-align: center;" >보고서 선택</td>
					<td width="70%" style="text-align: center; white-space: nowrap;">문구 작성</td>
				</tr>
				<tr>
					<td style="text-align: center; vertical-align: middle">
					<select id="report_code" onchange="findDefaultDesc()" class="form-control">
							<optgroup label="">
								<c:forEach var="report" items="${reportCode }">
									<option value="${report.code_id }">${report.code_name }</option>
								</c:forEach>
							</optgroup>
						</select>
					</td>
					<td style="text-align: center;">
						<textarea id="defaultdesc" rows="7" style="resize: none; width: 95%; float: left;" class="form-control"></textarea>
						<input type="button" style="float: right" class="btn btn-sm blue btn-outline sbold uppercase" onclick="addDefaultDesc()" value="저장" />
					</td>
				</tr>
            </table>
        </div>
	</div>

<form id="listForm" method="POST">
	<input type="hidden" name="current_menu_id" value="${currentMenuId}" />
    <input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" /> 
	<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" /> 
</form>

<script type="text/javascript">
	
	var customerInfoConfig = {
			"listUrl":"${rootPath}/customerInfo/list.html",
			"addUrl":"${rootPath}/customerInfo/upload.html"
	};
	
	$(document).ready(function(){
		findDefaultDesc();
	});
	
</script>
