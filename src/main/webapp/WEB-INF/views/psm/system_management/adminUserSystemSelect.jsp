<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<table style="border-top: 1px solid #e7ecf1"
						class="table table-striped table-bordered table-hover table-checkable dataTable no-footer"
						id="datatable_ajax_2" aria-describedby="datatable_ajax_2_info"
						role="grid"
						style="position: absolute; top: 0px; left: 0px; width: 100%;">

	<thead>
		<tr role="row" class="heading">
			<th width="50%" style="border-bottom: 1px solid #e7ecf1;">
				<input type="checkbox" class="auth_checkbox"
				id="authMenuAllCheck" onclick="allAuthMenuCheck(this)" />
			</th>
			<th width="50%" style="border-bottom: 1px solid #e7ecf1;">
				�ý��۸�</th>
		</tr>
	</thead>
	<tbody>
		<c:set var="tmpAuth" value=",${adminUserDetail.auth_ids }" />
		<c:forEach items="${systemMaster}" var="system" varStatus="status">
			<c:set var="tmpSys" value=",${system.system_seq }"/>
			<tr>
				<c:choose>
					<c:when test="${fn:contains(tmpAuth, tmpSys)}">
						<td>
							<input type="checkbox" class="auth_checkbox" id="${system.system_seq}" name="auth_id" value="${system.system_seq}" checked />
						</td>
					</c:when>
					<c:otherwise>
						<td>
							<input type="checkbox" class="auth_checkbox" id="${system.system_seq}" name="auth_id" value="${system.system_seq}" />
						</td>
					</c:otherwise>
				</c:choose>
				<td><c:out value="${system.system_name}" /></td>
			</tr>
		</c:forEach>
	</tbody>
</table>