<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>

<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/psm/system_management/talarmMngtDetail.js" type="text/javascript" charset="UTF-8"></script>


<h1 class="page-title">
	<c:choose>
		<c:when test="${!empty talarmDetail}">
			${currentMenuName}
		</c:when>
		<c:otherwise>
			알람 설정 상세보기
		</c:otherwise>
	</c:choose>
</h1>
<div class="portlet light portlet-fit bordered">
	<div class="portlet-body">
		<!-- BEGIN FORM-->
		<form id="talarmDetailForm" method="POST"
			class="form-horizontal form-bordered form-row-stripped">
			<input type="hidden" name="emp_user_id" value="${paramBean.emp_user_id }" />
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>		
						<th style="text-align: center;"><label class="control-label">사용자명</label></th>
						<c:choose>
							<c:when test="${empty talarmDetail}">
								<td style="vertical-align: middle;" class="form-group form-md-line-input">
									&nbsp;-
								</td>
							</c:when>
							<c:otherwise>
								<td style="vertical-align: middle;" class="form-group form-md-line-input">
									&nbsp;<c:out value="${talarmDetail.emp_user_name }" />
								</td>
							</c:otherwise>
						</c:choose>
						<th style="text-align: center;"><label class="control-label">사용자ID</label></th>
						<c:choose>
							<c:when test="${empty talarmDetail}">
								<td style="vertical-align: middle;" class="form-group form-md-line-input">
									&nbsp;-
								</td>
							</c:when>
							<c:otherwise>
								<td style="vertical-align: middle;" class="form-group form-md-line-input">
									&nbsp;<c:out value="${talarmDetail.emp_user_id }" />
								</td>
							</c:otherwise>
						</c:choose>
					</tr>
					<tr>
						<th style="text-align: center;"><label class="control-label">E-mail</label></th>
						<c:choose>
							<c:when test="${empty talarmDetail }">
								<td class="form-group form-md-line-input" style="vertical-align: middle;">
									<input type="text" name="email_address" class="form-control" />
									<div class="form-control-focus"></div>
								</td>
							</c:when>
							<c:otherwise>
								<td class="form-group form-md-line-input" style="vertical-align: middle;">
									<%-- <c:out value="${talarmDetail.ip }" /> --%>
									<input type="text" name="email_address_up" value="${talarmDetail.email_address }" class="form-control" />
									<div class="form-control-focus"></div>
								</td>
							</c:otherwise>
						</c:choose>
						<th style="text-align: center;"><label class="control-label">Mobile Number</label></th>
						<c:choose>
							<c:when test="${empty talarmDetail }">
								<td class="form-group form-md-line-input" style="vertical-align: middle;">
									<input type="text" name="mobile_number" class="form-control" />
									<div class="form-control-focus"></div>
								</td>
							</c:when>
							<c:otherwise>
								<td class="form-group form-md-line-input" style="vertical-align: middle;">
									<input type="text" name="mobile_number_up" value="${talarmDetail.mobile_number }" class="form-control" />
									<div class="form-control-focus"></div>
								</td>
							</c:otherwise>
						</c:choose>
					</tr>
					<tr>
						<th style="text-align: center;"><label
										class="control-label">E-mail 사용여부 <span class="required">*</span>
									</label></th>
									<td colspan="3" class="form-group form-md-line-input">
										<div class="md-radio-inline">
											<div class="md-radio col-md-3">
												<input type="radio" id="checkbox1_1" name="useyn_email"
													value="Y" class="md-radiobtn"
													${fn:containsIgnoreCase(talarmDetail.useyn_email,'Y') ? 'checked' : ''}>
												<label for="checkbox1_1"> <span></span> <span
													class="check"></span> <span class="box"></span> 사용
												</label>
											</div>
											<div class="md-radio col-md-3">
												<input type="radio" id="checkbox1_2" name="useyn_email"
													value="N" class="md-radiobtn"
													${!fn:containsIgnoreCase(talarmDetail.useyn_email,'Y') ? 'checked' : ''}>
												<label for="checkbox1_2"> <span></span> <span
													class="check"></span> <span class="box"></span> 미사용
												</label>
											</div>
										</div>
									</td>
						</tr>
						<tr>
							<th style="text-align: center;"><label class="control-label">E-mail 제목</label></th>
							<c:choose>
								<c:when test="${empty talarmDetail }">
									<td class="form-group form-md-line-input" style="vertical-align: middle;">
										<input type="text" name="email_address" class="form-control" />
										<div class="form-control-focus"></div>
									</td>
								</c:when>
								<c:otherwise>
									<td class="form-group form-md-line-input" style="vertical-align: middle;">
										<input type="text" name="email_subject" value="${talarmDetail.email_subject }" class="form-control" />
										<div class="form-control-focus"></div>
									</td>
								</c:otherwise>
							</c:choose>
						</tr>
						<tr>
							<th style="text-align: center; vertical-align: middle;">
								<label class="control-label">E-mail 내용</label>
							</th>
							<td style="vertical-align: middle;" class="form-group form-md-line-input" colspan="3">
								<textarea name="email_content" class="form-control" rows="7">${talarmDetail.email_content}</textarea>
								<div class="form-control-focus"></div>
							</td>
						</tr>
						<tr>	
						<th style="text-align: center;"><label
										class="control-label">문자 사용여부 <span class="required">*</span>
									</label></th>
									<td colspan="3" class="form-group form-md-line-input">
										<div class="md-radio-inline">
											<div class="md-radio col-md-3">
												<input type="radio" id="checkbox2_1" name="useyn_htel"
													value="Y" class="md-radiobtn"
													${fn:containsIgnoreCase(talarmDetail.useyn_htel,'Y') ? 'checked' : ''}>
												<label for="checkbox2_1"> <span></span> <span
													class="check"></span> <span class="box"></span> 사용
												</label>
											</div>
											<div class="md-radio col-md-3">
												<input type="radio" id="checkbox2_2" name="useyn_htel"
													value="N" class="md-radiobtn"
													${!fn:containsIgnoreCase(talarmDetail.useyn_htel,'Y') ? 'checked' : ''}>
												<label for="checkbox2_2"> <span></span> <span
													class="check"></span> <span class="box"></span> 미사용
												</label>
											</div>
										</div>
									</td>
								</tr>
							<tr>
							<th style="text-align: center; vertical-align: middle;">
								<label class="control-label">문자 내용</label>
							</th>
							<td style="vertical-align: middle;" class="form-group form-md-line-input" colspan="3">
								<textarea name="htel_content" class="form-control" rows="5">${talarmDetail.htel_content}</textarea>
								<div class="form-control-focus"></div>
							</td>
						</tr>
				</tbody>
			</table>
		</form>
			
		<div class="form-actions">
			<div class="row">
				<div class="col-md-offset-2 col-md-10" align="right"
					style="padding-right: 30px;">
					<button type="button"
						class="btn btn-sm grey-mint btn-outline sbold uppercase"
						onclick="moveTalarmList()">
						<i class="fa fa-list"></i> 목록
					</button>
					<button type="button"
						class="btn btn-sm blue btn-outline sbold uppercase"
						onclick="saveTalarmMngtOne(); saveTalarmMngtTwo();">
						<i class="fa fa-check"></i> 수정
					</button>
				</div>
			</div>
		</div>

		<form id="talarmListForm" method="POST">
			<!-- 메뉴 관련 input 시작 -->
			<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" />
			<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
			<input type="hidden" name="current_menu_id" value="${currentMenuId }" />
			<!-- 메뉴 관련 input 끝 -->
			
			<!-- 페이지 번호 -->
			<input type="hidden" name="page_num" value="${paramBean.page_num }" />
			
			<!-- 검색조건 관련 input 시작 -->
			<input type="hidden" name="emp_user_name" value="${paramBean.emp_user_name }" />
			<input type="hidden" name="emp_user_id" value="${paramBean.emp_user_id }" />
			<input type="hidden" name="deptLowSearchFlag" value="${paramBean.deptLowSearchFlag }" />
			<input type="hidden" name="status" value="${paramBean.status }" />
			<input type="hidden" name="isSearch" value="${paramBean.isSearch }" />
			<!-- 검색조건 관련 input 끝 -->
		</form>
		<!-- END FORM-->
	</div>
</div>

<script type="text/javascript">

	var talarmDetailConfig = {
		"talarmListUrl":"${rootPath}/talarmMngt/list.html"
		,"addTalarmUrl":"${rootPath}/talarmMngt/add.html"
		,"saveTalarmUrl":"${rootPath}/talarmMngt/save.html"
		,"save1TalarmUrl":"${rootPath}/talarmMngt/save1.html"
		,"removeTalarmUrl":"${rootPath}/talarmMngt/remove.html"
	};
	
</script>