<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}"/>
<ctl:drawNavMenu menuId="${currentMenuId}" />

<link href="${rootPath}/resources/css/common/jquery-ui.dynatree.css" rel="stylesheet" type="text/css">
<script src="${rootPath }/resources/js/common/jquery-ui-1.9.0.custom.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/common/jquery.dynatree.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath }/resources/js/common/jquery.cookie.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/system_management/ipMngtList.js" type="text/javascript" charset="UTF-8"></script>



<h1 class="page-title"> ${currentMenuName}</h1>
<!-- END PAGE TITLE-->
<!-- BEGIN PAGE CONTENT-->
                  
<div class="row">
	<!-- 메뉴트리 -->
	<div class="col-md-4">
	    <div id="ipTree">
			<ul>
				<c:set var="preDepth" value="0"/>
				<c:forEach items="${hierarchyDepartments}" var="ip">
					<c:choose>
						<c:when test="${ip.dept_depth == 0 }">
						</c:when>
						<c:when test="${preDepth < ip.dept_depth}">
							<ul>
						</c:when>
						<c:when test="${preDepth == ip.dept_depth}">
							</li>
						</c:when>
						<c:when test="${preDepth > ip.dept_depth }">
							<c:forEach begin="1" end="${preDepth - ip.dept_depth}" step="1">
								</li></ul>
							</c:forEach>
							</li>
						</c:when>
					</c:choose>
					<li id="${ip.dept_id}"> <a href="${ip.dept_id}">${ip.dept_name}</a>
					<c:set var="preDepth" value="${ip.dept_depth}" />
				</c:forEach>
				<c:forEach begin="0" end="${preDepth}" step="1">
					</ul></li>
				</c:forEach>
			</ul>
		</div>       
	</div>
	
	<!-- 상세 -->                        
    <div class="col-md-8">
    <div id="ipDetailDiv" class="portlet light bordered form-fit">
		<div class="portlet-title">
		    <div class="caption">
		        <span class="caption-subject font-blue-hoki bold uppercase" id="deptIdLabel"></span>	        
		    </div>	    
		</div>
        <div class="portlet-body form"> 
    	<!-- BEGIN FORM-->
		<form id="ipDetailForm" method="POST" role="form" action="#" class="form-horizontal form-bordered form-row-stripped"> 
		    <div class="form-body">	        
				<div class="form-group form-md-line-input" align="center"> 
	               <label class="col-md-2 control-label" for="form_control_1" style="text-align: center;">소속명
	               	<span class="required">*</span>
	               </label>
	               <div class="col-md-4">
	                   <input type="text" class="form-control" id="dept_nam" name="dept_nam" style="text-align: center;">
	                   <div class="form-control-focus"> </div>
	               </div>
	               <label class="col-md-2 control-label" for="form_control_2" style="text-align: center;">부모소속명							                    	
	               </label>
	               <div class="col-md-4">
	               		<div class="form-control form-control-static">							                        
                      	<span id="parent_dept_name"></span>
                      	</div>
                      	<div class="form-control-focus"> </div>                                                    
	               </div>
	            </div>                
				<div class="form-group form-md-line-input" id="insertTr" align="center"> 
					<label class="col-md-2 control-label" style="text-align: center;">등록자                    	
					</label>
					<div class="col-md-4">                                                
					    <div class="form-control form-control-static"> <span id="insertUserIdSpan"></span> </div>	
					    <div class="form-control-focus"> </div>
					</div>
					<label class="col-md-2 control-label" style="text-align: center;">등록일시                    	
					</label>
					<div class="col-md-4">
					    <div class="form-control form-control-static"> <span id="insertDatetimeSpan"></span> </div>  
					    <div class="form-control-focus"> </div>
					</div>
				</div>
				<div class="form-group form-md-line-input" id="updateTr" align="center"> 
					<label class="col-md-2 control-label" style="text-align: center;">수정자                    	
					</label>
					<div class="col-md-4">                                                
					    <div class="form-control form-control-static"> <span id="updateUserIdSpan"> </div>	
					    <div class="form-control-focus"> </div>
					</div>
					<label class="col-md-2 control-label" style="text-align: center;">수정일시                    	
					</label>
					<div class="col-md-4">
					    <div class="form-control form-control-static"> <span id="updateDatetimeSpan"> </div>  
					    <div class="form-control-focus"> </div>
					</div>
				</div> 
				<div class="form-group form-md-line-input"> 
					<label class="col-md-2 control-label"style="text-align: center;" >IP
					<span class="required">*</span>                    	
					</label>
					<div class="col-md-8">                                                
					    <input type="text" id ="ipinfo" name = "ipinfo"/> <font>-</font> <input type="text" id ="ipinfoto" name = "ipinfoto"/>
					    <div class="form-control-focus"> </div>
					</div>
				</div> 
			</div> 
		    
		    <input type="hidden" name="dept_id" />
		</form> 
		<!-- END FORM-->
		
		<!-- IP관리는 IP 수정만 가능하기 때문에 수정 버튼만 삽입 -->
			 
		<!-- 버튼 영역 -->
		<div class="form-actions" align="right" style="padding-right:10px;"> 
            <!-- <div class="btn-set pull-right"> -->
	        <button type="button" class="btn btn-sm green table-group-action-submit" id="addDepartmentBtn" onclick="addDepartment()">
	            <i class="fa fa-check"></i> 등록 </button>
	        <button type="button" class="btn btn-sm green table-group-action-submit" id="saveDepartmentBtn" onclick="saveIp()">
				<i class="fa fa-check"></i> 수정</button>		
			<button type="button" class="btn btn-sm red table-group-action-submit" id="removeDepartmentBtn" onclick="removeDepartment()">
				<i class="fa fa-close"></i> 삭제</button>
		</div>
		</div>
    </div>
    
    <div id="ipDetailDiv2" class="portlet light bordered form-fit">
		<div class="portlet-title">
		    <div class="caption">
		        <span class="caption-subject font-blue-hoki bold uppercase" id="newAdd">· 신규등록</span>	        
		    </div>	    
		</div>
        <div class="portlet-body form"> 
    	<!-- BEGIN FORM-->
		<form id="ipDetailForm2" method="POST" role="form" action="#" class="form-horizontal form-bordered form-row-stripped"> 
		    <div class="form-body">	        
				<div class="form-group form-md-line-input"> 
	               <label class="col-md-2 control-label" for="form_control_1" style="text-align: center;">소속명
	               	<span class="required">*</span>
	               </label>
	               <div class="col-md-4">
	                   <input type="text" class="form-control" id="dept_nam" name="dept_nam" style="text-align: center;">
	                   <div class="form-control-focus"> </div>
	               </div>
	            </div>                
		        
				<div class="form-group form-md-line-input"> 
					<label class="col-md-2 control-label" style="text-align: center;">IP
					<span class="required">*</span>                    	
					</label>
					<div class="col-md-8">                                                
					    <input type="text" id ="ipinfom" name = "ipinfom"/> <font>-</font> <input type="text" id ="ipinfomto" name = "ipinfomto"/>
					    <div class="form-control-focus"> </div>
					</div>
				</div> 
			</div> 
		    
		</form> 
		<!-- END FORM-->
		
		<!-- IP관리는 IP 수정만 가능하기 때문에 수정 버튼만 삽입 -->
			 
		<!-- 버튼 영역 -->
		<div class="form-actions" align="right" style="padding-right:10px;"> 
            <!-- <div class="btn-set pull-right"> -->
	        <button type="button" class="btn btn-sm green table-group-action-submit" id="addDepartmentBtn" onclick="addPart()">
	            <i class="fa fa-check"></i> 등록 </button>
	        <button type="button" class="btn btn-sm red table-group-action-submit" id="removeDepartmentBtn" onclick="removeAdd()">
				<i class="fa fa-close"></i> 취소</button>
		</div>
		</div>
    </div>
	</div>
</div>


<!-- END PAGE CONTENT-->

<form id="ipListForm" method="POST">
	<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" />
	<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
	<input type="hidden" name="current_menu_id" value="${currentMenuId }" />
</form>

<script type="text/javascript">

	var ipConfig = {
		"detailUrl":"${rootPath}/ipMngt/detail.html"
		,"addUrl":"${rootPath}/ipMngt/add.html"
		,"saveUrl":"${rootPath}/ipMngt/save.html"
		,"removeUrl":"${rootPath}/ipMngt/remove.html"
		,"loginPage" : "${rootPath}/loginView.html"
	};
	
</script>