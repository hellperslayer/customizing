<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script
	src="${rootPath}/resources/js/psm/system_management/groupCodeMngtList.js"
	type="text/javascript" charset="UTF-8"></script>


<!-- 환경설정/코드관리 메인 -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title">
	${currentMenuName}
	<!--bold font-blue-hoki <small>basic bootstrap tables with various options and styles</small> -->
</h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<div class="row">
	<div class="col-md-6">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet light bordered">
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-6">
							<div class="btn-group">
								<button class="btn btn-sm blue btn-outline sbold uppercase"
									onclick="findGroupCodeDetail('')">
									신규 <i class="fa fa-plus"></i>
								</button>
							</div>
						</div>
					</div>
				</div>
				<table
					class="table table-striped table-bordered table-hover order-column">
					<thead>
						<tr>
							<th style="text-align: center;">그룹코드ID</th>
							<th style="text-align: center;">그룹코드명</th>
							<th style="text-align: center;">사용여부</th>
							<th style="text-align: center;">상세코드</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${empty groupCodeList.groupCodes }">
								<tr>
									<td colspan="4" align="center">데이터가 없습니다.</td>
								</tr>
							</c:when>
							<c:otherwise>
								<tr class="odd gradeX" style="text-align: center;">
									<c:set value="${groupCodeList.page_total_count}" var="count" />
									<c:forEach items="${groupCodeList.groupCodes}" var="groupCode" varStatus="status">
										<tr onclick="findGroupCodeDetail('${groupCode.group_code_id}')" style="cursor: pointer;">
											<td align="left" style="padding-left: 20px;">${groupCode.group_code_id}</td>
											<td align="left" style="padding-left: 20px;">${groupCode.group_code_name}</td>
											<c:choose>
												<c:when
													test="${template_yesNo.options[groupCode.use_flag] == '사용'}">
													<td style="text-align: center;vertical-align: middle;"><span
														class="label label-sm label-success"> 사용 </span></td>
												</c:when>
												<c:otherwise>
													<td style="text-align: center;vertical-align: middle;"><span
														class="label label-sm label-warning ">미사용 </span></td>
												</c:otherwise>
											</c:choose>
											<td style="text-align: center;" onclick='event.cancelBubble=true;'>
												<div class="btn-group">
													<button class="btn btn-xs btn-default" type="button" onclick="findCodeList('${groupCode.group_code_id}')">
														코드
													</button>
												</div>
											</td>
										</tr>
										<c:set var="count" value="${count - 1 }" />
									</c:forEach>
								</tr>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
	<div class="col-md-6" id="codeListDiv">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<form id="groupCodeForm" method="POST">
	<input type="hidden" name="group_code_id" /> <input type="hidden"
		name="code_id" /> <input type="hidden" name="main_menu_id"
		value="${paramBean.main_menu_id}" /> <input type="hidden"
		name="sub_menu_id" value="${paramBean.sub_menu_id}" /> <input
		type="hidden" name="current_menu_id" value="${currentMenuId}" /> <input
		type="hidden" name="page_num" />
</form>
<script type="text/javascript">
	var groupCodeConfig = {
		"codeListUrl":"${rootPath}/codeMngt/list.html"
		,"groupCodeDetailUrl":"${rootPath}/groupCodeMngt/detail.html"
		,"codeDetailUrl":"${rootPath}/codeMngt/detail.html"
		,"codeListElementId":"codeListDiv"
	};
</script>
