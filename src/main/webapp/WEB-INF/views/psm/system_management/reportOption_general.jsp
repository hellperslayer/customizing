<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<c:set var="currentMenuId" value="${index_id}" />
<c:set var="selSystemSeq" value="${sel_system_seq}" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />
<c:set var="adminUserAuthId" value="${userSession.auth_id}" />
<script type="text/javascript" charset="UTF-8">
	var auth = '${auth}';
</script>
<script src="${rootPath}/resources/js/psm/system_management/reportOption.js" type="text/javascript" charset="UTF-8"></script>

<form id="reloadForm" method="POST">
	<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" />
	<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
	<input type="hidden" name="current_menu_id" value="${currentMenuId }" />
	<input type="hidden" name="auth_id" value="${auth }"/>
</form>
<div class="row">
	<div class="col-md-12">
		<form id="optionForm" method="POST"> 
		<table class="table table-striped table-bordered order-column">
		 	
			<c:forEach items="${general}" var="item" varStatus="status">
			<c:set var="flag_name" value="${fn:split(item.flag_name, ',') }"/>
			<tr>
				<td width="20%" style="text-align: center; vertical-align: middle;">${item.option_name }</td>
				<td class="form-group form-md-line-input" style="text-align:center; vertical-align:middle;">
					<div class="md-radio-inline">
						<div class="md-radio col-md-4">
							<input type="radio" id="checkbox${status.count }_1" name="${item.option_id}" value="Y"
								class="md-radiobtn" <c:if test="${item.value eq 'Y' }">checked="checked"</c:if>/>
							<label for="checkbox${status.count }_1"> <span></span> 
								<span class="check"></span> 
								<span class="box"></span> ${flag_name[0] } </label>
						</div>
						<div class="md-radio col-md-4">
							<input type="radio" id="checkbox${status.count }_2" name="${item.option_id }" value="N"
								class="md-radiobtn" <c:if test="${item.value eq 'N' }">checked="checked"</c:if>/>
							<label for="checkbox${status.count }_2"> <span></span> 
								<span class="check"></span>
								<span class="box"></span> ${flag_name[1] } </label>
						</div>
					</div>
				</td>
				
				<!-- 사용자 권한 설정 -->
				<c:if test="${auth eq 'AUTH00000' }">
					<td> 
						<select class="form-control" style="text-align: center;" id="auth" name="admin_auth_${item.option_id}" >
							<option value ="AUTH00000" <c:if test='${item.auth eq "AUTH00000"}'>selected="selected"</c:if>>${authMap["AUTH00000"].auth_name }</option> 
							<option value ="AUTH00001" <c:if test='${item.auth eq "AUTH00001"}'>selected="selected"</c:if>>${authMap["AUTH00001"].auth_name }</option> 
							<option value ="AUTH00003" <c:if test='${item.auth eq "AUTH00003"}'>selected="selected"</c:if>>${authMap["AUTH00003"].auth_name }</option> 
						</select>
					</td>
				</c:if>
				
			</tr>
			</c:forEach>
		</table>
		</form>
	</div>
	
	 <div class="col-md-12" style="text-align: center">
		<a style="text-align: right" onclick="updGeneral()" class="btn btn-sm blue btn-outline sbold uppercase"><i class="fa fa-check"></i> 수정</a>
	 </div>
</div>


 