<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>
<%@taglib prefix="procdate" tagdir="/WEB-INF/tags"%>

<c:set var="currentMenuId" value="${index_id }" />
<ctl:checkMenuAuth menuId="${currentMenuId}" />
<ctl:drawNavMenu menuId="${currentMenuId}" />

<script src="${rootPath}/resources/js/common/ezDatepicker.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/js/psm/system_management/summaryMngtList.js" type="text/javascript" charset="UTF-8"></script>
<script src="${rootPath}/resources/assets/pages/scripts/table-datatables-ajax.min.js" type="text/javascript"></script>

<style>
#loading {
 width: 100%;   
 height: 100%;   
 top: 0px;
 left: 0px;
 position: fixed;   
 display: block;   
 opacity: 0.7;   
 background-color: #fff;   
 z-index: 99;   
 text-align: center; }  
  
#loading-image {   
 position: absolute;   
 top: 50%;   
 left: 50%;  
 z-index: 100; } 
</style>

<script type="text/javascript">
/* $(document).ready(function() {
	$('#loading').show();
});
 */
$(window).load(function() {   
	$('#loading').hide();
});
$(document).ready(function() {
	refreshReportList($('#report_type'));
});

</script>

<h1 class="page-title">${currentMenuName}</h1>

<div id="loading"><img id="loading-image" width="30px;" src="${rootPath}/resources/image/loading3.gif" alt="Loading..." /></div>

<div class="row">
	<div class="col-md-12">
		<div class="portlet light portlet-fit portlet-datatable bordered">
			<div class="portlet-body">
				<div class="table-container">
					<div id="datatable_ajax_2_wrapper"
						class="dataTables_wrapper dataTables_extended_wrapper dataTables_extended_wrapper no-footer DTS">
						<div>
							<div class="col-md-6" style="width: 100%; margin-top: 10px; padding-left: 0px; padding-right: 0px;">
								<div class="portlet box grey-salt  ">
			                         <div class="portlet-title" style="background-color: #2B3643;">
			                             <div class="caption">
			                                 <i class="fa fa-plus"></i>생성</div>
			                             <div class="tools">
			                                 <a id="searchBar_icon" href="" class="collapse"> </a>
			                             </div>
			                         </div>
			                         <div id="searchBar" class="portlet-body form">
			                             <div class="form-body" style="padding-left:20px; padding-right:10px;">
			                                 <div class="form-group">
			                                     <form id="listForm" method="POST" class="mt-repeater form-horizontal">
			                                         <div data-repeater-list="group-a">
			                                         	<div class="row">
															<div class="col-md-2">
			                                         		 	<label class="control-label">마감타입</label>
			                                         		 	<select class="form-control" id="summary_type" name="summary_type" onchange="setSummaryType(this.value)"> 
																	<option value="Daily">전체일마감</option>
																	<option value="Statistics">접속기록일마감</option>
																	<option value="Download">다운로드일마감</option>
																	<option value="Reqtype">수행업무일마감</option>
																	<option value="Time">시간별일마감</option>
																	<option value="Rule">비정상위험추출</option>
																	<option value="Abnormal">비정상위험마감</option>
																	<!-- <option value="day">일마감</option> -->
																	<option value="month">월마감</option>
																</select>
															</div>
															<div class="col-md-2" id="div_day">
			                                                     <label class="control-label">기간</label>
			                                                     <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
																	<input type="text" class="form-control" id="search_fr" name="search_from" value="<procdate:convertDate value="${paramBean.search_from}" />"> 
																	<span class="input-group-addon"> &sim; </span> 
																	<input type="text" class="form-control" id="search_to" name="search_to" value="<procdate:convertDate value="${paramBean.search_to}" />">
																 </div> 
															</div>
															<div class="col-md-2" id="div_month" style="display: none;">
															 	<c:set var="tmpDate" value="${paramBean.search_to }" />
															 	<c:set var="tmpYear" value="${fn:split(tmpDate, '-')[0] }" />
															 	<c:set var="tmpMonth" value="${fn:split(tmpDate, '-')[1] }" />
															 	<fmt:parseNumber value="${tmpMonth }" var="mon" />
																<label class="control-label">연월선택</label> 
																<div class="row">
				                                                    <div class="col-md-6">
				                                                    	<select class="form-control input-small" id="sel_year" name="sel_year">
					                                                     	<c:forEach var="i" begin="0" end="3">
					                                                     		<option value="${tmpYear - i}">${tmpYear - i}년</option>
					                                                     	</c:forEach>
																		</select>
																	</div>
																	<div class="col-md-6">
																		<select class="form-control input-xsmall" id="sel_month" name="sel_month">
					                                                     	<c:forEach var="i" begin="1" end="12">
					                                                     		<option value="${i}" <c:if test="${mon eq i }">selected="selected"</c:if>>${i}월</option>
					                                                     	</c:forEach>
																		</select>
																	</div>
																</div>
															</div>
														</div>
			                                         </div>
			                                         <hr/>
			                                         <div align="right">
			                                          	<button id="create_btn" type="button" class="btn btn-sm blue btn-outline sbold uppercase" onclick="createSummaryNew()">
															<i class="fa fa-check"></i> 생성
														</button>
													</div>		
			                                     </form>
			                                 </div>
			                             </div>
			                         </div>
			                     </div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-6">
								<h4>업로드 보고서 관리</h4>
								<div class="col-md-12" style="padding-left: 0px">
									<select class="form-control" id="report_type" name="report_type" style="width: 25%; float: left;" onchange="refreshReportList(this)">
										<c:forEach var="report" items="${reportcode }">
											<option value="${report.code_id }">${report.code_name }</option>
										</c:forEach>
									</select>	
								</div>
								<table id="user" class="table table-bordered table-striped" style="margin-bottom: 0px">
									<tbody>
										<tr>
											<th style="width: 10%; text-align: center;">
												<label class="control-label">번호</label>
											</th>
											<th style="width: 40%; text-align: center;">
												<label class="control-label">보고서 분류</label>
											</th>
											<th style="width: 20%; text-align: center;">
												<label class="control-label">업로드일자</label>
											</th>
											<th style="width: 20%; text-align: center;">
												<label class="control-label">삭제일자</label>
											</th>
											<th style="width: 10%; text-align: center;">
												<label class="control-label">삭제여부</label>
											</th>
										</tr>
									</tbody>
								</table>
								<!-- /psm/report/uploadReportList.jsp 페이지를 ajax로 include함-->
								<div id="tableInclude" style="max-height: 360px; overflow: overlay;">
								</div>
							</div>
							<div class="col-md-6">
								<h4>작업 진행현황</h4>
								<div style="color:red;">※완료시까지 웹페이지를 닫지마세요.</div>
								<div id="listDiv" class="portlet light portlet-fit portlet-datatable bordered" style="height: 360px; overflow: auto;">
							</div>
						</div>
						</div>
	    			</div>
				</div>
			</div>
		</div>
	</div>
</div>
