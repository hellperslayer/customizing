<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>
<c:set var="rootPath" value="${pageContext.servletContext.contextPath}" scope="application"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>UBI SAFER-PSM</title>
		
		<script type="text/javascript" charset="UTF-8">
	  		rootPath = '${rootPath}';
	  		contextPath = '${pageContext.servletContext.contextPath}';
	 	</script>
	 	<%@include file="../common/include/config.jsp"%> 
	</head>
	
	<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-closed">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <%-- <%@include file="../common/include/top.jsp"%> --%>
            <tiles:insertAttribute name="top" flush="true"/>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                       
                        <!-- BEGIN PAGE BAR -->
                        <%@include file="subTopMenu.jsp"%>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <!-- <h1 class="page-title"> Fixed Sidebar
                            <small>fixed sidebar option</small>
                        </h1> -->
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <%-- <jsp:include page='${viewName}.jsp' flush="false"/> --%>
                        <tiles:insertAttribute name="body"/>
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
                
                
                <!-- BEGIN QUICK SIDEBAR -->
                <%-- <%@include file="rightTopMenu.jsp"%> --%> 
                <!-- END QUICK SIDEBAR -->
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <%-- <%@include file="bottom.jsp"%>  --%>
            <tiles:insertAttribute name="footer"/>
            <!-- END FOOTER -->
        </div>
       
        <div class="quick-nav-overlay"></div>
        
	</body>

<form name ="menuForm" id="menuForm" method ="POST">
	<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }"/>
	<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
</form>
</html>

<script type="text/javascript">
	function menuMove(url, mainMenuId, subMenuId){
		$("#menuForm input[name=main_menu_id]").val(mainMenuId);
		$("#menuForm input[name=sub_menu_id]").val(subMenuId);
		$("#menuForm").attr("action",rootPath + url);
		$("#menuForm").submit();
	
	}
	
	function menuMoveAll(url, mainMenuId, subMenuId, system_seq){
		
		$("#menuSearchForm input[name=main_menu_id]").val(mainMenuId);
		$("#menuSearchForm input[name=sub_menu_id]").val(subMenuId);
		$("#menuSearchForm input[name=system_seq]").val(system_seq);
		$("#menuSearchForm").attr("action",rootPath + url);
		$("#menuSearchForm").submit();
		
	}
	
	
	function menuMoveReq(url, mainMenuId, subMenuId, system_seq){
		$("#menuSearchForm input[name=main_menu_id]").val(mainMenuId);
		$("#menuSearchForm input[name=sub_menu_id]").val(subMenuId);
		$("#menuSearchForm input[name=system_seq]").val(system_seq);
		$("#menuSearchForm").attr("action",rootPath + url);
	/* 	$("#menuSearchForm input[name=emp_user_id]").val(); */
		$("#menuSearchForm").submit();

	}
		

// 		function system(system_seq){
// 			var system_seq = num; 
// 			alert(system_seq);
// 			return system_seq;
// 		}
//  			$.ajax({
//  			type: 'POST',
//  			url: rootPath+ '/allLogInq/list.html',
//  			data: {
//  				system_seq : system_seq
//  			}
//  			});

		
	</script>