<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="page-sidebar-wrapper">
	<!-- BEGIN SIDEBAR -->
	<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
	<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
	<div class="page-sidebar navbar-collapse collapse">
		<!-- BEGIN SIDEBAR MENU -->
		<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
		<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
		<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
		<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		
		
		<!-- <ul class="page-sidebar-menu  page-header-closed" -->
		<ul class="page-sidebar-menu  page-header-closed page-sidebar-menu-closed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="100" style="padding-top: 0px">
		<!-- <ul id="leftMenuTableNode" class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 0px"> -->
		
			<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
			<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
			<li class="sidebar-toggler-wrapper hide">
				<div class="sidebar-toggler">
					<span></span>
				</div>
			</li>
			<!-- END SIDEBAR TOGGLER BUTTON -->
			<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
			<!-- <li class="sidebar-search-wrapper">
				BEGIN RESPONSIVE QUICK SEARCH FORM DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box
				DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box
				<form class="sidebar-search  sidebar-search-bordered"
					action="page_general_search_3.html" method="POST">
					<a href="javascript:;" class="remove"> <i class="icon-close"></i>
					</a>
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search...">
						<span class="input-group-btn"> <a href="javascript:;"
							class="btn submit"> <i class="icon-magnifier"></i>
						</a>
						</span>
					</div>
				</form> END RESPONSIVE QUICK SEARCH FORM
			</li> -->
			<li id="leftMenuTableNode" class="">
				
			</li>
		</ul>
		<!-- END SIDEBAR MENU -->
		<!-- END SIDEBAR MENU -->
	</div>
	<!-- END SIDEBAR -->
</div>

<script type="text/javascript">
	var subMenuTarget = "leftMenuTable";
	var subMenuStr = '${subMenu}';
	var menuNum= '${menuNum}';
	var menuCh= '${menuCh}';
	var menuremain = '${menutitle}';
 	var system_seq= '${paramBean.system_seq}'; 
	var subSys = '${subSys}';
	var submenu = "${paramBean.sub_menu_id}";
	var path = "${path}";
	
	var main_menu_id = "${paramBean.main_menu_id}";
	if (main_menu_id == '') {
		main_menu_id = "${search.main_menu_id}";
	}
	
	var sub_menu_id = "${paramBean.sub_menu_id}";
	if (sub_menu_id == '') {
		sub_menu_id = "${search.sub_menu_id}";
	}
	
	drawSubMenu(subMenuTarget, subMenuStr, main_menu_id, sub_menu_id, menuNum, menuCh, menuremain,system_seq, subSys, path);

</script>