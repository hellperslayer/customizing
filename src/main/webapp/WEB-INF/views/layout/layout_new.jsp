<%@page import="com.easycerti.eframe.common.util.SystemHelper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl"%>

<c:set var="rootPath" value="${pageContext.servletContext.contextPath}" scope="application" />
<c:set var="search_type" value="${session.getAttribute('search_type')}" scope="application"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>UBI SAFER-PSM</title>
<link rel="shortcut icon" type="image/x-icon" href="${rootPath}/resources/image/common/favicon_H2.ico" />
<script type="text/javascript" charset="UTF-8">
	rootPath = '${rootPath}';
	contextPath = '${pageContext.servletContext.contextPath}';
	search_type = '${search_type}';
</script>
<%@include file="../common/include/config.jsp"%>
</head>

<!-- <body id="leftStyle" class="page-header-fixed page-content-white"> -->
<c:choose>
<c:when test="${side_bar_view eq 'Y' and isSideBar eq true}">
<!-- <body id="leftStyle" class="page-header-fixed page-content-white" oncontextmenu='return false' ondragstart='return false' onselectstart='return false'> -->
<body id="leftStyle" class="page-header-fixed page-content-white">
</c:when>
<c:otherwise>
<!-- <body id="leftStyle" class="page-header-fixed page-content-white page-sidebar-closed" oncontextmenu='return false' ondragstart='return false' onselectstart='return false'> -->
<body id="leftStyle" class="page-header-fixed page-content-white page-sidebar-closed">
</c:otherwise>
</c:choose>
	<div class="page-wrapper">
		<!-- BEGIN HEADER -->
		<%-- <%@include file="../common/include/top.jsp"%> --%>
		<tiles:insertAttribute name="top" flush="true" />
		<!-- END HEADER -->
		<!-- BEGIN HEADER & CONTENT DIVIDER -->
		<div class="clearfix"></div>
		<!-- END HEADER & CONTENT DIVIDER -->
		<!-- BEGIN CONTAINER -->
		<div class="page-container">
			<!-- BEGIN SIDEBAR -->
			<%-- <%@include file="../common/include/left.jsp"%> --%>
			<tiles:insertAttribute name="left" />
			<!-- END SIDEBAR -->
			<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<!-- BEGIN CONTENT BODY -->
				<div class="page-content">
					<!-- BEGIN PAGE HEADER-->

					<!-- BEGIN PAGE BAR -->
					<%-- <%@include file="subTopMenu.jsp"%> --%>
					<!-- END PAGE BAR -->
					<!-- BEGIN PAGE TITLE-->
					<!-- <h1 class="page-title"> Fixed Sidebar
                            <small>fixed sidebar option</small>
                        </h1> -->
					<!-- END PAGE TITLE-->
					<!-- END PAGE HEADER-->
					<%-- <jsp:include page='${viewName}.jsp' flush="false"/> --%>
					<tiles:insertAttribute name="body" />
				</div>
				<!-- END CONTENT BODY -->
			</div>
			<!-- END CONTENT -->


			<!-- BEGIN QUICK SIDEBAR -->
			<%-- <%@include file="rightTopMenu.jsp"%> --%>
			<!-- END QUICK SIDEBAR -->
		</div>
		<!-- END CONTAINER -->
		<!-- BEGIN FOOTER -->
		<%-- <%@include file="bottom.jsp"%>  --%>
		<tiles:insertAttribute name="footer" />
		<!-- END FOOTER -->
	</div>

	<div class="quick-nav-overlay"></div>

</body>

<form id="menuForm" method="POST">
	<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }" /> 
	<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
</form>


<script type="text/javascript">
	// 		function system(system_seq){
	// 			var system_seq = num; 
	// 			alert(system_seq);
	// 			return system_seq;
	// 		}
	//  			$.ajax({
	//  			type: 'POST',
	//  			url: rootPath+ '/allLogInq/list.html',
	//  			data: {
	//  				system_seq : system_seq
	//  			}
	//  			});
</script>
</html>