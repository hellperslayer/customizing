<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script>
function doBlink() {
	var blink = document.all.tags("BLINK");
    var nowTime = $("#nowTime").val();
    var limitTime = $("#limitTime").val(); 

	for (var i=0; i < blink.length; i++)
	blink[i].style.visibility = blink[i].style.visibility == "" ? "hidden" : "";
	}
	function startBlink() {
	if (document.all)
	setInterval("doBlink()",500);
	}
	window.onload = startBlink;
</script>


<c:set var="userAuth" value="${userSession.auth_id }" />

<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner ">
		<!-- BEGIN LOGO -->
		<div class="page-logo" style="width:245px; padding-left: 50px;">
			<div class="menu-toggler sidebar-toggler" style="float: left; margin: 30px 0 0 -35px;">
				<span><div style="opacity: 0; position: relative; top: -10px;">AA</div></span>
			</div>
			<div>
			<a onclick="logout()"> <img
				src="${rootPath}/resources/assets/layouts/layout/img/logo_new.png"
				alt="logo" class="logo-default" />
			</a>
			</div>
		</div>
		<!-- END LOGO -->
		<div id="mainMenus" class="hor-menu   hidden-sm hidden-xs" style="vertical-align: middle;"></div>
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="menu-toggler responsive-toggler"
			data-toggle="collapse" data-target=".navbar-collapse"> <span></span>
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		
		<!-- 접속자 정보 -->
		<form id = "popForm" action="" method = "post">
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
				<!-- BEGIN NOTIFICATION DROPDOWN -->
				<!-- DOC: Apply "dropdown-dark" class after "dropdown-extended" to change the dropdown styte -->
				<!-- DOC: Apply "dropdown-hoverable" class after below "dropdown" and remove data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to enable hover dropdown mode -->
				<!-- DOC: Remove "dropdown-hoverable" and add data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to the below A element with dropdown-toggle class -->
				<!-- END NOTIFICATION DROPDOWN -->
				
				<!-- BEGIN USER LOGIN DROPDOWN -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<!-- END USER LOGIN DROPDOWN -->
				<!-- BEGIN QUICK SIDEBAR TOGGLER -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<li class="dropdown dropdown-quick-sidebar-toggler"><a
					onclick="logout()" class="dropdown-toggle" data-toggle="tooltip" title="로그아웃"> <i
						class="icon-logout"></i>
				</a></li>
				<!-- END QUICK SIDEBAR TOGGLER -->
			</ul>
		</div>
		</form>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END HEADER INNER -->
</div>

<script type="text/javascript">
var mainMenuStr = '${mainMenu}';
var mainMenuId = '${paramBean.main_menu_id}';
var subMenuStr = '${subMenu}';

var helpConfig = {
		"help_master" : "${rootPath}/reqLogInq/help_master.html",
		"help_manager" : "${rootPath}/reqLogInq/help_manager.html"
	};

if(mainMenuId == null || mainMenuId == '') {
	mainMenuId = '${mainMenuId}';
}

drawMainMenu("mainMenus", mainMenuStr, mainMenuId, subMenuStr);

/* if(mainMenuId == 'MENU00040' || mainMenuId == 'MENU00416' || mainMenuId == 'MENU00080' || mainMenuId == 'MENU00010') {
	$('#leftStyle').attr('class', 'page-header-fixed page-content-white');
}	 */ 

function logout(){
	$.ajax({
		type: 'POST',
		url: '${rootPath}/logout.html',
		success: function(data) {
			var log_message = "로그아웃 성공 : ${userSession.admin_user_id}"; 
			
			$.ajax({
				type: 'POST',
				url: rootPath + '/managerActHist/add.html',
				data: {
					menu_id : null,
					log_message : log_message,
					log_action : 'SELECT'
				}
			});
			
			location.href = rootPath + "/loginView.html";
		},
		error: function() {
			var log_message = "로그아웃 실패"; 
			
			$.ajax({
				type: 'POST',
				url: rootPath + '/managerActHist/add.html',
				data: {
					menu_id : null,
					log_message : log_message,
					log_action : 'SELECT'
				}
			});
		}
	});
}

function goMain(){
	var mainUrl = rootPath + '/allLogInq/list.html';
	var main_menu_id = "MENU00040";
	/* var mainUrl = rootPath + '/reqLogInq/list.html';
	var main_menu_id = "MENU00416"; */
	var sub_menu_id = "";
	
	if('${userAuth}' == 'AUTH00004') {
		mainUrl = rootPath + '/callingReplyUser/list.html';
		main_menu_id = "MENU00063";
		sub_menu_id = "";
	}
	
	$("#menuForm input[name=main_menu_id]").val(main_menu_id);
	$("#menuForm input[name=sub_menu_id]").val(sub_menu_id);
	$('#menuForm').attr('action', mainUrl);
	$('#menuForm').submit();
}

function menuMove(url, mainMenuId, subMenuId) {
	$("#menuForm input[name=main_menu_id]").val(mainMenuId);
	$("#menuForm input[name=sub_menu_id]").val(subMenuId);
	$("#menuForm").attr("action", rootPath + url);
	$("#menuForm").submit();	
}

function menuMoveAll(url, mainMenuId, subMenuId, system_seq) {
	$("#menuSearchForm input[name=main_menu_id]").val(mainMenuId);
	$("#menuSearchForm input[name=sub_menu_id]").val(subMenuId);
	$("#menuSearchForm input[name=system_seq]").val(system_seq);
	$("#menuSearchForm").attr("action", rootPath + url);
	$("#menuSearchForm").submit();

}

function menuMoveReq(url, mainMenuId, subMenuId, system_seq) {
	$("#menuSearchForm input[name=main_menu_id]").val(mainMenuId);
	$("#menuSearchForm input[name=sub_menu_id]").val(subMenuId);
	$("#menuSearchForm input[name=system_seq]").val(system_seq);
	$("#menuSearchForm").attr("action", rootPath + url);
	/* 	$("#menuSearchForm input[name=emp_user_id]").val(); */
	$("#menuSearchForm").submit();

}
</script>