<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>
<c:set var="rootPath" value="${pageContext.servletContext.contextPath}" scope="application"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=9" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>UBI SAFER-PSM</title>
		
		<script type="text/javascript" charset="UTF-8">
	  		rootPath = '${rootPath}';
	  		contextPath = '${pageContext.servletContext.contextPath}';
	 	</script>
		
		<link rel="stylesheet" href="${rootPath}/resources/css/common/jquery-ui.css" />
		<%-- <link rel="stylesheet" type="text/css" href="${rootPath}/resources/css/common/common.css" /> --%>
		<c:if test="${chng_layout eq 0}">
			<link rel="stylesheet" type="text/css" href="${rootPath}/resources/css/common/style_gun.css" />
		</c:if>
		<c:if test="${chng_layout eq 1}">
			<link rel="stylesheet" type="text/css" href="${rootPath}/resources/css/common/style.css" />
		</c:if>
		<link rel="stylesheet" type="text/css" href="${rootPath}/resources/css/common/reset.css" />
		
		<!-- 
			jquery 1.10 버젼의 경우 기능오작동 케이스 발견
			ex) checkbox, radio, select 등의 값 추출 및 선택기능
		 -->
<%-- 	<script src="${rootPath}/resources/js/common/jquery-1.10.2.js" type="text/javascript" charset="UTF-8"></script> --%>
		<script src="${rootPath}/resources/js/common/jquery-1.8.2.min.js" type="text/javascript" charset="UTF-8"></script>
		<script src="${rootPath}/resources/js/common/jquery-ui-1.9.0.custom.js" type="text/javascript" charset="UTF-8"></script>
		<script src="${rootPath}/resources/js/common/jquery.ui.datepicker-ko.js" type="text/javascript" charset="UTF-8"></script>
		<script src="${rootPath}/resources/js/common/menu-util.js" type="text/javascript" charset="UTF-8"></script>
		<script src="${rootPath}/resources/js/common/ajax-util.js" type="text/javascript" charset="UTF-8"></script>
		<script src="${rootPath}/resources/js/common/common-prototype-util.js" type="text/javascript" charset="UTF-8"></script>
		<script src="${rootPath}/resources/js/common/spin.js" type="text/javascript" charset="UTF-8"></script>
		<script src="${rootPath}/resources/js/common/jquery.form.js" type="text/javascript" charset="UTF-8"></script>
		<script src="${rootPath}/resources/js/layout/layout.js" type="text/javascript" charset="UTF-8"></script>
		<script src="${rootPath}/resources/js/common/PageNavigater.js" type="text/javascript" charset="UTF-8"></script>
		
	</head>
	
	<body>
		<div class="wrapper">
			<div class="header">
				<tiles:insertAttribute name="top" flush="true"/>
			</div>
			
			<c:if test="${mainFlag == null or mainFlag == 'N'}">
				<div class="left_area left">
					<tiles:insertAttribute name="left"/>
				</div>
				<div class="container">
					<tiles:insertAttribute name="body"/>
				</div>
			</c:if>
			 
			<c:if test="${mainFlag == 'Y'}">
				<div class="container_dash left">
					<tiles:insertAttribute name="body"/>
				</div>
			</c:if>
			
			<!-- 관리자 행위이력조회 주석처리 (사용자) -->
			<%-- <div class="footer">
				<tiles:insertAttribute name="footer"/>
			</div> --%>
		</div>
	</body>
	
	<!-- 메뉴 정보 담는 공통 폼 (검색 조건 초기화 시 사용) -->
	<form id="menuForm" method="POST">
		<input type="hidden" name="main_menu_id" value="${paramBean.main_menu_id }"/>
		<input type="hidden" name="sub_menu_id" value="${paramBean.sub_menu_id }" />
	</form>
	
	<div id="spinLayoutDiv" class="spinLayoutClass" />
	
	<script type="text/javascript">
		function menuMove(url, mainMenuId, subMenuId){
			$("#menuForm input[name=main_menu_id]").val(mainMenuId);
			$("#menuForm input[name=sub_menu_id]").val(subMenuId);
			$("#menuForm").attr("action",rootPath + url);
			$("#menuForm").submit();
		}
	</script>
</html>