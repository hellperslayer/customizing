<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!-- header -->
<!-- 로고  -->
<div class="logo left" onclick="goMain()"></div>

<!-- 대메뉴 -->	
<div id="mainMenus" class="lnb left"></div>
	
<!-- 접속자 정보 -->
<div class="gnb right">
	<p class="btn_4 right">
		<a onclick="logout()">로그아웃</a>
	</p>
	<span class="right">
		<strong><c:out value="" />${userSession.emp_user_name}</strong> 님
	</span>
</div>
<!-- e:header -->

<script type="text/javascript">
	var mainMenuStr = '${mainMenu}';
	var mainMenuId = '${paramBean.main_menu_id}';
	
	if(mainMenuId == null || mainMenuId == '') {
		mainMenuId = '${mainMenuId}';
	}
	
	// 사용자용 메뉴 하드코딩
	drawMainMenu("mainMenus", '[{"menu_id":"MENU00063","menu_name":"소명응답","menu_url":"/callingReplyUser/list.psm"}]', "MENU00063");
	
	function logout(){
		location.href = rootPath + "/logout.psm";
	}
	
	function goMain(){
		var mainUrl = rootPath + '/callingReplyUser/list.psm';
		var main_menu_id = "MENU00063";
		var sub_menu_id = "";
		
		$("#menuForm input[name=main_menu_id]").val(main_menu_id);
		$("#menuForm input[name=sub_menu_id]").val(sub_menu_id);
		$('#menuForm').attr('action', mainUrl);
		$('#menuForm').submit();
	}
	
</script>
