<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="page-footer">
	<div class="page-footer-inner">
		Copyright &copy;2019 <a target="_blank"
			href="http://www.easycerti.com" title="EASYCERTI">EASYCERTI</a>. All rights reserved.
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>


<script type="text/javascript">
var check = $("#searchBar").length;
if(check) {
	if(search_type == "Y") {
		$("#searchBar").css("display", "block");
		$("#searchBar_icon").attr("class", "collapse");
	}else {
		$("#searchBar").css("display", "none");
		$("#searchBar_icon").attr("class", "expand");
	}
}
</script>