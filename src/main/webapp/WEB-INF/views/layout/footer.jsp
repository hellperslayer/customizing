<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<script type="text/javascript">
	
	addManagerActHist();

	function addManagerActHist() {
		
		var menu_id = $('input[name=current_menu_id]').val();
		
		$.ajax({
			type: 'POST',
			url: rootPath + '/managerActHist/add.html',
			data: {
				menu_id : menu_id,
				log_message : 'SUCCESS',
				log_action : 'SELECT'
			}
		});
	}
	
</script>

<!-- Copyright(c)EASYCERTI Corp.All Rights Reserved. -->
