<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script>
function doBlink() {
	var blink = document.all.tags("BLINK");
    var nowTime = $("#nowTime").val();
    var limitTime = $("#limitTime").val(); 

	for (var i=0; i < blink.length; i++)
	blink[i].style.visibility = blink[i].style.visibility == "" ? "hidden" : "";
	}
	function startBlink() {
	if (document.all)
	setInterval("doBlink()",500);
	}
	window.onload = startBlink;
</script>


<c:set var="userAuth" value="${userSession.auth_id }" />

<!--  Modal  -->
<div id="myModalPw" class="modal fade" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog" style="position: relative ;top: 20%; left: 0;">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #32c5d2;">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><b style="color: white;">비밀번호 변경</b></h4>
			</div>
			<div class="modal-body">
				<div class="scroller" style="height: 300px" data-always-visible="1" data-rail-visible1="1">
					<div class="row">
						<div class="col-md-12">
						<form id="repasswordForm" method="post">
							<p>
								<b>현재 비밀번호</b><input id="curr_pwd" type="password" class="col-md-12 form-control" autocomplete="off">
							</p>
							<br>
							<h5 style="color:red;">비밀번호는 9 ~ 20자 사이의 문자, 숫자, 특수문자(!@#$%&*) 조합으로 구성되어야합니다. </h5>
							<p>
								<b>새 비밀번호</b><input id="new_pwd" type="password" class="col-md-12 form-control" autocomplete="off">
							</p>
							<br>
							<p>
								<b>새 비밀번호 확인</b><input id="check_pwd" type="password" class="col-md-12 form-control" autocomplete="off">
							</p>
						</form>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer" style="margin-right: 12px;">
				<button type="button" data-dismiss="modal" class="btn btn-sm red-mint btn-outline"><i class="fa fa-remove"></i> 취소</button>
				<button type="button" class="btn btn-sm blue btn-outline" onclick="pwdChange()"><i class="fa fa-check"></i> 변경하기</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal end -->
	
<form id="eventForm" method="POST" action="${rootPath}/allLogInq/list.html">
	<input type="hidden" name="search_from_rp"/>
	<input type="hidden" name="search_to_rp"/>
	<input type="hidden" name="system_seq_rp"/>
	<input type="hidden" name="dept_name_rp"/>
	<input type="hidden" name="privacyType_rp"/>
	<input type="hidden" name="req_type_rp"/>
	<input type="hidden" name="emp_user_id_rp"/>
	<!-- <input type="hidden" name="start_h"/>
	<input type="hidden" name="end_h"/> -->
	<input type="hidden" name="scen_seq_rp"/>
</form>

<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner ">
		<!-- BEGIN LOGO -->
		<div class="page-logo" style="width:245px; padding-left: 50px;">
			<div class="menu-toggler sidebar-toggler" style="float: left; margin: 30px 0 0 -35px;">
				<span><div style="opacity: 0; position: relative; top: -10px;">AA</div></span>
			</div>
			<div>
			<a onclick="goMain()"> <img
				src="${rootPath}/resources/assets/layouts/layout/img/logo_new.png"
				alt="logo" class="logo-default" />
			</a>
			</div>
		</div>
		<!-- END LOGO -->
		<div id="mainMenus" class="hor-menu   hidden-sm hidden-xs" style="vertical-align: middle;"></div>
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="menu-toggler responsive-toggler"
			data-toggle="collapse" data-target=".navbar-collapse"> <span></span>
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		
		<!-- 접속자 정보 -->
		<form id = "popForm" action="" method = "post">
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
				<!-- BEGIN NOTIFICATION DROPDOWN -->
				<!-- DOC: Apply "dropdown-dark" class after "dropdown-extended" to change the dropdown styte -->
				<!-- DOC: Apply "dropdown-hoverable" class after below "dropdown" and remove data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to enable hover dropdown mode -->
				<!-- DOC: Remove "dropdown-hoverable" and add data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to the below A element with dropdown-toggle class -->
				<li class="dropdown dropdown-extended dropdown-notification"
					id="header_notification_bar"><a onclick="help()"
					class="dropdown-toggle" data-toggle="tooltip" 
					data-hover="dropdown" title="도움말" data-close-others="true" style="color: #c6cfda;">V3.0&nbsp;<i class="icon-question"></i>
				</a>
				</li>
				<!-- END NOTIFICATION DROPDOWN -->
				
				
				<!-- BEGIN USER LOGIN DROPDOWN -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<li class="dropdown dropdown-user">
					<a data-toggle="modal" data-target="#myModalPw" class="dropdown-toggle"> 
						<img alt="" class="img-circle" src="${rootPath}/resources/assets/layouts/layout/img/profile_user.png" />
						<span class="username username-hide-on-mobile"> <strong><c:out value="${userSession.admin_user_name}" /></strong> 님 </span> 
					</a>
					<%-- <a class="dropdown-toggle" onclick="openChangePassword()">
						<img alt="" class="img-circle" src="${rootPath}/resources/assets/layouts/layout/img/profile_user.png" />
						<span class="username username-hide-on-mobile"> <strong><c:out value="${userSession.admin_user_name}" /></strong> 님 </span> 
					</a> --%>
				</li>
				<!-- END USER LOGIN DROPDOWN -->
				<!-- BEGIN QUICK SIDEBAR TOGGLER -->
				<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
				<li class="dropdown dropdown-quick-sidebar-toggler"><a
					onclick="logout()" class="dropdown-toggle" data-toggle="tooltip" title="로그아웃"> <i
						class="icon-logout"></i>
				</a></li>
				<!-- END QUICK SIDEBAR TOGGLER -->
				
				<c:if test ="${nowTime >= limitTime}">
				<font class="limitd right" >
					<blink><strong style="color: #FF5E00">유지보수 기한이 만료되었습니다.</strong></blink> 
				</font>
				</c:if>
			</ul>
		</div>
		</form>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END HEADER INNER -->
</div>

<input type = "hidden" id = "nowTime" name = "nowTime" value = "${nowTime}">
<input type = "hidden" id = "limitTime" name = "limitTime" value = "${limitTime}">

<script type="text/javascript">
//모달 창 닫을 때 값 초기화
$('.modal').on('hidden.bs.modal', function (e) {
    console.log('modal close');
  $(this).find('#repasswordForm')[0].reset()
});

var mainMenuStr = '${mainMenu}';
var mainMenuId = '${paramBean.main_menu_id}';
var subMenuStr = '${subMenu}';

var helpConfig = {
		"help_master" : "${rootPath}/reqLogInq/help_master.html",
		"help_manager" : "${rootPath}/reqLogInq/help_manager.html"
	};

if(mainMenuId == null || mainMenuId == '') {
	mainMenuId = '${mainMenuId}';
}

drawMainMenu("mainMenus", mainMenuStr, mainMenuId, subMenuStr);

/* if(mainMenuId == 'MENU00040' || mainMenuId == 'MENU00416' || mainMenuId == 'MENU00080' || mainMenuId == 'MENU00010') {
	$('#leftStyle').attr('class', 'page-header-fixed page-content-white');
}	 */ 

function logout(){
	var admin_user_id = "${userSession.admin_user_id}";
	
	$.ajax({
		type: 'POST',
		url: '${rootPath}/logout.html',
		success: function(data) {
			//var log_message = "로그아웃 성공 : ${userSession.admin_user_id}";
			var log_message = "로그아웃 성공 : " + admin_user_id;
			
			$.ajax({
				type: 'POST',
				url: rootPath + '/managerActHist/add.html',
				data: {
					menu_id : 'logout',
					log_message : log_message,
					log_action : 'SELECT',
					admin_user_id_temp : admin_user_id
				}
			});
			
			location.href = rootPath + "/loginView.html";
		},
		error: function() {
			var log_message = "로그아웃 실패"; 
			
			$.ajax({
				type: 'POST',
				url: rootPath + '/managerActHist/add.html',
				data: {
					menu_id : 'logout',
					log_message : log_message,
					log_action : 'SELECT'
				}
			});
		}
	});
}

function goMain(){
	var mainUrl = rootPath + '${empty init_login_page? "/allLogInq/list.html" : init_login_page}';
	var main_menu_id = "MENU00040";
	/* var mainUrl = rootPath + '/reqLogInq/list.html';
	var main_menu_id = "MENU00416"; */
	/* var mainUrl = rootPath + '/dashboard/init_dashboardView_new.html';
	var main_menu_id = "MENU00030"; */
	var sub_menu_id = "";
	
	/* if('${userAuth}' == 'AUTH00004') {
		mainUrl = rootPath + '/callingReplyUser/list.html';
		main_menu_id = "MENU00063";
		sub_menu_id = "";
	} */
	
	if('${userAuth}' == 'AUTH00004') {
		mainUrl = rootPath + '/extrtCondbyInq/summonManageList.html';
		main_menu_id = "MENU00499";
		sub_menu_id = "";
	}
	
	if('${userAuth}' == 'AUTH20000') {
		mainUrl = rootPath + '/downloadLogInq/list.html';
		main_menu_id = "MENU00544";
		sub_menu_id = "";
	}
	
	$("#menuForm input[name=main_menu_id]").val(main_menu_id);
	$("#menuForm input[name=sub_menu_id]").val(sub_menu_id);
	$('#menuForm').attr('action', mainUrl);
	$('#menuForm').submit();
}

function goAdvice(){
	var url = "/psm_advice";
	var newWindow = window.open("about:blank");
	newWindow.location.href = url ;
}

function help(){
	
/* 	 var myForm = $("#popForm");
	 var url = "helpConfig['help']";
	 window.open("" ,"popForm", 
	        "toolbar=no, width=540, height=467, directories=no, status=no,    scrollorbars=no, resizable=no"); 
	 myForm.action =url; 
	 myForm.method="post";
	 myForm.target="popForm";

	myForm.submit(); */
	
	 var frm = document.getElementById("popForm");
	  window.open('', 'viewer', 'height=700px, width=835px, scrollbars=yes');
	 if('${userAuth}' == 'AUTH00000' || '${userAuth}' == 'AUTH00001'){
		 frm.action = helpConfig['help_master'];
	 }else{
		 frm.action = helpConfig['help_manager'];
	 }
	 frm.target = "viewer";
	 frm.method = "post";
	 frm.submit();    

	
	// window.open(helpConfig['help'], 'helpPop', 'height=800px, width=635px, scrollbars=yes'); 
} 

//새 비밀번호 입력 칸에서 엔터 입력시
$("#check_pwd").keydown(function(e){
	if(e.keyCode==13){
		pwdChange();
	}
});

$("#new_pwd").keydown(function(e){
	if(e.keyCode==13){
		pwdChange();
	}
});

function pwdChange() {
	if(checkPwd()) {
		var currentPasswd = $("#curr_pwd").val();
		var newPasswd = $("#new_pwd").val();
		var checkPasswd = $("#check_pwd").val();
		
		$.ajax({
			type: 'POST',
			url: '${rootPath}/changePassword.html',
			data: {
				currentPasswd : currentPasswd,
				newPasswd : newPasswd,
				checkPasswd : checkPasswd
			},
			success: function(data) {
				if( data.res == "ERROR01"){
					alert("비밀번호는 9 ~ 20자 사이의 문자, 숫자, 특수문자 조합으로 구성되어야합니다. (공백문자 제외)");
					$("#new_pwd").focus();
				}else if ( data.res == "SUCCESS" ) {
					alert("변경되었습니다.");
					//location.reload();
					var log_message = "비밀번호 변경: " + data.paramBean.admin_user_id;
					addManagerActHist(null, log_message, "SELECT");
					logout();
				} else if ( data.res == "WRONG_PWD" ) {
					alert("현재 비밀번호를 정확하게 입력해 주세요.");
					$("#curr_pwd").focus();
				} else if( data.res == "ALREADY_USED"){
					alert("최근 3회 이내 사용한 비밀번호로는 변경이 불가능합니다. ");
					$("#new_pwd").focus();
				}
			}
		});
	}	 
}

function addManagerActHist(menu_id, log_message, log_action) {
	$.ajax({
		type: 'POST',
		url: '${rootPath}/managerActHist/add.html',
		data: {
			menu_id : menu_id,
			log_message : log_message,
			log_action : log_action
		}
	});
}

function checkPwd(){
	var currentPasswd = $("#curr_pwd").val();
	var newPasswd = $("#new_pwd").val();
	var re_newPasswd = $("#check_pwd").val();
	//var regex = /^.*(?=^.{9,20}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[~,!,@,#,$,%,^,&,*,(,),=,+,_,.,|]).*$/;
	
	if ( currentPasswd != '' && newPasswd != '') {
		if ( currentPasswd == newPasswd ) {
			alert("기존 비밀번호와 다르게 입력해 주세요.");
			return false;
		}
	}
	
	if(newPasswd != '' && re_newPasswd != '') {
		/* if(!regex.test(newPasswd)){
			alert("비밀번호는 9 ~ 20자 사이의 문자, 숫자, 특수문자 조합으로 구성되어야합니다. (공백문자 제외)");
			$('input[name=password]').select();
			return false;
		} */
		
		if(newPasswd != re_newPasswd){
			alert("비밀번호와 비밀번호 확인이 일치하지 않습니다.");
			$('input[name=password]').select();
			return false;
		}
		
		/* if(newPasswd.indexOf(" ") > -1 ){
			alert("비밀번호에 공백문자를 포함할 수 없습니다.");
			$('input[name=password]').select();
			return false;
		} */
	}else{
		alert("필수항목을 모두 입력해주세요.");
		return false;
	}
	return true;	
}

function openChangePassword() {
	var url = "${rootPath}/changePasswordView.html";
	window.open(url, 'Popup', 'height=700px, width=800px, scrollbars=yes');
}
function menuMove(url, mainMenuId, subMenuId) {
	if(url == '' || url == null)return;
	$("#menuForm input[name=main_menu_id]").val(mainMenuId);
	$("#menuForm input[name=sub_menu_id]").val(subMenuId);
	$("#menuForm").attr("action", rootPath + url);
	$("#menuForm").submit();	
}

function menuMoveAll(url, mainMenuId, subMenuId, system_seq) {
	if(url == '' || url == null)return;
	$("#menuSearchForm input[name=main_menu_id]").val(mainMenuId);
	$("#menuSearchForm input[name=sub_menu_id]").val(subMenuId);
	$("#menuSearchForm input[name=system_seq]").val(system_seq);
	$("#menuSearchForm").attr("action", rootPath + url);
	$("#menuSearchForm").submit();

}

function menuMoveReq(url, mainMenuId, subMenuId, system_seq) {
	if(url == '' || url == null)return;
	$("#menuSearchForm input[name=main_menu_id]").val(mainMenuId);
	$("#menuSearchForm input[name=sub_menu_id]").val(subMenuId);
	$("#menuSearchForm input[name=system_seq]").val(system_seq);
	$("#menuSearchForm").attr("action", rootPath + url);
	/* 	$("#menuSearchForm input[name=emp_user_id]").val(); */
	$("#menuSearchForm").submit();

}


</script>