<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="page left" id="pagingframe"></div>
<script src="${rootPath}/resources/js/common/PageNavigater.js" type="text/javascript" charset="UTF-8"></script>
<script type="text/javascript">
	var pageNavigator = new PageNavigator("${search.size}", "${search.page_num}", "${search.total_count}", 10);
 	pageNavigator.showInfo(true);
 	$("#pagingframe").html(pageNavigator.getHtml());
 	
 	function goPage(num){
 		var $lstForm = $('#listForm');
 		$lstForm.find($('input[name=page_num]')).val(num);
 		$lstForm.submit();
 	}
</script>
<br /><br />

