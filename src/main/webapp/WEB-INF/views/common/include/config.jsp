<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<c:set var="rootPath" value="${pageContext.servletContext.contextPath}" scope="application"/>

	<!-- <meta http-equiv="X-UA-Compatible" content="IE=8" /> -->
	  <meta charset="utf-8" />
	   <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #1 for dark mega menu option" name="description" />
        <meta content="" name="author" />
        
         <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> -->
        <link href="${rootPath}/resources/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="${rootPath}/resources/assets/global/plugins/font-awesome/css/font-awesome_5.11.2.min.css" rel="stylesheet" type="text/css" />
        <link href="${rootPath}/resources/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="${rootPath}/resources/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${rootPath}/resources/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        
        <link href="${rootPath}/resources/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="${rootPath}/resources/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="${rootPath}/resources/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="${rootPath}/resources/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="${rootPath}/resources/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="${rootPath}/resources/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="${rootPath}/resources/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="${rootPath}/resources/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        
        <%-- <link href="${rootPath}/resources/css/common/style_gun.css" rel="stylesheet" type="text/css" /> --%>
<!--         <link rel="shortcut icon" href="favicon.ico" />  -->
        
      <%-- <script src="${rootPath}/resources/assets/global/plugins/jquery.min.js" type="text/javascript"></script> --%>
      <script src="${rootPath}/resources/js/common/jquery-1.10.2.js" type="text/javascript" charset="UTF-8"></script> 
<%-- 	<script src="${rootPath}/resources/js/common/jquery-1.8.2.min.js" type="text/javascript" charset="UTF-8"></script> --%>
    <script src="${rootPath}/resources/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="${rootPath}/resources/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="${rootPath}/resources/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="${rootPath}/resources/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="${rootPath}/resources/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    
    <!-- AMCHART SCRIPT START-->
    <script src="${rootPath}/resources/js/amchart/amcharts.js"></script>
	<script src="${rootPath}/resources/js/amchart/serial.js"></script>
	<script src="${rootPath}/resources/js/amchart/export.min.js"></script>
	<script src="${rootPath}/resources/js/amchart/amstock.js"></script>
	<script src="${rootPath}/resources/js/amchart/xy.js"></script>
	<script src="${rootPath}/resources/js/amchart/light.js"></script>
	<script src="${rootPath}/resources/js/amchart/gauge.js"></script>
	<script src="${rootPath}/resources/js/amchart/gantt.js"></script>
	<script src="${rootPath}/resources/js/amchart/pie.js"></script>
	<script src="${rootPath}/resources/js/amchart/radar.js"></script>
	<link rel="stylesheet" href="${rootPath}/resources/css/amchart/export.css" type="text/css" media="all" />
    <!-- AMCHART SCRIPT END -->
    
    <!-- END CORE PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="${rootPath}/resources/assets/global/scripts/app.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="${rootPath}/resources/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
    <script src="${rootPath}/resources/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
    <script src="${rootPath}/resources/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
    <script src="${rootPath}/resources/assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
    
    <script src="${rootPath}/resources/assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="${rootPath}/resources/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="${rootPath}/resources/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
    <script src="${rootPath}/resources/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <%-- <script src="${rootPath}/resources/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script> --%>
    
    <!-- 시군구 : 0 , 아카데미 : 1  -->
	<c:if test="${chng_layout eq 0}">
	<script src="${rootPath}/resources/js/common/menu-util_gun.js" type="text/javascript" charset="UTF-8"></script>
	</c:if>
	<c:if test="${chng_layout eq 1}">
	<script src="${rootPath}/resources/js/common/menu-util.js" type="text/javascript" charset="UTF-8"></script>
	</c:if>
	
	
	<script src="${rootPath}/resources/js/common/ajax-util.js" type="text/javascript" charset="UTF-8"></script>
	<script src="${rootPath}/resources/js/common/common-prototype-util.js" type="text/javascript" charset="UTF-8"></script>
	<script src="${rootPath}/resources/js/common/spin.js" type="text/javascript" charset="UTF-8"></script>
	<script src="${rootPath}/resources/js/common/jquery.form.js" type="text/javascript" charset="UTF-8"></script>
	<%-- <script src="${rootPath}/resources/js/common/PageNavigater.js" type="text/javascript" charset="UTF-8"></script>  --%>
<%-- 	<script src="${rootPath}/resources/js/common/jquery-ui-1.9.0.custom.js" type="text/javascript" charset="UTF-8"></script> --%>
	<%-- <script src="${rootPath}/resources/js/common/jquery.ui.datepicker-ko.js" type="text/javascript" charset="UTF-8"></script> --%>
		
<%--          <script src="${rootPath}/resources/assets/global/plugins/nouislider/wNumb.min.js" type="text/javascript"></script>
    <script src="${rootPath}/resources/assets/global/plugins/nouislider/nouislider.min.js" type="text/javascript"></script>
    <script src="${rootPath}/resources/assets/pages/scripts/components-nouisliders.min.js" type="text/javascript"></script> --%>
  	
  	<c:set var="textSearch" value="클릭시 검색창이 나타납니다." scope="application"/>
  	
<style type="text/css">
.input_date {
	background-color: #eef1f5; 
	padding: 0.3em 0.5em; 
	border: 1px solid #e4e4e4; 
	height: 30px; 
	font-size: 12px; 
	color: #555;
}

</style>