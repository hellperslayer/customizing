<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="rootPath" value="${pageContext.servletContext.contextPath}" scope="application"/>
<script src="${rootPath}/resources/js/common/jquery-1.10.2.js" type="text/javascript" charset="UTF-8"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#goForm").submit();
});
</script>
<form id="goForm" action="${rootPath}/allLogInq/list.html" method="POST"></form>