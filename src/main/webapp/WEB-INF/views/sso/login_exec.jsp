<!--
 ***********************************************
 * @source      : login_exec.jsp
 * @description : 세션생성
 ***********************************************
 * DATE         AUTHOR    DESCRIPTION
 * ---------------------------------------------
 * 2006/01/12   윤남정          최초작성
 ***********************************************
 -->
<%@page import="org.springframework.beans.factory.annotation.Value"%>
<%@page import="com.easycerti.eframe.common.util.JDBCConnectionUtil"%>
<%@page import="com.easycerti.eframe.psm.system_management.service.impl.EmpUserMngtSvcImpl"%>
<%@page import="com.easycerti.eframe.psm.system_management.dao.EmpUserMngtDao"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="com.easycerti.eframe.psm.system_management.vo.EmpUser"%>
<%@page import="com.easycerti.eframe.common.util.CommonHelper"%>
<%@page import="com.easycerti.eframe.psm.system_management.dao.AdminUserMngtDao"%>
<%@page import="com.initech.eam.nls.CookieManager"%>
<%@ page language="java" contentType="text/html;charset=EUC-KR" %>
<%@ page import="java.util.*" %>
<%@ page import="com.initech.eam.nls.*" %>
<%@ page import="com.initech.eam.nls.CookieManager" %>
<%@ page import="com.initech.eam.nls.command.*" %>
<%@ page import="com.initech.eam.smartenforcer.*" %>

<%@ include file="config.jsp" %>
<%
	//CookieManager.addCookie("InitechEamUID", "Z2019041", "", response);
	//CookieManager.addCookie("InitechEamUTOA", "3", "", response);
	
	// 쿠키 암호화 적용 -- 암호화 안되어 있다면 주석처리
	CookieManager.setEncStatus(false);
	
	String sso_id = null;
	String uurl = null; 
	//mj 추가
	String TOA = null;
	
		//1.SSO ID 수신
		sso_id = getSsoId(request); 
		//2.UURL 수신
		uurl = request.getParameter("UURL");
		//3.로그인 타입(1. 1 id/passwd  3 인증서)
		TOA = CookieManager.getCookieValue("InitechEamUTOA", request);
		
		
		/* TOA = "3";

		if(true) {
			sso_id = "Z2019043";
			session.setAttribute("SSO_ID", sso_id);
			String desc_access_cd = null;
			
			Map psmFindColumn = new HashMap<String,String>();
			psmFindColumn.put("DESC_ACCESS_CD", "string");
			Object[] psmParam = {};
			String sql = "SELECT DESC_ACCESS_CD FROM EMP_USER WHERE EMP_USER_ID = '"+sso_id+"'";
			List psmRes = JDBCConnectionUtil.ConnectionResult("org.postgresql.Driver", "jdbc:postgresql://192.1.1.185:5432/psm", "psm", "psm#redcross1!", sql, psmParam, psmFindColumn);
			if (psmRes.size() > 0) {
				Map psmResMap = (Map) psmRes.get(0);
				desc_access_cd = (String)psmResMap.get("DESC_ACCESS_CD");
			}
			if(desc_access_cd.equals("1")) {
				response.sendRedirect("login.html");	
			} else {
				response.sendRedirect("http://localhost:7070/psm_summon/requestList.do?id="+sso_id);
			}
			
			return;
		} */
		
		System.out.println("로그인 타입 : "+TOA);
	
		System.out.println("*================== [login_exec.jsp]  sso_id = "+sso_id);
		System.out.println("*================== [login_exec.jsp]  uurl ="+uurl);
		//인증서 -> TOA 가 3인거 체크 하는 로직!
		if(TOA == null){
			sso_id = null;
			System.out.println("TOA value : " + TOA);
		}
		
		else if(!TOA.equals("3")){
			sso_id = null;
			System.out.println("TOA value : " + TOA);
			
		}
		
		
		if (sso_id == null ) {
			//3.SSO ID 가 없다면 로그인 페이지로 이동
			if (uurl == null)	uurl = ASCP_URL;
			// uurl ===> PIMS URL
			goLoginPage(response, "http://pims.redcross.or.kr/logincheck2");
			return;
		} else {
			
			//4.쿠키 유효성 확인 :0(정상) 
			String retCode = getEamSessionCheck(request,response);
			if(!retCode.equals("0")){
				goErrorPage(response, Integer.parseInt(retCode));	
				return;
			}
			//5.업무시스템에 읽을 사용자 아이디를 세션으로 생성
			String EAM_ID = (String)session.getAttribute("SSO_ID");
			if(EAM_ID == null || EAM_ID.equals("")) {
				session.setAttribute("SSO_ID", sso_id);
				session.setAttribute("TOA", TOA);
				session.setAttribute("type", "3");
			}
			out.println("SSO 인증 성공!!");
			
			//권한가져오기
			String desc_access_cd = null;
			Map psmFindColumn = new HashMap<String,String>();
			psmFindColumn.put("DESC_ACCESS_CD", "string");
			Object[] psmParam = {};
			String sql = "SELECT DESC_ACCESS_CD FROM EMP_USER WHERE EMP_USER_ID = '"+sso_id+"'";
			List psmRes = JDBCConnectionUtil.ConnectionResult("org.postgresql.Driver", "jdbc:postgresql://192.1.1.185:5432/psm", "psm", "psm#redcross1!", sql, psmParam, psmFindColumn);
			if (psmRes.size() > 0) {
				Map psmResMap = (Map) psmRes.get(0);
				desc_access_cd = (String)psmResMap.get("DESC_ACCESS_CD");
			}
			if(desc_access_cd.equals("1")) {
				response.sendRedirect("login.html");	
			} else {
				response.sendRedirect("http://localhost:7070/psm_summon/requestList.do?id="+sso_id);
			}

			//6.업무시스템 페이지 호출(세션 페이지 또는 메인페이지 지정)  --> 업무시스템에 맞게 URL 수정!
			
			out.println("인증성공");
		}
%>
