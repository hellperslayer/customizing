<%@page import="com.initech.eam.nls.CookieManager"%>
<%@ page
	import="sun.misc.*,
			java.util.*,
			java.math.*,
			java.net.*,
			com.initech.eam.nls.*,
			com.initech.eam.nls.CookieManager,
			com.initech.eam.api.*,
			com.initech.eam.base.*,
			com.initech.eam.nls.command.*,
			com.initech.eam.smartenforcer.*, 
			examples.api.*"
%>
<%! 
	/************************************************************************
	* 업무시스템 설정 사항 (업무 환경에 맞게 변경)
	*************************************************************************/
	// 아래의 이름에 해당하는 어플리케이션이 SSO에 자원으로 등록되어 있어야 함
	private String SERVICE_NAME = "pims";
	//private String SERVICE_NAME = "monitor";
	// 업무시스템 접속 URL
	private String SERVER_URL = "http://pims.redcross.or.kr";
	//private String SERVER_URL = "http://localhost:8080/monitor/";
	// 업무시스템 접속 포트
	private String SERVER_PORT = "8080";
	//private String SERVER_PORT = "8080";
	//private String SERVER_PORT = "8080";
	// ASCP (업무시스템에 접근 시 SSO 인증 여부 체크 및 세션 처리
	private String ASCP_URL
		//= SERVER_URL + ":" + SERVER_PORT + "/PSM_CONSOLE_REDCROSS/ssologin";
	= SERVER_URL + ":" + SERVER_PORT + "/monitor/pimsadmin";
	//private String ASCP_URL
	//	= SERVER_URL + ":" + SERVER_PORT + "ssologin";
	//기본값 유지
	private String[] SKIP_URL
		= {"", "/", "/index.html", "/index.htm", "/index.jsp", "/index.asp"};

	/************************************************************************
	* SSO/EAM 환경 설정 사항 (변경사항 없음)
	*************************************************************************/
	// SSO 통합 로그인페이지 접속 URL
	//private String NLS_URL = "http://sso.redcross.or.kr";
	private String NLS_URL = "http://sso.redcross.or.kr";
	// SSO 통합 로그인페이지 접속 포트
	private String NLS_PORT = "";
	// SSO 통합 로그인을 위해 NLS로 redirect할 전체 URL (fail-over는 L4 기준)
	private String NLS_LOGIN_URL
		//= NLS_URL + ":" + NLS_PORT + "/nls3/clientLoginProcess.jsp?RTOA=3&UURL=http://pims.redcross.or.kr";
	= NLS_URL + ":" + NLS_PORT + "/nls3/clientLogin.jsp";
	// SSO 통합 로그아웃 URL
	private String NLS_LOGOUT_URL
		= NLS_URL + ":" + NLS_PORT + "/nls3/logout.jsp";
	// SSO 에러 페이지 URL
	private String NLS_ERROR_URL
		= NLS_URL + ":" + NLS_PORT + "/nls3/error.jsp";
	// Nexess API가 사용할 Nexess Daemon의 주소
	//private String ND_URL = NLS_URL + ":5480";
	private String ND_URL1 = "http://192.1.1.52:5480";
	private String ND_URL2 = "http://192.1.1.53:5480";
	// 인증 타입 (ID/PW 방식 : 1)
	private String TOA = "3";
	// 도메인 base (.initech.com)
	private String SSO_DOMAIN = ".redcross.or.kr";
%>

<%!
	// EAM 데몬 접속
	public NXContext getContext()
	{
		NXContext context = null;
		try {
			List serverurlList = new ArrayList();
			serverurlList.add(ND_URL1); 
			serverurlList.add(ND_URL2); 
			context = new NXContext(serverurlList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return context;
	}

	// 통합 SSO 로그인페이지 이동
	public void goLoginPage(HttpServletResponse response, String uurl)
	throws Exception {
		CookieManager.addCookie(SECode.USER_URL, uurl, SSO_DOMAIN, response);
		CookieManager.addCookie(SECode.R_TOA, TOA, SSO_DOMAIN, response);
		response.sendRedirect(NLS_LOGIN_URL);
		
	}

	// SSO 에러페이지 URL
	public void goErrorPage(HttpServletResponse response, int error_code)
	throws Exception {
		CookieManager.removeNexessCookie(SSO_DOMAIN, response);
		response.sendRedirect(NLS_ERROR_URL + "?errorCode=" + error_code);
	}

	// 통합 SSO ID 조회
	public String getSsoId(HttpServletRequest request) {
		String sso_id = null;

		sso_id = CookieManager.getCookieValue(SECode.USER_ID, request);
		return sso_id;
	}

	// EAM 데몬 상태 체크
	public boolean getStatus() {

		boolean bResult = false;

		Environment env = new Environment();
		bResult = env.getStatus(NLS_URL + ":" + NLS_PORT + "/rpc2");

		return bResult;
	}


	public String checkUurl(String uurl) {
		String uri = null;
		URL url = null;

		try {
			url = new URL(uurl);
			uri = url.getPath();
		} catch (Exception e) {
			// URI 인 경우
			uri = uurl;
		}

		for (int i = 0; i < SKIP_URL.length; i++) {
			if (SKIP_URL[i].equals(uri)) {
				uurl = null;
				break;
			}
		}

		return uurl;
	}

	/**
	 * SE 가 있을 경우에만 사용 가능 (즉 사용할 일이 없음)
	 */
	public int checkSsoId(HttpServletRequest request,
	HttpServletResponse response) throws Exception {
		int return_code = 0;

		return_code = CookieManager.readNexessCookie(request, response,
			SSO_DOMAIN);
		return return_code;
	}

	/*
	public String getSystemAccount(String sso_id) {
		NXContext context = null;
		NXUserAPI userAPI = null;
		String retValue = null;

		try {
			context = getContext();
			NXAccount account = userAPI.getUserAccount(sso_id, SERVICE_NAME);
			System.out.println(account.toString());
			retValue = account.getAccountName();
			System.out.println("retValue = " + retValue);
		} catch (APIException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retValue;
	}
	*/
	
	public String getSsoDomain(HttpServletRequest request) throws Exception {
		String sso_domain = null;

		sso_domain = NLSHelper.getCookieDomain(request);
		return sso_domain;
	}

	// 통합인증 세션을 체크 하기 위하여 사용되는 API
	public String getEamSessionCheck(HttpServletRequest request,HttpServletResponse response)
	{
		String retCode = "";
		NXContext context = null;
		try {
			context = getContext();
			NXNLSAPI nxNLSAPI = new NXNLSAPI(context);
			retCode = nxNLSAPI.readNexessCookie(request, response, 0, 0);
		} catch(Exception npe) {
			npe.printStackTrace();
		}
		return retCode;
	}

	public Properties getUserInfo(String userid)
	throws Exception {
		NXContext context = null;
		context = getContext();

		NXUserAPI userAPI = new NXUserAPI(context);
		Properties prop = null;

		if (userid==null || userid.length() <= 0)
			return prop;

		try {
			NXUserInfo userInfo = userAPI.getUserInfo(userid);
			prop = new Properties();

			prop.setProperty("USERID", userInfo.getUserId());
			prop.setProperty("EMAIL", userInfo.getEmail());
			prop.setProperty("ENABLE", String.valueOf(userInfo.getEnable()));
			prop.setProperty("STARTVALID", userInfo.getStartValid());
			prop.setProperty("ENDVALID", userInfo.getEndValid());
			prop.setProperty("NAME", userInfo.getName());
			prop.setProperty("LASTPASSWDCHANGE", userInfo.getLastpasswdchange());
			prop.setProperty("LastLoginIP", userInfo.getLastLoginIp());
			prop.setProperty("LastLoginTime", userInfo.getLastLoginTime());
			prop.setProperty("LastLoginAuthLevel",	userInfo.getLastLoginAuthLevel());
		} catch (EmptyResultException e) {
			e.printStackTrace();
		} catch (APIException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return prop;
	}

	/**
	 * SSO ID 가 존재하는지 검사
	 * 1. true : 존재
	 * 2. false : 존재안함
	 * 3. Exception : network
	 */
	public boolean existUser(String userid)
	throws Exception {
		NXContext context = getContext();
		NXUserAPI userAPI = new NXUserAPI(context);
		boolean returnFlag = false;

		try {
			returnFlag= userAPI.existUser(userid);
		} catch (EmptyResultException e) {
		} catch (Exception e) {
			throw e;
		}
		//System.out.println("existUser:[" + userid + ":" + returnFlag + "]");
		return returnFlag;
	}

	// 사용자의 외부계정(시스템 계정) 등록 API
	public boolean addAccountToUser(String userid, String serviceName,
	String accountName, String accountPasswd)
	throws Exception {
		NXContext context = getContext();
		NXUserAPI userAPI = new NXUserAPI(context);
		NXExternalFieldSet nxefs = null;
		boolean returnFlag = false;

		try {
			userAPI.addAccountToUser(userid, serviceName,
				accountName, accountPasswd);
			returnFlag = true;
		} catch (EmptyResultException e) {
			e.printStackTrace();
		} catch (APIException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		return returnFlag;
	}

	// 사용자의 외부계정(시스템 계정) 삭제 API
	public boolean removeAccountFromUser(String userid, String serviceName)
	throws Exception {
		NXContext context = getContext();
		NXUserAPI userAPI = new NXUserAPI(context);
		boolean returnFlag = false;

		try {
			userAPI.removeAccountFromUser(userid, serviceName);
			returnFlag = true;
		} catch (EmptyResultException e) {
			e.printStackTrace();
		} catch (APIException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		return returnFlag;
	}

	/**
	 * 사용자의 특정 확장 필드 정보 조회
	 * return : String
	 * 사용자가 존재하지 않거나 확장필드가 없다면 return null
	 */
	public String getUserExField(String userid, String exName)
	throws Exception {

		NXContext context = null;
		context = getContext();
		NXUserAPI userAPI = new NXUserAPI(context);
		NXExternalField nxef = null;
		String returnValue = null;

		if (userid==null || userid.length() <= 0)
			return returnValue;

		try {
			nxef = userAPI.getUserExternalField(userid, exName);
			returnValue = (String) nxef.getValue();

		} catch (EmptyResultException e) {
			e.printStackTrace();
		} catch (APIException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		/*
		System.out.println("getUserExField:[" + userid + ":"
			+ exName + ":" + returnValue+ "]");
		*/
		return returnValue;
	}

	/**
	 * 사용자가 존재하지 않다면 return null
	 * 속성을 갖지 않는다면 IllegalArgumentException
	 * multi value 는 없음
	 */
	public String getUserAttribute(String userid, String attrName)
	throws Exception {
		NXContext context = null;
		context = getContext();
		NXUserAPI userAPI = new NXUserAPI(context);
		String attrValue = null;
		List valueList = null;
		NXAttributeValue nxav = null;

		if (userid==null || userid.length() <= 0)
			return attrValue;

		try {
			valueList = userAPI.getUserAttributes(userid, attrName);
			for (int i = 0, j = valueList.size(); i < j; i++) {
				nxav = (NXAttributeValue) valueList.get(i);
				attrValue = nxav.toString();
			}
		} catch (EmptyResultException e) {
			e.printStackTrace();
		} catch (APIException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return attrValue;
	}


	/**
	 * 권한이 있는 자원 목록 조회
	 * 
	 * @param userId 사용자ID
	 * @param applicationBaseID 어플리케이션 베이스ID
	 * @param searchPath 자원
	 * @param searchLevel 조회레벨(-1=하위모든자원, 0=자기자신만, 1=차하위자원만)
	 * @return List[ Vector[ String[레벨], String[자원코드], String[자원명], String[권한정보] ] ]
	 * @throws Exception
	 */
	public List<Vector<String>> getUserResourceList(String userId, String applicationBaseID, String searchPath, int searchLevel) throws Exception {
		
		List<Vector<String>> resourceList = null;

		NXProfile nxProfile = new NXProfile(getContext());
		
		// -- 자원조회
		NXApplication nxApplication = null;		
		try {
			nxApplication = nxProfile.getNXApplication(userId, applicationBaseID, searchPath, searchLevel);
		} catch ( Exception e ) {
			e.printStackTrace();
		}		

		// -- 자원정보 리스트 생성
		if(nxApplication != null && nxApplication.size() > 0){
			
			resourceList = new ArrayList<Vector<String>>();
			
			Iterator<NXResourceSet> nxApplicationIter = nxApplication.iterator();
			while (nxApplicationIter.hasNext()) {
				
				NXResourceSet nxResourceSet = nxApplicationIter.next();
				
				Iterator<NXResource> nxResourceSetIter = nxResourceSet.iterator();				
				while(nxResourceSetIter.hasNext() ) {
					
					NXResource nxResource = nxResourceSetIter.next();
					
					// -- 자기자신은 제외한다.
					String codePath = nxResource.getCodePath().getSelfPath();
					if(searchLevel != 0 && codePath.equals(searchPath))
						continue;
					
					// -- 자원정보 벡터
					Vector<String> v = new Vector<String>();
					v.add(Integer.toString(nxResource.getCodePath().getDepth()));
					v.add(codePath);
					v.add(nxResource.getDescriptionPath().getSelfValue());
					
					String actionValues = "";
					Iterator<String> iter = nxResource.getAllowedActionValues().iterator();
					while(iter.hasNext()){
						actionValues += iter.next();
					}
					v.add(actionValues);
					
					// -- add
					resourceList.add(v);
				}
			}
		}
		
		return resourceList;
	}	

%>
