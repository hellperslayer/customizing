/*******************************************************************
 * 페이지 그룹이동 버튼 정의</br>
 * - 페이지 네비게이터의 그룹이동 이미지 버튼 정의 
 *******************************************************************/
var pageGroupBtnTag = {
	"first":"<img src='"+ rootPath + "/resources/image/common/bt_board_prev2.gif' />"
	,"pre":"<img src='"+ rootPath + "/resources/image/common/bt_board_prev.gif' />"
	,"next":"<img src='"+ rootPath + "/resources/image/common/bt_board_next.gif' />"
	,"end":"<img src='"+ rootPath + "/resources/image/common/bt_board_next2.gif' />"
};

/*******************************************************************
 * 로딩스핀 설정
 *******************************************************************/

// 로딩스킨 옵션
var opts = {
  lines: 13, // The number of lines to draw
  length: 21, // The length of each line
  width: 15, // The line thickness
  radius: 30, // The radius of the inner circle
  corners: 1, // Corner roundness (0..1)
  rotate: 0, // The rotation offset
  direction: 1, // 1: clockwise, -1: counterclockwise
  color: '#FFF', // #rgb or #rrggbb or array of colors
  speed: 0.9, // Rounds per second
  trail: 32, // Afterglow percentage
  shadow: false, // Whether to render a shadow
  hwaccel: false, // Whether to use hardware acceleration
  className: 'spinner', // The CSS class to assign to the spinner
  zIndex: 2e9, // The z-index (defaults to 2000000000)
  top: 'auto', // Top position relative to parent in px
  left: 'auto' // Left position relative to parent in px
};

// 로딩스핀 객체
var spinner = null;

// 로딩스핀 대상 element
var spinnerLayoutDiv = "spinLayoutDiv";

// 로딩스핀 보이기
function showSpin(){
	if(spinner == null){
		$("#" + spinnerLayoutDiv).show();
		spinner = new Spinner(opts).spin(document.getElementById(spinnerLayoutDiv));
	}
}

// 로딩스핀 감추기
function hideSpin(){
	if(spinner != null){
		spinner.spin(false);
		spinner = null;
		$("#" + spinnerLayoutDiv).empty();
		$("#" + spinnerLayoutDiv).hide();
	}
}


$(document).ready(function() {
	// 검색창 Enter -> Form Submit()
	$("#searchListDiv input").keydown(function(e) {
		if(e.which == 13) {
			$("#searchListDiv").parent().submit();
		}
	});
});


