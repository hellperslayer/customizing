Date.prototype.format = function(f) {
	if (!this.valueOf())
		return " ";

	var weekKorName = [ "일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일" ];
	var weekKorShortName = [ "일", "월", "화", "수", "목", "금", "토" ];
	var weekEngName = [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday",
			"Friday", "Saturday" ];
	var weekEngShortName = [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ];
	var d = this;

	return f.replace(/(yyyy|yy|MM|dd|KS|KL|ES|EL|HH|hh|mm|ss|a\/p)/gi,
			function($1) {
				switch ($1) {
				case "yyyy":
					return d.getFullYear(); // 년 (4자리)
				case "yy":
					return (d.getFullYear() % 1000).zf(2); // 년 (2자리)
				case "MM":
					return (d.getMonth() + 1).zf(2); // 월 (2자리)
				case "dd":
					return d.getDate().zf(2); // 일 (2자리)
				case "KS":
					return weekKorShortName[d.getDay()]; // 요일 (짧은 한글)
				case "KL":
					return weekKorName[d.getDay()]; // 요일 (긴 한글)
				case "ES":
					return weekEngShortName[d.getDay()]; // 요일 (짧은 영어)
				case "EL":
					return weekEngName[d.getDay()]; // 요일 (긴 영어)
				case "HH":
					return d.getHours().zf(2); // 시간 (24시간 기준, 2자리)
				case "hh":
					return ((h = d.getHours() % 12) ? h : 12).zf(2); // 시간 (12시간 기준, 2자리)
				case "mm":
					return d.getMinutes().zf(2); // 분 (2자리)
				case "ss":
					return d.getSeconds().zf(2); // 초 (2자리)
				case "a/p":
					return d.getHours() < 12 ? "오전" : "오후"; // 오전/오후 구분
				default:
					return $1;
				}
			});
};

String.prototype.string = function(len) {
	var s = '', i = 0;
	while (i++ < len) {
		s += this;
	}
	return s;
};
String.prototype.zf = function(len) {
	return "0".string(len - this.length) + this;
};
Number.prototype.zf = function(len) {
	return this.toString().zf(len);
};

function searchData(periodType) {
	var date = new Date();
	if (periodType == 'month') {
		if($('#periodType').val()=='month'){
			$('.search_from').val($('#year').val() + '-' + $('#month').val());
		}else{
			$('.search_from').val(date.format('yyyy-MM-dd'));
		}
	} /*else if (periodType == 'now') {
		$('#search_from').val(new Date().format('yyyy-MM-dd'));
		$('#search_to').val(new Date().format('yyyy-MM-dd'));
	}*/
	$('#periodType').val(periodType);
	$('#searchForm').submit();
}

/*
 * AmChart 수정본
 */

function drawchartTotal(data) {
	var dataProvider = data;

	var chartTotal = AmCharts.makeChart("chart4", {
		"type" : "pie",
		"theme" : "light",
		"dataProvider" : dataProvider,
		"titleField" : "privacy_desc",
		"valueField" : "nPrivCount",
		"descriptionField" : "result_type",
		"outlineColor" : "#25313b",
		"labelRadius" : 5,
		"color" : "white",
		"startDuration" : 0,
		"radius" : "35%",
		"innerRadius" : "20%",
		"labelText" : "[[title]]",
		"balloonText" : "[[title]]: [[value]]건 ([[percents]]%)",
		"export" : {
			"enabled" : false
		},
		"colors" : [ '#D4DFE6', '#8EC0E4', '#CADBE9', '#6AAFE6', '#379392',
				'#4FB0C6', '#4F86C6', '#6C49B8', '#84B1ED' ]

	});
}

function drawchart1(data) {

	var dataProvider = data;
	var chart1 = AmCharts.makeChart("donutchart_1", {
		"type" : "pie",
		"theme" : "light",
		"dataProvider" : dataProvider,
		"valueField" : "nPrivCount",
		"labelRadius" : 5,
		"color" : "white",
		"outlineColor" : "#25313b",
		"startDuration" : 0,
		"radius" : "43%",
		"innerRadius" : "80%",
		"labelText" : "[[title]]",
		"showBalloon": false,
		"export" : {
			"enabled" : false
		},
		"colors" : [ '#72dad1','#e5eaf6' ]
	});
	chart1.addListener("clickGraphItem", handleClick);
}

function drawchart2(data) {
	
	var dataProvider = data;
	
	var chart2 = AmCharts.makeChart("donutchart_2", {
		"type" : "pie",
		"theme" : "light",
		"dataProvider" : dataProvider,
		"valueField" : "nPrivCount",
		"labelRadius" : 5,
		"color" : "white",
		"outlineColor" : "#25313b",
		"startDuration" : 0,
		"radius" : "43%",
		"innerRadius" : "80%",
		"labelText" : "[[title]]",
		"showBalloon": false,
		"export" : {
			"enabled" : false
		},
		"colors" : [ '#72dad1','#e5eaf6' ]
	});
	chart2.addListener("clickGraphItem", handleClick);
}

function drawchart3(data) {
	
	var dataProvider = data;
	
	var chart3 = AmCharts.makeChart("donutchart_3", {
		"type" : "pie",
		"theme" : "light",
		"dataProvider" : dataProvider,
		"valueField" : "nPrivCount",
		"labelRadius" : 5,
		"color" : "white",
		"outlineColor" : "#25313b",
		"startDuration" : 0,
		"radius" : "43%",
		"innerRadius" : "80%",
		"labelText" : "[[title]]",
		"showBalloon": false,
		"export" : {
			"enabled" : false
		},
		"colors" : [ '#72dad1','#e5eaf6' ]
	});
	chart3.addListener("clickGraphItem", handleClick);
}

function drawchart4(data) {
	
	var dataProvider = data;
	
	var chart4 = AmCharts.makeChart("donutchart_4", {
		"type" : "pie",
		"theme" : "light",
		"dataProvider" : dataProvider,
		"valueField" : "nPrivCount",
		"labelRadius" : 5,
		"color" : "white",
		"outlineColor" : "#25313b",
		"startDuration" : 0,
		"radius" : "43%",
		"innerRadius" : "80%",
		"labelText" : "[[title]]",
		"showBalloon": false,
		"export" : {
			"enabled" : false
		},
		"colors" : [ '#72dad1','#e5eaf6' ]
	});
	chart4.addListener("clickGraphItem", handleClick);
}

function drawchart5(data) {
	
	var dataProvider = data;
	
	var chart5 = AmCharts.makeChart("donutchart_5", {
		"type" : "pie",
		"theme" : "light",
		"dataProvider" : dataProvider,
		"valueField" : "nPrivCount",
		"labelRadius" : 5,
		"color" : "white",
		"outlineColor" : "#25313b",
		"startDuration" : 0,
		"radius" : "43%",
		"innerRadius" : "80%",
		"labelText" : "[[title]]",
		"showBalloon": false,
		"export" : {
			"enabled" : false
		},
		"colors" : [ '#72dad1','#e5eaf6' ]
	});
	chart5.addListener("clickItem", handleClick);
}

function handleClick(event) {
	alert();
}

function activeSystemInfo(server_seq,btn){
	$('.system_info_name').each(function(idx,val){
		$(val).removeClass('btn_active');
	});
	$(btn).addClass('btn_active');
	$('.system_chart').each(function(idx,val){
		if(!$(val).hasClass('disable')){
			$(val).addClass('disable');
		}
	});
	$('#system'+server_seq).removeClass('disable');
}