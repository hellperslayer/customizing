var reportId;	//현재 선택된 보고서 고유번호
var reportYear;	//보고되는 해
var sysList;

var report_page = {
		type1 : rootPath+"/report/reportDetail_new.html",
		type2 : rootPath+"/report/reportCPO.html",
		type3 : rootPath+"/report/reportDetail_new.html",
		type4 : rootPath+"/report/reportDetail_new.html",
		type5 : rootPath+"/report/reportDetail_half.html",
		type9 : rootPath+"/report/reportDetail_download.html",
		type10 : rootPath+"/report/reportDetail_authInfo.html",
		type13 : rootPath+"/report/reportEmpLevel_new.html",
		type12 : rootPath+"/report/reportDetail_download_new.html",
		type14 : rootPath+"/report/reportTotalIntegration.html",
		callpage : function(type){
			report_type = type;
			return this["type"+type];
		}
}

Date.prototype.format = function(f) {
    if (!this.valueOf()) return " ";
 
    var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
    var d = this;
     
    return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
        switch ($1) {
            case "yyyy": return d.getFullYear();
            case "yy": return (d.getFullYear() % 1000).zf(2);
            case "MM": return (d.getMonth() + 1).zf(2);
            case "dd": return d.getDate().zf(2);
            case "E": return weekName[d.getDay()];
            case "HH": return d.getHours().zf(2);
            case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2);
            case "mm": return d.getMinutes().zf(2);
            case "ss": return d.getSeconds().zf(2);
            case "a/p": return d.getHours() < 12 ? "오전" : "오후";
            default: return $1;
        }
    });
};

String.prototype.string = function(len){var s = '', i = 0; while (i++ < len) { s += this; } return s;};
String.prototype.zf = function(len){return "0".string(len - this.length) + this;};
Number.prototype.zf = function(len){return this.toString().zf(len);};

function reportSelect(selectTarget){
	$('.report_select').removeClass("select");
	$('.select').removeClass("select");
	$(selectTarget).addClass("select");
	reportId = $(selectTarget).attr('value');
	report_allCheck();
	console.log(reportId);
	if(reportId=='2'){
		$("[name=excelBtn]").css("display","none");
	} else {
		$("[name=excelBtn]").css("display","");
	}
}

//DB에서 해당 연도의 모든 보고서 정보, 보고서 관련 옵션 정보를 호출에 페이지에 반영함
//페이지 최초 로딩, 보고서 선택시 호출
function report_allCheck(){
	report_allDisable();
	var yearval = reportYear;
	var date = new Date();
	var afterMonth = date.format('yyyyMM');
	$('.inpChk checkbox').prop('checked',true);
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/findReportOption.html',
		data: { 
			"code_id" : reportId,
			"proc_year" : reportYear,
			"allCheck" : "allCheck",
		},
		dataType: 'json',
		async:false,
		success: function(data) {
			for(var i = 0; i < data.length ; i++){
				var month = data[i].proc_month.substring(4,6);
				var num = Number(month);
				if(data[i].period_type=='4'){
					$('#input_chart_4_'+num).prop('checked',data[i].input_chart == 'true'?true:false);
					$('#input_description_4_'+num).prop('checked',data[i].input_description == 'true'?true:false);
				}
				else if(data[i].period_type=='3'){
				}
				else if(data[i].period_type=='2'){
					$('#input_chart_2_'+num).prop('checked',data[i].input_chart == 'true'?true:false);
					$('#input_description_2_'+num).prop('checked',data[i].input_description == 'true'?true:false);
				}
				else if(data[i].period_type=='1'){
					$('#input_chart_1_'+num).prop('checked',data[i].input_chart == 'true'?true:false);
					$('#input_description_1_'+num).prop('checked',data[i].input_description == 'true'?true:false);
				}
			}
		}
	});
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/report_Allcheck.html',
		data: { 
			"year" : reportYear,
			"reportType" : reportId,
		},
		dataType: 'json',
		success: function(data) {
			var checkboxHTML = '<input class="check_box" type="checkbox">';
			$('input[type=checkbox]').removeAttr('disabled');
			$('.btn_desc').removeAttr('disabled');
			for(var i = 0; i < data.length ; i++){
				var month = data[i].proc_month.substring(4,6);
				if(data[i].period_type=='4'){
					var num = month-1;
					var procdate = data[i].proc_month;
					var date = new Date(data[i].update_time);
					$('.active_target_4 > li').eq(num).removeClass("nodata");
					$('.active_target_4 .lastUpdate').eq(num).text(date.format('yyyy.MM.dd'));
					$('.active_target_4 > li  #reportSeq_4_'+(num+1)).val(data[i].report_seq);
					$('.active_target_4 > li .btn_desc').eq(num).prop('disabled',true);
					$('#input_chart_4_'+(num+1)).prop('disabled',true);
					$('#input_description_4_'+(num+1)).prop('disabled',true);
				}
				else if(data[i].period_type=='3'){
				}
				else if(data[i].period_type=='2'){
					var num = Math.ceil(month/3)-1;
					var procdate = data[i].proc_month;
					var date = new Date(data[i].update_time);
					$('.active_target_2 > li').eq(num).removeClass("nodata");
					$('.active_target_2 .lastUpdate').eq(num).text(date.format('yyyy.MM.dd'));
					$('.active_target_2 > li  #reportSeq_2_'+(num+1)).val(data[i].report_seq);
					$('.active_target_2 > li .btn_desc').eq(num).prop('disabled',true);
					$('#input_chart_2_'+(1+(num)*3)).prop('disabled',true);
					$('#input_description_2_'+(1+(num)*3)).prop('disabled',true);
				}
				else if(data[i].period_type=='1'){
					var num = month-1;
					var procdate = data[i].proc_month;
					var date = new Date(data[i].update_time);
					$('.active_target_1 > li').eq(num).removeClass("nodata");
					$('.active_target_1 .lastUpdate').eq(num).text(date.format('yyyy.MM.dd'));
					$('.active_target_1 > li  #reportSeq_1_'+(num+1)).val(data[i].report_seq);
					$('.active_target_1 > li .btn_desc').eq(num).prop('disabled',true);
					$('#input_chart_1_'+(num+1)).prop('disabled',true);
					$('#input_description_1_'+(num+1)).prop('disabled',true);
				}
			}
			$('.active_target_1 > li').each(function(idx,val){
				var procdate = yearval;
				idx += 1;
				if(idx<10){
					procdate = procdate+"0"+idx;
				}else{
					procdate = procdate+""+idx;
				}
				if(procdate<=install_month){
					$(val).find(".nonActiveReport > button").addClass("btn_line_disable").prop("disabled",true);
					$(val).addClass("nomake");
				}
				if(procdate>afterMonth){
					$(val).find(".nonActiveReport > button").addClass("btn_line_disable").prop("disabled",true);
				}
			});
			
		},
		beforeSend:function() {
	    },
	    complete:function() {
	    }
	});
}

function report_allDisable(){
	$('.active_target_4 > li').addClass('nodata');
	$('.active_target_2 > li').addClass('nodata');
	$('.active_target_1 > li').addClass('nodata');
	$('.btn_line_disable').removeAttr('disabled');
	$('.btn_line_disable').removeClass('btn_line_disable');
	$('.nomake').removeClass('nomake');
	$('.lastUpdate').text('');
}

function makeMonthReport(period_type, month){
	if(auth == 'N'){
		alert('보고서 생성 권한이 없습니다');
		return;
	}
	$('#loading').css('display','block');
	
	setTimeout(function() {
		if(createSummaryMonth(period_type, month)){
			make_report_ajax(period_type, month);
		}else{
			$('#loading').css('display','');
		}
	}, 100);
}

function makeReport(periodType, month, divdom) {	//분기,통합 보고서 생성
	var checkedLowerReport = 0;	//조건을 만족하지 못하는 하위 보고서 갯수
	var checkedMonth = 0;
	var checkedDisable = 0;
	
	month = 1+(month-1)*3;
	
	if(periodType == 2){
		if($('.active_target_1 > li').eq(month+1).hasClass('nodata')){
			checkedMonth++;
		}
		for (var i = month-1; i < month+2; i++) {
			if($('.active_target_1 > li').eq(i).hasClass('disable')){
				checkedDisable++;
			}
		}
		if(checkedMonth==0){
			$('#loading').css('display','block');
			make_report_ajax(periodType,month);
		}else{
			alert(Math.ceil(month/3)+'분기 보고서는 '+(month+2)+"월 보고서가 먼저 생성되어 있어야 합니다.");
		}
	}
	
	if(periodType == 4){
		if(!$('.active_target_4 > li').eq(11).hasClass('nodata')){
			make_report_ajax(periodType,month);
		}else{
			alert("연간 통합 보고서는 최소 12월 보고서가 먼저 생성되어 있어야 합니다.");
		}
	}
}

function make_report_ajax(period_type, month) {
	var yearval = reportYear;						
	var reportType = reportId;
	var date = new Date(yearval,month-1,1);
	var start_date = date.format('yyyy-MM-dd');
	var makeyear = date.format('yyyy');
	var end_date = '';
	var proc_date = date.format('yyyyMM');
	var periodTitle = '';
	var reportPage = report_page.callpage(reportType);
	var system_seq = '';
	reportOption(period_type, month);
	
	if(period_type == 4) {
		date.setMonth('12');
		periodTitle = '연간';
	} else if(period_type == 3) {
		date.setMonth(month+5);
		var part = "";
		if(month == 1) {
			part = "상";
		} else if(month == 7) {
			part = "하";
		}
		periodTitle = part+'반기';
	} else if(period_type == 2) {
		date.setMonth(month+2);
		var part = "";
		var quarter_type ="";
		if(month == 1) {
			part = "1";
			quarter_type = "1";
		} else if(month == 4) {
			part = "2";
			quarter_type = "2";
		} else if(month == 7) {
			part = "3";
			quarter_type = "3";
		} else if(month == 10) {
			part = "4";
			quarter_type = "4";
		}
		periodTitle = part+'분기';
	} else if(period_type == 1) {
		date.setMonth(month);
		periodTitle = month + '월';
	}
	date.setDate('0');
	end_date = date.format('yyyy-MM-dd');
	
	var log_title = yearval+'년_'+ periodTitle + '_' +report_title.calltitle(report_type);
		
	$('.sysCheck:checked').each(function(idx,val){
		system_seq += $(val).val()+',';
	});
	system_seq = system_seq.substr(0,system_seq.length-1);
	if(system_seq == ''){
		alert('하나 이상의 시스템을 선택해주세요.');
		$('#loading').css('display','');
		return;
	}
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportMake.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"proc_date" : proc_date,
			"report_type" : report_type,
			"period_type" : period_type,
			"quarter_type": quarter_type,
			"title" : log_title,
			"report_page" : reportPage,
			"system_seq" : system_seq
		},
		dataType: 'json',
		success: function(data) {
			console.log("data seq : " + data.seq);
			if(data.seq != null) {
				report_word_make(data.seq,data.fullpath);
			}
			else if(data.makeAuth == 'none'){
				alert('보고서 생성 권한이 없습니다.');
				$('#loading').css('display','none');
			}
		},
		error: function(a,b,c) {
			alert('pdf 생성 에러');
			console.log("error : " + a,b,c);
			$('#loading').css('display','none');
		}
	});
}

function report_word_make(report_seq,fullpath){
	$('#includeReport').attr('src',fullpath+'&report_seq='+report_seq);
	report_allCheck();
}

function report_delete(group,num){
	if(confirm('업로드된 보고서를 포함하여 전부 삭제됩니다. 정말 삭제하시겠습니까?')){
		var report_seq = $('#reportSeq_'+group+'_'+num).val();
		$('#reportSeq_'+group+'_'+num).closest('li').removeClass('select');
		$.ajax({
			type: 'POST',
			url: rootPath + '/report/reportListDelete.html',
			data: { 
				"report_seq" : report_seq
			},
			dataType: 'json',
			success: function(data) {
				if(data == 1){
					alert('삭제 성공');
					report_allCheck();
				}
			},
			error: function(data){
				alert('삭제 실패');
			}
		});
	}
}

function select(tag,group,num){
	var parentTag = $(tag).closest('li');
	var selected = parentTag.hasClass('select');
	var nodata = parentTag.hasClass('nodata');
	var report_seq = $('#reportSeq_'+group+'_'+num).val();
	
	
	if(selected){
		parentTag.removeClass('select');
	}else if(!selected&&!nodata){
		parentTag.addClass('select');
		$.ajax({
			type: 'POST',
			url: rootPath + '/report/uploadReportOne.html',
			data: { 
				"report_seq" : report_seq
			},
			dataType: 'json',
			success: function(data) {
				if(data.length > 0){
					parentTag.find('.fileName').val(data);
					console.log(parentTag.find('.btn_t_download').text());
					parentTag.find('.btn_t_download').css('display','');
				}else{
					parentTag.find('.btn_t_download').css('display','none');
				}
			}
		});
	}
}

function userReportDownload(group,num){
	var report_seq = $('#reportSeq_'+group+'_'+num).val();

	$('input[name=report_seq]').val(report_seq);
	$("#listForm").attr("action",rootPath+"/report/report_userfile.html");d
	$("#listForm").submit();
}

function report_pdf(group,num){
	var report_seq = $('#reportSeq_'+group+'_'+num).val();
	$('#listForm input[name=report_seq]').val(report_seq);
	$("#listForm").attr("action",rootPath+"/report/report_pdf.html");
	$("#listForm").submit();
}

function report_word(group,num) {
	var report_seq = $('#reportSeq_'+group+'_'+num).val();
	
	/*var log_message_params = '';
	var menu_id = $('input[name=current_menu_id]').val();
	var log_message_title = '점검보고서 WORD 다운로드';
	var log_action = 'WORD DOWNLOAD';*/
	
	$.ajax({
		url: rootPath + '/report/report_Download.html',
		type:'POST',
		data: { 
			"report_seq" : report_seq
		},
		dataType: "json",
		success: function(data){
			if(data.html_encode == ''){
				alert('보고서 데이터를 찾을 수 없습니다');
			}else{
				downLoad_type(data);
			}
		},
		error: function(){
			alert('실패');
		}	
	});
}

function createSummaryMonth(period_type, month){
	$('#loading').css('display','block');
	var startDate = new Date(reportYear, month-1, 1);
	var endDate = new Date(reportYear, month, 0);
	var summaryCount = 0;
	var makeYn = false;
	var reportType = reportId;
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/checkBeforeReport.html',
		async: false,
		data: { 
			"startDate" : startDate.format('yyyyMMdd'),
			"endDate" : endDate.format('yyyyMMdd'),
			"report_type" : reportType,
			"log_delimiter" : '0'
		},
		dataType: 'json',
		success: function(data) {
			summaryCount = data;
		}
	});
	$.ajax({
		type: 'POST',
		url: rootPath + '/summaryMngt/checkSummaryDaily.html',
		async: false,
		data: { 
			"startDate" : startDate.format('yyyyMMdd'),
			"endDate" : endDate.format('yyyyMMdd'),
			"reportType" : reportType,
			"summaryCount" : summaryCount
		},
		dataType: 'json',
		success: function(data) {
			if(data == "SUCCESS"){
				$.ajax({
					type: 'POST',
					url: rootPath + '/summaryMngt/createSummaryMonth.html',
					async: false,
					data: { 
						"startDate" : startDate.format('yyyyMMdd'),
						"endDate" : endDate.format('yyyyMMdd')
					},
					dataType: 'json',
					success: function(data) {
						makeYn = true;	//마감데이터 확인 후 월마감 진행
					}
				});
			}else if(data == "NONEDATA"){
				alert('일간 마감데이터가 없습니다.');
				$('#loading').css('display','none');
			}else if(data == "BEFOREDATA"){
				alert('이전 보고서를 먼저 생성해야 합니다.');
				$('#loading').css('display','none');
			}
		}
	});
	return makeYn;
}

function downLoad_type(data) {
	var agent = navigator.userAgent.toLowerCase();
	
	if ((navigator.appName == 'Netscape' && navigator.userAgent.search('Trident') != -1) || (agent.indexOf("msie") != -1) ) {
		report_downloadIe(data);
	} else {
		report_downloadChrome(data);
	}
}

function report_downloadChrome(data) {
	var date = new Date();
	var year = date.getFullYear();
	var month = new String(date.getMonth() + 1);
	//var day = new String(date.getDate());
	var source = data.html_encode;
	
	if(month.length == 1){ 
		month = "0" + month; 
	}
	
	var regdate = year + "" + month;
	var agent = navigator.userAgent.toLowerCase(); 
	
	var fileDownload = document.createElement("a");
    document.body.appendChild(fileDownload);
    fileDownload.href = source;
    fileDownload.download = data.report_title+'.doc';
    fileDownload.click();
    document.body.removeChild(fileDownload);
    
}

function report_downloadIe(data) {
	var source = data.html_encode;
	var decodeHTML = decodeURIComponent(source);
	
	var date = new Date();
	var year = date.getFullYear();
	var month = new String(date.getMonth() + 1);
	//var day = new String(date.getDate());
	if(month.length == 1){ 
		month = "0" + month; 
	}
	
	
	var regdate = year + "" + month;
	var reportName = data.report_title+'.doc';
	
	blob = new Blob(['\ufeff', decodeHTML], {
		type: 'application/msword'
		//type: 'application/vnd.ms-word'
	});
    url = URL.createObjectURL(blob);
    link = document.createElement('a');
    link.href = url;
    link.download = reportName;  // default name without extension 
    document.body.appendChild(link);
    if (navigator.msSaveOrOpenBlob ) navigator.msSaveOrOpenBlob( blob, reportName); // IE10-11
    else link.click();  // other browsers
    document.body.removeChild(link);
}

function systemOptionPopup(){
	$('#reportSysPopup').css('display','flex');
	system_seq = "";
	$('.sysCheck:checked').each(function(idx,val){
		system_seq += $(val).val()+',';
	});
	sysList = system_seq;
}

function popupClose(){
	$('.popup_wrap').css('display','none');
}

function popupCancle(){
	popupClose();
	var sysArr = sysList.split(',');
	$('.sysCheck').prop('checked',false);
	for(var i in sysArr){
		$('#sysCheck_'+sysArr[i]).prop('checked',true);
	}
}

function popupComplete(){
	var sysCheckCount = $('.sysCheck:checked').length;
	if(sysCheckCount>0){
		popupClose();
		$('#targetSysCount').text(sysCheckCount);
		system_seq = "";
		$('.sysCheck:checked').each(function(idx,val){
			system_seq += $(val).val()+',';
		});
		sysList = system_seq;
		deleteCookie('reportSysList');
		setCookie('reportSysList',sysList,30);
	}else{
		alert('시스템을 하나 이상 선택해야 합니다.');
	}
}

function checkAllCheck(){
	var sysCount = $('.sysCheck').length;
	var sysCheckCount = $('.sysCheck:checked').length;
	if(sysCount == sysCheckCount){
		$('#sysAllCheck').prop('checked',true);
	}else{
		$('#sysAllCheck').prop('checked',false);
	}
}

function AllCheck(){
	if($('#sysAllCheck').prop('checked')){
		$('.sysCheck').prop('checked',true);
	}else{
		$('.sysCheck').prop('checked',false);
	}
}

var setCookie = function(name, value, exp) {
	var date = new Date();
	date.setTime(date.getTime() + exp*24*60*60*1000);
	document.cookie = name + '=' + value + ';expires=' + date.toUTCString() + ';path=/';
};

var getCookie = function(name) {
	var value = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
	return value? value[2] : null;
};

var deleteCookie = function(name) {
	document.cookie = name + '=; expires=Thu, 01 Jan 1999 00:00:10 GMT;';
};

function reportOption(group,num){
	var optionResult = 0;
	var report_seq = $('#reportSeq_'+group+'_'+num).val();
	var proc_month = "";
	var month = num;
	var periodType = group;
	if(month < 10){
		proc_month = reportYear+"0"+month;
	}else{
		proc_month = reportYear+""+month;
	}
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/addReportOption.html',
		data: { 
			"code_id" : reportId,
			"input_chart" : $('#input_chart_'+group+'_'+num).prop('checked'),
			"input_description" : $('#input_description_'+group+'_'+num).prop('checked'),
			"period_type" : periodType,
			"proc_month" : proc_month
		},
		dataType: 'json',
		success: function(data) {
		}
	});
}

function descPopup(group,num){
	var periodType = group;
	var month = num;
	var optionResult = 0;
	if(month < 10){
		proc_month = reportYear+"0"+month;
	}else{
		proc_month = reportYear+""+month;
	}
	$('#period_type').val(periodType);
	$('#proc_month').val(proc_month);
	$('#description').val('');
	$('#descriptPopup').css('display','flex');
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/findReportOption.html',
		data: { 
			"code_id" : reportId,
			"period_type" : periodType,
			"proc_month" : proc_month
		},
		dataType: 'json',
		success: function(data) {
			if(data.length>0){
				$('#description').val(data[0].description);
			}
		}
	});
}

function descComplete(){
	var periodType = $('#period_type').val();
	var proc_month = $('#proc_month').val();
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/addReportOption.html',
		data: { 
			"code_id" : reportId,
			"period_type" : periodType,
			"proc_month" : proc_month,
			"description" : $('#description').val()
		},
		dataType: 'json',
		success: function(data) {
			alert('저장되었습니다.');
			$('#descriptPopup').css('display','none');
		}
	});
}

function selectYear(){
	var parentTag = $(".option-bar").closest('li');
	var selected = parentTag.hasClass('select');
	parentTag.removeClass('select');
}

function descReset(){
	$.ajax({
		type: 'POST',
		url: rootPath + '/customerInfo/findDefaultDesc.html',
		dataType : 'json',
		data: { 
			report_code : reportId
		},
		success: function(data) {
			$('#description').val(data);
		}
	});
}

$(document).ready(function(){
	reportId = $(".select").attr('value');
	
	if($("#select_year").val()){
		reportYear = $("#select_year").val().replace("년","");
	}
	else{
		reportYear = $("#year").val();
	}
	
	$('.year').text(reportYear);
	report_allCheck();

	$('#select_year').change(function(){
		reportYear = $("#select_year").val().replace("년","");
		$('.year').text(reportYear);
		report_allCheck();
	})
	
	$('.btn_file').click(function(){
		$(this).siblings('.file').click();
	});
	
	$('.fileBox .uploadBtn').change(function(tag){
		var options = {
				success : function(data) {
					$(this).val("");
					if(data == 1){
						alert('업로드 완료');
						$(tag.target).closest('li').find('.btn_t_download').css('display','');
					}
				},
				error : function(error) {
				}
		  };
		$(this).parent().find('input[name="report_type"]').val(reportId);
		$(this).parent().ajaxSubmit(options);
		console.log($(this));
	});
	if(install_date != '-'){
		install_month = new Date(install_date).format('yyyyMM');
	}else{
		install_month = '999999';
	}
	
	sysList = getCookie('reportSysList');
	if(sysList != null){
		popupCancle();
		checkAllCheck();
		var sysCheckCount = $('.sysCheck:checked').length;
		$('#targetSysCount').text(sysCheckCount);
	}
});

function saveInstallDate(){
	$.ajax({
		type: 'POST',
		url: rootPath + '/groupCodeMngt/add.html',
		data:{
			"group_code_id" : "SOLUTION_MASTER", 
			"group_code_name" : "솔루션 설치정보", 
			"use_flag" : "Y"
		},
		success: function(data) {}
	});
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/codeMngt/add.html',
		data:{
			"group_code_id" : "SOLUTION_MASTER", 
			"code_id" : "INSTALL_DATE", 
			"code_name" : $('#install_date').val(), 
			"use_flag" : "Y", 
			"code_type" : "SYSTEM",
			"sort_order" : "1"
		},
		success: function(data) {
			$.ajax({
				type: 'POST',
				url: rootPath + '/summaryMngt/addInstallDate.html',
				data:{
					"procdate" : $('#install_date').val()
				},
				success: function(data) {location.reload();}
			});
		}
	});
}

// =========================================
function report_excel(period_type, month) {
	var yearval = reportYear;
	var date = new Date(yearval,month-1,1);
	var start_date = date.format('yyyy-MM-dd');
	var makeyear = date.format('yyyy');
	var end_date = '';
	var proc_date = date.format('yyyyMM');
	var periodTitle = '';
	var system_seq = '';
	
	if(period_type == 4) {
		date.setMonth('12');
		periodTitle = '연간';
	} else if(period_type == 2) {
		month = 1+(month-1)*3;
		date.setMonth(month+2);
		var part = "";
		var quarter_type ="";
		if(month == 1) {
			part = "1";
			quarter_type = "1";
		} else if(month == 4) {
			part = "2";
			quarter_type = "2";
		} else if(month == 7) {
			part = "3";
			quarter_type = "3";
		} else if(month == 10) {
			part = "4";
			quarter_type = "4";
		}
		periodTitle = part+'분기';
	} else if(period_type == 1) {
		date.setMonth(month);
		periodTitle = month + '월';
	}
	date.setDate('0');
	end_date = date.format('yyyy-MM-dd');
	
		
	$('.sysCheck:checked').each(function(idx,val){
		system_seq += $(val).val()+',';
	});
	if(system_seq == ''){
		alert('하나 이상의 시스템을 선택해주세요.');
		$('#loading').css('display','');
		return;
	}
	var title="보고서 입니다.";
	console.log(start_date);
	console.log(end_date);
	console.log(period_type);
	console.log(quarter_type);
	console.log(title);
	console.log(system_seq);
	
	
	$("#excelForm input[name=start_date]").val(start_date);
	$("#excelForm input[name=end_date]").val(end_date);
	$("#excelForm input[name=period_type]").val(period_type);
	$("#excelForm input[name=quarter_type]").val(quarter_type);
	$("#excelForm input[name=title]").val(title);
	$("#excelForm input[name=system_seq]").val(system_seq);
	if(reportId=='13'){
		$("#excelForm").attr("action",rootPath+'/report/makeExcelReportAll.html');
	} else if(reportId=='12'){
		$("#excelForm").attr("action",rootPath+'/report/makeExcelReportDown.html');
	}
	$("#excelForm").submit();
	
}