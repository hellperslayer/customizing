var report_active = 0;


Date.prototype.format = function(f) {
    if (!this.valueOf()) return " ";
 
    var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
    var d = this;
     
    return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
        switch ($1) {
            case "yyyy": return d.getFullYear();
            case "yy": return (d.getFullYear() % 1000).zf(2);
            case "MM": return (d.getMonth() + 1).zf(2);
            case "dd": return d.getDate().zf(2);
            case "E": return weekName[d.getDay()];
            case "HH": return d.getHours().zf(2);
            case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2);
            case "mm": return d.getMinutes().zf(2);
            case "ss": return d.getSeconds().zf(2);
            case "a/p": return d.getHours() < 12 ? "오전" : "오후";
            default: return $1;
        }
    });
};
 
String.prototype.string = function(len){var s = '', i = 0; while (i++ < len) { s += this; } return s;};
String.prototype.zf = function(len){return "0".string(len - this.length) + this;};
Number.prototype.zf = function(len){return this.toString().zf(len);};

function report_pdf(btngroup){
	var checkgroup;
	var report_seq;
	var btn_group;
	if('.check_top:checked') {checkgroup = ".check_top";btn_group = ".year_group"}
	if('.check_mid:checked') {checkgroup = ".check_mid";btn_group = ".quarter_group"}
	if('.check_bot:checked') {checkgroup = ".check_bot";btn_group = ".month_group"}
	report_seq = $(btn_group).children('.btn_seq').val();
	$('input[name=report_seq]').val(report_seq);
	$("#listForm").attr("action",rootPath+"/report/report_pdf.html");
	$("#listForm").submit();
}

function saveInstallDate(){
	$.ajax({
		type: 'POST',
		url: rootPath + '/groupCodeMngt/add.html',
		data:{
			"group_code_id" : "SOLUTION_MASTER", 
			"group_code_name" : "솔루션 설치정보", 
			"use_flag" : "Y"
		},
		success: function(data) {}
	});
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/codeMngt/add.html',
		data:{
			"group_code_id" : "SOLUTION_MASTER", 
			"code_id" : "INSTALL_DATE", 
			"code_name" : $('#install_date').val(), 
			"use_flag" : "Y", 
			"code_type" : "SYSTEM",
			"sort_order" : "1"
		},
		success: function(data) {}
	});
	location.reload();
}

function report_find(month){
	var date =  year+'-'+month+'-01';
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/report_find.html',
		data: { 
			"year" : year,
			"month" : month,
			"title" : "테스트보고서"
		},
		success: function(data) {
			if(data == 0){
				
			}else{
				alert("파일찾음");
			}
		}
	});
}

function report_word_make(report_seq,fullpath){
	console.log(fullpath);
	$.ajax({
		type: 'POST',
		url: fullpath+'&report_seq='+report_seq,
		data: { },
		success: function(data) {
			$('#test').html(data);
		}
	});
}

function reportIntroChange(selectbox) {
	report_type = $(selectbox).val();
	report_allCheck(year);
}

function reportYearChange(selectbox) {
	year = $(selectbox).val();
	$('.year').text(year);
	report_allCheck(year);
}

function btn_active() {
	if($('.check_box:checked').length > 0){
		$('.btn_group').css('display','block');
	}else{
		$('.btn_group').css('display','none');
	}
}

function make_report_month(periodType, month, divdom) {	//월간보고서 생성
	if(createSummaryMonth(month)){
		var period_type = periodType;
		var reportDiv = $(divdom).closest('.report_month_t');
		reportDiv.hasClass('report_month_t') ? null : reportDiv.addClass('report_month_t');
		var seqDiv = reportDiv.parent().children('.report_seq');
		$(divdom).css('display','none');
		reportDiv.find('.report_loading').css('display','block');
		make_report_ajax(period_type,month,seqDiv,reportDiv);
	}
}

function make_report(periodType, month, divdom) {	//분기,통합 보고서 생성
	var checkedLowerReport = 0;	//조건을 만족하지 못하는 하위 보고서 갯수
	var checkedMonth = 0;
	
	if(periodType == 2){
		for (var i = month-1; i < month+2; i++) {
			if($('.report_month_t').eq(i).hasClass('report_disable')){
				checkedMonth++;
			}
		}
		if(checkedMonth==0){
			var period_type = periodType;
			var reportDiv = $(divdom).closest('.report');
			reportDiv.hasClass('.report_disable') ? null : reportDiv.addClass('.report_disable');
			var seqDiv = reportDiv.parent().children('.report_seq');
			$(divdom).css('display','none');
			reportDiv.find('.report_loading').css('display','block');
			make_report_ajax(period_type,month,seqDiv,reportDiv);
		}else{
			alert(Math.ceil(month/3)+'분기 보고서는 '+month+"월부터 "+(month+2)+"월 보고서가 먼저 생성되어 있어야 합니다.");
		}
	}
	
	if(periodType == 4){
		if($('.report_month_t').eq(11).hasClass('report_disable')){
			var period_type = periodType;
			var reportDiv = $(divdom).closest('.report');
			reportDiv.hasClass('.report_disable') ? null : reportDiv.addClass('.report_disable');
			var seqDiv = reportDiv.parent().children('.report_seq');
			$(divdom).css('display','none');
			reportDiv.find('.report_loading').css('display','block');
			make_report_ajax(period_type,month,seqDiv,reportDiv);
		}else{
			alert("연간 통합 보고서는 최소 12월 보고서가 먼저 생성되어 있어야 합니다.");
		}
	}
}

function make_report_ajax(period_type, month, seqDiv, reportDiv) {
	console.log("type : " + period_type + " type2 " + month  + " type3  " + seqDiv + " type4 " + reportDiv)
	var yearval = $('#year_select').val();
	var reportType = $('#report_select').val();
	var date = new Date(year,month-1,1);
	var start_date = date.format('yyyy-MM-dd');
	var makeyear = date.format('yyyy');
	var end_date = '';
	var proc_date = date.format('yyyyMM');
	var periodTitle = '';
	var reportPage = report_page.callpage(report_type);
	var system_seq = '';
	
	if(period_type == 4) {
		date.setMonth('12');
		periodTitle = '연간';
	} else if(period_type == 3) {
		date.setMonth(month+5);
		var part = "";
		if(month == 1) {
			part = "상";
		} else if(month == 7) {
			part = "하";
		}
		periodTitle = part+'반기';
	} else if(period_type == 2) {
		date.setMonth(month+2);
		var part = "";
		var quarter_type ="";
		if(month == 1) {
			part = "1";
			quarter_type = "1";
		} else if(month == 4) {
			part = "2";
			quarter_type = "2";
		} else if(month == 7) {
			part = "3";
			quarter_type = "3";
		} else if(month == 10) {
			part = "4";
			quarter_type = "4";
		}
		periodTitle = part+'분기';
	} else if(period_type == 1) {
		date.setMonth(month);
		periodTitle = month + '월';
	}
	date.setDate('0');
	end_date = date.format('yyyy-MM-dd');
	
	var log_title = year+'년_'+ periodTitle + '_' +report_title.calltitle(report_type);
		
	$('.reportSys:checked').each(function(idx,val){
		system_seq += $(val).val()+',';
	});
	if(system_seq == ''){
		alert('하나 이상의 시스템을 선택해주세요.');
		return;
	}
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportMake.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"proc_date" : proc_date,
			"report_type" : report_type,
			"period_type" : period_type,
			"quarter_type": quarter_type,
			"title" : log_title,
			"report_page" : reportPage,
			"system_seq" : system_seq
		},
		dataType: 'json',
		success: function(data) {
			if(data.result > 0) {
				if($('#year_select').val() == yearval && $('#report_select').val() == reportType) {	
					
					if(data.seq != null) {
						seqDiv.val(data.seq);
						report_word_make(data.seq,data.fullpath);
					}
					if(reportDiv.hasClass('report_disable')) {
						reportDiv.removeClass('report_disable');
						reportDiv.find('.report_new_btn').css('display','');
					}
					if(reportDiv.hasClass('report_month_t')) {
						//reportDiv.removeClass('report_month_t');
						reportDiv.find('.report_month_new_btn').css('display','');
					}
					reportDiv.find('.report_loading').css('display','none');
					reportDiv.next().prop('disabled',false);
				}
				//data.result == 1 ? alert('보고서 생성 완료'):alert('보고서 재생성 완료');
				reportIntroChange($('#report_select'));
			} else {
 				alert('데이터 베이스 에러')
			}
			
		},
		error: function() {
			alert('pdf 생성 에러');
		}
	});
	

}

function report_replace(btngroup) {
	var checkgroup;
	var reportDiv;
	var btn_group;
	var seqDiv;
	
	var reportClass = 'report';
	var month = 0;
	var period_type = 0;
	if($('.check_top:checked').length>0) {
		checkgroup = ".check_top";
		btn_group = $('.year_group');
		period_type = 4;
	} else if($('.check_mid:checked').length>0) {
		checkgroup = ".check_mid";
		btn_group = $('.quarter_group');
		period_type = 2;
	} else if($('.check_bot:checked').length>0) {
		checkgroup = ".check_bot";
		btn_group = $('.month_group');
		period_type = 1;
	}

	$(checkgroup).each(function() {
		if($(this).is(':checked')) {
			reportDiv = $(this).parent().children('.'+reportClass);
			seqDiv = $(this).parent().children('.report_seq');
			month = $(checkgroup).index(this)+1;
		}
	});

	if(period_type == 2)month = (month-1)*3+1;
	
	if(period_type == 1 && createSummaryMonth(month)){
		reportDiv.find('.report_loading').css('display','block');
		make_report_ajax(period_type, month, seqDiv, reportDiv);
	}else if(period_type != 1){
		reportDiv.find('.report_loading').css('display','block');
		make_report_ajax(period_type, month, seqDiv, reportDiv);
	}
}

function report_delete(btngroup){
	var checkgroup;
	var report_seq;
	var reportDiv;
	var btn_group = $('.btn_group');
	var reportClass = 'report';
	
	
	if($('.check_top:checked').length>0) {checkgroup = ".check_top";}
	if($('.check_mid:checked').length>0) {checkgroup = ".check_mid";}
	if($('.check_bot:checked').length>0) {checkgroup = ".check_bot";}
	checkgroup='.check_box';
	$(checkgroup).each(function(){
		if($(this).is(':checked')) {
			report_seq = $(this).parent().children('.report_seq').val();
			reportDiv = $(this).parent().children('.'+reportClass);
		}
	});

	reportDiv.addClass(reportClass+'_disable');
	reportDiv.removeClass(reportClass+'_active');
	reportDiv.find('.report_loading').css('display','block');
	reportDiv.find('.report_new_btn').css('display','none');
	reportDiv.find('.m_report_new_btn').css('display','none');
	reportDiv.parent().children('.userfile_yn').val('N');
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportListDelete.html',
		data: { 
			"report_seq" : report_seq
		},
		dataType: 'json',
		success: function(data) {
			if(data == 1){
				alert('삭제 성공')
				reportDiv.find('.report_loading').css('display','');
				reportDiv.find('.report_new_btn').css('display','');
				reportDiv.find('.m_report_new_btn').css('display','');
				reportDiv.parent().children('.check_box').removeAttr('checked');
				reportDiv.parent().children('.report_seq').val('');
				reportDiv.parent().children('.check_box').prop('disabled',true);
				btn_group.css('display','');
			}
		},
		error: function(data){
			alert('삭제 실패');
		}
	});
}

/*
 * 월마감 생성
 * 현재는 월마감 테이블 구분없이 전부 만듦
 * 월마감전 일마감 테이블에서 마감데이터가 있는지 확인하며
 * 하나도 존재하지 않으면 진행치 않음
 */
function createSummaryMonth(month){
	var startDate = new Date(year, month-1, 1);
	var endDate = new Date(year, month, 0);
	var summaryCount = 0;
	var makeYn = false;
	var reportType = $('#report_select').val();
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/checkBeforeReport.html',
		async: false,
		data: { 
			"startDate" : startDate.format('yyyyMMdd'),
			"endDate" : endDate.format('yyyyMMdd'),
			"report_type" : reportType,
			"log_delimiter" : '0'
		},
		dataType: 'json',
		success: function(data) {
			summaryCount = data;
		}
	});
	if(summaryCount > 0){
		$.ajax({
			type: 'POST',
			url: rootPath + '/summaryMngt/checkSummaryDaily.html',
			async: false,
			data: { 
				"startDate" : startDate.format('yyyyMMdd'),
				"endDate" : endDate.format('yyyyMMdd')
			},
			dataType: 'json',
			success: function(data) {
				summaryCount = data;
				if(summaryCount > 0){
					$.ajax({
						type: 'POST',
						url: rootPath + '/summaryMngt/createSummaryMonth.html',
						async: false,
						data: { 
							"startDate" : startDate.format('yyyyMMdd'),
							"endDate" : endDate.format('yyyyMMdd')
						},
						dataType: 'json',
						success: function(data) {
							makeYn = true;	//마감데이터 확인 후 월마감 진행
						}
					});
				}else{
					alert('일간 마감 데이터가 없습니다.');
				}
			}
		});
	}else{
		alert('이전달 보고서가 생성되어 있어야 합니다.');
	}
	
	
	return makeYn;
}

function report_allDisable(){
	$('.report_year').addClass('report_disable');
	$('.report_quarter').addClass('report_disable');
	$('.report_month').addClass("report_disable");
	$('.report_month_t').addClass("report_disable");
	$('.report_active').removeClass("report_active");
	$('.report_unActivity').removeClass("report_unActivity");
	$('.check_box').prop('disabled',true);
	$('.btn_group').css('display','none');
	$('.report_seq').val('');
}

/* 연도와 보고서 타입 기준으로 전체 보고서 가져옴 */
/* 보고서 기간타입 (period_type) 컬럼 추가 하기 전에(11월 6일 추가됨) 생성한 보고서는 가져오지 못함 */
function report_allCheck(yearval){
	report_allDisable();
	var reportType = report_type;
	var date = new Date();
	var afterMonth = date.format('yyyyMM');
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/findReportOption.html',
		data: { 
			"code_id" : reportType
		},
		dataType: 'json',
		async:false,
		success: function(data) {
			if(data.length>0){
				var chartbool = data[0].input_chart == 'true'? true:false;
				var descriptionbool = data[0].input_description == 'true'? true:false;
				$('#report_description').val(data[0].description);
				$('#input_chart').prop('checked',chartbool);
				$('#input_description').prop('checked',descriptionbool);
				input_description($('#input_description'));
			}else{
				$('#report_description').val("");
				$('#input_chart').prop('checked',false);
				$('#input_description').prop('checked',false);
				input_description($('#input_description'));
			}
		}
	});
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/report_Allcheck.html',
		data: { 
			"year" : yearval,
			"reportType" : reportType,
		},
		dataType: 'json',
		success: function(data) {
			if($('#year_select').val() == yearval && $('#report_select').val() == reportType){
				var checkboxHTML = '<input class="check_box" type="checkbox">';
				for(var i = 0; i < data.length ; i++){
					var month = data[i].proc_month.substring(4,6);
					if(data[i].period_type=='4'){
						$('.report_year').eq(0).removeClass('report_disable');
						$('.report_year').eq(0).next().prop('disabled',false).next().val(data[i].report_seq);
						var reportlabel = $('.report_year').eq(0).parent().children('.userfile_yn').val(data[i].userfile_path);
					}
					else if(data[i].period_type=='3'){
						var num = Math.ceil(month/6);
						$('.report_year').eq(num).removeClass('report_disable');
						$('.report_year').eq(num).next().prop('disabled',false).next().val(data[i].report_seq);
						var reportlabel = $('.report_year').eq(num).parent().children('.userfile_yn').val(data[i].userfile_path);
					}
					else if(data[i].period_type=='2'){
						var num =  Math.ceil((month-1)/3);
						$('.report_quarter').eq(num).removeClass('report_disable');
						$('.report_quarter').eq(num).next().prop('disabled',false).next().val(data[i].report_seq);
						var reportlabel = $('.report_quarter').eq(num).parent().children('.userfile_yn').val(data[i].userfile_path);
					}
					else if(data[i].period_type=='1'){
						var num = month-1;
						var procdate = data[i].proc_month;
						$('.report_month_t').eq(num).removeClass("report_disable");
						if(procdate>install_month){
							$('.report_month_t').eq(num).next().prop('disabled',false).next().val(data[i].report_seq);
							var reportlabel = $('.report_month_t').eq(num).parent().children('.userfile_yn').val(data[i].userfile_path);
						}else{
							$('.report_month_t').eq(num).addClass("report_unActivity");
						}
					}
				}
			}
			$('.report_month_t').each(function(idx,val){
				var procdate = yearval;
				idx += 1;
				if(idx<10){
					procdate = procdate+"0"+idx;
				}else{
					procdate = procdate+""+idx;
				}
				if(procdate<=install_month){
					$(val).removeClass("report_disable");
					$(val).addClass("report_unActivity");
				}
				if(procdate>afterMonth){
					$(val).removeClass("report_disable");
					$(val).addClass("report_unActivity");
				}
			});
		},
		beforeSend:function() {
			$('.report_new_btn').css('display','none');
			$('.report_loading').css('display','block');
	    },
	    complete:function() {
	    	$('.report_new_btn').css('display','');
			$('.report_loading').css('display','none');
	    }
	});
}

var userfile_yn;
var userfilebtn;
function report_upload(btngroup){
	var file = $('#file');
	file.click();
	var checkgroup;
	var report_seq;
	var reportDiv;
	var btn_group;
	var reportClass;
	var report_type;
	userfile_yn = null;
	userfilebtn = null;
	if($('.check_top:checked').length>0) {
		checkgroup = ".check_top";
		btn_group = '.year_group';
		userfilebtn=$('#year_seq').prev()
	}
	if($('.check_mid:checked').length>0) {
		checkgroup = ".check_mid";
		btn_group = '.quarter_group';
		userfilebtn=$('#quarter_seq').prev()
	}
	if($('.check_bot:checked').length>0) {
		checkgroup = ".check_bot";
		btn_group = '.month_group';
		userfilebtn=$('#month_seq').prev()
	}
	$(checkgroup).each(function(){
		if($(this).is(':checked')) {
			userfile_yn = $(this).parent().children('.userfile_yn');
		}
	});	
	report_seq = $(btn_group).children('.btn_seq').val();
	report_type = $('#report_select').val();
	$('#fileForm > input[name=report_seq]').val(report_seq);
	$('#fileForm > input[name=report_type]').val(report_type);
}

function report_userfile_download(btngroup){
	var report_seq = $('#btn_seq').val();

	$('input[name=report_seq]').val(report_seq);
	$("#listForm").attr("action",rootPath+"/report/report_userfile.html");
	$("#listForm").submit();
}

function findUploadReport(report_seq){
	$('#uploadReportName').val('');
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/uploadReportOne.html',
		data: { 
			"report_seq" : report_seq
		},
		dataType: 'json',
		success: function(data) {
			if(data != '' && data != null){
				$('#uploadReportName').val(data);
				$('.userfilebtn').css('display','inline');
			}else{
				$('#uploadReportName').val('업로드 파일 없음');
				$('.userfilebtn').css('display','none');
			}
		}
	});
}

function checkBoxChange(target,checkLine){
	var checkNum = $(target).index(checkLine);
	$(".check_box").prev().removeClass('report_active');
	$(target).prev().addClass("report_active");
	$('.check_box').prop('checked',false);
	$(checkLine).eq(checkNum).prop('checked',true);
	$('#btn_seq').val($(target).next().val());
	var obj = $(target).parent().first().find(".myclassTemp");
	findUploadReport($('#btn_seq').val());
	$("#LabelAtch").text(obj.parent().text()+" 보고서");
}

function modalClose(tag){
	$(tag).closest('.modal').css('display','none');
}

function modalOpen(){
	$('#report_option_modal').css('display','block');
}

function input_description(tag){
	if($(tag).prop('checked')){
		$('#report_description').removeAttr('disabled');
	}else{
		$('#report_description').attr('disabled','disabled');
	}
}

/*설정에서 가져온 차트포함여부, 총평포함 여부, 총평에 대한 데이터 추가*/
function saveReportOption(){
	var reportType = $('#report_select').val();
	var optionResult = 0;
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/findReportOption.html',
		data: { 
			"code_id" : reportType
		},
		dataType: 'json',
		async:false,
		success: function(data) {
			if(data.length>0){
				optionResult = 1;
			}
		}
	});
	
	if(optionResult>0){
		$.ajax({
			type: 'POST',
			url: rootPath + '/report/saveReportOption.html',
			data: { 
				"code_id" : reportType,
				"description" : $('#report_description').val(),
				"input_chart" : $('#input_chart').prop('checked'),
				"input_description" : $('#input_description').prop('checked')
			},
			dataType: 'json',
			success: function(data) {
				alert('적용되었습니다.');
				$('#report_option_modal').css('display','none');
			}
		});
	}else{
		$.ajax({
			type: 'POST',
			url: rootPath + '/report/addReportOption.html',
			data: { 
				"code_id" : reportType,
				"description" : $('#report_description').val(),
				"input_chart" : $('#input_chart').prop('checked'),
				"input_description" : $('#input_description').prop('checked')
			},
			dataType: 'json',
			success: function(data) {
				alert('적용되었습니다.');
				$('#report_option_modal').css('display','none');
			}
		});
	}
}

function report_word() {
	$.ajax({
		url: rootPath + '/report/report_Download.html',
		type:'POST',
		data: { 
			"num" : $('.btn_group').children('.btn_seq').val()
		},
		dataType: "json",
		success: function(data){
			if(data.html_encode == ''){
				alert('보고서 데이터를 찾을 수 없습니다')
			}else{
				downLoad_type(data);
			}
		},
		error: function(){
			alert('실패');
		}
	});
}

$(document).ready(function(){
	
	reportIntroChange($('#report_select'));
	
	//report_allCheck(year);
	
	$('.check_top').change(function(){
		if($(this).prop('checked')){
			checkBoxChange(this,'.check_top');
		}else{
			$(this).prev().removeClass("report_active");
			$('#report_seq').val('');
		}
		btn_active();
	});
	
	$('.check_mid').change(function(){
		if($(this).prop('checked')){
			checkBoxChange(this,'.check_mid');
		}else{
			$(this).prev().removeClass("report_active");
			$('#report_seq').val('');
		}
		btn_active();
	});
	
	$('.check_bot').change(function(){
		if($(this).prop('checked')){
			checkBoxChange(this,'.check_bot');
		}else{
			$(this).prev().removeClass('report_active');
			$('#report_seq').val('');
		}
		btn_active();
	});
	
	$('.file').change(function(){
		var filebtn = userfilebtn;
		var fileyn = userfile_yn;
		var options = {
				success : function(data) {
					$(".file").val("");
					if(data == 1){
						alert('업로드 완료');
						filebtn.css('display','inline-block');
						fileyn.val('Y');
					}
				},
				error : function(error) {
				}
		  };
		$("#fileForm").ajaxSubmit(options);
		console.log($(this));
	});
	
	$(document).on("click",'.report_unActivity',function(){
		alert('생성할수 없는 날짜의 보고서 입니다.');
	});
});