$(function(){
var chart = AmCharts.makeChart("chargeArea", {
    "type": "serial",
    "theme": "light",
    "marginRight": 80,
    "dataProvider": [{
        "lineColor": "#b7e021",
        "date": "2012-01-01",
        "duration": 408
    }, {
        "date": "2012-01-02",
        "duration": 482
    }, {
        "date": "2012-01-03",
        "duration": 562
    }, {
        "date": "2012-01-04",
        "duration": 379
    }, {
        "lineColor": "#fbd51a",
        "date": "2012-01-05",
        "duration": 501
    }, {
        "date": "2012-01-06",
        "duration": 443
    }, {
        "date": "2012-01-07",
        "duration": 405
    }, {
        "date": "2012-01-08",
        "duration": 309,
        "lineColor": "#2498d2"
    }, {
        "date": "2012-01-09",
        "duration": 287
    }, {
        "date": "2012-01-10",
        "duration": 485
    }, {
        "date": "2012-01-11",
        "duration": 890
    }, {
        "date": "2012-01-12",
        "duration": 810
    }],
    "balloon": {
        "cornerRadius": 6,
        "horizontalPadding": 15,
        "verticalPadding": 10
    },
    "valueAxes": [{
        "duration": "mm",
        "durationUnits": {
            "hh": "h ",
            "mm": "min"
        },
        "axisAlpha": 0
    }],
    "graphs": [{
        "bullet": "square",
        "bulletBorderAlpha": 1,
        "bulletBorderThickness": 1,
        "fillAlphas": 0.3,
        "fillColorsField": "lineColor",
        "legendValueText": "[[value]]",
        "lineColorField": "lineColor",
        "title": "duration",
        "valueField": "duration"
    }],
    "chartScrollbar": {

    },
    "chartCursor": {
        "categoryBalloonDateFormat": "YYYY MMM DD",
        "cursorAlpha": 0,
        "fullWidth": true
    },
    "dataDateFormat": "YYYY-MM-DD",
    "categoryField": "date",
    "categoryAxis": {
        "dateFormats": [{
            "period": "DD",
            "format": "DD"
        }, {
            "period": "WW",
            "format": "MMM DD"
        }, {
            "period": "MM",
            "format": "MMM"
        }, {
            "period": "YYYY",
            "format": "YYYY"
        }],
        "parseDates": true,
        "autoGridCount": false,
        "axisColor": "#555555",
        "gridAlpha": 0,
        "gridCount": 50
    },
    "export": {
        "enabled": true
    }
});

//chart.addListener("dataUpdated", zoomChart);

var chartData = [ {
	  "date": "2012-01-01",
	  "distance": 227,
	  "townName": "New York",
	  "townName2": "New York",
	  "townSize": 25,
	  "duration": 408
	}, {
	  "date": "2012-01-02",
	  "distance": 371,
	  "townName": "Washington",
	  "townSize": 14,
	  "duration": 482
	}, {
	  "date": "2012-01-03",
	  "distance": 433,
	  "townName": "Wilmington",
	  "townSize": 6,
	  "duration": 562
	}, {
	  "date": "2012-01-04",
	  "distance": 345,
	  "townName": "Jacksonville",
	  "townSize": 7,
	  "duration": 379
	}, {
	  "date": "2012-01-05",
	  "distance": 480,
	  "townName": "Miami",
	  "townName2": "Miami",
	  "townSize": 10,
	  "duration": 501
	}, {
	  "date": "2012-01-06",
	  "distance": 386,
	  "townName": "Tallahassee",
	  "townSize": 7,
	  "duration": 443
	}, {
	  "date": "2012-01-07",
	  "distance": 348,
	  "townName": "New Orleans",
	  "townSize": 10,
	  "duration": 405
	}, {
	  "date": "2012-01-08",
	  "distance": 238,
	  "townName": "Houston",
	  "townName2": "Houston",
	  "townSize": 16,
	  "duration": 309
	}, {
	  "date": "2012-01-09",
	  "distance": 218,
	  "townName": "Dalas",
	  "townSize": 17,
	  "duration": 287
	}, {
	  "date": "2012-01-10",
	  "distance": 349,
	  "townName": "Oklahoma City",
	  "townSize": 11,
	  "duration": 485
	}, {
	  "date": "2012-01-11",
	  "distance": 603,
	  "townName": "Kansas City",
	  "townSize": 10,
	  "duration": 890
	}, {
	  "date": "2012-01-12",
	  "distance": 534,
	  "townName": "Denver",
	  "townName2": "Denver",
	  "townSize": 18,
	  "duration": 810
	}, {
	  "date": "2012-01-13",
	  "townName": "Salt Lake City",
	  "townSize": 12,
	  "distance": 425,
	  "duration": 670,
	  "alpha": 0.4
	}, {
	  "date": "2012-01-14",
	  "duration": 470,
	  "townName": "Las Vegas",
	  "townName2": "Las Vegas",
	  "bulletClass": "lastBullet"
	}, {
	  "date": "2012-01-15"
	}, {
	  "date": "2012-01-16"
	}, {
	  "date": "2012-01-17"
	}, {
	  "date": "2012-01-18"
	}, {
	  "date": "2012-01-19"
	} ];

	var chartBar = AmCharts.makeChart( "chartBar", {
	  "type": "serial",
	"theme": "light",

	  "dataDateFormat": "YYYY-MM-DD",
	  "dataProvider": chartData,

	  "addClassNames": true,
	  "startDuration": 1,
	  //"color": "#FFFFFF",
	  "marginLeft": 0,

	  "categoryField": "date",
	  "categoryAxis": {
	    "parseDates": true,
	    "minPeriod": "DD",
	    "autoGridCount": false,
	    "gridCount": 50,
	    "gridAlpha": 0.1,
	    "gridColor": "#FFFFFF",
	    "axisColor": "#555555",
	    "dateFormats": [ {
	      "period": 'DD',
	      "format": 'DD'
	    }, {
	      "period": 'WW',
	      "format": 'MMM DD'
	    }, {
	      "period": 'MM',
	      "format": 'MMM'
	    }, {
	      "period": 'YYYY',
	      "format": 'YYYY'
	    } ]
	  },

	  "valueAxes": [ {
	    "id": "a1",
	    "title": "distance",
	    "gridAlpha": 0,
	    "axisAlpha": 0
	  }, {
	    "id": "a2",
	    "position": "right",
	    "gridAlpha": 0,
	    "axisAlpha": 0,
	    "labelsEnabled": false
	  }, {
	    "id": "a3",
	    "title": "duration",
	    "position": "right",
	    "gridAlpha": 0,
	    "axisAlpha": 0,
	    "inside": true,
	    "duration": "mm",
	    "durationUnits": {
	      "DD": "d. ",
	      "hh": "h ",
	      "mm": "min",
	      "ss": ""
	    }
	  } ],
	  "graphs": [ {
	    "id": "g1",
	    "valueField": "distance",
	    "title": "distance",
	    "type": "column",
	    "fillAlphas": 0.9,
	    "valueAxis": "a1",
	    "balloonText": "[[value]] miles",
	    "legendValueText": "[[value]] mi",
	    "legendPeriodValueText": "total: [[value.sum]] mi",
	    "lineColor": "#263138",
	    "alphaField": "alpha"
	  }, {
	    "id": "g2",
	    "valueField": "latitude",
	    "classNameField": "bulletClass",
	    "title": "latitude/city",
	    "type": "line",
	    "valueAxis": "a2",
	    "lineColor": "#786c56",
	    "lineThickness": 1,
	    "legendValueText": "[[value]]/[[description]]",
	    "descriptionField": "townName",
	    "bullet": "round",
	    "bulletSizeField": "townSize",
	    "bulletBorderColor": "#786c56",
	    "bulletBorderAlpha": 1,
	    "bulletBorderThickness": 2,
	    "bulletColor": "#000000",
	    "labelText": "[[townName2]]",
	    "labelPosition": "right",
	    "balloonText": "latitude:[[value]]",
	    "showBalloon": true,
	    "animationPlayed": true
	  }, {
	    "id": "g3",
	    "title": "duration",
	    "valueField": "duration",
	    "type": "line",
	    "valueAxis": "a3",
	    "lineColor": "#ff5755",
	    "balloonText": "[[value]]",
	    "lineThickness": 1,
	    "legendValueText": "[[value]]",
	    "bullet": "square",
	    "bulletBorderColor": "#ff5755",
	    "bulletBorderThickness": 1,
	    "bulletBorderAlpha": 1,
	    "dashLengthField": "dashLength",
	    "animationPlayed": true
	  } ],

	  "chartCursor": {
	    "zoomable": false,
	    "categoryBalloonDateFormat": "DD",
	    "cursorAlpha": 0,
	    "valueBalloonsEnabled": false
	  },
	  "legend": {
	    "bulletType": "round",
	    "equalWidths": false,
	    "valueWidth": 120,
	    "useGraphSettings": true,
	    //"color": "#FFFFFF"
	  }
	} );
	
	
	var overChart = AmCharts.makeChart( "overChart", {
		  "type": "serial",
		  "addClassNames": true,
		  "theme": "light",
		  "autoMargins": false,
		  "marginLeft": 30,
		  "marginRight": 8,
		  "marginTop": 10,
		  "marginBottom": 26,
		  "balloon": {
		    "adjustBorderColor": false,
		    "horizontalPadding": 10,
		    "verticalPadding": 8,
		    "color": "#ffffff"
		  },

		  "dataProvider": [ {
		    "year": "하루 임계치 초과",
		    "income": 30
		  }],
		  "valueAxes": [ {
		    "axisAlpha": 0,
		    "position": "left"
		  } ],
		  "startDuration": 1,
		  "graphs": [ {
		    "alphaField": "alpha",
		    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
		    "fillAlphas": 1,
		    "title": "Income",
		    "type": "column",
		    "valueField": "income",
		    "dashLengthField": "dashLengthColumn"
		  }],
		  "categoryField": "year",
		  "categoryAxis": {
		    "gridPosition": "start",
		    "axisAlpha": 0,
		    "tickLength": 0
		  },
		  "export": {
		    "enabled": true
		  }
		} );
	
	var specialChart = AmCharts.makeChart( "specialChart", {
		  "type": "serial",
		  "addClassNames": true,
		  "theme": "light",
		  "autoMargins": false,
		  "marginLeft": 30,
		  "marginRight": 8,
		  "marginTop": 10,
		  "marginBottom": 26,
		  "balloon": {
		    "adjustBorderColor": false,
		    "horizontalPadding": 10,
		    "verticalPadding": 8,
		    "color": "#ffffff"
		  },

		  "dataProvider": [ {
		    "year": "임계치",
		    "income": 20
		  },
		  {
			 "year": "나의 처리건수",
			 "income": 100
		  }],
		  "valueAxes": [ {
		    "axisAlpha": 0,
		    "position": "left"
		  } ],
		  "startDuration": 1,
		  "graphs": [ {
		    "alphaField": "alpha",
		    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
		    "fillAlphas": 1,
		    "title": "Income",
		    "type": "column",
		    "valueField": "income",
		    "dashLengthField": "dashLengthColumn"
		  }],
		  "categoryField": "year",
		  "categoryAxis": {
		    "gridPosition": "start",
		    "axisAlpha": 0,
		    "tickLength": 0
		  },
		  "export": {
		    "enabled": true
		  }
		});
	
	var abnormalChart = AmCharts.makeChart( "abnormalChart", {
		  "type": "serial",
		  "addClassNames": true,
		  "theme": "light",
		  "autoMargins": false,
		  "marginLeft": 30,
		  "marginRight": 8,
		  "marginTop": 10,
		  "marginBottom": 26,
		  "balloon": {
		    "adjustBorderColor": false,
		    "horizontalPadding": 10,
		    "verticalPadding": 8,
		    "color": "#ffffff"
		  },

		  "dataProvider": [ {
		    "year": "임계치",
		    "income": 30
		  },
		  {
			 "year": "나의 처리건수",
			 "income": 60
		  }],
		  "valueAxes": [ {
		    "axisAlpha": 0,
		    "position": "left"
		  } ],
		  "startDuration": 1,
		  "graphs": [ {
		    "alphaField": "alpha",
		    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
		    "fillAlphas": 1,
		    "title": "Income",
		    "type": "column",
		    "valueField": "income",
		    "dashLengthField": "dashLengthColumn"
		  }],
		  "categoryField": "year",
		  "categoryAxis": {
		    "gridPosition": "start",
		    "axisAlpha": 0,
		    "tickLength": 0
		  },
		  "export": {
		    "enabled": true
		  }
		});
	
	var timeChart = AmCharts.makeChart( "timeChart", {
		  "type": "serial",
		  "addClassNames": true,
		  "theme": "light",
		  "autoMargins": false,
		  "marginLeft": 30,
		  "marginRight": 8,
		  "marginTop": 10,
		  "marginBottom": 26,
		  "balloon": {
		    "adjustBorderColor": false,
		    "horizontalPadding": 10,
		    "verticalPadding": 8,
		    "color": "#ffffff"
		  },

		  "dataProvider": [ {
		    "year": "임계치",
		    "income": 20
		  },
		  {
			 "year": "나의 처리건수",
			 "income": 0
		  }],
		  "valueAxes": [ {
		    "axisAlpha": 0,
		    "position": "left"
		  } ],
		  "startDuration": 1,
		  "graphs": [ {
		    "alphaField": "alpha",
		    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
		    "fillAlphas": 1,
		    "title": "Income",
		    "type": "column",
		    "valueField": "income",
		    "dashLengthField": "dashLengthColumn"
		  }],
		  "categoryField": "year",
		  "categoryAxis": {
		    "gridPosition": "start",
		    "axisAlpha": 0,
		    "tickLength": 0
		  },
		  "export": {
		    "enabled": true
		  }
		});
	
	monthChart();
	weekChart();
	dayChart();
	personalChart();
	systemPrivChart();
	systemPrivBarChart();
});


function monthChart(){
	var monthChart = AmCharts.makeChart( "monthChartDiv", {
		  "type": "serial",
		  "addClassNames": true,
		  "theme": "light",
		  "autoMargins": false,
		  "marginLeft": 30,
		  "marginRight": 8,
		  "marginTop": 10,
		  "marginBottom": 26,
		  "balloon": {
		    "adjustBorderColor": false,
		    "horizontalPadding": 10,
		    "verticalPadding": 8,
		    "color": "#ffffff"
		  },

		  "dataProvider": [ {
		    "year": 2009,
		    "income": 23.5,
		    "expenses": 21.1
		  }, {
		    "year": 2010,
		    "income": 26.2,
		    "expenses": 30.5
		  }, {
		    "year": 2011,
		    "income": 30.1,
		    "expenses": 34.9
		  }, {
		    "year": 2012,
		    "income": 29.5,
		    "expenses": 31.1
		  }, {
		    "year": 2013,
		    "income": 30.6,
		    "expenses": 28.2,
		    "dashLengthLine": 5
		  }, {
		    "year": 2014,
		    "income": 34.1,
		    "expenses": 32.9,
		    "dashLengthColumn": 5,
		    "alpha": 0.2,
		    "additional": "(projection)"
		  } ],
		  "valueAxes": [ {
		    "axisAlpha": 0,
		    "position": "left"
		  } ],
		  "startDuration": 1,
		  "graphs": [ {
		    "alphaField": "alpha",
		    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
		    "fillAlphas": 1,
		    "title": "Income",
		    "type": "column",
		    "valueField": "income",
		    "dashLengthField": "dashLengthColumn"
		  }, {
		    "id": "graph2",
		    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
		    "bullet": "round",
		    "lineThickness": 3,
		    "bulletSize": 7,
		    "bulletBorderAlpha": 1,
		    "bulletColor": "#FFFFFF",
		    "useLineColorForBulletBorder": true,
		    "bulletBorderThickness": 3,
		    "fillAlphas": 0,
		    "lineAlpha": 1,
		    "title": "Expenses",
		    "valueField": "expenses",
		    "dashLengthField": "dashLengthLine"
		  } ],
		  "categoryField": "year",
		  "categoryAxis": {
		    "gridPosition": "start",
		    "axisAlpha": 0,
		    "tickLength": 0
		  },
		  "export": {
		    "enabled": true
		  }
		} );
}

function weekChart(){
	var monthChart = AmCharts.makeChart( "weekChartDiv", {
		  "type": "serial",
		  "addClassNames": true,
		  "theme": "light",
		  "autoMargins": false,
		  "marginLeft": 30,
		  "marginRight": 8,
		  "marginTop": 10,
		  "marginBottom": 26,
		  "balloon": {
		    "adjustBorderColor": false,
		    "horizontalPadding": 10,
		    "verticalPadding": 8,
		    "color": "#ffffff"
		  },

		  "dataProvider": [ {
		    "year": 2009,
		    "income": 23.5,
		    "expenses": 21.1
		  }, {
		    "year": 2010,
		    "income": 26.2,
		    "expenses": 30.5
		  }, {
		    "year": 2011,
		    "income": 30.1,
		    "expenses": 34.9
		  }, {
		    "year": 2012,
		    "income": 29.5,
		    "expenses": 31.1
		  }, {
		    "year": 2013,
		    "income": 30.6,
		    "expenses": 28.2,
		    "dashLengthLine": 5
		  }, {
		    "year": 2014,
		    "income": 34.1,
		    "expenses": 32.9,
		    "dashLengthColumn": 5,
		    "alpha": 0.2,
		    "additional": "(projection)"
		  } ],
		  "valueAxes": [ {
		    "axisAlpha": 0,
		    "position": "left"
		  } ],
		  "startDuration": 1,
		  "graphs": [ {
		    "alphaField": "alpha",
		    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
		    "fillAlphas": 1,
		    "title": "Income",
		    "type": "column",
		    "valueField": "income",
		    "dashLengthField": "dashLengthColumn"
		  }, {
		    "id": "graph2",
		    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
		    "bullet": "round",
		    "lineThickness": 3,
		    "bulletSize": 7,
		    "bulletBorderAlpha": 1,
		    "bulletColor": "#FFFFFF",
		    "useLineColorForBulletBorder": true,
		    "bulletBorderThickness": 3,
		    "fillAlphas": 0,
		    "lineAlpha": 1,
		    "title": "Expenses",
		    "valueField": "expenses",
		    "dashLengthField": "dashLengthLine"
		  } ],
		  "categoryField": "year",
		  "categoryAxis": {
		    "gridPosition": "start",
		    "axisAlpha": 0,
		    "tickLength": 0
		  },
		  "export": {
		    "enabled": true
		  }
		} );

}

function dayChart(){
	var chart = AmCharts.makeChart("dayChartDiv", {
	    "type": "serial",
	    "theme": "light",
	    "marginRight":30,
	    "legend": {
	        "equalWidths": false,
	        "periodValueText": "total: [[value.sum]]",
	        "position": "top",
	        "valueAlign": "left",
	        "valueWidth": 100
	    },
	    "dataProvider": [{
	        "year": 1994,
	        "cars": 1587,
	        "motorcycles": 650,
	        "bicycles": 121
	    }, {
	        "year": 1995,
	        "cars": 1567,
	        "motorcycles": 683,
	        "bicycles": 146
	    }, {
	        "year": 1996,
	        "cars": 1617,
	        "motorcycles": 691,
	        "bicycles": 138
	    }, {
	        "year": 1997,
	        "cars": 1630,
	        "motorcycles": 642,
	        "bicycles": 127
	    }, {
	        "year": 1998,
	        "cars": 1660,
	        "motorcycles": 699,
	        "bicycles": 105
	    }, {
	        "year": 1999,
	        "cars": 1683,
	        "motorcycles": 721,
	        "bicycles": 109
	    }, {
	        "year": 2000,
	        "cars": 1691,
	        "motorcycles": 737,
	        "bicycles": 112
	    }, {
	        "year": 2001,
	        "cars": 1298,
	        "motorcycles": 680,
	        "bicycles": 101
	    }, {
	        "year": 2002,
	        "cars": 1275,
	        "motorcycles": 664,
	        "bicycles": 97
	    }, {
	        "year": 2003,
	        "cars": 1246,
	        "motorcycles": 648,
	        "bicycles": 93
	    }, {
	        "year": 2004,
	        "cars": 1318,
	        "motorcycles": 697,
	        "bicycles": 111
	    }, {
	        "year": 2005,
	        "cars": 1213,
	        "motorcycles": 633,
	        "bicycles": 87
	    }, {
	        "year": 2006,
	        "cars": 1199,
	        "motorcycles": 621,
	        "bicycles": 79
	    }, {
	        "year": 2007,
	        "cars": 1110,
	        "motorcycles": 210,
	        "bicycles": 81
	    }, {
	        "year": 2008,
	        "cars": 1165,
	        "motorcycles": 232,
	        "bicycles": 75
	    }, {
	        "year": 2009,
	        "cars": 1145,
	        "motorcycles": 219,
	        "bicycles": 88
	    }, {
	        "year": 2010,
	        "cars": 1163,
	        "motorcycles": 201,
	        "bicycles": 82
	    }, {
	        "year": 2011,
	        "cars": 1180,
	        "motorcycles": 285,
	        "bicycles": 87
	    }, {
	        "year": 2012,
	        "cars": 1159,
	        "motorcycles": 277,
	        "bicycles": 71
	    }],
	    "valueAxes": [{
	        "stackType": "regular",
	        "gridAlpha": 0.07,
	        "position": "left",
	        "title": "Traffic incidents"
	    }],
	    "graphs": [{
	        "balloonText": "<img src='http://www.amcharts.com/lib/3/images/car.png' style='vertical-align:bottom; margin-right: 10px; width:28px; height:21px;'><span style='font-size:14px; color:#000000;'><b>[[value]]</b></span>",
	        "fillAlphas": 0.6,
	        "hidden": true,
	        "lineAlpha": 0.4,
	        "title": "Cars",
	        "valueField": "cars"
	    }, {
	        "balloonText": "<img src='http://www.amcharts.com/lib/3/images/motorcycle.png' style='vertical-align:bottom; margin-right: 10px; width:28px; height:21px;'><span style='font-size:14px; color:#000000;'><b>[[value]]</b></span>",
	        "fillAlphas": 0.6,
	        "lineAlpha": 0.4,
	        "title": "Motorcycles",
	        "valueField": "motorcycles"
	    }, {
	        "balloonText": "<img src='http://www.amcharts.com/lib/3/images/bicycle.png' style='vertical-align:bottom; margin-right: 10px; width:28px; height:21px;'><span style='font-size:14px; color:#000000;'><b>[[value]]</b></span>",
	        "fillAlphas": 0.6,
	        "lineAlpha": 0.4,
	        "title": "Bicycles",
	        "valueField": "bicycles"
	    }],
	    "plotAreaBorderAlpha": 0,
	    "marginTop": 10,
	    "marginLeft": 0,
	    "marginBottom": 0,
	    "chartScrollbar": {},
	    "chartCursor": {
	        "cursorAlpha": 0
	    },
	    "categoryField": "year",
	    "categoryAxis": {
	        "startOnAxis": true,
	        "axisColor": "#DADADA",
	        "gridAlpha": 0.07,
	        "title": "Year",
	        "guides": [{
	            category: "2001",
	            toCategory: "2003",
	            lineColor: "#CC0000",
	            lineAlpha: 1,
	            fillAlpha: 0.2,
	            fillColor: "#CC0000",
	            dashLength: 2,
	            inside: true,
	            labelRotation: 90,
	            label: "fines for speeding increased"
	        }, {
	            category: "2007",
	            lineColor: "#CC0000",
	            lineAlpha: 1,
	            dashLength: 2,
	            inside: true,
	            labelRotation: 90,
	            label: "motorcycle fee introduced"
	        }]
	    },
	    "export": {
	    	"enabled": true
	     }
	});

}

function personalChart(){
	var personalChart = AmCharts.makeChart( "personalChart", {
		  "type": "pie",
		  "theme": "light",
		  "titles": [ {
		    "text": "Visitors countries",
		    "size": 16
		  } ],
		  "dataProvider": [ {
		    "country": "United States",
		    "visits": 7252
		  }, {
		    "country": "China",
		    "visits": 3882
		  }, {
		    "country": "Japan",
		    "visits": 1809
		  }, {
		    "country": "Germany",
		    "visits": 1322
		  }, {
		    "country": "United Kingdom",
		    "visits": 1122
		  }, {
		    "country": "France",
		    "visits": 414
		  }, {
		    "country": "India",
		    "visits": 384
		  }, {
		    "country": "Spain",
		    "visits": 211
		  } ],
		  "valueField": "visits",
		  "titleField": "country",
		  "startEffect": "elastic",
		  "startDuration": 2,
		  "labelRadius": 15,
		  "innerRadius": "50%",
		  "depth3D": 10,
		  "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
		  "angle": 15,
		  "export": {
		    "enabled": true
		  }
		} );

}

function systemPrivChart(){
	var systemPrivChart = AmCharts.makeChart("systemPrivChartDiv", {
	    "type": "serial",
		"theme": "light",
	    "legend": {
	        "horizontalGap": 10,
	        "maxColumns": 1,
	        "position": "right",
			"useGraphSettings": true,
			"markerSize": 10
	    },
	    "dataProvider": [{
	        "year": 2003,
	        "europe": 2.5,
	        "namerica": 2.5,
	        "asia": 2.1,
	        "lamerica": 0.3,
	        "meast": 0.2,
	        "africa": 0.1
	    }, {
	        "year": 2004,
	        "europe": 2.6,
	        "namerica": 2.7,
	        "asia": 2.2,
	        "lamerica": 0.3,
	        "meast": 0.3,
	        "africa": 0.1
	    }, {
	        "year": 2005,
	        "europe": 2.8,
	        "namerica": 2.9,
	        "asia": 2.4,
	        "lamerica": 0.3,
	        "meast": 0.3,
	        "africa": 0.1
	    }],
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.5,
	        "gridAlpha": 0
	    }],
	    "graphs": [{
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "Europe",
	        "type": "column",
			"color": "#000000",
	        "valueField": "europe"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "North America",
	        "type": "column",
			"color": "#000000",
	        "valueField": "namerica"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "Asia-Pacific",
	        "type": "column",
			"color": "#000000",
	        "valueField": "asia"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "Latin America",
	        "type": "column",
			"color": "#000000",
	        "valueField": "lamerica"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "Middle-East",
	        "type": "column",
			"color": "#000000",
	        "valueField": "meast"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "Africa",
	        "type": "column",
			"color": "#000000",
	        "valueField": "africa"
	    }],
	    "rotate": true,
	    "categoryField": "year",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "gridAlpha": 0,
	        "position": "left"
	    },
	    "export": {
	    	"enabled": true
	     }
	});

}

