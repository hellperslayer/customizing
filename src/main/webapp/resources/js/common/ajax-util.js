/**
 * 비동기 요청 처리</br>
 * - 반복되는 코드를 줄이기 위함.</br>
 * - 기본적인 구성만 해 놓음.
 * 
 * 
 * @param reqUrl String
 * @param data JSON
 * @param successCallback function(data, dataType)
 * @param errorCallback function(XMLHttpRequest, textStatus, errorThrown)
 * 
 */

// POST 요청
function sendAjaxPostRequest(reqUrl,data,successCallback,errorCallback,actionType){
	ajaxRequest(reqUrl,'POST',data,successCallback,errorCallback,actionType);
}

// GET 요청
function sendAjaxGetRequest(reqUrl,data,successCallback,errorCallback,actionType){
	ajaxRequest(reqUrl,'GET',data,successCallback,errorCallback,actionType);
}

// RequestCall
function ajaxRequest(reqUrl,method,data,successCallback,errorCallback,actionType){
	if(reqUrl == null){
		return;
	}
	
	if(successCallback == null || typeof successCallback != 'function'){
		successCallback = ajaxDefaultSucCallback;
	}
	
	if(errorCallback == null || typeof errorCallback != 'function'){
		errorCallback = ajaxDefaultErrCallback;
	}
	
	$.ajax({
		type: method,
		url: reqUrl,
		dataType: 'json',
		accept : "application/json; charset=UTF-8;",
		contentsType: 'application/json',
		data: data,
		success : function(data, dataType){
			successCallback(data, dataType, actionType);
		},
		error : function(XMLHttpRequest, textStatus, errorThrown){
			errorCallback(XMLHttpRequest, textStatus, errorThrown, actionType);
		}
	});
}

function ajaxDefaultSucCallback(data, dataType, actionType){
	
}

function ajaxDefaultErrCallback(XMLHttpRequest, textStatus, errorThrown, actionType){
	alert("ajax request Error : " + textStatus);
}
