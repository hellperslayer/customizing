
/**
 * 막대그래프 기본설정값
 */
var defaultBarChartConfig = {
	chartColor : null, // 차트 컬러옵션 //[ "#37AE2E", "#EF7575"]
	chartMarge : true, // 막대그래프 병합여부
	barMargin : 60, // 막대그래프 간격설정
	clickHighlight: true, // 막대그래프 클릭시 하이라이트 옵션
	blockRightClick : true, // 우클릭 방지 옵션
	yMinValue : 0, // y축 최소값 설정
	showInfoLabel : true, // 라벨정보창 노출여부
	infoLabelPosition : 'ne', // 라벨정보창 위치 설정, compass direction, nw, n, ne, e, se, s, sw, w.
	infoLabelSide : 'inside' // 라벨정보창 side 설정 outside, inside
};

/**
 * 차트 타겟 element
 */
var elementTargetId = null;
/**
 * jqplot 막대차트 그리기
 * @param target drow 대상 엘리먼트
 * @param data 배열데이터
 * @param title 차트명
 * @param xLables x축 라벨
 * @param xyTitle x,y축 제목
 * @param infoLabels 정보창 라벨
 */
function drowBarChart(target, data, title, xLables, xyTitle, infoLabels){
	// target element validate (필수이므로)
	if(target == null || target == ''){
		alert('This target element has not been set.');
		return;
	}else{
		elementTargetId = target;
	}
	
	// data validate (필수이므로)
	if(data == null){
		alert('Data is not defined.');
		return;
	}
	
	// x축 라벨 설정
	if(xLables == null || xLables.length < data[0].length){
		xLables = [];
	}
	
	// 정보창 라벨 체크 및 설정
	var infoLabelArr = [];
	if(infoLabels == null || infoLabels.length < data.length){
		for(var i = 0; i < data.length; i++){
			var obj = new Object();
			obj.label = '';
			infoLabelArr.push(obj);
		};
	}else{
		for(var i = 0; i < infoLabels.length; i++){
			var obj = new Object();
			obj.label = infoLabels[i];
			infoLabelArr.push(obj);
		}
	}
	
	// x,y 축 라벨 체크
	if(xyTitle == null || xyTitle.length < 2){
		xyTitle = ['',''];
	}
	
	// 차트 그리기
	var chrt = $.jqplot(target, data, {
		seriesColors : defaultBarChartConfig['chartColor'],
		title:title,
		stackSeries: defaultBarChartConfig['chartMarge'],
		captureRightClick: defaultBarChartConfig['blockRightClick'],  
		series:infoLabelArr, // 정보창 라벨명 설정
		axesDefault:{
			show: false,
			tickRenderer : $.jqplot.CanvasAxisTickRenderer,
			tickOptions : {
				fontSize : '8pt',
			}
		},
		seriesDefaults:{
			renderer:$.jqplot.BarRenderer,
			rendererOptions: {
				barMargin: defaultBarChartConfig['barMargin'],
				highlightMouseDown: defaultBarChartConfig['clickHighlight']   
			},
			pointLabels: {show: true} // 그래프별 수치 노출여부
		},
		axes: {
			xaxis: { // x축 설정
				label: xyTitle[0], // x축 라벨
				renderer: $.jqplot.CategoryAxisRenderer,
				ticks:xLables,
			},
			yaxis: { // y축 설정
				label: xyTitle[1], // y축 라벨
				padMin: defaultBarChartConfig['yMinValue']
			}
		},
		legend: { // 정보창 설정
			show: defaultBarChartConfig['showInfoLabel'],
			location: defaultBarChartConfig['infoLabelPosition'],
			placement: defaultBarChartConfig['infoLabelSide']
		}      
	});
}

/**
 * 차트 클릭시 이벤트 핸들러 셋팅
 * @param eventHandler : function
 * @returns
 */
function barChartClickHandler(eventHandler){
	if(eventHandler != null && typeof eventHandler == 'function'){
		$('#' + elementTargetId).bind('jqplotDataClick',eventHandler);
	}
}
		