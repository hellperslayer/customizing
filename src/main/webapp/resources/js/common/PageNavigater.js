var PageNavigator = (
	/**
	 * <pre>PageNavigator 생성
	 * - pageSize (not null) : 현 페이지 노출 컨텐츠 갯수
	 * - currentPageNum (not null) : 현재 페이지 번호
	 * - totalCount (not null) : 컨텐츠 총 갯수
	 * - pageBarSize : 페이지 네비게이터 페이지번호 노출 갯수
	 * 참조) 페이지 이동 요청시 'goPage(num)' 호출
	 * </pre>
	 */
	function(pageSize,currentPageNum,totalCount,pageBarSize){
		/**
		 * 한 화면 컨텐츠 노출 갯수
		 */
		var pageSize;
		
		/**
		 * 현재 페이지 번호
		 */
		var pageNum;
		
		/**
		 * 전체 컨텐츠 갯수
		 */
		var totalCount;
		
		/**
		 * 페이지 네비게이터 버튼 갯수
		 */
		var pageBarSize;
		
		/**
		 * 초기화 Flag
		 */
		var initFlag;
		
		/**
		 * 페이지 부가정보 노출여부
		 */
		var infoFlag = true;
		
		/**
		 * 스타일 정의
		 */
		var classes = {
			"page left":null
			,"allow_l":null
			,"allow_r":null
			,"num":null
			,"select":null
			,"info":null
		};
		
		/**
		 * 그룹버튼 태그정의
		 */
		var groupTags = {
			"first":null
			,"pre":null
			,"next":null
			,"end":null
		};
		
		/**
		 * 초기화
		 */
		var initialize = function(pageSize, currentPageNum, totalCount, pageBarSize){
			var initFlag = true;
			
			if(isNaN(pageSize)){
				initFlag = false;
			}else{
				pageSize = typeof pageSize == "string" ? Number(pageSize) : pageSize; 
			}
			if(isNaN(currentPageNum)){
				initFlag = false;
			}else{
				pageNum = typeof currentPageNum == "string" ? Number(currentPageNum) : currentPageNum;
			}
			if(isNaN(totalCount)){
				initFlag = false;
			}else{
				totalCount = typeof totalCount == "string" ? Number(totalCount) : totalCount;
			}
			
			if(pageBarSize == null){
				pageBarSize = 10;
			}else if(isNaN(pageBarSize)){
				initFlag = false;
			}else{
				pageBarSize = typeof pageBarSize == "string" ? Number(pageBarSize) : pageBarSize;
			}
			
			return initFlag; 
		};
		
		/**
		 * make HTML Tag
		 */
		var makePageNavigatorHtmlTag = function(){
			var resultHtml = new Array();
			var lineCount = 0;
			
			if(!initFlag){
				resultHtml[lineCount++] = "[PageNavigator] ERROR : Require confirmation by of you arguments</br>";
			}else{
				// 전체 페이지 수
				var pageCount = parseInt(totalCount / pageSize);
				if(totalCount % pageSize > 0){
					pageCount++;
				}
				
				// 전체 페이지버튼 그룹 갯수
				var pageGroupCount = pageCount / pageBarSize;
				
				// 현재 페이지버튼 그룹
				var currentPageGroup = parseInt((pageNum - 1) / pageBarSize);

				resultHtml[lineCount++] = "<p>";
				 
				// 이전 페이지버튼 그룹
				var prePageGroup = currentPageGroup - 1;
				
				// 맨 앞으로
				resultHtml[lineCount++] = "<span><a class='arrow_l'";
				resultHtml[lineCount++] = " onclick='";
				if(pageNum == 1){
					resultHtml[lineCount++] = "alert(\"현재 첫번째 페이지 입니다.\");";
				}else{
					resultHtml[lineCount++] = "goPage(1)";
				}
				resultHtml[lineCount++] = "' >";
				resultHtml[lineCount++] = "&lt;&lt;";
				resultHtml[lineCount++] = "</a></span>";
				
				// 이전 그룹으로
				resultHtml[lineCount++] = "<span><a class='arrow_l2'";
				resultHtml[lineCount++] = " onclick='";
				if(currentPageGroup <= 0 && pageNum == 1){
					resultHtml[lineCount++] = "alert(\"현재 첫번째 페이지 입니다.\");";
				}else if(currentPageGroup <= 0 && pageNum != 1){
					resultHtml[lineCount++] = "goPage(1)";
				}else{
					// 해당 그룹의 첫번째 페이지 번호
					var preGroupPageNum = (prePageGroup * pageBarSize) + 1;
					resultHtml[lineCount++] = "goPage(" +preGroupPageNum+ ");";
				}
				resultHtml[lineCount++] = "' >";
				resultHtml[lineCount++] = "&lt;"; 
				resultHtml[lineCount++] = "</a></span>";
				resultHtml[lineCount++] = "&nbsp;";
				
				// 페이지번호
				for(var i = 1; i <= pageBarSize; i++){
					var tempNum = i + (currentPageGroup * pageBarSize);
					if(pageCount < tempNum){
						break;
					}else if(tempNum == pageNum){
						resultHtml[lineCount++] = "<span><strong><a>";
						resultHtml[lineCount++] = tempNum;
						resultHtml[lineCount++] = "</a></strong></span>";
					}else{
						resultHtml[lineCount++] = "<span><a";
						resultHtml[lineCount++] = " onclick='goPage(";
						resultHtml[lineCount++] = tempNum;
						resultHtml[lineCount++] = ");' >";
						resultHtml[lineCount++] = tempNum;
						resultHtml[lineCount++] = "</a></span>";
					}
					resultHtml[lineCount++] = "&nbsp;";
				}
				
				// 다음 그룹으로
				var nextPageGroup = currentPageGroup + 1;
				resultHtml[lineCount++] = "<span><a class='arrow_r2'";
				resultHtml[lineCount++] = " onclick='";
				if(nextPageGroup >= pageGroupCount && pageNum == pageCount){
					resultHtml[lineCount++] = "alert(\"현재 마지막 페이지 입니다.\");";
				}else if(nextPageGroup >= pageGroupCount && pageNum < pageCount){
					resultHtml[lineCount++] = "goPage(";
					resultHtml[lineCount++] = pageCount;
					resultHtml[lineCount++] = ");";
				}else{
					// 해당 그룹의 마지막 페이지 번호
					var nextGroupPageNum = (nextPageGroup * pageBarSize) + 1;
					resultHtml[lineCount++] = "goPage(";
					resultHtml[lineCount++] = nextGroupPageNum;
					resultHtml[lineCount++] = ");";
				}
				resultHtml[lineCount++] = "'>";
				resultHtml[lineCount++] = "&gt;";
				resultHtml[lineCount++] = "</a></span>";
				
				// 마지막 그룹으로
				resultHtml[lineCount++] = "<span><a class='arrow_r'";
				resultHtml[lineCount++] = " onclick='";
				if(pageNum == pageCount){
					resultHtml[lineCount++] = "alert(\"현재 마지막 페이지 입니다.\");";
				}else{
					resultHtml[lineCount++] = "goPage("; 
					resultHtml[lineCount++] = pageCount;
					resultHtml[lineCount++] = ");"; 
				}
				resultHtml[lineCount++] = "' >";
				resultHtml[lineCount++] = "&gt;&gt;";
				resultHtml[lineCount++] = "</a></span>"; 
				
				if(infoFlag){
					resultHtml[lineCount++] = "&nbsp;";
					resultHtml[lineCount++] = "<span style='float: left; padding: 10px 10px;'";
					resultHtml[lineCount++] = ">";
					resultHtml[lineCount++] = "총 ";
					resultHtml[lineCount++] = totalCount.addComma();
					resultHtml[lineCount++] = " 건";
					resultHtml[lineCount++] = " (";
					resultHtml[lineCount++] = pageNum.toString().addComma();
					resultHtml[lineCount++] = " / ";
					resultHtml[lineCount++] = pageCount.toString().addComma();
					resultHtml[lineCount++] = " page)";
					resultHtml[lineCount++] = "</span>";
				}
				
				resultHtml[lineCount++] = "</p>";
				/*resultHtml[lineCount++] = "</div>";*/
			}
			return resultHtml.join("");
		};
		
		/**
		 * make class attribute
		 */
		var getClassTag = function(name){
			var classTag = "";
			if(classes[name] != null){
				classTag = " class='";
				classTag += classes[name];
				classTag += "'";
			}
			return classTag;
		};
		
		/**
		 * <pre>스타일 지정
		 * - layoutClass : (div) Container class
		 * - groupClass : (a)pre,next group button class
		 * - numClass : (a)page number buttom class
		 * - selectNumClass : (a)selected page num button class
		 * - infoClass : (span)page info text class
		 * </pre>
		 */
		PageNavigator.prototype.setClass = function(layoutClass,groupClass,numClass,selectNumClass,infoClass){
			classes['page left'] = layoutClass;
			classes['group'] = groupClass;
			classes['num'] = numClass;
			classes['select'] = selectNumClass;
			classes['info'] = infoClass;
		};
		
		
		/**
		 * <pre>그룹이동 Tag 지정
		 * > 첫페이지, 이전페이지, 다음페이지, 마지막페이지 이동 버튼을 CustomTag 로 지정한다.
		 * - firstGroupTag : 첫페이지
		 * - preGroupTag : 이전 그룹
		 * - nextGroupTag : 다음 그룹
		 * - endGroupTag : 마지막 페이지
		 * </pre>
		 */
		PageNavigator.prototype.setGroupTag = function(firstGroupTag,preGroupTag,nextGroupTag,endGroupTag){
			groupTags['first'] = firstGroupTag;
			groupTags['pre'] = preGroupTag;
			groupTags['next'] = nextGroupTag;
			groupTags['end'] = endGroupTag;
		};
		
		/**
		 * 페이지 부가정보 노출 여부
		 * @param boolean
		 */
		PageNavigator.prototype.showInfo = function(boolean){
			if(typeof boolean == "boolean"){
				infoFlag = boolean;
			}else{
				initFlag = false;
			}
		};
		/**
		 * 페이지 네비게이터 HTML 정보를 가져온다.
		 * @returns
		 */
		PageNavigator.prototype.getHtml = function(){
			return makePageNavigatorHtmlTag();
		};
		/**
		 * 페이지 네비게이터 HTML 정보를 그린다.
		 */
		PageNavigator.prototype.draw = function(){
			document.write(makePageNavigatorHtmlTag());
		};
		
		initFlag = initialize(pageSize,currentPageNum,totalCount,pageBarSize);
	}
);
