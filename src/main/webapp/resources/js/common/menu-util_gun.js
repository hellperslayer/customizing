
/**
 * 메인메뉴 마우스 over
 * @param menuNum 메인메뉴번호 (순서)
 */
function mouseOver(menuNum) {
	var obj = document.getElementById("topMenuImg" + menuNum);
	obj.src = rootPath + "/resources/image/layout/menu/navi_menu" + menuNum + "_on.gif";
}

//특수문자처리(태크실행가능하게)
function htmlspecialchars(str) {
	 if (typeof(str) == "string") {
	  str = str.replace(/&/g, "&amp;"); /* must do &amp; first */
	  str = str.replace(/"/g, "&quot;");
	  str = str.replace(/'/g, "&#039;");
	  str = str.replace(/</g, "&lt;");
	  str = str.replace(/>/g, "&gt;");
	  }
	 return str;	 
}
//특수문자처리(태크실행안되게)
function rhtmlspecialchars(str) {
	 if (typeof(str) == "string") {
	  str = str.replace(/&gt;/ig, ">");
	  str = str.replace(/&lt;/ig, "<");
	  str = str.replace(/&#039;/g, "'");
	  str = str.replace(/&quot;/ig, '"');
	  str = str.replace(/&amp;/ig, '&'); /* must do &amp; last */
	  }
	 return str;
}
/**
 * 메인메뉴 마우스 out
 * @param menuNum 메인메뉴번호 (순서)
 */
function mouseOut(menuNum) {
	var obj = document.getElementById("topMenuImg" + menuNum);
	obj.src = rootPath + "/resources/image/layout/menu/navi_menu" + menuNum + "_off.gif";
}

/**
 * 메인메뉴 Draw
 * @param target 대상엘리먼트
 * @param mainMenuStr 메인메뉴Json
 * @param mainMenuId 선택된 메인메뉴ID
 */
function drawMainMenu(target, mainMenuStr, mainMenuId, subMenuStr){
	if(mainMenuStr.length > 0){
		mainMenuArr = JSON.parse(mainMenuStr);
	}

	if(mainMenuArr != null && mainMenuArr.length > 0){
		$('#' + target).empty();
		
		var mainMenuHtmlArr = new Array();
		var lineCount = 0;
		
		var subMenuArr = JSON.parse(subMenuStr);
		
		mainMenuHtmlArr[lineCount++] = "<ul class='nav navbar-nav'>";
		
		$.each(mainMenuArr,function(count, mainMenu){
			
			var tempArr = new Array();
			var subArr = subMenuArr[mainMenu.menu_id];
			for(var i in subArr) {
				var sb = subArr[i];
				if(sb.menu_depth == 1) {
					tempArr.push(sb);
				}
			}
/*			if(mainMenuId == mainMenu.menu_id){
				mainMenuHtmlArr[lineCount++] = "<li class='selected_li'><a class='selected' ";
			}else{
				mainMenuHtmlArr[lineCount++] = "<li><a ";
			}*/
			mainMenuHtmlArr[lineCount++] = "<li class='classic-menu-dropdown' aria-haspopup='true'><a ";
			if(mainMenu.menu_name.length > 10)
				mainMenuHtmlArr[lineCount++] = "onclick='menuMove(\"" + mainMenu.menu_url + "\",\"" + mainMenu.menu_id+ "\")' data-hover='megamenu-dropdown' data-close-others='true' style='padding-top:5px; padding-bottom:5px;'>";
			else
				mainMenuHtmlArr[lineCount++] = "onclick='menuMove(\"" + mainMenu.menu_url + "\",\"" + mainMenu.menu_id+ "\")' data-hover='megamenu-dropdown' data-close-others='true'>";
			mainMenuHtmlArr[lineCount++] = mainMenu.menu_name;
			
			if(tempArr.length > 0) {
				mainMenuHtmlArr[lineCount++] = "<i class='fa fa-angle-down'></i></a>";	
				mainMenuHtmlArr[lineCount++] = "<ul class='dropdown-menu pull-left'>";
				
				for(var i in tempArr) {
					var menu = tempArr[i];
					mainMenuHtmlArr[lineCount++] = "<li><a onclick='menuMove(\"" + menu.menu_url + "\",\"" + mainMenu.menu_id+ "\",\"" + menu.menu_id+ "\")'>";
					mainMenuHtmlArr[lineCount++] = menu.menu_name;
					mainMenuHtmlArr[lineCount++] = "</a></li>";
				}
				
				mainMenuHtmlArr[lineCount++] = "</ul></li>";
			} else
				mainMenuHtmlArr[lineCount++] = "</a></li>";
		
			/*
			 * 메인 메뉴 사이 | (IMAGE)
			 * 
			if(count + 1 != mainMenuArr.length){
				mainMenuHtmlArr[lineCount++] = "<li><img src='";
				mainMenuHtmlArr[lineCount++] = rootPath + "/resources/image/layout/menu/navi_line.gif'/></li>";
			}
			*/
		});
		
		mainMenuHtmlArr[lineCount++] = "</ul>";
		
		$('#' + target).append(mainMenuHtmlArr.join(""));	
	}
	
	/*
	 * 대메뉴 IMAGE 사용 시
	 * 
	if(mainMenuArr != null && mainMenuArr.length > 0){
		$('#' + target).empty();
		var mainMenuHtmlArr = new Array();
		var lineCount = 0;
		mainMenuHtmlArr[lineCount++] = "<tr>";
		$.each(mainMenuArr,function(count, mainMenu){
			mainMenuHtmlArr[lineCount++] = "<td class='topMenu'>";
			if(mainMenuId == mainMenu.menu_id){
				mainMenuHtmlArr[lineCount++] = "<img src='" + rootPath + "/resources/image/layout/menu/navi_menu" + (count+1) + "_on.gif' id='topMenuImg'" + (count+1) + "'>";
			}else{
				mainMenuHtmlArr[lineCount++] = "<img src='" + rootPath + "/resources/image/layout/menu/navi_menu" + (count+1) + "_off.gif' id='topMenuImg" + (count+1) + "'";
				mainMenuHtmlArr[lineCount++] = " onclick='menuMove(\"" + mainMenu.menu_url + "\",\"" + mainMenu.menu_id+ "\")'";
				mainMenuHtmlArr[lineCount++] = " onmouseover='mouseOver(\"" + (count+1) + "\");'";
				mainMenuHtmlArr[lineCount++] = " onmouseout='mouseOut(\"" + (count+1) + "\");'>";
			}
			mainMenuHtmlArr[lineCount++] = "</td>";
		});
		
		$('#' + target).append(mainMenuHtmlArr.join(""));	
	}
	 */
}

/**
 * 서브메뉴 Draw
 * @param target 대상엘리먼트
 * 
 * @param subMenuJson 서브메뉴Json
 * @param mainMenuId 선택된 메인메뉴ID
 * @param subMenuId 선택된 서브메뉴ID
 */
function drawSubMenu(target, subMenuJson, mainMenuId, subMenuId, menuNum,menuCh,menuremain,system_seq,subSys,path){
	if(system_seq != null && system_seq != "" && system_seq != 0){
		subMenuId="MENU00041";
	}
	
	if(mainMenuId == "MENU00416"){
		subMenuId="MENU00432";
	}
	
	var menutitle= menuCh + menuNum;
	if(subMenuJson != null && subMenuJson.length > 0){
		var menuArr = JSON.parse(subMenuJson);
		var sysArr = JSON.parse(subSys);
		
		if(mainMenuId == null || mainMenuId.length == 0){
			return;
		}
		menuArr = menuArr[mainMenuId];
		
		// 탭단위 메뉴 Array
		var tabArr = new Array();
		var lowSubArr = new Array();
		var sysSubArr = new Array();
		// 소단위 메뉴 object
		var tab;
	
		for(var i in menuArr){
			var menu = menuArr[i];
			
			if(menu != null && menu.hasOwnProperty('menu_depth')){
				var depth = menu.menu_depth;
				// 메뉴 2단 구성일 시
				if(depth == 0){
					tab = new Object();
					tab.title = menu.menu_name;
					tab.id = menu.menu_id;
					tab.subArr = new Array();
					tabArr.push(tab);
				}else if(depth == 1){
					if(tabArr.length > 0){
						for(var k in tabArr){
							if(tabArr[k].id != null && tabArr[k].id == menu.parent_menu_id){
								tabArr[k].subArr.push(menu);
								break;
							}
						}
					}
				}else if(depth == 2) {
					if(tabArr.length > 0){
						for(var k in tabArr){
							lowSubArr.push(menu);
						}
					}
				}
				
				
		/*		 * 메뉴 3단 구성일 시
				 * 
				if(depth == 0){
				$("#" + target + " td:first").text(menu.menu_name);
				}else if(depth == 1){
					tab = new Object();
					tab.title = menu.menu_name;
					tab.id = menu.menu_id;
					tab.subArr = new Array();
					tabArr.push(tab);
				}else if(depth == 2){
					if(tabArr.length > 0){
						for(var k in tabArr){
							if(tabArr[k].id != null && tabArr[k].id == menu.parent_menu_id){
								tabArr[k].subArr.push(menu);
								break;
							}
						}
					}
				}*/
				
			}
		}
		
		for(var o in sysArr){
			var sys = sysArr[o];
			sysSubArr.push(sys);
		}
		
		for(var j in tabArr){
			//drawSubMenuList(target,tabArr[j].title,tabArr[j].subArr,mainMenuId, subMenuId);
			drawSubMenuList_new(target,tabArr[j].title,tabArr[j].subArr,mainMenuId, subMenuId);
		}
		
		/*
		 * 서브메뉴 3depth
		 */
		
/*		if (lowSubArr != null || lowSubArr > 0) {
				
			for(var n=0; n < lowSubArr.length; n++) {				
			
			if(subMenuId == lowSubArr[n].menu_id || menutitle == lowSubArr[n].menu_id || menuremain == lowSubArr[n].menu_id || system_seq == lowSubArr[n].system_seq) {
					$('#' + lowSubArr[n].menu_id).attr("style", "color:#063e8d; border-right:10px solid #063e8d; background: #f0f3f7; font-weight: bold;");
			}
				
			}
		} */
		
		// 구버전
		/*if (sysSubArr != null || sysSubArr > 0) {
			$('#MENU00041').after("<div class='snb2' ><ul style='border-top:1px solid #000; border-bottom:1px solid #000;' id='depth2'></ul></div>");
			$('#MENU00432').after("<div class='snb2' ><ul style='border-top:1px solid #000; border-bottom:1px solid #000;' id='depth3'></ul></div>");
		
			for(var e=1; e < sysSubArr.length; e++) {			
				$('#depth2')
					.append("<li><a id='" +sysSubArr[e].system_seq
							+ "' onclick=\"menuMoveAll('"+ "/allLogInq/list.html"+ "','" + mainMenuId + "','" + subMenuId + "','" + sysSubArr[e].system_seq +"');\" >"      
							+ sysSubArr[e].system_name + "</a></li>");
				$('#depth3')
					.append("<li><a id='" +sysSubArr[e].system_seq
							+ "' onclick=\"menuMoveAll('"+ "/reqLogInq/list.html"+ "','" + mainMenuId + "','" + subMenuId + "','" + sysSubArr[e].system_seq +"');\" >"      
							+ sysSubArr[e].system_name + "</a></li>");
				
				if(system_seq == sysSubArr[e].system_seq) {
					$('#' + sysSubArr[e].system_seq).attr("style", "color:#063e8d; border-right:10px solid #063e8d; background: #f0f3f7; font-weight: bold;");
				}
			}
			
		}*/
		
		if (sysSubArr != null || sysSubArr > 0) {
			if(path == "/dbAccessInq/list.html" || path == "/dbAccessInq/detail.html")
				$('#MENU00487').after("<ul class='nav nav-pills nav-stacked' id='depth2_3'></ul>");
			else
				$('#MENU00041').after("<ul class='nav nav-pills nav-stacked' id='depth2_1'></ul>");
			
			$('#MENU00490').after("<ul class='nav nav-pills nav-stacked' id='depth2_2'></ul>");
			
			// 온나라 시스템
			if(document.getElementById("MENU00520")) {
				$('#MENU00520').after("<ul class='nav nav-pills nav-stacked' id='depth2_4'></ul>");
				
				$('#depth2_4')
				.append("<li id='onnara1'><a style='color:#fff;cursor:default;'><b>&nbsp;-&nbsp;온나라 문서</b></a></li>")
				.append("<li id='onnara2'><a style='color:#fff;cursor:default;'><b>&nbsp;-&nbsp;온나라 지식</b></a></li>")
				.append("<li id='onnara3'><a style='color:#fff;cursor:default;'><b>&nbsp;-&nbsp;온나라 이음</b></a></li>");
				
				for(var e=0; e < sysSubArr.length; e++) {
					var sys_cd = sysSubArr[e].system_seq;
					var type = sys_cd.substring(1,2);
					if(type == "0") {
						$('#onnara1').append("<li><a id='" +sysSubArr[e].system_seq
							+ "' style='color:#fff;' onmouseover=\"this.style.color='#A9F5F2'\" onmouseout=\"this.style.color='#fff'\" onclick=\"menuMoveAll('"+ "/onrLogInq/list.html"+ "','" + mainMenuId + "','" + subMenuId + "','" + sysSubArr[e].system_seq +"');\" >&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;"      
							+ sysSubArr[e].system_name + "</a></li>");
					}else if(type == "1") {
						$('#onnara2').append("<li><a id='" +sysSubArr[e].system_seq
								+ "' style='color:#fff;' onmouseover=\"this.style.color='#A9F5F2'\" onmouseout=\"this.style.color='#fff'\" onclick=\"menuMoveAll('"+ "/onrLogInq/list.html"+ "','" + mainMenuId + "','" + subMenuId + "','" + sysSubArr[e].system_seq +"');\" >&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;"      
								+ sysSubArr[e].system_name + "</a></li>");
					}else {
						$('#onnara3').append("<li><a id='" +sysSubArr[e].system_seq
								+ "' style='color:#fff;' onmouseover=\"this.style.color='#A9F5F2'\" onmouseout=\"this.style.color='#fff'\" onclick=\"menuMoveAll('"+ "/onrLogInq/list.html"+ "','" + mainMenuId + "','" + subMenuId + "','" + sysSubArr[e].system_seq +"');\" >&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;"      
								+ sysSubArr[e].system_name + "</a></li>");
					}
					
					if(system_seq == sysSubArr[e].system_seq) {
						$('#' + sysSubArr[e].system_seq).append(" <i class='fa fa-check' style='color:red;'></i>");
					}
				}
			}else {
				for(var e=0; e < sysSubArr.length; e++) {
					$('#depth2_1')
						.append("<li><a id='" +sysSubArr[e].system_seq
								+ "' style='color:#fff;' onmouseover=\"this.style.color='#A9F5F2'\" onmouseout=\"this.style.color='#fff'\" onclick=\"menuMoveAll('"+ "/allLogInq/list.html"+ "','" + mainMenuId + "','" + subMenuId + "','" + sysSubArr[e].system_seq +"');\" >&nbsp;-&nbsp;"      
								+ sysSubArr[e].system_name + "</a></li>");
					$('#depth2_2')
						.append("<li><a id='" +sysSubArr[e].system_seq
								+ "' style='color:#fff;' onmouseover=\"this.style.color='#A9F5F2'\" onmouseout=\"this.style.color='#fff'\" onclick=\"menuMoveAll('"+ "/reqLogInq/list.html"+ "','" + mainMenuId + "','" + subMenuId + "','" + sysSubArr[e].system_seq +"');\" >&nbsp;-&nbsp;"      
								+ sysSubArr[e].system_name + "</a></li>");
					
					$('#depth2_3')
					.append("<li><a id='" +sysSubArr[e].system_seq
							+ "' style='color:#fff;' onmouseover=\"this.style.color='#A9F5F2'\" onmouseout=\"this.style.color='#fff'\" onclick=\"menuMoveAll('"+ "/dbAccessInq/list.html"+ "','" + mainMenuId + "','" + subMenuId + "','" + sysSubArr[e].system_seq +"');\" >&nbsp;-&nbsp;"      
							+ sysSubArr[e].system_name + "</a></li>");
					
					if(system_seq == sysSubArr[e].system_seq) {
						$('#' + sysSubArr[e].system_seq).append(" <i class='fa fa-check' style='color:red;'></i>");
					}
				}
			}
		}
		/*if (sysSubArr != null || sysSubArr > 0) {
			$('#MENU00041').append("<ul class='sub-menu' id='depth2' style='display: block;'></ul>");
			$('#MENU00490').append("<ul class='sub-menu' id='depth3' style='display: block;'></ul>");
			
			for(var e=1; e < sysSubArr.length; e++) {
				var clsName = "nav-item";
				if(system_seq == sysSubArr[e].system_seq) {
					clsName = "nav-item active";
				}
				$('#depth2').append("<li id='" + sysSubArr[e].system_seq + "' class='" + clsName + "'><a onclick=\"menuMoveAll('"+ "/allLogInq/list.html"+ "','" + mainMenuId + "','" + subMenuId + "','" + sysSubArr[e].system_seq +"');\"> " + sysSubArr[e].system_name + " </a></li>");
				$('#depth3').append("<li id='" + sysSubArr[e].system_seq + "' class='" + clsName + "'><a onclick=\"menuMoveAll('"+ "/reqLogInq/list.html"+ "','" + mainMenuId + "','" + subMenuId + "','" + sysSubArr[e].system_seq +"');\"> " + sysSubArr[e].system_name + " </a></li>");
			}
		}*/
	}
}




/**
 * 서브메뉴 탭단위 Draw
 * @param target 대상엘리먼트
 * @param name 탭 명칭
 * @param subMenuArr 서브메뉴 Array (탭단위)
 * @param mainMenuId 선택된 메인메뉴ID
 * @param subMenuId 선택된 서브메뉴ID
 */
function drawSubMenuList(target, name, subMenuArr, mainMenuId, subMenuId){
	target = target + "Node";
	
	var subMenuHtmlArr = new Array();
	var lineCount = 0;
	var width= $(window).width();
	
	if(width<1348 ){
		if(mainMenuId !='MENU00416' && mainMenuId !='MENU00040'){
			subMenuHtmlArr[lineCount++] = "<h2 class='left_dep1'>";
			subMenuHtmlArr[lineCount++] = name;
			subMenuHtmlArr[lineCount++] = "</h2>";
		}
	}else{
		subMenuHtmlArr[lineCount++] = "<h2 class='left_dep1'>";
		subMenuHtmlArr[lineCount++] = name;
		subMenuHtmlArr[lineCount++] = "</h2>";
	}
	if(mainMenuId =='MENU00416' || mainMenuId =='MENU00040'){
	 if(width<1348 ){
		subMenuHtmlArr[lineCount++] = "<ul class='depth-2' style='margin-top:-30px;'>";
	 }else{
		 subMenuHtmlArr[lineCount++] = "<ul class='depth-2'>"; 
	 }
	 }else {
		subMenuHtmlArr[lineCount++] = "<ul class='depth-2'>";
	}
	if(subMenuArr != null && subMenuArr.length > 0){
		var i = 0;
		
//		subMenuHtmlArr[lineCount++] = "<ul>";
		
		// 메인 메뉴 클릭 진입 시 서브 메뉴 가장 상단 선택되어있도록
		if(subMenuId == null || subMenuId == '') {
			subMenuHtmlArr[lineCount++] = "<li class='left_menu_select_li'  id='";
			subMenuHtmlArr[lineCount++] = subMenuArr[i].menu_id;
			subMenuHtmlArr[lineCount++] = "'>";
			subMenuHtmlArr[lineCount++] = "<a class='left_menu_select_a' onclick=\"menuMove('";
			subMenuHtmlArr[lineCount++] = subMenuArr[i].menu_url;
			subMenuHtmlArr[lineCount++] = "','";
			subMenuHtmlArr[lineCount++] = mainMenuId;
			subMenuHtmlArr[lineCount++] = "','";
			subMenuHtmlArr[lineCount++] = subMenuArr[i].menu_id;
			subMenuHtmlArr[lineCount++] = "');\" >";
			subMenuHtmlArr[lineCount++] = subMenuArr[i].menu_name;
			subMenuHtmlArr[lineCount++] = "</a></li>";

			// 그 이후 서브 메뉴들은 선택되지 않은 모양으로
			for(var i = 1; i < subMenuArr.length; i++){
				subMenuHtmlArr[lineCount++] = "<li id='";
				subMenuHtmlArr[lineCount++] = subMenuArr[i].menu_id;
				subMenuHtmlArr[lineCount++] = "'>";
				subMenuHtmlArr[lineCount++] = "<a onclick=\"menuMove('";
				subMenuHtmlArr[lineCount++] = subMenuArr[i].menu_url;
				subMenuHtmlArr[lineCount++] = "','";
				subMenuHtmlArr[lineCount++] = mainMenuId;
				subMenuHtmlArr[lineCount++] = "','";
				subMenuHtmlArr[lineCount++] = subMenuArr[i].menu_id;
				subMenuHtmlArr[lineCount++] = "');\" >";
				subMenuHtmlArr[lineCount++] = subMenuArr[i].menu_name;
				subMenuHtmlArr[lineCount++] = "</a></li>";
			}
		}
		
		// 서브 메뉴 클릭 진입 시
		else {
			for(var i = 0; i < subMenuArr.length; i++){
				if(subMenuId == subMenuArr[i].menu_id){
					subMenuHtmlArr[lineCount++] = "<li class='left_menu_select_li' id='";
					subMenuHtmlArr[lineCount++] = subMenuArr[i].menu_id;
					subMenuHtmlArr[lineCount++] = "'>";
					subMenuHtmlArr[lineCount++] = "<a class='left_menu_select_a' onclick=\"menuMove('";
					subMenuHtmlArr[lineCount++] = subMenuArr[i].menu_url;
					subMenuHtmlArr[lineCount++] = "','";
					subMenuHtmlArr[lineCount++] = mainMenuId;
					subMenuHtmlArr[lineCount++] = "','";
					subMenuHtmlArr[lineCount++] = subMenuArr[i].menu_id;
					subMenuHtmlArr[lineCount++] = "');\" >";
					subMenuHtmlArr[lineCount++] = subMenuArr[i].menu_name;
					subMenuHtmlArr[lineCount++] = "</a></li>";
				}else{
					subMenuHtmlArr[lineCount++] = "<li id='";
					subMenuHtmlArr[lineCount++] = subMenuArr[i].menu_id;
					subMenuHtmlArr[lineCount++] = "'>";
					subMenuHtmlArr[lineCount++] = "<a onclick=\"menuMove('";
					subMenuHtmlArr[lineCount++] = subMenuArr[i].menu_url;
					subMenuHtmlArr[lineCount++] = "','";
					subMenuHtmlArr[lineCount++] = mainMenuId;
					subMenuHtmlArr[lineCount++] = "','";
					subMenuHtmlArr[lineCount++] = subMenuArr[i].menu_id;
					subMenuHtmlArr[lineCount++] = "');\" >";
					subMenuHtmlArr[lineCount++] = subMenuArr[i].menu_name;
					subMenuHtmlArr[lineCount++] = "</a></li>";
				}
			}
		}
		
		subMenuHtmlArr[lineCount++] = "</ul>";
		
		$('#' + target).append(subMenuHtmlArr.join(""));
	}
}

function drawSubMenuList_new(target, name, subMenuArr, mainMenuId, subMenuId){
	//alert("drawSubMenuList_new()");
	
	target = target + "Node";
	
	var subMenuHtmlArr = new Array();
	var lineCount = 0;
	
	// 홈버튼 슬라이드
	/*subMenuHtmlArr[lineCount++] = "<a class='nav-link nav-toggle'> <i class='icon-home'></i> <span class='title'>";
	subMenuHtmlArr[lineCount++] = name;
	subMenuHtmlArr[lineCount++] = "</span><span class='arrow'></span></a>";*/
	
	
	if(subMenuArr.length > 0)
		subMenuHtmlArr[lineCount++] = "<ul class='sub-menu' style='display:block;'>";
	
	subMenuHtmlArr[lineCount++] = "<li class='nav-item start' style='background-color:#2C3542;'><a class='nav-link' onclick='#'><i class='icon-home' style='color: white;'></i>&nbsp;&nbsp;<span style='color: white;font-size: 12pt;text-align: center;'>"+name+"</span></a></li>";
	
	for(var i = 0; i < subMenuArr.length; i++){
		var menu = subMenuArr[i];
		
		subMenuHtmlArr[lineCount++] = "<li class='nav-item start' id='" + menu.menu_id +"'><a onclick='menuMove(\"" + menu.menu_url + "\",\"" + mainMenuId+ "\")' class='nav-link '> <i class='icon-bar-chart'></i> <span class='title' style='color:#b4bcc8'>&nbsp;&nbsp;";
		subMenuHtmlArr[lineCount++] = menu.menu_name;
		subMenuHtmlArr[lineCount++] = "</span></a></li>";
	}
	
	$('#' + target).append(subMenuHtmlArr.join(""));
}

function drawSubMenuList_new2(target, name, subMenuArr, mainMenuId, subMenuId){
	//alert("drawSubMenuList_new()");
	
	target = target + "Node";
	
	var subMenuHtmlArr = new Array();
	var lineCount = 0;
	
	// 홈버튼 슬라이드
	/*subMenuHtmlArr[lineCount++] = "<a class='nav-link nav-toggle'> <i class='icon-home'></i> <span class='title'>";
	subMenuHtmlArr[lineCount++] = name;
	subMenuHtmlArr[lineCount++] = "</span><span class='arrow'></span></a>";*/
	
	if(subMenuArr.length > 0)
		subMenuHtmlArr[lineCount++] = "<li class='nav-item start active open'>";
	
	subMenuHtmlArr[lineCount++] = '<a href="javascript:;" class="nav-link nav-toggle"><i class="icon-shield"></i><span class="title">' + name + '</span><span class="arrow"></span></a>';
	subMenuHtmlArr[lineCount++] = '<ul class="sub-menu">';
	for(var i = 0; i < subMenuArr.length; i++){
		var menu = subMenuArr[i];
		
		var clsName = 'nav-item start ';
		if(subMenuId == menu.menu_id)
			clsName = 'nav-item start active';
		
		subMenuHtmlArr[lineCount++] = "<li class='" + clsName + "' id='" + menu.menu_id +"'><a onclick='menuMove(\"" + menu.menu_url + "\",\"" + mainMenuId+ "\",\"" + subMenuArr[i].menu_id + "\")' class='nav-link '>  <span class='title'>";
		subMenuHtmlArr[lineCount++] = menu.menu_name;
		subMenuHtmlArr[lineCount++] = "</span></a></li>";
	}
	subMenuHtmlArr[lineCount++] = '</ul></li>';
	
	$('#' + target).append(subMenuHtmlArr.join(""));
}

function moveallList() {
	$('input[name=page_num]').attr("value","1");
	$("#listForm").attr("action",reqLogInqConfig["alistUrl"]);
	$("#listForm").find("input[name=main_menu_id]").val('MENU0040');
	
	$("#listForm").submit();
}

function movereqList() {
	$('input[name=page_num]').attr("value","1");
	$("#listForm").attr("action",allLogInqConfig["reqUrl"]);
	$("#listForm").find("input[name=main_menu_id]").val('MENU000416');
	$("#listForm").submit();
}

//날짜변경시 기간선택 초기화
function resetPeriod() {
	$("#daySelect").val('');
}

