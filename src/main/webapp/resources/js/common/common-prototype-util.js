/**
 * 날짜(date)를 문자(string)형태로 변환(yyyy-MM-dd)
 * @returns {String}
 */
Date.prototype.convertDateToString = function(){
	var year = this.getFullYear().toString();
	var month = (this.getMonth() + 1).toString();
	var date = this.getDate().toString();
	
	return year.toDoubleDigits() + "-" + month.toDoubleDigits() +  "-" + date.toDoubleDigits(); 
};

/**
 * 날짜(date)를 문자(string)형태로 변환(yyyy-MM-dd hh:mm:ss)
 * @returns {String}
 */
Date.prototype.convertDatetimeToString = function(){
	var dateStr = this.convertDateToString();
	var hour = this.getHours().toString();
	var minute = this.getMinutes().toString();
	var seconds = this.getSeconds().toString();
	
	return dateStr + " " + hour.toDoubleDigits() + ":" + minute.toDoubleDigits() + ":" + seconds.toDoubleDigits();
};

/**
 * 숫자형 문자 데이터를 2자릿수로 맞춘다.
 * @returns {String}
 */
String.prototype.toDoubleDigits = function(){
	if(this.length < 2){
		return "0" + this;
	}else{
		return this;
	}
};

/**
 * 숫자형 문자 데이터에 ',' 를 추가한다.</br>
 * - 숫자형이 아닐경우 'NaN'
 * @returns
 */
String.prototype.addComma = function(){
	if(isNaN(this)){
		return "NaN"; 
	}else{
		var lineCount = 0;
		var arr = [];
		var str = this;
		for(var i = 0; i < this.length / 3; i++){
			arr[lineCount++] = str.substring((str.length - 3), str.length);
			str = str.substring(0, (str.length - 3));
		}
		arr.reverse();
		return arr.join(",");
	}
};

//관리자행위이력
$.fn.extend({
	appendInput : function(type, name, value) {
		return this.append($('<input/>', {'type': type, 'name': name, 'value': value}));
	},
	appendHiddenInput : function(name, value) {
		return this.appendInput('hidden', name, value);
	},
	appendLogMessageParamsInput : function(title, action, paramString, menu_id, from_ip, to_ip, dept_name, use_flag,file) {
		this.appendHiddenInput('log_message_title', title)
		.appendHiddenInput('log_message_params', paramString)
		.appendHiddenInput('log_action', action)
		.appendHiddenInput('menu_id', menu_id)
		.appendHiddenInput('from_ip',from_ip)
		.appendHiddenInput('to_ip',to_ip)
		//.appendHiddenInput('dept_name', dept_name)
		.appendHiddenInput('use_flag', use_flag)
		.appendHiddenInput('file', file);

		
		
	},
	appendLogMessageParamsInput_allLogInq : function(title, action, paramString, menu_id) {
		this.appendHiddenInput('log_message_title', title)
		.appendHiddenInput('log_message_params', paramString)
		.appendHiddenInput('log_action', action)
		.appendHiddenInput('menu_id', menu_id)		
	}
});

//날짜 형식 변경
function strToDateTime(str){
	if(str.length < 14){
		return str;
	}
	return str.substring(0, 4) + "-" + str.substring(4, 6) + "-" + str.substring(6, 8) + " " + str.substring(8, 10) 
	 + ":" + str.substring(10, 12) + ":" +  str.substring(12, 14);
}

//NULL 체크 '-' 반환
function check_null(data) {
	if(data == null || data == 'null'){
		return "-";
	}else{
		return data;
	}
}

//숫자만 입력
function check_number() {
	// 37 : ← (화살표)
	// 57 : 키보드상단 숫자 9
	// 96 : 키보드오른쪽 숫자 0
	// 105 : 키보드오른쪽 숫자 9
	// 8 : ← (백스페이스)
	// 9 : TAB
	// 189 : -
    if(!((event.keyCode >= 37 && event.keyCode <= 57) 
    		|| (event.keyCode >= 96 && event.keyCode <= 105)
    		|| (event.keyCode == 8  || event.keyCode == 9 || event.keyCode == 189))) {
         alert("숫자만 입력해주세요.");
         event.returnValue = false;
    } 
}

// 익스플로러인지 크롬인지
function is_ie() {
	if (navigator.userAgent.toLowerCase().indexOf("chrome") != -1)
		return false;
	if (navigator.userAgent.toLowerCase().indexOf("msie") != -1)
		return true;
	if (navigator.userAgent.toLowerCase().indexOf("windows nt") != -1)
		return true;
	return false;
}
 
// 클립보드 복사
function ClipUrl(text) {
	event.cancelBubble = true;
	
	if(text == "") {
		alert("존재하지 않는 url입니다.");
		return;
	}

	if (is_ie()) {
		window.clipboardData.setData("Text", text);
		alert("주소가 복사되었습니다. \'Ctrl+V\'를 눌러 붙여넣기 해주세요.");
	} else {
		window.prompt("Ctrl+C를 눌러 클립보드로 복사하세요", text);
	}
}

//검색조건 초기화
function resetOptions(url) {
	$('#menuForm').attr('action', url);
	$('#menuForm').submit();
}

function validate_empty() {
	$('.not_empty').each(function() {
		if ($(this).val() == null || $(this).val() == '') {
			alert('필수정보를 입력하십시오.');
			$(this).focus();
			return false;
		}
	});
	return true;
}
function ComSubmit(opt_formId) {
	alert("!!")
    this.formId = gfn_isNull(opt_formId) == true ? "listForm" : opt_formId;
    this.url = "";
     
    if(this.formId == "listForm"){
        $("#listForm")[0].reset();
    }
     
    this.setUrl = function setUrl(url){
        this.url = url;
    };
     
    this.addParam = function addParam(key, value){
        $("#"+this.formId).append($("<input type='hidden' name='"+key+"' id='"+key+"' value='"+value+"' >"));
    };
     
    this.submit = function submit(){
        var frm = $("#"+this.formId)[0];
        frm.action = this.url;
        frm.method = "post";
        frm.submit();  
    };
}


$(document).ready(function() {
	/*var currentMenuId = $('input[name=current_menu_id]').val();*/
	
	// 보고서 항목의 경우 검색조건 편 상태 유지
/*	if(currentMenuId != 'MENU00075' && currentMenuId != 'MENU00076' && currentMenuId != 'MENU00077') {
		$('.optgroup').slideUp(1000);
	}*/
	
	$('.option_tab').click(function() {
		if($(this).attr('title') == 'open') {
			$('.optgroup').slideDown(500);
			$(this).attr('style', 'display: none');
			$(this).siblings().attr('style', 'display: block');
		}
		else if($(this).attr('title') == 'close') {
			$('.optgroup').slideUp(500);
			$(this).attr('style', 'display: none');
			$(this).siblings().attr('style', 'display: block');
		}		
	});
	
	// 달력 상단에 안가리게
	$("#search_fr").click(function() {
	    // datepicker 오픈 전의 CSS를 지정
	    var position   = $(".datepicker").css("position");
	    var top          = $(".datepicker").css("top");
	    var left          = $(".datepicker").css("left");
	    var display     = $(".datepicker").css("display");

	    // datepicker 오픈 시 자동으로 변경되는 CSS 제거
	    $(".datepicker").css("z-index", "");

	    // 자동 지정되었던 CSS를 다시 지정
	    $(".datepicker").attr("style", "z-index:9999 !important");

	    // 저장했던 CSS 다시 지정
	    $(".datepicker").css("position", position);
	    $(".datepicker").css("top", top);
	    $(".datepicker").css("left", left);
	    $(".datepicker").css("display", display);
	});
	
	$("#search_to").click(function() {
	    // datepicker 오픈 전의 CSS를 지정
	    var position   = $(".datepicker").css("position");
	    var top          = $(".datepicker").css("top");
	    var left          = $(".datepicker").css("left");
	    var display     = $(".datepicker").css("display");

	    // datepicker 오픈 시 자동으로 변경되는 CSS 제거
	    $(".datepicker").css("z-index", "");

	    // 자동 지정되었던 CSS를 다시 지정
	    $(".datepicker").attr("style", "z-index:9999 !important");

	    // 저장했던 CSS 다시 지정
	    $(".datepicker").css("position", position);
	    $(".datepicker").css("top", top);
	    $(".datepicker").css("left", left);
	    $(".datepicker").css("display", display);
	});
});