$(document).ready(function() {
	// DatePicker
	var dates = '';
	dates = $('.search_fr, .search_to, .search_dt').datepicker(
		{	
			maxDate : 0,
			showOn: 'both',
			//buttonImage: rootPath + "/resources/image/common/icon_calender.gif",
			buttonImageOnly: true,
			showAnimation: 'slide',
			showOtherMonths: true,
			selectOtherMonths: true,
			changeYear: true,
			changeMonth: true,
			autoclose: true,
			setDate: new Date(),
			onSelect: function(selectedDate) {
				var option = $(this).hasClass("search_fr") ? "minDate" : "maxDate",
					instance = $(this).data("datepicker"),
					date = $.datepicker.parseDate(
							instance.settings.dateFormat || $.datepicker._defaults.dateFormat,
							selectedDate, instance.settings
					);
				dates.filter('.search_fr, .search_to').not(this).datepicker("option", option, date);
				$(".ui-datepicker-trigger").remove();
				
				//날짜 클릭되었을때 기간선택 값 초기화
				$(".daySelect_first").attr("selected", "'selected'");
			}
		}
	);
	$(".search_fr, .search_to, .search_dt").attr("readonly", "readonly");
	$("img.ui-datepicker-trigger").attr("style", "display: none;");
	
	
	
});