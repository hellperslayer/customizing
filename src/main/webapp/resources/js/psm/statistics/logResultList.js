// 페이지 이동
function goPage(num){

	$("#listForm").attr("action",allLogInqConfig["listUrl"]);
	$("#listForm input[name=page_num]").val(num);
	$("#listForm").submit();
}

// 검색
function resultSearch() {
	$('#loading').show();
	var listForm = $("#listForm");
	$('input[name=page_num]').attr("value","1");
	
	var shType = $('input[name=shType]').val();
	if(shType=='system'){
		var system_seq = $('select[name=system_seq]').val();
		console.log(system_seq);
		$('#listForm [name=system_seq]').prop('disabled', false);
	}
	
	$("#listForm").attr("action",allLogInqConfig["listUrl"]);
	$("#listForm").submit();
}

function resultReset() {
	var shType = $("#listForm input[name=shType]").val();
	
	if(shType != 'dept'){
		$("#listForm input[name=dept_name]").val('');
	} 
	if(shType != 'system') {
		$("#listForm select[name=system_seq]").val('');
	} 
	if(shType != 'user') {
		$("#listForm input[name=emp_user_id]").val('');
		$("#listForm input[name=emp_user_name]").val('');
	}
	$("#listForm input[name=user_ip]").val('');
	$("#listForm select[name=req_type]").val('');
	$("#listForm input[name=req_url]").val('');
	$("select[id=start_h]").attr("disabled", true);
	$("select[id=end_h]").attr("disabled", true);
	$("#listForm input[name=file_name]").val('');
	$('#loading').show();
	var listForm = $("#listForm");
	$('input[name=page_num]').attr("value","1");
	$("#listForm").attr("action",allLogInqConfig["listUrl"]);
	$("#listForm").submit();
}


// 기간선택 검색조건
function initDaySelect(){
	if($("#daySelect").val() == "Day"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
		$('#search_fr').attr('disabled', true);
		$('#search_to').attr('disabled', true);
	}else if($("#daySelect").val() == "WeekDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-7d");
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
		$('#search_fr').attr('disabled', true);
		$('#search_to').attr('disabled', true);
	}else if($("#daySelect").val() == "MonthDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-1m");
		var setDateStart = $("#search_fr").val();
		setDateStart = setDateStart.substr(0,8);
		setDateStart = setDateStart + "01";
		$("#search_fr").val(setDateStart);
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "YearDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
		var setDateStart = $("#search_fr").val();
		setDateStart = setDateStart.substr(0,5);
		setDateStart = setDateStart + "01-01";
		$("#search_fr").val(setDateStart);
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "TotalMonthDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-1m");
		var setDateStart = $("#search_fr").val();
		setDateStart = setDateStart.substr(0,8);
		setDateStart = setDateStart + "01";
		$("#search_fr").val(setDateStart);
		var setDateend =  $("#search_to").val();
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","0");
		setDateend = setDateend.substr(8,10);
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-"+setDateend+"d");
	}else if($("#daySelect").val() == "") {	// 직접입력
		$('#search_fr').attr('disabled', false);
		$('#search_to').attr('disabled', false);
	}else{
		dateDisabled(false);
	}
}

function dateDisabled(bool){
	$('.search_fr').attr('disabled', bool);
	$('.search_to').attr('disabled', bool);
	$('.ui-datepicker-trigger').css('visibility', bool == true ? 'hidden' : 'visible');
	$('.ui-datepicker-trigger').css('visibility', bool == true ? 'hidden' : 'visible');
}


//function resultExcel() {
//	
//}

function resultExcel(){
		var $form = $("#listForm");
		var log_message_params = '';
		var menu_id = $('input[name=current_menu_id]').val();
		var log_message_title = '통계보고 상세 엑셀 다운로드';
		var log_action = 'EXCEL DOWNLOAD';
		
		$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
		
		$("#listForm").attr("action",allLogInqConfig["downloadUrl"]);
		$("#listForm").submit();
}


function resultCsv() {
	var $form = $("#listForm");
	var log_message_params = '';
	var menu_id = $('input[name=current_menu_id]').val();
	var log_message_title = '통계보고 상세 CSV 다운로드';
	var log_action = 'CSV DOWNLOAD';
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	$("#listForm").attr("action",allLogInqConfig["downloadCSVUrl"]);
	$("#listForm").submit();
}

function downResultExcel(){
	var $form = $("#listForm");
	var log_message_params = '';
	var menu_id = $('input[name=current_menu_id]').val();
	var log_message_title = '접속기록조회 엑셀 다운로드';
	var log_action = 'EXCEL DOWNLOAD';
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	$("#listForm").attr("action",allLogInqConfig["downloadUrl"]);
	$("#listForm").submit();
}


function downResultCsv() {
var $form = $("#listForm");
var log_message_params = '';
var menu_id = $('input[name=current_menu_id]').val();
var log_message_title = '접속기록조회 CSV 다운로드';
var log_action = 'CSV DOWNLOAD';

$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);

$("#listForm").attr("action",allLogInqConfig["downloadCSVUrl"]);
$("#listForm").submit();
}

/*
$(function(){
	$('input[type=text]').attr("onkeydown","javascript:if(event.keyCode==13){moveallLogInqList();}");
	$('#search_fr').attr("onclick", "resetPeriod()");
	$('#search_to').attr("onclick", "resetPeriod()");
	
	var daySel = $('#daySelect').val();
	if (daySel == "Day" || daySel == "WeekDay") {
		$('#search_fr').attr('disabled', true);
		$('#search_to').attr('disabled', true);
	}
});
*/

function searhTime(obj) {
	var check = obj.checked;
	if(check == true) {
		$("select[id=start_h]").attr("disabled", false);
		$("select[id=end_h]").attr("disabled", false);
	} else {
		$("select[id=start_h]").attr("disabled", true);
		$("select[id=end_h]").attr("disabled", true);
	}
}


function setProcTime(type) {
	var start = Number($("#start_h").val());
	var end = Number($("#end_h").val());
	
	if (start >= end) {
		if (type == 'start') {
			$("#end_h").val(start + 1);
		} else {
			$("#start_h").val(end - 1);
		}
	}
}

$(function(){
	$('input[type=text]').attr("onkeydown","javascript:if(event.keyCode==13){resultSearch();}");
});