/**
 * statistics Javascript
 * @author yrchoo
 * @since 2015. 04. 17
 */

// 조건별 검색 - 기간 선택, 일별, 월별, 연도별 (chk_date_type) 
var moveStatistics = function() {
	var listForm = $("#listForm");
	var tDate = listForm.find("input[name=search_to]").val();
	var fDate = listForm.find("input[name=search_from]").val();
	if(tDate<fDate){
		alert("검색 기간을 확인하세요");
		return;
	}
	$("#listForm").find("input[name=sort_flag]").val(0);
	if (arguments.length < 1) {
		searchList();
	} else {
		$('input[name=chk_date_type]').val(arguments[0]);	
		searchList();
	}
}

function searchList() {
	$("input[name=isSearch]").val('Y');
	$("#listForm").attr("action",statisticsListConfig["listUrl"]);

	$("#listForm").submit();
}

var moveExcel = function() {
	$("#listForm").find("input[name=sort_flag]").val(0);
	var chk_date = $('input[name=check_dp]').val();
	if (chk_date.length < 1) {
		excelPredList();
	} else {
		$('input[name=chk_date_type]').val(chk_date);	
		excelPredList();
	}
}

var sysmoveExcel = function() {
	$("#listForm").find("input[name=sort_flag]").val(0);
	var chk_date = $('input[name=check_dp]').val();
	if (chk_date.length < 1) {
		excelSysList();
	} else {
		$('input[name=chk_date_type]').val(chk_date);	
		excelSysList();
	}
}

var deptmoveExcel = function() {
	$("#listForm").find("input[name=sort_flag]").val(0);
	var chk_date = $('input[name=check_dp]').val();
	if (chk_date.length < 1) {
		excelDeptList();
	} else {
		$('input[name=chk_date_type]').val(chk_date);	
		excelDeptList();
	}
}

var indvmoveExcel = function() {
	$("#listForm").find("input[name=sort_flag]").val(0);
	var chk_date = $('input[name=check_dp]').val();
	if (chk_date.length < 1) {
		excelIndvList();
	} else {
		$('input[name=chk_date_type]').val(chk_date);	
		excelIndvList();
	}
}


function excelPredList() {
	
	var $form = $("#listForm");
	var log_message_params = '';
	var menu_id = $('input[name=current_menu_id]').val();
	var log_message_title = '기간별유형통계 엑셀 다운로드';
	var log_action = 'EXCEL DOWNLOAD';
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	$("#listForm").attr("action",statisticsListConfig["downloadUrl"]);
	$("#listForm").submit();
}

function excelSysList() {
	
	var $form = $("#listForm");
	var log_message_params = '';
	var menu_id = $('input[name=current_menu_id]').val();
	var log_message_title = '시스템별유형통계 엑셀 다운로드';
	var log_action = 'EXCEL DOWNLOAD';
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	$("#listForm").attr("action",statisticsListConfig["downloadUrl"]);
	$("#listForm").submit();
}

function excelDeptList(){
	
	var $form = $("#listForm");
	var log_message_params = '';
	var menu_id = $('input[name=current_menu_id]').val();
	var log_message_title = '소속별유형통계 엑셀 다운로드';
	var log_action = 'EXCEL DOWNLOAD';
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	$("#listForm").attr("action",statisticsListConfig["downloadUrl"]);
	$("#listForm").submit();
}

function excelIndvList(){
	
	var $form = $("#listForm");
	var log_message_params = '';
	var menu_id = $('input[name=current_menu_id]').val();
	var log_message_title = '개인별위험도통계 엑셀 다운로드';
	var log_action = 'EXCEL DOWNLOAD';
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	$("#listForm").attr("action",statisticsListConfig["downloadUrl"]);
	$("#listForm").submit();
}

function moveToAll(proc_date, result_type, shType){
	var searchType = $("#listForm select[name=searchType]").val();
	
	var perdForm = $("#perdForm");
	$("#perdForm").attr("action",perdConfig["listUrl"]);
	perdForm.find("input[name=search_to]").val(proc_date);
	perdForm.find("input[name=search_from]").val(proc_date);
	perdForm.find("input[name=privacyType]").val(result_type);
	perdForm.find("input[name=shType]").val(shType);
	perdForm.find("input[name=searchType]").val(searchType);
	perdForm.submit();
	
}

function moveToAll2(proc_date, result_type, shType){
	var system_seq = $("#listForm select[name=system_seq]").val();
	var searchType = $("#listForm select[name=searchType]").val();
	var perdForm = $("#perdForm");
	
	if(searchType == "03"){
		$("#perdForm").attr("action",perdConfig["listUrl2"]);		
	}
	else{
		$("#perdForm").attr("action",perdConfig["listUrl"]);
	}
	perdForm.find("input[name=search_to]").val(proc_date);
	perdForm.find("input[name=search_from]").val(proc_date);
	perdForm.find("input[name=privacyType]").val(result_type);
	perdForm.find("input[name=shType]").val(shType);
	perdForm.find("input[name=searchType]").val(searchType);
	perdForm.find("input[name=system_seq]").val(system_seq);
	perdForm.submit();
	
}


function moveToAllsys(proc_date, result_type, system_seq, sub_menu_id){
	var sysForm = $("#sysForm");
	$("#sysForm").attr("action",statisticsListConfig["allListUrl"]);
	sysForm.find("input[name=search_to]").val(proc_date);
	sysForm.find("input[name=search_from]").val(proc_date);
	sysForm.find("input[name=privacyType]").val(result_type);
	sysForm.find("input[name=system_seq]").val(system_seq);
	sysForm.find("input[name=sub_menu_id]").val(sub_menu_id);
	
	sysForm.submit();
	
}

function moveToAllsys_new(result_type, system_seq, sub_menu_id, shType){
	var searchType = $("#listForm select[name=searchType]").val();
	var sysForm = $("#sysForm");
	$("#sysForm").attr("action",statisticsListConfig["allListUrl"]);
	
	sysForm.find("input[name=privacyType]").val(result_type);
	sysForm.find("input[name=system_seq]").val(system_seq);
	sysForm.find("input[name=sub_menu_id]").val(sub_menu_id);
	sysForm.find("input[name=shType]").val(shType);
	sysForm.find("input[name=searchType]").val(searchType);
	sysForm.submit();
	
}

function moveToAllsys_new2(result_type, system_seq, sub_menu_id, shType){
	var searchType = $("#listForm select[name=searchType]").val();
	console.log(searchType);
	var sysForm = $("#sysForm");
	
	if(searchType == "03"){
		$("#sysForm").attr("action",statisticsListConfig["allListUrl2"]);
	}
	else{
		$("#sysForm").attr("action",statisticsListConfig["allListUrl"]);
	}
	sysForm.find("input[name=privacyType]").val(result_type);
	sysForm.find("input[name=system_seq]").val(system_seq);
	sysForm.find("input[name=sub_menu_id]").val(sub_menu_id);
	sysForm.find("input[name=shType]").val(shType);
	sysForm.find("input[name=searchType]").val(searchType);
	sysForm.submit();
	
}

function moveToAlldept(proc_date, result_type, dept_id){
	var deptForm = $("#deptForm");
	$("#deptForm").attr("action",statisticsListConfig["allListUrl"]);
	deptForm.find("input[name=search_to]").val(proc_date);
	deptForm.find("input[name=search_from]").val(proc_date);
	deptForm.find("input[name=privacyType]").val(result_type);
	deptForm.find("input[name=dept_id]").val(dept_id);
	
	deptForm.submit();
	
}

function moveToAlldept_new(result_type, dept_name, shType, searchType){
	var deptForm = $("#deptForm");
	$("#deptForm").attr("action",statisticsListConfig["allListUrl"]);
	deptForm.find("input[name=privacyType]").val(result_type);
	deptForm.find("input[name=dept_name]").val(dept_name);
	deptForm.find("input[name=shType]").val(shType);
	deptForm.find("input[name=searchType]").val(searchType);
	deptForm.submit();
}

function moveToAlldept_new2(result_type, dept_name, shType, searchType){
	var searchType = document.getElementById('searchType').value;
	var system_seq = document.getElementById('system_seq').value;
	var deptForm = $("#deptForm");
	
	if(searchType == "03"){
		$("#deptForm").attr("action",statisticsListConfig["allListUrl2"]);
	}
	else{
		$("#deptForm").attr("action",statisticsListConfig["allListUrl"]);
	}
	
	deptForm.find("input[name=privacyType]").val(result_type);
	deptForm.find("input[name=dept_name]").val(dept_name);
	deptForm.find("input[name=shType]").val(shType);
	deptForm.find("input[name=searchType]").val(searchType);
	deptForm.find("input[name=system_seq]").val(system_seq);
	deptForm.submit();
	
}

function moveToAllind(proc_date, result_type, emp_user_id){
	var indForm = $("#indForm");
	$("#indForm").attr("action",statisticsListConfig["allListUrl"]);
	indForm.find("input[name=search_to]").val(proc_date);
	indForm.find("input[name=search_from]").val(proc_date);
	indForm.find("input[name=privacyType]").val(result_type);
	indForm.find("input[name=emp_user_id]").val(emp_user_id);
	
	indForm.submit();
	
}

function moveToAllind_new(result_type, emp_user_id, dept_name, shType, searchType){
	var indForm = $("#indForm");
	$("#indForm").attr("action",statisticsListConfig["allListUrl"]);
	indForm.find("input[name=dept_name]").val(dept_name);
	indForm.find("input[name=privacyType]").val(result_type);
	indForm.find("input[name=emp_user_id]").val(emp_user_id);
	indForm.find("input[name=shType]").val(shType);
	indForm.find("input[name=searchType]").val(searchType);
	indForm.submit();
	
}

function moveToAllind_new2(result_type, emp_user_id, dept_name, shType, searchType){
	var searchType = document.getElementById('searchType').value;
	var system_seq = document.getElementById('system_seq').value;
	var indForm = $("#indForm");
	
	if(searchType == "03"){
		$("#indForm").attr("action",statisticsListConfig["allListUrl2"]);
	}
	else{
		$("#indForm").attr("action",statisticsListConfig["allListUrl"]);
	}
	indForm.find("input[name=dept_name]").val(dept_name);
	indForm.find("input[name=privacyType]").val(result_type);
	indForm.find("input[name=emp_user_id]").val(emp_user_id);
	indForm.find("input[name=shType]").val(shType);
	indForm.find("input[name=searchType]").val(searchType);
	indForm.find("input[name=system_seq]").val(system_seq);
	indForm.submit();
	
}

function sendDesc(check_type, sort_flag){
	var listForm = $("#listForm");
	$("#listForm").attr("action",statisticsListConfig["listUrl"]);	
	listForm.find("input[name=check_type]").val(check_type);
	listForm.find("input[name=sort_flag]").val(sort_flag);
	$("#listForm").submit();
}

$(function(){
	var check_type = $('#check_type').val();
	var sort_flag = $('#sort_flag').val();
	var rootPath = $('#rootPath').val();
	if(sort_flag == 1){
		$("#"+ check_type).attr("onclick","javascript:sendDesc('" +check_type+"',2)").attr("src", rootPath+"/resources/image/common/asc_btn.png")
		.attr("title","오름차순").attr("alt","오름차순"); 		
	}
	
	$('#search_fr').attr("onclick", "resetPeriod2()");
	$('#search_to').attr("onclick", "resetPeriod2()");
});

function resetPeriod2() {
	$("#check_flag").val('');
}

function searchInspection() {
	var listForm = $("#listForm");
	$("#listForm").attr("action",inspectionConfig["listUrl"]);	
	$("#listForm").submit();
}

function goPage(num){
	$("#listForm").attr("action",statisticsListConfig["listUrl"]);
	$("#listForm input[name=page_num]").val(num);
	$("#listForm").submit();
}

$(function(){
	$('input[type=text]').attr("onkeydown","javascript:if(event.keyCode==13){moveStatistics();}");
});


var exportExcel = function() {
	$("#listForm").find("input[name=sort_flag]").val(0);
	var chk_date = $('input[name=check_dp]').val();
	if (chk_date.length < 1) {
		excelPredList();
	} else {
		$('input[name=chk_date_type]').val(chk_date);	
		excelPredList();
	}
}

function excelperdbysta() {
	var $form = $("#listForm");
	var log_message_params = '';
	var menu_id = $('input[name=current_menu_id]').val();
	var log_message_title = '기간별시스템별처리량';
	var log_action = 'EXCEL DOWNLOAD';
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	$("#listForm").attr("action",statisticsListConfig["downloadUrl"]);
	$("#listForm").submit();
}