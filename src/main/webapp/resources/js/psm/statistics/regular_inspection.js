
function searchInspection() {
	var listForm = $("#listForm");
	$("#listForm").attr("action",inspectionConfig["listUrl"]);	
	$("#listForm").submit();
}

function searchInspection2() {
	var listForm = $("#listForm2");
	$("#listForm2").attr("action",inspectionConfig["listUrl"]);	
	$("#listForm2").submit();
}

function createReport() {
	var url = inspectionConfig["reportUrl"];
	
	var search_fr = $("#search_fr").val();
	var search_to = $("#search_to").val();
	var proc_year = $("#proc_year").val();
	var proc_month = $("#proc_month").val();
	
	if(proc_month < 10){
		proc_month = "0"+proc_month;
	}
	
	var proc_date = proc_year+""+proc_month;
	
	url += "?search_fr=" + search_fr + "&search_to=" + search_to+"&proc_date="+proc_date;
	window.open(url, 'Popup', 'height=800px, width=800px, scrollbars=yes');	
}

function loadReport(report_seq) {
	var url = inspectionConfig["reportUrl"];
	url += "?report_seq=" + report_seq;
	window.open(url, 'Popup', 'height=800px, width=800px, scrollbars=yes');	
}

$(document).ready(function(){
	if(select_tab != ''){
		$(".nav-tabs li").eq(parseInt(select_tab)).find('a').click();
	}
});