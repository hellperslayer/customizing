

function searchList() {
	$("input[name=isSearch]").val('Y');
	$("#listForm").attr("action",statisticsListConfig["listUrl"]);

	$("#listForm").submit();
}

function goInspectionDetail(seq) {
	var listForm = $("#listForm");
	$("#listForm").attr("action",inspectionHistConfig["detailUrl"]);
	listForm.find("input[name=seq]").val(seq);
	listForm.submit();
}

function addInspectionHist() {
	var inspector = $('input[name=inspector]').val();
	if(inspector == '') {
		alert("점검자를 입력해주세요");
		$('input[name=inspector]').focus();
		return false;
	}
	
	var confirmVal = confirm("등록하시겠습니까?");
	if(confirmVal){
		sendAjaxInspectionHist("add");	
	}else{
		return false;
	}
}

function saveInspectionHist() {
	var inspector = $('input[name=inspector]').val();
	if(inspector == '') {
		alert("점검자를 입력해주세요");
		$('input[name=inspector]').focus();
		return false;
	}
	
	var confirmVal = confirm("등록하시겠습니까?");
	if(confirmVal){
		sendAjaxInspectionHist("save");
	}else{
		return false;
	}
}

function sendAjaxInspectionHist(type){
	var $form = $("#listForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = '';
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'add') {
		log_message_title = '정기점검 이력관리 등록';
		log_action = 'INSERT';
	} else if (type == 'save') {
		log_message_title = '정기점검 이력관리 수정';
		log_action = 'UPDATE';
	} else if (type == 'remove') {
		log_message_title = '정기점검 이력관리 삭제';
		log_action = 'REMOVE';
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(inspectionHistConfig[type + "Url"],$form.serialize(), ajaxInspectionHist_successHandler, ajaxInspectionHist_errorHandler, type);
}

function ajaxInspectionHist_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = inspectionHistConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}
		
		goInspectionHistList();
	}
}

function ajaxInspectionHist_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	log_message += codeLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("정기점검 이력관리 실패하였습니다." + textStatus);
			break;
		case "save":
			alert("정기점검 이력관리 실패하였습니다." + textStatus);
			break;
		case "remove":
			alert("정기점검 이력관리 실패하였습니다." + textStatus);
			break;
		default:
	}
}

function goInspectionHistList() {
	var listForm = $("#listForm");
	$("#listForm").attr("action",inspectionConfig["listUrl"]);
	listForm.submit();
}

function searchInspectionHistList() {
	$('input[name=page_num]').attr("value","1");
	$("#listForm").attr("action",inspectionConfig["listUrl"]);
	$("#listForm").submit();
}

function goPage(num){
	var tab_index = $(".nav-tabs").find(".active").index();
	$("#reportForm").attr("action",inspectionConfig["listUrl"]);
	$("#reportForm input[name=page_num]").val(num);
	$("#reportForm input[name=select_tab]").val(tab_index);
	$("#reportForm").submit();
}

function searchPatchStatus(){
	var tab_index = $(".nav-tabs").find(".active").index();
	$("#patchForm").attr("action",inspectionConfig["listUrl"]);
	$("#patchForm input[name=select_tab]").val(tab_index);
	$("#patchForm").submit();
}

function report_delete(report_seq){
	var tab_index = $(".nav-tabs").find(".active").index();
	$("#reportForm input[name=select_tab]").val(tab_index);
	$("#reportForm input[name=report_seq]").val(report_seq);
	var num = $("#reportForm input[name=page_num]").val();
	$("#reportForm").attr("action",rootPath +'/report/inspectionDataDelete.html');
	var options = ({
		type:'POST',
		dataType: "json",
		success: function(data){
			console.log(data);
			goPage(num);
		},
		error: function(){
		}
	});
	$("#reportForm").ajaxSubmit(options);
}