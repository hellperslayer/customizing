/**
 * @page : syjung
 * @desc : dashboard (지도가 들어간 버전)
 */
var systemType;
var graphColor;
var procDate;
$(document).ready(function() {
	if($("#search_fr").val() == "") {
		initDate1();
	}
	if($("#yearSel > option").length == 0) {
		var day = new Date();
		var year = day.getFullYear();
		for(var i=year-10; i<=year; i++) {
			$("<option value=" + i + ">" + i + "년</option>").appendTo("#yearSel");
		}
		initDate3();
	}
	$("#dateBtnDiv > button").each(function(idx) {
		if($(this).hasClass("btn-inline")) {
			$(this).click();
		}
	});
	$("#srchDiv").find(".tools > a").click();
	/*
	$("#search_to").datepicker({
		format: "yyyy-mm-dd",
		 language: "kr",
		 todayHighlight: false
	 });
	$("#search_fr").datepicker({
		format: "yyyy-mm-dd",
		language: "kr",
		todayHighlight: false
	});
	*/
	/*
	$("#search_fr").datepicker().on("changeDate", function(event) {
		if($(".datepicker").html() != undefined) {
			$(".datepicker").hide();
		}
	});
	$("#search_to").datepicker().on("changeDate", function(event) {
		if($(".datepicker").html() != undefined ) {
			$(".datepicker").hide();
		}
	});
	*/
	$("#search_fr").datepicker().on("changeDate", function(e) {
		$(".datepicker").hide();
	});
	$("#search_fr").click(function() {
		$(".datepicker").show();
		var obj = $("#search_fr").offset(); 
		$(".datepicker").css({left : obj.left});
	});
	$("#search_to").datepicker().on("changeDate", function(e) {
		$(".datepicker").hide();
	});
	$("#search_to").click(function() {
		$(".datepicker").show();
		var obj = $("#search_to").offset(); 
		$(".datepicker").css({left : obj.left});
	});
});

var fnNowClick = function(pos) {
	$("#dateBtnDiv > button").each(function(idx) { 
		if(pos == idx+1) {
			$(this).removeClass("btn-outline");
			$(this).addClass("btn-inline");
		} else {
			$(this).removeClass("btn-inline");
			$(this).addClass("btn-outline");
		}
	});
	if(pos == 1) {
		initDate1();
		initDate3();
		initDate4();
		$("#srchDiv").hide();
		$("#system_seq").children().first().prop("selected", true);
		setCharts();
		var realTimeVar = setInterval(setCharts(), 60000);
	} else if(pos == 2) {
		initDate1();
		initDate3();
		$("#srchDiv").show();
		$("#dateDiv").show();
		$("#MonthDiv").hide();
		clearInterval(realTimeVar);
		setDayCharts();
	} else if(pos == 3) {
		initDate1();
		initDate3();
		$("#srchDiv").show();
		$("#dateDiv").hide();
		$("#MonthDiv").show();
		clearInterval(realTimeVar);
		setMonthCharts();
	}
};

var initDate1 = function() {
	var day = new Date();
	var fromDay = new Date(day.valueOf() - (24 * 60 * 60 * 1000));
	var fromMonth = "" + (fromDay.getMonth()+1);
	var fromDate = "" + fromDay.getDate();
	if(fromMonth.length < 2) {
		fromMonth = "0" + fromMonth;
	}
	if(fromDate.length < 2) {
		fromDate = "0" + fromDate;
	}
	var tempDate = fromDay.getFullYear() + "-" + fromMonth + "-" + fromDate;
	$("#search_fr").val(tempDate);
	$("#search_to").val(tempDate);
	$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-1d");
	$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","d");
};


var initDate3 = function() {
	var day = new Date();
	var year = day.getFullYear();
	var month = (day.getMonth() + 1);
	$("#yearSel > option").each(function() {
		if($(this).val() == year) {
			$(this).prop("selected", true);
		} else {
			$(this).prop("selected", false);
		}
	});
	$("#monthSel > option").each(function() {
		if($(this).val() == month) {
			$(this).prop("selected", true);
		} else {
			$(this).prop("selected", false);
		}
	});
};

var initDate4 = function() {
	var fromDay = new Date();
	var fromMonth = "" + (fromDay.getMonth()+1);
	var fromDate = "" + fromDay.getDate();
	if(fromMonth.length < 2) {
		fromMonth = "0" + fromMonth;
	}
	if(fromDate.length < 2) {
		fromDate = "0" + fromDate;
	}
	$("#search_fr").val(fromDay.getFullYear() + "-" + fromMonth + "-" + fromDate);
};

var searchSystemType = function(system_seq) {
	var selBtn = 0;
	$("#dateBtnDiv > button").each(function(idx) {
		if($(this).hasClass("btn-inline")) {
			selBtn = idx + 1;
		}
	});
	if(selBtn == 1) {
		var tDate = $("#search_to").val();
		var fDate = $("#search_fr").val();
		if(tDate < fDate) {
			alert("검색 기간을 확인하세요");
			return;
		}
		if(system_seq == null) {
			systemType = $("#system_seq").val();
			if(systemType == null || systemType == "") {
				$("#show_all").css("display", "none");
				graphColor = null;
			} else {
				$("#show_all").css("display", "block");
			}
		} else {
			systemType=system_seq;
			$("#system_seq").val(system_seq);
			$("#show_all").css("display", "block");
		}
		setCharts();
	} else if(selBtn == 2) {
		var tDate = $("#search_to").val();
		var fDate = $("#search_fr").val();
		if(tDate < fDate) {
			alert("검색 기간을 확인하세요");
			return;
		}
		if(system_seq == null) {
			systemType = $("#system_seq").val();
			if(systemType == null || systemType == "") {
				$("#show_all").css("display", "none");
				graphColor = null;
			} else {
				$("#show_all").css("display", "block");
			}
		} else {
			systemType=system_seq;
			$("#system_seq").val(system_seq);
			$("#show_all").css("display", "block");
		}
		setDayCharts();
	} else if(selBtn == 3) {
		if(system_seq == null) {
			systemType = $("#system_seq").val();
			if(systemType == null || systemType == "") {
				$("#show_all").css("display", "none");
				graphColor = null;
			} else {
				$("#show_all").css("display", "block");
			}
		} else {
			systemType=system_seq;
			$("#system_seq").val(system_seq);
			$("#show_all").css("display", "block");
		}
		setMonthCharts();
	}	
};

var setCharts = function() {
	setChart2(1);
	setChart3(1);
	setChart4(1);
	setChart7(1);
	setChart8(1);
};

var setDayCharts = function() {
	setChart2(2);
	setChart3(2);
	setChart4(2);
	setChart7(2);
	setChart8(2);
};

var setMonthCharts = function() {
	setChart2(3);
	setChart3(3);
	setChart4(3);
	setChart7(3);
	setChart8(3);
};

function setColorIndex(idx) {
	graphColor = idx;
}

function showAllSystemType() {
	systemType = null;
	graphColor = null;
	$("#system_seq").val("");
	$("#show_all").css('display', 'none');
	setCharts();
}

function setChart2(selVal) {
	if(selVal == 1) {
		$.ajax({
			type: "POST",
			url : rootPath + "/dashboard/dashboard_chart2.html",
			data: { 
				system_seq : systemType,
				search_from : $("#search_fr").val(),
				search_to : $("#search_to").val()
			},
			success: function(data) {
				drawchart2(data.dashboard_chart2, 1);
			}
		});
	} else if(selVal == 2) {
		$.ajax({
			type: "POST",
			url : rootPath + "/dashboard/dashboard_chart2.html",
			data: { 
				system_seq : systemType,
				selVal : selVal,
				search_from : $("#search_fr").val(),
				search_to : $("#search_to").val()
			},
			success: function(data) {
				drawchart2(data.dashboard_chart2, 2);
			}
		});
	} else if(selVal == 3) {
		var year = $("#yearSel").find("option:selected").val();
		var dateTmp = $("#monthSel").find("option:selected").val();
		var date = dateTmp.length < 2 ? "0" + dateTmp : dateTmp;
		var tempDate = year + "-" + date;
		$.ajax({
			type: "POST",
			url : rootPath + "/dashboard/dashboard_chart2.html",
			data: { 
				system_seq : systemType,
				selVal : selVal,
				search_to : tempDate
			},
			success: function(data) {
				drawchart2(data.dashboard_chart2, 3);
			}
		});
	}
}

function setChart3(selVal) {
	if(selVal == 1) {
		$.ajax({
			type: "POST",
			url: rootPath + "/dashboard/dashboard_chart3.html",
			data: {
				system_seq : systemType,
				search_from : $("#search_fr").val(),
				search_to : $("#search_to").val()
			},
			success: function(data) {
				drawchart3(data.dashboard_chart3);
			}
		});
	} else if(selVal == 2) {
		$.ajax({
			type: "POST",
			url: rootPath + "/dashboard/dashboard_chart3.html",
			data: {
				system_seq : systemType,
				selVal : selVal,
				search_from : $("#search_fr").val(),
				search_to : $("#search_to").val()
			},
			success: function(data) {
				drawchart3(data.dashboard_chart3);
			}
		});
	} else if(selVal == 3) {
		var year = $("#yearSel").find("option:selected").val();
		var dateTmp = $("#monthSel").find("option:selected").val();
		var date = dateTmp.length < 2 ? "0" + dateTmp : dateTmp;
		var tempDate = year + "-" + date;
		$.ajax({
			type: "POST",
			url : rootPath + "/dashboard/dashboard_chart3.html",
			data: { 
				system_seq : systemType,
				selVal : selVal,
				search_to : tempDate
			},
			success: function(data) {
				drawchart3(data.dashboard_chart3);
			}
		});
	}
}

function setChart4(selVal) {
	if(selVal == 1) {
		$.ajax({
			type: 'POST',
			url: rootPath + '/dashboard/dashboard_chart4.html',
			data: { 
				system_seq: systemType,
				search_from: $("#search_fr").val(),
				search_to: $("#search_to").val()
			},
			success: function(data) {
				drawchart4(data.dashboard_chart4);
			}
		});
	} else if(selVal == 2) {
		$.ajax({
			type: 'POST',
			url: rootPath + '/dashboard/dashboard_chart4.html',
			data: { 
				system_seq: systemType,
				selVal : selVal,
				search_from: $("#search_fr").val(),
				search_to: $("#search_to").val()
			},
			success: function(data) {
				drawchart4(data.dashboard_chart4);
			}
		});
	} else if(selVal == 3) {
		var year = $("#yearSel").find("option:selected").val();
		var dateTmp = $("#monthSel").find("option:selected").val();
		var date = dateTmp.length < 2 ? "0" + dateTmp : dateTmp;
		var tempDate = year + "-" + date;
		$.ajax({
			type: "POST",
			url : rootPath + "/dashboard/dashboard_chart4.html",
			data: { 
				system_seq : systemType,
				selVal : selVal,
				search_to : tempDate
			},
			success: function(data) {
				drawchart4(data.dashboard_chart4);
			}
		});
	}
}

function setChart5() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chart5.html',
		data: { 
			system_seq: systemType,
			search_from: $("#search_fr").val(),
			search_to: $("#search_to").val()
		},
		success: function(data) {
			drawchart5(data.dashboard_chart5);
		}
	});
}

function setChart6() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chart6.html',
		data: { 
			system_seq: systemType,
			search_from: $("#search_fr").val(),
			search_to: $("#search_to").val()
		},
		success: function(data) {
			drawchart6(data.dashboard_chart6);
		}
	});
}

function setChart7(selVal) {
	if(selVal == 1) {
		$.ajax({
			type: 'POST',
			url: rootPath + '/dashboard/dashboard_chart7.html',
			data: { 
				system_seq: systemType,
				search_from: $("#search_fr").val(),
				search_to: $("#search_to").val()
			},
			success: function(data) {
				drawchart7(data.dashboard_chart7);
			}
		});
	} else if(selVal == 2) {
		$.ajax({
			type: 'POST',
			url: rootPath + '/dashboard/dashboard_chart7.html',
			data: { 
				system_seq: systemType,
				selVal : selVal,
				search_from: $("#search_fr").val(),
				search_to: $("#search_to").val()
			},
			success: function(data) {
				drawchart7(data.dashboard_chart7);
			}
		});
	} else if(selVal == 3) {
		var year = $("#yearSel").find("option:selected").val();
		var dateTmp = $("#monthSel").find("option:selected").val();
		var date = dateTmp.length < 2 ? "0" + dateTmp : dateTmp;
		var tempDate = year + "-" + date;
		$.ajax({
			type: "POST",
			url : rootPath + "/dashboard/dashboard_chart7.html",
			data: { 
				system_seq : systemType,
				selVal : selVal,
				search_to : tempDate
			},
			success: function(data) {
				drawchart7(data.dashboard_chart7);
			}
		});
	}
}

function setChart8(selVal) {
//	if(selVal == 1) {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chart8.html',
		data: { 
			system_seq: systemType,
			search_from: $("#search_fr").val(),
			search_to: $("#search_to").val()
		},
		success: function(data) {
			if(data.ui_type == "G") {
				drawchart8_stack(data.dashboard_chart8);
			} else {
				drawchart8(data.dashboard_chart8);
			}
			
		}
	});
	/*} else if(selVal == 2) {
		
	} else if(selVal == 3) {
		
	}*/
}

function drawchartEmp(data) {
	$("#chart_emp").find('tbody').empty();
	if(data != null) {		
		var d = data.occr_dt;
		var yyyy = d.substring(0,4);
		var mm = d.substring(4,6);
		var dd = d.substring(6,8);
		
		$("<tr onclick='setCheckExtrtDetailLog(\"" + data.occr_dt + "\", \"" + data.emp_user_id + "\", " + data.emp_detail_seq + ", \"" + data.rule_cd + "\")' style='cursor: pointer;'>").appendTo($("#chart_emp").find('tbody'))
		.append($("<td style='text-align: center;'>")
				.text(check_null(yyyy+"-"+mm+"-"+dd))
		)
		.append($("<td style='text-align: center;'>")
				.text(check_null(data.scen_name))
		)
		.append($("<td style='text-align: center;'>")
				.text(check_null(data.dept_name))
		)
		.append($("<td style='text-align: center;'>")
				.text(check_null(data.emp_user_id))
		)
		.append($("<td style='text-align: center;'>")
				.text(check_null(data.emp_user_name))
		)
		.fadeOut()
		.fadeIn("slow");
	}
}

function drawchart2(data, selVal) {
	$("#chart2").find('tbody').empty();
	if(data != null) {		
		$.each(data, function( i, item ) {
			if(selVal == 3) {
				var d = item.proc_date;
				var yyyy = d.substring(0,4);
				var mm = d.substring(4,6);
				var dd = d.substring(6,8);
				var dept_name = item.dept_name;
				if(dept_name.length > 11) {
					dept_name = dept_name.substring(0,10)+'..';
				}
				
				var userid = item.emp_user_id;
//				$("<tr onclick='goAllLogInqList(" + item.proc_date + ", \"" + item.dept_name + "\", \"" + item.emp_user_id + "\", \"" + item.emp_user_name + "\", \"" + item.user_ip + "\");' style='cursor: pointer;'>").appendTo($("#chart2").find('tbody'))
				$("<tr>").appendTo($("#chart2").find('tbody'))
				
				.append($("<td style='text-align: center;'>")
						.text(check_null(yyyy+"-"+mm))
				)
				.append($("<td style='text-align: center;'>")
						.text(check_null(dept_name))
				)
				.append($("<td style='text-align: center;'>")
						.text(check_null(item.emp_user_id))
				)
				.append($("<td style='text-align: center;'>")
						.text(check_null(item.emp_user_name))
				)
				/*
				.append($("<td style='text-align: center;'>")
						.text(check_null(item.user_ip))
				)*/
				.append($("<td style='text-align: center;'>")
						.text(check_null(item.cnt))
				)
				.fadeOut()
				.fadeIn("slow");
			} else {
				var d = item.proc_date;
				var yyyy = d.substring(0,4);
				var mm = d.substring(4,6);
				var dd = d.substring(6,8);
				var dept_name = item.dept_name;
				if(dept_name.length > 11) {
					dept_name = dept_name.substring(0,10)+'..';
				}
				
				var userid = item.emp_user_id;
//				$("<tr onclick='goAllLogInqList(" + item.proc_date + ", \"" + item.dept_name + "\", \"" + item.emp_user_id + "\", \"" + item.emp_user_name + "\", \"" + item.user_ip + "\");' style='cursor: pointer;'>").appendTo($("#chart2").find('tbody'))
				$("<tr>").appendTo($("#chart2").find('tbody'))
				
				.append($("<td style='text-align: center;'>")
						.text(check_null(yyyy+"-"+mm+"-"+dd))
				)
				.append($("<td style='text-align: center;'>")
						.text(check_null(dept_name))
				)
				.append($("<td style='text-align: center;'>")
						.text(check_null(item.emp_user_id))
				)
				.append($("<td style='text-align: center;'>")
						.text(check_null(item.emp_user_name))
				)
				/*
				.append($("<td style='text-align: center;'>")
						.text(check_null(item.user_ip))
				)*/
				.append($("<td style='text-align: center;'>")
						.text(check_null(item.cnt))
				)
				.fadeOut()
				.fadeIn("slow");
			}
		});
	}
}

function drawchart3 (data) {
	if(data == null) {
		$("#chart3").hide();
		$("#chart3_nodata").show();
		return;
	} else if(data.length == 0) {
		$("#chart3").hide();
		$("#chart3_nodata").show();
		return;
	} else {
		$("#chart3_nodata").hide();
		$("#chart3").show();
	}

	
	var dataProvider = new Array();
	var	color = ["#ff0000", "#ff8000", "#ffff00", "#80ff00", "#00ff00", "#00ff80", "#00ffff", "#0080ff", "#0000ff", "#8000ff"];
	
	for(var i=0; i<data.length; i++) {
		var t = i%10;
		if(graphColor != null)
			t = graphColor;
		var value = {"system_name": data[i].data1,"nPrivCount": data[i].cnt1, "system_seq": data[i].data2, "color": color[t], "index": t};
		dataProvider.push(value);
	}
	/*for (var i = 0; i < jsonData.length; i++) {
		var value = {"dept_name": jsonData[i].data1,"nPrivCount": jsonData[i].cnt1};
		dataProvider.push(value);
	}*/
	
	var chart3 = AmCharts.makeChart("chart3", {
		"type": "serial",
	     "theme": "light",
		"categoryField": "system_name",
		"rotate": true,
		"startDuration": 1,
		"categoryAxis": {
			"gridPosition": "start",
			"position": "left"
		},
		"trendLines": [],
		"graphs": [
			{
				"balloonText": "[[category]]: [[value]]건",
				"fillAlphas": 0.8,
				"id": "AmGraph-1",
				"lineAlpha": 0.2,
//				"title": "개인정보검출(건)",
				"type": "column",
				"valueField": "nPrivCount",
				"descriptionField": "system_seq",
				"labelText": "[[value]]",
				"fillColorsField": "color",
				"colorField": "index"
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"position": "bottom",
				"axisAlpha": 0,
				"title": "(개인정보취급자가 정보주체의 개인정보를 처리하기 위해 시도한 횟수)"
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider": dataProvider,
	    "export": {
	    	"enabled": false
	     }

	});
	
	chart3.addListener("clickGraphItem", handleClick);
}


function handleClick(event) {
	var system_seq = event.item.description;
	graphColor = event.item.color;
	searchSystemType(system_seq);
}

function drawchart4(data) {
	if(data == null) {
		$("#chart4").hide();
		$("#chart4_nodata").show();
		return;
	} else if(data.length == 0) {
		$("#chart4").hide();
		$("#chart4_nodata").show();
		return;
	} else {
		$("#chart4_nodata").hide();
		$("#chart4").show();
	}

	
	var dataProvider = new Array();
	for (var i = 0; i < data.length; i++) {
		var value = {"privacy_desc": data[i].data1,"nPrivCount": data[i].cnt1, "result_type": data[i].result_type };
		dataProvider.push(value);
	}
	
	/*var chart = AmCharts.makeChart("chart4", {
	    "type": "pie",
	    "theme": "light",
	    "innerRadius": "40%",
	    "gradientRatio": [-0.4, -0.4, -0.4, -0.4, -0.4, -0.4, 0, 0.1, 0.2, 0.1, 0, -0.2, -0.5],
	    "dataProvider": dataProvider,
	    "balloonText": "[[value]]",
	    "valueField": "nPrivCount",
	    "titleField": "privacy_desc",
	    "balloon": {
	        "drop": true,
	        "adjustBorderColor": false,
	        "color": "#FFFFFF",
	        "fontSize": 16
	    },
	    "export": {
	        "enabled": false
	    }
	});*/
	var chart4 = AmCharts.makeChart( "chart4", {
	  "type": "pie",
	  "theme": "light",
	  "dataProvider": dataProvider,
	  "titleField": "privacy_desc",
	  "valueField": "nPrivCount",
	  "descriptionField": "result_type",
	  "labelRadius": 5,

	  "radius": "40%",
	  "innerRadius": "35%",
	  "labelText": "[[title]]",
	  "balloonText": "[[title]]: [[value]]건 ([[percents]]%)",
	  "export": {
	    "enabled": false
	  }
	} );
	
//	chart4.addListener("clickSlice", handleClick2);
}


function handleClick2(event) {
	var result_type = event.dataItem.description;
	goAllLogInqList3(result_type);
}

function drawchart5(data) {
	if(data == null) {
	} else if(data.length == 0) {
		$("#chart5").hide();
		$("#chart5_nodata").show();
		return;
	} else {
		$("#chart5_nodata").hide();
		$("#chart5").show();
	}

	
	var dataProvider = new Array();
	for(var i=0; i<data.length; i++) {
		var value = {"proc_date": data[i].proc_date, "cnt": data[i].cnt1};
		dataProvider.push(value);
	}
	var chart = AmCharts.makeChart("chart5", {
	    "type": "serial",
	    "theme": "light",
	    "marginRight":30,
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "stackType": "regular",
	        "gridAlpha": 0.07,
	        "position": "left"
	    }],
	    "graphs": [{
	        "balloonText": "[[category]] : <span style='font-size:14px; color:#000000;'><b>[[value]]</b></span>",
	        "fillAlphas": 0.6,
	        "lineAlpha": 0.4,
	        "valueField": "cnt",
	        "labelText": "[[value]]"
	    }],
	    "plotAreaBorderAlpha": 0,
	    "marginTop": 10,
	    "marginLeft": 0,
	    "marginBottom": 0,
	    "chartCursor": {
	        "cursorAlpha": 0
	    },
	    "categoryField": "proc_date",
	    "categoryAxis": {
	        "startOnAxis": true,
	        "axisColor": "#DADADA",
	        "gridAlpha": 0.07
	    },
	    "export": {
	    	"enabled": false
	     }
	});

}

function drawchart6(data) {
	
	if(data.length == 0) {
		$("#chart6").hide();
		$("#chart6_nodata").show();
	} else {
		$("#chart6_nodata").hide();
		$("#chart6").show();
	}
	
	var dataProvider = new Array();
	var color = ["#ff0000", "#ff8000", "#ffff00", "#00ff00", "#0040ff"];
	
	for (var i = 0; i < data.length; i++) {
		var t = i%5;
		
		var temp = data[i].data1;
		if(data[i].data1.length > 8 ) {
			temp = data[i].data1.substring(0, 8) + "..";
		}
		var value = {"data1": temp,"nPrivCount": data[i].nPrivCount, "scen_seq": data[i].data1, "color": color[t]};
		dataProvider.push(value);
	}
	
	var chart6 = AmCharts.makeChart("chart6", {
	  "type": "serial",
	  "theme": "light",
	  "marginRight": 70,
	  //"rotate": true,
	  "dataProvider": dataProvider,
	  "valueAxes": [{
	    "axisAlpha": 0,
	    "position": "left",
	    "title": "추출검출(건)"
	  }],
	  "startDuration": 1,
	  "graphs": [{
	    "balloonText": "[[description]]: [[value]]건",
	    "fillColorsField": "color",
	    "fillAlphas": 0.9,
	    "lineAlpha": 0.2,
	    "type": "column",
	    "valueField": "nPrivCount",
	    "descriptionField": "scen_seq",
	    "labelText": "[[value]]"
	  }],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "data1",
	  "categoryAxis": {
	    "gridPosition": "start"
	    //,"labelRotation": 45
	  },
	  "export": {
	    "enabled": false
	  }

	});
	
	//chart6.addListener("clickGraphItem", handleClick_chart6);
}

function handleClick_chart6(event) {
	var scen_seq = event.item.description;
	goExtrtCondByInqList(scen_seq);
}

function drawchart7(data) {
//	$("#chart7").find('tbody').empty();
	$("#chart7").find('tbody').html("");
//	var maxTrCnt = 10;
	if(0 < data.length) {
//		objCnt = data.length;
		$.each(data, function( i, item ) {
			/*
			if(i >= maxTrCnt){
				return;
			}
			deptTopTrCnt = $("#chart7").find('tbody').find('tr').length;
			var limit = deptTopTrCnt + objCnt;
			if(deptTopTrCnt >= maxTrCnt) {
				$("#chart7").find('tbody').find('tr').eq(0).remove();
			}
			*/
			//부서별 로그 수집 현황에 부서명이 없을 경우 부서명 없음으로 출력
			if(item.data1=="" || item.data1==null){
				item.data1 = "부서명 없음";
			}
			
//			$("<tr onclick='goAllLogInqList2(" + item.proc_date + ", \"" + item.data1 + "\");' style='cursor: pointer;'>").appendTo($("#chart7").find('tbody'))
			$("<tr>").appendTo($("#chart7").find('tbody'))
			.append($("<td style='text-align: center;'>")
					.text(i+1)
			)
			.append($("<td style='text-align: center;'>")
					.text(check_null(item.data1))
			)
			.append($("<td style='text-align: center;'>")
					.text(check_null(item.cnt1.toString().addComma()))
			)
			.append($("<td style='text-align: center;'>")
					.text(check_null(item.cnt2.toString().addComma()))
			);
//			.fadeOut()
//			.fadeIn("slow");
		});
	}
}

function drawchart8(data) {
	if(data == null) {
		$("#chart8").hide();
		$("#chart8_nodata").show();
		return;
	} else if(data.length == 0) {
		$("#chart8").hide();
		$("#chart8_nodata").show();
		return;
	} else {
		$("#chart8_nodata").hide();
		$("#chart8").show();
	}
	
	var gaugeChart = AmCharts.makeChart("chart8", {
	  "type": "gauge",
	  "theme": "light",
	  "axes": [{
	    "axisAlpha": 0,
	    "tickAlpha": 0,
	    "labelsEnabled": false,
	    "startValue": 0,
	    "endValue": 100,
	    "startAngle": 0,
	    "endAngle": 270,
	    "bands": [{
	      "color": "#eee",
	      "startValue": 0,
	      "endValue": 100,
	      "radius": "100%",
	      "innerRadius": "85%"
	    }, {
	      "color": "#84b761",
	      "startValue": 0,
	      "endValue": data[0].cnt1,
	      "radius": "100%",
	      "innerRadius": "85%",
	      "balloonText": data[0].cnt1 + "%"
	    }, {
	      "color": "#eee",
	      "startValue": 0,
	      "endValue": 100,
	      "radius": "80%",
	      "innerRadius": "65%"
	    }, {
	      "color": "#fdd400",
	      "startValue": 0,
	      "endValue": data[0].cnt2,
	      "radius": "80%",
	      "innerRadius": "65%",
	      "balloonText": data[0].cnt2 + "%"
	    }, {
	      "color": "#eee",
	      "startValue": 0,
	      "endValue": 100,
	      "radius": "60%",
	      "innerRadius": "45%"
	    }, {
	      "color": "#cc4748",
	      "startValue": 0,
	      "endValue": data[0].cnt3,
	      "radius": "60%",
	      "innerRadius": "45%",
	      "balloonText": data[0].cnt3 + "%"
	    }/*, {
	      "color": "#eee",
	      "startValue": 0,
	      "endValue": 100,
	      "radius": "40%",
	      "innerRadius": "25%"
	    }, {
	      "color": "#67b7dc",
	      "startValue": 0,
	      "endValue": 68,
	      "radius": "40%",
	      "innerRadius": "25%",
	      "balloonText": "68%"
	    }*/]
	  }],
	  "allLabels": [{
	    "text": "CPU",
	    "x": "49%",
	    "y": "5%",
	    "size": 15,
	    "bold": true,
	    "color": "#84b761",
	    "align": "right"
	  }, {
	    "text": "MEMORY",
	    "x": "49%",
	    "y": "15%",
	    "size": 15,
	    "bold": true,
	    "color": "#fdd400",
	    "align": "right"
	  }, {
	    "text": "HDD",
	    "x": "49%",
	    "y": "24%",
	    "size": 15,
	    "bold": true,
	    "color": "#cc4748",
	    "align": "right"
	  }/*, {
	    "text": "Fourth option",
	    "x": "49%",
	    "y": "33%",
	    "size": 15,
	    "bold": true,
	    "color": "#67b7dc",
	    "align": "right"
	  }*/],
	  "export": {
	    "enabled": false
	  }
	});
}

function drawchart8_stack(data) {
	if(data == null) {
		$("#chart8").hide();
		$("#chart8_nodata").show();
		return;
	}
	var dataProvider = new Array();
	for(var i=0; i<data.length; i++) {
		var serverSeq = data[i].server_seq;

		var serverStr = "";
		if(serverSeq == 1) {
			serverStr = "공통기반";
		} else if(serverSeq == 2) {
			serverStr = "온나라";
		} else if(serverSeq == 3) {
			serverStr = "내부망";
		} else if(serverSeq == 4) {
			serverStr = "외부망";
		} else if(serverSeq == 5) {
			serverStr = "분석관리";
		} 
		
		var value = {"serverStr": serverStr, "server_seq": data[i].server_seq, "cnt1": data[i].cnt1, "cnt2": data[i].cnt2, "cnt3": data[i].cnt3};
		dataProvider.push(value);
	}
	
	
	if(data.length == 0) {
		$("#chart8").hide();
		$("#chart8_nodata").show();
		return;
	} else {
		$("#chart8_nodata").hide();
		$("#chart8").show();
	}
	var chart = AmCharts.makeChart("chart8", {
	    "type": "serial",
	    "theme": "none",
	    "legend": {},
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.3,
	        "gridAlpha": 0
	    }],
	    "graphs": [{
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:12px'>[[category]]: <b>[[value]]%</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "CPU",
	        "type": "column",
	        "color": "#000000",
	        "fillColors": "#FF6600",
	        "valueField": "cnt1"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:12px'>[[category]]: <b>[[value]]%</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "MEMORY",
	        "type": "column",
	        "color": "#000000",
	        "fillColors": "#FCD202",
	        "valueField": "cnt2"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:12px'>[[category]]: <b>[[value]]%</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "HDD",
	        "type": "column",
	        "color": "#000000",
	        "fillColors": "#B0DE09",
	        "valueField": "cnt3"
	    }],
	    "categoryField": "serverStr",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "gridAlpha": 0,
	        "position": "left"
	    },
	    "export": {
	    	"enabled": false
	     }

	});
}

function goAllLogInqList(proc_date, dept_name, emp_user_id, emp_user_name, user_ip) {
	$("#listForm").attr("action",dashboardConfig["listUrl"]);
	$("#listForm input[name=search_from]").val(proc_date);
	$("#listForm input[name=search_to]").val(proc_date);
	$("#listForm input[name=dept_name]").val(dept_name);
	$("#listForm input[name=emp_user_id]").val(emp_user_id);
	$("#listForm input[name=emp_user_name]").val(emp_user_name);
	$("#listForm input[name=user_ip]").val(user_ip);
	$("#listForm input[name=main_menu_id]").val('MENU00040');
	//$("#listForm input[name=system_seq").val(system_seq);
	$("#listForm").submit();
}

function goAllLogInqList2(proc_date, dept_name) {
	$("#listForm").attr("action",dashboardConfig["listUrl"]);
	$("#listForm input[name=search_from]").val(proc_date);
	$("#listForm input[name=search_to]").val(proc_date);
	$("#listForm input[name=dept_name]").val(dept_name);
	$("#listForm input[name=main_menu_id]").val('MENU00040');
	//$("#listForm input[name=system_seq").val(system_seq);
	$("#listForm").submit();
}

function goAllLogInqList3(privacyType) {
	$("#listForm").attr("action",dashboardConfig["listUrl"]);
	//$("#listForm input[name=search_from]").val(proc_date);
	//$("#listForm input[name=search_to]").val(proc_date);
	$("#listForm input[name=privacyType]").val(privacyType);
	$("#listForm input[name=main_menu_id]").val('MENU00040');
	$("#listForm input[name=emp_user_id]").val('');
	$("#listForm input[name=emp_user_name]").val('');
	$("#listForm input[name=dept_name]").val('');
	$("#listForm input[name=user_ip]").val('');
	//$("#listForm input[name=system_seq").val(system_seq);
	$("#listForm").submit();
}

function goExtrtCondByInqList(scen_seq) {
	$("#listForm").attr("action",dashboardConfig["extrtCondbyInqListUrl"]);
	$("#listForm input[name=scen_seq]").val(scen_seq);
	$("#listForm input[name=emp_user_id]").val('');
	$("#listForm input[name=emp_user_name]").val('');
	$("#listForm input[name=dept_name]").val('');
	$("#listForm input[name=user_ip]").val('');
	$("#listForm").submit();
}

function setCheckExtrtDetailLog(occr_dt,emp_user_id,emp_detail_seq,rule_cd) {
	$.ajax({
		type: 'POST',
		url: rootPath + '/extrtCondbyInq/setCheckExtrtDetail.html',
		data: { 
			"detailOccrDt" : occr_dt,
			"detailEmpDetailSeq" : emp_detail_seq
		},
		success: function(data) {
			fnExtrtDetailLog(occr_dt,emp_user_id,emp_detail_seq,rule_cd);
		}
	});
}

function fnExtrtDetailLog(occr_dt,emp_user_id,emp_detail_seq,rule_cd){
	$("input[name=detailOccrDt]").attr("value",occr_dt);
	$("input[name=detailEmpCd]").attr("value",emp_user_id);
	$("input[name=detailEmpDetailSeq]").attr("value",emp_detail_seq);
	
	$("#listForm").attr("action",dashboardConfig["extrtCondbyInqDetailUrl"]);
	$("#listForm").submit();
}

function removeDash(day) {
	var days = day.split('-');
	day = days[0]+days[1]+days[2];
	return day;
}

/*function dialog() {

    var dialogBox = $('.dialog'),
        dialogTrigger = $('.dialog__trigger'),
        dialogClose = $('.dialog__close'),
        dialogTitle = $('.dialog__title'),
        dialogContent = $('.dialog__content'),
        dialogAction = $('.dialog__action');

    // Open the dialog
    dialogTrigger.on('click', function(e) {
        dialogBox.toggleClass('dialog--active');
        e.stopPropagation()
    });

    // Close the dialog - click close button
    dialogClose.on('click', function() {
        dialogBox.removeClass('dialog--active');
    });

    // Close the dialog - press escape key // key#27
    $(document).keyup(function(e) {
        if (e.keyCode === 27) {
            dialogBox.removeClass('dialog--active');
        }
    });

    // Close dialog - click outside
    $(document).on("click", function(e) {
        if ($(e.target).is(dialogBox) === false &&
            $(e.target).is(dialogTitle) === false &&
            $(e.target).is(dialogContent) === false &&
            $(e.target).is(dialogAction) === false) {
            dialogBox.removeClass("dialog--active");
        }
    });

};

// Run function when the document has loaded
$(function() {
    dialog();
});*/


