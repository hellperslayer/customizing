

var ge;
var lookat;
var intervalA;
//var placemark;

google.load("earth", "1", {
	"other_params" : "sensor=false"
});

function init() {
	google.earth.createInstance('map3d', initCB, failureCB);
}

var placeArr = [ ['강남구청', 37.517287, 127.047382]
                ,['논현1동', 37.511486, 127.028521]
				,['논현2동', 37.517291, 127.037187]
				,['삼성1동', 37.514319, 127.062547]
				,['삼성2동', 37.511166, 127.045955]
				,['대치1동', 37.493240, 127.056770]
				,['대치2동', 37.502294, 127.064157]
				,['대치4동', 37.499682, 127.057880] 
				,['역삼 1동', 37.495377, 127.033217]
				,['역삼 2동', 37.495989, 127.046767] 
				,['도곡 1동', 37.488268, 127.038936]
				,['도곡 2동', 37.483712, 127.046437]
				,['개포 1동', 37.481936, 127.057464]
				,['개포 2동', 37.489711, 127.068544]
				,['개포 4동', 37.478796, 127.051630]
				,['일원본동', 37.483345, 127.086446]
				,['일원1동', 37.491832, 127.088013]
				,['일원2동', 37.492083, 127.073674]
				,['청담동', 37.525041, 127.049295]
				,['신사동', 37.485379, 126.918087]
				,['압구정동', 37.530605, 127.030636]
				,['세곡동', 37.469014, 127.106891]
				,['수서동', 37.488854, 127.104984]
];
//var placeArr = [ ['사상구청', 35.152628, 128.991176],
//                 ['삼락동 주민센터', 35.176970, 128.977762],
//                 ['모라1동 주민센터', 35.191108, 128.992343],
//                 ['모라3동 주민센터', 35.184637, 128.996009],
//                 ['덕포1동 주민센터', 35.170360, 128.983261],
//                 ['덕포2동 주민센터', 35.174471, 128.982983],
//                 ['사상보건소', 35.152418, 128.992038],
//                 ['사상도서관', 35.178109, 128.989788],
//                 ['괘법동 주민센터', 35.163777, 128.987205],
//                 ['감전동 주민센터', 35.154279, 128.979429],
//                 ['주례1동 주민센터', 35.151696, 128.998010],
//                 ['주례2동 주민센터', 35.150058, 129.010509],
//                 ['주례3동 주민센터', 35.147364, 129.001453],
//                 ['학장동 주민센터', 35.144141, 128.987289],
//                 ['엄궁동 주민센터', 35.128698, 128.972207]];
//var placeArr = [ [ '영동군청', 36.17529, 127.783452 ],
//                 [ '용산면사무소', 36.259682, 127.829478 ],
//                 [ '심천면사무소', 36.238182, 127.721379 ],
//                 [ '양산면사무소', 36.122277, 127.669052 ],
//                 [ '학산면사무소', 36.099391, 127.684886 ],
//                 [ '용화면사무소', 36.020820, 127.767184 ],
//                 [ '매곡면사무소', 36.190967, 127.932137 ],
//                 [ '상촌면사무소', 36.148695, 127.914445 ],
//                 [ '황간면사무소', 36.228437, 127.910444 ],
//                 [ '양강면사무소', 36.152470, 127.751017 ],
//                 [ '추풍령면사무소', 36.214497, 127.998685 ] ];

var placemarks = new Array();
function initCB(instance) {
	ge = instance;
	ge.getWindow().setVisibility(true);

	
	for( var index = 0; index < placeArr.length ; index++ ){
		var tmpInfo = placeArr[index];
		placemarks[index] = addPlacemark(tmpInfo[0], tmpInfo[1], tmpInfo[2]);
	}
	
	var sla = placemarks[0].getGeometry().getLatitude();
	var slo = placemarks[0].getGeometry().getLongitude();
	
	lookat = ge.createLookAt('');
	lookat.set(sla, slo, 1000, ge.ALTITUDE_RELATIVE_TO_GROUND, 0, 0, 0);
	ge.getView().setAbstractView(lookat);

	createPolygon();
	
	showLine();
	setTimeout('showBallon()',2000);
	showBallons();
}

function goLookat(toLat, toLon, fromLat, fromLon){
	goLookatStep1(toLat, toLon, fromLat, fromLon);
}

function myLookat(toLat, toLon, fromLat, fromLon, height, tilt){
	lookat.set(toLat, toLon, 0, ge.ALTITUDE_RELATIVE_TO_GROUND, 0, tilt, height);
	ge.getView().setAbstractView(lookat);
}

var lowHeight = 500;
var highHeight = 20000;

function goLookatStep1(toLat, toLon, fromLat, fromLon){
	destroyChart();
	ge.setBalloon(null);
	myLookat(fromLat, fromLon, fromLat, fromLon, highHeight, 0);
	setTimeout('goLookatStep2('+toLat+','+toLon+','+fromLat+','+fromLon+')', 1000);
}

function goLookatStep2(toLat, toLon, fromLat, fromLon){
	
	myLookat(toLat, toLon, fromLat, fromLon, highHeight, 25);
	setTimeout('goLookatStep3('+toLat+','+toLon+','+fromLat+','+fromLon+')', 1000);
}

function goLookatStep3(toLat, toLon, fromLat, fromLon){
	myLookat(toLat, toLon, toLat, toLon, lowHeight, 60);
	setTimeout('addBallon()',1000);
}

function destroyChart(){
	if( $('#container').highcharts() != null ){
		$('#container').highcharts().destroy();
	}
}

var pm1;
function addBallon(){
	destroyChart();
	{
		var balloon = ge.createHtmlStringBalloon('');
		
		
		if( curBallonNum == 0 && pm1 == undefined ){
			pm1 = placemarks[0]
		}
		else if( curBallonNum == 0 && pm1 != undefined ){
			pm1 = placemarks[placemarks.length-1];
		}
		else {
			pm1 = placemarks[curBallonNum-1];
		}
		
		balloon.setFeature(pm1);
		balloon.setMaxWidth(610);
		
		var content = pm1.getDescription();

		var num1 = Math.floor(Math.random() * 100);
		var num2 = Math.floor(Math.random() * 100);
		var num3 = Math.floor(Math.random() * 100);
		var num4 = Math.floor(Math.random() * 100);
		var num5 = Math.floor(Math.random() * 100);
		var num6 = Math.floor(Math.random() * 100);
		var num7 = Math.floor(Math.random() * 100);
		var num8 = Math.floor(Math.random() * 100);
		var num9 = Math.floor(Math.random() * 100);
		var numTotal = num1+num2+num3+num4+num5+num6+num7+num8+num9;
		
		balloon.setContentString('<table border=0 cellspacing=1 cellpadding=1><tr><td colspan="2">&nbsp;</td></tr>'
				+'<tr><td>&nbsp;&nbsp;<font size=5><b>'+content+'</b></font></td><td align="right">'+numTotal+'건&nbsp;&nbsp;</td></tr><tr><td colspan="2">'
				+'<div id="container" style="min-width: 560px; height: 300px; margin: 0 auto;"></div>'
				+'</td></tr></table>');
		
		ge.setBalloon(balloon);
	}
	
	setTimeout('drawChart('+num1+','+num2+','+num3+','+num4+','+num5+','+num6+','+num7+','+num8+','+num9+')', 100);
}

function failureCB(errorCode) {
}

function goHome() {
	// Update the view in Google Earth.
	clearBallons();
	lookat.set(36.167531, 127.816907, 20, ge.ALTITUDE_RELATIVE_TO_GROUND,
			0, 0, 36000);
	ge.getView().setAbstractView(lookat);
}

function addPlacemark(name, latitude, longtitude) {
	var placemark = ge.createPlacemark('');
	
	var icon = ge.createIcon('');
	//icon.setHref('http://maps.google.com/mapfiles/kml/paddle/red-circle.png');
	icon.setHref(hosturl + 'resources/image/psm/dashboard/sasanggu.png');
//	icon.setHref('http://172.16.0.86:8080/GoogleEarth-1/img/sasanggu.png');
	var style = ge.createStyle(''); //create a new style
	style.getIconStyle().setIcon(icon); //apply the icon to the style
	placemark.setStyleSelector(style); //apply the style to the placemark

	
	placemark.setDescription(name);
	placemark.setName(name);
	var point = ge.createPoint('');
	point.setLatLng(latitude, longtitude);
	point.setAltitudeMode(ge.ALTITUDE_CLAMP_TO_GROUND);
	placemark.setGeometry(point);
	ge.getFeatures().appendChild(placemark);

	return placemark;
}

var curBallonNum = 0;
var nowPlacemark;
function showBallon() {
	if( nowPlacemark == undefined || nowPlacemark == null ){
		nowPlacemark = placemarks[0];
		addBallon();
	}
	else {
		goLookat(
				placemarks[curBallonNum].getGeometry().getLatitude()
				,placemarks[curBallonNum].getGeometry().getLongitude()
				,nowPlacemark.getGeometry().getLatitude()
				,nowPlacemark.getGeometry().getLongitude()
		);
	}
	
	nowPlacemark = placemarks[curBallonNum];
	curBallonNum++;
	if (curBallonNum >= placemarks.length) {
		curBallonNum = 0;
	}
}

function showBallons() {
	intervalA = setInterval('showBallon()', 12000);
}

function clearBallons() {
	clearInterval(intervalA);
}

google.setOnLoadCallback(init);

function addToLineString(lineString, lat, lng, latOffset, lngOffset) {
	var altitude = 3.0; // give it some altitude
	lineString.getCoordinates().pushLatLngAlt(lat + latOffset, lng + lngOffset,
			altitude);
}

function addToLineString2(lineString, lat, lng) {
	var altitude = 3.0; // give it some altitude
	lineString.getCoordinates().pushLatLngAlt(lat, lng, altitude);
}

var colorSet = ['0000ff'
                ,'ffff00'
                //,'ff00ff'
                ,'00ff00'
                ,'ff0000'
                ,'00ffff'
                ];

function addToLineString3(lat1, lng1, lat2, lng2) {
	var lookAt = ge.getView().copyAsLookAt(ge.ALTITUDE_RELATIVE_TO_GROUND);
	var lat = lookAt.getLatitude();
	var lng = lookAt.getLongitude();

	// create the line string placemark
	var lineStringPlacemark = ge.createPlacemark('');

	// create the line string geometry
	var lineString = ge.createLineString('');
	lineStringPlacemark.setGeometry(lineString);

	// tessellate (i.e. conform to ground elevation)
	lineString.setTessellate(true);
	lineString.setAltitudeMode(ge.ALTITUDE_CLAMP_TO_GROUND);
	
	//linestyle set
	// If lineStringPlacemark doesn't already have a Style associated
	// with it, we create it now.
	if (!lineStringPlacemark.getStyleSelector()) {
		lineStringPlacemark.setStyleSelector(ge.createStyle(''));
	}

	// The Style of a Feature is retrieved as feature.getStyleSelector().
	// The Style itself contains a LineStyle, which is what we manipulate
	// to change the color and width of the line.
	var lineStyle = lineStringPlacemark.getStyleSelector().getLineStyle();
	lineStyle.setWidth(4);
	var rn = Math.floor(Math.random() * 5);
	lineStyle.getColor().set('88'+colorSet[rn]); // aabbggrr format	
	//end linestyle set
	
	var altitude = 3.0; // give it some altitude
	lineString.getCoordinates().pushLatLngAlt(lat1, lng1, altitude);
	lineString.getCoordinates().pushLatLngAlt(lat2, lng2, altitude);
	
	ge.getFeatures().appendChild(lineStringPlacemark);
}

function showLine() {
//	var lookAt = ge.getView().copyAsLookAt(ge.ALTITUDE_RELATIVE_TO_GROUND);
//	var lat = lookAt.getLatitude();
//	var lng = lookAt.getLongitude();
//
//	// create the line string placemark
//	var lineStringPlacemark = ge.createPlacemark('');
//
//	// create the line string geometry
//	var lineString = ge.createLineString('');
//	lineStringPlacemark.setGeometry(lineString);
//
//	// tessellate (i.e. conform to ground elevation)
//	lineString.setTessellate(true);
//	lineString.setAltitudeMode(ge.ALTITUDE_CLAMP_TO_GROUND);
//	
//	//linestyle set
//	// If lineStringPlacemark doesn't already have a Style associated
//	// with it, we create it now.
//	if (!lineStringPlacemark.getStyleSelector()) {
//		lineStringPlacemark.setStyleSelector(ge.createStyle(''));
//	}
//
//	// The Style of a Feature is retrieved as feature.getStyleSelector().
//	// The Style itself contains a LineStyle, which is what we manipulate
//	// to change the color and width of the line.
//	var lineStyle = lineStringPlacemark.getStyleSelector().getLineStyle();
//	lineStyle.setWidth(7);
//	lineStyle.getColor().set('660000ff'); // aabbggrr format	
//	//end linestyle set
//
	var sla = placemarks[0].getGeometry().getLatitude();
	var slo = placemarks[0].getGeometry().getLongitude();

	for( var index = 0; index < placeArr.length ; index++ ){
		var tmpInfo = placemarks[index];
		//placemarks[index] = addPlacemark(tmpInfo[0],tmpInfo[1],tmpInfo[2]);
//		addToLineString2(lineString, placemarks[index].getGeometry()
//				.getLatitude(), placemarks[index].getGeometry()
//				.getLongitude());
//		addToLineString2(lineString, sla, slo);
		addToLineString3(sla, slo, placemarks[index].getGeometry().getLatitude(), placemarks[index].getGeometry().getLongitude());
	}
//	ge.getFeatures().appendChild(lineStringPlacemark);
}

function sleep(delay) {
	var start = new Date().getTime();
	while (new Date().getTime() < start + delay);
}


function finished(object) {
	  if (!object) {
	    // wrap alerts in API callbacks and event handlers
	    // in a setTimeout to prevent deadlock in some browsers
	    setTimeout(function() {
	      alert('Bad or null KML.');
	    }, 0);
	    return;
	  }
	  ge.getFeatures().appendChild(object);
	}
function loadKml(){
	var url = hosturl + '/kml/style1.kml';
	google.earth.fetchKml(ge, url, function(kmlObject) {
	      if (kmlObject)
	         ge.getFeatures().appendChild(kmlObject);
	});
}

function drawChart(num1,num2,num3,num4,num5,num6,num7,num8,num9) {
    // Create the chart
	$('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: '개인정보 유형별 탐지건수'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [
                '주민(외국인)번호',
                '운전면허번호',
                '여권번호',
                '신용카드번호',
                '금융계좌번호',
                '휴대전화번호',
                '일반전화번호',
                '이메일주소',
                '건강보험번호'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: '탐지'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} 건</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: '건수',
            data: [num1,num2,num3,num4,num5,num6,num7,num8,num9]
        }]
    });
}

function createPolygon(){
	var polygonPlacemark = ge.createPlacemark('');

	// Create the polygon.
	var polygon = ge.createPolygon('');
	polygon.setAltitudeMode(ge.ALTITUDE_RELATIVE_TO_GROUND);
	polygonPlacemark.setGeometry(polygon);
	
	
	// If polygonPlacemark doesn't already have a Style associated
	// with it, we create it now.
	if (!polygonPlacemark.getStyleSelector()) {
	    polygonPlacemark.setStyleSelector(ge.createStyle(''));
	}

	// The Style of a Feature is retrieved as
	// feature.getStyleSelector().  The Style itself contains a
	// LineStyle and a PolyStyle, which are what we manipulate to change
	// the line color, line width, and inner color of the polygon.
	var lineStyle = polygonPlacemark.getStyleSelector().getLineStyle();
	lineStyle.setWidth(5);
	// Color is specified in 'aabbggrr' format.
	lineStyle.getColor().set('44ffff00');

	// Color can also be specified by individual color components.
	var polyColor = polygonPlacemark.getStyleSelector().getPolyStyle().getColor();
	polyColor.setA(100);
	polyColor.setB(0);
	polyColor.setG(255);
	polyColor.setR(255);

	// Add points for the outer shape.
	var outer = ge.createLinearRing('');
	outer.setAltitudeMode(ge.ALTITUDE_RELATIVE_TO_GROUND);
	outer.getCoordinates().pushLatLngAlt(35.1966657956,128.9882460518,700);
	outer.getCoordinates().pushLatLngAlt(35.1880044996,129.0213558357,700);
	outer.getCoordinates().pushLatLngAlt(35.1811693655,129.0238501809,700);
	outer.getCoordinates().pushLatLngAlt(35.1732027423,129.0099378334,700);
	outer.getCoordinates().pushLatLngAlt(35.1424886049,129.0163601654,700);
	outer.getCoordinates().pushLatLngAlt(35.138551214,129.012502835,700);
	outer.getCoordinates().pushLatLngAlt(35.1397613644,129.0047277072,700);
	outer.getCoordinates().pushLatLngAlt(35.1227943807,128.9981779251,700);
	outer.getCoordinates().pushLatLngAlt(35.1251742573,128.9878975781,700);
	outer.getCoordinates().pushLatLngAlt(35.1170437626,128.9765104432,700);
	outer.getCoordinates().pushLatLngAlt(35.1205241323,128.9601140234,700);
	outer.getCoordinates().pushLatLngAlt(35.1275531161,128.9570848736,700);
	outer.getCoordinates().pushLatLngAlt(35.1396883447,128.9616252285,700);
	outer.getCoordinates().pushLatLngAlt(35.1822343394,128.9622215967,700);
	outer.getCoordinates().pushLatLngAlt(35.1967544721,128.9822716589,700);
	outer.getCoordinates().pushLatLngAlt(35.1966657956,128.9882460518,700);
	polygon.setOuterBoundary(outer);

	// Add inner points.
//	var inner = ge.createLinearRing('');
//	inner.setAltitudeMode(ge.ALTITUDE_RELATIVE_TO_GROUND);
//	inner.getCoordinates().pushLatLngAlt(35.1966657956,128.9882460518,700);
//	inner.getCoordinates().pushLatLngAlt(35.1880044996,129.0213558357,700);
//	inner.getCoordinates().pushLatLngAlt(35.1811693655,129.0238501809,700);
//	inner.getCoordinates().pushLatLngAlt(35.1732027423,129.0099378334,700);
//	inner.getCoordinates().pushLatLngAlt(35.1424886049,129.0163601654,700);
//	inner.getCoordinates().pushLatLngAlt(35.138551214,129.012502835,700);
//	inner.getCoordinates().pushLatLngAlt(35.1397613644,129.0047277072,700);
//	inner.getCoordinates().pushLatLngAlt(35.1227943807,128.9981779251,700);
//	inner.getCoordinates().pushLatLngAlt(35.1251742573,128.9878975781,700);
//	inner.getCoordinates().pushLatLngAlt(35.1170437626,128.9765104432,700);
//	inner.getCoordinates().pushLatLngAlt(35.1205241323,128.9601140234,700);
//	inner.getCoordinates().pushLatLngAlt(35.1275531161,128.9570848736,700);
//	inner.getCoordinates().pushLatLngAlt(35.1396883447,128.9616252285,700);
//	inner.getCoordinates().pushLatLngAlt(35.1822343394,128.9622215967,700);
//	inner.getCoordinates().pushLatLngAlt(35.1967544721,128.9822716589,700);
//	inner.getCoordinates().pushLatLngAlt(35.1966657956,128.9882460518,700);
//	polygon.getInnerBoundaries().appendChild(inner);

	//Create a style and set width and color of line
//	polygonPlacemark.setStyleSelector(ge.createStyle(''));
//	var lineStyle = polygonPlacemark.getStyleSelector().getLineStyle();
//	lineStyle.setWidth(5);
//	lineStyle.getColor().set('9900ffff');

	// Add the placemark to Earth.
	ge.getFeatures().appendChild(polygonPlacemark);
	
	
}