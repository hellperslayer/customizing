/**
 * Dashboard Javascript
 * @author yjyoo
 * @since 2015. 05. 04
 */


function sleep(msecs) {
    var start = new Date().getTime();
    var cur = start;
    while (cur - start < msecs) { 
        cur = new Date().getTime();
    }
}

$(document).ready(function() {

	// 시스템 리스트 height 계산
	var li_list = $('.chart_nav ul').find('li').get();	// li 리스트 가져오기
	var li_cnt = li_list.length;						// li 갯수
	var ul_height = $('.chart_nav ul').height();		// ul height
	var li_height = li_height = ul_height / li_cnt;		// 각 li별 height 계산
	
	$('.chart_nav ul li').each(function() {
		$(this).css('height', li_height + 'px');
		$(this).find('a').css('padding-top', ((li_height/3)+(10-li_cnt)) + 'px');
	});
	
	initDash();
	var dstype = $("#dstype").val();
	if(dstype == 'type1'){
		var system = $("#select_system_seq").val();
		recentLoginfo(system);
	}
	
	
		// 리플래시 1분 주기
	real = setInterval(function run() {
			// 선택된 대상시스템 표시
	var system_seq = $("#select_system_seq").val();
	selectSystem(system_seq);
	dashFunc("noReal");
			
	}, 60000);
	
	// 리플래시 1분 주기
	real = setInterval(function run() {
		// 선택된 대상시스템 표시
		var system_seq = $("#select_system_seq").val();
		selectSystem(system_seq);
		
		dashFunc("noReal");
		
	}, 60000);
	
});


// 대시보드 초기화면 설정
function initDash() {
	$("#select_system_seq").val();
	
	dashFunc(null);
}

// 선택된 대상 시스템 표시
function selectSystem(system_seq) {
	if(system_seq == 'all') {
		$('#dash_menu').find('li[class*=nav_all]').attr('class', 'nav_all_selected nav_all');
		$('#dash_menu').find('li[class*=nav_sys]').attr('class', 'nav_sys_noSelected nav_sys');
	}
	else {
		$('#dash_menu').find('li[class*=nav_all]').attr('class', 'nav_all_noSelected nav_all');
		$('#dash_menu').find('li[class*=nav_sys]').attr('class', 'nav_sys_noSelected nav_sys');
		$('#dash_menu').find('li[id=' + system_seq + ']').attr('class', 'nav_sys_selected nav_sys');
	}
}

function recentLoginfo(system_seq){

	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_recent.html',
		data: {
			  system_seq : system_seq
		},
		success: function(data) {
		 $("#bizTbl").find('tbody').find('tr').remove();
		
		 for( var i=0; i<data.dashboard_recent.length; ++i ) {
		 $("<TR style='background: #f8f8f8;'>").appendTo($("#bizTbl").find('tbody'))
			.append($("<TD>")
				.text(strToDateTime(check_null(data.dashboard_recent[i].proc_date)))
			)
			.append($("<TD>")
					.text(check_null(data.dashboard_recent[i].dept_name))
			)
			.append($("<TD>")
					.text(check_null(data.dashboard_recent[i].emp_user_id))
			)
			.append($("<TD>")
					.text(check_null(data.dashboard_recent[i].emp_user_name))
			)
			.append($("<TD>")
					.text(check_null(data.dashboard_recent[i].user_ip))
			)
			.append($("<TD>")
					.text(check_null(data.dashboard_recent[i].system_name))
			)
		.fadeOut()
		.fadeIn("slow");
			
		}
		}
	});
}

// 대시보드 그리기
function dashFunc(option) {
	// 선택한 대상 시스템
	var select_system_seq = $("#select_system_seq").val();
	var dstype = $("#dstype").val();
	// chart1, 2 리로딩 X
	if("type0" == dstype){
		if(option != "noReal") {
			getChart1(select_system_seq);
		}
	}
	
	if("type1" == dstype){
		if(option != "noReal") {
			getChart1(select_system_seq);
		}
	}
	if("type2" == dstype){
	getChart3(select_system_seq);
	}
	if("type3" == dstype){
	getChart4(select_system_seq);
	}
	if("type4" == dstype){
	getChart5(select_system_seq);
	}
	if("type5" == dstype){
	getChart6(select_system_seq);
	}
	if("type6" == dstype){
	getChart7(select_system_seq);
	}
	if("type7" == dstype){
	getChart8();
	}
}

// 대시보드 1, 2 - 최근 로그 수집 현황, TOP 10
function getChart1(system_seq) {
	drawChart1(system_seq);
}

// 대시보드 3 - 시스템별 로그 수집 현황 데이터 가져오기
function getChart3(system_seq) {
	var pop_from = $("input[name=pop_from]").val();
	var pop_to = $("input[name=pop_to]").val();
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chart3.html',
		data: { 
			system_seq : system_seq,
			pop_from : pop_from,
			pop_to : pop_to
		},
		success: function(data) {
			drawChart3(data.dashboard_chart3);
		}
	});
}

// 대시보드 4 - 개인정보 유형별 로그 수집 현황
function getChart4(system_seq) {
	var pop_from = $("input[name=pop_from]").val();
	var pop_to = $("input[name=pop_to]").val();
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chart4.html',
		data: { 
			system_seq : system_seq,
			pop_from : pop_from,
			pop_to : pop_to
		},
		success: function(data) {
			drawChart4(data.dashboard_chart4);
		}
	});
}

// 대시보드 5 - 추출 로그 추이 분석 현황
function getChart5(system_seq) {
	var pop_from = $("input[name=pop_from]").val();
	var pop_to = $("input[name=pop_to]").val();
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chart5.html',
		data: { 
			system_seq : system_seq,
			pop_from : pop_from,
			pop_to : pop_to
		},
		success: function(data) {
			drawChart5(data.dashboard_chart5);
		}
	});
}

// 대시보드 6 - 추출조건별 추출로그 현황
function getChart6(system_seq) {
	var pop_from = $("input[name=pop_from]").val();
	var pop_to = $("input[name=pop_to]").val();
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chart6.html',
		data: { 
			system_seq : system_seq,
			pop_from : pop_from,
			pop_to : pop_to
		},
		success: function(data) {
			drawChart6(data.dashboard_chart6);
		}
	});
}

// 대시보드 7 - 로그 수집 TOP10 부서 현황
function getChart7(system_seq) {
	var pop_from = $("input[name=pop_from]").val();
	var pop_to = $("input[name=pop_to]").val();
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chart7.html',
		data: { 
			system_seq : system_seq,
			pop_from : pop_from,
			pop_to : pop_to
		},
		success: function(data) {
			drawChart7(data.dashboard_chart7);
		}
	});
}

// 대시보드 8 - 수집 서버 현황
function getChart8() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chart8.html',
		data: { 
		},
		success: function(data) {
			drawChart8(data.dashboard_chart8);
		}
	});
}

// 대시보드 1 - 최근 전체 로그 건수 > 전체 수집 로그 건수, 개인정보 처리 로그 건수
function drawChart1(system_seq) {
	Highcharts.setOptions({
		global: {
			useUTC: false
		}
	});
	
	var chart;
	
	var bizTbl = $("#bizTbl");
	var maxTrCnt = 9;
	
	$('#chart1').highcharts({
		chart: {
			renderTo: 'container',
			plotShadow: false,
			type: 'spline',
			animation: Highcharts.svg, // don't animate in old IE
			marginRight: 10,
			events: {
				load: function() {
					// set up the updating of the chart each second
					var series = this.series[0];
					
					t = setInterval(function() {
						var x = (new Date()).getTime() - 300000, // current time (1000 * 60)
						chartDate = new Date(x);
						
						$.ajax({
							type: 'POST',
							url: rootPath + '/dashboard/dashboard_chart2.html',
							data: { 
								proc_hour : chartDate.getHours()
								,proc_min : chartDate.getMinutes()-7 
								,proc_sec : chartDate.getSeconds()
								,system_seq : system_seq
							},
							success: function(data) {
								if(data.dashboard_chart2 != null) {
									y = data.cnt;
									series.addPoint([x, y], true, true);
									
									drawChart2(data.dashboard_chart2);
								}
							}
						});
					}, 1000);
				}
			},
			borderRadius: 0
		},
		title: {
			text: false
		},
		xAxis: {
			type: 'datetime',
			tickPixelInterval: 100
		},
		yAxis: {
			min: 0,
			maxPadding: 0.5,
			title: {
				text: false
			},
			plotLines: [{
				value: 0,
				width: 1,
				color: '#808080'
			}],
			type: "number"
		},
		tooltip: {
			formatter: function() {
				return '<b>'+ this.series.name +'</b><br/>'+
						Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) +'<br/>'+
                        Highcharts.numberFormat(this.y, 0) + " 건";
			}
		},
		legend: {
			enabled: false
		},
		exporting: {
			enabled: false
		},
		series: [{
			name: "로그 수",
			data: (
				function() {
					// generate an array of random data
					var data = [],
					time = (new Date()).getTime() - 300000,
					i;
                    for (i = -19 ; i <= 0; i++) {
                    	data.push({
                    		x: time + i * 1000,
                    		y: 0
                    	});
                    }
                    return data;
                }
			)()
		}]
	});
	
	// 최근 수집로그 TOP10 테이블 그리기 (리얼 테이블 추가 로직)
	function drawChart2(obj) {
		objCnt = obj.length;
        	
		$.each(obj, function( i, item ) {
			
			if(item.dept_name==""||item.dept_name==null){
				item.dept_name = "부서명 없음";
			}
			
			if(i >= maxTrCnt){
				return;
			}
			
			bizTrCnt = $("#bizTbl").find('tbody').find('tr').length;
			var limit = bizTrCnt + objCnt;
			if(bizTrCnt >= maxTrCnt) {
				$("#bizTbl").find('tbody').find('tr').eq(0).remove();
			}
			
		//	var tmp = bizTrCnt % 2 == 0 ? "#ffffff" : "#f8f8f8";
			$("<TR style='background: #f8f8f8';>").appendTo($("#bizTbl").find('tbody'))
					.append($("<TD>")
						.text(strToDateTime(check_null(item.proc_date)))
    				)
    				.append($("<TD>")
   						.text(check_null(item.dept_name))
    				)
    				.append($("<TD>")
   						.text(check_null(item.emp_user_id))
    				)
    				.append($("<TD>")
   						.text(check_null(item.emp_user_name))
    				)
    				.append($("<TD>")
   						.text(check_null(item.user_ip))
    				)
    				.append($("<TD>")
   						.text(check_null(item.system_name))
    				)
    			//.fadeOut()
    			.fadeIn("slow");
    	});
    }
}

// 대시보드 3 - 시스템별 로그 수집 현황 그리기
function drawChart3(chart3) {
	if(chart3 != null) {
		var dataSource = new Array();
		var series = new Array();
		
		$.each(chart3, function(i, item) {
			if(i == 0) {
				aa = { system : check_null(item.data1), cnt0 : item.cnt1 };
				bb = { valueField : "cnt0", name : check_null(item.data2) };
			}
			else if(i == 1) {
				aa = { system : check_null(item.data1), cnt1 : item.cnt1 };
				bb = { valueField : "cnt1", name : check_null(item.data2) };
			}
			else if(i == 2) {
				aa = { system : check_null(item.data1), cnt2 : item.cnt1 };
				bb = { valueField : "cnt2", name : check_null(item.data2) };
			}
			else if(i == 3) {
				aa = { system : check_null(item.data1), cnt3 : item.cnt1 };
				bb = { valueField : "cnt3", name : check_null(item.data2) };
			}
			else if(i == 4) {
				aa = { system : check_null(item.data1), cnt4 : item.cnt1 };
				bb = { valueField : "cnt4", name : check_null(item.data2) };
			}
			else if(i == 5) {
				aa = { system : check_null(item.data1), cnt5 : item.cnt1 };
				bb = { valueField : "cnt5", name : check_null(item.data2) };
			}
			else if(i == 6) {
				aa = { system : check_null(item.data1), cnt6 : item.cnt1 };
				bb = { valueField : "cnt6", name : check_null(item.data2) };
			}
			else if(i == 7) {
				aa = { system : check_null(item.data1), cnt7 : item.cnt1 };
				bb = { valueField : "cnt7", name : check_null(item.data2) };
			}
			else if(i == 8) {
				aa = { system : check_null(item.data1), cnt8 : item.cnt1 };
				bb = { valueField : "cnt8", name : check_null(item.data2) };
			}
			else if(i == 9) {
				aa = { system : check_null(item.data1), cnt9 : item.cnt1 };
				bb = { valueField : "cnt9", name : check_null(item.data2) };
			}
			else if(i == 10) {
				aa = { system : check_null(item.data1), cnt10 : item.cnt1 };
				bb = { valueField : "cnt10", name : check_null(item.data2) };
			}
			else if(i == 11) {
				aa = { system : check_null(item.data1), cnt11 : item.cnt1 };
				bb = { valueField : "cnt11", name : check_null(item.data2) };
			}
			else if(i == 12) {
				aa = { system : check_null(item.data1), cnt12 : item.cnt1 };
				bb = { valueField : "cnt12", name : check_null(item.data2) };
			}
			else if(i == 13) {
				aa = { system : check_null(item.data1), cnt13 : item.cnt1 };
				bb = { valueField : "cnt13", name : check_null(item.data2) };
			}
			else if(i == 14) {
				aa = { system : check_null(item.data1), cnt14 : item.cnt1 };
				bb = { valueField : "cnt14", name : check_null(item.data2) };
			}
			
			dataSource.push(aa);
			series.push(bb);
		});
		
		var chart3 = $("#chart3").dxChart({
			equalBarWidth : false,
			dataSource : dataSource,
			rotated : true,
			commonSeriesSettings : {
				argumentField : "system",
				type : "bar",
				label : {
					visible : true,
					format : "fixedPoint",
					precision : 0,
					backgroundColor : 'none',
					font : {
						color : 'black',
						weight : '600'
					}
				}
			},
			series : series,
			palette : ['#6fc0ff', '#31a0ff', '#0d7ad8', '#2433ff', '#6724ff', '#8440c9', '#b956de', '#f57cf4', '#f57ca4', '#ffa488'],
			//palette : 'Soft',
			title : false,
			legend : {
				visible : false,    
			},
			tooltip : {
				enabled : true,
				shared : true,
				font : { 
					color: 'black',
					size : 13
				},
				customizeText : function (point) {
					return  point.argument + ": " + point.points[0].originalValue;
				}
			},
			argumentAxis: { 
				visible : true,
				label : {
					font : { color : 'black', size : 13 }
				}
			},
			valueAxis : {
				visible : true,
				label : {
					font : { color : 'black', size : 13 }
				}
			},
			pointClick : function(point) {
				var today = new Date().convertDateToString();	// 오늘 날짜
				var system_seq = point.series.name;				// 선택한 SYSTEM_SEQ
				
				var allLogInqListForm = $("#allLogInqListForm");
				allLogInqListForm.find("input[name=search_to]").val(today);
				allLogInqListForm.find("input[name=search_from]").val(today);
				allLogInqListForm.find("input[name=system_seq]").val(system_seq);
				/*allLogInqListForm.submit();*/
				
				point.isSelected() ? point.clearSelection() : point.select();
				
				
			}
		}).dxChart('instance');
	}
}

// 대시보드 4 - 개인정보 유형별 로그 수집 현황
function drawChart4(chart4) {
	if(chart4 != null) {
		var dataSource = new Array();
		var series = new Array();
		
		$.each(chart4, function(i, item) {
			aa = { privacy : check_null(item.data1), cnt : item.cnt1 };

			dataSource.push(aa);
		});
     
		var chart4 = $("#chart4").dxPieChart({
			dataSource : dataSource,
			series: [{
				/* type : "doughnut",	// 도넛 모양 */	
		        argumentField : "privacy",
		        valueField : "cnt",
		        label : {
		            visible : true,
		            connector : {
		                visible : true,
		                width : 3.0
		            },
		            font: {
		            	size:32
		            },
		            format : "fixedPoint",
		            customizeText : function (point) {
		                return point.argumentText;
		            }
		        },
		        smallValuesGrouping: {
		            mode: "smallValueThreshold",
		            threshold: 3.0
		        }
		    }],
		    size : {
		    	height : 620
		    },
		    margin : {top:100},
		    palette : ['#6fc0ff', '#31a0ff', '#0d7ad8', '#2433ff', '#6724ff', '#8440c9', '#b956de', '#f57cf4', '#f57ca4', '#ffa488'],
//			palette : 'Pastel',
			title : false,
			
			legend : {
				visible : false,
				font:{size : 14}
			},
			
			tooltip : {
				enabled : true,
				shared : true,
				font : {
					size : 19
				},
				customizeText : function (point) {
					return  point.valueText + " (" + point.percentText + ")";
				}
			},
			onPointClick : function(e) {
				var point = e.target;
				point.isVisible() ? point.hide() : point.show();
		    }
		});
	}
}

// 대시보드 5 - 추출 로그 추이 분석 현황
function drawChart5(chart5) {
	if(chart5 != null) {
		var type = "area";
		
		if(chart5.length < 2) {
			type = "bar";
		}
		
		var dataSource = new Array();
		var series = new Array();
		
		$.each(chart5, function(i, item) {
			aa = { proc_date : item.proc_date, cnt : item.cnt1 };
			
			dataSource.push(aa);
		});
		
		var chart5 = $("#chart5").dxChart({
			dataSource : dataSource,
			commonSeriesSettings: {
				argumentField: "proc_date",
				/*type: "line",*/
				type: type,
				label: {
					visible: true,
					format: "fixedPoint",
					precision: 0		      			            
				}
			},
			series: [{ valueField: "cnt", name: "추출건수" }],
			palette : ['#6fc0ff', '#31a0ff', '#0d7ad8', '#2433ff', '#6724ff', '#8440c9', '#b956de', '#f57cf4', '#f57ca4', '#ffa488'],
//			palette : 'Soft Pastel',
			title: false,
			argumentAxis: {
				visible : true,
				valueMarginsEnabled: true,
				label : {
					font : { color : 'black', size : 13 }
				}
			},
			yAxis: {
				min: 0,
				tickInterval: 500,
				type: "number"
			},
			valueAxis : {
				visible : true,
				label : {
					font : { color : 'black', size : 13 },
					format: "fixedPoint"
				}
			},
			legend: {
				visible : false
			},
			tooltip: {
				enabled: true,
				shared: true,
				font : {
					size : 15
				}
			}
		});
	}
}

// 대시보드 6 - 추출조건 별 추출로그 현황
function drawChart6(chart6) {
	if(chart6 != null) {
		var dataSource = new Array();
		var series = new Array();
		
		$.each(chart6, function(i, item) {
			if(i == 0) {
				aa = { rule : check_null(item.data1), cnt0 :  item.cnt1 };
				bb = { valueField : "cnt0", name : check_null(item.data2) };
			}
			else if(i == 1) {
				aa = { rule : check_null(item.data1), cnt1 : item.cnt1 };
				bb = { valueField : "cnt1", name : check_null(item.data2) };
			}
			else if(i == 2) {
				aa = { rule : check_null(item.data1), cnt2 : item.cnt1 };		
				bb = { valueField : "cnt2", name : check_null(item.data2) };
			}
			else if(i == 3) {
				aa = { rule : check_null(item.data1), cnt3 : item.cnt1 };
				bb = { valueField : "cnt3", name : check_null(item.data2) };
			}
			else if(i == 4) {
				aa = { rule : check_null(item.data1), cnt4 : item.cnt1 };
				bb = { valueField : "cnt4", name : check_null(item.data2) };
			}
			else if(i == 5) {
				aa = { rule : check_null(item.data1), cnt5 : item.cnt1 };
				bb = { valueField : "cnt5", name : check_null(item.data2) };
			}
			else if(i == 6) {
				aa = { rule : check_null(item.data1), cnt6 : item.cnt1 };
				bb = { valueField : "cnt6", name : check_null(item.data2) };
			}
			else if(i == 7) {
				aa = { rule : check_null(item.data1), cnt7 : item.cnt1 };
				bb = { valueField : "cnt7", name : check_null(item.data2) };
			}
			else if(i == 8) {
				aa = { rule : check_null(item.data1), cnt8 : item.cnt1 };
				bb = { valueField : "cnt8", name : check_null(item.data2) };
			}
			else if(i == 9) {
				aa = { rule : check_null(item.data1), cnt9 : item.cnt1 };
				bb = { valueField : "cnt9", name : check_null(item.data2) };
			}
			
			dataSource.push(aa);
			series.push(bb);
		});
		
		$("#chart6").dxChart({
			equalBarWidth : false,
			dataSource : dataSource,
			commonSeriesSettings : {
				argumentField : "rule",
				type : "bar",
				label : {
					visible : true,
					format : "fixedPoint",
					precision : 0,
					backgroundColor : 'none',
					font : {
						color : 'black',
						weight : '600'
					}
				}
			},
			series : series,
			palette : ['#6fc0ff', '#31a0ff', '#0d7ad8', '#2433ff', '#6724ff', '#8440c9', '#b956de', '#f57cf4', '#f57ca4', '#ffa488'],
//			palette : 'Soft',
			title : false,
			legend : {
				visible : false,    
			},
			argumentAxis: { 
				visible : true,
				label : {
					font : { color : 'black', size : 13 }
				}
			},
			valueAxis : {
				visible : true,
				label : {
					font : { color : 'black', size : 13 }
				}
			},
			tooltip : {
				enabled : true,
				shared : true,
				font : { 
					color: 'black',
					size : 13
				}
			}
		});
	}
}

// 대시보드 7 - 부서별 로그 수집 현황
function drawChart7(data) {
	$("#deptTopTbl").find('tbody').empty();
	
	var maxTrCnt = 10;
	
	if(data != null) {
		objCnt = data.length;
		
		$.each(data, function( i, item ) {
			if(i >= maxTrCnt){
				return;
			}
			
			deptTopTrCnt = $("#deptTopTbl").find('tbody').find('tr').length;
			var limit = deptTopTrCnt + objCnt;
			if(deptTopTrCnt >= maxTrCnt) {
				$("#deptTopTbl").find('tbody').find('tr').eq(0).remove();
			}
			
			//부서별 로그 수집 현황에 부서명이 없을 경우 부서명 없음으로 출력
			if(item.data1==""||item.data1==null){
				item.data1 = "부서명 없음";
			}
			
			$("<TR style='background: #f8f8f8';>").appendTo($("#deptTopTbl").find('tbody'))
			.append($("<TD>")
					.text(i+1)
			)
			.append($("<TD>")
					.text(check_null(item.data1))
			)
			.append($("<TD>")
					.text(check_null(item.cnt1.toString().addComma()))
			)
			.append($("<TD>")
					.text(check_null(item.cnt2.toString().addComma()))
			)
			.fadeOut()
			.fadeIn("slow");
		});
	}
}

// 대시보드 8 - 수집 서버 현황
function drawChart8(chart8) {
	if(chart8 != null) {
		var dataSource;
		
		$.each(chart8, function(i, item) {
			var totalMem = item.data1;
			var i = totalMem.indexOf(" ");
			totalMem = totalMem.substr(0, i);
			
			var totalHdd = item.data2;
			var j = totalHdd.indexOf(" ");
			totalHdd = totalHdd.substr(0, j);
			
			dataSource = [{
				target: "CPU",
				total: 100,
				usage: item.cnt1,
				free: parseInt(100) - parseInt(item.cnt1)
			}, {
				target: "MEMORY",
				total: totalMem,
				usage: item.cnt2,
				free: parseInt(100) - parseInt(item.cnt2)
			}, {
				target: "HDD",
				total: totalHdd,
				usage: item.cnt3,
				free: parseInt(100) - parseInt(item.cnt3)
			}];
		});
		
		$("#chart8").dxChart({
			rotated: true,
		    pointSelectionMode: "multiple",
		    dataSource: dataSource,
		    commonSeriesSettings: {
		        argumentField: "target",
		        /*type: "stackedbar",*/
		        type: "fullstackedbar",
		        selectionStyle: {
		            hatching: {
		                direction: "left"
		            }
		        }
		    },
			palette : ['#6fc0ff', '#31a0ff', '#0d7ad8', '#2433ff', '#6724ff', '#8440c9', '#b956de', '#f57cf4', '#f57ca4', '#ffa488'],
			series: [
			         { valueField: "usage", name: "USAGE", color: '#f57ca4' },
			         { valueField: "free", name: "FREE", color: '#6fc0ff' }
			],
			legend: {
				verticalAlignment : 'middle',
				horizontalAlignment : 'center',
				font : { color : 'black', size : 13 }
			},
			argumentAxis: { 
				visible : true,
				label : {
					font : { color : 'black', size : 13 }
				}
			},
			valueAxis : {
				visible : true,
				label : {
					font : { color : 'black', size : 13 }
				}
			},
			tooltip : {
				enabled : true,
				font : { color: 'black', size : 13 }
			}
		});
	}
}

