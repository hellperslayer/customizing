function updateCoord(coord_id, index){
	var coord_date =$("input[name=coord_date"+index + "]").val();
	var coord_name =$("input[name=coord_name"+index + "]").val();
	var coordinate =$("input[name=coordinate"+index + "]").val();
	var coord_memo =$("input[name=coord_memo"+index + "]").val();
	var coord_type =$("input[name=coord_type"+index + "]").val();
	
	$("input[name=coord_date]").val(coord_date);
	$("input[name=coord_name]").val(coord_name);
	$("input[name=coordinate]").val(coordinate);
	$("input[name=coord_memo]").val(coord_memo);
	$("input[name=coord_type]").val(coord_type);
	$("input[name=coord_id]").val(coord_id);
	
	sendAjaxGoogle("save");
}

function deleteCoord(coord_id){

	$("input[name=coord_id]").val(coord_id);
	
	sendAjaxGoogle("remove");
}


function sendAjaxGoogle(type){
	var $form = $("#popListForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'auth_name';
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'save') {
		log_message_title = '수정';
		log_action = 'UPDATE';
	} else if (type == 'remove') {
		log_message_title = '삭제';
		log_action = 'REMOVE';
	}

	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(dashboardPopConfig[type + "Url"], $form.serialize(), ajaxAuth_successHandler, ajaxAuth_errorHandler, type);
}

// 권한 ajax call - 성공
function ajaxAuth_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = authDetailConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		switch(actionType){
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}

		moveList();
	}
}

// 권한 ajax call - 실패
function ajaxAuth_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){
	
	log_message += authLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("권한 등록 실패하였습니다." + textStatus);
			break;
		case "save":
			alert("권한 수정 실패하였습니다." + textStatus);
			break;
		case "remove":
			alert("권한 삭제 실패하였습니다." + textStatus);
			break;
	}
}

function moveList(){
	
	$("#popListForm").attr("action",dashboardPopConfig["urlList"]);
	$("#popListForm").submit();
}