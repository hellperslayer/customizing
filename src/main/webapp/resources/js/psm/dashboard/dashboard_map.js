/**
 * dashboard (지도가 들어간 버전)
 */

$(document).ready(function() {
	setCharts();
	
	setInterval(function(){ 
		
		
	}, 10000);
	
	
	// 새로고침
	/*setInterval(function(){ 
		window.location.reload(true);
	}, 600000);*/
});

function setCharts() {
	setChart1();
	setChart2();
	setChart3();
	setChart4();
	setChart5();
	setChart6();
}

// 개인정보 처리 위험현황
function setChart1() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_map1.html',
		data: { 
		},
		success: function(data) {
			drawchart1(data);
		}
	});
}

// MAP
function setChart2() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_map2.html',
		data: { 
		},
		success: function(data) {
			drawchart2(data);
		}
	});
}

// 조직별 위험현황 순위
function setChart3() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_map3.html',
		data: { 
		},
		success: function(data) {
			drawchart3(data);
		}
	});
}

// 위험유형 유형별 비율
function setChart4() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_map4.html',
		data: { 
		},
		success: function(data) {
			drawchart4(data);
		}
	});
}

// 월별 위험현황
function setChart5() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_map5.html',
		data: { 
		},
		success: function(data) {
			drawchart5(data);
		}
	});
}

// 개인별 위험현황 순위
function setChart6() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_map6.html',
		data: { 
		},
		success: function(data) {
			drawchart6(data);
		}
	});
}

function drawchart1(data) {

}

function drawchart2(data) {
	
}

function drawchart3(data) {
	$("#bizTbl").find('tbody').find('tr').remove();
	
	for( var i=0; i<data.length; ++i ) {
		if(i < 3)
			$("<TR style='background: #f8f8f8; color: #ff0000'>").appendTo($("#bizTbl").find('tbody'))
		else 
			$("<TR style='background: #f8f8f8;'>").appendTo($("#bizTbl").find('tbody'))
			
		.append($("<TD>")
			.text(check_null(data[i].dept_name))
		)
		.append($("<TD>")
			.text(check_null(data[i].cnt))
		)
		.fadeOut()
		.fadeIn("slow");
	}
}

function drawchart4(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for(var i=0; i<jsonData.length; i++) {
		var value = {"rule": jsonData[i].rule_nm, "cnt": jsonData[i].cnt};
		dataProvider.push(value);
	}
	var chart = AmCharts.makeChart("chart4", {
	    "type": "pie",
	    "theme": "dark",
	    "innerRadius": "40%",
	    "gradientRatio": [-0.4, -0.4, -0.4, -0.4, -0.4, -0.4, 0, 0.1, 0.2, 0.1, 0, -0.2, -0.5],
	    "dataProvider": dataProvider,
	    "balloonText": "[[value]]",
	    "valueField": "cnt",
	    "titleField": "rule",
	    "balloon": {
	        "drop": true,
	        "adjustBorderColor": false,
	        "color": "#FFFFFF",
	        "fontSize": 16
	    },
	    "export": {
	        "enabled": false
	    }
	});
}

function drawchart5(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for(var i=0; i<jsonData.length; i++) {
		var value = {"month": jsonData[i].month, "cnt": jsonData[i].cnt};
		dataProvider.push(value);
	}
	var chart = AmCharts.makeChart("chart5", {
	    "type": "serial",
	    "theme": "dark",
	    "legend": {
	        "useGraphSettings": true
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "integersOnly": true,
	        //"maximum": 10,
	        "minimum": 0,
	        "reversed": false,
	        "axisAlpha": 0,
	        "dashLength": 5,
	        "gridCount": 10,
	        "position": "left",
	        "title": "위험현황"
	    }],
	    "startDuration": 0.5,
	    "graphs": [{
	        "balloonText": "[[category]]: [[value]]",
	        "bullet": "round",
	        "hidden": true,
	        "title": "",
	        "valueField": "cnt",
			"fillAlphas": 0
	    }/*, {
	        "balloonText": "place taken by Germany in [[category]]: [[value]]",
	        "bullet": "round",
	        "title": "Germany",
	        "valueField": "germany",
			"fillAlphas": 0
	    }, {
	        "balloonText": "place taken by UK in [[category]]: [[value]]",
	        "bullet": "round",
	        "title": "United Kingdom",
	        "valueField": "uk",
			"fillAlphas": 0
	    }*/],
	    "chartCursor": {
	        "cursorAlpha": 0,
	        "zoomable": false
	    },
	    "categoryField": "month",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "fillAlpha": 0.05,
	        "fillColor": "#000000",
	        "gridAlpha": 0,
	        "position": "bottom"
	    },
	    "export": {
	    	"enabled": false,
	        "position": "bottom-right"
	     }
	});
}

function drawchart6(data) {
	$("#bizTbl2").find('tbody').find('tr').remove();
	
	for( var i=0; i<data.length; ++i ) {
		if(i < 3)
			$("<TR style='background: #f8f8f8; color: #ff0000;'>").appendTo($("#bizTbl2").find('tbody'))
		else
			$("<TR style='background: #f8f8f8;'>").appendTo($("#bizTbl2").find('tbody'))
			
		.append($("<TD>")
			.text(i+1)
		)
		.append($("<TD>")
			.text(check_null(data[i].emp_user_name))
		)
		.append($("<TD>")
			.text(check_null(data[i].dept_name))
		)
		.append($("<TD>")
			.text(check_null(data[i].cnt))
		)
		.append($("<TD>")
			.text(check_null(data[i].rrn))
		)
		.append($("<TD>")
			.text(check_null(data[i].phone))
		)
		.append($("<TD>")
			.text(check_null(data[i].email))
		)
		.fadeOut()
		.fadeIn("slow");
	}
}
