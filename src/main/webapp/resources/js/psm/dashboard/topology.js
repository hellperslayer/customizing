/**
 * Topology Javascript
 * @author yjyoo
 * @since 2015. 05. 06
 */

// 토폴로지 interval 변수
var topo;

$(document).ready(function() {
	
	// 토폴로지 탭 클릭
	$("#tab2").click(function() {
		clearInterval(real);	// 대시보드 - interval 중지
		clearInterval(t);		// 대시보드 - 실시간차트 interval 중지
		$("#dashboardPeriod").css("display", "none");
		changeTab(total, 2);
		$("#dashContent").attr("style", "display: none;");
		$("#topoContent").attr("style", "display: block;");
		$("#tabId").val("tab2");
		
		setTopo();
		
		// 일정 시간에 따른 리로딩
		refresh();
	});
	
	// 기존 선택한 탭이 tab2(토폴로지) 일 경우
	if($("#tabId").val() == "tab2") {
		changeTab(total, 2);
		$("#dashContent").attr("style", "display: none;");
		$("#topoContent").attr("style", "display: block;");
		$("#tabId").val("tab2");
		
		topoFunc();
	}
	
	// 검색조건 펼침 여부
	if($("#toggleCd").val() == "1"){
		$("#searchBar").hide();
	}
	else{
		$("#searchBar").show();
	}
	
	// 검색조건 펼침 여부
	$("#searchBarControlBt").click(function(){ 
		if($("#toggleCd").val() == "2"){
			$("#searchBar").hide("slide", { direction:"left" }, 1000);
			$("#searchBarControlBt").attr("src", rootPath + "/resources/image/psm/dashboard/dashboard_add_input.gif");
			$("#toggleCd").val("1");
		}
		else{
			$("#searchBar").show("slide", { direction:"left" }, 1000);
			$("#searchBarControlBt").attr("src", rootPath + "/resources/image/psm/dashboard/dashboard_delete_input.gif");
			$("#toggleCd").val("2");
		}
	});
	
	// 실시간 모니터링 선택 컨트롤
	$("#realtime").click(function(){
		if($(this).attr("checked")){
			$("#searchDate").find("input").attr("disabled", "disabled").attr("class", "input_cal_tp");
			$("#searchDate").find("select").attr("disabled", "disabled");
			$("#realtime").val("on");
			setTopo();

			// 일정 시간에 따른 리로딩
			refresh();
		}
		else{
			$("#searchDate").find("input").removeAttr("disabled").attr("class", "calender");
			$("#searchDate").find("select").removeAttr("disabled");
			$("#realtime").val("off");
			
			// 기존 리로딩 클리어
			clearInterval(topo);
		}
	});
	
	// 실시간 모니터링 선택 컨트롤
	if($("#realtime").attr("checked")){
		$("#searchDate").find("input").attr("disabled", "disabled").attr("class", "input_cal_tp");
		$("#searchDate").find("select").attr("disabled", "disabled");

		// 일정 시간에 따른 리로딩
		refresh();
	}
	else{
		$("#searchDate").find("input").removeAttr("disabled").attr("class", "calender");
		$("#searchDate").find("select").removeAttr("disabled");

		// 기존 리로딩 클리어
		clearInterval(topo);
	}

	// 개인정보건수 상위 N명 선택 컨트롤
	$("#isTop_n").click(function(){
		if($(this).attr("checked")){
			$("#top_n").removeAttr("disabled");
			$("#isTop_n").val("on");
		}
		else{
			$("#top_n").attr("disabled", "disabled");
			$("#isTop_n").val("off");
		}
	});

	// 개인정보건수 상위 N명 선택 컨트롤
	if($("#isTop_n").attr("checked")){
		$("#top_n").removeAttr("disabled");
	}
	else{
		$("#top_n").attr("disabled", "disabled");
	}
	
	// 토폴로지 조회 버튼 클릭
	$("#topo_search").click(function(){	
		var starth = $("#starth").val();							// 시작 시
    	var startm = $("#startm").val();							// 시작 분
    	var endh = $("#endh").val();								// 종료 시
    	var endm = $("#endm").val();								// 종료 분
    	var realtime = $("#realtime").val();
    	    		
    	if(starth == endh && startm > endm){
    		alert("시작시간이 종료시간보다 큽니다.");
    	}else if(endh-starth <0 ){
		alert("시작시간이 종료시간보다 큽니다.");
    	}
    	else{
    		drawTopo();
    	}
		
	});
	
	// DatePicker
	var dates = '';
	dates = $("input[name=start_date]").datepicker(
		{	
			maxDate:0,
			showOn: 'both',
			buttonImage: rootPath + "/resources/image/common/icon_calender.gif",
			buttonImageOnly: true,
			showAnimation: 'slide',
			showOtherMonths: true,
			selectOtherMonths: true,
			changeYear: true,
			changeMonth: true,
			onSelect: function(selectedDate) {
				var option = this.name == "search_from" ? "minDate" : "maxDate",
					instance = $(this).data("datepicker"),
					date = $.datepicker.parseDate(
							instance.settings.dateFormat || $.datepicker._defaults.dateFormat,
							selectedDate, instance.settings
					);
				dates.not(this).datepicker("option", option, date);
			}
		}
	);
	$("input[name=start_date]").attr("readonly", "readonly");
	$("img.ui-datepicker-trigger").attr("style", "display: none;");
	
	// 토폴로지 날짜 기본 셋팅
	if($("#start_date").val() == null || $("#start_date").val() == '') {
		// 오늘 날짜
		$("#start_date").datepicker("setDate", "0");
	}
});


// 토폴로지 탭 클릭 or 실시간 모니터링 on 선택시 액션
function setTopo() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/topology/init_topologyView.html',
		data: { },
		success: function() {
			changeTab(total, 2);
			$("#dashContent").attr("style", "display: none;");
			$("#topoContent").attr("style", "display: block;");
			$("#tabId").val("tab2");
			
			initTopo();
		}
	});
}

// 토폴로지 초기화면 설정
function initTopo() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/topology/init_topologySearch.html',
		data: { },
		success: function(data) {
			// 실시간 모니터링 여부
			if(data.paramBean.realtime == "on") {
				$("#realtime").attr("checked", "checked");
				$("#searchDate").find("input").attr("disabled", "disabled").attr("class", "input_cal_tp");
				$("#searchDate").find("select").attr("disabled", "disabled");
			}
			
			// 날짜
			$("#start_date").datepicker("setDate", "0");
			
			// 시간
			var starth = data.paramBean.starth;
			var startm = data.paramBean.startm;
			var endh = data.paramBean.endh;
			var endm = data.paramBean.endm;
			startm = parseInt(startm);
			/*if(startm < 20) {
				$("#starth").val(starth - 1);
				$("#startm").val(startm + 40);
			}
			else {
				$("#starth").val(starth);
				$("#startm").val(startm - 30);
			}*/
			$("#starth").val(data.paramBean.starth);
			$("#startm").val(data.paramBean.startm);
			$("#endh").val(data.paramBean.endh);
			$("#endm").val(data.paramBean.endm);
			
			// 개인정보건수 상위 N명 여부
			if(data.paramBean.isTop_n == "on") {
				$("#isTop_n").attr("checked", "checked");
				$("#top_n").removeAttr("disabled");
			}

			topoFunc();
		}
	});
}

// 토폴로지 그리기
function topoFunc() {
	$("#graph").empty();
	
	drawTopo();
}

// 토폴로지 그리기
function drawTopo() {
	$("#graph").empty();
	
	// Step 1. Create a graph:
	var graph = Viva.Graph.graph();	
	var dept = 0;
	var idealLength = 120;
	var layout = Viva.Graph.Layout.forceDirected(graph, {
		springLength : idealLength,
		springCoeff : 0.0008,
//		dragCoeff : 0.02,
		gravity : -10,
		springTransform: function (link, spring) {
			spring.length = idealLength * (1 - link.data.connectionStrength);
		}
	});	

	// Step 3. Customize node appearance.
	var graphics = Viva.Graph.View.svgGraphics();
  
	// we use this method to highlight all realted links
	// when user hovers mouse over a node:
	highlightRelatedNodes = function(nodeId, isOn) {
		// just enumerate all realted nodes and update link color:
		graph.forEachLinkedNode(nodeId, function(node, link){
			if (link && link.ui) {
				// link.ui is a special property of each link
				// points to the link presentation object.
				link.ui.attr('stroke', isOn ? 'white' : 'red');
				link.ui.attr('stroke-width', '1');
			}
		});
	};
	
	// This function let us override default node appearance and create
	// something better than blue dots:

	graphics.node(function(node) {
		var xValue = 0;
		var text = node.data.this_value;
		if(text && text.length > 6) {
			xValue = 10;
		}
		
		// node.data holds custom object passed to graph.addNode():
		var ui = Viva.Graph.svg('g'),
		svgText = Viva.Graph.svg('text')
			.attr('x', '-' + xValue + 'px')
        	.attr('y', '-4px')
        	.attr('fill','white')
        	.attr('font-size', 12)
        	.text(node.data.this_value),
        img = Viva.Graph.svg('image')
        	.attr('width', node.data.img_size)
        	.attr('height',  node.data.img_size)
        	.link(rootPath + node.data.img_src);

		ui.append(svgText);
		ui.append(img);
		ui.myCustomSize =  node.data.img_size;

		$(ui).hover(function() { // mouse over
			highlightRelatedNodes(node.id, true);
			$('#explanation').html(node.data);
		}, function() { // mouse out
			highlightRelatedNodes(node.id, false);
		});
		
		$(ui).click(function() { 
			if (!node || !node.position || node.data.this_key == '01') {
				return;
			}
			
//			alert("nodekey : " + node.data.this_key);
			
        	renderer.rerender();
        	loadData(graph,node.id, node.data.dept);
        	
        	var allLogInqListForm = $("#allLogInqListForm");
        	var start_date = $("#start_date").val();					// 날짜
        	var starth = $("#starth").val();							// 시작 시
        	var startm = $("#startm").val();							// 시작 분
        	var endh = $("#endh").val();								// 종료 시
        	var endm = $("#endm").val();								// 종료 분
        	var this_key = node.data.this_key;							// 사번+시스템코드
        	var system_seq = node.data.parent_key;						// 시스템코드
        	var system_search = node.data.system_seq;
        	var sub_menu_id = node.data.sub_menu_id;
        	var emp_user_id = this_key.substr(0, this_key.length-node.data.parent_key.length);	// 사번
        	var search_ip = $("#search_ip").val();
        	var privacyType = $("#privacyType").val();
        	if(node.data.dept == 3){
        		emp_user_id = emp_user_id.split("TOPO_")[1];
        		system_seq = system_seq.split(emp_user_id)[1];
        	}
        	if(starth < 10) { starth = "0" + starth; }
        	if(startm < 10) { startm = "0" + startm; }
        	if(endh < 10) { endh = "0" + endh; }
        	if(endm < 10) { endm = "0" + endm; }
        	
        	allLogInqListForm.find("input[name=search_to]").val(start_date);
        	allLogInqListForm.find("input[name=search_from]").val(start_date);
        	allLogInqListForm.find("input[name=starthm]").val(starth + startm + "00");
        	allLogInqListForm.find("input[name=endhm]").val(endh + endm + "59");
        	allLogInqListForm.find("input[name=system_seq]").val(system_search);
        	allLogInqListForm.find("input[name=sub_menu_id]").val(sub_menu_id);
        	allLogInqListForm.find("input[name=emp_user_id]").val(emp_user_id);
        	allLogInqListForm.find("input[name=user_ip]").val(search_ip);
        	allLogInqListForm.find("input[name=privacyType]").val(privacyType);

        	allLogInqListForm.submit();
        	
        	tid = setTimeout(function(){
        		renderer.pause();
        	},3000);
		});
		
		return ui;
	});
	
	graphics.link(function(link) {
		 return Viva.Graph.svg('path')
		 	.attr('stroke', 'red')
		 	.attr('stroke-dasharray', '5, 5');
	  }).placeLink(function(linkUI, fromPos, toPos) {
		  // linkUI - is the object returned from link() callback above. 
//		  var data = 'M' + fromPos.x + ',' + fromPos.y + 'L' + toPos.x + ',' + toPos.y; 
		  var data = 'M' + fromPos.x/1.2 + ',' + fromPos.y/1.2 + 'L' + toPos.x/1.2 + ',' + toPos.y/1.2; 
		  // 'Path.data' (http://www.w3.org/TR/SVG/paths.html#DAttribute) 
		  // is a common way of rendering paths in SVGs: 
		  linkUI.attr("d", data);
	  });

	graphics.placeNode(function(nodeUI, pos) {
		var half = nodeUI.myCustomSize/2;
		// 'g' element doesn't have convenient (x,y) attributes, instead
		// we have to deal with transforms: http://www.w3.org/TR/SVG/coords.html#SVGGlobalTransformAttribute 
		// nodeUI.attr('transform', 'translate(' + (pos.x - half) + ',' + (pos.y - half) + ')');
		nodeUI.attr('transform', 'translate(' + (pos.x/1.2 - half) + ',' + (pos.y/1.2 - half) + ')');
	});

	var renderer = Viva.Graph.View.renderer(graph, {
		container:document.getElementById('graph'),
		layout:layout,
		graphics:graphics,
		/*prerender: 20,*/
		renderLinks:true
	});

	renderer.run();

	neoid = "01";
 
	loadData(graph, neoid, dept);
  
	tid = setTimeout(function(){
		renderer.pause();
	}, 1000 * 10);
}

function addNeo(graph, data) {
	function addNode(id, label) {
		if (!id || typeof id == "undefined") return null;
		var node = graph.getNode(id);
		if (!node) node = graph.addNode(id, label);
        return node;
    }

	if(data.edges != null) {
		$.each(data.edges, function(i, item){
			if (item.this_key) {
				addNode(item.this_key, item);
			}
			if (item.parent_key) {
				addNode(item.parent_key, item);
			}
		});
		
		$.each(data.edges, function(i, item){
			var found = false;
			graph.forEachLinkedNode(item.this_key, function (node, link) {
				if (node.id == item.parent_key) {
					found = true;
				}
			});
			
			var deptLength;
			if(item.dept == 1) {
				deptLength = 0.1;
			}
			else if(item.dept == 2) {
				deptLength = 0.4;
			}
			else if(item.dept == 3) {
				deptLength = 0.7;
			}
			else{
				deptLength = 0.1; 
			}
			
			if (!found && item.this_key && item.parent_value) {
				graph.addLink(item.this_key, item.parent_key, {connectionStrength : deptLength});
			}
			
			// DEPTH 늘어나면 주석 해제
			loadData(graph, item.this_key, item.dept);
		});
	}
	
}

function loadData(graph, id, dept) {
	if(dept > 2){
		return;
	}
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/topology/list_allDepth.html',
		data: {
			key : id, 
			dept : dept, 
			search_ip : $("#search_ip").val(),
			privacyType : $("#privacyType").val(),
			start_date : $("#start_date").val(),
			top_n : $("#top_n").val(),
			isTop_n : $("#isTop_n").attr("checked")? "on" : "off",
			starth : $("#starth").val(),
			startm : $("#startm").val(),
			system_seq : $("#system_seq").val(),
			endh : $("#endh").val(),
			endm : $("#endm").val(),
			realtime : $("#realtime").attr("checked")? "on" : "off"
		},
		success: function(data) {
			addNeo(graph, { edges : data.topologys });
		}
	});
}

function refresh(){
	var timeChange = parseInt($("#refreshBox").val());
	
	setTopo();
	
	clearInterval(topo);
	// 일정 시간에 따른 리로딩
	topo = setInterval(function(){
		timeChange = parseInt($("#refreshBox").val());
		
		var dt = new Date();
		
		setTopo();
	}, timeChange);
}

