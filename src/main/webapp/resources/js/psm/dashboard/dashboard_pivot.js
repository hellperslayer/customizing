/**
 * Dashboard Javascript
 * @author yjyoo
 * @since 2015. 05. 04
 */

// 탭 갯수
var total = 3;
// 대시보드 interval 변수
var real;
// 대시보드 - 실시간차트 interval 변수
var t;

function dashboardPop(type) {
	var wX = screen.availWidth;
	var wY = screen.availHeight-100;
	ml=(wX-650)/2;        //가운데 띄우기위한 창의 x위치
	mt=(wY-650)/2;
	$("#dashForm").find("input[name=dstype]").val(type);
	
	//클래스 만들기
	var typeValue = type.substring(4);
	typeValue = parseInt(typeValue);
	typeValue = typeValue+1;
	var typeValueFrom = ".ch"+typeValue+"From";
	var typeValueTo = ".ch"+typeValue+"To";
	
	var from = $(typeValueFrom).val();
	var to = $(typeValueTo).val();
	
	$("input[name=pop_from]").val(from);
	$("input[name=pop_to]").val(to);
	
	 var frm = document.getElementById("dashForm");
	 window.open('', 'viewer', 'height=' + wY +', width=' + wX + ', scrollbars=no, top='+mt+',left='+0);
	 frm.target = "viewer";
	 frm.action = "dashboardPopView.html";
	 frm.method = "post";
	 frm.submit();    
}

// 탭 변경
function changeTab(total, current){
	
	current_tab_id = current;
	for(var i = 1; i <= total; i++){
		var link = $('#tab' + i).text();
		if(i == current){
			$('#tab' + i).attr('class', 'tab select');
			$('#tab' + i).html(link);
		}else{
			$('#tab' + i).attr('class', 'tab');
			$('#tab' + i).html('<A>' + link +'</A>');
		}
	}
}

function sleep(msecs) {
    var start = new Date().getTime();
    var cur = start;
    while (cur - start < msecs) { 
        cur = new Date().getTime();
    }
}

$(document).ready(function() {

	// 대시보드 탭 클릭
	$("#tab1").click(function() {
		$("#dashboardPeriod").css("display", "inline");
		changeTab(total, 1);
		$("#dashContent1").attr("style", "display: block;");
		$("#dashContent2").attr("style", "display: none;");
		$("#tabId").val("tab1");
	});
	
	// 기존 선택한 탭이 tab1(대시보드) 일 경우
	if($("#tabId").val() == "tab1") {
		changeTab(total, 1);
		$("#dashContent1").attr("style", "display: block;");
		$("#dashContent2").attr("style", "display: none;");
		$("#tabId").val("tab1");
	}
	
	$("#tab2").click(function() {
		$("#dashboardPeriod").css("display", "inline");
		changeTab(total, 2);
		$("#dashContent1").attr("style", "display: none;");
		$("#dashContent2").attr("style", "display: block;");
		$("#tabId").val("tab2");
	});
	
	// 기존 선택한 탭이 tab2(대시보드) 일 경우
	if($("#tabId").val() == "tab2") {
		changeTab(total, 2);
		$("#dashContent1").attr("style", "display: none;");
		$("#dashContent2").attr("style", "display: block;");
		$("#tabId").val("tab2");
	}
});


