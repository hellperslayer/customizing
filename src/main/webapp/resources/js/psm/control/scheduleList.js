/**
 * AuthMngtList Javascript
 * @author yjyoo
 * @since 2015. 04. 17
 */

// 페이지 이동
function goPage(num){
	$("#scheduleListForm input[name=page_num]").val(num);
	moveAuthList();
}

// 권한 리스트로 이동
function moveScheduleList(){
	$("#scheduleListForm").attr("action",scheduleListConfig["listUrl"]);
	$("#scheduleListForm").submit();
}

function moveDetail(schId) {
	var $listForm = $('#scheduleListForm');
	$listForm.appendHiddenInput('schId', schId);
	$listForm.attr("action",scheduleListConfig["detailUrl"]);
	$listForm.submit();
}

// 권한 상세로 이동
//function moveDetail(authId){
//	$("#scheduleListForm input[name=auth_id]").val(authId);
//	$("#scheduleListForm").attr("action",scheduleListConfig["detailUrl"]);
//	$("#scheduleListForm").submit();
//}


//엑셀 다운로드
function excelAuthList() {
	$("#scheduleListForm").attr("action",scheduleListConfig["downloadUrl"]);
	$("#scheduleListForm").submit();
}