/**
 * CodeMngtDetail Javascript
 */

// 코드 등록
function addSchedule(){
	var beanId = $('select[name=beanId]').val();
	
	if(beanId == '') {
		alert("Job Bean을 선택해주세요");
		$('input[name=beanId]').focus();
		return false;
	}
	
	sendAjaxCode("add");
}

// 코드 수정
function saveSchedule(){
	var beanId = $('select[name=beanId]').val();
	
	if(beanId == '') {
		alert("Job Bean을 선택해주세요");
		$('input[name=beanId]').focus();
		return false;
	}
	
	sendAjaxCode("save");
}

// 코드 삭제
function removeSchedule(){
	if(confirm("삭제하시겠습니까?")) {
		sendAjaxCode("remove");
	}
}

// 코드 ajax call
function sendAjaxCode(type){
	var $form = $("#scheduleDetailForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'beanId';
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'add') {
		log_message_title = '스케쥴등록';
		log_action = 'INSERT';
	} else if (type == 'save') {
		log_message_title = '스케쥴수정';
		log_action = 'UPDATE';
	} else if (type == 'remove') {
		log_message_title = '스케쥴삭제';
		log_action = 'REMOVE';
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);

	sendAjaxPostRequest(scheduleConfig[type + "Url"], $form.serialize(), ajaxCode_successHandler, ajaxCode_errorHandler, type);
}

// 코드 ajax call - 성공
function ajaxCode_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			alert("세션이 만료되었습니다.");
			location.href = codeConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	} else{
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}
		
		moveScheduleList();
	}
}

// 코드 ajax call - 실패
function ajaxCode_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){
	alert(textStatus);
	//log_message += codeLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("스케쥴 등록 실패하였습니다." + textStatus);
			break;
		case "save":
			alert("스케쥴 수정 실패하였습니다." + textStatus);
			break;
		case "remove":
			alert("스케쥴 삭제 실패하였습니다." + textStatus);
			break;
	}
}

//목록으로 이동
function moveScheduleList(){
	$("#scheduleDetailForm").attr("action",scheduleConfig["listUrl"]);
	$("#scheduleDetailForm").submit();
}