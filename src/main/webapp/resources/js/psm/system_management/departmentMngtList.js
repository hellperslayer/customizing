/**
 * DepartmentMngtList Javascript
 */

$(function(){
	
	setDefaultTree();
});

function setDefaultTree() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/departmentMngt/defaultTree.html',
		data: { 
		},
		success: function(data) {
			drawDefaultTree(data.hierarchyDepartments);
		}
	});
}

function drawDefaultTree(dt) {
	var data = new Array();
	var rootDeptId = "";
    $.each(dt, function(idx, item){
    	//console.log(item.dept_id+", "+item.parent_dept_id+", "+item.dept_name);
        data[idx] = {id:item.dept_id, parent:item.parent_dept_id, text:item.dept_name};
        if(item.parent_dept_id == "#") {
        	rootDeptId = item.dept_id; 
        }
    });
	$('#tree_123').on("select_node.jstree", 
		function (e, data) { 
			//alert("node_id: " + data.node.id);
			//alert("node_id: " + data.node.text);
			//alert("node_id: " + data.node.parent);
			var parentText = '';
			if(data.node.id == rootDeptId) {
				parentText = '최상위부서';
			} else {
				parentText = $("a#" + data.node.parent + "_anchor")[0].innerText;
			}
			$("#dept_id").val(data.node.id);
			$("#dept_name").val(data.node.text);
			$("#parent_dept_id").val(data.node.parent);
			$("#parent_dept_name").val(parentText);
			
			
		}
	);

	
    $("#tree_123").jstree({
        core: {
            data: data,    //데이터 연결
            themes: {
            	dots:true
            }
        },
        types: {
            'default': {
                'icon': 'jstree-folder'
            }
        }
    });

    var interval_id = setInterval(function(){
        if($("li#"+rootDeptId).length != 0){
            clearInterval(interval_id)
            $("#tree_123").jstree("open_node", $("li#"+rootDeptId));
        	$('#tree_123').jstree('select_node', "li#"+rootDeptId);
         }
   }, 5);
    
    showDepartmentDetail("new");
}



// 부서 추가
function addDepartment(){
	var dept_name = $('input[name=dept_name]').val();
	var parent_dept_id = $('select[name=parent_dept_id]').val();
	var sort_order = $('input[name=sort_order]').val();
	
	if(dept_name == '') {
		alert("부서명을 입력해주세요");
		$('input[name=dept_name]').focus();
		return false;
	}
	if(parent_dept_id == '') {
		alert("부모부서를 선택해주세요");
		return false;
	}
	
	sendAjaxDepartment("add");
}

// 부서 수정
function saveDepartment(){
	var dept_name = $("#dept_name").val();

	if(dept_name == '') {
		alert("부서명을 입력해주세요");
		$('input[name=dept_name]').focus();
		return false;
	}

	sendAjaxDepartment("save");	
}

// 부서 삭제
function removeDepartment(){
	if(confirm("삭제하시겠습니까?")) {
		sendAjaxDepartment("remove");
	}
}

// 부서 ajax call
function sendAjaxDepartment(type){
	var $form = $("#departmentDetailForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'dept_name,parent_dept_id';
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'add') {
		log_message_title = '부서등록';
		log_action = 'INSERT';
	} else if (type == 'save') {
		log_message_title = '부서수정';
		log_action = 'UPDATE';
	} else if (type == 'remove') {
		log_message_title = '부서삭제';
		log_action = 'REMOVE';
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(departmentConfig[type+"Url"], $form.serialize(), ajaxDepartment_successHandler, ajaxDepartment_errorHandler, type);
}

//부서 상세화면 노출
function showDepartmentDetail(data){
	$("#departmentDetailDiv").show();
	
	if(data == null || data == ''){ // 신규
		if($("#departmentDetailForm input[name=parent_dept_id]") == '') {
			alert("부모부서를 지정 후 신규버튼을 눌러주세요");
			return false;
		}
		
		$("#departmentDetailForm input[name=parent_dept_name]").val($("#departmentDetailForm input[name=dept_name]").val());
		$("#departmentDetailForm input[name=parent_dept_id]").val($("#departmentDetailForm input[name=dept_id]").val());
		
		$("#departmentDetailForm input[name=dept_name]").val('');
		$("#departmentDetailForm input[name=dept_id]").val('');
		$("#departmentDetailForm input[name=dept_id]").attr("readonly", false);
		
		$("#insertTr").hide();
		$("#updateTr").hide();
		
		$("#addDepartmentBtn").show();
		$("#saveDepartmentBtn").hide();
		$("#removeDepartmentBtn").hide();
		
	}else{ // 상세

		$("#insertTr").show();
		$("#updateTr").show();
		
		$("#addDepartmentBtn").hide();
		$("#saveDepartmentBtn").show();
		$("#removeDepartmentBtn").show();
		
		$("#departmentDetailForm input[name=dept_id]").attr("readonly", true);
	}
}

// 부서 ajax call - 성공
function ajaxDepartment_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = departmentConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}
		
		moveDepartmentList();
	}
}

// 부서 ajax call - 실패
function ajaxDepartment_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){
	
	log_message += deptLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("부서 등록 실패하였습니다." + textStatus);
			break;
		case "save":
			alert("부서 수정 실패하였습니다." + textStatus);
			break;
		case "remove":
			alert("부서 삭제 실패하였습니다." + textStatus);
			break;
		default:
	}
}

function moveDepartmentList(){
	$("#departmentListForm").attr("action",departmentConfig["listUrl"]);
	$("#departmentListForm").submit();
}