

function setAlarmMngt() {
	var $form = $("#optionForm");
	var agentSeqList = $("#agentSeqList").val().split(',');

	var totAgentSeq = "";
	var totAgentOptions = "";
	var count = 0;
	$('.optionList1').each(function(i,val){
		var dbtype = $("#"+agentSeqList[i]+"_dbtype").val();
		var jdbc_url = $("#"+agentSeqList[i]+"_jdbc_url").val();
		var id = $("#"+agentSeqList[i]+"_id").val();
		var pw = $("#"+agentSeqList[i]+"_pw").val();
		
		var agent_options = '{"DBTYPE": "'+dbtype+'", "JDBC_URL": "'+jdbc_url+'", "ID": "'+id+'", "PW": "'+pw+'"}';
		if(i == 0) {
			totAgentSeq = agentSeqList[i];
			totAgentOptions = agent_options;
		} else {
			totAgentSeq = totAgentSeq + "/@!#" + agentSeqList[i];
			totAgentOptions = totAgentOptions + "/@!#" + agent_options;
		}
		count = i;
	});
	$('.optionList2').each(function(i,val){
		count != 0 ? count++:count=0;
		//count += i;
		
		var SenderEmail = $("#"+agentSeqList[count]+"_SenderEmail").val();
		var SenderPass = $("#"+agentSeqList[count]+"_SenderPass").val();
		var ReceiverEmail = $("#"+agentSeqList[count]+"_ReceiverEmail").val();
		var SiteName = $("#"+agentSeqList[count]+"_SiteName").val();
		
		var agent_options = '{"Sender_Email": "'+SenderEmail+'", "Sender_Pass": "'+SenderPass+'", "Receiver_Email": "'+ReceiverEmail+'", "Site_Name": "'+SiteName+'"}';
		if(totAgentSeq == "") {
			totAgentSeq = agentSeqList[count];
			totAgentOptions = agent_options;
		} else {
			totAgentSeq = totAgentSeq + "/@!#" + agentSeqList[count];
			totAgentOptions = totAgentOptions + "/@!#" + agent_options;
		}
	});
	
	/*$('.optionList2').each(function(i,val){
		count != 0 ? count++:count=0;
		count += i;
		var SenderEmail = $("#"+agentSeqList[count]+"_SenderEmail").val();
		var SenderPass = $("#"+agentSeqList[count]+"_SenderPass").val();
		var ReceiverEmail = $("#"+agentSeqList[count]+"_ReceiverEmail").val();
		var SiteName = $("#"+agentSeqList[count]+"_SiteName").val();
		
		var agent_options = '{"Sender_Email": "'+SenderEmail+'", "Sender_Pass": "'+SenderPass+'", "Receiver_Email": "'+ReceiverEmail+'", "Site_Name": "'+SiteName+'"}';
		if(totAgentSeq == "") {
			totAgentSeq = agentSeqList[count];
			totAgentOptions = agent_options;
		} else {
			totAgentSeq = totAgentSeq + "/@!#" + agentSeqList[count];
			totAgentOptions = totAgentOptions + "/@!#" + agent_options;
		}
	});*/
	
	/*for(var i = 0; i < agentSeqList.length; i++) {
		if(i<3){
			
		}else if(i==3){
			
		}
	}*/
	
	$("#tot_agent_seq").val(totAgentSeq);
	$("#tot_agent_options").val(totAgentOptions);
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/alarmMngt/setAlarmMngt.html',
		data: $form.serialize(),
		success: function(data) {
			alert("수정되었습니다.");
			moveSystemList();
		}
	});
}


function moveSystemList(){
	$("#reloadForm").submit();
}

function testmail(tagNum){
	$.ajax({
		type: 'POST',
		url: rootPath + '/alarmMngt/testmail.html',
		data:{
			"id":$("#"+tagNum+"_SenderEmail").val(),
			"pw":$("#"+tagNum+"_SenderPass").val(),
			"to_id":$("#"+tagNum+"_ReceiverEmail").val(),
			"site_name":$("#"+tagNum+"_SiteName").val()
		},
		dataType:'JSON',
		success: function(data) {
			if(data == 'success'){
				alert("발송되었습니다.");
			}else if(data == 'autherror'){
				alert("메일 송신 계정 권한 오류");
			}else if(data == 'error'){
				alert("메일 발송 오류");
			}else if(data == 'missingdata'){
				alert("메일발송 정보 부족");
			}	
		}
	});
}

$(document).ready(function(){
	var agentSeqList = "";
	$('.agent_seq').each(function(idx,val){
		agentSeqList = agentSeqList+","+$(val).val();
	});
	agentSeqList = agentSeqList.substring(1,agentSeqList.length);
	$('#agentSeqList').val(agentSeqList);
	
});