$(document).ready(function(){
	misdetectSelect();
});


function addMisdetectSummry(){
	var result_title = $('#result_title').val();
	var result_content = $('#result_content').val();
	var privacy_type = $('#privacy_type').val();
	var misdetect_type = $('#misdetect_type').val();
	var log_seq = $('#log_seq').val();
	var misdetect_type = $('#misdetect_type').val();
	var range_to = "";
	var range_from = "";
	
	if(privacy_type == null || privacy_type.trim().length < 1){
		alert('개인정보 유형을 선택해 주세요.');
		$('#result_title').focus();
		return;
	}
	if(result_title == null || result_title.trim().length < 1){
		alert('타이틀명을 작성해 주세요.');
		$('#result_title').focus();
		return;
	}
	if(misdetect_type == null || misdetect_type.trim().length < 1){
		alert('제외유형을 선택해 주세요.');
		$('#misdetect_type').focus();
		return;
	}
	
	if(misdetect_type == 1){
		range_to = $('#range_to').val();
		range_from = $('#range_from').val();
		var result_contentVali = false;
		if(range_to == null || range_to.trim().length < 1){
			result_contentVali = true;
		}else if(range_from == null || range_from.trim().length < 1){
			result_contentVali = true;
		}
		if(result_contentVali){
			alert('제외문자를 작성해 주세요.');
			$('#range_to').focus();
			return;
		}
	}else{
		if(result_content == null || result_content.trim().length < 1){
			alert('제외문자를 작성해 주세요.');
			$('#result_content').focus();
			return;
		}
	}
	
	$.ajax({
		type:'POST',
		url : rootPath + '/misdetect/addMisdetectSummry.html',
		data: {
			privacy_type : $('#privacy_type').val(),
			system_seq : $('#system_seq').val(),
			result_title : $('#result_title').val(),
			result_content : result_content,
			range_to : range_to,
			range_from : range_from,
			use_yn : $('input[name=use_flag]:checked').val(),
			misdetect_type : $('#misdetect_type').val(),
			log_seq : log_seq
		},
		success : function(data){
			alert('저장되었습니다.');
			moveList();
		}
	});
}

function goPage(num){
	$("#listForm input[name=page_num]").val(num);
	moveList();
}

function misDetectDetail(log_seq){
	$("input[name=log_seq]").attr("value",log_seq);
	$("#listForm").attr("action",misdetectConfig["detailUrl"]);
	$("#listForm").submit();
}

function moveList(num){
	if(num!=null){
		$("#listForm input[name=page_num]").val(num);
	}
	$("#listForm").attr("action",misdetectConfig["listUrl"]);
	$("#listForm").submit();
}

function misdetectSelect(){
	var misdetect_type = $('#misdetect_type').val();
	if(misdetect_type == 1){
		$('#result_content_range').css('display','');
		$('#result_content_text').css('display','none');
	}else{
		$('#result_content_range').css('display','none');
		$('#result_content_text').css('display','');
	}
}