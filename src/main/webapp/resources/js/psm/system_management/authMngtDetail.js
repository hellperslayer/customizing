/**
 * AuthMngtDetail Javascript
 */
$(document).ready(function(){
	allCheckCount('sys_auth_ids');
	allCheckCount('sys_report_ids');
	allCheckCount('make_report_ids');
	
});

// 권한 등록
function addAuth(){
	var auth_name = $('input[name=auth_name]').val();
	var use_flag = $('input[name=use_flag]:checked').val();
	
	if(auth_name == '') {
		alert("권한명을 입력해주세요");
		$('input[name=auth_name]').focus();
		return false;
	}
	if(use_flag == '') {
		alert("사용여부를 선택해주세요");
		return false;
	}
	
	sendAjaxAuth("add");
}
// 권한 수정
function saveAuth(){
	var auth_name = $('input[name=auth_name]').val();
	var use_flag = $('input[name=use_flag]:checked').val();
	
	if(auth_name == '') {
		alert("권한명을 입력해주세요");
		$('input[name=auth_name]').focus();
		return false;
	}
	if(use_flag == '') {
		alert("사용여부를 선택해주세요");
		return false;
	}
	
	sendAjaxAuth("save");
}

// 권한 삭제
function removeAuth(){
	if(confirm("권한을 삭제하면 해당 권한에 속한 관리자의 권한도 함께 삭제됩니다. 권한을 삭제하시겠습니까?")) {
		sendAjaxAuth("remove");
	}
}

// 권한 ajax call
function sendAjaxAuth(type){
	var $form = $("#authDetailForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'auth_name';
	var menu_id = $('input[name=current_menu_id]').val();

	var selectedSysIds = idsSerialize('sys_auth_ids');
	
	$('input[name=sys_auth_ids]').val(selectedSysIds);
	$('input[name=sys_report_ids]').val(idsSerialize('sys_report_ids'));
	$('input[name=make_report_ids]').val(idsSerialize('make_report_ids'));
	/*$('input[name=report_auth_user]').val(idsSerialize('report_auth_user'));
	$('input[name=not_report_auth_user]').val(idsReversSerialize('report_auth_user'));*/
	
	if(selectedSysIds == ''){
		alert('하나 이상의 시스템을 선택해야 합니다');
		return;
	}
	
	if (type == 'add') {
		log_message_title = '권한등록';
		log_action = 'INSERT';
	} else if (type == 'save') { 
		log_message_title = '권한수정';
		log_action = 'UPDATE';
	} else if (type == 'remove') {
		log_message_title = '권한삭제';
		log_action = 'REMOVE';
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(authDetailConfig[type + "Url"], $form.serialize(), ajaxAuth_successHandler, ajaxAuth_errorHandler, type);
}

//체크박스에 체크된 데이터를 ,로 구분하는 한줄 문자열 데이터로 바꿔줌
function idsSerialize(className){
	var ids = '';
	$('.'+className+':checked').each(function(index,item){
		if(index!=0){
			ids += ',';
		}
		ids +=$(this).val();
	});
	return ids;
}

function idsReversSerialize(className){
	var ids = '';
	$('.'+className).not(':checked').each(function(index,item){
		if(index!=0){
			ids += ',';
		}
		ids +=$(this).val();
	});
	return ids;
}

// 권한 ajax call - 성공
function ajaxAuth_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = authDetailConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}

		moveAuthList();
	}
}

// 권한 ajax call - 실패
function ajaxAuth_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){
	
	log_message += authLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("권한 등록 실패하였습니다." + textStatus);
			break;
		case "save":
			alert("권한 수정 실패하였습니다." + textStatus);
			break;
		case "remove":
			alert("권한 삭제 실패하였습니다." + textStatus);
			break;
	}
}

//목록으로 이동
function moveAuthList(){
	$("#authListForm").attr("action",authDetailConfig["listUrl"]);
	$("#authListForm").submit();
}

function allCheckCount(allCheckName){
	var allcount = $('.'+allCheckName).length;
	var checkcount = $('.'+allCheckName+':checked').length;
	if(allcount==checkcount){
		$('#'+allCheckName).prop('checked',true);
	}else{
		$('#'+allCheckName).prop('checked',false);
	}
}

function allCheck(check){
	var checked = $(check).prop('checked');
	var className = $(check).attr('id'); 
	console.log(checked);
	if(checked){
		$('.'+className).prop('checked',true);
	}else{
		$('.'+className).prop('checked',false);
	}
}

function findAdminUserDetail(admin_user_id){
	$("#adminUserListForm").attr("action", authDetailConfig["adminDetailUrl"]);
	$("#adminUserListForm").appendHiddenInput('admin_user_id', admin_user_id);
	$("#adminUserListForm").submit();
}

function batchMenu(){
	var isCheck = $("#sub_batch").prop("checked");
	var btn = $("#subBatchBtn");
	if(isCheck){
		$("#sub_batch").prop("checked",false);
		btn.removeClass("blue");
		btn.addClass("btn-default");
		$("#checkFont").removeClass("fa-check-square");
		$("#checkFont").addClass("fa-square");
	}else{
		$("#sub_batch").prop("checked",true);
		btn.removeClass("btn-default");
		btn.addClass("blue");
		$("#checkFont").removeClass("fa-square");
		$("#checkFont").addClass("fa-check-square");
	}
	btn.blur();
	//sendAjaxMenu("add");
}