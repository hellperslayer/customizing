/**
 * AgentMngtList Javascript
 */

var $clone;

$(document).ready(function() {
	$clone = $("#agentListForm").clone();
});


// 에이전트 동작 버튼 클릭
function actionAgent(agent_seq, server_seq, ip, port, status) {
	
	var actionStatus = "";
	var log_message_title = "";
	var menu_id = $('input[name=current_menu_id]').val();
	
	if(status == 'false') {
		actionStatus = "use";
		log_message_title = "에이전트 동작";
	}
	else if(status == 'use') {
		actionStatus = "false";
		log_message_title = "에이전트 정지";
	}
	
	if(confirm(log_message_title + "하시겠습니까?")) {
		//showSpin();
		
		log_message_title = log_message_title + " : " + agent_seq; 

		$.ajax({
			type: 'POST',
			url: rootPath + '/agentMngt/actionAgent.html',
			data: {
				agent_seq : agent_seq,
				server_seq : server_seq,
				ip : ip,
				port : port,
				status : actionStatus
			},
			success : function(data) {
				addManagerActHist(menu_id, log_message_title, "UPDATE");
				
				if(data.paramBean.result ==  1) {
					alert("정상적으로 반영되었습니다.");
				}
				else {
					alert("에이전트 상태 변경 중 문제가 발생하였습니다.");
				}
				
				moveAgentList();
			},
			error : function() {
				alert("에이전트 상태 변경 중 문제가 발생하였습니다.");
			}
		});
	}
}

// 에이전트 동작 버튼 클릭
function agentUnusual() {
	alert("에이전트 상태를 확인할 수 없습니다.");
}

// 페이지 이동
function goPage(num){
	$clone.attr("action", agentListConfig["listUrl"]);
	$clone.find("input[name=page_num]").val(num);
	$clone.attr("style", "display: none;");
	$('body').append($clone);
	$clone.submit();
}

// 에이전트관리 리스트로 이동
function moveAgentList(){
	$("#agentListForm").attr("action",agentListConfig["listUrl"]);
	$("#agentListForm input[name=page_num]").val('1');
	$("#agentListForm").submit();
}

// 에이전트동작정지 관련 관리자행위이력 추가
function addManagerActHist(menu_id, log_message, log_action) {
	$.ajax({
		type: 'POST',
		url: rootPath + '/managerActHist/add.html',
		data: {
			menu_id : menu_id,
			log_message : log_message,
			log_action : log_action
		}
	});
}

function goAgentDetail(seq) {
	$("#agentListForm input[name=agent_seq]").val(seq);
	$("#agentListForm").attr("action",agentListConfig["detailUrl"]);
	$("#agentListForm").submit();
}

function addAgent() {
	var agent_seq = $("input[name=agent_seq]").val();
	var agent_name = $("input[name=agent_name]").val();
	var ip = $("input[name=ip]").val();
	var port = $("input[name=port]").val();
	
	if(agent_seq == '') {
		alert("에이전트ID를 입력해주세요.");
		$("input[name=agent_seq]").focus();
	} else if(agent_name == '') {
		alert("에이전트명을 입력해주세요.");
		$("input[name=agent_name]").focus();
	} else if(ip == '') {
		alert("IP를 입력해주세요.");
		$("input[name=ip]").focus();
	} else if(port == '') {
		alert("PORT를 입력해주세요.");
		$("input[name=port]").focus();
	}
	
	var confirmVal = confirm("등록하시겠습니까?");
	if(confirmVal){
		sendAjaxAgent("add");	
	}else{
		return false;
	}
}

function removeAgent() {
	
	var confirmVal = confirm("삭제하시겠습니까?");
	if(confirmVal){
		sendAjaxAgent("remove");	
	}else{
		return false;
	}
}

function saveAgent() {
	var agent_seq = $("input[name=agent_seq]").val();
	var agent_name = $("input[name=agent_name]").val();
	var ip = $("input[name=ip]").val();
	var port = $("input[name=port]").val();
	
	if(agent_seq == '') {
		alert("에이전트ID를 입력해주세요.");
		$("input[name=agent_seq]").focus();
	} else if(agent_name == '') {
		alert("에이전트명을 입력해주세요.");
		$("input[name=agent_name]").focus();
	} else if(ip == '') {
		alert("IP를 입력해주세요.");
		$("input[name=ip]").focus();
	} else if(port == '') {
		alert("PORT를 입력해주세요.");
		$("input[name=port]").focus();
	}
	
	var confirmVal = confirm("수정하시겠습니까?");
	if(confirmVal){
		sendAjaxAgent("save");	
	}else{
		return false;
	}
}

function sendAjaxAgent(type){
	var $form = $("#agentDetailForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = '';
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'add') {
		log_message_title = '에이전트 등록';
		log_action = 'INSERT';
	} else if (type == 'save') {
		log_message_title = '에이전트 수정';
		log_action = 'UPDATE';
	} else if (type == 'remove') {
		log_message_title = '에이전트 삭제';
		log_action = 'REMOVE';
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(agentListConfig[type + "Url"],$form.serialize(), ajaxAgent_successHandler, ajaxAgent_errorHandler, type);
}

function ajaxAgent_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = inspectionHistConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}
		
		moveAgentList();
	}
}

function ajaxAgent_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	log_message += codeLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("에이전트 실패하였습니다." + textStatus);
			break;
		case "save":
			alert("에이전트 실패하였습니다." + textStatus);
			break;
		case "remove":
			alert("에이전트 실패하였습니다." + textStatus);
			break;
		default:
	}
}

function setAlarmMngt_email() {
	var $form = $("#mailForm");
	
		var dbtype = $("input[name=dbtype]").val();
		var jdbc_url = $("input[name=jdbc_url]").val();
		var id = $("input[name=id]").val();
		var pw = $("input[name=pw]").val();
		var to_id = $("input[name=to_id]").val();
		
		//var agent_options = '{"DBTYPE": "'+dbtype+'", "JDBC_URL": "'+jdbc_url+'", "ID": "'+id+'", "PW": "'+pw+'"}';
		var agent_options = '{"DBTYPE": "'+dbtype+'", "JDBC_URL": "'+jdbc_url+'", "ID": "'+id+'", "PW": "'+pw+'", "TO_ID": "'+to_id+'"}';
		$("#agent_options").val(agent_options);
	$.ajax({
		type: 'POST',
		url: rootPath + '/alarmMngt/setAlarmMngt_email.html',
		data: $form.serialize(),
		success: function(data) {
			alert("수정되었습니다.");
			moveAgentList();
		}
	});
}

function setLicenCe_Hist() {
	var $form = $("#licenseForm");
	
		var history = $("textarea[name=history]").val();
		
	$.ajax({
		type: 'POST',
		url: rootPath + '/agentMngt/license_Hist.html',
		data: $form.serialize(),
		success: function(data) {
			alert("수정되었습니다.");
			moveAgentList();
		}
	});
}
var mailFlag = 'N';
function mailFormYn(){
	if(mailFlag == 'Y'){
		$("#mailDiv").css("display","none");
		mailFlag = 'N';
	}else if(mailFlag == 'N'){
		$("#mailDiv").css("display","block");
		mailFlag = 'Y';
	}
	
}

var licenseFlag = 'N';
function licenseFormYn(){
	if(licenseFlag == 'Y'){
		$("#licenseDiv").css("display","none");
		licenseFlag = 'N';
	}else if(licenseFlag == 'N'){
		$("#licenseDiv").css("display","block");
		licenseFlag = 'Y';
	}
}