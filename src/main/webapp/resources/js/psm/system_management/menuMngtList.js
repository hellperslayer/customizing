/**
 * MenuMngtList Javascript
 */

// dynatree rendering
$(function(){
	$("#menuTree").dynatree({
		minExpandLevel: 2
		,debugLevel: 1
      	,persist: true 
		,onPostInit: function(isReloading, isError) {
			this.reactivate();
		}
		,onActivate: selectMenuTree
	});
	
	var cookieData = document.cookie;
	var active_index = cookieData.indexOf("dynatree-active=");
	
	// 메뉴 최초 접근
	if(active_index == -1) {
		findMenuDetail("MENU00000");
	}
	
	$("#menuDetailDiv").hide();
});

// 트리 메뉴 선택
function selectMenuTree(node){
	if(node == null){
		return false;
	}
	
	var menuId = node.data.href;
	findMenuDetail(menuId);
}

/*$(function(){
	var menuId = $("#menuListForm input[name=sel_menu_id]").val();
	// 메뉴 최초 접근
	if(menuId == null || menuId =="") {
		$("#MENU00000").attr('data-jstree', '{ "opened" : true, "selected" : true }');
		findMenuDetail("MENU00000");
	}else{
		$("#"+menuId).attr('data-jstree', '{ "opened" : true, "selected" : true }');
		findMenuDetail(menuId);
	}
});*/

// 메뉴 상세 가져오기
function findMenuDetail(menuId){
	sendAjaxPostRequest(menuConfig["detailUrl"],{"menu_id":menuId},findMenuDetail_successHandler, findMenuDetail_errorHandler);
	/*
	$.ajax({
		type: 'POST',
		url: rootPath + '/menuMngt/detail.html',
		data:{
			"menu_id": menuId
		},
		success: function() {
		}
	})*/
}

// 메뉴 상세 가져오기 - 성공
function findMenuDetail_successHandler(data, dataType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = menuConfig['loginPage'];
			return false;
		}
	}
	
	if(data == null || !data.hasOwnProperty("menuDetail")){
		alert("상세 데이터 없음.");
		return false;
	}
	showMenuDetail(data);
}

// 메뉴 상세 가져오기 - 실패
function findMenuDetail_errorHandler(XMLHttpRequest, textStatus, errorThrown){
	alert("findMenuDetail_errorHandler : " + textStatus);
}

// 메뉴 상세 노출
function showMenuDetail(data){
	$("#menuDetailDiv").show();
	
	var menuId = initMenuDetail();
	
	if(data == null || data == ''){ // 신규
		$("#menuIdLabel").html("");
		$("#menuDetailForm input[name=parent_menu_id]").removeAttr("name");
		$("#menuDetailForm select[id=parent_menu_id_select]").attr("name", "parent_menu_id");
		$("#menuDetailForm select[name=parent_menu_id]").show();
		$("#menuDetailForm select[name=parent_menu_id]").val(menuId);
		$("#rootMenuParentNameSpan").hide();
		
		$("#insertTr").hide();
		$("#updateTr").hide();
		
		$("#authListDiv").hide();
		$("#addMenuBtn").show();
		$("#saveMenuBtn").hide();
		$("#subBatchBtn").hide();
		$("#removeMenuBtn").hide();
	}else{ // 상세
		var menuDetail = data.menuDetail;

		$("#menuIdLabel").html("· MENU ID : " + menuDetail.menu_id);
		
		if(menuDetail.parent_menu_id == null || menuDetail.parent_menu_id == 'MENU'){
			$("#menuDetailForm select[name=parent_menu_id]").removeAttr("name");
			$("#rootMenuParentNameSpan").show();
			$("#menuDetailForm select[id=parent_menu_id_select]").hide();
			$("#menuDetailForm input[id=parent_menu_id]").attr("name", "parent_menu_id");
			$("#menuDetailForm input[name=parent_menu_id]").val('MENU');
		}else{
			$("#menuDetailForm input[name=parent_menu_id]").removeAttr("name");
			$("#rootMenuParentNameSpan").hide();
			$("#menuDetailForm select[id=parent_menu_id_select]").show();
			$("#menuDetailForm select[id=parent_menu_id_select]").attr("name", "parent_menu_id");
			$("#menuDetailForm select[name=parent_menu_id]").val(menuDetail.parent_menu_id);
		}
		
		$("#menuDetailForm input[name=menu_id]").val(menuDetail.menu_id);
		$("#menuDetailForm input[name=menu_name]").val(menuDetail.menu_name);
		$("#menuDetailForm input[name=menu_url]").val(menuDetail.menu_url);
		$("#menuDetailForm span[id=menu_url]").text(menuDetail.menu_url == null ? '' : menuDetail.menu_url);
		$("#menuDetailForm input[name=sort_order]").val(menuDetail.sort_order);
		$("#menuDetailForm input[name=description]").val(menuDetail.description);
		$("#menuListForm input[name=sel_menu_id]").val(menuDetail.menu_id);
		
		$("#menuDetailDiv span[id=insertUserIdSpan]").text(menuDetail.insert_user_id == null ? '' : menuDetail.insert_user_id);
		$("#menuDetailDiv span[id=insertDatetimeSpan]").text(menuDetail.insert_datetime == null ? '' : new Date(menuDetail.insert_datetime).convertDatetimeToString());
		$("#menuDetailDiv span[id=updateUserIdSpan]").text(menuDetail.update_user_id == null ? '' : menuDetail.update_user_id);
		$("#menuDetailDiv span[id=updateDatetimeSpan]").text(menuDetail.update_datetime == null ? '' : new Date(menuDetail.update_datetime).convertDatetimeToString());
		
		// jquery 오작동으로 인한 주석
		//$("#menuDetailForm input[name=use_flag]:input[value=" + menuDetail.use_flag + "]").attr("checked",true);
		var elems = document.getElementsByName("use_flag");
		for(var i=0; i<elems.length; i++) {
			var elem = elems[i];
			if(elem.value == menuDetail.use_flag) {
				elem.checked = true;
			}
		}
		
		var menuAuths = data.menuAuths;
		for(var i in menuAuths){
			/*for(var j in document.getElementsByName("auth_ids")){
				var checkbox = document.getElementsByName("auth_ids")[j];
				if(menuAuths[i].auth_id == checkbox.value){
					checkbox.checked = true;
					break;
				}
			}*/
			var auth_ids = document.getElementsByName("auth_ids");
			for(var j=0; j<auth_ids.length; j++) {
				var auth_id = auth_ids[j];
				if(auth_id.value == menuAuths[i].auth_id) {
					auth_id.checked = true;
				}
			}
		}
		
		// jquery 오작동으로 인한 주석
		/*for(var i in menuAuths){
			$("#authListDiv input[name=auth_ids]").each(function(){
				if($(this).val() == menuAuths[i].auth_id){
					$(this).attr("checked",true);
				}
			});
		}*/
		
		$("#insertTr").show();
		$("#updateTr").show();
		$("#authListDiv").show();
		
		$("#addMenuBtn").hide();
		$("#saveMenuBtn").show();
		$("#subBatchBtn").show();
		$("#removeMenuBtn").show();
	}
}

// 메뉴 상세화면 초기화
function initMenuDetail(){
	var menuId = $("#menuDetailForm input[name=menu_id]").val();
	$("#menuDetailForm input[name=menu_id]").val('');
	$("#menuDetailForm input[name=menu_name]").val('');
	$("#menuDetailForm input[id=parent_menu_id]").val(menuId);
	$("#menuDetailForm input[name=menu_url]").val('');
	$("#menuDetailForm input[name=sort_order]").val('');
	$("#menuDetailForm input[name=description]").val('');
	$("#menuDetailDiv span[id=insertUserIdSpan]").text('');
	$("#menuDetailDiv span[id=insertDatetimeSpan]").text('');
	$("#menuDetailDiv span[id=updateUserIdSpan]").text('');
	$("#menuDetailDiv span[id=updateDatetimeSpan]").text('');
	$("#menuDetailForm input[name=use_flag]:input[value=Y]").attr("checked",true);
	$("#menuListForm input[name=sel_menu_id]").val('');
	
	allAuthCheckControl(false);
	return menuId;
}

// 전체 권한 체크박스 컨트롤
function allAuthCheckControl(flag){
	// 익스플로러에서 안됨..
	/*for(var i in document.getElementsByName("auth_ids")){
		var checkbox = document.getElementsByName("auth_ids")[i];
			checkbox.checked = flag;
	}*/
	var elems = document.getElementsByName("auth_ids");
	for(var i=0; i<elems.length; i++) {
		var elem = elems[i];
		elem.checked = flag;
	}
	
	//jquery 오작동으로 인한 주석(크롬에서 안됨)
	//$("#authListDiv input[name='auth_ids']:checkbox").attr("checked",flag);
}

// 전체 권한 체크박스 컨트롤
function allAuthMenuCheck(checkbox){
	allAuthCheckControl($(checkbox).is(":checked"));
}

function batchMenu(){
	var isCheck = $("#sub_batch").prop("checked");
	var btn = $("#subBatchBtn");
	if(isCheck){
		$("#sub_batch").prop("checked",false);
		btn.removeClass("blue");
		btn.addClass("btn-default");
		$("#checkFont").removeClass("fa-check-square");
		$("#checkFont").addClass("fa-square");
	}else{
		$("#sub_batch").prop("checked",true);
		btn.removeClass("btn-default");
		btn.addClass("blue");
		$("#checkFont").removeClass("fa-square");
		$("#checkFont").addClass("fa-check-square");
	}
	btn.blur();
	//sendAjaxMenu("add");
}

// 메뉴 추가
function addMenu(){
	var menu_name = $('input[name=menu_name]').val();
	var use_flag = $('input[name=use_flag]:checked').val();
	var sort_order = $('input[name=sort_order]').val();
	
	if(menu_name == '') {
		alert("메뉴명을 입력해주세요");
		$('input[name=menu_name]').focus();
		return false;
	}
	if(use_flag == '') {
		alert("사용여부를 선택해주세요");
		return false;
	}
	if(sort_order != '' && sort_order < 0) {
		alert("정렬순서는 0보다 작을 수 없습니다. 다시 입력해주세요!");
		$('input[name=sort_order]').select();
		return false;
	}
	
	sendAjaxMenu("add");
}

// 메뉴 수정
function saveMenu(){
	var menu_name = $('input[name=menu_name]').val();
	var use_flag = $('input[name=use_flag]:checked').val();
	var sort_order = $('input[name=sort_order]').val();
	
	if(menu_name == '') {
		alert("메뉴명을 입력해주세요");
		$('input[name=menu_name]').focus();
		return false;
	}
	if(use_flag == '') {
		alert("사용여부를 선택해주세요");
		return false;
	}
	if(sort_order != '' && sort_order < 0) {
		alert("정렬순서는 0보다 작을 수 없습니다. 다시 입력해주세요!");
		$('input[name=sort_order]').select();
		return false;
	}
	
	var authIds = new Array();
	$("#authListDiv input[name=auth_ids]").each(function(){
		if($(this).is(":checked")){authIds.push($(this).val());}
	});
	$("#menuDetailForm input[name=auth_ids]").val(authIds.join(","));
	
	sendAjaxMenu("save");
}
	
// 메뉴 삭제
function removeMenu(){
	if(confirm("삭제하시겠습니까?")) {
		sendAjaxMenu("remove");
	}
}

// 메뉴 ajax call
function sendAjaxMenu(type){
	var $form = $("#menuDetailForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'menu_name';
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'add') {
		log_message_title = '메뉴등록';
		log_action = 'INSERT';
	} else if (type == 'save') {
		log_message_title = '메뉴수정';
		log_action = 'UPDATE';
	} else if (type == 'remove') {
		log_message_title = '메뉴삭제';
		log_action = 'REMOVE';
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(menuConfig[type+"Url"], $form.serialize(), ajaxMenu_successHandler, ajaxMenu_errorHandler, type);
}

// 메뉴 ajax call - 성공
function ajaxMenu_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			alert("세션이 만료되었습니다.");
			location.href = menuConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	} else{
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}
		
		moveMenuList();
	}
}

// 메뉴 ajax call - 실패
function ajaxMenu_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){
	
	log_message += menuLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("메뉴 등록 실패하였습니다." + textStatus);
			break;
		case "save":
			alert("메뉴 수정 실패하였습니다." + textStatus);
			break;
		case "remove":
			alert("메뉴 삭제 실패하였습니다." + textStatus);
			break;
		default:
	}
}

// 메뉴목록 이동
function moveMenuList(){
	$("#menuListForm").attr("action",menuConfig["listUrl"]);
	$("#menuListForm").submit();
}

