function saveTalarmMngtOne(){
	var emp_user_id = $("#talarmListForm input[name=emp_user_id]").val();
	var emp_user_name = $("#talarmListForm input[name=emp_user_name]").val();
	var email_address_up = $("#talarmDetailForm input[name=email_address_up]").val();
	var mobile_number_up = $("#talarmDetailForm input[name=mobile_number_up]").val();
	var useyn_email = $('input[name=useyn_email]:checked').val();
	var useyn_htel = $('input[name=useyn_htel]:checked').val();

	
	if(email_address_up == '') {
		alert("메일 주소를 넣어주세요");
		$("#talarmDetailForm input[name=email_address_up]").focus();
		return false;
	}
	if(mobile_number_up == '') {
		alert("메일 주소를 넣어주세요");
		$("#talarmDetailForm input[name=mobile_number_up]").focus();
		return false;
	}

	if(checkValidation_save()){
		sendAjaxTalarm("save");	
	}
}

function saveTalarmMngtTwo(){
	var emp_user_id = $("#talarmListForm input[name=emp_user_id]").val();
	var emp_user_name = $("#talarmListForm input[name=emp_user_name]").val();
	var email_address_up = $("#talarmDetailForm input[name=email_address_up]").val();
	var mobile_number_up = $("#talarmDetailForm input[name=mobile_number_up]").val();
	var useyn_email = $('input[name=useyn_email]:checked').val();
	var useyn_htel = $('input[name=useyn_htel]:checked').val();
	var email_subject = $("#talarmDetailForm input[name=email_subject]").val();
	var email_content = $("#talarmDetailForm input[name=email_content]").val();
	var htel_content = $("#talarmDetailForm input[name=htel_content]").val();
	
	if(email_address_up == '') {
		alert("메일 주소를 넣어주세요");
		$("#talarmDetailForm input[name=email_address_up]").focus();
		return false;
	}
	if(mobile_number_up == '') {
		alert("메일 주소를 넣어주세요");
		$("#talarmDetailForm input[name=mobile_number_up]").focus();
		return false;
	}	
	if(checkValidation_save()){
		sendAjaxTalarm("save1");
	}
}

// 사원 삭제
function removeTalarm(){
	if(confirm("사원을 삭제하시겠습니까?")) {
		sendAjaxTalarm("remove");
	}
}

// 사원 ajax call
function sendAjaxTalarm(type){
	var $form = $("#talarmDetailForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'emp_user_id';
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'add') {
		log_message_title = '사원등록';
		log_action = 'INSERT';
	} else if (type == 'save') {
		log_message_title = '사원수정';
		log_action = 'UPDATE';
	} else if (type == 'save1') {
		log_message_title = '사원수정';
		log_action = 'UPDATE';
	} else if (type == 'remove') {
		log_message_title = '사원삭제';
		log_action = 'REMOVE';
	}

	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(talarmDetailConfig[type + "TalarmUrl"], $form.serialize(), ajaxTalarm_successHandler, ajaxTalarm_errorHandler, type);
}

// 사원 ajax call - 성공
function ajaxTalarm_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "save1":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}
		
		moveTalarmList();
	}
}

// 사원 ajax call - 실패
function ajaxTalarm_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){
	moveTalarmList();
	log_message += talarmLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("사원 등록 실패하였습니다." + textStatus);
			break;
		case "save":
			alert("사원 수정 실패하였습니다." + textStatus);
			break;
		case "save1":
			alert("사원 수정 실패하였습니다." + textStatus);
			break;
		case "remove":
			alert("사원 삭제 실패하였습니다." + textStatus);
			break;
		default:
	}
}

// 필수 항목 확인 (신규)
function checkValidation_add(){
	var emp_user_id = $("#talarmDetailForm input[name=emp_user_id]").val();
	
	if(emp_user_id == '') {
		alert("사용자ID를 입력해주세요!");
		$("#talarmDetailForm input[name=emp_user_id]").focus();
		return false;
	}
	
	return true;
}

// 필수 항목 확인 (수정)
function checkValidation_save(){
	var emp_user_id = $("#talarmDetailForm input[name=emp_user_id]").val();
	
	if(emp_user_id == '') {
		alert("사용자ID를 입력해주세요!");
		$("#talarmDetailForm input[name=emp_user_id]").focus();
		return false;
	}
	return true;
}

// 목록으로 이동
function moveTalarmList(){
	$("#talarmListForm").attr("action", talarmDetailConfig["talarmListUrl"]);
	$("#talarmListForm").submit();
}

