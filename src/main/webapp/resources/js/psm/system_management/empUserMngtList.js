/**
 * EmpUserMngtList Javascript
 * @author yjyoo
 * @since 2015. 04. 21
 */



var $clone;

$(document).ready(function() {
	$clone = $("#empUserListForm").clone();
});

// 페이지 이동
function goPage(num){
	$clone.attr("action", empUserListConfig["listUrl"]);
	$clone.find("input[name=page_num]").val(num);
	$clone.attr("style", "display: none;");
	$('body').append($clone);
	$clone.submit();
}

//엑셀 업로드 팝업 기능
function excelUpLoadList(){
	var excelUp = $(".popup_wrap");
	excelUp.attr("style", "display: block;");
}

function closeUpload(){
	var excelUp = $(".popup_wrap");
	var file = $("#file");
	excelUp.attr("style", "display: none;");
	file.replaceWith( file.clone(true));
}

function excelEmpUserUpLoad(){
	
	var data = $("#result").val();
	
		var options = {
				
				success : function(data) {
					var checkdata = "\"true\"";
					if(checkdata == data){
						alert("성공적으로 업로드 되었습니다!");
						moveEmpUserList();
					}else{
						var result = data.split(",");
						if (result.length > 1) {
							if (result[0] == "\"NO_DEPT") {
								alert("존재하지 않는 소속명입니다. \"" + result[1]);
							} else if (result[0] == "\"NO_SYSTEM") {
								alert("존재하지 않는 시스템명입니다. \"" + result[1]);
							} else {
								alert("업로드에 실패하였습니다. (엑셀파일의 셀안에 공백이 들어있거나 필수 입력정보를 입력하였는지 확인해주십시오.)");
							}
						} else {
							alert("업로드에 실패하였습니다. (엑셀파일의 셀안에 공백이 들어있거나 필수 입력정보를 입력하였는지 확인해주십시오.)");
						}
					}
					
				},
				error : function(error) {
					alert("요청 처리 중 오류가 발생하였습니다.");
				}
		  };
		    
    $("#fileForm").ajaxSubmit(options);

    return false;
}


// 검색
function moveEmpUserList() {
	$("#empUserListForm").attr("action", empUserListConfig["listUrl"]);
	$("#empUserListForm input[name=page_num]").val('1');
	$("#empUserListForm").submit();
}

function searchEmpUserList() {
	$("#empUserListForm").attr("action", empUserListConfig["listUrl"]);
	$("#empUserListForm input[name=isSearch]").val('Y');
	$("#empUserListForm input[name=page_num]").val('1');
	$("#empUserListForm").submit();
}

// 상세 페이지 이동 (신규, 수정)
function findEmpUserMngtOne(emp_user_id, system_seq){
	$("#empUserListForm").attr("action", empUserListConfig["detailUrl"]);
	$('#empUserListForm').appendHiddenInput('emp_user_id', emp_user_id);
	$('#empUserListForm').appendHiddenInput('system_seq', system_seq);
	// 아래의 코드와 같다. 공통 js 참조
	// $('#empUserListForm').append($('<input type="hidden" name="emp_user_id" value="' + emp_user_id + '" />'));
	$("#empUserListForm").submit();
}

// 엑셀 다운로드
function excelEmpUserList() {
	
	var $form = $("#empUserListForm");
	var log_message_params = '';
	var menu_id = $('input[name=current_menu_id]').val();
	var log_message_title = '사원관리 엑셀 다운로드';
	var log_action = 'EXCEL DOWNLOAD';
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	$("#empUserListForm").attr("action", empUserListConfig["downloadUrl"]);
	$("#empUserListForm").submit();
}

/*function sendAjaxEmpUser(){
	var form = $("#fileForm");
	var type = "upload";
	var formData = new FormData();
	formData.append("file",$("#f2 input[name = file]")[0].files[0]);
	var log_message_title = '엑셀업로드';
	var log_action = 'INSERT';
	var log_message_params = 'file';
	var menu_id = $('input[name=current_menu_id]').val();
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id, file);

	sendAjaxPostRequest(empUserListConfig["uploadUrl"], formData, ajaxEmpUser_successHandler, ajaxEmpUser_errorHandler, type);
}*/

function ajaxDefaultSucCallback(data, dataType, actionType){
	
}

function ajaxDefaultErrCallback(XMLHttpRequest, textStatus, errorThrown, actionType){
	alert("ajax request Error : " + textStatus);
}


// 사원 ajax call - 성공
function ajaxEmpUser_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		alert("성공적으로 등록되었습니다.");
		moveEmpUserList();
	}
}

// 사원 ajax call - 실패
function ajaxEmpUser_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){	
	alert("엑셀 업로드에 실패하였습니다. 셀값에 빈공간이 있거나 값이 제대로 들어가있는지 확인해 주십시오." + textStatus);
}

function exDown(){
	$("#exdown").submit();
}



$(function(){
	$('input[type=text]').attr("onkeydown","javascript:if(event.keyCode==13){moveEmpUserList();}");
});

