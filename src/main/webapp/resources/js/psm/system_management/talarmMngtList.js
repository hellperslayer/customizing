/**
 * TalarmMngtList Javascript
 * @author yjyoo
 * @since 2015. 04. 21
 */



var $clone;

$(document).ready(function() {
	$clone = $("#talarmListForm").clone();
});

// 페이지 이동
function goPage(num){
	$clone.attr("action", talarmListConfig["listUrl"]);
	$clone.find("input[name=page_num]").val(num);
	$clone.attr("style", "display: none;");
	$('body').append($clone);
	$clone.submit();
}

//엑셀 업로드 팝업 기능
function excelUpLoadList(){
	var excelUp = $(".popup_wrap");
	excelUp.attr("style", "display: block;");
}

function closeUpload(){
	var excelUp = $(".popup_wrap");
	var file = $("#file");
	excelUp.attr("style", "display: none;");
	file.replaceWith( file.clone(true));
}

function excelTalarmUpLoad(){
	
	var data = $("#result").val();
	
		var options = {
				
				success : function(data) {
					var checkdata = "\"true\"";
					if(checkdata == data){
						alert("성공적으로 업로드 되었습니다!");
						moveTalarmList();
					}else if(checkdata != data){
						alert("업로드에 실패하였습니다. (엑셀파일의 셀안에 공백이 들어있거나 필수 입력정보를 입력하였는지, 혹은 기존 소속명 외의 소속명이 포합되어 있는지 확인해주십시오.)");
					}
					
				},
				error : function(error) {
					alert("요청 처리 중 오류가 발생하였습니다.");
				}
		  };
		    
    $("#fileForm").ajaxSubmit(options);

    return false;
}


// 검색
function moveTalarmList() {
	$("#talarmListForm").attr("action", talarmListConfig["listUrl"]);
	$("#talarmListForm input[name=page_num]").val('1');
	$("#talarmListForm").submit();
}

function searchTalarmList() {
	$("#talarmListForm").attr("action", talarmListConfig["listUrl"]);
	$("#talarmListForm input[name=isSearch]").val('Y');
	$("#talarmListForm input[name=page_num]").val('1');
	$("#talarmListForm").submit();
}

// 상세 페이지 이동 (신규, 수정)
function findTalarmMngtOne(emp_user_id){
	$("#talarmListForm").attr("action", talarmListConfig["detailUrl"]);
	$('#talarmListForm').appendHiddenInput('emp_user_id', emp_user_id);
	$("#talarmListForm").submit();
}

// 엑셀 다운로드
function excelTalarmList() {
	$("#talarmListForm").attr("action", talarmListConfig["downloadUrl"]);
	$("#talarmListForm").submit();
}

/*function sendAjaxTalarm(){
	var form = $("#fileForm");
	var type = "upload";
	var formData = new FormData();
	formData.append("file",$("#f2 input[name = file]")[0].files[0]);
	var log_message_title = '엑셀업로드';
	var log_action = 'INSERT';
	var log_message_params = 'file';
	var menu_id = $('input[name=current_menu_id]').val();
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id, file);

	sendAjaxPostRequest(talarmListConfig["uploadUrl"], formData, ajaxTalarm_successHandler, ajaxTalarm_errorHandler, type);
}*/

function ajaxDefaultSucCallback(data, dataType, actionType){
	
}

function ajaxDefaultErrCallback(XMLHttpRequest, textStatus, errorThrown, actionType){
	alert("ajax request Error : " + textStatus);
}


// 사원 ajax call - 성공
function ajaxTalarm_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		alert("성공적으로 등록되었습니다.");
		moveTalarmList();
	}
}

// 사원 ajax call - 실패
function ajaxTalarm_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){	
	alert("엑셀 업로드에 실패하였습니다. 셀값에 빈공간이 있거나 값이 제대로 들어가있는지 확인해 주십시오." + textStatus);
}

function exDown(){
	$("#exdown").submit();
}



$(function(){
	$('input[type=text]').attr("onkeydown","javascript:if(event.keyCode==13){moveTalarmList();}");
});

