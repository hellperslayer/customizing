/**
 * GroupCodeMngtDetail Javascript
 */

// 그룹코드 추가
function addGroupCode(){
	var group_code_id = $('input[name=group_code_id]').val();
	var group_code_name = $('input[name=group_code_name]').val();
	var use_flag = $('input[name=use_flag]:checked').val();
	
	if(group_code_id == '') {
		alert("그룹코드를 입력해주세요");
		$('input[name=group_code_id]').focus();
		return false;
	}
	if(group_code_name == '') {
		alert("그룹코드명을 입력해주세요");
		$('input[name=group_code_name]').focus();
		return false;
	}
	if(use_flag == '') {
		alert("사용여부를 선택해주세요");
		return false;
	}
	
	sendAjaxGroupCode("add");
}

// 그룹코드 수정
function saveGroupCode(){
	var group_code_id = $('input[name=group_code_id]').val();
	var group_code_name = $('input[name=group_code_name]').val();
	var use_flag = $('input[name=use_flag]:checked').val();
	
	if(group_code_id == '') {
		alert("그룹코드를 입력해주세요");
		$('input[name=group_code_id]').focus();
		return false;
	}
	if(group_code_name == '') {
		alert("그룹코드명을 입력해주세요");
		$('input[name=group_code_name]').focus();
		return false;
	}
	if(use_flag == '') {
		alert("사용여부를 선택해주세요");
		return false;
	}
	
	sendAjaxGroupCode("save");
}

// 그룹코드 삭제
function removeGroupCode(){
	if(confirm('삭제하시겠습니까?')) {
		sendAjaxGroupCode("remove");
	}
}

// 그룹코드 ajax call
function sendAjaxGroupCode(type){
	var $form = $("#groupCodeDetailForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'group_code_id,group_code_name';
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'add') {
		log_message_title = '그룹코드등록';
		log_action = 'INSERT';
	} else if (type == 'save') {
		log_message_title = '그룹코드수정';
		log_action = 'UPDATE';
	} else if (type == 'remove') {
		log_message_title = '그룹코드삭제';
		log_action = 'REMOVE';
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(groupCodeConfig[type + "Url"], $form.serialize(), ajaxGroupCode_successHandler, ajaxGroupCode_errorHandler, type);
}

// 그룹코드 ajax call - 성공
function ajaxGroupCode_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			alert("세션이 만료되었습니다.");
			location.href = groupCodeConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	} else {
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}
		
		moveGroupCodeList();
	}
}

// 그룹코드 ajax call - 실패
function ajaxGroupCode_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	log_message += groupCodeLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("그룹코드 등록 실패하였습니다. : " + textStatus);
			break;
		case "save":
			alert("그룹코드 수정 실패하였습니다. : " + textStatus);
			break;
		case "remove":
			alert("그룹코드 삭제 실패하였습니다. : " + textStatus);
			break;
	}
}

// 목록으로 이동
function moveGroupCodeList(){
	$("#groupCodeDetailForm").attr("action",groupCodeConfig["listUrl"]);
	$("#groupCodeDetailForm").submit();
}
