
var resDate = "";

function createSummary(){ 
	
	var summary_type = $("#summary_type").val();
	
	if (resDate == "") {
		$("#create_btn").attr("disabled", "disabled");
		var start = $("#search_fr").val();
		resDate = start;
		$("#listDiv").html("<b>===== 시작 (summary_" + summary_type + ") =====</b>");
		$('#loading').show();
	} else {
		var end = $("#search_to").val();
		//var arr2 = end.split("-");
		//var date2 = new Date(arr2[0], arr2[1], arr2[2]);
		//var endDate = date2.getFullYear() + "-" + cfSetAddZero(date2.getMonth()) + "-" + cfSetAddZero(date2.getDate());
		
		if(end == resDate) {
			var html = $("#listDiv").html();
			var resHtml = html + "<br><b>===== 종료 (summary_" + summary_type + ") =====</b>"; 
			$("#listDiv").html(resHtml);
			$("#listDiv").scrollTop($("#listDiv").get(0).scrollHeight);
			$("#create_btn").removeAttr("disabled");
			alert("완료되었습니다.");
			$('#loading').hide();
			resDate = "";
			return;
		} else {
			var arr1 = resDate.split("-");
			var date1 = new Date(arr1[0], parseInt(arr1[1])-1, arr1[2]);
			date1.setDate(date1.getDate() + 1);
			resDate = date1.getFullYear() + "-" + cfSetAddZero(date1.getMonth() + 1) + "-" + cfSetAddZero(date1.getDate());
		}
	}
	
	$("#listDiv").html($("#listDiv").html() + "<br>" + resDate)
	
	$.ajax({ 
		type: 'POST',
		url: rootPath + '/summaryMngt/createSummary.html',
		data: {
			type : summary_type,
			procdate : resDate
		},
		success: function(data) { 
			var resHtml = $("#listDiv").html() + " 완료 (insert: " + data.cnt + ")";
			$("#listDiv").html(resHtml);
			$("#listDiv").scrollTop($("#listDiv").get(0).scrollHeight);
		},
		complete: createSummary
		//,timeout: 300000 
	}); 
}

function createSummaryAll(){ 
	
	if (resDate == "") {
		$("#create_btn").attr("disabled", "disabled");
		var start = $("#search_fr").val();
		resDate = start;
		$("#listDiv").html("<b>===== 시작 (summary_daily) =====</b>");
		$('#loading').show();
	} else {
		var end = $("#search_to").val();
		//var arr2 = end.split("-");
		//var date2 = new Date(arr2[0], arr2[1], arr2[2]);
		//var endDate = date2.getFullYear() + "-" + cfSetAddZero(date2.getMonth()) + "-" + cfSetAddZero(date2.getDate());
		
		if(end == resDate) {
			var html = $("#listDiv").html();
			var resHtml = html + "<br><b>===== 종료 (summary_daily) =====</b>"; 
			$("#listDiv").html(resHtml);
			$("#listDiv").scrollTop($("#listDiv").get(0).scrollHeight);
			$("#create_btn").removeAttr("disabled");
			alert("완료되었습니다.");
			$('#loading').hide();
			resDate = "";
			return;
		} else {
			var arr1 = resDate.split("-");
			var date1 = new Date(arr1[0], parseInt(arr1[1])-1, arr1[2]);
			date1.setDate(date1.getDate() + 1);
			resDate = date1.getFullYear() + "-" + cfSetAddZero(date1.getMonth() + 1) + "-" + cfSetAddZero(date1.getDate());
		}
	}
	
	$("#listDiv").html($("#listDiv").html() + "<br>" + resDate)
	var summary_type = $("#summary_type").val();
	$.ajax({ 
		type: 'POST',
		url: rootPath + '/summaryMngt/createSummary'+summary_type+'.html',
		data: {
			procdate : resDate
		},
		success: function(data) { 
			var resHtml = $("#listDiv").html() + " 완료 (insert: " + data.cnt + ")";
			$("#listDiv").html(resHtml);
			$("#listDiv").scrollTop($("#listDiv").get(0).scrollHeight);
		},
		complete: createSummaryAll
		//,timeout: 300000 
	}); 
}

function createSummaryMonth() {
	
	$("#create_btn").attr("disabled", "disabled");
	$("#listDiv").html("<b>===== 시작 (summary_month) =====</b><br>");
	$('#loading').show();
	
	var year = $("#sel_year").val();
	var month = $("#sel_month").val(); 
	var lastDay = (new Date(year, month, 0)).getDate();
	
	var startDate = year + cfSetAddZero(month) + "01";
	var endDate = year + cfSetAddZero(month) + lastDay;
	
	$.ajax({ 
		type: 'POST',
		url: rootPath + '/summaryMngt/createSummaryMonth.html',
		data: {
			startDate : startDate,
			endDate : endDate
		},
		success: function(data) { 
			var resHtml = $("#listDiv").html() + " 완료 (insert: " + data.cnt + ")";
			resHtml = resHtml + "<br><b>===== 종료 (summary_month) =====</b>"; 
			$("#listDiv").html(resHtml);
			$("#listDiv").scrollTop($("#listDiv").get(0).scrollHeight);
			$("#create_btn").removeAttr("disabled");
			alert("완료되었습니다.");
			$('#loading').hide();
		}
	});
}

function createSummaryNew(){ 
	var type = $("#summary_type").val();
	if (type == "month") {
		createSummaryMonth();
	} else if(type == "Rule") {
		createSummaryRule();
	} else {
		createSummaryAll();
	}
}

function createSummaryRule(){ 
	$("#create_btn").attr("disabled", "disabled");
	var search_from = $("#search_fr").val();
	var search_to = $("#search_to").val();
	$("#listDiv").html("<b>===== 시작  =====</b>");
	$('#loading').show();
	
	$.ajax({ 
		type: 'POST',
		url: rootPath + '/summaryMngt/createSummaryRule.html',
		data: {
			search_from : search_from,
			search_to : search_to
		},
		timeout : 30000,
		success: function(data) { 
			console.log(data);
			var resHtml = $("#listDiv").html();
			resHtml = resHtml + "<br><b>===== 종료 (summary_month) =====</b>"; 
			$("#listDiv").html(resHtml);
			$("#listDiv").scrollTop($("#listDiv").get(0).scrollHeight);
			$('#loading').hide();
			$("#create_btn").removeAttr("disabled");
		},
		error: function(request, status, error) {
			console.log(request.status + ' ' + request.responseText + ' ' + error);
			var resHtml = $("#listDiv").html();
			resHtml = resHtml + "<br><b>===== 시간이 오래 걸리고 있습니다. <br>추후 비정상위험 리스트에서 확인해주세요. =====</b>"; 
			$("#listDiv").html(resHtml);
			$('#loading').hide();
		}
	}); 
}


function cfSetAddZero(target) {     
    
    var num = parseInt(target);
    var str = num > 9 ? num : "0" + num;
    
    return str.toString();
}

function setSummaryType(val) {
	if (val == "month") {
		$("#div_day").css("display", "none");
		$("#div_month").css("display", "block");
	} else {
		$("#div_day").css("display", "block");
		$("#div_month").css("display", "none");
	}
}

function refreshReportList(val){
	var report_type = $(val).val();
	$.ajax({ 
		type: 'POST',
		url: rootPath + '/report/uploadReportList.html',
		data: {
			report_type : report_type
		},
		success: function(data) { 
			$('#tableInclude').html(data);
		}
	});
}

function upLoadReportRemove(report_id){
	$.ajax({ 
		type: 'POST',
		url: rootPath + '/report/uploadReportRemove.html',
		dataType:"JSON",
		data: {
			report_id : report_id
		},
		success: function(data) {
			if(data == "success"){
				alert('삭제되었습니다.');
				refreshReportList($('#report_type'));
			}else{
				alert('오류가 발생했습니다.')
			}
		}
	});
}