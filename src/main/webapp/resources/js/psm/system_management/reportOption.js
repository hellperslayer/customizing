
function updGeneral() {
	var $form = $("#optionForm");
	$.ajax({
		type: 'POST',
		url: rootPath + '/reportOption/updGeneral.html',
		data: $form.serialize(),
		success: function(data) {
			alert("수정되었습니다.");
			moveSystemList();
		}
	});

}



function moveSystemList() {
	$("#reloadForm").submit();
}





$(function() {
	AuthorCount();
	$('#reportOptionForm').ajaxForm({
		success: function(result) {
			alert("성공적으로 등록되었습니다.");
			movereportOption();
		}
	});
});



$(function() {
	$('#reportOptionForm2').ajaxForm({
		success: function(result) {
			alert("성공적으로 등록되었습니다.");
			movereportOption();
		}
	});
});



$(document).ready(function() {
	findDefaultDesc();
});


function check_1() {
	var file = reportOptionForm.filename.value;
	if (file != "") {
		var fileExt = file.substring(file.lastIndexOf(".") + 1);
		var reg = /gif|jpg|jpeg|png/i;	// 업로드 가능 확장자.
		if (reg.test(fileExt) == false) {
			alert("첨부파일은 gif, jpg, png로 된 이미지만 가능합니다.");
			return false;
		} else {
			return true;
		}
	} else {
		alert("이미지를 등록해 주세요.");
		return false;
	}
}



function check_2() {
	var file = reportOptionForm2.filename2.value;
	if (file != "") {
		var fileExt = file.substring(file.lastIndexOf(".") + 1);
		var reg = /gif|jpg|jpeg|png/i;	// 업로드 가능 확장자.
		if (reg.test(fileExt) == false) {
			alert("첨부파일은 gif, jpg, png로 된 이미지만 가능합니다.");
			return false;
		} else {
			return true;
		}
	} else {
		alert("이미지를 등록해 주세요.");
		return false;
	}
}


function addFile() {

	var file = reportOptionForm.filename.value;
	if (file != "") {
		var fileExt = file.substring(file.lastIndexOf(".") + 1);
		var reg = /gif|jpg|jpeg|png/i;	// 업로드 가능 확장자.
		if (reg.test(fileExt) == false) {
			alert("첨부파일은 gif, jpg, png로 된 이미지만 가능합니다.");
			return;
		}
	}

	var confirmVal = confirm("등록하시겠습니까?");
	if (confirmVal) {
		sendAjaxRegisterImageLogo();
	} else {
		return false;
	}
	//$("#reportOptionForm").attr("action", reportOptionConfig["addFileUrl"]);
	//$("#reportOptionForm").submit();
}


function sendAjaxRegisterImageLogo() {
	/*$.ajax({
		type: 'POST',
		url: rootPath + '/reportOption/upload.html',
		data: {
		},
		success: function() {
			alert("성공적으로 등록되었습니다.");
			movereportOption();
		}
	});*/
	$('#reportOptionForm').ajaxForm({
		url: rootPath + '/reportOption/add.html',
		success: function(result) {
			alert("성공적으로 등록되었습니다.");
		}
	});

	//$("#reportOptionForm").submit();
}

function movereportOption() {
/*	$("#listForm").attr("action", reportOptionConfig["listUrl"]);*/
	$("#listForm").submit();
}



function useLogoImage(idx, type) {
	var text;
	if (type == 1)
		text = "사용을 해제하시겠습니까?";
	else if (type == 2)
		text = "사용하시겠습니까?";

	if (confirm(text)) {
		$.ajax({
			type: 'POST',
			url: rootPath + '/reportOption/useLogoImage.html',
			data: {
				idx: idx,
				type: type
			},
			success: function(data) {
				alert("변경되었습니다.");
				movereportOption();
			}
		});
	}
}

function useLogoImage2(idx, type) {
	var text;
	if (type == 1)
		text = "사용을 해제하시겠습니까?";
	else if (type == 2)
		text = "사용하시겠습니까?";

	if (confirm(text)) {
		$.ajax({
			type: 'POST',
			url: rootPath + '/reportOption/useLogoImage2.html',
			data: {
				idx: idx,
				type: type
			},
			success: function(data) {
				alert("변경되었습니다.");
				movereportOption();
			}
		});
	}
}

function deleteLogoImage(idx) {

	if (confirm("삭제하시겠습니까?")) {
		$.ajax({
			type: 'POST',
			url: rootPath + '/reportOption/deleteLogoImage.html',
			data: {
				idx: idx
			},
			success: function(data) {
				alert("삭제되었습니다.");
				movereportOption();
			}
		});
	}
}

function AuthorCount() {
	var count = $('#countSelect').val();
	$('#html_source').html(
		'<TABLE border="1" id="preview" cellspacing="0" cellpadding="0" style="border-collapse:collapse; border: 1px solid black">'
		+ '<TR id="authorNameTr">'
		+ '<TD rowspan="2" id="authorNameFirst" valign="middle" style="width: 27px; height: 114px; padding: 1.4pt 5.1pt 1.4pt 6pt;">'
		+ '<P><SPAN>결</SPAN></P>'
		+ '<P STYLE="margin: 0px"><SPAN>재</SPAN></P>'
		+ '</TD>'
		+ '</TR>'
		+ '<TR id="authorMarkTr">'
		+ '</TR>'
		+ '</TABLE>');
	$('.authorName').prop('disabled', true);
	$('#preview').css('display', '');
	$(".authorNameTd").remove();
	$(".authorMarkTd").remove();
	switch (count) {
		case '3':
			$('#authorName3').removeAttr('disabled');
			tableAddContent($('#authorName3').val());
		case '2':
			$('#authorName2').removeAttr('disabled');
			tableAddContent($('#authorName2').val());
		case '1':
			$('#authorName1').removeAttr('disabled');
			tableAddContent($('#authorName1').val());
			break;
		default:
			$('#preview').css('display', 'none');
	}
}

function tableAddContent(name) {
	$('#authorNameFirst').after(
		'<TD valign="middle" class="authorNameTd">'
		+ '<P STYLE="margin: 0px; text-align: center;"><SPAN>' + name + '</SPAN></P>'
		+ '</TD>');
	$('#authorMarkTr').append(
		'<TD valign="middle" class="authorMarkTd" style="width: 91px; height: 79px; padding: 1.4pt 5.1pt 1.4pt 5.1pt;">'
		+ '<P><SPAN>&nbsp;</SPAN></P>'
		+ '</TD>');
}

function addAuthorizeLine() {
	AuthorCount();
	$.ajax({
		type: 'POST',
		url: rootPath + '/reportOption/addAuthorizeLine.html',
		dataType: 'json',
		data: {
			count: $('#countSelect').val(),
			name1: $('#authorName1').val(),
			name2: $('#authorName2').val(),
			name3: $('#authorName3').val(),
			html_source: $('#html_source').html()
		},
		success: function(data) {
			if (data == 'success') {
				alert("변경되었습니다.")
			} else {
				alert("오류가 발생했습니다.");
			}
		}
	});
}




function findDefaultDesc() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/reportOption/findDefaultDesc.html',
		dataType: 'json',
		data: {
			report_code: $('#report_code').val()
		},
		success: function(data) {
			$('#defaultdesc').val(data);
		}
	});
}

function addDefaultDesc() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/reportOption/addDefaultDesc.html',
		dataType: 'json',
		data: {
			report_code: $('#report_code').val()
			, description: $('#defaultdesc').val()
		},
		success: function(data) {
			if (data == 'success') {
				alert("변경되었습니다.")
			} else {
				alert("오류가 발생했습니다.");
			}
		}
	});
}




function tabNo() {
   setTimeout(function() {
      var classVal = $("#tabIndex1").attr('class');
      if(classVal=='active') {
         $("#moveForm input[name=tab_flag]").val('1');
      }
      var classVal2 = $("#tabIndex2").attr('class');
      if(classVal2=='active') {
         $("#moveForm input[name=tab_flag]").val('2');
      }
      var classVal3 = $("#tabIndex3").attr('class');
      if(classVal3=='active') {
         $("#moveForm input[name=tab_flag]").val('3');
      }
      var classVal4 = $("#tabIndex4").attr('class');
      if(classVal4=='active') {
         $("#moveForm input[name=tab_flag]").val('4');
      }
   }, 1000);
   
}



function updReportApproval(system_seq, idx) {
	$.ajax({
		type: 'POST',
		url: rootPath + '/reportOption/updReportApproval.html',
		dataType: 'json',
		data: {
			system_seq: system_seq
			, first_auth: $('#first_auth' + idx).val()
			, second_auth: $('#second_auth' +idx).val()
			, third_auth: $('#third_auth' + idx).val()
		},
		success: function(data) {
			if (data == 'success') {
				alert("변경되었습니다.")
			} else {
				alert("오류가 발생했습니다.");
			}
		}
	});
}


function wholeUpdReportApproval() {

	var $form = $("#approvalForm");
	$.ajax({
		type: 'POST',
		url: rootPath + '/reportOption/wholeUpdReportApproval.html',
		data: $form.serialize(),
		success: function(data) {
			alert("수정되었습니다.");
			moveApproval();
		}
	});

}


function moveApproval() {
	$("#appForm").submit();
}


