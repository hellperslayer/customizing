//IP테이블 추가
function addIpPerm(){
	var admin_id = $('select[name=admin_id]').val();
	var ip_content = $('input[name=ip_content]').val();
	var use_flag = $('input[name=use_flag]:checked').val();
//	alert("use_flag:"+use_flag);

	if(admin_id == '') {
		alert("관리자를 선택해주세요");
		$('select[name=admin_id]').focus();
		return false;
	}
	if(ip_content == '') {
		alert("IP를 입력해주세요");
		$('input[name=ip_content]').focus();
		return false;
	}
	if(use_flag == '') {
		alert("사용여부를 선택해주세요");
		return false;
	}
	checkFormIp(ip_content,"add");
}
function checkFormIp(ip,type){
	$.ajax({	
		type:'POST',
		url:rootPath + '/ipPerm/checkIpForm.html',
		data:{
			ip:ip
		},
		success: function(data){
			var flag=eval(data);	
//			alert("flag:"+flag);
			if(flag == true){
				sendAjaxIpPerm(type);
			}else{
				alert("IP 형식이 올바르지 않습니다");
				return false;
			}
		},
		error:function(e){
			alert("Error:"+e);
		}
	});	
}
// IP테이블 수정
function saveIpPerm(){
	var ip_content = $('input[name=ip_content]').val();
	var use_flag = $('input[name=use_flag]:checked').val();
	
	if(ip_content == '') {
		alert("IP를 입력해주세요");
		$('input[name=ip_content]').focus();
		return false;
	}

	if(use_flag == '') {
		alert("사용여부를 선택해주세요");
		return false;	}
	
	checkFormIp(ip_content,"save");
}

// IP테이블 삭제
function removeIpPerm(){
	if(confirm('삭제하시겠습니까?')) {
		sendAjaxIpPerm("remove");
	}
}

// 그룹코드 ajax call
function sendAjaxIpPerm(type){
	var $form = $("#ipPermDetailForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'ip_seq,ip_content,use_flag,admin_id';
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'add') {
		log_message_title = 'IP등록';
		log_action = 'INSERT';
	} else if (type == 'save') {
		log_message_title = 'IP수정';
		log_action = 'UPDATE';
	} else if (type == 'remove') {
		log_message_title = 'IP삭제';
		log_action = 'REMOVE';
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(ipPermConfig[type + "Url"], $form.serialize(), ajaxIpPerm_successHandler, ajaxIpPerm_errorHandler, type);
}

// 그룹코드 ajax call - 성공
function ajaxIpPerm_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			alert("세션이 만료되었습니다.");
			location.href = ipPermConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	} else {
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}
		
		moveIpPermList();
	}
}

// 그룹코드 ajax call - 실패
function ajaxIpPerm_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	log_message += ipPermLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("IP 등록 실패하였습니다. : " + textStatus);
			break;
		case "save":
			alert("IP 수정 실패하였습니다. : " + textStatus);
			break;
		case "remove":
			alert("IP 삭제 실패하였습니다. : " + textStatus);
			break;
	}
}

// 목록으로 이동
function moveIpPermList(){
	$("#ipPermListForm").attr("action",ipPermConfig["listUrl"]);
	$("#ipPermListForm").submit();
}
