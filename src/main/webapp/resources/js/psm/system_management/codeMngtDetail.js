/**
 * CodeMngtDetail Javascript
 */

// 코드 등록
function addCode(){
	var code_id = $('input[name=code_id]').val();
	var code_name = $('input[name=code_name]').val();
	var code_type = $('input[name=code_type]:checked').val();
	var use_flag = $('input[name=use_flag]:checked').val();
	
	if(code_id == '') {
		alert("코드를 입력해주세요");
		$('input[name=code_id]').focus();
		return false;
	}
	if(code_name == '') {
		alert("코드명을 입력해주세요");
		$('input[name=code_name]').focus();
		return false;
	}
	if(code_type == '') {
		alert("코드타입을 선택해주세요");
		return false;
	}
	if(use_flag == '') {
		alert("사용여부를 선택해주세요");
		return false;
	}
	
	sendAjaxCode("add");
}

// 코드 수정
function saveCode(){
	var code_id = $('input[name=code_id]').val();
	var code_type = $('input[name=code_type]:checked').val();
	var use_flag = $('input[name=use_flag]:checked').val();
	
	if(code_id == '') {
		alert("코드를 입력해주세요");
		$('input[name=code_id]').focus();
		return false;
	}
	if(code_type == '') {
		alert("코드타입을 선택해주세요");
		return false;
	}
	if(use_flag == '') {
		alert("사용여부를 선택해주세요");
		return false;
	}
	
	sendAjaxCode("save");
}

// 코드 삭제
function removeCode(){
	if(confirm("삭제하시겠습니까?")) {
		sendAjaxCode("remove");
	}
}

// 코드 ajax call
function sendAjaxCode(type){
	var $form = $("#codeDetailForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'group_code_id,code_id,code_name';
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'add') {
		log_message_title = '코드등록';
		log_action = 'INSERT';
	} else if (type == 'save') {
		log_message_title = '코드수정';
		log_action = 'UPDATE';
	} else if (type == 'remove') {
		log_message_title = '코드삭제';
		log_action = 'REMOVE';
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);

	sendAjaxPostRequest(codeConfig[type + "Url"], $form.serialize(), ajaxCode_successHandler, ajaxCode_errorHandler, type);
}

// 코드 ajax call - 성공
function ajaxCode_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			alert("세션이 만료되었습니다.");
			location.href = codeConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	} else{
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}
		
		moveGroupCodeList();
	}
}

// 코드 ajax call - 실패
function ajaxCode_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){
	
	log_message += codeLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("코드 등록 실패하였습니다." + textStatus);
			break;
		case "save":
			alert("코드 수정 실패하였습니다." + textStatus);
			break;
		case "remove":
			alert("코드 삭제 실패하였습니다." + textStatus);
			break;
	}
}

//목록으로 이동
function moveGroupCodeList(){
	$("#codeDetailForm").attr("action",codeConfig["listUrl"]);
	$("#codeDetailForm").submit();
}

