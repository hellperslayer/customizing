//테마 수정

// IP테이블 수정
function saveTheme(){
	
	if(confirm('수정하시겠습니까?')) {
		sendAjaxTheme("save");
	}
	
}

// 그룹코드 ajax call
function sendAjaxTheme(type){
	var $form = $("#themeChangeForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'ip_seq,ip_content,use_flag';
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'add') {
		log_message_title = '등록';
		log_action = 'INSERT';
	} else if (type == 'save') {
		log_message_title = '수정';
		log_action = 'UPDATE';
	} else if (type == 'remove') {
		log_message_title = '삭제';
		log_action = 'REMOVE';
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(changeThemeConfig[type + "Url"], $form.serialize(), ajaxTheme_successHandler, ajaxTheme_errorHandler, type);
}

// 그룹코드 ajax call - 성공
function ajaxTheme_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			alert("세션이 만료되었습니다.");
			location.href = changeThemeConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	} else {
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}
		
		moveThemeList();
	}
}

// 그룹코드 ajax call - 실패
function ajaxTheme_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){
	
	switch(actionType){
		case "add":
			alert("테마 수정이 실패하였습니다. : " + textStatus);
			break;
		case "save":
			alert("테마 수정이 실패하였습니다. : " + textStatus);
			break;
		case "remove":
			alert("테마 수정이 실패하였습니다. : " + textStatus);
			break;
	}
}

// 목록으로 이동
function moveThemeList(){
	$("#themeChangeForm").attr("action",changeThemeConfig["listUrl"]);
	$("#themeChangeForm").submit();
}