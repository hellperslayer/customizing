/**
 * AdminUserMngtList Javascript
 * @author yjyoo
 * @since 2015. 04. 17
 */

var $clone;

$(document).ready(function() {
	$clone = $("#adminUserListForm").clone();
});

// 페이지 이동
function goPage(num){
	$clone.attr("action",adminUserListConfig["listUrl"]);
	$clone.find("input[name=page_num]").val(num);
	$clone.attr("style", "display: none;");
	$('body').append($clone);
	$clone.submit();
}

// 검색
function moveAdminUserList() {
	$("#adminUserListForm").attr("action", adminUserListConfig["listUrl"]);
	$("#adminUserListForm input[name=isSearch]").val('Y');
	$("#adminUserListForm input[name=page_num]").val('1');
	$("#adminUserListForm").submit();
}

// 상세 페이지 이동 (신규, 수정)
function findAdminUserDetail(admin_user_id){
	$("#adminUserListForm").attr("action", adminUserListConfig["detailUrl"]);
	$("#adminUserListForm").appendHiddenInput('admin_user_id', admin_user_id);
	$("#adminUserListForm").submit();
}

// 엑셀 다운로드
function excelAdminUserList() {
	var $form = $("#adminUserListForm");
	var log_message_params = '';
	var menu_id = $('input[name=current_menu_id]').val();
	var log_message_title = '관리자관리 엑셀 다운로드';
	var log_action = 'EXCEL DOWNLOAD';
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	$("#adminUserListForm").attr("action",adminUserListConfig["downloadUrl"]);
	$("#adminUserListForm").submit();
}
