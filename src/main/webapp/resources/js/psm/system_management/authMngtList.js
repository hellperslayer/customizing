/**
 * AuthMngtList Javascript
 * @author yjyoo
 * @since 2015. 04. 17
 */

// 페이지 이동
function goPage(num){
	$("#authForm input[name=page_num]").val(num);
	moveAuthList();
}

// 권한 리스트로 이동
function moveAuthList(){
	$("#authForm").attr("action",authListConfig["listUrl"]);
	$("#authForm").submit();
}

// 검색
function searchAuthList() {
	$("#authForm input[name=page_num]").val("1");
	$("#authForm input[name=isSearch]").val("Y");
	$("#authForm").attr("action",authListConfig["listUrl"]);
	$("#authForm").submit();
}

// 권한 상세로 이동
function moveDetail(authId){
	$("#authForm input[name=auth_id]").val(authId);
	$("#authForm").attr("action",authListConfig["detailUrl"]);
	$("#authForm").submit();
}


//엑셀 다운로드
function excelAuthList() {
	$("#authForm").attr("action",authListConfig["downloadUrl"]);
	$("#authForm").submit();
}