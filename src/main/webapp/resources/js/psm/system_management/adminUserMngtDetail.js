/**
 * AdminUserDetail Javascript
 */

// 관리자 추가
function addAdminUser(){
	var cnt = $('input:checkbox[name=auth_id]:checked').length;
	var admin_user_id =htmlspecialchars($('input[name=admin_user_id]').val());
	var admin_user_name =htmlspecialchars($('input[name=admin_user_name]').val());
	var email_address = htmlspecialchars($('input[name=email_address]').val());
	//var password =htmlspecialchars( $('input[name=password]').val());
	//var password_check =htmlspecialchars($('input[name=password_check]').val());
	if(admin_user_id == '') {
		alert("관리자ID를 입력해주세요");
		$('input[name=admin_user_id]').focus();
		return false;
	}
	if(admin_user_name == '') {
		alert("관리자명을 입력해주세요");
		$('input[name=admin_user_name]').focus();
		return false;
	}
	
	/*if(password == '') {
		alert("비밀번호를 입력해주세요");
		$('input[name=password]').focus();
		return false;
	}
	if(password_check == '') {
		alert("비밀번호확인을 입력해주세요");
		$('input[name=password_check]').focus();
		return false;
	}*/
	
	var authIds = new Array();
	//authIds.push('01');
	$("#authListDiv input[name=auth_id]").each(function(){
		if($(this).is(":checked")){authIds.push($(this).val());}
	});
	$("#adminUserDetailForm input[name=auth_ids]").val(authIds.join(","));
	
	/*if(authIds.length < 0) {
		alert("생성하시는 관리자의 관리시스템을 선택하여주세요.");
		return false;
	}*/
	if(cnt <= 0) {
		alert("생성하시는 관리자의 관리시스템을 선택하여주세요.");
		return false;
	}
	if($("#auth_id").val() == 'AUTH00004' || $("#auth_id").val() == 'AUTH00005'){
		if(email_address == '' || email_address.indexOf('@') == -1){
			alert("올바른 이메일을 입력해주세요.");
			$('input[name=email_address]').focus();
			return false;
		}
		
	}
	
	/*if(checkPassword()){
		sendAjaxAdminUser("add");	
	}else{
		//alert("비밀번호를 확인해주세요");
	}*/
	sendAjaxAdminUser("add");
}

// 관리자 수정
function saveAdminUser(){
	var cnt = $('input:checkbox[name=auth_id]:checked').length;
	var admin_user_id =htmlspecialchars($('input[name=admin_user_id]').val());
	var admin_user_name =htmlspecialchars($('input[name=admin_user_name]').val());
	var email_address = htmlspecialchars($('input[name=email_address]').val());
	//var password =htmlspecialchars( $('input[name=password]').val());
	//var password_check =htmlspecialchars($('input[name=password_check]').val());
	if(admin_user_id == '') {
		alert("관리자ID를 입력해주세요");
		$('input[name=admin_user_id]').focus();
		return false;
	}
	if(admin_user_name == '') {
		alert("관리자명을 입력해주세요");
		$('input[name=admin_user_name]').focus();
		return false;
	}
	/*if(password != '') {
		if(password_check == '') {
			alert("비밀번호확인을 입력해주세요");
			$('input[name=password_check]').focus();
			return false;
		}
	}
	if(password_check != '') {
		if(password == '') {
			alert("비밀번호를 입력해주세요");
			$('input[name=password]').focus();
			return false;
		}
	}*/
	
	/*if(authIds.length < 0) {
		alert("생성하시는 관리자의 관리시스템을 선택하여주세요.");
		return false;
	}*/
	
	if(cnt <= 0) {
		alert("생성하시는 관리자의 관리시스템을 선택하여주세요.");
		return false;
	}
	
	var authIds = new Array();
	//authIds.push('01');
	$("#authListDiv input[name=auth_id]").each(function(){
		if($(this).is(":checked")){authIds.push($(this).val());}
	});
	$("#adminUserDetailForm input[name=auth_ids]").val(authIds.join(","));
	
	if($("#auth_id").val() == 'AUTH00004' || $("#auth_id").val() == 'AUTH00005'){
		if(email_address == '' || email_address.indexOf('@') == -1){
			alert("올바른 이메일을 입력해주세요.");
			$('input[name=email_address]').focus();
			return false;
		}
		
	}
	/*if(checkPassword()){
		sendAjaxAdminUser("save");	
	}else{
		//alert("비밀번호를 확인해주세요");
	}*/
	sendAjaxAdminUser("save");
}

// 관리자 삭제
function removeAdminUser(){
	if(confirm("삭제하시겠습니까?")) {
		sendAjaxAdminUser("remove");
	}
}

// 관리자 ajax call
function sendAjaxAdminUser(type){
	var $form = $("#adminUserDetailForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'admin_user_id,admin_user_name';
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'add') {
		log_message_title = '관리자등록';
		log_action = 'INSERT';
	} else if (type == 'save') {
		var password = $("#adminUserDetailForm input[name=password]").val();
		if(password != null && password != '') {
			log_message_title = '관리자수정(비밀번호수정)';
		}
		else {
			log_message_title = '관리자수정';
		}
		log_action = 'UPDATE';
	} else if (type == 'remove') {
		log_message_title = '관리자삭제';
		log_action = 'REMOVE';
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(adminUserDetailConfig[type + "Url"],$form.serialize(), ajaxAdminUser_successHandler, ajaxAdminUser_errorHandler, type);
}

// 관리자 ajax call - 성공
function ajaxAdminUser_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = adminUserDetailConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}
		
		moveAdminUserList();
	}
}

// 관리자 ajax call - 실패
function ajaxAdminUser_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	log_message += codeLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("관리자 등록 실패하였습니다." + textStatus);
			break;
		case "save":
			alert("관리자 수정 실패하였습니다." + textStatus);
			break;
		case "remove":
			alert("관리자 삭제 실패하였습니다." + textStatus);
			break;
		default:
	}
}

// 비밀번호 확인
function checkPassword(){
	
	var password = $("#adminUserDetailForm input[name=password]").val();
	var passwordCheck = $("#adminUserDetailForm input[name=password_check]").val();
	
	// 관리자 비밀번호 검사 (정규식 패턴)
	var regex = /^.*(?=^.{9,20}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[~,!,@,#,$,%,^,&,*,(,),=,+,_,.,|]).*$/;

	if(password != '' && passwordCheck != '') {
		if(!regex.test(password)){
			alert("비밀번호는 9 ~ 20자 사이의 문자, 숫자, 특수문자 조합으로 구성되어야합니다. (공백문자 제외)");
			$('input[name=password]').select();
			return false;
		}
		
		if(password != passwordCheck){
			alert("비밀번호와 비밀번호 확인이 일치하지 않습니다.");
			$('input[name=password]').select();
			return false;
		}
		
		if(password.indexOf(" ") > -1 ){
			alert("비밀번호에 공백문자를 포함할 수 없습니다.");
			$('input[name=password]').select();
			return false;
		}
	}
	
	return true;
}

// 목록으로 이동
function moveAdminUserList(){
	$("#adminUserListForm").attr("action",adminUserDetailConfig["listUrl"]);
	$("#adminUserListForm").submit();
}

//전체 권한 체크박스 컨트롤
function allAuthMenuCheck(checkbox){
	allAuthCheckControl($(checkbox).is(":checked"));
}

//전체 권한 체크박스 컨트롤
function allAuthCheckControl(flag){
	var elems = document.getElementsByName("auth_id");
	for(var i=0; i<elems.length; i++) {
		var elem = elems[i];
		elem.checked = flag;
	}
	// 익스플로러에서 안됨..
	/*for(var i in document.getElementsByName("auth_ids")){
		var checkbox = document.getElementsByName("auth_ids")[i];
			checkbox.checked = flag;
	}*/
	
	//크롬에서 안됨..
	//jquery 오작동으로 인한 주석
	//$("#authListDiv input[name='auth_ids']:checkbox").attr("checked",flag);
}

function initPass(admin_user_id) {
	if(confirm("비밀번호 초기화 하시겠습니까?")) {
		
		$.ajax({
			type: 'POST',
			url: rootPath + '/adminUserMngt/initPassword.html',
			data: { 
				"admin_user_id" : admin_user_id
			},
			success: function(data) {
				if(data = 1) {
					alert("초기화 되었습니다.");
					moveAdminUserList();
				} else {
					alert("초기화에 실패하였습니다.");
				}
			}
		});
	}
	
}

function callAdminSysList(){
	$.ajax({
		type: 'POST',
		url: rootPath + '/adminUserMngt/adminUserSysList.html',
		data: { 
			"admin_user_id" : admin_user_id
			,"auth_id" : auth_id
		},
		success: function(data) {
			$('#authListDiv').html(data);
		}
	});
}

function sysListChange(tag){
	auth_id = $(tag).val();
	callAdminSysList();
}

$(document).ready(function(){
	callAdminSysList();
});
