/**
 * EmpUserMngtDetail Javascript
 * @author yjyoo
 * @since 2015. 04. 21
 */

function onlyEng() {
	var obj = $("#empUserDetailForm input[name=emp_user_id]").val();
	//var obj = $('#emp_user_id').val();
	var deny_char = /^[a-z|A-Z|0-9||\*]+$/

	if (!deny_char.test(obj)) {
		alert("영문자와 숫자만을 입력하세요");
		$("#empUserDetailForm input[name=emp_user_id]").val("");
		return false;
	}
} 

// 사원 추가
function addEmpUserMngtOne(){
	
	var emp_user_name = $("#empUserDetailForm input[name=emp_user_name]").val();
	var emp_user_id = $("#empUserDetailForm input[name=emp_user_id]").val();
	var emp_user_name = $("#empUserDetailForm input[name=emp_user_name]").val();
	var dept_id = $("#empUserDetailForm select[name=dept_id]").val();
	var system_seq = $("empUserDetailForm select[name=system_seq]").val();
	//var emp_user_id = $('#emp_user_id').val();
	//var emp_user_name = $('#emp_user_name').val();
	//var dept_id = $('#dept_id').val();
	
	if(emp_user_name == '') {
		alert("사용자명을 입력해주세요!");
		$("#empUserDetailForm input[name=emp_user_name]").focus();
		return false;
	}
	if(emp_user_id == '') {
		alert("사용자ID를 입력해주세요!");
		$("#empUserDetailForm input[name=emp_user_id]").focus();
		return false;
	}
	if(emp_user_name == '') {
		alert("사용자명을 입력해주세요!");
		$("#empUserDetailForm input[name=emp_user_name]").focus();
		return false;
	}
	if(dept_id == '') {
		alert("부서를 선택해주세요!");
		return false;
	}
	if(system_seq == '') {
		alert("시스템을 선택해주세요!");
		return false;
	}
	
	if(checkValidation_add()){
		sendAjaxEmpUser("add");	
	}
}

// 사원 수정
function saveEmpUserMngtOne(){
	
	var emp_user_name = $("#empUserDetailForm input[name=emp_user_name]").val();
	var emp_user_id = $("#empUserDetailForm input[name=emp_user_id]").val();
	var emp_user_name = $("#empUserDetailForm input[name=emp_user_name]").val();
	var dept_id = $('#dept_id').val();
	
	if(emp_user_name == '') {
		alert("사용자명을 입력해주세요!");
		$("#empUserDetailForm input[name=emp_user_name]").focus();
		return false;
	}
	if(emp_user_id == '') {
		alert("사용자ID를 입력해주세요!");
		$("#empUserDetailForm input[name=emp_user_id]").focus();
		return false;
	}
	if(dept_id == '') {
		alert("부서를 선택해주세요!");
		return false;
	}
	if(emp_user_name == '') {
		alert("사용자명을 입력해주세요!");
		return false;
	}	
	if(checkValidation_save()){
		sendAjaxEmpUser("save");	
	}
}

// 사원 삭제
function removeEmpUser(){
	if(confirm("사원을 삭제하시겠습니까?")) {
		sendAjaxEmpUser("remove");
	}
	
}

// 사원 ajax call
function sendAjaxEmpUser(type){
	var $form = $("#empUserDetailForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'emp_user_id,dept_id';
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'add') {
		log_message_title = '사원등록';
		log_action = 'INSERT';
	} else if (type == 'save') {
		log_message_title = '사원수정';
		log_action = 'UPDATE';
	} else if (type == 'remove') {
		log_message_title = '사원삭제';
		log_action = 'REMOVE';
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(empUserDetailConfig[type + "EmpUserUrl"], $form.serialize(), ajaxEmpUser_successHandler, ajaxEmpUser_errorHandler, type);
}

// 사원 ajax call - 성공
function ajaxEmpUser_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}
		
		moveEmpUserList();
	}
}

// 사원 ajax call - 실패
function ajaxEmpUser_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){
	moveEmpUserList();
	log_message += empUserLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("사원 등록 실패하였습니다." + textStatus);
			break;
		case "save":
			alert("사원 수정 실패하였습니다." + textStatus);
			break;
		case "remove":
			alert("사원 삭제 실패하였습니다." + textStatus);
			break;
		default:
	}
}

// 필수 항목 확인 (신규)
function checkValidation_add(){
	var emp_user_name = $("#empUserDetailForm input[name=emp_user_name]").val();
	var emp_user_id = $("#empUserDetailForm input[name=emp_user_id]").val();
	var dept_id = $("#empUserDetailForm select[name=dept_id]").val();
	var system_seq = $("#empUserDetailForm select[name=system_seq]").val();
	var desc_access_cd = $("#desc_access_cd option:selected").val();
	
	if(emp_user_name == '') {
		alert("사용자명을 입력해주세요!");
		$("#empUserDetailForm input[name=emp_user_name]").focus();
		return false;
	}
	else if(emp_user_id == '') {
		alert("사용자ID를 입력해주세요!");
		$("#empUserDetailForm input[name=emp_user_id]").focus();
		return false;
	}
	else if(dept_id == '') {
		alert("부서를 선택해주세요!");
		return false;
	} else if(system_seq == '') {
		alert("시스템을 선택해주세요!");
		return false;
	}
	
	if(desc_access_cd == '2') {
		var desc_access_org_cd = $("#desc_access_org_cd option:selected").val();
		if(desc_access_org_cd == '') {
			alert("소명권한이 중간관리자일경우 소명담당부서를 선택해주세요");
			return false;
		}
	}
	
	return true;
}

// 필수 항목 확인 (수정)#empUserDetailForm input[name=emp_user_id]
function checkValidation_save(){
	var emp_user_id = $("#empUserDetailForm input[name=emp_user_id]").val();
	var dept_id = $("#empUserDetailForm select[name=dept_id]").val();
	var desc_access_cd = $("#desc_access_cd option:selected").val();
	
	if(emp_user_id == '') {
		alert("사용자ID를 입력해주세요!");
		$("#empUserDetailForm input[name=emp_user_id]").focus();
		return false;
	}
	else if(dept_id == '') {
		alert("부서를 선택해주세요!");
		return false;
	}
	
	if(desc_access_cd == '2') {
		var desc_access_org_cd = $("#desc_access_org_cd option:selected").val();
		if(desc_access_org_cd == '') {
			alert("소명권한이 중간관리자일경우 소명담당부서를 선택해주세요");
			return false;
		}
	}
	
	if(desc_access_cd == '1' || desc_access_cd == '2') {
		$("#empUserDetailForm input[name=desc_access_org_cd]").val(desc_access_org_cd);
	}
	
	return true;
}

// 목록으로 이동
function moveEmpUserList(){
	$("#empUserListForm").attr("action", empUserDetailConfig["empUserListUrl"]);
	$("#empUserListForm").submit();
}

function showDescDepthList() {
	var desc_access_cd = $("#desc_access_cd option:selected").val();
	
	if(desc_access_cd == "3") {
		$("#desc_access_org_cd").attr("disabled", "true");
	} else {
		$("#desc_access_org_cd").removeAttr("disabled");
	}
}

function initPass(emp_user_id, system_seq) {
	if(confirm("비밀번호 초기화 하시겠습니까?")) {
		
		$.ajax({
			type: 'POST',
			url: rootPath + '/empUserMngt/initPassword.html',
			data: { 
				"emp_user_id" : emp_user_id,
				"system_seq" : system_seq
			},
			success: function(data) {
				if(data = 1) {
					alert("초기화 되었습니다.");
					moveEmpUserList();
				} else {
					alert("초기화에 실패하였습니다.");
				}
			}
		});
	}
	
}


