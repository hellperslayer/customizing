/**
 * EmpUserMngtList Javascript
 * @author yjyoo
 * @since 2015. 04. 21
 */

$(function(){
	$('input[type=text]').attr("onkeydown","javascript:if(event.keyCode==13){searchEmpUserList();}");
});

var $clone;

$(document).ready(function() {
	$clone = $("#empUserListForm").clone();
});

// 페이지 이동
function goPage(num){
	$clone.attr("action", accessAuthListConfig["listUrl"]);
	$clone.find("input[name=page_num]").val(num);
	$clone.attr("style", "display: none;");
	$('body').append($clone);
	$clone.submit();
}

//엑셀 업로드 팝업 기능
function excelUpLoadList(){
	var excelUp = $(".popup_wrap");
	excelUp.attr("style", "display: block;");
}

function closeUpload(){
	var excelUp = $(".popup_wrap");
	var file = $("#file");
	excelUp.attr("style", "display: none;");
	file.replaceWith( file.clone(true));
}

function excelEmpUserUpLoad(){
	
	var data = $("#result").val();
	
		var options = {
				
				success : function(data) {
					var checkdata = "\"true\"";
					if(checkdata == data){
						alert("성공적으로 업로드 되었습니다!");
						moveEmpUserList();
					}else if(checkdata != data){
						alert("업로드에 실패하였습니다. (엑셀파일의 셀안에 공백이 들어있거나 필수 입력정보를 입력하였는지, 혹은 기존 시스템 외의 시스템이 포합되어 있는지 확인해주십시오.)");
					}
					
				},
				error : function(error) {
					alert("요청 처리 중 오류가 발생하였습니다.");
				}
		  };
		    
    $("#fileForm").ajaxSubmit(options);

    return false;
}

function addAccessAuth() {
	var emp_user_name = $('input[name=emp_user_name]').val();
	var emp_user_id = $('input[name=emp_user_id]').val();
	var approver = $('input[name=approver]').val();
	
	if(emp_user_name == '') {
		alert("사용자명을 입력해주세요");
		$('input[name=appremp_user_nameover]').focus();
		return false;
	}
	
	if(emp_user_id == '') {
		alert("사용자ID를 입력해주세요");
		$('input[name=emp_user_id]').focus();
		return false;
	}
	
	if(approver == '') {
		alert("관리자명를 입력해주세요");
		$('input[name=approver]').focus();
		return false;
	}
	
	sendAjaxAccessAuth("add");
}

function saveAccessAuth(){
	var approver = $('input[name=approver]').val();
	
	if(approver == '') {
		alert("승인자를 입력해주세요");
		$('input[name=approver]').focus();
		return false;
	}
	
	sendAjaxAccessAuth("save");
}

function removeAccessAuth() {
	sendAjaxAccessAuth("remove");
}

function sendAjaxAccessAuth(type){
	var $form = $("#AccessAuthDetailForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = '';
	
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'add') {
		log_message_title = '권한관리등록';
		log_action = 'INSERT';
		log_message_params = 'emp_user_id,emp_user_name';
	} else if (type == 'save') {
		log_message_title = '권한관리수정';
		log_action = 'UPDATE';
		log_message_params = 'seq';
	} else if (type == 'remove') {
		log_message_title = '권한관리삭제';
		log_action = 'REMOVE';
		log_message_params = 'seq';
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(accessAuthListConfig[type + "Url"],$form.serialize(), ajaxAccessAuth_successHandler, ajaxAccessAuth_errorHandler, type);
}

function ajaxAccessAuth_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = accessAuthListConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}
		
		goList();
	}
}

// 관리자 ajax call - 실패
function ajaxAccessAuth_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	log_message += codeLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("등록 실패하였습니다." + textStatus);
			break;
		case "save":
			alert("수정 실패하였습니다." + textStatus);
			break;
		case "remove":
			alert("삭제 실패하였습니다." + textStatus);
			break;
		default:
	}
}


// 검색
function moveEmpUserList() {
	$("#empUserListForm").attr("action", accessAuthListConfig["listUrl"]);
	$("#empUserListForm input[name=page_num]").val('1');
	$("#empUserListForm").submit();
}

function searchEmpUserList() {
	
	var filter =  /^[ㄱ-ㅎ가-힣a-zA-Z]+$/;
	var chkIp = $("#empUserListForm input[name=approver_ip]").val();
	
	$("#empUserListForm").attr("action", accessAuthListConfig["listUrl"]);
	$("#empUserListForm input[name=isSearch]").val('Y');
	$("#empUserListForm input[name=page_num]").val('1');
	
	//ip 주소 정규식
	if(chkIp != ''){
		if(filter.test(chkIp)==false){
		
		$("#empUserListForm").submit();
		
		}else{
			alert("IP 주소 형식이 틀립니다.");
			$("#empUserListForm input[name=approver_ip]").val('');
			$("#empUserListForm input[name=approver_ip]").focus();
			return ;
		}
	}
	
	$("#empUserListForm").submit();
}

// 상세 페이지 이동 (신규, 수정)
function findAccessAuthDetail(seq){
	$("#detailForm").attr("action", accessAuthListConfig["detailUrl"]);
	$("#detailForm input[name=seq]").val(seq);
	// 아래의 코드와 같다. 공통 js 참조
	// $('#empUserListForm').append($('<input type="hidden" name="emp_user_id" value="' + emp_user_id + '" />'));
	$("#detailForm").submit();
}

function goList() {
	$("#menuForm").attr("action", accessAuthListConfig["listUrl"]);
	$("#menuForm input[name=page_num]").val('1');
	$("#menuForm").submit();
}

// 엑셀 다운로드
function excelEmpUserList() {
	var $form = $("#empUserListForm");
	var log_message_params = '';
	var menu_id = $('input[name=current_menu_id]').val();
	var log_message_title = '접근권한 관리 엑셀 다운로드';
	var log_action = 'EXCEL DOWNLOAD';
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	$("#empUserListForm").attr("action", accessAuthListConfig["downloadUrl"]);
	$("#empUserListForm").submit();
}


function ajaxDefaultSucCallback(data, dataType, actionType){
	
}

function ajaxDefaultErrCallback(XMLHttpRequest, textStatus, errorThrown, actionType){
	alert("ajax request Error : " + textStatus);
}


// 사원 ajax call - 성공
function ajaxEmpUser_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		alert("성공적으로 등록되었습니다.");
		moveEmpUserList();
	}
}

// 사원 ajax call - 실패
function ajaxEmpUser_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){	
	alert("엑셀 업로드에 실패하였습니다. 셀값에 빈공간이 있거나 값이 제대로 들어가있는지 확인해 주십시오." + textStatus);
}

function exDown(){
	$("#exdown").submit();
}

