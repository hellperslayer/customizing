/**
 * GroupCodeMngtList Javascript
 */

// 그룹코드 상세 
function findGroupCodeDetail(groupCodeId){
	$("#groupCodeForm input[name=group_code_id]").val(groupCodeId);
	$("#groupCodeForm").attr("action",groupCodeConfig["groupCodeDetailUrl"]);
	$("#groupCodeForm").submit();
}

// 코드리스트
function findCodeList(groupCodeId){
	$("#groupCodeForm input[name=group_code_id]").val(groupCodeId);
	sendAjaxPostRequest(groupCodeConfig["codeListUrl"], $('#groupCodeForm').serialize(), codeList_successHandler, codeList_errorHandler);
	//$("#groupCodeForm").submit();
}

// 코드리스트 - 성공 
function codeList_successHandler(data, dataType){
	if(data == null || !data.hasOwnProperty("codeList")){
		alert("데이터이상함.");
	}
	
	var groupCodeId = data.paramBean.group_code_id;
	
	drawCodeList(groupCodeConfig["codeListElementId"],data.codeList.codes,groupCodeId);
}

// 코드리스트 - 성공 - 그리기
function drawCodeList(target,codes,groupCodeId){
	$("#" + target).empty();
	var lineCount = 0;
	var codeListHtmlArr = new Array();
	
	codeListHtmlArr[lineCount++] = "<div class='portlet light bordered'>";
	codeListHtmlArr[lineCount++] = "<div class='portlet-body'>";
	codeListHtmlArr[lineCount++] = "<div class='table-toolbar'>";
	codeListHtmlArr[lineCount++] = "<div class='row'>";
	codeListHtmlArr[lineCount++] = "<div class='col-md-6'>";
	codeListHtmlArr[lineCount++] = "<div class='btn-group'>";
	codeListHtmlArr[lineCount++] = "<button class='btn btn-sm blue btn-outline sbold uppercase' onclick='findCodeDetail(\"" + groupCodeId + "\",\"\")'> 신규";
	codeListHtmlArr[lineCount++] = "<i class='fa fa-plus'></i></button></div></div></div></div>";
	codeListHtmlArr[lineCount++] = "<table class='table table-striped table-bordered table-hover order-column'><thead><tr>";
	codeListHtmlArr[lineCount++] = "<th style='text-align: center; '> 코드ID </th>";
	codeListHtmlArr[lineCount++] = "<th style='text-align: center; '> 코드명 </th>";
	codeListHtmlArr[lineCount++] = "<th style='text-align: center; '> 사용여부 </th>";
	codeListHtmlArr[lineCount++] = "<th style='text-align: center; '> 정렬순서 </th></tr></thead><tbody>";
	                
	if(codes.length > 0) {
		for(var i in codes) {
			var code = codes[i];
			codeListHtmlArr[lineCount++] = "<tr class='odd gradeX' style='text-align: center; cursor: pointer;' onclick='findCodeDetail(\"" + groupCodeId + "\",\"" + code.code_id + "\")'><td>";	                        
			//codeListHtmlArr[lineCount++] = "<a onclick='findCodeDetail(\"" + groupCodeId + "\",\"" + code.code_id + "\")'>";
			codeListHtmlArr[lineCount++] = code.code_id + "</td>";
			codeListHtmlArr[lineCount++] = "<td align='left' style='padding-left: 20px;'>" + code.code_name + "</td>";
			if(code.use_flag == 'Y') 
				codeListHtmlArr[lineCount++] = "<td style='text-align: center;vertical-align: middle; '><span class='label label-sm label-success'> 사용 </span></td>";
			else 
				codeListHtmlArr[lineCount++] = "<td style='text-align: center;vertical-align: middle; '><span class='label label-sm label-warning'> 미사용 </span></td>";
			
			codeListHtmlArr[lineCount++] = "<td style='text-align: center; ' class='center'>" + code.sort_order + "</td></tr>";
		}
	}else {
		codeListHtmlArr[lineCount++] = "<tr align='center'><td colspan='4'>데이터가 없습니다.</td></tr>";
	}
	
	codeListHtmlArr[lineCount++] = "</tbody></table></div></div>"
	
	$("#" + target).append(codeListHtmlArr.join(''));
}

// 코드리스트 - 실패
function codeList_errorHandler(XMLHttpRequest, textStatus, errorThrown){
	alert("codeList_errorHandler" + textStatus);
}

// 코드 상세
function findCodeDetail(groupCodeId, codeId){
	$("#groupCodeForm input[name=group_code_id]").val(groupCodeId);
	$("#groupCodeForm input[name=code_id]").val(codeId);
	$("#groupCodeForm").attr("action",groupCodeConfig["codeDetailUrl"]);
	$("#groupCodeForm").submit();
}
