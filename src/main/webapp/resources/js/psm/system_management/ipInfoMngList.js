function addUserInfo() {
	var user_Id = $("#add_Id").val();
	var login_Ip = $("#add_Ip").val();
	$.ajax({
		type: 'POST',
		url: rootPath + '/ipInfo/userInfoAdd.html',
		data: { 
			user_Id : user_Id,
			login_Ip : login_Ip
		},
		success: function(data) {
			if(data == 1) {
				alert("추가되었습니다.");
				moveList();
			} 
			else {
				alert("중복된 사용자ID, 중복된 사용자IP 있습니다.");
			}
		}
	});
}

function deleteUserInfo(user_Id, login_Ip) {
	$.ajax({
		type: 'POST',
		url: rootPath + '/ipInfo/delete.html',
		data: {  
			user_Id : user_Id,
			login_Ip : login_Ip
		},
		success: function(data) {
			alert("삭제되었습니다.");
			moveList();
		}
	});
}

// 그룹코드 ajax call - 성공
/*function ajaxIpPerm_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			alert("세션이 만료되었습니다.");
			location.href = ipPermConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	} else {
		switch(actionType){
		case "add":
			alert("성공적으로 등록되었습니다.");
			break;
		case "delete":
			alert("성공적으로 삭제되었습니다.");
			break;
	}
		
		moveList();
	}
}

// 그룹코드 ajax call - 실패
function ajaxIpPerm_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){
	alert("실패하였습니다. : " + textStatus);
}*/
function goPage(num){
	$("#misForm input[name=page_num]").val(num);
	moveList();
}

//목록으로 이동
function moveList(){
	$("#misForm").attr("action", ipInfoListConfig["listUrl"]);
	$("#misForm").submit();
}

//검색
function moveIpInfoList(){
	$("#misForm input[name=page_num]").val("1");
	$("#misForm input[name=isSearch]").val("Y");
	$("#misForm").attr("action", ipInfoListConfig["listUrl"]);
	$("#misForm").submit();
}