

function setOptionSetting() {
	var $form = $("#optionForm");
	$.ajax({
		type: 'POST',
		url: rootPath + '/optionSetting/setOptionSetting.html',
		data: $form.serialize(),
		success: function(data) {
			alert("수정되었습니다.");
			moveSystemList();
		}
	});
}

function setMaster() {
	var $form = $("#masterForm");
	var flag = $('input[name=use_flag]:checked').val();
	$.ajax({
		type: 'POST',
		url: rootPath + '/optionSetting/setMaster.html',
		data: $form.serialize(),
		success: function(data) {
			alert("수정되었습니다.");
			moveSystemList();
		}
	});
}

function setSessionTime() {
	var $form = $("#sessionForm");
	$.ajax({
		type: 'POST',
		url: rootPath + '/optionSetting/setSessionTime.html',
		data: $form.serialize(),
		success: function(data) {
			alert("수정되었습니다.");
			moveSystemList();
		}
	});
}

function setResultTypeView() {
	var $form = $("#resultTypeForm");
	$.ajax({
		type: 'POST',
		url: rootPath + '/optionSetting/setResultTypeView.html',
		data: $form.serialize(),
		success: function(data) {
			alert("수정되었습니다.");
			moveSystemList();
		}
	});
}

function setSidebarView() {
	var $form = $("#sidebarForm");
	$.ajax({
		type: 'POST',
		url: rootPath + '/optionSetting/setSidebarView.html',
		data: $form.serialize(),
		success: function(data) {
			alert("수정되었습니다.");
			moveSystemList();
		}
	});
}

function setScrnNameView() {
	var $form = $("#scrnNameForm");
	$.ajax({
		type: 'POST',
		url: rootPath + '/optionSetting/setScrnNameView.html',
		data: $form.serialize(),
		success: function(data) {
			alert("수정되었습니다.");
			moveSystemList();
		}
	});
}

function setMappingId() {
	var $form = $("#mappingForm");
	$.ajax({
		type: 'POST',
		url: rootPath + '/optionSetting/setMappingId.html',
		data: $form.serialize(),
		success: function(data) {
			alert("수정되었습니다.");
			moveSystemList();
		}
	});
}


function setUse_systemSeq() {
	var $form = $("#use_systemSeqForm");
	$.ajax({
		type: 'POST',
		url: rootPath + '/optionSetting/setUse_systemSeq.html',
		data: $form.serialize(),
		success: function(data) {
			alert("수정되었습니다.");
			moveSystemList();
		}
	});
}

function setUi_type() {
	var $form = $("#ui_type");
	$.ajax({
		type: 'POST',
		url: rootPath + '/optionSetting/setUi_type.html',
		data: $form.serialize(),
		success: function(data) {
			alert("수정되었습니다.");
			moveSystemList();
		}
	});
}

function moveSystemList(){
	$("#reloadForm").submit();
}