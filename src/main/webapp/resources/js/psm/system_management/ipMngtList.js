/**
 * DepartmentMngtList Javascript
 */

// dynatree rendering
$(function(){
	$("#ipTree").dynatree({
		minExpandLevel: 2
		,debugLevel: 1
      	,persist: true      
      	,keyboard: true
		,onPostInit: function(isReloading, isError) {
			this.reactivate();
		}
		,onActivate: selectDepartmentTree
	});
	
	var cookieData = document.cookie;
	
	var active_index = cookieData.indexOf("dynatree-active=");
	
	// 메뉴 최초 접근
	if(active_index == -1) {
		findDepartmentDetail("ROOT");
	}
	
	$("#ipDetailDiv").hide();
});

// 트리 노드 선택
function selectDepartmentTree(node){
	if(node == null){
		return false;
	}
	
	var departmentId = node.data.href;
	findDepartmentDetail(departmentId);
}

// 부서 상세정보 요청
function findDepartmentDetail(departmentId){
	sendAjaxPostRequest(ipConfig["detailUrl"],{'dept_id':departmentId},findDepartmentDetail_successHandler, findDepartmentDetail_errorHandler);
}

// 부서 상세정보 요청 - 성공
function findDepartmentDetail_successHandler(data, dataType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = ipConfig['loginPage'];
			return false;
		}
	}
	
	if(data == null || !data.hasOwnProperty("ipDetail")){
		alert("상세 데이터 없음");
		return false;
	}
	
	showDepartmentDetail(data);
}

// 부서 상세정보 요청 - 실패
function findDepartmentDetail_errorHandler(XMLHttpRequest, textStatus, errorThrown){
	alert("findDepartmentDetail_errorHandler : " + textStatus);
}

// 부서 상세화면 노출
function showDepartmentDetail(data){
	$("#ipDetailDiv").show();
	$("#ipDetailDiv2").hide();	
	var departmentId = initDepartmentDetail();
	
	if(data == null || data == ''){ // 신규
		$("#ipDetailForm input[name=parent_dept_id]").removeAttr("name");
		$("#ipDetailForm select[id=parent_dept_id_select]").attr("name", "parent_dept_id");
		$("#ipDetailForm select[name=parent_dept_id]").show();
		$("#ipDetailForm select[name=parent_dept_id]").val(departmentId);
		$("#rootDepartmentParentNameSpan").hide();
		
		$("#insertTr").hide();
		$("#updateTr").hide();
		
		$("#addDepartmentBtn").show();
		$("#saveDepartmentBtn").hide();
		$("#removeDepartmentBtn").hide();
	}else{ // 상세
		var departmentDetail = data.ipDetail;
		
		$("#deptIdLabel").html("· DEPT ID : " + departmentDetail.dept_id);
		
		$("#ipDetailForm input[name=dept_id]").val(departmentDetail.dept_id);
		$("#dept_nam").val(departmentDetail.dept_name);

		if(departmentDetail.parent_dept_id == null || departmentDetail.parent_dept_id == "DEPT00000"){
			$("#parent_dept_name").text("최상위부서");
		}else{
			$("#parent_dept_name").text(departmentDetail.parent_dept_name);
		}
		
		$("#insertUserIdSpan").text(departmentDetail.insert_user_id == null ? '' : departmentDetail.insert_user_id);
		$("#insertDatetimeSpan").text(departmentDetail.insert_datetime == null ? '' : new Date(departmentDetail.insert_datetime).convertDatetimeToString());
		$("#updateUserIdSpan").text(departmentDetail.update_user_id == null ? '' : departmentDetail.update_user_id);
		$("#updateDatetimeSpan").text(departmentDetail.update_datetime == null ? '' : new Date(departmentDetail.update_datetime).convertDatetimeToString());
		$("#ipinfo").val(departmentDetail.from_ip);
		$("#ipinfoto").val(departmentDetail.to_ip);
		$("#insertTr").show();
		$("#updateTr").show();
	}
}

// 부서 상세화면 초기화
function initDepartmentDetail(){
	var departmentId = $("#ipDetailForm input[name=dept_id]").val();
	$("#ipDetailForm input[name=dept_id]").val('');
	$("#ipDetailForm input[name=dept_name]").val('');
	$("#ipDetailForm input[id=parent_dept_id]").val(departmentId);
	
	$("#insertUserIdSpan").text('');
	$("#insertDatetimeSpan").text('');
	$("#updateUserIdSpan").text('');
	$("#updateDatetimeSpan").text('');
	return departmentId;
}

// 부서 수정
/*function saveDepartment(){
	var description = $('input[name=ipinfo]').val();
	var menu_id = $('input[name=current_menu_id]').val();
	
	$.ajax({
		url : ipConfig["saveUrl"],
		async : false,
		type : "POST",
		data : {
			description : description,
			dept_id : menu_id
		},

		success : function(data) {
			alert(data);
			if (data == "true") {
				alert("ss");
			
			} else {
				alert("qq");
			}
		},
		error : function(request, status, error) {
			alert("code:" + request.status + "\n" + "message:"
					+ request.responseText + "\n" + "error:" + error);
		}
	});
}*/

function addDepartment(){
	$("#ipDetailDiv").css("display","none");
	$("#ipDetailDiv2").css("display","inline");
}

function addPart(){
	var dept_name = $('input[name=dept_nam]').val();
	var from_ip = $('input[name=ipinfom]').val();
	if(dept_name == ''){
		alert("부서명을 입력해주세요.");
		return false;
	}
	if(from_ip == '') {
		alert("IP를 입력해주세요.");
		return false;
	}
	sendAjaxDepartment2("add");	
}

function sendAjaxDepartment2(type){
	var $form = $("#ipDetailForm2");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'dept_name,parent_dept_id,description';
	var menu_id = $('input[name=current_menu_id]').val();
    var from_ip = $('input[name=ipinfom]').val();
    var to_ip = $('input[name=ipinfomto]').val();
    var dept_name = $('input[name=dept_nam]').val();
    var use_flag = $(':radio[name="check"]:checked').val();
  
    if (type == 'add') {
		log_message_title = '부서등록';
		log_action = 'INSERT';
	} 
  
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id, from_ip, to_ip, dept_name, use_flag);

	sendAjaxPostRequest(ipConfig[type+"Url"], $form.serialize(), ajaxDepartment_successHandler, ajaxDepartment_errorHandler, type);
}


function saveIp(){
	var dept_name = $('input[name=dept_name]').val();
	var parent_dept_id = $('select[name=parent_dept_id]').val();
	var use_flag = $('input[name=use_flag]:checked').val();
	var sort_order = $('input[name=sort_order]').val();
	var from_ip = $('input[name=ipinfo]').val();
	var to_ip = $('input[name=ipinfoto]').val();
	/*var dept_name = $('input[name=dept_nam]').val();*/
	
	if(dept_name == ''){
		alert("부서명을 입력해주세요.");
		return false;
	}
	if(from_ip == '') {
		alert("IP를 입력해주세요.");
		return false;
	}
	
	sendAjaxDepartment("save");	
}

// 부서 삭제
function removeDepartment(){
	if(confirm("삭제하시겠습니까?")) {
		sendAjaxDepartment("remove");
	}
}

// 부서 ajax call
function sendAjaxDepartment(type){
	var $form = $("#ipDetailForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'dept_name,parent_dept_id,description';
	var menu_id = $('input[name=current_menu_id]').val();
    var from_ip = $('input[name=ipinfo]').val();
    var to_ip = $('input[name=ipinfoto]').val();
    var dept_name = $('input[name=dept_name]').val();
    var use_flag = $(':radio[name="checky"]:checked').val();
 
    if (type == 'save') {
		log_message_title = 'ip수정';
		log_action = 'UPDATE';
	} else if (type == 'remove') {
		log_message_title = 'ip삭제';
		log_action = 'REMOVE';
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id, from_ip, to_ip, dept_name,use_flag);

	sendAjaxPostRequest(ipConfig[type+"Url"], $form.serialize(), ajaxDepartment_successHandler, ajaxDepartment_errorHandler, type);

	
}

// 부서 ajax call - 성공
function ajaxDepartment_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = ipConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}
		
		moveDepartmentList();
	}
}

// 부서 ajax call - 실패
function ajaxDepartment_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){
	
	//log_message += deptLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("부서 등록 실패하였습니다." + textStatus);
			break;
		case "save":
			alert("부서 수정 실패하였습니다." + textStatus);
			break;
		case "remove":
			alert("부서 삭제 실패하였습니다." + textStatus);
			break;
		default:
	}
}

function removeAdd(){
	$("#ipDetailDiv").css("display","inline");
	$("#ipDetailDiv2").css("display","none");
	$("input[name=dept_nam]").val("");
	$("input[name=ipinfom]").val("");
	
}

// 목록으로 이동
function moveDepartmentList(){
	$("#ipListForm").attr("action",ipConfig["listUrl"]);
	$("#ipListForm").submit();
}
