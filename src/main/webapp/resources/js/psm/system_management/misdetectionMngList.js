function deletePrivacy(id){
	sendAjaxIpPerm("remove",id,"misForm");	
}

function addPrivType(){
	sendAjaxIpPerm("add","0","addForm");
}

function addPrivTypeAll(checkPatt, checkPrivt){
	
	
	var $form = $("#"+"addForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'use_flag, misdetect_id';
	var menu_id = $('input[name=current_menu_id]').val();
	$('input[name=misdetect_id]').attr("value", "0");	
	log_message_title = '개인정보 탐지 제외 추가';
	
	//$('input[name=privacy_type]').attr("value",checkPrivt);
	$('input[name=misdetect_pattern]').attr("value",checkPatt);
	
    $('#privacy_type option:contains(' + checkPrivt + ')').prop({selected: true});
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	sendAjaxPostRequest(misListConfig["addUrl"], $form.serialize(), ajaxIpPerm_successHandler, ajaxIpPerm_errorHandler, "add");
}


function sendAjaxIpPerm(type,id,formname){
	var $form = $("#"+formname);
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'use_flag, misdetect_id';
	var menu_id = $('input[name=current_menu_id]').val();
	$('input[name=misdetect_id]').attr("value", id);	
	if(type=="remove"){
		log_message_title = '개인정보 탐지 제외 삭제';
		log_action = 'REMOVE';
	}else if(type=="add"){
		log_message_title = '개인정보 탐지 제외 추가';
		log_action = 'ADD';
		var checkPatt=$('#addForm input[name=misdetect_pattern]').val();
		var checkPrivt=$('#addForm select[name=privacy_type]').val();
		if(checkPatt=="" || checkPatt == null){
			alert('개인정보패턴을 입력해주십시오');
			return;
		}else if(checkPrivt==""|| checkPrivt == null){
			alert('개인정보 유형을 선택해주십시오');
			return;
		}
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(misListConfig[type + "Url"], $form.serialize(), ajaxIpPerm_successHandler, ajaxIpPerm_errorHandler, type);
}

// 그룹코드 ajax call - 성공
function ajaxIpPerm_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			alert("세션이 만료되었습니다.");
			location.href = ipPermConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	} else {
		switch(actionType){
		case "add":
			alert("성공적으로 등록되었습니다.");
			break;
		case "delete":
			alert("성공적으로 삭제되었습니다.");
			break;
	}
		
		moveList();
	}
}

// 그룹코드 ajax call - 실패
function ajaxIpPerm_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){
	alert("실패하였습니다. : " + textStatus);
}
function goPage(num){
	$("#misForm input[name=page_num]").val(num);
	moveList();
}

//목록으로 이동
function moveList(){
	$("#misForm").attr("action", misListConfig["listUrl"]);
	$("#misForm").submit();
}

//검색
function moveMisdetectionList(){
	$("#misForm input[name=page_num]").val("1");
	$("#misForm input[name=isSearch]").val("Y");
	$("#misForm").attr("action", misListConfig["listUrl"]);
	$("#misForm").submit();
}

function excelMisdetectList(){
//	if(arguments[0] >= 65535){
//		alert("엑셀다운로드는 65535건까지 가능합니다.");
//	}else{
		
		var $form = $("#misForm");
		var log_message_params = '';
		var menu_id = $('input[name=current_menu_id]').val();
		var log_message_title = '예외처리DB 엑셀 다운로드';
		var log_action = 'EXCEL DOWNLOAD';
		
		$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
		
		$("#misForm").attr("action",misListConfig["downloadUrl"]);
		$("#misForm").submit();
//	}
}

function excelUpLoadList(){
	var excelUp = $(".popup_wrap");
	excelUp.attr("style", "display: block;");
}

function closeUpload(){
	var excelUp = $(".popup_wrap");
	var file = $("#file");
	excelUp.attr("style", "display: none;");
	file.replaceWith( file.clone(true));
}

function excelmisDetection(){
	var data = $("#result").val();
	
		var options = {
				success : function(data) {
					var checkdata = "\"true\"";
					if(checkdata == data){
						alert("성공적으로 업로드 되었습니다!");
						moveList();
					}else if(checkdata != data){
						alert("업로드에 실패하였습니다. (엑셀파일의 셀안에 공백이 들어있거나 필수 입력정보를 입력하였는지 확인해주십시오.)");
					}
					
				},
				error : function(error) {
					alert("요청 처리 중 오류가 발생하였습니다.");
				}
		  };
		    
    $("#fileForm").ajaxSubmit(options);

    return false;
}

function exDown(){
	$("#exdown").submit();
}

$(function(){
	$('input[type=text]').attr("onkeydown","javascript:if(event.keyCode==13){moveEmpUserList();}");
});