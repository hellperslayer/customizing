/**
 * AgentParamMngtList Javascript
 * @author yjyoo
 * @since 2015. 04. 17
 */

var $clone;

$(document).ready(function() {
	$clone = $("#agentParamListForm").clone();
});

function moveAgentParamList() {
	$("#agentParamListForm").attr("action", agentParamListConfig["listUrl"]);
	$("#agentParamListForm").submit();
}

// 상세 페이지 이동 (신규, 수정)
function findAgentParamDetail(agent_name){
	$("#agentParamListForm").attr("action", agentParamListConfig["detailUrl"]);
	$("#agentParamListForm").appendHiddenInput('agent_name', agent_name);
	$("#agentParamListForm").submit();
}

function saveAgentParam() {
	$("#agentParamDetailForm").attr("action", agentParamListConfig["saveUrl"]);
	
	var listcnt = $("#seqLength").val();
	var valArray = "";
	for(var a = 1; a < parseInt(listcnt)+1; a++) {
		var paramVal = $("#paramValue"+a).val();
		if(a == 1) {
			valArray = paramVal;
		} else {
			valArray = valArray+"///"+paramVal;
		}
	}
	
	$("#valueArray").val(valArray);
	sendAjaxAdminParam("save");
}

function sendAjaxAdminParam(type){
	var $form = $("#agentParamDetailForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = '';
	var menu_id = $('input[name=current_menu_id]').val();
/*	if (type == 'add') {
		log_message_title = '관리자등록';
		log_action = 'INSERT';
	} else if (type == 'save') {
		var password = $("#adminUserDetailForm input[name=password]").val();
		if(password != null && password != '') {
			log_message_title = '관리자수정(비밀번호수정)';
		}
		else {
			log_message_title = '관리자수정';
		}
		log_action = 'UPDATE';
	} else if (type == 'remove') {
		log_message_title = '관리자삭제';
		log_action = 'REMOVE';
	}*/
	
	log_message_title = '값수정';
	log_action = 'save';
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(agentParamListConfig[type + "Url"],$form.serialize(), ajaxAgentParam_successHandler, ajaxAgentParam_errorHandler, type);
}

//관리자 ajax call - 성공
function ajaxAgentParam_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = agentParamListConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}
		
		moveAgentParamList();
	}
}

// 관리자 ajax call - 실패
function ajaxAgentParam_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	log_message += codeLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("관리자 등록 실패하였습니다." + textStatus);
			break;
		case "save":
			alert("관리자 수정 실패하였습니다." + textStatus);
			break;
		case "remove":
			alert("관리자 삭제 실패하였습니다." + textStatus);
			break;
		default:
	}
}