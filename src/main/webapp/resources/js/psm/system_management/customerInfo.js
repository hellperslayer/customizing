
$(function(){
	AuthorCount();
	$('#customerInfoForm').ajaxForm({
		success: function(result){
			alert("성공적으로 등록되었습니다.");
			moveCustomerInfo();
		}
	});
});

$(function(){
	$('#customerInfoForm2').ajaxForm({
		success: function(result){
			alert("성공적으로 등록되었습니다.");
			moveCustomerInfo();
		}
	});
});

$(document).ready(function(){
	findDefaultDesc();
});


function check() {
	var file = customerInfoForm.filename.value;
	if(file != "") {
		var fileExt = file.substring(file.lastIndexOf(".") + 1);
		var reg = /gif|jpg|jpeg|png/i;	// 업로드 가능 확장자.
		if(reg.test(fileExt) == false) {
			alert("첨부파일은 gif, jpg, png로 된 이미지만 가능합니다.");
			return false;
		} else {
			return true;
		}
	} else {
		alert("이미지를 등록해 주세요.");
		return false;
	}
}

function check2() {
	var file = customerInfoForm2.filename2.value;
	if(file != "") {
		var fileExt = file.substring(file.lastIndexOf(".") + 1);
		var reg = /gif|jpg|jpeg|png/i;	// 업로드 가능 확장자.
		if(reg.test(fileExt) == false) {
			alert("첨부파일은 gif, jpg, png로 된 이미지만 가능합니다.");
			return false;
		} else {
			return true;
		}
	} else {
		alert("이미지를 등록해 주세요.");
		return false;
	}
}

function addFile() {
	
	var file = customerInfoForm.filename.value;
	if(file != "") {
		var fileExt = file.substring(file.lastIndexOf(".") + 1);
		var reg = /gif|jpg|jpeg|png/i;	// 업로드 가능 확장자.
		if(reg.test(fileExt) == false) {
			alert("첨부파일은 gif, jpg, png로 된 이미지만 가능합니다.");
			return;
		}
	}
	
	var confirmVal = confirm("등록하시겠습니까?");
	if(confirmVal){
		sendAjaxRegisterImageLogo();	
	}else{
		return false;
	}
	//$("#customerInfoForm").attr("action", customerInfoConfig["addFileUrl"]);
	//$("#customerInfoForm").submit();
}


function sendAjaxRegisterImageLogo() {
	/*$.ajax({
		type: 'POST',
		url: rootPath + '/customerInfo/upload.html',
		data: {
		},
		success: function() {
			alert("성공적으로 등록되었습니다.");
			moveCustomerInfo();
		}
	});*/
	$('#customerInfoForm').ajaxForm({
		url: rootPath + '/customerInfo/add.html',
		success: function(result){
			alert("성공적으로 등록되었습니다.");
		}
	});
	
	//$("#customerInfoForm").submit();
}

function moveCustomerInfo(){
	$("#listForm").attr("action",customerInfoConfig["listUrl"]);
	$("#listForm").submit();
}



function useLogoImage(idx, type) {
	var text;
	if(type == 1)
		text = "사용을 해제하시겠습니까?";
	else if(type == 2)
		text = "사용하시겠습니까?";
	
	if(confirm(text)) {
		$.ajax({
			type: 'POST',
			url: rootPath + '/customerInfo/useLogoImage.html',
			data: { 
				idx: idx,
				type: type
			},
			success: function(data) {
				alert("변경되었습니다.");
				moveCustomerInfo();
			}
		});
	}
}

function useLogoImage2(idx, type) {
	var text;
	if(type == 1)
		text = "사용을 해제하시겠습니까?";
	else if(type == 2)
		text = "사용하시겠습니까?";
	
	if(confirm(text)) {
		$.ajax({
			type: 'POST',
			url: rootPath + '/customerInfo/useLogoImage2.html',
			data: { 
				idx: idx,
				type: type
			},
			success: function(data) {
				alert("변경되었습니다.");
				moveCustomerInfo();
			}
		});
	}
}

function deleteLogoImage(idx) {
	
	if(confirm("삭제하시겠습니까?")) {
		$.ajax({
			type: 'POST',
			url: rootPath + '/customerInfo/deleteLogoImage.html',
			data: { 
				idx: idx
			},
			success: function(data) {
				alert("삭제되었습니다.");
				moveCustomerInfo();
			}
		});
	}
}

function AuthorCount(){
	var count = $('#countSelect').val();
	$('#html_source').html(
			'<TABLE border="1" id="preview" cellspacing="0" cellpadding="0" style="border-collapse:collapse; border: 1px solid black">'
				+'<TR id="authorNameTr">'
				+'<TD rowspan="2" id="authorNameFirst" valign="middle" style="width: 27px; height: 114px; padding: 1.4pt 5.1pt 1.4pt 6pt;">'
				+'<P><SPAN>결</SPAN></P>'
				+'<P STYLE="margin: 0px"><SPAN>재</SPAN></P>'
				+'</TD>'
				+'</TR>'
				+'<TR id="authorMarkTr">'
				+'</TR>'
				+'</TABLE>');
	$('.authorName').prop('disabled',true);
	$('#preview').css('display','');
	$(".authorNameTd").remove();
	$(".authorMarkTd").remove();
	switch(count){
	case '3':
		$('#authorName3').removeAttr('disabled');
		tableAddContent($('#authorName3').val());
	case '2':
		$('#authorName2').removeAttr('disabled');
		tableAddContent($('#authorName2').val());
	case '1':
		$('#authorName1').removeAttr('disabled');
		tableAddContent($('#authorName1').val());
		break;
	default:
		$('#preview').css('display','none');
	}
}

function tableAddContent(name){
	$('#authorNameFirst').after(
			'<TD valign="middle" class="authorNameTd">'
			+'<P STYLE="margin: 0px; text-align: center;"><SPAN>'+name+'</SPAN></P>'
			+'</TD>');
	$('#authorMarkTr').append(
			'<TD valign="middle" class="authorMarkTd" style="width: 91px; height: 79px; padding: 1.4pt 5.1pt 1.4pt 5.1pt;">'
			+'<P><SPAN>&nbsp;</SPAN></P>'
			+'</TD>');
}

function addAuthorizeLine(){
	AuthorCount();
	$.ajax({
		type: 'POST',
		url: rootPath + '/customerInfo/addAuthorizeLine.html',
		dataType : 'json',
		data: { 
			count : $('#countSelect').val(),
			name1 : $('#authorName1').val(),
			name2 : $('#authorName2').val(),
			name3 : $('#authorName3').val(),
			html_source : $('#html_source').html()
		},
		success: function(data) {
			if(data == 'success'){
				alert("변경되었습니다.")
			}else{
				alert("오류가 발생했습니다.");
			}
		}
	});
}

function findDefaultDesc(){
	$.ajax({
		type: 'POST',
		url: rootPath + '/customerInfo/findDefaultDesc.html',
		dataType : 'json',
		data: { 
			report_code : $('#report_code').val()
		},
		success: function(data) {
			$('#defaultdesc').val(data);
		}
	});
}

function addDefaultDesc(){
	$.ajax({
		type: 'POST',
		url: rootPath + '/customerInfo/addDefaultDesc.html',
		dataType : 'json',
		data: { 
			report_code : $('#report_code').val()
			,description : $('#defaultdesc').val()
		},
		success: function(data) {
			if(data == 'success'){
				alert("변경되었습니다.")
			}else{
				alert("오류가 발생했습니다.");
			}
		}
	});
}