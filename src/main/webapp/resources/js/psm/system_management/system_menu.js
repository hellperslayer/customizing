$(function(){
	var systemSeq = $("#systemListForm input[name=sel_system_seq]").val();
	// 메뉴 최초 접근
	if(systemSeq == null || systemSeq =="") {
		return null;
	}else{
		findSystemDetail(systemSeq);
	}
});

// 부서 상세정보 요청
function findSystemDetail(systemSeq){
	sendAjaxPostRequest(systemConfig["detailUrl"],{'system_seq':systemSeq},findSystemDetail_successHandler, findSystemDetail_errorHandler);
}

// 부서 상세정보 요청 - 성공
function findSystemDetail_successHandler(data, dataType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = systemConfig['loginPage'];
			return false;
		}
	}
	
	if(data == null || !data.hasOwnProperty("systemDetail")){
		alert("상세 데이터 없음");
		return false;
	}
	
	showSystemDetail(data);
}

// 부서 상세정보 요청 - 실패
function findSystemDetail_errorHandler(XMLHttpRequest, textStatus, errorThrown){
	alert("findSystemDetail_errorHandler : " + textStatus);
}

// 부서 상세화면 노출
function showSystemDetail(data){
	$("#systemDetailDiv").show();
	var systemSeq = initSystemDetail();
	
	if(data == null || data == ''){ // 신규
		$("#systemDetailForm input[name=parent_system_seq]").removeAttr("name");
		$("#systemDetailForm select[id=parent_system_seq_select]").attr("name", "parent_system_seq");
		$("#systemDetailForm select[name=parent_system_seq]").show();
		$("#systemDetailForm select[name=parent_system_seq]").val(systemSeq);
		$("#rootSystemParentNameSpan").hide();
		
		$("#insertTr").hide();
		$("#updateTr").hide();
		
		$("#addSystemBtn").show();
		$("#saveSystemBtn").hide();
		$("#removeSystemBtn").hide();
	}else{ // 상세
		var systemDetail = data.systemDetail;
		
		$("#systemSeqLabel").html("· SYSTEM SEQ : " + systemDetail.system_seq);
		$("#systemDetailForm input[name=system_seq]").val(systemDetail.system_seq);

		$("#systemDetailForm input[name=system_name]").val(systemDetail.system_name);
		$("#systemDetailForm select[name=system_type]").val(systemDetail.system_type);
		$("#systemDetailForm input[name=description]").val(systemDetail.description);
		$("#systemDetailForm input[name=main_url]").val(systemDetail.main_url);
		$("#systemDetailForm input[name=administrator]").val(systemDetail.administrator);
		$("#systemListForm input[name=sel_system_seq]").val(systemDetail.system_seq);
		
		$("#systemDetailForm select[name=agent_type]").val(systemDetail.agent_type);
		$("#systemDetailForm input[name=collection_server_ip]").val(systemDetail.collection_server_ip);
		$("#systemDetailForm input[name=analysis_server_ip]").val(systemDetail.analysis_server_ip);
		
		var network_type = systemDetail.network_type;
		$("#systemDetailForm input:radio[name=network_type]").prop("checked", false);
		$("#systemDetailForm input:radio[name=network_type]:radio[value="+network_type+"]").prop("checked", true);
		
		$("#systemDetailForm #threshold").val(systemDetail.threshold);
		var identiInfoList = data.identiInfoList;
		if(identiInfoList.length==0) {
			for(var i=0; i<4; i++){
				var num = i+1;
				$("#identiInfoForm input:radio[name=existence_yn_"+num+"]:radio[value='Y']").prop("checked", false);
				$("#identiInfoForm input:radio[name=existence_yn_"+num+"]:radio[value='N']").prop("checked", false);
				$("#identiInfoForm input[name=information_ct_"+num+"]").css("display", "");
				$("#identiInfoForm input[name=information_ct_"+num+"]").val('');
			}
		}
		for(var i=0; i<identiInfoList.length; i++){
			var existence_yn = identiInfoList[i].existence_yn
			var num = i+1;
			if(existence_yn=='Y'){
				$("#identiInfoForm input:radio[name=existence_yn_"+num+"]:radio[value='Y']").prop("checked", true);
				var information_ct = identiInfoList[i].information_ct
				$("#identiInfoForm input[name=information_ct_"+num+"]").css("display", "");
				$("#identiInfoForm input[name=information_ct_"+num+"]").val(information_ct);
			} else {
				$("#identiInfoForm input:radio[name=existence_yn_"+num+"]:radio[value='N']").prop("checked", true);
				$("#identiInfoForm input[name=information_ct_"+num+"]").css("display", "none");
			}
		}
		$("#insertTr").show();
		$("#updateTr").show();
	}
}

// 부서 상세화면 초기화
function initSystemDetail(){
	var systemId = $("#systemDetailForm input[name=system_seq]").val();
	$("#systemDetailForm input[name=system_seq]").val('');
	$("#systemDetailForm input[name=system_name]").val('');
	$("#systemDetailForm input[id=parent_system_seq]").val(systemId);
	
	
	return systemId;
}


function addSystem(){
	$("#systemListForm").attr("action",systemConfig["addView"]);
	$("#systemListForm").submit();
}

function addPart(){
	var system_name = $('#systemDetailForm2 input[name=system_name]').val();
	var system_type = $('#systemDetailForm2 input[name=system_type]').val();
	var system_seq = $('#systemDetailForm2 input[name=system_seq]').val();
	var description = $('#systemDetailForm2 input[name=description]').val();
	
	if(system_seq == '') {
		alert("시스템 코드를 입력해주세요.");
		return false;
	}
	if(system_name == '') {
		alert("시스템명을 입력해주세요.");
		return false;
	}
	if(system_type == '') {
		alert("시스템 타입을 입력해주세요.");
		return false;
	}
	if(description == ''){
		alert("정렬순서를 입력해주세요.");
		return false;
	}else if(description == '0'){
		alert("정렬순서에 '0'을 입력할 수 없습니다.");
		return false;
	}
	
	sendAjaxDepartment2("add");	
}


function sendAjaxDepartment2(type){
	var $form = $("#systemDetailForm2");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'system_name,parent_system_id,system_type,description,system_seq';
	var menu_id = $('input[name=current_menu_id]').val();
    if (type == 'add') {
		log_message_title = '시스템등록';
		log_action = 'INSERT';
	} 
  
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);

	sendAjaxPostRequest(systemConfig[type+"Url"], $form.serialize(), ajaxDepartment_successHandler, ajaxDepartment_errorHandler, type);
}


function saveSystem(){
	var system_name = $('#systemDetailForm input[name=system_name]').val();
	var system_type = $('#systemDetailForm input[name=system_type]').val();
	var description = $('#systemDetailForm input[name=description]').val();

	if(system_name == ''){
		alert("시스템명을 입력해주세요.");
		return false;
	}
	if(system_type == '') {
		alert("시스템 타입을 입력해주세요.");
		return false;
	}
	if(description == ''){
		alert("정렬순서를 입력해주세요.");
		return false;
	}else if(description == '0'){
		alert("정렬순서에 '0'을 입력할 수 없습니다.");
		return false;
	}
	sendAjaxDepartment("save");	
}

// 부서 삭제
function removeSystem(){
	if(confirm("삭제하시겠습니까?")) {
		sendAjaxDepartment("remove");
	}
}

// 부서 ajax call
function sendAjaxDepartment(type){
	var $form = $("#systemDetailForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'system_name,system_type,description,system_seq';
	var menu_id = $('input[name=current_menu_id]').val();
    if (type == 'save') {
		log_message_title = '시스템수정';
		log_action = 'UPDATE';
	} else if (type == 'remove') {
		log_message_title = '시스템삭제';
		log_action = 'REMOVE';
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);

	sendAjaxPostRequest(systemConfig[type+"Url"], $form.serialize(), ajaxDepartment_successHandler, ajaxDepartment_errorHandler, type);

	
}

// 부서 ajax call - 성공
function ajaxDepartment_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = ipConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}
		
		moveSystemList();
	}
}

// 부서 ajax call - 실패
function ajaxDepartment_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){
	
	//log_message += deptLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("부서 등록 실패하였습니다." + textStatus);
			break;
		case "save":
			alert("부서 수정 실패하였습니다." + textStatus);
			break;
		case "remove":
			alert("부서 삭제 실패하였습니다." + textStatus);
			break;
		default:
	}
}

function removeAdd(){
	$("#systemDetailDiv").css("display","block");
	$("#systemDetailForm2 input[name=system_name]").val("");
	$("#systemDetailForm2 input[name=system_type]").val("");
	$("#systemDetailForm2 input[name=descriptioe]").val("");
	
}

// 목록으로 이동
function moveSystemList(){
	$("#systemListForm").attr("action",systemConfig["listUrl"]);
	$("#systemListForm").submit();
}

function inputTagCk(no) {
	var existence_yn = $("#identiInfoForm input:radio[name=existence_yn_"+no+"]:checked").val();
	if(existence_yn=='Y'){
		$("#identiInfoForm input[name=information_ct_"+no+"]").css("display", "");
		$("#identiInfoForm input[name=information_ct_"+no+"]").val(0);
	} else {
		$("#identiInfoForm input[name=information_ct_"+no+"]").val(0);
		$("#identiInfoForm input[name=information_ct_"+no+"]").css("display", "none");
	}
}

function saveIdentiInfo() {
	var system_seq = $("#systemDetailForm input[name=system_seq]").val();
	console.log(system_seq);
	if(system_seq==null || system_seq==''){
		alert("시스템을 선택해주세요");
		return false;
	}
	var existence_yn_1 = $("#identiInfoForm input:radio[name=existence_yn_1]:checked").val();
	var information_ct_1 = $("#identiInfoForm input[name=information_ct_1]").val();
	
	var existence_yn_2 = $("#identiInfoForm input:radio[name=existence_yn_2]:checked").val();
	var information_ct_2 = $("#identiInfoForm input[name=information_ct_2]").val();

	var existence_yn_3 = $("#identiInfoForm input:radio[name=existence_yn_3]:checked").val();
	var information_ct_3 = $("#identiInfoForm input[name=information_ct_3]").val();

	var existence_yn_4 = $("#identiInfoForm input:radio[name=existence_yn_4]:checked").val();
	var information_ct_4 = $("#identiInfoForm input[name=information_ct_4]").val();
	
	if(existence_yn_1==null || existence_yn_2==null || existence_yn_3 == null || existence_yn_4 ==null){
		alert("고유식별보유현황 유무를 선택해주세요.");
		return false;
	}
	$.ajax({
		type: 'POST',
		url: rootPath + '/systemMngt/saveIdentiInfo.html',
		data: { 
			system_seq : system_seq,
			existence_yn_1 : existence_yn_1,
			information_ct_1 : information_ct_1,
			existence_yn_2 : existence_yn_2,
			information_ct_2 : information_ct_2,
			existence_yn_3: existence_yn_3,
			information_ct_3: information_ct_3,
			existence_yn_4: existence_yn_4,
			information_ct_4: information_ct_4
		},
		success: function(data) {
			var result = JSON.parse(data);
			console.log(result);
			if(result=='success') {
				alert("저장에 성공하였습니다.");
			} else {
				alert("저장에 실패하였습니다.");
			}
		},
		error:function(){
			alert("저장에 실패하였습니다.");
		}
	});
	
}

function batchMenu(){
	var isCheck = $("#sub_batch").prop("checked");
	var btn = $("#subBatchBtn");
	if(isCheck){
		$("#sub_batch").prop("checked",false);
		btn.removeClass("blue");
		btn.addClass("btn-default");
		$("#checkFont").removeClass("fa-check-square");
		$("#checkFont").addClass("fa-square");
	}else{
		$("#sub_batch").prop("checked",true);
		btn.removeClass("btn-default");
		btn.addClass("blue");
		$("#checkFont").removeClass("fa-square");
		$("#checkFont").addClass("fa-check-square");
	}
	btn.blur();
	//sendAjaxMenu("add");
}






