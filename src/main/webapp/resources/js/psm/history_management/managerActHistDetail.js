/**
 * ManagerActList Javascript
 * @author yjyoo
 * @since 2015. 04. 23
 */
var $clone;

$(document).ready(function() {
	$clone = $("#managerActHistListForm").clone();
});

function moveManagerActHistList() {
	$("#managerActHistListForm").attr("action",managerActHistDetailConfig["listUrl"]);
	$("#managerActHistListForm").submit();
}
