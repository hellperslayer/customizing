/**
 * BackupList Javascript
 * @author yjyoo
 * @since 2015. 04. 23
 */

var $clone;

$(document).ready(function() {
	$clone = $("#backupHistListForm").clone();
	
	// DatePicker
	/*var dates = '';
	dates = $("input[name=search_from], input[name=search_to]").datepicker(
		{	
			showOn: 'both',
			buttonImage: rootPath + "/resources/image/common/icon_calender.gif",
			buttonImageOnly: true,
			showAnimation: 'slide',
			showOtherMonths: true,
			selectOtherMonths: true,
			changeYear: true,
			changeMonth: true,
			maxDate: 0,
			onSelect: function(selectedDate) {
				var option = this.name == "search_from" ? "minDate" : "maxDate",
					instance = $(this).data("datepicker"),
					date = $.datepicker.parseDate(
							instance.settings.dateFormat || $.datepicker._defaults.dateFormat,
							selectedDate, instance.settings
					);
				dates.not(this).datepicker("option", option, date);
			}
		}
	);
	$("input[name=search_from], input[name=search_to]").attr("readonly", "readonly");
	$("img.ui-datepicker-trigger").attr("style", "display: none;");*/
});

// 페이지 이동
function goPage(num){
	$("#backupHistListForm").attr("action",backupHistConfig["listUrl"]);
	$("#backupHistListForm input[name=page_num]").val(num);
	$("#backupHistListForm").submit();
}

// 검색
function moveBackupHistList() {
	var listForm = $("#backupHistListForm");
	var tDate = listForm.find("input[name=search_to]").val();
	var fDate = listForm.find("input[name=search_from]").val();
	if(tDate<fDate){
		alert("검색 기간을 확인하세요");
		return;
	}
	$("#backupHistListForm").attr("action",backupHistConfig["listUrl"]);
	$("#backupHistListForm input[name=page_num]").val('1');
	$("#backupHistListForm input[name=isSearch]").val('Y');
	$("#backupHistListForm").submit();
}

// 엑셀 다운로드
function excelBackupHistList() {
	
	var $form = $("#backupHistListForm");
	var log_message_params = '';
	var menu_id = $('input[name=current_menu_id]').val();
	var log_message_title = '백업이력조회 엑셀 다운로드';
	var log_action = 'EXCEL DOWNLOAD';
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	$("#backupHistListForm").attr("action",backupHistConfig["downloadUrl"]);
	$("#backupHistListForm").submit();
}

function checkIntegrity(id) {
	//var $form = $("#backupHistListForm");
	$.ajax({
		type: 'POST',
		url: rootPath + '/backupHist/checkIntegrity.html',
		data: {
			"backup_log_id": id
		},
		success: function(data) {
			if (data.result == "OK") {
				alert("위변조 확인 결과 이상없습니다.\nHASHCODE : "+id);
			} else if (data.result == "NO_FILE") {
				alert("해당 파일이 존재하지 않습니다.");
			} else if (data.result == "NO_HASH") {
				if (confirm("위변조가 발생하였습니다.\n복구하시겠습니까?")) {
					restore(id);
				}
			} else {
				alert("오류가 발생하였습니다.");
			}
		}
	});
}

function restore(id) {
	$.ajax({
		type: 'POST',
		url: rootPath + '/backupHist/restore.html',
		data: {
			"backup_log_id": id
		},
		success: function(data) {
			if (data.restore_result == "OK") {
				alert("복구하였습니다.");
				moveBackupHistList();
			}  else {
				alert("오류가 발생하였습니다.");
			}
		}
	});
}

function checkFile(id) {
	$.ajax({
		type: 'POST',
		url: rootPath + '/backupHist/checkFile.html',
		data: {
			"backup_log_id": id
		},
		success: function(data) {
			if (data.result == "NO_FILE") {
				alert("파일이 없습니다.");
			}else if (data.result == "NO_DATA"){
				alert("백업 정보가 없습니다.");
			}else if (data.result == "ERROR"){
				alert("오류가 발생했습니다.");
			}else{
				alert("암호화 데이터 : "+data.result+"...");
			}
			console.log(data);
		}
	});
}


function checkDetail(dateVal, log_type) {
	$.ajax({
		type: 'POST',
		url: rootPath + '/backupHist/checkLogCount.html',
		data: {
			proc_date : dateVal,
			log_type : log_type
		},
		success: function(data) {
			$('#countInfo').empty();
			$('#countInfo').append(data);
			$("#myModal").modal();
		}
	});
}
