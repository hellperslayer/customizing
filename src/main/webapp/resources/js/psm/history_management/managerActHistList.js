/**
 * ManagerActList Javascript
 * @author yjyoo
 * @since 2015. 04. 23
 */

var $clone;

$(document).ready(function() {
	var listForm = $("#managerActHistListForm");
	var tDate = listForm.find("input[name=search_to]").val();
	var fDate = listForm.find("input[name=search_from]").val();
	if(tDate<fDate){
		alert("검색 기간을 확인하세요");
		return;
	}
	$clone = $("#managerActHistListForm").clone();
	
	// DatePicker
	/*var dates = '';
	dates = $("input[name=search_from], input[name=search_to]").datepicker(
		{	
			maxDate: 0,
			showOn: 'both',
			buttonImage: rootPath + "/resources/image/common/icon_calender.gif",
			buttonImageOnly: true,
			showAnimation: 'slide',
			showOtherMonths: true,
			selectOtherMonths: true,
			changeYear: true,
			changeMonth: true,
			onSelect: function(selectedDate) {
				var option = this.name == "search_from" ? "minDate" : "maxDate",
					instance = $(this).data("datepicker"),
					date = $.datepicker.parseDate(
							instance.settings.dateFormat || $.datepicker._defaults.dateFormat,
							selectedDate, instance.settings
					);
				dates.not(this).datepicker("option", option, date);
			}
		}
	);
	$("input[name=search_from], input[name=search_to]").attr("readonly", "readonly");
	$("img.ui-datepicker-trigger").attr("style", "display: none;");*/
});

// 페이지 이동
function goPage(num){
	$clone.attr("action",managerActHistConfig["listUrl"]);
	$clone.find("input[name=page_num]").val(num);
	$clone.attr("style", "display: none;");
	$('body').append($clone);
	$clone.submit();
}

// 검색
function moveManagerActHistList() {
	$("#managerActHistListForm").attr("action",managerActHistConfig["listUrl"]);
	$("#managerActHistListForm input[name=isSearch]").val('Y');
	$("#managerActHistListForm input[name=page_num]").val('1');
	$("#managerActHistListForm").submit();
}

//검색
function moveManagerActHistDetail(log_auth_id) {
	$("#managerActHistListForm").attr("action",managerActHistConfig["detailUrl"]);
	$("#managerActHistListForm input[name=isSearch]").val('Y');
	$("#managerActHistListForm input[name=page_num]").val('1');
	$("#managerActHistListForm input[name=log_auth_id]").val(log_auth_id);
	$("#managerActHistListForm").submit();
}

// 엑셀 다운로드
function excelManagerActHistList() {
	
	var $form = $("#managerActHistListForm");
	var log_message_params = '';
	var menu_id = $('input[name=current_menu_id]').val();
	var log_message_title = '감사이력 엑셀 다운로드';
	var log_action = 'EXCEL DOWNLOAD';
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	$("#managerActHistListForm").attr("action",managerActHistConfig["downloadUrl"]);
	$("#managerActHistListForm").submit();
}
