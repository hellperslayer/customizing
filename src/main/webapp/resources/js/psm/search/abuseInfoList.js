/**
 * 추출조건별조회 Javascript
 * @author tjlee
 * @since 2014. 4. 10
 */
	 	
 // 상세로 이동

$(document).ready(function() {
	
	$('.left_area').css('display', 'none');
});

function fnextrtCondbyInqDetail(log_seq,proc_date){
	$("input[name=detailEmpCd]").attr("value",log_seq);
	$("input[name=detailOccrDt]").attr("value",proc_date);
	
	$("#extrtCondbyInqDetailForm").attr("action",abuseInfoConfig["detailUrl"]);
	$("#extrtCondbyInqDetailForm").submit();
}

// 상세에서 전체로그 상세로 이동
function fnAbuseInfo(desc_seq){
	$("input[name=desc_seq]").attr("value",desc_seq);
	
	$("#listForm").attr("action",abuseInfoConfig["detailUrl"]);
	$("#listForm").submit();
}

// 페이지 이동
function goPage(num){
	$("#listForm").attr("action",abuseInfoConfig["listUrl"]);
	$("#listForm input[name=page_num]").val(num);
	$("#listForm").submit();
}

// 상세페이지 페이징 
function goPageextrtCondbyInqDetail(num){
	$("#listForm").attr("action",abuseInfoConfig["detailUrl"]);
	$("#listForm input[name=extrtCondbyInqDetail_page_num]").val(num);
	$("#listForm").submit();
}

// 검색
function moveAbuseInfoList() {
	$('input[name=page_num]').attr("value","1");
	$('input[name=isSearch]').attr("value","Y");
	$("#listForm").attr("action",abuseInfoConfig["listUrl"]);
	$("#listForm").submit();
}


// 기간선택 검색조건
function initDaySelect(){
	if($("#daySelect").val() == "Day"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "WeekDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-7d");
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "MonthDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-1m");
		var setDateStart = $("#search_fr").val();
		setDateStart = setDateStart.substr(0,8);
		setDateStart = setDateStart + "01";
		$("#search_fr").val(setDateStart);
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "YearDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
		var setDateStart = $("#search_fr").val();
		setDateStart = setDateStart.substr(0,5);
		setDateStart = setDateStart + "01-01";
		$("#search_fr").val(setDateStart);
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "TotalMonthDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-1m");
		var setDateStart = $("#search_fr").val();
		setDateStart = setDateStart.substr(0,8);
		setDateStart = setDateStart + "01";
		$("#search_fr").val(setDateStart);
		var setDateend =  $("#search_to").val();
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","0");
		setDateend = setDateend.substr(8,10);
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-"+setDateend+"d");
	}else{
		dateDisabled(false);
	}
}

function goAbuseInfoList() {
	$("#abuseDetailForm").attr("action",abuseInfoConfig["listUrl"]);
	$("#abuseDetailForm").submit();
}

function dateDisabled(bool){
	$('.search_fr').attr('disabled', bool);
	$('.search_to').attr('disabled', bool);
	$('.ui-datepicker-trigger').css('visibility', bool == true ? 'hidden' : 'visible');
	$('.ui-datepicker-trigger').css('visibility', bool == true ? 'hidden' : 'visible');
}

// 상세창 클릭
function fnExtrtDetailLog(occr_dt,emp_user_id,emp_detail_seq,rule_cd){
	$("input[name=detailOccrDt]").attr("value",occr_dt);
	$("input[name=detailEmpCd]").attr("value",emp_user_id);
	$("input[name=detailEmpDetailSeq]").attr("value",emp_detail_seq);
	$("input[name=bbs_id]").attr("value","empDetailInq");
	$("select[name=rule_cd]").attr("value","");
	
	$("#listForm").attr("action",abuseInfoConfig["detailUrl"]);
	$("#listForm").submit();
}

function fnExtrtCondbyInqListTrClick(rule_cd){
	$('input[name=detailRuleCd]').attr("value",rule_cd);
	var $lstForm = $('#listForm');
	$lstForm.submit();
}

/**
 * 엑셀 다운로드 추가 2015/6/4
 */
function excelAbuseInfoList(){
//	if(arguments[0] >= 65535){
//		alert("엑셀다운로드는 65535건까지 가능합니다.");
//	}else{
		
		var $form = $("#listForm");
		var log_message_params = '';
		var menu_id = $('input[name=current_menu_id]').val();
		var log_message_title = '부적정 판정자 처분결과 엑셀 다운로드';
		var log_action = 'EXCEL DOWNLOAD';
		
		$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
		
		$("#listForm").attr("action",abuseInfoConfig["downloadUrl"]);
		$("#listForm").submit();
//	}
}

/**
2017.01.18 사용자 개인정보 유형 분석
*/

function fnExtrtEmpDetailInfo(emp_user_id, user_ip, detailOccrDt) {
	//$("input[name=emp_user_name]").attr("value",emp_user_name);
	$("input[name=userId]").attr("value",emp_user_id);
	$("input[name=user_ip]").attr("value",user_ip);
	$("input[name=detailOccrDt]").attr("value",detailOccrDt);
/*	$("input[name=detailOccrDt]").attr("value",occr_dt);
	$("input[name=detailEmpCd]").attr("value",emp_user_id);
	$("input[name=detailEmpDetailSeq]").attr("value",emp_detail_seq);
	$("input[name=bbs_id]").attr("value","empDetailInq");
	$("select[name=rule_cd]").attr("value","");*/
	
	$("#listForm").attr("action",abuseInfoConfig["detailChartUrl"]);
	$("#listForm").submit();
}

function fnExtrtEmpDetailInfo2(detailOccrDt, emp_user_id, dept_id) {
	
	parent.$("input[name=detailOccrDt]").attr("value",detailOccrDt);
	parent.$("input[name=userId]").attr("value",emp_user_id);
	parent.$("input[name=dept_id]").attr("value",dept_id);
	parent.$("#listForm").attr("action","detailChart.html");
	parent.$("#listForm").submit();
}

/**
2017.06.19 부서별 비정상위험분석 상세차트
*/


/**
2017.04.17 테이블 정렬 
*/

function sendDesc(check_type, sort_flag){
	var listForm = $("#listForm");
	$("#listForm").attr("action",abuseInfoConfig["listUrl"]);	
	listForm.find("input[name=check_type]").val(check_type);
	listForm.find("input[name=sort_flag]").val(sort_flag);
	$("#listForm").submit();
}

function selScenario() {
	var scen_seq = $("#scen_seq").val();
	
	$.ajax({
		type:'POST',
		url : rootPath + '/extrtCondbyInq/findScenarioByScenSeq.html',
		data: {
			scen_seq : scen_seq
		},
		success : function(data){
			setRuleTblList(data);
		}
	});
}

function saveAbuseInfo() {
	/*var followup = $('textarea[name=followup]').val();
	if(followup == '') {
		alert("조치 내용을 입력해주세요");
		$('textarea[name=followup]').focus();
		return false;
	}*/
	
	var confirmVal = confirm("저장하시겠습니까?");
	if(confirmVal){
		var $form = $("#abuseDetailForm");
		var log_action = 'UPDATE';
		var log_message_params = '';
		var menu_id = $('input[name=current_menu_id]').val();
		var log_message_title = '부적정 판정자 처분 저장';
		var type = "save";
		
		$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
		sendAjaxPostRequest(abuseInfoConfig["saveUrl"],$form.serialize(), ajaxAbuseInfo_successHandler, ajaxAbuseInfo_errorHandler, type);
	}else{
		return false;
	}
}

function ajaxAbuseInfo_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = abuseInfoConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		alert("성공적으로 저장되었습니다.");
		
		window.location.reload();
	}
}

function ajaxAbuseInfo_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	alert("부적정 판정자 처분 저장 실패하였습니다." + textStatus);
}
