function summonUserAdd() {
	var user_Id = $("#emp_user").val();
	var search_to = $("#search_to").val();
	var search_from = $("#search_from").val();
	var auth_ids = $("#auth_ids").val();
	if(user_Id == "") {
		alert("위임자를 선택하시길 바랍니다.");
	}
	$.ajax({
		type: 'POST',
		url: rootPath + '/extrtCondbyInq/summonUserAdd.html',
		data: { 
			user_Id : user_Id,
			auth_ids : auth_ids,
			search_to : search_to,
			search_from : search_from
		},
		success: function(data) {
			if(data == 1) {
				alert("위임되었습니다.");
				moveList();
			} 
			else if(data == 2){
				alert("동일 위임자가 있어 수정되었습니다.");
				moveList();
			}
		}
	});
}

function summonUserRemove(delegation_seq, delegation_id) {
	$.ajax({
		type: 'POST',
		url: rootPath + '/extrtCondbyInq/summonUserRemove.html',
		data: {  
			delegation_seq : delegation_seq,
			delegation_id : delegation_id
		},
		success: function(data) {
			alert("취소되었습니다.");
			moveList();
		}
	});
}

function goPage(num){
	$("#misForm input[name=page_num]").val(num);
	moveList();
}

//목록으로 이동
function moveList(){
	$("#misForm").attr("action", summonDelegation["listUrl"]);
	$("#misForm").submit();
}

//검색
function moveIpInfoList(){
	$("#misForm input[name=page_num]").val("1");
	$("#misForm input[name=isSearch]").val("Y");
	$("#misForm").attr("action", ipInfoListConfig["listUrl"]);
	$("#misForm").submit();
}