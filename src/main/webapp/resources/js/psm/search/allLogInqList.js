/**
 * 전체로그조회 Javascript
 * @author tjlee
 * @since 2014. 4. 10
 */


function gotoReqList(system_seq){
	var mainMenuStr = '[{"menu_id":"MENU00416","menu_name":"접속이력조회","menu_url":"/reqLogInq/list.html","auth_id":"AUTH00000"},{"menu_id":"MENU00040","menu_name":"접근기록조회","menu_url":"/allLogInq/list.html","auth_id":"AUTH00000"},{"menu_id":"MENU00050","menu_name":"접근기록분석","menu_url":"/extrtCondbyInq/list.html","auth_id":"AUTH00000"},{"menu_id":"MENU00070","menu_name":"통계및보고","menu_url":"/statistics/list_perdbyTypeAnals.html","auth_id":"AUTH00000"},{"menu_id":"MENU00030","menu_name":"모니터링","menu_url":"/dashboard/init_dashboardView.html","auth_id":"AUTH00000"},{"menu_id":"MENU00080","menu_name":"정책설정관리","menu_url":"/indvinfoTypeSetup/list.html","auth_id":"AUTH00000"},{"menu_id":"MENU00010","menu_name":"환경설정","menu_url":"/groupCodeMngt/list.html","auth_id":"AUTH00000"}]';
	$("input[name=system_seq]").attr("value",system_seq);
	$("#listForm").attr("action",allLogInqConfig["reqUrl"]);
	$("#listForm").submit();
	drawMainMenu("mainMenus", mainMenuStr, "MENU00416");
}


// 상세로 이동
/*function fnAllLogInqDetail(log_seq,proc_date,proc_time,emp_user_name,user_id,result_type){
	$("input[name=detailLogSeq]").attr("value",log_seq);
	$("input[name=detailProcDate]").attr("value",proc_date);
	$("input[name=detailProcTime]").attr("value",proc_time);
	$("input[name=user_name]").attr("value",emp_user_name);
	$("input[name=result_type]").attr("value",result_type);
	$("input[name=user_id]").attr("value",user_id);
	
	$("#listForm").attr("action",allLogInqConfig["detailUrl"]);
	$("#listForm").submit();

}*/
function fnAllLogInqDetail(log_seq, proc_date){
	$("input[name=detailLogSeq]").attr("value",log_seq);
	$("input[name=detailProcDate]").attr("value",proc_date);
	
	$('#search_fr').attr('disabled', false);
	$('#search_to').attr('disabled', false);
	
	$("#listForm").attr("action",allLogInqConfig["detailUrl"]);
	$("#listForm").submit();
}

//부서 상세차트로 이동
function fnExtrtDeptDetailInfo(dept_id, detailOccrDt) {
	event.cancelBubble = true;
	
	var $form = $("#listForm");
	var log_message_params = '';
	var menu_id = $('input[name=current_menu_id]').val();
	var log_message_title = '접속기록조회상세 부서 상세차트';
	var log_action = '[SELECT]';
	
	$("input[name=dept_id]").attr("value",dept_id);
	$("input[name=detailOccrDt]").attr("value",detailOccrDt);
	
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	$("#listForm").attr("action",allLogInqConfig["deptDetailChartUrl"]);
	$("#listForm").submit();
}


// 페이지 이동
function goPage(num){
	// disabled된 input데이터 전송을 위해
	$('#search_fr').attr('disabled', false);
	$('#search_to').attr('disabled', false);
	
	$("#listForm").attr("action",allLogInqConfig["listUrl"]);
	$("#listForm input[name=page_num]").val(num);
	$("#listForm").submit();
}

//상세화면 페이지 이동
function goPageallLogInqDetail(num){
	$("#listForm").attr("action",allLogInqConfig["detailUrl"]);
	$("#listForm input[name=allLogInqDetail_page_num]").val(num);
	$("#listForm").submit();
}

// 검색

//유효성 검사

function chkForm(){
	
	var dept = $('input[name=dept_name]').val().trim();
	$('input[name=dept_name]').val(dept);
	
	var id = $('input[name=emp_user_id]').val().trim();
	$('input[name=emp_user_id]').val(id);
	
	var name = $('input[name=emp_user_name]').val().trim();
	$('input[name=emp_user_name]').val(name);
	
	var ip = $('input[name=user_ip]').val().trim();
	$('input[name=user_ip]').val(ip);
	
	var url = $('input[name=req_url]').val().trim();
	$('input[name=req_url]').val(url);
	
	
	moveallLogInqList();
}

function moveallLogInqList() {
	$('#loading').show();
	var listForm = $("#listForm");
	$('input[name=page_num]').attr("value","1");
	
	// disabled된 input데이터 전송을 위해
	$('#search_fr').attr('disabled', false);
	$('#search_to').attr('disabled', false);
	
	$("#listForm").attr("action",allLogInqConfig["listUrl"]);
	$("#listForm").submit();
}

function moveallLogInqList_ajax() {
	var listForm = $("#listForm").serialize();

	$('input[name=page_num]').attr("value","1");
	$.ajax({
		type: 'POST',
		url: rootPath + '/allLogInq/list.html',
		data: listForm,
		success: function(data) {
			alert("success");
		},
		beforeSend:function() {
			//$('#loading').show();
		},
		complete:function() {
			//$('#loading').hide();
		}
	});
}

function moveallLogInqListByReqType() {
	var listForm = $("#listForm");
	$("#listForm").attr("action",allLogInqByReqTypeConfig["listUrl"]);
	$("#listForm").submit();
}

// 기간선택 검색조건
function initDaySelect(){
	if($("#daySelect").val() == "Day"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
		$('#search_fr').attr('disabled', true);
		$('#search_to').attr('disabled', true);
	}else if($("#daySelect").val() == "WeekDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-7d");
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
		$('#search_fr').attr('disabled', true);
		$('#search_to').attr('disabled', true);
	}else if($("#daySelect").val() == "MonthDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-1m");
		var setDateStart = $("#search_fr").val();
		setDateStart = setDateStart.substr(0,8);
		setDateStart = setDateStart + "01";
		$("#search_fr").val(setDateStart);
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "YearDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
		var setDateStart = $("#search_fr").val();
		setDateStart = setDateStart.substr(0,5);
		setDateStart = setDateStart + "01-01";
		$("#search_fr").val(setDateStart);
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "TotalMonthDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-1m");
		var setDateStart = $("#search_fr").val();
		setDateStart = setDateStart.substr(0,8);
		setDateStart = setDateStart + "01";
		$("#search_fr").val(setDateStart);
		var setDateend =  $("#search_to").val();
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","0");
		setDateend = setDateend.substr(8,10);
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-"+setDateend+"d");
	}else if($("#daySelect").val() == "") {	// 직접입력
		$('#search_fr').attr('disabled', false);
		$('#search_to').attr('disabled', false);
	}else{
		dateDisabled(false);
	}
}

function dateDisabled(bool){
	$('.search_fr').attr('disabled', bool);
	$('.search_to').attr('disabled', bool);
	$('.ui-datepicker-trigger').css('visibility', bool == true ? 'hidden' : 'visible');
	$('.ui-datepicker-trigger').css('visibility', bool == true ? 'hidden' : 'visible');
}

//검색
function goList() {
	$("#listForm").attr("action",allLogInqConfig["listUrl"]);
	$("#listForm").submit();
}

function goBack() {
		$("#listForm").attr("action",allLogInqConfig["goBackUrl_shcd"]);
		$("#listForm").submit();
	}

// 추출조건 상세화면으로 이동
function moveExtrtCondbyInqDetail(){
	$("#listForm").attr("action",extrtCondbyInqConfig["detailUrl"]);
	$("#listForm").submit();
}
/**
 * 엑셀 다운로드 추가 2015/6/4
 */
function excelAllLogInqList(){
//	if(arguments[0] >= 65535){
//		alert("엑셀다운로드는 65535건까지 가능합니다.");
//	}else{
		
		var $form = $("#listForm");
		var log_message_params = '';
		var menu_id = $('input[name=current_menu_id]').val();
		var log_message_title = '접속기록조회 엑셀 다운로드';
		var log_action = 'EXCEL DOWNLOAD';
		
		$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
		
		$("#listForm").attr("action",allLogInqConfig["downloadUrl"]);
		$("#listForm").submit();
//	}
}

function excelAllLogInqDetail(){
//	if(arguments[0] >= 65535){
//		alert("엑셀다운로드는 65535건까지 가능합니다.");
//	}else{
		
		var $form = $("#listForm");
		var log_message_params = '';
		var menu_id = $('input[name=current_menu_id]').val();
		var log_message_title = '접속기록조회상세 엑셀 다운로드';
		var log_action = 'EXCEL DOWNLOAD';
		
		$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
		
		$("#listForm").attr("action",allLogInqConfig["downloadUrl"]);
		$("#listForm").submit();
//	}
}

function csvAllLogInqList() {
	
	var $form = $("#listForm");
	var log_message_params = '';
	var menu_id = $('input[name=current_menu_id]').val();
	var log_message_title = '접속기록조회 CSV 다운로드';
	var log_action = 'CSV DOWNLOAD';
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	$("#listForm").attr("action",allLogInqConfig["downloadCSVUrl"]);
	$("#listForm").submit();
}

function csvAllLogInqDetail() {
	
	var $form = $("#listForm");
	var log_message_params = '';
	var menu_id = $('input[name=current_menu_id]').val();
	var log_message_title = '접속기록조회 CSV 다운로드';
	var log_action = 'CSV DOWNLOAD';
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	$("#listForm").attr("action",allLogInqConfig["downloadCSVUrl"]);
	$("#listForm").submit();
}

function excelAllLogInqListByReqType() {
//	if(arguments[0] >= 65535){
//		alert("엑셀다운로드는 65535건까지 가능합니다.");
//	}else{
		$("#listForm").attr("action",allLogInqByReqTypeConfig["downloadUrl"]);
		$("#listForm").submit();
//	}
}

/*개인정보 제외 처리*/

function deletePrivacy(bizLog,privType,pattern){
	sendAjaxIpPerm("add",bizLog,privType,pattern);
}

function restorePrivacy(bizLog,privType,pattern) {
	sendAjaxIpPerm("remove",bizLog,privType,pattern);
}

function sendAjaxIpPerm(type,bizLog,privType,pattern){
	var $form = $("#listForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'misdetect_pattern, privacy_type';
	var menu_id = $('input[name=current_menu_id]').val();
	$('input[name=privacy_type]').attr("value",privType);
	$('input[name=misdetect_pattern]').attr("value",pattern);
	$('input[name=biz_log_result_seq]').attr("value",bizLog);
	
	if(type == "add") {
		log_message_title = '개인정보 제외';
		log_action = 'INSERT';
	}else if(type == "remove") {
		log_message_title = '개인정보 복원';
		log_action = 'DELETE';
	}
	
	$form.appendLogMessageParamsInput_allLogInq(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(allLogInqConfig[type + "Url"], $form.serialize(), ajaxIpPerm_successHandler, ajaxIpPerm_errorHandler, type);
}

// 그룹코드 ajax call - 성공
function ajaxIpPerm_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			alert("세션이 만료되었습니다.");
			location.href = allLogInqConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	} else {
		
		if(actionType == "add"){
			alert("성공적으로 제외되었습니다.");			
	
		}	
		else if(actionType == "remove"){
			alert("성공적으로 복원되었습니다.");
			
			
		}
		moveList();
		
	}
}

// 그룹코드 ajax call - 실패
function ajaxIpPerm_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	if(actionType == "add")
		alert("제외를 실패하였습니다. : " + textStatus);
	else if(actionType == "remove")
		alert("복원을 실패하였습니다. : " + textStatus);

}

function moveList(){
	$("#listForm").attr("action", allLogInqConfig["detailUrl"]);
	$("#listForm").submit();
}


$(function(){
	$('input[type=text]').attr("onkeydown","javascript:if(event.keyCode==13){moveallLogInqList();}");
	$('#search_fr').attr("onclick", "resetPeriod()");
	$('#search_to').attr("onclick", "resetPeriod()");
	
	var daySel = $('#daySelect').val();
	if (daySel == "Day" || daySel == "WeekDay") {
		$('#search_fr').attr('disabled', true);
		$('#search_to').attr('disabled', true);
	}
});

function fnExtrtEmpDetailInfo(emp_user_id, user_ip, detailOccrDt, system_seq) {
	
	var $form = $("#listForm");
	var log_message_params = '';
	var menu_id = $('input[name=current_menu_id]').val();
	var log_message_title = '[접속기록조회상세] 업무패턴보기';
	var log_action = 'SELECT';
	
	//$("input[name=emp_user_name]").attr("value",emp_user_name);
	$("input[name=userId]").attr("value",emp_user_id);
	$("input[name=user_ip]").attr("value",user_ip);
	$("input[name=detailOccrDt]").attr("value",detailOccrDt);
	$("input[name=system_seq]").attr("value",system_seq);
	
/*	$("input[name=detailOccrDt]").attr("value",occr_dt);
	$("input[name=detailEmpCd]").attr("value",emp_user_id);
	$("input[name=detailEmpDetailSeq]").attr("value",emp_detail_seq);
	$("input[name=bbs_id]").attr("value","empDetailInq");
	$("select[name=rule_cd]").attr("value","");*/
	
	
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	$("#listForm").attr("action",extrtCondbyInqConfig["detailChartUrl"]);
	$("#listForm").submit();
}

//목록으로 이동
function moveSummonDetail(){
	$("#listForm").attr("action",allLogInqConfig["dummondetailUrl"]);
	$("#listForm").submit();
}


//소명 요청
function addSummon(){
//	var cnt = $('input:checkbox[name=auth_ids]:checked').length;
	var description1 =htmlspecialchars($('textarea[name=description1]').val());
	var email_address =htmlspecialchars($('input[name=email_address]').val());
	//var description2 =htmlspecialchars($('textarea[name=description2]').val());
	var rule_nm =htmlspecialchars($('input[name=rule_nm]').val());
	/*if(description1 == '') {
		alert("소명요청사유를 입력해주세요");
		$('textarea[name=description1]').focus();
		return false;
	}*/
	if(email_address == '') {
		alert("E-MAIL을 입력해주세요");
		$('input[name=email_address]').focus();
		return false;
	}
	if(description1 == '') {
		alert("내용을 입력해주세요");
		$('textarea[name=description1]').focus();
		return false;
	}
		sendAjaxSummon("add");	
	
}


//소명요청 ajax call
function sendAjaxSummon(type){
	var $form = $("#summonDetailForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'description1,email_address';
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'add') {
		log_message_title = '소명요청';
		log_action = 'INSERT';
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(allLogInqConfig[type + "summonUrl"],$form.serialize(), ajaxAdminUser_successHandler, ajaxAdminUser_errorHandler, type);
}


//관리자 ajax call - 성공
function ajaxAdminUser_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = adminUserDetailConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}
		
		moveSummonDetail();
	}
}


// 관리자 ajax call - 실패
function ajaxAdminUser_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	log_message += codeLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("관리자 등록 실패하였습니다." + textStatus);
			break;
		case "save":
			alert("관리자 수정 실패하였습니다." + textStatus);
			break;
		case "remove":
			alert("관리자 삭제 실패하였습니다." + textStatus);
			break;
		default:
	}
}

function sendDesc(sort_flag){
	var listForm = $("#listForm");
	$("#listForm").attr("action",allLogInqConfig["listUrl"]);	
	listForm.find("input[name=sort_flag]").val(sort_flag);
	$("#listForm").submit();
}

$(function(){
	var sort_flag = $('#sort_flag').val();
	if(sort_flag == 1){
		$("#sort_result_type").attr("onclick","javascript:sendDesc(2)").attr("src", rootPath+"/resources/image/common/asc_btn.png")
		.attr("title","오름차순").attr("alt","오름차순"); 		
	}
});

function goMisdetect(bizLog,privType,pattern) {
	
	$('input[name=privacy_type]').attr("value",privType);
	$('input[name=misdetect_pattern]').attr("value",pattern);
	$('input[name=biz_log_result_seq]').attr("value",bizLog);
	
	$("#listForm").attr("action",allLogInqConfig["goMisdetectUrl"]);
	$("#listForm").submit();
}

function searhTime(obj) {
	var check = obj.checked;
	if(check == true) {
		$("select[id=start_h]").attr("disabled", false);
		$("select[id=end_h]").attr("disabled", false);
	} else {
		$("select[id=start_h]").attr("disabled", true);
		$("select[id=end_h]").attr("disabled", true);
	}
}

function searchResultType(result_type) {
	//var resultType = $('select[name=detailResultType]').val();
	$("#listForm").attr("action",allLogInqConfig["detailUrl"]);
	$("#listForm input[name=allLogInqDetail_page_num]").val(1);
	$('input[name=detailResultType]').val(result_type);
	$("#listForm").submit();
}

function showPrivacyInfo(resultContent, result_type) {
	if(result_type < 10) {
		result_type = 'privacy_0'+result_type;
	} else {
//		alert("정보주체조회를 지원하지 않는 개인정보유형입니다.");
//		return false;
		result_type = 'privacy_'+result_type;
	}
	
	$("#privacyForm input[name=privacyType]").val(result_type)
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/allLogInq/checkPrivacyInfo.html',
		data: { 
			"privacy": resultContent,
			"privacyType": result_type,
			"system_seq": $("#privacyForm input[name=system_seq]").val()
		},
		success: function(data) {
			var jsonData = JSON.parse(data);
			var errorCode = jsonData[0].errorCode;
			if(errorCode == '') {
				var title = "개인정보주체조회";
				var status = "toolbar=no,width=500,height=300";
				var url = rootPath + '/allLogInq/findPrivacyInfo.html?privacy='+jsonData[0].privacy+'&name='+jsonData[0].name;
				window.open(url,title,status);
			} else {
				if(errorCode == 'err1') {
					alert("접속정보가 존재하지 않습니다.");
				} else if(errorCode == 'err2') {
					alert("데이터가 존재하지 않습니다.");
				} else if(errorCode == 'err3') {
					alert("해당 개인정보가 존재하지 않습니다.");
				} else if(errorCode == 'err4') {
					alert("에러가 발생하였습니다.\n관리자에게 문의하세요.");
				}
			}
		},
		error:function(e){
			alert(e);
		}
	});
}


function showBizLog80Popup(log_seq) {
	var title = "개인정보주체조회";
	var status = "toolbar=no,width=1000,height=400,scrollbars=1";
	var url = rootPath + '/allLogInq/findBizLog80Popup.html?log_seq='+log_seq;
	window.open(url,title,status);
}

function ComSubmit(opt_formId) {
	this.formId = "";
	if(opt_formId == null)
		this.formId = "listForm";
	else
		this.formId = opt_formId;
	
	this.url = "";
     
    if(this.formId == "listForm"){
        $("#listForm")[0].reset();
    }
     
    this.setUrl = function setUrl(url){
        this.url = url;
    };
     
    this.addParam = function addParam(key, value){
        $("#"+this.formId).append($("<input type='hidden' name='"+key+"' id='"+key+"' value='"+value+"' >"));
    };
     
    this.submit = function submit(){
        var frm = $("#"+this.formId)[0];
        frm.action = this.url;
        frm.method = "post";
        frm.submit();  
    };
}

function fileDownload(file_path, file_name) {
	var comSubmit = new ComSubmit();
	comSubmit.setUrl(rootPath + '/allLogInq/fileDownload.html');
	comSubmit.addParam("file_path", file_path);
	comSubmit.addParam("file_name", file_name);
	comSubmit.submit();
}

function setProcTime(type) {
	var start = Number($("#start_h").val());
	var end = Number($("#end_h").val());
	
	if (start >= end) {
		if (type == 'start') {
			$("#end_h").val(start + 1);
		} else {
			$("#start_h").val(end - 1);
		}
	}
}

function showResultType(log_seq, proc_date, result_owner, system_seq, index) {
	var disp = $("#subResultType_" + index).css("display");
	if(disp == "none") {
		$.ajax({
			type: 'POST',
			url: rootPath + '/allLogInq/getResultType.html',
			data: { 
				"log_seq" : log_seq,
				"proc_date" : proc_date,
				"system_seq": system_seq,
				"result_owner": result_owner
			},
			success: function(data) {
				drawSubResultType(data.list, index);
			}
		});
	} else {
		$("#subResultType_" + index).hide();
	}
}

function drawSubResultType(data, index) {
	
	$("#subResultTable_" + index).find('tbody').empty();
	if(data != null) {
		$.each(data, function( i, item ) {
			
			$("<tr>").appendTo($("#subResultTable_" + index).find('tbody'))
			.append($("<td width='120px' style='text-align: center;'>")
					.text(i + 1)
			)
			.append($("<td style='text-align: center;'>")
					.text(item.result_type)
			)
			.append($("<td style='text-align: center;'>")
					.text(item.result_content)
			)
		});
	}
	
	$("#subResultType_" + index).show();
}

// 상세화면 페이지 이동( sql 이동 )
function goPageallLogInqDetail_sql(num){
	$("#listForm").attr("action",allLogInqConfig["detailUrl"]);
	$("#listForm input[name=logSqlPageNum]").val(num);
	$("#listForm input[name=tab_flag]").val("2");
	$("#listForm").submit();
}
//상세화면 페이지 이동( sql result 이동 )
function goPageallLogInqDetailResult(num){
	$("#listForm").attr("action",allLogInqConfig["detailUrl"]);
	$("#listForm input[name=logSqlResultPageNum]").val(num);
	$("#listForm input[name=tab_flag]").val("2");
	$("#listForm").submit();
}

function sqlCollectYn(sql_seq, collect_yn) {
	$.ajax({
		type: 'POST',
		url: rootPath + '/allLogInq/updateSqlCollectYn.html',
		data: { 
			"sql_seq" : sql_seq,
			"collect_yn" : collect_yn
		},
		success: function(data) {
			var result = JSON.parse(data);
			if(result=='success'){
				alert("적용하였습니다.");
				$("#listForm").attr("action",allLogInqConfig["detailUrl"]);
				$("#listForm input[name=tab_flag]").val("2");
				$("#listForm").submit();
			} else {
				alert("적용에 실패하였습니다.");
			}
		}
	});
}

// sql excel export
function logSqlExcel() {
	$("#logSqlForm").attr("action",allLogInqConfig["logSqlExcel"]);
	$("#logSqlForm").submit();
}
// sql css export
function logSqlCss() {
	$("#logSqlForm").attr("action",allLogInqConfig["logSqlCss"]);
	$("#logSqlForm").submit();
}
