/**
 * EmpUserMngtList Javascript
 * @author yjyoo
 * @since 2015. 04. 21
 */




// 페이지 이동
function goPage(num){
	$clone.attr("action", empUserListConfig["listUrl"]);
	$clone.find("input[name=page_num]").val(num);
	$clone.attr("style", "display: none;");
	$('body').append($clone);
	$clone.submit();
}

// 검색
function moveEmpUserList() {
	$("#empUserListForm").attr("action", empUserListConfig["listUrl"]);
	//$("#empUserListForm input[name=page_num]").val('1');
	$("#empUserListForm").submit();
}



// ==================
function searchEmpUserList() {
	$("#empUserListForm").attr("action", empUserListConfig["listUrl"]);
	$("#empUserListForm input[name=page_num]").val('1');
	$("#empUserListForm").submit();
}

function moveList() {
	$("#detailForm").attr("action", selectConfig["listUrl"]);
	$("#detailForm select[name=system_seq]").val('');
	$("#detailForm input[name=system_seq]").val('');
	$("#detailForm").submit();
}


function moveDetail(rule_seq) {
	$("#moveForm input[name=rule_seq]").val(rule_seq);
	$("#moveForm").submit();
}

function addThresholdDept(type) {
	var rule_seq = 0;
	if(type=='add') {
		rule_seq = $("select[name=rule_seq]").val();
	} else {
		rule_seq = $("input[name=rule_seq]").val();
	}
	var sizeVal = $("input[name=threshold]").length;
	var arr = [];
	for(i=0; i<sizeVal;i++){
		var dept_id = $("#dept_id"+i).val();
		var threshold = $("#threshold"+i).val();
		if(threshold == null || threshold ==''){
			continue;
		}
		var temp = {
			dept_id : dept_id,
			rule_seq : rule_seq,
			threshold : threshold
		}
		arr.push(temp);
	}
	var jsonData = JSON.stringify(arr);
	$.ajax({
		type : 'POST',
		url : rootPath + '/extrtBaseSetup/saveThreshold_dept.html',
		data : {jsonData:jsonData},
		dataType : 'json',
		success : function(data) {
			if(data=='success'){
				alert("저장하였습니다.");
				moveList();
			} else {
				alert("저장에 실패하였습니다.");
			}
		}
	});
}

$(function(){
	$('input[type=text]').attr("onkeydown","javascript:if(event.keyCode==13){searchEmpUserList();}");
});

function deleteThresholdDept(dept_id) {
	var rule_seq = $("input[name=rule_seq]").val();
	
	$.ajax({
		type : 'POST',
		url : rootPath + '/extrtBaseSetup/deleteThreshold_dept.html',
		data : {
			dept_id : dept_id,
			rule_seq : rule_seq
		},
		dataType : 'json',
		success : function(data) {
			if(data=='success'){
				alert("삭제하였습니다.");
				window.location.reload();
			} else {
				alert("삭제에 실패하였습니다.");
			}
		}
	});
}

function thresholdAll() {
	var noVal=$("input[name=thresholdAll]").val();
	var sizeVal = $("input[name=threshold]").length;
	for(i=0; i<sizeVal;i++){
		$("#threshold"+i).val(noVal);
	}
}

