/**
 * EmpUserMngtList Javascript
 * @author yjyoo
 * @since 2015. 04. 21
 */



var $clone;

$(document).ready(function() {
	$clone = $("#empUserListForm").clone();
});

// 페이지 이동
function goPage(num){
	$clone.attr("action", empUserListConfig["listUrl"]);
	$clone.find("input[name=page_num]").val(num);
	$clone.attr("style", "display: none;");
	$('body').append($clone);
	$clone.submit();
}

// 검색
function moveEmpUserList() {
	$("#empUserListForm").attr("action", empUserListConfig["listUrl"]);
	//$("#empUserListForm input[name=page_num]").val('1');
	$("#empUserListForm").submit();
}

function searchEmpUserList() {
	$("#empUserListForm").attr("action", empUserListConfig["listUrl"]);
	$("#empUserListForm input[name=page_num]").val('1');
	$("#empUserListForm").submit();
}


function ajaxDefaultSucCallback(data, dataType, actionType){
	
}

function ajaxDefaultErrCallback(XMLHttpRequest, textStatus, errorThrown, actionType){
	alert("ajax request Error : " + textStatus);
}


// 사원 ajax call - 성공
function ajaxEmpUser_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		alert("성공적으로 등록되었습니다.");
		searchEmpUserList();
	}
}

// 사원 ajax call - 실패
function ajaxEmpUser_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){	
	alert("엑셀 업로드에 실패하였습니다. 셀값에 빈공간이 있거나 값이 제대로 들어가있는지 확인해 주십시오." + textStatus);
}

function exDown(){
	$("#exdown").submit();
}



//$(function(){
//	$('input[type=text]').attr("onkeydown","javascript:if(event.keyCode==13){moveEmpUserList();}");
//});

/*function setThreshold(obj, emp_user_id, system_seq) {
	if(!isNumber(obj.value))
		alert("숫자만 입력이 가능합니다.");
	else {
		$.ajax({
			type : 'POST',
			url : rootPath + '/extrtBaseSetup/saveThreshold_indv.html',
			data : {
				"emp_user_id" : emp_user_id,
				"system_seq" : system_seq,
				"threshold" : obj.value.trim()
			},
			success : function(data) {
				alert("적용 되었습니다.");
				moveEmpUserList();
			}
		});
	}
}*/

function setThreshold(index, emp_user_id, system_seq) {
	var obj = document.getElementById("row_"+index);
	if(!isNumber(obj.value))
		alert("숫자만 입력이 가능합니다.");
	else {
		$.ajax({
			type : 'POST',
			url : rootPath + '/extrtBaseSetup/saveThreshold_indv.html',
			data : {
				"emp_user_id" : emp_user_id,
				"system_seq" : system_seq,
				"threshold" : obj.value.trim()
			},
			success : function(data) {
				alert("적용 되었습니다.");
				moveEmpUserList();
			}
		});
	}
}


function isNumber(s) {
	s += ''; // 문자열로 변환
	s = s.replace(/^\s*|\s*$/g, ''); // 좌우 공백 제거
	if (s == '-')
		return true;
	
	if (s == '' || isNaN(s))
		return false;
	return true;
}

// =======================

function moveList() {
	$("#detailForm").attr("action", selectConfig["listUrl"]);
	$("#detailForm input[name=emp_user_id]").val('');
	$("#detailForm select[name=emp_user_id]").val('');
	$("#detailForm select[name=system_seq]").val('');
	$("#detailForm input[name=system_seq]").val('');
	$("#detailForm").submit();
}

function moveDetail(rule_seq) {
	$("#moveForm input[name=rule_seq]").val(rule_seq);
	$("#moveForm").submit();
}
function addThresholdUser(type) {
	if(type=='add') {
		var rule_seq = $("select[name=rule_seq]").val();
	} else {
		var rule_seq = $("input[name=rule_seq]").val();
	}
	var sizeVal = $("input[name=threshold]").length;
	var arr = [];
	for(i=0; i<sizeVal;i++){
		var emp_user_id = $("#emp_user_id"+i).val();
		var system_seq = $("#system_seq"+i).val();
		var threshold = $("#threshold"+i).val();
		if(threshold == null || threshold ==''){
			continue;
		}
		var temp = {
			emp_user_id : emp_user_id,
			system_seq : system_seq,
			rule_seq : rule_seq,
			threshold : threshold
		}
		arr.push(temp);
	}
	var jsonData = JSON.stringify(arr);
	$.ajax({
		type : 'POST',
		url : rootPath + '/extrtBaseSetup/saveThreshold_indv.html',
		data : {jsonData:jsonData},
		dataType : 'json',
		success : function(data) {
			if(data=='success'){
				alert("저장하였습니다.");
				moveList();
			} else {
				alert("저장에 실패하였습니다.");
			}
		}
	});
}

$(function(){
	$('input[type=text]').attr("onkeydown","javascript:if(event.keyCode==13){searchEmpUserList();}");
});

function deleteThresholdUser(emp_user_id, system_seq) {
	var rule_seq = $("input[name=rule_seq]").val();
	
	$.ajax({
		type : 'POST',
		url : rootPath + '/extrtBaseSetup/deleteThreshold_indv.html',
		data : {
			emp_user_id : emp_user_id,
			system_seq : system_seq,
			rule_seq : rule_seq
		},
		dataType : 'json',
		success : function(data) {
			if(data=='success'){
				alert("삭제하였습니다.");
				window.location.reload();
			} else {
				alert("삭제에 실패하였습니다.");
			}
		}
	});
}

function thresholdAll() {
	var noVal=$("input[name=thresholdAll]").val();
	var sizeVal = $("input[name=threshold]").length;
	for(i=0; i<sizeVal;i++){
		$("#threshold"+i).val(noVal);
	}
}