// 
var start_idx=0;
var timeline_sc;
var timeline_size;
var timeline_frame;//가장 바깥 틀
var timeline_hub;//타임라인 중심축
var timeline_ul;//타임라인
var timeline_data;//불러온 타임라인 데이터
var hub_height=0;
var sheet;
var Dsearch_from;
var Dsearch_to;
var Dsystem_seq;
var Drule_cd;
var Duser_id;
//var flag=false;//scroll로 다시 그려 주는 경우는 sheet 없애주기(Y)/ 처음 생성되거나 검색으로 초기화될 경우는 remove 시키면 에러남(N)
var reachEndFlag=false;//데이터 전부 그려졌을 때 중복 그려주지 않도록

/*function resetTimeline(){
	$('#loadingT').show();
	$("#timeline_frame").empty();
	 $('html, div').animate({
         scrollTop : 0
     }, 1);
	setTimeline();//타임라인 초기화
	doAjax(Dsearch_from,Dsearch_to,Dsystem_seq,Drule_cd,Duser_id);//
}
*/
/*
$(document).ready(function() {
	Dsearch_from=$("#search_fr").val();
	Dsearch_to=$("#search_to").val();
	Dsystem_seq=$("#system_seq").val();
	Drule_cd=$("#rule_cd").val();
	Duser_id=$("#user_id").val();
	setTimeline();//타임라인 초기화
	getTimelineData();//데이터 불러오기 및 초기 8개 그려주기
	$("#timeline_frame").scroll(function(){
		
		var elem = $("#timeline_frame");	
	    if(elem[0].scrollHeight - elem.scrollTop() == elem.outerHeight()) {
	    	//8게 단위로 끝에 있는 데이타 도달하기전까지 작동
	    	if(!reachEndFlag && start_idx!=0){
	    		if(timeline_data.length>8){
	    			document.body.removeChild(sheet);  
		    		drawTimeline2();// 추가로 보여주기
		    		start_idx=start_idx+8;
	    		}  		
	    	}
	    	//끝에 도달했을 때
	    	if(reachEndFlag){
	    		alert("더이상 데이터가 없습니다");
	    	}
	    	if((timeline_data.length-1)<=start_idx){
	    		start_idx=0;
	    		reachEndFlag=true;
	    	}
	    	
	    }
	});
});
*/

/*function initTimeline() {
	Dsearch_from=$("#search_fr").val();
	Dsearch_to=$("#search_to").val();
	Dsystem_seq=$("#system_seq").val();
	Drule_cd=$("#rule_cd").val();
	Duser_id=$("#user_id").val();
	setTimeline();//타임라인 초기화
	getTimelineData();//데이터 불러오기 및 초기 8개 그려주기
	$("#timeline_frame").scroll(function(){
		
		var elem = $("#timeline_frame");	
	    if(elem[0].scrollHeight - elem.scrollTop() == elem.outerHeight()) {
	    	//8게 단위로 끝에 있는 데이타 도달하기전까지 작동
	    	if(!reachEndFlag && start_idx!=0){
	    		if(timeline_data.length>8){
	    			document.body.removeChild(sheet);  
		    		drawTimeline2();// 추가로 보여주기
		    		start_idx=start_idx+8;
	    		}  		
	    	}
	    	//끝에 도달했을 때
	    	if(reachEndFlag){
	    		alert("더이상 데이터가 없습니다");
	    	}
	    	if((timeline_data.length-1)<=start_idx){
	    		start_idx=0;
	    		reachEndFlag=true;
	    	}
	    	
	    }
	});
}

//새로 타임라인 불러올 때 초기화 설정
function setTimeline(){
	reachEndFlag=false;
	start_idx=0;
	hub_height=0;
	sheet = document.createElement('style');//축의 높이를 설정해 주기 위해 생성해주는 스타일 요소
	timeline_frame=document.getElementById("timeline_frame");//가장 바깥 틀
	timeline_frame.setAttribute("class","mt-timeline-2");
	timeline_hub=document.createElement("div");
	timeline_hub.setAttribute("class","mt-timeline-line border-grey-steel");	
	timeline_ul=document.createElement("ul");
	timeline_ul.setAttribute("class","mt-container");
	timeline_ul.style.padding="0px 20px 0px 0px";
//	timeline_ul.style.padding_right="20px";
//	$(timeline_ul).css("padding-right","20px");
	timeline_frame.appendChild(timeline_ul);
	document.body.appendChild(sheet);
}


//처음 상세 페이지 들어올때 혹인 검색할 때 데이터 불러오기 역할
function doAjax(search_from,search_to,system_seq,rule_cd,user_id,user_name,user_ip,dept_name){
	$.ajax({
		type: 'POST',
		url: rootPath + '/extrtCondbyInq/timeline.html',
		data: { 
			search_from : search_from,
			search_to : search_to,
			system_seq : system_seq,
			rule_cd : rule_cd,
			user_id: user_id,
			user_name: user_name,
			user_ip: user_ip,
			dept_name: dept_name
		},
		success: function(data) {
			timeline_data=eval(data);			
			
		},
		complete: function(){
			$('#loadingT').hide();	
			if(start_idx==0){
				drawTimeline2();//불러온 데이터로 그려주기(처음 8개만)
				start_idx=start_idx+8;
			}
		},
		error:function(e){
			alert("Error:"+e);
		}
	});
}

function getTimelineData() {
	var search_from = $("#search_fr").val();
	var search_to = $("#search_to").val();
	var system_seq=$("#system_seq").val();
	var rule_cd=$("#rule_cd").val();
	var user_id=$("#user_id").val();
	var user_name=$("#user_name").val();
	var user_ip=$("#user_ip").val();
	var dept_name=$("#dept_name").val();
	doAjax(search_from,search_to,system_seq,rule_cd,user_id,user_name,user_ip,dept_name);
}

//타임라인 유형 2: 불러온 데이터로 실제 그려주기(맨 처음 8개만)
function drawTimeline2() {
	var dataList=timeline_data;
	if(dataList!=null && dataList!=""){
		var cnt=1;
		$(dataList).each(function(idx,item){
			if(idx >= start_idx && idx < (start_idx+8)){
				var timeline_item_li=document.createElement("li");//<li>
				timeline_item_li.setAttribute("class","mt-item");
				var timeline_icon=document.createElement("div");
				$(timeline_icon).addClass("mt-timeline-icon");
				timeline_icon.style.width="50px";
				timeline_icon.style.height="50px";
				timeline_icon.style.textAlign="center";
				var timeline_content=$("<div></div>").addClass("mt-timeline-content");
				var timeline_container=document.createElement("div");
				timeline_container.setAttribute("onclick", "goDetail(" + item.log_seq + ", '" + item.proc_date + "')");
				timeline_container.style.cursor = "pointer";
				$(timeline_container).addClass("mt-content-container");
				timeline_container.style.padding="15px 30px 5px 30px";
				var timeline_title=document.createElement("div");
				$(timeline_title).addClass("mt-title");
				var timeline_subContent=document.createElement("div");
				$(timeline_subContent).addClass("mt-content");
				timeline_subContent.style.padding="10px 0 0 0";
				var timeline_mt_author=document.createElement("div");
				$(timeline_mt_author).addClass("mt-author");
				timeline_mt_author.style.margin="10px 0 0 0";
				var timeline_mt_avatar=$("<div></div>").addClass("mt-avatar");
				var user_name=$("<div></div>").addClass("mt-author-name");
				if(item.rule_cd!=0){
					timeline_icon.style.background="#d91e18";
					hub_height=hub_height+227;
//					$(timeline_icon).addClass("bg-red bg-font-red border-grey-steel");
					$(timeline_title).append($('<h3/>',{
						class: "mt-content-title",
						text:item.scen_name
					}));
					$(timeline_title).append($('<h6/>',{
						text:item.rule_nm
					}));
					$(timeline_subContent).append('<p><b>접속한 시스템명</b> : '+item.system_name+'<br/>접속 ID   :   '+item.user_id+'<br/>접속 IP   :   '+item.user_ip+'</p>');
					
					$(timeline_subContent).addClass("border-red");
					if(cnt%2==0){
						$(timeline_container).addClass("border-right-before-red border-red");
					}else{
						$(timeline_container).addClass("border-left-before-red border-red");
					}
					cnt++;
					var img=document.createElement("img");
					$(img).attr("src", rootPath + "/resources/image/psm/search/abnormal.png");
					img.style.width="26px";
					img.style.height="35px";
					img.style.margin="7.5px";
					$(timeline_icon).append(img);
					timeline_title.style.margin="0";
				}else{
					timeline_icon.style.background="#26c281";
					hub_height=hub_height+219;
//					$(timeline_icon).addClass("bg-green-turquoise bg-font-green-turquoise border-grey-steel");				
					$(timeline_title).append($('<h3/>',{
						class: "mt-content-title",
						text:item.scen_name
					})).appendTo(timeline_container);
					$(timeline_subContent).append('<p><b> 접속한 시스템명 : </b>'+item.system_name+'<br/>접속 ID   :   '+item.user_id+'<br/>접속 IP   :   '+item.user_ip+'</p>');				
					$(timeline_subContent).addClass("border-green-turquoise");
					if(cnt%2==0){
						$(timeline_container).addClass("border-green-turquoise border-right-before-green-turquoise");
					}else{
						$(timeline_container).addClass("border-green-turquoise border-left-before-green-turquoise");
					}
					cnt++;
					var img=document.createElement("img");
					$(img).attr("src", rootPath + "/resources/image/psm/search/normal.png");
					img.style.width="26px";
					img.style.height="35px";
					img.style.margin="7.5px";
					$(timeline_icon).append(img);
				};			
				$(timeline_mt_avatar).append($('<img>',{
					class: "img-circle",
					src: rootPath + "/resources/assets/layouts/layout/img/profile_user.png"
				})).appendTo(timeline_mt_author);
				$(user_name).append($('<a/>',{
					class: "font-blue-madison",
					text: item.user_name
				})).appendTo(timeline_mt_author);
				$(timeline_mt_author).append($('<div/>',{
					class: "mt-author-notes font-grey-mint",
					text: item.proc_date+"      "+item.proc_time
				}));
				var proc_date=item.proc_date;
				var upProc_date=proc_date.substring(0,4)+proc_date.substring(5,7)+proc_date.substring(8,9);
//				$(timeline_subContent).append($('<a/>',{
//					class: "btn blue btn-outline",
//					text: "상세보기",
//					href: "javascript:fnAllLogInqDetail(\'"+item.log_seq+"\',\'"+upProc_date+"\',\'"+item.user_id+"\');"
//				}));
				$(timeline_container).append(timeline_title,timeline_mt_author,timeline_subContent);
				$(timeline_content).append(timeline_container);
				$(timeline_item_li).append(timeline_icon,timeline_content);
				timeline_ul.appendChild(timeline_item_li);
				
				
				//축 그려주기(//전체 데이터 끝이거나, 불러오기 데이터 끝일 때)
				if(idx == (dataList.length-1) || idx == (start_idx+7)){
					sheet.innerHTML = ".mt-timeline-line.border-grey-steel{ height: "+eval(hub_height)+"px;}";
					document.body.appendChild(sheet);
					$(timeline_frame).append(timeline_hub);
				}
			}
		});	
	}else{
		var timeline_item_li=document.createElement("li");//<li>
		timeline_item_li.setAttribute("class","mt-item");
		var timeline_icon=document.createElement("div");
		$(timeline_icon).addClass("mt-timeline-icon bg-purple-medium bg-font-purple-medium border-grey-steel");
		timeline_icon.style.width="50px";
		timeline_icon.style.height="50px";
		var timeline_content=$("<div></div>").addClass("mt-timeline-content");		
		var timeline_container=$("<div></div>").addClass("mt-content-container");	
		var timeline_title=$("<div></div>").addClass("mt-title");
		var timeline_subContent=$("<div></div>").addClass("mt-content border-grey-salt");	
		
		$(timeline_title).append($('<h3/>',{
			class: "mt-content-title",
			text:"데이터 없음"
		})).appendTo(timeline_container);		
//		$(timeline_container).append(timeline_title,timeline_subContent);
		$(timeline_content).append(timeline_container);
		$(timeline_item_li).append(timeline_icon,timeline_content);
		timeline_ul.appendChild(timeline_item_li);
		sheet.innerHTML = ".mt-timeline-line.border-grey-steel{ height: 640px;}";
		document.body.appendChild(sheet);
		$(timeline_frame).append(timeline_hub);
		timeline_frame.appendChild(timeline_ul);
	}	
};

//검색
function findTimeline() {
	var user_id=$("#user_id").val();
	if(user_id == '') {
		alert("사용자ID를 입력하여 주세요");
		$("#user_id").focus();
		return false;
	}
	$('#loadingT').show();
	$("#timeline_frame").empty();
	 $('html, div').animate({
         scrollTop : 0
     }, 1);
	setTimeline();//타임라인 초기화
	getTimelineData();//데이터 불러오기 및 초기 8개 그래주기
}

function fnAllLogInqDetail(log_seq,proc_date,emp_user_id){
	$("input[name=detailLogSeq]").attr("value",log_seq);
	$("input[name=detailProcDate]").attr("value",proc_date);
	$("input[name=user_id]").attr("value",emp_user_id);
	
	$("#timeDetailForm").attr("action",allLogInqConfig["detailUrl"]);
	$("#timeDetailForm").submit();
}

var allLogInqConfig = {
		"detailUrl" : "${rootPath}/allLogInq/detail.html"
	};
//타임라인 유형 1
function drawTimeline1(data) {
	
	var dataList =eval("("+data+")");	
	var timeline_frame=document.getElementById("timeline_frame");
	timeline_frame.setAttribute("class","timeline");
	var timelineTable=document.createElement("table");//가장 바깥 틀
	timelineTable.style.width="90%";	
	timeline_frame.appendChild(timelineTable);
	
	if(dataList.length==0){
		var timeline_itemTR=document.createElement("tr");;//<tr>
		var timeline_itemTD=document.createElement("td");//<td>
		timeline_itemTD.setAttribute("class","timeline-item");
		var timeline_badge=$("<div></div>").addClass("timeline-badge");
		var timeline_body=$("<div></div>").addClass("timeline-body");		
		var timeline_body_head=$("<div></div>").addClass("timeline-body-head");
		var timeline_body_head_caption=$("<div></div>").addClass("timeline-body-head-caption");
		timeline_itemTR.appendChild(timeline_itemTD);
		timelineTable.appendChild(timeline_itemTR);
		
		$(timeline_body_head_caption).append($('<span/>',{
			class: "timeline-body-title font-blue-madison",
			text:"데이터 없음"
		})).appendTo(timeline_body_head);
		$(timeline_badge).append($('<img>',{
			class: "timeline-badge-userpic",
			src: rootPath + "/resources/image/psm/setup/icon_statgreen.png"
		}));
		$(timeline_body).append(timeline_body_head);
		$(timeline_itemTD).append(timeline_badge,timeline_body);
	}
	
	$(dataList).each(function(idx,item){
		if(idx >= start_idx && idx < (start_idx+8)){
			var timeline_itemTR=document.createElement("tr");;//<tr>
			var timeline_itemTD=document.createElement("td");//<td>
			timeline_itemTD.setAttribute("class","timeline-item");
			var timeline_badge=$("<div></div>").addClass("timeline-badge");
			var timeline_body=$("<div></div>").addClass("timeline-body");
			var timeline_body_arrow=$("<div></div>").addClass("timeline-body-arrow");
			var timeline_body_head=$("<div></div>").addClass("timeline-body-head");
			var timeline_body_head_caption=$("<div></div>").addClass("timeline-body-head-caption");
			var timeline_body_content=$("<div></div>").addClass("timeline-body-content");
			timeline_itemTR.appendChild(timeline_itemTD);
						
			if(item.rule_cd!=0){
				$(timeline_body_head_caption).append($('<span/>',{
					class: "timeline-body-title font-blue-madison",
					text:item.scen_name
				}));
				$(timeline_badge).append($('<img>',{
					class: "timeline-badge-userpic",
					src: rootPath + "/resources/image/psm/setup/icon_statred.png"
				}));
				$(timeline_body_content).append('<div class=\'font-grey-cascade\'>'+item.rule_nm+'<br/><b>접속한 시스템명 : </b>'+item.system_name+'<br/>접속 ID   :   '+item.user_id+'<br/>접속 IP   :   '+item.user_ip+'</div>');
			}else{
				$(timeline_body_head_caption).append($('<span/>',{
					class: "timeline-body-title font-blue-madison",
					text:item.scen_name
				}));
				$(timeline_badge).append($('<img>',{
					class: "timeline-badge-userpic",
					src: rootPath + "/resources/image/psm/setup/icon_statblue.png"
				}));
				$(timeline_body_content).append('<div class=\'font-grey-cascade\'>'+'<br/><b>접속한 시스템명 : </b>'+item.system_name+'<br/>접속 ID   :   '+item.user_id+'<br/>접속 IP   :   '+item.user_ip+'</div>');
			};		
			$(timeline_body_head_caption).append($('<span/>',{
				class: "timeline-body-time font-grey-cascade",
				text: "접속시간  :  "+item.proc_date+"      "+item.proc_time
			})).appendTo(timeline_body_head);		
			$(timeline_body).append(timeline_body_arrow,timeline_body_head,timeline_body_content);
			$(timeline_itemTD).append(timeline_badge,timeline_body);
			timelineTable.appendChild(timeline_itemTR);
		}	
		
	});
	
};*/

/*function goDetail(log_seq, proc_date) {
	url = rootPath + '/allLogInq/detail.html';
	proc_date = proc_date.replace(/\./gi, "");
	
	$('#allLogInqForm input[name=detailLogSeq]').attr("value",log_seq);
	$('#allLogInqForm input[name=detailProcDate]').attr("value",proc_date);
	
	$("#allLogInqForm").attr("action", url);
	$("#allLogInqForm").submit();
}*/


function findTimeline1() {
	$("#timeline_frame").empty();
	$.ajax({ 
		type: 'POST',
		url: rootPath + '/extrtCondbyInq/timeline.html',
		data: $("#timeListForm").serialize(),
		success: function(data) {
			$('#timeline_frame').append(data);
			
		}
	});
}
