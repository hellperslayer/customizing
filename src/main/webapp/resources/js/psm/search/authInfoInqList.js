function fnAuthInfoInqDetail(log_seq, proc_date){
	
	$("input[name=detailLogSeq]").attr("value",log_seq);
	$("input[name=detailProcDate]").attr("value",proc_date);
	$('#search_fr').attr('disabled', false);
	$('#search_to').attr('disabled', false);
	$("#listForm").attr("action",authInfoInqConfig["detailUrl"]);
	$("#listForm").submit();
}

// 페이지 이동
function goPage(num){
	$('#search_fr').attr('disabled', false);
	$('#search_to').attr('disabled', false);
	$("#listForm").attr("action",authInfoInqConfig["listUrl"]);
	$("#listForm input[name=page_num]").val(num);
	$("#listForm").submit();
}
//상세화면 페이지 이동
function goPageauthInfoInqDetail(num){
	$('#search_fr').attr('disabled', false);
	$('#search_to').attr('disabled', false);
	$("#listForm").attr("action",authInfoInqConfig["detailUrl"]);
	$("#listForm input[name=authInfoInqDetail_page_num]").val(num);
	$("#listForm").submit();
}
// 검색
function moveauthInfoInqList() {
	$('#search_fr').attr('disabled', false);
	$('#search_to').attr('disabled', false);
	var listForm = $("#listForm");
	var tDate = listForm.find("input[name=search_to]").val();
	var fDate = listForm.find("input[name=search_from]").val();
	if(tDate<fDate){
		alert("검색 기간을 확인하세요");
		return;
	}
	$('input[name=page_num]').attr("value","1");
	$('input[name=isSearch]').attr("value","Y");
	$("#listForm").attr("action",authInfoInqConfig["listUrl"]);
		$("#listForm").submit();

		
}

// 기간선택 검색조건
function initDaySelect(){
	if($("#daySelect").val() == "Day"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
		$('#search_fr').attr('disabled', true);
		$('#search_to').attr('disabled', true);
	}else if($("#daySelect").val() == "WeekDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-7d");
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
		$('#search_fr').attr('disabled', true);
		$('#search_to').attr('disabled', true);
	}else if($("#daySelect").val() == "MonthDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-1m");
		var setDateStart = $("#search_fr").val();
		setDateStart = setDateStart.substr(0,8);
		setDateStart = setDateStart + "01";
		$("#search_fr").val(setDateStart);
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "YearDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
		var setDateStart = $("#search_fr").val();
		setDateStart = setDateStart.substr(0,5);
		setDateStart = setDateStart + "01-01";
		$("#search_fr").val(setDateStart);
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "TotalMonthDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-1m");
		var setDateStart = $("#search_fr").val();
		setDateStart = setDateStart.substr(0,8);
		setDateStart = setDateStart + "01";
		$("#search_fr").val(setDateStart);
		var setDateend =  $("#search_to").val();
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","0");
		setDateend = setDateend.substr(8,10);
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-"+setDateend+"d");
	} else if($("#daySelect").val() == "") {	
		// 직접입력
		$('#search_fr').attr('disabled', false);
		$('#search_to').attr('disabled', false);
	} else{
		dateDisabled(false);
	}
}

function dateDisabled(bool){
	$('.search_fr').attr('disabled', bool);
	$('.search_to').attr('disabled', bool);
	$('.ui-datepicker-trigger').css('visibility', bool == true ? 'hidden' : 'visible');
	$('.ui-datepicker-trigger').css('visibility', bool == true ? 'hidden' : 'visible');
}

//검색
function goList() {
	$("#listForm").attr("action",authInfoInqConfig["listUrl"]);
	$("#listForm").submit();
}

// 추출조건 상세화면으로 이동
function moveExtrtCondbyInqDetail(){
	$("#listForm").attr("action",extrtCondbyInqConfig["detailUrl"]);
	$("#listForm").submit();
}
/**
 * 엑셀 다운로드 추가 2015/6/4
 */
function excelauthInfoInqList(){
//	if(arguments[0] >= 65535){
//		alert("엑셀다운로드는 65535건까지 가능합니다.");
//	}else{
		
		var $form = $("#listForm");
		var log_message_params = '';
		var menu_id = $('input[name=current_menu_id]').val();
		var log_message_title = '관리자권한이력조회 엑셀 다운로드';
		var log_action = 'EXCEL DOWNLOAD';
		
		$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
		
		$("#listForm").attr("action",authInfoInqConfig["downloadUrl"]);
		$("#listForm").submit();
//	}
}

function csvauthInfoInqList(){
	var $form = $("#listForm");
	var log_message_params = '';
	var menu_id = $('input[name=current_menu_id]').val();
	var log_message_title = '관리자권한이력조회 CSV 다운로드';
	var log_action = 'CSV DOWNLOAD';
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	$("#listForm").attr("action",authInfoInqConfig["downloadCSVUrl"]);
	$("#listForm").submit();
}

$(function(){
	var daySel = $('#daySelect').val();
	if (daySel == "Day" || daySel == "WeekDay") {
		$('#search_fr').attr('disabled', true);
		$('#search_to').attr('disabled', true);
	}
	$('input[type=text]').attr("onkeydown","javascript:if(event.keyCode==13){moveauthInfoInqList();}")
	$('#search_fr').attr("onclick", "resetPeriod()");
	$('#search_to').attr("onclick", "resetPeriod()");
})

function fnExtrtEmpDetailInfo(emp_user_name,emp_user_id, user_ip) {
	$("input[name=emp_user_name]").attr("value",emp_user_name);
	$("input[name=emp_user_id]").attr("value",emp_user_id);
	$("input[name=user_ip]").attr("value",user_ip);
	
	$("#listForm").attr("action",extrtCondbyInqConfig["detailChartUrl"]);
	$("#listForm").submit();
}

function searhTime(obj) {
	var check = obj.checked;
	if(check == true) {
		$("select[id=start_h]").attr("disabled", false);
		$("select[id=end_h]").attr("disabled", false);
	} else {
		$("select[id=start_h]").attr("disabled", true);
		$("select[id=end_h]").attr("disabled", true);
	}
}

function setProcTime(type) {
	var start = Number($("#start_h").val());
	var end = Number($("#end_h").val());
	
	if (start >= end) {
		if (type == 'start') {
			$("#end_h").val(start + 1);
		} else {
			$("#start_h").val(end - 1);
		}
	}
}
