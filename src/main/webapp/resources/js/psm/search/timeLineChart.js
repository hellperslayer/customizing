// 
var start_idx=0;
var timeline_sc;
var timeline_size;
var timeline_frame;//가장 바깥 틀
var timeline_hub;//타임라인 중심축
var timeline_ul;//타임라인
var timeline_data;//불러온 타임라인 데이터
var hub_height=0;
var sheet;
var Dsearch_from;
var Dsearch_to;
var Dsystem_seq;
var Drule_cd;
var Duser_id;
//var flag=false;//scroll로 다시 그려 주는 경우는 sheet 없애주기(Y)/ 처음 생성되거나 검색으로 초기화될 경우는 remove 시키면 에러남(N)
var reachEndFlag=false;//데이터 전부 그려졌을 때 중복 그려주지 않도록

function selScenario() {
	var scen_seq = $("#scen_seq").val();
	
	$.ajax({
		type:'POST',
		url : rootPath + '/extrtCondbyInq/findScenarioByScenSeq.html',
		data: {
			scen_seq : scen_seq
		},
		success : function(data){
			setRuleTblList(data);
		}
	});
}
function setRuleTblList(data) {
	var select = document.getElementById("rule_cd");
	while (select.options.length) {
		select.remove(0);
    }
	var none = document.createElement("option");
	none.text = "----- 전 체 -----";
	none.value = "";
	select.add(none);
	
	var jsonData = JSON.parse(data);
	for(var i=0; i<jsonData.length; i++) {
		var option = document.createElement("option");
		option.text = jsonData[i].rule_nm;
		option.value = jsonData[i].rule_cd;
		select.add(option);
	}
}
function dataChange() {
	var shType = $("#data1").val();
	if(shType=='senario'){
		document.getElementById("senario1").style.display = "";
		document.getElementById("senario2").style.display = "";
	} else {
		document.getElementById("senario1").style.display = "none";
		document.getElementById("senario2").style.display = "none";
	}
}
function timeLineSearch() {
	var idVal = $("#listForm input[name=emp_user_id]").val();
	var nameVal = $("#listForm input[name=emp_user_name]").val();
	if(idVal == '' && nameVal =='') {
		alert("사용자ID 또는 사용자명을 입력해주세요.");
		return;
	}
	var listForm = $("#listForm");
	$("#listForm").attr("action",selectConfig["listUrl"]);
	$("#listForm").submit();
}
function timeLineReset() {
	var today = new Date();
	var year = today.getFullYear();
	var month = today.getMonth() +1+'';
	if(month.length==1){
		month = '0'+month;
	}
	var day = today.getDate()+'';
	if(day.length==1){
		day='0'+day;
	}
	var dateVal = year+"-"+month+"-"+day;
	$("#listForm input[name=search_from]").val(dateVal);
	$("#listForm input[name=search_to]").val(dateVal);
	$("#listForm input[name=emp_user_id]").val('');
	$("#listForm input[name=emp_user_name]").val('');
	$("#listForm select[name=data1]").val('');
	$("#listForm select[name=system_seq]").val('');
	$("#listForm select[name=scen_seq]").val('');
	selScenario();
	$("#timeline_frame").empty();
}

var pageNum;
function appendData() {
	var total = $("#listForm input[name=total_count]").val();
	var size = $("#listForm input[name=size]").val();
	var ch = total/size;
	if(total%size!=0){
		ch += 1;
	}
	if(pageNum==null || pageNum==''){pageNum=1}
	$("#listForm input[name=page_num]").val(pageNum);
	$.ajax({ 
		type: 'POST',
		url: rootPath + '/extrtCondbyInq/timeLineChartAppend.html',
		data: $("#listForm").serialize(),
		success: function(data) {
			$('#timelineUl').append(data);
			pageNum +=1;
			if(pageNum >= ch){
				document.getElementById("appendButton").style.display = "none";
			}
			var ct = $("#timelineUl").find("li").length;
			var heightVal = ct*215;
			document.getElementById("lineBar").style.removeProperty("height");
			document.getElementById("lineBar").style.height = heightVal+'px';
		}
	});
}

$(function(){
	dataChange();
	$('input[type=text]').attr("onkeydown","javascript:if(event.keyCode==13){timeLineSearch();}");
});
