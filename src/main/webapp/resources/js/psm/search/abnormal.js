/**
 * 
 */
$(document).ready(function() {
	setchart1();
	setchart2();
	setDeptChart();
	setUserChart();
});


// 일별
function setchart1() {
	var searchType = $("input[name=searchType]").val();

	var system_seq;
	if(searchType=='day'){
		system_seq = $("select[id=day_system_seq]").val();
	} else if(searchType=='month'){
		system_seq = $("select[id=month_system_seq]").val();
	}
	
	var search_to = $("#searchForm input[name=search_to]").val();
	var search_from = $("#searchForm input[name=search_from]").val();
	
	$.ajax({
		type : 'POST',
		url : rootPath + '/extrtCondbyInq/findAbnormalChart0.html',
		data : {
			searchType : searchType,
			search_from : search_from,
			search_to : search_to,
			system_seq : system_seq
		},
		success : function(data) {
			drawchart1(data);
		}
	});
}
// 월별
function setchart2() {
	var searchType = $("input[name=searchType]").val();
	
	var system_seq;
	if(searchType=='day'){
		system_seq = $("select[id=day_system_seq]").val();
	} else if(searchType=='month'){
		system_seq = $("select[id=month_system_seq]").val();
	}
	
	var search_to = $("#searchForm input[name=search_to]").val();
	
	$.ajax({
		type : 'POST',
		url : rootPath + '/extrtCondbyInq/findAbnormalChart1.html',
		data : {
			searchType : searchType,
			search_to : search_to,
			system_seq : system_seq
		},
		success : function(data) {
			drawchart2(data);
		}
	});
}

function drawchart1(data) {
	var dataProvider = new Array();
	var graphs = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		dataProvider.push(jsonData[i].dataProvider);
	}
	if ( jsonData.length > 0) {
		var graphsTemp = jsonData[0].graphs;
		for (var j = 0; j < graphsTemp.length; j++) {
			graphs.push(graphsTemp[j]);
		}
	}
	
	var chart1 = AmCharts.makeChart("dashboard_amchart_1", {
	    "type": "serial",
	    "theme": "light",
	    "legend": {
	        "useGraphSettings": true,
	        "position": "bottom"
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "integersOnly": true,
	        "reversed": false,
	        "axisAlpha": 0,
	        "dashLength": 5,
	        "gridCount": 10,
	        "position": "left"
	    }],
	    "startDuration": 0,
	    "sequencedAnimation": false,
	    "startEffect": "easeOutSine",
	    "graphs": graphs,
	    "chartCursor": {
	        "cursorAlpha": 0,
	        "zoomable": false
	    },
	    "categoryField": "occr_dt",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "fillAlpha": 0.05,
	        "fillColor": "#000000",
	        "gridAlpha": 0,
	        "position": "bottom"
	    },
	    "export": {
	    	"enabled": false,
	        "position": "bottom-right"
	     }
	});
	chart1.addListener("clickGraphItem", function(event){
		var dateVal = event.item.category;
		var scen_seq = event.item.graph.valueField;
		goExtrtListDaily(dateVal, scen_seq);
	});
}
function goExtrtListDaily(dateVal, scen_seq) {
	$("#countForm input[name=search_to]").val(dateVal);
	$("#countForm input[name=search_from]").val(dateVal);
	$("#countForm input[name=scen_seq]").val(scen_seq);
	$("#countForm").attr("action",extrtCondbyInqConfig["listUrl"]);
	
	$("#countForm").submit();
}


function drawchart2(data) {
	var jsonData = JSON.parse(data);
	var dataProvider = new Array();
	dataProvider = jsonData[0];
	var graph = new Array();
	graph = jsonData[1];
	
	var chart = AmCharts.makeChart("dashboard_amchart_2", {
		  "type": "serial",
		  "theme": "light",
		  "sortColumns": true,
		  "legend": {
		    "horizontalGap": 10,
		    "maxColumns": 5,
		    "position": "bottom",
		    "useGraphSettings": true,
		    "markerSize": 10
		  },
		  "dataProvider": dataProvider,
		  "valueAxes": [{
		    "axisAlpha": 0.3,
		    "gridAlpha": 0
		  }],
		  "graphs": graph,
		  "categoryField": "occr_dt",
		  "categoryAxis": {
		    "gridPosition": "start",
		    "gridAlpha": 0,
		    "position": "left"
		  }
	});
	chart.addListener("clickGraphItem", function(event){
		var dateVal = event.item.category;
		var scen_seq = event.item.graph.valueField;
		goExtrtListMonth(dateVal, scen_seq);
	});
}
function goExtrtListMonth(dateVal, scen_seq) {
	var year = dateVal.substring(0,4);
	var month = dateVal.substring(4,6);
	var temp = new Date(year, month, 0);
	var lastDay = temp.getDate();
	var search_to = dateVal + lastDay;
	var search_from = dateVal + '01';
	
	$("#countForm input[name=search_to]").val(search_to);
	$("#countForm input[name=search_from]").val(search_from);
	$("#countForm input[name=scen_seq]").val(scen_seq);
	$("#countForm").attr("action",extrtCondbyInqConfig["listUrl"]);
	
	$("#countForm").submit();
}

function goExtrtCondbyList1(type, data) {
	var search_to = $("#searchForm input[name=search_to]").val();
	var search_from = $("#searchForm input[name=search_from]").val();
	
	$("#countForm input[name=isMonth]").val(type);
	
	$("#countForm input[name=search_to]").val(search_to);
	$("#countForm input[name=search_from]").val(search_from);
	
	$("#countForm input[name=scen_seq]").val(data);
	$("#countForm input[name=main_menu_id]").val("MENU00050");
	$("#countForm").attr("action",extrtCondbyInqConfig["listUrl"]);
	
	$("#countForm").submit();
}


function searchData(type, isSearch) {
	$("#searchForm input[name=searchType]").val(type);
	var system_seq;
	if(type=='day'){
		system_seq = $("select[id=day_system_seq]").val();
	} else if(type=='month'){
		system_seq = $("select[id=month_system_seq]").val();
	}
	$("#searchForm input[name=system_seq]").val(system_seq);
	$("#searchForm input[name=isSearch]").val(isSearch);
	$("#searchForm").attr("action",extrtCondbyInqConfig["dashboard"]);
	$("#searchForm").submit();
	
	var div = document.getElementById(type);
	var divReverse;
	var todayATag = document.getElementById("todayA");
	var dayATag = document.getElementById("dayA");
	var monthATag = document.getElementById("monthA");
	if(type=='day'){
		divReverse = document.getElementById('month');
		div.style.display = 'flex';
		divReverse.style.display = 'none';
		dayATag.style.color="#337ab7";
		todayATag.style.color="#a8b0b6";
		monthATag.style.color="#a8b0b6";
	} else if(type=='month'){
		divReverse = document.getElementById('day');
		div.style.display = 'flex';
		divReverse.style.display = 'none';
		monthATag.style.color="#337ab7";
		todayATag.style.color="#a8b0b6";
		dayATag.style.color="#a8b0b6";
	} else {
		divReverse = document.getElementById('day');
		div = document.getElementById('month');
		div.style.display = 'none';
		divReverse.style.display = 'none';
		todayATag.style.color="#337ab7";
		monthATag.style.color="#a8b0b6";
		dayATag.style.color="#a8b0b6";
	}
}

function setDeptChart() {
	var searchType = $("input[name=searchType]").val();
	
	var system_seq;
	if(searchType=='day'){
		system_seq = $("select[id=day_system_seq]").val();
	} else if(searchType=='month'){
		system_seq = $("select[id=month_system_seq]").val();
	}
	
	var search_to = $("#searchForm input[name=search_to]").val();
	var search_from = $("#searchForm input[name=search_from]").val();
	
	
	$.ajax({
		type : 'POST',
		url : rootPath + '/extrtCondbyInq/findAbnormalDeptDonutChart.html',
		data : {
			searchType : searchType,
			search_from : search_from,
			search_to : search_to,
			system_seq : system_seq
		},
		success : function(data) {
			drawDeptChart(data);
		}
	});
}

function drawDeptChart(data) {
	var jsonData = JSON.parse(data);
	var lengthVal = jsonData.length;
	
	var temp = document.getElementById("donutchart_temp");
	if(lengthVal==0){
		temp.style.display = 'block';
	}
	
	var dataProvider = new Array();
	for(i=0; i<lengthVal; i++){
		var dataPro = jsonData[i];
		var chartName = "donutchart_" + i;
		var dept_id = dataPro[0].dept_id;
		var chartDiv = document.getElementById(chartName);
		chartDiv.style.display = '';
		if(dept_id != 'DEPT99999'){
			$("#"+chartName).attr("onclick" , "detailDeptChart('"+dept_id+"')");
		}
		
		AmCharts.makeChart(chartName, {
			"type" : "pie",
			"theme" : "light",
			"dataProvider" : dataPro,
			"valueField" : "cnt",
			"titleField" : "dept_name",
			"labelRadius" : -60,
			"radius" : "40%",
			"innerRadius" : "60%",
			"labelText" : "[[title]]",
			"export" : {
				"enabled" : false
			},
			"balloonText" : "[[value]]",
			"colors" : [ '#72dad1','#e5eaf6' ]
		});
		
		var textName = "donutText_" + i;
		var textBar = document.getElementById(textName);
		var cnt = dataPro[0].cnt;
		var total = dataPro[1].cnt;
		var htmlStr = "<table style='width: 100%; height: 100%; font-size: 15px;'>";
		htmlStr += "<tr><td style='text-align: center; vertical-align: middle;'><span style='color:red;'> "+cnt+"건 </span>/ 총 "+total+"건 </td></tr></table>";
		textBar.innerHTML = htmlStr;
		textBar.style.display = '';
	}
}


function setUserChart() {
	var searchType = $("input[name=searchType]").val();
	
	var search_to = $("#searchForm input[name=search_to]").val();
	var search_from = $("#searchForm input[name=search_from]").val();
	
	var system_seq;
	if(searchType=='day'){
		system_seq = $("select[id=day_system_seq]").val();
	} else if(searchType=='month'){
		system_seq = $("select[id=month_system_seq]").val();
	}
	
	$.ajax({
		type : 'POST',
		url : rootPath + '/extrtCondbyInq/findAbnormalUserDonutChart.html',
		data : {
			searchType : searchType,
			search_from : search_from,
			search_to : search_to,
			system_seq : system_seq
		},
		success : function(data) {
			drawUserChart(data);
		}
	});
}

function drawUserChart(data) {
	var jsonData = JSON.parse(data);
	var lengthVal = jsonData.length;
	
	var temp = document.getElementById("donutchartP_temp");
	if(lengthVal==0){
		temp.style.display = 'block';
	}
	
	for(i=0; i<lengthVal; i++){
		var dataPro = jsonData[i];
		var emp_user_id = dataPro[0].user_id;
		var chartName = "donutchartP_" + i;
		var chartDiv = document.getElementById(chartName);
		chartDiv.style.display = '';
		if(emp_user_id != 'ZZZZ99999'){
			$("#"+chartName).attr("onclick" , "detailUserChart('"+emp_user_id+"')");
		}
		
		AmCharts.makeChart(chartName, {
			"type" : "pie",
			"theme" : "light",
			"dataProvider" : dataPro,
			"valueField" : "cnt",
			"titleField" : "user_name",
			"labelRadius" : -60,
			"radius" : "40%",
			"innerRadius" : "60%",
			"labelText" : "[[title]]",
			"export" : {
				"enabled" : false
			},
			"balloonText" : "[[value]]",
			"colors" : [ '#72dad1','#e5eaf6' ]
		});
		
		var textName = "donutPText_" + i;
		var textBar = document.getElementById(textName);
		var cnt = dataPro[0].cnt;
		var total = dataPro[1].cnt;
		var htmlStr = "<table style='width: 100%; height: 100%; font-size: 15px;'>";
		htmlStr += "<tr><td style='text-align: center; vertical-align: middle;'><span style='color:red;'> "+cnt+"건 </span>/ 총 "+total+"건 </td></tr></table>";
		textBar.innerHTML = htmlStr;
		textBar.style.display = '';
	}
}

function detailUserChart(emp_user_id) {
	$("#detailForm input[name=userId]").val(emp_user_id);
	var detailOccrDt = $("input[name=search_to]").val();
	$("#detailForm input[name=detailOccrDt]").val(detailOccrDt);
	$("#detailForm").attr("action",extrtCondbyInqConfig["detailChartUrl"]);
	$("#detailForm").submit();
}


function detailDeptChart(dept_id) {
	$("#detailForm input[name=dept_id]").val(dept_id);
	var detailOccrDt = $("input[name=search_to]").val();
	$("#detailForm input[name=detailOccrDt]").val(detailOccrDt);
	$("#detailForm").attr("action",extrtCondbyInqConfig["deptDetailChartUrl"]);
	$("#detailForm").submit();
}
