
$(document).ready(function() {
	/*getChart1();*/
	getChart2();
	getChart3();
	getChart4();
	getChart5();
	getChart20();
	/*getChart6();
	getChart7();
	getChart8();
	getChart9();
	getChart10();
	getChart11();
	getChart12();
	getChart13();
	getChart14();*/
});

function setTab(type, sub){
	if(type == 1) {
		/*getChart1();*/
		getChart2();
		getChart3();
		getChart4();
		getChart5();
		getChart20();
	}else if(type == 2) {
		if(sub == 1) {
			getChart6();
			getChart7();
		}else if(sub == 2) {
			getChart8();
		}else if(sub == 3) {
			getChart9();
		}else if(sub == 4) {
			getChart10_noAverage();
			/*var use_detailchart = $("#use_detailchart").val();
			if(use_detailchart == "yes")
				getChart10();
			else
				getChart10_noAverage();*/
		}else if(sub == 5) {
			getChart11();
			getChart12();
			getChart13();
			getChart14();
		}
	}else if(type ==3) {
		findTimeline1();
	}
}

/*function getChart1() {
	var emp_user_id = $("input[name=emp_user_id]").val();
	var dept_id = $("input[name=dept_id]").val();
	var search_from = $("input[name=search_from_chart]").val();
	var search_to = $("input[name=search_to_chart]").val();
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/extrtCondbyInq/findAbnormalDetailChart1.html',
		data: { 
			emp_user_name : emp_user_id,
			dept_id : dept_id,
			search_from : search_from,
			search_to : search_to
		},
		success: function(data) {
			drawChart1(data);
		},
		beforeSend:function() {
			$('#loading').show();
		},
		complete:function() {
			$('#loading').hide();
		}
	});
}*/

function getChart2() {
	var emp_user_id = $("input[name=emp_user_id]").val();
	var dept_id = $("input[name=dept_id]").val();
	var search_from = $("input[name=search_from_chart]").val();
	var search_to = $("input[name=search_to_chart]").val();
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/extrtCondbyInq/findAbnormalDetailDngChart.html',
		data: { 
			emp_user_name : emp_user_id,
			dept_id : dept_id,
			search_from : search_from,
			search_to : search_to,
			scen_seq : '3000'
		},
		success: function(data) {
			drawChart2(data);
		},
		beforeSend:function() {
			$('#loading').show();
		},
		complete:function() {
			$('#loading').hide();
		}
	});
}

function getChart3() {
	var emp_user_id = $("input[name=emp_user_id]").val();
	var dept_id = $("input[name=dept_id]").val();
	var search_from = $("input[name=search_from_chart]").val();
	var search_to = $("input[name=search_to_chart]").val();
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/extrtCondbyInq/findAbnormalDetailDngChart.html',
		data: { 
			emp_user_name : emp_user_id,
			dept_id : dept_id,
			search_from : search_from,
			search_to : search_to,
			scen_seq : '2000'
		},
		success: function(data) {
			drawChart3(data);
		},
		beforeSend:function() {
			$('#loading').show();
		},
		complete:function() {
			$('#loading').hide();
		}
	});
}

function getChart4() {
	var emp_user_id = $("input[name=emp_user_id]").val();
	var dept_id = $("input[name=dept_id]").val();
	var search_from = $("input[name=search_from_chart]").val();
	var search_to = $("input[name=search_to_chart]").val();
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/extrtCondbyInq/findAbnormalDetailDngChart.html',
		data: { 
			emp_user_name : emp_user_id,
			dept_id : dept_id,
			search_from : search_from,
			search_to : search_to,
			scen_seq : '4000'
		},
		success: function(data) {
			drawChart4(data);
		},
		beforeSend:function() {
			$('#loading').show();
		},
		complete:function() {
			$('#loading').hide();
		}
	});
}

function getChart5() {
	var emp_user_id = $("input[name=emp_user_id]").val();
	var dept_id = $("input[name=dept_id]").val();
	var search_from = $("input[name=search_from_chart]").val();
	var search_to = $("input[name=search_to_chart]").val();
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/extrtCondbyInq/findAbnormalDetailDngChart.html',
		data: { 
			emp_user_name : emp_user_id,
			dept_id : dept_id,
			search_from : search_from,
			search_to : search_to,
			scen_seq : '1000'
		},
		success: function(data) {
			drawChart5(data);
		},
		beforeSend:function() {
			$('#loading').show();
		},
		complete:function() {
			$('#loading').hide();
		}
	});
}

function getChart6() {
	var emp_user_id = $("input[name=emp_user_id]").val();
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/extrtCondbyInq/findAbnormalDetailChart6.html',
		data: { 
			emp_user_name : emp_user_id
		},
		success: function(data) {
			drawChart6(data);
		},
		beforeSend:function() {
			$('#loading').show();
		},
		complete:function() {
			$('#loading').hide();
		}
	});
}

function getChart7() {
	var emp_user_id = $("input[name=emp_user_id]").val();
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/extrtCondbyInq/findAbnormalDetailChart7.html',
		data: { 
			emp_user_name : emp_user_id
		},
		success: function(data) {
			drawChart7(data);
		},
		beforeSend:function() {
			$('#loading').show();
		},
		complete:function() {
			$('#loading').hide();
		}
	});
}

function getChart8() {
	var emp_user_id = $("input[name=emp_user_id]").val();
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/extrtCondbyInq/findAbnormalDetailChart8.html',
		data: { 
			emp_user_name : emp_user_id
		},
		success: function(data) {
			drawChart8(data);
		},
		beforeSend:function() {
			$('#loading').show();
		},
		complete:function() {
			$('#loading').hide();
		}
	});
}

function getChart9() {
	var emp_user_id = $("input[name=emp_user_id]").val();
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/extrtCondbyInq/findAbnormalDetailChart9.html',
		data: { 
			emp_user_name : emp_user_id
		},
		success: function(data) {
			drawChart9(data);
		},
		beforeSend:function() {
			$('#loading').show();
		},
		complete:function() {
			$('#loading').hide();
		}
	});
}

function getChart10() {
	var emp_user_id = $("input[name=emp_user_id]").val();
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/extrtCondbyInq/findAbnormalDetailChart10.html',
		data: { 
			emp_user_name : emp_user_id
		},
		success: function(data) {
			drawChart10(data);
		},
		beforeSend:function() {
			$('#loading').show();
		},
		complete:function() {
			$('#loading').hide();
		}
	});
}

function getChart10_noAverage() {
	var emp_user_id = $("input[name=emp_user_id]").val();
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/extrtCondbyInq/findAbnormalDetailChart10_noAverage.html',
		data: { 
			emp_user_name : emp_user_id
		},
		success: function(data) {
			drawChart10_noAverage(data);
		},
		beforeSend:function() {
			$('#loading').show();
		},
		complete:function() {
			$('#loading').hide();
		}
	});
}

function getChart11() {
	var emp_user_id = $("input[name=emp_user_id]").val();
	var search_from = $("input[name=search_from]").val();
	var search_to = $("input[name=search_to]").val();
	$.ajax({
		type: 'POST',
		url: rootPath + '/extrtCondbyInq/findAbnormalDetailChart11.html',
		data: { 
			emp_user_name : emp_user_id,
			search_from : search_from,
			search_to : search_to,
		},
		success: function(data) {
			drawChart11(data);
		},
		beforeSend:function() {
			$('#loading').show();
		},
		complete:function() {
			$('#loading').hide();
		}
	});
}

function getChart12() {
	var emp_user_id = $("input[name=emp_user_id]").val();
	var search_from = $("input[name=search_from]").val();
	var search_to = $("input[name=search_to]").val();

	$.ajax({
		type: 'POST',
		url: rootPath + '/extrtCondbyInq/findAbnormalDetailChart12.html',
		data: { 
			emp_user_name : emp_user_id,
			search_from : search_from,
			search_to : search_to,
		},
		success: function(data) {
			drawChart12(data);
		},
		beforeSend:function() {
			$('#loading').show();
		},
		complete:function() {
			$('#loading').hide();
		}
	});
}

function getChart13() {
	var emp_user_id = $("input[name=emp_user_id]").val();
	var search_from = $("input[name=search_from]").val();
	var search_to = $("input[name=search_to]").val();

	$.ajax({
		type: 'POST',
		url: rootPath + '/extrtCondbyInq/findAbnormalDetailChart13.html',
		data: { 
			emp_user_name : emp_user_id,
			search_from : search_from,
			search_to : search_to,
		},
		success: function(data) {
			drawChart13(data);
		},
		beforeSend:function() {
			$('#loading').show();
		},
		complete:function() {
			$('#loading').hide();
		}
	});
}

function getChart14() {
	var emp_user_id = $("input[name=emp_user_id]").val();
	var search_from = $("input[name=search_from]").val();
	var search_to = $("input[name=search_to]").val();

	$.ajax({
		type: 'POST',
		url: rootPath + '/extrtCondbyInq/findAbnormalDetailChart14.html',
		data: { 
			emp_user_name : emp_user_id,
			search_from : search_from,
			search_to : search_to,
		},
		success: function(data) {
			drawChart14(data);
		},
		beforeSend:function() {
			$('#loading').show();
		},
		complete:function() {
			$('#loading').hide();
		}
	});
}


/*function drawChart1(data) {
	var dataProvider = new Array();
	var color = ["FF9E01", "#FF9E01", "#FF9E01", "#FF9E01", "#FF9E01"];
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var t = i%5;
		
		var value = {"data1": "개인","nPrivCount": jsonData[i].data1};
		dataProvider.push(value);
		
		var value1 = {"data1": "소속","nPrivCount": jsonData[i].data2};
		dataProvider.push(value1);
		
	}
	var chart12 = AmCharts.makeChart("extrt_amchart_1", {
		  "type": "serial",
		  "theme": "light",
		  "marginRight": 20,
		  "dataProvider": dataProvider,
		  "valueAxes": [{
		    "axisAlpha": 0,
		    "position": "left",
		    "integersOnly": true,
			"minimum": 0,
		    "title": "비정상접근(접속지정보)(위험도)"
		  }],
		  "startDuration": 1,
		  "graphs": [{
		    "balloonText": "<b>[[category]]: [[value]]</b>",
		    "fillColorsField": "color",
		    "fillAlphas": 0.9,
		    "lineAlpha": 0.2,
		    "type": "column",
		    "valueField": "nPrivCount"
		  }],
		  "chartCursor": {
		    "categoryBalloonEnabled": false,
		    "cursorAlpha": 0,
		    "zoomable": false
		  },
		  "categoryField": "data1",
		  "categoryAxis": {
		    "gridPosition": "start",
		    "labelRotation": 45
		  },
		  "export": {
		    "enabled": false
		  }

		});
}*/

function drawChart2(data) {
	var dataProvider = new Array();
	var color = ["FF9E01", "#FF9E01", "#FF9E01", "#FF9E01", "#FF9E01"];
		
		var jsonData = JSON.parse(data);
		for (var i = 0; i < jsonData.length; i++) {
			var t = i%5;
			
			var value = {"data1": "개인","nPrivCount": jsonData[i].data1};
			dataProvider.push(value);
			
			var value1 = {"data1": "소속","nPrivCount": jsonData[i].data2};
			dataProvider.push(value1);
			
		}
		
		
		
		/*dashboard_amchart_12*/
		var chart12 = AmCharts.makeChart("extrt_amchart_2", {
			  "type": "serial",
			  "theme": "light",
			  "marginRight": 20,
			  "dataProvider": dataProvider,
			  "valueAxes": [{
			    "axisAlpha": 0,
			    "position": "left",
			    "integersOnly": true,
				"minimum": 0,
			    "title": "특정인처리(위험도)"
			  }],
			  "startDuration": 1,
			  "graphs": [{
			    "balloonText": "<b>[[category]]: [[value]]</b>",
			    "fillColorsField": "color",
			    "fillAlphas": 0.9,
			    "lineAlpha": 0.2,
			    "type": "column",
			    "valueField": "nPrivCount"
			  }],
			  "chartCursor": {
			    "categoryBalloonEnabled": false,
			    "cursorAlpha": 0,
			    "zoomable": false
			  },
			  "categoryField": "data1",
			  "categoryAxis": {
			    "gridPosition": "start",
			    "labelRotation": 45
			  },
			  "export": {
			    "enabled": false
			  }

			});
}

function drawChart3(data) {
	var dataProvider = new Array();
var color = ["FF9E01", "#FF9E01", "#FF9E01", "#FF9E01", "#FF9E01"];
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var t = i%5;
		
		var value = {"data1": "개인","nPrivCount": jsonData[i].data1};
		dataProvider.push(value);
		
		var value1 = {"data1": "소속","nPrivCount": jsonData[i].data2};
		dataProvider.push(value1);
	}
	var chart12 = AmCharts.makeChart("extrt_amchart_3", {
		  "type": "serial",
		  "theme": "light",
		  "marginRight": 20,
		  "dataProvider": dataProvider,
		  "valueAxes": [{
		    "axisAlpha": 0,
		    "position": "left",
		    "integersOnly": true,
			"minimum": 0,
		    "title": "비정상접근(계정)(위험도)"
		  }],
		  "startDuration": 1,
		  "graphs": [{
		    "balloonText": "<b>[[category]]: [[value]]</b>",
		    "fillColorsField": "color",
		    "fillAlphas": 0.9,
		    "lineAlpha": 0.2,
		    "type": "column",
		    "valueField": "nPrivCount"
		  }],
		  "chartCursor": {
		    "categoryBalloonEnabled": false,
		    "cursorAlpha": 0,
		    "zoomable": false
		  },
		  "categoryField": "data1",
		  "categoryAxis": {
		    "gridPosition": "start",
		    "labelRotation": 45
		  },
		  "export": {
		    "enabled": false
		  }

		});
}

function drawChart4(data) {
	var dataProvider = new Array();
var color = ["FF9E01", "#FF9E01", "#FF9E01", "#FF9E01", "#FF9E01"];
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var t = i%5;
		
		var value = {"data1": "개인","nPrivCount": jsonData[i].data1};
		dataProvider.push(value);
		
		var value1 = {"data1": "소속","nPrivCount": jsonData[i].data2};
		dataProvider.push(value1);
		
	}
	
	
	
	/*dashboard_amchart_12*/
	var chart12 = AmCharts.makeChart("extrt_amchart_4", {
		  "type": "serial",
		  "theme": "light",
		  "marginRight": 20,
		  "dataProvider": dataProvider,
		  "valueAxes": [{
		    "axisAlpha": 0,
		    "position": "left",
		    "integersOnly": true,
			"minimum": 0,
		    "title": "특정시간대 처리(위험도)"
		  }],
		  "startDuration": 1,
		  "graphs": [{
		    "balloonText": "<b>[[category]]: [[value]]</b>",
		    "fillColorsField": "color",
		    "fillAlphas": 0.9,
		    "lineAlpha": 0.2,
		    "type": "column",
		    "valueField": "nPrivCount"
		  }],
		  "chartCursor": {
		    "categoryBalloonEnabled": false,
		    "cursorAlpha": 0,
		    "zoomable": false
		  },
		  "categoryField": "data1",
		  "categoryAxis": {
		    "gridPosition": "start",
		    "labelRotation": 45
		  },
		  "export": {
		    "enabled": false
		  }

		});
}

function drawChart5(data) {
	var dataProvider = new Array();
	var color = ["FF9E01", "#FF9E01", "#FF9E01", "#FF9E01", "#FF9E01"];
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var t = i%5;
		
		var value = {"data1": "개인","nPrivCount": jsonData[i].data1};
		dataProvider.push(value);
		
		var value1 = {"data1": "소속","nPrivCount": jsonData[i].data2};
		dataProvider.push(value1);
	}
	var chart12 = AmCharts.makeChart("extrt_amchart_5", {
		  "type": "serial",
		  "theme": "light",
		  "marginRight": 20,
		  "dataProvider": dataProvider,
		  "valueAxes": [{
		    "axisAlpha": 0,
		    "position": "left",
		    "integersOnly": true,
			"minimum": 0,
		    "title": "개인정보과다처리(위험도)"
		  }],
		  "startDuration": 1,
		  "graphs": [{
		    "balloonText": "<b>[[category]]: [[value]]</b>",
		    "fillColorsField": "color",
		    "fillAlphas": 0.9,
		    "lineAlpha": 0.2,
		    "type": "column",
		    "valueField": "nPrivCount"
		  }],
		  "chartCursor": {
		    "categoryBalloonEnabled": false,
		    "cursorAlpha": 0,
		    "zoomable": false
		  },
		  "categoryField": "data1",
		  "categoryAxis": {
		    "gridPosition": "start",
		    "labelRotation": 45
		  },
		  "export": {
		    "enabled": false
		  }

		});
}

function drawChart6(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for (var i = 0; i < jsonData.length; i++) {
		dataProvider.push(jsonData[i]);
	}	
	
	/*dashboard_amchart_12*/
	var chart12 = AmCharts.makeChart("extrt_amchart_6", {
			"type": "serial",
			"categoryField" : "proc_date",
			"startDuration": 1,
			"categoryAxis": {
				"gridPosition": "start",
				"labelRotation": 45
			},
			"trendLines": [],
			"graphs": [
				{
					"balloonText": "[[title]]:[[value]] 건",
					"fillAlphas": 1,
					"id": "AmGraph-1",
					"lineColor": "#9182ED",
					"pointPosition": "star",
					"title": "일일취급량",
					"type": "column",
					"valueField": "value1"
				}
			],
			"guides": [],
			"valueAxes": [],
			"allLabels": [],
			"balloon": {},
			"legend": {
				"enabled": true,
				"useGraphSettings": true
			},
			"dataProvider" : dataProvider
		});
}

function drawChart7(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);

	for (var i = 0; i < jsonData.length; i++) {
		dataProvider.push(jsonData[i]);
	}	
	
	
	/*dashboard_amchart_12*/
	var chart12 = AmCharts.makeChart("extrt_amchart_7", {
			"type": "serial",
			"categoryField": "data1",
			"startDuration": 1,
			"categoryAxis": {
				"gridPosition": "start",
				"labelRotation": 45
			},
			"trendLines": [],
			"graphs": [
				{
					"balloonText": "[[value]] 건",
					/*"bullet": "round",*/
					"fillAlphas": 1,
					"id": "AmGraph-2",
					"lineColor": "#0000FF",
					/*"lineThickness": 2,*/
					"type": "column",
					"title": "하루 평균 개인정보 처리 건수(3개월)",
					"valueField": "cnt1"
				},
				{
					"balloonText": "[[value]] 건",
					"fillAlphas": 1,
					"id": "AmGraph-1",
					"title": "전일 처리 건수",
					"type": "column",
					"valueField": "cnt2"
				}
			],
			"guides": [],
			"valueAxes": [],
			"allLabels": [],
			"balloon": {},
			"legend": {
				"enabled": true,
				"useGraphSettings": true
			},
			"dataProvider" : dataProvider
		});
}

function drawChart8(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);

	for (var i = 0; i < jsonData.length; i++) {
		dataProvider.push(jsonData[i]);
	}	
	
	
	/*dashboard_amchart_12*/
	var chart12 = AmCharts.makeChart("extrt_amchart_8", {
			"type": "serial",
			"categoryField": "date",
			"startDuration": 1,
			"categoryAxis": {
				"gridPosition": "start"
			},
			"trendLines": [],
			"graphs": [
				{
					"balloonText": "[[title]]:[[value]] 건",
					"fillAlphas": 1,
					"id": "AmGraph-1",
					"lineColor": "#7AC9F3",
					"title": "월별 누적량",
					"type": "column",
					"valueField": "cnt"
				}
			],
			"guides": [],
			"valueAxes": [],
			"allLabels": [],
			"balloon": {},
			"legend": {
				"enabled": true,
				"useGraphSettings": true
			},
			"dataProvider": dataProvider
		});
}

function drawChart9(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);

	for (var i = 0; i < jsonData.length; i++) {
		dataProvider.push(jsonData[i]);
	}	
	
	
	/*dashboard_amchart_12*/
	var chart12 = AmCharts.makeChart("extrt_amchart_9", {
			"type": "serial",
			"categoryField": "date",
			"startDuration": 1,
			"categoryAxis": {
				"gridPosition": "start"
			},
			"trendLines": [],
			"graphs": [
				{
					"balloonText": "[[title]]:[[value]] 건",
					"fillAlphas": 1,
					"id": "AmGraph-1",
					"lineColor": "#7AC9F3",
					"title": "지난주 현황",
					"type": "column",
					"valueField": "cnt1"
				},
				{
					"balloonText": "[[title]]:[[value]] 건",
					"bullet": "round",
					"fixedColumnWidth": -2,
					"id": "AmGraph-2",
					"lineColor": "#E24444",
					"lineThickness": 2,
					"title": "3개월 평균 추이",
					"type": "smoothedLine",
					"valueField": "cnt2"
				}
			],
			"guides": [],
			"valueAxes": [],
			"allLabels": [],
			"balloon": {},
			"legend": {
				"enabled": true,
				"useGraphSettings": true
			},
			"dataProvider": dataProvider
		});
}

function drawChart10(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);

	for (var i = 0; i < jsonData.length; i++) {
		dataProvider.push(jsonData[i]);
	}	
	
	
	/*dashboard_amchart_12*/
	var chart12 = AmCharts.makeChart("extrt_amchart_10", {
			"type": "serial",
			"categoryField": "date",
			"categoryAxis": {
				"gridPosition": "start"
			},
			"trendLines": [],
			"graphs": [
				{
					"balloonText": "[[title]]:[[value]] 건",
					"bullet": "round",
					"fillAlphas": 0.7,
					"id": "AmGraph-1",
					"lineColor": "#9182ED",
					"lineAlpha": 0,
					"title": "전일",
					"type": "smoothedLine",
					"valueField": "cnt1"
				},
				{
					"balloonText": "[[title]]:[[value]] 건",
					"bullet": "square",
					"fillAlphas": 0.3,
					"id": "AmGraph-2",
					"lineAlpha": 0,
					"title": "3개월 평균",
					"type": "smoothedLine",
					"valueField": "cnt2"
				}
			],
			"legend": {
				"enabled": true
			},
			"dataProvider": dataProvider
	});
}

function drawChart10_noAverage(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);

	for (var i = 0; i < jsonData.length; i++) {
		dataProvider.push(jsonData[i]);
	}
	
	/*dashboard_amchart_12*/
	var chart12 = AmCharts.makeChart("extrt_amchart_10", {
			"type": "serial",
			"categoryField": "date",
			"categoryAxis": {
				"gridPosition": "start"
			},
			"trendLines": [],
			"graphs": [
				{
					"balloonText": "[[title]]:[[value]] 건",
					"bullet": "round",
					"fillAlphas": 0.7,
					"id": "AmGraph-1",
					"lineColor": "#9182ED",
					"lineAlpha": 0,
					"title": "최근 30일 평균 추이",
					"type": "smoothedLine",
					"valueField": "cnt1"
				}/*,
				{
					"balloonText": "[[title]]:[[value]] 건",
					"bullet": "square",
					"fillAlphas": 0.3,
					"id": "AmGraph-2",
					"lineAlpha": 0,
					"title": "3개월 평균",
					"type": "smoothedLine",
					"valueField": "cnt2"
				}*/
			],
			"legend": {
				"enabled": true
			},
			"dataProvider": dataProvider
	});
}

function drawChart11(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	if(jsonData.length > 0) {
		$("#extrt_amchart_11_nodata").css("display","none");
		$("#extrt_amchart_11").css("display","");
		for (var i = 0; i < jsonData.length; i++) {
			dataProvider.push(jsonData[i]);
		}

		/*var chart11 = AmCharts.makeChart("extrt_amchart_11", {
		    "type": "pie",
		    "theme": "light",
		    "innerRadius": "40%",
		    "gradientRatio": [-0.4, -0.4, -0.4, -0.4, -0.4, -0.4, 0, 0.1, 0.2, 0.1, 0, -0.2, -0.5],
		    "dataProvider": dataProvider,
		    "balloonText": "[[value]]",
			"categoryField": "data",
		    "valueField": "cnt",
		    "titleField": "data",
		    "balloon": {
		        "drop": true,
		        "adjustBorderColor": false,
		        "color": "#FFFFFF",
		        "fontSize": 16
		    }
		});*/
		var chart11 = AmCharts.makeChart( "extrt_amchart_11", {
			  "type": "pie",
			  "theme": "light",
			  "dataProvider": dataProvider,
			  "titleField": "data",
			  "valueField": "cnt",
			  "labelRadius": 5,
			  "radius": "45%",
			  "innerRadius": "35%",
			  "labelText": "[[title]]",
			  "export": {
			    "enabled": false
			  }
			} );
	} else {
		$("#extrt_amchart_11").css("display","none");
		$("#extrt_amchart_11_nodata").css("display","");
	}
	
}

function drawChart12(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	if(jsonData.length > 0) {
		$("#extrt_amchart_12_nodata").css("display","none");
		$("#extrt_amchart_12").css("display","");
		for (var i = 0; i < jsonData.length; i++) {
			dataProvider.push(jsonData[i]);
		}
		var chart12 = AmCharts.makeChart("extrt_amchart_12", {
			"type": "serial",
			"categoryField": "system_name",
			"startDuration": 1,
			"theme": "light",
			"categoryAxis": {
				"gridPosition": "start",
			    "labelRotation": 45
			},
			"trendLines": [],
			"graphs": [
				{
					"balloonText": "<b>[[category]]</b><br>[[title]]:[[value]]",
					"fillAlphas": 0.5,
					"id": "AmGraph-1",
					"title": "주민번호",
					"type": "column",
					"valueField": "cnt1"
				},
				{
					"balloonText": "<b>[[category]]</b><br>[[title]]:[[value]]",
					"fillAlphas": 0.5,
					"id": "AmGraph-2",
					"title": "운전면허",
					"type": "column",
					"valueField": "cnt2"
				},
				{
					"balloonText": "<b>[[category]]</b><br>[[title]]:[[value]]",
					"fillAlphas": 0.5,
					"id": "AmGraph-3",
					"title": "여권번호",
					"type": "column",
					"valueField": "cnt3"
				},
				{
					"balloonText": "<b>[[category]]</b><br>[[title]]:[[value]]",
					"fillAlphas": 0.5,
					"id": "AmGraph-4",
					"title": "외국인번호",
					"type": "column",
					"valueField": "cnt10"
				},
				{
					"balloonText": "<b>[[category]]</b><br>[[title]]:[[value]]",
					"fillAlphas": 0.5,
					"id": "AmGraph-5",
					"title": "기타",
					"type": "column",
					"valueField": "cnt"
				}
			],
			"guides": [],
			"valueAxes": [
				{
					"id": "ValueAxis-1",
					"stackType": "regular"
				}
			],
			"allLabels": [],
			"balloon": {},
			"legend": {
				"enabled": true,
				"useGraphSettings": true
			},
			"titles": [],
			"dataProvider": dataProvider
		});
	} else {
		$("#extrt_amchart_12").css("display","none");
		$("#extrt_amchart_12_nodata").css("display","");
	}
}

function drawChart13(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	if(jsonData.length > 0) {
		$("#extrt_amchart_13_nodata").css("display","none");
		$("#extrt_amchart_13").css("display","");
		for (var i = 0; i < jsonData.length; i++) {
			dataProvider.push(jsonData[i]);
		}	
		var chart13 = AmCharts.makeChart("extrt_amchart_13", {
			"type": "serial",
			"theme": "light",
			"categoryField": "data",
			"startDuration": 1,
			"categoryAxis": {
				"gridPosition": "start"
			},
			"trendLines": [],
			"graphs": [
				{
					"balloonText": "[[category]]: [[value]] 건",
					"fillAlphas": 1,
					"id": "AmGraph-1",
					"title": "시스템별 처리량",
					"type": "column",
			        "labelText": "[[value]]",
					"valueField": "cnt"
				}
			],
			"guides": [],
			"valueAxes": [],
			"allLabels": [],
			"balloon": {},
			"dataProvider" : dataProvider
		});
		
	} else {
		$("#extrt_amchart_13").css("display","none");
		$("#extrt_amchart_13_nodata").css("display","");
	}
}

function drawChart14(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	if(jsonData.length > 0) {
		$("#extrt_amchart_14_nodata").css("display","none");
		$("#extrt_amchart_14").css("display","");
		for (var i = 0; i < jsonData.length; i++) {
			dataProvider.push(jsonData[i]);
		}	
		/*var chart14 = AmCharts.makeChart("extrt_amchart_14", {
		    "type": "pie",
		    "theme": "light",
		    "innerRadius": "40%",
		    "gradientRatio": [-0.4, -0.4, -0.4, -0.4, -0.4, -0.4, 0, 0.1, 0.2, 0.1, 0, -0.2, -0.5],
		    "dataProvider": dataProvider,
		    "balloonText": "[[value]]",
			"categoryField": "data",
		    "titleField": "data",
		    "valueField": "cnt",
		    "balloon": {
		        "drop": true,
		        "adjustBorderColor": false,
		        "color": "#FFFFFF",
		        "fontSize": 16
		    }
		});*/
		var chart14 = AmCharts.makeChart( "extrt_amchart_14", {
			  "type": "pie",
			  "theme": "light",
			  "dataProvider": dataProvider,
			  "titleField": "data",
			  "valueField": "cnt",
			  "labelRadius": 5,

			  "radius": "45%",
			  "innerRadius": "35%",
			  "labelText": "[[title]]",
			  "export": {
			    "enabled": false
			  }
			} );
	} else {
		$("#extrt_amchart_14").css("display","none");
		$("#extrt_amchart_14_nodata").css("display","");
	}
}


function getChart20() {
	var emp_user_id = $("input[name=emp_user_id]").val();
	var dept_id = $("input[name=dept_id]").val();
	var search_from = $("input[name=search_from_chart]").val();
	var search_to = $("input[name=search_to_chart]").val();
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/extrtCondbyInq/findAbnormalDetailDngChart.html',
		data: { 
			emp_user_name : emp_user_id,
			dept_id : dept_id,
			search_from : search_from,
			search_to : search_to,
			scen_seq : '5000'
		},
		success: function(data) {
			drawChart20(data);
		},
		beforeSend:function() {
			$('#loading').show();
		},
		complete:function() {
			$('#loading').hide();
		}
	});
}

function drawChart20(data) {
	var dataProvider = new Array();
	var color = ["FF9E01", "#FF9E01", "#FF9E01", "#FF9E01", "#FF9E01"];
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var t = i%5;
		var value = {"data1": "개인","nPrivCount": jsonData[i].data1};
		dataProvider.push(value);
		var value1 = {"data1": "소속","nPrivCount": jsonData[i].data2};
		dataProvider.push(value1);
	}
	var chart20 = AmCharts.makeChart("extrt_amchart_D6", {
		  "type": "serial",
		  "theme": "light",
		  "marginRight": 20,
		  "dataProvider": dataProvider,
		  "valueAxes": [{
		    "axisAlpha": 0,
		    "position": "left",
		    "integersOnly": true,
			"minimum": 0,
		    "title": "다운로드 의심행위(위험도)"
		  }],
		  "startDuration": 1,
		  "graphs": [{
		    "balloonText": "<b>[[category]]: [[value]]</b>",
		    "fillColorsField": "color",
		    "fillAlphas": 0.9,
		    "lineAlpha": 0.2,
		    "type": "column",
		    "valueField": "nPrivCount"
		  }],
		  "chartCursor": {
		    "categoryBalloonEnabled": false,
		    "cursorAlpha": 0,
		    "zoomable": false
		  },
		  "categoryField": "data1",
		  "categoryAxis": {
		    "gridPosition": "start",
		    "labelRotation": 45
		  },
		  "export": {
		    "enabled": false
		  }
		});
}