/**
 * 위험도별조회 Javascript
 * @author tjlee
 * @since 2014. 4. 10
 */
	 	
 // 상세로 이동
function fnempDetailInqDetail(emp_user_id,startday,endday){
	$("input[name=detailEmpCd]").attr("value",emp_user_id);
	$("input[name=detailStartDay]").attr("value",startday);
	$("input[name=detailEndDay]").attr("value",endday);
	
	$("#listForm").attr("action",empDetailInqConfig["detailUrl"]);
	$("#listForm").submit();
}

// 페이지 이동
function goPage(num){
	$("#listForm").attr("action",empDetailInqConfig["listUrl"]);
	$("#listForm input[name=page_num]").val(num);
	$("#listForm").submit();
}

//상세페이지 페이징 
function goPageempDetailInqDetail(num){
	$("#listForm").attr("action",empDetailInqConfig["detailUrl"]);
	$("#listForm input[name=empDetailInqDetail_page_num]").val(num);
	$("#listForm").submit();
}

// 검색
function moveempDetailInqList() {
	$('input[name=page_num]').attr("value","1");
	$("#listForm").attr("action",empDetailInqConfig["listUrl"]);
	$("#listForm").submit();
}

// 목록으로
function goList() {
	$("#listForm").attr("action",empDetailInqConfig["listUrl"]);
	$("#listForm").submit();
}

// 기간선택 검색조건
function initDaySelect(){
	if($("#daySelect").val() == "Day"){
		$(".search_fr").datepicker("setDate","-0d");
		$(".search_to").datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "WeekDay"){
		$(".search_fr").datepicker("setDate","-7d");
		$(".search_to").datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "MonthDay"){
		$(".search_fr").datepicker("setDate","-1m");
		var setDateStart = $(".search_fr").val();
		setDateStart = setDateStart.substr(0,8);
		setDateStart = setDateStart + "01";
		$(".search_fr").val(setDateStart);
		$(".search_to").datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "YearDay"){
		$(".search_fr").datepicker("setDate","-0d");
		var setDateStart = $(".search_fr").val();
		setDateStart = setDateStart.substr(0,5);
		setDateStart = setDateStart + "01-01";
		$(".search_fr").val(setDateStart);
		$(".search_to").datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "TotalMonthDay"){
		$(".search_fr").datepicker("setDate","-1m");
		var setDateStart = $(".search_fr").val();
		setDateStart = setDateStart.substr(0,8);
		setDateStart = setDateStart + "01";
		$(".search_fr").val(setDateStart);
		var setDateend =  $(".search_to").val();
		$(".search_to").datepicker("setDate","0");
		setDateend = setDateend.substr(8,10);
		$(".search_to").datepicker("setDate","-"+setDateend+"d");
	}else{
		dateDisabled(false);
	}
}

function dateDisabled(bool){
	$('.search_fr').attr('disabled', bool);
	$('.search_to').attr('disabled', bool);
	$('.ui-datepicker-trigger').css('visibility', bool == true ? 'hidden' : 'visible');
	$('.ui-datepicker-trigger').css('visibility', bool == true ? 'hidden' : 'visible');
}

// 위험도별 상세에서 한줄클릭 시 추출조건별 상세로 이동
function fnExtrtCondbyDetailList(occr_dt,emp_user_id,emp_detail_seq){
	$("input[name=detailOccrDt]").attr("value",occr_dt);
	$("input[name=detailEmpCd]").attr("value",emp_user_id);
	$("input[name=detailEmpDetailSeq]").attr("value",emp_detail_seq);
	$("input[name=bbs_id]").attr("value","empDetailInq");
	
	//메뉴명 추출조건별조회로 변경
	$('input[name=main_menu_id]').attr("value","MENU00050");
	$('input[name=sub_menu_id]').attr("value","MENU00043");
	$("#listForm").attr("action",extrtCondbyInqDetailConfig["detailUrl"]);
	$("#listForm").submit();
}

// 위험도별 상세 로그 뿌리기
function fnExtrtDetailLogView(data){
	$.each(data.aa, function(index, item){
		var detailLogOutput = "";
		detailLogOutput += "<tr>";
		detailLogOutput += "<td>"+ item;
		$('#extrtDetailLogList').append();
	});
}

// 상세창에서 클릭
function fnempDetailInqListTrClick(rule_cd){
	$('input[name=detailRuleCd]').attr("value",rule_cd);
	var $lstForm = $('#listForm');
	$lstForm.submit();
}

/**
 * 엑셀 다운로드 추가 2015/6/4
 */
function excelEmpDetailInqList(){
//	if(arguments[0] >= 65535){
//		alert("엑셀다운로드는 65535건까지 가능합니다.");
//	}else{
		$("#listForm").attr("action",empDetailInqConfig["downloadUrl"]);
		$("#listForm").submit();
//	}
}

