/**
 * 전체로그조회 Javascript
 * @author tjlee
 * @since 2014. 4. 10
 */


function gotoReqList(system_seq){
	var mainMenuStr = '[{"menu_id":"MENU00416","menu_name":"접속이력조회","menu_url":"/reqLogInq/list.html","auth_id":"AUTH00000"},{"menu_id":"MENU00040","menu_name":"접근기록조회","menu_url":"/allLogInq/list.html","auth_id":"AUTH00000"},{"menu_id":"MENU00050","menu_name":"접근기록분석","menu_url":"/extrtCondbyInq/list.html","auth_id":"AUTH00000"},{"menu_id":"MENU00070","menu_name":"통계및보고","menu_url":"/statistics/list_perdbyTypeAnals.html","auth_id":"AUTH00000"},{"menu_id":"MENU00030","menu_name":"모니터링","menu_url":"/dashboard/init_dashboardView.html","auth_id":"AUTH00000"},{"menu_id":"MENU00080","menu_name":"정책설정관리","menu_url":"/indvinfoTypeSetup/list.html","auth_id":"AUTH00000"},{"menu_id":"MENU00010","menu_name":"환경설정","menu_url":"/groupCodeMngt/list.html","auth_id":"AUTH00000"}]';
	$("input[name=system_seq]").attr("value",system_seq);
	$("#listForm").attr("action",allLogInqConfig["reqUrl"]);
	$("#listForm").submit();
	drawMainMenu("mainMenus", mainMenuStr, "MENU00416");
}


// 상세로 이동
function fnAllLogInqDetail(log_seq, proc_date){
	$("input[name=detailLogSeq]").attr("value",log_seq);
	$("input[name=detailProcDate]").attr("value",proc_date);
	
	$("#listForm").attr("action",allLogInqConfig["detailUrl"]);
	$("#listForm").submit();

}

// 페이지 이동
function goPage(num){
	$("#listForm").attr("action",allLogInqConfig["listUrl"]);
	$("#listForm input[name=page_num]").val(num);
	$("#listForm").submit();
}

//상세화면 페이지 이동
function goPageallLogInqDetail(num){
	$("#listForm").attr("action",allLogInqConfig["detailUrl"]);
	$("#listForm input[name=allLogInqDetail_page_num]").val(num);
	$("#listForm").submit();
}

// 검색
function moveallLogInqList() {
	var listForm = $("#listForm");
	var tDate = listForm.find("input[name=search_to]").val();
	var fDate = listForm.find("input[name=search_from]").val();
	if(tDate<fDate){
		alert("검색 기간을 확인하세요");
		return;
	}
	$('input[name=page_num]').attr("value","1");
	$('input[name=isSearch]').attr("value","Y");
	$("#listForm").attr("action",allLogInqConfig["listUrl"]);
	$("#listForm").submit();
}

// 기간선택 검색조건
function initDaySelect(){
	if($("#daySelect").val() == "Day"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "WeekDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-7d");
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "MonthDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-1m");
		var setDateStart = $("#search_fr").val();
		setDateStart = setDateStart.substr(0,8);
		setDateStart = setDateStart + "01";
		$("#search_fr").val(setDateStart);
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "YearDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
		var setDateStart = $("#search_fr").val();
		setDateStart = setDateStart.substr(0,5);
		setDateStart = setDateStart + "01-01";
		$("#search_fr").val(setDateStart);
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "TotalMonthDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-1m");
		var setDateStart = $("#search_fr").val();
		setDateStart = setDateStart.substr(0,8);
		setDateStart = setDateStart + "01";
		$("#search_fr").val(setDateStart);
		var setDateend =  $("#search_to").val();
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","0");
		setDateend = setDateend.substr(8,10);
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-"+setDateend+"d");
	}else{
		dateDisabled(false);
	}
}

function dateDisabled(bool){
	$('.search_fr').attr('disabled', bool);
	$('.search_to').attr('disabled', bool);
	$('.ui-datepicker-trigger').css('visibility', bool == true ? 'hidden' : 'visible');
	$('.ui-datepicker-trigger').css('visibility', bool == true ? 'hidden' : 'visible');
}

//검색
function goList() {
	$("#listForm").attr("action",allLogInqConfig["listUrl"]);
	$("#listForm").submit();
}

// 추출조건 상세화면으로 이동
function moveExtrtCondbyInqDetail(){
	$("#listForm").attr("action",extrtCondbyInqConfig["detailUrl"]);
	$("#listForm").submit();
}
/**
 * 엑셀 다운로드 추가 2015/6/4
 */
function excelAllLogInqList(){
//	if(arguments[0] >= 65535){
//		alert("엑셀다운로드는 65535건까지 가능합니다.");
//	}else{
		$("#listForm").attr("action",allLogInqConfig["downloadUrl"]);
		$("#listForm").submit();
//	}
}

/*개인정보 제외 처리*/

function deletePrivacy(bizLog,privType,pattern){
	sendAjaxIpPerm("add",bizLog,privType,pattern);
}

function sendAjaxIpPerm(type,bizLog,privType,pattern){
	var $form = $("#listForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'misdetect_pattern, privacy_type';
	var menu_id = $('input[name=current_menu_id]').val();
	$('input[name=privacy_type]').attr("value",privType);
	$('input[name=misdetect_pattern]').attr("value",pattern);
	$('input[name=biz_log_result_seq]').attr("value",bizLog);
	
	log_message_title = '개인정보 제외';
	log_action = 'INSERT';
	
	$form.appendLogMessageParamsInput_allLogInq(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(allLogInqConfig[type + "Url"], $form.serialize(), ajaxIpPerm_successHandler, ajaxIpPerm_errorHandler, 'add');
}

// 그룹코드 ajax call - 성공
function ajaxIpPerm_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			alert("세션이 만료되었습니다.");
			location.href = allLogInqConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	} else {
		
		alert("성공적으로 제외되었습니다.");
		moveList();
	}
}

// 그룹코드 ajax call - 실패
function ajaxIpPerm_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	alert("제외를 실패하였습니다. : " + textStatus);

}

function moveList(){
	$("#listForm").attr("action", allLogInqConfig["detailUrl"]);
	$("#listForm").submit();
}


$(function(){
	$('input[type=text]').attr("onkeydown","javascript:if(event.keyCode==13){moveallLogInqList();}");
});

function fnExtrtEmpDetailInfo(emp_user_name,emp_user_id, user_ip) {
	
	$("input[name=emp_user_name]").attr("value",emp_user_name);
	$("input[name=emp_user_id]").attr("value",emp_user_id);
	$("input[name=user_ip]").attr("value",user_ip);
	
/*	$("input[name=detailOccrDt]").attr("value",occr_dt);
	$("input[name=detailEmpCd]").attr("value",emp_user_id);
	$("input[name=detailEmpDetailSeq]").attr("value",emp_detail_seq);
	$("input[name=bbs_id]").attr("value","empDetailInq");
	$("select[name=rule_cd]").attr("value","");*/
	
	$("#listForm").attr("action",extrtCondbyInqConfig["detailChartUrl"]);
	$("#listForm").submit();
}

//목록으로 이동
function moveSummonDetail(){
	$("#listForm").attr("action",allLogInqConfig["dummondetailUrl"]);
	$("#listForm").submit();
}


//소명 요청
function addSummon(){
//	var cnt = $('input:checkbox[name=auth_ids]:checked').length;
	var description1 =htmlspecialchars($('textarea[name=description1]').val());
	var email_address =htmlspecialchars($('input[name=email_address]').val());
	var description2 =htmlspecialchars($('textarea[name=description2]').val());
	var rule_nm =htmlspecialchars($('input[name=rule_nm]').val());
	if(description1 == '') {
		alert("소명요청사유를 입력해주세요");
		$('textarea[name=description1]').focus();
		return false;
	}
	if(email_address == '') {
		alert("E-MAIL을 입력해주세요");
		$('input[name=email_address]').focus();
		return false;
	}
	if(description2 == '') {
		alert("내용을 입력해주세요");
		$('textarea[name=description2]').focus();
		return false;
	}
		sendAjaxSummon("add");	
	
}


//소명요청 ajax call
function sendAjaxSummon(type){
	var $form = $("#summonDetailForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'description1,email_address,description2';
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'add') {
		log_message_title = '소명요청';
		log_action = 'INSERT';
	}
	/* else if (type == 'save') {
		var password = $("#adminUserDetailForm input[name=password]").val();
		if(password != null && password != '') {
			log_message_title = '관리자수정(비밀번호수정)';
		}
		else {
			log_message_title = '관리자수정';
		}
		log_action = 'UPDATE';
	} else if (type == 'remove') {
		log_message_title = '관리자삭제';
		log_action = 'REMOVE';
	}*/
	
//	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(allLogInqConfig[type + "summonUrl"],$form.serialize(), ajaxAdminUser_successHandler, ajaxAdminUser_errorHandler, type);
}


//관리자 ajax call - 성공
function ajaxAdminUser_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = adminUserDetailConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}
		
		moveSummonDetail();
	}
}


// 관리자 ajax call - 실패
function ajaxAdminUser_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	log_message += codeLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("관리자 등록 실패하였습니다." + textStatus);
			break;
		case "save":
			alert("관리자 수정 실패하였습니다." + textStatus);
			break;
		case "remove":
			alert("관리자 삭제 실패하였습니다." + textStatus);
			break;
		default:
	}
}

function moveSpcList() {
	$("#listForm").attr("action",allLogInqspcDetailConfig["listUrl"]);
	$("#listForm").submit();
}
