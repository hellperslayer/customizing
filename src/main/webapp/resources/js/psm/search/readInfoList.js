/**
 * 전체로그조회 Javascript
 * @author tjlee
 * @since 2014. 4. 10
 */


// 상세로 이동
function fnAllLogInqDetail(log_seq, proc_date){
	$('#search_fr').attr('disabled', false);
	$('#search_to').attr('disabled', false);
	$("input[name=detailProcDate]").attr("value",proc_date);
	$("input[name=detailLogSeq]").attr("value",log_seq);
	$("input[name=bbs_id]").attr("value",'readInfoList');
	
	$("#listForm").attr("action",readInfoConfig["detailUrl"]);
	$("#listForm").submit();

}

// 페이지 이동
function goPage(num){
	$('#search_fr').attr('disabled', false);
	$('#search_to').attr('disabled', false);
	$("#listForm").attr("action",readInfoConfig["listUrl"]);
	$("#listForm input[name=page_num]").val(num);
	$("#listForm").submit();
}

// 검색
function moveallLogInqList() {
	var privacy = $("input[name=privacy").val();
	if(privacy==null || privacy=='') {
		alert("개인정보내용을 입력해주세요.");
		return;
	}
	
	$('#search_fr').attr('disabled', false);
	$('#search_to').attr('disabled', false);
	var listForm = $("#listForm");
	$('input[name=page_num]').attr("value","1");
	$("#listForm").attr("action",readInfoConfig["listUrl"]);
	$("#listForm").submit();
}

// 기간선택 검색조건
function initDaySelect(){
	if($("#daySelect").val() == "Day"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
		$('#search_fr').attr('disabled', true);
		$('#search_to').attr('disabled', true);
	}else if($("#daySelect").val() == "WeekDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-7d");
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
		$('#search_fr').attr('disabled', true);
		$('#search_to').attr('disabled', true);
	}else if($("#daySelect").val() == "MonthDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-1m");
		var setDateStart = $("#search_fr").val();
		setDateStart = setDateStart.substr(0,8);
		setDateStart = setDateStart + "01";
		$("#search_fr").val(setDateStart);
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "YearDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
		var setDateStart = $("#search_fr").val();
		setDateStart = setDateStart.substr(0,5);
		setDateStart = setDateStart + "01-01";
		$("#search_fr").val(setDateStart);
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "TotalMonthDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-1m");
		var setDateStart = $("#search_fr").val();
		setDateStart = setDateStart.substr(0,8);
		setDateStart = setDateStart + "01";
		$("#search_fr").val(setDateStart);
		var setDateend =  $("#search_to").val();
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","0");
		setDateend = setDateend.substr(8,10);
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-"+setDateend+"d");
	} else if($("#daySelect").val() == "") {	
		// 직접입력
		$('#search_fr').attr('disabled', false);
		$('#search_to').attr('disabled', false);
	} else{
		dateDisabled(false);
	}
}

function dateDisabled(bool){
	$('.search_fr').attr('disabled', bool);
	$('.search_to').attr('disabled', bool);
	$('.ui-datepicker-trigger').css('visibility', bool == true ? 'hidden' : 'visible');
	$('.ui-datepicker-trigger').css('visibility', bool == true ? 'hidden' : 'visible');
}

$(function(){
	$('input[type=text]').attr("onkeydown","javascript:if(event.keyCode==13){moveallLogInqList();}");
	$('#search_fr').attr("onclick", "resetPeriod()");
	$('#search_to').attr("onclick", "resetPeriod()");
	
	var daySel = $('#daySelect').val();
	if (daySel == "Day" || daySel == "WeekDay") {
		$('#search_fr').attr('disabled', true);
		$('#search_to').attr('disabled', true);
	}
});


