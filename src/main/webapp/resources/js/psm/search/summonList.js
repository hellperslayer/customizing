/**
 * 추출조건별조회 Javascript
 * @author tjlee
 * @since 2014. 4. 10
 */
	 
 // 상세로 이동
var system_check_tab = false;
$(document).ready(function() {
	$('.left_area').css('display', 'none');
	
	$('#system_check').attr("tabindex", -1).blur(function(){
		var hovertag1 = $('#system_check input[type=checkbox]:hover').length;
		var hovertag2 = $('#system_seq:hover').length;
		var hovercount = hovertag1+hovertag2;
		if(hovercount <= 0){
			$('#system_check').css('display','none');
			system_check_tab = false; 
		}
	});
	
	$('.system_check').click(function(){
		$('#system_check').css('display','table');
		$('#system_check').attr("tabindex", -1).focus();
	});
	
	//소명 진행상태 최종판정 눌럿을때 승인구분 비활성화처리(DGB전용)
	var status = $('select[name=summon_status]').val();
	if(status == 60){
		$('select[name=decision_status]').prop('disabled',true);
	}
	$('select[name=summon_status]').change(function(){
		status = $('select[name=summon_status]').val();
		if(status == 60){
			$('select[name=decision_status]').prop('disabled',true);
		}else{
			$('select[name=decision_status]').prop('disabled',false);
		}
	});
	
});
function systemCheckDisplay(){
	if(system_check_tab){
		$('#system_check').css('display','none');
		$('#system_seq').attr("tabindex", -1).blur();
		system_check_tab = false;
	}else{
		$('#system_check').css('display','table');
		$('#system_check').attr("tabindex", -1).focus();
		system_check_tab = true;
	}
}

function systemAllcheckCount(){
	var checklist = "";
	$('.syscheck:checked').each(function(idx,val){
		checklist = checklist+$(val).val()+','
	});
	if($('.syscheck:checked').length == $('.syscheck').length){
		$('#sysallcheck').prop('checked',true);
		systemCheckText(true);
	}else{
		$('#sysallcheck').prop('checked',false);
		systemCheckText(false);
	}
}

function systemAllcheck(){
	if($('#sysallcheck').prop('checked')){
		$('.syscheck').prop('checked',true);
		systemCheckText(true);
	}else{
		$('.syscheck').prop('checked',false);
		systemCheckText(false);
	}
}

function systemCheckText(allcheck){
	var firstSys = "";
	
	if(allcheck){
		$('#system_seq').text('전 체');
	}else if($('.syscheck:checked').length>1){
		firstSys = $('.syscheck:checked').eq(0).next().text();
		$('#system_seq').text(firstSys+' 외 ' + ($('.syscheck:checked').length-1) + '개 시스템');
	}else if($('.syscheck:checked').length==1){
		firstSys = $('.syscheck:checked').eq(0).next().text();
		$('#system_seq').text(firstSys+' 시스템');
	}else{
		$('#system_seq').text('시스템 미선택');
	}
}

function fnsummonDetail(log_seq,proc_date){
	$("input[name=detailEmpCd]").attr("value",log_seq);
	$("input[name=detailOccrDt]").attr("value",proc_date);
	
	$("#summonDetailForm").attr("action",summonConfig["detailUrl"]);
	$("#summonDetailForm").submit();
}

// 상세에서 전체로그 상세로 이동
function fnAllLogInqDetail(log_seq,proc_date,proc_time,emp_user_name,result_type){
	$("input[name=detailLogSeq]").attr("value",log_seq);
	$("input[name=detailProcDate]").attr("value",proc_date);
	$("input[name=detailProcTime]").attr("value",proc_time);
	$("input[name=user_name]").attr("value",emp_user_name);
	$("input[name=result_type]").attr("value",result_type);
	if($("input[name=bbs_id]").val() == ''){
		$("input[name=bbs_id]").attr("value","summon");
	}
	
	$("#listForm").attr("action",allLogInqDetailConfig["detailUrl"]);
	$("#listForm").submit();
}

// 페이지 이동
function goPage(num){
	$("#listForm").attr("action",summonManageConfig["listUrl"]);
	$("#listForm input[name=page_num]").val(num);
	$("#listForm").submit();
}

// 상세페이지 페이징 
function goPagesummonDetail(num){
	$("#listForm").attr("action",summonManageConfig["detailUrl"]);
	$("#listForm input[name=page_num]").val(num);
	$("#listForm").submit();
}

function LogFilterSearch(){
	$("#listForm").attr("action",summonManageConfig["detailUrl"]);
	$("#listForm input[name=page_num]").val(1);
	$("#listForm").submit();
}

// 검색
function movesummonList() {
	var syscheck = '';
	$('input[name=syscheck]:checked').each(function(idx,val){
		syscheck += $(val).val()+',';
	});
	syscheck = syscheck.substr(0,syscheck.length-1);
	$('input[name=system_seq]').val(syscheck);
	$('input[name=page_num]').attr("value","1");
	$("#listForm").attr("action",summonConfig["listUrl"]);
	$("#listForm").submit();
}

// 목록으로 (소명조회화면)
function goSummonList(cur_num) {
	$("#listForm").attr("action",summonConfig["goSummonListUrl"]);
	$('input[name=page_num]').val(cur_num);
	$("#listForm").submit();
}

function goSummonManageList(cur_num){
	$("#listForm").attr("action",summonManageDetailConfig["summonManageListUrl"]);
	$('input[name=page_num]').val(cur_num);
	$("#listForm").submit();
}

// 목록으로 (소명조회화면)-
function movecenterSummonList() {
	$("#listForm").attr("action",centerSummonConfig["listUrl"]);
	$("#listForm").submit();
}

// 목록으로 (위험도별조회상세화면)
function goEmpDetailInqList() {
	
	//메뉴명위험도별조회상세로 변경
	$('input[name=main_menu_id]').attr("value","MENU00050");
	$('input[name=sub_menu_id]').attr("value","MENU00047");
	
	$("#listForm").attr("action",summonConfig["goEmpDetailInqListUrl"]);
	$("#listForm").submit();
}

// 목록으로 (소명요청화면)
function goCallingDemandList() {
	//메뉴명위험도별조회상세로 변경
	$('input[name=main_menu_id]').attr("value","MENU00060");
	$('input[name=sub_menu_id]').attr("value","MENU00067");
	
	$("#listForm").attr("action",summonConfig["goCallingDemandListUrl"]);
	$("#listForm").submit();
}



// 기간선택 검색조건
function initDaySelect(){
	if($("#daySelect").val() == "Day"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "WeekDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-7d");
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "MonthDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-1m");
		var setDateStart = $("#search_fr").val();
		setDateStart = setDateStart.substr(0,8);
		setDateStart = setDateStart + "01";
		$("#search_fr").val(setDateStart);
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "YearDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
		var setDateStart = $("#search_fr").val();
		setDateStart = setDateStart.substr(0,5);
		setDateStart = setDateStart + "01-01";
		$("#search_fr").val(setDateStart);
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "TotalMonthDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-1m");
		var setDateStart = $("#search_fr").val();
		setDateStart = setDateStart.substr(0,8);
		setDateStart = setDateStart + "01";
		$("#search_fr").val(setDateStart);
		var setDateend =  $("#search_to").val();
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","0");
		setDateend = setDateend.substr(8,10);
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-"+setDateend+"d");
	}else{
		dateDisabled(false);
	}
}

function dateDisabled(bool){
	$('.search_fr').attr('disabled', bool);
	$('.search_to').attr('disabled', bool);
	$('.ui-datepicker-trigger').css('visibility', bool == true ? 'hidden' : 'visible');
	$('.ui-datepicker-trigger').css('visibility', bool == true ? 'hidden' : 'visible');
}

// 상세창 클릭
function fnExtrtDetailLog(occr_dt,emp_user_id,emp_detail_seq){
	$("input[name=detailOccrDt]").attr("value",occr_dt);
	$("input[name=detailEmpCd]").attr("value",emp_user_id);
	$("input[name=emp_detail_seq]").attr("value",emp_detail_seq);
	$("input[name=bbs_id]").attr("value","empDetailInq");
	
	//summonDetail.html을 접근할 때 
	// 기존 소명 목록에서 접근할 때와 log리스트에서 페이지를 타고 이동하는 search의 page_num값이 같아서 
	//page가 2이상이되면 로그가 제대로안나오는걸 목록에서 접근할땐 항상 page_num=1로 고정
	/*$("input[name=page_num]").attr("value",1);*/	
	$("#listForm input[name=page_num]").val(1);
	$("#listForm").attr("action",summonConfig["detailUrl"]);
	$("#listForm").submit();
}

function fnSummonManageDetail(occr_dt,emp_user_id,emp_detail_seq,emp_user_name,summon_status,decision_status,decision_status_second){
	$("input[name = occr_dt]").attr("value",occr_dt);
	$("input[name=detailOccrDt]").attr("value",occr_dt);
	$("input[name = detail_emp_user_id]").attr("value",emp_user_id);
	$("input[name = emp_detail_seq]").attr("value",emp_detail_seq);
	$("input[name = detail_summon_status]").attr("value",summon_status);
	$("input[name = detail_decision_status]").attr("value",decision_status);
	$("input[name = detail_decision_status_second]").attr("value",decision_status_second);
	$("input[name = page_num]").attr("value",1);
	
	$("#listForm").attr("action",summonManageConfig["detailUrl"]);
	$("#listForm").submit();

}

function moveSummonManageList(cur_num) {
	$('input[name=page_num]').attr("value","1");
	$('input[name=isSearch]').attr("value","Y");
	$("#listForm").attr("action",summonManageConfig["listUrl"]);
	$("#listForm").submit();
}

function fnSummonListTrClick(rule_cd){
	$('input[name=detailRuleCd]').attr("value",rule_cd);
	var $lstForm = $('#listForm');
	$lstForm.submit();
}

/**
 * 엑셀 다운로드 추가
 */
// 소명 목록 조회 엑셀
function excelSummonList(){
//	if(arguments[0] >= 65535){
//		alert("엑셀다운로드는 65535건까지 가능합니다.");
//	}else{
	
	var $form = $("#listForm");
	var log_message_params = '';
	var menu_id = $('input[name=current_menu_id]').val();
	var log_message_title = '소명요청 및 판정현황 엑셀 다운로드';
	var log_action = 'EXCEL DOWNLOAD';
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
		$("#listForm").attr("action",summonConfig["downloadUrl"]);
		$("#listForm").submit();
//	}
} 

// 소명 요청 상세 조회 엑셀
function excelSummonDetailUpLoad(emp_user_id,emp_user_name){
//	if(arguments[0] >= 65535){
//		alert("엑셀다운로드는 65535건까지 가능합니다.");
//	}else{
	
	var $form = $("#listForm");
	var log_message_params = '';
	var menu_id = $('input[name=current_menu_id]').val();
	var log_message_title = '소명요청 상세화면 및 판정현황 엑셀 다운로드';
	var log_action = 'EXCEL DOWNLOAD';
	$('input[name=emp_user_id]').attr("value",emp_user_id);
	$('input[name=emp_user_name]').attr("value",emp_user_name);
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
		$("#listForm").attr("action",summonConfig["downloadUrl"]);
		$("#listForm").submit();
//	}
} 

function excelSummonManageList(){
//	if(arguments[0] >= 65535){
//		alert("엑셀다운로드는 65535건까지 가능합니다.");
//	}else{
	var $form = $("#listForm");
	var log_message_params = '';
	var menu_id = $('input[name=current_menu_id]').val();
	var log_message_title = '소명관리 엑셀 다운로드';
	var log_action = 'EXCEL DOWNLOAD';
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
		$("#listForm").attr("action",summonManageConfig["downloadUrl"]);
		$("#listForm").submit();
//	}
}

/**
2017.01.18 사용자 개인정보 유형 분석
*/

function fnExtrtEmpDetailInfo(emp_user_id, user_ip, detailOccrDt) {
	
	/*$("input[name=emp_user_id]").attr("value",emp_user_id);
	$("input[name=user_ip]").attr("value",user_ip);*/
	
	$("input[name=userId]").attr("value",emp_user_id);
	$("input[name=user_ip]").attr("value",user_ip);
	$("input[name=detailOccrDt]").attr("value",detailOccrDt);
	
/*	$("input[name=detailOccrDt]").attr("value",occr_dt);
	$("input[name=detailEmpCd]").attr("value",emp_user_id);
	$("input[name=detailEmpDetailSeq]").attr("value",emp_detail_seq);
	$("input[name=bbs_id]").attr("value","empDetailInq");
	$("select[name=rule_cd]").attr("value","");*/
	
	$("#listForm").attr("action",summonConfig["detailChartUrl"]);
	$("#listForm").submit();
}



//판정
var doubleSubmitFlag1 = true;
function addSummonResult(emp_detail_seq){
	$("input[name=emp_detail_seq]").attr("value",emp_detail_seq);
	
	var msg = $("textarea[name=msg]").val();
	if(msg == '') {
		alert("판정의견을 입력해주세요.");
		$('textarea[name=msg]').focus();
		return false;
	}
	var decision_status = $("select[name=decision_status]").val();
	if(decision_status == '') {
		alert("판정을 선택해주세요.");
		return false;
	}
	//소명 부서 판정
		if(decision_status == 20 ){
			sendAjaxSummonResult("re");
		}else if(decision_status == 30 ){
			sendAjaxSummonResult("addResult");
		}else if(decision_status == 40 ){
			sendAjaxSummonResult("addResult");
		}
	//소명 최종 판정
		
	/*if(log_gubun == 30)
		sendAjaxSummonResult("re");
	else */
	/*
	 
	if(doubleSubmitFlag1){
		doubleSubmitFlag1 = false;
		sendAjaxSummonResult("addResult");
	}else{
		return false;
	}
	*/
}

//최종판정(DGB전용) 만든사람: 권창우
function addSummonResultConfirm(emp_detail_seq){
	$("input[name=emp_detail_seq]").attr("value",emp_detail_seq);
	
	var decision_status_second = $("select[name=decision_status_second]").val();
	if(decision_status_second == '') {
		alert("최종판정을 선택해주세요.");
		return false;
	}
	sendAjaxSummonResult("ResultConfirm");
	
}	



//2차판정
var doubleSubmitFlag2 = true;
function addSummonResult2(desc_seq){
	var result2Form = $("#resultForm2");
	result2Form.find("input[name=desc_seq]").val(desc_seq);
	
	var deci_nm = result2Form.find("input[name=result2_user_name]").val();
	if(deci_nm == '') {
		alert("판정자를 입력해주세요.");
		$('input[name=result2_user_name]').focus();
		return false;
	}
	var decision = result2Form.find("textarea[name=result2_body]").val();
	if(decision == '') {
		alert("판정의견을 입력해주세요.");
		result2Form.find("textarea[name=result2_body]").focus();
		return false;
	}
	var log_gubun = result2Form.find("select[name=desc_result2]").val();
	if(log_gubun == '') {
		alert("판정을 선택해주세요.");
		return false;
	}
	
	
	if(log_gubun == '20') {
		var abusePurpose = $("select[name=abuse_purpose]").val();
		var abuseType = $("select[name=abuse_type]").val();
		var faultDegree = $("select[name=fault_degree]").val();
		
		if(abusePurpose == '') {
			alert("오남용 목적을 선택해주세요.");
			return false;
		}
		if(abuseType == '') {
			alert("오남용 유형을 선택해주세요.");
			return false;
		}
		if(faultDegree == '') {
			alert("과실정도를 선택해주세요.");
			return false;
		}
	}
	if(doubleSubmitFlag2){
		doubleSubmitFlag2 = false;
		sendAjaxSummonResult("addReResult");
	}else{
		return false;
	}
	
	
}

//소명초기화
function removeSummonResult(desc_seq){
	var confirmVal = confirm("소명을 초기화하시겠습니까?");
	if(confirmVal){
		$("input[name=desc_seq]").attr("value",desc_seq);
		sendAjaxSummonResult("remove");
	}
}


//소명요청 ajax call
function sendAjaxSummonResult(type){
	var $form;
	if(type == 'addReResult') {
		$form = $("#resultForm2");
	} else {
		$form = $("#resultForm");
	}
	
	var log_message_title = '';
	var log_action = '';
	var log_message_params;
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'addResult') {
		alert("판정");
		log_message_title = '판정';
		log_action = 'INSERT';
		$("input[name=summon_status]").attr("value",'40');
		$("input[name=status]").attr("value",'40');
	} else if(type == 're') {
		alert("재소명");
		log_message_title = '재요청';
		log_action = 'UPDATE';
		$("input[name=summon_status]").attr("value",'20');
		$("input[name=status]").attr("value",'20');
	} else if(type == 'remove') {
		log_message_title = '소명초기화';
		log_action = 'REMOVE';
	} else if(type == 'update') {
		log_message_title = '수정';
		log_action = 'UPDATE';
	} else if(type == 'addReResult') {
		log_message_title = '2차판정';
		log_action = 'UPDATE';
	} else if(type == 'ResultConfirm'){
		alert("최종판정");
		log_message_title ='최종판정';
		log_action = 'UPDATE';
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(summonManageDetailConfig[type + "SummonResultUrl"],$form.serialize(), ajaxAdminUser_successHandler, ajaxAdminUser_errorHandler, type);
}
//관리자 ajax call - 성공
function ajaxAdminUser_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = summonManageDetailConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		switch(actionType){
			case "add":
				if(data.apprMode == "Y") {
					var form = document.getElementById("apprForm");
					var urlSplit = data.reqUrl.split(",");
					$("#apprForm input[name=type]").val(data.type);
					for(var i in urlSplit) {
						var url = urlSplit[i];

						form.action=url;
						form.submit();
					}
					break;
				}
				alert("성공적으로 요청되었습니다.");
				break;
			case "remove":
				alert("성공적으로 초기화되었습니다.");
				break;
			case "addReResult":
				alert("성공적으로 판정되었습니다.");
				break;
			case "update":
				alert("성공적으로 수정되었습니다.");
				break;
			case "addResult":
				alert("성공적으로 판정되었습니다.");
				break;
			case "ResultConfirm":
				alert("성공적으로 최종판정되었습니다.");
				break;
		}
		$("input[name=emp_user_id]").attr("value","");
		moveSummonManageList();
	}
}

// 관리자 ajax call - 실패
function ajaxAdminUser_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	log_message += codeLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("관리자 등록 실패하였습니다." + textStatus);
			break;
	}
}

function apprUpload(desc_seq) {
	$.ajax({
		type: 'POST',
		url: rootPath + '/extrtCondbyInq/findCenterDescApprInfo.html',
		data: { 
			"desc_seq": desc_seq
		},
		success: function(data) {
			data = data.replace(/"/gi,"");
			if(data != null && data != '') {
				var form = document.getElementById("apprForm");
				var url = data;
				form.action=url;
				form.submit();
			} else {
				alert("옵션설정의 결재연동사용유무값을 확인해주세요");
			}
		}
	});
}

//소명알람 미수신
function summonEmailAlramUnused(){

	//소명알람 미수신 체크박스가 체크된 경우
		$("input[name=email_check_val]").val('Y');
		
		var msg =htmlspecialchars($('textarea[name=msg]').val());
		var rule_nm =htmlspecialchars($('input[name=rule_nm]').val());
		var external_staff = htmlspecialchars($('input[name=external_staff]').val());
		
		if(msg == '') {
			alert("내용을 입력해주세요");
			$('textarea[name=msg]').focus();
			return false;
		}

		if(doubleSubmitFlag3){
			doubleSubmitFlag3 = false;
			sendAjaxSummon("add");
		}
		else{
	        return false;
	    }
	
}//END summonEmailAlramUnused()

//소명정보 삭제
function summonRemove(){
	
	var confirmVal = confirm("소명 정보를 삭제하시겠습니까?");
	
	if(confirmVal == true){

		var emp_detail_seq = $("#emp_detail_seq").val();
		
		$.ajax({
			type: 'POST',
			url: rootPath + '/extrtCondbyInq/summonRemove.html',
			data: { 
				"emp_detail_seq" : emp_detail_seq
			},
			success: function(data) {
				alert("소명정보를 삭제했습니다. 목록 화면으로 이동합니다");
				goSummonList();
			},
			error:function(){
				alert("소명정보 삭제에 실패했습니다");
			}
		});
	}
	
	
}


//소명 요청
var doubleSubmitFlag3 = true;
function addSummon(){
	var msg =htmlspecialchars($('textarea[name=msg]').val());
	var rule_nm =htmlspecialchars($('input[name=rule_nm]').val());
	
	
	if(ui_type != 'Shcd'){
		if(msg == '') {
			alert("내용을 입력해주세요");
			$('textarea[name=msg]').focus();
			return false;
		}
	}

	if(doubleSubmitFlag3){
		doubleSubmitFlag3 = false;
		sendAjaxSummon("add");
	}
	else{
        return false;
    }
}


//소명요청 ajax call
function sendAjaxSummon(type){
	var $form = $("#listForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'msg,email_address';
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'add') {
		log_message_title = '소명요청';
		log_action = 'INSERT';
	}
	
	//$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(summonConfig[type + "summonUrl"],$form.serialize(), ajaxAdminUser_successHandler, ajaxAdminUser_errorHandler, type);
}

function moveAdminUserList(){
	$("#adminUserListForm").attr("action",adminUserDetailConfig["listUrl"]);
	$("#adminUserListForm").submit();
}

function fnallCheck(){
	var checkbox = $("input[name^=summonCheck]");
	var count = checkbox.length;
	
	var chkstatus = $('#allCheck').is(":checked");
	if(chkstatus){
		for(var i=0; i<count; i++) {
			var e = $('input[name=summonCheck' + i + ']').is(":disabled");
			$('input[name=summonCheck' + i + ']').prop('checked', !e);
		}
		//$('input[name^=summonCheck]').prop('checked', true);
	}else{
		$('input[name^=summonCheck]').prop('checked', false);
	}
}

function showResultType(log_seq, proc_date, index, system_seq, log_delimiter) {
	var disp = $("#subResultType_" + index).css("display");
	if(disp == "none") {
		$.ajax({
			type: 'POST',
			url: rootPath + '/extrtCondbyInq/getResultType.html',
			data: { 
				"log_seq" : log_seq,
				"proc_date" : proc_date,
				"system_seq": system_seq,
				"log_delimiter": log_delimiter
			},
			success: function(data) {
				drawSubResultType(data.list, index);
			}
		});
	} else {
		$("#subResultType_" + index).hide();
	}
}

function showResultTypeDownload(log_seq, proc_date, index, system_seq) {
	var disp = $("#subResultType_" + index).css("display");
	var log_type = $("#logType").val();
	if(disp == "none") {
		$.ajax({
			type: 'POST',
			url: rootPath + '/extrtCondbyInq/getResultType.html',
			data: { 
				"log_seq" : log_seq,
				"proc_date" : proc_date,
				"system_seq": system_seq,
				"log_delimiter" : log_type
			},
			success: function(data) {
				drawSubResultType(data.list, index);
			}
		});
	} else {
		$("#subResultType_" + index).hide();
	}
}

function drawSubResultType(data, index) {
	
	$("#subResultTable_" + index).find('tbody').empty();
	if(data != null) {
		$.each(data, function( i, item ) {
			
			$("<tr>").appendTo($("#subResultTable_" + index).find('tbody'))
			.append($("<td width='120px' style='text-align: center;'>")
					.text(i + 1)
			)
			.append($("<td style='text-align: center;'>")
					.text(item.result_type)
			)
			.append($("<td style='text-align: center;'>")
					.text(item.result_content)
			)
		});
	}
	
	$("#subResultType_" + index).show();
}
function ComSubmit(opt_formId) {
	this.formId = "";
	if(opt_formId == null)
		this.formId = "listForm";
	else
		this.formId = opt_formId;
	
	this.url = "";
     
    if(this.formId == "listForm"){
        $("#listForm")[0].reset();
    }
     
    this.setUrl = function setUrl(url){
        this.url = url;
    };
     
    this.addParam = function addParam(key, value){
        $("#"+this.formId).append($("<input type='hidden' name='"+key+"' id='"+key+"' value='"+value+"' >"));
    };
     
    this.submit = function submit(){
        var frm = $("#"+this.formId)[0];
        frm.action = this.url;
        frm.method = "post";
        frm.submit();  
    };
}
function fileDownload(fileName, orginName) {
	var comSubmit = new ComSubmit();
	comSubmit.setUrl(rootPath + '/extrtCondbyInq/fileDownload.html');
	comSubmit.addParam("fileName", fileName);
	comSubmit.addParam("orginName", orginName);
	comSubmit.submit();
}

/**
2017.04.17 테이블 정렬 
*/

function sendDesc(check_type, sort_flag){
	var listForm = $("#listForm");
	$("#listForm").attr("action",summonConfig["listUrl"]);	
	listForm.find("input[name=check_type]").val(check_type);
	listForm.find("input[name=sort_flag]").val(sort_flag);
	$("#listForm").submit();
}

function sendDesc2(sort_flag){
	var listForm = $("#listForm");
	$("#listForm").attr("action",summonConfig["listUrl"]);	
	listForm.find("input[name=sort_flag]").val(sort_flag);
	$("#listForm").submit();
}

function selScenario() {
	var scen_seq = $("#scen_seq").val();
	
	$.ajax({
		type:'POST',
		url : rootPath + '/extrtCondbyInq/findScenarioByScenSeq.html',
		data: {
			scen_seq : scen_seq
		},
		success : function(data){
			setRuleTblList(data);
		}
	});
}

function setRuleTblList(data) {
	var select = document.getElementById("rule_cd");
	while (select.options.length) {
		select.remove(0);
    }
	var none = document.createElement("option");
	none.text = "----- 선 택 -----";
	none.value = "";
	select.add(none);
	
	var jsonData = JSON.parse(data);
	for(var i=0; i<jsonData.length; i++) {
		var option = document.createElement("option");
		option.text = jsonData[i].rule_nm;
		option.value = jsonData[i].rule_cd;
		select.add(option);
	}
}

function summonUpdate(desc_seq) {
	$("input[name=desc_seq]").attr("value",desc_seq);
	$("input[name=expect_dt]").attr("value",$("#expect_dt").val());
	sendAjaxSummonResult("update");
}

$(function(){
	var check_type = $('#check_type').val();
	var sort_flag = $('#sort_flag').val();
	var rootPath = $('#rootPath').val();
	var menu_name = "";
	if(menu_name == "소명 요청") {
		if(sort_flag == 1){
			$("#sort_cnt").attr("onclick","javascript:sendDesc2(2)").attr("src", rootPath+"/resources/image/common/asc_btn.png")
			.attr("title","오름차순").attr("alt","오름차순"); 		
		}
	}else {
		if(sort_flag == 1){
			$("#"+ check_type).attr("onclick","javascript:sendDesc('" +check_type+"',2)").attr("src", rootPath+"/resources/image/common/asc_btn.png")
			.attr("title","오름차순").attr("alt","오름차순"); 		
		}
	}
	
	if(menu_name == "소명 요청") {
		$('input[type=text]').attr("onkeydown","javascript:if(event.keyCode==13){movesummonList();}");
	} else {
		$('input[type=text]').attr("onkeydown","javascript:if(event.keyCode==13){moveSummonManageList();}");
	}
});

function showAbuseTr(){
	var descResult = $("select[name=desc_result]").val();
	if(descResult == "20") {
		$("#abuseTr0").css("display","");
		$("#abuseTr1").css("display","");
	} else {
		$("#abuseTr0").css("display","none");
		$("#abuseTr1").css("display","none");
	}
	
}

function showAbuseTr2(){
	var descResult = $("select[name=desc_result2]").val();
	if(descResult == "20") {
		$("#abuseTr2").css("display","");
		$("#abuseTr3").css("display","");
	} else {
		$("#abuseTr2").css("display","none");
		$("#abuseTr3").css("display","none");
	}
	
}

function findReportSummon() {
	
	var search_from = $('#search_fr').val();
	var search_to = $('#search_to').val();
	var dept_name = $('#dept_name').val();
	
	var url = summonManageConfig["repot_summon"];
	
	url = url + "?search_from=" + search_from;
	url = url + "&search_to=" + search_to;
	url = url + "&dept_name=" + dept_name;
	
	window.open(url, 'Popup', 'height=800px, width=800px, scrollbars=yes');
}

function allCheck(checkbox){
	allCheckControl($(checkbox).is(":checked"));
}

//전체 권한 체크박스 컨트롤
function allCheckControl(flag){
	var elems = $(".checkList");
	for(var i=0; i<elems.length; i++) {
		var elem = elems[i];
		elem.checked = flag;
	}
}

function allCheckCount(){
	var bool = $(".checkList").length==$("input:checkbox[name=checkList]:checked").length;
	$("#allCheck").prop("checked",bool);
}

function batchSummon(){
	var summonSeqList="";
	$("input:checkbox[name=checkList]:checked").each(function(idx,val){
		summonSeqList += $(val).val()+",";
	});
	if(summonSeqList.length>0){
		$('.popup_wrap').css('display','none');
		$('#loading').css('display','block');
		summonSeqList = summonSeqList.substr(0,summonSeqList.length-1);
		$.ajax({
			type:'POST',
			url : rootPath + '/extrtCondbyInq/summonListAdd.html',
			data: {
				emp_detail_seq : summonSeqList
				,description : $('#description').val()
				,desc_status : $('#desc_status').val()
			},
			success : function(data){
				var jsonData = JSON.parse(data);
				alert(jsonData.success+"건 소명 요청이 완료 되었습니다. ("+jsonData.fail+"건 소명 요청 실패)");
				location.reload(true);
			},
		})
	}else{
		alert("하나 이상의 소명건을 선택해 주세요.");
	}
}

function popupClose(){
	$('.popup_wrap').css('display','none');
}

function popupView(){
	$('.popup_wrap').css('display','');
}

//상세화면에서 해당 biz_log_result 화면으로 이동 ( 접속기록 )
function detailResultLogView(log_seq, occr_dt) {
	$("#moveForm input[name=detailProcDate]").val(occr_dt);
	$("#moveForm input[name=detailLogSeq]").val(log_seq);
	
	var log_delimiter = $("input[name=log_delimiter]").val();
	
	if(log_delimiter=='BA'){
		$("#moveForm").attr("action",allLogInqDetailConfig["detailUrl"]);
	} else if(log_delimiter=='DN'){
		$("#moveForm").attr("action",allLogInqDetailConfig["downloadDetailUrl"]);
	}
	$("#moveForm").submit();
}

function excelSummonUpLoad(){
	
	var data = $("#result").val();
	
		var options = {
				
				success : function(data) {
					var checkdata = "\"true\"";
					if(checkdata == data){
						alert("성공적으로 업로드 되었습니다!");
						moveSummonManageList();
					}else{
						if (result != "true") {
							alert(data.replace("\"", "").replace(",", "\n").replace(",", "\n").replace("\"", ""));
						}
					}
					
				},
				error : function(error) {
					alert("요청 처리 중 오류가 발생하였습니다.");
				}
		  };
		    
    $("#fileForm").ajaxSubmit(options);

    return false;
}

function exDown(){
	$("#exdown").submit();
}
//소명 관리 목록리스트 응답내용 상세보기

