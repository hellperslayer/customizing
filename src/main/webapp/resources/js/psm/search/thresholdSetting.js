
$(document).ready(function(){
	//임계치 설정 사용 여부를 통한 설정 버튼 제어
	//	- 사용 시, setting row 출력, 미사용 시 setting row 감추기
	
	if($("select[name=system_seq]").val()==''){
		$("input:radio[name=threshold_use]").attr("disabled", "disabled");
		$("input:radio[name=threshold]").attr("disabled", "disabled");
	}
	
	if($("input:radio[name=threshold_use]:checked").val() == 'N'){
		$("input:radio[name=threshold]").attr("disabled", "disabled");
		
	}else if($("input:radio[name=threshold_use]:checked").val() == 'Y'){
		$("input:radio[name=threshold]").removeAttr("disabled");
		
	}
	$("input:radio[name=threshold_use]").click(function(){
		if($("input:radio[name=threshold_use]:checked").val() == 'N'){
			$("input:radio[name=threshold]").attr("disabled", "disabled");
			$("input:text[name=threshold_total_inbox]").attr("readonly","readonly");
		}else if($("input:radio[name=threshold_use]:checked").val() == 'Y'){
			$("input:radio[name=threshold]").removeAttr("disabled");
			$("input:text[name=threshold_total_inbox]").removeAttr("readonly");
		}
	});
	
	if($("input:radio[name=threshold]:checked").val() == 'etc'){
		$("#threshold_input_box").css("display", "inline-block");
	}else{
		$("#threshold_input_box").css("display", "none");
	}
	
	$("input:radio[name=threshold]").click(function(){
		if($("input:radio[name=threshold]:checked").val() == 'etc'){
			$("#threshold_input_box").css("display", "inline-block");
		}else{
			$("#threshold_input_box").css("display", "none");
		}
	});
	
});


/**
 * 파일 확장자 예외 설정 Javascript
 */
function fnExceptionExtSetupDetail(seq){
	$("input[name=seq]").attr("value",seq);
	$("#listForm").attr("action",exceptionExtSetupConfig["detailUrl"]);
	$("#listForm").submit();
}

// 페이지 이동
function goPage(num){
	$("#thresholdDetailForm").attr("action",thresholdSettingConfig["listUrl"]);
	$("#thresholdDetailForm input[name=page_num]").val(num);
	$("#thresholdDetailForm").submit();
}


function changeThresholdSystem(){
	$("#thresholdDetailForm").attr("action",thresholdSettingConfig["listUrl"]);
	$('input[name=type]').attr("value","list");
	$("#thresholdDetailForm").submit();
}

function updateThresholdSetting(){
	
	if($("input[name=threshold_use]:checked").val() == 'Y'){
		if($("input[name=threshold]:checked").val() == null || $("input[name=threshold]:checked").val() == ''){
			alert("시스템 임계치를 설정하여주세요");
			return false;
		}
		
		if($("input[name=threshold_use]:checked").val() == 'etc'){
			if($("input[name=threshold_total_inbox]").val() == null || $("input[name=threshold_total_inbox]").val() == '' || $("input[name=threshold_total_inbox]").val() == '0'){
				alert("시스템 임계치 설정 값을 입력해주세요");
				$("#threshold_total_inbox").focus();
				return false;
			}
		}
		
		if($("input[name=threshold_total_inbox]").val() % 1 != 0 ){
			alert("시스템 임계치 값은 소수값을 가질 수 없습니다.");
			$("#threshold_total_inbox").focus();
			return false;
		}
	}
	
	if(confirm("개인정보 검출 임계치를 수정하시겠습니까?")){
		$.ajax({
			type:'POST',
			url: thresholdSettingConfig["saveUrl"],
			data: $("#thresholdDetailForm").serialize(),
			success: function(data){
				if(data.result == 1){
					alert("개인정보 검출 임계치 저장에 성공하였습니다.");
					changeThresholdSystem();
				}else{
					alert("개인정보 검출 임계치 저장에 실패했습니다.");
					return false;
				}
			},
			error: function(a,b,c){
				alert("임계치 설정에 오류가 발생하였습니다.");
				console.log(a,b,c);
			}
		});
	}
}

//input 태그 -> 숫자만 입력 하도록
function checkNumber(){
	var threshold_total_inbox = $("#threshold_total_inbox").val().trim();
	if(isNaN(threshold_total_inbox)){
		alert("임계치는 숫자만 입력가능합니다.");
		$("#threshold_total_inbox").val("");
		return false;
	}
	if(threshold_total_inbox < 0){
		alert("임계치는 음수값으로 설정할 수 없습니다.");
		$("#threshold_total_inbox").val("");
		return false;
	}
}