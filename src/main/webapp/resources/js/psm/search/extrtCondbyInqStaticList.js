// 검색
function moveextrtCondbyInqList() {
	$('input[name=page_num]').attr("value","1");
	$("#listForm").attr("action",selectConfig["listUrl"]);
	$("#listForm").submit();
}

// 기간별 위험분석 - 기간 선택, 일별, 월별, 연도별 (chk_date_type)
var moveStatistics = function() {
	var listForm = $("#listForm");
	var tDate = listForm.find("input[name=search_to]").val();
	var fDate = listForm.find("input[name=search_from]").val();
	if(tDate<fDate){
		alert("검색 기간을 확인하세요");
		return;
	}
	$("#listForm").find("input[name=sort_flag]").val(0);
	if (arguments.length < 1) {
		searchList();
	} else {
		$('input[name=chk_date_type]').val(arguments[0]);	
		searchList();
	}
}

// 엑셀 파일 export
function excelExtrtCondbyInqList() {
	$("#listForm").attr("action",selectConfig["downloadUrl"]);
	$("#listForm").submit();
}



// 리스트 화면으로 이동 ( 기간별 위험분석일 때 )
function moveExtrtListByDate(proc_date, scen_seq) {
	var system_seq = $("#listForm select[name=system_seq]").val();
	
	$("#moveForm").attr("action",selectConfig["detailUrl"]);
	$("#moveForm input[name=search_from]").val(proc_date);
	$("#moveForm input[name=search_to]").val(proc_date);
	$("#moveForm input[name=system_seq]").val(system_seq);
	$("#moveForm input[name=scen_seq]").val(scen_seq);
	$("#moveForm").submit();
}
// 리스트 화면으로 이동 ( 기간별 위험분석이 아닐 때 )
function moveExtrtList(emp_user_id, dept_id, system_seq, scen_seq) {
	var search_from = $("#listForm input[name=search_from]").val();
	var search_to = $("#listForm input[name=search_to]").val();
	if(system_seq == null || system_seq == '') {
		system_seq = $("#listForm select[name=system_seq]").val();
	}
	
	$("#moveForm").attr("action",selectConfig["detailUrl"]);
	$("#moveForm input[name=search_from]").val(search_from);
	$("#moveForm input[name=search_to]").val(search_to);
	$("#moveForm input[name=emp_user_id]").val(emp_user_id);
	$("#moveForm input[name=dept_id]").val(dept_id);
	$("#moveForm input[name=system_seq]").val(system_seq);
	$("#moveForm input[name=scen_seq]").val(scen_seq);
	$("#moveForm").submit();
}