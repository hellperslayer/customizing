/**
 * 추출조건별조회 Javascript
 * @author tjlee
 * @since 2014. 4. 10
 */
	 	
 // 상세로 이동

$(document).ready(function() {
	
	$('.left_area').css('display', 'none');
});

function fnextrtCondbyInqDetail(log_seq,proc_date){
	$("input[name=detailEmpCd]").attr("value",log_seq);
	$("input[name=detailOccrDt]").attr("value",proc_date);
	
	$("#extrtCondbyInqDetailForm").attr("action",extrtCondbyInqConfig["detailUrl"]);
	$("#extrtCondbyInqDetailForm").submit();
}


// 페이지 이동
function goPage(num){
	$("#listForm").attr("action",extrtCondbyInqConfig["listUrl"]);
	$("#listForm input[name=page_num]").val(num);
	$("#listForm").submit();
}

// 상세페이지 페이징 
function goPageextrtCondbyInqDetail(num){
	$("#listForm").attr("action",extrtCondbyInqConfig["detailUrl"]);
	$("#listForm input[name=extrtCondbyInqDetail_page_num]").val(num);
	$("#listForm").submit();
}

// 검색
function moveextrtCondbyInqList() {
	$('input[name=page_num]').attr("value","1");
	$('input[name=isSearch]').attr("value","Y");
	$("#listForm").attr("action",extrtCondbyInqConfig["listUrl"]);
	$("#listForm").submit();
}

// 목록으로 (추출조건별조회화면)
function goExtrtCondbyInqList() {
	$("#listForm").attr("action",extrtCondbyInqConfig["goExtrtCondbyInqListUrl"]);
	$("#listForm").submit();
}

// 목록으로 (위험도별조회상세화면)
function goEmpDetailInqList() {
	
	//메뉴명위험도별조회상세로 변경
	$('input[name=main_menu_id]').attr("value","MENU00050");
	$('input[name=sub_menu_id]').attr("value","MENU00047");
	
	$("#listForm").attr("action",extrtCondbyInqConfig["goEmpDetailInqListUrl"]);
	$("#listForm").submit();
}

// 목록으로 (소명요청화면)
function goCallingDemandList() {
	//메뉴명위험도별조회상세로 변경
	$('input[name=main_menu_id]').attr("value","MENU00060");
	$('input[name=sub_menu_id]').attr("value","MENU00067");
	
	$("#listForm").attr("action",extrtCondbyInqConfig["goCallingDemandListUrl"]);
	$("#listForm").submit();
}



// 기간선택 검색조건
function initDaySelect(){
	if($("#daySelect").val() == "Day"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "WeekDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-7d");
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "MonthDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-1m");
		var setDateStart = $("#search_fr").val();
		setDateStart = setDateStart.substr(0,8);
		setDateStart = setDateStart + "01";
		$("#search_fr").val(setDateStart);
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "YearDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
		var setDateStart = $("#search_fr").val();
		setDateStart = setDateStart.substr(0,5);
		setDateStart = setDateStart + "01-01";
		$("#search_fr").val(setDateStart);
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "TotalMonthDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-1m");
		var setDateStart = $("#search_fr").val();
		setDateStart = setDateStart.substr(0,8);
		setDateStart = setDateStart + "01";
		$("#search_fr").val(setDateStart);
		var setDateend =  $("#search_to").val();
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","0");
		setDateend = setDateend.substr(8,10);
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-"+setDateend+"d");
	}else{
		dateDisabled(false);
	}
}

/*function initAdverseSelect(){
	if($("#use_adverse").val() == ""){
		$("#use_adverse").val() == "";
	}else if($("#use_adverse").val() == "Y"){
		$("#use_adverse").val() == "Y"
	}
}*/

function dateDisabled(bool){
	$('.search_fr').attr('disabled', bool);
	$('.search_to').attr('disabled', bool);
	$('.ui-datepicker-trigger').css('visibility', bool == true ? 'hidden' : 'visible');
	$('.ui-datepicker-trigger').css('visibility', bool == true ? 'hidden' : 'visible');
}

// 상세창 클릭
function fnExtrtDetailLog(occr_dt,emp_user_id,emp_detail_seq,rule_cd){
	$("input[name=detailOccrDt]").attr("value",occr_dt);
	$("input[name=detailEmpCd]").attr("value",emp_user_id);
	$("input[name=detailEmpDetailSeq]").attr("value",emp_detail_seq);
	$("input[name=bbs_id]").attr("value","empDetailInq");
	$("select[name=rule_cd]").attr("value","");
	
	$("#listForm").attr("action",extrtCondbyInqConfig["detailUrl"]);
	$("#listForm").submit();
}

function fnExtrtCondbyInqListTrClick(rule_cd){
	$('input[name=detailRuleCd]').attr("value",rule_cd);
	var $lstForm = $('#listForm');
	$lstForm.submit();
}

/**
 * 엑셀 다운로드 추가 2015/6/4
 */
function excelExtrtCondbyInqList(){
//	if(arguments[0] >= 65535){
//		alert("엑셀다운로드는 65535건까지 가능합니다.");
//	}else{
		
		var $form = $("#listForm");
		var log_message_params = '';
		var menu_id = $('input[name=current_menu_id]').val();
		var log_message_title = '비정상위험분석리스트 엑셀 다운로드';
		var log_action = 'EXCEL DOWNLOAD';
		
		$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
		
		$("#listForm").attr("action",extrtCondbyInqConfig["downloadUrl"]);
		$("#listForm").submit();
//	}
}

/**
2017.01.18 사용자 개인정보 유형 분석
*/

function fnExtrtEmpDetailInfo(emp_user_id, detailOccrDt) {
	//$("input[name=emp_user_name]").attr("value",emp_user_name);
	$("input[name=userId]").attr("value",emp_user_id);
	$("input[name=detailOccrDt]").attr("value",detailOccrDt);
/*	$("input[name=detailOccrDt]").attr("value",occr_dt);
	$("input[name=detailEmpCd]").attr("value",emp_user_id);
	$("input[name=detailEmpDetailSeq]").attr("value",emp_detail_seq);
	$("input[name=bbs_id]").attr("value","empDetailInq");
	$("select[name=rule_cd]").attr("value","");*/
	
	$("#listForm").attr("action",extrtCondbyInqConfig["detailChartUrl"]);
	$("#listForm").submit();
}

function fnExtrtEmpDetailInfo2(detailOccrDt, emp_user_id, dept_id) {
	
	parent.$("input[name=detailOccrDt]").attr("value",detailOccrDt);
	parent.$("input[name=userId]").attr("value",emp_user_id);
	parent.$("input[name=dept_id]").attr("value",dept_id);
	parent.$("#listForm").attr("action","detailChart.html");
	parent.$("#listForm").submit();
}

/**
2017.06.19 부서별 비정상위험분석 상세차트
*/

function fnExtrtDeptDetailInfo(dept_name,dept_id) {

	$("input[name=dept_name]").attr("value",dept_name);
	$("input[name=dept_id]").attr("value",dept_id);
	$("#deptForm").attr("action",extrtCondbyInqConfig["deptDetailChartUrl"]);
	$("#deptForm").submit();
}

function fnExtrtUserDetailInfo(emp_user_name, emp_user_id) {

	$("input[name=emp_user_name]").attr("value", emp_user_name);
	$("input[name=userId]").attr("value", emp_user_id);
	$("#userForm").attr("action",extrtCondbyInqConfig["detailChartUrl"]);
	$("#userForm").submit();
}

function fnExtrtDeptDetailInfo2(dept_name,dept_id) {

	parent.$("input[name=dept_name]").attr("value",dept_name);
	parent.$("input[name=dept_id]").attr("value",dept_id);
	parent.$("#listForm").attr("action","deptDetailChart.html");
	parent.$("#listForm").submit();
}

function fnExtrtDeptDetailInfo3(dept_id, detailOccrDt, system_seq) {

	//$("input[name=dept_name]").attr("value",dept_name);
	$("input[name=dept_id]").attr("value",dept_id);
	$("input[name=detailOccrDt]").attr("value",detailOccrDt);
	$("input[name=detailSystemSeq]").attr("value",system_seq);
	$("#listForm").attr("action",extrtCondbyInqConfig["deptDetailChartUrl"]);
	$("#listForm").submit();
}

/**
2017.04.17 테이블 정렬 
*/

function sendDesc(check_type, sort_flag){
	var listForm = $("#listForm");
	$("#listForm").attr("action",extrtCondbyInqConfig["listUrl"]);	
	listForm.find("input[name=check_type]").val(check_type);
	listForm.find("input[name=sort_flag]").val(sort_flag);
	$("#listForm").submit();
}

function selScenario() {
	var scen_seq = $("#scen_seq").val();
	
	$.ajax({
		type:'POST',
		url : rootPath + '/extrtCondbyInq/findScenarioByScenSeq.html',
		data: {
			scen_seq : scen_seq
		},
		success : function(data){
			setRuleTblList(data);
		}
	});
}

function setRuleTblList(data) {
	var select = document.getElementById("rule_cd");
	while (select.options.length) {
		select.remove(0);
    }
	var none = document.createElement("option");
	none.text = "----- 선 택 -----";
	none.value = "";
	select.add(none);
	
	var jsonData = JSON.parse(data);
	for(var i=0; i<jsonData.length; i++) {
		var option = document.createElement("option");
		option.text = jsonData[i].rule_nm;
		option.value = jsonData[i].rule_cd;
		select.add(option);
	}
}

function saveFollowUp() {
	var followup = $('textarea[name=followup]').val();
	if(followup == '') {
		alert("조치 내용을 입력해주세요");
		$('textarea[name=followup]').focus();
		return false;
	}
	
	var confirmVal = confirm("저장하시겠습니까?");
	if(confirmVal){
		var $form = $("#listForm");
		var log_action = 'INSERT';
		var log_message_params = '';
		var menu_id = $('input[name=current_menu_id]').val();
		var log_message_title = '비정상위험분석 후속조치 저장';
		var type = "add";
		
		$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
		sendAjaxPostRequest(extrtCondbyInqConfig["saveFollowUpUrl"],$form.serialize(), ajaxFollowUp_successHandler, ajaxFollowUp_errorHandler, type);
	}else{
		return false;
	}
}

function ajaxFollowUp_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = extrtCondbyInqConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		alert("성공적으로 저장되었습니다.");
		
		window.location.reload();
	}
}

function ajaxFollowUp_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	alert("비정상위험분석 후속조치 저장 실패하였습니다." + textStatus);
}

$(function(){
	$('input[type=text]').attr("onkeydown","javascript:if(event.keyCode==13){moveextrtCondbyInqList();}");
	
	var check_type = $('#check_type').val();
	var sort_flag = $('#sort_flag').val();
	var rootPath = $('#rootPath').val();
	if(sort_flag == 1){
		$("#"+ check_type).attr("onclick","javascript:sendDesc('" +check_type+"',2)").attr("src", rootPath+"/resources/image/common/asc_btn.png")
		.attr("title","오름차순").attr("alt","오름차순"); 		
	}
	
	$('#search_fr').attr("onclick", "resetPeriod()");
	$('#search_to').attr("onclick", "resetPeriod()");
	
});

//부서 상세차트로 이동
function fnExtrtDeptDetailChart(dept_id, detailOccrDt) {
	$("#detailchartForm input[name=dept_id]").attr("value",dept_id);
	$("#detailchartForm input[name=detailOccrDt]").attr("value",detailOccrDt);
	var url = rootPath + "/extrtCondbyInq/deptDetailChart.html";
	$("#detailchartForm").attr("action", url);
	$("#detailchartForm").submit();
}


// ------------------
// 걸린 시나리오 상세 화면으로 이동
function scenarioDetail(occr_dt,emp_user_id,emp_detail_seq,rule_cd){
	$("#moveForm input[name=occr_dt]").attr("value",occr_dt);
	$("#moveForm input[name=detailOccrDt]").attr("value",occr_dt);
	$("#moveForm input[name=emp_user_id]").attr("value",emp_user_id);
	$("#moveForm input[name=emp_detail_seq]").attr("value",emp_detail_seq);
	/*$("input[name=bbs_id]").attr("value","empDetailInq");*/
	$("#moveForm input[name=rule_cd]").attr("value",rule_cd);
	
	$("#moveForm").attr("action",extrtCondbyInqConfig["detailUrl"]);
	$("#moveForm").submit();
}
// 상세화면에서 해당 biz_log_result 화면으로 이동 ( 접속기록 )
function detailResultLogView(log_seq, occr_dt) {
	$("#moveForm input[name=detailProcDate]").val(occr_dt);
	$("#moveForm input[name=detailLogSeq]").val(log_seq);
	
	var log_delimiter = $("input[name=log_delimiter]").val();
	
	if(log_delimiter=='BA'){
		$("#moveForm").attr("action",allLogInqDetailConfig["detailUrl"]);
	} else if(log_delimiter=='DN'){
		$("#moveForm").attr("action",allLogInqDetailConfig["downloadDetailUrl"]);
	}
	$("#moveForm").submit();
}
/*
// 상세화면에서 해당 download_log_result 화면으로 이동 ( 다운로드 )
function fnAllLogInqDetail(log_seq,proc_date,proc_time,emp_user_name,user_id,result_type,log_delimiter){
	$("input[name=detailLogSeq]").attr("value",log_seq);
	$("input[name=detailProcDate]").attr("value",proc_date);
	$("input[name=detailProcTime]").attr("value",proc_time);
	$("input[name=user_name]").attr("value",emp_user_name);
	$("input[name=result_type]").attr("value",result_type);
	$("input[name=user_id]").attr("value",user_id);
	
	if($("input[name=bbs_id]").val() == ''){
		$("input[name=bbs_id]").attr("value","extrtCondbyInq");
	}
	
	$("#listForm").attr("action",allLogInqDetailConfig["downloadDetailUrl"]);
	$("#listForm").submit();
}*/


//20201222 hbjang 명지전문대 비정상위험분석 보고서 커스터마이징
function showExtrtReport(emp_detail_seq, occr_dt) {
	var url = extrtCondbyInqConfig["extrtReportUrl"]+"?emp_detail_seq="+emp_detail_seq+"&occr_dt="+occr_dt;
	window.open(url, 'Popup', 'height=800px, width=1000px, scrollbars=yes, left=0, top=0');
}

//20210312 hbjang 명지전문대 비정상위험분석 엑셀 커스터마이징
function excelExtrtReport(){
	var $form = $("#listForm");
	var log_message_params = '';
	var menu_id = $('input[name=current_menu_id]').val();
	var log_message_title = '비정상위험분석리스트 엑셀 다운로드';
	var log_action = 'EXCEL DOWNLOAD';
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	$('#loading').show();
	$("#listForm").attr("action",extrtCondbyInqConfig["extrtExcelUrl"]);
	$("#listForm").submit();
	$('#loading').hide();
}