/**
 * 추출조건별조회 Javascript
 * @author tjlee
 * @since 2014. 4. 10
 */
	 	
 // 상세로 이동

$(document).ready(function() {
	
	$('.left_area').css('display', 'none');
});


// 페이지 이동
function goPage(num){
	$("#listForm").attr("action",summonConfig["listUrl"]);
	$("#listForm input[name=page_num]").val(num);
	$("#listForm").submit();
}

// 검색
function movesummonList() {
	$('input[name=page_num]').attr("value","1");
	$("#listForm").attr("action",summonConfig["listUrl"]);
	$("#listForm").submit();
}

function moveSummonIndvby(emp_user_id,search_from,search_to) {
	$("input[name=search_from]").attr("value",search_from);
	$("input[name=search_to]").attr("value",search_to);
	$("input[name=emp_user_id]").attr("value",emp_user_id);
	$('input[name=page_num]').attr("value","1");
	$("#listForm").attr("action",summonConfig["moveSummoList"]);
	$("#listForm").submit();
}

function moveSummonDeptby(dept_name,search_from,search_to) {
	$("input[name=search_from]").attr("value",search_from);
	$("input[name=search_to]").attr("value",search_to);
	$("input[name=dept_name]").attr("value",dept_name);
	$('input[name=page_num]').attr("value","1");
	$("#listForm").attr("action",summonConfig["moveSummoList"]);
	$("#listForm").submit();
}

// 목록으로 (소명조회화면)
function goSummonList() {
	$("#listForm").attr("action",summonConfig["goSummonListUrl"]);
	$("#listForm").submit();
}

// 목록으로 (위험도별조회상세화면)
function goEmpDetailInqList() {
	
	//메뉴명위험도별조회상세로 변경
	$('input[name=main_menu_id]').attr("value","MENU00050");
	$('input[name=sub_menu_id]').attr("value","MENU00047");
	
	$("#listForm").attr("action",summonConfig["goEmpDetailInqListUrl"]);
	$("#listForm").submit();
}

// 목록으로 (소명요청화면)
function goCallingDemandList() {
	//메뉴명위험도별조회상세로 변경
	$('input[name=main_menu_id]').attr("value","MENU00060");
	$('input[name=sub_menu_id]').attr("value","MENU00067");
	
	$("#listForm").attr("action",summonConfig["goCallingDemandListUrl"]);
	$("#listForm").submit();
}



function dateDisabled(bool){
	$('.search_fr').attr('disabled', bool);
	$('.search_to').attr('disabled', bool);
	$('.ui-datepicker-trigger').css('visibility', bool == true ? 'hidden' : 'visible');
	$('.ui-datepicker-trigger').css('visibility', bool == true ? 'hidden' : 'visible');
}

// 상세창 클릭
function fnExtrtDetailLog(occr_dt,emp_user_id,emp_detail_seq){
	$("input[name=detailOccrDt]").attr("value",occr_dt);
	$("input[name=detailEmpCd]").attr("value",emp_user_id);
	$("input[name=detailEmpDetailSeq]").attr("value",emp_detail_seq);
	$("input[name=bbs_id]").attr("value","empDetailInq");	
	$("#listForm").attr("action",summonConfig["detailUrl"]);
	$("#listForm").submit();
}

function fnSummonManageDetail(occr_dt,emp_user_id,log_seq,desc_seq,emp_user_name,emp_detail_seq){
	$("input[name=detailOccrDt]").attr("value",occr_dt);
	$("input[name=detailEmpCd]").attr("value",emp_user_id);
	$("input[name=desc_seq]").attr("value",desc_seq);
	$("input[name=logSeqs]").attr("value",log_seq);
	$("input[name=user_name]").attr("value",emp_user_name);
	$("input[name=detailEmpDetailSeq]").attr("value",emp_detail_seq);
	
	$("#listForm").attr("action",summonManageConfig["detailUrl"]);
	$("#listForm").submit();

}

function moveSummonManageList() {
	$('input[name=page_num]').attr("value","1");
	$('input[name=isSearch]').attr("value","Y");
	$("#listForm").attr("action",summonManageConfig["listUrl"]);
	$("#listForm").submit();
}

function fnSummonListTrClick(rule_cd){
	$('input[name=detailRuleCd]').attr("value",rule_cd);
	var $lstForm = $('#listForm');
	$lstForm.submit();
}

/**
 * 엑셀 다운로드 추가 2015/6/4
 */
function excelSummonList(){
//	if(arguments[0] >= 65535){
//		alert("엑셀다운로드는 65535건까지 가능합니다.");
//	}else{
	
	var $form = $("#listForm");
	var log_message_params = '';
	var menu_id = $('input[name=current_menu_id]').val();
	var log_message_title = '개인별 소명 현황 엑셀 다운로드';
	var log_action = 'EXCEL DOWNLOAD';
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
		$("#listForm").attr("action",summonConfig["downloadUrl"]);
		$("#listForm").submit();
//	}
}

function excelSummonDetail(){
//	if(arguments[0] >= 65535){
//		alert("엑셀다운로드는 65535건까지 가능합니다.");
//	}else{
		$("#listForm").attr("action",summonConfig["downloadDetailUrl"]);
		$("#listForm").submit();
//	}
}

/**
2017.01.18 사용자 개인정보 유형 분석
*/

function fnExtrtEmpDetailInfo(emp_user_name,emp_user_id, user_ip) {
	
	$("input[name=emp_user_id]").attr("value",emp_user_id);
	$("input[name=user_ip]").attr("value",user_ip);
	
/*	$("input[name=detailOccrDt]").attr("value",occr_dt);
	$("input[name=detailEmpCd]").attr("value",emp_user_id);
	$("input[name=detailEmpDetailSeq]").attr("value",emp_detail_seq);
	$("input[name=bbs_id]").attr("value","empDetailInq");
	$("select[name=rule_cd]").attr("value","");*/
	
	$("#listForm").attr("action",summonConfig["detailChartUrl"]);
	$("#listForm").submit();
}



//판정
function addSummonResult(desc_seq){
	$("input[name=desc_seq]").attr("value",desc_seq);
	
	var deci_nm = $("input[name=deci_nm]").val();
	if(deci_nm == '') {
		alert("승인권자를 입력해주세요.");
		$('input[name=deci_nm]').focus();
		return false;
	}
	var decision = $("textarea[name=decision]").val();
	if(decision == '') {
		alert("검토의견을 입력해주세요.");
		$('textarea[name=decision]').focus();
		return false;
	}
	var log_gubun = $("select[name=desc_result]").val();
	if(log_gubun == '') {
		alert("승인을 선택해주세요.");
		return false;
	}
	sendAjaxSummonResult("add");
}


//소명요청 ajax call
function sendAjaxSummonResult(type){
	var $form = $("#resultForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'deci_nm,log_gubun';
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'add') {
		log_message_title = '승인요청';
		log_action = 'INSERT';
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(summonManageDetailConfig[type + "SummonResultUrl"],$form.serialize(), ajaxAdminUser_successHandler, ajaxAdminUser_errorHandler, type);
}


//관리자 ajax call - 성공
function ajaxAdminUser_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = summonManageDetailConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
		}
		
		//$("input[name=rule_cd]").attr("value","");
		$("input[name=emp_user_id]").attr("value","");
		moveSummonManageList();
	}
}


// 관리자 ajax call - 실패
function ajaxAdminUser_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	log_message += codeLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("관리자 등록 실패하였습니다." + textStatus);
			break;
	}
}

//소명 요청
function addSummon(){
	var checkbox = $("input[type=checkbox]");
	var count = checkbox.length;
	var res = "";
	for (var i = 0; i < count; i++) {
		var checkVal = $("input:checkbox[name=summonCheck" + i + "]:checked").val();
		
		if(checkVal != 'undefined' && checkVal != null) {
			if(res == "") {
				res += checkVal;
				$('input[name=seq]').val(checkVal);
				
			}else {
				res += "," + checkVal;
			}
		}
	}

	if(res == "") {
		alert("하나 이상을 선택하셔야합니다.");
		return;
	}
	
	$('input[name=logSeqs]').val(res);
//	var cnt = $('input:checkbox[name=auth_ids]:checked').length;
	var description1 =htmlspecialchars($('textarea[name=description1]').val());
	var email_address =htmlspecialchars($('input[name=email_address]').val());
	//var description2 =htmlspecialchars($('textarea[name=description2]').val());
	var rule_nm =htmlspecialchars($('input[name=rule_nm]').val());
	/*if(description1 == '') {
		alert("소명요청사유를 입력해주세요");
		$('textarea[name=description1]').focus();
		return false;
	}*/
	if(email_address == '') {
		alert("E-MAIL을 입력해주세요");
		$('input[name=email_address]').focus();
		return false;
	}
	if(description1 == '') {
		alert("내용을 입력해주세요");
		$('textarea[name=description1]').focus();
		return false;
	}
	sendAjaxSummon("add");
}


//소명요청 ajax call
function sendAjaxSummon(type){
	var $form = $("#listForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'description1,email_address';
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'add') {
		log_message_title = '소명요청';
		log_action = 'INSERT';
	}
	
	//$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(summonConfig[type + "summonUrl"],$form.serialize(), ajaxAdminUser_successHandler, ajaxAdminUser_errorHandler, type);
}

function fnallCheck(){
	var checkbox = $("input[name^=summonCheck]");
	var count = checkbox.length;
	
	var chkstatus = $('#allCheck').is(":checked");
	if(chkstatus){
		for(var i=0; i<count; i++) {
			var e = $('input[name=summonCheck' + i + ']').is(":disabled");
			$('input[name=summonCheck' + i + ']').prop('checked', !e);
		}
		//$('input[name^=summonCheck]').prop('checked', true);
	}else{
		$('input[name^=summonCheck]').prop('checked', false);
	}
}

function showResultType(log_seq, proc_date, index, system_seq) {
	var disp = $("#subResultType_" + index).css("display");
	if(disp == "none") {
		$.ajax({
			type: 'POST',
			url: rootPath + '/extrtCondbyInq/getResultType.html',
			data: { 
				"log_seq" : log_seq,
				"proc_date" : proc_date,
				"system_seq": system_seq
			},
			success: function(data) {
				drawSubResultType(data.list, index);
			}
		});
	} else {
		$("#subResultType_" + index).hide();
	}
}

function drawSubResultType(data, index) {
	
	$("#subResultTable_" + index).find('tbody').empty();
	if(data != null) {
		$.each(data, function( i, item ) {
			
			$("<tr>").appendTo($("#subResultTable_" + index).find('tbody'))
			.append($("<td width='120px' style='text-align: center;'>")
					.text(i + 1)
			)
			.append($("<td style='text-align: center;'>")
					.text(item.result_type)
			)
			.append($("<td style='text-align: center;'>")
					.text(item.result_content)
			)
		});
	}
	
	$("#subResultType_" + index).show();
}
function ComSubmit(opt_formId) {
	this.formId = "";
	if(opt_formId == null)
		this.formId = "listForm";
	else
		this.formId = opt_formId;
	
	this.url = "";
     
    if(this.formId == "listForm"){
        $("#listForm")[0].reset();
    }
     
    this.setUrl = function setUrl(url){
        this.url = url;
    };
     
    this.addParam = function addParam(key, value){
        $("#"+this.formId).append($("<input type='hidden' name='"+key+"' id='"+key+"' value='"+value+"' >"));
    };
     
    this.submit = function submit(){
        var frm = $("#"+this.formId)[0];
        frm.action = this.url;
        frm.method = "post";
        frm.submit();  
    };
}
function fileDownload(fileName, orginName) {
	var comSubmit = new ComSubmit();
	comSubmit.setUrl(rootPath + '/extrtCondbyInq/fileDownload.html');
	comSubmit.addParam("fileName", fileName);
	comSubmit.addParam("orginName", orginName);
	comSubmit.submit();
}

/**
2017.04.17 테이블 정렬 
*/

function sendDesc(check_type, sort_flag){
	var listForm = $("#listForm");
	$("#listForm").attr("action",summonConfig["listUrl"]);	
	listForm.find("input[name=check_type]").val(check_type);
	listForm.find("input[name=sort_flag]").val(sort_flag);
	$("#listForm").submit();
}

function sendDesc2(sort_flag){
	var listForm = $("#listForm");
	$("#listForm").attr("action",summonConfig["listUrl"]);	
	listForm.find("input[name=sort_flag]").val(sort_flag);
	$("#listForm").submit();
}

$(function(){
	var check_type = $('#check_type').val();
	var sort_flag = $('#sort_flag').val();
	var rootPath = $('#rootPath').val();
	
	if(menu_name == "소명 요청") {
		if(sort_flag == 1){
			$("#sort_cnt").attr("onclick","javascript:sendDesc2(2)").attr("src", rootPath+"/resources/image/common/asc_btn.png")
			.attr("title","오름차순").attr("alt","오름차순"); 		
		}
	}else {
		if(sort_flag == 1){
			$("#"+ check_type).attr("onclick","javascript:sendDesc('" +check_type+"',2)").attr("src", rootPath+"/resources/image/common/asc_btn.png")
			.attr("title","오름차순").attr("alt","오름차순"); 		
		}
	}
	
	if(menu_name == "소명 요청") {
		$('input[type=text]').attr("onkeydown","javascript:if(event.keyCode==13){movesummonList();}");
	} else {
		$('input[type=text]').attr("onkeydown","javascript:if(event.keyCode==13){moveSummonManageList();}");
	}
});

