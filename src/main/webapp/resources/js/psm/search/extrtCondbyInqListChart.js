/**
 * 추출조건별조회 Javascript
 * @author tjlee
 * @since 2014. 4. 10
 */
	 	
 // 상세로 이동

$(function(){
	getChart();	
});

function getChart() {

	/*var search_from = '2016-05-30';
	var search_to = '2016-11-30';*/
	
	var search_from = $("input[name=search_from]").val();
	var search_to = $("input[name=search_to]").val();
	var day_month_year = $("#day_month_year").val();
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/extrtCondbyInq/linechart.html',
		data: { 
			search_from : search_from,
			search_to : search_to,
			dataCheck : 1,
			day_month_year : day_month_year
		},
		success: function(data) {
			drawChart(data.lineChart);
		}
	});
}


function drawChart(chart) {
	if(chart != null) {
		var dataSource = new Array();
		var series = new Array();
		
		$.each(chart, function(i, item) {
			if(item.data3.length == 8){
				var temp = item.data3;
				var data3 = temp.substring(0, 4)+"-"+temp.substring(4, 6)+"-"+temp.substring(6, item.data3.length);  
			}else if(item.data3.length == 6){
				var temp = item.data3;
				var data3 = temp.substring(0, 4)+"-"+temp.substring(4, item.data3.length);  
			}else if(item.data3.length == 4){
				var data3 = item.data3;
			}
			
			if(i == 0) {
				aa = { system : check_null(data3), cnt0 : item.cnt1 };
				bb = { valueField : "cnt0", name : check_null(item.data2) };
			}
			else if(i == 1) {
				aa = { system : check_null(data3), cnt1 : item.cnt1 };
				bb = { valueField : "cnt1", name : check_null(item.data2) };
			}
			else if(i == 2) {
				aa = { system : check_null(data3), cnt2 : item.cnt1 };
				bb = { valueField : "cnt2", name : check_null(item.data2) };
			}
			else if(i == 3) {
				aa = { system : check_null(data3), cnt3 : item.cnt1 };
				bb = { valueField : "cnt3", name : check_null(item.data2) };
			}
			else if(i == 4) {
				aa = { system : check_null(data3), cnt4 : item.cnt1 };
				bb = { valueField : "cnt4", name : check_null(item.data2) };
			}
			else if(i == 5) {
				aa = { system : check_null(data3), cnt5 : item.cnt1 };
				bb = { valueField : "cnt5", name : check_null(item.data2) };
			}
			else if(i == 6) {
				aa = { system : check_null(data3), cnt6 : item.cnt1 };
				bb = { valueField : "cnt6", name : check_null(item.data2) };
			}
			else if(i == 7) {
				aa = { system : check_null(data3), cnt7 : item.cnt1 };
				bb = { valueField : "cnt7", name : check_null(item.data2) };
			}
			else if(i == 8) {
				aa = { system : check_null(data3), cnt8 : item.cnt1 };
				bb = { valueField : "cnt8", name : check_null(item.data2) };
			}
			else if(i == 9) {
				aa = { system : check_null(data3), cnt9 : item.cnt1 };
				bb = { valueField : "cnt9", name : check_null(item.data2) };
			}
			else if(i == 10) {
				aa = { system : check_null(data3), cnt10 : item.cnt1 };
				bb = { valueField : "cnt10", name : check_null(item.data2) };
			}
			else if(i == 11) {
				aa = { system : check_null(data3), cnt11 : item.cnt1 };
				bb = { valueField : "cnt11", name : check_null(item.data2) };
			}
			else if(i == 12) {
				aa = { system : check_null(data3), cnt12 : item.cnt1 };
				bb = { valueField : "cnt12", name : check_null(item.data2) };
			}
			else if(i == 13) {
				aa = { system : check_null(data3), cnt13 : item.cnt1 };
				bb = { valueField : "cnt13", name : check_null(item.data2) };
			}
					
			dataSource.push(aa);
			series.push(bb);
		});
		
		var chart3 = $("#chartLine").dxChart({
			equalBarWidth : false,
			dataSource : dataSource,
			rotated : false,
			commonSeriesSettings : {
				argumentField : "system",
				type : "line",
				label : {
					visible : true,
					format : "fixedPoint",
					precision : 0,
					backgroundColor : 'none',
					font : {
						color : 'black',
						weight : '550'
					}
				}
				
			},
			series : series,
			palette : ['#6fc0ff', '#31a0ff', '#0d7ad8', '#2433ff', '#6724ff', '#8440c9', '#b956de', '#f57cf4', '#f57ca4', '#ffa488'],
			//palette : 'Soft',
			title : false,
			legend : {
				visible : true,    
			},
			argumentAxis: { 
				visible : true,
				label : {
					font : { color : 'black', size : 12 }
				}
			},
			valueAxis : {
				visible : true,
				label : {
					font : { color : 'black', size : 12 }
				}
			},
			tooltip : {
				enabled : true,
				shared : true,
				font : { 
					color: 'black',
					size : 13
				},
				customizeText : function (point) {
					return  point.points[0].seriesName + " : " + point.points[0].originalValue;
				}
			}
			
		}).dxChart('instance');
	}
}

$(function () {
	var search_from = $("input[name=search_from]").val();
	var search_to = $("input[name=search_to]").val();
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/extract_chart.html',
		data: { 
			search_from: search_from,
			search_to : search_to
		},
		success: function(data) {
		var series = new Array();
		
		
		
		  for(var j=0; j < data.extract_name.length; j++){
		
			var jsonData = new Object();
			var sData = new Array();
			jsonData.name = data.extract_name[j].rule_nm;
			 for(var i=0; i < data.dashboard_extract.length; i++){
			  if(data.extract_name[j].rule_cd == data.dashboard_extract[i].rule_cd){
				var xyData = new Array();
				var month = (data.dashboard_extract[i].occr_dt).substring(4,6);
				month = month-1;
				xyData.push(Date.UTC((data.dashboard_extract[i].occr_dt).substring(0,4),month,(data.dashboard_extract[i].occr_dt).substring(6,8)))
				xyData.push(data.dashboard_extract[i].cnt)
				sData.push(xyData);
			  }	
			 }
			jsonData.data = sData
			series.push(jsonData)
		  }
						
			extract_chart(series);
		}
	});	
	
   
});

function extract_chart(srdata){
	if(srdata.length !=0){
	 Highcharts.chart('chart_extract', {
	        chart: {
	            type: 'spline'
	        },
	        title: {
	            text: '추출조건을 만족하는 개인정보 접근 통계'
	        },
	        subtitle: {
	            text: ''
	        }, credits: {
	            enabled: false
	        }, exporting: { 
	        	enabled: false 
	        },
	        xAxis: {
	            type: 'datetime',
	            labels: {
	                format: '{value:%m-%d}',
	                align: 'center'
	            },
	            title: {
	                text: '날짜'
	            }
	        },
	        yAxis: {
	            title: {
	                text: '개인정보 건수'
	            },
	            min: 0
	        },
	        tooltip: {
	            headerFormat: '<b>{series.name}</b><br>',
	            pointFormat: '{point.x:%e. %b}: {point.y: f}건'
	        },

	        plotOptions: {
	            spline: {
	                marker: {
	                    enabled: true
	                }
	            }
	        },

	        series: srdata
	    });
	}
}
