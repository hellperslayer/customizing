/**
 * 추출환경설정 Javascript
 * @author tjlee
 * @since 2014. 4. 10
 */
 // 상세로 이동

$(document).ready(function() {
	$("span[id^='scen2_']").hide();
});

// 신규페이지, 상세페이지 이동
function fnextrtBaseSetupDetail(rule_seq){
	$("input[name=rule_seq]").attr("value",rule_seq);
	$("#listForm").attr("action",extrtBaseSetupConfig["detailUrl"]);
	$("#listForm").submit();
}

// 페이지 이동
function goPage(num){
	$("#listForm").attr("action",extrtBaseSetupConfig["listUrl"]);
	$("#listForm input[name=page_num]").val(num);
	$("#listForm").submit();
}



function fnextrtBaseSetup(scen_seq, scen_name){
	$("input[name=scen_seq]").val(scen_seq);
	$("input[name=seq]").val(scen_name);
	$("#listForm").attr("action",scenarioConfig["detailUrl"]);
	$("#listForm").submit();
}

// 시나리오 시뮬레이션 검색
function moveScenarioList() {
	$('input[name=scen_seq]').attr("value",0);
	$('input[name=isSearch]').attr("value","Y");
	$("#listForm").attr("action",scenarioConfig["listUrl"]);
	$("#listForm").submit();
}

//취소 버튼
function cancelExtrtBaseSetupList() {
	$('select[name=is_realtime_extract_search]').val("");
	$('select[name=use_yn_search]').val("");
	$('select[name=alarm_yn_search]').val("");
	$("#listForm").attr("action",extrtBaseSetupConfig["listUrl"]);
	$("#listForm").submit();
}

//추출환경 ajax call
function sendAjaxextrtBaseSetup(type){
	
	var $form = $("#extrtBaseSetupDetailForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'rule_seq,rule_nm,dng_val,use_yn,is_realtime_extract,user_limit_cnt_yn,alarm_yn';
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'add') {
		log_message_title = '추출환경등록';
		log_action = 'INSERT';
	} else if (type == 'save') {
		log_message_title = '추출환경수정';
		log_action = 'UPDATE';
	} else if (type == 'remove') {
		log_message_title = '추출환경삭제';
		log_action = 'REMOVE';
	}

	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);

	sendAjaxPostRequest(extrtBaseSetupConfig[type + "Url"],$form.serialize(), ajaxExtrtBaseSetup_successHandler, ajaxExtrtBaseSetup_errorHandler, type);

}

//추출환경 ajax call - 성공
function ajaxExtrtBaseSetup_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = indvifoTypeSetupConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}
		
		moveextrtBaseSetupListtoDetail();
	}
}

//추출환경 ajax call - 실패
function ajaxExtrtBaseSetup_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	log_message += codeLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("추출환경 등록 실패하였습니다." + textStatus);
			break;
		case "save":
			alert("추출환경 수정 실패하였습니다." + textStatus);
			break;
		case "remove":
			alert("추출환경 삭제 실패하였습니다." + textStatus);
			break;
		default:
	}
}

//목록으로 이동
function moveextrtBaseSetupListtoDetail(){
	$("#listForm").attr("action",extrtBaseSetupConfig["listUrl"]);
	$("#listForm").submit();
}

function moveextrtBaseSetupDetail(){
	$("#extrtBaseSetupDetailForm").attr("action",extrtBaseSetupConfig["detailUrl"]);
	$("#extrtBaseSetupDetailForm").submit();
}

//목록으로 이동
function movescenarioList(){
	
	$("#listForm").attr("action",extrtBaseSetupConfig["scenarioUrl"]);
	$("#listForm").submit();
}

// 사용여부 체크 
function fnextrtBaseSetupListUseYnCheck(){
	$("#listForm").attr("action",extrtBaseSetupConfig["listUrl"]);
	
	if ($("input[name=use_yn]:checked")) {
		alert($("input[name=use_yn]").val());
	}else{
		$("input[name=use_yn]").attr("value", "N");
		alert($("input[name=use_yn]").val());
	}
	
	var checkbox_useyn = $("input[name=use_yn]:checked").val();
	alert(checkbox_useyn);
	$("#listForm").submit();
}

/**
 * 엑셀 다운로드 추가 2015/6/4
 */
function excelExtrtBaseSetup(){
//	if(arguments[0] >= 65535){
//		alert("엑셀다운로드는 65535건까지 가능합니다.");
//	}else{
		
		var $form = $("#listForm");
		var log_message_params = '';
		var menu_id = $('input[name=current_menu_id]').val();
		var log_message_title = '시나리오관리 엑셀 다운로드';
		var log_action = 'EXCEL DOWNLOAD';
		
		$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
		
		$("#listForm").attr("action",extrtBaseSetupConfig["downloadUrl"]);
		$("#listForm").submit();
//	}
}

function setPrctAll() {
	
	/*var extrtcond = $("select[name=rule_seq]").val();
	if(extrtcond == '' || extrtcond == null) { 
		alert("추출조건을 선택해주세요.");
		return;
	}*/
	
	var prct_val = $("input[name=prct_val]").val();
	if(prct_val == '' || prct_val == null) {
		alert("비율을 적어주세요.");
		$("input[name=prct_val]").focus();
		return;
	}
	
	var num_check = /^[0-9]*$/;
	if(!num_check.test(prct_val)) {
		alert("숫자만 적어주세요.");
		$("input[name=prct_val]").focus();
		return;
	}
	
	if(confirm("일괄 적용하시겠습니까?")) {
		sendAjaxIndv();
	}
}

//ajax call
function sendAjaxIndv(){
	var $form = $("#extrtBaseSetupDetailForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'rule_seq, prct_val';
	var menu_id = $('input[name=current_menu_id]').val();
	
	$form.appendLogMessageParamsInput('개인별 위험도 일괄수정', 'UPDATE', log_message_params, menu_id);
	
	sendAjaxPostRequest(extrtBaseSetupConfig["saveIndvUrl"],$form.serialize(), ajaxIndv_successHandler, ajaxIndv_errorHandler, "save");
}

// ajax call - 성공
function ajaxIndv_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = addUrlDetailConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		alert("성공적으로 적용되었습니다.");
		
		moveextrtBaseSetupDetail();
	}
}

// ajax call - 실패
function ajaxIndv_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	log_message += codeLogMessage[actionType + "Action"];
	
	alert("적용 실패하였습니다." + textStatus);
}

function updateScenario(scen_seq) {
	event.cancelBubble = true;
	
	var scen1 = document.getElementById("scen1_" + scen_seq);
	scen1.style.display = "none";
	var scen2 = document.getElementById("scen2_" + scen_seq);
	scen2.style.display = "block";
	document.getElementById("submitScenarioBtn_"+scen_seq).style.display = "";
	document.getElementById("updateScenarioBtn_"+scen_seq).style.display = "none";
	document.getElementById("cancelScenarioBtn_"+scen_seq).style.display = "";
	document.getElementById("removeScenarioBtn_"+scen_seq).style.display = "none";
	
	$("input[name=scen_name]").focus();
}




// --------------------------------------

function detailScenario(scen_seq) {
	$("#listForm input[name=scen_seq]").val(scen_seq);
	$("#listForm").attr("action",scenarioConfig["scenarioDetail"]);
	$("#listForm").submit();
}

// 분석위험기준 선택값에 따른 임계치 영역
function radioChange(limitCntVal) {
	if(limitCntVal=='4'||limitCntVal=='5') {
		$("#limit_type_cnt_div").css("display", "inline-block");
		$("#limit_type_cnt_div1").css("display", "inline-block");
	} else {
		$("#limit_type_cnt_div").css("display", "none");
		$("#limit_type_cnt_div1").css("display", "none");
	}
}


// 시스템 modal 에서 선택한 값 input에 넣기
function systemCheckbox() {
	var systemVal = '';
	var systemName = '';
	var checkbox = $("input:checkbox[id='system_seq']:checked");
	checkbox.each(function(i) {
		systemVal += this.value +",";
		var tr = checkbox.parent().parent().eq(i);
		var td = tr.children();
		systemName += td.eq(1).text()+", ";
	})
	if(systemVal != ''){
		systemVal = systemVal.slice(0,-1);
		systemName = systemName.slice(0,-2);
	}
	$("input[name=system_seq]").val(systemVal);
	$("input[id=system_name]").val(systemName);
}
// 시스템 modal에서 전체 선택
function checkAll1() {
	if($('#systemAll').is(':checked')) {
		$('input:checkbox[id=system_seq]').prop('checked', true);
	} else {
		$('input:checkbox[id=system_seq]').prop('checked', false);
	}
}
//시스템 명을 클릭해도 체크 되도록
function checkSystem(tr) {
	var index = tr.rowIndex;
	var systemCheckbox = tr.cells[0].getElementsByTagName("input")[0];
	if(systemCheckbox.checked) {
		systemCheckbox.checked = false;
	} else {
		systemCheckbox.checked = true;
	}
}
// 개인정보 유형을 클릭해도 체크 되도록
function checkIndv(tr) {
	var index = tr.rowIndex;
	var indvCheckbox = tr.cells[0].getElementsByTagName("input")[0];
	if(indvCheckbox.checked) {
		indvCheckbox.checked = false;
	} else {
		indvCheckbox.checked = true;
	}
}
// 개인정보유형 modal에서 전체 선택
function checkAll2() {
	if($('#privacyAll').is(':checked')) {
		$('input:checkbox[id=privacy_seq]').prop('checked', true);
	} else {
		$('input:checkbox[id=privacy_seq]').prop('checked', false);
	}
}
// 개인정보유형 modal 에서 선택한 값 input에 넣기
function indivCheckbox() {
	var indivVal = '';
	var privacyName = '';
	var checkbox = $("input:checkbox[id=privacy_seq]:checked"); 
	checkbox.each(function(i) {
		indivVal += this.value +",";
		var tr = checkbox.parent().parent().eq(i);
		var td = tr.children();
		privacyName += td.eq(1).text()+", ";
	})
	if(indivVal != ''){
		indivVal = indivVal.slice(0,-1);
		privacyName = privacyName.slice(0,-2);
	}
	$("input[name=privacy_seq]").val(indivVal);
	$("input[id=privacy_desc]").val(privacyName);
}
// 분석 대상 선택에 따라 시스템 선택 영역 노출/미노출 제어
function radioChange2(indv_yn) {
	if(indv_yn=='Y') {
		$("#systemBtn").css("display", "none");
		$("#system_name").css("display", "none");
		$("input[name=system_seq]").val('');
		$("input[id=system_name]").val('');
		$("input:checkbox[id=system_seq]").prop("checked", false);
		$("input:checkbox[id=systemAll]").prop("checked", false);
	} else {
		$("#systemBtn").css("display", "inline-block");
		$("#system_name").css("display", "inline-block");
	}
}

// 참조값 영역 컨트롤 - 추가
function addRefVal(){
	var auth_id = document.getElementById('extrtBaseSetupAuthId').value;
	var reftable = document.getElementById('reftable');
	var row = reftable.insertRow(reftable.rows.length);
	var cell1 = row.insertCell(0);
	var cell2 = row.insertCell(1);
	var cell3 = row.insertCell(2);
	
	if(auth_id=='AUTH00000') {
		var cell4 = row.insertCell(3);
		cell1.innerHTML = '<input type="text" class="form-control"/>';
		cell2.innerHTML = '<input type="text" class="form-control"/>';
		cell3.innerHTML = '<input type="text" class="form-control"/>';
		cell4.innerHTML = '<button type="button" class="btn" onclick="removeRefVal(this.parentElement)">삭제</button>';
	} else {
		cell1.innerHTML = '<input type="text" class="form-control" readonly="readonly"/>';
		cell2.innerHTML = '<input type="text" class="form-control"/>';
		cell3.innerHTML = '<input type="text" class="form-control" readonly="readonly"/>';
	}
}
//참조값 영역 컨트롤 - 삭제
function removeRefVal(td){
	var reftable = document.getElementById('reftable');
	var tr = td.parentElement.rowIndex;
	reftable.deleteRow(tr);
}
//참조값 영역 컨트롤 - text로 변환
function textRefVal() {
	var reftable = document.getElementById('reftable');
	var lengthVal = reftable.rows.length;
	var ref_val = "";
	for(i=1; i<lengthVal ; i++){
		var tr = reftable.getElementsByTagName('tr')[i];
		var refName = tr.cells[0].getElementsByTagName('input')[0].value;
		if(refName=='') break;
		var refValue = tr.cells[1].getElementsByTagName('input')[0].value;
		if(refValue=='') {
			alert("변수를 입력해주세요.");
			return false;
		}
		var refExplain = tr.cells[2].getElementsByTagName('input')[0].value;
		ref_val += refName+"|"+refValue+"|"+refExplain+",";
	}
	ref_val = ref_val.slice(0,-1);
	$("input[name=ref_val]").val(ref_val);
	return true;
}

// 상세시나리오 리스트 검색
function moveextrtBaseSetupList() {
	$('input[name=page_num]').attr("value","1");
	$('input[name=isSearchDetail]').attr("value","Y");
	$("#listForm").attr("action",extrtBaseSetupConfig["listUrl"]);
	$("#listForm").submit();
}

//상세시나리오 추가 시 시나리오 선택 할 때
function scenseqChange() {
	var scen_seq = $("select[name=scen_seq]").val();
	$("#moveForm input[name=scen_seq]").val(scen_seq);
	$("#moveForm").attr("action",extrtBaseSetupConfig["detailUrl"]);
	$("#moveForm").submit();
}

// 상세시나리오 등록
function addextrtBaseSetup(){
	if(valueCheck()){
		var detailForm = $('#extrtBaseSetupDetailForm').serialize();
		$.ajax({
			type : 'POST',
			url : rootPath + '/extrtBaseSetup/add.html',
			data : detailForm,	
			success : function(data) {
				var result = data.result;
				if(result=='SUCCESS') {
					alert("등록하였습니다.");
					moveextrtBaseSetupListtoDetail();
				} else if(result == 'EXIST') {
					alert('시나리오코드가 존재 합니다.');
				} else {
					alert('등록에 실패하였습니다.');
				}
			}
		});
	}
}

//추출환경 수정
function saveextrtBaseSetup(){
	// 개인별 또는 부서별 설정 후 다른 것으로 변경하면 개인별, 부서별임계치 설정에서 설정한 값 모두 삭제
	var limit_type_info = $("input[name=limit_type_info]").val();
	var limit_type = $("input[name=limit_type]:checked").val();
	if(limit_type_info=='2' && limit_type!='2') {
		var retrurn = confirm("기존에 설정한 개인별 임계치는 모두 삭제됩니다.");
		if(!retrurn) return false;
		$("input[name=use_yn_search]").val('2');
	} else if(limit_type_info=='3' && limit_type!='3') {
		var retrurn = confirm("기존에 설정한 부서별 임계치는 모두 삭제됩니다.");
		if(!retrurn) return false;
		$("input[name=use_yn_search]").val('3');
	}
	
	if(valueCheck()){
		var detailForm = $('#extrtBaseSetupDetailForm').serialize();
		$.ajax({
			type : 'POST',
			url : rootPath + '/extrtBaseSetup/save.html',
			data : detailForm,	
			success : function(data) {
				if(data.result=='SUCCESS'){
					alert("수정하였습니다.");
					$("input[name=indv_yn]").val("");
					moveextrtBaseSetupListtoDetail();
				} else {
					alert("실패하였습니다.");
				}
			}
		});
	}
}

function valueCheck() {
	var rule_nm = $('input[name=rule_nm]').val();
	var dng_val = $('input[name=dng_val]').val();
	var limit_cnt=$('input[name=limit_cnt]').val();
	var limit_type = $("input[name=limit_type]").val();
	var limit_type_cnt = $("input[name=limit_type_cnt]").val();
	var indv_yn = $('input[name=indv_yn]:checked').val();
	var system_seq = $("input[name=system_seq]").val();
	var rule_seq = $("input[name=rule_seq]").val();
	var dngValRest = $("input[name=dngValRest]").val();
	
	var scen_seq = scen_seq = $("input[name=scen_seq]").val();
	if(scen_seq==''){
		scen_seq = $('select[name=scen_seq]').val();
	}
	
	if(rule_nm == '') {
		alert("상세시나리오명을 입력해주세요");
		$('input[name=rule_nm]').focus();
		return false;
	}
	if(dng_val == '') {
		alert("위험도를 입력해주세요");
		$('input[name=dng_val]').focus();
		return false;
	}else if(parseInt(dng_val) > parseInt(dngValRest)){
		alert("현재 "+dngValRest+"이하만 입력할 수 있습니다.")
		$('input[name=dng_val]').focus();
		return false;
	}
	if(limit_cnt == ''){
		alert("임계치 설정값을 입력해주세요");
		$('input[name=limit_cnt]').focus();
		return false;
	}else if(limit_cnt < 0 || limit_cnt > 2147483647){
		alert("임계치는 0 이상 2147483647 이하만 입력할 수 있습니다.")
		$('input[name=limit_cnt]').focus();
		return false;
	}
	if(limit_type=='4'||limit_type=='5'){
		if(limit_type_cnt ==''){
			alert("임계치 설정값(N)을 입력해주세요");
			$('input[name=limit_type_cnt]').focus();
			return false;
		}
	}
	if(indv_yn=='N' && system_seq == ''){
		alert("대상 시스템을 선택해주세요");
		return false;
	}
	
	// 개인정보유형 임시 처리
	var rule_view_type = $('input[name=rule_view_type]:checked').val();
	if(rule_view_type=='R'){
		$('input[name=result_type_yn]').val('Y');
	}
	
	return textRefVal();
}

// 상세 시나리오 삭제
function removeextrtBaseSetup(){
	if(!confirm("삭제하시겠습니까?")) {
		return false;
	}
	var rule_seq = $("input[name=rule_seq]").val();
	$.ajax({
		type : 'POST',
		url : rootPath + '/extrtBaseSetup/remove.html',
		data : {
			rule_seq : rule_seq
		},
		success : function(data) {
			alert("삭제하였습니다.");
			moveextrtBaseSetupListtoDetail();
		}
	});
}

function simulationForm() {
	var si = document.getElementById("simul");
	si.style.display = "block";
}

// 시뮬레이션 실행
function simulate() {
	if(!valueCheck()){
		return false;
	}
	var search_from = document.getElementById("search_fr").value;
	if(search_from==null || search_from==''){
		alert("추출일을 선택해주세요.");
		return false;
	}
	$("input[name=search_from]").val(search_from);
	var detailForm = $('#extrtBaseSetupDetailForm').serialize();
	var limit_cnt = $('input[name=limit_cnt]').val();
	$.ajax({
		type: 'POST',
		url: rootPath + '/extrtCondbyInq/runScript.html',
		data: detailForm,
		success: function(data) {
			if(data.hasOwnProperty("statusCode")) {
				var statusCode = data.statusCode;
				if(statusCode == 'SYS006V') {
					location.href = adminUserDetailConfig['loginPage'];
					return false;
				}
				
				if(data.hasOwnProperty("message")) {
					var message = data.message;
					alert(message);
				}
			} else {
				drawTable(data.logList);
			}
		},
		beforeSend:function() {
			$('#loading').show();
		},
		complete:function() {
			$('#loading').hide();
		}
	});
}

// 참조값 설정하는 표
function drawTable(data) {
	$("#tempTable").find('tbody').empty();
	console.log(data);
	if(data != null && data.length != 0) {		
		$.each(data, function( i, item ) {
			$("<tr>").appendTo($("#tempTable").find('tbody'))
			.append($("<td style='text-align: center;'>").text(check_null(item.occr_dt)))
			.append($("<td style='text-align: center;'>").text(check_null(item.emp_user_id)))
			.append($("<td style='text-align: center;'>").text(check_null(item.emp_user_name)))
			.append($("<td style='text-align: center;'>").text(check_null(item.dept_id)))
			.append($("<td style='text-align: center;'>").text(check_null(item.dept_name)))
			.append($("<td style='text-align: center;'>").text(check_null(item.system_name)))
			.append($("<td style='text-align: center;'>").text(check_null(item.limit_cnt)))
			.append($("<td style='text-align: center;'>").text(check_null(item.rule_cnt)))
			.append($("<td style='text-align: center;'>").text(check_null(item.result_content)))
			.fadeOut()
			.fadeIn("slow");
		});
		$("#tempDiv").css("display", "");
	} else {
		$("<tr>").appendTo($("#tempTable").find('tbody'))
		.append($("<td style='text-align: center;' colspan='9'>").text("데이터가 존재하지 않습니다."));
		$("#tempDiv").css("display", "");
	}
}

// 테이블접근형태 N:1 일 때는 원시로그접근기준의 처리량 선택 불가능 하도록
function ruleResultTypeChange(val) {
	if(val=='N') {
		$("input:radio[name=rule_view_type]:radio[value='M']").prop('checked', false);
		$("input:radio[name=rule_view_type]:radio[value='R']").prop('checked', true);
		$("input:radio[name=rule_view_type]:radio[value='M']").attr("onclick", "return(false);");
		$("input:radio[name=rule_view_type]:radio[value='R']").attr("onclick", "return(false);");
		alert("테이블접근형태 N:1 선택 시 처리량 선택 불가");
	} else {
		$("input:radio[name=rule_view_type]:radio[value='M']").removeAttr("onclick");
		$("input:radio[name=rule_view_type]:radio[value='R']").removeAttr("onclick");
	}
}

