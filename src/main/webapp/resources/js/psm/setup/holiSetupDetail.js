/**
 * 휴일정보 상세 Javascript
 * @author yrchoo
 * @since 2015. 4. 24
 */

$(function(){
	var date = new Date();
	var year = date.getFullYear();
	
	var yearArray = [year-2, year-1, year, year+1, year+2];
	
	for(var i=0; i < yearArray.length; i++) {
		$('#add_year').append("<option value='" + yearArray[i] + "'>" +  yearArray[i] + "</option>");
	}
	
	$("#add_year > option[value=" + year + "]").attr("selected","selected");
	
});

//숫자만 입력가능
function onlyNumber() {
	var agt = navigator.userAgent.toLowerCase(); 
	if(!((event.keyCode >= 37 && event.keyCode <= 57) 
			|| event.keyCode == 13 
          || (event.keyCode >= 96 && event.keyCode <= 105)
          || event.keyCode == 8  || event.keyCode == 9)) {
         if (agt.indexOf("msie") != -1){
        	 event.returnValue = false;
         }
         else if (agt.indexOf("trident") != -1){
	         event.preventDefault();
         }
         alert("숫자만 입력해주세요.");
    } 
}

// 추가
function addHoli(){
	var work_start_tm = $('input[name=work_start_tm]').val();
	var work_end_tm = $('input[name=work_end_tm]').val();
	
	if(work_start_tm == '') {
		alert("근무 시작시간을 입력해주세요");
		$('input[name=work_start_tm]').focus();
		return false;
	}
	if(work_end_tm == '') {
		alert("근무 종료시간을 입력해주세요");
		$('input[name=work_end_tm]').focus();
		return false;
	}
		
	sendAjaxHoli("add");	
}

// 수정
function saveHoli(){
	var holi_nm = $('input[name=holi_nm]').val();
	var work_start_tm = $('input[name=work_start_tm]').val();
	var work_end_tm = $('input[name=work_end_tm]').val();
	
	if(holi_nm == '') {
		alert("휴일명을 입력해주세요");
		$('input[name=holi_nm]').focus();
		return false;
	}
		
	sendAjaxHoli("save");	
	
}

// ajax call
function sendAjaxHoli(type){
	var $form = $("#holiDetailForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'holi_nm, holi_yn, work_start_tm, work_end_tm';
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'add') {
		log_message_title = '휴일등록';
		log_action = 'INSERT';
	} else if (type == 'save') {
		log_message_title = '휴일수정';
		log_action = 'UPDATE';
	} else if (type == 'remove') {
		log_message_title = '휴일삭제';
		log_action = 'REMOVE';
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(holiDetailConfig[type + "Url"],$form.serialize(), ajaxHoli_successHandler, ajaxHoli_errorHandler, type);
}

// ajax call - 성공
function ajaxHoli_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = holiDetailConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}
		
		moveHoliList();
	}
}

// ajax call - 실패
function ajaxHoli_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	log_message += codeLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("휴일 등록 실패하였습니다." + textStatus);
			break;
		case "save":
			alert("휴일 수정 실패하였습니다." + textStatus);
			break;
		case "remove":
			alert("휴일 삭제 실패하였습니다." + textStatus);
			break;
		default:
	}
}

// 목록으로 이동
function moveHoliList(){
	$("#holiDetailForm").attr("action",holiDetailConfig["listUrl"]);
	$("#holiDetailForm").submit();
}

