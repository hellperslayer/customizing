/**
 * 개인정보유형별설정 Javascript
 * @author tjlee
 * @since 2014. 4. 10
 */
 // 상세로 이동
function fnReferenceManagerDetail(search_datas){
	//리스트에 출력된 컬럼들 item_value 로 , 구분되어지게 해서 보낸다.
	//var item_values = $('input[name=item_value]').val();
	$("input[name=detail_search_datas]").attr("value",search_datas);
	$("#listForm").attr("action",referenceManagerConfig["detailUrl"]);
	$("#listForm").submit();
}

// 페이지 이동
function goPage(num){
	$("#listForm").attr("action",referenceManagerConfig["listUrl"]);
	$("#listForm input[name=page_num]").val(num);
	$("#listForm").submit();
}

// 검색
function moveindvinfoTypeSetupList() {
	$('input[name=page_num]').attr("value","1");
	$('input[name=isSearch]').attr("value","Y");
	$("#listForm").attr("action",referenceManagerConfig["listUrl"]);
	$("#listForm").submit();
}

// 참조테이블 검색
function moveColumnDetailList() {
	$('input[name=page_num]').attr("value","1");
	$("#listForm").attr("action",referenceManagerConfig["listUrl"]);
	$("#listForm").submit();
} 

function fnReferenceManagerTableInsert(type) {
	$("input[name=type]").attr("value",type);
	$("#listForm").attr("action",referenceManagerConfig["detailUrl"]);
	$("#listForm").submit();
}

//개인정보유형 추가
function addindvinfoTypeSetup(){
	var privacy_seq = $('input[name=privacy_seq]').val();
	var privacy_desc =$('input[name=privacy_desc]').val();
	//var regular_expression = $('input[name=regular_expression]').val();
	
	if(privacy_seq == '') {
		alert("개인정보유형번호를 입력해주세요");
		$('input[name=privacy_seq]').focus();
		return false;
	}
	if(privacy_seq.length > 5) {
		alert("개인정보유형번호는 5자리 이하의 숫자만 입력해주세요.");
		$('input[name=privacy_seq]').focus();
		return false;
	}
	if(privacy_desc == '') {
		alert("개인정보유형명을 입력해주세요");
		$('input[name=privacy_desc]').focus();
		return false;
	}
	/*if(regular_expression == '') {
		alert("정규식을 입력해주세요");
		$('input[name=regular_expression]').focus();
		return false;
	}*/
	
	var confirmVal = confirm("등록하시겠습니까?");
	if(confirmVal){
		sendAjaxindvinfoTypeSetup("add");	
	}else{
		return false;
	}
}

//개인정보유형 수정
function saveReferenceTableColumnInfo(){
	var result = "";
	/*
	var num = Number(status_count) + 1;
	
	for (var i = 1; i < num; i++) {
		
		if ($('input[name=input_data_'+i+']').val() == '') {
			alert("모든 컬럼의 데이터를 입력하세요");
			$('input[name=input_data_'+i+']').focus();
			return false;
		}
	}
	*/
	var confirmVal = confirm("수정하시겠습니까?");
	if(confirmVal){
		sendAjaxindvinfoTypeSetup("save");	
	}else{
		return false;
	}
}

//개인정보유형 삭제
function removeindvinfoTypeSetup(){
	if(confirm("삭제하시겠습니까?")) {
		sendAjaxindvinfoTypeSetup("remove");
	}
}

//개인정보유형 ajax call
function sendAjaxindvinfoTypeSetup(type){
	var $form = $("#indvinfoTypeSetupDetailForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'privacy_seq,privacy_desc,regular_expression';
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'add') {
		log_message_title = '참조테이블데이터등록';
		log_action = 'INSERT';
	} else if (type == 'save') {
		log_message_title = '참조테이블데이터수정';
		log_action = 'UPDATE';
	} else if (type == 'remove') {
		log_message_title = '참조테이블데이터삭제';
		log_action = 'REMOVE';
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(referenceManagerConfig[type + "Url"],$form.serialize(), ajaxIndvifoTypeSetup_successHandler, ajaxIndvifoTypeSetup_errorHandler, type);
}

//개인정보유형 ajax call - 성공
function ajaxIndvifoTypeSetup_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = indvifoTypeSetupConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}
		
		moveindvinfoTypeSetupListtoDetail();
	}
}

//개인정보유형 ajax call - 실패
function ajaxIndvifoTypeSetup_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	log_message += codeLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("참조테이블데이터 등록 실패하였습니다." + textStatus);
			break;
		case "save":
			alert("참조테이블데이터 수정 실패하였습니다." + textStatus);
			break;
		case "remove":
			alert("참조테이블데이터 삭제 실패하였습니다." + textStatus);
			break;
		default:
	}
}

//목록으로 이동
function moveindvinfoTypeSetupListtoDetail(){
	$("#indvinfoTypeSetupDetailForm").attr("action",referenceManagerConfig["listUrl"]);
	$("#indvinfoTypeSetupDetailForm").submit();
}

/**
 * 엑셀 다운로드 추가 2015/6/4
 */
function excelIndvinfoTypeSetup(){
//	if(arguments[0] >= 65535){
//		alert("엑셀다운로드는 65535건까지 가능합니다.");
//	}else{
		
		var $form = $("#listForm");
		var log_message_params = '';
		var menu_id = $('input[name=current_menu_id]').val();
		var log_message_title = '개인정보설정 엑셀 다운로드';
		var log_action = 'EXCEL DOWNLOAD';
		
		$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
		
		$("#listForm").attr("action",referenceManagerConfig["downloadUrl"]);
		$("#listForm").submit();
//	}
}

function selColum() {
	var table_name = $("#table_name").val();
	
	$.ajax({
		type:'POST',
		url : rootPath + '/indvinfoTypeSetup/findColumByTableName.html',
		data: {
			table_name : table_name
		},
		success : function(data){
			setColumList(data);
		}
	});
}

function setColumList(data) {
	var select = document.getElementById("column_name");
	while (select.options.length) {
		select.remove(0);
    }
	var none = document.createElement("option");
	none.text = "--선택안함--";
	none.value = "";
	select.add(none);
	
	var jsonData = JSON.parse(data);
	for(var i=0; i<jsonData.length; i++) {
		var option = document.createElement("option");
		option.text = jsonData[i].column_name;
		option.value = jsonData[i].column_name;
		select.add(option);
	}
}