function findRuleScript(rule_seq) {
	$.ajax({
		type: 'POST',
		url: rootPath + '/extrtCondbyInq/findRuleScript.html',
		data: { 
			"rule_seq":$("select[name=rule_select]").val()
		},
		success: function(data) {
			var item = data.object;
			$("#script").val(item.script);
			$("#limit_cnt").val(item.limit_cnt);
		},
		beforeSend:function() {
			$('#loading').show();
		},
		complete:function() {
			$('#loading').hide();
		}
	});
}

//페이지 이동
function goPage(num){
	$("#menuSearchForm").attr("action",simulateConfig["listUrl"]);
	$("#menuSearchForm input[name=page_num]").val(num);
	$("#menuSearchForm").submit();
}

function runScript() {
	var script = $("#script").val();
	if(script.toLowerCase().indexOf("truncate") != -1 || script.toLowerCase().indexOf("delete") != -1) {
		if(!confirm("데이터 삭제는 시스템 오류의 원인이 될 수 있습니다.\n실행하시겠습니까?")) {
			return false;
		}
	}
	
	var limit_cnt = $("#limit_cnt").val();
	var search_from = $("#search_fr").val();
	$.ajax({
		type: 'POST',
		url: rootPath + '/extrtCondbyInq/runScript.html',
		data: { 
			"script":script
			, "limit_cnt":limit_cnt
			, "search_from":search_from
		},
		success: function(data) {
			if(data.hasOwnProperty("statusCode")) {
				var statusCode = data.statusCode;
				if(statusCode == 'SYS006V') {
					location.href = adminUserDetailConfig['loginPage'];
					return false;
				}
				
				if(data.hasOwnProperty("message")) {
					var message = data.message;
					alert(message);
				}
			} else {
				drawTable(data.logList);
			}
			
		},
		beforeSend:function() {
			$("#runButton").css("display","none");
			$('#loading').show();
		},
		complete:function() {
			$("#runButton").css("display","");
			$('#loading').hide();
		}
	});
}

function drawTable(data) {
	$("#tempTable").find('tbody').empty();
	console.log(data);
	if(data != null && data.length != 0) {		
		$.each(data, function( i, item ) {
			var userid = item.emp_user_id;
			$("<tr>").appendTo($("#tempTable").find('tbody'))
			.append($("<td style='text-align: center;'>").text(check_null(item.log_seq)))
			.append($("<td style='text-align: center;'>").text(check_null(item.proc_date)))
			.append($("<td style='text-align: center;'>").text(check_null(item.proc_time)))
			.append($("<td style='text-align: center;'>").text(check_null(item.emp_user_id)))
			.append($("<td style='text-align: center;'>").text(check_null(item.emp_user_name)))
			.append($("<td style='text-align: center;'>").text(check_null(item.dept_id)))
			.append($("<td style='text-align: center;'>").text(check_null(item.dept_name)))
			.append($("<td style='text-align: center;'>").text(check_null(item.user_ip)))
			.append($("<td style='text-align: center;'>").text(check_null(item.user_id)))
			.append($("<td style='text-align: center;'>").text(check_null(item.scrn_id)))
			.append($("<td style='text-align: center;'>").text(check_null(item.scrn_name)))
			.append($("<td style='text-align: center;'>").text(check_null(item.req_type)))
			.append($("<td style='text-align: center;'>").text(check_null(item.system_seq)))
			.append($("<td style='text-align: center;'>").text(check_null(item.server_seq)))
			.append($("<td style='text-align: center;'>").text(check_null(item.req_context)))
			.append($("<td style='text-align: center;'>").text(check_null(item.req_url)))
			.append($("<td style='text-align: center;'>").text(check_null(item.req_end_time)))
			.append($("<td style='text-align: center;'>").text(check_null(item.result_type)))
			.fadeOut()
			.fadeIn("slow");
		});
		$("#tempDiv").css("display", "");
	} else {
		$("<tr>").appendTo($("#tempTable").find('tbody'))
		.append($("<td style='text-align: center;' colspan='18'>").text("데이터가 존재하지 않습니다."));
		$("#tempDiv").css("display", "");
	}
}


$(function(){
	$('#loading').hide();
});