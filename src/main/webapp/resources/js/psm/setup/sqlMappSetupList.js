/**
 * Sql매핑설정 Javascript
 * @author tjlee
 * @since 2014. 4. 10
 */

 // 상세로 이동
function fnsqlMappSetupDetail(tag_sql_url_seq){
	$("input[name=tag_sql_url_seq]").attr("value",tag_sql_url_seq);
	$("#listForm").attr("action",sqlMappSetupConfig["detailUrl"]);
	$("#listForm").submit();
}

// 페이지 이동
function goPage(num){
	$("#listForm").attr("action",sqlMappSetupConfig["listUrl"]);
	$("#listForm input[name=page_num]").val(num);
	$("#listForm").submit();
}

// 검색
function movesqlMappSetupList() {
	$('input[name=page_num]').attr("value","1");
	$('input[name=isSearch]').attr("value","Y");
	$("#listForm").attr("action",sqlMappSetupConfig["listUrl"]);
	$("#listForm").submit();
}

function moveList(){
	$("#listForm").attr("action", sqlMappSetupConfig["listUrl"]);
	$("#listForm").submit();
}

function excelMappSetupUpload(){
	var data = $("#result").val();

	var options = {
			success : function(data) {
				console.log(data);
				var checkdata = "\"true\"";
				if(checkdata == data){
					alert("성공적으로 업로드 되었습니다!");
					moveList();
				}else if(checkdata != data){
					alert("업로드에 실패하였습니다. (엑셀파일의 셀안에 공백이 들어있거나 필수 입력정보를 입력하였는지 확인해주십시오.)");
				}
				
			},
			error : function(error) {
				alert("요청 처리 중 오류가 발생하였습니다.");
			}
	  };
	    
$("#fileForm").ajaxSubmit(options);

    return false;
}

function excelUpLoadList(){
	var excelUp = $(".popup_wrap");
	excelUp.attr("style", "display: block;");
}

function closeUpload(){
	var excelUp = $(".popup_wrap");
	var file = $("#file");
	excelUp.attr("style", "display: none;");
	file.replaceWith( file.clone(true));
}

function exDown(){
	$("#exdown").submit();
}

// sql매핑 삭제
function removesqlMappSetup(){
	if(confirm("삭제하시겠습니까?")) {
//		$('#rule_seq').attr("name", "rule_seq");
		var tag_sql_url_seq = $("#tag_sql_url_seq").val();
		sendAjaxsqlMappSetup("remove");
	}
}

// 매뉴매핑 ajax call
function sendAjaxsqlMappSetup(type){
	var $form = $("#sqlMappSetupDetailForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'tag_sql_url_seq, url';
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'add') {
		log_message_title = '매뉴매핑등록';
		log_action = 'INSERT';
	} else if (type == 'save') {
		log_message_title = '매뉴매핑수정';
		log_action = 'UPDATE';
	} else if (type == 'remove') {
		log_message_title = '매뉴매핑삭제';
		log_action = 'REMOVE';
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(sqlMappSetupConfig[type + "Url"],$form.serialize(), ajaxsqlMappSetup_successHandler, ajaxsqlMappSetup_errorHandler, type);
}

// Sql매핑 ajax call - 성공
function ajaxsqlMappSetup_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = indvifoTypeSetupConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}
		
		movesqlMappSetupListtoDetail();
	}
}

// Sql매핑 ajax call - 실패
function ajaxsqlMappSetup_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	log_message += codeLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("Sql매핑 등록 실패하였습니다." + textStatus);
			break;
		case "save":
			alert("Sql매핑 수정 실패하였습니다." + textStatus);
			break;
		case "remove":
			alert("Sql매핑 삭제 실패하였습니다." + textStatus);
			break;
		default:
	}
}

// 목록으로 이동
function movesqlMappSetupListtoDetail(){
	$("#sqlMappSetupDetailForm").attr("action",sqlMappSetupConfig["listUrl"]);
	$("#sqlMappSetupDetailForm").submit();
}

/**
 * 엑셀 다운로드 추가 2015/6/4
 */
function excelSqlMappSetup(){
//	if(arguments[0] >= 65535){
//		alert("엑셀다운로드는 65535건까지 가능합니다.");
//	}else{
		
		var $form = $("#listForm");
		var log_message_params = '';
		var menu_id = $('input[name=current_menu_id]').val();
		var log_message_title = 'Sql매핑설정 엑셀 다운로드';
		var log_action = 'EXCEL DOWNLOAD';
		
		$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
		
		$("#listForm").attr("action",sqlMappSetupConfig["downloadUrl"]);
		$("#listForm").submit();
//	}
}