
$(document).ready(function() {
	$("span[id^='scen2_']").hide();
});

function fnextrtBaseSetupDetail(rule_seq){
	$("input[name=rule_seq]").attr("value",rule_seq);
	//$("input[name=scen_seq]").attr("value",0);
	$("#listForm").attr("action",extrtBaseSetupConfig["detailUrl"]);
	$("#listForm").submit();
}

// 페이지 이동
function goPage(num){
	$("#listForm").attr("action",extrtBaseSetupConfig["listUrl"]);
	$("#listForm input[name=page_num]").val(num);
	$("#listForm").submit();
}

// 검색
function moveextrtBaseSetupList() {
	$('input[name=page_num]').attr("value","1");
	$('input[name=isSearchDetail]').attr("value","Y");
	$("#listForm").attr("action",extrtBaseSetupConfig["listUrl"]);
	$("#listForm").submit();
}

function fnextrtBaseSetup(scen_seq, scen_name){
	$("input[name=scen_seq]").val(scen_seq);
	$("input[name=seq]").val(scen_name);
	$("input[name=isSearch]").val("Y");
	$("#listForm").attr("action",scenarioConfig["detailUrl"]);
	$("#listForm").submit();
}

// 시나리오 시뮬레이션 검색
function moveScenarioList() {
	$('input[name=scen_seq]').attr("value",0);
	$('input[name=isSearch]').attr("value","Y");
	$("#listForm").attr("action",scenarioConfig["listUrl"]);
	$("#listForm").submit();
}


//목록으로 이동
function moveextrtBaseSetupListtoDetail(){
	$("#extrtBaseSetupDetailForm").attr("action",extrtBaseSetupConfig["listUrl"]);
	$("#extrtBaseSetupDetailForm").submit();
}

function moveextrtBaseSetupDetail(){
	$("#extrtBaseSetupDetailForm").attr("action",extrtBaseSetupConfig["detailUrl"]);
	$("#extrtBaseSetupDetailForm").submit();
}



// 사용여부 체크 
function fnextrtBaseSetupListUseYnCheck(){
	$("#listForm").attr("action",extrtBaseSetupConfig["listUrl"]);
	
	if ($("input[name=use_yn]:checked")) {
		alert($("input[name=use_yn]").val());
	}else{
		$("input[name=use_yn]").attr("value", "N");
		alert($("input[name=use_yn]").val());
	}
	var checkbox_useyn = $("input[name=use_yn]:checked").val();
	alert(checkbox_useyn);
	$("#listForm").submit();
}


// -------------------------------------------------------
function radioChange(limitCntVal) {
	if(limitCntVal=='4'||limitCntVal=='5') {
		$("#limit_type_cnt_div").css("display", "inline-block");
		$("#limit_type_cnt_div1").css("display", "inline-block");
	} else {
		$("#limit_type_cnt_div").css("display", "none");
		$("#limit_type_cnt_div1").css("display", "none");
	}
}

function detailScenario(scen_seq) {
	$("#listForm input[name=scen_seq]").val(scen_seq);
	$("#listForm").attr("action",scenarioConfig["scenarioDetail"]);
	$("#listForm").submit();
}

function systemCheckbox() {
	var systemVal = '';
	var systemName = '';
	
	var checkbox = $("input:checkbox[id='system_seq']:checked");
	checkbox.each(function(i) {
		systemVal += this.value +",";
		var tr = checkbox.parent().parent().eq(i);
		var td = tr.children();
		systemName += td.eq(1).text()+", ";
	})
	if(systemVal != ''){
		systemVal = systemVal.slice(0,-1);
		systemName = systemName.slice(0,-2);
	}
	$("input[name=system_seq]").val(systemVal);
	$("input[id=system_name]").val(systemName);
}
function radioChange2(indv_yn) {
	if(indv_yn=='Y') {
		$("#systemBtn").css("display", "none");
		$("#system_name").css("display", "none");
		$("input[name=system_seq]").val('');
		$("input[id=system_name]").val('');
		$("input:checkbox[id=system_seq]").prop("checked", false);
	} else {
		$("#systemBtn").css("display", "inline-block");
		$("#system_name").css("display", "inline-block");
	}
}
// 시스템 명을 클릭해도 체크 되도록
function checkSystem(tr) {
	var index = tr.rowIndex;
	var systemCheckbox = tr.cells[0].getElementsByTagName("input")[0];
	if(systemCheckbox.checked) {
		systemCheckbox.checked = false;
	} else {
		systemCheckbox.checked = true;
	}
}
//목록으로 이동
function moveList(){
	$("#listForm").attr("action",scenarioConfig["scenarioUrl"]);
	$("#listForm").submit();
}

// 시나리오 추가
function addextrtBaseSetup(){
	var scen_seq = $('input[name=scen_seq]').val();
	var scen_name = $('input[name=scen_name]').val();
	var dng_val = $('input[name=dng_val]').val();
	var limit_cnt= $('input[name=limit_cnt]').val();
	var limit_type = $("input[name=limit_type]:checked").val();
	var limit_type_cnt = $("input[name=limit_type_cnt]").val();
	var indv_yn = $('input[name=indv_yn]:checked').val();
	var system_seq = $("input[name=system_seq]").val();
	var dngValRest = $("input[name=dngValRest]").val();
	
	if(scen_seq ==''){
		alert("시나리오코드를 입력해주세요");
		$('input[name=scen_seq]').focus();
		return false;
	}
	if(scen_name == '') {
		alert("시나리오명을 입력해주세요");
		$('input[name=scen_name]').focus();
		return false;
	}
	if(dng_val == '') {
		alert("복합위험도를 입력해주세요");
		$('input[name=dng_val]').focus();
		return false;
	} else if(dng_val < 0 || dng_val >= dngValRest){
		alert("현재 "+dngValRest+"이하만 입력할 수 있습니다.");
		$('input[name=dng_val]').focus();
		return false;
	}
	if(limit_cnt == ''){
		alert("임계치 설정값을 입력해주세요");
		$('input[name=limit_cnt]').focus();
		return false;
	} else if(limit_cnt < 0 || limit_cnt > 2147483647){
		alert("임계치는 0 이상 2147483647 이하만 입력할 수 있습니다.")
		$('input[name=limit_cnt]').focus();
		return false;
	}
	if(limit_type=='4'||limit_type=='5'){
		if(limit_type_cnt ==''){
			alert("임계치 설정값(N)을 입력해주세요");
			$('input[name=limit_type_cnt]').focus();
			return false;
		}
	}
	if(indv_yn=='N' && system_seq == ''){
		alert("대상 시스템을 선택해주세요");
		return false;
	}
	
	var detailForm = $('#detailForm').serialize();
	
	$.ajax({
		type : 'POST',
		url : rootPath + '/extrtBaseSetup/addScenario.html',
		data : detailForm,	
		success : function(data) {
			var result = data.result;
			if(result == 'scen_seq duplication'){
				alert("이미 사용중인 시나리오 코드입니다. 변경해주세요.");
				$('input[name=scen_seq]').focus();
			} else {
				alert("시나리오를 추가하였습니다.");
				moveList();
			}
		}
	});
}

// 시나리오 수정(아래 설정 부분)
function saveextrtBaseSetup(){
	var scen_name = $('input[name=scen_name]').val();
	var dng_val = $('input[name=dng_val]').val();
	var limit_cnt= $('input[name=limit_cnt]').val();
	var limit_type = $("input[name=limit_type]:checked").val();
	var limit_type_cnt = $("input[name=limit_type_cnt]").val();
	var indv_yn = $('input[name=indv_yn]:checked').val();
	var system_seq = $("input[name=system_seq]").val();
	var dngValRest = $("input[name=dngValRest]").val();
	
	if(scen_name == '') {
		alert("시나리오명을 입력해주세요");
		$('input[name=scen_name]').focus();
		return false;
	}
	if(dng_val == '') {
		alert("복합위험도를 입력해주세요");
		$('input[name=dng_val]').focus();
		return false;
	} else if(dng_val < 0 || dng_val > dngValRest){
		alert("현재 "+dngValRest+"이하만 입력할 수 있습니다.");
		$('input[name=dng_val]').focus();
		return false;
	}
	if(limit_cnt == ''){
		alert("임계치 설정값을 입력해주세요");
		$('input[name=limit_cnt]').focus();
		return false;
	} else if(limit_cnt < 0 || limit_cnt > 2147483647){
		alert("임계치는 0 이상 2147483647 이하만 입력할 수 있습니다.")
		$('input[name=limit_cnt]').focus();
		return false;
	}
	if(limit_type=='4'||limit_type=='5'){
		if(limit_type_cnt ==''){
			alert("임계치에서 설정값(N)을 입력해주세요");
			$('input[name=limit_type_cnt]').focus();
			return false;
		}
	}
	if(indv_yn=='N' && system_seq == ''){
		alert("대상 시스템을 선택해주세요");
		$('input[name=system_seq]').focus();
		return false;
	}
	

	var detailForm = $('#detailForm').serialize();
	$.ajax({
		type : 'POST',
		url : rootPath + '/extrtBaseSetup/saveScenario.html',
		data : detailForm,	
		success : function(data) {
			alert("저장하였습니다.");
			moveList();
		}
	});
}

// 시나리오 삭제
function removeScenario(scen_seq) {
	var scen_seq = $("input[name=scen_seq]").val();
	if(confirm("삭제하시겠습니까?")) {
		$.ajax({
			type : 'POST',
			url : rootPath + '/extrtBaseSetup/findRuleTableByScenario.html',
			async:false,
			data : {
				"scen_seq" : scen_seq
			},	
			success : function(data) {
				if(data.result == 1) {
					alert("해당 상세시나리오를 먼저 삭제해주세요.");
				} else {
					$.ajax({
						type : 'POST',
						url : rootPath + '/extrtBaseSetup/removeScenario.html',
						data : {
							scen_seq : scen_seq
						},
						success : function(data) {
							alert("삭제하였습니다.");
							moveList();
						}
					});
				}
			}
		});
	}
}