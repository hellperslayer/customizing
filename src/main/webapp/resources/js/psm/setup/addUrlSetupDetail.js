/**
 * 추가URL상세 Javascript
 * @author yrchoo
 * @since 2015. 4. 24
 */

// 추가
function addAddUrl(){
	var except_url = $('input[name=except_url]').val();
	var url_desc = $('textarea[name=url_desc]').val();
	var use_yn = $('input[name=use_yn]:checked').val();
	var url_code = $('select[name=url_code]').val();
	var system_seq = $('select[name=system_seq]').val();
	
	if(except_url == '') {
		alert("URL를 입력해주세요");
		$('input[name=admin_user_id]').focus();
		return false;
	}
	if(url_desc == '') {
		alert("URL 설명을 입력해주세요");
		$('input[name=url_desc]').focus();
		return false;
	}
	if(use_yn == undefined) {
		alert("URL 사용여부를 선택해 주세요");
		$('input[name=use_yn]').focus();
		return false;
	}
	if(url_code == '') {
		alert("URL 속성을 선택해 해주세요");
		$('input[name=url_code]').focus();
		return false;
	}
		
	sendAjaxAddUrl("add");	
}

// 수정
function saveAddUrl(){
	var except_url = $('input[name=except_url]').val();
	var url_desc = $('textarea[name=url_desc]').val();
	
	if(except_url == '') {
		alert("URL을 입력해주세요");
		$('input[name=except_url]').focus();
		return false;
	}
	if(url_desc == '') {
		alert("URL 설명을 입력해주세요");
		$('input[name=url_desc]').focus();
		return false;
	}
		
	sendAjaxAddUrl("save");	
	
}

// 삭제
function removeAddUrl(){
	if(confirm("삭제하시겠습니까?")) {
		sendAjaxAddUrl("remove");
	}
}

// ajax call
function sendAjaxAddUrl(type){
	var $form = $("#addUrlDetailForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'except_url,url_desc';
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'add') {
		log_message_title = 'URL등록';
		log_action = 'INSERT';
	} else if (type == 'save') {
		log_message_title = 'URL수정';
		log_action = 'UPDATE';
	} else if (type == 'remove') {
		log_message_title = 'URL삭제';
		log_action = 'REMOVE';
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(addUrlDetailConfig[type + "Url"],$form.serialize(), ajaxAddUrl_successHandler, ajaxAddUrl_errorHandler, type);
}

// ajax call - 성공
function ajaxAddUrl_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = addUrlDetailConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}
		
		moveAddUrlList();
	}
}

// ajax call - 실패
function ajaxAddUrl_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	log_message += codeLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("URL 등록 실패하였습니다." + textStatus);
			break;
		case "save":
			alert("URL 수정 실패하였습니다." + textStatus);
			break;
		case "remove":
			alert("URL 삭제 실패하였습니다." + textStatus);
			break;
		default:
	}
}

// 목록으로 이동
function moveAddUrlList(){
	$("#addUrlDetailForm").attr("action",addUrlDetailConfig["listUrl"]);
	$("#addUrlDetailForm").submit();
}

