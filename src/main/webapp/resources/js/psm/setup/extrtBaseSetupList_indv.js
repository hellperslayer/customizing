/**
 * EmpUserMngtList Javascript
 * @author yjyoo
 * @since 2015. 04. 21
 */



var $clone;

$(document).ready(function() {
	$clone = $("#empUserListForm").clone();
});

// 페이지 이동
function goPage(num){
	$clone.attr("action", empUserListConfig["listUrl"]);
	$clone.find("input[name=page_num]").val(num);
	$clone.attr("style", "display: none;");
	$('body').append($clone);
	$clone.submit();
}

//엑셀 업로드 팝업 기능
function excelUpLoadList(){
	var excelUp = $(".popup_wrap");
	excelUp.attr("style", "display: block;");
}

function closeUpload(){
	var excelUp = $(".popup_wrap");
	var file = $("#file");
	excelUp.attr("style", "display: none;");
	file.replaceWith( file.clone(true));
}

function excelEmpUserUpLoad(){
	
	var data = $("#result").val();
	
		var options = {
				
				success : function(data) {
					var checkdata = "\"true\"";
					if(checkdata == data){
						alert("성공적으로 업로드 되었습니다!");
						moveEmpUserList();
					}else if(checkdata != data){
						alert("업로드에 실패하였습니다. (엑셀파일의 셀안에 공백이 들어있거나 필수 입력정보를 입력하였는지, 혹은 기존 소속명 외의 소속명이 포합되어 있는지 확인해주십시오.)");
					}
					
				},
				error : function(error) {
					alert("요청 처리 중 오류가 발생하였습니다.");
				}
		  };
		    
    $("#fileForm").ajaxSubmit(options);

    return false;
}


// 검색
function moveEmpUserList() {
	$("#empUserListForm").attr("action", empUserListConfig["listUrl"]);
	$("#empUserListForm input[name=page_num]").val('1');
	$("#empUserListForm").submit();
}

function searchEmpUserList() {
	$("#empUserListForm").attr("action", empUserListConfig["listUrl"]);
	$("#empUserListForm input[name=isSearch]").val('Y');
	$("#empUserListForm input[name=page_num]").val('1');
	$("#empUserListForm").submit();
}


// 상세 페이지 이동 (신규, 수정)
function findEmpUserMngtOne(emp_user_id, emp_user_name, dept_name, ip){
	$("#empUserListForm").attr("action", empUserListConfig["detailUrl"]);
	$('#empUserListForm').appendHiddenInput('emp_user_id', emp_user_id);
	//$("#empUserListForm input[name=emp_user_name]").val(emp_user_name);
	$('#empUserListForm').appendHiddenInput('dept_name', dept_name);
	$('#empUserListForm').appendHiddenInput('ip', ip);
	// 아래의 코드와 같다. 공통 js 참조
	// $('#empUserListForm').append($('<input type="hidden" name="emp_user_id" value="' + emp_user_id + '" />'));
	$("#empUserListForm").submit();
}

// 엑셀 다운로드
function excelEmpUserList() {
	$("#empUserListForm").attr("action", empUserListConfig["downloadUrl"]);
	$("#empUserListForm").submit();
}

/*function sendAjaxEmpUser(){
	var form = $("#fileForm");
	var type = "upload";
	var formData = new FormData();
	formData.append("file",$("#f2 input[name = file]")[0].files[0]);
	var log_message_title = '엑셀업로드';
	var log_action = 'INSERT';
	var log_message_params = 'file';
	var menu_id = $('input[name=current_menu_id]').val();
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id, file);

	sendAjaxPostRequest(empUserListConfig["uploadUrl"], formData, ajaxEmpUser_successHandler, ajaxEmpUser_errorHandler, type);
}*/

function ajaxDefaultSucCallback(data, dataType, actionType){
	
}

function ajaxDefaultErrCallback(XMLHttpRequest, textStatus, errorThrown, actionType){
	alert("ajax request Error : " + textStatus);
}


// 사원 ajax call - 성공
function ajaxEmpUser_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		alert("성공적으로 등록되었습니다.");
		moveEmpUserList();
	}
}

// 사원 ajax call - 실패
function ajaxEmpUser_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){	
	alert("엑셀 업로드에 실패하였습니다. 셀값에 빈공간이 있거나 값이 제대로 들어가있는지 확인해 주십시오." + textStatus);
}

function setPrctAll() {
	
	var extrtcond = $("select[name=rule_seq]").val();
	if(extrtcond == '' || extrtcond == null) { 
		alert("추출조건을 선택해주세요.");
		return;
	}
	
	var prct_val = $("input[name=prct_val]").val();
	if(prct_val == '' || prct_val == null) {
		alert("비율을 적어주세요.");
		$("input[name=prct_val]").focus();
		return;
	}
	
	var num_check = /^[0-9]*$/;
	if(!num_check.test(prct_val)) {
		alert("숫자만 적어주세요.");
		$("input[name=prct_val]").focus();
		return;
	}
	
	if(confirm("일괄 적용하시겠습니까?")) {
		sendAjaxIndv();
	}
}

//ajax call
function sendAjaxIndv(){
	var $form = $("#empUserListForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'rule_seq, prct_val';
	var menu_id = $('input[name=current_menu_id]').val();
	
	$form.appendLogMessageParamsInput('개인별 위험도 일괄수정', 'UPDATE', log_message_params, menu_id);
	
	sendAjaxPostRequest(empUserListConfig["saveUrl"],$form.serialize(), ajaxIndv_successHandler, ajaxIndv_errorHandler, "save");
}

// ajax call - 성공
function ajaxIndv_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = addUrlDetailConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		alert("성공적으로 적용되었습니다.");
		
		goList();
	}
}

// ajax call - 실패
function ajaxIndv_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	log_message += codeLogMessage[actionType + "Action"];
	
	alert("적용 실패하였습니다." + textStatus);
}

function goList() {
	$("#empUserListForm").attr("action", empUserListConfig["listUrl"]);
	$("#empUserListForm").submit();
}

function exDown(){
	$("#exdown").submit();
}


$(function(){
	$('input[type=text]').attr("onkeydown","javascript:if(event.keyCode==13){moveEmpUserList();}");
});

