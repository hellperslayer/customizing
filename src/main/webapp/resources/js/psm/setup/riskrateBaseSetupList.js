/**
 * 위험도설정 Javascript
 * @author yrchoo
 * @since 2015. 4. 24
 */

// 숫자만 입력가능
function onlyNumber() {
	var agt = navigator.userAgent.toLowerCase(); 
	if(!((event.keyCode >= 37 && event.keyCode <= 57) 
			|| event.keyCode == 13 
          || (event.keyCode >= 96 && event.keyCode <= 105)
          || event.keyCode == 8  || event.keyCode == 9)) {
         alert("숫자만 입력해주세요.");
         if (agt.indexOf("msie") != -1){
        	 event.returnValue = false;
         }
         else if (agt.indexOf("trident") != -1){
	         event.preventDefault();
         }
    } 
}

// 수정
function saveDngVal(){
	for (var i = 1; i < 6; i++) {
		var dng_val = $('input[name=dng_val_' + i + ']').val();
		if (dng_val == '' ) {
			alert("위험지수를 입력해주세요");
			$('input[name=dng_val_' + i + ']').focus();
			return;
		}
	}
		
	sendAjaxRiskrateBase("save");
	
}

// ajax call
function sendAjaxRiskrateBase(type){
	var $form = $("#riskrateBaseSetupForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'dng_cd, dng_val';
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'add') {
		log_message_title = 'URL등록';
		log_action = 'INSERT';
	} else if (type == 'save') {
		log_message_title = '위험지수 수정';
		log_action = 'UPDATE';
	} else if (type == 'remove') {
		log_message_title = 'URL삭제';
		log_action = 'REMOVE';
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(riskrateBaseSetupConfig[type + "Url"],$form.serialize(), ajaxRiskrateBase_successHandler, ajaxRiskrateBase_errorHandler, type);
}

// ajax call - 성공
function ajaxRiskrateBase_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = addUrlDetailConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}
		
		moveAddUrlList();
	}
}

// ajax call - 실패
function ajaxRiskrateBase_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	log_message += codeLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("URL 등록 실패하였습니다." + textStatus);
			break;
		case "save":
			alert("URL 수정 실패하였습니다." + textStatus);
			break;
		case "remove":
			alert("URL 삭제 실패하였습니다." + textStatus);
			break;
		default:
	}
}
