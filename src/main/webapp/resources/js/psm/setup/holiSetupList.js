/**
 * 휴일목록 Javascript
 * @author yrchoo
 * @since 2015. 4. 24
 */

// 검색
function moveHoliSetup() {
	$('input[name=page_num]').attr("value","1");
	$("#listForm").attr("action",holiListConfig["listUrl"]);
	$("#listForm").submit();
}

//상세 페이지 요청
function moveDetail(holi_dt){
	$("#listForm").attr("action",holiListConfig["detailUrl"]);
	$("#listForm input[name=holi_dt]").val(holi_dt);
	$("#listForm").submit();
}

function goPage(num){
	$("#listForm").attr("action",holiListConfig["listUrl"]);
	$("#listForm input[name=page_num]").val(num);
	$("#listForm").submit();
}

/**
 * 엑셀 다운로드 추가 2015/6/4
 */
function excelHoliSetup(){
//	if(arguments[0] >= 65535){
//		alert("엑셀다운로드는 65535건까지 가능합니다.");
//	}else{
	var $form = $("#listForm");
	var log_message_params = '';
	var menu_id = $('input[name=current_menu_id]').val();
	var log_message_title = '휴일설정관리 엑셀 다운로드';
	var log_action = 'EXCEL DOWNLOAD';
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
		$("#listForm").attr("action",holiListConfig["downloadUrl"]);
		$("#listForm").submit();
//	}
}

// 엑셀 업로드
function excelEmpUserUpLoad(){
	var data = $("#result").val();
		var options = {
				success : function(data) {
					var checkdata = "\"true\"";
					if(checkdata == data){
						alert("성공적으로 업로드 되었습니다!");
						moveEmpUserList();
					}else if(data == '\"collision\"'){
						alert("중복된 날짜가 있습니다.");
					}else if(checkdata != data){
						alert("업로드에 실패하였습니다. (엑셀파일의 셀안에 공백이 들어있거나 필수 입력정보를 입력하였는지 확인해주세요.)");
					}
				},
				error : function(error) {
					alert("요청 처리 중 오류가 발생하였습니다.");
				}
		  };
		var $form = $("#listForm");
		var log_message_params = '';
		var menu_id = $('input[name=current_menu_id]').val();
		var log_message_title = '개인정보설정 엑셀 다운로드';
		var log_action = 'EXCEL DOWNLOAD';
		
		$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
    $("#fileForm").ajaxSubmit(options);
    return false;
}

function exDown(){
	$("#exdown").submit();
}