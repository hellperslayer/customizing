
function goList() {
	$("#extrtBaseSetupDetailForm").attr("action", extrtBaseSetupConfig["listUrl"]);
	$("#extrtBaseSetupDetailForm").submit();
}

/**
 * 위험도설정 Javascript
 * @author yrchoo
 * @since 2015. 4. 24
 */

// 숫자만 입력가능
function onlyNumber() {
	var agt = navigator.userAgent.toLowerCase(); 
	if(!((event.keyCode >= 37 && event.keyCode <= 57) 
			|| event.keyCode == 13 
          || (event.keyCode >= 96 && event.keyCode <= 105)
          || event.keyCode == 8  || event.keyCode == 9)) {
         alert("숫자만 입력해주세요.");
         if (agt.indexOf("msie") != -1){
        	 event.returnValue = false;
         }
         else if (agt.indexOf("trident") != -1){
	         event.preventDefault();
         }
    } 
}

// 수정
function saveDngVal(){
	//var size = $("input[name^=prct_val]");
	var size = $('input[name=totalcnt]').val();
	for (var i = 0; i < size; i++) {
		var prct_val = $('input[name=prct_val_' + i + ']').val();
		if (prct_val == '' ) {
			alert("위험지수를 입력해주세요");
			$('input[name=prct_val_' + i + ']').focus();
			return;
		}
	}
	$('input[name=totalcnt]').attr("value", size);
	sendAjaxIndv();
	
}

// ajax call
function sendAjaxIndv(){
	var $form = $("#extrtBaseSetupDetailForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'rule_seq, prct_val';
	var menu_id = $('input[name=current_menu_id]').val();
	
	$form.appendLogMessageParamsInput('개인별 위험도 수정', 'UPDATE', log_message_params, menu_id);
	
	sendAjaxPostRequest(extrtBaseSetupConfig["saveUrl"],$form.serialize(), ajaxIndv_successHandler, ajaxIndv_errorHandler, "save");
}

// ajax call - 성공
function ajaxIndv_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = addUrlDetailConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		alert("성공적으로 수정되었습니다.");
		
		goList();
	}
}

// ajax call - 실패
function ajaxIndv_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	log_message += codeLogMessage[actionType + "Action"];
	
	alert("수정 실패하였습니다." + textStatus);
}

