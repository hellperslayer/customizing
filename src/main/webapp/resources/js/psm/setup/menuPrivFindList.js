/**
 * 매뉴매핑설정 Javascript
 * @author tjlee
 * @since 2014. 4. 10
 */

 // 상세로 이동
function fnMenuPrivFindList(){
	$("#detailSearchForm").attr("action",menuPrivFindConfig["privFindUrl"]);
	$("#detailSearchForm").submit();
}

// 페이지 이동
function goPagemenuPrivFindList(num){
	$("#detailSearchForm").attr("action",menuPrivFindConfig["privFindUrl"]);
	$("#detailSearchForm input[name=page_num]").val(num);
	$("#detailSearchForm").submit();
}

function moveMenuMappDetail(){
	$("#detailSearchForm").attr("action", menuPrivFindConfig["detailUrl"]);
	$("#detailSearchForm").submit();
}

function clearOption(){
	$("#addBtn").css("display","");
	$("#saveBtn").css("display","none");
	$("#clearBtn").css("display","none");
	
	$("#tag_regexp_seq").val("");
	$("#privacy_type").val("");
	$("textarea[name=regular_expression]").val("");
	$("input[name=get_group_num]").val("");
	$("#tag_regexp_seq").val("")
}

//개인정보탐지설정 추가
function addMenuPrivFind(){
	var regular_expression = $('textarea[name=regular_expression]').val();
	var privacy_type = $('#privacy_type').val();
	
	if(privacy_type == '') {
		alert("개인정보유형을 선택해주세요");
		return false;
	}
	if(regular_expression == ''){
		alert("정규식을 입력해주세요");
		$('input[name=regular_expression]').focus();
		return false;
	}
	
	var confirmVal = confirm("등록하시겠습니까?");
	if(confirmVal){
		sendAjaxmenuPrivFindSetup("add");	
	}else{
		return false;
	}
}

//개인정보탐지설정 업데이트전 값 넣기
function updateMenuPrivFindForm(regexp_seq, privacy_type, regular_expression, get_group_num, use_flag ){
	$("#addBtn").css("display","none");
	$("#saveBtn").css("display","");
	$("#clearBtn").css("display","");
	
	$("#tag_regexp_seq").val(regexp_seq);
	$("#privacy_type").val(privacy_type);
	$("textarea[name=regular_expression]").val(regular_expression);
	$("input[name=get_group_num]").val(get_group_num);
	if(use_flag == 'Y') {
		$("#checkbox1_1").prop('checked', true);
		$("#checkbox1_2").prop('checked', false);
	} else {
		$("#checkbox1_1").prop('checked', false);
		$("#checkbox1_2").prop('checked', true);
	}
}

//개인정보탐지설정 추가
function saveMenuPrivFind(){
	var regular_expression = $('textarea[name=regular_expression]').val();
	var privacy_type = $('#privacy_type').val();
	
	if(privacy_type == '') {
		alert("개인정보유형을 선택해주세요");
		return false;
	}
	if(regular_expression == ''){
		alert("정규식을 입력해주세요");
		$('input[name=regular_expression]').focus();
		return false;
	}
	
	var confirmVal = confirm("수정하시겠습니까?");
	if(confirmVal){
		sendAjaxmenuPrivFindSetup("save");	
	}else{
		return false;
	}
}

//개인정보탐지설정 삭제
function removeMenuPrivFind(seq){
	$("#tag_regexp_seq").val(seq);
	var confirmVal = confirm("삭제하시겠습니까?");
	if(confirmVal){
		sendAjaxmenuPrivFindSetup("remove");	
	}else{
		return false;
	}
}

//매뉴매핑 ajax call
function sendAjaxmenuPrivFindSetup(type){
	var $form = $("#menuPrivFindListForm");
	var log_message_title = '';
	var log_message_params='';
	var log_action = '';
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'add') {
		log_message_title = '매뉴개인정보탐지설정등록';
		log_action = 'INSERT';
	} else if (type == 'save') {
		log_message_title = '매뉴개인정보탐지설정수정';
		log_action = 'UPDATE';
	} else if (type == 'remove') {
		log_message_title = '매뉴개인정보탐지설정삭제';
		log_action = 'REMOVE';
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(menuPrivFindConfig[type + "Url"],$form.serialize(), ajaxmenuPrivFindSetup_successHandler, ajaxmenuPrivFindSetup_errorHandler, type);
}

// 매뉴매핑 ajax call - 성공
function ajaxmenuPrivFindSetup_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = menuPrivFindConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}
		
		fnMenuPrivFindList();
	}
}

// 매뉴매핑 ajax call - 실패
function ajaxmenuPrivFindSetup_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	log_message += codeLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("개인정보탐지설정 등록 실패하였습니다." + textStatus);
			break;
		case "save":
			alert("개인정보탐지설정 수정 실패하였습니다." + textStatus);
			break;
		case "remove":
			alert("개인정보탐지설정 삭제 실패하였습니다." + textStatus);
			break;
		default:
	}
}
