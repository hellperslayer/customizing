/**
 * 매뉴매핑설정 Javascript
 * @author tjlee
 * @since 2014. 4. 10
 */

 // 상세로 이동
function fnExtraBizLogMappSetupDetail(seq){
	$("input[name=seq]").attr("value",seq);
	$("#listForm").attr("action",extraBizLogMappSetupConfig["detailUrl"]);
	$("#listForm").submit();
}

// 페이지 이동
function goPage(num){
	$("#listForm").attr("action",extraBizLogMappSetupConfig["listUrl"]);
	$("#listForm input[name=page_num]").val(num);
	$("#listForm").submit();
}

// 검색
function searchExtraBizLogMappSetupList() {
	$('input[name=page_num]').attr("value","1");
	$('input[name=isSearch]').attr("value","Y");
	$("#listForm").attr("action",extraBizLogMappSetupConfig["listUrl"]);
	$("#listForm").submit();
}

function moveList(){
	$("#listForm").attr("action", extraBizLogMappSetupConfig["listUrl"]);
	$("#listForm").submit();
}

//매뉴매핑 추가
function addExtraBizLogMappSetup(){
	//var tag_seq = $('input[name=tag_seq]').val();
	var url = $('input[name=url]').val();
	//var system_seq = $('#system_seq').val();
	//var crud_type = $('#crud_type').val();
	
	/*if(tag_seq == '') {
		alert("매뉴매핑ID를 입력해주세요");
		$('input[name=tag_seq]').focus();
		return false;
	}*/
	if(url == '') {
		alert("URL을 입력해주세요");
		$('input[name=url]').focus();
		return false;
	}
	/*if(crud_type == ''){
		alert("수행업무를 선택해주세요");
		$('#crud_type').focus();
		return false;
	}*/
	/*if(system_seq == ''){
		alert("시스템을 선택해주세요");
		$('#system_seq').focus();
		return false;
	}*/
	
	var confirmVal = confirm("등록하시겠습니까?");
	if(confirmVal){
		sendAjaxExtraBizLogMappSetup("add");	
	}else{
		return false;
	}
}

function excelMappSetupUpload(){
	var data = $("#result").val();

	var options = {
			success : function(data) {
				var checkdata = "\"true\"";
				if(checkdata == data){
					alert("성공적으로 업로드 되었습니다!");
					moveList();
				}else if(checkdata != data){
					alert("업로드에 실패하였습니다. (엑셀파일의 셀안에 공백이 들어있거나 필수 입력정보를 입력하였는지 확인해주십시오.)");
				}
				
			},
			error : function(error) {
				alert("요청 처리 중 오류가 발생하였습니다.");
			}
	  };
	    
$("#fileForm").ajaxSubmit(options);

    return false;
}

function excelUpLoadList(){
	var excelUp = $(".popup_wrap");
	excelUp.attr("style", "display: block;");
}

function closeUpload(){
	var excelUp = $(".popup_wrap");
	var file = $("#file");
	excelUp.attr("style", "display: none;");
	file.replaceWith( file.clone(true));
}

function exDown(){
	$("#exdown").submit();
}

// 매뉴매핑 수정
function saveExtraBizLogMappSetup(){
	//var tag_seq = $('input[name=tag_seq]').val();
	var url = $('input[name=url]').val();
//	var system_seq = $('#system_seq').val();
//	var crud_type = $('#crud_type').val();
	
	/*if(tag_seq == '') {
		alert("매뉴매핑ID를 입력해주세요");
		$('input[name=tag_seq]').focus();
		return false;
	}*/
	if(url == '') {
		alert("URL을 입력해주세요");
		$('input[name=url]').focus();
		return false;
	}
	/*if(system_seq == ''){
		alert("시스템을 선택해주세요");
		$('#system_seq').focus();
		return false;
	}*/
	/*if(crud_type == ''){
		alert("수행업무를 선택해주세요");
		$('#crud_type').focus();
		return false;
	}*/
	var confirmVal = confirm("수정하시겠습니까?");
	if(confirmVal){
		sendAjaxExtraBizLogMappSetup("save");	
	}else{
		return false;
	}
	
}

// 매뉴매핑 삭제
function removeExtraBizLogMappSetup(){
	if(confirm("삭제하시겠습니까?")) {
		//$('#rule_seq').attr("name", "rule_seq");
		sendAjaxExtraBizLogMappSetup("remove");
	}
}

// 매뉴매핑 ajax call
function sendAjaxExtraBizLogMappSetup(type){
	var $form = $("#extraBizLogMappSetupDetailForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'tag_seq, url';
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'add') {
		log_message_title = '추가로그매핑등록';
		log_action = 'INSERT';
	} else if (type == 'save') {
		log_message_title = '추가로그매핑수정';
		log_action = 'UPDATE';
	} else if (type == 'remove') {
		log_message_title = '추가로그매핑삭제';
		log_action = 'REMOVE';
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(extraBizLogMappSetupConfig[type + "Url"],$form.serialize(), ajaxExtraBizLogMappSetup_successHandler, ajaxExtraBizLogMappSetup_errorHandler, type);
}

// 매뉴매핑 ajax call - 성공
function ajaxExtraBizLogMappSetup_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = indvifoTypeSetupConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}
		
		moveExtraBizLogMappSetupList();
	}
}

// 매뉴매핑 ajax call - 실패
function ajaxExtraBizLogMappSetup_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	log_message += codeLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("추가로그매핑 등록 실패하였습니다." + textStatus);
			break;
		case "save":
			alert("추가로그매핑 수정 실패하였습니다." + textStatus);
			break;
		case "remove":
			alert("추가로그매핑 삭제 실패하였습니다." + textStatus);
			break;
		default:
	}
}

// 목록으로 이동
function moveExtraBizLogMappSetupList(){
	$("#extraBizLogMappSetupDetailForm").attr("action",extraBizLogMappSetupConfig["listUrl"]);
	$("#extraBizLogMappSetupDetailForm").submit();
}

// 사용여부 체크 
function fnmenuMappSetupListUseYnCheck(){
	$("#menuMappSetupListForm").attr("action",menuMappSetupConfig["listUrl"]);
	
	if ($("input[name=use_yn]:checked")) {
		alert($("input[name=use_yn]").val());
	}else{
		$("input[name=use_yn]").attr("value", "N");
		alert($("input[name=use_yn]").val());
	}
	
	var checkbox_useyn = $("input[name=use_yn]:checked").val();
	alert(checkbox_useyn);
	$("#menuMappSetupListForm").submit();
}
/**
 * 엑셀 다운로드 추가 2015/6/4
 */
function excelMenuMappSetup(){
//	if(arguments[0] >= 65535){
//		alert("엑셀다운로드는 65535건까지 가능합니다.");
//	}else{
		
		var $form = $("#listForm");
		var log_message_params = '';
		var menu_id = $('input[name=current_menu_id]').val();
		var log_message_title = '메뉴매핑설정 엑셀 다운로드';
		var log_action = 'EXCEL DOWNLOAD';
		
		$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
		
		$("#listForm").attr("action",menuMappSetupConfig["downloadUrl"]);
		$("#listForm").submit();
//	}
}

function showExtLogDiv(type) {
	if (type == 1) {
		$("#extLogDiv").show();
	} else {
		$("#extLogDiv").hide();
	}
}