/**
 * 개인정보유형별설정 Javascript
 * @author tjlee
 * @since 2014. 4. 10
 */
 // 상세로 이동
function fnindvinfoTypeSetupDetail(privacy_seq){
	$("input[name=privacy_seq]").attr("value",privacy_seq);
	$("#listForm").attr("action",indvinfoTypeSetupConfig["detailUrl"]);
	$("#listForm").submit();
}

// 페이지 이동
function goPage(num){
	$("#listForm").attr("action",indvinfoTypeSetupConfig["listUrl"]);
	$("#listForm input[name=page_num]").val(num);
	$("#listForm").submit();
}

// 검색
function moveindvinfoTypeSetupList() {
	$('input[name=page_num]').attr("value","1");
	$('input[name=isSearch]').attr("value","Y");
	$("#listForm").attr("action",indvinfoTypeSetupConfig["listUrl"]);
	$("#listForm").submit();
}

// 참조테이블 검색
function moveColumnDetailList() {
	$('input[name=page_num]').attr("value","1");
	$("#listForm").attr("action",referenceManagerConfig["listUrl"]);
	$("#listForm").submit();
}

//개인정보유형 추가
function addindvinfoTypeSetup(){
	var privacy_seq = $('input[name=privacy_seq]').val();
	var privacy_desc =$('input[name=privacy_desc]').val();
	//var regular_expression = $('input[name=regular_expression]').val();
	
	var result_type_order = $('input[name=result_type_order]').val();
	if(result_type_order>19){
		alert("매핑코드는 1~18사이의 값만 사용할 수 있습니다.");
		$('input[name=result_type_order]').focus();
		return false;
	}
	
	if(privacy_seq == '') {
		alert("개인정보유형번호를 입력해주세요");
		$('input[name=privacy_seq]').focus();
		return false;
	}
	if(privacy_seq.length > 5) {
		alert("개인정보유형번호는 5자리 이하의 숫자만 입력해주세요.");
		$('input[name=privacy_seq]').focus();
		return false;
	}
	if(privacy_desc == '') {
		alert("개인정보유형명을 입력해주세요");
		$('input[name=privacy_desc]').focus();
		return false;
	}
	/*if(regular_expression == '') {
		alert("정규식을 입력해주세요");
		$('input[name=regular_expression]').focus();
		return false;
	}*/
	
	var confirmVal = confirm("등록하시겠습니까?");
	if(confirmVal){
		sendAjaxindvinfoTypeSetup("add");	
	}else{
		return false;
	}
}

//개인정보유형 수정
function saveindvinfoTypeSetup(){
	var privacy_seq = $('input[name=privacy_seq]').val();
	var privacy_desc = $('input[name=privacy_desc]').val();
	var regular_expression = $('textarea[name=regular_expression]').val();
	
	var result_type_order = $('input[name=result_type_order]').val();
	if(result_type_order>19){
		alert("매핑코드는 1~18사이의 값만 사용할 수 있습니다.");
		$('input[name=result_type_order]').focus();
		return false;
	}
	
	$('#privacy_seq').attr("name", "privacy_seq");
	
	if(privacy_seq == '') {
		alert("개인정보유형번호를 입력해주세요");
		$('input[name=privacy_seq]').focus();
		return false;
	}
	if(privacy_desc == '') {
		alert("개인정보유형명을 입력해주세요");
		$('input[name=privacy_desc]').focus();
		return false;
	}
	if(regular_expression == '') {
		alert("정규식을 입력해주세요");
		$('textarea[name=regular_expression]').focus();
		return false;
	}
	
	var confirmVal = confirm("수정하시겠습니까?");
	if(confirmVal){
		sendAjaxindvinfoTypeSetup("save");	
	}else{
		return false;
	}
	
}

//개인정보유형 삭제
function removeindvinfoTypeSetup(){
	if(confirm("삭제하시겠습니까?")) {
		$('#privacy_seq').attr("name", "privacy_seq");
		sendAjaxindvinfoTypeSetup("remove");
	}s
}

//개인정보유형 ajax call
function sendAjaxindvinfoTypeSetup(type){
	var $form = $("#indvinfoTypeSetupDetailForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'privacy_seq,privacy_desc,regular_expression';
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'add') {
		log_message_title = '개인정보유형등록';
		log_action = 'INSERT';
	} else if (type == 'save') {
		log_message_title = '개인정보유형수정';
		log_action = 'UPDATE';
	} else if (type == 'remove') {
		log_message_title = '개인정보유형삭제';
		log_action = 'REMOVE';
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(indvifoTypeSetupConfig[type + "Url"],$form.serialize(), ajaxIndvifoTypeSetup_successHandler, ajaxIndvifoTypeSetup_errorHandler, type);
}

//개인정보유형 ajax call - 성공
function ajaxIndvifoTypeSetup_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = indvifoTypeSetupConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}
		
		moveindvinfoTypeSetupListtoDetail();
	}
}

//개인정보유형 ajax call - 실패
function ajaxIndvifoTypeSetup_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){

	log_message += codeLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("개인정보유형 등록 실패하였습니다." + textStatus);
			break;
		case "save":
			alert("개인정보유형 수정 실패하였습니다." + textStatus);
			break;
		case "remove":
			alert("개인정보유형 삭제 실패하였습니다." + textStatus);
			break;
		default:
	}
}

//목록으로 이동
function moveindvinfoTypeSetupListtoDetail(){
	$("#indvinfoTypeSetupDetailForm").attr("action",indvifoTypeSetupConfig["listUrl"]);
	$("#indvinfoTypeSetupDetailForm").submit();
}

/**
 * 엑셀 다운로드 추가 2015/6/4
 */
function excelIndvinfoTypeSetup(){
//	if(arguments[0] >= 65535){
//		alert("엑셀다운로드는 65535건까지 가능합니다.");
//	}else{
		
		var $form = $("#listForm");
		var log_message_params = '';
		var menu_id = $('input[name=current_menu_id]').val();
		var log_message_title = '개인정보설정 엑셀 다운로드';
		var log_action = 'EXCEL DOWNLOAD';
		
		$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
		
		$("#listForm").attr("action",indvinfoTypeSetupConfig["downloadUrl"]);
		$("#listForm").submit();
//	}
}

function selColum() {
	var table_name = $("#table_name").val();
	
	$.ajax({
		type:'POST',
		url : rootPath + '/indvinfoTypeSetup/findColumByTableName.html',
		data: {
			table_name : table_name
		},
		success : function(data){
			setColumList(data);
		}
	});
}

function setColumList(data) {
	var select = document.getElementById("column_name");
	while (select.options.length) {
		select.remove(0);
    }
	var none = document.createElement("option");
	none.text = "----- 선 택 -----";
	none.value = "";
	select.add(none);
	
	var jsonData = JSON.parse(data);
	for(var i=0; i<jsonData.length; i++) {
		var option = document.createElement("option");
		option.text = jsonData[i].column_name;
		option.value = jsonData[i].column_name;
		select.add(option);
	}
}

function fnindvinfoTypeSetupDetail_kdic(privacy_table_info_seq){
	$("input[name=privacy_table_info_seq]").attr("value",privacy_table_info_seq);
	$("#listForm").attr("action",indvinfoTypeSetupConfig["detailUrl"]);
	$("#listForm").submit();
}