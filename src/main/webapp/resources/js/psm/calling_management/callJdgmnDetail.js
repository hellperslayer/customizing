//권한 ajax call
function sendAjaxCalling(type){
	var $form = $("#demand");
	
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'inserter_id';
	var menu_id = $('input[name=cll_dmnd_id]').val();
	if (type == 'add') {
		log_message_title = '소명판정';
		log_action = 'INSERT';
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(extrtCondbyInqConfig[type + "Url"], $form.serialize(), ajaxCalling_successHandler, ajaxCalling_errorHandler, type);
}

//권한 ajax call - 성공
function ajaxCalling_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = extrtCondbyInqConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}

		moveCallingList();
	}
}

// 권한 ajax call - 실패
function ajaxCalling_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){
	alert(actionType);
	alert(textStatus);
	log_message += authLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("권한 등록 실패하였습니다." + textStatus);
			break;
		case "save":
			alert("권한 수정 실패하였습니다." + textStatus);
			break;
		case "remove":
			alert("권한 삭제 실패하였습니다." + textStatus);
			break;
	}
}

//목록으로 이동
function moveCallingList(){
	$("#demand").attr("action",CallDemandDetailConfig["listUrl"]);
	$("#demand").submit();
}
