/**
 * 소명요청 리스트 
 * @author hwkim
 * @since 2015. 06. 18
 */

var $clone;

$(document).ready(function() {
	$clone = $("#adminUserListForm").clone();
	
	// 리스트 클릭시 소명요청 상세페이지로 이동
	$('tr[id*=tr-list]').click(function() {
		var $input = $(this).find('input[name="cll_dmnd_id"]');
		var $callingAllListForm = $('#callingAllListForm');
		
		$callingAllListForm.attr("method",'POST');
		$callingAllListForm.attr("action",CallingDemandConfig["detailUrl"]);
		$callingAllListForm.append($input);
		$callingAllListForm.appendHiddenInput('listUrl', CallingDemandConfig['listUrl']);
		$callingAllListForm.submit();
	});
});

//페이지 이동
function goPage(num){
	$clone.attr("action",CallingDemandConfig["listUrl"]);
	$clone.find("input[name=page_num]").val(num);
	$clone.attr("style", "display: none;");
	$('body').append($clone);
	$clone.submit();
}

function moveCallingAllList() {
	$('#callingAllListForm').attr("action", CallingDemandConfig['listUrl']);
	$('#callingAllListForm').submit();
}



