/**
 * callingDetail Javascript
 * @author hwkim
 * @since 2015. 06. 18
 */

// 최종 클릭된 emp_detail_seq 변경
// empDetail 리스트 클릭할 때마다 Ajax를 이용해 로그 리스트를 출력
function fnChangeCurrentSeq(seq) {

	var $cur_emp_detail_seq = $('#cur_emp_detail_seq');
	if ($cur_emp_detail_seq.val() != seq) {
		$cur_emp_detail_seq.val(seq);
		goPage(1);
	}
}

// biz_log List 리스트 보기
function goPage(page_num) {
	var emp_detail_seq = $('#cur_emp_detail_seq').val();
	if (emp_detail_seq == 0)
		return false;
	
	var data = {'emp_detail_seq':emp_detail_seq, 'page_num':page_num};
	sendAjaxGetRequest(callingDetailConfig['bizLogListUrl'], data, ajaxBizLog_successHandler, ajaxBizLog_errorHandler, '');
}

// biz_log List ajax call(콜백 : 성공)
function ajaxBizLog_successHandler(data, dataType, actionType){
	var $div = $('#div_biz_log_list');
	var $tbody = $('#tbd_biz_log_list');
	
	$div.hide();
	if (data.bizLogList.length < 1) {
		var $tr = $('<tr />').append($('<td colspan="5" align="center">데이터가 없습니다.</td>'));
		$tbody.html($tr);
		$div.show();
		return;
	}
	
	var htmlString = '';
	$(data.bizLogList).each(function(index, log) { 
		htmlString += '<tr';
		if (index % 2 != 0)
			htmlString += ' class="tr_gray"';
		
		htmlString += '>';
		//htmlString += '<tr style="cursor: pointer;">\n';
		htmlString += '	<td style="text-align: center;">' + check_null(log.proc_date) + '</td>\n';
		htmlString += '	<td style="text-align: center;">' + check_null(log.proc_time) + '</td>\n';
		htmlString += '	<td style="text-align: center;">' + check_null(log.user_ip) + '</td>\n';
		htmlString += '	<td style="text-align: center;">' + check_null(log.scrn_name) + '</td>\n';
		htmlString += '	<td style="text-align: center;">' + check_null(log.req_url) + '</td>\n';
		htmlString += '</tr>\n';
	});
	
	$tbody.html(htmlString);
	
	var pageNavigator = new PageNavigator(String(data.search.size), String(data.search.page_num), String(data.search.total_count), '10');
 	pageNavigator.showInfo(true);
 	$("#pagingframe").html(pageNavigator.getHtml());
	
	$div.show();
}

// biz_log List ajax call(콜백 : 실패)
function ajaxBizLog_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){
	var $div = $('#div_biz_log_list');
	var $tbody = $('#tbd_biz_log_list');
	var $tr = $('<tr />').append($('<td colspan="5" align="center">데이터를 일시적으로 가져올 수 없습니다.</td>'));
	$div.hide();
	$tbody.html($tr);
	$div.show();
}

function moveCallingReplyUserList() {
	var $form = $("#callingDetailForm");
	$form.attr('action', callingDetailConfig['replyListUrl']);
	$form.submit();
}

function moveCallingList() {
	var $form = $("#callingDetailForm");
	$form.attr('action', callingDetailConfig['listUrl']);
	$form.submit();
}

// 관리자 ajax call
function sendAjaxCalling(type){
	var $form = $("#callingDetailForm");
	var log_message_title = '';
	var log_action = '';
	var log_message_params = '';
	if (type == 'reply')
		log_message_params = 'cll_dmnd_id,reply';
	else
		log_message_params = 'cll_dmnd_id,judge';
	
	var menu_id = $('input[name=current_menu_id]').val();
	
	if (type == 'reply') {
		log_message_title = '소명응답';
		log_action = 'INSERT';
	} else if (type == 'judge') {
		log_message_title = '소명판정';
		log_action = 'INSERT';
	}

	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	sendAjaxPostRequest(callingDetailConfig[type + "Url"],$form.serialize(), ajaxCalling_successHandler, ajaxCalling_errorHandler, type);
}

// biz_log List ajax call(콜백 : 성공)
function ajaxCalling_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = adminUserDetailConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		alert("성공적으로 등록되었습니다.");
		switch(actionType){
			case "reply":
				//moveCallingReplyUserList();
				break;
			case "judge":
				
				break;
		}
		moveCallingList();
	}
}

// biz_log List ajax call(콜백 : 실패)
function ajaxCalling_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){
	var	log_message = codeLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "reply":
			alert("소명응답 등록 실패하였습니다." + textStatus);
			break;
		case "judge":
			alert("소명판정 등록 실패하였습니다." + textStatus);
			break;
		default:
	}
}

// 검색
function movePreviousList() {
	$("#callingDetailForm").attr("action", callingDetailConfig["listUrl"]);
	$("#callingDetailForm input[name=page_num]").val('1');
	$("#callingDetailForm").submit();
}

$(document).ready(function() {
	
	if ($('#cur_emp_detail_seq').val() > 0)
		goPage(1);
	
	$('#btnAddReply').click(function() {
		
		var $reply = $('textarea[name="reply"]');
		if ($.trim($reply.val()).length == 0) {
			alert('열람사유를 입력해주세요');
			$reply.focus();
			return false;
		}
		
		sendAjaxCalling('reply');
	});
	
	$('#btnAddJudge').click(function() {
		
		var $judge = $('textarea[name="judge"]');
		if ($.trim($judge.val()).length == 0) {
			alert('판정의견을 입력해주세요');
			$judge.focus();
			return false;
		}
		
		var $judge_status = $('select[name="judge_result"]');
		if ($judge_status.val() == '') {
			alert('판정을 선택해주세요');
			$judge_status.focus();
			return false;
		}
		
		sendAjaxCalling('judge');
	});
});
