/**
 * 소명답변 리스트, 상세 js 
 * @author tjlee
 * @since 2015. 5. 13
 */

//검색
function moveCallingReplyUser() {
	$("#listForm").attr("action",callingReplyUserConfig["listUrl"]);
	$('#listForm').find('input[name=page_num]').val('1');
	$("#listForm").submit();
}

function fnCallingReplyUserDetail(id) {
	$("#listForm").attr("action",callingReplyUserConfig["detailUrl"]);
	$('#listForm').appendHiddenInput('cll_dmnd_id', id);
	$('#listForm').appendHiddenInput('listUrl', callingReplyUserConfig['listUrl']);
	$("#listForm").submit();
}

// 소명답변
function fnCallingReplyUserReply(detailCll_dmnd_id){
	$('input[name=detailCll_dmnd_id]').attr('value',detailCll_dmnd_id);
	
	$('#callingReplyUserDetailForm').attr("action",callingReplyUserConfig["replyUrl"]);
	$("#callingReplyUserDetailForm").submit();
}

// 목록으로
function fnCallingReplyUserReplyList(){
	$('#callingReplyUserDetailForm').attr("action",callingReplyUserConfig["listUrl"]);
	$("#callingReplyUserDetailForm").submit();
}


