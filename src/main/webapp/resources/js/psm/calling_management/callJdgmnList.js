/**
 * 소명판정목록 Javascript
 * @author yrchoo
 * @since 2015. 4. 24
 */

// 검색
function moveCallJdgmn() {
	$("#callJdgmnListForm").attr("action",CallJdgmnListConfig["listUrl"]);
	$("#callJdgmnListForm").submit();
}

//상세 페이지 요청
function fnCallJdgmnDetail(){
	$("#CallJdgmnDetailForm").attr("action",CallJdgmnListConfig["detailUrl"]);
	$("#CallJdgmnDetailForm").submit();
}

//페이지이동
function goPage(num){
	$("#callJdgmnListForm").attr("action",CallJdgmnListConfig["listUrl"]);
	$("#callJdgmnListForm input[name=page_num]").val(num);
	$("#callJdgmnListForm").submit();
}