/**
 * 추출조건별조회 Javascript
 * @author tjlee
 * @since 2014. 4. 10
 */
	 	
 // 상세로 이동
function fnextrtCondbyInqDetail(log_seq,proc_date){
	$("input[name=detailEmpCd]").attr("value",log_seq);
	$("input[name=detailOccrDt]").attr("value",proc_date);
	
	$("#extrtCondbyInqDetailForm").attr("action",extrtCondbyInqConfig["detailUrl"]);
	$("#extrtCondbyInqDetailForm").submit();
}

// 상세에서 전체로그 상세로 이동
function fnAllLogInqDetail(log_seq,proc_date){
	$("input[name=detailLogSeq]").attr("value",log_seq);
	$("input[name=detailProcDate]").attr("value",proc_date);
	$("input[name=bbs_id]").attr("value","extrtCondbyInq");
	
	$("#listForm").attr("action",allLogInqDetailConfig["detailUrl"]);
	$("#listForm").submit();
}

// 페이지 이동
function goPage(num){
	$("#listForm").attr("action",extrtCondbyInqConfig["listUrl"]);
	$("#listForm input[name=page_num]").val(num);
	$("#listForm").submit();
}

// 검색
function moveextrtCondbyInqList() {
	$('input[name=page_num]').attr("value","1");
	$("#listForm").attr("action",extrtCondbyInqConfig["listUrl"]);
	$("#listForm").submit();
}

// 목록으로 (추출조건별조회화면)
function goExtrtCondbyInqList() {
	$("#listForm").attr("action",extrtCondbyInqConfig["goExtrtCondbyInqListUrl"]);
	$("#listForm").submit();
}

// 목록으로 (위험도별조회화면)
function goEmpDetailInqList() {
	$("#listForm").attr("action",extrtCondbyInqConfig["goEmpDetailInqListUrl"]);
	$("#listForm").submit();
}

// 기간선택 검색조건
function initDaySelect(){
	/*if($("#daySelect").val() == "Day"){
		$(".search_fr").datepicker("setDate","-0d");
		$(".search_to").datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "WeekDay"){
		$(".search_fr").datepicker("setDate","-7d");
		$(".search_to").datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "MonthDay"){
		$(".search_fr").datepicker("setDate","-1m");
		var setDateStart = $(".search_fr").val();
		setDateStart = setDateStart.substr(0,8);
		setDateStart = setDateStart + "01";
		$(".search_fr").val(setDateStart);
		$(".search_to").datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "YearDay"){
		$(".search_fr").datepicker("setDate","-0d");
		var setDateStart = $(".search_fr").val();
		setDateStart = setDateStart.substr(0,5);
		setDateStart = setDateStart + "01-01";
		$(".search_fr").val(setDateStart);
		$(".search_to").datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "TotalMonthDay"){
		$(".search_fr").datepicker("setDate","-1m");
		var setDateStart = $(".search_fr").val();
		setDateStart = setDateStart.substr(0,8);
		setDateStart = setDateStart + "01";
		$(".search_fr").val(setDateStart);
		var setDateend =  $(".search_to").val()
		$(".search_to").datepicker("setDate","0");
		setDateend = setDateend.substr(8,10);
		$(".search_to").datepicker("setDate","-"+setDateend+"d");
	}else{
		dateDisabled(false);
	}*/
	if($("#daySelect").val() == "Day"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "WeekDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-7d");
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "MonthDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-1m");
		var setDateStart = $("#search_fr").val();
		setDateStart = setDateStart.substr(0,8);
		setDateStart = setDateStart + "01";
		$("#search_fr").val(setDateStart);
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "YearDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
		var setDateStart = $("#search_fr").val();
		setDateStart = setDateStart.substr(0,5);
		setDateStart = setDateStart + "01-01";
		$("#search_fr").val(setDateStart);
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-0d");
	}else if($("#daySelect").val() == "TotalMonthDay"){
		$("#search_fr").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-1m");
		var setDateStart = $("#search_fr").val();
		setDateStart = setDateStart.substr(0,8);
		setDateStart = setDateStart + "01";
		$("#search_fr").val(setDateStart);
		var setDateend =  $("#search_to").val();
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","0");
		setDateend = setDateend.substr(8,10);
		$("#search_to").datepicker({format:"yyyy-mm-dd"}).datepicker("setDate","-"+setDateend+"d");
	}else{
		dateDisabled(false);
	}
}

function dateDisabled(bool){
	$('.search_fr').attr('disabled', bool);
	$('.search_to').attr('disabled', bool);
	$('.ui-datepicker-trigger').css('visibility', bool == true ? 'hidden' : 'visible');
	$('.ui-datepicker-trigger').css('visibility', bool == true ? 'hidden' : 'visible');
}

// 상세창 클릭
function fnExtrtDetailLog(occr_dt,emp_user_id,emp_detail_seq){
	$("input[name=detailOccrDt]").attr("value",occr_dt);
	$("input[name=detailEmpCd]").attr("value",emp_user_id);
	$("input[name=detailEmpDetailSeq]").attr("value",emp_detail_seq);
	$("input[name=bbs_id]").attr("value","callingDemand");
	
	$("#listForm").attr("action",extrtCondbyInqConfig["detailUrl"]);
	$("#listForm").submit();
}

function fnExtrtCondbyInqListTrClick(rule_cd){
	$('input[name=detailRuleCd]').attr("value",rule_cd);
	var $lstForm = $('#listForm');
	$lstForm.submit();
}

/**
 * 엑셀 다운로드 추가 2015/6/4
 */
function excelExtrtCondbyInqList(){
	$("#listForm").attr("action",extrtCondbyInqConfig["downloadUrl"]);
	$("#listForm").submit();
}

//소명체크박스 전체선택
function fnallCheck(){
	var chkstatus = $('#allCheck').is(":checked");
	if(chkstatus){
		$('input[name^=somyungCheck]').prop('checked', true);
	}else{
		$('input[name^=somyungCheck]').prop('checked', false);
	}
}

//권한 ajax call
function sendAjaxCalling(type){
	var $form = $("#listForm");
	
	var log_message_title = '';
	var log_action = '';
	var log_message_params = 'inserter_id';
	var menu_id = $('input[name=current_menu_id]').val();
	if (type == 'add') {
		log_message_title = '소명요청';
		log_action = 'INSERT';
	}
	
	$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
	
	sendAjaxPostRequest(extrtCondbyInqConfig[type + "Url"], $form.serialize(), ajaxCalling_successHandler, ajaxCalling_errorHandler, type);
}

//권한 ajax call - 성공
function ajaxCalling_successHandler(data, dataType, actionType){
	if(data.hasOwnProperty("statusCode")) {
		var statusCode = data.statusCode;
		if(statusCode == 'SYS006V') {
			location.href = extrtCondbyInqConfig['loginPage'];
			return false;
		}
		
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			alert(message);
		}
	}else{
		switch(actionType){
			case "add":
				alert("성공적으로 등록되었습니다.");
				break;
			case "save":
				alert("성공적으로 수정되었습니다.");
				break;
			case "remove":
				alert("성공적으로 삭제되었습니다.");
				break;
		}

		moveCallingList();
	}
}

// 권한 ajax call - 실패
function ajaxCalling_errorHandler(XMLHttpRequest, textStatus, errorThrown, actionType){
	alert(actionType);
	alert(textStatus);
	log_message += authLogMessage[actionType + "Action"];
	
	switch(actionType){
		case "add":
			alert("권한 등록 실패하였습니다." + textStatus);
			break;
		case "save":
			alert("권한 수정 실패하였습니다." + textStatus);
			break;
		case "remove":
			alert("권한 삭제 실패하였습니다." + textStatus);
			break;
	}
}

//목록으로 이동
function moveCallingList(){
	$("#listForm").attr("action",extrtCondbyInqConfig["listUrl"]);
	$("#listForm").submit();
}

$(document).ready(function() {
	
	// 모두 선택 handling
	$('#allCheck').click(function() {
		if ($(this).is(':checked')) {
			$('input[type=checkbox][name*=empDetailList]').each(function(index) {
				if (!$(this).attr('disabled'))
					$(this).attr('checked', true);
			});
		}
		else
			$('input[type=checkbox][name*=empDetailList]').attr('checked', false);
	});
	
	// 결재선 지정 라디오 박스 선택에 따른 handling
	$('input[name=use_approval]').change(function() {
		if ($(this).val() == 'YES' && $('#tr-approval').is(':hidden')){
			$('#tr-approval').show();
			var adminInfo = $('#myname').val();
			
			// 사용일때 세션사용중인 관리자를 한줄 추가
			var $td_approval = $('<td/>', {'text': adminInfo.split('_')[1] + '(' + adminInfo.split('_')[0] + ') - ' + adminInfo.split('_')[2]})
			.appendHiddenInput('approvalList[0].approver_id', adminInfo.split('_')[0]);
			var $tr_approval = $('<tr/>', {'id' : 'tr-dynamic0'}).append($('<th/>', {'text' : '1차 결재자'})).append($td_approval);
			$('#auto-demand').before($tr_approval);
		}
	
		if ($(this).val() == 'NO' && $('#tr-approval').is(':visible')) {
			$('#tr-approval').hide();
			$('tr[id^=tr-dynamic]').each(function() {
				$(this).remove();
			});
		}
	});
	
	// 결재자 추가
	$('#btnAdd').click(function() {
		var selectedAdmin = $('#selectAdmin').val();
		if (selectedAdmin == '') {
			alert('결재자가 선택되지 않았습니다.');
			$('#selectAdmin').focus();
			return false;
		}
		
		var adminInfos = selectedAdmin.split('_');
		var admin_user_id = adminInfos[0];
		var admin_user_name = adminInfos[1];
		var admin_dept_id = adminInfos[2];
		
		var isBreak = false;
		$('input[name^="approvalList"]').each(function() {
			if ($(this).val() == admin_user_id) {
				alert('이미 추가된 결재자 입니다.');
				isBreak = true;
				return;
			}
		});
		if (isBreak) return false;
		
		var dynamicIndex = $('tr[id^=tr-dynamic]').size();
		
		var $td_approval = $('<td/>', {'text': admin_user_name + '(' + admin_user_id + ') - ' + admin_dept_id})
							.appendHiddenInput('approvalList[' + dynamicIndex + '].approver_id', admin_user_id)
							.append($('<img id="btnDel" src="' + contextPath + '/resources/image/common/bt_del.gif" border="0" style="cursor: pointer;" />'));
		
		var $tr_approval = $('<tr/>', {'id' : 'tr-dynamic' + dynamicIndex}).append($('<th/>', {'text' : (dynamicIndex + 1) + '차 결재자'}))
									.append($td_approval);
		
		$('#auto-demand').before($tr_approval);
	});
	
	// 결재자 삭제
	$('.table_td').on('click', '#btnDel', function() {
		var $removed = $(this).closest('tr');
		$removed.nextAll('tr[id^=tr-dynamic]').each(function () {
			var oldIndex = $(this).attr('id').replace('tr-dynamic', '');
			var newIndex = (parseInt(oldIndex) - 1).toString();
			$(this).attr('id', 'tr-dynamic' + newIndex);
			$(this).children('th').html(oldIndex + '차 결재자');
			$(this).children('input[name^=approvalList]').attr('name', 'approvalList[' + newIndex + '].approver_id');
		});
		$removed.remove();
	});
	
	// 소명 요청
	$('#executeCalling').click(function() {
		if (!custom_validate())
			return false;
		
		$('input[name^="emp_detail_seq"]').each(function() {
			if (!$(this).is(':checked'))
				$(this).val(0);
		});
		
		sendAjaxCalling('add');
	});
	
	// 소명 요청 전 validation check
	function custom_validate() {
		var result = true;
		// check box validate 
		if ($('input[name*=empDetailList]:checked').length < 1) {
			alert('1개 이상 선택하셔야합니다.');
			return false;
		}
		
		if ($('input[name="use_approval"]:checked').val() == 'YES' && $('input[name^="approvalList"]').length < 1) {
			alert('지정된 결재선이 없습니다.');
			$('select[name="selectAdmin"]').focus();
			return false;
		}
		
		// empty check
		$('.not_empty').each(function() {
			if ($(this).val() == null || $(this).val() == '') {
				alert('필수정보를 입력하십시오.');
				$(this).focus();
				result = false;
				return false;
			}
		});
		
		// 소명요청사유 empty check
		if($('callingDemandTextArea').val() == '' || $('autoCallingDemandSelectBox').val()==''){
			alert('필수정보를 입력하십시오.');
			$(this).focus();
			result = false;
			return false;
		}
		return result;
	}
	
	// 자동소명사용여부 라디오 박스 선택에 따른 handling
	$('#callingDemandTextArea').show();
	$('input[name=autoCallingDemand]').change(function() {
		if ($(this).val() == 'Y') {
			$('#callingDemandTextArea').hide();
			$('#autoCallingDemandSelectBox').show();
			$('#callingDemandTextArea').prop("name","");
			$('#autoCallingDemandSelectBox').prop("name","reasonList[0].reason");
			//alert('셀박 : ' + $('#autoCallingDemandSelectBox').attr("name"));
			//alert('텍박 : ' + $('#callingDemandTextArea').attr("name"));
		}
		if ($(this).val() == 'N') {
			$('#callingDemandTextArea').show();
			$('#autoCallingDemandSelectBox').hide();
			$('#callingDemandTextArea').prop("name","reasonList[0].reason");
			$('#autoCallingDemandSelectBox').prop("name","");
			//alert('셀박 : ' + $('#autoCallingDemandSelectBox').attr("name"));
			//alert('텍박 : ' + $('#callingDemandTextArea').attr("name"));
		}
	});
});