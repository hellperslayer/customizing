Date.prototype.format = function(f) {
    if (!this.valueOf()) return " ";
 
    var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
    var d = this;
     
    return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
        switch ($1) {
            case "yyyy": return d.getFullYear();
            case "yy": return (d.getFullYear() % 1000).zf(2);
            case "MM": return (d.getMonth() + 1).zf(2);
            case "dd": return d.getDate().zf(2);
            case "E": return weekName[d.getDay()];
            case "HH": return d.getHours().zf(2);
            case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2);
            case "mm": return d.getMinutes().zf(2);
            case "ss": return d.getSeconds().zf(2);
            case "a/p": return d.getHours() < 12 ? "오전" : "오후";
            default: return $1;
        }
    });
};

String.prototype.string = function(len){var s = '', i = 0; while (i++ < len) { s += this; } return s;};
String.prototype.zf = function(len){return "0".string(len - this.length) + this;};
Number.prototype.zf = function(len){return this.toString().zf(len);};


function savereport(report_seq){
	var defaultTitle = new Date().format('yyyy년 MM월 정기점검');
	var title = prompt('보고서 제목을 입력해 주세요',defaultTitle);
	if(title == null){
		alert('보고서 제목을 입력해주세요');
	}else if(confirm('보고서를 저장 하시겠습니까?')){
		var dataJson = "{";
		var procDate = "";
		var datacheck = false;
		$('.saveInput').each(function(idx,val){
			var key = $(val).attr('id');
			var value = $(val).val();
			if(value != ""){
				dataJson = dataJson + "\"" + key + "\"" + ":" + "\"" + value + "\"" + ",";
				datacheck = true;
			}
		});
		$('.saveTextarea').each(function(idx,val){
			var key = $(val).attr('id');
			var value = $(val).val();
			value = value.replace(/(?:\r\n|\r|\n)/g, '<br/>');
			if(value != ""){
				dataJson = dataJson + "\"" + key + "\"" + ":" + "\"" + value + "\"" + ",";
				datacheck = true;
			}
		});
		$('.saveCheckbox').each(function(idx,val){
			var key = $(val).attr('id');
			var value = $(val).prop("checked");
			if(value != ""){
				dataJson = dataJson + "\"" + key + "\"" + ":" + value + ",";
				datacheck = true;
			}
		});
		
		if(datacheck){
			dataJson = dataJson.slice(0,-1);
			dataJson = dataJson + "}";
			
			console.log(dataJson);
			var parent = window.opener;
			$.ajax({
				url: rootPath + '/report/inspectionDataSave.html',
				type:'POST',
				data: {
					report_seq : report_seq,
					report_data : dataJson,
					proc_date : proc_date,
					search_fr : search_fr,
					search_to : search_to,
					report_type : 'sw',
					report_title : title
				},
				dataType: "json",
				success: function(data){
					if(data == "success"){
						alert("저장완료");
						parent.location.reload();
						window.close();
					}else if(data == "fail"){
						alert("오류")
					}
				},
				error: function(){
					alert("오류")
				}
			});
		}else{
			alert("보고서를 작성 후 저장해주세요.");
		}
	}
}

function inspectionDataLoad(){
	$.ajax({
		url: rootPath + '/report/inspectionDataLoad.html',
		type:'POST',
		data: {
			report_seq : report_seq,
			report_type : 'sw'
		},
		dataType: "json",
		success: function(data){
			console.log(data);
			var jsonObj = JSON.parse(data);
			Object.keys(jsonObj).forEach(function(key){
				if($('#'+key).hasClass('saveCheckbox')){
					$('#'+key).prop("checked",jsonObj[key]);
				}else if($('#'+key).hasClass('saveTextarea')){
					var editData = jsonObj[key].split('<br/>').join("\r\n");
					$('#'+key).val(editData);
				}else{
					$('#'+key).val(jsonObj[key]);
				}
			});
		},
		error: function(){
		}
	});
}

$(document).ready(function(){
	if(report_seq != ''){
		inspectionDataLoad()
	}
});