var imageList = new Array();
var imageList = {};

$(document).ready(function() {
	if(download_type == 'pdf') {
		$("#buttonDiv").css("display","none");
		$(".downloadChartClass").css("height","320px");
		$(".downloadChartClass").css("width","850px");
		$(".downloadClass").css("width","850px");
		$(".downloadClass_left").css("width","200px");
		$(".downloadClass_right").css("width","100px");
		$(".HStyle5").css("width","850px");
		for (var x = 0; x < AmCharts.charts.length; x++) {
			$("#"+AmCharts.charts[x].div.id).css("width","850px");
		}
		setInterval(function() {
			setChart.init();
		},500);
		
		
	} else {
		/*$(".downloadClass").css("width","750px");
		$(".downloadChartClass").css("width","750px");
		for (var x = 0; x < AmCharts.charts.length; x++) {
			$("#"+AmCharts.charts[x].div.id).css("width","750px");
		}*/	
		setChart.init();
	}
	
	if(period_type == 5){
		$('#saveButton').hide();
	}
});

function setCharts() {
	setchart_new1();
	setchart_new1_1();
	setchart_new1_2();
	setchart_new2();
	setchart_new3();
	setchart_new4();
}

var setChart = {
		init : function(){
			var chartlist = $('.downloadChartClass');
			for(var i=0 ; i < chartlist.length ; i++){
				eval('setChart.'+chartlist[i].id+'()');
			}
		},
		chart_new1 : function(){setchart_new1()},
		chart_new1_nofullscan : function(){setchart_new1()},
		chart_new1_1 : function(){setchart_new1_1()},
		chart_new1_2 : function(){setchart_new1_2()},
		chart_new2 : function(){setchart_new2()},
		chart_new2_nofullscan : function(){setchart_new2()},
		chart_new3 : function(){setchart_new3()},
		chart_new4 : function(){setchart_new4()}
}

function saveAsDoc() {
	
	$.ajax({
		type : 'POST',
		url : rootPath + '/report/getImages.html?type=1',
		data : imageList,
		/*data : {
			"imageList" : imageList,
			"report_type" : "report1"
		}*/
		success : function(data) {
			/*for(var i=0; i<imageList.length; i++) {
				var image = "#chartImage" + i;
				$(image).css("display", "block");
			}*/
			var i=0;
			for(var img in imageList) { 
				var image = "#chartImage" + i;
				$(image).css("display", "block");
				i++;
			}
			
			var date = new Date();
			var year = date.getFullYear();
			var month = new String(date.getMonth() + 1);
			var day = new String(date.getDate());
			if(month.length == 1){ 
				month = "0" + month; 
			} 
			if(day.length == 1){ 
				day = "0" + day; 
			} 
			var regdate = year + "" + month + "" + day;
			var agent = navigator.userAgent.toLowerCase();
			if (agent.indexOf("msie") != -1) {
				saveAsPage(regdate);
			} else {
				exportHTML(regdate);
			}
			
			self.close();
		}
	});
}


function setchart_new1() {
	
	//alert("full scan : " + use_fullscan);
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/donwload_reportDetail_chart_new1.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			if(use_fullscan == 'Y')
				drawchart_new1(data);
			else
				drawchart_new1_nofullscan(data);
		}
	});
}

function drawchart_new1(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		if(i==0) 
			$("#reportBySystem").html(jsonData[i].system_name);
		var value = {"system_name": jsonData[i].system_name,"logCnt": jsonData[i].logCnt,"privacyCnt": jsonData[i].privacyCnt};
		dataProvider.push(value);
	}
	var title1 = "개인정보 처리량";
	var title2 = "개인정보 이용량";
	
	var chart_new1 = AmCharts.makeChart( "chart_new1", {
	  "type": "serial",
	  "theme": "light",
	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "title": "개인정보검출(건)"
	  } ],
	  "legend": {
	        "useGraphSettings": true
		},
	  "startDuration": 0,
	  "graphs": [ 
		  {
				"balloonText": "[[title]]:[[value]]",
				"title": title1,
				"id": "AmGraph-2",
				"lineAlpha": "10",
				"bullet" : "round",
				"type": "line",
				"valueField": "logCnt",
				"labelPosition": "bottom",
				"labelText": "[[value]]"
			},
			{
				"balloonText": "[[title]]:[[value]]",
				"title": title2,
				"id": "AmGraph-1",
				"lineAlpha": "10",
				"bullet" : "round",
				"type": "line",
				"valueField": "privacyCnt",
				"labelPosition": "bottom",
				"labelText": "[[value]]"
	  } ],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "system_name",
	  "categoryAxis": {
	    "gridPosition": "start",
		"axisAlpha": 0,
		"tickLength": 0,
	    "labelRotation": 45
	  },
	  "export": {
	    "enabled": true
	  }

	} );
	$("#chart_new1").css("display", "");
}


function drawchart_new1_nofullscan(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		if(i==0) 
			$("#reportBySystem").html(jsonData[i].system_name);
		var value = {"system_name": jsonData[i].system_name,"logCnt": jsonData[i].logCnt,"privacyCnt": jsonData[i].privacyCnt};
		dataProvider.push(value);
	}
	var title1 = "개인정보 처리량";
	//var title2 = "개인정보 이용량";
	
	var chart_new1 = AmCharts.makeChart( "chart_new1_nofullscan", {
	  "type": "serial",
	  "theme": "light",
	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "title": "개인정보검출(건)"
	  } ],
	  "legend": {
	        "useGraphSettings": true
		},
	  "startDuration": 0,
	  "graphs": [ 
		  {
				"balloonText": "[[title]]:[[value]]",
				"title": title1,
				"id": "AmGraph-2",
				"lineAlpha": "10",
				"bullet" : "round",
				"type": "line",
				"valueField": "logCnt",
				"labelPosition": "bottom",
				"labelText": "[[value]]"
			}],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "system_name",
	  "categoryAxis": {
	    "gridPosition": "start",
		"axisAlpha": 0,
		"tickLength": 0,
	    "labelRotation": 45
	  },
	  "export": {
	    "enabled": true
	  }

	} );
	$("#chart_new1_nofullscan").css("display", "");
}


function setchart_new1_2() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart_new1.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart_new1_2(data);
		}
	});
}

function drawchart_new1_2(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		if(i==0) 
			$("#reportBySystem").html(jsonData[i].system_name);
		var value = {"system_name": jsonData[i].system_name,"logCnt": jsonData[i].logCnt,"privacyCnt": jsonData[i].privacyCnt};
		dataProvider.push(value);
	}
	
	var title1 = "개인정보 처리량";
	var title2 = "개인정보 이용량";
	
	var chart_new1_2 = AmCharts.makeChart( "chart_new1_2", {
	  "type": "serial",
	  "theme": "light",
	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "title": "개인정보검출(건)"
	  } ],
	  "legend": {
	        "useGraphSettings": true
		},
	  "startDuration": 0,
	  "graphs": [ 
		  {
				"balloonText": "[[title]]:[[value]]",
				"fillAlphas": 0.8,
				"title": title1,
				"id": "AmGraph-2",
				"lineAlpha": 0.2,
				"type": "column",
				"valueField": "logCnt",
				"labelPosition": "bottom",
				"labelText": "[[value]]"
			},
			{
				"balloonText": "[[title]]:[[value]]",
				"fillAlphas": 0.8,
				"title": title2,
				"id": "AmGraph-1",
				"lineAlpha": 0.2,
				"type": "column",
				"valueField": "privacyCnt",
				"labelPosition": "bottom",
				"labelText": "[[value]]"
	  } ],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "system_name",
	  "categoryAxis": {
	    "gridPosition": "start",
		"axisAlpha": 0,
		"tickLength": 0,
	    "labelRotation": 45
	  },
	  "export": {
	    "enabled": true
	  }

	} );
	
}

function setchart_new1_1() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart_new1.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart_new1_1(data);
		}
	});
}

function drawchart_new1_1(data) {
	
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for (var i = 0; i < jsonData.length; i++) {
		if(i==0) 
			$("#reportBySys").html(jsonData[i].system_name);
		var value = {"system_name": jsonData[i].system_name,"logCnt": jsonData[i].logCnt,"privacyCnt": jsonData[i].privacyCnt};
		dataProvider.push(value);
	}

	var title1 = "개인정보 처리량";
	var title2 = "개인정보 이용량";
	
	var chart_new1_1 = AmCharts.makeChart("chart_new1_1", {
		"type": "serial",
	     "theme": "light",
		"categoryField": "system_name",
		"rotate": true,
		"startDuration": 0,
		"categoryAxis": {
			"gridPosition": "start",
			"position": "left"
		},
		"legend": {
	        "useGraphSettings": true
		},
		"trendLines": [],
		"graphs": [
			{
				"balloonText": "[[title]]:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-2",
				"lineAlpha": 0.2,
				"title": title2,
				"type": "column",
				"valueField": "logCnt",
				"labelPosition": "left",
				"labelText": "[[value]]"
			},
			{
				"balloonText": "[[title]]:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-1",
				"lineAlpha": 0.2,
				"title": title1,
				"type": "column",
				"valueField": "privacyCnt",
				"labelPosition": "left",
				"labelText": "[[value]]"
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"position": "bottom",
				"axisAlpha": 0,
				"title": "개인정보검출(건)"
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider":dataProvider,
	    "export": {
	    	"enabled": true
	     }

	});
}

//2
function setchart_new2() {
	$.ajax({
		type: 'POST',
		//url: rootPath + '/report/reportDetail_chart_new2.html',
		url: rootPath + '/report/download_reportDetail_chart_new2.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			if(use_fullscan == 'Y')
				drawchart_new2(data);
			else
				drawchart_new2_nofullscan(data);
		}
	});
}

function drawchart_new2(data) {
	
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for (var i = 0; i < jsonData.length; i++) {
		if(i==0) 
			$("#reportByDept").html(jsonData[i].dept_name);
		var value = {"dept_name": jsonData[i].dept_name,"logCnt": jsonData[i].logCnt,"privacyCnt": jsonData[i].privacyCnt};
		dataProvider.push(value);
	}

	var title1 = "개인정보 처리량";
	var title2 = "개인정보 이용량";
	
	var chart_new2 = AmCharts.makeChart("chart_new2", {
		"type": "serial",
	     "theme": "light",
		"categoryField": "dept_name",
		"rotate": true,
		"startDuration": 0,
		"categoryAxis": {
			"gridPosition": "start",
			"position": "left"
		},
		"legend": {
	        "useGraphSettings": true
		},
		"trendLines": [],
		"graphs": [
			{
				"balloonText": "[[title]]:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-2",
				"lineAlpha": 0.2,
				"title": title2,
				"type": "column",
				"valueField": "logCnt",
				"labelPosition": "left",
				"labelText": "[[value]]"
			},
			{
				"balloonText": "[[title]]:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-1",
				"lineAlpha": 0.2,
				"title": title1,
				"type": "column",
				"valueField": "privacyCnt",
				"labelPosition": "left",
				"labelText": "[[value]]"
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"position": "bottom",
				"axisAlpha": 0,
				"title": "개인정보검출(건)"
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider":dataProvider,
	    "export": {
	    	"enabled": true
	     }

	});
	
	$("#chart_new2").css("display", "");
}

function drawchart_new2_nofullscan(data) {
	
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for (var i = 0; i < jsonData.length; i++) {
		if(i==0) 
			$("#reportByDept").html(jsonData[i].dept_name);
		var value = {"dept_name": jsonData[i].dept_name,"logCnt": jsonData[i].logCnt,"privacyCnt": jsonData[i].privacyCnt};
		dataProvider.push(value);
	}

	var title1 = "개인정보 처리량";
	//var title2 = "개인정보 이용량";
	
	var chart_new2 = AmCharts.makeChart("chart_new2_nofullscan", {
		"type": "serial",
	     "theme": "light",
		"categoryField": "dept_name",
		"rotate": true,
		"startDuration": 0,
		"categoryAxis": {
			"gridPosition": "start",
			"position": "left"
		},
		"legend": {
	        "useGraphSettings": true
		},
		"trendLines": [],
		"graphs": [
			{
				"balloonText": "[[title]]:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-2",
				"lineAlpha": 0.2,
				"title": title1,
				"type": "column",
				"valueField": "logCnt",
				"labelPosition": "left",
				"labelText": "[[value]]"
			}],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"position": "bottom",
				"axisAlpha": 0,
				"title": "개인정보검출(건)"
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider":dataProvider,
	    "export": {
	    	"enabled": true
	     }

	});
	
	$("#chart_new2_nofullscan").css("display", "");
}

//2
function setchart_new3() {
	$.ajax({
		type: 'POST',
		//url: rootPath + '/report/reportDetail_chart_new3.html',
		url: rootPath + '/report/download_reportDetail_chart_new3.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart_new3(data);
		}
	});
}

function drawchart_new3(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		if(i==0) 
			$("#reportByCode").html(jsonData[i].dept_name);
		var value = {"code_name": jsonData[i].code_name,"privacyCnt": jsonData[i].privacyCnt};
		dataProvider.push(value);
	}
	
	var chart_new3 = AmCharts.makeChart( "chart_new3", {
	  "type": "serial",
	  "theme": "light",
	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "title": "개인정보검출(건)"
	  } ],
	  "startDuration": 0,
	  "graphs": [ {
	    "balloonText": "[[category]]: <b>[[value]]</b>",
	    "fillAlphas": 1,
	    "type": "column",
	    "valueField": "privacyCnt",
	    "labelText": "[[value]]",
	    "labelPosition": "bottom",
	    "labelOffset": -1
	  } ],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "code_name",
	  "categoryAxis": {
	    "gridPosition": "start",
		"axisAlpha": 0,
		"tickLength": 0,
	    "labelRotation": 45
	  },
	  "export": {
	    "enabled": true
	  }

	} );
}

function setchart_new4() {
	$.ajax({
		type: 'POST',
		//url: rootPath + '/report/reportDetail_chart6.html',
		url: rootPath + '/report/download_reportDetail_chart4.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart_new4(data);
		}
	});
}

function drawchart_new4(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	console.log("data : " + data);
	
	for(var i=0; i<jsonData.length; i++) {
		var resReq = "";
		
		
		var value = {"file_ext": jsonData[i].file_ext, "value": parseInt(jsonData[i].file_ext_cnt)};
		dataProvider.push(value);
	} 
	var chart_new4 = AmCharts.makeChart("chart_new4", {
	  "type": "pie",
	  "startDuration": 0,
	   "theme": "light",
	  "addClassNames": true,
	  "legend":{
	   	"position":"right",
	    "marginRight":0,
	    "autoMargins":true,
	    "title": "파일 확장자"
	  },
	  "innerRadius": "30%",
	  "defs": {
	    "filter": [{
	      "id": "shadow",
	      "width": "200%",
	      "height": "200%",
	      "feOffset": {
	        "result": "offOut",
	        "in": "SourceAlpha",
	        "dx": 0,
	        "dy": 0
	      },
	      "feGaussianBlur": {
	        "result": "blurOut",
	        "in": "offOut",
	        "stdDeviation": 5
	      },
	      "feBlend": {
	        "in": "SourceGraphic",
	        "in2": "blurOut",
	        "mode": "normal"
	      }
	    }]
	  },
	  "dataProvider": dataProvider,
	  "valueField": "value",
	  "titleField": "file_ext",
	  "descriptionField": "file_ext",
	  "export": {
	    "enabled": true
	  }
	});
	
}

var charts = {}; 
function saveReport() {
	$("#chart_new1").css("display", "none");
	$("#chart_new1_1").css("display", "none");
	$("#chart_new2").css("display", "none");
	$("#chart_new3").css("display", "none");
	$("#chart_new4").css("display", "none");
	
	var ids = [];
	if(period_type == 1)
		ids = [ "chart_new1", "chart_new1_1", "chart_new2", "chart_new3", "chart_new4"];
	else
		ids = [ "chart_new1", "chart_new1_1", "chart_new2", "chart_new3", "chart_new4"];

	// Collect actual chart objects out of the AmCharts.charts array
	
	var charts_remaining = ids.length;
	for (var i = 0; i < ids.length; i++) {
		for (var x = 0; x < AmCharts.charts.length; x++) {
			if (AmCharts.charts[x].div.id == ids[i]) 
				charts[ids[i]] = AmCharts.charts[x];
		}
	}
	
	// Trigger export of each chart
	for ( var x in charts) {
		if (charts.hasOwnProperty(x)) {
			var chart = charts[x];
			chart["export"].capture({}, function() {
				this.toPNG({}, function(data) {

					this.setup.chart.exportedImage = data;
					
					charts_remaining--;

					if (charts_remaining == 0) {
						for(var ch in charts) {
							var temp_chart = charts[ch];
							imageList[ch] = temp_chart.exportedImage;
						}
						saveAsDoc();
					}

				});
			});
		}
	}
}

function show() {
	for(var x in charts) {
		var chart = charts[x];
		alert(chart.div.id);
	}
}

function goAllLogInqList(req_type) {
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["req_type_rp"].value = req_type;
	opener.eventForm.submit();
}

