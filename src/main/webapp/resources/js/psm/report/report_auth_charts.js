var imageList = new Array();
var imageList = {};


$(document).ready(function() {
	if(download_type == 'pdf') {
		$("#buttonDiv").css("display","none");
		$(".downloadChartClass").css("height","500px");
		$(".downloadChartClass").css("width","850px");
		$(".HStyle5").css("width","850px");
		for (var x = 0; x < AmCharts.charts.length; x++) {
			$("#"+AmCharts.charts[x].div.id).css("width","850px");
		}
		setInterval(function() {
			setChart.init();
		},500);
		
		
	} else {
		$(".downloadChartClass").css("width","750px");
		for (var x = 0; x < AmCharts.charts.length; x++) {
			$("#"+AmCharts.charts[x].div.id).css("width","750px");
		}	
		setChart.init();
	}
	
	if(period_type == 10){
		$('#saveButton').hide();
	}
});

/*function setCharts() {
	setAuthChart0();
	setAuthChart1();
}*/

var setChart = {
		init : function(){
			var chartlist = $('.downloadChartClass');
			for(var i=0 ; i < chartlist.length ; i++){
				eval('setChart.'+chartlist[i].id+'()');
			}
		},
		
		authChart0 : function(){setAuthChart0()},
		authChart1 : function(){setAuthChart1()}
}

function saveAsDoc() {
	$.ajax({
		type : 'POST',
		url : rootPath + '/report/getImages.html?type=5',
		data : imageList,
		/*data : {
			"imageList" : imageList,
			"report_type" : "report3"
		}*/
		success : function(data) {
			/*for(var i=0; i<imageList.length; i++) {
				var image = "#chartImage" + i;
				$(image).css("display", "block");
			}*/
			var i=0;
			for(var img in imageList) { 
				var image = "#chartImage" + i;
				$(image).css("display", "block");
				i++;
			}
			
			var date = new Date();
			var year = date.getFullYear();
			var month = new String(date.getMonth() + 1);
			var day = new String(date.getDate());
			if(month.length == 1){ 
				month = "0" + month; 
			} 
			if(day.length == 1){ 
				day = "0" + day; 
			} 
			var regdate = year + "" + month + "" + day;
			var agent = navigator.userAgent.toLowerCase();
			if (agent.indexOf("msie") != -1) {
				saveAsPage(regdate);
			} else {
				exportHTML(regdate);
			}
			
			self.close();
		}
	});
}

/*function exportHTML(regdate){
    var header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' "+
         "xmlns:w='urn:schemas-microsoft-com:office:word' "+
         "xmlns='http://www.w3.org/TR/REC-html40'>"+
         "<head><meta charset='utf-8'><title>Export HTML to Word Document with JavaScript</title></head><body>";
    var footer = "</body></html>";
    var sourceHTML = header+document.getElementById("source-html").innerHTML+footer;
    
    var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
    var fileDownload = document.createElement("a");
    document.body.appendChild(fileDownload);
    fileDownload.href = source;
    fileDownload.download = 'report_'+regdate+'.doc';
    //fileDownload.download = '권한부여보고_'+regdate+'.doc';
    fileDownload.click();
    document.body.removeChild(fileDownload);
 }*/

function saveAsPage(regdate) {
	if (document.execCommand) {
		//document.execCommand("SaveAs", false, "권한부여보고_" + regdate + ".doc");
		document.execCommand("SaveAs", false, "report_" + regdate + ".doc");
	} else {
		alert("error..");
	}
}

function setAuthChart0() {
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_authChart0.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawAuthChart0(data);
		}
	});
}

function drawAuthChart0(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		if(i==0) {
			$("#reportBySys").html(jsonData[i].system_name);
		}
		var value = {"system_seq": jsonData[i].system_seq, "system_name": jsonData[i].system_name, "cnt": jsonData[i].cnt};
		dataProvider.push(value);
	}
	
	var authChart0 = AmCharts.makeChart( "authChart0", {
	  "type": "serial",
	  "theme": "light",
	  "titles": [{
	        "text": "[시스템별 접근권한 관리 총 횟수]",
	        "size": 15
	    }],
	  "dataProvider": dataProvider,
	  "rotate": true,
	  "valueAxes": [ {
		"axisAlpha": 0,
	    "title": "관리 내역(건)"
	  } ],
	  "startDuration": 0,
	  "graphs": [ {
	    "balloonText": "[[category]]: <b>[[value]]</b>",
	    "fillAlphas": 1,
	    "type": "column",
	    "valueField": "cnt",
	    "labelText": "[[value]]",
	    "labelPosition": "left",
	    "descriptionField": "system_seq"
	  } ],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "system_name",
	  "categoryAxis": {
	    "gridPosition": "start",
		"axisAlpha": 0,
		"tickLength": 0,
	    "labelRotation": 45
	  },
	  "export": {
	    "enabled": true
	  }

	} );
	
	authChart0.addListener("clickGraphItem", handleClick0);
}

function handleClick0(event) {
	
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["system_seq_rp"].value = event.item.description;
	opener.eventForm.submit();
}

function setAuthChart1(){
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_authChart1.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawAuthChart1(data);
		}
	});
}

function drawAuthChart1(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"system_name": jsonData[i].system_name, 
				"cnt1": jsonData[i].cnt1,
				"cnt2": jsonData[i].cnt2,
				"cnt3": jsonData[i].cnt3};
		
		dataProvider.push(value);
	}
	console.log(dataProvider);
		    	    
	var authChart1 = AmCharts.makeChart("authChart1", {
	    "type": "serial",
		"theme": "light",
		"titles": [{
	        "text": "[시스템별 접근권한 관리 행위별 내역]",
	        "position": "top",
	        "size": 15
	    }],
	    "legend": {
	        "horizontalGap": 1,
	        "maxColumns": 3,
			"useGraphSettings": true,
			"markerSize": 10
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.8,
	        "gridAlpha": 0,
	        "title": "관리 내역(건)"
	    }],
	    "graphs": [{
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 1,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.8,
	        "title": "생성",
	        "type": "column",
			"color": "#000000",
	        "valueField": "cnt1"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 1,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.8,
	        "title": "삭제",
	        "type": "column",
			"color": "#000000",
	        "valueField": "cnt2"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 1,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.8,
	        "title": "수정",
	        "type": "column",
			"color": "#000000",
	        "valueField": "cnt3"
	    }], 
	    "rotate": true,
	    "startDuration": 0,
	    "categoryField": "system_name",
		  "categoryAxis": {
		    "gridPosition": "start",
			"axisAlpha": 0,
			"tickLength": 0,
		    "labelRotation": 45
	    },
	    "export": {
	    	"enabled": true
	     }
	});
	
	authChart1.addListener("clickGraphItem", handleClick1);
}

function handleClick1(event) {
	
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["dept_name_rp"].value = event.item.category;
	opener.eventForm.submit();
}

var charts = {}; 
function saveReport() {
	
	$("#authChart0").css("display", "none");
	$("#authChart1").css("display", "none");
	
	var ids = [ "authChart0", "authChart1" ];

	// Collect actual chart objects out of the AmCharts.charts array
	var charts = {}; 
	var charts_remaining = ids.length;
	for (var i = 0; i < ids.length; i++) {
		for (var x = 0; x < AmCharts.charts.length; x++) {
			if (AmCharts.charts[x].div.id == ids[i])
				charts[ids[i]] = AmCharts.charts[x];
		}
	}

	// Trigger export of each chart
	var idx = 0;
	for ( var x in charts) {
		if (charts.hasOwnProperty(x)) {
			var chart = charts[x];
			chart["export"].capture({}, function() {
				this.toPNG({}, function(data) {

					this.setup.chart.exportedImage = data;

					charts_remaining--;

					if (charts_remaining == 0) {
						for(var ch in charts) {
							var temp_chart = charts[ch];
							imageList[ch] = temp_chart.exportedImage;
						}
						saveAsDoc();
					}

				});
			});
		}
	}
}

function show() {
	for(var x in charts) {
		var chart = charts[x];
		alert(chart.div.id);
	}
}