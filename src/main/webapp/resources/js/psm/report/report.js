function searchAnals() {
	
	// date
	var date = new Date();
	var year = date.getFullYear();
	var mon = date.getMonth() + 1;
	var day = date.getDate();
//	var day = '10';
	
	// 이번주 월요일이 몇일전인지 
	var date_of_week = date.getDay();
//	var date_of_week = '5';
	
	// start date, end_date 
	var start_date_val = $('input[name=start_date]').val();
	var end_date_val = $('input[name=end_date]').val();
	
	if (arguments.length < 1) {
		var sd = start_date_val.replace(/-/gi, "");
		var ed = end_date_val.replace(/-/gi, "");
		
		$('input[name=start_date]').val(sd);
		$('input[name=end_date]').val(ed);
	} else {
		var chk_date_type = arguments[0];
		switch (chk_date_type) {
		case 'DAY':
			// today 
			var today = year + "" + (mon < 10 ? "0" + mon : mon)  + "" + (day < 10 ? "0" + day : day);
			$('input[name=start_date]').val(today);
			$('input[name=end_date]').val(today);
			break;
			
		case 'WEEK':
//			alert( "[day] : " +  day  + "\n[date_of_week] : " +  date_of_week);
			var today = year + "" + (mon < 10 ? "0" + mon : mon)  + "" + (day < 10 ? "0" + day : day);
			
			// 오늘이 일요일일 때 ( 일요일이 주의 시작 ) 
			if(date_of_week == '0'){
				$('input[name=start_date]').val(today);
				
			// 일요일이 아닐때  day_of_week 가 0/일, 1/월, 2/화, 3/수, 4/목, 5/금, 6/토
			}else{
				
				// 요일숫자를 뺀 숫자가 현재 날짜보다 작거나 같을 때 (차가 이전달일 때 ) 
				if(day - date_of_week <= '0'){
					
					// 이번달이 1월달일 때 ( 차가 전해일 때 )  
					if(mon == '1'){
						var start_date = new Date(year, mon , day - date_of_week);
//						alert( "[start_date_year] : " + start_date.getFullYear() + "[start_date_month] : " + start_date.getMonth() + "\n[start_date_day] : " + start_date.getDate());
						var start_day  = (start_date.getFullYear() - 1) + "12" + start_date.getDate();
						$('input[name=start_date]').val(start_day);
						
					// 이번달이 1월달이 아닐때 ( 차가 올해일 때 ) 
					}else{
						// 날짜 구하기 
						var start_date = new Date(year, mon, day - date_of_week);
//						alert( "[start_date_month] : " + start_date.getMonth() + "\n[start_date_day] : " + start_date.getDate());
						var start_day  = year + "" + (start_date.getMonth() < 10 ? "0" + (start_date.getMonth()) : (start_date.getMonth())) + "" + (start_date.getDate() < 10 ? "0" + start_date.getDate() : start_date.getDate());
						$('input[name=start_date]').val(start_day);
					}
					// 요일숫자를 뺀 숫자가 현재 날짜보다 클때 ( 차가 현재달일 때 ) 
				}else{
					var start_day  = year + "" + (mon < 10 ? "0" + mon : mon) + "" + (day - date_of_week < 10 ? "0" + (day - date_of_week) : (day - date_of_week));
					$('input[name=start_date]').val(start_day);
				}
			}
			$('input[name=end_date]').val(today);
			break;
			
		case 'MON':
			// this month
			var mon_start_date = year + "" + (mon < 10 ? "0" + mon : mon) + "01";
			
			mon_date = new Date(year, mon, 0);
			mon_day = mon_date.getDate();
			var mon_end_date = year + "" + (mon < 10 ? "0" + mon : mon) + "" + mon_day;
			
			$('input[name=start_date]').val(mon_start_date);
			$('input[name=end_date]').val(mon_end_date);
			break;
		default:
			break;
		}
	}
	
	$("#listForm").attr("action",reportConfig["listUrl"]);
	$("#listForm").attr("target","new");
	$("#listForm").submit();
	
	var startdate = $('input[name=start_date]').val();
	var enddate = $('input[name=end_date]').val();
	
	$('input[name=start_date]').attr("value", startdate.substring(0,4) + "-" + startdate.substring(4,6) + "-" + startdate.substring(6,8)); 
	$('input[name=end_date]').attr("value", enddate.substring(0,4) + "-" + enddate.substring(4,6) + "-" + enddate.substring(6,8)); 
	
}

function fnallCheck(){
	var chkstatus = $('#allCheck').is(":checked");
	if(chkstatus){
		$('input[name^=reportCheck]').prop('checked', true);
	}else{
		$('input[name^=reportCheck]').prop('checked', false);
	}
}

function allNoCheck(){
	$('input[id=allCheck]').prop('checked', false);
	$('input[name^=reportCheck]').prop('checked', false);
}

function report_Download(num) {
	$.ajax({
		url: rootPath + '/report/report_Download.html',
		type:'POST',
		data: { 
			"num" : num
		},
		dataType: "json",
		success: function(data){
			downLoad_type(data);
		},
		error: function(){
			alert('실패');
		}
	});
}

function report_PDF(num){
	$.ajax({
		url: rootPath + '/report/reportFileValidation.html',
		type:'POST',
		data: { 
			"num" : num
		},
		success: function(data){
			if(data == 0){
				$("#listForm").attr("action", rootPath +"/report/report_pdf.html");
				$("#listForm input[name=report_seq]").val(num);	//파일번호
				$("#listForm").submit();
			}else if(data == 1){
				alert('파일이 없습니다. \n보고서를 다시 생성하거나 관리자에게 문의해주세요.');
			}
		}
	});
}

function downLoad_type(data) {
	var agent = navigator.userAgent.toLowerCase();
	
	if ((navigator.appName == 'Netscape' && navigator.userAgent.search('Trident') != -1) || (agent.indexOf("msie") != -1) ) {
		report_downloadIe(data);
	} else {
		report_downloadChrome(data);
	}
}

function report_downloadChrome(data) {
	var date = new Date();
	var year = date.getFullYear();
	var month = new String(date.getMonth() + 1);
	//var day = new String(date.getDate());
	var source = data.html_encode;
	
	console.log(data.html_encode);
	
	if(month.length == 1){ 
		month = "0" + month; 
	}
	
	var regdate = year + "" + month;
	var agent = navigator.userAgent.toLowerCase(); 
	
	var fileDownload = document.createElement("a");
    document.body.appendChild(fileDownload);
    fileDownload.href = source;
    fileDownload.download = data.report_title+'.doc';
    fileDownload.click();
    document.body.removeChild(fileDownload);
    
}

function report_downloadIe(data) {
	var source = data.html_encode;
	var decodeHTML = decodeURIComponent(source);
	
	var date = new Date();
	var year = date.getFullYear();
	var month = new String(date.getMonth() + 1);
	//var day = new String(date.getDate());
	if(month.length == 1){ 
		month = "0" + month; 
	}
	
	
	var regdate = year + "" + month;
	var reportName = data.report_title+'.doc';
	
	blob = new Blob(['\ufeff', decodeHTML], {
		type: 'application/msword'
		//type: 'application/vnd.ms-word'
	});
    url = URL.createObjectURL(blob);
    link = document.createElement('a');
    link.href = url;
    link.download = reportName;  // default name without extension 
    document.body.appendChild(link);
    if (navigator.msSaveOrOpenBlob ) navigator.msSaveOrOpenBlob( blob, reportName); // IE10-11
    else link.click();  // other browsers
    document.body.removeChild(link);
}

function report_Delete(num){
	if(confirm('삭제 하시겠습니까?')){
		$.ajax({
			url:'reportListDelete.html?report_seq='+num,
			type:'POST',
			success: function(data){
				console.log(data);
				location.reload();
			},
			error: function(){
				alert('실패');
			}
		});
	}
}

function goPage(num){
	$("#listForm").attr("action",rootPath+"/report/reportManagement.html");
	$("#listForm input[name=page_num]").val(num);
	$("#listForm").submit();
}

function report_Search(){
	var start_year = parseInt(document.getElementById('startyear').value);
	var start_month = parseInt(document.getElementById('startmonth').value);
	var end_year = parseInt(document.getElementById('endyear').value);
	var end_month = parseInt(document.getElementById('endmonth').value);
	
	var report_type = document.getElementById('reportType').value;
	
	var dateCheck = document.getElementById('dateCheck').checked;
	
	if(start_year>end_year)start_year = end_year;
	if(start_year==end_year){
		if(start_month>end_month){start_month = end_month;}
	}
	if(start_month<10)start_month = '0'+start_month;
	if(end_month<10)end_month = '0'+end_month;
	
	var search_from = start_year+''+start_month;
	var search_to = end_year+''+end_month;
	
	$("#listForm").attr("action",rootPath+"/report/reportManagement.html");
	$("#listForm input[name=search_from]").val(search_from);
	$("#listForm input[name=search_to]").val(search_to);
	$("#listForm input[name=report_type]").val(report_type);
	$("#listForm input[name=page_num]").val(1);
	if(dateCheck){
		$("#listForm input[name=isSearch]").val('true');
	}else{
		$("#listForm input[name=isSearch]").val(null);
		$("#listForm input[name=search_from]").val(null);
		$("#listForm input[name=search_to]").val(null);
	}
	$("#listForm").submit();
}

function dateSelect(){
	var dateCheck = !document.getElementById('dateCheck').checked;
	$('#startyear').attr('disabled', dateCheck);
	$('#startmonth').attr('disabled', dateCheck);
	$('#endyear').attr('disabled', dateCheck);
	$('#endmonth').attr('disabled', dateCheck);
}

function monthReplace(){
	var start_year = parseInt(document.getElementById('startyear').value);
	var start_month = parseInt(document.getElementById('startmonth').value);
	var end_year = parseInt(document.getElementById('endyear').value);
	var end_month = parseInt(document.getElementById('endmonth').value);
	if(start_year==end_year){
		if(start_month>end_month)$('#startmonth option:eq('+(end_month-1)+')').prop("selected", true);
	}
}

function yearReplace(){
	var start_year = parseInt(document.getElementById('startyear').value);
	var end_year = parseInt(document.getElementById('endyear').value);
	if(start_year>end_year){
		$('#startyear').val(end_year).prop("selected", true);
		monthReplace()
	}
}