var imageList = new Array();
var imageList = {};

$(document).ready(function() {
	setCharts();
	
	/*var repeat = setInterval(function(){
		if(!checkImageList()) {
			//$('#loading').hide();
			$('#buttonGroup').css('display', 'block');
			clearInterval(repeat);
		}
	}, 1000);*/
});

function setCharts() {
	setchart1();
	setchart2();
	setchart3();
	setchart4();
}

function saveAsDoc() {
	$.ajax({
		type : 'POST',
		url : rootPath + '/report/getImages.html?type=2',
		data : imageList,
		/*data : {
			"imageList" : imageList,
			"report_type" : "report2"
		}*/
		success : function(data) {
			for(var i=0; i<imageList.length; i++) {
				var image = "#chartImage" + i;
				$(image).css("display", "block");
			}

			/*var date = new Date();
			var year = date.getFullYear();
			var month = new String(date.getMonth() + 1);
			var day = new String(date.getDate());
			if(month.length == 1){ 
				month = "0" + month; 
			} 
			if(day.length == 1){ 
				day = "0" + day; 
			} 
			var regdate = year + "" + month + "" + day;
			var agent = navigator.userAgent.toLowerCase();
			if (agent.indexOf("msie") != -1) {
				saveAsPage(regdate);
			} else {
				exportHTML(regdate);
			}*/
			exportHTML(regdate);
		}
	});
}

function exportHTML(regdate){
    var header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' "+
         "xmlns:w='urn:schemas-microsoft-com:office:word' "+
         "xmlns='http://www.w3.org/TR/REC-html40'>"+
         "<head><meta charset='utf-8'><title>Export HTML to Word Document with JavaScript</title></head><body>";
    var footer = "</body></html>";
    var sourceHTML = header+document.getElementById("source-html").innerHTML+footer;
    
    var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
    var fileDownload = document.createElement("a");
    document.body.appendChild(fileDownload);
    fileDownload.href = source;
    fileDownload.download = 'report_'+regdate+'.doc';
    fileDownload.click();
    document.body.removeChild(fileDownload);
 }

function saveAsPage(regdate) {
	if (document.execCommand) {
		document.execCommand("SaveAs", false, "report2_" + regdate + ".doc");
	} else {
		alert("error..");
	}
}

function checkImageList() {
	for(var i=0; i<imageList_length; i++) {
		if(i == 1)
			continue;
		if(imageList[i] == null) {
			return true;
		}
	}
	return false;
}


function setchart1() {
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportSummon_chart1.html',
		data: { 
			"search_from" : search_from,
			"search_to" : search_to
		},
		success: function(data) {
			drawchart1(data);
		}
	});
}

function drawchart1(data) {
	var jsonData = JSON.parse(data);
	
	var rule_list = jsonData["rule_list"];
	var emp_list = jsonData["emp_list"];
	
	var graphs = new Array();
	for(var i=0; i<rule_list.length; i++) {
		var value = {"balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
			        "fillAlphas": 0.8,
			        "labelText": "[[value]]",
			        "lineAlpha": 0.3,
			        "title": rule_list[i].rule_nm,
			        "type": "column",
					"color": "#000000",
			        "valueField": "r"+(i+1),
			        "columnWidth" : 0.6};
		graphs.push(value);
	}
	
	var chart1 = AmCharts.makeChart("chart1", {
	    "type": "serial",
		"theme": "light",
		"startDuration": 0,
	    "legend": {
	        "horizontalGap": 10,
	        "maxColumns": 1,
	        "position": "right",
			"useGraphSettings": true,
			"markerSize": 10
	    },
	    "depth3D": 20,
	    "angle": 40,
	    "dataProvider": emp_list,
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.3,
	        "gridAlpha": 0.3,
	        "title": "소명 진행상태 건수(건)"
	    }],
	    "graphs": graphs,
	    "categoryField": "emp_user_name",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 30,
	        "gridAlpha": 0,
	        "position": "left"
	    },
	    "export": {
	    	"enabled": false
	     },
	     "menu": [ {
	    	  "class": "export-main",
	    	  "menu": [ {
	    	    "label": "Download",
	    	    "menu": [ "PNG", "JPG", "CSV", "XLSX" ]
	    	  }, {
	    	    "label": "Annotate",
	    	    "action": "draw",
	    	    "menu": [ {
	    	      "class": "export-drawing",
	    	      "menu": [ "JPG", "PNG", "SVG", "PDF" ]
	    	    } ]
	    	  } ]
	    	} ]
	});
	
	
}

function setchart2() {
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportSummon_chart2.html',
		data: { 
			"search_from" : search_from,
			"search_to" : search_to
		},
		success: function(data) {
			drawchart2(data);
		}
	});
}

function drawchart2(data) {
	var jsonData = JSON.parse(data);
	
	var rule_list = jsonData["rule_list"];
	var emp_list = jsonData["emp_list"];
	
	var graphs = new Array();
	for(var i=0; i<rule_list.length; i++) {
		var value = {"balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
			        "fillAlphas": 0.8,
			        "labelText": "[[value]]",
			        "lineAlpha": 0.3,
			        "title": rule_list[i].rule_nm,
			        "type": "column",
					"color": "#000000",
			        "valueField": "rs"+(i),
			        "columnWidth" : 0.6};
		graphs.push(value);
	}
	
	var chart2 = AmCharts.makeChart("chart2", {
	    "type": "serial",
		"theme": "light",
		"startDuration": 0,
	    "legend": {
	        "horizontalGap": 10,
	        "maxColumns": 1,
	        "position": "right",
			"useGraphSettings": true,
			"markerSize": 10
	    },
	    "depth3D": 20,
	    "angle": 40,
	    "dataProvider": emp_list,
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.3,
	        "gridAlpha": 0.3,
	        "title": "소명 판정결과 건수(건)"
	    }],
	    "graphs": graphs,
	    "categoryField": "emp_user_name",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 30,
	        "gridAlpha": 0,
	        "position": "left"
	    },
	    "export": {
	    	"enabled": true
	     }

	});
	
	
}

function setchart3() {
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportSummon_chart3.html',
		data: { 
			"search_from" : search_from,
			"search_to" : search_to
		},
		success: function(data) {
			drawchart3(data);
		}
	});
}

function drawchart3(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for(var i=0; i<jsonData.length; i++) {
		var resReq = "";
		switch(jsonData[i].req_type) {
		case "1101":
		case "1201":
			resReq = "주민등록번호";
			break;
		case "1102":
		case "1202":
			resReq = "운전면허번호";
			break;
		case "1103":
		case "1203":
			resReq = "여권번호";
			break;
		case "1104":
		case "1204":
			resReq = "계좌번호";
			break;
		case "1105":
		case "1205":
			resReq = "건강보험번호";
			break;
		case "1106":
		case "1206":
			resReq = "전화번호";
			break;
		case "1107":
		case "1207":
			resReq = "이메일주소";
			break;
		case "1108":
		case "1208":
			resReq = "휴대폰번호";
			break;
		case "1109":
		case "1209":
			resReq = "신용카드번호";
			break;
		case "1110":
		case "1210":
			resReq = "외국인등록번호";
			break;
		}
		
		var value = {"category": resReq, "value": jsonData[i].cnt};
		dataProvider.push(value);
	}
	var chart3 = AmCharts.makeChart("chart3", {
	  "type": "pie",
	  "startDuration": 0,
	   "theme": "light",
	  "addClassNames": true,
	  "legend":{
	   	"position":"right",
	    "marginRight":0,
	    "autoMargins":true,
	    "title": "개인정보검출(건)"
	  },
	  "innerRadius": "30%",
	  "defs": {
	    "filter": [{
	      "id": "shadow",
	      "width": "200%",
	      "height": "200%",
	      "feOffset": {
	        "result": "offOut",
	        "in": "SourceAlpha",
	        "dx": 0,
	        "dy": 0
	      },
	      "feGaussianBlur": {
	        "result": "blurOut",
	        "in": "offOut",
	        "stdDeviation": 5
	      },
	      "feBlend": {
	        "in": "SourceGraphic",
	        "in2": "blurOut",
	        "mode": "normal"
	      }
	    }]
	  },
	  "dataProvider": dataProvider,
	  "valueField": "value",
	  "titleField": "category",
	  "export": {
	    "enabled": true
	  }
	});
}

function setchart4() {
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportSummon_chart4.html',
		data: { 
			"search_from" : search_from,
			"search_to" : search_to
		},
		success: function(data) {
			drawchart4(data);
		}
	});
}

function drawchart4(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		if(i==0) 
			$("#reportByDept").html(jsonData[i].system_name);
		var value = {"system_name": jsonData[i].system_name,"cnt": jsonData[i].cnt};
		dataProvider.push(value);
	}
	
	var chart4 = AmCharts.makeChart( "chart4", {
	  "type": "serial",
	  "theme": "light",
	 /* "titles": [{
	        "text": "[소속별 결과]",
	        "size": 15
	    }],*/
	  "dataProvider": dataProvider,
	  "valueAxes": [ {
//	    "gridColor": "#FFFFFF",
//	    "gridAlpha": 0.2,
//	    "dashLength": 0
	    "axisAlpha": 0,
	    "title": "소명검출(건)"
	  } ],
	  //"gridAboveGraphs": true,
	  "startDuration": 0,
	  "graphs": [ {
	    "balloonText": "[[category]]: <b>[[value]]</b>",
	    "fillAlphas": 1,
	    //"lineAlpha": 0.2,
	    "type": "column",
	    "valueField": "cnt",
	    "labelText": "[[value]]"
	  } ],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "system_name",
	  "categoryAxis": {
	    /*"gridPosition": "start",
	    "gridAlpha": 0,
	    "tickPosition": "start",
	    "tickLength": 20,
	    "labelRotation": 45*/
	    "gridPosition": "start",
		"axisAlpha": 0,
		"tickLength": 0,
	    "labelRotation": 45
	  },
	  "export": {
	    "enabled": true
	  }

	} );
}

function saveReport() {
	
	$("#chart1").css("display", "none");
	$("#chart2").css("display", "none");
	$("#chart3").css("display", "none");
	$("#chart4").css("display", "none");
	
	var ids = [ "chart1", "chart2", "chart3", "chart4" ];

	// Collect actual chart objects out of the AmCharts.charts array
	var charts = {}; 
	var charts_remaining = ids.length;
	for (var i = 0; i < ids.length; i++) {
		for (var x = 0; x < AmCharts.charts.length; x++) {
			if (AmCharts.charts[x].div.id == ids[i])
				charts[ids[i]] = AmCharts.charts[x];
		}
	}

	// Trigger export of each chart
	for ( var x in charts) {
		if (charts.hasOwnProperty(x)) {
			var chart = charts[x];
			chart["export"].capture({}, function() {
				this.toPNG({}, function(data) {

					this.setup.chart.exportedImage = data;

					charts_remaining--;

					if (charts_remaining == 0) {
						for(var ch in charts) {
							var temp_chart = charts[ch];
							imageList[ch] = temp_chart.exportedImage;
						}
						saveAsDoc();
					}

				});
			});
		}
	}
}



/**
 * Define export function
 */
function exportReport() {

	// So that we know export was started
	console.log("Starting export...");

	// Define IDs of the charts we want to include in the report
	var ids = [ "chart1", "chart2", "chart3", "chart4" ];

	// Collect actual chart objects out of the AmCharts.charts array
	var charts = {}, charts_remaining = ids.length;
	for (var i = 0; i < ids.length; i++) {
		for (var x = 0; x < AmCharts.charts.length; x++) {
			if (AmCharts.charts[x].div.id == ids[i])
				charts[ids[i]] = AmCharts.charts[x];
		}
	}

	// Trigger export of each chart
	var idx = 0;
	for ( var x in charts) {
		if (charts.hasOwnProperty(x)) {
			var chart = charts[x];
			chart["export"].capture({}, function() {
				this.toJPG({}, function(data) {

					// Save chart data into chart object itself
					// this.setup.chart.exportedImage = data;
					imageList[idx] = data;
					idx++;

					// Reduce the remaining counter
					charts_remaining--;

					// Check if we got all of the charts
					if (charts_remaining == 0) {
						// Yup, we got all of them
						// Let's proceed to putting PDF together
						// generatePDF();

					}

				});
			});
		}
	}

	function generatePDF() {

		// Log
		console.log("Generating PDF...");

		// Initiliaze a PDF layout
		var layout = {
			"content" : [],
			"styles" : {
				"green" : {
					"fillColor" : "green",
					"color" : "white"
				},
				"blue" : {
					"fillColor" : "blue",
					"color" : "white"
				},
				"orange" : {
					"fillColor" : "orange",
					"color" : "white"
				},
				"purple" : {
					"fillColor" : "purple",
					"color" : "white"
				},
				"pink" : {
					"fillColor" : "pink",
					"color" : "white"
				}
			}
		}

		// Let's add a custom title
		layout.content.push({
			"text" : document.getElementById("title").innerHTML,
			"fontSize" : 15
		});

		layout.content.push({
			"text" : "❍ 점검 대상 개인정보 처리시스템",
			"backgroundColor" : "#000"
		});

		// Let's add a table
		/*layout.content.push({
			"table" : {
				// headers are automatically repeated if the table spans over multiple pages
				// you can declare how many rows should be treated as headers
				"headerRows" : 1,
				"widths" : [ "10%", "10%", "45%" ],
				"body" : [ [ "번호", "개인정보처리시스템 명칭", "URL" ],
						[ "1", "보조사업 관리", "http://192.168.0.1" ],
						[ "2", "상수도 관리", "http://192.168.0.2" ],
						[ "3", "이호조", "http://192.168.0.3" ],
						[ "4", "고객메일 시스템", "http://192.168.0.4" ],
						[ "5", "고객구매 시스템", "http://192.168.0.5" ] ]
			}
		});*/

		// Add bigger chart
		layout.content.push({
			"image" : charts["chart1"].exportedImage,
			"fit" : [ 800, 600 ]
		});

		/*// Put two next charts side by side in columns
		layout.content.push({
		  "columns": [{
		    "width": "50%",
		    "image": charts["chartdiv2"].exportedImage,
		    "fit": [250, 300]
		  }, {
		    "width": "*",
		    "image": charts["chartdiv3"].exportedImage,
		    "fit": [250, 300]
		  }],
		  "columnGap": 10
		});

		// Add chart and text next to each other
		layout.content.push({
		  "columns": [{
		    "width": "25%",
		    "image": charts["chartdiv4"].exportedImage,
		    "fit": [125, 300]
		  }, {
		    "width": "*",
		    "stack": [
		      document.getElementById("note1").innerHTML,
		      "\n\n",
		      document.getElementById("note2").innerHTML
		    ]
		  }],
		  "columnGap": 10
		});*/

		// Trigger the generation and download of the PDF
		// We will use the first chart as a base to execute Export on
		chart["export"].toPDF(layout, function(data) {
			this.download(data, "application/pdf", "amCharts.pdf");
		});

	}
}

function savePDFReport() {
	
	// 현재 document.body의 html을 A4 크기에 맞춰 PDF로 변환
	html2canvas(document.body, {
	  onrendered: function(canvas) {
	 
	    // 캔버스를 이미지로 변환
	    var imgData = canvas.toDataURL('image/png');
	     
	    var imgWidth = 210; // 이미지 가로 길이(mm) A4 기준
	    var pageHeight = imgWidth * 1.414;  // 출력 페이지 세로 길이 계산 A4 기준
	    var imgHeight = canvas.height * imgWidth / canvas.width;
	    var heightLeft = imgHeight;
	     
	        var doc = new jsPDF('p', 'mm');
	        var position = 0;
	         
	        // 첫 페이지 출력
	        doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
	        heightLeft -= pageHeight;
	         
	        // 한 페이지 이상일 경우 루프 돌면서 출력
	        while (heightLeft >= 20) {
	          position = heightLeft - imgHeight;
	          doc.addPage();
	          doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
	          heightLeft -= pageHeight;
	        }
	 
	        // 파일 저장
	        doc.save('sample_A4.pdf');
	  }
	});
}