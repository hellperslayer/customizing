
$(document).ready(function() {
	setCharts();
});

function setCharts() {
	setchart1_1();
	setchart1_2();
	setchart2();
	setchart3_1();
	setchart3_2();
	setchart3_3();
	setchart4();
	setchart5();
	setchart6();
	setchart7();
	setchart8();
	setchart9();
	setchart10();
}

function setchart1_1() {
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart1_1.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date
		},
		success: function(data) {
			drawchart1_1(data);
		}
	});
}

function drawchart1_1(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		if(i==0) 
			$("#reportBySys").html(jsonData[i].system_name);
		var value = {"system_name": jsonData[i].system_name,"cnt": jsonData[i].cnt};
		dataProvider.push(value);
	}
	
	var chart = AmCharts.makeChart( "chart1_1", {
	  "type": "serial",
	  "theme": "light",
	  "titles": [{
	        "text": "[개인정보 처리시스템별 결과]",
	        "size": 15
	    }],
	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    "gridColor": "#FFFFFF",
	    "gridAlpha": 0.2,
	    "dashLength": 0
	  } ],
	  "gridAboveGraphs": true,
	  "startDuration": 1,
	  "graphs": [ {
	    "balloonText": "[[category]]: <b>[[value]]</b>",
	    "fillAlphas": 0.8,
	    "lineAlpha": 0.2,
	    "type": "column",
	    "valueField": "cnt"
	  } ],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "system_name",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "gridAlpha": 0,
	    "tickPosition": "start",
	    "tickLength": 20,
	    "labelRotation": 0
	  },
	  "export": {
	    "enabled": false
	  }

	} );
}

function setchart1_2() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart1_2.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date
		},
		success: function(data) {
			drawchart1_2(data);
		}
	});
}

function drawchart1_2(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		if(i==0) 
			$("#reportByDept").html(jsonData[i].dept_name);
		var value = {"dept_name": jsonData[i].dept_name,"cnt": jsonData[i].cnt};
		dataProvider.push(value);
	}
	
	var chart = AmCharts.makeChart( "chart1_2", {
	  "type": "serial",
	  "theme": "light",
	  "titles": [{
	        "text": "[소속별 결과]",
	        "size": 15
	    }],
	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    "gridColor": "#FFFFFF",
	    "gridAlpha": 0.2,
	    "dashLength": 0
	  } ],
	  "gridAboveGraphs": true,
	  "startDuration": 1,
	  "graphs": [ {
	    "balloonText": "[[category]]: <b>[[value]]</b>",
	    "fillAlphas": 0.8,
	    "lineAlpha": 0.2,
	    "type": "column",
	    "valueField": "cnt"
	  } ],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "dept_name",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "gridAlpha": 0,
	    "tickPosition": "start",
	    "tickLength": 20,
	    "labelRotation": 0
	  },
	  "export": {
	    "enabled": false
	  }

	} );
}

function setchart2() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart2.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date
		},
		success: function(data) {
			drawchart2(data);
		}
	});
}

function drawchart2(data) {
	var dataProvider = new Array();
	
	/*var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"day": jsonData[i].day, "value": jsonData[i].value, "average": jsonData[i].average};
		dataProvider.push(value);
	}*/
	var chart = AmCharts.makeChart( "chart2", {
	  "type": "serial",
	  "addClassNames": true,
	  "theme": "light",
	  "autoMargins": true,
	  "balloon": {
	    "adjustBorderColor": false,
	    "horizontalPadding": 10,
	    "verticalPadding": 8,
	    "color": "#ffffff"
	  },
	  "titles": [{
	        "text": "개인정보 접속기록 전체현황",
	        "size": 20
	  }],

	  "dataProvider": [ {
	    "day": "2017-02-01",
	    "value": 110000,
	    "average": 136667
	  }, {
	    "day": "2017-02-02",
	    "value": 100000,
	    "average": 136667
	  }, {
	    "day": "2017-02-03",
	    "value": 120000,
	    "average": 136667
	  }, {
	    "day": "2017-02-04",
	    "value": 135000,
	    "average": 136667
	  }, {
	    "day": "2017-02-05",
	    "value": 200000,
	    "average": 136667
	    //"dashLengthLine": 5
	  }, {
	    "day": "2017-02-06",
	    "value": 150000,
	    "average": 136667
	  }, {
	    "day": "2017-02-07",
	    "value": 125000,
	    "average": 136667
	  }, {
	    "day": "2017-02-08",
	    "value": 150000,
	    "average": 136667
	  }, {
	    "day": "2017-02-09",
	    "value": 140000,
	    "average": 136667
	  } ],
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "position": "left"
	  } ],
	  "startDuration": 1,
	  "graphs": [ {
	    "alphaField": "alpha",
	    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	    "fillAlphas": 1,
	    "title": "개인정보 접속기록",
	    "type": "column",
	    "valueField": "value",
	    "dashLengthField": "dashLengthColumn"
	  }, {
	    "id": "graph2",
	    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	    "bullet": "round",
	    "lineThickness": 3,
	    "bulletSize": 7,
	    "bulletBorderAlpha": 1,
	    "bulletColor": "#FFFFFF",
	    "useLineColorForBulletBorder": true,
	    "bulletBorderThickness": 3,
	    "fillAlphas": 0,
	    "lineAlpha": 1,
	    "title": "평균",
	    "valueField": "average",
	    "dashLengthField": "dashLengthLine"
	  } ],
	  "categoryField": "day",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "axisAlpha": 0,
	    "tickLength": 0
	  },
	  "export": {
	    "enabled": false
	  }
	} );
}

function setchart3_1() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart3_1.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date
		},
		success: function(data) {
			drawchart3_1(data);
		}
	});
}

function drawchart3_1(data) {
	var dataProvider = new Array();
	
	/*var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"day": jsonData[i].day, "value": jsonData[i].value, "average": jsonData[i].average};
		dataProvider.push(value);
	}*/
	var chart = AmCharts.makeChart( "chart3_1", {
	  "type": "serial",
	  "theme": "light",
	  "titles": [{
	        "text": "[월별]",
	        "size": 15
	    }],
	  "dataProvider": [ {
	    "country": "USA",
	    "visits": 2025
	  }, {
	    "country": "China",
	    "visits": 1882
	  }, {
	    "country": "Japan",
	    "visits": 1809
	  }, {
	    "country": "Germany",
	    "visits": 1322
	  }, {
	    "country": "UK",
	    "visits": 1122
	  }, {
	    "country": "France",
	    "visits": 1114
	  }, {
	    "country": "India",
	    "visits": 984
	  }, {
	    "country": "Spain",
	    "visits": 711
	  }, {
	    "country": "Netherlands",
	    "visits": 665
	  }, {
	    "country": "Russia",
	    "visits": 580
	  }, {
	    "country": "South Korea",
	    "visits": 443
	  }, {
	    "country": "Canada",
	    "visits": 441
	  }, {
	    "country": "Brazil",
	    "visits": 395
	  } ],
	  "valueAxes": [ {
	    "gridColor": "#FFFFFF",
	    "gridAlpha": 0.2,
	    "dashLength": 0
	  } ],
	  "gridAboveGraphs": true,
	  "startDuration": 1,
	  "graphs": [ {
	    "balloonText": "[[category]]: <b>[[value]]</b>",
	    "fillAlphas": 0.8,
	    "lineAlpha": 0.2,
	    "type": "column",
	    "valueField": "visits"
	  } ],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "country",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "gridAlpha": 0,
	    "tickPosition": "start",
	    "tickLength": 20
	  },
	  "export": {
	    "enabled": false
	  }

	} );
}

function setchart3_2() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart3_2.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date
		},
		success: function(data) {
			drawchart3_2(data);
		}
	});
}

function drawchart3_2(data) {
	var dataProvider = new Array();
	
	/*var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"day": jsonData[i].day, "value": jsonData[i].value, "average": jsonData[i].average};
		dataProvider.push(value);
	}*/
	var chart = AmCharts.makeChart( "chart3_2", {
	  "type": "serial",
	  "theme": "light",
	  "titles": [{
	        "text": "[월별]",
	        "size": 15
	    }],
	  "dataProvider": [ {
	    "week": "Mon",
	    "value": 2025
	  }, {
	    "week": "Tue",
	    "value": 1882
	  }, {
	    "week": "Wed",
	    "value": 1809
	  }, {
	    "week": "Thu",
	    "value": 1322
	  }, {
	    "week": "Fri",
	    "value": 1122
	  }, {
	    "week": "Sat",
	    "value": 1114
	  }, {
	    "week": "Sun",
	    "value": 984
	  }],
	  "valueAxes": [ {
	    "gridColor": "#FFFFFF",
	    "gridAlpha": 0.2,
	    "dashLength": 0
	  } ],
	  "gridAboveGraphs": true,
	  "startDuration": 1,
	  "graphs": [ {
	    "balloonText": "[[category]]: <b>[[value]]</b>",
	    "fillAlphas": 0.8,
	    "lineAlpha": 0.2,
	    "type": "column",
	    "valueField": "value"
	  } ],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "week",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "gridAlpha": 0,
	    "tickPosition": "start",
	    "tickLength": 20
	  },
	  "export": {
	    "enabled": false
	  }

	} );
}

function setchart3_3() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart3_3.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date
		},
		success: function(data) {
			drawchart3_3(data);
		}
	});
}

function drawchart3_3(data) {
	var dataProvider = new Array();
	
	var chart = AmCharts.makeChart("chart3_3", {
	    "type": "serial",
	    "theme": "light",
	    "legend": {
	        "useGraphSettings": true,
	        "enabled":false	// 밑에 캡션
	    },
	    "titles": [{
	        "text": "[일별]",
	        "size": 15
	    }],
	    "dataProvider": [{
	        "day": "0시",
	        "avg": 1,
	        "today": 5
	    }, {
	        "day": "3시",
	        "avg": 1,
	        "today": 2
	    }, {
	        "day": "6시",
	        "avg": 2,
	        "today": 3
	    }, {
	        "day": "9시",
	        "avg": 3,
	        "today": 4
	    }, {
	        "day": "12시",
	        "avg": 5,
	        "today": 1
	    }, {
	        "day": "15시",
	        "avg": 3,
	        "today": 2
	    }, {
	        "day": "18시",
	        "avg": 1,
	        "today": 2
	    }, {
	        "day": "21시",
	        "avg": 2,
	        "today": 1
	    }, {
	        "day": "24시",
	        "avg": 3,
	        "today": 5
	    }],
	    "valueAxes": [{
	        "integersOnly": true,
	        "maximum": 6,
	        "minimum": 1,
	        "reversed": false,
	        "axisAlpha": 0,
	        "dashLength": 5,
	        "gridCount": 10
	    }],
	    "startDuration": 0.5,
	    "graphs": [{
	        //"balloonText": "place taken by Italy in [[category]]: [[value]]",
	        "bullet": "round",
	        "title": "avg",
	        "valueField": "avg",
	        "type": "smoothedLine",
			"fillAlphas": 0
	    }, {
	        //"balloonText": "place taken by Germany in [[category]]: [[value]]",
	        "bullet": "round",
	        "title": "today",
	        "valueField": "today",
	        "type": "smoothedLine",
			"fillAlphas": 0
	    }],
	    "chartCursor": {
	        "cursorAlpha": 0,
	        "zoomable": false
	    },
	    "categoryField": "day",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "fillAlpha": 0.05,
	        "fillColor": "#000000",
	        "gridAlpha": 0,
	        "position": "bottom"
	    },
	    "export": {
	    	"enabled": false,
	        "position": "bottom-right"
	     }
	});
}

function setchart4() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart4.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date
		},
		success: function(data) {
			drawchart4(data);
		}
	});
}

function drawchart4(data) {
	var chart = AmCharts.makeChart( "chart4", {
	  "type": "serial",
	  "addClassNames": true,
	  "theme": "light",
	  "autoMargins": true,
	  "balloon": {
	    "adjustBorderColor": false,
	    "horizontalPadding": 10,
	    "verticalPadding": 8,
	    "color": "#ffffff"
	  },
	  "legend": {
	        "useGraphSettings": true
	  },

	  "dataProvider": [ {
	    "team": "보조사업 관리",
	    "pre": 500,
	    "cur": 10000
	  }, {
	    "team": "상수도 관리",
	    "pre": 600,
	    "cur": 600
	  }, {
	    "team": "이호조",
	    "pre": 600,
	    "cur": 1000
	  }, {
	    "team": "고객메일 시스템",
	    "pre": 600,
	    "cur": 1000
	  }, {
	    "team": "고객구매 시스템",
	    "pre": 600,
	    "cur": 1000
	    //"dashLengthLine": 5
	  }, {
	    "team": "홍보메일시스템",
	    "pre": 1000,
	    "cur": 500
	  }, {
	    "team": "만족도조사시스템",
	    "pre": 1000,
	    "cur": 500
	  }, {
	    "team": "통계시스템",
	    "pre": 1000,
	    "cur": 500
	  }, {
	    "team": "학사정보시스템",
	    "pre": 500,
	    "cur": 600
	  }, {
	    "team": "고객관리시스템",
	    "pre": 500,
	    "cur": 600
	  } ],
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "position": "left"
	  } ],
	  "startDuration": 1,
	  "graphs": [ {
	    "alphaField": "alpha",
	    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	    "fillAlphas": 1,
	    "title": "이전월",
	    "type": "column",
	    "valueField": "pre",
	    "dashLengthField": "dashLengthColumn"
	  }, {
	    "id": "graph2",
	    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	    "bullet": "round",
	    "lineThickness": 3,
	    "bulletSize": 7,
	    "bulletBorderAlpha": 1,
	    "bulletColor": "#FFFFFF",
	    "useLineColorForBulletBorder": true,
	    "bulletBorderThickness": 3,
	    "fillAlphas": 0,
	    "lineAlpha": 1,
	    "title": "금월",
	    "valueField": "cur",
	    "dashLengthField": "dashLengthLine"
	  } ],
	  "categoryField": "team",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "axisAlpha": 0,
	    "tickLength": 0,
	    "labelRotation": 45
	  },
	  "export": {
	    "enabled": false
	  }
	} );
}

function setchart5() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart5.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date
		},
		success: function(data) {
			drawchart5(data);
		}
	});
}

function drawchart5(data) {
	var chart = AmCharts.makeChart("chart5", {
		"type": "serial",
	     "theme": "light",
		"categoryField": "team",
		"rotate": true,
		"startDuration": 1,
		"categoryAxis": {
			"gridPosition": "start",
			"position": "left"
		},
		"legend": {
	        "useGraphSettings": true
		},
		"trendLines": [],
		"graphs": [
			{
				"balloonText": "금월:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-1",
				"lineAlpha": 0.2,
				"title": "금월",
				"type": "column",
				"valueField": "cur"
			},
			{
				"balloonText": "이전월:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-2",
				"lineAlpha": 0.2,
				"title": "이전월",
				"type": "column",
				"valueField": "pre"
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"position": "bottom",
				"axisAlpha": 0
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider": [
			{
				"team": "홍보팀",
				"pre": 10000,
				"cur": 500
			},
			{
				"team": "경영기획팀",
				"pre": 600,
				"cur": 600
			},
			{
				"team": "운영팀",
				"pre": 1000,
				"cur": 600
			},
			{
				"team": "서비스지원팀",
				"pre": 1000,
				"cur": 600
			},
			{
				"team": "마케팅팀",
				"pre": 1000,
				"cur": 600
			},
			{
				"team": "기획팀",
				"pre": 500,
				"cur": 1000
			},
			{
				"team": "콜센터",
				"pre": 500,
				"cur": 1000
			}
		],
	    "export": {
	    	"enabled": false
	     }

	});
}

function setchart6() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart6.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date
		},
		success: function(data) {
			drawchart6(data);
		}
	});
}

function drawchart6(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for(var i=0; i<jsonData.length; i++) {
		var resReq;
		switch(jsonData[i].req_type) {
		case "RD":
			resReq = "조회";
			break;
		case "CR":
			resReq = "등록";
			break;
		case "UD":
			resReq = "수정";
			break;
		case "DL":
			resReq = "삭제";
			break;
		case "EX":
			resReq = "다운로드";
			break;
		case "PR":
			resReq = "출력";
			break;
		}
		
		var value = {"category": resReq, "value": jsonData[i].cnt};
		dataProvider.push(value);
	}
	var chart = AmCharts.makeChart("chart6", {
	  "type": "pie",
	  "startDuration": 0,
	   "theme": "light",
	  "addClassNames": true,
	  "legend":{
	   	"position":"right",
	    "marginRight":0,
	    "autoMargins":true
	  },
	  "innerRadius": "30%",
	  "defs": {
	    "filter": [{
	      "id": "shadow",
	      "width": "200%",
	      "height": "200%",
	      "feOffset": {
	        "result": "offOut",
	        "in": "SourceAlpha",
	        "dx": 0,
	        "dy": 0
	      },
	      "feGaussianBlur": {
	        "result": "blurOut",
	        "in": "offOut",
	        "stdDeviation": 5
	      },
	      "feBlend": {
	        "in": "SourceGraphic",
	        "in2": "blurOut",
	        "mode": "normal"
	      }
	    }]
	  },
	  "dataProvider": dataProvider,
	  "valueField": "value",
	  "titleField": "category",
	  "export": {
	    "enabled": false
	  }
	});
}

function setchart7() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart7.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date
		},
		success: function(data) {
			drawchart7(data);
		}
	});
}

function drawchart7(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	alert(strResult.length);
	for(var i=0; i<jsonData.length; i++) {
		var value = {"category":jsonData[i].category, "value":jsonData[i].value, "average": 1500};
		dataProvider.push(value);
	}
	var chart = AmCharts.makeChart( "chart7", {
	  "type": "serial",
	  "addClassNames": true,
	  "theme": "light",
	  "autoMargins": true,
	  "balloon": {
	    "adjustBorderColor": false,
	    "horizontalPadding": 10,
	    "verticalPadding": 8,
	    "color": "#ffffff"
	  },
	  "legend": {
	        "useGraphSettings": true
	  },

	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "position": "left"
	  } ],
	  "startDuration": 1,
	  "graphs": [ {
	    "alphaField": "alpha",
	    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	    "fillAlphas": 1,
	    "title": "개인정보 접속기록",
	    "type": "column",
	    "valueField": "value",
	    "dashLengthField": "dashLengthColumn"
	  }, {
	    "id": "graph2",
	    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	    "bullet": "round",
	    "lineThickness": 3,
	    "bulletSize": 7,
	    "bulletBorderAlpha": 1,
	    "bulletColor": "#FFFFFF",
	    "useLineColorForBulletBorder": true,
	    "bulletBorderThickness": 3,
	    "fillAlphas": 0,
	    "lineAlpha": 1,
	    "title": "평균",
	    "valueField": "average",
	    "dashLengthField": "dashLengthLine"
	  } ],
	  "categoryField": "category",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "axisAlpha": 0,
	    "tickLength": 0,
	    "labelRotation": 45
	  },
	  "export": {
	    "enabled": false
	  }
	} );
}

function setchart8(){
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart8.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date
		},
		success: function(data) {
			drawchart8(data);
		}
	});
}

function drawchart8(data) {
	var chart = AmCharts.makeChart("chart8", {
	    "type": "serial",
		"theme": "light",
	    "legend": {
	        "horizontalGap": 1,
	        "maxColumns": 3,
	        "position": "top",
			"useGraphSettings": true,
			"markerSize": 10
	    },
	    "dataProvider": [{
	        "category": "기부금",
	        "type1": 15000,
	        "type2": 1000,
	        "type3": 19500,
	        "type4": 10000,
	        "type5": 16000,
	        "type6": 10000
	    }, {
	        "category": "교원업적",
	        "type1": 4000,
	        "type2": 2000,
	        "type3": 18000,
	        "type4": 7800,
	        "type5": 10000,
	        "type6": 20000
	    }, {
	        "category": "학사관리시스템",
	        "type1": 16000,
	        "type2": 1000,
	        "type3": 15000,
	        "type4": 11000,
	        "type5": 15000,
	        "type6": 3000
	    }, {
	        "category": "ERP",
	        "type1": 17000,
	        "type2": 2000,
	        "type3": 10000,
	        "type4": 10000,
	        "type5": 20000,
	        "type6": 2000
	    }, {
	        "category": "SSO",
	        "type1": 10000,
	        "type2": 1500,
	        "type4": 16000,
	        "type5": 5000,
	        "type6": 3000
	    }, {
	        "category": "도서관리",
	        "type1": 1000,
	        "type3": 4000,
	        "type4": 5000,
	        "type5": 3000,
	        "type6": 18000
	    }],
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.5,
	        "gridAlpha": 0
	    }],
	    "graphs": [{
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "주민등록번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type1"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "신용카드번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type2"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "계좌번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type3"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "전화번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type4"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "핸드폰번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type5"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "이메일",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type6"
	    }],
	    "rotate": true,
	    "categoryField": "category",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "gridAlpha": 0,
	        "position": "left"
	    },
	    "export": {
	    	"enabled": false
	     }
	});
}

function setchart9(){
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart9.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date
		},
		success: function(data) {
			drawchart9(data);
		}
	});
}

function drawchart9(data) {
	var chart = AmCharts.makeChart("chart9", {
	    "type": "serial",
		"theme": "light",
	    "legend": {
	        "horizontalGap": 1,
	        "maxColumns": 3,
	        "position": "top",
			"useGraphSettings": true,
			"markerSize": 10
	    },
	    "dataProvider": [{
	        "category": "마케팅",
	        "type1": 15000,
	        "type2": 1000,
	        "type3": 19500,
	        "type4": 10000,
	        "type5": 16000,
	        "type6": 10000
	    }, {
	        "category": "영업지원",
	        "type1": 4000,
	        "type2": 2000,
	        "type3": 18000,
	        "type4": 7800,
	        "type5": 10000,
	        "type6": 20000
	    }, {
	        "category": "빅데이터개발",
	        "type1": 16000,
	        "type2": 1000,
	        "type3": 15000,
	        "type4": 11000,
	        "type5": 15000,
	        "type6": 3000
	    }, {
	        "category": "플랫폼개발",
	        "type1": 17000,
	        "type2": 2000,
	        "type3": 10000,
	        "type4": 10000,
	        "type5": 20000,
	        "type6": 2000
	    }, {
	        "category": "연구기획",
	        "type1": 10000,
	        "type2": 1500,
	        "type4": 16000,
	        "type5": 5000,
	        "type6": 3000
	    }, {
	        "category": "경영기획",
	        "type1": 1000,
	        "type3": 4000,
	        "type4": 5000,
	        "type5": 3000,
	        "type6": 18000
	    }],
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.5,
	        "gridAlpha": 0
	    }],
	    "graphs": [{
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "주민등록번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type1"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "신용카드번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type2"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "계좌번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type3"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "전화번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type4"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "핸드폰번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type5"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "이메일",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type6"
	    }],
	    "rotate": true,
	    "categoryField": "category",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "gridAlpha": 0,
	        "position": "left"
	    },
	    "export": {
	    	"enabled": false
	     }
	});
}

function setchart10(){
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart10.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date
		},
		success: function(data) {
			drawchart10(data);
		}
	});
}

function drawchart10(data) {
	var chart = AmCharts.makeChart("chart10", {
	    "type": "serial",
		"theme": "light",
	    "legend": {
	        "horizontalGap": 1,
	        "maxColumns": 3,
	        "position": "top",
			"useGraphSettings": true,
			"markerSize": 10
	    },
	    "dataProvider": [{
	        "category": "김태근",
	        "type1": 15000,
	        "type2": 1000,
	        "type3": 19500,
	        "type4": 10000,
	        "type5": 16000,
	        "type6": 10000
	    }, {
	        "category": "신재희",
	        "type1": 4000,
	        "type2": 2000,
	        "type3": 18000,
	        "type4": 7800,
	        "type5": 10000,
	        "type6": 20000
	    }, {
	        "category": "김세민",
	        "type1": 16000,
	        "type2": 1000,
	        "type3": 15000,
	        "type4": 11000,
	        "type5": 15000,
	        "type6": 3000
	    }, {
	        "category": "서은희",
	        "type1": 17000,
	        "type2": 2000,
	        "type3": 10000,
	        "type4": 10000,
	        "type5": 20000,
	        "type6": 2000
	    }, {
	        "category": "이상아",
	        "type1": 10000,
	        "type2": 1500,
	        "type4": 16000,
	        "type5": 5000,
	        "type6": 3000
	    }, {
	        "category": "박진만",
	        "type1": 1000,
	        "type3": 4000,
	        "type4": 5000,
	        "type5": 3000,
	        "type6": 18000
	    }],
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.5,
	        "gridAlpha": 0
	    }],
	    "graphs": [{
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "주민등록번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type1"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "신용카드번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type2"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "계좌번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type3"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "전화번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type4"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "핸드폰번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type5"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "이메일",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type6"
	    }],
	    "rotate": true,
	    "categoryField": "category",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "gridAlpha": 0,
	        "position": "left"
	    },
	    "export": {
	    	"enabled": false
	     }
	});
}

