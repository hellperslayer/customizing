var imageList = new Array();
var imageList = {};

$(document).ready(function() {
	if(download_type == 'pdf') {
		$("#buttonDiv").css("display","none");
		$(".downloadChartClass").css("height","500px");
		$(".downloadChartClass").css("width","930px");
		$(".downloadClass").css("width","900px");
		$(".logo").css("width","100%");
		for (var x = 0; x < AmCharts.charts.length; x++) {
			$("#"+AmCharts.charts[x].div.id).css("width","930px");
		}
		setInterval(function() {
			setChart.init();
		},500);
	} else {
		$(".downloadClass").css("width","100%");
		$(".downloadChartClass").css("width","100%");
		for (var x = 0; x < AmCharts.charts.length; x++) {
			$("#"+AmCharts.charts[x].div.id).css("width","100%");
		}
		setChart.init();
	}
	
	if(period_type == 5){
		$('#saveButton').hide();
	}
});


var setChart = {
		init : function() {
			var chartlist = $('.downloadChartClass');
			for(var i=0 ; i < chartlist.length ; i++){
				eval('setChart.'+chartlist[i].id+'()');
			}
		},
		systemChart : function(){systemChart()},
		deptChart : function(){deptChart()},
		userChart : function(){userChart()}
}

function systemChart() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/abnormalSystemChart.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawSystemChart(data);
		}
	});
}
function drawSystemChart(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"system_name": jsonData[i].system_name,"totalCnt": jsonData[i].totalCnt,"logcnt": jsonData[i].logcnt};
		dataProvider.push(value);
	}
	var title1 = "처리량";
	var title2 = "이용량";
	
	var systemChart = AmCharts.makeChart( "systemChart", {
	  "type": "serial",
	  "theme": "light",
	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "title": "비정상행위(건)"
	  } ],
	  "legend": {
	        "useGraphSettings": true
		},
	  "startDuration": 0,
	  "graphs": [ 
		  {
				"balloonText": "[[title]]:[[value]]",
				"title": title1,
				"id": "AmGraph-2",
				"lineAlpha": "10",
				"bullet" : "round",
				"type": "line",
				"valueField": "totalCnt",
				"labelPosition": "bottom",
				"labelText": "[[value]]"
			},
			{
				"balloonText": "[[title]]:[[value]]",
				"title": title2,
				"id": "AmGraph-1",
				"lineAlpha": "10",
				"bullet" : "round",
				"type": "line",
				"valueField": "logcnt",
				"labelPosition": "bottom",
				"labelText": "[[value]]"
	  } ],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "system_name",
	  "categoryAxis": {
	    "gridPosition": "start",
		"axisAlpha": 0,
		"tickLength": 0,
	    "labelRotation": 45
	  },
	  "export": {
	    "enabled": true
	  }
	} );
}

function deptChart() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/abnormalDeptChart.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawDeptChart(data);
		}
	});
}
function drawDeptChart(data) { // ctrl reportDetail_chart_new2 참고하기
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"dept_name": jsonData[i].dept_name,"totalCnt": jsonData[i].totalCnt,"logcnt": jsonData[i].logcnt};
		dataProvider.push(value);
	}
	var title1 = "이용량";
	var title2 = "처리량";
	
	var deptChart = AmCharts.makeChart("deptChart", {
		"type": "serial",
	     "theme": "light",
		"categoryField": "dept_name",
		"rotate": true,
		"startDuration": 0,
		"categoryAxis": {
			"gridPosition": "start",
			"position": "left"
		},
		"legend": {
	        "useGraphSettings": true
		},
		"trendLines": [],
		"graphs": [
			{
				"balloonText": "[[title]]:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-2",
				"lineAlpha": 0.2,
				"title": title2,
				"type": "column",
				"valueField": "totalCnt",
				"labelPosition": "right",
				"labelText": "[[value]]"
			},
			{
				"balloonText": "[[title]]:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-1",
				"lineAlpha": 0.2,
				"title": title1,
				"type": "column",
				"valueField": "logcnt",
				"labelPosition": "right",
				"labelText": "[[value]]"
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"position": "bottom",
				"axisAlpha": 0,
				"title": "비정상행위(건)"
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider":dataProvider,
	    "export": {
	    	"enabled": true
	     }
	});
}

function userChart() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/abnormalUserChart.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawUserChart(data);
		}
	});
}
function drawUserChart(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"emp_user_name": jsonData[i].emp_user_name,"totalCnt": jsonData[i].totalCnt,"logcnt": jsonData[i].logcnt};
		dataProvider.push(value);
	}
	var title1 = "이용량";
	var title2 = "처리량";
	
	var userChart = AmCharts.makeChart("userChart", {
		"type": "serial",
	     "theme": "light",
		"categoryField": "emp_user_name",
		"rotate": true,
		"startDuration": 0,
		"categoryAxis": {
			"gridPosition": "start",
			"position": "left"
		},
		"legend": {
	        "useGraphSettings": true
		},
		"trendLines": [],
		"graphs": [
			{
				"balloonText": "[[title]]:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-2",
				"lineAlpha": 0.2,
				"title": title2,
				"type": "column",
				"valueField": "totalCnt",
				"labelPosition": "right",
				"labelText": "[[value]]"
			},
			{
				"balloonText": "[[title]]:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-1",
				"lineAlpha": 0.2,
				"title": title1,
				"type": "column",
				"valueField": "logcnt",
				"labelPosition": "right",
				"labelText": "[[value]]"
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"position": "bottom",
				"axisAlpha": 0,
				"title": "비정상행위(건)"
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider":dataProvider,
	    "export": {
	    	"enabled": true
	     }
	});
}

/*var setChart = {
		init : function(){
			var chartlist = $('.downloadChartClass');
			for(var i=0 ; i < chartlist.length ; i++){
				eval('setChart.'+chartlist[i].id+'()');
			}
		},
		
		chart1 : function(){setchart1();},
		chart3 : function(){setchart3();},
		chart4 : function(){setchart4();}
}
*/

/*function setchart1() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail2_chart1.html',
		data: { 
			"period_type" : period_type,
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart1(data);
		}
	});
}

function drawchart1(data) {
	var dataProvider = new Array();
	var JsonData = JSON.parse(data);
	var arrCur = JsonData["cur"];
	var arrPre = JsonData["pre"];
	for(var i=0; i<arrCur.length; i++) {
		var value = {"rule": arrCur[i].rule_nm, "cur": arrCur[i].cnt, "pre": arrPre[i].cnt, "seq": arrCur[i].rule_cd};
		dataProvider.push(value);
	}
	var title1 = JsonData["title1"];
	var title2 = JsonData["title2"];
	var chart1 = AmCharts.makeChart("chart1", {
		"type": "serial",
	     "theme": "light",
		"categoryField": "rule",
//		"depth3D": 10,
//	    "angle": 30,
		"rotate": true,
		"startDuration": 0,
		"autoMargins": true,
		"categoryAxis": {
			"gridPosition": "start",
			"position": "left"
		},
		"legend": {
	        "useGraphSettings": true,
	        "position": "bottom",
	        "autoMargins": true,
		},
		"trendLines": [],
		"graphs": [
			{
				"balloonText": "[[title]]:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-2",
				"lineAlpha": 0.2,
				"title": title2,
				"type": "column",
				"valueField": "pre",
				"labelText": "[[value]]",
				"labelPosition": "left",
				"descriptionField": "seq"
			},
			{
				"balloonText": "[[title]]:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-1",
				"lineAlpha": 0.2,
				"title": title1,
				"type": "column",
				"valueField": "cur",
				"labelText": "[[value]]",
				"labelPosition": "left",
				"descriptionField": "seq"
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"position": "bottom",
				"axisAlpha": 0,
				"title": "비정상행위 의심건수(건)"
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider": dataProvider,
	    "export": {
	    	"enabled": true
	     },
	      "addClassNames": true
	});
	chart1.addListener("clickGraphItem", handleClick1);
}

function handleClick1(event) {
	opener.eventForm.action = rootPath + '/extrtCondbyInq/list.html';
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["scen_seq_rp"].value = event.item.description;
	opener.eventForm.submit();
}
	

function setchart2() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail2_chart2.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date
		},
		success: function(data) {
			drawchart2(data);
		}
	});
}

function drawchart2(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"rule": jsonData[i].rule,"cnt": jsonData[i].cnt};
		dataProvider.push(value);
	}
	var chart2 = AmCharts.makeChart("chart2", {
	    "theme": "light",
	    "type": "serial",
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        //"title": "Income in millions, USD"
	    }],
	    "graphs": [{
	        "balloonText": "[[category]]:[[value]]",
	    	"fillAlphas": 0.85,
	        "lineAlpha": 0.1,
	        "type": "column",
	        "topRadius":1,
	        "title": "세부지표",
	        "valueField": "cnt",
	        "columnWidth": 0.3
	    }],
	    "depth3D": 10,
	    "angle": 30,
	    "rotate": true,
	    "startDuration": 0,
	    "categoryField": "rule",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "fillAlpha": 0.05,
	        "position": "left",
	        "title": "비정상행위 의심건수(건)"
	    },
	    "export": {
	    	"enabled": true
	     },
	      "addClassNames": true
	});
}
	

function setchart3() {
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail2_chart3.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart3(data);
		}
	});
}

function handleClick3(event) {
	opener.eventForm.action = rootPath + '/extrtCondbyInq/list.html';
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["dept_name_rp"].value = event.item.category;
	opener.eventForm.submit();
}

function drawchart3(data) {
	var jsonData = JSON.parse(data);
	
	var rule_list = jsonData["rule_list"];
	var emp_list = jsonData["emp_list"];
	
	var graphs = new Array();
	for(var i=0; i<rule_list.length; i++) {
		var value = {"balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
			        "fillAlphas": 0.8,
			        "labelText": "[[value]]",
			        "lineAlpha": 0.3,
			        "title": rule_list[i].rule_nm,
			        "type": "column",
					"color": "#000000",
			        "valueField": "cnt_"+(i+1)+"000",
			        "columnWidth" : 0.6};
		graphs.push(value);
	}
	
	var chart3 = AmCharts.makeChart("chart3", {
	    "type": "serial",
		"theme": "light",
		"startDuration": 0,
	    "legend": {
	        "horizontalGap": 10,
	        "maxColumns": 1,
	        "position": "right",
			"useGraphSettings": true,
			"markerSize": 10
	    },
//	    "depth3D": 20,
//	    "angle": 40,
	    "dataProvider": emp_list,
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.3,
	        "gridAlpha": 0.3,
	        "title": "비정상행위 의심건수(건)"
	    }],
	    "graphs": graphs,
	    "categoryField": "dept_name",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "gridAlpha": 0,
	        "position": "left"
	    },
	    "export": {
	    	"enabled": true
	     },
	      "addClassNames": true

	});
	chart3.addListener("clickGraphItem", handleClick3);
}
function setchart4() {
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail2_chart4.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart4(data);
		}
	});
}

function drawchart4(data) {
	var jsonData = JSON.parse(data);
	var listEmpDetail = jsonData["listEmpDetail"];
	var dataProvider = new Array();
	for (var i = 0; i < listEmpDetail.length; i++) {
		var value = {"emp_user_name": listEmpDetail[i].emp_user_name,"cnt": listEmpDetail[i].cnt};
		dataProvider.push(value);
	}
	var chart4 = AmCharts.makeChart( "chart4", {
		  "type": "serial",
		  "theme": "light",
		  "dataProvider": dataProvider,
		  "valueAxes": [ {
		    "axisAlpha": 0,
		    "title": "비정상행위 의심건수(건)"
		  } ],
		  "startDuration": 0,
		  "graphs": [ {
		    "balloonText": "[[category]]: <b>[[value]]</b>",
		    "fillAlphas": 1,
		    "type": "column",
		    "valueField": "cnt",
		    "labelText": "[[value]]",
		    "labelPosition": "bottom",
		    "labelOffset": -1
		  } ],
		  "chartCursor": {
		    "categoryBalloonEnabled": false,
		    "cursorAlpha": 0,
		    "zoomable": false
		  },
		  "categoryField": "emp_user_name",
		  "categoryAxis": {
		    "gridPosition": "start",
			"axisAlpha": 0,
			"tickLength": 0,
		    "labelRotation": 45
		  },
		  "export": {
		    "enabled": true
		  }
		} );
}*/


