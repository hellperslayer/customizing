var imageList = new Array();
var imageList = {};
	
$(document).ready(function() {
	if(download_type == 'pdf') {
		$("#buttonDiv").css("display","none");
		$(".downloadChartClass").css("height","500px");
		$(".downloadChartClass").css("width","850px");
		$(".downloadClass").css("width","850px");
		$(".HStyle5").css("width","850px");
		$('.updatetd').html("");		
		for (var x = 0; x < AmCharts.charts.length; x++) {
			$("#"+AmCharts.charts[x].div.id).css("width","850px");
		}
		setInterval(function() {
			setChart.init();
		},500);
	} else {
		$(".downloadClass").css("width","750px");
		$(".downloadChartClass").css("width","750px");
		for (var x = 0; x < AmCharts.charts.length; x++) {
			$("#"+AmCharts.charts[x].div.id).css("width","750px");
		}	
		setChart.init();
	}
	
});

function setCharts() {
	setchart1();
	setchart3();
	setchart4();
	setchart5();
	setchart6();
	setchart7();
	setchart8();
	setchart9_1();
	setchart9_3();
	setchart10();
	setchart11();
	setchart12();
	setchart13();
	setchart14_new();
	setchart15_new();
	setchart16_new();
	setchart17();
	setchart18();
	setchart19();
	
}

var setChart = {
		init : function(){
			var chartlist = $('.downloadChartClass');
			for(var i=0 ; i < chartlist.length ; i++){
				eval('setChart.'+chartlist[i].id+'()');
			}
		},
		
		chart1 : function(){setchart1();},
		chart3 : function(){setchart3();},
		chart4 : function(){setchart4();},
		chart5 : function(){setchart5();},
		chart6 : function(){setchart6();},
		chart7 : function(){setchart7();},
		chart8 : function(){setchart8();},
		chart9_1 : function(){setchart9_1();},
		chart9_3 : function(){setchart9_3();},
		chart10 : function(){setchart10();},
		chart11 : function(){setchart11();},
		chart12 : function(){setchart12();},
		chart13 : function(){setchart13();},
		chart14 : function(){setchart14_new();},
		chart15 : function(){setchart15_new();},
		chart16 : function(){setchart16_new();},
		chart17 : function(){setchart17();},
		chart18 : function(){setchart18();},
		chart19 : function(){setchart19();}
}

function saveAsDoc() {
	
	$.ajax({
		type : 'POST',
		url : rootPath + '/report/getImages.html?type=1',
		data : imageList,
		success : function(data) {
			var i=0;
			for(var img in imageList) { 
				var image = "#chartImage" + i;
				$(image).css("display", "block");
				i++;
			}
			
			var date = new Date();
			var year = date.getFullYear();
			var month = new String(date.getMonth() + 1);
			var day = new String(date.getDate());
			if(month.length == 1){ 
				month = "0" + month; 
			} 
			if(day.length == 1){ 
				day = "0" + day; 
			} 
			var regdate = year + "" + month + "" + day;
			var agent = navigator.userAgent.toLowerCase();
			if (agent.indexOf("msie") != -1) {
				saveAsPage(regdate);
			} else {
				exportHTML(regdate);
			}
			
			self.close();
		}
	});
}

function exportHTML(regdate){
    var header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' "+
         "xmlns:w='urn:schemas-microsoft-com:office:word' "+
         "xmlns='http://www.w3.org/TR/REC-html40'>"+
         "<head><meta charset='utf-8'><title>Export HTML to Word Document with JavaScript</title></head><body>";
    var footer = "</body></html>";
    var sourceHTML = header+document.getElementById("source-html").innerHTML+footer;
    
    var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
    var fileDownload = document.createElement("a");
    document.body.appendChild(fileDownload);
    fileDownload.href = source;
    fileDownload.download = 'report_'+regdate+'.doc';
    fileDownload.click();
    document.body.removeChild(fileDownload);
 }

function saveAsPage(regdate) {
	if (document.execCommand) {
		document.execCommand("SaveAs", false, "report5_" + regdate + ".doc");
	} else {
		alert("error..");
	}
}

function setchart1() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart1_1.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart1(data);
			drawtable1(data);
		}
	});
}

function drawchart1(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"system_name": jsonData[i].system_name,"cnt": jsonData[i].cnt};
		dataProvider.push(value);
	}
	
	var chart = AmCharts.makeChart( "chart1", {
	  "type": "serial",
	  "theme": "light",
	  "rotate": true,
	  "titles": [{
	        "text": "[개인정보 처리시스템별 접속기록 현황]",
	        "size": 15
	    }],
	  "dataProvider": dataProvider,
	  "valueAxes": [ {
		"axisAlpha": 0,
	    "title": "개인정보검출(건)"
	  } ],
	  "startDuration": 0,
	  "graphs": [ {
	    "balloonText": "[[category]]: <b>[[value]]</b>",
	    "fillAlphas": 1,
	    "type": "column",
	    "valueField": "cnt",
	    "labelText": "[[value]]",
	    "labelPosition": "left"
	  } ],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "system_name",
	  "categoryAxis": {
	    "gridPosition": "start",
		"axisAlpha": 0,
		"tickLength": 0,
	    "labelRotation": 45
	  },
	  "export": {
	    "enabled": true
	  },
      "addClassNames": true
	} );
}

function drawtable1(data) {
	$("#table1").find('tbody').empty();
	var jsonData = JSON.parse(data);
	var total = 0;
	
	if(jsonData != null) {		
		$.each(jsonData, function( i, item ) {
			
			var system_name = item.system_name;
			var cnt = item.cnt;
			
			$("<TR>").appendTo($("#table1").find('tbody'))
			.append($("<TD valign='middle' style='text-align: center;width:302;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;line-height:100%'>")
					.text(system_name)
			)
			.append($("<TD valign='middle' style='text-align: center;width:250;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;line-height:100%;'><SPAN STYLE='font-size:11.0pt;line-height:100%'>")
					.text(addCommaString(cnt.toString()))
			)
			.append($("<TD valign='middle' style='text-align: center;width:87;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;line-height:180%'>")
					.text("")
			)
			.show()
			
			total = Number(total) + Number(cnt);
		});
		
		$("<TR>").appendTo($("#table1").find('tbody'))
		.append($("<TD valign='middle' style='text-align: center;width:302;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;line-height:100%'>")
				.text("합계")
		)
		.append($("<TD valign='middle' style='text-align: center;width:250;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;line-height:100%;'><SPAN STYLE='font-size:11.0pt;line-height:100%'>")
				.text(addCommaString(total.toString()))
		)
		.append($("<TD valign='middle' style='text-align: center;width:87;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;line-height:180%'>")
				.text("")
		)
		.show()
	}
}


function setchart3() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportHalf_chart3.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq,
			"half_type" : half_type
		},
		success: function(data) {
			drawchart3(data);
			drawtable3(data, half_type);
		}
	});
}

function drawchart3(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for(var i=0; i<jsonData.length; i++) {
		var value = {"system_name":jsonData[i].system_name, "cnt":jsonData[i].cnt};
		dataProvider.push(value);
	}
	
	var chart = AmCharts.makeChart( "chart3", {
	  "type": "serial",
	  "theme": "light",
	  "rotate": true,
	  "titles": [{
	        "text": "[개인정보 처리시스템별 처리 현황]",
	        "size": 15
	    }],
	  "dataProvider": dataProvider,
	  "valueAxes": [ {
		"axisAlpha": 0,
	    "title": "개인정보처리량"
	  } ],
	  "startDuration": 0,
	  "graphs": [ {
	    "balloonText": "[[category]]: <b>[[value]]</b>",
	    "fillAlphas": 1,
	    "type": "column",
	    "valueField": "cnt",
	    "labelText": "[[value]]",
	    "labelPosition": "left"
	  } ],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "system_name",
	  "categoryAxis": {
	    "gridPosition": "start",
		"axisAlpha": 0,
		"tickLength": 0,
	    "labelRotation": 45
	  },
	  "export": {
	    "enabled": true
	  },
      "addClassNames": true

	} );
}

function drawtable3(data, half_type) {
	$("#table3").find('tbody').empty();
	var jsonData = JSON.parse(data);
	var total = 0;
	var max_amount = 0;
	var max_system;
	if(jsonData != null) {		
		$.each(jsonData, function( i, item ) {
			
			var system_name = item.system_name;
			var cnt = item.cnt;
			
			if(Number(cnt) > Number(max_amount)) {
				max_amount = cnt;
				max_system = system_name;
			}
			$("<TR>").appendTo($("#table3").find('tbody'))
			.append($("<TD valign='middle' style='text-align:center;width:101;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;line-height:160%'>")
					.text(i + 1)
			)
			.append($("<TD valign='middle' style='text-align:center;width:201;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;line-height:100%'>")
					.text(system_name)
			)
			.append($("<TD valign='middle' style='text-align:center;width:250;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle15><SPAN STYLE='font-size:11.0pt;line-height:100%'>")
					.text(addCommaString(cnt.toString()))
			)
			.append($("<TD valign='middle' style='text-align:center;width:87;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:12.0pt;line-height:180%'>")
					.text("")
			)
			.show()
			
			total = Number(total) + Number(cnt);
		});
		
		if(half_type == 1)
			quarter = "상반기";
		else
			quarter = "하반기";
		
		$("<TR>").appendTo($("#table3").find('tbody'))
		.append($("<TD colspan='2' valign='middle' style='text-align:center;width:302;height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;line-height:160%'>")
				.text(quarter + " 총계")
		)
		.append($("<TD colspan='2' valign='middle' style='text-align:center;width:338;height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;line-height:100%;'><SPAN STYLE='font-size:11.0pt;line-height:100%'>")
				.text(addCommaString(total.toString()))
		)
		.show()
		
		$("#total_cnt").html(addCommaString(total.toString()));
		$("#total_cnt2").html(addCommaString(total.toString()));
		$("#max_system").html(max_system);
		$("#max_amount").html(addCommaString(max_amount.toString()));
	}
}

function setchart4() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportHalf_chart4.html',
		data: { 
			"start_date" : stDate,
			"end_date" : edDate,
			"system_seq" : system_seq,
			"half_type" : half_type
		},
		success: function(data) {
			drawchart4(data);
			drawtable4(data, half_type);
		}
	});
}

function drawchart4(data) {
	
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for (var i = 0; i < jsonData.length; i++) {
		var date = jsonData[i].proc_date.substring(0,4) + "년 " + jsonData[i].proc_date.substring(4,6) + "월";
		var value = {"day": date, "value": jsonData[i].cnt};
		dataProvider.push(value);
	}
	
	AmCharts.makeChart( "chart4", {
	  "type": "serial",
	  "addClassNames": true,
	  "theme": "light",
	  "autoMargins": true,
	  "balloon": {
	    "adjustBorderColor": false,
	    "horizontalPadding": 10,
	    "verticalPadding": 8,
	    "color": "#ffffff"
	  },
	  "titles": [{
        "text": "[개인정보 처리량 월별 추이]",
        "size": 15
      }],
  	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "position": "left",
	    "title": "개인정보처리량"
	  } ],
	  "startDuration": 0,
	  "graphs": [ {
	    "alphaField": "alpha",
	    "balloonText": "<span style='font-size:12px;'>[[title]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	    "fillAlphas": 1,
	    "title": "개인정보 건수",
	    "type": "column",
	    "valueField": "value",
	    "dashLengthField": "dashLengthColumn",
	    "labelText": "[[value]]",
	    "labelPosition": "bottom",
	    "labelOffset": -1
	  } ],
	  "categoryField": "day",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "axisAlpha": 0,
	    "tickLength": 0
	  },
	  "export": {
	    "enabled": true
	  },
      "addClassNames": true
	} );
}

function drawtable4(data, half_type) {
	$("#table4").find('tbody').empty();
	var jsonData = JSON.parse(data);
	var total = 0;
	var max_amount = 0;
	var max_month;
	var prev_mount = 0;
	if(jsonData != null) {		
		$.each(jsonData, function( i, item ) {
			
			var proc_date = item.proc_date;
			var cnt = item.cnt;
			var diff_mount = 0;
			var string_diff;
			var res;
			if(i > 0) {
				diff_mount = Number(prev_mount) - Number(cnt);
				if(diff_mount > 0) {
					string_diff = "▼ ";
					diff_mount = Math.abs(diff_mount);
				}else {
					string_diff = "▲ ";
					diff_mount = Math.abs(diff_mount);
				}
				res = string_diff + addCommaString(diff_mount.toString());
			}else {
				res = "-";
			}
			
			if(Number(cnt) > Number(max_amount)) {
				max_amount = cnt;
				max_month = proc_date;
			}
			$("<TR>").appendTo($("#table4").find('tbody'))
			.append($("<TD valign='middle' style='text-align:center;width:123;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;line-height:160%'>")
					.text(proc_date)
			)
			.append($("<TD valign='middle' style='text-align:center;width:197;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;line-height:100%;'><SPAN STYLE='font-size:12.0pt;line-height:100%'>")
					.text(addCommaString(cnt.toString()))
			)
			.append($("<TD valign='middle' style='text-align:center;width:231;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;line-height:160%'>")
					.text(res)
			)
			.append($("<TD valign='middle' style='text-align:center;width:87;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:12.0pt;line-height:180%'>")
					.text("")
			)
			.show()
			
			total = Number(total) + Number(cnt);
			prev_mount = cnt;
		});
		
		
		$("<TR>").appendTo($("#table4").find('tbody'))
		.append($("<TD valign='middle' style='text-align:center;width:123;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;line-height:160%'>")
				.text(" 총계")
		)
		.append($("<TD valign='middle' style='text-align:center;width:197;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;line-height:100%;'><SPAN STYLE='font-size:12.0pt;line-height:100%'>")
				.text(addCommaString(total.toString()))
		)
		.append($("<TD valign='middle' style='text-align:center;width:231;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;line-height:160%'>")
				.text("-")
		)
		.append($("<TD valign='middle' style='text-align:center;width:87;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:12.0pt;line-height:180%'>")
				.text("")
		)
		.show()
		
		var avg = Math.round(total / jsonData.length);
		$("#avg_cnt4").html(addCommaString(avg.toString()));
		$("#avg_total").html(addCommaString(avg.toString()));
		$("#max_month4").html(max_month.substring(4,6));
	}
}

function setchart5() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportHalf_chart5.html',
		data: { 
			"start_date" : stDate,
			"end_date" : edDate,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart5(data);
		}
	});
}

function drawchart5(data) {
	
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for(var i=0; i<jsonData.length; i++) {
		var value = {"dept_name":jsonData[i].dept_name, "cnt":jsonData[i].cnt};
		dataProvider.push(value);
	}

	AmCharts.makeChart( "chart5", {
	  "type": "serial",
	  "addClassNames": true,
	  "theme": "light",
	  "autoMargins": true,
	  "balloon": {
	    "adjustBorderColor": false,
	    "horizontalPadding": 10,
	    "verticalPadding": 8,
	    "color": "#ffffff"
	  },
	  "rotate": true,
	  "titles": [{
        "text": "[개인정보 처리 현황 소속 TOP10]",
        "size": 15
      }],
  	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "position": "left",
	    "title": "개인정보처리량"
	  } ],
	  "startDuration": 0,
	  "graphs": [ {
	    "alphaField": "alpha",
	    "balloonText": "<span style='font-size:12px;'>[[title]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	    "fillAlphas": 1,
	    "title": "개인정보 건수",
	    "type": "column",
	    "valueField": "cnt",
	    "dashLengthField": "dashLengthColumn",
	    "labelText": "[[value]]",
	    "labelPosition": "left"
	  } ],
	  "categoryField": "dept_name",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "axisAlpha": 0,
	    "tickLength": 0
	  },
	  "export": {
	    "enabled": true
	  },
      "addClassNames": true
	} );
}

function setchart6() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportHalf_chart6.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq,
			"half_type" : half_type
		},
		success: function(data) {
			drawchart6(data, half_type);
			drawtable6(data);
		}
	});
}

function drawchart6(data, half_type) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	var quarter1,quarter2,strRes;
	if(half_type == 1) {
		quarter1 = "1분기";
		quarter2 = "2분기";
	}else {
		quarter1 = "3분기";
		quarter2 = "4분기";
	}
	
	if(jsonData.type1 > jsonData.type2)
		strRes = "감소함";
	else
		strRes = "증가함";
	
	AmCharts.makeChart( "chart6", {
	  "type": "serial",
	  "addClassNames": true,
	  "theme": "light",
	  "autoMargins": true,
	  "balloon": {
	    "adjustBorderColor": false,
	    "horizontalPadding": 10,
	    "verticalPadding": 8,
	    "color": "#ffffff"
	  },
	  "titles": [{
        "text": "[개인정보 접속기록 분기별 추이]",
        "size": 15
      }],
  	  "dataProvider": [ {
  		  "quarter" : quarter1,
  		  "cnt" : jsonData.type1
  	  }, {
  		"quarter" : quarter2,
		  "cnt" : jsonData.type2
  	  }  
  	  ],
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "position": "left",
	    "title": "개인정보처리량"
	  } ],
	  "startDuration": 0,
	  "graphs": [ {
	    "alphaField": "alpha",
	    "balloonText": "<span style='font-size:12px;'>[[title]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	    "fillAlphas": 1,
	    "title": "개인정보 건수",
	    "type": "column",
	    "valueField": "cnt",
	    "dashLengthField": "dashLengthColumn",
	    "labelText": "[[value]]",
	    "labelPosition": "bottom",
	    "labelOffset": -1
	  } ],
	  "categoryField": "quarter",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "axisAlpha": 0,
	    "tickLength": 0
	  },
	  "export": {
	    "enabled": true
	  },
      "addClassNames": true
	} );
	
	var avg = Math.round(jsonData.type99 / 2);
	$("#avg_cnt6").html(addCommaString(avg.toString()));
	$("#quarter1").html(quarter1);
	$("#quarter2").html(quarter2);
	$("#res6").html(strRes);
}

function drawtable6(data) {
	$("#table6").find('tbody').empty();
	var jsonData = JSON.parse(data);
	if(jsonData != null) {		
		var string_diff = "";
		var diff_mount = Number(jsonData.type1) - Number(jsonData.type2);
		var res;
		if(diff_mount > 0) {
			string_diff = "▼ ";
			res = string_diff + addCommaString(diff_mount.toString());
		}else if(diff_mount < 0){
			string_diff = "▲ ";
			diff_mount = Math.abs(diff_mount);
			res = string_diff + addCommaString(diff_mount.toString());
		}else {
			res = "-";
		}
		
		$("<TR>").appendTo($("#table6").find('tbody'))
		.append($("<TD valign='middle' style='text-align:center;width:186;height:44;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:12.0pt;line-height:180%'>")
				.text(addCommaString(jsonData.type1.toString()))
		)
		.append($("<TD valign='middle' style='text-align:center;width:186;height:44;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle14 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:12.0pt;line-height:180%'>")
				.text(addCommaString(jsonData.type2.toString()))
		)
		.append($("<TD valign='middle' style='text-align:center;width:174;height:44;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle15 STYLE='line-height:180%;'><SPAN STYLE='font-size:12.0pt;line-height:180%'>")
				.text(res)
		)
		.show()
	}
}

function setchart7() {
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail4_chart1_new.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart7(data);
		}
	});
}

function drawchart7(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for(var i=0; i<jsonData.length; i++) {
		var value = {"category":jsonData[i].category, "value":jsonData[i].value};
		dataProvider.push(value);
	}

	AmCharts.makeChart("chart7", {
		"type": "serial",
		"theme": "light",
		"marginRight": 50,
		"categoryField": "category",
	    "angle": 30,
		"rotate": true,
		"startDuration": 0,
		"autoMargins": true,
		"categoryAxis": {
			"gridPosition": "start",
			"position": "left"
		},
		"trendLines": [],
		"graphs": [
			{
				"balloonText": "<b>[[category]]</b>: [[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-1",
				"lineAlpha": 0.2,
				"title": "",
				"type": "column",
				"valueField": "value",
				"labelText": "[[value]]",
				"labelPosition": "left"
			}
		],
		"guides": [],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider": dataProvider,
	    "export": {
	    	"enabled": true,
	    	"menu": []
	     },
	      "addClassNames": true
		
	});
}

function setchart8() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart2.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart8(data);
		}
	});
}

function drawchart8(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"day": jsonData[i].proc_date, "value": jsonData[i].cnt, "week": jsonData[i].week};
		dataProvider.push(value);
	}
	var chart8 = AmCharts.makeChart( "chart8", {
	  "type": "serial",
	  "addClassNames": true,
	  "theme": "light",
	  "autoMargins": true,
	  "balloon": {
	    "adjustBorderColor": false,
	    "horizontalPadding": 10,
	    "verticalPadding": 8,
	    "color": "#ffffff"
	  },
	  "titles": [{
	        "text": "개인정보 처리 현황",
	        "size": 20
	  }],

	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "position": "left",
	    "title": "개인정보검출(건)"
	  } ],
	  "startDuration": 0,
	  "graphs": [ {
	    "alphaField": "alpha",
	    "balloonText": "<span style='font-size:12px;'>[[title]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	    "fillAlphas": 1,
	    "title": "개인정보 건수",
	    "type": "column",
	    "valueField": "value",
	    "dashLengthField": "dashLengthColumn",
	    "labelText": "[[value]]",
	    "labelPosition": "bottom",
	    "labelOffset": -1,
	    "descriptionField": "week"

	  } ],
	  "categoryField": "day",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "axisAlpha": 0,
	    "tickLength": 0
	  },
	  "export": {
	    "enabled": true
	  }
	} );
	
	chart8.addListener("clickGraphItem", handleClick8);
}

function handleClick8(event) {
	
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.submit();
}

function setchart9_1() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart3_1.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart9_1(data);
		}
	});
}

function drawchart9_1(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for(var i=0; i<jsonData.length; i++) {
		var value = {"country":jsonData[i].proc_date, "visits":jsonData[i].cnt};
		dataProvider.push(value);
	}
	
	var chart9_1 = AmCharts.makeChart( "chart9_1", {
	  "type": "serial",
	  "theme": "light",
	  "titles": [{
	        "text": "[월별]",
	        "size": 15
	    }],
	  "dataProvider": dataProvider,
	  "valueAxes": [ {
		"axisAlpha": 0,
	    "title": "개인정보검출(건)"
	  } ],
	  "startDuration": 0,
	  "graphs": [ {
	    "balloonText": "[[category]]: <b>[[value]]</b>",
	    "fillAlphas": 1,
	    "type": "column",
	    "valueField": "visits",
	    "labelPosition": "bottom",
	    "labelOffset": -1,
	    "labelText": "[[value]]"
	  } ],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "country",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "axisAlpha": 0,
	    "tickLength": 0
	  },
	  "export": {
	    "enabled": true
	  },
	  "addClassNames": true

	} );
	
	chart9_1.addListener("clickGraphItem", handleClick9_1);
}

function handleClick9_1(event) {
	
	alert("해당 차트는 접속기록 내 처리된 개인정보 유형들의 수량으로\r\n접속기록 건수와는 차이가 있습니다.");
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.submit();
}

function setchart9_3() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart3_3.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart9_3(data);
		}
	});
}

function drawchart9_3(data) {
	
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for(var i=0; i<jsonData.length; i++) {
		var value = {"day": jsonData[i].proc_date, "everydayAvg": jsonData[i].cnt1, "time": jsonData[i].proc_time};
		dataProvider.push(value);
	}
	
	var chart9_3 = AmCharts.makeChart("chart9_3", {
	    "type": "serial",
	    "theme": "light",
	    "titles": [{
	        "text": "[시간별(평균)]",
	        "size": 15
	    }],
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "title": "개인정보검출(건)",
	        "axisAlpha": 0
	    }],
	    "startDuration": 0,
	    "graphs": [{
	        "title": "전체평균",
	        "valueField": "everydayAvg",
	        "type": "column",
	        "fillAlphas": 1,
			"labelText": "[[value]]",
		    "labelRotation": -45,
		    "descriptionField": "time"
	    }
	    ],
	    "chartCursor": {
	        "cursorAlpha": 0,
	        "zoomable": false
	    },
	    "categoryField": "day",
	    "categoryAxis": {
	    	"gridPosition": "start",
		    "axisAlpha": 0,
		    "tickLength": 0
	    },
	    "export": {
	    	"enabled": true
	     },
		  "addClassNames": true
	});
	
	chart9_3.addListener("clickGraphItem", handleClick9_3);
}


function handleClick9_3(event) {
	
	alert("해당 차트는 접속기록 내 처리된 개인정보 유형들의 수량으로\r\n접속기록 건수와는 차이가 있습니다.");
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.submit();
}

function setchart10() {
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart4.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"period_type" : period_type,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart10(data);
		}
	});
}

function drawchart10(data) {
	
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	var arrData = jsonData["arrData"];
	for(var i=0; i<arrData.length; i++) {
		var value = {"team":arrData[i].dept_name, "pre":arrData[i].type1, "cur": arrData[i].type0, "system_seq": arrData[i].system_seq};
		dataProvider.push(value);
	}

	var title1 = jsonData["title1"];
	var title2 = jsonData["title2"];

	var chart10 = AmCharts.makeChart( "chart10", {
	  "type": "serial",
	  "rotate": true,
	  "addClassNames": true,
	  "theme": "light",
	  "autoMargins": true,
	  "balloon": {
	    "adjustBorderColor": false,
	    "horizontalPadding": 10,
	    "verticalPadding": 8,
	    "color": "#ffffff"
	  },
	  "legend": {
	        "useGraphSettings": true
	  },

	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "position": "left",
	    "title": "개인정보검출(건)"
	  } ],
	  "startDuration": 0,
	  "graphs": [{
	        "balloonText": "<span style='font-size:12px;'>[[title]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	        "fillAlphas": 0.8,
	        "lineAlpha": 0.2,
		    "title": title2,
	        "type": "column",
	        "valueField": "pre",
	        "labelText": "[[value]]",
	        "labelPosition": "left",
	        "descriptionField": "system_seq"
	    }, {
	        "balloonText": "<span style='font-size:12px;'>[[title]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	        "fillAlphas": 0.8,
	        "lineAlpha": 0.2,
	        "title": title1,
	        "type": "column",
	        "valueField": "cur",
	        "labelText": "[[value]]",
	        "labelPosition": "left",
	        "descriptionField": "system_seq"
	    }],
	  "categoryField": "team",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "axisAlpha": 0,
	    "tickLength": 0,
	    "labelRotation": 45
	  },
	  "export": {
	    "enabled": true
	  }
	} );
	
	chart10.addListener("clickGraphItem", handleClick10);
}


function handleClick10(event) {
	
	alert("해당 차트는 접속기록 내 처리된 개인정보 유형들의 수량으로\r\n접속기록 건수와는 차이가 있습니다.");
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["system_seq_rp"].value = event.item.description;
	opener.eventForm.submit();
}

function setchart11() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart5.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"period_type" : period_type,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart11(data);
		}
	});
}

function drawchart11(data) {
	
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	var arrData = jsonData["arrData"];
	for(var i=0; i<arrData.length; i++) {
		var value = {"team":arrData[i].dept_name, "pre":arrData[i].type1, "cur": arrData[i].type0};
		dataProvider.push(value);
	}

	var title1 = jsonData["title1"];
	var title2 = jsonData["title2"];
	
	var chart11 = AmCharts.makeChart("chart11", {
		"type": "serial",
	     "theme": "light",
		"categoryField": "team",
		"rotate": true,
		"startDuration": 0,
		"categoryAxis": {
			"gridPosition": "start",
			"position": "left"
		},
		"legend": {
	        "useGraphSettings": true
		},
		"graphs": [
			{
				"balloonText": "[[title]]:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-2",
				"lineAlpha": 0.2,
				"title": title2,
				"type": "column",
				"valueField": "pre",
				"labelPosition": "left",
				"labelText": "[[value]]"
			},
			{
				"balloonText": "[[title]]:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-1",
				"lineAlpha": 0.2,
				"title": title1,
				"type": "column",
				"valueField": "cur",
				"labelPosition": "left",
				"labelText": "[[value]]"
			}
		],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"position": "bottom",
				"axisAlpha": 0,
				"title": "개인정보검출(건)"
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider":dataProvider,
	    "export": {
	    	"enabled": true
	     },
		  "addClassNames": true

	});
	
	chart11.addListener("clickGraphItem", handleClick11);
}


function handleClick11(event) {
	
	alert("해당 차트는 접속기록 내 처리된 개인정보 유형들의 수량으로\r\n접속기록 건수와는 차이가 있습니다.");
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["dept_name_rp"].value = event.item.category;
	opener.eventForm.submit();
}

function setchart12() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart6.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart12(data);
		}
	});
}

function drawchart12(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for(var i=0; i<jsonData.length; i++) {
		var resReq = "";
		switch(jsonData[i].req_type) {
		case "RD":
			resReq = "조회";
			break;
		case "CR":
			resReq = "등록";
			break;
		case "UD":
			resReq = "수정";
			break;
		case "DL":
			resReq = "삭제";
			break;
		case "DN":
			resReq = "다운로드";
			break;
		case "EX":
			resReq = "실행";
			break;
		case "PR":
			resReq = "출력";
			break;
		case "CO":
			resReq = "수집";
			break;
		case "NE":
			resReq = "생성";
			break;
		case "BE":
			resReq = "연계";
			break;
		case "IN":
			resReq = "연동";
			break;
		case "WR":
			resReq = "기록";
			break;
		case "SA":
		case "SV":
			resReq = "저장";
			break;
		case "SU":
			resReq = "보유";
			break;
		case "FI":
			resReq = "가공";
			break;
		case "UP":
			resReq = "편집";
			break;
		case "SC":
			resReq = "검색";
			break;
		case "CT":
			resReq = "정정";
			break;
		case "RE":
			resReq = "복구";
			break;
		case "US":
			resReq = "이용";
			break;
		case "OF":
			resReq = "제공";
			break;
		case "OP":
			resReq = "공개";
			break;
		case "AN":
			resReq = "파기";
			break;
		case "PRRD":
			resReq = "출력(RD)";
			break;
		case "PREX":
			resReq = "출력(Excel)";
			break;
		default:
			resReq = "기타";
			break;
		}
		
		var value = {"req_type": jsonData[i].req_type, "category": resReq, "value": jsonData[i].cnt};
		dataProvider.push(value);
	}
	var chart12 = AmCharts.makeChart("chart12", {
	  "type": "pie",
	  "startDuration": 0,
	   "theme": "light",
	  "addClassNames": true,
	  "legend":{
	   	"position":"right",
	    "marginRight":0,
	    "autoMargins":true,
	    "title": "개인정보검출(건)"
	  },
	  "innerRadius": "30%",
	  "defs": {
	    "filter": [{
	      "id": "shadow",
	      "width": "200%",
	      "height": "200%",
	      "feOffset": {
	        "result": "offOut",
	        "in": "SourceAlpha",
	        "dx": 0,
	        "dy": 0
	      },
	      "feGaussianBlur": {
	        "result": "blurOut",
	        "in": "offOut",
	        "stdDeviation": 5
	      },
	      "feBlend": {
	        "in": "SourceGraphic",
	        "in2": "blurOut",
	        "mode": "normal"
	      }
	    }]
	  },
	  "dataProvider": dataProvider,
	  "valueField": "value",
	  "titleField": "category",
	  "descriptionField": "req_type",
	  "export": {
	    "enabled": true
	  }
	});
	
	chart12.addListener("clickSlice", handleClick12);
}

function handleClick12(event) {
	
	alert("해당 차트는 접속기록 내 처리된 개인정보 유형들의 수량으로\r\n접속기록 건수와는 차이가 있습니다.");
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["req_type_rp"].value = event.dataItem.description;
	opener.eventForm.submit();
}

function setchart13() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart7.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart13(data);
		}
	});
}

function drawchart13(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	var sum = new Number();
	
	for (var i = 0; i < jsonData.length; i++) {
		sum=sum+Number(jsonData[i].value);
	}
	
	for(var i=0; i<jsonData.length; i++) {
		var value = {"result_type":jsonData[i].result_type, "category":jsonData[i].category, "value":jsonData[i].value, "average": sum/jsonData.length};
		dataProvider.push(value);
	}
	var chart13 = AmCharts.makeChart( "chart13", {
	  "type": "serial",
	  "addClassNames": true,
	  "theme": "light",
	  "autoMargins": true,
	  "balloon": {
	    "adjustBorderColor": false,
	    "horizontalPadding": 10,
	    "verticalPadding": 8,
	    "color": "#ffffff"
	  },
	  "legend": {
	        "useGraphSettings": true
	  },

	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "position": "left",
	    "title": "개인정보검출(건)"
	  } ],
	  "startDuration": 0,
	  "graphs": [ {
	    "alphaField": "alpha",
	    "balloonText": "<span style='font-size:12px;'>[[title]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	    "fillAlphas": 1,
	    "title": "개인정보 접속기록",
	    "type": "column",
	    "valueField": "value",
	    "dashLengthField": "dashLengthColumn",
	    "labelText": "[[value]]",
	    "labelPosition": "bottom",
	    "labelOffset": -1,
	    "descriptionField": "result_type"
	  }, {
	    "id": "graph2",
	    "balloonText": "<span style='font-size:12px;'>[[title]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	    "bullet": "round",
	    "lineThickness": 3,
	    "bulletSize": 7,
	    "bulletBorderAlpha": 1,
	    "bulletColor": "#FFFFFF",
	    "useLineColorForBulletBorder": true,
	    "bulletBorderThickness": 3,
	    "fillAlphas": 0,
	    "lineAlpha": 1,
	    "title": "평균",
	    "valueField": "average",
	    "labelPosition": "bottom",
	    "labelOffset": -1,
	    "dashLengthField": "dashLengthLine"
	  } ],
	  "categoryField": "category",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "axisAlpha": 0,
	    "tickLength": 0,
	    "labelRotation": 45
	  },
	  "export": {
	    "enabled": true
	  },
	  "addClassNames": true
	} );
	
	chart13.addListener("clickGraphItem", handleClick13);
}

function handleClick13(event) {
	
	alert("해당 차트는 접속기록 내 처리된 개인정보 유형들의 수량으로\r\n접속기록 건수와는 차이가 있습니다.");
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["privacyType_rp"].value = event.item.description;
	opener.eventForm.submit();
}

function setchart14_new(){
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart8_new.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart14_new(data);
		}
	});
}


function drawchart14_new(data) {

	var jsonData = JSON.parse(data);
	var dataProvider = jsonData["dataProvider"];
	var graphs = jsonData["graphs"];
	
	var chart14 = AmCharts.makeChart("chart14", {
	    "type": "serial",
		"theme": "light",
	    "legend": {
	        "horizontalGap": 1,
	        "maxColumns": 3,
	        "position": "top",
			"useGraphSettings": true,
			"markerSize": 10
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.5,
	        "gridAlpha": 0,
	        "title": "개인정보검출(건)"
	    }],
	    "graphs": graphs,
	    "rotate": true,
	    "startDuration": 0,
	    "categoryField": "category",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "gridAlpha": 0,
	        "position": "left"
	    },
	    "export": {
	    	"enabled": true
	     },
		  "addClassNames": true
	});
	chart14.addListener("clickGraphItem", handleClick14);
}

function handleClick14(event) {
	alert("해당 차트는 접속기록 내 처리된 개인정보 유형들의 수량으로\r\n접속기록 건수와는 차이가 있습니다.");
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["system_seq_rp"].value = event.item.description;
	opener.eventForm.submit();
}

function setchart15_new(){
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart9_new.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart15_new(data);
		}
	});
}

function drawchart15_new(data) {

	var jsonData = JSON.parse(data);
	var dataProvider = jsonData["dataProvider"];
	var graphs = jsonData["graphs"];
	
	var chart15 = AmCharts.makeChart("chart15", {
	    "type": "serial",
		"theme": "light",
	    "legend": {
	        "horizontalGap": 1,
	        "maxColumns": 3,
	        "position": "top",
			"useGraphSettings": true,
			"markerSize": 10
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.5,
	        "gridAlpha": 0,
	        "title": "개인정보검출(건)"
	    }],
	    "graphs": graphs,
	    "rotate": true,
	    "startDuration": 0,
	    "categoryField": "category",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "gridAlpha": 0,
	        "position": "left"
	    },
	    "export": {
	    	"enabled": true
	     },
		  "addClassNames": true
	});
	
	chart15.addListener("clickGraphItem", handleClick15);
}

function handleClick15(event) {
	
	alert("해당 차트는 접속기록 내 처리된 개인정보 유형들의 수량으로\r\n접속기록 건수와는 차이가 있습니다.");
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["dept_name_rp"].value = event.item.category;
	opener.eventForm.submit();
}

function setchart16_new(){
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart10_new.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart16_new(data);
		}
	});
}

function drawchart16_new(data) {
	
	var jsonData = JSON.parse(data);
	
	var dataProvider = jsonData["dataProvider"];
	var graphs = jsonData["graphs"];
	
	var chart16 = AmCharts.makeChart("chart16", {
	    "type": "serial",
		"theme": "light",
	    "legend": {
	        "horizontalGap": 1,
	        "maxColumns": 3,
	        "position": "top",
			"useGraphSettings": true,
			"markerSize": 10
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.5,
	        "gridAlpha": 0,
	        "title": "개인정보검출(건)"
	    }],
	    "graphs": graphs,
	    "rotate": true,
	    "startDuration": 0,
	    "categoryField": "category",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "gridAlpha": 0,
	        "position": "left"
	    },
	    "export": {
	    	"enabled": true
	     },
		  "addClassNames": true
	});
	
	chart16.addListener("clickGraphItem", handleClick16);
}

function handleClick16(event) {
	
	alert("해당 차트는 접속기록 내 처리된 개인정보 유형들의 수량으로\r\n접속기록 건수와는 차이가 있습니다.");
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["emp_user_id_rp"].value = event.item.description;
	opener.eventForm.submit();
}

function setchart17() {
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail4_chart2_new.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart17(data);
		}
	});
}

function drawchart17(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for(var i=0; i<jsonData.length; i++) {
		var value = {"category":jsonData[i].category, "type1":jsonData[i].type1, "type2":jsonData[i].type2, "type3":jsonData[i].type3, "type4":jsonData[i].type4};
		dataProvider.push(value);
	}

	var chart17 = AmCharts.makeChart("chart17", {
		"type": "serial",
		"theme": "light",
	    "legend": {
	        "horizontalGap": 10,
	        "maxColumns": 1,
	        "position": "right",
			"useGraphSettings": true,
			"markerSize": 10
	    },
		"categoryField": "category",
	    "angle": 30,
		"rotate": true,
		"startDuration": 0,
		"autoMargins": true,
		"categoryAxis": {
			"gridPosition": "start",
			"position": "left"
		},
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.3,
	        "gridAlpha": 0
	    }],
		"trendLines": [],
	    "graphs": [{
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "주민등록번호",
	        "type": "column",
	        "valueField": "type1",
	        "labelText": "[[value]]"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "운전면허번호",
	        "type": "column",
	        "valueField": "type2"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "여권번호",
	        "type": "column",
	        "valueField": "type3"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "외국인등록번호",
	        "type": "column",
	        "valueField": "type10"
	    }],
		"guides": [],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider": dataProvider,
	    "export": {
	    	"enabled": true
	     },
	      "addClassNames": true
		
	});
}
	

function setchart18() {
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail4_chart3_new.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart18(data);
		}
	});
}

function drawchart18(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for(var i=0; i<jsonData.length; i++) {
		var value = {"category":jsonData[i].category, "type1":jsonData[i].type1, "type2":jsonData[i].type2, "type3":jsonData[i].type3, "type4":jsonData[i].type4};
		dataProvider.push(value);
	}

	var chart18 = AmCharts.makeChart("chart18", {
		"type": "serial",
		"theme": "light",
	    "legend": {
	        "horizontalGap": 10,
	        "maxColumns": 1,
	        "position": "right",
			"useGraphSettings": true,
			"markerSize": 10
	    },
		"categoryField": "category",
	    "angle": 30,
		"rotate": true,
		"startDuration": 0,
		"autoMargins": true,
		"categoryAxis": {
			"gridPosition": "start",
			"position": "left"
		},
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.3,
	        "gridAlpha": 0
	    }],
		"trendLines": [],
	    "graphs": [{
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "주민등록번호",
	        "type": "column",
	        "valueField": "type1",
	        "labelText": "[[value]]"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "운전면허번호",
	        "type": "column",
	        "valueField": "type2"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "여권번호",
	        "type": "column",
	        "valueField": "type3"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "외국인등록번호",
	        "type": "column",
	        "valueField": "type10"
	    }],
		"guides": [],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider": dataProvider,
	    "export": {
	    	"enabled": true
	     },
	      "addClassNames": true
		
	});
}


function setchart19() {
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail4_chart4_new.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart19(data);
		}
	});
}

function drawchart19(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for(var i=0; i<jsonData.length; i++) {
		var value = {"category":jsonData[i].category, "type1":jsonData[i].type1, "type2":jsonData[i].type2, "type3":jsonData[i].type3, "type4":jsonData[i].type4};
		dataProvider.push(value);
	}

	var chart19 = AmCharts.makeChart("chart19", {
		"type": "serial",
		"theme": "light",
	    "legend": {
	        "horizontalGap": 10,
	        "maxColumns": 1,
	        "position": "right",
			"useGraphSettings": true,
			"markerSize": 10
	    },
		"categoryField": "category",
	    "angle": 30,
		"rotate": true,
		"startDuration": 0,
		"autoMargins": true,
		"categoryAxis": {
			"gridPosition": "start",
			"position": "left"
		},
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.3,
	        "gridAlpha": 0
	    }],
		"trendLines": [],
	    "graphs": [{
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "주민등록번호",
	        "type": "column",
	        "valueField": "type1",
	        "labelText": "[[value]]"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "운전면허번호",
	        "type": "column",
	        "valueField": "type2"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "여권번호",
	        "type": "column",
	        "valueField": "type3"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "외국인등록번호",
	        "type": "column",
	        "valueField": "type10"
	    }],
		"guides": [],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider": dataProvider,
	    "export": {
	    	"enabled": true
	     },
	      "addClassNames": true
		
	});
}


function uploadImage1() {
	var form = $("#uploadForm1")[0];
	
	if(check(form)) {
	    var formData = new FormData(form);
	    
		$.ajax({
			type : 'POST',
			url : rootPath + '/report/uploadImage1.html',
			data : formData,
			processData : false,
			contentType : false,
			success : function(data) {
				var src = scheme + "://" + serverName + ":" + port + rootPath + "/resources/reportUpload/" + data.fileName;
				$("<img align='center' src='" + src + "' width='651px' height='301px' style='display: block;position: absolute;text-align: center;'>").prependTo($("#uploadtable1"));
			}
		});
	}
}

function uploadImage2() {
	var form = $("#uploadForm2")[0];
	if(check(form)) {
	    var formData = new FormData(form);
	    
		$.ajax({
			type : 'POST',
			url : rootPath + '/report/uploadImage2.html',
			data : formData,
			processData : false,
			contentType : false,
			success : function(data) {
				var src = scheme + "://" + serverName + ":" + port + rootPath + "/resources/reportUpload/" + data.fileName;
				$("<img align='center' src='" + src + "' width='651px' height='301px' style='display: block;position: absolute;text-align: center;'>").prependTo($("#uploadtable2"));
			}
		});
	}
}

function check(form) {
	var file = form.filename.value;
	if(file != "") {
		var fileExt = file.substring(file.lastIndexOf(".") + 1);
		var reg = /gif|jpg|jpeg|png/i;	// 업로드 가능 확장자.
		if(reg.test(fileExt) == false) {
			alert("첨부파일은 gif, jpg, png로 된 이미지만 가능합니다.");
			return false;
		} else {
			var fileNameCheck = file.substring(file.lastIndexOf("\\") + 1, file.length).toLowerCase();
			for(i=0; i<fileNameCheck.length; i++) {
				var chk = fileNameCheck.charCodeAt(i);
				if(chk > 128) {
					alert("파일명이 한글로 된 이미지는 등록할 수 없습니다.");
					return false;
				}
			}
			return true;
		}
	} else {
		alert("선택된 이미지가 없습니다.");
		return false;
	}
}

var charts = {}; 
function saveReport() {
	
	$("#chart1").css("display", "none");
	$("#chart3").css("display", "none");
	$("#chart4").css("display", "none");
	$("#chart5").css("display", "none");
	$("#chart6").css("display", "none");
	$("#chart7").css("display", "none");
	$("#chart8").css("display", "none");
	$("#chart9_1").css("display", "none");
	$("#chart9_3").css("display", "none");
	$("#chart10").css("display", "none");
	$("#chart11").css("display", "none");
	$("#chart12").css("display", "none");
	$("#chart13").css("display", "none");
	$("#chart14").css("display", "none");
	$("#chart15").css("display", "none");
	$("#chart16").css("display", "none");
	$("#chart17").css("display", "none");
	$("#chart18").css("display", "none");
	$("#chart19").css("display", "none");
	
	var ids = [];
	if(period_type == 1)
		ids = [ "chart1", "chart2", "chart3", "chart4", "chart5", "chart6", "chart7", "chart8", "chart9_1", "chart9_3", "chart10", "chart11", "chart12", "chart13", "chart14", "chart15", "chart16", "chart17", "chart18", "chart19" ];

	// Collect actual chart objects out of the AmCharts.charts array
	
	var charts_remaining = ids.length;
	for (var i = 0; i < ids.length; i++) {
		for (var x = 0; x < AmCharts.charts.length; x++) {
			if (AmCharts.charts[x].div.id == ids[i]) 
				charts[ids[i]] = AmCharts.charts[x];
		}
	}
	
	// Trigger export of each chart
	for ( var x in charts) {
		if (charts.hasOwnProperty(x)) {
			var chart = charts[x];
			chart["export"].capture({}, function() {
				this.toPNG({}, function(data) {

					this.setup.chart.exportedImage = data;
					
					charts_remaining--;

					if (charts_remaining == 0) {
						for(var ch in charts) {
							var temp_chart = charts[ch];
							imageList[ch] = temp_chart.exportedImage;
						}
						saveAsDoc();
					}

				});
			});
		}
	}
}

function show() {
	for(var x in charts) {
		var chart = charts[x];
		alert(chart.div.id);
	}
}

function addCommaString(str){
	if(isNaN(str)){
		return "NaN"; 
	}else{
		var lineCount = 0;
		var arr = [];
		var str2 = str;
		for(var i = 0; i < str.length / 3; i++){
			arr[lineCount++] = str2.substring((str2.length - 3), str2.length);
			str2 = str2.substring(0, (str2.length - 3));
		}
		arr.reverse();
		return arr.join(",");
	}
};