
$(document).ready(function() {
	setCharts();
});

function setCharts() {
	setchart1();
	setchart2();
	setchart3();
}

function setchart1() {
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail2_chart1.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date
		},
		success: function(data) {
			drawchart1(data);
		}
	});
}

function drawchart1(data) {
	
	var dataProvider = new Array();
	var JsonData = JSON.parse(data);
	
	var arrCur = JsonData["cur"];
	var arrPre = JsonData["pre"];
	for(var i=0; i<arrCur.length; i++) {
		var value = {"rule": arrCur[i].rule, "cur": arrCur[i].cnt, "pre": arrPre[i].cnt};
		dataProvider.push(value);
	}
	
	var chart = AmCharts.makeChart("chart1", {
		"type": "serial",
	     "theme": "light",
		"categoryField": "rule",
		"depth3D": 10,
	    "angle": 30,
		"rotate": true,
		"startDuration": 1,
		"autoMargins": true,
		"categoryAxis": {
			"gridPosition": "start",
			"position": "left"
		},
		"legend": {
	        "useGraphSettings": true,
	        "position": "bottom",
	        "autoMargins": true,
		},
		"trendLines": [],
		"graphs": [
			{
				"balloonText": "저번달:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-2",
				"lineAlpha": 0.2,
				"title": "저번달",
				"type": "column",
				"valueField": "pre"
			},
			{
				"balloonText": "이번달:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-1",
				"lineAlpha": 0.2,
				"title": "이번달",
				"type": "column",
				"valueField": "cur"
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"position": "bottom",
				"axisAlpha": 0
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider": dataProvider,
	    "export": {
	    	"enabled": true,
	    	"menu": []
	     }
		
	});
}
	

function setchart2() {
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail2_chart2.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date
		},
		success: function(data) {
			drawchart2(data);
		}
	});
}

function drawchart2(data) {
	
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"rule": jsonData[i].rule,"cnt": jsonData[i].cnt};
		dataProvider.push(value);
	}
	
	var chart = AmCharts.makeChart("chart2", {
	    "theme": "light",
	    "type": "serial",
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        //"title": "Income in millions, USD"
	    }],
	    "graphs": [{
	        "balloonText": "[[category]]:[[value]]",
	    	"fillAlphas": 0.85,
	        "lineAlpha": 0.1,
	        "type": "column",
	        "topRadius":1,
	        "title": "세부지표",
	        "valueField": "cnt",
	        "columnWidth": 0.3
	    }],
	    "depth3D": 10,
	    "angle": 30,
	    "rotate": true,
	    "categoryField": "rule",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "fillAlpha": 0.05,
	        "position": "left"
	    },
	    "export": {
	    	"enabled": true,
	    	"menu": []
	     }
	});
}
	

function setchart3() {
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail2_chart3.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date
		},
		success: function(data) {
			drawchart3(data);
		}
	});
}

function drawchart3(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	console.log(jsonData)
	var rule_list = jsonData["rule_list"];
	var emp_list = jsonData["emp_list"];
	
	for (var i = 0; i < emp_list.length; i++) {
		var value = {"team": emp_list[i].dept_name,"content1": emp_list[i].cnt$1,"content2": emp_list[i].cnt$3,
				"content3": emp_list[i].cnt$5,"content4": emp_list[i].cnt$7,"content5": emp_list[i].cnt$9,"content6": emp_list[i].cnt$11};
		dataProvider.push(value);
	}
	
	var chart = AmCharts.makeChart("chart3", {
	    "type": "serial",
		"theme": "light",
	    "legend": {
	        "horizontalGap": 10,
	        "maxColumns": 1,
	        "position": "right",
			"useGraphSettings": true,
			"markerSize": 10
	    },
	    "depth3D": 20,
	    "angle": 40,
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.3,
	        "gridAlpha": 0.3
	    }],
	    "graphs": [{
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": rule_list[0].rule_nm,
	        "type": "column",
			"color": "#000000",
	        "valueField": "content1",
	        "columnWidth" : 0.4
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": rule_list[1].rule_nm,
	        "type": "column",
			"color": "#000000",
	        "valueField": "content2",
	        "columnWidth" : 0.4
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": rule_list[2].rule_nm,
	        "type": "column",
			"color": "#000000",
	        "valueField": "content3",
	        "columnWidth" : 0.4
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": rule_list[3].rule_nm,
	        "type": "column",
			"color": "#000000",
	        "valueField": "content4",
	        "columnWidth" : 0.4
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": rule_list[4].rule_nm,
	        "type": "column",
			"color": "#000000",
	        "valueField": "content5",
	        "columnWidth" : 0.4
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": rule_list[5].rule_nm,
	        "type": "column",
			"color": "#000000",
	        "valueField": "content6",
	        "columnWidth" : 0.4
	    }],
	    "categoryField": "team",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "gridAlpha": 0,
	        "position": "left"
	    },
	    "export": {
	    	"enabled": true,
	    	"menu": []
	     }

	});
}

/**
 * Define export function
 */
function exportReport() {

  // So that we know export was started
  console.log("Starting export...");

  // Define IDs of the charts we want to include in the report
  var ids = ["chart1", "chart2", "chart3"];

  // Collect actual chart objects out of the AmCharts.charts array
  var charts = {},
    charts_remaining = ids.length;
  for (var i = 0; i < ids.length; i++) {
    for (var x = 0; x < AmCharts.charts.length; x++) {
      if (AmCharts.charts[x].div.id == ids[i])
        charts[ids[i]] = AmCharts.charts[x];
    }
  }

  // Trigger export of each chart
  for (var x in charts) {
    if (charts.hasOwnProperty(x)) {
      var chart = charts[x];
      chart["export"].capture({}, function() {
        this.toJPG({}, function(data) {

          // Save chart data into chart object itself
          this.setup.chart.exportedImage = data;

          // Reduce the remaining counter
          charts_remaining--;

          // Check if we got all of the charts
          if (charts_remaining == 0) {
            // Yup, we got all of them
            // Let's proceed to putting PDF together
            generatePDF();
          }

        });
      });
    }
  }

  function generatePDF() {

    // Log
    console.log("Generating PDF...");

    // Initiliaze a PDF layout
    var layout = {
      "content": [],
      "styles": {
        "green": {
          "fillColor": "green",
          "color": "white"
        },
        "blue": {
          "fillColor": "blue",
          "color": "white"
        },
        "orange": {
          "fillColor": "orange",
          "color": "white"
        },
        "purple": {
          "fillColor": "purple",
          "color": "white"
        },
        "pink": {
          "fillColor": "pink",
          "color": "white"
        }
      }
    }

    // Let's add a custom title
    layout.content.push({
      "text": document.getElementById("title").innerHTML,
      "fontSize": 15
    });

    
    layout.content.push({
        "text": "❍ 점검 대상 개인정보 처리시스템",
        "backgroundColor" : "#000"
      });

    

    // Let's add a table
    layout.content.push({
      "table": {
        // headers are automatically repeated if the table spans over multiple pages
        // you can declare how many rows should be treated as headers
        "headerRows": 1,
        "widths": ["10%", "10%", "45%"],
        "body": [
          ["번호", "개인정보처리시스템 명칭", "URL"],
          ["1", "보조사업 관리", "http://192.168.0.1"],
          ["2", "상수도 관리", "http://192.168.0.2"],
          ["3", "이호조", "http://192.168.0.3"],
          ["4", "고객메일 시스템", "http://192.168.0.4"],
          ["5", "고객구매 시스템", "http://192.168.0.5"]
        ]
      }
    });
    
 // Add bigger chart
    layout.content.push({
      "image": charts["chart1"].exportedImage,
      "fit": [800, 600]
    });

    /*// Put two next charts side by side in columns
    layout.content.push({
      "columns": [{
        "width": "50%",
        "image": charts["chartdiv2"].exportedImage,
        "fit": [250, 300]
      }, {
        "width": "*",
        "image": charts["chartdiv3"].exportedImage,
        "fit": [250, 300]
      }],
      "columnGap": 10
    });

    // Add chart and text next to each other
    layout.content.push({
      "columns": [{
        "width": "25%",
        "image": charts["chartdiv4"].exportedImage,
        "fit": [125, 300]
      }, {
        "width": "*",
        "stack": [
          document.getElementById("note1").innerHTML,
          "\n\n",
          document.getElementById("note2").innerHTML
        ]
      }],
      "columnGap": 10
    });*/

    // Trigger the generation and download of the PDF
    // We will use the first chart as a base to execute Export on
    chart["export"].toPDF(layout, function(data) {
      this.download(data, "application/pdf", "amCharts.pdf");
    });

  }
}