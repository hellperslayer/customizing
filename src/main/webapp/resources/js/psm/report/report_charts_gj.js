var imageList = new Array();
var imageList = {};

$(document).ready(function() {
	setChart.init();
});

function setCharts() {
	setchart1_1();
	setchart4();
	setchart6();
	setchart7();
	//setchart8();
	setchart8_new();
}

var setChart = {
		init : function(){
			var chartlist = $('.downloadChartClass');
			for(var i=0 ; i < chartlist.length ; i++){
				eval('setChart.'+chartlist[i].id+'()');
			}
		},
		
		chart1_1 : function(){setchart1_1();},
		chart4 : function(){setchart4();},
		chart6 : function(){setchart6();},
		chart7 : function(){setchart7();},
		chart8 : function(){setchart8_new();}
}

function saveAsDoc() {
	
	$.ajax({
		type : 'POST',
		url : rootPath + '/report/getImages.html?type=1',
		data : imageList,
		/*data : {
			"imageList" : imageList,
			"report_type" : "report1"
		}*/
		success : function(data) {
			/*for(var i=0; i<imageList.length; i++) {
				var image = "#chartImage" + i;
				$(image).css("display", "block");
			}*/
			var i=0;
			for(var img in imageList) { 
				var image = "#chartImage" + i;
				$(image).css("display", "block");
				i++;
			}
			saveAsPage();
			self.close();
		}
	});
}

function saveAsPage() {
	if (document.execCommand) {
		var date = new Date();
		var year = date.getFullYear();
		var month = new String(date.getMonth() + 1);
		var day = new String(date.getDate());
		if(month.length == 1){ 
			month = "0" + month; 
		} 
		if(day.length == 1){ 
			day = "0" + day; 
		} 
		var regdate = year + "" + month + "" + day;
		document.execCommand("SaveAs", false, "report_" + regdate + ".doc");
	} else {
		alert("error..");
	}
}

function setchart1_1() {
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart1_1.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart1_1(data);
		}
	});
}

function drawchart1_1(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		if(i==0) {
			$("#reportBySys").html(jsonData[i].system_name);
		}
		var value = {"system_seq": jsonData[i].system_seq, "system_name": jsonData[i].system_name, "cnt": jsonData[i].cnt};
		dataProvider.push(value);
	}
	
	var chart1_1 = AmCharts.makeChart( "chart1_1", {
	  "type": "serial",
	  "theme": "light",
	  "titles": [{
	        "text": "[개인정보 처리시스템별 결과]",
	        "size": 15
	    }],
	  "dataProvider": dataProvider,
	  "rotate": true,
	  "valueAxes": [ {
		"axisAlpha": 0,
	    "title": "개인정보검출(건)"
	  } ],
	  "startDuration": 0,
	  "graphs": [ {
	    "balloonText": "[[category]]: <b>[[value]]</b>",
	    "fillAlphas": 1,
	    //"lineAlpha": 0.2,
	    "type": "column",
	    "valueField": "cnt",
	    "labelText": "[[value]]",
	    "descriptionField": "system_seq"
	  } ],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "system_name",
	  "categoryAxis": {
	    "gridPosition": "start",
		"axisAlpha": 0,
		"tickLength": 0,
	    "labelRotation": 45,
	    "minVerticalGap": 10,
	    "fontSize":10
	  },
	  "export": {
	    "enabled": true
	  }

	} );
	
	chart1_1.addListener("clickGraphItem", handleClick1_1);
}

function handleClick1_1(event) {
	
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["system_seq_rp"].value = event.item.description;
	opener.eventForm.submit();
}

function setchart4() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart4.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"period_type" : period_type,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart4(data);
		}
	});
}

function drawchart4(data) {
	
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	var arrData = jsonData["arrData"];
	for(var i=0; i<arrData.length; i++) {
		var value = {"team":arrData[i].dept_name, "pre":arrData[i].type1, "cur": arrData[i].type0, "system_seq": arrData[i].system_seq};
		dataProvider.push(value);
	}

	var title1 = jsonData["title1"];
	var title2 = jsonData["title2"];

	var chart4 = AmCharts.makeChart( "chart4", {
	  "type": "serial",
	  "addClassNames": true,
	  "theme": "light",
	  "autoMargins": true,
	  "balloon": {
	    "adjustBorderColor": false,
	    "horizontalPadding": 10,
	    "verticalPadding": 8,
	    "color": "#ffffff"
	  },
	  "legend": {
	        "useGraphSettings": true
	  },

	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "position": "left",
	    "title": "개인정보검출(건)"
	  } ],
	  "startDuration": 0,
	  "graphs": [ {
	    "alphaField": "alpha",
	    "balloonText": "<span style='font-size:12px;'>[[title]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	    "fillAlphas": 1,
	    "title": title2,
	    "type": "column",
	    "valueField": "pre",
	    "dashLengthField": "dashLengthColumn",
	    "labelText": "[[value]]",
	    "labelRotation": -45,
	    "descriptionField": "system_seq"
	  }, {
	    "id": "graph2",
	    "balloonText": "<span style='font-size:12px;'>[[title]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	    "bullet": "round",
	    "lineThickness": 3,
	    "bulletSize": 7,
	    "bulletBorderAlpha": 1,
	    "bulletColor": "#FFFFFF",
	    "useLineColorForBulletBorder": true,
	    "bulletBorderThickness": 3,
	    "fillAlphas": 0,
	    "lineAlpha": 1,
	    "title": title1,
	    "valueField": "cur",
	    "dashLengthField": "dashLengthLine"
	    /*"labelText": "[[value]]",
	    "labelRotation": -45*/
	  } ],
	  "categoryField": "team",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "axisAlpha": 0,
	    "tickLength": 0,
	    "labelRotation": 90,
	    "minVerticalGap": 5,
	    
	  },
	  "export": {
	    "enabled": true
	  }
	} );
	
	chart4.addListener("clickGraphItem", handleClick4);
}


function handleClick4(event) {
	
	alert("해당 차트는 접속기록 내 처리된 개인정보 유형들의 수량으로\r\n접속기록 건수와는 차이가 있습니다.");
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["system_seq_rp"].value = event.item.description;
	//opener.eventForm.elements["end_h"].value = event.item.description + 1;
	opener.eventForm.submit();
}

function setchart6() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart6.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart6(data);
		}
	});
}

function drawchart6(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for(var i=0; i<jsonData.length; i++) {
		var resReq = "";
		switch(jsonData[i].req_type) {
		case "RD":
			resReq = "조회";
			break;
		case "CR":
			resReq = "등록";
			break;
		case "UD":
			resReq = "수정";
			break;
		case "DL":
			resReq = "삭제";
			break;
		case "DN":
			resReq = "다운로드";
			break;
		case "EX":
			resReq = "실행";
			break;
		case "PR":
			resReq = "출력";
			break;
		case "CO":
			resReq = "수집";
			break;
		case "NE":
			resReq = "생성";
			break;
		case "BE":
			resReq = "연계";
			break;
		case "IN":
			resReq = "연동";
			break;
		case "WR":
			resReq = "기록";
			break;
		case "SA":
		case "SV":
			resReq = "저장";
			break;
		case "SU":
			resReq = "보유";
			break;
		case "FI":
			resReq = "가공";
			break;
		case "UP":
			resReq = "편집";
			break;
		case "SC":
			resReq = "검색";
			break;
		case "CT":
			resReq = "정정";
			break;
		case "RE":
			resReq = "복구";
			break;
		case "US":
			resReq = "이용";
			break;
		case "OF":
			resReq = "제공";
			break;
		case "OP":
			resReq = "공개";
			break;
		case "AN":
			resReq = "파기";
			break;
		case "PRRD":
			resReq = "출력(RD)";
			break;
		case "PREX":
			resReq = "출력(Excel)";
			break;
		default:
			resReq = "기타";
			break;
		}
		
		var value = {"req_type": jsonData[i].req_type, "category": resReq, "value": jsonData[i].cnt};
		dataProvider.push(value);
	}
	
	var chart6 = AmCharts.makeChart( "chart6", {
		  "type": "serial",
		  "theme": "light",
		  "dataProvider": dataProvider,
		  "rotate": true,
		  "valueAxes": [ {
			"axisAlpha": 0,
		    "title": "개인정보검출(건)"
		  } ],
		  "startDuration": 0,
		  "graphs": [ {
		    "balloonText": "[[category]]: <b>[[value]]</b>",
		    "fillAlphas": 1,
		    //"lineAlpha": 0.2,
		    "type": "column",
		    "valueField": "value",
		    "labelText": "[[value]]",
		    "descriptionField": "category"
		  } ],
		  "chartCursor": {
		    "categoryBalloonEnabled": false,
		    "cursorAlpha": 0,
		    "zoomable": false
		  },
		  "categoryField": "category",
		  "categoryAxis": {
		    "gridPosition": "start",
			"axisAlpha": 0,
			"tickLength": 0,
		    "labelRotation": 45,
		    "minVerticalGap": 10,
		    "fontSize":10
		  },
		  "export": {
		    "enabled": true
		  }

		} );
	
	/*var chart6 = AmCharts.makeChart("chart6", {
	  "type": "pie",
	  "startDuration": 0,
	   "theme": "light",
	  "addClassNames": true,
	  "legend":{
	   	"position":"right",
	    "marginRight":0,
	    "autoMargins":true,
	    "title": "개인정보검출(건)"
	  },
	  "innerRadius": "30%",
	  "defs": {
	    "filter": [{
	      "id": "shadow",
	      "width": "200%",
	      "height": "200%",
	      "feOffset": {
	        "result": "offOut",
	        "in": "SourceAlpha",
	        "dx": 0,
	        "dy": 0
	      },
	      "feGaussianBlur": {
	        "result": "blurOut",
	        "in": "offOut",
	        "stdDeviation": 5
	      },
	      "feBlend": {
	        "in": "SourceGraphic",
	        "in2": "blurOut",
	        "mode": "normal"
	      }
	    }]
	  },
	  "dataProvider": dataProvider,
	  "valueField": "value",
	  "titleField": "category",
	  "descriptionField": "req_type",
	  "export": {
	    "enabled": true
	  }
	});*/
	
	chart6.addListener("clickSlice", handleClick6);
}

function handleClick6(event) {
	
	alert("해당 차트는 접속기록 내 처리된 개인정보 유형들의 수량으로\r\n접속기록 건수와는 차이가 있습니다.");
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["req_type_rp"].value = event.dataItem.description;
	opener.eventForm.submit();
}

function setchart7() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart7.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart7(data);
		}
	});
}

function drawchart7(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	
	for(var i=0; i<jsonData.length; i++) {
		var value = {"result_type":jsonData[i].result_type, "category":jsonData[i].category, "value":jsonData[i].value, "average": jsonData[i].value/jsonData[i].systemCnt};
		dataProvider.push(value);
	}
	var chart7 = AmCharts.makeChart( "chart7", {
	  "type": "serial",
	  "addClassNames": true,
	  "theme": "light",
	  "autoMargins": true,
	  "rotate": true,
	  "balloon": {
	    "adjustBorderColor": false,
	    "horizontalPadding": 10,
	    "verticalPadding": 8,
	    "color": "#ffffff"
	  },
	  "legend": {
	        "useGraphSettings": true
	  },

	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "position": "left",
	    "title": "개인정보검출(건)"
	  } ],
	  "startDuration": 0,
	  "graphs": [ {
	    "alphaField": "alpha",
	    "balloonText": "<span style='font-size:12px;'>[[title]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	    "fillAlphas": 1,
	    "title": "개인정보 접속기록",
	    "type": "column",
	    "valueField": "value",
	    "dashLengthField": "dashLengthColumn",
	    "labelText": "[[value]]",
	    "descriptionField": "result_type"
	  }, {
	    "id": "graph2",
	    "balloonText": "<span style='font-size:12px;'>[[title]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	    "bullet": "round",
	    "lineThickness": 3,
	    "bulletSize": 7,
	    "bulletBorderAlpha": 1,
	    "bulletColor": "#FFFFFF",
	    "useLineColorForBulletBorder": true,
	    "bulletBorderThickness": 3,
	    "fillAlphas": 0,
	    "lineAlpha": 1,
	    "title": "평균",
	    "valueField": "average",
	    "dashLengthField": "dashLengthLine"
	  } ],
	  "categoryField": "category",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "axisAlpha": 0,
	    "tickLength": 0,
	    "labelRotation": 45
	  },
	  "export": {
	    "enabled": true
	  }
	} );
	
	chart7.addListener("clickGraphItem", handleClick7);
}

function handleClick7(event) {
	
	alert("해당 차트는 접속기록 내 처리된 개인정보 유형들의 수량으로\r\n접속기록 건수와는 차이가 있습니다.");
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["privacyType_rp"].value = event.item.description;
	opener.eventForm.submit();
}

function setchart8(){
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart8.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			if(use_studentId == 'yes')
				drawchart8_useStudentId(data);
			else
				drawchart8(data);
		}
	});
}

function setchart8_new(){
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart8_new.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart8_new(data);
		}
	});
}

function drawchart8(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"category": jsonData[i].category, 
				"type1": jsonData[i].type1,
				"type2": jsonData[i].type2,
				"type3": jsonData[i].type3,
				"type4": jsonData[i].type4,
				"type5": jsonData[i].type5,
				"type6": jsonData[i].type6,
				"type7": jsonData[i].type7,
				"type8": jsonData[i].type8,
				"type9": jsonData[i].type9,
				"type10": jsonData[i].type10,
				"type11": jsonData[i].type11,
				"type12": jsonData[i].type12};
		
		dataProvider.push(value);
	}
	
	var chart8 = AmCharts.makeChart("chart8", {
	    "type": "serial",
		"theme": "light",
	    "legend": {
	        "horizontalGap": 1,
	        "maxColumns": 3,
	        "position": "top",
			"useGraphSettings": true,
			"markerSize": 10
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.5,
	        "gridAlpha": 0,
	        "title": "개인정보검출(건)"
	    }],
	    "graphs": [{
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "주민등록번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type1"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "운전면허번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type2"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "여권번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type3"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "신용카드번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type4"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "건강보험번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type5"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "전화번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type6"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "이메일",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type7"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "휴대전화번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type8"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "계좌번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type9"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "외국인등록번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type10"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "환자번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type11"
	    }],
	    "rotate": true,
	    "startDuration": 0,
	    "categoryField": "category",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "gridAlpha": 0,
	        "position": "left"
	    },
	    "export": {
	    	"enabled": true
	     }
	});
}

function drawchart8_useStudentId(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"category": jsonData[i].category, 
				"type1": jsonData[i].type1,
				"type2": jsonData[i].type2,
				"type3": jsonData[i].type3,
				"type4": jsonData[i].type4,
				"type5": jsonData[i].type5,
				"type6": jsonData[i].type6,
				"type7": jsonData[i].type7,
				"type8": jsonData[i].type8,
				"type9": jsonData[i].type9,
				"type10": jsonData[i].type10,
				"type99": jsonData[i].type99};
		
		dataProvider.push(value);
	}
	
	var chart8 = AmCharts.makeChart("chart8", {
	    "type": "serial",
		"theme": "light",
	    "legend": {
	        "horizontalGap": 1,
	        "maxColumns": 3,
	        "position": "top",
			"useGraphSettings": true,
			"markerSize": 10
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.5,
	        "gridAlpha": 0,
	        "title": "개인정보검출(건)"
	    }],
	    "graphs": [{
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "주민등록번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type1"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "운전면허번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type2"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "여권번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type3"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "신용카드번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type4"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "건강보험번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type5"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "전화번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type6"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "이메일",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type7"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "휴대전화번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type8"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "계좌번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type9"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "외국인등록번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type10"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "학번",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type99"
	    }],
	    "rotate": true,
	    "startDuration": 0,
	    "categoryField": "category",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "gridAlpha": 0,
	        "position": "left"
	    },
	    "export": {
	    	"enabled": true
	     }
	});
}

function drawchart8_new(data) {

	var jsonData = JSON.parse(data);
	var dataProvider = jsonData["dataProvider"];
	var graphs = jsonData["graphs"];
	
	var chart8 = AmCharts.makeChart("chart8", {
	    "type": "serial",
		"theme": "light",
	    "legend": {
	        "horizontalGap": 1,
	        "maxColumns": 3,
	        "position": "top",
			"useGraphSettings": true,
			"markerSize": 10
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.5,
	        "gridAlpha": 0,
	        "title": "개인정보검출(건)"
	    }],
	    "graphs": graphs,
	    "startDuration": 0,
	    "categoryField": "category",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "gridAlpha": 0,
	        "position": "left"
	    },
	    "export": {
	    	"enabled": true
	     }
	});
	
	chart8.addListener("clickGraphItem", handleClick8);
}

function handleClick8(event) {
	
	alert("해당 차트는 접속기록 내 처리된 개인정보 유형들의 수량으로\r\n접속기록 건수와는 차이가 있습니다.");
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["system_seq_rp"].value = event.item.description;
	opener.eventForm.submit();
}


var charts = {}; 
function saveReport() {
	
	$("#chart1_1").css("display", "none");
	$("#chart4").css("display", "none");
	$("#chart6").css("display", "none");
	$("#chart7").css("display", "none");
	$("#chart8").css("display", "none");
	
	var ids = [];
	if(period_type == 1)
		ids = [ "chart1_1", "chart4", "chart6", "chart7", "chart8" ];
	else
		ids = [ "chart1_1", "chart4", "chart6", "chart7", "chart8" ];

	// Collect actual chart objects out of the AmCharts.charts array
	
	var charts_remaining = ids.length;
	for (var i = 0; i < ids.length; i++) {
		for (var x = 0; x < AmCharts.charts.length; x++) {
			if (AmCharts.charts[x].div.id == ids[i]) 
				charts[ids[i]] = AmCharts.charts[x];
		}
	}
	
	// Trigger export of each chart
	for ( var x in charts) {
		if (charts.hasOwnProperty(x)) {
			var chart = charts[x];
			chart["export"].capture({}, function() {
				this.toPNG({}, function(data) {

					this.setup.chart.exportedImage = data;
					
					charts_remaining--;

					if (charts_remaining == 0) {
						for(var ch in charts) {
							var temp_chart = charts[ch];
							imageList[ch] = temp_chart.exportedImage;
						}
						saveAsDoc();
					}

				});
			});
		}
	}
}

function show() {
	for(var x in charts) {
		var chart = charts[x];
		alert(chart.div.id);
	}
}

function goAllLogInqList(req_type) {
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["req_type_rp"].value = req_type;
	opener.eventForm.submit();
}