var imageList = new Array();
var imageList = {};


$(document).ready(function() {
	if(download_type == 'pdf') {
		$("#buttonDiv").css("display","none");
		$(".downloadChartClass").css("height","500px");
		$(".downloadChartClass").css("width","950px");
		$(".downloadClass").css("width","950px");
		for (var x = 0; x < AmCharts.charts.length; x++) {
			$("#"+AmCharts.charts[x].div.id).css("width","950px");
		}
		setInterval(function() {
			setChart.init();
		},500);
	} else {
		$(".downloadClass").css("width","100%");
		$(".downloadChartClass").css("width","100%");
		for (var x = 0; x < AmCharts.charts.length; x++) {
			$("#"+AmCharts.charts[x].div.id).css("width","100%");
		}	
		setChart.init();
	}
	
	if(period_type == 5){
		$('#saveButton').hide();
	}
});

function setCharts() {
	setchart1();
	setchart2();
	setchart3();
	setchart4();
}

var setChart = {
		init : function(){
			var chartlist = $('.downloadChartClass');
			for(var i=0 ; i < chartlist.length ; i++){
				eval('setChart.'+chartlist[i].id+'()');
			}
		},
		
		chart1 : function(){setchart1();},
		chart2 : function(){setchart2();},
		chart3 : function(){setchart3();},
		chart4 : function(){setchart4();}
}

function saveAsDoc() {
	$.ajax({
		type : 'POST',
		url : rootPath + '/report/getImages.html?type=3',
		data : imageList,
		/*data : {
			"imageList" : imageList,
			"report_type" : "report3"
		}*/
		success : function(data) {
			for(var i=0; i<imageList.length; i++) {
				var image = "#chartImage" + i;
				$(image).css("display", "block");
			}

			var date = new Date();
			var year = date.getFullYear();
			var month = new String(date.getMonth() + 1);
			var day = new String(date.getDate());
			if(month.length == 1){ 
				month = "0" + month; 
			} 
			if(day.length == 1){ 
				day = "0" + day; 
			} 
			var regdate = year + "" + month + "" + day;
			var agent = navigator.userAgent.toLowerCase();
			if (agent.indexOf("msie") != -1) {
				saveAsPage(regdate);
			} else {
				exportHTML(regdate);
			}
			
			self.close();
		}
	});
}

function exportHTML(regdate){
    var header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' "+
         "xmlns:w='urn:schemas-microsoft-com:office:word' "+
         "xmlns='http://www.w3.org/TR/REC-html40'>"+
         "<head><meta charset='utf-8'><title>Export HTML to Word Document with JavaScript</title></head><body>";
    var footer = "</body></html>";
    var sourceHTML = header+document.getElementById("source-html").innerHTML+footer;
    
    var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
    var fileDownload = document.createElement("a");
    document.body.appendChild(fileDownload);
    fileDownload.href = source;
    fileDownload.download = 'report_'+regdate+'.doc';
    fileDownload.click();
    document.body.removeChild(fileDownload);
 }

function saveAsPage(regdate) {
	if (document.execCommand) {
		document.execCommand("SaveAs", false, "report4_" + regdate + ".doc");
	} else {
		alert("error..");
	}
}


function setchart1() {
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail4_chart1_new.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart1(data);
		}
	});
}

function drawchart1(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for(var i=0; i<jsonData.length; i++) {
		var value = {"category":jsonData[i].category, "value":jsonData[i].value};
		dataProvider.push(value);
	}

	var chart1 = AmCharts.makeChart("chart1", {
		"type": "serial",
		"theme": "light",
		"marginRight": 50,
		"categoryField": "category",
	    "angle": 30,
		"rotate": true,
		"startDuration": 0,
		"autoMargins": true,
		"categoryAxis": {
			"gridPosition": "start",
			"position": "left"
		},
		"trendLines": [],
		"graphs": [
			{
				"balloonText": "<b>[[category]]</b>: [[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-1",
				"lineAlpha": 0.2,
				"title": "",
				"type": "column",
				"valueField": "value",
				"labelText": "[[value]]",
				"labelPosition": "left"
			}
		],
		"guides": [],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider": dataProvider,
	    "export": {
	    	"enabled": true,
	    	"menu": []
	     },
	      "addClassNames": true
		
	});
}
	

function setchart2() {
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail4_chart2_new.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart2(data);
		}
	});
}

function drawchart2(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for(var i=0; i<jsonData.length; i++) {
		var value = {"category":jsonData[i].category, "type1":jsonData[i].type1, "type2":jsonData[i].type2, "type3":jsonData[i].type3, "type4":jsonData[i].type4};
		dataProvider.push(value);
	}

	var chart2 = AmCharts.makeChart("chart2", {
		"type": "serial",
		"theme": "light",
	    "legend": {
	        "horizontalGap": 10,
	        "maxColumns": 1,
	        "position": "right",
			"useGraphSettings": true,
			"markerSize": 10
	    },
		"categoryField": "category",
	    "angle": 30,
		"rotate": true,
		"startDuration": 0,
		"autoMargins": true,
		"categoryAxis": {
			"gridPosition": "start",
			"position": "left"
		},
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.3,
	        "gridAlpha": 0
	    }],
		"trendLines": [],
	    "graphs": [{
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "주민등록번호",
	        "type": "column",
	        "valueField": "type1",
	        "labelText": "[[value]]"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "운전면허번호",
	        "type": "column",
	        "valueField": "type2"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "여권번호",
	        "type": "column",
	        "valueField": "type3"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "외국인등록번호",
	        "type": "column",
	        "valueField": "type10"
	    }],
		"guides": [],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider": dataProvider,
	    "export": {
	    	"enabled": true
	     },
	      "addClassNames": true
		
	});
}
	

function setchart3() {
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail4_chart3_new.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart3(data);
		}
	});
}

function drawchart3(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for(var i=0; i<jsonData.length; i++) {
		var value = {"category":jsonData[i].category, "type1":jsonData[i].type1, "type2":jsonData[i].type2, "type3":jsonData[i].type3, "type4":jsonData[i].type4};
		dataProvider.push(value);
	}

	var chart3 = AmCharts.makeChart("chart3", {
		"type": "serial",
		"theme": "light",
	    "legend": {
	        "horizontalGap": 10,
	        "maxColumns": 1,
	        "position": "right",
			"useGraphSettings": true,
			"markerSize": 10
	    },
		"categoryField": "category",
	    "angle": 30,
		"rotate": true,
		"startDuration": 0,
		"autoMargins": true,
		"categoryAxis": {
			"gridPosition": "start",
			"position": "left"
		},
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.3,
	        "gridAlpha": 0
	    }],
		"trendLines": [],
	    "graphs": [{
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "주민등록번호",
	        "type": "column",
	        "valueField": "type1",
	        "labelText": "[[value]]"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "운전면허번호",
	        "type": "column",
	        "valueField": "type2"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "여권번호",
	        "type": "column",
	        "valueField": "type3"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "외국인등록번호",
	        "type": "column",
	        "valueField": "type10"
	    }],
		"guides": [],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider": dataProvider,
	    "export": {
	    	"enabled": true
	     },
	      "addClassNames": true
		
	});
}


function setchart4() {
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail4_chart4_new.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart4(data);
		}
	});
}

function drawchart4(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for(var i=0; i<jsonData.length; i++) {
		var value = {"category":jsonData[i].category, "type1":jsonData[i].type1, "type2":jsonData[i].type2, "type3":jsonData[i].type3, "type4":jsonData[i].type4};
		dataProvider.push(value);
	}

	var chart4 = AmCharts.makeChart("chart4", {
		"type": "serial",
		"theme": "light",
	    "legend": {
	        "horizontalGap": 10,
	        "maxColumns": 1,
	        "position": "right",
			"useGraphSettings": true,
			"markerSize": 10
	    },
		"categoryField": "category",
	    "angle": 30,
		"rotate": true,
		"startDuration": 0,
		"autoMargins": true,
		"categoryAxis": {
			"gridPosition": "start",
			"position": "left"
		},
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.3,
	        "gridAlpha": 0
	    }],
		"trendLines": [],
	    "graphs": [{
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "주민등록번호",
	        "type": "column",
	        "valueField": "type1",
	        "labelText": "[[value]]"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "운전면허번호",
	        "type": "column",
	        "valueField": "type2"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "여권번호",
	        "type": "column",
	        "valueField": "type3"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.5,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "외국인등록번호",
	        "type": "column",
	        "valueField": "type10"
	    }],
		"guides": [],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider": dataProvider,
	    "export": {
	    	"enabled": true
	     },
	      "addClassNames": true
		
	});
}


function saveReport() {
	
	$("#chart1").css("display", "none");
	$("#chart2").css("display", "none");
	$("#chart3").css("display", "none");
	$("#chart4").css("display", "none");
	
	var ids = [ "chart1", "chart2", "chart3", "chart4" ];

	// Collect actual chart objects out of the AmCharts.charts array
	var charts = {}; 
	var charts_remaining = ids.length;
	for (var i = 0; i < ids.length; i++) {
		for (var x = 0; x < AmCharts.charts.length; x++) {
			if (AmCharts.charts[x].div.id == ids[i])
				charts[ids[i]] = AmCharts.charts[x];
		}
	}

	// Trigger export of each chart
	var idx = 0;
	for ( var x in charts) {
		if (charts.hasOwnProperty(x)) {
			var chart = charts[x];
			chart["export"].capture({}, function() {
				this.toPNG({}, function(data) {

					this.setup.chart.exportedImage = data;

					charts_remaining--;

					if (charts_remaining == 0) {
						for(var ch in charts) {
							var temp_chart = charts[ch];
							imageList[ch] = temp_chart.exportedImage;
						}
						saveAsDoc();
					}

				});
			});
		}
	}
}