var imageList = new Array();
var imageList = {};

$(document).ready(function() {
	if(download_type == 'pdf') {
		$("#buttonDiv").css("display","none");
		$(".downloadChartClass").css("height","500px");
		$(".downloadChartClass").css("width","950px");
		$(".downloadClass").css("width","950px");
		for (var x = 0; x < AmCharts.charts.length; x++) {
			$("#"+AmCharts.charts[x].div.id).css("width","950px");
		}
		setInterval(function() {
			setChart.init();
		},500);
	} else {
		$(".downloadClass").css("width","100%");
		$(".downloadChartClass").css("width","100%");
		for (var x = 0; x < AmCharts.charts.length; x++) {
			$("#"+AmCharts.charts[x].div.id).css("width","100%");
		}	
		setChart.init();
	}
	if(period_type == 5){
		$('#saveButton').hide();
	}
});

/*$(window).load(function(){
	var k = 0;
	var lt = setInterval(function() {
		for (var x = 0; x < AmCharts.charts.length; x++) {
			console.log(x);
			if (typeof $("#"+AmCharts.charts[x].div.id).attr("name") === "undefined") {
				$("#"+AmCharts.charts[x].div.id).css("display","none");
			}
			k++;
			
			if(k == AmCharts.charts.length) {
				clearInterval(lt);	
			}
		}
	}, 500);
		
});*/
function setCharts() {
	
	setchart11_1();
	setchart11_2();
	if(use_fullscan == 'Y'){
		setchart11_3();
	}
}

var setChart = {
		init : function(){
			var chartlist = $('.downloadChartClass');
			for(var i=0 ; i < chartlist.length ; i++){
				eval('setChart.'+chartlist[i].id+'()');
			}
		},
		
		chart11_1 : function(){setchart11_1();},
		chart11_2 : function(){setchart11_2();},
		chart11_3 : function(){setchart11_3();},
}

function saveAsDoc() {
	
	$.ajax({
		type : 'POST',
		url : rootPath + '/report/getImages.html?type=1',
		data : imageList,
		/*data : {
			"imageList" : imageList,
			"report_type" : "report1"
		}*/
		success : function(data) {
			/*for(var i=0; i<imageList.length; i++) {
				var image = "#chartImage" + i;
				$(image).css("display", "block");
			}*/
			var i=0;
			for(var img in imageList) { 
				var image = "#chartImage" + i;
				$(image).css("display", "block");
				i++;
			}
			
			var date = new Date();
			var year = date.getFullYear();
			var month = new String(date.getMonth() + 1);
			var day = new String(date.getDate());
			if(month.length == 1){ 
				month = "0" + month; 
			} 
			if(day.length == 1){ 
				day = "0" + day; 
			} 
			var regdate = year + "" + month + "" + day;
			var agent = navigator.userAgent.toLowerCase();
			if (agent.indexOf("msie") != -1) {
				saveAsPage(regdate);
			} else {
				exportHTML(regdate);
			}
			
			self.close();
		}
	});
}

function exportHTML(regdate){
    var header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' "+
         "xmlns:w='urn:schemas-microsoft-com:office:word' "+
         "xmlns='http://www.w3.org/TR/REC-html40'>"+
         "<head><meta charset='utf-8'><title>Export HTML to Word Document with JavaScript</title></head><body>";
    var footer = "</body></html>";
    var sourceHTML = header+document.getElementById("source-html").innerHTML+footer;
    
    var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
    var fileDownload = document.createElement("a");
    document.body.appendChild(fileDownload);
    fileDownload.href = source;
    fileDownload.download = 'report_'+regdate+'.doc';
    fileDownload.click();
    document.body.removeChild(fileDownload);
 }

function saveAsPage(regdate) {
	if (document.execCommand) {
		document.execCommand("SaveAs", false, "report_" + regdate + ".doc");
	} else {
		alert("error..");
	}
}


function setchart11_1() {
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart11_1.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			
			drawchart11_1(data);
		}
	});
}

function drawchart11_1(data) {
	
	var dataProvider = new Array();
	
	
	
	var jsonData = JSON.parse(data);
	
	
	for (var i = 0; i < jsonData.length; i++) {
		if(i==0) {
			$("#reportBySys").html(jsonData[i].system_name);
		}
		var value = {"system_seq": jsonData[i].system_seq, "system_name": jsonData[i].system_name, "cnt": jsonData[i].cnt};
		dataProvider.push(value);
	}
	
	var chart11_1 = AmCharts.makeChart( "chart11_1", {
	  "type": "serial",
	  "theme": "light",
	  "dataProvider": dataProvider,
	  "rotate": true,
	  "valueAxes": [ {
		"axisAlpha": 0,
	    "title": "다운로드 (회)"
	  } ],
	  "startDuration": 0,
	  "graphs": [ {
	    "balloonText": "[[category]]: <b>[[value]]</b>",
	    "fillAlphas": 1,
	    "type": "column",
	    "valueField": "cnt",
	    "labelText": "[[value]]",
	    "labelPosition": "left",
	    "descriptionField": "system_seq"
	  } ],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "system_name",
	  "categoryAxis": {
	    "gridPosition": "start",
		"axisAlpha": 0,
		"tickLength": 0,
	    "labelRotation": 45
	  },
	  "export": {
	    "enabled": true
	  }

	} );
	
	//chart11_1.addListener("clickGraphItem", handleClick11_1);
}

/*function handleClick11_1(event) {
	
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["system_seq_rp"].value = event.item.description;
	opener.eventForm.submit();
}
*/
function setchart11_2() {
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart11_2.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart11_2(data);
		}
	});
}

function drawchart11_2(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		if(i==0) 
			$("#reportByDept").html(jsonData[i].dept_name);
		var value = {"dept_name": jsonData[i].dept_name,"cnt": jsonData[i].cnt};
		dataProvider.push(value);
	}
	
	var chart11_2 = AmCharts.makeChart( "chart11_2", {
	  "type": "serial",
	  "theme": "light",
	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "title": "다운로드 (회)"
	  } ],
	  "startDuration": 0,
	  "graphs": [ {
	    "balloonText": "[[category]]: <b>[[value]]</b>",
	    "fillAlphas": 1,
	    "type": "column",
	    "valueField": "cnt",
	    "labelText": "[[value]]",
	    "labelPosition": "bottom",
	    "labelOffset": -1,
	    "descriptionField": "dept_id"
	  } ],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "dept_name",
	  "categoryAxis": {
	    "gridPosition": "start",
		"axisAlpha": 0,
		"tickLength": 0,
	    "labelRotation": 0
	  },
	  "export": {
	    "enabled": true
	  }

	} );
	
	//chart11_2.addListener("clickGraphItem", handleClick11_2);
}

/*function handleClick11_2(event) {
	
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["dept_name_rp"].value = event.item.category;
	opener.eventForm.submit();
}*/

function setchart11_3() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart11_3.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart11_3(data);
		}
	});
}

function drawchart11_3(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for(var i=0; i<jsonData.length; i++) {
		var resReq = "";
		switch(jsonData[i].result_type) {
		case "1":
			resReq = "주민등록번호";
			break;
		case "2":
			resReq = "운전면허번호";
			break;
		case "3":
			resReq = "여권번호";
			break;
		case "4":
			resReq = "신용카드번호";
			break;
		case "5":
			resReq = "건강보험번호";
			break;
		case "6":
			resReq = "전화번호";
			break;
		case "7":
			resReq = "이메일";
			break;
		case "8":
			resReq = "휴대폰번호";
			break;
		case "9":
			resReq = "계좌번호";
			break;
		case "10":
			resReq = "외국인등록번호";
			break;
		default:
			resReq = "기타";
			break;
		}
		
		var value = {"result_type": jsonData[i].result_type, "category": resReq, "value": jsonData[i].cnt};
		dataProvider.push(value);
	}
	var chart11_3 = AmCharts.makeChart("chart11_3", {
	  "type": "pie",
	  "startDuration": 0,
	   "theme": "light",
	  "addClassNames": true,
	  "legend":{
	   	"position":"right",
	    "marginRight":0,
	    "autoMargins":true,
	    "title": "다운로드 (회)"
	  },
	  "innerRadius": "30%",
	  "defs": {
	    "filter": [{
	      "id": "shadow",
	      "width": "200%",
	      "height": "200%",
	      "feOffset": {
	        "result": "offOut",
	        "in": "SourceAlpha",
	        "dx": 0,
	        "dy": 0
	      },
	      "feGaussianBlur": {
	        "result": "blurOut",
	        "in": "offOut",
	        "stdDeviation": 5
	      },
	      "feBlend": {
	        "in": "SourceGraphic",
	        "in2": "blurOut",
	        "mode": "normal"
	      }
	    }]
	  },
	  "dataProvider": dataProvider,
	  "valueField": "value",
	  "titleField": "category",
	  "descriptionField": "result_type",
	  "export": {
	    "enabled": true
	  }
	});
	
	//chart11_3.addListener("clickSlice", handleClick11_3);
}
/*
function handleClick11_3(event) {
	
	alert("해당 차트는 접속기록 내 처리된 개인정보 유형들의 수량으로\r\n접속기록 건수와는 차이가 있습니다.");
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["result_type_rp"].value = event.dataItem.description;
	opener.eventForm.submit();
}*/

var charts = {}; 
function saveReport() {
	
	$("#chart1_1").css("display", "none");
	$("#chart1_2").css("display", "none");
	$("#chart2").css("display", "none");
	$("#chart3_1").css("display", "none");
	$("#chart3_3").css("display", "none");
	$("#chart4").css("display", "none");
	$("#chart5").css("display", "none");
	$("#chart6").css("display", "none");
	$("#chart7").css("display", "none");
	$("#chart8").css("display", "none");
	$("#chart9").css("display", "none");
	$("#chart10").css("display", "none");
	
	var ids = [];
	if(period_type == 1)
		ids = [ "chart1_1", "chart1_2", "chart2", "chart3_3", "chart4", "chart5", "chart6", "chart7", "chart8", "chart9", "chart10", "chart3_1" ];
	else
		ids = [ "chart1_1", "chart1_2", "chart2", "chart3_3", "chart4", "chart5", "chart6", "chart7", "chart8", "chart9", "chart10" ];

	// Collect actual chart objects out of the AmCharts.charts array
	
	var charts_remaining = ids.length;
	for (var i = 0; i < ids.length; i++) {
		for (var x = 0; x < AmCharts.charts.length; x++) {
			if (AmCharts.charts[x].div.id == ids[i]) 
				charts[ids[i]] = AmCharts.charts[x];
		}
	}
	
	// Trigger export of each chart
	for ( var x in charts) {
		if (charts.hasOwnProperty(x)) {
			var chart = charts[x];
			chart["export"].capture({}, function() {
				this.toPNG({}, function(data) {

					this.setup.chart.exportedImage = data;
					
					charts_remaining--;

					if (charts_remaining == 0) {
						for(var ch in charts) {
							var temp_chart = charts[ch];
							imageList[ch] = temp_chart.exportedImage;
						}
						saveAsDoc();
					}

				});
			});
		}
	}
}

function show() {
	for(var x in charts) {
		var chart = charts[x];
		alert(chart.div.id);
	}
}

function goAllLogInqList(req_type) {
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["req_type_rp"].value = req_type;
	opener.eventForm.submit();
}