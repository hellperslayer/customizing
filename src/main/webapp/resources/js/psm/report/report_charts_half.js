var imageList = new Array();
var imageList = {};
	
$(document).ready(function() {
	if(download_type == 'pdf') {
		$("#buttonDiv").css("display","none");
		$(".downloadChartClass").css("height","500px");
		$(".downloadChartClass").css("width","850px");
		$(".downloadClass").css("width","850px");
		$(".HStyle5").css("width","850px");
		$('.updatetd').html("");		
		for (var x = 0; x < AmCharts.charts.length; x++) {
			$("#"+AmCharts.charts[x].div.id).css("width","850px");
		}
//		setInterval(function() {
//			setChart.init();
//		},500);
	} else {
		$(".downloadClass").css("width","750px");
		$(".downloadChartClass").css("width","750px");
		for (var x = 0; x < AmCharts.charts.length; x++) {
			$("#"+AmCharts.charts[x].div.id).css("width","750px");
		}	
//		setChart.init();
	}
	setCharts();
});

function setCharts() {
//	setchart1();
//	setchart2();
	setchart3();
	setchart4();
	setchart5();
	setchart6();
}

var setChart = {
		init : function(){
			var chartlist = $('.downloadChartClass');
			for(var i=0 ; i < chartlist.length ; i++){
				eval('setChart.'+chartlist[i].id+'()');
			}
		},
		
//		chart1 : function(){setchart1();},
//		chart2 : function(){setchart2();},
//		chart3 : function(){setchart3();},
		chart4 : function(){setchart4();},
		chart5 : function(){setchart5();},
		chart6 : function(){setchart6();}
}

function saveAsDoc() {
	
	$.ajax({
		type : 'POST',
		url : rootPath + '/report/getImages.html?type=1',
		data : imageList,
		/*data : {
			"imageList" : imageList,
			"report_type" : "report1"
		}*/
		success : function(data) {
			/*for(var i=0; i<imageList.length; i++) {
				var image = "#chartImage" + i;
				$(image).css("display", "block");
			}*/
			var i=0;
			for(var img in imageList) { 
				var image = "#chartImage" + i;
				$(image).css("display", "block");
				i++;
			}
			
			var date = new Date();
			var year = date.getFullYear();
			var month = new String(date.getMonth() + 1);
			var day = new String(date.getDate());
			if(month.length == 1){ 
				month = "0" + month; 
			} 
			if(day.length == 1){ 
				day = "0" + day; 
			} 
			var regdate = year + "" + month + "" + day;
			var agent = navigator.userAgent.toLowerCase();
			if (agent.indexOf("msie") != -1) {
				saveAsPage(regdate);
			} else {
				exportHTML(regdate);
			}
			
			self.close();
		}
	});
}

function exportHTML(regdate){
    var header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' "+
         "xmlns:w='urn:schemas-microsoft-com:office:word' "+
         "xmlns='http://www.w3.org/TR/REC-html40'>"+
         "<head><meta charset='utf-8'><title>Export HTML to Word Document with JavaScript</title></head><body>";
    var footer = "</body></html>";
    var sourceHTML = header+document.getElementById("source-html").innerHTML+footer;
    
    var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
    var fileDownload = document.createElement("a");
    document.body.appendChild(fileDownload);
    fileDownload.href = source;
    fileDownload.download = 'report_'+regdate+'.doc';
    fileDownload.click();
    document.body.removeChild(fileDownload);
 }

function saveAsPage(regdate) {
	if (document.execCommand) {
		document.execCommand("SaveAs", false, "report5_" + regdate + ".doc");
	} else {
		alert("error..");
	}
}

function setchart1() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart1_1.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart1(data);
//			drawtable1(data);
		}
	});
}

function drawchart1(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"system_name": jsonData[i].system_name,"cnt": jsonData[i].cnt};
		dataProvider.push(value);
	}
	
	var chart = AmCharts.makeChart( "chart1", {
	  "type": "serial",
	  "theme": "light",
	  "rotate": true,
	  "titles": [{
	        "text": "[개인정보 처리시스템별 접속기록 현황]",
	        "size": 15
	    }],
	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    /*"gridColor": "#FFFFFF",
	    "gridAlpha": 0.2,
	    "dashLength": 0,*/
		"axisAlpha": 0,
	    "title": "개인정보검출(건)"
	  } ],
	  //"gridAboveGraphs": true,
	  "startDuration": 0,
	  "graphs": [ {
	    "balloonText": "[[category]]: <b>[[value]]</b>",
	    "fillAlphas": 1,
	    //"lineAlpha": 0.2,
	    "type": "column",
	    "valueField": "cnt",
	    "labelText": "[[value]]",
	    "labelPosition": "left"
	  } ],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "system_name",
	  "categoryAxis": {
	    /*"gridPosition": "start",
	    "gridAlpha": 0,
	    "tickPosition": "start",
	    "tickLength": 20,*/
	    "gridPosition": "start",
		"axisAlpha": 0,
		"tickLength": 0,
	    "labelRotation": 45
	  },
	  "export": {
	    "enabled": true
	  },
      "addClassNames": true
	} );
}

function drawtable1(data) {
	$("#table1").find('tbody').empty();
	var jsonData = JSON.parse(data);
	var total = 0;
	
	if(jsonData != null) {		
		$.each(jsonData, function( i, item ) {
			
			var system_name = item.system_name;
			var cnt = item.cnt;
			
			$("<TR>").appendTo($("#table1").find('tbody'))
			.append($("<TD valign='middle' style='text-align: center;width:302;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;line-height:100%'>")
					.text(system_name)
			)
			.append($("<TD valign='middle' style='text-align: center;width:250;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;line-height:100%;'><SPAN STYLE='font-size:11.0pt;line-height:100%'>")
					.text(addCommaString(cnt.toString()))
			)
			.append($("<TD valign='middle' style='text-align: center;width:87;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;line-height:180%'>")
					.text("")
			)
			.show()
			
			total = Number(total) + Number(cnt);
		});
		
		$("<TR>").appendTo($("#table1").find('tbody'))
		.append($("<TD valign='middle' style='text-align: center;width:302;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;line-height:100%'>")
				.text("합계")
		)
		.append($("<TD valign='middle' style='text-align: center;width:250;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;line-height:100%;'><SPAN STYLE='font-size:11.0pt;line-height:100%'>")
				.text(addCommaString(total.toString()))
		)
		.append($("<TD valign='middle' style='text-align: center;width:87;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:11.0pt;line-height:180%'>")
				.text("")
		)
		.show()
	}
}

function setchart2() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportHalf_chart2.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart2(data);
//			drawtable2(data);
		}
	});
}

function drawchart2(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	var total = 0;
	var max = 0;
	var max_month;
	for (var i = 0; i < jsonData.length; i++) {
		if(Number(jsonData[i].cnt) > Number(max)) {
			max = jsonData[i].cnt;
			max_month = jsonData[i].proc_date;
		}
		var date = jsonData[i].proc_date.substring(0,4) + "년 " + jsonData[i].proc_date.substring(4,6) + "월";
		var value = {"day": date, "value": jsonData[i].cnt};
		dataProvider.push(value);
		total = Number(total) + Number(jsonData[i].cnt);
	}
	var avg = Math.round(total / jsonData.length);
	$("#avg_logcnt").html(addCommaString(avg.toString()));
	$("#max_month").html(max_month.substring(0,4) + "년 " + max_month.substring(4,6) + "월");
	
	var chart2 = AmCharts.makeChart( "chart2", {
	  "type": "serial",
	  "addClassNames": true,
	  "theme": "light",
	  "autoMargins": true,
	  "balloon": {
	    "adjustBorderColor": false,
	    "horizontalPadding": 10,
	    "verticalPadding": 8,
	    "color": "#ffffff"
	  },
  	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "position": "left",
	    "title": "개인정보검출(건)"
	  } ],
	  "startDuration": 0,
	  "graphs": [ {
	    "alphaField": "alpha",
	    "balloonText": "<span style='font-size:12px;'>[[title]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	    "fillAlphas": 1,
	    "title": "개인정보 건수",
	    "type": "column",
	    "valueField": "value",
	    "dashLengthField": "dashLengthColumn",
	    "labelText": "[[value]]",
	    "labelPosition": "bottom",
	    "labelOffset": -1
	  } ],
	  "categoryField": "day",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "axisAlpha": 0,
	    "tickLength": 0
	  },
	  "export": {
	    "enabled": true
	  },
      "addClassNames": true
	} );
}

function drawtable2(data) {
	$("#table2").find('tbody').empty();
	var jsonData = JSON.parse(data);
	var total = 0;
	var prev_mount = 0;
	if(jsonData != null) {		
		$.each(jsonData, function( i, item ) {
			
			var proc_date = item.proc_date;
			var cnt = item.cnt;
			var diff_mount = 0;
			var string_diff;
			var res;
			if(i > 0) {
				diff_mount = Number(prev_mount) - Number(cnt);
				if(diff_mount > 0) {
					string_diff = "▼ ";
					diff_mount = Math.abs(diff_mount);
				}else {
					string_diff = "▲ ";
					diff_mount = Math.abs(diff_mount);
				}
				res = string_diff + addCommaString(diff_mount.toString());
			}else {
				res = "-";
			}
			
			$("<TR>").appendTo($("#table2").find('tbody'))
			.append($("<TD valign='middle' style='text-align: center;width:192;height:41;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;line-height:100%'>")
					.text(proc_date.substring(0,4)+"년 " + proc_date.substring(4,6)+"월")
			)
			.append($("<TD valign='middle' style='text-align: center;width:224;height:41;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;line-height:100%'>")
					.text(addCommaString(cnt.toString()))
			)
			.append($("<TD valign='middle' style='text-align: center;width:220;height:41;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;line-height:100%'>")
					.text(res)
			)
			.show()
			
			prev_mount = cnt;
			total = Number(total) + Number(cnt);
		});
		
		$("<TR>").appendTo($("#table2").find('tbody'))
		.append($("<TD valign='middle' style='text-align: center;width:192;height:41;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;line-height:100%'>")
				.text("합계")
		)
		.append($("<TD valign='middle' style='text-align: center;width:224;height:41;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;line-height:100%'>")
				.text(addCommaString(total.toString()))
		)
		.append($("<TD valign='middle' style='text-align: center;width:220;height:41;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;line-height:100%'>")
				.text("-")
		)
		.show()
	}
}

function setchart3() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportHalf_chart3.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq,
			"half_type" : half_type
		},
		success: function(data) {
			drawchart3(data);
			drawtable3(data, half_type);
		}
	});
}

function drawchart3(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for(var i=0; i<jsonData.length; i++) {
		var value = {"system_name":jsonData[i].system_name, "cnt":jsonData[i].cnt};
		dataProvider.push(value);
	}
	
	var chart = AmCharts.makeChart( "chart3", {
	  "type": "serial",
	  "theme": "light",
	  "rotate": true,
	  "titles": [{
	        "text": "[개인정보 처리시스템별 처리 현황]",
	        "size": 15
	    }],
	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    /*"gridColor": "#FFFFFF",
	    "gridAlpha": 0.2,
	    "dashLength": 0,*/
		"axisAlpha": 0,
	    "title": "개인정보처리량"
	  } ],
	  //"gridAboveGraphs": true,
	  "startDuration": 0,
	  "graphs": [ {
	    "balloonText": "[[category]]: <b>[[value]]</b>",
	    "fillAlphas": 1,
	    //"lineAlpha": 0.2,
	    "type": "column",
	    "valueField": "cnt",
	    "labelText": "[[value]]",
	    "labelPosition": "left"
	  } ],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "system_name",
	  "categoryAxis": {
	    /*"gridPosition": "start",
	    "gridAlpha": 0,
	    "tickPosition": "start",
	    "tickLength": 20,*/
	    "gridPosition": "start",
		"axisAlpha": 0,
		"tickLength": 0,
	    "labelRotation": 45
	  },
	  "export": {
	    "enabled": true
	  },
      "addClassNames": true

	} );
}

function drawtable3(data, half_type) {
//	$("#table3").find('tbody').empty();
	var jsonData = JSON.parse(data);
	var total = 0;
	var max_amount = 0;
	var max_system;
	if(jsonData != null) {		
		$.each(jsonData, function( i, item ) {
			
			var system_name = item.system_name;
			var cnt = item.cnt;
			
			if(Number(cnt) > Number(max_amount)) {
				max_amount = cnt;
				max_system = system_name;
			}
			/*
			$("<TR>").appendTo($("#table3").find('tbody'))
			.append($("<TD valign='middle' style='text-align:center;width:101;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;line-height:160%'>")
					.text(i + 1)
			)
			.append($("<TD valign='middle' style='text-align:center;width:201;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle14 STYLE='text-align:center;'><SPAN STYLE='font-size:11.0pt;line-height:100%'>")
					.text(system_name)
			)
			.append($("<TD valign='middle' style='text-align:center;width:250;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle15><SPAN STYLE='font-size:11.0pt;line-height:100%'>")
					.text(addCommaString(cnt.toString()))
			)
			.append($("<TD valign='middle' style='text-align:center;width:87;height:26;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:12.0pt;line-height:180%'>")
					.text("")
			)
			.show()
			*/
			total = Number(total) + Number(cnt);
		});
		
		if(half_type == 1) {
			quarter = "상반기";
		}
		else {
			quarter = "하반기";
		}
		/*
		$("<TR>").appendTo($("#table3").find('tbody'))
		.append($("<TD colspan='2' valign='middle' style='text-align:center;width:302;height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;line-height:160%'>")
				.text(quarter + " 총계")
		)
		.append($("<TD colspan='2' valign='middle' style='text-align:center;width:338;height:33;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;line-height:100%;'><SPAN STYLE='font-size:11.0pt;line-height:100%'>")
				.text(addCommaString(total.toString()))
		)
		.show()
		*/
		$("#spanPeriType").text(quarter);
		$("#total_cnt").html(addCommaString(total.toString()));
		$("#total_cnt2").html(addCommaString(total.toString()));
		$("#max_system").html(max_system);
		$("#max_amount").html(addCommaString(max_amount.toString()));
	}
}

function setchart4() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportHalf_chart4.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq,
			"half_type" : half_type
		},
		success: function(data) {
			drawchart4(data);
			drawtable4(data, half_type);
		}
	});
}

function drawchart4(data) {
	
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for (var i = 0; i < jsonData.length; i++) {
		var date = jsonData[i].proc_date.substring(0,4) + "년 " + jsonData[i].proc_date.substring(4,6) + "월";
		var value = {"day": date, "value": jsonData[i].cnt};
		dataProvider.push(value);
	}
	
	AmCharts.makeChart( "chart4", {
	  "type": "serial",
	  "addClassNames": true,
	  "theme": "light",
	  "autoMargins": true,
	  "balloon": {
	    "adjustBorderColor": false,
	    "horizontalPadding": 10,
	    "verticalPadding": 8,
	    "color": "#ffffff"
	  },
	  "titles": [{
        "text": "[개인정보 처리량 월별 추이]",
        "size": 15
      }],
  	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "position": "left",
	    "title": "개인정보처리량"
	  } ],
	  "startDuration": 0,
	  "graphs": [ {
	    "alphaField": "alpha",
	    "balloonText": "<span style='font-size:12px;'>[[title]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	    "fillAlphas": 1,
	    "title": "개인정보 건수",
	    "type": "column",
	    "valueField": "value",
	    "dashLengthField": "dashLengthColumn",
	    "labelText": "[[value]]",
	    "labelPosition": "bottom",
	    "labelOffset": -1
	  }],
	  "categoryField": "day",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "axisAlpha": 0,
	    "tickLength": 0
	  },
	  "export": {
	    "enabled": true
	  },
      "addClassNames": true
	} );
}

function drawtable4(data, half_type) {
//	$("#table4").find('tbody').empty();
	var jsonData = JSON.parse(data);
	var total = 0;
	var max_amount = 0;
	var max_month;
	var prev_mount = 0;
	if(jsonData != null) {		
		$.each(jsonData, function( i, item ) {
			
			var proc_date = item.proc_date;
			var cnt = item.cnt;
			var diff_mount = 0;
			var string_diff;
			var res;
			if(i > 0) {
				diff_mount = Number(prev_mount) - Number(cnt);
				if(diff_mount > 0) {
					string_diff = "▼ ";
					diff_mount = Math.abs(diff_mount);
				}else {
					string_diff = "▲ ";
					diff_mount = Math.abs(diff_mount);
				}
				res = string_diff + addCommaString(diff_mount.toString());
			}else {
				res = "-";
			}
			
			if(Number(cnt) > Number(max_amount)) {
				max_amount = cnt;
				max_month = proc_date;
			}
			/*
			$("<TR>").appendTo($("#table4").find('tbody'))
			.append($("<TD valign='middle' style='text-align:center;width:123;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;line-height:160%'>")
					.text(proc_date)
			)
			.append($("<TD valign='middle' style='text-align:center;width:197;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;line-height:100%;'><SPAN STYLE='font-size:12.0pt;line-height:100%'>")
					.text(addCommaString(cnt.toString()))
			)
			.append($("<TD valign='middle' style='text-align:center;width:231;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;line-height:160%'>")
					.text(res)
			)
			.append($("<TD valign='middle' style='text-align:center;width:87;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:12.0pt;line-height:180%'>")
					.text("")
			)
			.show()
			*/
			total = Number(total) + Number(cnt);
			prev_mount = cnt;
		});
		
		if(half_type == 1) {
			quarter = "상반기";
		}
		else {
			quarter = "하반기";
		}
		/*
		$("<TR>").appendTo($("#table4").find('tbody'))
		.append($("<TD valign='middle' style='text-align:center;width:123;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;line-height:160%'>")
				.text(quarter + " 총계")
		)
		.append($("<TD valign='middle' style='text-align:center;width:197;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;line-height:100%;'><SPAN STYLE='font-size:12.0pt;line-height:100%'>")
				.text(addCommaString(total.toString()))
		)
		.append($("<TD valign='middle' style='text-align:center;width:231;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;'><SPAN STYLE='font-size:12.0pt;line-height:160%'>")
				.text("-")
		)
		.append($("<TD valign='middle' style='text-align:center;width:87;height:38;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:12.0pt;line-height:180%'>")
				.text("")
		)
		.show()
		*/
		var avg = Math.round(total / jsonData.length);
		$("#avg_cnt4").html(addCommaString(avg.toString()));
		$("#avg_total").html(addCommaString(avg.toString()));
		$("#max_month4").html(max_month.substring(4,6));
	}
}

function setchart5() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportHalf_chart5.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart5(data);
		}
	});
}

function drawchart5(data) {
	
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for(var i=0; i<jsonData.length; i++) {
		var value = {"dept_name":jsonData[i].dept_name, "cnt":jsonData[i].cnt};
		dataProvider.push(value);
	}

	AmCharts.makeChart( "chart5", {
	  "type": "serial",
	  "addClassNames": true,
	  "theme": "light",
	  "autoMargins": true,
	  "balloon": {
	    "adjustBorderColor": false,
	    "horizontalPadding": 10,
	    "verticalPadding": 8,
	    "color": "#ffffff"
	  },
	  "rotate": true,
	  "titles": [{
        "text": "[개인정보 처리 현황 소속 TOP10]",
        "size": 15
      }],
  	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "position": "left",
	    "title": "개인정보처리량"
	  } ],
	  "startDuration": 0,
	  "graphs": [ {
	    "alphaField": "alpha",
	    "balloonText": "<span style='font-size:12px;'>[[title]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	    "fillAlphas": 1,
	    "title": "개인정보 건수",
	    "type": "column",
	    "valueField": "cnt",
	    "dashLengthField": "dashLengthColumn",
	    "labelText": "[[value]]",
	    "labelPosition": "left"
	  } ],
	  "categoryField": "dept_name",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "axisAlpha": 0,
	    "tickLength": 0
	  },
	  "export": {
	    "enabled": true
	  },
      "addClassNames": true
	} );
}

function setchart6() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportHalf_chart6.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq,
			"half_type" : half_type
		},
		success: function(data) {
			drawchart6(data, half_type);
//			drawtable6(data);
		}
	});
}

function drawchart6(data, half_type) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	var quarter1,quarter2,strRes;
	if(half_type == 1) {
		quarter1 = "1분기";
		quarter2 = "2분기";
	}else {
		quarter1 = "3분기";
		quarter2 = "4분기";
	}
	
	if(jsonData.type1 > jsonData.type2)
		strRes = "감소함";
	else
		strRes = "증가함";
	
	AmCharts.makeChart( "chart6", {
	  "type": "serial",
	  "addClassNames": true,
	  "theme": "light",
	  "autoMargins": true,
	  "balloon": {
	    "adjustBorderColor": false,
	    "horizontalPadding": 10,
	    "verticalPadding": 8,
	    "color": "#ffffff"
	  },
	  "titles": [{
        "text": "[개인정보 접속기록 분기별 추이]",
        "size": 15
      }],
  	  "dataProvider": [ {
  		  "quarter" : quarter1,
  		  "cnt" : jsonData.type1
  	  }, {
  		"quarter" : quarter2,
		  "cnt" : jsonData.type2
  	  }  
  	  ],
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "position": "left",
	    "title": "개인정보처리량"
	  } ],
	  "startDuration": 0,
	  "graphs": [ {
	    "alphaField": "alpha",
	    "balloonText": "<span style='font-size:12px;'>[[title]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	    "fillAlphas": 1,
	    "title": "개인정보 건수",
	    "type": "column",
	    "valueField": "cnt",
	    "dashLengthField": "dashLengthColumn",
	    "labelText": "[[value]]",
	    "labelPosition": "bottom",
	    "labelOffset": -1
	  } ],
	  "categoryField": "quarter",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "axisAlpha": 0,
	    "tickLength": 0
	  },
	  "export": {
	    "enabled": true
	  },
      "addClassNames": true
	} );
	
	var avg = Math.round(jsonData.type99 / 2);
	$("#avg_cnt6").html(addCommaString(avg.toString()));
	$("#quarter1").html(quarter1);
	$("#quarter2").html(quarter2);
	$("#res6").html(strRes);
}

function drawtable6(data) {
	$("#table6").find('tbody').empty();
	var jsonData = JSON.parse(data);
	if(jsonData != null) {		
		var string_diff = "";
		var diff_mount = Number(jsonData.type1) - Number(jsonData.type2);
		var res;
		if(diff_mount > 0) {
			string_diff = "▼ ";
			res = string_diff + addCommaString(diff_mount.toString());
		}else if(diff_mount < 0){
			string_diff = "▲ ";
			diff_mount = Math.abs(diff_mount);
			res = string_diff + addCommaString(diff_mount.toString());
		}else {
			res = "-";
		}
		
		$("<TR>").appendTo($("#table6").find('tbody'))
		.append($("<TD valign='middle' style='text-align:center;width:186;height:44;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle0 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:12.0pt;line-height:180%'>")
				.text(addCommaString(jsonData.type1.toString()))
		)
		.append($("<TD valign='middle' style='text-align:center;width:186;height:44;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle14 STYLE='text-align:center;line-height:180%;'><SPAN STYLE='font-size:12.0pt;line-height:180%'>")
				.text(addCommaString(jsonData.type2.toString()))
		)
		.append($("<TD valign='middle' style='text-align:center;width:174;height:44;border-left:solid #000000 0.4pt;border-right:solid #000000 0.4pt;border-top:solid #000000 0.4pt;border-bottom:solid #000000 0.4pt;padding:1.4pt 5.1pt 1.4pt 5.1pt'><P CLASS=HStyle15 STYLE='line-height:180%;'><SPAN STYLE='font-size:12.0pt;line-height:180%'>")
				.text(res)
		)
		.show()
	}
}

function uploadImage1() {
	var form = $("#uploadForm1")[0];
	
	if(check(form)) {
	    var formData = new FormData(form);
	    
		$.ajax({
			type : 'POST',
			url : rootPath + '/report/uploadImage1.html',
			data : formData,
			processData : false,
			contentType : false,
			success : function(data) {
				var src = scheme + "://" + serverName + ":" + port + rootPath + "/resources/reportUpload/" + data.fileName;
				$("<img align='center' src='" + src + "' width='651px' height='301px' style='display: block;position: absolute;text-align: center;'>").prependTo($("#uploadtable1"));
			}
		});
	}
}

function uploadImage2() {
	var form = $("#uploadForm2")[0];
	if(check(form)) {
	    var formData = new FormData(form);
	    
		$.ajax({
			type : 'POST',
			url : rootPath + '/report/uploadImage2.html',
			data : formData,
			processData : false,
			contentType : false,
			success : function(data) {
				var src = scheme + "://" + serverName + ":" + port + rootPath + "/resources/reportUpload/" + data.fileName;
				$("<img align='center' src='" + src + "' width='651px' height='301px' style='display: block;position: absolute;text-align: center;'>").prependTo($("#uploadtable2"));
			}
		});
	}
}

function check(form) {
	var file = form.filename.value;
	if(file != "") {
		var fileExt = file.substring(file.lastIndexOf(".") + 1);
		var reg = /gif|jpg|jpeg|png/i;	// 업로드 가능 확장자.
		if(reg.test(fileExt) == false) {
			alert("첨부파일은 gif, jpg, png로 된 이미지만 가능합니다.");
			return false;
		} else {
			var fileNameCheck = file.substring(file.lastIndexOf("\\") + 1, file.length).toLowerCase();
			for(i=0; i<fileNameCheck.length; i++) {
				var chk = fileNameCheck.charCodeAt(i);
				if(chk > 128) {
					alert("파일명이 한글로 된 이미지는 등록할 수 없습니다.");
					return false;
				}
			}
			return true;
		}
	} else {
		alert("선택된 이미지가 없습니다.");
		return false;
	}
}

var charts = {}; 
function saveReport() {
	
	$("#chart1").css("display", "none");
	$("#chart2").css("display", "none");
	$("#chart3").css("display", "none");
	$("#chart4").css("display", "none");
	$("#chart5").css("display", "none");
	$("#chart6").css("display", "none");
	
	var ids = [];
	if(period_type == 1)
		ids = [ "chart1", "chart2", "chart3", "chart4", "chart5", "chart6" ];

	// Collect actual chart objects out of the AmCharts.charts array
	
	var charts_remaining = ids.length;
	for (var i = 0; i < ids.length; i++) {
		for (var x = 0; x < AmCharts.charts.length; x++) {
			if (AmCharts.charts[x].div.id == ids[i]) 
				charts[ids[i]] = AmCharts.charts[x];
		}
	}
	
	// Trigger export of each chart
	for ( var x in charts) {
		if (charts.hasOwnProperty(x)) {
			var chart = charts[x];
			chart["export"].capture({}, function() {
				this.toPNG({}, function(data) {

					this.setup.chart.exportedImage = data;
					
					charts_remaining--;

					if (charts_remaining == 0) {
						for(var ch in charts) {
							var temp_chart = charts[ch];
							imageList[ch] = temp_chart.exportedImage;
						}
						saveAsDoc();
					}

				});
			});
		}
	}
}

function show() {
	for(var x in charts) {
		var chart = charts[x];
		alert(chart.div.id);
	}
}

function addCommaString(str){
	if(isNaN(str)){
		return "0"; 
	}else{
		var lineCount = 0;
		var arr = [];
		var str2 = str;
		for(var i = 0; i < str.length / 3; i++){
			arr[lineCount++] = str2.substring((str2.length - 3), str2.length);
			str2 = str2.substring(0, (str2.length - 3));
		}
		arr.reverse();
		return arr.join(",");
	}
};