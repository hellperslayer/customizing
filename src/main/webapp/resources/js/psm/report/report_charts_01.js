var imageList = new Array();
var imageList = {};

$(document).ready(function() {
	if(download_type == 'pdf') {
		$("#buttonDiv").css("display","none");
		$(".downloadChartClass").css("height","670px");
		$(".downloadChartClass").css("width","850px");
		$(".downloadClass").css("width","850px");
		$(".downloadClass_left").css("width","557px");
		$(".downloadClass_right").css("width","280px");
		$(".HStyle5").css("width","850px");
		for (var x = 0; x < AmCharts.charts.length; x++) {
			$("#"+AmCharts.charts[x].div.id).css("width","850px");
		}
		setInterval(function() {
			setChart.init();
		},500);
		
		
	} else {
		$(".downloadClass").css("width","750px");
		$(".downloadChartClass").css("width","750px");
		for (var x = 0; x < AmCharts.charts.length; x++) {
			$("#"+AmCharts.charts[x].div.id).css("width","750px");
		}	
		setChart.init();
	}
	
	if(period_type == 5){
		$('#saveButton').hide();
	}
});

function setCharts() {
	setchart_new1();
	setchart_new2();
	setchart_new3();
	setchart_new4();
	setchart_new5();
	
	setchart1();
	setchart3();
	setchart4();
}

var setChart = {
		init : function(){
			var chartlist = $('.downloadChartClass');
			for(var i=0 ; i < chartlist.length ; i++){
				eval('setChart.'+chartlist[i].id+'()');
			}
		},
		chart_new1 : function(){setchart_new1()},
		chart_new2 : function(){setchart_new2()},
		chart_new3 : function(){setchart_new3()},
		chart_new4 : function(){setchart_new4()},
		chart_new5 : function(){setchart_new5()}
		
		,chart1 : function(){setchart1()}
		,chart3 : function(){setchart3()}
		,chart4 : function(){setchart4()}
}

function saveAsDoc() {
	
	$.ajax({
		type : 'POST',
		url : rootPath + '/report/getImages.html?type=1',
		data : imageList,
		/*data : {
			"imageList" : imageList,
			"report_type" : "report1"
		}*/
		success : function(data) {
			/*for(var i=0; i<imageList.length; i++) {
				var image = "#chartImage" + i;
				$(image).css("display", "block");
			}*/
			var i=0;
			for(var img in imageList) { 
				var image = "#chartImage" + i;
				$(image).css("display", "block");
				i++;
			}
			
			var date = new Date();
			var year = date.getFullYear();
			var month = new String(date.getMonth() + 1);
			var day = new String(date.getDate());
			if(month.length == 1){ 
				month = "0" + month; 
			} 
			if(day.length == 1){ 
				day = "0" + day; 
			} 
			var regdate = year + "" + month + "" + day;
			var agent = navigator.userAgent.toLowerCase();
			if (agent.indexOf("msie") != -1) {
				saveAsPage(regdate);
			} else {
				exportHTML(regdate);
			}
			
			self.close();
		}
	});
}


function setchart_new1() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart_new1.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart_new1(data);
		}
	});
}

function drawchart_new1(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		if(i==0) 
			$("#reportBySystem").html(jsonData[i].system_name);
		var value = {"system_name": jsonData[i].system_name,"logCnt": jsonData[i].logCnt,"privacyCnt": jsonData[i].privacyCnt};
		dataProvider.push(value);
	}
	var title1 = "개인정보 처리량";
	var title2 = "개인정보 이용량";
	
	var chart_new1 = AmCharts.makeChart( "chart_new1", {
	  "type": "serial",
	  "theme": "light",
	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "title": "개인정보검출(건)"
	  } ],
	  "legend": {
	        "useGraphSettings": true
		},
	  "startDuration": 0,
	  "graphs": [ 
		  {
				"balloonText": "[[title]]:[[value]]",
				"title": title1,
				"id": "AmGraph-2",
				"lineAlpha": "10",
				"bullet" : "round",
				"type": "line",
				"valueField": "logCnt",
				"labelPosition": "bottom",
				"labelText": "[[value]]"
			},
			{
				"balloonText": "[[title]]:[[value]]",
				"title": title2,
				"id": "AmGraph-1",
				"lineAlpha": "10",
				"bullet" : "round",
				"type": "line",
				"valueField": "privacyCnt",
				"labelPosition": "bottom",
				"labelText": "[[value]]"
	  } ],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "system_name",
	  "categoryAxis": {
	    "gridPosition": "start",
		"axisAlpha": 0,
		"tickLength": 0,
	    "labelRotation": 45
	  },
	  "export": {
	    "enabled": true
	  }

	} );
	
}




//2
function setchart_new2() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart_new2.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart_new2(data);
		}
	});
}

function drawchart_new2(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for (var i = 0; i < jsonData.length; i++) {
		if(i==0) 
			$("#reportByDept").html(jsonData[i].dept_name);
		if(jsonData[i].dept_name == '' || jsonData[i].dept_name == 'null' || jsonData[i].dept_name == null)
			jsonData[i].dept_name = "부서명 없음";
		var value = {"dept_name": jsonData[i].dept_name,"logCnt": jsonData[i].logCnt,"privacyCnt": jsonData[i].privacyCnt};
		dataProvider.push(value);
	}

	var title1 = "개인정보 이용량";
	var title2 = "개인정보 처리량";
	
	var chart_new2 = AmCharts.makeChart("chart_new2", {
		"type": "serial",
	     "theme": "light",
		"categoryField": "dept_name",
		"rotate": true,
		"startDuration": 0,
		"categoryAxis": {
			"gridPosition": "start",
			"position": "left"
		},
		"legend": {
	        "useGraphSettings": true
		},
		"trendLines": [],
		"graphs": [
			{
				"balloonText": "[[title]]:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-2",
				"lineAlpha": 0.2,
				"title": title2,
				"type": "column",
				"valueField": "logCnt",
				"labelPosition": "right",
				"labelText": "[[value]]"
			},
			{
				"balloonText": "[[title]]:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-1",
				"lineAlpha": 0.2,
				"title": title1,
				"type": "column",
				"valueField": "privacyCnt",
				"labelPosition": "right",
				"labelText": "[[value]]"
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"position": "bottom",
				"axisAlpha": 0,
				"title": "개인정보검출(건)"
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider":dataProvider,
	    "export": {
	    	"enabled": true
	     }

	});
}

//2
function setchart_new3() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart_new3.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart_new3(data);
		}
	});
}

function drawchart_new3(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		if(i==0) 
			$("#reportByCode").html(jsonData[i].dept_name);
		var value = {"code_name": jsonData[i].code_name,"privacyCnt": jsonData[i].privacyCnt};
		dataProvider.push(value);
	}
	
	var chart_new3 = AmCharts.makeChart( "chart_new3", {
	  "type": "serial",
	  "theme": "light",
	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "title": "개인정보검출(건)"
	  } ],
	  "startDuration": 0,
	  "graphs": [ {
	    "balloonText": "[[category]]: <b>[[value]]</b>",
	    "fillAlphas": 1,
	    "type": "column",
	    "valueField": "privacyCnt",
	    "labelText": "[[value]]",
	    "labelPosition": "top",
	    "labelOffset": -1
	  } ],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "code_name",
	  "categoryAxis": {
	    "gridPosition": "start",
		"axisAlpha": 0,
		"tickLength": 0,
	    "labelRotation": 45
	  },
	  "export": {
	    "enabled": true
	  }

	} );
	
}

function setchart_new4() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart6.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart_new4(data);
		}
	});
}

function drawchart_new4(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for(var i=0; i<jsonData.length; i++) {
		var resReq = "";
		switch(jsonData[i].req_type) {
		case "RD":
			resReq = "조회";
			break;
		case "CR":
			resReq = "등록";
			break;
		case "UD":
			resReq = "수정";
			break;
		case "DL":
			resReq = "삭제";
			break;
		case "DN":
			resReq = "다운로드";
			break;
		case "EX":
			resReq = "실행";
			break;
		case "PR":
			resReq = "출력";
			break;
		case "CO":
			resReq = "수집";
			break;
		case "NE":
			resReq = "생성";
			break;
		case "BE":
			resReq = "연계";
			break;
		case "IN":
			resReq = "연동";
			break;
		case "WR":
			resReq = "기록";
			break;
		case "SA":
		case "SV":
			resReq = "저장";
			break;
		case "SU":
			resReq = "보유";
			break;
		case "FI":
			resReq = "가공";
			break;
		case "UP":
			resReq = "편집";
			break;
		case "SC":
			resReq = "검색";
			break;
		case "CT":
			resReq = "정정";
			break;
		case "RE":
			resReq = "복구";
			break;
		case "US":
			resReq = "이용";
			break;
		case "OF":
			resReq = "제공";
			break;
		case "OP":
			resReq = "공개";
			break;
		case "AN":
			resReq = "파기";
			break;
		case "PRRD":
			resReq = "출력(RD)";
			break;
		case "PREX":
			resReq = "출력(Excel)";
			break;
		default:
			resReq = "기타";
			break;
		}
		
		var value = {"req_type": jsonData[i].req_type, "category": resReq, "value": jsonData[i].cnt};
		dataProvider.push(value);
	}
	var chart_new4 = AmCharts.makeChart("chart_new4", {
	  "type": "pie",
	  "startDuration": 0,
	   "theme": "light",
	  "addClassNames": true,
	  "legend":{
	   	"position":"right",
	    "marginRight":0,
	    "autoMargins":true,
	    "title": "개인정보검출(건)"
	  },
	  "innerRadius": "30%",
	  "defs": {
	    "filter": [{
	      "id": "shadow",
	      "width": "200%",
	      "height": "200%",
	      "feOffset": {
	        "result": "offOut",
	        "in": "SourceAlpha",
	        "dx": 0,
	        "dy": 0
	      },
	      "feGaussianBlur": {
	        "result": "blurOut",
	        "in": "offOut",
	        "stdDeviation": 5
	      },
	      "feBlend": {
	        "in": "SourceGraphic",
	        "in2": "blurOut",
	        "mode": "normal"
	      }
	    }]
	  },
	  "dataProvider": dataProvider,
	  "valueField": "value",
	  "titleField": "category",
	  "descriptionField": "req_type",
	  "export": {
	    "enabled": true
	  }
	});
	
}



function setchart1() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail2_chart1.html',
		data: { 
			"period_type" : period_type,
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart1(data);
		}
	});
}



function setchart_new5() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart_new5.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart_new5(data);
		}
	});
}

function drawchart_new5(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"emp_user_name": jsonData[i].emp_user_name,"logCnt": jsonData[i].logCnt,"privacyCnt": jsonData[i].privacyCnt};
		dataProvider.push(value);
	}

	var title1 = "개인정보 이용량";
	var title2 = "개인정보 처리량";
	
	var chart_new2 = AmCharts.makeChart("chart_new5", {
		"type": "serial",
	     "theme": "light",
		"categoryField": "emp_user_name",
		"rotate": true,
		"startDuration": 0,
		"categoryAxis": {
			"gridPosition": "start",
			"position": "left"
		},
		"legend": {
	        "useGraphSettings": true
		},
		"trendLines": [],
		"graphs": [
			{
				"balloonText": "[[title]]:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-2",
				"lineAlpha": 0.2,
				"title": title2,
				"type": "column",
				"valueField": "logCnt",
				"labelPosition": "right",
				"labelText": "[[value]]"
			},
			{
				"balloonText": "[[title]]:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-1",
				"lineAlpha": 0.2,
				"title": title1,
				"type": "column",
				"valueField": "privacyCnt",
				"labelPosition": "right",
				"labelText": "[[value]]"
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"position": "bottom",
				"axisAlpha": 0,
				"title": "개인정보검출(건)"
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider":dataProvider,
	    "export": {
	    	"enabled": true
	     }

	});
}






// ============================================ CPO 보고서 차트 =================================================================

function drawchart1(data) {
	var dataProvider = new Array();
	var JsonData = JSON.parse(data);
	var arrCur = JsonData["cur"];
	var arrPre = JsonData["pre"];
	for(var i=0; i<arrCur.length; i++) {
		var value = {"rule": arrCur[i].rule_nm, "cur": arrCur[i].cnt, "pre": arrPre[i].cnt, "seq": arrCur[i].rule_cd};
		dataProvider.push(value);
	}
	var title1 = JsonData["title1"];
	var title2 = JsonData["title2"];
	var chart1 = AmCharts.makeChart("chart1", {
		"type": "serial",
	     "theme": "light",
		"categoryField": "rule",
//		"depth3D": 10,
//	    "angle": 30,
		"rotate": true,
		"startDuration": 0,
		"autoMargins": true,
		"categoryAxis": {
			"gridPosition": "start",
			"position": "left"
		},
		"legend": {
	        "useGraphSettings": true,
	        "position": "bottom",
	        "autoMargins": true,
		},
		"trendLines": [],
		"graphs": [
			{
				"balloonText": "[[title]]:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-2",
				"lineAlpha": 0.2,
				"title": title2,
				"type": "column",
				"valueField": "pre",
				"labelText": "[[value]]",
				"labelPosition": "left",
				"descriptionField": "seq"
			},
			{
				"balloonText": "[[title]]:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-1",
				"lineAlpha": 0.2,
				"title": title1,
				"type": "column",
				"valueField": "cur",
				"labelText": "[[value]]",
				"labelPosition": "left",
				"descriptionField": "seq"
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"position": "bottom",
				"axisAlpha": 0,
				"title": "비정상행위 의심건수(건)"
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider": dataProvider,
	    "export": {
	    	"enabled": true
	     },
	      "addClassNames": true
	});
	chart1.addListener("clickGraphItem", handleClick1);
}




function setchart3() {
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail2_chart3.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart3(data);
		}
	});
}
function drawchart3(data) {
	var jsonData = JSON.parse(data);
	
	var rule_list = jsonData["rule_list"];
	var emp_list = jsonData["emp_list"];
	
	var graphs = new Array();
	for(var i=0; i<rule_list.length; i++) {
		var value = {"balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
			        "fillAlphas": 0.8,
			        "labelText": "[[value]]",
			        "lineAlpha": 0.3,
			        "title": rule_list[i].rule_nm,
			        "type": "column",
					"color": "#000000",
			        "valueField": "cnt_"+(i+1)+"000",
			        "columnWidth" : 0.6};
		graphs.push(value);
	}
	
	var chart3 = AmCharts.makeChart("chart3", {
	    "type": "serial",
		"theme": "light",
		"startDuration": 0,
	    "legend": {
	        "horizontalGap": 10,
	        "maxColumns": 1,
	        "position": "right",
			"useGraphSettings": true,
			"markerSize": 10
	    },
//	    "depth3D": 20,
//	    "angle": 40,
	    "dataProvider": emp_list,
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.3,
	        "gridAlpha": 0.3,
	        "title": "비정상행위 의심건수(건)"
	    }],
	    "graphs": graphs,
	    "categoryField": "dept_name",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "gridAlpha": 0,
	        "position": "left"
	    },
	    "export": {
	    	"enabled": true
	     },
	      "addClassNames": true

	});
	chart3.addListener("clickGraphItem", handleClick3);
}
function setchart4() {
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail2_chart4.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart4(data);
		}
	});
}

function drawchart4(data) {
	var jsonData = JSON.parse(data);
	var listEmpDetail = jsonData["listEmpDetail"];
	var dataProvider = new Array();
	for (var i = 0; i < listEmpDetail.length; i++) {
		var value = {"emp_user_name": listEmpDetail[i].emp_user_name,"cnt": listEmpDetail[i].cnt};
		dataProvider.push(value);
	}
	var chart4 = AmCharts.makeChart( "chart4", {
		  "type": "serial",
		  "theme": "light",
		  "dataProvider": dataProvider,
		  "valueAxes": [ {
		    "axisAlpha": 0,
		    "title": "비정상행위 의심건수(건)"
		  } ],
		  "startDuration": 0,
		  "graphs": [ {
		    "balloonText": "[[category]]: <b>[[value]]</b>",
		    "fillAlphas": 1,
		    "type": "column",
		    "valueField": "cnt",
		    "labelText": "[[value]]",
		    "labelPosition": "bottom",
		    "labelOffset": -1
		  } ],
		  "chartCursor": {
		    "categoryBalloonEnabled": false,
		    "cursorAlpha": 0,
		    "zoomable": false
		  },
		  "categoryField": "emp_user_name",
		  "categoryAxis": {
		    "gridPosition": "start",
			"axisAlpha": 0,
			"tickLength": 0,
		    "labelRotation": 45
		  },
		  "export": {
		    "enabled": true
		  }
		} );
}



var charts = {}; 
function saveReport() {
	$("#chart_new1").css("display", "none");
	$("#chart_new1_1").css("display", "none");
	$("#chart_new1_2").css("display", "none");
	$("#chart_new2").css("display", "none");
	$("#chart_new3").css("display", "none");
	$("#chart_new4").css("display", "none");
	//$("#chart_new5").css("display", "none");
	
	var ids = [];
	if(period_type == 1)
		ids = [ "chart_new1", "chart_new1_1", "chart_new1_2", "chart_new2", "chart_new3", "chart_new4"];
	else
		ids = [ "chart_new1", "chart_new1_1", "chart_new1_2",  "chart_new2", "chart_new3", "chart_new4"];

	// Collect actual chart objects out of the AmCharts.charts array
	
	var charts_remaining = ids.length;
	for (var i = 0; i < ids.length; i++) {
		for (var x = 0; x < AmCharts.charts.length; x++) {
			if (AmCharts.charts[x].div.id == ids[i]) 
				charts[ids[i]] = AmCharts.charts[x];
		}
	}
	
	// Trigger export of each chart
	for ( var x in charts) {
		if (charts.hasOwnProperty(x)) {
			var chart = charts[x];
			chart["export"].capture({}, function() {
				this.toPNG({}, function(data) {

					this.setup.chart.exportedImage = data;
					
					charts_remaining--;

					if (charts_remaining == 0) {
						for(var ch in charts) {
							var temp_chart = charts[ch];
							imageList[ch] = temp_chart.exportedImage;
						}
						saveAsDoc();
					}

				});
			});
		}
	}
}

function show() {
	for(var x in charts) {
		var chart = charts[x];
		alert(chart.div.id);
	}
}

function goAllLogInqList(req_type) {
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["req_type_rp"].value = req_type;
	opener.eventForm.submit();
}

