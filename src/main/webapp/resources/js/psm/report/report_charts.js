var imageList = new Array();
var imageList = {};

$(document).ready(function() {
	if(download_type == 'pdf') {
		$("#buttonDiv").css("display","none");
		$(".downloadChartClass").css("height","500px");
		$(".downloadChartClass").css("width","850px");
		$(".downloadClass").css("width","850px");
		$(".HStyle5").css("width","850px");
		for (var x = 0; x < AmCharts.charts.length; x++) {
			$("#"+AmCharts.charts[x].div.id).css("width","850px");
		}
		setInterval(function() {
			setChart.init();
		},500);
		
		
	} else {
		$(".downloadClass").css("width","750px");
		$(".downloadChartClass").css("width","750px");
		for (var x = 0; x < AmCharts.charts.length; x++) {
			$("#"+AmCharts.charts[x].div.id).css("width","750px");
		}	
		setChart.init();
	}
	
	if(period_type == 5){
		$('#saveButton').hide();
	}
});

/*$(window).load(function(){
	var k = 0;
	var lt = setInterval(function() {
		for (var x = 0; x < AmCharts.charts.length; x++) {
			console.log(x);
			if (typeof $("#"+AmCharts.charts[x].div.id).attr("name") === "undefined") {
				$("#"+AmCharts.charts[x].div.id).css("display","none");
			}
			k++;
			
			if(k == AmCharts.charts.length) {
				clearInterval(lt);	
			}
		}
	}, 500);
		
});*/
function setCharts() {

//	setchart1_1();
//	setchart1_2();
	setchart2();
	if(period_type != 1)
		setchart3_1();
	//setchart3_2();
	setchart3_3();
	setchart4();
	setchart4_1();
	setchart5();
	setchart5_1();
	setchart6();
	setchart7();
	//setchart8();
	setchart8_new();
	//setchart9();
	setchart9_new();
	//setchart10();
	setchart10_new();
//	setchart11_1();
//	setchart11_2();
//	setchart11_3();
}

var setChart = {
		init : function(){
			var chartlist = $('.downloadChartClass');
			for(var i=0 ; i < chartlist.length ; i++){
				eval('setChart.'+chartlist[i].id+'()');
			}
		},
		
//		chart1_1 : function(){setchart1_1();},
//		chart1_2 : function(){setchart1_2()},
		chart2 : function(){setchart2()},
		chart3_1 : function(){setchart3_1()},
//		chart3_2 : function(){setchart3_2()},
		chart3_3 : function(){setchart3_3()},
		chart4 : function(){setchart4()},
		chart4_1 : function(){setchart4_1()},
		chart5 : function(){setchart5()},
		chart5_1 : function(){setchart5_1()},
		chart6 : function(){setchart6()},
		chart7 : function(){setchart7()},
		chart8 : function(){setchart8_new()},
		chart9 : function(){setchart9_new()},
		chart10 : function(){setchart10_new()},
		chart11_1 : function(){setchart11_1()},
		chart11_2 : function(){setchart11_2()},
		chart11_3 : function(){setchart11_3()}
}

function saveAsDoc() {
	
	$.ajax({
		type : 'POST',
		url : rootPath + '/report/getImages.html?type=1',
		data : imageList,
		/*data : {
			"imageList" : imageList,
			"report_type" : "report1"
		}*/
		success : function(data) {
			/*for(var i=0; i<imageList.length; i++) {
				var image = "#chartImage" + i;
				$(image).css("display", "block");
			}*/
			var i=0;
			for(var img in imageList) { 
				var image = "#chartImage" + i;
				$(image).css("display", "block");
				i++;
			}
			
			var date = new Date();
			var year = date.getFullYear();
			var month = new String(date.getMonth() + 1);
			var day = new String(date.getDate());
			if(month.length == 1){ 
				month = "0" + month; 
			} 
			if(day.length == 1){ 
				day = "0" + day; 
			} 
			var regdate = year + "" + month + "" + day;
			var agent = navigator.userAgent.toLowerCase();
			if (agent.indexOf("msie") != -1) {
				saveAsPage(regdate);
			} else {
				exportHTML(regdate);
			}
			
			self.close();
		}
	});
}

/*function exportHTML(regdate){
    var header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' "+
         "xmlns:w='urn:schemas-microsoft-com:office:word' "+
         "xmlns='http://www.w3.org/TR/REC-html40'>"+
         "<head><meta charset='utf-8'><title>Export HTML to Word Document with JavaScript</title></head><body>";
    var footer = "</body></html>";
    var sourceHTML = header+document.getElementById("source-html").innerHTML+footer;
    var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
    var fileDownload = document.createElement("a");
    document.body.appendChild(fileDownload);
    fileDownload.href = source;
    fileDownload.download = 'report_'+regdate+'.doc';
    fileDownload.click();
    document.body.removeChild(fileDownload);
 }

function saveAsPage(regdate) {
	if (document.execCommand) {
		document.execCommand("SaveAs", false, "report_" + regdate + ".doc");
	} else {
		alert("error..");
	}
}*/

function setchart1_1() {
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart1_1.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart1_1(data);
		}
	});
}

function drawchart1_1(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		if(i==0) {
			$("#reportBySys").html(jsonData[i].system_name);
		}
		var value = {"system_seq": jsonData[i].system_seq, "system_name": jsonData[i].system_name, "cnt": jsonData[i].cnt};
		dataProvider.push(value);
	}
	
	var chart1_1 = AmCharts.makeChart( "chart1_1", {
	  "type": "serial",
	  "theme": "light",
	  "titles": [{
	        "text": "[개인정보 처리시스템별 결과]",
	        "size": 15
	    }],
	  "dataProvider": dataProvider,
	  "rotate": true,
	  "valueAxes": [ {
	    /*"gridColor": "#FFFFFF",
	    "gridAlpha": 0.2,
	    "dashLength": 0,*/
		"axisAlpha": 0,
	    "title": "개인정보검출(건)"
	  } ],
	  //"gridAboveGraphs": true,
	  "startDuration": 0,
	  "graphs": [ {
	    "balloonText": "[[category]]: <b>[[value]]</b>",
	    "fillAlphas": 1,
	    //"lineAlpha": 0.2,
	    "type": "column",
	    "valueField": "cnt",
	    "labelText": "[[value]]",
	    "labelPosition": "right",
	    "descriptionField": "system_seq"
	  } ],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "system_name",
	  "categoryAxis": {
	    /*"gridPosition": "start",
	    "gridAlpha": 0,
	    "tickPosition": "start",
	    "tickLength": 20,*/
	    "gridPosition": "start",
		"axisAlpha": 0,
		"tickLength": 0,
	    "labelRotation": 45,
	    "fontSize":8,
	    "minVerticalGap":0
	  },
	  "export": {
	    "enabled": true
	  },
	  "addClassNames": true

	} );
	
	chart1_1.addListener("clickGraphItem", handleClick1_1);
}

function handleClick1_1(event) {
	
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["system_seq_rp"].value = event.item.description;
	opener.eventForm.submit();
}

function setchart1_2() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart1_2.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart1_2(data);
		}
	});
}

function drawchart1_2(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		if(i==0) 
			$("#reportByDept").html(jsonData[i].dept_name);
		var value = {"dept_name": jsonData[i].dept_name,"cnt": jsonData[i].cnt};
		dataProvider.push(value);
	}
	
	var chart1_2 = AmCharts.makeChart( "chart1_2", {
	  "type": "serial",
	  "theme": "light",
	  "titles": [{
	        "text": "[소속별 결과]",
	        "size": 15
	    }],
	  "dataProvider": dataProvider,
	  "valueAxes": [ {
//	    "gridColor": "#FFFFFF",
//	    "gridAlpha": 0.2,
//	    "dashLength": 0
	    "axisAlpha": 0,
	    "title": "개인정보검출(건)"
	  } ],
	  //"gridAboveGraphs": true,
	  "startDuration": 0,
	  "graphs": [ {
	    "balloonText": "[[category]]: <b>[[value]]</b>",
	    "fillAlphas": 1,
	    //"lineAlpha": 0.2,
	    "type": "column",
	    "valueField": "cnt",
	    "labelText": "[[value]]",
	    "labelPosition": "bottom",
	    "labelOffset": -1,
	    "descriptionField": "dept_id"
	  } ],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "dept_name",
	  "categoryAxis": {
	    /*"gridPosition": "start",
	    "gridAlpha": 0,
	    "tickPosition": "start",
	    "tickLength": 20,
	    "labelRotation": 45*/
	    "gridPosition": "start",
		"axisAlpha": 0,
		"tickLength": 0,
	    "labelRotation": 45
	  },
	  "export": {
	    "enabled": true
	  },
	  "addClassNames": true

	} );
	
	chart1_2.addListener("clickGraphItem", handleClick1_2);
}

function handleClick1_2(event) {
	
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["dept_name_rp"].value = event.item.category;
	opener.eventForm.submit();
}

function setchart2() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart2.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart2(data);
		}
	});
}

function drawchart2(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
/*	
	var sum = new Number();
	
	for (var i = 0; i < jsonData.length; i++) {
		sum=sum+Number(jsonData[i].cnt);
	}
*/	
	for (var i = 0; i < jsonData.length; i++) {
//		var value = {"day": jsonData[i].proc_date, "value": jsonData[i].cnt, "average": sum/jsonData.length};
		var value = {"day": jsonData[i].proc_date, "value": jsonData[i].cnt, "week": jsonData[i].week};
		dataProvider.push(value);
	}
	var chart2 = AmCharts.makeChart( "chart2", {
	  "type": "serial",
	  "addClassNames": true,
	  "theme": "light",
	  "autoMargins": true,
	  "balloon": {
	    "adjustBorderColor": false,
	    "horizontalPadding": 10,
	    "verticalPadding": 8,
	    "color": "#ffffff"
	  },
	  "titles": [{
	        "text": "개인정보 처리 현황",
	        "size": 20
	  }],

	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "position": "left",
	    "title": "개인정보검출(건)"
	  } ],
	  "startDuration": 0,
	  "graphs": [ {
	    "alphaField": "alpha",
	    "balloonText": "<span style='font-size:12px;'>[[title]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	    "fillAlphas": 1,
	    "title": "개인정보 건수",
	    "type": "column",
	    "valueField": "value",
	    "dashLengthField": "dashLengthColumn",
	    "labelText": "[[value]]",
	    "labelPosition": "bottom",
	    "labelOffset": -1,
	    "descriptionField": "week"
/*	    	
	  }, {
	    "id": "graph2",
	    "balloonText": "<span style='font-size:12px;'>[[title]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	    "bullet": "round",
	    "lineThickness": 3,
	    "bulletSize": 7,
	    "bulletBorderAlpha": 1,
	    "bulletColor": "#FFFFFF",
	    "useLineColorForBulletBorder": true,
	    "bulletBorderThickness": 3,
	    "fillAlphas": 0,
	    "lineAlpha": 1,
	    "title": "평균",
	    "valueField": "average",
	    "dashLengthField": "dashLengthLine"
*/
	  } ],
	  "categoryField": "day",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "axisAlpha": 0,
	    "tickLength": 0
	  },
	  "export": {
	    "enabled": true
	  }
	} );
	
	chart2.addListener("clickGraphItem", handleClick2);
}



function handleClick2(event) {
	
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.submit();
}

function setchart3_1() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart3_1.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart3_1(data);
		}
	});
}

function drawchart3_1(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for(var i=0; i<jsonData.length; i++) {
		var value = {"country":jsonData[i].proc_date, "visits":jsonData[i].cnt};
		dataProvider.push(value);
	}
	
	
	var chart3_1 = AmCharts.makeChart( "chart3_1", {
	  "type": "serial",
	  "theme": "light",
	  "titles": [{
	        "text": "[월별]",
	        "size": 15
	    }],
	  "dataProvider": dataProvider,
	  "valueAxes": [ {
//	    "gridColor": "#FFFFFF",
//	    "gridAlpha": 0.2,
//	    "dashLength": 0,
//	    "minimum": 0,
		"axisAlpha": 0,
	    "title": "개인정보검출(건)"
	  } ],
	  //"gridAboveGraphs": true,
	  "startDuration": 0,
	  "graphs": [ {
	    "balloonText": "[[category]]: <b>[[value]]</b>",
	    "fillAlphas": 1,
	    //"lineAlpha": 0.2,
	    "type": "column",
	    "valueField": "visits",
	    "labelPosition": "bottom",
	    "labelOffset": -1,
	    "labelText": "[[value]]"
	  } ],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "country",
	  "categoryAxis": {
	    /*"gridPosition": "start",
	    "gridAlpha": 0,
	    "tickPosition": "start",
	    "tickLength": 20*/
	    "gridPosition": "start",
	    "axisAlpha": 0,
	    "tickLength": 0
	  },
	  "export": {
	    "enabled": true
	  },
	  "addClassNames": true

	} );
	
	chart3_1.addListener("clickGraphItem", handleClick3_1);
}

function handleClick3_1(event) {
	
	alert("해당 차트는 접속기록 내 처리된 개인정보 유형들의 수량으로\r\n접속기록 건수와는 차이가 있습니다.");
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.submit();
}

function setchart3_2() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart3_2.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date
		},
		success: function(data) {
			drawchart3_2(data);
		}
	});
}

function drawchart3_2(data) {
	var dataProvider = new Array();
	
	/*var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"day": jsonData[i].day, "value": jsonData[i].value, "average": jsonData[i].average};
		dataProvider.push(value);
	}*/
	var chart3_2 = AmCharts.makeChart( "chart3_2", {
	  "type": "serial",
	  "theme": "light",
	  "titles": [{
	        "text": "[요일별]",
	        "size": 15
	    }],
	  "dataProvider": [ {
	    "week": "월",
	    "value": 2025
	  }, {
	    "week": "화",
	    "value": 1882
	  }, {
	    "week": "수",
	    "value": 1809
	  }, {
	    "week": "목",
	    "value": 1322
	  }, {
	    "week": "금",
	    "value": 1122
	  }, {
	    "week": "토",
	    "value": 1114
	  }, {
	    "week": "일",
	    "value": 984
	  }],
	  "valueAxes": [ {
	    "gridColor": "#FFFFFF",
	    "gridAlpha": 0.2,
	    "dashLength": 0
	  } ],
	  "gridAboveGraphs": true,
	  "startDuration": 0,
	  "graphs": [ {
	    "balloonText": "[[category]]: <b>[[value]]</b>",
	    "fillAlphas": 0.8,
	    "lineAlpha": 0.2,
	    "type": "column",
	    "valueField": "value"
	  } ],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "week",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "gridAlpha": 0,
	    "tickPosition": "start",
	    "tickLength": 20
	  },
	  "export": {
	    "enabled": true
	  }

	} );
}

function setchart3_3() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart3_3.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart3_3(data);
		}
	});
}

function drawchart3_3(data) {
	
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for(var i=0; i<jsonData.length; i++) {
		//var value = {"day": jsonData[i].proc_date, "everydayAvg": jsonData[i].cnt1, "weekdayAvg": jsonData[i].cnt2};
		var value = {"day": jsonData[i].proc_date, "everydayAvg": jsonData[i].cnt1, "time": jsonData[i].proc_time};
		dataProvider.push(value);
	}
	
	var chart3_3 = AmCharts.makeChart("chart3_3", {
	    "type": "serial",
	    "theme": "light",
//	    "legend": {
//	        "useGraphSettings": true
//	    },
	    "titles": [{
	        "text": "[시간별(평균)]",
	        "size": 15
	    }],
	    "dataProvider": dataProvider,
	    "valueAxes": [{
//	        "integersOnly": true,
//	        "reversed": false,
//	        "axisAlpha": 0,
//	        "dashLength": 5,
//	        "gridCount": 10,
	        "title": "개인정보검출(건)",
	        "axisAlpha": 0
//	    	"gridColor": "#FFFFFF",
//		    "gridAlpha": 0.2,
//		    "dashLength": 0,
//		    "minimum": 0,
	    }],
	    "startDuration": 0,
	    "graphs": [{
	        //"bullet": "round",
	        "title": "전체평균",
	        "valueField": "everydayAvg",
	        "type": "column",
	        "fillAlphas": 1,
			"labelText": "[[value]]",
		    "labelRotation": -45,
		    "descriptionField": "time"
	    }
//	    , {
//	        "bullet": "round",
//	        "title": "주중평균",
//	        "valueField": "weekdayAvg",
//	        "type": "smoothedLine",
//			"fillAlphas": 0
//			/*"labelText": "[[value]]",
//		    "labelRotation": -45*/
//	    }
	    ],
	    "chartCursor": {
	        "cursorAlpha": 0,
	        "zoomable": false
	    },
	    "categoryField": "day",
	    "categoryAxis": {
	        /*"gridPosition": "start",
	        "axisAlpha": 0,
	        "fillAlpha": 0.05,
	        "fillColor": "#000000",
	        "gridAlpha": 0,
	        "position": "bottom"*/
	    	"gridPosition": "start",
		    "axisAlpha": 0,
		    "tickLength": 0
	    },
	    "export": {
	    	"enabled": true
	     },
		  "addClassNames": true
	});
	
	chart3_3.addListener("clickGraphItem", handleClick3_3);
}


function handleClick3_3(event) {
	
	alert("해당 차트는 접속기록 내 처리된 개인정보 유형들의 수량으로\r\n접속기록 건수와는 차이가 있습니다.");
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	//opener.eventForm.elements["start_h"].value = event.item.description;
	//opener.eventForm.elements["end_h"].value = event.item.description + 1;
	opener.eventForm.submit();
}

function setchart4() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart4.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"period_type" : period_type,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart4(data);
		}
	});
}

function drawchart4(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	var arrData = jsonData["arrData"];
	for(var i=0; i<arrData.length; i++) {
		var value = {
			"team":arrData[i].dept_name,
			"pre":arrData[i].type1,
			"cur": arrData[i].type0,
			"system_seq": arrData[i].system_seq
		};
		dataProvider.push(value);
	}

	var title1 = jsonData["title1"];
	var title2 = jsonData["title2"];

	var chart4 = AmCharts.makeChart( "chart4", {
	  "type": "serial",
	  "rotate": true,
	  "addClassNames": true,
	  "theme": "light",
	  "autoMargins": true,
	  "balloon": {
	    "adjustBorderColor": false,
	    "horizontalPadding": 10,
	    "verticalPadding": 8,
	    "color": "#ffffff"
	  },
	  "legend": {
	        "useGraphSettings": true
	  },

	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "position": "left",
	    "title": "개인정보검출(건)"
	  } ],
	  "startDuration": 0,
	  "graphs": [{
	        "balloonText": "<span style='font-size:12px;'>[[title]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	        "fillAlphas": 0.8,
	        "lineAlpha": 0.2,
		    "title": title2,
	        "type": "column",
	        "valueField": "pre",
	        "labelText": "[[value]]",
	        "labelPosition": "left",
	        "descriptionField": "system_seq"
	    }, {
	        "balloonText": "<span style='font-size:12px;'>[[title]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	        "fillAlphas": 0.8,
	        "lineAlpha": 0.2,
	        "title": title1,
	        "type": "column",
	        "valueField": "cur",
	        "labelText": "[[value]]",
	        "labelPosition": "left",
	        "descriptionField": "system_seq"
	    }],
	  "categoryField": "team",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "axisAlpha": 0,
	    "tickLength": 0,
	    "labelRotation": 45
	  },
	  "export": {
	    "enabled": true
	  }
	} );
	chart4.addListener("clickGraphItem", handleClick4);
}


function handleClick4(event) {
	
	alert("해당 차트는 접속기록 내 처리된 개인정보 유형들의 수량으로\r\n접속기록 건수와는 차이가 있습니다.");
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["system_seq_rp"].value = event.item.description;
	//opener.eventForm.elements["end_h"].value = event.item.description + 1;
	opener.eventForm.submit();
}

//1014
function setchart4_1() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart4_1.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"period_type" : period_type,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart4_1(data);
		}
	});
}

function drawchart4_1(data) {
	
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	var arrData = jsonData["arrData"];
	for(var i=0; i<arrData.length; i++) {
		var value = {"team":arrData[i].dept_name, "pre":arrData[i].type1, "cur": arrData[i].type0, "system_seq": arrData[i].system_seq};
		dataProvider.push(value);
	}

	var title1 = jsonData["title1"];
	var title2 = jsonData["title2"];

	var chart4_1 = AmCharts.makeChart( "chart4_1", {
	  "type": "serial",
	  "rotate": true,
	  "addClassNames": true,
	  "theme": "light",
	  "autoMargins": true,
	  "balloon": {
	    "adjustBorderColor": false,
	    "horizontalPadding": 10,
	    "verticalPadding": 8,
	    "color": "#ffffff"
	  },
	  "legend": {
	        "useGraphSettings": true
	  },

	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "position": "left",
	    "title": "개인정보검출(건)"
	  } ],
	  "startDuration": 0,
	  "graphs": [{
	        "balloonText": "<span style='font-size:12px;'>[[title]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	        "fillAlphas": 0.8,
	        "lineAlpha": 0.2,
		    "title": "전년 "+title1,
	        "type": "column",
	        "valueField": "pre",
	        "labelText": "[[value]]",
	        "labelPosition": "left",
	        "descriptionField": "system_seq"
	    }, {
	        "balloonText": "<span style='font-size:12px;'>[[title]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	        "fillAlphas": 0.8,
	        "lineAlpha": 0.2,
	        "title": title1,
	        "type": "column",
//	        "clustered":false,
//	        "columnWidth":0.5,
	        "valueField": "cur",
	        "labelText": "[[value]]",
	        "labelPosition": "left",
	        "descriptionField": "system_seq"
	    }],
	  "categoryField": "team",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "axisAlpha": 0,
	    "tickLength": 0,
	    "labelRotation": 45
	  },
	  "export": {
	    "enabled": true
	  }
	} );
	
	chart4_1.addListener("clickGraphItem", handleClick4_1);
}


function handleClick4_1(event) {
	
	alert("해당 차트는 접속기록 내 처리된 개인정보 유형들의 수량으로\r\n접속기록 건수와는 차이가 있습니다.");
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["system_seq_rp"].value = event.item.description;
	opener.eventForm.submit();
}

function setchart5() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart5.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"period_type" : period_type,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart5(data);
		}
	});
}

function drawchart5(data) {
	
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	var arrData = jsonData["arrData"];
	for(var i=0; i<arrData.length; i++) {
		var value = {"team":arrData[i].dept_name, "pre":arrData[i].type1, "cur": arrData[i].type0};
		dataProvider.push(value);
	}

	var title1 = jsonData["title1"];
	var title2 = jsonData["title2"];
	
	var chart5 = AmCharts.makeChart("chart5", {
		"type": "serial",
	     "theme": "light",
		"categoryField": "team",
		"rotate": true,
		"startDuration": 0,
		"categoryAxis": {
			"gridPosition": "start",
			"position": "left"
		},
		"legend": {
	        "useGraphSettings": true
		},
		"graphs": [
			{
				"balloonText": "[[title]]:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-2",
				"lineAlpha": 0.2,
				"title": title2,
				"type": "column",
				"valueField": "pre",
				"labelPosition": "left",
				"labelText": "[[value]]"
			},
			{
				"balloonText": "[[title]]:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-1",
				"lineAlpha": 0.2,
				"title": title1,
				"type": "column",
				"valueField": "cur",
				"labelPosition": "left",
				"labelText": "[[value]]"
			}
		],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"position": "bottom",
				"axisAlpha": 0,
				"title": "개인정보검출(건)"
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider":dataProvider,
	    "export": {
	    	"enabled": true
	     },
		  "addClassNames": true

	});
	
	chart5.addListener("clickGraphItem", handleClick5);
}


function handleClick5(event) {
	
	alert("해당 차트는 접속기록 내 처리된 개인정보 유형들의 수량으로\r\n접속기록 건수와는 차이가 있습니다.");
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["dept_name_rp"].value = event.item.category;
	//opener.eventForm.elements["end_h"].value = event.item.description + 1;
	opener.eventForm.submit();
}

function setchart5_1() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart5_1.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"period_type" : period_type,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart5_1(data);
		}
	});
}

function drawchart5_1(data) {
	
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	var arrData = jsonData["arrData"];
	for(var i=0; i<arrData.length; i++) {
		var value = {"team":arrData[i].dept_name, "pre":arrData[i].type1, "cur": arrData[i].type0};
		dataProvider.push(value);
	}

	var title1 = jsonData["title1"];
	var title2 = jsonData["title2"];
	
	var chart5_1 = AmCharts.makeChart("chart5_1", {
		"type": "serial",
	     "theme": "light",
		"categoryField": "team",
		"rotate": true,
		"startDuration": 0,
		"categoryAxis": {
			"gridPosition": "start",
			"position": "left"
		},
		"legend": {
	        "useGraphSettings": true
		},
		"graphs": [
			{
				"balloonText": "[[title]]:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-2",
				"lineAlpha": 0.2,
				"title": "전년 "+title1,
				"type": "column",
				"valueField": "pre",
				"labelPosition": "left",
				"labelText": "[[value]]"
			},
			{
				"balloonText": "[[title]]:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-1",
				"lineAlpha": 0.2,
				"title": title1,
				"type": "column",
				"valueField": "cur",
				"labelPosition": "left",
				"labelText": "[[value]]"
			}
		],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"position": "bottom",
				"axisAlpha": 0,
				"title": "개인정보검출(건)"
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider":dataProvider,
	    "export": {
	    	"enabled": true
	     },
		  "addClassNames": true

	});
	
	chart5_1.addListener("clickGraphItem", handleClick5_1);
}


function handleClick5_1(event) {
	
	alert("해당 차트는 접속기록 내 처리된 개인정보 유형들의 수량으로\r\n접속기록 건수와는 차이가 있습니다.");
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["dept_name_rp"].value = event.item.category;
	//opener.eventForm.elements["end_h"].value = event.item.description + 1;
	opener.eventForm.submit();
}

function setchart6() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart6.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart6(data);
		}
	});
}

function drawchart6(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for(var i=0; i<jsonData.length; i++) {
		var resReq = "";
		switch(jsonData[i].req_type) {
		case "RD":
			resReq = "조회";
			break;
		case "CR":
			resReq = "등록";
			break;
		case "UD":
			resReq = "수정";
			break;
		case "DL":
			resReq = "삭제";
			break;
		case "DN":
			resReq = "다운로드";
			break;
		case "EX":
			resReq = "실행";
			break;
		case "PR":
			resReq = "출력";
			break;
		case "CO":
			resReq = "수집";
			break;
		case "NE":
			resReq = "생성";
			break;
		case "BE":
			resReq = "연계";
			break;
		case "IN":
			resReq = "연동";
			break;
		case "WR":
			resReq = "기록";
			break;
		case "SA":
		case "SV":
			resReq = "저장";
			break;
		case "SU":
			resReq = "보유";
			break;
		case "FI":
			resReq = "가공";
			break;
		case "UP":
			resReq = "편집";
			break;
		case "SC":
			resReq = "검색";
			break;
		case "CT":
			resReq = "정정";
			break;
		case "RE":
			resReq = "복구";
			break;
		case "US":
			resReq = "이용";
			break;
		case "OF":
			resReq = "제공";
			break;
		case "OP":
			resReq = "공개";
			break;
		case "AN":
			resReq = "파기";
			break;
		case "PRRD":
			resReq = "출력(RD)";
			break;
		case "PREX":
			resReq = "출력(Excel)";
			break;
		default:
			resReq = "기타";
			break;
		}
		
		var value = {"req_type": jsonData[i].req_type, "category": resReq, "value": jsonData[i].cnt};
		dataProvider.push(value);
	}
	var chart6 = AmCharts.makeChart("chart6", {
	  "type": "pie",
	  "startDuration": 0,
	   "theme": "light",
	  "addClassNames": true,
	  "legend":{
	   	"position":"right",
	    "marginRight":0,
	    "autoMargins":true,
	    "title": "개인정보검출(건)"
	  },
	  "innerRadius": "30%",
	  "defs": {
	    "filter": [{
	      "id": "shadow",
	      "width": "200%",
	      "height": "200%",
	      "feOffset": {
	        "result": "offOut",
	        "in": "SourceAlpha",
	        "dx": 0,
	        "dy": 0
	      },
	      "feGaussianBlur": {
	        "result": "blurOut",
	        "in": "offOut",
	        "stdDeviation": 5
	      },
	      "feBlend": {
	        "in": "SourceGraphic",
	        "in2": "blurOut",
	        "mode": "normal"
	      }
	    }]
	  },
	  "dataProvider": dataProvider,
	  "valueField": "value",
	  "titleField": "category",
	  "descriptionField": "req_type",
	  "export": {
	    "enabled": true
	  }
	});
	
	chart6.addListener("clickSlice", handleClick6);
}

function handleClick6(event) {
	
	alert("해당 차트는 접속기록 내 처리된 개인정보 유형들의 수량으로\r\n접속기록 건수와는 차이가 있습니다.");
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["req_type_rp"].value = event.dataItem.description;
	opener.eventForm.submit();
}

function setchart7() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart7.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart7(data);
		}
	});
}

function drawchart7(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	var sum = new Number();
	
	for (var i = 0; i < jsonData.length; i++) {
		sum=sum+Number(jsonData[i].value);
	}
	
	for(var i=0; i<jsonData.length; i++) {
		var value = {"result_type":jsonData[i].result_type, "category":jsonData[i].category, "value":jsonData[i].value, "average": sum/jsonData.length};
		dataProvider.push(value);
	}
	var chart7 = AmCharts.makeChart( "chart7", {
	  "type": "serial",
	  "addClassNames": true,
	  "theme": "light",
	  "autoMargins": true,
	  "balloon": {
	    "adjustBorderColor": false,
	    "horizontalPadding": 10,
	    "verticalPadding": 8,
	    "color": "#ffffff"
	  },
	  "legend": {
	        "useGraphSettings": true
	  },

	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "position": "left",
	    "title": "개인정보검출(건)"
	  } ],
	  "startDuration": 0,
	  "graphs": [ {
	    "alphaField": "alpha",
	    "balloonText": "<span style='font-size:12px;'>[[title]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	    "fillAlphas": 1,
	    "title": "개인정보 접속기록",
	    "type": "column",
	    "valueField": "value",
	    "dashLengthField": "dashLengthColumn",
	    "labelText": "[[value]]",
	    "labelPosition": "bottom",
	    "labelOffset": -1,
	    "descriptionField": "result_type"
	  }, {
	    "id": "graph2",
	    "balloonText": "<span style='font-size:12px;'>[[title]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	    "bullet": "round",
	    "lineThickness": 3,
	    "bulletSize": 7,
	    "bulletBorderAlpha": 1,
	    "bulletColor": "#FFFFFF",
	    "useLineColorForBulletBorder": true,
	    "bulletBorderThickness": 3,
	    "fillAlphas": 0,
	    "lineAlpha": 1,
	    "title": "평균",
	    "valueField": "average",
	    "labelPosition": "bottom",
	    "labelOffset": -1,
	    "dashLengthField": "dashLengthLine"
	  } ],
	  "categoryField": "category",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "axisAlpha": 0,
	    "tickLength": 0,
	    "labelRotation": 45
	  },
	  "export": {
	    "enabled": true
	  },
	  "addClassNames": true
	} );
	
	chart7.addListener("clickGraphItem", handleClick7);
}

function handleClick7(event) {
	
	alert("해당 차트는 접속기록 내 처리된 개인정보 유형들의 수량으로\r\n접속기록 건수와는 차이가 있습니다.");
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["privacyType_rp"].value = event.item.description;
	opener.eventForm.submit();
}

function setchart8(){
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart8.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			if(use_studentId == 'yes')
				drawchart8_useStudentId(data);
			else
				drawchart8(data);
		}
	});
}

function setchart8_new(){
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart8_new.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart8_new(data);
		}
	});
}

function drawchart8(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"category": jsonData[i].category, 
				"type1": jsonData[i].type1,
				"type2": jsonData[i].type2,
				"type3": jsonData[i].type3,
				"type4": jsonData[i].type4,
				"type5": jsonData[i].type5,
				"type6": jsonData[i].type6,
				"type7": jsonData[i].type7,
				"type8": jsonData[i].type8,
				"type9": jsonData[i].type9,
				"type10": jsonData[i].type10,
				"type11": jsonData[i].type11,
				"type12": jsonData[i].type12};
		
		dataProvider.push(value);
	}
	
	var chart8 = AmCharts.makeChart("chart8", {
	    "type": "serial",
		"theme": "light",
	    "legend": {
	        "horizontalGap": 1,
	        "maxColumns": 3,
	        "position": "top",
			"useGraphSettings": true,
			"markerSize": 10
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.5,
	        "gridAlpha": 0,
	        "title": "개인정보검출(건)"
	    }],
	    "graphs": [{
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "주민등록번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type1"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "운전면허번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type2"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "여권번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type3"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "신용카드번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type4"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "건강보험번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type5"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "전화번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type6"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "이메일",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type7"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "휴대전화번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type8"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "계좌번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type9"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "외국인등록번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type10"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "환자번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type11"
	    }],
	    "rotate": true,
	    "startDuration": 0,
	    "categoryField": "category",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "gridAlpha": 0,
	        "position": "left"
	    },
	    "export": {
	    	"enabled": true
	     },
		  "addClassNames": true
	});
}

function drawchart8_useStudentId(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"category": jsonData[i].category, 
				"type1": jsonData[i].type1,
				"type2": jsonData[i].type2,
				"type3": jsonData[i].type3,
				"type4": jsonData[i].type4,
				"type5": jsonData[i].type5,
				"type6": jsonData[i].type6,
				"type7": jsonData[i].type7,
				"type8": jsonData[i].type8,
				"type9": jsonData[i].type9,
				"type10": jsonData[i].type10,
				"type99": jsonData[i].type99};
		
		dataProvider.push(value);
	}
	
	var chart8 = AmCharts.makeChart("chart8", {
	    "type": "serial",
		"theme": "light",
	    "legend": {
	        "horizontalGap": 1,
	        "maxColumns": 3,
	        "position": "top",
			"useGraphSettings": true,
			"markerSize": 10
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.5,
	        "gridAlpha": 0,
	        "title": "개인정보검출(건)"
	    }],
	    "graphs": [{
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "주민등록번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type1"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "운전면허번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type2"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "여권번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type3"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "신용카드번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type4"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "건강보험번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type5"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "전화번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type6"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "이메일",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type7"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "휴대전화번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type8"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "계좌번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type9"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "외국인등록번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type10"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "학번",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type99"
	    }],
	    "rotate": true,
	    "startDuration": 0,
	    "categoryField": "category",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "gridAlpha": 0,
	        "position": "left"
	    },
	    "export": {
	    	"enabled": true
	     },
		  "addClassNames": true
	});
}

function drawchart8_new(data) {

	var jsonData = JSON.parse(data);
	var dataProvider = jsonData["dataProvider"];
	var graphs = jsonData["graphs"];
	
	var chart8 = AmCharts.makeChart("chart8", {
	    "type": "serial",
		"theme": "light",
	    "legend": {
	        "horizontalGap": 1,
	        "maxColumns": 3,
	        "position": "top",
			"useGraphSettings": true,
			"markerSize": 10
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.5,
	        "gridAlpha": 0,
	        "title": "개인정보검출(건)"
	    }],
	    "graphs": graphs,
	    "rotate": true,
	    "startDuration": 0,
	    "categoryField": "category",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "gridAlpha": 0,
	        "position": "left"
	    },
	    "export": {
	    	"enabled": true
	     },
		  "addClassNames": true
	});
	
	chart8.addListener("clickGraphItem", handleClick8);
}

function handleClick8(event) {
	
	alert("해당 차트는 접속기록 내 처리된 개인정보 유형들의 수량으로\r\n접속기록 건수와는 차이가 있습니다.");
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["system_seq_rp"].value = event.item.description;
	opener.eventForm.submit();
}

function setchart9(){
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart9.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date
		},
		success: function(data) {
			drawchart9(data);
		}
	});
}

function setchart9_new(){
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart9_new.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart9_new(data);
		}
	});
}

function drawchart9(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"category": jsonData[i].category, 
				"type1": jsonData[i].type1,
				"type2": jsonData[i].type2,
				"type3": jsonData[i].type3,
				"type4": jsonData[i].type4,
				"type5": jsonData[i].type5,
				"type6": jsonData[i].type6,
				"type7": jsonData[i].type7,
				"type8": jsonData[i].type8,
				"type9": jsonData[i].type9,
				"type10": jsonData[i].type10};
		
		dataProvider.push(value);
	}
	
	var chart = AmCharts.makeChart("chart9", {
	    "type": "serial",
		"theme": "light",
	    "legend": {
	        "horizontalGap": 1,
	        "maxColumns": 3,
	        "position": "top",
			"useGraphSettings": true,
			"markerSize": 10
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.5,
	        "gridAlpha": 0,
	        "title": "개인정보검출(건)"
	    }],
	    "graphs": [{
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "주민등록번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type1"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "운전면허번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type2"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "여권번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type3"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "신용카드번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type4"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "건강보험번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type5"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "전화번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type6"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "이메일",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type7"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "휴대전화번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type8"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "계좌번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type9"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "외국인등록번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type10"
	    }],
	    "rotate": true,
	    "categoryField": "category",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "gridAlpha": 0,
	        "position": "left"
	    },
	    "export": {
	    	"enabled": true
	     },
		  "addClassNames": true
	});
}

function drawchart9_new(data) {

	var jsonData = JSON.parse(data);
	var dataProvider = jsonData["dataProvider"];
	var graphs = jsonData["graphs"];
	
	var chart9 = AmCharts.makeChart("chart9", {
	    "type": "serial",
		"theme": "light",
	    "legend": {
	        "horizontalGap": 1,
	        "maxColumns": 3,
	        "position": "top",
			"useGraphSettings": true,
			"markerSize": 10
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.5,
	        "gridAlpha": 0,
	        "title": "개인정보검출(건)"
	    }],
	    "graphs": graphs,
	    "rotate": true,
	    "startDuration": 0,
	    "categoryField": "category",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "gridAlpha": 0,
	        "position": "left"
	    },
	    "export": {
	    	"enabled": true
	     },
		  "addClassNames": true
	});
	
	chart9.addListener("clickGraphItem", handleClick9);
}

function handleClick9(event) {
	
	alert("해당 차트는 접속기록 내 처리된 개인정보 유형들의 수량으로\r\n접속기록 건수와는 차이가 있습니다.");
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["dept_name_rp"].value = event.item.category;
	opener.eventForm.submit();
}

function setchart10(){
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart10.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date
		},
		success: function(data) {
			drawchart10(data);
		}
	});
}

function drawchart10(data) {
var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"category": jsonData[i].category, 
				"type1": jsonData[i].type1,
				"type2": jsonData[i].type2,
				"type3": jsonData[i].type3,
				"type4": jsonData[i].type4,
				"type5": jsonData[i].type5,
				"type6": jsonData[i].type6,
				"type7": jsonData[i].type7,
				"type8": jsonData[i].type8,
				"type9": jsonData[i].type9,
				"type10": jsonData[i].type10};
		
		dataProvider.push(value);
	}
	
	var chart = AmCharts.makeChart("chart10", {
	    "type": "serial",
		"theme": "light",
	    "legend": {
	        "horizontalGap": 1,
	        "maxColumns": 3,
	        "position": "top",
			"useGraphSettings": true,
			"markerSize": 10
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.5,
	        "gridAlpha": 0,
	        "title": "개인정보검출(건)"
	    }],
	    "graphs": [{
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "주민등록번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type1"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "운전면허번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type2"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "여권번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type3"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "신용카드번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type4"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "건강보험번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type5"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "전화번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type6"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "이메일",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type7"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "휴대전화번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type8"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "계좌번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type9"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "외국인등록번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "type10"
	    }],
	    "rotate": true,
	    "categoryField": "category",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "gridAlpha": 0,
	        "position": "left"
	    },
	    "export": {
	    	"enabled": true
	     },
		  "addClassNames": true
	});
}

function setchart10_new(){
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart10_new.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart10_new(data);
		}
	});
}

function drawchart10_new(data) {
	
	var jsonData = JSON.parse(data);
	
	var dataProvider = jsonData["dataProvider"];
	var graphs = jsonData["graphs"];
	
	var chart10 = AmCharts.makeChart("chart10", {
	    "type": "serial",
		"theme": "light",
	    "legend": {
	        "horizontalGap": 1,
	        "maxColumns": 3,
	        "position": "top",
			"useGraphSettings": true,
			"markerSize": 10
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.5,
	        "gridAlpha": 0,
	        "title": "개인정보검출(건)"
	    }],
	    "graphs": graphs,
	    "rotate": true,
	    "startDuration": 0,
	    "categoryField": "category",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "gridAlpha": 0,
	        "position": "left"
	    },
	    "export": {
	    	"enabled": true
	     },
		  "addClassNames": true
	});
	
	chart10.addListener("clickGraphItem", handleClick10);
}

function handleClick10(event) {
	
	alert("해당 차트는 접속기록 내 처리된 개인정보 유형들의 수량으로\r\n접속기록 건수와는 차이가 있습니다.");
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["emp_user_id_rp"].value = event.item.description;
	opener.eventForm.submit();
}

function setchart11_1() {
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart11_1.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			
			drawchart11_1(data);
		}
	});
}

function drawchart11_1(data) {
	
	var dataProvider = new Array();
	
	
	
	var jsonData = JSON.parse(data);
	
	
	for (var i = 0; i < jsonData.length; i++) {
		if(i==0) {
			$("#reportBySys").html(jsonData[i].system_name);
		}
		var value = {"system_seq": jsonData[i].system_seq, "system_name": jsonData[i].system_name, "cnt": jsonData[i].cnt};
		dataProvider.push(value);
	}
	
	var chart11_1 = AmCharts.makeChart( "chart11_1", {
	  "type": "serial",
	  "theme": "light",
	  "dataProvider": dataProvider,
	  "rotate": true,
	  "valueAxes": [ {
		"axisAlpha": 0,
	    "title": "다운로드 (회)"
	  } ],
	  "startDuration": 0,
	  "graphs": [ {
	    "balloonText": "[[category]]: <b>[[value]]</b>",
	    "fillAlphas": 1,
	    "type": "column",
	    "valueField": "cnt",
	    "labelText": "[[value]]",
	    "labelPosition": "left",
	    "descriptionField": "system_seq"
	  } ],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "system_name",
	  "categoryAxis": {
	    "gridPosition": "start",
		"axisAlpha": 0,
		"tickLength": 0,
	    "labelRotation": 45
	  },
	  "export": {
	    "enabled": true
	  }

	} );
	
	//chart11_1.addListener("clickGraphItem", handleClick11_1);
}

/*function handleClick11_1(event) {
	
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["system_seq_rp"].value = event.item.description;
	opener.eventForm.submit();
}
*/
function setchart11_2() {
	
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart11_2.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart11_2(data);
		}
	});
}

function drawchart11_2(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		if(i==0) 
			$("#reportByDept").html(jsonData[i].dept_name);
		var value = {"dept_name": jsonData[i].dept_name,"cnt": jsonData[i].cnt};
		dataProvider.push(value);
	}
	
	var chart11_2 = AmCharts.makeChart( "chart11_2", {
	  "type": "serial",
	  "theme": "light",
	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "title": "다운로드 (회)"
	  } ],
	  "startDuration": 0,
	  "graphs": [ {
	    "balloonText": "[[category]]: <b>[[value]]</b>",
	    "fillAlphas": 1,
	    "type": "column",
	    "valueField": "cnt",
	    "labelText": "[[value]]",
	    "labelPosition": "bottom",
	    "labelOffset": -1,
	    "descriptionField": "dept_id"
	  } ],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "dept_name",
	  "categoryAxis": {
	    "gridPosition": "start",
		"axisAlpha": 0,
		"tickLength": 0,
	    "labelRotation": 0
	  },
	  "export": {
	    "enabled": true
	  }

	} );
	
	//chart11_2.addListener("clickGraphItem", handleClick11_2);
}

/*function handleClick11_2(event) {
	
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["dept_name_rp"].value = event.item.category;
	opener.eventForm.submit();
}*/

function setchart11_3() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/report/reportDetail_chart11_3.html',
		data: { 
			"start_date" : start_date,
			"end_date" : end_date,
			"system_seq" : system_seq
		},
		success: function(data) {
			drawchart11_3(data);
		}
	});
}

function drawchart11_3(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for(var i=0; i<jsonData.length; i++) {
		var resReq = "";
		switch(jsonData[i].result_type) {
		case "1":
			resReq = "주민등록번호";
			break;
		case "2":
			resReq = "운전면허번호";
			break;
		case "3":
			resReq = "여권번호";
			break;
		case "4":
			resReq = "신용카드번호";
			break;
		case "5":
			resReq = "건강보험번호";
			break;
		case "6":
			resReq = "전화번호";
			break;
		case "7":
			resReq = "이메일";
			break;
		case "8":
			resReq = "휴대폰번호";
			break;
		case "9":
			resReq = "계좌번호";
			break;
		case "10":
			resReq = "외국인등록번호";
			break;
		default:
			resReq = "기타";
			break;
		}
		
		var value = {"result_type": jsonData[i].result_type, "category": resReq, "value": jsonData[i].cnt};
		dataProvider.push(value);
	}
	var chart11_3 = AmCharts.makeChart("chart11_3", {
	  "type": "pie",
	  "startDuration": 0,
	   "theme": "light",
	  "addClassNames": true,
	  "legend":{
	   	"position":"right",
	    "marginRight":0,
	    "autoMargins":true,
	    "title": "다운로드 (회)"
	  },
	  "innerRadius": "30%",
	  "defs": {
	    "filter": [{
	      "id": "shadow",
	      "width": "200%",
	      "height": "200%",
	      "feOffset": {
	        "result": "offOut",
	        "in": "SourceAlpha",
	        "dx": 0,
	        "dy": 0
	      },
	      "feGaussianBlur": {
	        "result": "blurOut",
	        "in": "offOut",
	        "stdDeviation": 5
	      },
	      "feBlend": {
	        "in": "SourceGraphic",
	        "in2": "blurOut",
	        "mode": "normal"
	      }
	    }]
	  },
	  "dataProvider": dataProvider,
	  "valueField": "value",
	  "titleField": "category",
	  "descriptionField": "result_type",
	  "export": {
	    "enabled": true
	  }
	});
	
	//chart11_3.addListener("clickSlice", handleClick11_3);
}
/*
function handleClick11_3(event) {
	
	alert("해당 차트는 접속기록 내 처리된 개인정보 유형들의 수량으로\r\n접속기록 건수와는 차이가 있습니다.");
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["result_type_rp"].value = event.dataItem.description;
	opener.eventForm.submit();
}*/

var charts = {}; 
function saveReport() {
	$("#chart1_1").css("display", "none");
	$("#chart1_2").css("display", "none");
	$("#chart2").css("display", "none");
	$("#chart3_1").css("display", "none");
	$("#chart3_3").css("display", "none");
	$("#chart4").css("display", "none");
	$("#chart4_1").css("display", "none");
	$("#chart5").css("display", "none");
	$("#chart5_1").css("display", "none");
	$("#chart6").css("display", "none");
	$("#chart7").css("display", "none");
	$("#chart8").css("display", "none");
	$("#chart9").css("display", "none");
	$("#chart10").css("display", "none");
	
	
	var ids = [];
	if(period_type == 1)
		ids = [ "chart1_1", "chart1_2", "chart2", "chart3_1", "chart3_3", "chart4", "chart4_1", "chart5", "chart5_1", "chart6", "chart7", "chart8", "chart9", "chart10" ];
	else
		ids = [ "chart1_1", "chart1_2", "chart2", "chart3_1", "chart3_3", "chart4", "chart4_1", "chart5", "chart5_1", "chart6", "chart7", "chart8", "chart9", "chart10" ];

	// Collect actual chart objects out of the AmCharts.charts array
	
	var charts_remaining = ids.length;
	for (var i = 0; i < ids.length; i++) {
		for (var x = 0; x < AmCharts.charts.length; x++) {
			if (AmCharts.charts[x].div.id == ids[i]) 
				charts[ids[i]] = AmCharts.charts[x];
		}
	}
	
	// Trigger export of each chart
	for ( var x in charts) {
		if (charts.hasOwnProperty(x)) {
			var chart = charts[x];
			chart["export"].capture({}, function() {
				this.toPNG({}, function(data) {

					this.setup.chart.exportedImage = data;
					
					charts_remaining--;

					if (charts_remaining == 0) {
						for(var ch in charts) {
							var temp_chart = charts[ch];
							imageList[ch] = temp_chart.exportedImage;
						}
						saveAsDoc();
					}

				});
			});
		}
	}
}

function show() {
	for(var x in charts) {
		var chart = charts[x];
		alert(chart.div.id);
	}
}

function goAllLogInqList(req_type) {
	opener.eventForm.elements["search_from_rp"].value = start_date;
	opener.eventForm.elements["search_to_rp"].value = end_date;
	opener.eventForm.elements["req_type_rp"].value = req_type;
	opener.eventForm.submit();
}

