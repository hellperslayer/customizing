/**
 * dashboard (지도가 들어간 버전)
 */

function setTime() { // internal clock//
	var today=new Date();
	//var y=today.getFullYear();
	var M=today.getMonth();
	var d=today.getDate();
	var h=today.getHours();
	var m=today.getMinutes();
	var s=today.getSeconds();
	m = checkTime(m);
	s = checkTime(s);
	M = checkDate(M);
	M = checkTime(M);
	//var time=y+"-"+M+"-"+d+" "+h+":"+m+":"+s;
	$("#con_date").html(M + "/" + d);
	$("#con_time").html(h + ":" + m + ":" + s);
}

function checkTime(i) {
if (i<10) {i = "0" + i};  // add zero in front of numbers < 10 
	return i;
}

function checkDate(i) {
 	i = i+1 ;  // to adjust real month
   	return i;
}

var idx = 0;

$(document).ready(function() {
	//setCharts();
	setTime();
	setMap();
	setInterval(function(){ 
		if(idx >= 15)
			idx = 0;
		else
			idx = idx + 1;
		
		//setCharts();
		
	}, 5000);
	
	setInterval(setTime, 1000);
	// 새로고침
	/*setInterval(function(){ 
		window.location.reload(true);
	}, 600000);*/
});

function setCharts() {
	setChart1();
	setChart2();
	setChart3();
	setChart4();
	setChart5();
	setChart6();
}

function setMap() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_map_init.html',
		data: { 
		},
		success: function(data) {
			drawMap(data);
		}
	});
}

function drawMap(data) {
	var jsonData = JSON.parse(data);
	var mapInfo = jsonData["mapInfo"];
	var deptInfo = jsonData["deptInfo"];
	
	console.log(mapInfo.img_name);
	var url = rootPath + '/resources/dashboard/images/map/' + mapInfo.img_name;
	$("#con_map").css('background-image', 'url("' + url + '")');
	
	/*for(var i=0; i<jsonData.length; i++) {
		var id = "#" + jsonData[i].id;
		var image = rootPath + "/resources/dashboard/images/" + jsonData[i].image;
		$(id).attr("src", image);
	}*/
}

// 개인정보 처리 위험현황
function setChart1() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_map1.html',
		data: { 
			"index":idx
		},
		success: function(data) {
			drawchart1(data);
		}
	});
}

// MAP
function setChart2() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_map2.html',
		data: { 
			"index": idx
		},
		success: function(data) {
			drawchart2(data);
		}
	});
}

// 조직별 위험현황 순위
function setChart3() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_map3.html',
		data: { 
			"index":idx
		},
		success: function(data) {
			drawchart3(data);
		}
	});
}

// 위험유형 유형별 비율
function setChart4() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_map4.html',
		data: { 
			"index":idx
		},
		success: function(data) {
			drawchart4(data);
		}
	});
}

// 월별 위험현황
function setChart5() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_map5.html',
		data: { 
			"index":idx
		},
		success: function(data) {
			drawchart5(data);
		}
	});
}

// 개인별 위험현황 순위
function setChart6() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_map6.html',
		data: { 
			"index": idx
		},
		success: function(data) {
			drawchart6(data);
		}
	});
}

function drawchart1(data) {
	
	data = JSON.parse(data);
	var str;
	var color;
	switch(data["waringStep"]) {
		case 0:
			str = "심각";
			color = "#f28400";
			break;
		case 1:
			str = "경계";
			color="#f9f68f"
			break;
		case 2:
			str = "주의";
			color="#1cd181"
			break;
		case 3:
			str = "관심";
			color="#05c2ce"
			break;
		case 4:
			str = "정상";
			color="#09a0f2"
			break;
	}
	
	$("#con_attention").css("color", color);
	$("#con_attention").html(str);
	
	var name = data["name"];
	$("#con_city").html("부산광역시 - " + name);
}

function drawchart2(data) {
	
	data = JSON.parse(data);
	var waringStep = data["waringStep"];
	
	var str;
	switch(waringStep) {
	case 0:
		str = "심각";
		break;
	case 1:
		str = "경계";
		break;
	case 2:
		str = "주의";
		break;
	case 3:
		str = "관심";
		break;
	case 4:
		str = "정상";
		break;
	}
	var list = data["list"];
	//$("#txtCon").text("개인정보 처리 위험도!!! " + list.length + " 단계\r\n");
	var strRes;
	strRes = "개인정보 처리 위험도 " + str + " 단계\r\n";
	for(var i=0; i<list.length; i++) {
		strRes += "- " + list[i].category + " " + list[i].cnt + " 건\r\n";
	}
	$("#txtCon").text(strRes);
}

function drawchart3(data) {
	data = JSON.parse(data);
	$("#bizTbl").find('tbody').find('tr').remove();
	
	for( var i=0; i<data.length; ++i ) {
		if(i > 11)
			break;
		
		var td = "<td>";
		if(i < 3)
			td = "<td style='color:#F28400;'>";
		$("<tr>").appendTo($("#bizTbl").find('tbody'))
		.append($(td)
			.text(i+1)
		)
		.append($(td)
			.text(data[i].city)
		)
		.append($(td)
			.text(data[i].cnt + " 건")
		)
		.fadeOut()
		.fadeIn("fast");
	}
}

function drawchart4(data) {
	data = JSON.parse(data);
	var dataProvider = new Array();
	var labels = new Array();
	var color = ["#09A9F2", "#05C2CE", "#73D876", "#BBE075", "#F9F68F"];
	var y = ["4%", "13%", "22%", "31%", "40%"];
	var radius = ["100%", "80%", "60%", "40%", "20%"];
	var innerRadius = ["85%", "65%", "45%", "25%", "5%"];
	
	for(var i=0; i<data.length; i++) {
		var value = {
	      "color": "#233745",
	      "startValue": 0,
	      "endValue": 100,
	      "radius": radius[i],
	      "innerRadius": innerRadius[i]
	    }
		dataProvider.push(value);
		value = {
	      "color": color[i],
	      "startValue": 0,
	      "endValue": data[i].cnt,
	      "radius": radius[i],
	      "innerRadius": innerRadius[i],
	      "balloonText": data[i].cnt + "%"
	    }
		dataProvider.push(value);
		value = {
			"text": data[i].category,
		    "x": "49%",
		    "y": y[i],
		    "size": 12,
		    "bold": false,
		    "color": color[i],
		    "align": "right"
		}
		labels.push(value);
	}
	var gaugeChart = AmCharts.makeChart("con03_box", {
		  "type": "gauge",
		  "theme": "dark",
		  "axes": [{
		    "axisAlpha": 0,
		    "tickAlpha": 0,
		    "labelsEnabled": false,
		    "startValue": 0,
		    "endValue": 100,
		    "startAngle": 0,
		    "endAngle": 270,
		    "bands": dataProvider
		  }],
		  "allLabels": labels,
		  "export": {
		    "enabled": false
		  }
		});
}

function drawchart5(data) {
	data = JSON.parse(data);
	var dataProvider = new Array();
	
	for(var i=0; i<data.length; i++) {
		var value = {"month": data[i].month, "cnt": data[i].cnt, "color": data[i].color};
		dataProvider.push(value);
	}
	var chart = AmCharts.makeChart("con03_box2", {
		  "type": "serial",
		  "theme": "light",
		  "marginRight": 70,
		  "dataProvider": dataProvider,
		  "color": "#A1B0B7",
		  "valueAxes": [{
		    "axisAlpha": 0,
		    "position": "left",
		    "title": "단위 (건수)",
		    "gridColor": "#A1B0B7",
		    "gridAlpha": 0.3
		  }],
		  "startDuration": 1,
		  "graphs": [{
		    "balloonText": "<b>[[category]]: [[value]]</b>",
		    "fillColorsField": "color",
		    "fillAlphas": 0.9,
		    "lineAlpha": 0.2,
		    "type": "column",
		    "valueField": "cnt"
		  }],
		  "chartCursor": {
		    "categoryBalloonEnabled": false,
		    "cursorAlpha": 0,
		    "zoomable": false
		  },
		  "categoryField": "month",
		  "categoryAxis": {
		    "gridPosition": "start",
		    "labelRotation": 0,
		    "gridColor": "#A1B0B7",
		    "gridAlpha": 0.3
		  },
		  "export": {
		    "enabled": false
		  }

		});
}

function drawchart6(data) {
	data = JSON.parse(data);
	$("#bizTbl2").find('tbody').find('tr').remove();
	
	for( var i=0; i<data.length; ++i ) {
		var td = "<td>";
		if(i < 3)
			td = "<td style='color:#F28400;'>";
		
		$("<TR>").appendTo($("#bizTbl2").find('tbody'))	
		.append($(td)
			.text(i+1)
		)
		.append($(td)
			.text(data[i].name)
		)
		.append($(td)
			.text(data[i].dept)
		)
		.append($(td)
			.text(data[i].sum)
		)
		.append($(td)
			.text(data[i].cnt1)
		)
		.append($(td)
			.text(data[i].cnt2)
		)
		.append($(td)
			.text(data[i].cnt3)
		)
		.fadeOut()
		.fadeIn("fast");
	}
}

