

$(document).ready(function() {
	setchart1();
	setchart2();
	setchart3();
	setchart4();
	setchart5();
	setchart6();
	setchart7();
	setchart8();
	setchart9();
	setchart10();
	setchart11();
	setchart12();
	setchart13();
});


function setchart1 () {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/new_dashboard_chart1.html',
		data: { 
		},
		success: function(data) {
			drawchart1(data);
		}
	});
}

function setchart2 () {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/new_dashboard_chart2.html',
		data: { 
		},
		success: function(data) {
			drawchart2(data);
		}
	});
}

function setchart3 () {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/new_dashboard_chart3.html',
		data: { 
		},
		success: function(data) {
			drawchart3(data);
		}
	});
}

function setchart4 () {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/new_dashboard_chart4.html',
		data: { 
		},
		success: function(data) {
			drawchart4(data);
		}
	});
}

function setchart5 () {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/new_dashboard_chart5.html',
		data: { 
		},
		success: function(data) {
			drawchart5(data);
		}
	});
}

function setchart6 () {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/new_dashboard_chart6.html',
		data: { 
		},
		success: function(data) {
			drawchart6(data);
		}
	});
}

function setchart7 () {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/new_dashboard_chart7.html',
		data: { 
		},
		success: function(data) {
			drawchart7(data);
		}
	});
}

function setchart8 () {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/new_dashboard_chart8.html',
		data: { 
		},
		success: function(data) {
			drawchart8(data);
		}
	});
}

function setchart9 () {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/new_dashboard_chart9.html',
		data: { 
		},
		success: function(data) {
			drawchart9(data);
		}
	});
}

function setchart10 () {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/new_dashboard_chart10.html',
		data: { 
		},
		success: function(data) {
			drawchart10(data);
		}
	});
}

function setchart11 () {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/new_dashboard_chart11.html',
		data: { 
		},
		success: function(data) {
			drawchart11(data);
		}
	});
}

function setchart12 () {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/new_dashboard_chart12.html',
		data: { 
		},
		success: function(data) {
			drawchart12(data);
		}
	});
}

function setchart13 () {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/new_dashboard_chart13.html',
		data: { 
		},
		success: function(data) {
			drawchart13(data);
		}
	});
}















function drawchart1 (data) {
	
	var dataProvider = new Array();
	var graphs = new Array();
	
	var jsonData = JSON.parse(data);
	
	var  valueTemp="";
	var  nameTemp="";
	
	for (var i = 0; i < jsonData.length; i++) {
		dataProvider.push(jsonData[i].dataProvider);
		
	}
	
	if ( jsonData.length > 0) {
		var graphsTemp = jsonData[0].graphs;
		
		for (var j = 0; j < graphsTemp.length; j++) {
			graphs.push(graphsTemp[j]);
		}
	}
	
	/*dashboard_amchart_1*/
	var chart1 = AmCharts.makeChart("dashboard_amchart_1", {
	    "type": "serial",
	    "theme": "light",
	    "legend": {
	        "useGraphSettings": true
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "integersOnly": true,
	        "reversed": true,
	        "axisAlpha": 0,
	        "dashLength": 5,
	        "gridCount": 10,
	        "position": "left",
	        "title": "개인정보검출(건)"
	    }],
	    "startDuration": 0.5,
	    "graphs": graphs,
	    "chartCursor": {
	        "cursorAlpha": 0,
	        "zoomable": false
	    },
	    "categoryField": "proc_date",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "fillAlpha": 0.05,
	        "fillColor": "#000000",
	        "gridAlpha": 0,
	        "position": "top"
	    },
	    "export": {
	    	"enabled": false,
	        "position": "bottom-right"
	     }
	});
}

function drawchart2 (data) {
	
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"emp_user_name": jsonData[i].emp_user_name,"nPrivCount": jsonData[i].nPrivCount };
		dataProvider.push(value);
	}
	
	var chart2 = AmCharts.makeChart("dashboard_amchart_2", {
		"theme": "light",
	    "type": "serial",
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "stackType": "3d",
	        "unit": "",
	        "position": "left",
	        "title": "개인정보검출(건)",
	    }],
	    "startDuration": 1,
	    "graphs": [{
	        "balloonText": "<b>[[value]]</b>",
	        "fillAlphas": 0.9,
	        "lineAlpha": 0.2,
	        "title": "",
	        "type": "column",
	        "valueField": "nPrivCount"
	    }],
	    "plotAreaFillAlphas": 0.1,
	    "depth3D": 60,
	    "angle": 30,
	    "categoryField": "emp_user_name",
	    "categoryAxis": {
	        "gridPosition": "start"
	    },
	    "export": {
	    	"enabled": false
	     }
	});
	jQuery('.chart-input').off().on('input change',function() {
		var property	= jQuery(this).data('property');
		var target		= chart;
		chart.startDuration = 0;

		if ( property == 'topRadius') {
			target = chart.graphs[0];
	      	if ( this.value == 0 ) {
	          this.value = undefined;
	      	}
		}

		target[property] = this.value;
		chart.validateNow();
	});

}

function drawchart3 (data) {
	
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var name = jsonData[i].system_name;
		if ( name != null && name.length > 5 ) name= name.substring(0,5)+"...";
		
		var value = {"system_name": name,"priv1": jsonData[i].priv1,"priv2": jsonData[i].priv2,"priv3": jsonData[i].priv3,"priv4": jsonData[i].priv4,"priv5": jsonData[i].priv5,"priv6": jsonData[i].priv6,"priv7": jsonData[i].priv7,"priv8": jsonData[i].priv8,"priv9": jsonData[i].priv9,"priv10": jsonData[i].priv10 };
		dataProvider.push(value);
	}
	
	/*dashboard_amchart_3*/
	var chart3 = AmCharts.makeChart("dashboard_amchart_3", {
	    "type": "serial",
		"theme": "light",
	    "legend": {
	        "horizontalGap": 10,
	        "maxColumns": 1,
	        "position": "right",
			"useGraphSettings": true,
			"markerSize": 10
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "stackType": "regular",
	        "axisAlpha": 0.3,
	        "gridAlpha": 0
	    }],
	    "graphs": [{
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "주민등록번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "priv1"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "운전면허번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "priv2"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "여권번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "priv3"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "신용카드번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "priv4"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "건강보험번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "priv5"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "전화번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "priv6"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "이메일",
	        "type": "column",
			"color": "#000000",
	        "valueField": "priv7"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "휴대전화번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "priv8"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "계좌번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "priv9"
	    }, {
	        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "labelText": "[[value]]",
	        "lineAlpha": 0.3,
	        "title": "외국인등록번호",
	        "type": "column",
			"color": "#000000",
	        "valueField": "priv10"
	    }],
	    "categoryField": "system_name",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "gridAlpha": 0,
	        "position": "left"
	    },
	    "export": {
	    	"enabled": false
	     }

	});
}

function drawchart4 (data) {

	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"privacy_desc": jsonData[i].privacy_desc,"nPrivCount": jsonData[i].nPrivCount };
		dataProvider.push(value);
	}
	
	
	var chart4 = AmCharts.makeChart( "dashboard_amchart_4", {
		  "type": "radar",
		  "theme": "light",
		  "dataProvider": dataProvider,
		  "valueAxes": [ {
		    "axisTitleOffset": 20,
		    "minimum": 0,
		    "axisAlpha": 0.15
		  } ],
		  "startDuration": 2,
		  "graphs": [ {
		    "balloonText": "[[category]]: [[value]]",
		    "bullet": "round",
		    "lineThickness": 2,
		    "valueField": "nPrivCount"
		  } ],
		  "categoryField": "privacy_desc",
		  "export": {
		    "enabled": false
		  }
		} );
}

function drawchart5 (data) {
	
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"proc_date": jsonData[i].proc_date,"priv1": jsonData[i].priv1,"priv2": jsonData[i].priv2,"priv3": jsonData[i].priv3,"priv4": jsonData[i].priv4,"priv5": jsonData[i].priv5,"priv6": jsonData[i].priv6,"priv7": jsonData[i].priv7,"priv8": jsonData[i].priv8,"priv9": jsonData[i].priv9,"priv10": jsonData[i].priv10 };
		dataProvider.push(value);
	}
	
	
	/*dashboard_amchart_5*/
	var chart5 = AmCharts.makeChart("dashboard_amchart_5", {
	    "type": "serial",
	    "theme": "light",
	    "marginRight":10,
	    "legend": {
	        "equalWidths": false,
	        "periodValueText": "",
	        "position": "right",
	        "valueAlign": "left",
	        "valueWidth": 50
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "stackType": "regular",
	        "gridAlpha": 0.2,
	        "position": "left",
	        "title": "개인정보검출(건)"
	    }],
	    "graphs": [{
	    	"balloonText": "<b>[[value]]</b></span>","fillAlphas": 0.6,
	        "lineAlpha": 0.2,
	        "title": "주민등록번호",
	        "valueField": "priv1"
	    }, {
	    	"balloonText": "<b>[[value]]</b></span>","fillAlphas": 0.6,
	        "lineAlpha": 0.2,
	        "title": "운전면허번호",
	        "valueField": "priv2"
	    }, {
	    	"balloonText": "<b>[[value]]</b></span>","fillAlphas": 0.6,
	        "lineAlpha": 0.2,
	        "title": "여권번호",
	        "valueField": "priv3"
	    }, {
	    	"balloonText": "<b>[[value]]</b></span>","fillAlphas": 0.6,
	        "lineAlpha": 0.2,
	        "title": "신용카드번호",
	        "valueField": "priv4"
	    }, {
	    	"balloonText": "<b>[[value]]</b></span>","fillAlphas": 0.6,
	        "lineAlpha": 0.2,
	        "title": "건강보험번호",
	        "valueField": "priv5"
	    }, {
	    	"balloonText": "<b>[[value]]</b></span>","fillAlphas": 0.6,
	        "lineAlpha": 0.2,
	        "title": "전화번호",
	        "valueField": "priv6"
	    }, {
	    	"balloonText": "<b>[[value]]</b></span>","fillAlphas": 0.6,
	        "lineAlpha": 0.2,
	        "title": "이메일",
	        "valueField": "priv7"
	    }, {
	    	"balloonText": "<b>[[value]]</b></span>","fillAlphas": 0.6,
	        "lineAlpha": 0.2,
	        "title": "휴대전화번호",
	        "valueField": "priv8"
	    }, {
	    	"balloonText": "<b>[[value]]</b></span>","fillAlphas": 0.6,
	        "lineAlpha": 0.2,
	        "title": "계좌번호",
	        "valueField": "priv9"
	    }, {
	    	"balloonText": "<b>[[value]]</b></span>","fillAlphas": 0.6,
	        "lineAlpha": 0.2,
	        "title": "외국인등록번호",
	        "valueField": "priv10"
	    }],
	    "plotAreaBorderAlpha": 0,
	    "marginTop": 10,
	    "marginLeft": 0,
	    "marginBottom": 0,
	    "chartScrollbar": {},
	    "chartCursor": {
	        "cursorAlpha": 0
	    },
	    "categoryField": "proc_date",
	    "categoryAxis": {
	        "startOnAxis": true,
	        "axisColor": "#DADADA",
	        "gridAlpha": 0.07,
	        "title": "날짜",
	        "guides": [{
	            category: "2001",
	            toCategory: "2003",
	            lineColor: "#CC0000",
	            lineAlpha: 1,
	            fillAlpha: 0.2,
	            fillColor: "#CC0000",
	            dashLength: 2,
	            inside: true,
	            labelRotation: 90,
	            label: "fines for speeding increased"
	        }, {
	            category: "2007",
	            lineColor: "#CC0000",
	            lineAlpha: 1,
	            dashLength: 2,
	            inside: true,
	            labelRotation: 90,
	            label: "motorcycle fee introduced"
	        }]
	    },
	    "export": {
	    	"enabled": false
	     }
	});
}

function drawchart6 (data) {
	
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"dept_name": jsonData[i].dept_name,"nPrivCount": jsonData[i].nPrivCount};
		dataProvider.push(value);
	}
	
	/*dashboard_amchart_6*/
	var chart6 = AmCharts.makeChart("dashboard_amchart_6", {
		"type": "serial",
	     "theme": "light",
		"categoryField": "dept_name",
		"rotate": true,
		"startDuration": 1,
		"categoryAxis": {
			"gridPosition": "start",
			"position": "left"
		},
		"trendLines": [],
		"graphs": [
			{
				"balloonText": "[[category]]: [[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-1",
				"lineAlpha": 0.2,
				"title": "개인정보검출(건)",
				"type": "column",
				"valueField": "nPrivCount"
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"position": "top",
				"axisAlpha": 0
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider": dataProvider,
	    "export": {
	    	"enabled": false
	     }

	});
}

function drawchart7 (data) {
	
	var dataProvider = new Array();
	var graphs = new Array();
	
	var jsonData = JSON.parse(data);
	
	var  valueTemp="";
	var  nameTemp="";
	
	for (var i = 0; i < jsonData.length-1; i++) {
		dataProvider.push(jsonData[i]);
		
	}
	
	if ( jsonData.length > 0) {
		var graphsTemp = jsonData[jsonData.length-1];
		
		for (var j = 0; j < graphsTemp.length; j++) {
			graphs.push(graphsTemp[j]);
		}
	}
	
	/*dashboard_amchart_7*/
	var chart7 = AmCharts.makeChart("dashboard_amchart_7", {
	    "type": "xy",
	    "theme": "light",
	    "marginRight": 80,
	    "startDuration": 1.5,
	    "trendLines": [],
	    "balloon": {
	        "adjustBorderColor": false,
	        "shadowAlpha": 0,
	        "fixedPosition":true
	    },
	    "graphs": graphs,
	    "valueAxes": [{
	        "id": "ValueAxis-1",
	        "axisAlpha": 0
	    }, {
	        "id": "ValueAxis-2",
	        "axisAlpha": 0,
	        "position": "bottom"
	    }],
	    "allLabels": [],
	    "titles": [],
	    "dataProvider": dataProvider,

	    "export": {
	        "enabled": false
	    },

	    "chartScrollbar": {
	        "offset": 15,
	        "scrollbarHeight": 5
	    },

	    "chartCursor":{
	       "pan":true,
	       "cursorAlpha":0,
	       "valueLineAlpha":0
	    }
	});
}

function drawchart8 (data) {
	var dataProvider = new Array();
	var graphs = new Array();
	
	var jsonData = JSON.parse(data);
	
	var  valueTemp="";
	var  nameTemp="";
	
	for (var i = 0; i < jsonData.length; i++) {
		dataProvider.push(jsonData[i].dataProvider);
		
	}
	
	if ( jsonData.length > 0) {
		var graphsTemp = jsonData[0].graphs;
		
		for (var j = 0; j < graphsTemp.length; j++) {
			graphs.push(graphsTemp[j]);
		}
	}
	
	var chart1 = AmCharts.makeChart("dashboard_amchart_8", {
	    "type": "serial",
	    "theme": "light",
	    "legend": {
	        "useGraphSettings": true
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "integersOnly": true,
	        "reversed": true,
	        "axisAlpha": 0,
	        "dashLength": 5,
	        "gridCount": 10,
	        "position": "left",
	        "title": "개인정보검출(건)"
	    }],
	    "startDuration": 0.5,
	    "graphs": graphs,
	    "chartCursor": {
	        "cursorAlpha": 0,
	        "zoomable": false
	    },
	    "categoryField": "proc_date",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "axisAlpha": 0,
	        "fillAlpha": 0.05,
	        "fillColor": "#000000",
	        "gridAlpha": 0,
	        "position": "top"
	    },
	    "export": {
	    	"enabled": false,
	        "position": "bottom-right"
	     }
	});
}

function drawchart9 (data) {
	
	var dataProvider = new Array();
	var graphs = new Array();
	
	var jsonData = JSON.parse(data);
	
	var  valueTemp="";
	var  nameTemp="";
	
	for (var i = 0; i < jsonData.length-1; i++) {
		dataProvider.push(jsonData[i]);
		
	}
	
	if ( jsonData.length > 0) {
		var graphsTemp = jsonData[jsonData.length-1];
		
		for (var j = 0; j < graphsTemp.length; j++) {
			graphs.push(graphsTemp[j]);
		}
	}
	
	/*dashboard_amchart_9*/
	var chart9 = AmCharts.makeChart("dashboard_amchart_9", {
		"type": "serial",
		"theme": "light",
	    "titles": [{
	        "text": "",
	        "size": 15
	    }],
	    "legend": {
	        "align": "center",
	        "equalWidths": false,
	        "periodValueText": "total: [[value.sum]]",
	        "valueAlign": "left",
	        "valueText": "[[value]] ([[percents]]%)",
	        "valueWidth": 100
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "stackType": "100%",
	        "gridAlpha": 0.07,
	        "position": "left",
	        "title": "percent"
	    }],
	    "graphs": graphs,
	    "plotAreaBorderAlpha": 0,
	    "marginLeft": 0,
	    "marginBottom": 0,
	    "chartCursor": {
	        "cursorAlpha": 0,
	        "zoomable": false
	    },
	    "categoryField": "year",
	    "categoryAxis": {
	        "startOnAxis": true,
	        "axisColor": "#DADADA",
	        "gridAlpha": 0.07
	    },
	    "export": {
	    	"enabled": false
	     }
	});
}

function drawchart10 (data) {
	
	/*dashboard_amchart_10*/
	var dataProvider = new Array();
	var graphs = new Array();
	
	var jsonData = JSON.parse(data);
	
	var  valueTemp="";
	var  nameTemp="";
	
	for (var i = 0; i < jsonData.length-1; i++) {
		dataProvider.push(jsonData[i]);
		
	}
	
	if ( jsonData.length > 0) {
		var graphsTemp = jsonData[jsonData.length-1];
		
		for (var j = 0; j < graphsTemp.length; j++) {
			graphs.push(graphsTemp[j]);
		}
	}
	
	/*dashboard_amchart_7*/
	var chart10 = AmCharts.makeChart("dashboard_amchart_10", {
	    "type": "xy",
	    "theme": "light",
	    "marginRight": 80,
	    "startDuration": 1.5,
	    "trendLines": [],
	    "balloon": {
	        "adjustBorderColor": false,
	        "shadowAlpha": 0,
	        "fixedPosition":true
	    },
	    "graphs": graphs,
	    "valueAxes": [{
	        "id": "ValueAxis-1",
	        "axisAlpha": 0
	    }, {
	        "id": "ValueAxis-2",
	        "axisAlpha": 0,
	        "position": "bottom"
	    }],
	    "allLabels": [],
	    "titles": [],
	    "dataProvider": dataProvider,

	    "export": {
	        "enabled": false
	    },

	    "chartScrollbar": {
	        "offset": 15,
	        "scrollbarHeight": 5
	    },

	    "chartCursor":{
	       "pan":true,
	       "cursorAlpha":0,
	       "valueLineAlpha":0
	    }
	});
	
}
	
function drawchart11 (data) {
	
	var dataProvider = new Array();
	var graphs = new Array();
	
	var jsonData = JSON.parse(data);
	
	var  valueTemp="";
	var  nameTemp="";
	
	for (var i = 0; i < jsonData.length-1; i++) {
		dataProvider.push(jsonData[i].dataProvider);
		
	}
	
	if ( jsonData.length > 0) {
		var graphsTemp = jsonData[jsonData.length-1];
		
		for (var j = 0; j < graphsTemp.length; j++) {
			graphs.push(graphsTemp[j]);
		}
	}
	
	/*dashboard_amchart_11*/
	var chart11 = AmCharts.makeChart("dashboard_amchart_11", {
		"theme": "light",
	    "type": "serial",
	    "dataProvider": dataProvider,


	    "categoryField": "continent",

	    "categoryAxis": {
	        "gridAlpha": 0.1,
	        "axisAlpha": 0,
	        "widthField": "",
	        "gridPosition": "start"
	    },

	    "valueAxes": [{
	        "stackType": "100% stacked",
	        "gridAlpha": 0.1,
	        "unit": "",
	        "axisAlpha": 0
	    }],

	    "graphs": graphs,

	    "legend": {},
	    "export": {
	    	"enabled": false
	     }
	});
}

function drawchart12 (data) {
	
	var dataProvider = new Array();
	var color = ["#FF0F00", "#FF6600", "#FF9E01", "#FCD202", "#F8FF01"];
	
	var jsonData = JSON.parse(data);
	
	var info="";
	
	
	for (var i = 0; i < jsonData.length; i++) {
		var t = i%5;
		
		var value = {"data1": jsonData[i].data1,"data2": jsonData[i].data2,"nPrivCount": jsonData[i].nPrivCount,"color": color[t]};
		dataProvider.push(value);
		
		if ( jsonData[i].data1 == 1101 ) info=info+"1101 : 1명이 동일한 주민등록번호를 여러번 처리한 경우(임계치 이상)\r\n\r\n";
		else if ( jsonData[i].data1 == 1103 )info=info+"1103 : 1명이 동일한 여권번호를 여러번 처리한 경우(임계치 이상)\r\n\r\n";
		else if ( jsonData[i].data1 == 1104 )info=info+"1104 : 1명이 동일한 계좌번호를 여러번 처리한 경우(임계치 이상)\r\n\r\n";
		else if ( jsonData[i].data1 == 1201 )info=info+"1201 : 여러명이 동일한 주민등록번호를 임계치 이상 처리한 경우\r\n\r\n";
		else if ( jsonData[i].data1 == 1202 )info=info+"1202 : 여러명이 동일한 운전면허번호를 임계치 이상 처리한 경우\r\n\r\n";
		else if ( jsonData[i].data1 == 1203 )info=info+"1203 : 여러명이 동일한 여권번호를 임계치 이상 처리한 경우\r\n\r\n";
		else if ( jsonData[i].data1 == 1204 )info=info+"1204 : 여러명이 동일한 계좌번호를 임계치 이상 처리한 경우\r\n\r\n";
		else if ( jsonData[i].data1 == 1205 )info=info+"1205 : 여러명이 동일한 신용카드번호를 임계치 이상 처리한 경우\r\n\r\n";
		else if ( jsonData[i].data1 == 3003 )info=info+"3003 : 업무시간을 제외한 특정시간대에 개인정보를 처리한 경우\r\n\r\n";
		else if ( jsonData[i].data1 == 4001 )info=info+"4001 : 동일한 ID가 2개 이상의 다른 IP에서 접속된 경우\r\n\r\n";
		else if ( jsonData[i].data1 == 4002 )info=info+"4002 : 하나의 IP에서 여러개의 다른 ID로 접속한 경우\r\n\r\n";
	}
	
	/*dashboard_amchart_12*/
	var chart12 = AmCharts.makeChart("dashboard_amchart_12", {
		  "type": "serial",
		  "theme": "light",
		  "marginRight": 70,
		  "dataProvider": dataProvider,
		  "valueAxes": [{
		    "axisAlpha": 0,
		    "position": "left",
		    "title": "추출검출(건)"
		  }],
		  "startDuration": 1,
		  "graphs": [{
		    "balloonText": "<b>"+info+"[[value]]건</b>",
		    "fillColorsField": "color",
		    "fillAlphas": 0.9,
		    "lineAlpha": 0.2,
		    "type": "column",
		    "valueField": "nPrivCount"
		  }],
		  "chartCursor": {
		    "categoryBalloonEnabled": false,
		    "cursorAlpha": 0,
		    "zoomable": false
		  },
		  "categoryField": "data1",
		  "categoryAxis": {
		    "gridPosition": "start",
		    "labelRotation": 45
		  },
		  "export": {
		    "enabled": false
		  }

		});
	
}

function drawchart13 (data) {
var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"proc_date": jsonData[i].proc_date,"nPrivCount": jsonData[i].nPrivCount};
		dataProvider.push(value);
	}
	
	
	/*dashboard_amchart_5*/
	var chart5 = AmCharts.makeChart("dashboard_amchart_13", {
	    "type": "serial",
	    "theme": "light",
	    "marginRight":10,
	    "legend": {
	        "equalWidths": false,
	        "periodValueText": "",
	        "position": "right",
	        "valueAlign": "left",
	        "valueWidth": 50
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "stackType": "regular",
	        "gridAlpha": 0.2,
	        "position": "left",
	        "title": "개인정보검출(건)"
	    }],
	    "graphs": [{
	    	"balloonText": "<b>[[value]]</b></span>","fillAlphas": 0.6,
	        "lineAlpha": 0.2,
	        "title": "개인정보검출",
	        "valueField": "nPrivCount"
	    }],
	    "plotAreaBorderAlpha": 0,
	    "marginTop": 10,
	    "marginLeft": 0,
	    "marginBottom": 0,
	    "chartScrollbar": {},
	    "chartCursor": {
	        "cursorAlpha": 0
	    },
	    "categoryField": "proc_date",
	    "categoryAxis": {
	        "startOnAxis": true,
	        "axisColor": "#DADADA",
	        "gridAlpha": 0.07,
	        "title": "날짜",
	        "guides": [{
	            category: "2001",
	            toCategory: "2003",
	            lineColor: "#CC0000",
	            lineAlpha: 1,
	            fillAlpha: 0.2,
	            fillColor: "#CC0000",
	            dashLength: 2,
	            inside: true,
	            labelRotation: 90,
	            label: "fines for speeding increased"
	        }, {
	            category: "2007",
	            lineColor: "#CC0000",
	            lineAlpha: 1,
	            dashLength: 2,
	            inside: true,
	            labelRotation: 90,
	            label: "motorcycle fee introduced"
	        }]
	    },
	    "export": {
	    	"enabled": false
	     }
	});
}
		
