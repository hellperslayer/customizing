
$(document).ready(function() {
	setCharts();
	
	setInterval(function(){ 
		setchart3();
		setchart5();
		setchart6();
		setchart7();
		setchart8();
		setchart9();
		setchart10();
		setchart11();
		setchart12();
		setchart13();
		setchart14();
		setchart15();
		setchart16();
		setchart17();
		setchart18();
		setchart19();
		setchart20();
		
		}, 10000);
	
	setInterval(function(){ 
		setchart2();
		setchart4();
		}, 1000);
	
	setInterval(function(){ 
		window.location.reload(true);
		}, 600000);
});

function setCharts() {
	setchart1();
	setchart2();
	setchart3();
	setchart4();
	setchart5();
	setchart6();
	setchart7();
	setchart8();
	setchart9();
	setchart10();
	setchart11();
	setchart12();
	setchart13();
	setchart14();
	setchart15();	
	setchart16();
	setchart17();
	setchart18();
	setchart19();
	setchart20();
}

//------------------------------------------------------
function setchart1() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chartDemo1.html',
		data: { 
		},
		success: function(data) {
			drawchart1(data);
		}
	});
}

function setchart2() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chartDemo2.html',
		data: { 
		},
		success: function(data) {
			drawchart2(data);
		}
	});
}

function setchart3() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chartDemo3.html',
		data: { 
		},
		success: function(data) {
			drawchart3_new(data);
		}
	});
}

function setchart4() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chartDemo4.html',
		data: { 
		},
		success: function(data) {
			drawchart4(data);
		}
	});
}

function setchart5() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chartDemo5.html',
		data: { 
		},
		success: function(data) {
			drawchart5(data);
		}
	});
}

function setchart6() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chartDemo6.html',
		data: { 
		},
		success: function(data) {
			drawchart6(data);
		}
	});
}

function setchart7() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chartDemo7.html',
		data: { 
		},
		success: function(data) {
			drawchart7(data);
		}
	});
}

function setchart8() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chartDemo8.html',
		data: { 
		},
		success: function(data) {
			drawchart8(data);
		}
	});
}

function setchart9() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chartDemo9.html',
		data: { 
		},
		success: function(data) {
			drawchart9(data);
		}
	});
}

function setchart10() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chartDemo10.html',
		data: { 
		},
		success: function(data) {
			drawchart10(data);
		}
	});
}

function setchart11() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chartDemo11.html',
		data: { 
		},
		success: function(data) {
			drawchart11(data);
		}
	});
}

function setchart12() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chartDemo12.html',
		data: { 
		},
		success: function(data) {
			drawchart12(data);
		}
	});
}

function setchart13() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chartDemo13.html',
		data: { 
		},
		success: function(data) {
			drawchart13(data);
		}
	});
}

function setchart14() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chartDemo14.html',
		data: { 
		},
		success: function(data) {
			drawchart14(data);
		}
	});
}

function setchart15() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chartDemo15.html',
		data: { 
		},
		success: function(data) {
			drawchart15(data);
		}
	});
}

function setchart16() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chartDemo16.html',
		data: { 
		},
		success: function(data) {
			drawchart16(data);
		}
	});
}

function setchart17() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chartDemo17.html',
		data: { 
		},
		success: function(data) {
			drawchart17(data);
		}
	});
}

function setchart18() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chartDemo18.html',
		data: { 
		},
		success: function(data) {
			drawchart18(data);
		}
	});
}

function setchart19() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chartDemo19.html',
		data: { 
		},
		success: function(data) {
			drawchart19(data);
		}
	});
}

function setchart20() {
	$.ajax({
		type: 'POST',
		url: rootPath + '/dashboard/dashboard_chartDemo20.html',
		data: { 
		},
		success: function(data) {
			drawchart20(data);
		}
	});
}


//------------------------------------------------------
function drawchart1(data) {
	var dataProvider = new Array();
	var jsonData = JSON.parse(data);
	
	for(var i=0; i<jsonData.length; i++) {
		var value = {"category": jsonData[i].category, "segments": jsonData[i].segments};
		dataProvider.push(value);
	}
	
	var chart1 = AmCharts.makeChart( "dashboard_amchart_1", {
	  "type": "gantt",
	  "theme": "light",
	  "period": "hh",
	  "dataDateFormat": "JJ:NN",
	  "balloonDateFormat": "JJ:NN",
	  "columnWidth": 0.5,
	  "marginRight": 70,
	  "marginTop": -5,
	  "valueAxis": {
	    "type": "date",
	    //"minimum": 0,
	    //"maximum": 24,
	    "axisColor": "#ffffff",
	    "axisAlpha": 1,
	    "gridColor": "#ffffff",
	    "gridAlpha": 0.3
	  },
	  "brightnessStep": 7,
		"graph" : {
		"fillAlphas" : 1,
		"lineAlpha" : 1,
		"lineColor" : "#000",
		"fillAlphas" : 0.85,
		"balloonText" : "<b>[[task]]</b>:<br />[[start]] ~ [[end]]"
		},
	  "rotate": true,
	  "categoryField": "category",
	  "segmentsField": "segments",
	  "colorField": "color",
	  "startDateField": "start",
	  "endDateField": "end",
	  "dataProvider": dataProvider,
	  "backgroundAlpha": 0,
	  "color": "#ffffff",
	  "categoryAxis": {
		  "axisColor": "#ffffff",
		  "axisAlpha": 1,
		  "gridColor": "#ffffff",
		  "gridAlpha": 0.3
	  },
	  "valueScrollbar": {
	    "autoGridCount": true,
	    "enabled": false
	  },
	  "chartCursor": {
	    "cursorColor": "#55bb76",
	    "valueBalloonsEnabled": false,
	    "cursorAlpha": 0,
	    "valueLineAlpha": 0.5,
	    "valueLineBalloonEnabled": true,
	    "valueLineEnabled": true,
	    "zoomable": false,
	    "valueZoomable": false
	  },
	  "export": {
	    "enabled": false
	  }
	} );
}

function getDateFormat(date)
{
    var yyyy = date.getFullYear().toString();
    var mm = (date.getMonth() + 1).toString();
    var dd = date.getDate().toString();
    var hh = date.getHours().toString();
    var mm2 = date.getMinutes().toString();
    var ss = date.getSeconds().toString();

    return yyyy + "-" + (mm[1] ? mm : '0'+mm[0]) + "-" + (dd[1] ? dd : '0'+dd[0]) + " " + (hh[1] ? hh : '0'+hh[0]) + ":" + (mm2[1] ? mm2 : '0'+mm2[0]) + ":" + (ss[1] ? ss : '0'+ss[0]);
}

function getShortDateFormat(date)
{
    var yyyy = date.getFullYear().toString();
    var mm = (date.getMonth() + 1).toString();
    var dd = date.getDate().toString();

    return yyyy + "-" + (mm[1] ? mm : '0'+mm[0]) + "-" + (dd[1] ? dd : '0'+dd[0]);
}

var dataProvider2;
function drawchart2(data) {
	if(dataProvider2 == null) {
		dataProvider2 = new Array();
		var jsonData = JSON.parse(data);
		for (var i = 0; i < jsonData.length; i++) {
			var date = jsonData[i].log_date.split(" ");
			var value = {"log_date": date[1],"log_value": jsonData[i].log_value};
			dataProvider2.push(value);
		}
	}else {
		var lastIndex = dataProvider2.length - 1;
		var lastValue = dataProvider2[lastIndex].log_value;
		var lastDate = dataProvider2[lastIndex].log_date;
		var date = new Date(getShortDateFormat(new Date()) + " " + lastDate);
		date.setSeconds(date.getSeconds() + 1);
		
		var ran = (Math.random() > 0.5 ? 1 : -1);
		var value = lastValue + Math.floor(Math.random() * 10 * ran);
		var date2 = getDateFormat(date).split(" ");
		var res = {"log_date": date2[1], "log_value": Math.max(value, 0)};
		dataProvider2.shift();
		dataProvider2.push(res);
	}
	
	var chart2 = AmCharts.makeChart("dashboard_amchart_2", {
		  "type": "serial",
		  "theme": "light",
		  "marginTop": 0,
		  "marginRight": 0,
		  "marginLeft": 40,
		  "autoMarginOffset": 0,
		  "dataDateFormat": "HH:mm:ss",
		  "color": "#fff",
		  "valueAxes": [ {
		    "id": "v1",
		    "axisAlpha": 1,
		    "axisColor": "#ffffff",
		    "position": "left",
		    "ignoreAxisWidth": true,
		    "gridColor": "#ffffff",
			"gridAlpha": 0.3
		  } ],
		  "balloon": {
		    "borderThickness": 1,
		    "shadowAlpha": 0
		  },
		  "graphs": [ {
		    "id": "g1",
		    "balloon": {
		      "drop": true,
		      "adjustBorderColor": false,
		      "color": "#ffffff",
		      "type": "smoothedLine"
		    },
		    "fillAlphas": 0.2,
		    "bullet": "round",
		    "bulletBorderAlpha": 1,
		    "bulletColor": "#FFFFFF",
		    "bulletSize": 5,
		    "hideBulletsCount": 50,
		    "lineThickness": 2,
		    "title": "red line",
		    "useLineColorForBulletBorder": true,
		    "valueField": "log_value",
		    "balloonText": "<span style='font-size:18px;'>[[log_value]]</span>",
		    "lineColor": "#39BB9D" 
		  } ],
		  "chartCursor": {
		    "valueLineEnabled": true,
		    "valueLineBalloonEnabled": true,
		    "cursorAlpha": 0,
		    "zoomable": false,
		    "valueZoomable": true,
		    "valueLineAlpha": 0.5
		  },
		  "valueScrollbar": {
		    "autoGridCount": true,
		    "color": "#000000",
		    "scrollbarHeight": 50,
		    "enabled": false
		  },
		  "categoryField": "log_date",
		  "categoryAxis": {
		    //"parseDates": true,
		    "dashLength": 1,
		    "minorGridEnabled": true,
		    "labelRotation": 0,
		    "gridColor": "#ffffff",
			"gridAlpha": 0.3,
			"axisColor": "#ffffff",
			"axisAlpha": 1
		  },
		  "export": {
		    "enabled": false
		  },
		  "dataProvider": dataProvider2
		} );
}

var dataProvider3;
function drawchart3(data) {
	if(dataProvider3 == null) {
		dataProvider3 = new Array();
		
		var jsonData = JSON.parse(data);
		for (var i = 0; i < jsonData.length; i++) {
			var value = {"category_team": jsonData[i].category_team,"info_average": jsonData[i].info_average,
					"info_value": jsonData[i].info_value};
			dataProvider3.push(value);
		}
	}else {
		var tmpProvider = new Array();
		for(var i=0; i<dataProvider3.length; i++) {
			var ran = Math.floor(Math.random() * 10 + 1);
			var value = {"category_team": dataProvider3[i].category_team,"info_average": dataProvider3[i].info_average + ran,
					"info_value": dataProvider3[i].info_value + ran};
			tmpProvider.push(value);
		}
		dataProvider3 = tmpProvider;
	}
	
	var chart3 = AmCharts.makeChart("dashboard_amchart_3", {
	    "theme": "light",
	    "type": "serial",
	    "marginTop": 0,
	    "autoMarginOffset": 0,
	    "dataProvider": dataProvider3,
	    "color": "#fff",
	    "valueAxes": [{
	        //"unit": "%",
	        "position": "left",
	        "gridColor": "#ffffff",
			"gridAlpha": 0.3,
			"axisColor": "#ffffff",
			"axisAlpha": 1
	        //"title": "부서별 개인정보 처리 현황"
	    }],
	    "startDuration": 1,
	    "graphs": [{
	        "balloonText": "[[category]] (평균): <b>[[value]]</b>",
	        "fillAlphas": 1,
	        "lineAlpha": 0.2,
	        "title": "평균",
	        "type": "column",
	        "valueField": "info_average",
	        "lineColor": "#431D44"
	    }, {
	        "balloonText": "[[category]] (현재): <b>[[value]]</b>",
	        "fillAlphas": 1,
	        "lineAlpha": 0.2,
	        "title": "현재",
	        "type": "column",
	        "clustered":false,
	        "columnWidth":0.5,
	        "valueField": "info_value",
	        "lineColor": "#8B2249"
	    }],
	    //"plotAreaFillAlphas": 0.1,
	    "categoryField": "category_team",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "gridColor": "#ffffff",
			"gridAlpha": 0.3,
			"axisColor": "#ffffff",
			"axisAlpha": 1
	    },
	    "export": {
	    	"enabled": false
	     }

	});
}

function drawchart3_new(data) {
	if(dataProvider3 == null) {
		dataProvider3 = new Array();
		
		var jsonData = JSON.parse(data);
		for (var i = 0; i < jsonData.length; i++) {
			var value = {"category_team": jsonData[i].category_team,"info_average": jsonData[i].info_average,
					"info_value": jsonData[i].info_value};
			dataProvider3.push(value);
		}
	}else {
		var tmpProvider = new Array();
		for(var i=0; i<dataProvider3.length; i++) {
			var ran = Math.floor(Math.random() * 10 + 1);
			var value = {"category_team": dataProvider3[i].category_team,"info_average": dataProvider3[i].info_average + ran,
					"info_value": dataProvider3[i].info_value + ran};
			tmpProvider.push(value);
		}
		dataProvider3 = tmpProvider;
	}
	
	var chart = AmCharts.makeChart("dashboard_amchart_3", {
		"type": "serial",
	     "theme": "light",
	     "color": "#fff",
		"categoryField": "category_team",
		//"rotate": true,
		"startDuration": 1,
		"categoryAxis": {
			"gridPosition": "start",
			"position": "left",
			"gridColor": "#ffffff",
			"gridAlpha": 0.3,
			"axisColor": "#ffffff",
			"axisAlpha": 1
		},
		"trendLines": [],
		"graphs": [
			{
				"balloonText": "평균:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-1",
				"lineAlpha": 0.2,
				"title": "평균",
				"type": "column",
				"valueField": "info_average",
				"lineColor": "#511549"
			},
			{
				"balloonText": "현재:[[value]]",
				"fillAlphas": 0.8,
				"id": "AmGraph-2",
				"lineAlpha": 0.2,
				"title": "현재",
				"type": "column",
				"valueField": "info_value",
				"lineColor": "#39BB9D"
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"position": "bottom",
				"gridColor": "#ffffff",
				"gridAlpha": 0.3,
				"axisColor": "#ffffff",
				"axisAlpha": 1
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [],
		"dataProvider": dataProvider3,
	    "export": {
	    	"enabled": false
	     }

	});
}
var dataProvider4;
function drawchart4(data) {
	if(dataProvider4 == null) {
		dataProvider4 = new Array();
		var jsonData = JSON.parse(data);
		for (var i = 0; i < jsonData.length; i++) {
			var date = jsonData[i].log_date.split(" ");
			var value = {"log_date": date[1],"log_value": jsonData[i].log_value};
			dataProvider4.push(value);
		}
	}else {
		var lastIndex = dataProvider4.length - 1;
		var lastValue = dataProvider4[lastIndex].log_value;
		var lastDate = dataProvider4[lastIndex].log_date;
		var date = new Date(getShortDateFormat(new Date()) + " " + lastDate);
		date.setSeconds(date.getSeconds() + 1);
		dataProvider4.shift();
		var ran = (Math.random() > 0.5 ? 1 : -1);
		var value = lastValue + Math.floor(Math.random() * 10 * ran);
		var date2 = getDateFormat(date).split(" ");
		var res = {"log_date": date2[1], "log_value": Math.max(value, 0)};
		dataProvider4.push(res);
	}
	
	var chart4 = AmCharts.makeChart("dashboard_amchart_4", {
		  "type": "serial",
		  "theme": "light",
		  "marginTop": 0,
		  "marginRight": 0,
		  "marginLeft": 40,
		  "autoMarginOffset": 0,
		  "color": "#fff",
		  "dataDateFormat": "YYYY-MM-DD",
		  "valueAxes": [ {
		    "id": "v1",
		    "gridColor": "#ffffff",
			"gridAlpha": 0.3,
			"axisColor": "#ffffff",
			"axisAlpha": 1,
		    "position": "left",
		    "ignoreAxisWidth": true
		  } ],
		  "balloon": {
		    "borderThickness": 1,
		    "shadowAlpha": 0
		  },
		  "graphs": [ {
		    "id": "g1",
		    "balloon": {
		      "drop": true,
		      "adjustBorderColor": false,
		      "color": "#ffffff",
		      "type": "smoothedLine"
		    },
		    "fillAlphas": 0.2,
		    "bullet": "round",
		    "bulletBorderAlpha": 1,
		    "bulletColor": "#FFFFFF",
		    "bulletSize": 5,
		    "hideBulletsCount": 50,
		    "lineThickness": 2,
		    "title": "red line",
		    "useLineColorForBulletBorder": true,
		    "valueField": "log_value",
		    "balloonText": "<span style='font-size:18px;'>[[log_value]]</span>",
		    "lineColor": "#39BB9D"
		  } ],
		  "chartCursor": {
		    "valueLineEnabled": true,
		    "valueLineBalloonEnabled": true,
		    "cursorAlpha": 0,
		    "zoomable": false,
		    "valueZoomable": true,
		    "valueLineAlpha": 0.5
		  },
		  "valueScrollbar": {
		    "autoGridCount": true,
		    "color": "#000000",
		    "scrollbarHeight": 50,
		    "enabled":false	// 스크롤바 없애기
		  },
		  "categoryField": "log_date",
		  "categoryAxis": {
		    "parseDates": false,
		    "dashLength": 1,
		    "minorGridEnabled": true,
		    "labelRotation": 0,
		    "gridColor": "#ffffff",
			"gridAlpha": 0.3,
			"axisColor": "#ffffff",
			"axisAlpha": 1
		  },
		  "export": {
		    "enabled": false
		  },
		  "dataProvider": dataProvider4
		} );
}

var dataProvider5;
function drawchart5(data) {
	if(dataProvider5 == null) {
		dataProvider5 = new Array();
		
		var jsonData = JSON.parse(data);
		for (var i = 0; i < jsonData.length; i++) {
			var value = {
					"x": i, "x2": i, "y": 5, "y2": 8, "x3": i, "y3": 11,
					"value": jsonData[i].rrnum,"value2": jsonData[i].accnum,"value3": jsonData[i].phonenum};
			dataProvider5.push(value);
		}
	}else {
		var tmpProvider = new Array();
		for (var i = 0; i < dataProvider5.length; i++) {
			var ran = Math.floor(Math.random() * 10 + 1);
			var value = {
					"x": i, "x2": i, "y": 5, "y2": 8, "x3": i, "y3": 11,
					"value": dataProvider5[i].rrnum + ran,"value2": dataProvider5[i].accnum + ran,"value3": dataProvider5[i].phonenum + ran};
			tmpProvider.push(value);
		}
		dataProvider = tmpProvider;
	}
	
	
	var chart5 = AmCharts.makeChart( "dashboard_amchart_5", {
	  "type": "xy",
	  //"path": "http://www.amcharts.com/lib/3/",
	  "theme": "light",
	  "marginTop": 0,
	  "marginBottom": 0,
	  "autoMarginOffset": 0,
	  "color": "#fff",
	  "dataProvider": dataProvider5,
	  "valueAxes": [ {
	    "position": "bottom",
	    "gridColor": "#ffffff",
		"gridAlpha": 0.3,
		"axisColor": "#ffffff",
		"axisAlpha": 1,
	    "integersOnly": true,
	    "labelRotation": 0,
	    "labelFunction": function( value ) {
	      
	      // define categories
	      var cats = [
	        "마케팅",
	        "인사",
	        "정보",
	        "시설",
	        "홍보",
	        "기획",
	        "대외협력",
	        "경영관리",
	        "개발",
	        "연구",
	        "컨설팅",
	        "기술지원",
	        ""
	      ];
	      return cats[value];
	    }
	  }, {
		  "gridColor": "#ffffff",
			"gridAlpha": 0.3,
			"axisColor": "#ffffff",
			"axisAlpha": 1,
	    "position": "left"
	  } ],
	  "startDuration": 1.5,
	  "graphs": [ {
	    "balloonText": "주민번호: [[value]]",
	    "bullet": "circle",
	    "bulletBorderAlpha": 0.2,
	    "bulletAlpha": 0.8,
	    "lineAlpha": 0,
	    "fillAlphas": 0,
	    "valueField": "value",
	    "xField": "x",
	    "yField": "y",
	    "maxBulletSize": 100,
	    "bulletColor" : "#003870"
	  }, {
	    "balloonText": "계좌번호: [[value2]]",
	    "bullet": "circle",
	    "bulletBorderAlpha": 0.2,
	    "bulletAlpha": 0.8,
	    "lineAlpha": 0,
	    "fillAlphas": 0,
	    "valueField": "value2",
	    "xField": "x2",
	    "yField": "y2",
	    "maxBulletSize": 100,
	    "bulletColor" : "#006673"
	  }, {
	    "balloonText": "전화번호: [[value3]]",
	    "bullet": "circle",
	    "bulletBorderAlpha": 0.2,
	    "bulletAlpha": 0.8,
	    "lineAlpha": 0,
	    "fillAlphas": 0,
	    "valueField": "value3",
	    "xField": "x3",
	    "yField": "y3",
	    "maxBulletSize": 100,
	    "bulletColor" : "#39BB9D"
	  }],
	} );
}

function drawchart7(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"personal_type": jsonData[i].personal_type,"personal_value": jsonData[i].personal_value};
		dataProvider.push(value);
	}
	
	var chart7 = AmCharts.makeChart("dashboard_amchart_7", {
	  "type": "pie",
	  "startDuration": 0,
	   "theme": "light",
	   "autoMarginOffset": 0,
	  "addClassNames": true,
	  "color": "#fff",
	  "legend":{
	   	"position":"right",
	    "marginRight":0,
	    "autoMargins":false,
	    "showEntries":true,
	    "verticalGap":1,
	    "fontSize":15,
	    "color": "#fff"
	  },
	  "colors": ["#511549", "#CC323A", "#037F76", "#003870"],
	  "innerRadius": "30%",
	  "defs": {
	    "filter": [{
	      "id": "shadow",
	      "width": "200%",
	      "height": "200%",
	      "feOffset": {
	        "result": "offOut",
	        "in": "SourceAlpha",
	        "dx": 0,
	        "dy": 0
	      },
	      "feGaussianBlur": {
	        "result": "blurOut",
	        "in": "offOut",
	        "stdDeviation": 5
	      },
	      "feBlend": {
	        "in": "SourceGraphic",
	        "in2": "blurOut",
	        "mode": "normal"
	      }
	    }]
	  },
	  "labelsEnabled": false,
	  "dataProvider": dataProvider,
	  "valueField": "personal_value",
	  "titleField": "personal_type",
	  "export": {
	    "enabled": false,
	    "position": "bottom-left"
	  }
	});
	
	chart7.addListener("init", handleInit);
	
	chart7.addListener("rollOverSlice", function(e) {
	  handleRollOver(e);
	});
	
	function handleInit(){
	  chart7.legend.addListener("rollOverItem", handleRollOver);
	}

	function handleRollOver(e){
	  var wedge = e.dataItem.wedge.node;
	  wedge.parentNode.appendChild(wedge);
	}
}

function drawchart8(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"personal_type": jsonData[i].personal_type,"personal_value": jsonData[i].personal_value};
		dataProvider.push(value);
	}
	
	var chart8 = AmCharts.makeChart("dashboard_amchart_8", {
	  "type": "pie",
	  "startDuration": 0,
	   "theme": "light",
	   "autoMarginOffset": 0,
	  "addClassNames": true,
	  "legend":{
	   	"position":"right",
	    "marginRight":0,
	    "autoMargins":false,
	    "verticalGap":1,
	    "fontSize":15,
	    "color": "#fff"
	  },
	  "colors": ["#801F63", "#E56542", "#39BB9D"],
	  "innerRadius": "30%",
	  "defs": {
	    "filter": [{
	      "id": "shadow",
	      "width": "200%",
	      "height": "200%",
	      "feOffset": {
	        "result": "offOut",
	        "in": "SourceAlpha",
	        "dx": 0,
	        "dy": 0
	      },
	      "feGaussianBlur": {
	        "result": "blurOut",
	        "in": "offOut",
	        "stdDeviation": 5
	      },
	      "feBlend": {
	        "in": "SourceGraphic",
	        "in2": "blurOut",
	        "mode": "normal"
	      }
	    }]
	  },
	  "labelsEnabled": false,
	  "dataProvider": dataProvider,
	  "valueField": "personal_value",
	  "titleField": "personal_type",
	  "export": {
	    "enabled": false,
	    "position": "bottom-left"
	  }
	});
	
	chart8.addListener("init", handleInit);
	
	chart8.addListener("rollOverSlice", function(e) {
	  handleRollOver(e);
	});
	
	function handleInit(){
	  chart8.legend.addListener("rollOverItem", handleRollOver);
	}

	function handleRollOver(e){
	  var wedge = e.dataItem.wedge.node;
	  wedge.parentNode.appendChild(wedge);
	}
}

function drawchart9(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"day": jsonData[i].day,"last": jsonData[i].last,"current": jsonData[i].current };
		dataProvider.push(value);
	}
	
	var chart9 = AmCharts.makeChart( "dashboard_amchart_9", {
	  "type": "serial",
	  "addClassNames": true,
	  "theme": "light",
	  "autoMargins": false,
	  "marginLeft": 30,
	  "marginRight": 8,
	  "marginTop": 0,
	  "autoMarginOffset": 0,
	  "color": "#fff",
	  "balloon": {
	    "adjustBorderColor": false,
	    "horizontalPadding": 10,
	    "verticalPadding": 8,
	    "color": "#ffffff"
	  },

	  "dataProvider": dataProvider,
	  "valueAxes": [ {
	    "axisAlpha": 0,
	    "position": "left",
	    "gridColor": "#ffffff",
		"gridAlpha": 0.3,
		"axisColor": "#ffffff",
		"axisAlpha": 1
	  } ],
	  "startDuration": 1,
	  "graphs": [ {
	    "alphaField": "alpha",
	    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	    "fillAlphas": 1,
	    "title": "금주",
	    "type": "column",
	    "valueField": "current",
	    "dashLengthField": "dashLengthColumn",
	    "lineColor": "#39BB9D"
	  }, {
	    "id": "graph2",
	    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
	    "bullet": "round",
	    "lineThickness": 3,
	    "bulletSize": 7,
	    "bulletBorderAlpha": 1,
	    "bulletColor": "#FFFFFF",
	    "useLineColorForBulletBorder": true,
	    "bulletBorderThickness": 3,
	    "fillAlphas": 0,
	    "lineAlpha": 1,
	    "title": "평균",
	    "valueField": "last",
	    "dashLengthField": "dashLengthLine",
	    "lineColor": "#E56542"
	  } ],
	  "categoryField": "day",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "gridColor": "#ffffff",
		"gridAlpha": 0.3,
		"axisColor": "#ffffff",
		"axisAlpha": 1,
	    "tickLength": 0
	  },
	  "export": {
	    "enabled": false
	  }
	} );
}

function drawchart10(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"rrn_date": jsonData[i].rrn_date,"rrn_fvalue": jsonData[i].rrn_fvalue,
				"rrn_tvalue": jsonData[i].rrn_tvalue,"rrn_value": jsonData[i].rrn_value };
		dataProvider.push(value);
	}
	
	var chart10= AmCharts.makeChart("dashboard_amchart_10", {
	    "type": "serial",
	    "theme": "light",
	    "autoMarginOffset":0,
	    "marginRight":0,
	    "marginTop": 0,
		"marginBottom": 0,
		"autoMarginOffset": 0,
		"color": "#fff",
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	    	"gridColor": "#ffffff",
			"gridAlpha": 0.3,
			"axisColor": "#ffffff",
			"axisAlpha": 1,
	        "position": "left"
	    }],
	    "graphs": [{
	        "id": "fromGraph",
	        "lineAlpha": 0,
	        "showBalloon": false,
	        "valueField": "rrn_fvalue",
	        "fillAlphas": 0
	    }, {
	        "fillAlphas": 0.2,
	        "fillToGraph": "fromGraph",
	        "lineAlpha": 0,
	        "showBalloon": false,
	        "valueField": "rrn_tvalue",
	        "lineColor": "#39BB9D"
	    }, {
	        "valueField": "rrn_value",
	        "balloonText":"<div style='margin:10px; text-align:left'><span style='font-size:11px'>allowed: [[rrn_fvalue]] - [[rrn_tvalue]]</span><br><span style='font-size:18px'>Value:[[rrn_value]]</span></div>",
	        "fillAlphas": 0,
	        "lineColor": "#39BB9D"
	    }],
	    "chartCursor": {
	        "fullWidth": true,
	        "cursorAlpha": 0.05,
	        "valueLineEnabled":true,
	        "valueLineAlpha":0.5,
	        "valueLineBalloonEnabled":true
	    },
	    "dataDateFormat": "YYYY-MM-DD",
	    "categoryField": "rrn_date",
	    "categoryAxis": {
	     "position":"bottom",
	        "parseDates": true,
	        "gridColor": "#ffffff",
			"gridAlpha": 0.3,
			"axisColor": "#ffffff",
			"axisAlpha": 1,
	        "minHorizontalGap": 25,
	        "gridAlpha": 0,
	        "tickLength": 0,
	        "dateFormats": [{
	            "period": 'fff',
	            "format": 'JJ:NN:SS'
	        }, {
	            "period": 'ss',
	            "format": 'JJ:NN:SS'
	        }, {
	            "period": 'mm',
	            "format": 'JJ:NN'
	        }, {
	            "period": 'hh',
	            "format": 'JJ:NN'
	        }, {
	            "period": 'DD',
	            "format": 'DD'
	        }, {
	            "period": 'WW',
	            "format": 'DD'
	        }, {
	            "period": 'MM',
	            "format": 'MMM'
	        }, {
	            "period": 'YYYY',
	            "format": 'YYYY'
	        }]
	    },

	    "chartScrollbar":{
	    	"enabled":false	// 스크롤바 없애기
	    },

	    "export": {
	        "enabled": false
	    }
	});

	chart10.addListener("dataUpdated", zoomChart2);

	function zoomChart2(){
	    chart10.zoomToDates(new Date(2009,9,20, 15), new Date(2009,10,3,12));
	}
}
	
function drawchart12(data) {
	var dataProvider = new Array();
	
	var color = ["#003870", "#004856", "#006673", "#037F76", "#39BB9D", "#E56542", "#CC323A", "#931A2E", "#911751", "#801F63", "#511549"];
	for(var i=0;i<12;i++) {
		AmCharts.makeChart( "line" + i, {
			  "type": "serial",
			  "theme": "light",
			  "dataProvider": [ {
			    "day": 1,
			    "value": 120
			  }, {
			    "day": 2,
			    "value": 124
			  }, {
			    "day": 3,
			    "value": 127
			  }, {
			    "day": 4,
			    "value": 122
			  }, {
			    "day": 5,
			    "value": 121
			  }, {
			    "day": 6,
			    "value": 123
			  }, {
			    "day": 7,
			    "value": 118
			  }, {
			    "day": 8,
			    "value": 113
			  }, {
			    "day": 9,
			    "value": 122
			  }, {
			    "day": 10,
			    "value": 125,
			    "bullet": "round"
			  } ],
			  "categoryField": "day",
			  "autoMargins": false,
			  "marginLeft": 0,
			  "marginRight": 5,
			  "marginTop": 0,
			  "marginBottom": 0,
			  "graphs": [ {
			    "valueField": "value",
			    "bulletField": "bullet",
			    "showBalloon": false,
			    "lineColor": color[i]
			  } ],
			  "valueAxes": [ {
			    "gridAlpha": 0,
			    "axisAlpha": 0
			  } ],
			  "categoryAxis": {
			    "gridAlpha": 0,
			    "axisAlpha": 0,
			    "startOnAxis": true
			  }
			} );
			
			AmCharts.makeChart( "column" + i, {
			  "type": "serial",
			  "dataProvider": [ {
			    "day": 1,
			    "value": -5
			  }, {
			    "day": 2,
			    "value": 3
			  }, {
			    "day": 3,
			    "value": 7
			  }, {
			    "day": 4,
			    "value": -3
			  }, {
			    "day": 5,
			    "value": 3
			  }, {
			    "day": 6,
			    "value": 4
			  }, {
			    "day": 7,
			    "value": 6
			  }, {
			    "day": 8,
			    "value": -3
			  }, {
			    "day": 9,
			    "value": -2
			  }, {
			    "day": 10,
			    "value": 6
			  } ],
			  "categoryField": "day",
			  "autoMargins": false,
			  "marginLeft": 0,
			  "marginRight": 0,
			  "marginTop": 0,
			  "marginBottom": 0,
			  "graphs": [ {
			    "valueField": "value",
			    "type": "column",
			    "fillAlphas": 1,
			    "lineColor": color[i],
			    "showBalloon": false
			  } ],
			  "valueAxes": [ {
			    "gridAlpha": 0,
			    "axisAlpha": 0
			  } ],
			  "categoryAxis": {
			    "gridAlpha": 0,
			    "axisAlpha": 0
			  }
			} );
	}
	
}

function drawchart13(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"day": jsonData[i].day,"last": jsonData[i].last,"current": jsonData[i].current };
		dataProvider.push(value);
	}
	
	var chart13 = AmCharts.makeChart("dashboard_amchart_13", {
	    "type": "serial",
	    "theme": "light",
	    "marginTop": 0,
	    "marginBottom": 0,
	    "autoMarginOffset": 0,
	    "color": "#fff",
	    "legend": {
	        "useGraphSettings": true,
	        "enabled":false	// 밑에 캡션
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "integersOnly": true,
	        "maximum": 100000,
	        "minimum": 1,
	        "reversed": true,
	        "gridColor": "#ffffff",
			"gridAlpha": 0.3,
			"axisColor": "#ffffff",
			"axisAlpha": 1,
	        "dashLength": 10,
	        "gridCount": 10,
	        "position": "left"
	        //,"title": "Place taken"
	    }],
	    "startDuration": 0.5,
	    "graphs": [{
	        //"balloonText": "[[category]]: [[value]]",
	    	"balloonText": "[[day]]: [[value]]",
	        "bullet": "round",
	        "title": "last",
	        "type": "smoothedLine",
	        "valueField": "last",
			"fillAlphas": 0,
			"lineColor" : "#39BB9D"
	    }, {
	        //"balloonText": "[[category]]: [[value]]",
	    	"balloonText": "[[day]]: [[value]]",
	        "bullet": "round",
	        "title": "current",
	        "type": "smoothedLine",
	        "valueField": "current",
			"fillAlphas": 0,
			"lineColor" : "#E56542"
	    }],
	    "chartCursor": {
	        "cursorAlpha": 0,
	        "zoomable": false,
	    },
	    "categoryField": "day",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "gridColor": "#ffffff",
			"gridAlpha": 0.3,
			"axisColor": "#ffffff",
			"axisAlpha": 1,
	        "fillAlpha": 0.05,
	        "fillColor": "#000000",
	        "position": "bottom"
	    },
	    "export": {
	    	"enabled": false,
	        "position": "bottom-right"
	     }
	});
}

function drawchart15(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"psm_type": jsonData[i].psm_type,"psm_value": jsonData[i].psm_value };
		dataProvider.push(value);
	}
	
	var chart15 = AmCharts.makeChart( "dashboard_amchart_15", {
	  "type": "serial",
	  "theme": "light",
	  "marginTop": 0,
   		"marginBottom": 0,
   		"autoMarginOffset": 0,
   		"color": "#fff",
	  "dataProvider": dataProvider,
	  "valueAxes": [ {
		  "gridColor": "#ffffff",
			"gridAlpha": 0.3,
			"axisColor": "#ffffff",
			"axisAlpha": 1,
	    "dashLength": 0
	  } ],
	  "gridAboveGraphs": true,
	  "startDuration": 1,
	  "graphs": [ {
	    "balloonText": "[[category]]: <b>[[value]]</b>",
	    "fillAlphas": 1,
	    "lineAlpha": 1,
	    "type": "column",
	    "valueField": "psm_value",
	    "lineColor" : "#39BB9D"
	  } ],
	  "chartCursor": {
	    "categoryBalloonEnabled": false,
	    "cursorAlpha": 0,
	    "zoomable": false
	  },
	  "categoryField": "psm_type",
	  "categoryAxis": {
	    "gridPosition": "start",
	    "gridColor": "#ffffff",
		"gridAlpha": 0.3,
		"axisColor": "#ffffff",
		"axisAlpha": 1,
	    "tickPosition": "start",
	    "tickLength": 20
	  },
	  "export": {
	    "enabled": false
	  }

	} );
}
	
function drawchart16(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"content_date": jsonData[i].content_date,"content1": jsonData[i].content1,
				"content2": jsonData[i].content2,"content3": jsonData[i].content3};
		dataProvider.push(value);
	}
	
	var chart16 = AmCharts.makeChart("dashboard_amchart_16", {
	    "type": "serial",
		"theme": "light",
		"marginTop": 0,
	    "marginBottom": 0,
	    "autoMarginOffset": 0,
	    "color": "#fff",
	    "titles": [{
	        "text": "전체 컨텐츠 로그 수집 상태",
	        "size": 15,
	        "enabled":false
	    }],
	    "legend": {
	        "align": "center",
	        "equalWidths": false,
	        "periodValueText": "total: [[value.sum]]",
	        "valueAlign": "left",
	        "valueText": "[[value]] ([[percents]]%)",
	        "valueWidth": 100,
	        "autoMargins": false,
	        "enabled":false
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "stackType": "100%",
	        "gridColor": "#ffffff",
			"gridAlpha": 0.3,
			"axisColor": "#ffffff",
			"axisAlpha": 1,
	        "position": "left",
	        //"title": "percent"
	    }],
	    "graphs": [{
	        //"balloonText": "<img src='http://www.amcharts.com/lib/3/images/car.png' style='vertical-align:bottom; margin-right: 10px; width:18px; height:21px;'><span style='font-size:14px; color:#000000;'><b>[[value]]</b></span>",
	    	"balloonText": "<span style='font-size:xx-small; color:#000000;'><b>[[title]]: [[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "lineAlpha": 0.5,
	        "title": "Content1",
	        "valueField": "content1",
	        "lineColor": "#003870"
	    }, {
	        //"balloonText": "<img src='http://www.amcharts.com/lib/3/images/motorcycle.png' style='vertical-align:bottom; margin-right: 10px; width:28px; height:21px;'><span style='font-size:14px; color:#000000;'><b>[[value]]</b></span>",
	    	"balloonText": "<span style='font-size:xx-small; color:#000000;'><b>[[title]]: [[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "lineAlpha": 0.5,
	        "title": "Content2",
	        "valueField": "content2",
	        "lineColor": "#006673"
	    }, {
	        //"balloonText": "<img src='http://www.amcharts.com/lib/3/images/bicycle.png' style='vertical-align:bottom; margin-right: 10px; width:28px; height:21px;'><span style='font-size:14px; color:#000000;'><b>[[value]]</b></span>",
	    	"balloonText": "<span style='font-size:xx-small; color:#000000;'><b>[[title]]: [[value]]</b></span>",
	        "fillAlphas": 0.8,
	        "lineAlpha": 0.5,
	        "title": "Content3",
	        "valueField": "content3",
	        "lineColor": "#39BB9D"
	    }],
	    "plotAreaBorderAlpha": 0,
	    "marginLeft": 0,
	    "chartCursor": {
	        "cursorAlpha": 0,
	        "zoomable": false
	    },
	    "categoryField": "content_date",
	    "categoryAxis": {
	        "startOnAxis": true,
	        "gridColor": "#ffffff",
			"gridAlpha": 0.3,
			"axisColor": "#ffffff",
			"axisAlpha": 1,
	    },
	    "export": {
	    	"enabled": false
	     }
	});
}
	
var dataProvider17;
function drawchart17(data) {
	
	var d = new Date();
	var h = d.getHours();
	if(h == 0)
		h = 24;
	
	if(dataProvider17 == null) {
		dataProvider17 = new Array();
		var jsonData = JSON.parse(data);
		for (var i = 0; i < jsonData.length; i++) {
			var value;
			if(jsonData[i].time > h) {
				value = {"info": 0,"time": jsonData[i].time+"시","color": "#eee" };
			}else {
				value = {"info": jsonData[i].info,"time": jsonData[i].time+"시","color": "#511549" };
			}
			dataProvider17.push(value);
		}
	}else {
		var tmpProvider = new Array();
		for(var i=0; i<dataProvider17.length; i++) {
			var value;
			if(parseInt(dataProvider17[i].time) > h) {
				value = {"info": 0,"time": parseInt(dataProvider17[i].time)+"시","color": "#eee" };
			}else {
				if(dataProvider17[i].info == 0) { // 값이 없을때
					// 이전값에서 값을 가져와 비교해 넣는다.
					var preVal = dataProvider17[i-1].info;
					var resVal;
					if(dataProvider17[i].time > 14) // 14시가 넘어가면 감소
						resVal = preVal - Math.floor(Math.random() * 100 + 1);
					else
						resVal = preVal + Math.floor(Math.random() * 100 + 1);
					
					value = {"info": dataProvider17[i].info + resVal,"time": parseInt(dataProvider17[i].time)+"시","color": "#511549" };
				} else {	// 값이 있을때는 마지막 시간에만 값을 변화시킨다.
					if(parseInt(dataProvider17[i].time) == h) {
						var resVal = Math.floor(Math.random()*2);
						value = {"info": dataProvider17[i].info + resVal,"time": parseInt(dataProvider17[i].time)+"시","color": "#511549" };
					} else {
						value = {"info": dataProvider17[i].info,"time": dataProvider17[i].time,"color": "#511549" };
					}
				}
			}
			tmpProvider.push(value);
		}
		dataProvider17 = tmpProvider;
	}
	
	var chart17 = AmCharts.makeChart("dashboard_amchart_17", {
	    "theme": "light",
	    "type": "serial",
		"startDuration": 2,
		"marginTop": 0,
		"marginBottom": 0,
		"autoMarginOffSet": 0,
		"color": "#fff",
	    "dataProvider": dataProvider17,
	    "valueAxes": [{
	        "position": "left",
	        "gridColor": "#ffffff",
			"gridAlpha": 0.3,
			"axisColor": "#ffffff",
			"axisAlpha": 1
	        //"title": "개인정보 처리 로그"
	    }],
	    
	    "graphs": [{
	        "balloonText": "[[category]]: <b>[[value]]</b>",
	        "fillColorsField": "color",
	        "fillAlphas": 1,
	        "lineAlpha": 0.1,
	        "type": "column",
	        "valueField": "info"
	    }],
	    /*"depth3D": 10,
		"angle": 30,*/
	    "chartCursor": {
	        "categoryBalloonEnabled": false,
	        "cursorAlpha": 0,
	        "zoomable": false
	    },
	    "categoryField": "time",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "labelRotation": 0,
	        "fontSize": 12,
	        //"inside": true,
	        "labelsEnabled":true,
	        "gridColor": "#ffffff",
			"gridAlpha": 0.3,
			"axisColor": "#ffffff",
			"axisAlpha": 1
	    },
	    "export": {
	    	"enabled": false
	     }

	});
}
	
var dataProvider18;
function drawchart18(data) {
	var d = new Date();
	var h = d.getHours();
	if(h == 0)
		h = 24;
	
	if(dataProvider18 == null) {
		dataProvider18 = new Array();
		var jsonData = JSON.parse(data);
		for (var i = 0; i < jsonData.length; i++) {
			var value;
			if(jsonData[i].time > h) {
				value = {"info": 0,"time": jsonData[i].time+"시","color": "#eee" };
			}else {
				value = {"info": jsonData[i].info,"time": jsonData[i].time+"시","color": "#511549" };
			}
			dataProvider18.push(value);
		}
	}else {
		var tmpProvider = new Array();
		for(var i=0; i<dataProvider18.length; i++) {
			var value;
			if(parseInt(dataProvider18[i].time) > h) {
				value = {"info": 0,"time": parseInt(dataProvider18[i].time)+"시","color": "#eee" };
			}else {
				if(dataProvider18[i].info == 0) { // 값이 없을때
					// 이전값에서 값을 가져와 비교해 넣는다.
					var preVal = dataProvider18[i-1].info;
					var resVal;
					if(dataProvider18[i].time > 14) // 14시가 넘어가면 감소
						resVal = preVal - Math.floor(Math.random() * 100 + 1);
					else
						resVal = preVal + Math.floor(Math.random() * 100 + 1);
					
					value = {"info": dataProvider18[i].info + resVal,"time": parseInt(dataProvider18[i].time)+"시","color": "#511549" };
				} else {	// 값이 있을때는 마지막 시간에만 값을 변화시킨다.
					if(parseInt(dataProvider18[i].time) == h) {
						var resVal = Math.floor(Math.random()*2);
						value = {"info": dataProvider18[i].info + resVal,"time": parseInt(dataProvider18[i].time)+"시","color": "#511549" };
					} else {
						value = {"info": dataProvider18[i].info,"time": dataProvider18[i].time,"color": "#511549" };
					}
				}
			}
			tmpProvider.push(value);
		}
		dataProvider18 = tmpProvider;
	}
	
	var chart18 = AmCharts.makeChart("dashboard_amchart_18", {
	    "theme": "light",
	    "type": "serial",
		"startDuration": 2,
		"marginTop": 0,
		"marginBottom": 0,
		"autoMarginOffSet": 0,
		"color": "#fff",
	    "dataProvider": dataProvider18,
	    "valueAxes": [{
	        "position": "left",
	        "gridColor": "#ffffff",
			"gridAlpha": 0.3,
			"axisColor": "#ffffff",
			"axisAlpha": 1
	        //"title": "개인정보 취급자"
	    }],
	    
	    "graphs": [{
	        "balloonText": "[[category]]: <b>[[value]]</b>",
	        "fillColorsField": "color",
	        "fillAlphas": 1,
	        "lineAlpha": 0.1,
	        "type": "column",
	        "valueField": "info"
	    }],
	    /*"depth3D": 10,
		"angle": 30,*/
	    "chartCursor": {
	        "categoryBalloonEnabled": false,
	        "cursorAlpha": 0,
	        "zoomable": false
	    },
	    "categoryField": "time",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "labelRotation": 0,
	        "fontSize": 12,
	        "gridColor": "#ffffff",
			"gridAlpha": 0.3,
			"axisColor": "#ffffff",
			"axisAlpha": 1
	    },
	    "export": {
	    	"enabled": false
	     }

	});
}
	
function drawchart19(data) {
	var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	for (var i = 0; i < jsonData.length; i++) {
		var value = {"day": jsonData[i].day19,"hdd": jsonData[i].hdd,"memory": jsonData[i].memory,"cpu": jsonData[i].cpu };
		dataProvider.push(value);
	}
	
	var chart19 = AmCharts.makeChart("dashboard_amchart_19", {
	    "type": "serial",
	    "theme": "light",
	    "marginTop": 0,
		"marginBottom": 0,
		"autoMarginOffSet": 0,
		"color": "#fff",
	    "legend": {
	        "useGraphSettings": true,
	        "enabled":false	
	    },
	    "dataProvider": dataProvider,
	    "valueAxes": [{
	        "integersOnly": true,
	        "maximum": 6,
	        "minimum": 1,
	        "reversed": true,
	        "gridColor": "#ffffff",
			"gridAlpha": 0.3,
			"axisColor": "#ffffff",
			"axisAlpha": 1,
	        "dashLength": 5,
	        "gridCount": 10,
	        "position": "left"
	        //,"title": "Place taken"
	    }],
	    "startDuration": 0.5,
	    "graphs": [{
	        "balloonText": "[[title]]: [[value]]([[category]])",
	        "bullet": "round",
	        "title": "HDD",
	        "type": "smoothedLine",
	        "valueField": "hdd",
			"fillAlphas": 0,
			"lineColor": "#E56542"
	    }, {
	        "balloonText": "[[title]]: [[value]]([[category]])",
	        "bullet": "round",
	        "title": "Memory",
	        "type": "smoothedLine",
	        "valueField": "memory",
			"fillAlphas": 0,
			"lineColor": "#801F63"
	    }
	    , {
	        "balloonText": "[[title]]: [[value]]([[category]])",
	        "bullet": "round",
	        "title": "CPU",
	        "type": "smoothedLine",
	        "valueField": "cpu",
			"fillAlphas": 0,
			"lineColor": "#39BB9D"
	    }],
	    "chartCursor": {
	        "cursorAlpha": 0,
	        "zoomable": false
	    },
	    "categoryField": "day",
	    "categoryAxis": {
	        "gridPosition": "start",
	        "fillAlpha": 0.05,
	        "fillColor": "#000000",
	        "gridColor": "#ffffff",
			"gridAlpha": 0.3,
			"axisColor": "#ffffff",
			"axisAlpha": 1,
	        "position": "bottom"
	    },
	    "export": {
	    	"enabled": false,
	        "position": "bottom-right"
	     }
	});
}
	
//var dataProvider20;
function drawchart20(data) {
	//var dataProvider = new Array();
	
	var jsonData = JSON.parse(data);
	/*for (var i = 0; i < jsonData.length; i++) {
		var value = {"hdd": jsonData[i].hdd,"memory": jsonData[i].memory,"cpu": jsonData[i].cpu };
		dataProvider.push(value);
	}
	
	if(dataProvider20 == null) {
		dataProvider20 = new Array();
		
		var jsonData = JSON.parse(data);
		for (var i = 0; i < jsonData.length; i++) {
			var value = {"hdd": jsonData[i].hdd,"memory": jsonData[i].memory,"cpu": jsonData[i].cpu};
			dataProvider20.push(value);
		}
	}else {
		var psmProvider = new Array();
		for(var i=0; i<dataProvider20.length; i++) {
			var ran1 = Math.random() > 0.5 ? Math.floor(Math.random() * 10 + 1) : -Math.floor(Math.random() * 10 + 1);
			var ran2 = Math.random() > 0.5 ? Math.floor(Math.random() * 10 + 1) : -Math.floor(Math.random() * 10 + 1);
			var ran3 = Math.random() > 0.5 ? Math.floor(Math.random() * 10 + 1) : -Math.floor(Math.random() * 10 + 1);
			var value = {"hdd": jsonData[i].hdd + ran1,"memory": jsonData[i].memory + ran2,"cpu": jsonData[i].cpu + ran3};
			psmProvider.push(value);
		}
		dataProvider20 = psmProvider;
	}	*/
	
	var chart20 = AmCharts.makeChart("dashboard_amchart_20", {
	  "type": "gauge",
	  "theme": "light",
	  "marginTop": -10,
	  "marginBottom": 0,
	  "autoMarginOffSet": 0,
	  "color": "#fff",
	  "axes": [{
	    "axisAlpha": 0,
	    "tickAlpha": 0,
	    "labelsEnabled": false,
	    "startValue": 0,
	    "endValue": 100,
	    "startAngle": 0,
	    "endAngle": 270,
	    "bands": [{
	      "color": "#eee",
	      "startValue": 0,
	      "endValue": 100,
	      "radius": "80%",
	      "innerRadius": "65%"
	    }, {
	      "color": "#E56542",
	      "startValue": 0,
	      "endValue": jsonData[0].hdd,
	      "radius": "80%",
	      "innerRadius": "65%",
	      "balloonText": jsonData[0].hdd+"%"
	    }, {
	      "color": "#eee",
	      "startValue": 0,
	      "endValue": 100,
	      "radius": "60%",
	      "innerRadius": "45%"
	    }, {
	      "color": "#801F63",
	      "startValue": 0,
	      "endValue": jsonData[0].memory,
	      "radius": "60%",
	      "innerRadius": "45%",
	      "balloonText": jsonData[0].memory+"%"
	    }, {
	      "color": "#eee",
	      "startValue": 0,
	      "endValue": 100,
	      "radius": "40%",
	      "innerRadius": "25%"
	    }, {
	      "color": "#39BB9D",
	      "startValue": 0,
	      "endValue": jsonData[0].cpu,
	      "radius": "40%",
	      "innerRadius": "25%",
	      "balloonText": jsonData[0].cpu+"%"
	    }]
	  }],
	  "allLabels": [{
	    "text": "HDD",
	    "x": "49%",
	    "y": "9%",
	    "size": 16,
	    "bold": true,
	    "color": "#E56542",
	    "align": "right"
	  }, {
	    "text": "Memory",
	    "x": "49%",
	    "y": "18%",
	    "size": 16,
	    "bold": true,
	    "color": "#801F63",
	    "align": "right"
	  }, {
	    "text": "CPU",
	    "x": "49%",
	    "y": "28%",
	    "size": 16,
	    "bold": true,
	    "color": "#39BB9D",
	    "align": "right"
	  }],
	  "export": {
	    "enabled": false
	  }
	});
}
	
var logList;
function intlogList() {
	$("#table_log1").find('tbody').find('tr').remove();

	for (var i = 0; i < 5; ++i) {
		$("<tr style='text-align: center;'>").appendTo(
				$("#table_log1").find('tbody')).append($("<TD>").text("test0")// .text(strToDateTime(check_null(data.dashboard_recent[i].proc_date)))
		).append($("<TD>").text("test" + i))
		.append($("<TD>").text("test" + i))
				.append($("<TD>").text("test" + i))
				.append($("<TD>").text("test" + i))
				.append($("<TD>").text("test" + i))
	}
}

var logData1;
function drawchart6(data){
	$("#table_log1").find('tbody').find('tr').remove();
	if(logData1 == null) {
		logData1 = new Array();
		var jsonData = JSON.parse(data);
		logData1 = jsonData;
	} else {
		var tmpData = logData1[0];
		logData1.shift();
		logData1.push(tmpData);
	}
	for (var i = 0; i < logData1.length; i++) {
		$("<tr style='text-align: center;'>").appendTo($("#table_log1").find('tbody'))
		.append($("<TD style='height:15px; font-size:13px'>")
				.text(logData1[i].system)//.text(strToDateTime(check_null(data.dashboard_recent[i].proc_date)))
		)
		.append($("<TD style='height:15px; font-size:13px'>")
				.text(getShortDateFormat(new Date()))
		)
		.append($("<TD style='height:15px; font-size:13px'>")
				.text(logData1[i].cate)
		)
		.append($("<TD style='height:15px; font-size:13px'>")
				.text(logData1[i].id)
		)
		.append($("<TD style='height:15px; font-size:13px'>")
				.text(logData1[i].name)
		)
		.append($("<TD style='height:15px; font-size:13px'>")
				.text(logData1[i].ip)
		)
	//.fadeOut()
	.fadeIn("slow");
		
	}
}

var logData3;
function drawchart14(data){
	$("#table_log3").find('tbody').find('tr').remove();
	if(logData3 == null) {
		logData3 = new Array();
		var jsonData = JSON.parse(data);
		logData3 = jsonData;
	} else {
		var tmpData = logData3[0];
		logData3.shift();
		logData3.push(tmpData);
	}
	for (var i = 0; i < logData3.length; i++) {
		$("<tr style='text-align: center;'>").appendTo($("#table_log3").find('tbody'))
		.append($("<TD style='height:15px; font-size:13px'>")
				.text(logData3[i].name)//.text(strToDateTime(check_null(data.dashboard_recent[i].proc_date)))
		)
		.append($("<TD style='height:15px; font-size:13px'>")
				.text(logData3[i].logcnt)
		)
		.append($("<TD style='height:15px; font-size:13px'>")
				.text(logData3[i].cnt)
		)
	//.fadeOut()
	.fadeIn("slow");
		
	}
}

var logData2;
function drawchart11(data){
	$("#table_log2").find('tbody').find('tr').remove();
	if(logData2 == null) {
		logData2 = new Array();
		var jsonData = JSON.parse(data);
		logData2 = jsonData;
	} else {
		var tmpData = logData2[0];
		logData2.shift();
		logData2.push(tmpData);
	}
	for (var i = 0; i < logData2.length; i++) {
		$("<tr style='text-align: center;'>").appendTo($("#table_log2").find('tbody'))
		.append($("<TD style='height:15px; font-size:13px'>")
				.text(logData2[i].name)//.text(strToDateTime(check_null(data.dashboard_recent[i].proc_date)))
		)
		.append($("<TD style='height:15px; font-size:13px'>")
				.text(logData2[i].logcnt)
		)
		.append($("<TD style='height:15px; font-size:13px'>")
				.text(logData2[i].cnt)
		)
	//.fadeOut()
	.fadeIn("slow");
		
	}
}
	
	
