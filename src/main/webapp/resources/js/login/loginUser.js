/**
 * Login Javascript
 * @author tjlee
 * @since 2015. 05. 07
 */

$(document).ready(function() {
	$("input[name=login_id]").focus();
	
	// 로그인 버튼 클릭 시
	$("#btn_login").click(function() {
		var userType = $("input[name=userType]:checked").val();
		
		// 관리자
		if(userType == "A") {
			loginSubmit();
		}

		// 사용자
		else if(userType == "E") {
			//alert("준비중입니다.");
			loginSubmit();
//			location.href = "/eframe_psm/loginUser.psm?emp_user_id=" + $('login_id').val();
		}
	});
	
	// 비밀번호칸에서 엔터 입력 시
	$("input[name=login_id]").keydown(function(e){
		if(e.which == 13){
			var userType = $("input[name=userType]:checked").val();
			
			// 관리자
			if(userType == "A") {
				loginSubmit();
			}

			// 사용자
			else if(userType == "E") {
				loginSubmit();
			}
		}
	});
});


// 로그인 시도
function loginSubmit(){
	var emp_user_id = $("input[name=login_id]").val();
	var system_seq = $('#system_seq').val();
	sendAjaxPostRequest(
		"loginUser.psm"
		,{
			emp_user_id : emp_user_id,
			system_seq : system_seq
		}
		,loginSuccessHandler
		,loginErrorHandler
	);
}

function loginSuccessHandler(data, dataType, actionType) {
	if(data.hasOwnProperty("statusCode")) {
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			var log_message = "로그인 실패 : " + message;
			
//			addManagerActHist(null, log_message, "SELECT");
			
			alert(message);
		}
	} else {
//		var log_message = "로그인 성공 : " + data.paramBean.admin_user_id;
		
//		addManagerActHist(null, log_message, "SELECT");
		location.href = "callingReplyUser/list.psm";
	}
}

function loginErrorHandler(XMLHttpRequest, textStatus, errorThrown, actionType) {
	var log_message = "로그인 실패 : " + textStatus;
	
//	addManagerActHist(null, log_message, "SELECT");
	
	alert("로그인 실패하였습니다." + textStatus);
}

function addManagerActHist(menu_id, log_message, log_action) {
	$.ajax({
		type: 'POST',
		url: 'managerActHist/add.html',
		data: {
			menu_id : menu_id,
			log_message : log_message,
			log_action : log_action
		}
	});
}
