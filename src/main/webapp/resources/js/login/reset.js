/**
 * 
 */

var reset = {
	
	init : function() {
		
		reset.event();
	},
	
	event : function(){
		
		$("#chkIdBtn").on("click",function(){
			
			reset.chkEmail();
		})
		
		$("#closeWin").on("click",function(){
			reset.closeWin();
		})
	},
	
	chkEmail : function(){
		
		var id = $("#myId").val();
		
		$.ajax({
			type : "GET",
			url : "chkEmail.html",
			data : {
				id : id
			},
			success : function(data){
				
				if(data==="\"1\""){
					$("#infoTxt").empty();
					$("#infoTxt").append("<h5 style='color: red;'>등록된 아이디가 없습니다.</h5>");
					console.log(data);
				}else{
					console.log(data);
					$("#infoTxt").empty();
					$("#infoTxt").append("<h5 style='color: red;'>해당 아이디에 등록된 이메일로 임시 비밀번호가 전송되었습니다.</h5>");
				}
			},
			error: function (request, status, error){
		        console.log('code: '+request.status+"\n"+'message: '+request.responseText+"\n"+'error: '+error);
		    }
		})
		
	},
	
	closeWin : function(){
		
		self.close(); 
	}
	
}

$(document).ready(function() {
	
	reset.init();	

});
