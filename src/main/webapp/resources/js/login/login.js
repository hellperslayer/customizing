/**
 * Login Javascript
 * @author yjyoo
 * @since 2015. 05. 07
 */

$(document).ready(function() {
	$("input[name=login_id]").focus();
	
	// 로그인 버튼 클릭 시
	$("#btn_login").click(function() {
		var userType = $("input[name=userType]:checked").val();
		
		// 관리자
		if(userType == "A") {
			loginSubmit();
		}

		// 사용자
		else if(userType == "E") {
			alert("준비중입니다.");
		}
	});
	
	// 비밀번호칸에서 엔터 입력 시
	$("input[name=login_pw]").keydown(function(e){
		if(e.which == 13){
			var userType = $("input[name=userType]:checked").val();
			
			// 관리자
			if(userType == "A") {
				loginSubmit();
			}

			// 사용자
			else if(userType == "E") {
				alert("준비중입니다.");
			}
		}
	});
});


// 로그인 시도
function loginSubmit(){
	var userid = $("input[name=login_id]").val();
	var pw = $("input[name=login_pw]").val();
	
	// rsa 암호화
	var rsa = new RSAKey();
	rsa.setPublic($('#RSAModulus').val(),$('#RSAExponent').val());

	var admin_user_id = rsa.encrypt(userid);
	
	var password = "";
	if (pw != "") {
		password = rsa.encrypt(pw);
	}
	
	sendAjaxPostRequest(
		"login.html"
		,{
			admin_user_id : admin_user_id,
			password : password
		}
		,loginSuccessHandler
		,loginErrorHandler
	);
}

function loginSuccessHandler(data, dataType, actionType) {
	if(data.hasOwnProperty("statusCode")) {
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			var log_message = "로그인 실패 : " + message;
			
			addManagerActHist('login', log_message, "SELECT");
			
			alert(message);
		}
	} else {
		// 패스워드 변경 팝업 구현 
//		alert("접속시간  : " + data.isRePassword);
		if(data.isRePassword == "Y"){
			if(data.isRePassword_sha == "Y") {
				$("#afterChange_btn").hide();
				$("#close_btn").hide();
			} else if(data.isInitLogin == "Y") {
				$("#afterChange_btn").hide();
				$("input[name=isInitLogin]").val("Y");
			}
			$('.popup_wrap').show();
			$('.rePasswordDiv').css("z-index", "6");
			$('input[name=currentPasswd]').focus();
		}else{
			var log_message = "로그인 성공 : " + data.paramBean.admin_user_id;
			
			if(data.isInspection == "Y") {
				alert("점검 예정일은 " + data.inspection_date.substring(0,4) + "년 " + data.inspection_date.substring(4,6) + "월 " + data.inspection_date.substring(6,8) + "일 " + "입니다.");
			}
			
			addManagerActHist('login', log_message, "SELECT");
			location.href = "main.html";
		}
	}
}

function loginErrorHandler(XMLHttpRequest, textStatus, errorThrown, actionType) {
	var log_message = "로그인 실패 : " + textStatus;
	
	addManagerActHist('login', log_message, "SELECT");
	
	alert("로그인 실패하였습니다.");
}

function addManagerActHist(menu_id, log_message, log_action) {
	$.ajax({
		type: 'POST',
		url: 'managerActHist/add.html',
		data: {
			menu_id : menu_id,
			log_message : log_message,
			log_action : log_action
		}
	});
}

//비밀번호 변경 팝업 닫기
function rePasswordPass(adminUserId){
	var log_message = "로그인 성공 : " + adminUserId;
	
	addManagerActHist('login', log_message, "SELECT");
	location.href = "main.html";
}

function closeDate(){
	$('.popup_wrap2').hide();
}

// 비밀번호 다음에 변경
function afterChange(adminUserId){
	var log_message = "로그인 성공 : " + adminUserId;
	
	addManagerActHist('login', log_message, "SELECT");
	location.href = "main.html";
}


//비밀번호 변경 시도 
function passwdChange(){
	var checkPw = checkPasswd();
	if(checkPw){
		var $form = $("#rePasswordForm");
		var log_message_title = '비밀번호 변경';
		var log_action = 'UPDATE';
		var admin_user_id = $("input[name=login_id]").val();
		//var log_message_params = 'admin_user_id,currentPasswd,newPasswd';
		var log_message_params = 'admin_user_id';
		var menu_id = $('input[name=current_menu_id]').val();
		var isInitLogin = $('input[name=isInitLogin]').val();
		
		$form.appendLogMessageParamsInput(log_message_title, log_action, log_message_params, menu_id);
		sendAjaxPostRequest("rePassword.html",$form.serialize() + "&admin_user_id=" + admin_user_id, rePasswordSuccessHandler, rePasswordErrorHandler, "save");
	}
}

//비밀번호 변경 SuccessHandler
function rePasswordSuccessHandler(data, dataType, actionType){


	if(data.hasOwnProperty("statusCode")) {
		if(data.hasOwnProperty("message")) {
			var message = data.message;
			var log_message = "비밀번호 변경 실패 : " + message;
			
			addManagerActHist('login', log_message, "SELECT");
			
			alert(message);
		}
	} else {
		if(data.res == "SUCCESS"){
			var log_message = "로그인 성공 : " + data.paramBean.admin_user_id;
			addManagerActHist('login', log_message, "SELECT");
			var log_message = "비밀번호 변경 성공 : " + data.paramBean.admin_user_id;
			addManagerActHist('login', log_message, "SELECT");
			
			if (data.paramBean.isInitLogin == "Y") {
				alert("변경되었습니다.");
				location.href = "loginView.html";
			} else {
				location.href = "main.html";
			}
		}else if( data.res == "ERROR01"){
			alert("비밀번호는 9 ~ 20자 사이의 문자, 숫자, 특수문자 조합으로 구성되어야합니다. (공백문자 제외)");
		}
		
	}
}

//비밀번호 변경 ErrorHandler
function rePasswordErrorHandler(XMLHttpRequest, textStatus, errorThrown, actionType) {
	var log_message = "비밀번호 변경 실패 : " + textStatus;
	
	addManagerActHist('login', log_message, "SELECT");
	
	alert("비밀번호 변경 실패하였습니다." + textStatus);
}

//관리자 비밀번호 검사 (정규식 패턴)
function checkPasswd(){
	var currentPasswd = $("#rePasswordForm input[name=currentPasswd]").val();
	var newPasswd = $("#rePasswordForm input[name=newPasswd]").val();
	var re_newPasswd = $("#rePasswordForm input[name=re_newPasswd]").val();
	var password = $("#login_pw").val();
	//var regex = /^.*(?=^.{9,20}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[~,!,@,#,$,%,^,&,*,(,),=,+,_,.,|]).*$/;

//	if(password != currentPasswd){
//		alert("현재 비밀번호를 정확하게 입력해 주세요.");
//		return false;
//	}
	
	if ( currentPasswd == newPasswd ) {
		alert("기존 비밀번호와 다르게 입력해 주세요.");
		return false;
	}
	
	if(newPasswd != '' && re_newPasswd != '') {
		/*if(!regex.test(newPasswd)){
			alert("비밀번호는 9 ~ 20자 사이의 문자, 숫자, 특수문자 조합으로 구성되어야합니다. (공백문자 제외)");
			$('input[name=password]').select();
			return false;
		}*/
		
		if(newPasswd != re_newPasswd){
			alert("비밀번호와 비밀번호 확인이 일치하지 않습니다.");
			$('input[name=password]').select();
			return false;
		}
		
		/*if(newPasswd.indexOf(" ") > -1 ){
			alert("비밀번호에 공백문자를 포함할 수 없습니다.");
			$('input[name=password]').select();
			return false;
		}*/
	}else{
		alert("필수항목을 모두 입력해주세요.");
		return false;
	}
	return true;	
}
