package com.easycerti.psm.log.generator.util;

import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

public class CommandClient {
	public static void main(String[] args) {
		//System.out.println( CommandClient.sendCommand("192.168.1", 56091, "CheckAgentEnable", "tmp") );
//		System.out.println( CommandClient.sendCommand("localhost", 56091, args[0], args[1]) );
//		System.out.println( CommandClient.sendCommand("192.168.50.32", 56091, "ez.mon.log.filegroup.cnt", "200") );
//		System.out.println( CommandClient.sendCommand("192.168.50.32", 56091, "ez.send.thread.num", "3") );
	}
	
	public static String sendCommand(String ip, int port, String type, String value){
		CommandObj cmdObj = new CommandObj();
		cmdObj.setCommandType(type);
		cmdObj.setCommandValue(value);
		
		return CommandClient.sendCommand(ip, port, cmdObj);
	}

	public static String sendCommand(String ip, int port, CommandObj cmdObj){
		String rcvStr = null;
		Socket sock = null;
		InputStream is = null;
		ObjectOutputStream oos = null;
		try {
			sock = new Socket();
			SocketAddress s_addr = new InetSocketAddress(ip,port);
			sock.connect(s_addr, 1000*5);
			//os = ;
			is = sock.getInputStream();
			
			oos = new ObjectOutputStream(sock.getOutputStream());
			
			oos.writeObject(cmdObj);
			oos.flush();
			
			byte[] rcvData = new byte[1024]; 
			is.read(rcvData);
			rcvStr = new String(rcvData);
		} catch (Exception e) {
			//e.printStackTrace();
		} finally {
			if (is != null) try { is.close(); } catch (Exception e2) { e2.printStackTrace(); }
			if (oos != null) try { oos.close(); } catch (Exception e2) { e2.printStackTrace(); }
			if (sock != null) try { sock.close(); } catch (Exception e2) { e2.printStackTrace(); }
		}
		
		return rcvStr;
	}
}
