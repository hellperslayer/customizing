package com.easycerti.psm.log.generator.util;

import java.io.Serializable;

public class CommandObj implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -94840956509047517L;

	private String commandType;
	private String commandValue;
	public String getCommandType() {
		return commandType;
	}
	public void setCommandType(String commandType) {
		this.commandType = commandType;
	}
	public String getCommandValue() {
		return commandValue;
	}
	public void setCommandValue(String commandValue) {
		this.commandValue = commandValue;
	}
}

