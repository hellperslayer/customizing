package com.easycerti.eframeuser.psm.calling_management.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.RedirectView;

import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.psm.calling_management.service.CallingDemandSvc;
import com.easycerti.eframe.psm.calling_management.service.CallingReplyUserSvc;
import com.easycerti.eframe.psm.calling_management.type.Status;
import com.easycerti.eframe.psm.calling_management.vo.CallingDemand;
import com.easycerti.eframe.psm.calling_management.vo.CallingSearch;
import com.easycerti.eframe.psm.system_management.vo.EmpUser;
/**
 * 
 * 설명 : 소명답변(사용자) Controller
 * @author tjlee
 * @since 2015. 5. 13.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 13.           tjlee            최초 생성
 *
 * </pre>
 */
@Controller
@RequestMapping(value="/callingReplyUser/*")
public class CallingReplyUserCtrl {

	private final static Logger logger = LoggerFactory.getLogger(CallingReplyUserCtrl.class);
	
	@Autowired
	private CallingReplyUserSvc callingReplyUserSvc;
	@Autowired
	private CallingDemandSvc callingDemandSvc;
	
	@RequestMapping(value="list.psm",method={RequestMethod.POST, RequestMethod.GET})
	public DataModelAndView findCallingReplyUserList(@ModelAttribute("search") CallingSearch search, 
			@RequestParam Map<String, String> parameters, 
			HttpServletRequest request, ModelMap modelMap) {
		logger.info("################# CallingReplyUserCtrl - findCallingReplyUserList with {}", search);
		
		// 날짜 확인 및 설정
		EmpUser empUser = SystemHelper.findSessionEmpUser(request);
		if(empUser == null){
			DataModelAndView modelAndView = new DataModelAndView();
			RedirectView redirectView = new RedirectView(request.getContextPath() + "/loginUserView.psm");
			redirectView.setExposeModelAttributes(false);
			modelAndView.setView(redirectView);
			return modelAndView;
		}
		search.initDatesWithDefaultIfEmpty();
		search.setEmp_user_id(empUser.getEmp_user_id());
		
		modelMap.put("replyList", callingReplyUserSvc.findCallingReplyUserList(search));
		modelMap.put("paramBean", parameters);
		modelMap.put("search", search);
		
		return new DataModelAndView("callingReplyUser.list", modelMap);
	}
	
	@RequestMapping(value = "add.psm", method = { RequestMethod.POST })
	public DataModelAndView addCallingReplyOne(
			@ModelAttribute CallingDemand callingDemand,
			@RequestParam Map<String, String> parameters,
			HttpServletRequest request, ModelMap modelMap) {

		logger.info("################# CallingReplyUserCtrl - addCallingReplyOne with {}", callingDemand);
		
		EmpUser empUser = SystemHelper.findSessionEmpUser(request);
		callingDemand.setInserter_id(empUser.getEmp_user_id());
		callingDemand.setInserter_dept_id(empUser.getDept_id());
		callingReplyUserSvc.addCallingReply(callingDemand);
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@RequestMapping(value="detail.psm",method={RequestMethod.POST, RequestMethod.GET})
	public DataModelAndView findCallingReplyUserDetail(@RequestParam int cll_dmnd_id, 
			@RequestParam Map<String, String> parameters, CallingSearch search,
			ModelMap modelMap, HttpServletRequest request) {
		
		EmpUser empUser = SystemHelper.findSessionEmpUser(request);
		if(empUser == null){
			DataModelAndView modelAndView = new DataModelAndView();
			RedirectView redirectView = new RedirectView(request.getContextPath() + "/loginUserView.psm");
			redirectView.setExposeModelAttributes(false);
			modelAndView.setView(redirectView);
			return modelAndView;
		}
		
		CallingDemand demand = callingDemandSvc.findCallingDemandOne(cll_dmnd_id);
		System.out.println(demand.getStatus());
		String pageTitle = Status.DEMAND.equals(demand.getStatus())?"소명응답":"소명응답 상세";
		
		modelMap.put("demand", demand);
		modelMap.put("search", search);
		modelMap.put("paramBean", parameters);
		modelMap.put("userType", "user");
		modelMap.put("activeStatus", "user");
		modelMap.put("pageTitle", pageTitle);
		modelMap.put("bizLogUrl", "callingReplyUser/bizLogList.psm");
		
		return new DataModelAndView("callingReplyUser.detail", modelMap);
	}
	
	@RequestMapping(value="bizLogList.psm", method = RequestMethod.GET)
	public @ResponseBody Map<String, ?> findCallingBizLogListAjax(
			@RequestParam long emp_detail_seq, @RequestParam int page_num) {
		Map<String, Object> map = callingDemandSvc.findCallingBizLogList(emp_detail_seq, page_num);
		return map;
	}
}
