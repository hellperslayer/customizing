package com.easycerti.eframeuser.psm.calling_management.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.psm.system_management.service.EmpUserMngtSvc;

/**
 * 
 * 설명 : 로그인 컨트롤러( 사용자 ) 
 * @author tjlee
 * @since 2015. 6. 12.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 6. 12.           tjlee            최초 생성
 *
 * </pre>
 */
@Controller
public class LoginCtrlUser {

	@Autowired
	private EmpUserMngtSvc empUserMngtSvc;
	
	/**
	 * 설명 : 사용자 로그인 처리
	 * @author tjlee
	 * @since 2015. 6. 11.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="loginUser.psm", method={RequestMethod.POST})
	public ModelAndView loginUser(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		String[] args = {"emp_user_id", "system_seq"};
		
		if( !CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		DataModelAndView modelAndView = empUserMngtSvc.login(parameters, request);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	/**
	 * 설명 : 사용자 로그인 화면 
	 * @author tjlee
	 * @since 2015. 6. 16.
	 * @return ModelAndView
	 */
	@RequestMapping(value="loginUserView.psm",method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView loginUserView(@RequestParam Map<String,String> parameters, HttpServletRequest request){
		DataModelAndView modelAndView = new DataModelAndView("loginUser");
		return modelAndView;
	}
	
	/**
	 * 로그아웃 처리
	 * 
	 * @author tjlee
	 * @since 2015. 6. 16.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="logout.psm", method={RequestMethod.GET, RequestMethod.POST})
	public DataModelAndView logout(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		
		CommonHelper.removeSessionUser(request);
		CommonHelper.removeSessionUserType(request);
		
		DataModelAndView modelAndView = new DataModelAndView();
		RedirectView redirectView = new RedirectView("loginUserView.psm");
		redirectView.setExposeModelAttributes(false);
		modelAndView.setView(redirectView);
		
		return modelAndView;
	}
}
