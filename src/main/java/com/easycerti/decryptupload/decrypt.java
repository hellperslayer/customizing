package com.easycerti.decryptupload;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

public class decrypt {

	private static SecretKey getKey() throws InvalidKeySpecException, NoSuchAlgorithmException, InvalidKeyException {

		final String key = "easycerti_download_map_url";
		byte[] desKeyData = key.getBytes();
		DESKeySpec desKeySpec = new DESKeySpec(desKeyData);
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
		return keyFactory.generateSecret(desKeySpec);
	}

	public static void decryptFile(String infile, String outfile) throws Exception {

		// 파일 처리용 버퍼 크기
		final int BUFFER_SIZE = 8192;

		// 비밀키 생성
//		KeyGenerator keygenerator = KeyGenerator.getInstance("DES");
//		SecretKey secretKey = keygenerator.generateKey();

		Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, getKey());

		FileInputStream in = new FileInputStream(infile);
		FileOutputStream fileOut = new FileOutputStream(outfile);

		CipherOutputStream out = new CipherOutputStream(fileOut, cipher);
		byte[] buffer = new byte[BUFFER_SIZE];
		int length;
		while ((length = in.read(buffer)) != -1)
			out.write(buffer, 0, length);
		in.close();
		out.close();
	}

}
