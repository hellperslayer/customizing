package com.easycerti.eframe.psm.calling_management.vo;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

public class CallingBizLog implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 8196104843383884741L;

	// fields
	private long log_seq;
	private String proc_date;
	private String proc_time;
	private String user_ip;
	private String scrn_id;
	private String scrn_name;
	private String req_type;
	private String system_seq;
	private String server_seq;
	private String req_context;
	private String req_url;
	private String req_end_time;
	private String inserter_id;
	private Timestamp inserted_date;
	
	private List<CallingBizLogResult> bizLogResultList;
	
	// getters and setters
	public long getLog_seq() {
		return log_seq;
	}
	public void setLog_seq(long log_seq) {
		this.log_seq = log_seq;
	}
	public String getProc_date() {
		return proc_date;
	}
	public void setProc_date(String proc_date) {
		this.proc_date = proc_date;
	}
	public String getProc_time() {
		return proc_time;
	}
	public void setProc_time(String proc_time) {
		this.proc_time = proc_time;
	}
	public String getUser_ip() {
		return user_ip;
	}
	public void setUser_ip(String user_ip) {
		this.user_ip = user_ip;
	}
	public String getScrn_id() {
		return scrn_id;
	}
	public void setScrn_id(String scrn_id) {
		this.scrn_id = scrn_id;
	}
	public String getScrn_name() {
		return scrn_name;
	}
	public void setScrn_name(String scrn_name) {
		this.scrn_name = scrn_name;
	}
	public String getReq_type() {
		return req_type;
	}
	public void setReq_type(String req_type) {
		this.req_type = req_type;
	}
	public String getSystem_seq() {
		return system_seq;
	}
	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}
	public String getServer_seq() {
		return server_seq;
	}
	public void setServer_seq(String server_seq) {
		this.server_seq = server_seq;
	}
	public String getReq_context() {
		return req_context;
	}
	public void setReq_context(String req_context) {
		this.req_context = req_context;
	}
	public String getReq_url() {
		return req_url;
	}
	public void setReq_url(String req_url) {
		this.req_url = req_url;
	}
	public String getReq_end_time() {
		return req_end_time;
	}
	public void setReq_end_time(String req_end_time) {
		this.req_end_time = req_end_time;
	}
	public String getInserter_id() {
		return inserter_id;
	}
	public void setInserter_id(String inserter_id) {
		this.inserter_id = inserter_id;
	}
	public Timestamp getInserted_date() {
		return inserted_date;
	}
	public void setInserted_date(Timestamp inserted_date) {
		this.inserted_date = inserted_date;
	}
	
	public List<CallingBizLogResult> getBizLogResultList() {
		return bizLogResultList;
	}
	public void setBizLogResultList(List<CallingBizLogResult> bizLogResultList) {
		this.bizLogResultList = bizLogResultList;
	}
	
	// toString
	@Override
	public String toString() {
		return "CallingBizLog [log_seq=" + log_seq + ", proc_date=" + proc_date
				+ ", proc_time=" + proc_time + ", user_ip=" + user_ip
				+ ", scrn_id=" + scrn_id + ", scrn_name=" + scrn_name
				+ ", req_type=" + req_type + ", system_seq=" + system_seq
				+ ", server_seq=" + server_seq + ", req_context=" + req_context
				+ ", req_url=" + req_url + ", req_end_time=" + req_end_time
				+ ", inserter_id=" + inserter_id + ", inserted_date="
				+ inserted_date + ", bizLogResultList=" + bizLogResultList
				+ "]";
	}
}
