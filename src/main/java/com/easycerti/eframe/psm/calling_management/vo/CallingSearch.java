package com.easycerti.eframe.psm.calling_management.vo;

import com.easycerti.eframe.common.vo.SearchBase;

public class CallingSearch extends SearchBase {

	// fields
	private String demander_name;
	private String demander_dept_name;
	private String emp_user_id;
	private String emp_user_name;
	private String emp_dept_name;
	private String status;
	private String judge_name;
	private String loginUserId;
	
	private long emp_detail_seq;
	private String listUrl;
	
	// constructors
	public CallingSearch() {super();}
	public CallingSearch(int page_num, int page_size) {
		super(page_num, page_size);
	}
	
	// getters and setters
	public String getDemander_name() {
		return (demander_name == null?"":demander_name);
	}
	public void setDemander_name(String demander_name) {
		this.demander_name = demander_name;
	}
	public String getDemander_dept_name() {
		return (demander_dept_name == null?"":demander_dept_name);
	}
	public void setEmp_dept_name(String emp_dept_name) {
		this.emp_dept_name = emp_dept_name;
	}
	public String getEmp_user_id() {
		return (emp_user_id == null?"":emp_user_id);
	}
	public void setEmp_user_id(String emp_user_id) {
		this.emp_user_id = emp_user_id;
	}
	public String getEmp_user_name() {
		return (emp_user_name == null?"":emp_user_name);
	}
	public void setEmp_user_name(String emp_user_name) {
		this.emp_user_name = emp_user_name;
	}
	public String getEmp_dept_name() {
		return (emp_dept_name == null?"":emp_dept_name);
	}
	public void setDemander_dept_name(String demander_dept_name) {
		this.demander_dept_name = demander_dept_name;
	}
	public long getEmp_detail_seq() {
		return emp_detail_seq;
	}
	public void setEmp_detail_seq(long emp_detail_seq2) {
		this.emp_detail_seq = emp_detail_seq2;
	}
	public String getListUrl() {
		return listUrl;
	}
	public void setListUrl(String listUrl) {
		this.listUrl = listUrl;
	}
	public String getStatus() {
		return (status == null?"":status);
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getJudge_name() {
		return judge_name;
	}
	public void setJudge_name(String judge_name) {
		this.judge_name = judge_name;
	}
	public String getLoginUserId() {
		return loginUserId;
	}
	public void setLoginUserId(String loginUserId) {
		this.loginUserId = loginUserId;
	}
	
	// toString
	@Override
	public String toString() {
		return "CallingSearch [demander_name=" + demander_name
				+ ", demander_dept_name=" + demander_dept_name
				+ ", emp_user_id=" + emp_user_id + ", emp_user_name="
				+ emp_user_name + ", emp_dept_name=" + emp_dept_name
				+ ", status=" + status + ", judge_name=" + judge_name
				+ ", loginUserId=" + loginUserId + ", emp_detail_seq="
				+ emp_detail_seq + ", listUrl=" + listUrl + "]";
	}
}
