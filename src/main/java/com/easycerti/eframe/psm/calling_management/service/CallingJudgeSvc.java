package com.easycerti.eframe.psm.calling_management.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.calling_management.vo.CallingJudge;
import com.easycerti.eframe.psm.calling_management.vo.CallingSearch;

public interface CallingJudgeSvc {
	
	List<Map<String, String>> findCallingJudgeList(CallingSearch search);
	List<Map<String, String>> findCallingStandByList(CallingSearch search);
	
	DataModelAndView findCallJdgmnOne(Map<String, String> parameters, HttpServletRequest request);
	
	Map<String, Object> findCallingDemandOne(int cll_dmnd_id);
	
	void addCallingJudgeOne(CallingJudge judge);
}
