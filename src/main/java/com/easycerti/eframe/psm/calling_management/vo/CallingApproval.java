package com.easycerti.eframe.psm.calling_management.vo;

import java.io.Serializable;
import java.sql.Timestamp;

import com.easycerti.eframe.core.type.YesNo;
import com.easycerti.eframe.psm.calling_management.type.Judge;

public class CallingApproval implements Serializable {

	private static final long serialVersionUID = 2738413073053062419L;

	// fields
	private int cll_dmnd_id;
	private short repeat_cnt;
	private short approval_seq;
	private String approver_id;
	private String approver_name;
	private String approver_dept_id;
	private String approver_dept_name;
	private YesNo is_approved;
	private Judge judge_result;
	private String judge;
	private String inserter_id;
	private Timestamp inserted_date;
	private String updater_id;
	private Timestamp updated_date;
	
	// methods
	public boolean isApprover(String user_id) {
		if (approver_id.equals(user_id))
			return true;
		return false;
	}
	
	// getters and setters
	public int getCll_dmnd_id() {
		return cll_dmnd_id;
	}
	public void setCll_dmnd_id(int cll_dmnd_id) {
		this.cll_dmnd_id = cll_dmnd_id;
	}
	public short getRepeat_cnt() {
		return repeat_cnt;
	}
	public void setRepeat_cnt(short repeat_cnt) {
		this.repeat_cnt = repeat_cnt;
	}
	public short getApproval_seq() {
		return approval_seq;
	}
	public void setApproval_seq(short approval_seq) {
		this.approval_seq = approval_seq;
	}
	public String getApprover_id() {
		return approver_id;
	}
	public void setApprover_id(String approver_id) {
		this.approver_id = approver_id;
	}
	public String getApprover_name() {
		return approver_name;
	}
	public void setApprover_name(String approver_name) {
		this.approver_name = approver_name;
	}
	public String getApprover_dept_id() {
		return approver_dept_id;
	}
	public void setApprover_dept_id(String approver_dept_id) {
		this.approver_dept_id = approver_dept_id;
	}
	public String getApprover_dept_name() {
		return approver_dept_name;
	}
	public void setApprover_dept_name(String approver_dept_name) {
		this.approver_dept_name = approver_dept_name;
	}
	public YesNo getIs_approved() {
		return (is_approved == null?YesNo.NO:is_approved);
	}
	public void setIs_approved(YesNo is_approved) {
		this.is_approved = is_approved;
	}
	public Judge getJudge_result() {
		return judge_result;
	}
	public void setJudge_result(Judge judge_result) {
		this.judge_result = judge_result;
	}
	public String getJudge() {
		return judge;
	}
	public void setJudge(String judge) {
		this.judge = judge;
	}
	public String getInserter_id() {
		return inserter_id;
	}
	public void setInserter_id(String inserter_id) {
		this.inserter_id = inserter_id;
	}
	public Timestamp getInserted_date() {
		return inserted_date;
	}
	public void setInserted_date(Timestamp inserted_date) {
		this.inserted_date = inserted_date;
	}
	public String getUpdater_id() {
		return updater_id;
	}
	public void setUpdater_id(String updater_id) {
		this.updater_id = updater_id;
	}
	public Timestamp getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(Timestamp updated_date) {
		this.updated_date = updated_date;
	}

	// toString
	@Override
	public String toString() {
		return "CallingApproval [cll_dmnd_id=" + cll_dmnd_id + ", repeat_cnt="
				+ repeat_cnt + ", approval_seq=" + approval_seq
				+ ", approver_id=" + approver_id + ", approver_name="
				+ approver_name + ", approver_dept_id=" + approver_dept_id
				+ ", approver_dept_name=" + approver_dept_name
				+ ", is_approved=" + is_approved + ", judge_result="
				+ judge_result + ", judge=" + judge + ", inserter_id="
				+ inserter_id + ", inserted_date=" + inserted_date
				+ ", updater_id=" + updater_id + ", updated_date="
				+ updated_date + "]";
	}
}
