package com.easycerti.eframe.psm.calling_management.service;

import java.util.List;
import java.util.Map;

import com.easycerti.eframe.psm.calling_management.vo.CallingDemand;
import com.easycerti.eframe.psm.calling_management.vo.CallingSearch;
/**
 * 
 * 설명 : 소명답변(사용자) Svc 
 * @author tjlee
 * @since 2015. 5. 13.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 13.           tjlee            최초 생성
 *
 * </pre>
 */
public interface CallingReplyUserSvc {
	/**
	 * 설명 : 소명요청 리스트출력
	 * @author tjlee
	 * @since 2015. 5. 13.
	 * @return DataModelAndView
	 */
	List<Map<String, String>> findCallingReplyUserList(CallingSearch search);
	void addCallingReply(CallingDemand demand);
//	/**
//	 * 설명 : 소명요청 리스트 상세 출력
//	 * @author tjlee
//	 * @since 2015. 5. 13.
//	 * @return DataModelAndView
//	 */
//	public DataModelAndView findCallingReplyUserDetail(Map<String, String> parameters, HttpServletRequest request);
//
//	/**
//	 * 설명 : 소명답변
//	 * @author tjlee
//	 * @since 2015. 5. 13.
//	 * @return DataModelAndView
//	 */
//	public DataModelAndView saveCallingReplyUserDetailReply(Map<String, String> parameters, HttpServletRequest request);
	
}
