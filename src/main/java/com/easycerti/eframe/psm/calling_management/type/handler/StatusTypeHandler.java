package com.easycerti.eframe.psm.calling_management.type.handler;

import com.easycerti.eframe.core.type.handler.AbstractSymbolicTypeHandler;
import com.easycerti.eframe.psm.calling_management.type.Status;

public class StatusTypeHandler extends AbstractSymbolicTypeHandler<Status> {
	public StatusTypeHandler(Class<Status> javaType) {
		super(javaType);
	}
}
