package com.easycerti.eframe.psm.calling_management.type;

import com.easycerti.eframe.core.type.Symbolic;

public enum Judge implements Symbolic {

	PROPER("0", "적정"), NONPROPER("1", "부적정"),REDEMAND("2", "재소명");

	private String symbol;
	private String name;

	Judge(String symbol, String name) {
		this.symbol = symbol;
		this.name = name;
	}

	public String getSymbol() {
		return symbol;
	}

	public String getName() {
		return name;
	}
}
