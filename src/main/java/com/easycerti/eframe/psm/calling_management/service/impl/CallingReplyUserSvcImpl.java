package com.easycerti.eframe.psm.calling_management.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.easycerti.eframe.psm.calling_management.dao.CallingDemandDao;
import com.easycerti.eframe.psm.calling_management.dao.CallingReplyUserDao;
import com.easycerti.eframe.psm.calling_management.service.CallingReplyUserSvc;
import com.easycerti.eframe.psm.calling_management.type.Status;
import com.easycerti.eframe.psm.calling_management.vo.CallingDemand;
import com.easycerti.eframe.psm.calling_management.vo.CallingSearch;
import com.easycerti.eframe.psm.search.dao.ExtrtCondbyInqDao;
/**
 * 
 * 설명 : 소명응답(사용자) SvcImpl 
 * @author tjlee
 * @since 2015. 5. 12.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 12.           tjlee            최초 생성
 *
 * </pre>
 */
@Service
public class CallingReplyUserSvcImpl implements CallingReplyUserSvc {
	
	@Autowired
	private ExtrtCondbyInqDao extrtCondbyInqDao;
	@Autowired
	private CallingDemandDao callingDemandDao;
	@Autowired
	private CallingReplyUserDao callingReplyUserDao;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;

//	@Autowired
//	private MenuMngtDao menuDao;
	
	@Override
	public List<Map<String, String>> findCallingReplyUserList(CallingSearch search) {
		
		int pageSize = 10;
		if ( defaultPageSize != null && !("".equals(defaultPageSize)))
			pageSize = Integer.valueOf(defaultPageSize);
		
		search.setSize(pageSize);
		search.setTotal_count(callingReplyUserDao.findCallingReplyUserList_count(search));
		
		return callingReplyUserDao.findCallingReplyUserList(search);
	}

	@Override
	public void addCallingReply(CallingDemand demand) {
		demand.setStatus(Status.STANDBY);
		callingDemandDao.saveCallingStatusOne(demand);
		callingReplyUserDao.addCallingReplyOne(demand);
	}

//	@Override
//	public DataModelAndView findCallingReplyUserDetail(Map<String, String> parameters, HttpServletRequest request) {
//		PageInfo pageInfo = new PageInfo(parameters, defaultPageSize);
//		
//		CallingDemand paramBean = CommonHelper.convertMapToBean(parameters, CallingDemand.class);
////		paramBean.setCll_dmnd_id(parameters.get("detailCll_dmnd_id") == null ? 0 : Integer.parseInt(parameters.get("detailCll_dmnd_id")));
////		paramBean.setPageInfo(pageInfo);
////		paramBean.setEmp_user_id(SystemHelper.findSessionUser(request).getAdmin_user_id());
////		
////		ExtrtCondbyInq extrtCondbyInq = new ExtrtCondbyInq();
////		extrtCondbyInq.setEmp_user_id(paramBean.getEmp_user_id());
////		
////		// 유저정보
////		EmpUser empUser = extrtCondbyInqDao.findEmpUser(extrtCondbyInq);
////		// 소명요청정보
////		CallingDemand callingReplyUserList = callingReplyUserDao.findCallingReplyUserListOne(paramBean);
////		// 소명답변정보
////		CallingDemand callingReplyUserReply = callingReplyUserDao.findCallingReplyUserReply(paramBean);
////		
////		// 소명요청된 추출조건 리스트
////		List<ExtrtCondbyInq> callingReplyUserDetailList = callingReplyUserDao.findCallingReplyUserDetail(paramBean);
////		// 소명요청된 추출조건의 개인정보로그( Biz_log ) 리스트 
////		List<AllLogInq> callingReplyUserDetailLogList = callingReplyUserDao.findCallingReplyDetailLogList(paramBean);
//////		pageInfo.setTotal_count(callingReplyUserDao.findCallingReplyDetailLogListCount(paramBean));
//		
//		DataModelAndView modelAndView = new DataModelAndView();
//		
////		CallingDemand get = callingReplyUserDao.findCallingDemandOne(paramBean);
//		
////		logger.debug("################## callingdemand with {}", get);
//		
////		modelAndView.addObject("empUser", empUser);
////		modelAndView.addObject("callingReplyUserList", callingReplyUserList);
////		modelAndView.addObject("callingReplyUserDetailList", callingReplyUserDetailList);
////		modelAndView.addObject("callingReplyUserDetailLogList", callingReplyUserDetailLogList);
////		modelAndView.addObject("callingReplyUserReply", callingReplyUserReply);
////		modelAndView.addObject("paramBean", paramBean);
////		modelAndView.addObject("pageInfo", pageInfo);
//		
//		return modelAndView;
//	}
//
//	@Override
//	public DataModelAndView saveCallingReplyUserDetailReply(Map<String, String> parameters, HttpServletRequest request) {
//		PageInfo pageInfo = new PageInfo(parameters, defaultPageSize);
//		CallingDemand paramBean = CommonHelper.convertMapToBean(parameters, CallingDemand.class);
//		
//		paramBean.setCll_dmnd_id(parameters.get("detailCll_dmnd_id") == null || parameters.get("detailCll_dmnd_id") == ""  ? 0 : Integer.parseInt(parameters.get("detailCll_dmnd_id")));
//		
//		paramBean = callingReplyUserDao.findCallingReplyUserListOne(paramBean);
//		paramBean.setEmp_user_id(SystemHelper.findSessionUser(request).getAdmin_user_id());
//		paramBean.setReason(parameters.get("callingReplyDesc"));
//		paramBean.setStatus(Status.STANDBY);
//		
//		// 유저정보 가져오기
//		ExtrtCondbyInq extrtCondbyInq = new ExtrtCondbyInq();
//		extrtCondbyInq.setEmp_user_id(paramBean.getEmp_user_id());
//		EmpUser empUser = extrtCondbyInqDao.findEmpUser(extrtCondbyInq);
//		paramBean.setEmp_dept_id(empUser.getDept_id());
//		
//		// 소명테이블의 Status를 STANDBY로 변경
//		callingReplyUserDao.saveCallingDemand(paramBean);
//
//		// 답변테이블에 PK가 없을때만 인서트
//		int cnt = callingReplyUserDao.findCallingReplyUserReplyYn(paramBean);
//		if(cnt == 0){
//			// 답변테이블에 답변 insert
//			callingReplyUserDao.addCallingReply(paramBean);
//		}else{
//			callingReplyUserDao.saveCallingReply(paramBean);
//		}
//		
//		DataModelAndView modelAndView = new DataModelAndView();
//		modelAndView.addObject("paramBean", paramBean);
//		modelAndView.addObject("pageInfo", pageInfo);
//		
//		return modelAndView;
//	}
	
}
