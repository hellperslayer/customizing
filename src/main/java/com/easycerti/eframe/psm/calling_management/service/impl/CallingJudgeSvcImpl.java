package com.easycerti.eframe.psm.calling_management.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.core.type.YesNo;
import com.easycerti.eframe.psm.calling_management.dao.CallingApprovalDao;
import com.easycerti.eframe.psm.calling_management.dao.CallingBizLogDao;
import com.easycerti.eframe.psm.calling_management.dao.CallingDemandDao;
import com.easycerti.eframe.psm.calling_management.dao.CallingJudgeDao;
import com.easycerti.eframe.psm.calling_management.service.CallingJudgeSvc;
import com.easycerti.eframe.psm.calling_management.type.Judge;
import com.easycerti.eframe.psm.calling_management.type.Status;
import com.easycerti.eframe.psm.calling_management.vo.CallingApproval;
import com.easycerti.eframe.psm.calling_management.vo.CallingDemand;
import com.easycerti.eframe.psm.calling_management.vo.CallingDemandReason;
import com.easycerti.eframe.psm.calling_management.vo.CallingJudge;
import com.easycerti.eframe.psm.calling_management.vo.CallingSearch;

@Service
public class CallingJudgeSvcImpl implements CallingJudgeSvc {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private CallingDemandDao callingDemandDao;
	@Autowired
	private CallingJudgeDao callingJudgeDao;
	@Autowired
	private CallingBizLogDao callingBizLogDao;
	@Autowired
	private CallingApprovalDao callingApprovalDao;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;

	@Override
	public List<Map<String, String>> findCallingJudgeList(CallingSearch search) {
				
		int pageSize = 10;
		if ( defaultPageSize != null && !("".equals(defaultPageSize)))
			pageSize = Integer.valueOf(defaultPageSize);
		
//		search.setSize(pageSize);
//		search.setTotal_count(callingJudgeDao.findCallingJudgeList_count(search));
		
		return callingJudgeDao.findCallingJudgeList(search);
	}
	
	@Override
	public List<Map<String, String>> findCallingStandByList(CallingSearch search) {
		int pageSize = 10;
		if ( defaultPageSize != null && !("".equals(defaultPageSize)))
			pageSize = Integer.valueOf(defaultPageSize);
		
		search.setSize(pageSize);
		search.setTotal_count(callingJudgeDao.findCallingStandByList_count(search));
		
		return callingJudgeDao.findCallingStandByList(search);
	}
	
	@Override
	public DataModelAndView findCallJdgmnOne(Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView();
		return modelAndView;
	}

	@Override
	public Map<String, Object> findCallingDemandOne(int cll_dmnd_id) {
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("demand", callingDemandDao.findCallingDemandOne(cll_dmnd_id));
		//map.put("bizLogList", callingBizLogDao.findCallingBizLogList(cll_dmnd_id));
		
		return map;
	}

	@Override
	public void addCallingJudgeOne(CallingJudge judge) { 
		callingJudgeDao.addCallingJudgeOne(judge);
		int cll_dmnd_id = judge.getCll_dmnd_id();
		short repeat_cnt = judge.getRepeat_cnt();
		
		CallingDemand demand = callingDemandDao.findCallingDemandWithoutEmpOne(judge.getCll_dmnd_id());
		
		// 재소명 요청
		if (Judge.REDEMAND.equals(judge.getJudge_result())) {
			String reason = judge.getJudge();
			demand.setStatus(Status.RECALLING);
			demand.setRepeat_cnt(++repeat_cnt);
			demand.setUpdater_id(judge.getInserter_id());
			callingDemandDao.saveCallingForRecalling(demand);
			
			CallingDemandReason callingReason = new CallingDemandReason(cll_dmnd_id, repeat_cnt, reason);
			callingReason.setInserter_id(judge.getInserter_id());
			callingReason.setInserter_dept_id(judge.getInserter_dept_id());
			callingDemandDao.addCallingDemandReasonOne(callingReason);
			callingApprovalDao.initCallingApprovalList(demand);
		} else {
			if (YesNo.YES.equals(demand.getUse_approval())) {
				CallingApproval approval = demand.getCurrentApproval();
				approval.setIs_approved(YesNo.YES);
				approval.setUpdater_id(judge.getInserter_id());
				callingJudgeDao.saveCallingApproval(approval);
				String lastJudgeId = callingApprovalDao.findLastApprover(demand.getCll_dmnd_id());
				if (lastJudgeId.equals(judge.getInserter_id())) {
					demand.setStatus(Status.COMPLETE);
					demand.setUpdater_id(judge.getInserter_id());
					callingDemandDao.saveCallingStatusOne(demand);
				} 
			} else {
				demand.setStatus(Status.COMPLETE);
				demand.setUpdater_id(judge.getInserter_id());
				callingDemandDao.saveCallingStatusOne(demand);
			}
		}
	}
}
