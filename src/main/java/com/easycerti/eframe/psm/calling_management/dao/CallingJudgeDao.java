package com.easycerti.eframe.psm.calling_management.dao;

import java.util.List;
import java.util.Map;

import com.easycerti.eframe.psm.calling_management.vo.CallingApproval;
import com.easycerti.eframe.psm.calling_management.vo.CallingJudge;
import com.easycerti.eframe.psm.calling_management.vo.CallingSearch;

/**
 * 관리자 관련 Dao Interface
 * 
 * @author yjyoo
 * @since 2015. 4. 20.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 20.           yjyoo          최초 생성
 *
 * </pre>
 */
public interface CallingJudgeDao {
	
	/**
	 * 관리자 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return List<AdminUser>
	 */
	List<Map<String, String>> findCallingJudgeList(CallingSearch search);
	int findCallingJudgeList_count(CallingSearch search);
	List<Map<String, String>> findCallingStandByList(CallingSearch search);
	int findCallingStandByList_count(CallingSearch search);
	void addCallingJudgeOne(CallingJudge judge);
	void saveCallingApproval(CallingApproval approval);
}
