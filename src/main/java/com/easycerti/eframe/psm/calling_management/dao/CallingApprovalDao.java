package com.easycerti.eframe.psm.calling_management.dao;

import java.util.List;

import com.easycerti.eframe.psm.calling_management.vo.CallingApproval;
import com.easycerti.eframe.psm.calling_management.vo.CallingDemand;

/**
 * 관리자 관련 Dao Interface
 * 
 * @author hwkim
 * @since 2015. 5. 11.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 20.        crzstriker          최초 생성
 *
 * </pre>
 */
public interface CallingApprovalDao {
	
	void addCallingApprovalList(CallingDemand callingDemand);
	List<CallingApproval> findCallingApprovalList(int cll_dmnd_id);
	void initCallingApprovalList(CallingDemand demand);
	String findLastApprover(int cll_dmnd_id);
}
