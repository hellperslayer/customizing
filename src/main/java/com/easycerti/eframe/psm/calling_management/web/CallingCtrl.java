package com.easycerti.eframe.psm.calling_management.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.core.type.YesNo;
import com.easycerti.eframe.psm.calling_management.service.CallingDemandSvc;
import com.easycerti.eframe.psm.calling_management.type.Status;
import com.easycerti.eframe.psm.calling_management.vo.CallingApproval;
import com.easycerti.eframe.psm.calling_management.vo.CallingDemand;
import com.easycerti.eframe.psm.calling_management.vo.CallingSearch;
import com.easycerti.eframe.psm.scheduler.service.ScheduleMngtSvc;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;

@Controller
@RequestMapping(value="/calling/*")
public class CallingCtrl {

	private final static Logger logger = LoggerFactory.getLogger(CallingCtrl.class);
	
	@Autowired
	private CallingDemandSvc callingDemandSvc;
	@Autowired
	private ScheduleMngtSvc scheduleMngtSvc;
	@Autowired
	private CommonDao commonDao;  
	/**
	 * 
	 * 설명
	 * @author easy
	 * @since 2015. 5. 22.
	 * @return List<Code>
	 */
	@RequestMapping(value="getBizLogList.html", method = RequestMethod.GET)
	public @ResponseBody
	Map<String, Object> getBizLogList(@RequestParam long emp_detail_seq, @RequestParam int page_num) {
		
		return callingDemandSvc.findCallingBizLogList(emp_detail_seq, page_num);
	}
	
	/**
	 * 
	 * 설명
	 * @author easy
	 * @since 2015. 6. 11.
	 * @return DataModelAndView
	 */
	@RequestMapping(value = "list.html")
	public DataModelAndView findCallingAllList(
			@ModelAttribute("search") CallingSearch search,
			@RequestParam Map<String, String> parameters, ModelMap modelMap) {
		logger.info("################# CallingDemandCtrl - findCallingDemandList with {}", search);
		
		SimpleCode simpleCode = new SimpleCode();
		String index = "/calling/list.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		// 날짜 확인 및 설정
		search.initDatesWithDefaultIfEmpty();
		modelMap.put("demandList", callingDemandSvc.findCallingDemandList(search));
		modelMap.put("paramBean", parameters);
		modelMap.put("search", search);
		modelMap.put("index_id", index_id);
		//modelMap.put("", value);
		
		return new DataModelAndView("calling.list", modelMap);
	}
	
	@RequestMapping(value = "detail.html", method = { RequestMethod.POST })
	public DataModelAndView findCallingDetail(@RequestParam int cll_dmnd_id,
			@RequestParam Map<String, String> parameters, CallingSearch search,
			HttpServletRequest request, ModelMap modelMap) {
		
		CallingDemand demand = callingDemandSvc.findCallingDemandOne(cll_dmnd_id);
		CallingApproval currentApproval = demand.getCurrentApproval();
		AdminUser adminUser = SystemHelper.getAdminUserInfo(request);
		boolean judgeable = false;
		if (Status.STANDBY.equals(demand.getStatus())) {
			// 위의 조건에서 현재 판정대기 상태이고,
			// 결재선을 사용하지 않거나,
			// 결재선을 사용하지만 현재 결재자와 로그인 유저와 일치하면 판정 가능 상태가 된다.
			if (YesNo.NO.equals(demand.getUse_approval()) || 
					(currentApproval != null && adminUser.getAdmin_user_id().equals(currentApproval.getApprover_id())))
				judgeable = true;
		}
		
		//System.out.println(demand.getStatus());
		String pageTitle = judgeable?"소명판정":"소명상세";
		
		modelMap.put("demand", demand);
		modelMap.put("search", search);
		modelMap.put("paramBean", parameters);
		modelMap.put("pageTitle", pageTitle);
		modelMap.put("userType", "admin");
		modelMap.put("activeStatus", judgeable?"judgeable":"readOnly");
		modelMap.put("bizLogUrl", "calling/bizLogList.html");
		
		SimpleCode simpleCode = new SimpleCode();
		String index = "/calling/detail.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		modelMap.put("index_id", index_id);
		
		return new DataModelAndView("calling.detail", modelMap);
	}
	
	@RequestMapping(value="bizLogList.html", method = RequestMethod.GET)
	public @ResponseBody Map<String, ?> findCallingBizLogListAjax(
			@RequestParam long emp_detail_seq, @RequestParam int page_num) {
		Map<String, Object> map = callingDemandSvc.findCallingBizLogList(emp_detail_seq, page_num);
		return map;
	}
}
