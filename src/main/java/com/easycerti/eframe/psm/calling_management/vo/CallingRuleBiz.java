package com.easycerti.eframe.psm.calling_management.vo;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 
 * @author easy
 *
 */
public class CallingRuleBiz implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -1973940397246352411L;

	private int cll_dmnd_id;
	private long emp_detail_seq;
	private long log_seq;
	private Timestamp inserted_date;
	
	// getters and setters
	public int getCll_dmnd_id() {
		return cll_dmnd_id;
	}
	public void setCll_dmnd_id(int cll_dmnd_id) {
		this.cll_dmnd_id = cll_dmnd_id;
	}
	public long getEmp_detail_seq() {
		return emp_detail_seq;
	}
	public void setEmp_detail_seq(long emp_detail_seq) {
		this.emp_detail_seq = emp_detail_seq;
	}
	public long getLog_seq() {
		return log_seq;
	}
	public void setLog_seq(long log_seq) {
		this.log_seq = log_seq;
	}
	public Timestamp getInserted_date() {
		return inserted_date;
	}
	public void setInserted_date(Timestamp inserted_date) {
		this.inserted_date = inserted_date;
	}
	
	// toString
	@Override
	public String toString() {
		return "CallingRuleBiz [cll_dmnd_id=" + cll_dmnd_id
				+ ", emp_detail_seq=" + emp_detail_seq + ", log_seq=" + log_seq
				+ ", inserted_date=" + inserted_date + "]";
	}
}
