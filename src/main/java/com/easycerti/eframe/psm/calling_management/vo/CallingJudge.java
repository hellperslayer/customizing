package com.easycerti.eframe.psm.calling_management.vo;

import java.sql.Timestamp;

import com.easycerti.eframe.psm.calling_management.type.Judge;

public class CallingJudge {

	// fields
	private int cll_jdg_id;
	private int cll_dmnd_id;
	private short repeat_cnt;
	private short approval_seq;
	private String judge;
	private Judge judge_result;
	private String inserter_id;
	private String inserter_dept_id;
	private Timestamp inserted_date;
	private String updater_id;
	private Timestamp updated_date;
	
	// getters and setters
	public int getCll_jdg_id() {
		return cll_jdg_id;
	}
	public void setCll_jdg_id(int cll_jdg_id) {
		this.cll_jdg_id = cll_jdg_id;
	}
	public int getCll_dmnd_id() {
		return cll_dmnd_id;
	}
	public void setCll_dmnd_id(int cll_dmnd_id) {
		this.cll_dmnd_id = cll_dmnd_id;
	}
	public short getRepeat_cnt() {
		return repeat_cnt;
	}
	public void setRepeat_cnt(short repeat_cnt) {
		this.repeat_cnt = repeat_cnt;
	}
	public short getApproval_seq() {
		return approval_seq;
	}
	public void setApproval_seq(short approval_seq) {
		this.approval_seq = approval_seq;
	}
	public String getJudge() {
		return judge;
	}
	public void setJudge(String judge) {
		this.judge = judge;
	}
	public Judge getJudge_result() {
		return judge_result;
	}
	public void setJudge_result(Judge judge_result) {
		this.judge_result = judge_result;
	}
	public String getInserter_id() {
		return inserter_id;
	}
	public void setInserter_id(String inserter_id) {
		this.inserter_id = inserter_id;
	}
	public String getInserter_dept_id() {
		return inserter_dept_id;
	}
	public void setInserter_dept_id(String inserter_dept_id) {
		this.inserter_dept_id = inserter_dept_id;
	}
	public Timestamp getInserted_date() {
		return inserted_date;
	}
	public void setInserted_date(Timestamp inserted_date) {
		this.inserted_date = inserted_date;
	}
	public String getUpdater_id() {
		return updater_id;
	}
	public void setUpdater_id(String updater_id) {
		this.updater_id = updater_id;
	}
	public Timestamp getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(Timestamp updated_date) {
		this.updated_date = updated_date;
	}
	
	// toString
	@Override
	public String toString() {
		return "CallingJudge [cll_jdg_id=" + cll_jdg_id + ", cll_dmnd_id="
				+ cll_dmnd_id + ", repeat_cnt=" + repeat_cnt
				+ ", approval_seq=" + approval_seq + ", judge=" + judge
				+ ", judge_result=" + judge_result + ", inserter_id=" + inserter_id
				+ ", inserter_dept_id=" + inserter_dept_id + ", inserted_date="
				+ inserted_date + ", updater_id=" + updater_id
				+ ", updated_date=" + updated_date + "]";
	}
}
