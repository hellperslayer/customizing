package com.easycerti.eframe.psm.calling_management.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.easycerti.eframe.core.type.YesNo;
import com.easycerti.eframe.psm.calling_management.dao.CallingApprovalDao;
import com.easycerti.eframe.psm.calling_management.dao.CallingBizLogDao;
import com.easycerti.eframe.psm.calling_management.dao.CallingDemandDao;
import com.easycerti.eframe.psm.calling_management.dao.CallingEmpDetailDao;
import com.easycerti.eframe.psm.calling_management.dao.CallingJudgeDao;
import com.easycerti.eframe.psm.calling_management.dao.CallingReplyUserDao;
import com.easycerti.eframe.psm.calling_management.service.CallingDemandSvc;
import com.easycerti.eframe.psm.calling_management.type.Status;
import com.easycerti.eframe.psm.calling_management.vo.CallingApproval;
import com.easycerti.eframe.psm.calling_management.vo.CallingDemand;
import com.easycerti.eframe.psm.calling_management.vo.CallingSearch;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInq;

@Service
public class CallingDemandSvcImpl implements CallingDemandSvc {

	private final static Logger logger = LoggerFactory.getLogger(CallingDemandSvcImpl.class);

	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;
	
	@Autowired
	private CallingDemandDao callingDemandDao;
	@Autowired
	private CallingApprovalDao callingApprovalDao;
	@Autowired
	private CallingEmpDetailDao callingEmpDetailDao;
	@Autowired
	private CallingJudgeDao callingJudgeDao;
	@Autowired
	private CallingBizLogDao callingBizLogDao;
	@Autowired
	private CallingReplyUserDao callingReplyUserDao;
	
	@Override
	public void addCallingDemandOne(CallingDemand callingDemand) {
		
		// next val
		final int cll_dmnd_id = callingDemandDao.findCallingDemandNextValOne();
		callingDemand.setCll_dmnd_id(cll_dmnd_id);
		callingDemandDao.addCallingDemandOne(callingDemand);
		callingDemandDao.addCallingDemandReasonList(callingDemand);
		callingEmpDetailDao.saveCallingEmpDetailList(callingDemand);
		
		for (ExtrtCondbyInq empDetail : callingDemand.getEmpDetailList()) {
			callingBizLogDao.addCallingRuleBizList(empDetail);
			callingBizLogDao.addCallingBizLogList(empDetail);
			callingBizLogDao.addCallingBizLogResultList(empDetail);
		}
		
		// 결재선 지정시에만 결재선 정보 insert
		if (callingDemand.getUse_approval() == YesNo.YES) {
			List<CallingApproval> approvalList = new ArrayList<CallingApproval>();
			for (CallingApproval approval : callingDemand.getApprovalList()) {
				if (approval.getApprover_id() != null && !"".equals(approval.getApprover_id())) {
					approvalList.add(approval);
				}
			}
			
			callingApprovalDao.addCallingApprovalList(callingDemand);
		}
	}

	@Override
	public void addCallingDemandList(CallingDemand callingDemand) {
		
		List<ExtrtCondbyInq> empDetailList = callingDemand.getEmpDetailList();
		if (empDetailList == null)
			return;
		final short REPEAT_CNT = 0;
		callingDemand.setRepeat_cnt(REPEAT_CNT);
		callingDemand.setStatus(Status.DEMAND);
		List<CallingDemand> callingDemandList = new ArrayList<CallingDemand>();
		List<String> empUserIdList = new ArrayList<String>();
		// 여러 사용자의 리스트를 한꺼번에 체크한 후 소명요청 했을때 사용자별로 분리해서 insert 하자.
		// 아님 여기 수정하자.
		for (ExtrtCondbyInq empDetail : empDetailList) {
			if (empDetail.getEmp_detail_seq() == 0)
				continue;
			String empUserId = empDetail.getEmp_user_id();
			if (!empUserIdList.contains(empUserId)) {
				empUserIdList.add(empUserId);
				CallingDemand clone = callingDemand.cloneWithInitEmpDetailList();
				clone.setEmp_user_id(empUserId);
				callingDemandList.add(clone);
			}
			
			int index = empUserIdList.indexOf(empUserId);
			CallingDemand existing = callingDemandList.get(index);
			existing.addEmpDetail(empDetail);
		}
		//logger.debug("############################# callingDemandList with {}", callingDemandList);
		for (CallingDemand _demand : callingDemandList) {
			addCallingDemandOne(_demand);
		}
	}

	@Override
	public CallingDemand findCallingDemandOne(int cll_dmnd_id) {
		return callingDemandDao.findCallingDemandOne(cll_dmnd_id);	// 소명요청정보, 소명요청사유, 결재자 + 판정자 리스트
	}
	
	@Override
	public Map<String, Object> findCallingBizLogList(long emp_detail_seq, int page_num) {
		
		int pageSize = 10;
		if ( defaultPageSize != null && !("".equals(defaultPageSize)))
			pageSize = Integer.valueOf(defaultPageSize);
		
		CallingSearch search = new CallingSearch(page_num, pageSize);
		search.setEmp_detail_seq(emp_detail_seq);
		
		int total_count = callingBizLogDao.findCallingBizLogList_count(search);
		search.setTotal_count(total_count);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("bizLogList", callingBizLogDao.findCallingBizLogList(search));
		map.put("search", search);
		
		return map;
	}

	@Override
	public List<Map<String, String>> findCallingDemandList(CallingSearch search) {
		int pageSize = 10;
		if ( defaultPageSize != null && !("".equals(defaultPageSize)))
			pageSize = Integer.valueOf(defaultPageSize);
		
		search.setSize(pageSize);
		search.setTotal_count(callingDemandDao.findCallingDemandList_count(search));
		
		return callingDemandDao.findCallingDemandList(search);
	}
}
