package com.easycerti.eframe.psm.calling_management.dao;

import java.util.List;

import com.easycerti.eframe.psm.calling_management.vo.CallingDemand;
import com.easycerti.eframe.psm.calling_management.vo.CallingSearch;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInq;

/**
 * 관리자 관련 Dao Interface
 * 
 * @author crzstriker
 * @since 2015. 5. 11.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 20.        crzstriker          최초 생성
 *
 * </pre>
 */
public interface CallingEmpDetailDao {
	
	void saveCallingEmpDetailList(CallingDemand callingdemand);
	List<ExtrtCondbyInq> findCallingEmpDetailList(CallingSearch search);
	
}
