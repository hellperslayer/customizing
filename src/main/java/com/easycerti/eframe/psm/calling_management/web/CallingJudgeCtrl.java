package com.easycerti.eframe.psm.calling_management.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.core.type.YesNo;
import com.easycerti.eframe.psm.calling_management.service.CallingDemandSvc;
import com.easycerti.eframe.psm.calling_management.service.CallingJudgeSvc;
import com.easycerti.eframe.psm.calling_management.type.Status;
import com.easycerti.eframe.psm.calling_management.vo.CallingApproval;
import com.easycerti.eframe.psm.calling_management.vo.CallingDemand;
import com.easycerti.eframe.psm.calling_management.vo.CallingJudge;
import com.easycerti.eframe.psm.calling_management.vo.CallingSearch;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;

@Controller
@RequestMapping(value="/callingJudge/*")
public class CallingJudgeCtrl {

	private final static Logger logger = LoggerFactory.getLogger(CallingJudgeCtrl.class);
	
	@Autowired
	private CallingDemandSvc callingDemandSvc;
	@Autowired
	private CallingJudgeSvc callingJudgeSvc;
	@Autowired
	private CommonDao commonDao;
	
	@RequestMapping(value="list.html")
	public DataModelAndView findCallingJudgeStandByList(@ModelAttribute("search") CallingSearch search, 
			@RequestParam Map<String, String> parameters, 
			HttpServletRequest request, ModelMap modelMap) {
		logger.info("################# CallingJudgeCtrl - findCallingJudgeList with {}", search);
		
		// 날짜 확인 및 설정
		search.initDatesWithDefaultIfEmpty();
		AdminUser adminUser = SystemHelper.findSessionUser(request);
		search.setLoginUserId(adminUser.getAdmin_user_id());
		modelMap.put("judgeList", callingJudgeSvc.findCallingStandByList(search));
		modelMap.put("paramBean", parameters);
		modelMap.put("search", search);
		
		SimpleCode simpleCode = new SimpleCode();
		String index = "/callingJudge/list.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		modelMap.put("index_id", index_id);
		
		return new DataModelAndView("callingJudge.list", modelMap);
	}
	
	@RequestMapping(value="detail.html",method={RequestMethod.POST})
	public DataModelAndView findCallingJudgeDetail(
			@RequestParam int cll_dmnd_id,
			@RequestParam Map<String, String> parameters, CallingSearch search,
			HttpServletRequest request, ModelMap modelMap) {
		
		CallingDemand demand = callingDemandSvc.findCallingDemandOne(cll_dmnd_id);
		CallingApproval currentApproval = demand.getCurrentApproval();
		AdminUser adminUser = SystemHelper.getAdminUserInfo(request);
		boolean judgeable = false;
		if (Status.STANDBY.equals(demand.getStatus())) {
			// 위의 조건에서 현재 판정대기 상태이고,
			// 결재선을 사용하지 않거나,
			// 결재선을 사용하지만 현재 결재자와 로그인 유저와 일치하면 판정 가능 상태가 된다.
			if (YesNo.NO.equals(demand.getUse_approval()) || 
					(currentApproval != null && adminUser.getAdmin_user_id().equals(currentApproval.getApprover_id())))
				judgeable = true;
		}
		
		System.out.println(demand.getStatus());
		String pageTitle = judgeable?"소명판정":"소명상세";
		
		modelMap.put("demand", demand);
		modelMap.put("search", search);
		modelMap.put("paramBean", parameters);
		modelMap.put("pageTitle", pageTitle);
		modelMap.put("userType", "admin");
		modelMap.put("activeStatus", judgeable?"judgeable":"readOnly");
		
		SimpleCode simpleCode = new SimpleCode();
		String index = "/callingJudge/datail.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		modelMap.put("index_id", index_id);
		
		return new DataModelAndView("calling.detail", modelMap);
	}
	
	@RequestMapping(value = "add.html", method=RequestMethod.POST)
	public DataModelAndView addCallingJudgeOne(CallingJudge judge,
			@RequestParam Map<String, String> parameters, 
			ModelMap modelMap, HttpServletRequest request) {
		logger.info("################# CallingJudgeCtrl - addCallingJudgeOne with {}", judge);
		
		AdminUser adminUser = SystemHelper.getAdminUserInfo(request);
		judge.setInserter_id(adminUser.getAdmin_user_id());
		judge.setInserter_dept_id(adminUser.getDept_id());
		
		callingJudgeSvc.addCallingJudgeOne(judge);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
}
