package com.easycerti.eframe.psm.calling_management.web;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.service.CommonService;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.calling_management.service.CallingDemandSvc;
import com.easycerti.eframe.psm.calling_management.vo.CallingDemand;
import com.easycerti.eframe.psm.search.service.ExtrtCondbyInqSvc;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;

@Controller
@RequestMapping(value="/callingDemand/*")
public class CallingDemandCtrl {

	private final static Logger logger = LoggerFactory.getLogger(CallingDemandCtrl.class);
	
	@Autowired
	private CallingDemandSvc callingDemandSvc;
	@Autowired
	private ExtrtCondbyInqSvc extrtCondbyInqSvc;
	@Autowired
	private CommonService commonService;
	@Autowired
	private CommonDao commonDao;
	
	/**
	 * 추출조건별조회 List
	 */
	@RequestMapping(value={"list.html"}, method={RequestMethod.POST})
	public DataModelAndView findExtrtCondbyInqList(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {

		Date today = new Date();
		search.initDatesIfEmpty(today, today);
		
		DataModelAndView modelAndView = extrtCondbyInqSvc.findExtrtCondbyInqList(search);
		AdminUser adminUser = SystemHelper.getAdminUserInfo(request);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("adminUser", adminUser);
		modelAndView.addObject("search", search);
		
		SimpleCode simpleCode = new SimpleCode();
		String index = "/callingDemand/list.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("adminUsers", commonService.getAdminUsers());
		modelAndView.setViewName("callingDemand.list");
		
		return modelAndView;
	}
	
	@RequestMapping(value = "add.html", method = { RequestMethod.POST })
	public DataModelAndView getaddCallingDemandList(
			@ModelAttribute("callingDemand") CallingDemand callingDemand,
			@RequestParam Map<String, String> parameters,
			HttpServletRequest request, ModelMap modelMap) {

		logger.info("################# CallingDemandCtrl - addCallingDemandList with {}", callingDemand);
		
		AdminUser adminUser = SystemHelper.getAdminUserInfo(request);
		callingDemand.setInserter_id(adminUser.getAdmin_user_id());
		callingDemandSvc.addCallingDemandList(callingDemand);
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
}
