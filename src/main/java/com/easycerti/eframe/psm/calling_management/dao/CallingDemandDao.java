package com.easycerti.eframe.psm.calling_management.dao;

import java.util.List;
import java.util.Map;

import com.easycerti.eframe.psm.calling_management.vo.CallingDemand;
import com.easycerti.eframe.psm.calling_management.vo.CallingDemandReason;
import com.easycerti.eframe.psm.calling_management.vo.CallingSearch;

/**
 * 관리자 관련 Dao Interface
 * 
 * @author crzstriker
 * @since 2015. 5. 11.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 20.        crzstriker          최초 생성
 *
 * </pre>
 */
public interface CallingDemandDao {

	/**
	 * 소명관리 테이블 id 추출
	 * 
	 * @author hwkim
	 * @since 2015. 4. 20.
	 * @return AdminUser
	 */
	int findCallingDemandNextValOne();
	void addCallingDemandOne(CallingDemand callingDemand);
	void addCallingDemandReasonOne(CallingDemandReason reason);
	void addCallingDemandReasonList(CallingDemand callingDemand);
	CallingDemand findCallingDemandOne(int cll_dmnd_id);
	CallingDemand findCallingDemandWithoutEmpOne(int cll_dmnd_id);
	void saveCallingStatusOne(CallingDemand demand);
	void saveCallingForRecalling(CallingDemand demand);
	public List<Map<String, String>> findCallingDemandList(CallingSearch search);
	public int findCallingDemandList_count(CallingSearch search);
	
}
