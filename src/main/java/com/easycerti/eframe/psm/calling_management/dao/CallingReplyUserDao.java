package com.easycerti.eframe.psm.calling_management.dao;

import java.util.List;
import java.util.Map;

import com.easycerti.eframe.psm.calling_management.vo.CallingDemand;
import com.easycerti.eframe.psm.calling_management.vo.CallingSearch;

/**
 * 
 * 설명 : 소명응답 CallingReplyUser 
 * @author tjlee
 * @since 2015. 5. 12.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 12.           tjlee            최초 생성
 *
 * </pre>
 */
public interface CallingReplyUserDao {
	/**
	 * 설명 : 소명응답 리스트
	 * @author tjlee
	 * @since 2015. 5. 12.
	 * @return List<AdminUser>
	 */
	List<Map<String, String>> findCallingReplyUserList(CallingSearch searchs);

	int findCallingReplyUserList_count(CallingSearch search);

	void addCallingReplyOne(CallingDemand demand);
//	public CallingDemand findCallingReplyUserListOne(CallingDemand paramBean);
//	
//	public List<ExtrtCondbyInq> findCallingReplyUserDetail(CallingDemand paramBean);
//	
//	public List<AllLogInq> findCallingReplyDetailLogList(CallingDemand paramBean);
//
//	public int findCallingReplyDetailLogListCount(CallingDemand paramBean);
//	
//	public CallingDemand findCallingDemandOne(CallingDemand demand);
//
//	public void saveCallingDemand(CallingDemand paramBean);
//
//	public void addCallingReply(CallingDemand paramBean);
//
//	public int findCallingReplyUserReplyYn(CallingDemand paramBean);
//
//	public CallingDemand findCallingReplyUserReply(CallingDemand paramBean);
//
//	public void saveCallingReply(CallingDemand paramBean);
}
