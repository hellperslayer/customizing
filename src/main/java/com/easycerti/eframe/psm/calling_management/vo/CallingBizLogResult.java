package com.easycerti.eframe.psm.calling_management.vo;

import java.io.Serializable;
import java.sql.Timestamp;

public class CallingBizLogResult implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 4321728618510399484L;
	
	// fields
	private long biz_log_result_seq;
	private long log_seq;
	private String result_type;
	private String result_content;
	private Timestamp inserted_date;
	
	// getters and setters
	public long getBiz_log_result_seq() {
		return biz_log_result_seq;
	}
	public void setBiz_log_result_seq(long biz_log_result_seq) {
		this.biz_log_result_seq = biz_log_result_seq;
	}
	public long getLog_seq() {
		return log_seq;
	}
	public void setLog_seq(long log_seq) {
		this.log_seq = log_seq;
	}
	public String getResult_type() {
		return result_type;
	}
	public void setResult_type(String result_type) {
		this.result_type = result_type;
	}
	public String getResult_content() {
		return result_content;
	}
	public void setResult_content(String result_content) {
		this.result_content = result_content;
	}
	public Timestamp getInserted_date() {
		return inserted_date;
	}
	public void setInserted_date(Timestamp inserted_date) {
		this.inserted_date = inserted_date;
	}
	
	// toString
	@Override
	public String toString() {
		return "CallingBizLogResult [biz_log_result_seq=" + biz_log_result_seq
				+ ", log_seq=" + log_seq + ", result_type=" + result_type
				+ ", result_content=" + result_content + ", inserted_date="
				+ inserted_date + "]";
	}
}
