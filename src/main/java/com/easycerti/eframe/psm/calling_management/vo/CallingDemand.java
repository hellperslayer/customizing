package com.easycerti.eframe.psm.calling_management.vo;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.easycerti.eframe.core.type.YesNo;
import com.easycerti.eframe.psm.calling_management.type.Judge;
import com.easycerti.eframe.psm.calling_management.type.Status;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInq;

/**
 * 소명관리 전반적인 정보를 모두 포함하고 있는 vo 
 * 
 * @author hwkim
 * @since 2015. 6. 12.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 6. 12.           easy            최초 생성
 *
 * </pre>
 */
public class CallingDemand implements Serializable {

	private static final long serialVersionUID = -8945230395946273255L;

	// fields
	private int cll_dmnd_id;
	private short repeat_cnt;
	private String emp_user_id;
	private String emp_user_name;
	private String emp_ip;
	private String emp_dept_id;
	private String emp_dept_name;
	private YesNo use_approval;
	private Status status;
	private String inserter_id;
	private String inserter_dept_id;
	private Timestamp inserted_date;
	private String updater_id;
	private Timestamp updated_date;
	private String reason;
	private String reply;
	private String judge;
	private Judge judge_result;
	
	private List<ExtrtCondbyInq> empDetailList;
	private List<CallingDemandReason> reasonList;
	private List<CallingApproval> approvalList;
	private List<CallingReply> replyList;
	
	//private PageInfo pageInfo;
	// methods
	public CallingDemand cloneWithInitEmpDetailList() {
		CallingDemand clone = (CallingDemand)org.apache.commons.lang.SerializationUtils.clone(this);
		if (clone.getEmpDetailList() != null)
			clone.setEmpDetailList(null);
		return clone;
	}
	public void addEmpDetail(ExtrtCondbyInq empDetail) {
		if (this.empDetailList == null)
			empDetailList = new ArrayList<ExtrtCondbyInq>();
		empDetailList.add(empDetail);
	}
	// 현재 판정정보 가져오기
	public CallingApproval getCurrentApproval() {
		if (approvalList == null)
			return null;
		
		if (!status.equals(Status.STANDBY))
			return null;
		
		CallingApproval result = null;
		for (CallingApproval approval : approvalList) {
			if (approval.getIs_approved() == null || YesNo.NO.equals(approval.getIs_approved())) {
				result = approval;
				break;
			}
		}
		return result;
	}
	
	// 해당 답변 가져오기(재요청 이전 모든 답변을 가지고 있음.)
	public CallingReply getCurrentReply() {
		if (replyList == null)
			return null;
		
		return replyList.get(repeat_cnt);
	}
	
	// 마지막 판정자
//	public boolean isLastJudge(String judge_id) {
//		if (approvalList == null)
//			return true;
//		int index = approvalList.size() - 1;
//		if (judge_id.equals(approvalList.get(index).getApprover_id()))
//			return true;
//		return false;
//	}
	
	// getters and setters
	public int getCll_dmnd_id() {
		return cll_dmnd_id;
	}
	public void setCll_dmnd_id(int cll_dmnd_id) {
		this.cll_dmnd_id = cll_dmnd_id;
	}
	public short getRepeat_cnt() {
		return repeat_cnt;
	}
	public void setRepeat_cnt(short repeat_cnt) {
		this.repeat_cnt = repeat_cnt;
	}
	public String getEmp_user_id() {
		return emp_user_id;
	}
	public void setEmp_user_id(String emp_user_id) {
		this.emp_user_id = emp_user_id;
	}
	public String getEmp_user_name() {
		return emp_user_name;
	}
	public void setEmp_user_name(String emp_user_name) {
		this.emp_user_name = emp_user_name;
	}
	public String getEmp_ip() {
		return emp_ip;
	}
	public void setEmp_ip(String emp_ip) {
		this.emp_ip = emp_ip;
	}
	public String getEmp_dept_id() {
		return emp_dept_id;
	}
	public void setEmp_dept_id(String emp_dept_id) {
		this.emp_dept_id = emp_dept_id;
	}
	public String getEmp_dept_name() {
		return emp_dept_name;
	}
	public void setEmp_dept_name(String emp_dept_name) {
		this.emp_dept_name = emp_dept_name;
	}
	public YesNo getUse_approval() {
		return use_approval;
	}
	public void setUse_approval(YesNo use_approval) {
		this.use_approval = use_approval;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public String getInserter_id() {
		return inserter_id;
	}
	public void setInserter_id(String inserter_id) {
		this.inserter_id = inserter_id;
	}
	public String getInserter_dept_id() {
		return inserter_dept_id;
	}
	public void setInserter_dept_id(String inserter_dept_id) {
		this.inserter_dept_id = inserter_dept_id;
	}
	public Timestamp getInserted_date() {
		return inserted_date;
	}
	public void setInserted_date(Timestamp inserted_date) {
		this.inserted_date = inserted_date;
	}
	public String getUpdater_id() {
		return updater_id;
	}
	public void setUpdater_id(String updater_id) {
		this.updater_id = updater_id;
	}
	public Timestamp getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(Timestamp updated_date) {
		this.updated_date = updated_date;
	}
	public List<ExtrtCondbyInq> getEmpDetailList() {
		return empDetailList;
	}
	public void setEmpDetailList(List<ExtrtCondbyInq> empDetailList) {
		this.empDetailList = empDetailList;
	}
	public List<CallingDemandReason> getReasonList() {
		return reasonList;
	}
	public void setReasonList(List<CallingDemandReason> reasonList) {
		this.reasonList = reasonList;
	}
	public List<CallingApproval> getApprovalList() {
		return approvalList;
	}
	public void setApprovalList(List<CallingApproval> approvalList) {
		this.approvalList = approvalList;
	}
	public String getReason() {
			return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getReply() {
		return reply;
	}
	public void setReply(String reply) {
		this.reply = reply;
	}
	public List<CallingReply> getReplyList() {
		return replyList;
	}
	public void setReplyList(List<CallingReply> replyList) {
		this.replyList = replyList;
	}
	public String getJudge() {
		return judge;
	}
	public void setJudge(String judge) {
		this.judge = judge;
	}	
	public Judge getJudge_result() {
		return judge_result;
	}
	public void setJudge_result(Judge judge_result) {
		this.judge_result = judge_result;
	}
	
	// toString
	@Override
	public String toString() {
		return "CallingDemand [cll_dmnd_id=" + cll_dmnd_id + ", repeat_cnt="
				+ repeat_cnt + ", emp_user_id=" + emp_user_id
				+ ", emp_user_name=" + emp_user_name + ", emp_ip=" + emp_ip
				+ ", emp_dept_id=" + emp_dept_id + ", emp_dept_name="
				+ emp_dept_name + ", use_approval=" + use_approval
				+ ", status=" + status + ", inserter_id=" + inserter_id
				+ ", inserter_dept_id=" + inserter_dept_id + ", inserted_date="
				+ inserted_date + ", updater_id=" + updater_id
				+ ", updated_date=" + updated_date + ", reason=" + reason
				+ ", reply=" + reply + ", judge=" + judge + ", judge_result="
				+ judge_result + ", empDetailList=" + empDetailList
				+ ", reasonList=" + reasonList + ", approvalList="
				+ approvalList + ", replyList=" + replyList + "]";
	}
}
