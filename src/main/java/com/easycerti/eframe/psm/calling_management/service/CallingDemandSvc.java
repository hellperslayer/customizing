package com.easycerti.eframe.psm.calling_management.service;

import java.util.List;
import java.util.Map;

import com.easycerti.eframe.psm.calling_management.vo.CallingDemand;
import com.easycerti.eframe.psm.calling_management.vo.CallingSearch;

public interface CallingDemandSvc {
	
	void addCallingDemandOne(CallingDemand callingDemand);
	void addCallingDemandList(CallingDemand callingDemand);
	List<Map<String, String>> findCallingDemandList(CallingSearch search);
	CallingDemand findCallingDemandOne(int cll_cmnd_id);
	Map<String, Object> findCallingBizLogList(long emp_detail_seq, int page_num);
}
