package com.easycerti.eframe.psm.calling_management.type;

import com.easycerti.eframe.core.type.Symbolic;

public enum Status implements Symbolic {

	DEMANDSTANDBY("15", "소명요청결재대기"), DEMAND("20", "소명요청"), RECALLING("30", "재소명"), RESPONSESTANDBY("35", "소명답변결재대기"), STANDBY("40", "판정대기"), RERESULT("60", "2차판정"), COMPLETE("80", "판정완료"), CANCLE("90","소명취소");

	private String symbol;
	private String name;

	Status(String symbol, String name) {
		this.symbol = symbol;
		this.name = name;
	}

	public String getSymbol() {
		return symbol;
	}

	public String getName() {
		return name;
	}
}
