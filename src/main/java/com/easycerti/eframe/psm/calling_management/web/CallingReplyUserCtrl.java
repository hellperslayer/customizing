package com.easycerti.eframe.psm.calling_management.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.psm.calling_management.service.CallingDemandSvc;
import com.easycerti.eframe.psm.calling_management.service.CallingReplyUserSvc;
import com.easycerti.eframe.psm.calling_management.type.Status;
import com.easycerti.eframe.psm.calling_management.vo.CallingDemand;
import com.easycerti.eframe.psm.calling_management.vo.CallingSearch;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;
/**
 * 
 * 설명 : 소명답변(사용자) Controller
 * @author tjlee
 * @since 2015. 5. 13.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 13.           tjlee            최초 생성
 *	 2015. 6. 12.			hwkim			 
 * </pre>
 */
@Controller
@RequestMapping(value="/callingReplyUser/*")
public class CallingReplyUserCtrl {

	private final static Logger logger = LoggerFactory.getLogger(CallingReplyUserCtrl.class);
	
	@Autowired
	private CallingReplyUserSvc callingReplyUserSvc;
	@Autowired
	private CallingDemandSvc callingDemandSvc;
	
	@RequestMapping(value="list.html",method={RequestMethod.POST})
	public DataModelAndView findCallingReplyUserList(@ModelAttribute("search") CallingSearch search, 
			@RequestParam Map<String, String> parameters, 
			HttpServletRequest request, ModelMap modelMap) {
		logger.info("################# CallingReplyUserCtrl - findCallingReplyUserList with {}", search);
		
		AdminUser adminUser = SystemHelper.getAdminUserInfo(request);
		// 날짜 확인 및 설정
		search.initDatesWithDefaultIfEmpty();
		// 임시로 관리자 세션을 받자...
		search.setEmp_user_id(adminUser.getAdmin_user_id());
		modelMap.put("replyList", callingReplyUserSvc.findCallingReplyUserList(search));
		modelMap.put("paramBean", parameters);
		modelMap.put("search", search);
		
		return new DataModelAndView("callingReplyUser.list", modelMap);
	}
	
	@RequestMapping(value = "add.html", method = { RequestMethod.POST })
	public DataModelAndView addCallingReplyOne(
			@ModelAttribute CallingDemand callingDemand,
			@RequestParam Map<String, String> parameters,
			HttpServletRequest request, ModelMap modelMap) {

		logger.info("################# CallingReplyUserCtrl - addCallingReplyOne with {}", callingDemand);
		
		AdminUser adminUser = SystemHelper.getAdminUserInfo(request);
		callingDemand.setInserter_id(adminUser.getAdmin_user_id());
		callingDemand.setInserter_dept_id(adminUser.getDept_id());
		callingReplyUserSvc.addCallingReply(callingDemand);
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@RequestMapping(value="detail.html",method={RequestMethod.POST})
	public DataModelAndView findCallingReplyUserDetail(
			@RequestParam int cll_dmnd_id, CallingSearch search,
			@RequestParam Map<String, String> parameters, ModelMap modelMap) {
		
		CallingDemand demand = callingDemandSvc.findCallingDemandOne(cll_dmnd_id);
		System.out.println(demand.getStatus());
		//boolean
		String pageTitle = Status.DEMAND.equals(demand.getStatus())?"소명응답":"소명응답 상세";
		
		modelMap.put("demand", demand);
		modelMap.put("search", search);
		modelMap.put("paramBean", parameters);
		modelMap.put("userType", "user");
		modelMap.put("activeStatus", "user");
		modelMap.put("pageTitle", pageTitle);
		
		return new DataModelAndView("callingReplyUser.detail", modelMap);
	}
}
