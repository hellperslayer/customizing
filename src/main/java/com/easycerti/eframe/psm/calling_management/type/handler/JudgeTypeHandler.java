package com.easycerti.eframe.psm.calling_management.type.handler;

import com.easycerti.eframe.core.type.handler.AbstractSymbolicTypeHandler;
import com.easycerti.eframe.psm.calling_management.type.Status;

public class JudgeTypeHandler extends AbstractSymbolicTypeHandler<Status> {
	public JudgeTypeHandler(Class<Status> javaType) {
		super(javaType);
	}
}
