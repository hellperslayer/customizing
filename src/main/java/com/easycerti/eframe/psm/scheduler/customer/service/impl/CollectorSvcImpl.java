package com.easycerti.eframe.psm.scheduler.customer.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycerti.eframe.psm.scheduler.customer.service.CollectorSvc;
import com.easycerti.eframe.psm.scheduler.dao.CollectorDao;

/**
 * 관리자 관련 Service Implements
 * 
 * @author yjyoo
 * @since 2015. 4. 20.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 20.           yjyoo            최초 생성
 *
 * </pre>
 */
@Service
public class CollectorSvcImpl implements CollectorSvc {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private CollectorDao collectorDao;
//	@Autowired
//	private LinkDbDao linkDbDao;
//
//	@Override
//	public List<Map<String, String>> findPersonalInfoList() {
//		return linkDbDao.findPersonalInfoList();
//	}
	public void upsertPersonalInfoList(List<Map<String, String>> personalInfoList) throws Exception {
		try {
			if (personalInfoList != null) {
				for (Map<String, String> map : personalInfoList) {
					collectorDao.updatePersonalInfoOne(map);
					collectorDao.insertPersonalInfoOne(map);
				}
			}
		} catch (Exception e) {
			throw e;
		}
	}
}
