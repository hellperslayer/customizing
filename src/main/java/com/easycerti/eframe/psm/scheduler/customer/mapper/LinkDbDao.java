package com.easycerti.eframe.psm.scheduler.customer.mapper;

import java.util.List;
import java.util.Map;

/**
 * 통계 Dao
 * @author yrchoo
 * @since 2015. 4. 21.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 21.           yrchoo            최초 생성
 *
 * </pre>
 */
public interface LinkDbDao {
	
	List<Map<String, String>> findPersonalInfoList();
	
}
