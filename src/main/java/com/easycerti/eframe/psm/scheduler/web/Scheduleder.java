package com.easycerti.eframe.psm.scheduler.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.mail.AuthenticationFailedException;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.core.util.SessionManager;
import com.easycerti.eframe.psm.search.dao.ExtrtCondbyInqDao;
import com.easycerti.eframe.psm.search.vo.AllLogInq;
import com.easycerti.eframe.psm.system_management.dao.AgentMngtDao;
import com.easycerti.eframe.psm.system_management.dao.AlarmMngtDao;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;
import com.easycerti.eframe.psm.system_management.vo.AgentMaster;
import com.easycerti.eframe.psm.system_management.vo.AgentMasterSearch;
import com.easycerti.eframe.psm.system_management.vo.AlarmMngt;
import com.easycerti.eframe.psm.system_management.vo.SendMailInfo;
import com.easycerti.psm.log.generator.util.CommandClient;

@Component
public class Scheduleder {

	@Autowired
	private CommonDao commonDao;
	@Autowired
	private ExtrtCondbyInqDao extrtCondbyInqDao;
	@Autowired
	private AgentMngtDao agentMngtDao;
	@Autowired
	private AlarmMngtDao alarmMngtDao;
	

	// @Scheduled(fixedRate=1000)
	/*
	 * @Scheduled(cron = "0 05 10 * * *")
	 * public void cron() {
	 * //소명 emp_detail -> 조회
	 * List<AdminUser> empDetailCheckIs = commonDao.empdetailCheckIs();
	 * String summonYn = "";
	 * String autoSummon = commonDao.checkValue();
	 * // emp_deatil N 이면 insert
	 * if("Y".equals(autoSummon)) {
	 * for (int i = 0; i < empDetailCheckIs.size(); i++) {
	 * summonYn = empDetailCheckIs.get(i).getIs_check();
	 * if(!"Y".equals(summonYn)) {
	 * AdminUser au = empDetailCheckIs.get(i);
	 * empDetailCheckIs.get(i).setAdmin_user_id("easycerti");
	 * String desc_seq = extrtCondbyInqDao.getMaxDescSeqBySummon();
	 * int ds = Integer.parseInt(desc_seq) + 1;
	 * empDetailCheckIs.get(i).setDesc_seq(ds);
	 * long empDetailSeq = empDetailCheckIs.get(i).getEmp_detail_seq();
	 * List<AllLogInq> summonLogseqs = commonDao.getbizlogSummonLogs(au);
	 * List<Integer> summonLogseq = commonDao.getSummonLogseq(au);
	 * String logs = "";
	 * String seq = "";
	 * for (int j = 0; j < summonLogseq.size(); j++) {
	 * String log_seq = Integer.toString(summonLogseq.get(j));
	 * if(summonLogseqs.toString().indexOf(log_seq) > -1) {
	 * au.setSummon_flag("N");
	 * } else {
	 * au.setSummon_flag("Y");
	 * logs += log_seq + ",";
	 * seq = Integer.toString(summonLogseq.get(0));
	 * }
	 * }
	 * if(!"".equals(logs)) {
	 * String logseqs = logs.substring(0, logs.length()-1);
	 * System.out.println(logseqs);
	 * empDetailCheckIs.get(i).setSeq(seq);
	 * empDetailCheckIs.get(i).setLogSeqs(logseqs.trim());
	 * empDetailCheckIs.get(i).setDescription2("소명 요청합니다.");
	 * extrtCondbyInqDao.addAutoSummonOne(empDetailCheckIs.get(i));
	 * extrtCondbyInqDao.addAutoSummonOne2(empDetailCheckIs.get(i));
	 * String text = "슈퍼관리자님이 소명요청하였습니다.";
	 * empDetailCheckIs.get(i).setDeci_nm("슈퍼관리자");
	 * empDetailCheckIs.get(i).setDescription(text);
	 * extrtCondbyInqDao.addSummonMemo(empDetailCheckIs.get(i));
	 * commonDao.updateEmpdetailCheckIs(Long.toString(empDetailSeq));
	 * System.out.println("summonAdd finished");
	 * }
	 * } // end if
	 * } // end for
	 * } // end if
	 * } // end void
	 */

	//@Scheduled(cron = "00 * * * * *")
	public void agentMngt_check() {

		AgentMasterSearch agentMasterSearch = new AgentMasterSearch();
		SendMailInfo sendMailInfo = new SendMailInfo();

		String type_name = "agent_status";
		sendMailInfo = agentMngtDao.findSendMailInfo(type_name);

		// 상태 정보 가져오기 (socket)
		String retStatus = "";
		
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyyMMdd");
		String nowDate = dayTime.format(new Date());
		
		int compare =-1;
		try {
			if(sendMailInfo.getSend_dt() != null) {
				Date tmpDate = dayTime.parse(sendMailInfo.getSend_dt());
				Date comDate = dayTime.parse(nowDate);
				compare = tmpDate.compareTo(comDate);
			}
			
			if( (compare == -1 || sendMailInfo.getSend_dt() == null) && "Y".equals(sendMailInfo.getUse_yn())) {
				
				// search.setAgent_type("F"); // 일단 Filter Agent만 가져오기
				List<AgentMaster> agentMasterList = agentMngtDao.findAgentMngtList(agentMasterSearch);
				
				for (AgentMaster am : agentMasterList) {
					try {
						String ip = am.getIp();
						int port = Integer.parseInt(am.getPort());

						retStatus = CommandClient.sendCommand(ip, port, "CheckAgentEnable", "tmp");

						if (retStatus != null && retStatus.indexOf("use") > -1) {
							am.setStatus("정상");
						} else if (retStatus != null && retStatus.indexOf("false") > -1) {
							am.setStatus("중지");
						} else {
							am.setStatus("비정상");
						}
					} catch (Exception e) {
						am.setStatus("비정상");
					}
				}
				
				sendMailInfo.setSubject("[에이전트 동작 확인 요청]");
				String body = "<table border=\"1\" cellspacing=\"0\" cellpadding=\"0\" style=\"word-break: normal; width: 1182px; height: 60px; font-size: 10pt; border-collapse: collapse; border: 1px none rgb(0, 0, 0);\">"
						+"<tr>"
						+"<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\"><p>에이전트명</p></td>"
						+"<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\"><p>에이전트타입</p></td>"
						+"<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\"><p>동작상태</p></td>"
						+"<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\"><p>대상시스템</p></td>"
						+"<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\"><p>수집서버명</p></td>"
						+"<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\"><p>수집서버IP</p></td>"
						+"<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\"><p>대상서버IP</p></td>"
						+"<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\"><p>접속기록로그(전일)</p></td>"
						+"<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\"><p>다운로드로그(전일)</p></td>"
						+"<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\"><p>시스템로그(전일)</p></td>"
						+"</tr>";
				
				for (int i = 0; i < agentMasterList.size(); i++) {
					
					body = body +"<tr>"
							+"<td style=\\\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\\\"><p>"+agentMasterList.get(i).getAgent_name()+"</p></td>"
							+"<td style=\\\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\\\"><p>"+agentMasterList.get(i).getAgent_type()+"</p></td>"
							+"<td style=\\\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\\\"><p>"+agentMasterList.get(i).getStatus()+"</p></td>"
							+"<td style=\\\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\\\"><p>"+agentMasterList.get(i).getSystem_name()+"</p></td>"
							+"<td style=\\\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\\\"><p>"+agentMasterList.get(i).getManager_name()+"</p></td>"
							+"<td style=\\\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\\\"><p>"+agentMasterList.get(i).getManager_ip()+"</p></td>"
							+"<td style=\\\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\\\"><p>"+agentMasterList.get(i).getIp()+"</p></td>"
							+"<td style=\\\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\\\"><p>"+agentMasterList.get(i).getBa_logcnt()+"</p></td>"
							+"<td style=\\\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\\\"><p>"+agentMasterList.get(i).getDn_logcnt()+"</p></td>"
							+"<td style=\\\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\\\"><p>"+agentMasterList.get(i).getBs_logcnt()+"</p></td>"
							+"</tr>";
					// 상태 값이 중지 or 비정상일 경우
					/*
					if ("중지".equals(agentMasterList.get(i).getStatus()) || "비정상".equals(agentMasterList.get(i).getStatus())) {
						sendAgentEmail(agentMasterList.get(i));
						System.out.println("메일발송 성공");
					}
					*/
					if ("중지".equals(agentMasterList.get(i).getStatus()) || "비정상".equals(agentMasterList.get(i).getStatus())) {
						sendMailInfo.setSend_yn("N");
					}
					
				}
				body= body + "</table>";
				sendMailInfo.setBody(body);
				agentMngtDao.updateMailInfo(sendMailInfo);
				
				if("N".equals(sendMailInfo.getSend_yn())){
					List<AlarmMngt> targetMail = alarmMngtDao.findAlarmMngtList_email();
					AlarmMngt mailAddr = null;
					if((mailAddr = targetMail.get(0))!=null) {
						sendMailInfo.setFrom_mail_address(mailAddr.getId());
						sendMailInfo.setFrom_mail_pass(mailAddr.getPw());
						sendMailInfo.setSend_mail_address(mailAddr.getTo_id());
					}
					
					if("mail".equals(sendMailInfo.getSend_type())&&sendMailInfo.getSend_yn().equals("N")){
						try {
							sendEmail(sendMailInfo);
							agentMngtDao.updateMailInfo_sendYn(type_name);
							System.out.println("메일발송 성공");
						}catch (AuthenticationFailedException e){ 
							System.out.println("메일 계정 인증 실패");
						}
						catch (Exception e) {
							e.printStackTrace();
							System.out.println("메일발송 실패");
						}
					}
					
				}
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
			
		
		
	} // end void
	
	public void sendEmail(SendMailInfo sendMailInfo) throws Exception{
		// 메일 정보
		//String from_mail_address = "easycerti087@gmail.com";	//보내는사람
		//String send_mail_address = "hhroh@easycerti.com";		//받는사람
		String subject = sendMailInfo.getSubject(); 	// 메일 제목
		String body = sendMailInfo.getBody();		// 메일 내용
		String from_mail_address = sendMailInfo.getFrom_mail_address();	//보내는사람
		String send_mail_address = sendMailInfo.getSend_mail_address();	//받는사람
		
		

		Properties props = new Properties();
		/*
		props.put("mail.smtp.host", "outbound.daouoffice.com");
		props.put("mail.smtp.port", "25");
		props.put("mail.debug", "false");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.EnableSSL.enable", "true");
		props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.setProperty("mail.smtp.socketFactory.fallback", "false");
		props.setProperty("mail.smtp.port", "465");
		props.setProperty("mail.smtp.socketFactory.port", "465");

		Authenticator auth = new MyAuthentication(sendMailInfo.getFrom_mail_address(), sendMailInfo.getFrom_mail_pass());
		Session mailSession = Session.getDefaultInstance(props, auth);
		*/
		
		props.put("mail.transprot.protocol", "smtp");
		props.put("mail.smtp.host", "outbound.daouoffice.com");
		props.put("mail.smtp.auth", "true");
		Authenticator auth = new MyAuthentication(sendMailInfo.getFrom_mail_address(), sendMailInfo.getFrom_mail_pass());
		Session mailSession = null;
		try {
			mailSession = Session.getInstance(props, auth );
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		

		if (send_mail_address != null && !send_mail_address.equals("")) {
			Message msg = new MimeMessage(mailSession);
			// msg.setFrom(new InternetAddress(MimeUtility.encodeText(fromName, "UTF-8",
			// "B")));
			msg.setFrom(new InternetAddress(from_mail_address));
			InternetAddress to = new InternetAddress(send_mail_address);
			msg.setRecipient(Message.RecipientType.TO, to);
			msg.setSubject(subject);
			msg.setSentDate(new java.util.Date());
			msg.setContent(body, "text/html;charset=euc-kr");
			msg.setHeader("content-Type", "text/html");

			Transport.send(msg);
		}

	}

	public void sendAgentEmail(AgentMaster agentInfo) throws Exception{
		// agent 정보
		String agent_name = agentInfo.getAgent_name(); // 에이전트명
		String agent_type = agentInfo.getAgent_type(); // 에이전트타입
		String system_name = agentInfo.getSystem_name(); // 대상시스템명
		String manager_name = agentInfo.getManager_name(); // 수집서버명
		String manager_ip = agentInfo.getManager_ip(); // 수집서버IP
		String target_ip = agentInfo.getIp(); // 대상서버IP
		String ba_logcnt = agentInfo.getBa_logcnt(); // 접속기록로그(전날)
		String dn_logcnt = agentInfo.getDn_logcnt(); // 다운로드로그(전날)
		String bs_logcnt = agentInfo.getBs_logcnt(); // 시스템로그(전날)
		Date command_datetime = agentInfo.getCommand_datetime(); // 동작변경시간
		String status = agentInfo.getStatus(); // 동작상태

		// 메일 정보
		List<AlarmMngt> alarm_info = alarmMngtDao.findAlarmMngtList_email();
		String mail_address = alarm_info.get(0).getId();

		// 메일 제목
		String subject = "[" + agent_name + "] 동작 상태 확인 요청";
		
		String body = "<table border=\"1\" cellspacing=\"0\" cellpadding=\"0\" style=\"word-break: normal; width: 1182px; height: 60px; font-size: 10pt; border-collapse: collapse; border: 1px none rgb(0, 0, 0);\">"
				+"<tr>"
				+"<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\"><p>에이전트명</p></td>"
				+"<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\"><p>에이전트타입</p></td>"
				+"<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\"><p>동작상태</p></td>"
				+"<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\"><p>대상시스템</p></td>"
				+"<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\"><p>수집서버명</p></td>"
				+"<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\"><p>수집서버IP</p></td>"
				+"<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\"><p>대상서버IP</p></td>"
				+"<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\"><p>접속기록로그(전일)</p></td>"
				+"<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\"><p>다운로드로그(전일)</p></td>"
				+"<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\"><p>시스템로그(전일)</p></td>"
				+"</tr>"
				+"<tr>"
				+"<td style=\\\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\\\"><p>"+agent_name+"</p></td>"
				+"<td style=\\\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\\\"><p>"+agent_type+"</p></td>"
				+"<td style=\\\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\\\"><p>"+status+"</p></td>"
				+"<td style=\\\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\\\"><p>"+system_name+"</p></td>"
				+"<td style=\\\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\\\"><p>"+manager_name+"</p></td>"
				+"<td style=\\\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\\\"><p>"+manager_ip+"</p></td>"
				+"<td style=\\\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\\\"><p>"+target_ip+"</p></td>"
				+"<td style=\\\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\\\"><p>"+ba_logcnt+"</p></td>"
				+"<td style=\\\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\\\"><p>"+dn_logcnt+"</p></td>"
				+"<td style=\\\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px;\\\"><p>"+bs_logcnt+"</p></td>"
				+"</tr>"
				+"</table>";
		
		// if(body.equals("") || body == null) {
		String body2 = "<br>□ 에이전트 동작상태 정보<br>" 
				+ "- 에이전트 명 : "+agent_name+"<br>"
				+ "- 에이전트 타입 : "+agent_type+"<br>"
				+ "- 에이전트 동작상태 : "+status+"<br>"
				+ "- 대상시스템 : "+system_name+"<br>"
				+ "- 수집서버 명 : "+manager_name+"<br>"
				+ "- 수집서버 IP : "+manager_ip+"<br>"
				+ "- 대상서버 IP : "+target_ip+"<br>"
				+ "- 접속기록로그(전일) : "+ba_logcnt+"<br>"
				+ "- 접속기록로그(전일) : "+dn_logcnt+"<br>"
				+ "- 접속기록로그(전일) : "+bs_logcnt+"<br>";
	
		Properties props = new Properties();
		
		props.put("mail.smtp.host", "smtp.easycerti.daouoffice.com");
		props.put("mail.smtp.port", "25");
		props.put("mail.debug", "false");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.EnableSSL.enable", "true");
		props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.setProperty("mail.smtp.socketFactory.fallback", "false");
		props.setProperty("mail.smtp.port", "465");
		props.setProperty("mail.smtp.socketFactory.port", "465");

		Authenticator auth = new MyAuthentication();
		Session mailSession = Session.getDefaultInstance(props, auth);
		/*
		props.put("mail.transprot.protocol", "smtp");
		props.put("mail.smtp.host", "localhost");
		Session mailSession = Session.getDefaultInstance(props);
		*/
		

		if (mail_address != null && !mail_address.equals("")) {
			Message msg = new MimeMessage(mailSession);
			// msg.setFrom(new InternetAddress(MimeUtility.encodeText(fromName, "UTF-8",
			// "B")));
			msg.setFrom(new InternetAddress(mail_address));
			InternetAddress to = new InternetAddress(mail_address);
			msg.setRecipient(Message.RecipientType.TO, to);
			msg.setSubject(subject);
			msg.setSentDate(new java.util.Date());
			msg.setContent(body, "text/html;charset=euc-kr");
			msg.setHeader("content-Type", "text/html");

			Transport.send(msg);
		}
	}
	
	public static class MyAuthentication extends Authenticator {

		PasswordAuthentication pa;

		public MyAuthentication() { // 생성자를 통해 구글 ID/PW 인증

			 //String smtpId = "easycerti087@gmail.com"; // 구글 ID
			 //String smtpPw = "dlwltjxl1!"; // 구글 비밀번호

			// ID와 비밀번호를 입력한다.
			pa = new PasswordAuthentication("easycerti087@gmail.com", "dlwltjxl1!");
		}
		
		public MyAuthentication(String id, String pass) {
			pa = new PasswordAuthentication(id, pass);
		}

		// 시스템에서 사용하는 인증정보
		public PasswordAuthentication getPasswordAuthentication() {
			return pa;
		}
	}
} // end class
