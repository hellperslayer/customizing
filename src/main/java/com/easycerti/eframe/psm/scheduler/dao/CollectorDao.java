package com.easycerti.eframe.psm.scheduler.dao;

import java.util.Map;

public interface CollectorDao {
	void insertPersonalInfoOne(Map<String, String> map);
	void updatePersonalInfoOne(Map<String, String> map);
}
