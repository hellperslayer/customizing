package com.easycerti.eframe.psm.scheduler.customer.service;

import java.util.List;
import java.util.Map;


/**
 * 
 * 설명
 * @author easy
 * @since 2015. 6. 24.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 6. 24.           easy            최초 생성
 *
 * </pre>
 */
public interface LinkDbSvc {
	
	List<Map<String, String>> findPersonalInfoList();
	
}
