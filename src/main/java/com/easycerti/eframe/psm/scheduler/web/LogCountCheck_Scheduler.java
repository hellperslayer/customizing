package com.easycerti.eframe.psm.scheduler.web;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import javax.mail.AuthenticationFailedException;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.psm.search.dao.AllLogInqDao;
import com.easycerti.eframe.psm.search.dao.ExtrtCondbyInqDao;
import com.easycerti.eframe.psm.search.vo.AllLogInq;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.system_management.dao.AgentMngtDao;
import com.easycerti.eframe.psm.system_management.dao.AlarmMngtDao;
import com.easycerti.eframe.psm.system_management.vo.AlarmMngt;
import com.easycerti.eframe.psm.system_management.vo.SendMailInfo;

@Component
public class LogCountCheck_Scheduler {

	@Autowired
	private CommonDao commonDao;
	@Autowired
	private ExtrtCondbyInqDao extrtCondbyInqDao;
	@Autowired
	private AgentMngtDao agentMngtDao;
	@Autowired
	private AlarmMngtDao alarmMngtDao;
	@Autowired
	private AllLogInqDao allLogInqDao;

	@Value("#{configProperties.smtp_host}")
	private String smtp_host;
	@Value("#{configProperties.smtp_port}")
	private String smtp_port;
	
	@Value("#{versionProperties.version}")
	private String version;
	
	public void log_check() {
		sendEmail();
	}
	
	public void sendEmail() {
		SendMailInfo sendMailInfo = new SendMailInfo(); // 메일전송 정보 담아두는 vo
		boolean sendBool = false;
		AlarmMngt mailInfo = null;
				
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Calendar cal = GregorianCalendar.getInstance(Locale.KOREA);
		Date yesterday = new Date();
		SimpleDateFormat mailDate = new SimpleDateFormat("yyyy년 MM월 dd일");

		cal.setTime(yesterday);
		cal.add(Calendar.DATE, -1);
		String procdate = sdf.format(cal.getTime());

		SearchSearch search = new SearchSearch();
		search.setProc_date(procdate);
		List<AllLogInq> systemCount = new ArrayList<AllLogInq>();
		List<AlarmMngt> systemMailInfo = new ArrayList<AlarmMngt>();
		try {
			systemCount = allLogInqDao.findLogCountBySystem(search);
			systemMailInfo = alarmMngtDao.findAlarmMngtSystemMail();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		/*
		 * for (AllLogInq allLogInq : systemCount) { if (allLogInq.getCnt() < 1) {
		 * sendBool = true; break; } }
		 */
		//sendBool&&
		if(systemMailInfo.size()>0) {
			mailInfo = systemMailInfo.get(0);
			sendMailInfo.setSubject("["+mailInfo.getSite_name()+" 자동 메일] 시스템별 로그 수집 내용입니다.");
			
			String body = "<h1 style='display:inline'>"+mailInfo.getSite_name()+"</h1> 에서 발송된 "+mailDate.format(cal.getTime())+"자 로그 수집 결과 입니다. (adminVer. "+version+")</br></br>"
					+ "<table border=\"1\" cellspacing=\"0\" cellpadding=\"0\" style=\"word-break: normal; width: 1182px; height: 60px; font-size: 10pt; border-collapse: collapse; border: 1px none rgb(0, 0, 0);\">"
					+ "<tr>"
					+ "<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px; background-color : #e2d9d9; text-align: center; \"><p>시스템 번호</p></td>"
					+ "<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px; background-color : #e2d9d9; text-align: center;\"><p>시스템 이름</p></td>"
					+ "<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px; background-color : #e2d9d9; text-align: center;\"><p>ID로그</p></td>"
					+ "<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px; background-color : #e2d9d9; text-align: center;\"><p>시스템로그</p></td>"
					+ "</tr>";
			for (AllLogInq allLogInq : systemCount) {
				String bgColor = "none";
				if((allLogInq.getCnt()+allLogInq.getCnt2())==0) {bgColor = "#ff00003b";}
				body = body + "<tr>" 
							+ "<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px; background-color : "+bgColor+"\"><p>"+ allLogInq.getSystem_seq() +"</p></td>"
							+ "<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px; background-color : "+bgColor+"\"><p>"+ allLogInq.getSystem_name() +"</p></td>"
							+ "<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px; background-color : "+bgColor+"\"><p>"+ allLogInq.getCnt2() +"</p></td>" 
							+ "<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px; background-color : "+bgColor+"\"><p>"+ allLogInq.getCnt() +"</p></td>" 
							+ "</tr>";
			}
			body = body + "</table>";
			sendMailInfo.setBody(body);
			sendMailInfo.setFrom_mail_address(mailInfo.getId());
			sendMailInfo.setFrom_mail_pass(mailInfo.getPw());
			sendMailInfo.setSend_mail_address(mailInfo.getTo_id());
			
			try {
				sendEmail(sendMailInfo);
				System.out.println("로그 체크 메일발송 성공");
			} catch (AuthenticationFailedException e) {
				System.out.println("메일 계정 인증 실패");
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("메일발송 실패");
			}
		}
	}
	
	public void sendEmail(SendMailInfo sendMailInfo) throws Exception{
		String subject = sendMailInfo.getSubject(); 	// 메일 제목
		String body = sendMailInfo.getBody();		// 메일 내용
		String from_mail_address = sendMailInfo.getFrom_mail_address();	//보내는사람
		String send_mail_address = sendMailInfo.getSend_mail_address();	//받는사람
		String from_mail_pass = sendMailInfo.getFrom_mail_pass();

		Properties props = new Properties();
		
		props.put("mail.transprot.protocol", "smtp");
		props.put("mail.smtp.host", smtp_host);
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.port", smtp_port);
		props.put("mail.smtp.ssl.enable", "true");
		Authenticator auth = new MyAuthentication(from_mail_address, from_mail_pass);
		Session mailSession = null;
		try {
			mailSession = Session.getInstance(props, auth );
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (send_mail_address != null && !send_mail_address.equals("")) {
			Message msg = new MimeMessage(mailSession);
			// msg.setFrom(new InternetAddress(MimeUtility.encodeText(fromName, "UTF-8",
			// "B")));
			msg.setFrom(new InternetAddress(from_mail_address));
			InternetAddress to = new InternetAddress(send_mail_address);
			msg.setRecipient(Message.RecipientType.TO, to);
			msg.setSubject(subject);
			msg.setSentDate(new java.util.Date());
			msg.setContent(body, "text/html;charset=euc-kr");
			msg.setHeader("content-Type", "text/html");

			Transport.send(msg);
		}
	}
	
	public class MyAuthentication extends Authenticator {

		PasswordAuthentication pa;

		public MyAuthentication() { 
			
			// ID와 비밀번호를 입력한다.
			pa = new PasswordAuthentication("easycerti087@gmail.com", "dlwltjxl1!");
		}
		
		public MyAuthentication(String id, String pass) {
			pa = new PasswordAuthentication(id, pass);
		}

		// 시스템에서 사용하는 인증정보
		public PasswordAuthentication getPasswordAuthentication() {
			return pa;
		}
	}
} // end class
