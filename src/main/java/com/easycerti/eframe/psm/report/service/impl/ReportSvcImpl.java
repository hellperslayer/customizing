package com.easycerti.eframe.psm.report.service.impl;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.PageInfo;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.vo.SearchBase;
import com.easycerti.eframe.core.util.SessionManager;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.report.dao.ReportDao;
import com.easycerti.eframe.psm.report.service.ReportSvc;
import com.easycerti.eframe.psm.report.vo.InspectionReport;
import com.easycerti.eframe.psm.report.vo.Report;
import com.easycerti.eframe.psm.report.vo.ReportData;
import com.easycerti.eframe.psm.report.vo.ReportList;
import com.easycerti.eframe.psm.report.vo.UploadReport;
import com.easycerti.eframe.psm.search.dao.AllLogInqDao;
import com.easycerti.eframe.psm.search.vo.AllLogInq;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.statistics.dao.StatisticsDao;
import com.easycerti.eframe.psm.statistics.vo.Statistics;
import com.easycerti.eframe.psm.system_management.dao.AdminUserMngtDao;
import com.easycerti.eframe.psm.system_management.dao.AgentMngtDao;
import com.easycerti.eframe.psm.system_management.dao.AuthMngtDao;
import com.easycerti.eframe.psm.system_management.dao.CodeMngtDao;
import com.easycerti.eframe.psm.system_management.dao.SummaryMngtDao;
import com.easycerti.eframe.psm.system_management.vo.AccessAuth;
import com.easycerti.eframe.psm.system_management.vo.AccessAuthSearch;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;
import com.easycerti.eframe.psm.system_management.vo.Auth;
import com.easycerti.eframe.psm.system_management.vo.PrivacyReport;
import com.easycerti.eframe.psm.system_management.vo.System;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

@Repository
public class ReportSvcImpl implements ReportSvc {

	@Autowired
	private AgentMngtDao agentMngtDao;
	
	@Autowired
	private ReportDao reportDao;
	
	@Autowired
	private AllLogInqDao allLogInqDao;
	
	@Autowired
	private SummaryMngtDao summaryMngtDao;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;

	@Autowired
	private CodeMngtDao codeMngtDao;

	@Autowired
	private AuthMngtDao authMngtDao;
	
	@Autowired
	private AdminUserMngtDao adminUserMngtDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private StatisticsDao statisticsDao;
	
	@Override
	public List<SystemMaster> findSystemMasterList() {
		
		return agentMngtDao.findSystemMasterList();
	}
/*	
	@Override
	public List<SystemMaster> findSystemMasterDetailList(List<String> list) {
		return agentMngtDao.findSystemMasterDetailList(list);
	}
*/
	
	@Override
	public List<SystemMaster> findSystemMasterDetailList(PrivacyReport pr) {
		return agentMngtDao.findSystemMasterDetailList(pr);
	}
	
	@Override
	public List<SystemMaster> findSystemMasterDetailDownList(PrivacyReport pr) {
		return agentMngtDao.findSystemMasterDetailDownList(pr);
	}

	@Override
	public List<PrivacyReport> findPrivacyReportCntList(PrivacyReport tmpBean) {
		
		return agentMngtDao.findPrivacyReportCntList(tmpBean);
	}

	@Override
	public List<PrivacyReport> findPrivacyReportCntbyEmp(PrivacyReport tmpBean) {
		return agentMngtDao.findPrivacyReportCntbyEmp(tmpBean);
	}

	@Override
	public String findDeptNameByEmpUserId(String emp_user_id) {
		return agentMngtDao.findDeptNameByEmpUserId(emp_user_id);
	}

	@Override
	public String findNameByEmpUserId(String emp_user_id) {
		return agentMngtDao.findNameByEmpUserId(emp_user_id);
	}

	@Override
	public List<PrivacyReport> findSystemMasterTop10(PrivacyReport tmpBean) {
		return agentMngtDao.findSystemMasterTop10(tmpBean);
	}

	@Override
	public List<SystemMaster> findSystemMasterList(SearchSearch search) {
		return agentMngtDao.findSystemMasterList_new(search);
	}
	
	@Override
	public List<PrivacyReport> findPrivacyReportByDept(PrivacyReport pr) {
		return agentMngtDao.findPrivacyReportByDept(pr);
	}
	
	@Override
	public List<PrivacyReport> findPrivacyReportByDeptUseStudentId(PrivacyReport pr) {
		return agentMngtDao.findPrivacyReportByDeptUseStudentId(pr);
	}
	
	@Override
	public List<PrivacyReport> findPrivacyReportBySystem(PrivacyReport pr) {
		return agentMngtDao.findPrivacyReportBySystem(pr);
	}
	
	@Override
	public PrivacyReport findPrivacyReportByDeptTop1(PrivacyReport pr) {
		return agentMngtDao.findPrivacyReportByDeptTop1(pr);
	}
	
	@Override
	public PrivacyReport findPrivacyReportBySystemTop1(PrivacyReport pr) {
		return agentMngtDao.findPrivacyReportBySystemTop1(pr);
	}
	
	@Override
	public int findEmpDetailCount(PrivacyReport pr) {
		return agentMngtDao.findEmpDetailCount(pr);
	}
	
	
	@Override
	public List<PrivacyReport> findPrivTypeCount(PrivacyReport pr) {
		return agentMngtDao.findPrivTypeCount(pr);
	}
	
	
	
	
	@Override
	public List<PrivacyReport> findPrivacyReportByReqtype(PrivacyReport pr) {
		return agentMngtDao.findPrivacyReportByReqtype(pr);
	}
	
	@Override
	public List<PrivacyReport> findPrivacyReportByReqtype_download(PrivacyReport pr) {
		return agentMngtDao.findPrivacyReportByReqtype_download(pr);
	}
	
	@Override
	public List<PrivacyReport> findPrivacyReportByReqtypeCnt(PrivacyReport pr) {
		return agentMngtDao.findPrivacyReportByReqtypeCnt(pr);
	}
	
	@Override
	public List<PrivacyReport> findPrivacyReportByResultType(PrivacyReport pr) {
		return agentMngtDao.findPrivacyReportByResultType(pr);
	}

	@Override
	public List<PrivacyReport> findEmpDetailList(PrivacyReport pr) {
		return agentMngtDao.findEmpDetailList(pr);
	}
	
	@Override
	public List<PrivacyReport> findPrivTypeResultCountTop10(PrivacyReport pr) {
		return agentMngtDao.findPrivTypeResultCountTop10(pr);
	}
	
	@Override
	public List<PrivacyReport> findPrivTypeResultCount(PrivacyReport report) {
		return agentMngtDao.findPrivTypeResultCount(report);
	}

	@Override
	public List<PrivacyReport> findDeptTypeResultCountTop10(PrivacyReport pr) {
		return agentMngtDao.findDeptTypeResultCountTop10(pr);
	}

	@Override
	public List<PrivacyReport> findDeptTypeResultCount(PrivacyReport report) {
		return agentMngtDao.findDeptTypeResultCount(report);
	}

	@Override
	public List<PrivacyReport> findSystemTypeResultCountTop10(PrivacyReport pr) {
		return agentMngtDao.findSystemTypeResultCountTop10(pr);
	}

	@Override
	public List<PrivacyReport> findSystemTypeDownloadResultCountTop10(PrivacyReport pr) {
		return agentMngtDao.findSystemTypeDownloadResultCountTop10(pr);
	}

	@Override
	public List<PrivacyReport> findSystemTypeDownloadResultCount(PrivacyReport pr) {
		return agentMngtDao.findSystemTypeDownloadResultCount(pr);
	}

	@Override
	public List<PrivacyReport> findSystemTypeResultCount(PrivacyReport report) {
		return agentMngtDao.findSystemTypeResultCount(report);
	}

	@Override
	public List<PrivacyReport> findSystemMaster(PrivacyReport pr) {
		return agentMngtDao.findSystemMaster(pr);
	}

	@Override
	public List<PrivacyReport> findSystemCurdCount(PrivacyReport tmp) {
		return agentMngtDao.findSystemCurdCount(tmp);
	}

	@Override
	public List<PrivacyReport> findPrivacyReportByDept_1(PrivacyReport pr) {
		return agentMngtDao.findPrivacyReportByDept_1(pr);
	}
	
	@Override
	public List<PrivacyReport> findPrivacyReportByDept_2(PrivacyReport pr) {
		return agentMngtDao.findPrivacyReportByDept_2(pr);
	}
	
	@Override
	public List<PrivacyReport> findReportDetail2_chart1(PrivacyReport pr) {
		return agentMngtDao.findReportDetail2_chart1(pr);
	}
	
	@Override
	public List<PrivacyReport> findReportDetail2_chart2(PrivacyReport pr) {
		return agentMngtDao.findReportDetail2_chart2(pr);
	}
	
	@Override
	public List<PrivacyReport> findReportDetailbyIndividual(PrivacyReport pr) {
		return agentMngtDao.findReportDetailbyIndividual(pr);
	}
	
	@Override
	public List<PrivacyReport> findReportDetail_chart5(PrivacyReport pr) {
		return agentMngtDao.findReportDetail_chart5(pr);
	}
	
	@Override
	public List<PrivacyReport> findReportDetail_chart5_useStudentId(PrivacyReport pr) {
		return agentMngtDao.findReportDetail_chart5_useStudentId(pr);
	}

	@Override
	public PrivacyReport findReportDetail_chart5_detail(PrivacyReport report) {
		return agentMngtDao.findReportDetail_chart5_detail(report);
	}


	@Override
	public List<PrivacyReport> findPrivacyReportDetailChart3_1(PrivacyReport pr) {
		return agentMngtDao.findPrivacyReportDetailChart3_1(pr);
	}
	
	@Override
	public List<PrivacyReport> findPrivacyReportDetailChart3_3(PrivacyReport pr) {
		return agentMngtDao.findPrivacyReportDetailChart3_3(pr);
	}

	@Override
	public List<PrivacyReport> findPrivacyReportDetailChart3_4(PrivacyReport pr) {
		return agentMngtDao.findPrivacyReportDetailChart3_4(pr);
	}

	@Override
	public List<PrivacyReport> findEmpDetailListCount(PrivacyReport pr) {
		return agentMngtDao.findEmpDetailListCount(pr);
	}

	@Override
	public List<PrivacyReport> findPrivTypeCountWeek(PrivacyReport pr) {
		return agentMngtDao.findPrivTypeCountWeek(pr);
	}
	
	@Override
	public List<PrivacyReport> findPrivTypeResultCountBySysTop10(PrivacyReport pr) {
		return agentMngtDao.findPrivTypeResultCountBySysTop10(pr);
	}
	
	@Override
	public List<PrivacyReport> findPrivTypeResultCountBySys(PrivacyReport report) {
		return agentMngtDao.findPrivTypeResultCountBySys(report);
	}
	
	@Override
	public List<PrivacyReport> findDeptTypeResultCountBySys(PrivacyReport report) {
		return agentMngtDao.findDeptTypeResultCountBySys(report);
	}
	
	@Override
	public List<PrivacyReport> findDeptTypeResultCountBySysTop10(PrivacyReport pr) {
		return agentMngtDao.findDeptTypeResultCountBySysTop10(pr);
	}

	@Override
	public List<PrivacyReport> findTypeReportCount(PrivacyReport pr) {
		return agentMngtDao.findTypeReportCount(pr);
	}

	@Override
	public List<PrivacyReport> findSystemReportCount(PrivacyReport pr) {
		return agentMngtDao.findSystemReportCount(pr);
	}

	@Override
	public List<PrivacyReport> findDeptReportCount(PrivacyReport pr) {
		return agentMngtDao.findDeptReportCount(pr);
	}

	@Override
	public List<PrivacyReport> findIndvReportCount(PrivacyReport pr) {
		return agentMngtDao.findIndvReportCount(pr);
	}
	
	@Override
	public List<PrivacyReport> findReportHalf_chart2(PrivacyReport pr) {
		return reportDao.findReportHalf_chart2(pr);
	}
	
	@Override
	public List<PrivacyReport> findReportHalf_chart3(PrivacyReport pr) {
		return reportDao.findReportHalf_chart3(pr);
	}
	
	@Override
	public List<PrivacyReport> findReportHalf_chart4(PrivacyReport pr) {
		return reportDao.findReportHalf_chart4(pr);
	}
	
	@Override
	public List<PrivacyReport> findReportHalf_chart5(PrivacyReport pr) {
		return reportDao.findReportHalf_chart5(pr);
	}
	
	@Override
	public List<PrivacyReport> findReportHalf_table5(PrivacyReport pr) {
		return reportDao.findReportHalf_table5(pr);
	}
	
	@Override
	public PrivacyReport findReportHalf_chart6(PrivacyReport pr) {
		return reportDao.findReportHalf_chart6(pr);
	}
	
	@Override
	public List<Code> findResultTypeList() {
		return reportDao.findResultTypeList();
	}
	
	@Override
	public DataModelAndView addFile(MultipartHttpServletRequest multi, int type) {
		
		DataModelAndView modelAndView = new DataModelAndView();
		String root = multi.getSession().getServletContext().getRealPath("/") ;
		String path = root + "resources/reportUpload/";
	    String newFileName = "";
	    
		File dir = new File(path);
		if(!dir.isDirectory())
			dir.mkdirs();
		else {
			File[] files = dir.listFiles();
			for(File file : files) {
				String name = file.getName();
				if(name.split("_")[0].equals("type" + type))
					file.delete();
			}
		}
		
		Iterator<String> iFiles = multi.getFileNames();
		while(iFiles.hasNext()) {
			String uploadFile = iFiles.next();
			
			MultipartFile mFile = multi.getFile(uploadFile);
			String fileName = mFile.getOriginalFilename();
			newFileName = "type" + type + "_" + fileName;
			
			try {
				mFile.transferTo(new File(path + newFileName));
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
	    
		modelAndView.addObject("fileName", newFileName);
		modelAndView.addObject("type", type);
		
		return modelAndView;
	}
	
	@Override
	public List<PrivacyReport> getLogByReqType(List<PrivacyReport> list) {
		PrivacyReport prev = new PrivacyReport();
		List<PrivacyReport> list_res = new ArrayList<PrivacyReport>();
		String prevId = "";
		int cnt = 0;
		for(PrivacyReport report : list) {
			
			if(prevId == "" || prevId.equals(report.getEmp_user_id())) {
				ReportData data = new ReportData();
				data.setPrivacy_desc(report.getPrivacy_desc());
				data.setCnt(Integer.parseInt(report.getCnt()));
				
				if(cnt == 0) {
					report.getReportData().add(data);
					prev = report;
				}else { 
					prev.getReportData().add(data);
				}
			}else {
				list_res.add(prev);
				
				ReportData data = new ReportData();
				data.setPrivacy_desc(report.getPrivacy_desc());
				data.setCnt(Integer.parseInt(report.getCnt()));
				report.getReportData().add(data);
				prev = report;
			}
			prevId = report.getEmp_user_id();
			
			cnt++;
			
			if(list.size() == cnt) {
				list_res.add(prev);
			}
		}
		
		return list_res;
	}

	@Override
	public List<AllLogInq> findAllLogInqList(PrivacyReport pr) {
		return reportDao.findAllLogInqList(pr);
	}

	@Override
	public List<AccessAuth> findAccessAuthList(AccessAuthSearch search) {
		return reportDao.findAccessAuthList(search);
	}
	
	@Override
	public int findDownCountTot(PrivacyReport pr) {
		return reportDao.findDownCountTot(pr);
	}
	
	@Override
	public String findDownBySysCountTot(PrivacyReport pr) {
		return reportDao.findDownBySysCountTot(pr);
	}
	
	@Override
	public List<PrivacyReport> findDownloadCntBySysTop5(PrivacyReport pr) {
		return reportDao.findDownloadCntBySysTop5(pr);
	}
	
	@Override
	public List<PrivacyReport> findDownloadCntByDeptTop5(PrivacyReport pr) {
		return reportDao.findDownloadCntByDeptTop5(pr);
	}
	
	@Override
	public List<PrivacyReport> findDownloadCntByResultTypeTop5(PrivacyReport pr) {
		return reportDao.findDownloadCntByResultTypeTop5(pr);
	}
	
	@Override
	public List<PrivacyReport> findDownloadCntBySys(PrivacyReport pr) {
		return reportDao.findDownloadCntBySys(pr);
	}
	
	@Override
	public List<PrivacyReport> findResultCntBySys(PrivacyReport pr) {
		return reportDao.findResultCntBySys(pr);
	}
	
	@Override
	public List<PrivacyReport> findDownloadCntByDeptTop30(PrivacyReport pr) {
		return reportDao.findDownloadCntByDeptTop30(pr);
	}
	
	@Override
	public List<PrivacyReport> findResultCntByDeptTop30(PrivacyReport pr) {
		return reportDao.findResultCntByDeptTop30(pr);
	}
	
	@Override
	public List<PrivacyReport> findDownloadCntByUserTop10(PrivacyReport pr) {
		return reportDao.findDownloadCntByUserTop10(pr);
	}
	
	@Override
	public List<PrivacyReport> findResultCntByUserTop10(PrivacyReport pr) {
		return reportDao.findResultCntByUserTop10(pr);
	}

	@Override
	public List<PrivacyReport> findDownloadCntByUrlTop5(PrivacyReport pr) {
		return reportDao.findDownloadCntByUrlTop5(pr);
	}
	
	@Override
	public List<PrivacyReport> findResultCntByUrlTop5(PrivacyReport pr) {
		return reportDao.findResultCntByUrlTop5(pr);
	}

	@Override
	public List<PrivacyReport> findDownloadCntByResultTop10(PrivacyReport pr) {
		return reportDao.findDownloadCntByResultTop10(pr);
	}
	
	@Override
	public List<PrivacyReport> findResultCntByResultTop10(PrivacyReport pr) {
		return reportDao.findResultCntByResultTop10(pr);
	}
	
	@Override
	public List<PrivacyReport> findSystemNameByUserTop10(PrivacyReport pr) {
		return reportDao.findSystemNameByUserTop10(pr);
	}
	
	@Override
	public List<PrivacyReport> findDownloacCntByReasonTop10(PrivacyReport pr) {
		return reportDao.findDownloacCntByReasonTop10(pr);
	}
	
	@Override
	public List<PrivacyReport> findResultCntByReasonTop10(PrivacyReport pr) {
		return reportDao.findResultCntByReasonTop10(pr);
	}
	
	@Override
	public long removeReportDelete(long report_seq) {
		Report report = reportDao.findReportPdfPath(String.valueOf(report_seq));
		try {
			File pdfFile = new File(report.getFile_path());
			File wordFile = new File(report.getHtml_encode());
			pdfFile.delete();
			wordFile.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return reportDao.removeReportDelete(report_seq);
	}

	@Override
	public String saveReportDownloadTime(long report_seq) {
		reportDao.saveReportDownloadTime(report_seq);
		return reportDao.findReportOne(report_seq);
	}

	@Override
	public DataModelAndView findReportList(SearchBase search,HttpServletRequest request) {
		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		int count = reportDao.findReportList_count(search);
		search.setTotal_count(count);

		DataModelAndView modelAndView = new DataModelAndView();
		
		AdminUser adminUser = (AdminUser) request.getSession().getAttribute("userSession");
		
		Code code = new Code();
		code.setGroup_code_id("REPORT_TYPE");
		code.setUse_flag("Y");
		code.setAuth_id(adminUser.getAuth_id());
		List<Code> reportcode = codeMngtDao.findCodeMngtList(code);
		
		//솔루션 최초 설치일 찾기
		code = new Code();
		code.setGroup_code_id("SOLUTION_MASTER");
		code.setCode_id("INSTALL_DATE");
		code.setUse_flag("Y");
		Code install_date = codeMngtDao.findCodeMngtOne(code);
		Map<String, String> reportMap = new HashMap<String, String>();
		for (Code code2 : reportcode) {
			reportMap.put(code2.getCode_id(),code2.getCode_name());
		}
		
		//접속한 계정이 접근할수 있는 시스템 코드를 가져옴
		AdminUser adminUserBean;
		adminUserBean = SystemHelper.findSessionUser(request);
		AdminUser authBean = adminUserMngtDao.findAdminUserMngtOne(adminUserBean.getAdmin_user_id());
		
		if(authBean.getAuth_ids()==null || authBean.getAuth_ids().trim().equals("") ) {
		}else {
			String[] sys_auth_ids = authBean.getAuth_ids().split(",");
			modelAndView.addObject("sysAuthIds", sys_auth_ids);
		}

		//전체 시스템 목록 MAP으로 담아 넘겨줌
		System system = new System();
		List<SystemMaster> systems = adminUserMngtDao.getSystemMaster();
		Map<String, String> systemMap = new HashMap<String, String>(); 
		for (SystemMaster systemMaster : systems) {
			systemMap.put(systemMaster.getSystem_seq(), systemMaster.getSystem_name());
		}
		
		
		
		modelAndView.addObject("install_date", install_date);
		modelAndView.addObject("userAuth", authBean);
		modelAndView.addObject("systemMap", systemMap);
		modelAndView.addObject("reportMap", reportMap);
		modelAndView.addObject("reportcode", reportcode);
		
		return modelAndView;
	}

	@Override
	public Report findReportPdfPath(String report_seq) {
		return reportDao.findReportPdfPath(report_seq);
	}

	@Override
	public int reportFileValidation(Report report) {
		int result = 0;
		File file = new File(report.getFile_path());
		if(!file.isFile()) {
			result = 1;		//경로내 파일 없음
		}else{
			result = 0;
		}
		return result;
	}
	
	@Override
	public int findAuthInfoAllCount(PrivacyReport pr) {
		return reportDao.findAuthInfoAllCount(pr);
	}
	
	
	@Override
	public List<PrivacyReport> findAuthInfoBySysCount(PrivacyReport pr) {
		return reportDao.findAuthInfoBySysCount(pr);
	}
	
	@Override
	public List<PrivacyReport> findAuthInfoAllCountByDept(PrivacyReport pr) {
		return reportDao.findAuthInfoAllCountByDept(pr);
	}
	
	@Override
	public List<PrivacyReport> findAuthInfoAllCountByAuth(PrivacyReport pr) {
		return reportDao.findAuthInfoAllCountByAuth(pr);
	}
	
	@Override
	public List<PrivacyReport> findAuthInfoAllCountByReqType(PrivacyReport pr) {
		return reportDao.findAuthInfoAllCountByReqType(pr);
	}
		
	@Override
	public List<PrivacyReport> findAuthInfoByDeptCountByReqType(PrivacyReport pr) {
		return reportDao.findAuthInfoByDeptCountByReqType(pr);
	}
	
	@Override
	public List<PrivacyReport> findAuthInfoByAuthCountByReqType(PrivacyReport pr) {
		return reportDao.findAuthInfoByAuthCountByReqType(pr);
	}
		
	@Override
	public List<PrivacyReport> findAuthInfoList2(PrivacyReport pr) {
		return reportDao.findAuthInfoList2(pr);
	}
	
	@Override
	public int findSystemCnt() {
		return reportDao.findSystemCnt();
	}
	
	@Override
	public int findSummaryLogCnt(Report report) {
		return reportDao.findSummaryLogCnt(report);
	}
	
	@Override
	public int findSummonReqCnt(Report report) {
		return reportDao.findSummonReqCnt(report);
	}
	
	@Override
	public int findSummonUserCnt(Report report) {
		return reportDao.findSummonUserCnt(report);
	}
	
	@Override
	public PrivacyReport findSummonSystem(Report report) {
		return reportDao.findSummonSystem(report);
	}
	
	@Override
	public PrivacyReport finddeptTop(Report report) {
		return reportDao.finddeptTop(report);
	}
	
	@Override
	public List<PrivacyReport> findSummonList(Report report) {
		return reportDao.findSummonList(report);
	}
	
	@Override
	public List<PrivacyReport> findSummonStatusList(Report report) {
		return reportDao.findSummonStatusList(report);
	}
	
	@Override
	public List<PrivacyReport> findSummonResultList(Report report) {
		return reportDao.findSummonResultList(report);
	}
	
	@Override
	public List<PrivacyReport> findPrivacytype(Report report) {
		return reportDao.findPrivacytype(report);
	}
	
	@Override
	public List<PrivacyReport> findSummonSystemRepot(Report report) {
		return reportDao.findSummonSystemRepot(report);
	}

	@Override
	public Map<String, String> findReportCode() {
		List<Code> list = reportDao.findReportCodeList();
		Map<String, String> result = new HashMap<String, String>();
		for (Code code : list) {
			result.put(code.getCode_id(), code.getCode_name());
		}
		return result;
	}
	
	//수준진단보고서 START
	@Override
	public int findPrivacyTotalLogCnt(PrivacyReport pr) {
		return reportDao.findPrivacyTotalLogCnt(pr);
	}	
	@Override
	public int findPrivacyTotalCnt(PrivacyReport pr) {
		return reportDao.findPrivacyTotalCnt(pr);
	}
	
	@Override
	public List<PrivacyReport> findPrivacylogCntBySystem(PrivacyReport pr) {
		return reportDao.findPrivacylogCntBySystem(pr);
	}
	@Override
	public PrivacyReport findPrivacylogCntBySystemCompare(PrivacyReport report) {
		return reportDao.findPrivacylogCntBySystemCompare(report);
	}	
	@Override
	public List<PrivacyReport> findPrivacyCntBySystem(PrivacyReport pr) {
		return reportDao.findPrivacyCntBySystem(pr);
	}
	@Override
	public PrivacyReport findPrivacyCntBySystemCompare(PrivacyReport report) {
		return reportDao.findPrivacyCntBySystemCompare(report);
	}
	
	@Override
	public List<PrivacyReport> findPrivacylogCntByDept(PrivacyReport pr) {
		return reportDao.findPrivacylogCntByDept(pr);
	}
	@Override
	public PrivacyReport findPrivacylogCntByDeptCompare(PrivacyReport report) {
		return reportDao.findPrivacylogCntByDeptCompare(report);
	}	
	@Override
	public PrivacyReport findPrivacyCntByDeptCompare(PrivacyReport report) {
		return reportDao.findPrivacyCntByDeptCompare(report);
	}
	
	
	//년간
	
	@Override
	public List<PrivacyReport> privacyProcessCntBySystem(PrivacyReport pr) {
		return reportDao.privacyProcessCntBySystem(pr);
	}
	@Override
	public List<PrivacyReport> privacyUseCntBySystem(PrivacyReport pr) {
		return reportDao.privacyUseCntBySystem(pr);
	}
	@Override
	public List<PrivacyReport> comparePrevCntBySystem(PrivacyReport pr) {
		return reportDao.comparePrevCntBySystem(pr);
	}
	@Override
	public List<PrivacyReport> privacyProcessCntByDept(PrivacyReport pr) {
		return reportDao.privacyProcessCntByDept(pr);
	}
	@Override
	public List<PrivacyReport> privacyUseCntByDept(PrivacyReport pr) {
		return reportDao.privacyUseCntByDept(pr);
	}
	public List<PrivacyReport> comparePrevCntByDept(PrivacyReport pr) {
		return reportDao.comparePrevCntByDept(pr);
	}
	@Override
	public List<PrivacyReport> privacyProcessCntByReqType(PrivacyReport pr) {
		return reportDao.privacyProcessCntByReqType(pr);
	}
	@Override
	public List<PrivacyReport> comparePrevCntByReqType(PrivacyReport pr) {
		return reportDao.comparePrevCntByReqType(pr);
	}
	
	@Override
	public List<PrivacyReport> chart_privacyUseCntBySystem(PrivacyReport pr) {
		return reportDao.chart_privacyUseCntBySystem(pr);
	}
	@Override
	public List<PrivacyReport> chart_privacyUseCntByDept(PrivacyReport pr) {
		return reportDao.chart_privacyUseCntByDept(pr);
	}
	@Override
	public PrivacyReport chart_privacyUseCntByResultType(PrivacyReport pr) {
		return reportDao.chart_privacyUseCntByResultType(pr);
	}
	
	
	
	@Override
	public List<PrivacyReport> sumLogCntByDeptYear(PrivacyReport pr) {
		return reportDao.sumLogCntByDeptYear(pr);
	}
	@Override
	public List<PrivacyReport> findLogCntByDept(PrivacyReport pr) {
		return reportDao.findLogCntByDept(pr);
	}
	
	@Override
	public PrivacyReport findPrivacylogByResultType(PrivacyReport pr) {
		return reportDao.findPrivacylogByResultType(pr);
	}
	
	@Override
	public PrivacyReport privacyUseCntByResultType(PrivacyReport pr) {
		return reportDao.privacyUseCntByResultType(pr);
	}
	
	@Override
	public PrivacyReport comparePrevCntByResultType(PrivacyReport pr) {
		return reportDao.comparePrevCntByResultType(pr);
	}
	
	//수준진단보고서  END

	@SuppressWarnings("unchecked")
	@Override
	public DataModelAndView uploadReportList(UploadReport uploadReport, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView();
		AdminUser upload_user = (AdminUser) request.getSession().getAttribute("userSession");
		uploadReport.setUpload_user(upload_user.getAdmin_user_id());
		List<UploadReport> reportList = reportDao.findUploadReportHist(uploadReport);
		modelAndView.addObject("reportList", reportList);
		modelAndView.setViewName("uploadReportList");
		
		return modelAndView;
	}

	@Override
	public String uploadReportRemove(UploadReport uploadReport, HttpServletRequest request) {
		String result = "success";
		try {
			uploadReport = reportDao.findUploadReportHistOne(uploadReport);
			File file = new File(uploadReport.getFile_path());
			file.delete();
			reportDao.removeUploadReport(uploadReport.getReport_id());
		} catch (Exception e) {
			result = e.getMessage();
		}

		return result;
	}
	
	//다운로드 로그 보고서
	@Override
	public int downloadTotalLogCnt(PrivacyReport pr) {
		return reportDao.downloadTotalLogCnt(pr);
	}
	
	@Override
	public int downloadTotalCnt(PrivacyReport pr) {
		return reportDao.downloadTotalCnt(pr);
	}
	
	@Override
	public List<PrivacyReport> downloadProcessCntBySystem(PrivacyReport pr1) {
		return reportDao.downloadProcessCntBySystem(pr1);
	}
	
	@Override
	public List<PrivacyReport> downloadUseCntBySystem(PrivacyReport pr1) {
		return reportDao.downloadUseCntBySystem(pr1);
	}
	
	@Override
	public List<PrivacyReport> downloadcomparePrevCntBySystem(PrivacyReport pr) {
		return reportDao.downloadcomparePrevCntBySystem(pr);
	}
	@Override
	public List<PrivacyReport> downloadProcessCntByDept(PrivacyReport pr1) {
		return reportDao.downloadProcessCntByDept(pr1);
	}
	
	@Override
	public List<PrivacyReport> downloadUseCntByDept(PrivacyReport pr1) {
		return reportDao.downloadUseCntByDept(pr1);
	}
	
	@Override
	public List<PrivacyReport> downloadComparePrevCntByDept(PrivacyReport pr) {
		return reportDao.downloadComparePrevCntByDept(pr);
	}
	
	@Override
	public PrivacyReport downloadlogByResultType(PrivacyReport pr) {
		return reportDao.downloadlogByResultType(pr);
	}

	@Override
	public int report_upload(Map<String, String> parameters, HttpServletRequest request) {
		String report_seq = parameters.get("report_seq");
		String report_type = parameters.get("report_type");
		String filePath = "";
		AdminUser upload_user = (AdminUser) request.getSession().getAttribute("userSession");
		Report report = new Report();
		MultipartHttpServletRequest mhr = (MultipartHttpServletRequest) request;
		MultipartFile mfile = mhr.getFile("file");
		UUID uuid = UUID.randomUUID();
		report = reportDao.findReportPdfPath(report_seq);
		filePath = report.getFile_path().replace(report.getReport_title()+".pdf", "")+uuid.toString()+"__"+mfile.getOriginalFilename();
		File file = new File(filePath);
		report.setReport_seq(report_seq);
		report.setUserfile_path(filePath);
		
		try {
			mfile.transferTo(file);
		} catch (IllegalStateException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		UploadReport uploadReport = new UploadReport();
		uploadReport.setUpload_user(upload_user.getAdmin_user_id());
		uploadReport.setReport_fk_id(Integer.parseInt(report.getReport_seq()));
		reportDao.saveUploadReportHist(uploadReport);
		
		uploadReport.setReport_type(report.getReport_type());
		uploadReport.setFile_path(filePath);
		uploadReport.setReport_type(report_type);
		
		reportDao.addUploadReportHist(uploadReport);
		return reportDao.updateUserfilePath(report);
	}
	
	@Override
	public List<PrivacyReport> chart_privacyUseCntBySystem_download(PrivacyReport pr) {
		return reportDao.chart_privacyUseCntBySystem_download(pr);
	}
	
	@Override
	public List<PrivacyReport> chart_privacyUseCntByDept_download(PrivacyReport pr) {
		return reportDao.chart_privacyUseCntByDept_download(pr);
	}
	
	@Override
	public PrivacyReport chart_privacyUseCntByResultType_download(PrivacyReport pr) {
		return reportDao.chart_privacyUseCntByResultType_download(pr);
	}
	@Override
	public List<PrivacyReport> getFileExtension_chart(PrivacyReport pr) {
		return reportDao.getFileExtension_chart(pr);
	}

	@Override
	public String uploadReportOne(Map<String, String> parameters) {
		UploadReport uploadReport = new UploadReport();
		int report_fk_id = Integer.parseInt(parameters.get("report_seq"));
		String realFilePath = "";
		uploadReport.setReport_fk_id(report_fk_id);
		uploadReport = reportDao.findUploadReportHistOne(uploadReport);
		if(uploadReport != null) {
			realFilePath = uploadReport.getFile_path();
			String[] filePath = realFilePath.split("/");
			filePath = filePath[filePath.length-1].split("__");
			realFilePath = filePath[filePath.length-1];
		}else {
			realFilePath = "";
		}
		
		return realFilePath;
	}
	
	@Override
	public PrivacyReport downloadComparePrevCntBySystemSum(PrivacyReport pr) {
		return reportDao.downloadComparePrevCntBySystemSum(pr);
	}
	
	@Override
	public List<PrivacyReport> downloadcomparePrevCntBySystemTop3(PrivacyReport pr) {
		return reportDao.downloadcomparePrevCntBySystemTop3(pr);
	}
	
	@Override
	public List<PrivacyReport> downloadComparePrevCntByDeptTop5(PrivacyReport pr) {
		return reportDao.downloadComparePrevCntByDeptTop5(pr);
	}
	
	@Override
	public PrivacyReport comparePrevCntByResultType_download(PrivacyReport pr) {
		return reportDao.comparePrevCntByResultType_download(pr);
	}
	
	@Override
	public List<PrivacyReport> getFileExtensionCompare_chart(PrivacyReport pr) {
		return reportDao.getFileExtensionCompare_chart(pr);
	}
	
	@Override
	public List<PrivacyReport> getFileExtensionCompare_chart_top5(PrivacyReport pr) {
		return reportDao.getFileExtensionCompare_chart_top5(pr);
	}
	
	@Override
	public PrivacyReport getFileExtensionCompare_after(PrivacyReport pr) {
		return reportDao.getFileExtensionCompare_after(pr);
	}

	@Override
	public String addReportOption(Map<String, String> parameters) {
		String result = "success";
		try {
			reportDao.addReportOption(parameters);
		} catch (Exception e) {
			result = "fail";
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public String saveReportOption(Map<String, String> parameters) {
		String result = "success";
		try {
			String desciption = parameters.get("description");
			if(desciption != null) {
				desciption = desciption.replace("\n", "<br>");
			}
			parameters.put("description", desciption);
 			reportDao.saveReportOption(parameters);
		} catch (Exception e) {
			result = "fail";
			e.printStackTrace();
		}
 		return result;
	}

	@Override
	public List<Map<String,String>> findReportOption(Map<String, String> parameters) {
		List<Map<String,String>> result = reportDao.findReportOption(parameters);
		String allCheck = parameters.get("allCheck");
		
		if((result == null || result.size()<1)&&allCheck == null) {	// 내용이 하나도 없을경우 디폴트 멘트 호출
			parameters.put("report_code", parameters.get("code_id"));
			result = new ArrayList<Map<String,String>>();
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("description", reportDao.findDefaultDesc(parameters));
			result.add(map);
		}
		return result;
	}
	
	@Override
	public List<PrivacyReport> getWordReportData1(PrivacyReport pr1) {
		return reportDao.getWordReportData1(pr1);
	}
	
	@Override
	public List<PrivacyReport> getWordReportData2(PrivacyReport pr1) {
		return reportDao.getWordReportData2(pr1);
	}
	
	@Override
	public List<PrivacyReport> getEmpWordGrid01(PrivacyReport pr1) {
		return reportDao.getEmpWordGrid01(pr1);
	}
	@Override
	public List<PrivacyReport> getEmpWordGrid02(PrivacyReport pr1) {
		return reportDao.getEmpWordGrid02(pr1);
	}
	
	@Override
	public Integer checkBeforeReport(Map<String, String> parameters) {
		Integer result = 0;
		String startDate = parameters.get("startDate");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Calendar calendar = new GregorianCalendar();
		int installdate = 0;
		int procdate = 0;
		
		try {
			Date date = sdf.parse(startDate);
			calendar.setTime(date);
			calendar.add(Calendar.MONTH, -1);
			date = calendar.getTime();
			procdate = Integer.parseInt(new SimpleDateFormat("yyyyMM").format(date));
			parameters.put("proc_month",new SimpleDateFormat("yyyyMM").format(date));
			result = reportDao.findBeforeReport(parameters);
			if(result == 0) {
				Code code = new Code();
				code.setCode_id("INSTALL_DATE");
				code.setGroup_code_id("SOLUTION_MASTER");
				code = codeMngtDao.findCodeMngtOne(code);
				installdate = Integer.parseInt(code.getCode_name().replace("-", "").substring(0,6));
				if(installdate == procdate) {
					result = 1;
				}
			} 
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public void makeExcelReportAll(DataModelAndView modelAndView, Map<String, String> map, HttpServletRequest request) {
		String[] dateVal = map.get("start_date").split("-");
		String fileName = dateVal[0]+"년_";
		String period_type = map.get("period_type");
		
		String system_report = map.get("system_report");
		String system_name = "";
		if("Y".equals(system_report)) {
			system_name = reportDao.getSystem_name(map.get("system_seq"));
		}
		
		if(period_type.equals("1")) { // 월간
			if("Y".equals(system_report)) {
				fileName += dateVal[1] + "월_수준진단보고서_" + system_name;
			}else
				fileName += dateVal[1] + "월_수준진단보고서";
		} else if(period_type.equals("2")) { // 분기
			if("Y".equals(system_report)) {
				fileName += map.get("quarter_type") + "분기_수준진단보고서_" + system_name;
			}else
				fileName += map.get("quarter_type") + "분기_수준진단보고서";
		} else if(period_type.equals("4")) { // 연간
			if("Y".equals(system_report)) {
				fileName += "통합수준진단보고서_" + system_name;
			}else
				fileName += "통합수준진단보고서";
		}
		List<Map<String, Object>> modelList = new ArrayList<>();
		String sheetName = "";
		
		Statistics paramBean = new Statistics();
		paramBean.setSearch_from(map.get("start_date"));
		paramBean.setSearch_to(map.get("end_date"));
		String[] systemList = map.get("system_seq").split(",");
		List<String> auth_idsList = Arrays.asList(systemList);
		paramBean.setAuth_idsList(auth_idsList);
		Map<String, Object> temp = new HashMap<>();
		
		// 처리량
		// - 접속기록조회
		paramBean.setLog_delimiter("BA");
		List<Map<String, String>> statics = statisticsDao.findPerdbyStatisticsList(paramBean);
		List<Map<String, String>> procSum = statisticsDao.perdbyStatisticsProcDateSum(paramBean);
		for(int i=0; i<statics.size();i++) {
			Map<String, String> mapSt = statics.get(i);
			String proc_date = mapSt.get("proc_date");
			for(int j=0; j<procSum.size();j++) {
				Map<String, String> procMap = procSum.get(j);
				String proc_date_temp = procMap.get("proc_date");
				if(proc_date.equals(proc_date_temp)) {
					procMap.remove("proc_date");
					mapSt.putAll(procMap);
					break;
				}
			}
		}
		Integer mapCt = 0;
		if(statics!=null && statics.size()>0) {
			mapCt = statics.get(0).size();
		}
		Map<String, String> totalMap = statisticsDao.perdbyStatisticsSum(paramBean);
		Map<String, String> staticsSum = statisticsDao.perdbyStatisticsListSum(paramBean);
		staticsSum.put("proc_date", "합계");
		staticsSum.putAll(totalMap);
		statics.add(staticsSum);
		
		List<SystemMaster> sysList = statisticsDao.findSystemListByAdmin(paramBean);
		String[] columns = new String[mapCt];
		columns[0] = "proc_date";
		for(int i=1; i<mapCt-1 ;i++) {
			int index = i-1;
			String column = "a"+index;
			columns[i] = column;
		}
		columns[mapCt-1] = "logcnt";
		
		String[] heads = new String[sysList.size()+2];
		heads[0] = "날짜";
		for(int i=1; i<heads.length-1 ;i++) {
			heads[i] = sysList.get(i-1).getSystem_name();
		}
		heads[heads.length-1] = "합계";
		
		sheetName = "기간별시스템별처리량(접속기록조회)";
		temp.put("sheetName", sheetName);
		temp.put("excelData", statics);
		temp.put("columns", columns);
		temp.put("heads", heads);
		temp.put("sheetNum", 0);
		modelList.add(temp);
		// - 시스템로그조회
		paramBean.setLog_delimiter("BS");
		statics = statisticsDao.findPerdbyStatisticsList(paramBean);
		procSum = statisticsDao.perdbyStatisticsProcDateSum(paramBean);
		for(int i=0; i<statics.size();i++) {
			Map<String, String> mapSt = statics.get(i);
			String proc_date = mapSt.get("proc_date");
			for(int j=0; j<procSum.size();j++) {
				Map<String, String> procMap = procSum.get(j);
				String proc_date_temp = procMap.get("proc_date");
				if(proc_date.equals(proc_date_temp)) {
					procMap.remove("proc_date");
					mapSt.putAll(procMap);
					break;
				}
			}
		}
		totalMap = statisticsDao.perdbyStatisticsSum(paramBean);
		staticsSum = statisticsDao.perdbyStatisticsListSum(paramBean);
		staticsSum.put("proc_date", "합계");
		staticsSum.putAll(totalMap);
		statics.add(staticsSum);
		
		temp = new HashMap<>();
		sheetName = "기간별시스템별처리량(시스템로그조회)";
		temp.put("sheetName", sheetName);
		temp.put("excelData", statics);
		temp.put("columns", columns);
		temp.put("heads", heads);
		temp.put("sheetNum", 1);
		modelList.add(temp);
		
		// 기간별
		List<Statistics> statistics = new ArrayList<>();
		Statistics statisticsSum = new Statistics();
		List<Code> resultTypes = commonDao.getPrivacyDescList();
		columns = new String[resultTypes.size() + 2];
		columns[0] = "proc_date_statistics";
		for (int i = 1; i <= resultTypes.size(); i++) {
			Code code = resultTypes.get(i-1);
			columns[i] = "type" + code.getResult_type_order();
		}
		columns[resultTypes.size() + 1] = "type_sum";
		heads = new String[resultTypes.size() + 2];
		heads[0] = "날짜";
		for (int i = 1; i <= resultTypes.size(); i++) {
			Code result = resultTypes.get(i - 1);
			heads[i] = result.getCode_name();
		}
		heads[resultTypes.size() + 1] = "합계";
		// - 접속기록조회
		paramBean.setSearchType("01");
		statistics = statisticsDao.findPerdbyTypeAnalsList(paramBean);
		statisticsSum = statisticsDao.findStatisticsSumByPrivacyType(paramBean);
		statisticsSum.setProc_date_statistics("합계");
		statistics.add(statisticsSum);
		temp = new HashMap<>();
		sheetName = "기간별유형통계(접속기록조회)";
		temp.put("sheetName", sheetName);
		temp.put("excelData", statistics);
		temp.put("columns", columns);
		temp.put("heads", heads);
		temp.put("sheetNum", 2);
		modelList.add(temp);
		// - 시스템로그조회
		paramBean.setSearchType("02");
		statistics = statisticsDao.findPerdbyTypeAnalsList(paramBean);
		statisticsSum = statisticsDao.findStatisticsSumByPrivacyType(paramBean);
		statisticsSum.setProc_date_statistics("합계");
		statistics.add(statisticsSum);
		temp = new HashMap<>();
		sheetName = "기간별유형통계(시스템로그조회)";
		temp.put("sheetName", sheetName);
		temp.put("excelData", statistics);
		temp.put("columns", columns);
		temp.put("heads", heads);
		temp.put("sheetNum", 3);
		modelList.add(temp);
		
		// 시스템별
		columns = new String[resultTypes.size() + 2];
		columns[0] = "system_name";
		for (int i = 1; i <= resultTypes.size(); i++) {
			Code code = resultTypes.get(i-1);
			columns[i] = "type" + code.getResult_type_order();
		}
		columns[resultTypes.size() + 1] = "type_sum";
		heads = new String[resultTypes.size() + 2];
		heads[0] = "시스템명";
		for (int i = 1; i <= resultTypes.size(); i++) {
			Code result = resultTypes.get(i - 1);
			heads[i] = result.getCode_name();
		}
		heads[resultTypes.size() + 1] = "합계";
		// - 접속기록조회
		paramBean.setSearchType("01");
		statistics = statisticsDao.findSysbyTypeAnalsList_new(paramBean);
		statisticsSum = statisticsDao.findStatisticsSumByPrivacyType(paramBean);
		statisticsSum.setSystem_name("합계");
		statistics.add(statisticsSum);
		temp = new HashMap<>();
		sheetName = "시스템별유형통계(접속기록조회)";
		temp.put("sheetName", sheetName);
		temp.put("excelData", statistics);
		temp.put("columns", columns);
		temp.put("heads", heads);
		temp.put("sheetNum", 4);
		modelList.add(temp);
		// - 시스템로그조회
		paramBean.setSearchType("02");
		statistics = statisticsDao.findSysbyTypeAnalsList_new(paramBean);
		statisticsSum = statisticsDao.findStatisticsSumByPrivacyType(paramBean);
		statisticsSum.setSystem_name("합계");
		statistics.add(statisticsSum);
		temp = new HashMap<>();
		sheetName = "시스템별유형통계(시스템로그조회)";
		temp.put("sheetName", sheetName);
		temp.put("excelData", statistics);
		temp.put("columns", columns);
		temp.put("heads", heads);
		temp.put("sheetNum", 5);
		modelList.add(temp);
		
		// 소속별
		columns = new String[resultTypes.size() + 2];
		columns[0] = "dept_name";
		for (int i = 1; i <= resultTypes.size(); i++) {
			Code code = resultTypes.get(i-1);
			columns[i] = "type" + code.getResult_type_order();
		}
		columns[resultTypes.size() + 1] = "type_sum";

		heads = new String[resultTypes.size() + 2];
		heads[0] = "소속";
		for (int i = 1; i <= resultTypes.size(); i++) {
			Code result = resultTypes.get(i - 1);
			heads[i] = result.getCode_name();
		}
		heads[resultTypes.size() + 1] = "합계";
		// - 접속기록조회
		paramBean.setSearchType("01");
		statistics = statisticsDao.findDeptbyTypeAnalsList_new(paramBean);
		statisticsSum = statisticsDao.findStatisticsSumByPrivacyType(paramBean);
		statisticsSum.setDept_name("합계");
		statistics.add(statisticsSum);
		temp = new HashMap<>();
		sheetName = "소속별유형통계(접속기록조회)";
		temp.put("sheetName", sheetName);
		temp.put("excelData", statistics);
		temp.put("columns", columns);
		temp.put("heads", heads);
		temp.put("sheetNum", 6);
		modelList.add(temp);

		// 개인별
		columns = new String[resultTypes.size() + 3];
		columns[0] = "emp_user_name";
		columns[1] = "dept_name";
		for (int i = 1; i <= resultTypes.size(); i++) {
			Code code = resultTypes.get(i-1);
			columns[i + 1] = "type" + code.getResult_type_order();
		}
		columns[resultTypes.size() + 2] = "type_sum";

		heads = new String[resultTypes.size() + 3];
		heads[0] = "사용자명";
		heads[1] = "소속";
		for (int i = 1; i <= resultTypes.size(); i++) {
			Code result = resultTypes.get(i - 1);
			heads[i + 1] = result.getCode_name();
		}
		heads[resultTypes.size() + 2] = "합계";
		PageInfo pageInfo = new PageInfo(1, 1);
		pageInfo.setUseExcel("true");
		paramBean.setPageInfo(pageInfo);
		paramBean.setSearchType("01");
		// - 접속기록조회
		statistics = statisticsDao.findIndvbyTypeAnalsList_new(paramBean);
		statisticsSum = statisticsDao.findStatisticsSumByPrivacyType(paramBean);
		statisticsSum.setEmp_user_name("합계");
		statistics.add(statisticsSum);
		temp = new HashMap<>();
		sheetName = "개인별유형통계(접속기록조회)";
		temp.put("sheetName", sheetName);
		temp.put("excelData", statistics);
		temp.put("columns", columns);
		temp.put("heads", heads);
		temp.put("sheetNum", 7);
		modelList.add(temp);
		
		
		// 시스템별수행업무통계
		paramBean.setLog_delimiter("BA");
		paramBean.setReq_type_list(statisticsDao.findReportReqTypeList(paramBean));
		
		columns = new String[paramBean.getReq_type_list().size()+2];
		columns[0] = "system_name";
		columns[columns.length-1] = "result_sum";
		
		heads = new String[columns.length];
		heads[0] = "시스템명";
		heads[heads.length-1] = "합계";
		
		for (int i = 1; i < columns.length-1; i++) {
			columns[i] = paramBean.getReq_type_list().get(i-1).split(",")[0].toLowerCase();
			heads[i] = paramBean.getReq_type_list().get(i-1).split(",")[1];
			paramBean.getReq_type_list().set(i-1, columns[i].toUpperCase());
		}
		// - 접속기록조회
		List<Map<String,String>> reqCount = statisticsDao.findReportReqTypeCountList(paramBean);
		temp = new HashMap<>();
		sheetName = "시스템별수행업무통계(접속기록조회)";
		temp.put("sheetName", sheetName);
		temp.put("excelData", reqCount);
		temp.put("columns", columns);
		temp.put("heads", heads);
		temp.put("sheetNum", 8);
		modelList.add(temp);
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("modelList", modelList);
		
		// 시스템별수행업무통계
		paramBean.setLog_delimiter("BS");
		paramBean.setReq_type_list(statisticsDao.findReportReqTypeList(paramBean));
		
		columns = new String[paramBean.getReq_type_list().size()+2];
		columns[0] = "system_name";
		columns[columns.length-1] = "result_sum";
		
		heads = new String[columns.length];
		heads[0] = "시스템명";
		heads[heads.length-1] = "합계";
		
		for (int i = 1; i < columns.length-1; i++) {
			columns[i] = paramBean.getReq_type_list().get(i-1).split(",")[0].toLowerCase();
			heads[i] = paramBean.getReq_type_list().get(i-1).split(",")[1];
			paramBean.getReq_type_list().set(i-1, columns[i].toUpperCase());
		}
		// - 시스템로그조회
		reqCount = statisticsDao.findReportReqTypeCountList(paramBean);
		temp = new HashMap<>();
		sheetName = "시스템별수행업무통계(시스템로그조회)";
		temp.put("sheetName", sheetName);
		temp.put("excelData", reqCount);
		temp.put("columns", columns);
		temp.put("heads", heads);
		temp.put("sheetNum", 9);
		modelList.add(temp);
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("modelList", modelList);
		
	
		
	}

	
	@Override
	public void makeExcelReportDown(DataModelAndView modelAndView, Map<String, String> map, HttpServletRequest request) {
		String[] dateVal = map.get("start_date").split("-");
		String fileName = dateVal[0]+"년_";
		String period_type = map.get("period_type");
		String system_report = map.get("system_report");
		String system_name = "";
		if("Y".equals(system_report)) {
			system_name = reportDao.getSystem_name(map.get("system_seq"));
		}
		
		if(period_type.equals("1")) { // 월간
			if("Y".equals(system_report)) {
				fileName += dateVal[1] + "월_다운로드보고서_" + system_name;
			}else
				fileName += dateVal[1] + "월_다운로드보고서";
		} else if(period_type.equals("2")) { // 분기
			if("Y".equals(system_report)) {
				fileName += map.get("quarter_type") + "분기_다운로드보고서_" + system_name;
			}else
				fileName += map.get("quarter_type") + "분기_다운로드보고서";
		} else if(period_type.equals("4")) { // 연간
			if("Y".equals(system_report)) {
				fileName += "통합다운로드보고서_" + system_name;
			}else
				fileName += "통합다운로드보고서";
		}
		List<Map<String, Object>> modelList = new ArrayList<>();
		String sheetName = "";
		
		Statistics paramBean = new Statistics();
		paramBean.setSearch_from(map.get("start_date"));
		paramBean.setSearch_to(map.get("end_date"));
		String[] systemList = map.get("system_seq").split(",");
		List<String> auth_idsList = Arrays.asList(systemList);
		paramBean.setAuth_idsList(auth_idsList);
		Map<String, Object> temp = new HashMap<>();
		
		
		// 처리량
		paramBean.setLog_delimiter("DN");
		List<Map<String, String>> statics = statisticsDao.findPerdbyStatisticsList(paramBean);
		List<Map<String, String>> procSum = statisticsDao.perdbyStatisticsProcDateSum(paramBean);
		
		for(int i=0; i<statics.size();i++) {
			Map<String, String> mapSt = statics.get(i);
			String proc_date = mapSt.get("proc_date");
			for(int j=0; j<procSum.size();j++) {
				Map<String, String> procMap = procSum.get(j);
				String proc_date_temp = procMap.get("proc_date");
				if(proc_date.equals(proc_date_temp)) {
					procMap.remove("proc_date");
					mapSt.putAll(procMap);
					break;
				}
			}
		}
		Integer mapCt = 0;
		if(statics!=null && statics.size()>0) {
			mapCt = statics.get(0).size();
		}
		Map<String, String> totalMap = statisticsDao.perdbyStatisticsSum(paramBean);
		Map<String, String> staticsSum = statisticsDao.perdbyStatisticsListSum(paramBean);
		staticsSum.put("proc_date", "합계");
		staticsSum.putAll(totalMap);
		statics.add(staticsSum);
		
		List<SystemMaster> sysList = statisticsDao.findSystemListByAdmin(paramBean);
		String[] columns = new String[mapCt];
		columns[0] = "proc_date";
		for(int i=1; i<mapCt-1 ;i++) {
			int index = i-1;
			String column = "a"+index;
			columns[i] = column;
		}
		columns[mapCt-1] = "logcnt";
		
		String[] heads = new String[mapCt];
		heads[0] = "날짜";
		for(int i=1; i<mapCt-1 ;i++) {
			heads[i] = sysList.get(i-1).getSystem_name();
		}
		heads[mapCt-1] = "합계";
		
		sheetName = "기간별시스템별처리량(다운로드로그조회)";
		temp.put("sheetName", sheetName);
		temp.put("excelData", statics);
		temp.put("columns", columns);
		temp.put("heads", heads);
		temp.put("sheetNum", 0);
		modelList.add(temp);
		
		// 기간별
		List<Statistics> statistics = new ArrayList<>();
		Statistics statisticsSum = new Statistics();
		List<Code> resultTypes = commonDao.getPrivacyDescList();
		columns = new String[resultTypes.size() + 2];
		columns[0] = "proc_date_statistics";
		for (int i = 1; i <= resultTypes.size(); i++) {
			Code code = resultTypes.get(i-1);
			columns[i] = "type" + code.getResult_type_order();
		}
		columns[resultTypes.size() + 1] = "type_sum";
		heads = new String[resultTypes.size() + 2];
		heads[0] = "날짜";
		for (int i = 1; i <= resultTypes.size(); i++) {
			Code result = resultTypes.get(i - 1);
			heads[i] = result.getCode_name();
		}
		heads[resultTypes.size() + 1] = "합계";
		// - 다운로드 로그조회
		statistics = statisticsDao.findPerdbyDownTypeAnalsList(paramBean);
		statisticsSum = statisticsDao.findStatisticsSumByDownloadType(paramBean);
		statisticsSum.setProc_date_statistics("합계");
		statistics.add(statisticsSum);
		temp = new HashMap<>();
		sheetName = "기간별유형통계(다운로드로그조회)";
		temp.put("sheetName", sheetName);
		temp.put("excelData", statistics);
		temp.put("columns", columns);
		temp.put("heads", heads);
		temp.put("sheetNum", 1);
		modelList.add(temp);
		
		// 시스템별
		columns = new String[resultTypes.size() + 2];
		columns[0] = "system_name";
		for (int i = 1; i <= resultTypes.size(); i++) {
			Code code = resultTypes.get(i-1);
			columns[i] = "type" + code.getResult_type_order();
		}
		columns[resultTypes.size() + 1] = "type_sum";
		heads = new String[resultTypes.size() + 2];
		heads[0] = "시스템명";
		for (int i = 1; i <= resultTypes.size(); i++) {
			Code result = resultTypes.get(i - 1);
			heads[i] = result.getCode_name();
		}
		heads[resultTypes.size() + 1] = "합계";
		// - 다운로드 로그조회
		statistics = statisticsDao.findSysbyDownloadTypeAnalsList(paramBean);
		statisticsSum = statisticsDao.findStatisticsSumByDownloadType(paramBean);
		statisticsSum.setSystem_name("합계");
		statistics.add(statisticsSum);
		temp = new HashMap<>();
		sheetName = "시스템별유형통계(다운로드로그조회)";
		temp.put("sheetName", sheetName);
		temp.put("excelData", statistics);
		temp.put("columns", columns);
		temp.put("heads", heads);
		temp.put("sheetNum", 2);
		modelList.add(temp);
		
		// 소속별
		columns = new String[resultTypes.size() + 2];
		columns[0] = "dept_name";
		for (int i = 1; i <= resultTypes.size(); i++) {
			Code code = resultTypes.get(i-1);
			columns[i] = "type" + code.getResult_type_order();
		}
		columns[resultTypes.size() + 1] = "type_sum";

		heads = new String[resultTypes.size() + 2];
		heads[0] = "소속";
		for (int i = 1; i <= resultTypes.size(); i++) {
			Code result = resultTypes.get(i - 1);
			heads[i] = result.getCode_name();
		}
		heads[resultTypes.size() + 1] = "합계";
		// - 다운로드 로그조회
		statistics = statisticsDao.findDeptbyDownloadTypeAnalsList(paramBean);
		statisticsSum = statisticsDao.findStatisticsSumByDownloadType(paramBean);
		statisticsSum.setDept_name("합계");
		statistics.add(statisticsSum);
		temp = new HashMap<>();
		sheetName = "소속별유형통계(다운로드로그조회)";
		temp.put("sheetName", sheetName);
		temp.put("excelData", statistics);
		temp.put("columns", columns);
		temp.put("heads", heads);
		temp.put("sheetNum", 3);
		modelList.add(temp);

		// 개인별
		columns = new String[resultTypes.size() + 3];
		columns[0] = "emp_user_name";
		columns[1] = "dept_name";
		for (int i = 1; i <= resultTypes.size(); i++) {
			Code code = resultTypes.get(i-1);
			columns[i + 1] = "type" + code.getResult_type_order();
		}
		columns[resultTypes.size() + 2] = "type_sum";

		heads = new String[resultTypes.size() + 3];
		heads[0] = "사용자명";
		heads[1] = "소속";
		for (int i = 1; i <= resultTypes.size(); i++) {
			Code result = resultTypes.get(i - 1);
			heads[i + 1] = result.getCode_name();
		}
		heads[resultTypes.size() + 2] = "합계";
		PageInfo pageInfo = new PageInfo(1, 1);
		pageInfo.setUseExcel("true");
		paramBean.setPageInfo(pageInfo);
		// - 다운로드 로그조회
		statistics = statisticsDao.findIndvbyDownloadTypeAnalsList(paramBean);
		statisticsSum = statisticsDao.findStatisticsSumByDownloadType(paramBean);
		statisticsSum.setEmp_user_name("합계");
		statistics.add(statisticsSum);
		temp = new HashMap<>();
		sheetName = "개인별유형통계(다운로드로그조회)";
		temp.put("sheetName", sheetName);
		temp.put("excelData", statistics);
		temp.put("columns", columns);
		temp.put("heads", heads);
		temp.put("sheetNum", 4);
		modelList.add(temp);
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("modelList", modelList);
		
	}

	@Override
	public String reportAuthorizeInfo() {
		return reportDao.findReportAuthorizeInfo();
	}

	@Override
	public String inspectionDataSave(Map<String, String> map,HttpServletRequest request) {
		String reportDate = map.get("proc_date");
		String jsonData = map.get("report_data");
		String reportType = map.get("report_type");
		String search_fr = map.get("search_fr");
		String search_to = map.get("search_to");
		String report_title = map.get("report_title");
		String update_id = ((AdminUser)request.getSession().getAttribute("userSession")).getAdmin_user_id();
		String reportSeq = null;
		InspectionReport report = new InspectionReport();
		if((reportSeq = map.get("report_seq"))!=null) {
			report.setReport_seq(reportSeq);
		}
		
		String result = "success";
		
		report.setProc_date(reportDate);
		report.setReport_data(jsonData);
		report.setReport_type(reportType);
		report.setUpdate_id(update_id);
		report.setSearch_fr(search_fr);
		report.setSearch_to(search_to);
		report.setReport_title(report_title);
		try {
			reportDao.addInspectionReport(report);
		} catch (Exception e) {
			e.printStackTrace();
			result = "fail";
		}
		return result;
	}

	@Override
	public List<PrivacyReport> chart_findPrivacyReportByReqtype(PrivacyReport pr) {
		return reportDao.chart_findPrivacyReportByReqtype(pr);
	}
	
	@Override
	public String inspectionDataLoad(Map<String, String> param) {
		String reportDate = param.get("proc_date");
		String reportType = param.get("report_type");
		String reportSeq = null;
		
		InspectionReport report = new InspectionReport();
		if((reportSeq = param.get("report_seq"))!=null) {
			report.setReport_seq(reportSeq);
		}
		report.setProc_date(reportDate);
		report.setReport_type(reportType);
		
		report = reportDao.findInspectionReportOne(report);

		String result = "{}";
		if(report != null) {
			result = report.getReport_data();
		}
		return result;
	}

	@Override
	public DataModelAndView report_inspection(String search_fr, String search_to) {
		DataModelAndView modelAndView = new DataModelAndView();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	   	search_fr = search_fr.replaceAll("-", "");
	   	search_to = search_to.replaceAll("-", "");
	   	
	    Calendar cal = sdf.getCalendar();
	    try {
			cal.setTime(sdf.parse(search_fr));
			String frDate = sdf.format(cal.getTime());
		    cal.add(Calendar.MONTH, -1);
		    String frDate2 = sdf.format(cal.getTime());
		    
		    cal.setTime(sdf.parse(search_to));
		    String toDate = sdf.format(cal.getTime());
		    cal.add(Calendar.MONTH, -1);
		    String toDate2 = sdf.format(cal.getTime());
		    
		    PrivacyReport pr = new PrivacyReport();
		    pr.setStart_date(frDate);
		    pr.setEnd_date(toDate);
		    List<PrivacyReport> list = reportDao.findReport_inspection(pr);
		    
		    pr.setStart_date(frDate2);
		    pr.setEnd_date(toDate2);
		    List<PrivacyReport> list2 = reportDao.findReport_inspection(pr);
		    
		    for(int i=0; i<list.size(); i++) {
		    	PrivacyReport report = list.get(i);
		    	report.setType2(list2.get(i).getType1());
		    }
		    
		    modelAndView.addObject("frDate", frDate);
		    modelAndView.addObject("frDate2", frDate2);
		    modelAndView.addObject("toDate", toDate);
		    modelAndView.addObject("toDate2", toDate2);
		    modelAndView.addObject("list", list);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    modelAndView.addObject("today", sdf.format(new Date()));
		return modelAndView;
	}

	@Override
	public DataModelAndView report_inspection(String report_seq) {
		InspectionReport report = new InspectionReport();
		report.setReport_seq(report_seq);
		report = reportDao.findInspectionReportOne(report);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	   	String search_fr = report.getSearch_fr().replaceAll("-", "");
	   	String search_to = report.getSearch_to().replaceAll("-", "");
	   	
	    report_inspection(search_fr, search_to);
		return report_inspection(search_fr, search_to);
	}

	@Override
	public String inspectionDataDelete(Map<String, String> param, HttpServletRequest request) {
		String result = "success";
		try {
			InspectionReport report = new InspectionReport();
			report.setReport_seq(param.get("report_seq"));
			reportDao.deleteInspectionReportOne(report);
		} catch (Exception e) {
			result = "fail";
		}
		
		return result;
	}

	@Override
	public List<PrivacyReport> abnormalTotalListBySystem(PrivacyReport pr, int period_type, String start_date) {
		List<PrivacyReport> list = new ArrayList<>();
		try {
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
			cal.setTime(sdf.parse(start_date));
			if(period_type == 2) {// 분기
				List<ReportData> reportData = new ArrayList<>();
				for(int i=0 ; i<3 ;i ++) {
					ReportData rd = new ReportData();
					rd.setStart_date(sdf.format(cal.getTime()));
					rd.setEnd_date(sdf.format(cal.getTime()));
					cal.add(Calendar.MONTH, 1);
					reportData.add(rd);
				}
				pr.setReportData(reportData);
			} else if(period_type == 4) {// 연간
				List<ReportData> reportData = new ArrayList<>();
				for(int i=0 ; i<4 ;i ++) {
					ReportData rd = new ReportData();
					rd.setStart_date(sdf.format(cal.getTime()));
					cal.add(Calendar.MONTH, 2);
					rd.setEnd_date(sdf.format(cal.getTime()));
					cal.add(Calendar.MONTH, 1);
					reportData.add(rd);
				}
				pr.setReportData(reportData);
			}
			list = reportDao.abnormalTotalListBySystem(pr);
			int totalCnt = 0, logcnt = 0;
			int cnt0 = 0, cnt1 = 0, cnt2 = 0, cnt3 = 0;
			int logcnt0 = 0, logcnt1 = 0, logcnt2 = 0, logcnt3 = 0;
			for( PrivacyReport p : list ) {
				totalCnt += p.getTotalCnt();
				logcnt += p.getLogcnt();
				if(period_type==2) {
					cnt0 += p.getCnt0();
					cnt1 += p.getCnt1();
					cnt2 += p.getCnt2();
					logcnt0 += p.getLogcnt0();
					logcnt1 += p.getLogcnt1();
					logcnt2 += p.getLogcnt2();
				}
				if(period_type==4) {
					cnt3 += p.getCnt3();
					logcnt3 += p.getLogcnt3();
				}
			}
			PrivacyReport temp = new PrivacyReport();
			temp.setTotalCnt(totalCnt);
			temp.setLogcnt(logcnt);
			if(period_type==2) {
				temp.setCnt0(cnt0);
				temp.setCnt1(cnt1);
				temp.setCnt2(cnt2);
				temp.setLogcnt0(logcnt0);
				temp.setLogcnt1(logcnt1);
				temp.setLogcnt2(logcnt2);
			}
			if(period_type==4) {
				temp.setCnt3(cnt3);
				temp.setLogcnt3(logcnt3);
			}
			list.add(temp);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	

	@Override
	public int abnormalTotalCnt(PrivacyReport pr) {
		return reportDao.abnormalTotalCnt(pr);
	}
	@Override
	public List<PrivacyReport> abnormalSystemList(PrivacyReport pr) {
		List<PrivacyReport> list = reportDao.abnormalSystemList(pr);
		return abnormalScenarioSumCal(list);
	}
	@Override
	public List<PrivacyReport> abnormalDeptList(PrivacyReport pr) {
		List<PrivacyReport> list = reportDao.abnormalDeptList(pr);
		return abnormalScenarioSumCal(list);
	}
	@Override
	public List<PrivacyReport> abnormalUserList(PrivacyReport pr) {
		List<PrivacyReport> list = reportDao.abnormalUserList(pr);
		return abnormalScenarioSumCal(list);
	}
	public List<PrivacyReport> abnormalScenarioSumCal(List<PrivacyReport> list) {
		int scenario1 = 0;
		int scenario2 = 0;
		int scenario3 = 0;
		int scenario4 = 0;
		int scenario5 = 0;
		int totalCnt = 0;
		for(PrivacyReport p : list) {
			scenario1 += p.getScenario1();
			scenario2 += p.getScenario2();
			scenario3 += p.getScenario3();
			scenario4 += p.getScenario4();
			scenario5 += p.getScenario5();
			totalCnt += p.getTotalCnt();
		}
		PrivacyReport temp = new PrivacyReport();
		temp.setScenario1(scenario1);
		temp.setScenario2(scenario2);
		temp.setScenario3(scenario3);
		temp.setScenario4(scenario4);
		temp.setScenario5(scenario5);
		temp.setTotalCnt(totalCnt);
		list.add(temp);
		return list;
	}

	@Override
	public List<PrivacyReport> abnormalSystemTop5(PrivacyReport pr) {
		List<PrivacyReport> list = reportDao.abnormalSystemTop5(pr);
		PrivacyReport other = reportDao.abnormalSystemTop5Other(pr);
		//if(other != null)
		//	list.add(other);
		return abnormalSumCal(list);
	}

	@Override
	public List<PrivacyReport> abnormalDeptTop5(PrivacyReport pr) {
		List<PrivacyReport> list = reportDao.abnormalDeptTop5(pr);
		PrivacyReport other = reportDao.abnormalDeptTop5Other(pr);
		//if(other != null)
		//	list.add(other);
		return abnormalSumCal(list);
	}

	@Override
	public List<PrivacyReport> abnormalUserTop5(PrivacyReport pr) {
		List<PrivacyReport> list = reportDao.abnormalUserTop5(pr);
		PrivacyReport other = reportDao.abnormalUserTop5Other(pr);
		//if(other != null)
		//	list.add(other);
		return abnormalSumCal(list);
	}
	public List<PrivacyReport> abnormalSumCal(List<PrivacyReport> list) {
		int totalCnt = 0;
		int logcnt = 0;
		int compareCnt = 0;
		int compareLogCnt = 0;
		
		for(PrivacyReport p : list) {
			if(p != null) {
				totalCnt += p.getTotalCnt();
				logcnt += p.getLogcnt();
				compareCnt += p.getCompareCnt();
				compareLogCnt += p.getCompareLogCnt();
			}
		}
		PrivacyReport temp = new PrivacyReport();
		temp.setTotalCnt(totalCnt);
		temp.setLogcnt(logcnt);
		temp.setCompareCnt(compareCnt);
		temp.setCompareLogCnt(compareLogCnt);
		list.add(temp);
		return list;
	}

	@Override
	public List<PrivacyReport> abnormalSystemTop5Chart(PrivacyReport pr) {
		return reportDao.abnormalSystemTop5Chart(pr);
	}

	@Override
	public List<PrivacyReport> abnormalDeptTop5Chart(PrivacyReport pr) {
		return reportDao.abnormalDeptTop5Chart(pr);
	}

	@Override
	public List<PrivacyReport> abnormalUserTop5Chart(PrivacyReport pr) {
		return reportDao.abnormalUserTop5Chart(pr);
	}
	
	@Override
	public List<PrivacyReport> comparePrevCntByUser(PrivacyReport pr) {
		return reportDao.comparePrevCntByUser(pr);
	}
	@Override
	public List<PrivacyReport> chart_privacyUseCntByUser(PrivacyReport pr) {
		return reportDao.chart_privacyUseCntByUser(pr);
	}

	
	@Override
	public int systemReportDelete(Report parameters) {
		return reportDao.systemReportDelete(parameters);
	}
	@Override
	public Report findSystemReportPath(Report report) {
		return reportDao.findSystemReportPath(report);
	}
	
	@Override
	public String reportAuthorizeInfo_system(String system_seq) {
		return reportDao.reportAuthorizeInfo_system(system_seq);
	}
	@Override
	public List<PrivacyReport> findOrgRuleInfoIsks() {
		return reportDao.findOrgRuleInfoIsks();
	}
	@Override
	public PrivacyReport findTotalSummonCntIsks(Report report) {
		return reportDao.findTotalSummonCntIsks(report);
	}
	@Override
	public PrivacyReport findTotalExtrtCntIsks(Report report) {
		return reportDao.findTotalExtrtCntIsks(report);
	}
	@Override
	public List<PrivacyReport> findTotalRuleSystemCntIsks(Report report) {
		return reportDao.findTotalRuleSystemCntIsks(report);

	}
}