package com.easycerti.eframe.psm.report.vo;

import java.util.Date;

public class ReportList {
	private int rownum;
	private long report_seq;
	private String report_title;
	private String update_id;
	private Date update_time;
	private String url;
	private Date download_time;
	private Date delete_time;
	private String download_flag;
	private int download_cnt;
	private String proc_month;
	private String report_type;
	private String period_type;
	private String userfile_path;
	private String auth_id;
	private String auth_type;
	
	private String system_report;
	
	public ReportList() {}


	
	
	public String getAuth_type() {
		return auth_type;
	}
	public void setAuth_type(String auth_type) {
		this.auth_type = auth_type;
	}
	public String getAuth_id() {
		return auth_id;
	}
	public void setAuth_id(String auth_id) {
		this.auth_id = auth_id;
	}
	public String getUserfile_path() {
		return userfile_path;
	}

	public void setUserfile_path(String userfile_path) {
		this.userfile_path = userfile_path;
	}

	public String getPeriod_type() {
		return period_type;
	}
	
	public void setPeriod_type(String period_type) {
		this.period_type = period_type;
	}


	public String getProc_month() {
		return proc_month;
	}


	public void setProc_month(String proc_month) {
		this.proc_month = proc_month;
	}


	public String getReport_type() {
		return report_type;
	}

	public void setReport_type(String report_type) {
		this.report_type = report_type;
	}

	public int getRownum() {
		return rownum;
	}

	public void setRownum(int rownum) {
		this.rownum = rownum;
	}

	public long getReport_seq() {
		return report_seq;
	}

	public void setReport_seq(long report_seq) {
		this.report_seq = report_seq;
	}

	public String getReport_title() {
		return report_title;
	}

	public void setReport_title(String report_title) {
		this.report_title = report_title;
	}

	public String getUpdate_id() {
		return update_id;
	}

	public void setUpdate_id(String update_id) {
		this.update_id = update_id;
	}

	public Date getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(Date update_time) {
		this.update_time = update_time;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Date getDownload_time() {
		return download_time;
	}

	public void setDownload_time(Date download_time) {
		this.download_time = download_time;
	}

	public Date getDelete_time() {
		return delete_time;
	}

	public void setDelete_time(Date delete_time) {
		this.delete_time = delete_time;
	}

	public String getDownload_flag() {
		return download_flag;
	}

	public void setDownload_flag(String download_flag) {
		this.download_flag = download_flag;
	}
	
	public int getDownload_cnt() {
		return download_cnt;
	}

	public void setDownload_cnt(int download_cnt) {
		this.download_cnt = download_cnt;
	}public String getSystem_report() {
		return system_report;
	}
	public void setSystem_report(String system_report) {
		this.system_report = system_report;
	}	
}
