package com.easycerti.eframe.psm.report.dao;

import java.util.List;
import java.util.Map;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.vo.SearchBase;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.report.vo.InspectionReport;
import com.easycerti.eframe.psm.report.vo.Report;
import com.easycerti.eframe.psm.report.vo.ReportList;
import com.easycerti.eframe.psm.report.vo.UploadReport;
import com.easycerti.eframe.psm.search.vo.AllLogInq;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;
import com.easycerti.eframe.psm.statistics.vo.Statistics;
import com.easycerti.eframe.psm.system_management.vo.AccessAuth;
import com.easycerti.eframe.psm.system_management.vo.AccessAuthSearch;
import com.easycerti.eframe.psm.system_management.vo.PrivacyReport;

public interface ReportDao {
	
	public List<Report> findIpRiskrateAnalsList(Report report);
	public List<Report> findOvtimeRiskrateAnalsList(Report report);
	public List<Report> findRiskidxAnalsListByRule(Report report);
	public List<Report> findRiskidxAnalsListByDt(Report report);
	public List<Report> findRuleTable(SetupSearch ss);
	public List<Map<String, Object>> findReportDetail2_chart3(Map map);
	public int findEmpDetailCount(PrivacyReport pr);
	public List<Map> findReportDetail2_chart3Detail(Map map);
	
	public List<PrivacyReport> findReportHalf_chart2(PrivacyReport pr);
	public List<PrivacyReport> findReportHalf_chart3(PrivacyReport pr);
	public List<PrivacyReport> findReportHalf_chart4(PrivacyReport pr);
	public List<PrivacyReport> findReportHalf_chart5(PrivacyReport pr);
	public List<PrivacyReport> findReportHalf_table5(PrivacyReport pr);
	public PrivacyReport findReportHalf_chart6(PrivacyReport pr);
	
	public List<Code> findResultTypeList();
	
	public List<PrivacyReport> getLogTop10ByReqType(PrivacyReport pr);
	
	public List<PrivacyReport> findReport_inspection(PrivacyReport pr);
	public List<AllLogInq> findAllLogInqList(PrivacyReport pr);
	public List<AccessAuth> findAccessAuthList(AccessAuthSearch search);
	
	//개인정보 다운로드
	public String findDownBySysCountTot(PrivacyReport pr);
	public int findDownCountTot(PrivacyReport pr);
	public List<PrivacyReport> findDownloadCntBySysTop5(PrivacyReport pr);			//시스템별 다운로드 현황 TOP5
	public List<PrivacyReport> findDownloadCntByDeptTop5(PrivacyReport pr);			//시스템별 다운로드 현황 TOP5
	public List<PrivacyReport> findDownloadCntByResultTypeTop5(PrivacyReport pr); 	//유형별 다운로드 현황 TOP5
	
	public List<PrivacyReport> findDownloadCntBySys(PrivacyReport pr);				//시스템별 개인정보 다운로드 현황
	public List<PrivacyReport> findResultCntBySys(PrivacyReport pr);				//시스템별 개인정보 건수
	public List<PrivacyReport> findDownloadCntByDeptTop30(PrivacyReport pr);		//주요 부서별 개인정보 다운로드 현황 TOP30
	public List<PrivacyReport> findResultCntByDeptTop30(PrivacyReport pr);			//주요 부서별 개인정보 건수 TOP30
	public List<PrivacyReport> findDownloadCntByUserTop10(PrivacyReport pr);		//주요 사용자별 개인정보 다운로드 현황 TOP10
	public List<PrivacyReport> findResultCntByUserTop10(PrivacyReport pr);			//주요 사용자별 개인정보 건수 TOP10
	public List<PrivacyReport> findSystemNameByUserTop10(PrivacyReport pr);			//주요 사용자별 접근 시스템
	public List<PrivacyReport> findDownloadCntByUrlTop5(PrivacyReport pr);			//주요 URL별 다운로드 현황 TOP5
	public List<PrivacyReport> findResultCntByUrlTop5(PrivacyReport pr);			//주요 URL별 개인정보 건수 TOP5
	public List<PrivacyReport> findDownloadCntByResultTop10(PrivacyReport pr);		//주요 개인정보 유형별 다운로드한 현황TOP10 
	public List<PrivacyReport> findResultCntByResultTop10(PrivacyReport pr);		//주요 개인정보 유형별 개인정보 건수 TOP10 	
	public List<PrivacyReport> findDownloacCntByReasonTop10(PrivacyReport pr);		//주요 사유별 개인정보 다운로드 현황 TOP10
	public List<PrivacyReport> findResultCntByReasonTop10(PrivacyReport pr);		//주요 사유별 개인정보 건수 TOP10
	
	// sysong report_download
	public void addReport(Report report);
	public Map<String, Object> getHtmlEncode(String report_seq);
	public void updateReportMangementInfo(String report_seq);
	/**
	 * 보고서를 보고서 타입, 보고날짜, 기간타입으로 검색하여 있는지 확인
	 */
	public int getReportTitleFlag(Report report);
	public void updateReport(Report report);

	// 점검 보고서 관리 페이지
	public List<ReportList> findReportList(SearchBase search);
	public long removeReportDelete(long report_num);
	public int saveReportDownloadTime(long report_seq);
	public String findReportOne(long report_num);
	public int findReportList_count(SearchBase search);
	public Report findReportPdfPath(String report_seq);
	
	//소명_보고서
	public int findSystemCnt();
	public int findSummaryLogCnt(Report report);
	public int findSummonReqCnt(Report report);
	public int findSummonUserCnt(Report report);
	public PrivacyReport findSummonSystem(Report report);
	public PrivacyReport finddeptTop(Report report);
	public List<PrivacyReport> findSummonList(Report report);
	public List<PrivacyReport> findSummonStatusList(Report report);
	public List<PrivacyReport> findSummonResultList(Report report);
	public List<Map> findReportSummon_chart1(Map map);
	public List<Map> findReportSummon_chart2(Map map);
	public List<PrivacyReport> findPrivacytype(Report report);
	public List<PrivacyReport> findSummonSystemRepot(Report report);
	
	//권한관리_보고서
	public Integer findAuthInfoAllCount(PrivacyReport pr);					//접근권한신청현황 전체시스템 총 횟수		
	List<PrivacyReport> findAuthInfoBySysCount(PrivacyReport pr);			//시스템별 접근권한신청현황  총 횟수
	List<PrivacyReport> findAuthInfoAllCountByDept(PrivacyReport pr);		//부서별 접근권한신청현황  총 횟수
	List<PrivacyReport> findAuthInfoAllCountByAuth(PrivacyReport pr);		//권한별 접근권한신청현황  총 횟수
	List<PrivacyReport> findAuthInfoAllCountByReqType(PrivacyReport pr);	//시스템별 접근권한신청현황 행위 횟수
	List<PrivacyReport> findAuthInfoByDeptCountByReqType(PrivacyReport pr);	//부서별 접근권한신청현황 행위 횟수
	List<PrivacyReport> findAuthInfoByAuthCountByReqType(PrivacyReport pr);	//권한별 접근권한신청현황 행위 횟수
	List<PrivacyReport> findAuthInfoList2(PrivacyReport pr);				//접근권한 상세 이력 현황

	public List<ReportList> findReportAllCheck(ReportList data);

	public List<Code> findReportCodeList();

	public int updateUserfilePath(Report report);
	
	public List<PrivacyReport> getMajorResultType(PrivacyReport pr);
	public List<PrivacyReport> getMajorDownResultType(PrivacyReport pr);
	
	//노홍현 2020.03.17
	//수준진단보고서 (접속기록로그) START
	public int findPrivacyTotalLogCnt(PrivacyReport pr);							//접속기록 총 처리량(로그건수)
	
	public List<PrivacyReport> privacyProcessCntBySystem(PrivacyReport pr);			//1.시스템별 개인정보 접속기록 처리현황
	public List<PrivacyReport> privacyUseCntBySystem(PrivacyReport pr);				//2.시스템별 개인정보 접속기록 이용현황
	public List<PrivacyReport> comparePrevCntBySystem(PrivacyReport pr);			//3.시스템별 개인정보 접속기록 현황(비교)
	public List<PrivacyReport> privacyProcessCntByDept(PrivacyReport pr);			//4.소속별 개인정보 접속기록 처리현황
	public List<PrivacyReport> privacyUseCntByDept(PrivacyReport pr);				//5.소속별 개인정보 접속기록 이용현황
	public List<PrivacyReport> comparePrevCntByDept(PrivacyReport pr);				//6.소속별 개인정보 접속기록 현황(비교)
	
	public PrivacyReport findPrivacylogByResultType(PrivacyReport pr);				//7.개인정보 유형별 접속기록 현황 (당월,누적)(년간)
	public PrivacyReport privacyUseCntByResultType(PrivacyReport pr);				//7.개인정보 유형별 접속기록 이용현황
	public PrivacyReport comparePrevCntByResultType(PrivacyReport pr);				//7.개인정보 유형별 접속기록 이용현황(비교)
	
	
	public List<PrivacyReport> privacyProcessCntByReqType(PrivacyReport pr);		//9.수행업무별 접속기록 처리현황
	public List<PrivacyReport> comparePrevCntByReqType(PrivacyReport pr);			//10.수행업무별 접속기록 처리현황(비교)
	
	
	//report_chart
	public List<PrivacyReport> chart_privacyUseCntBySystem(PrivacyReport pr);		//1.시스템별 개인정보 접속기록 이용 현황(차트)
	public List<PrivacyReport> chart_privacyUseCntByDept(PrivacyReport pr);			//2.소속별 개인정보 접속기록 이용 현황(차트)
	public PrivacyReport chart_privacyUseCntByResultType(PrivacyReport pr);			//3.개인정보유형별 개인정보 접속기록 이용 현황(차트)
	
	
	public List<PrivacyReport> sumLogCntByDeptYear(PrivacyReport pr);				//4.소속별 개인정보 접속기록 현황(분기,누적)(년간)
	public List<PrivacyReport> findLogCntByDept(PrivacyReport pr);					//5.소속별 개인정보 접속기록 현황(비교)(년간)
	
	
	public List<PrivacyReport> findPrivacylogCntBySystem(PrivacyReport pr);			//시스템별  접속기록 총 처리량(로그건수)
	
	
	
	public PrivacyReport findPrivacylogCntBySystemCompare(PrivacyReport report);	//시스템별  접속기록 총 처리량 비교(로그건수) 전월,전년비교
	public Integer findPrivacyTotalCnt(PrivacyReport pr);							//접속기록  총 이용량(개인정보유형처리(이용)건수)
	public List<PrivacyReport> findPrivacyCntBySystem(PrivacyReport pr);			//시스템별  접속기록 총 이용량(개인정보유형처리(이용)건수)
	public PrivacyReport findPrivacyCntBySystemCompare(PrivacyReport report);		//시스템별  접속기록 총 이용량 비교(개인정보유형처리(이용)건수) 전월,전년비교
	
	public List<PrivacyReport> findPrivacylogCntByDept(PrivacyReport pr);			//부서별  접속기록 총 처리량(로그건수)
	public PrivacyReport findPrivacylogCntByDeptCompare(PrivacyReport report);		//부서별  접속기록 총 처리량 비교(로그건수) 전월,전년비교
	//public List<PrivacyReport> findPrivacyCntByDept(PrivacyReport pr);				//부서별  접속기록 총 이용량(개인정보유형처리(이용)건수)
	public PrivacyReport findPrivacyCntByDeptCompare(PrivacyReport report);			//부서별  접속기록 총 이용량 비교(개인정보유형처리(이용)건수) 전월,전년비교
	
	
	
	//수준진단보고서 END
	
	//업로드 보고서 이력관리
	public List<UploadReport> findUploadReportHist(UploadReport uploadReport);
	public UploadReport findUploadReportHistOne(UploadReport uploadReport);
	public void removeUploadReport(Integer report_id);
	public void addUploadReportHist(UploadReport uploadReport);
	public void saveUploadReportHist(UploadReport uploadReport);
	
	//다운로드 보고서
	public int downloadTotalLogCnt(PrivacyReport pr);
	public int downloadTotalCnt(PrivacyReport pr);
	public List<PrivacyReport> downloadProcessCntBySystem(PrivacyReport pr1);
	public List<PrivacyReport> downloadUseCntBySystem(PrivacyReport pr1);
	public List<PrivacyReport> downloadcomparePrevCntBySystem(PrivacyReport pr);
	public List<PrivacyReport> downloadProcessCntByDept(PrivacyReport pr1);
	public List<PrivacyReport> downloadUseCntByDept(PrivacyReport pr1);
	public List<PrivacyReport> downloadComparePrevCntByDept(PrivacyReport pr);
	public PrivacyReport downloadlogByResultType(PrivacyReport pr);
	public List<PrivacyReport> chart_privacyUseCntBySystem_download(PrivacyReport pr);
	public List<PrivacyReport> chart_privacyUseCntByDept_download(PrivacyReport pr);
	public PrivacyReport chart_privacyUseCntByResultType_download(PrivacyReport pr);
	//public List<PrivacyReport> findPrivacyReportByReqtype_download(PrivacyReport pr);
	public List<PrivacyReport> getFileExtension_chart(PrivacyReport pr);
	public String findUploadReportOne(Map<String, String> parameters);
	public PrivacyReport downloadComparePrevCntBySystemSum(PrivacyReport pr);
	public List<PrivacyReport> downloadcomparePrevCntBySystemTop3(PrivacyReport pr);
	public List<PrivacyReport> downloadComparePrevCntByDeptTop5(PrivacyReport pr);
	public PrivacyReport comparePrevCntByResultType_download(PrivacyReport pr);
	public List<PrivacyReport> getFileExtensionCompare_chart(PrivacyReport pr);
	public List<PrivacyReport> getFileExtensionCompare_chart_top5(PrivacyReport pr);
	public PrivacyReport getFileExtensionCompare_after(PrivacyReport pr);
	
	public void addReportOption(Map<String, String> parameters);
	public void saveReportOption(Map<String, String> parameters);
	public List<Map<String, String>> findReportOption(Map<String, String> parameters);
	public void updateWord(Report report);
	public String findReportOne_Seq(Report report);
	public List<PrivacyReport> getWordReportData1(PrivacyReport pr1);
	public List<PrivacyReport> getWordReportData2(PrivacyReport pr1);

	public List<PrivacyReport> getEmpWordGrid01(PrivacyReport pr1);
	public List<PrivacyReport> getEmpWordGrid02(PrivacyReport pr1);
	public Integer findBeforeReport(Map<String, String> parameters);
	
	public String findPrivacyCtInfo(PrivacyReport pr);
	public String findPrivacyDesc(String privacy_type);
	public Map<String, String> getAuthorizeLine();
	public void addAuthorizeLine(Map<String, String> parameters);
	public void addDefaultDesc(Map<String, String> parameters);
	public String findDefaultDesc(Map<String, String> parameters);
	
	public String findReportAuthorizeInfo();
	public List<PrivacyReport> chart_findPrivacyReportByReqtype(PrivacyReport pr);
	public void addInspectionReport(InspectionReport report);
	public InspectionReport findInspectionReportOne(InspectionReport report);
	public String getReportDescStr(Map<String, String> optionMap);
	
	public List<InspectionReport> findReportInspectionList(Statistics paramBean);
	public int findReportInspectionList_count(Statistics paramBean);
	public void deleteInspectionReportOne(InspectionReport report);
	//public List<PrivacyReport> comparePrevCntByDept_new(PrivacyReport pr);
	//public List<PrivacyReport> comparePrevCntByDept_new2(PrivacyReport pr);
	
	// 오남용보고서
	public List<PrivacyReport> abnormalTotalListBySystem(PrivacyReport pr);
	
	public int abnormalTotalCnt(PrivacyReport pr);
	public List<PrivacyReport> abnormalSystemList(PrivacyReport pr);
	public List<PrivacyReport> abnormalDeptList(PrivacyReport pr);
	public List<PrivacyReport> abnormalUserList(PrivacyReport pr);
	
	public List<PrivacyReport> abnormalSystemTop5(PrivacyReport pr);
	public PrivacyReport abnormalSystemTop5Other(PrivacyReport pr);
	public List<PrivacyReport> abnormalDeptTop5(PrivacyReport pr);
	public PrivacyReport abnormalDeptTop5Other(PrivacyReport pr);
	public List<PrivacyReport> abnormalUserTop5(PrivacyReport pr);
	public PrivacyReport abnormalUserTop5Other(PrivacyReport pr);
	
	public List<PrivacyReport> abnormalSystemTop5Chart(PrivacyReport pr);
	public List<PrivacyReport> abnormalDeptTop5Chart(PrivacyReport pr);
	public List<PrivacyReport> abnormalUserTop5Chart(PrivacyReport pr);
	public List<PrivacyReport> comparePrevCntByUser(PrivacyReport pr);
	public List<PrivacyReport> chart_privacyUseCntByUser(PrivacyReport pr);

	public String getSystem_name(String system_seq); //system_name
	public String getReportSeq(Report report);
	public int systemReportDelete(Report parameters);
	public Report findSystemReportPath(Report report);
	public void updateSystemReportInfo(Report report);
	public Map<String, Object> getSystemHtmlEncode(Report report);
	public void updateSystemReportMangementInfo(String report_seq);
	public String getAuthSystemSeq(String auth_id);
	public String reportAuthorizeInfo_system(String system_seq);
	
	public List<PrivacyReport> findOrgRuleInfoIsks();
	public PrivacyReport findTotalSummonCntIsks(Report report);
	public PrivacyReport findTotalExtrtCntIsks(Report report);
	public List<PrivacyReport> findTotalRuleSystemCntIsks(Report report);

}
