package com.easycerti.eframe.psm.report.web;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.DateUtil;
import com.easycerti.eframe.common.util.ListComparator;
import com.easycerti.eframe.common.util.PdfUtil;
import com.easycerti.eframe.common.vo.SearchBase;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.core.util.SessionManager;
import com.easycerti.eframe.core.web.RequestWrapper;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.report.dao.ReportDao;
import com.easycerti.eframe.psm.report.service.ReportSvc;
import com.easycerti.eframe.psm.report.vo.Report;
import com.easycerti.eframe.psm.report.vo.ReportList;
import com.easycerti.eframe.psm.report.vo.UploadReport;
import com.easycerti.eframe.psm.search.dao.AllLogInqDao;
import com.easycerti.eframe.psm.search.vo.AllLogInq;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.system_management.dao.AccessAuthDao;
import com.easycerti.eframe.psm.system_management.dao.AdminUserMngtDao;
import com.easycerti.eframe.psm.system_management.vo.AccessAuth;
import com.easycerti.eframe.psm.system_management.vo.AccessAuthSearch;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;
import com.easycerti.eframe.psm.system_management.vo.PrivacyReport;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;
import com.mortennobel.imagescaling.AdvancedResizeOp;
import com.mortennobel.imagescaling.MultiStepRescaleOp;

@Controller
@RequestMapping("/report/*")
public class ReportCtrl {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private ReportSvc reportSvc;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private ReportDao reportDao;

	@Autowired
	private AccessAuthDao accessAuthDao;
	
	@Autowired
	private AllLogInqDao allLogInqDao;
	
	@Autowired
	private PdfUtil pdfUtil;
	
	@Autowired
	private AdminUserMngtDao adminUserMngtDao;
	
	@Value("#{configProperties.use_studentId}")
	private String use_studentId;
	
	@Value("#{configProperties.logo_report_url}")
	private String logo_report_url;
	
	@Value("#{configProperties.use_approval_line}")
	private String use_approval_line;
	
	@Value("#{configProperties.report_path}")
	private String report_path;
	
	
	@RequestMapping(value="list_riskidxAnals.html",method={RequestMethod.POST})
	public DataModelAndView findRiskidxAnalsList(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		parameters = CommonHelper.checkSearchDate(parameters);
				
		DataModelAndView modelAndView = new DataModelAndView();
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName("riskidxAnalsList");
		
		return modelAndView;
	}
	
//	@RequestMapping(value = "pdfriskidxAnals.html")
//	public ModelAndView samplesReportPdf(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
//		Report paramBean = CommonHelper.convertMapToBean(parameters, Report.class);
//		
//		List<Report> reportVal = reportDao.findIpRiskrateAnalsList(paramBean);
//		
//		//menuList.add(menu);
//		JRDataSource ds = new JRBeanCollectionDataSource(reportVal);
//		
//		Map<String, Object> parameterMap = new HashMap<String, Object>();
//		parameterMap.put("datasource", ds);
//
//		ModelAndView modelAndView = new ModelAndView("pdfipRiskrateAnalsReport", parameterMap);
//		
//		return modelAndView;
//	}
//
//	@RequestMapping(value = "htmlriskidxAnals.html")
//	public ModelAndView samplesReportHtml(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
//		Report paramBean = CommonHelper.convertMapToBean(parameters, Report.class);
//		
//		List<Report> reportVal = reportDao.findIpRiskrateAnalsList(paramBean);
//		
//		//menuList.add(menu);
//		JRDataSource ds = new JRBeanCollectionDataSource(reportVal);
//		
//		Map<String, Object> parameterMap = new HashMap<String, Object>();
//		parameterMap.put("datasource", ds);
//
//		ModelAndView modelAndView = new ModelAndView("htmlipRiskrateAnalsReport", parameterMap);
//		
//		return modelAndView;
//	}

	
	@RequestMapping(value="list_ipRiskrateAnals.html",method={RequestMethod.POST})
	public DataModelAndView findIpRiskrateAnalsList(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		parameters = CommonHelper.checkSearchDate(parameters);
				
		DataModelAndView modelAndView = new DataModelAndView();
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName("ipRiskrateAnalsList");
		
		return modelAndView;
	}
	
	@RequestMapping(value="list_ovtimeRiskrateAnals.html",method={RequestMethod.POST})
	public DataModelAndView findOvtimeRiskrateAnalsList(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		parameters = CommonHelper.checkSearchDate(parameters);
				
		DataModelAndView modelAndView = new DataModelAndView();
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName("ovtimeRiskrateAnalsList");
		
		return modelAndView;
	}

	@ModelAttribute("systemList")
	public List<SystemMaster> findSystemList() {
		return reportSvc.findSystemMasterList();
	}
	
	@RequestMapping(value="reportList.html",method={RequestMethod.POST})
	public DataModelAndView findReportList(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		parameters = CommonHelper.checkSearchDateByWeek(parameters);
		
		SimpleCode simpleCode = new SimpleCode();
		String index = "/report/reportList.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
				
		DataModelAndView modelAndView = new DataModelAndView();
		
		//2016.12.27 by hjpark
	    List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    
	    SearchSearch search = new SearchSearch();
	    search.setAuth_idsList(list);
	    
	    HttpSession session = request.getSession();
	    String master = (String)session.getAttribute("master");
		modelAndView.addObject("masterflag", master);
		
		String ui_type = (String)session.getAttribute("ui_type");
		if (ui_type != null & ui_type.length() > 0) {
			modelAndView.addObject("ui_type", ui_type);
		}
		
		String mode_access_auth = (String)session.getAttribute("mode_access_auth");
		if (mode_access_auth != null & mode_access_auth.length() > 0) {
			modelAndView.addObject("mode_access_auth", mode_access_auth);
		}
		
		List<SystemMaster> systemList = reportSvc.findSystemMasterList(search);
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("systemList", systemList);
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName("reportList");
		
		return modelAndView;
	}
	
	
	
	//인천공항공사
	//	- 점검보고서 페이지
	@RequestMapping(value="reportList_airport.html",method={RequestMethod.POST})
	public DataModelAndView reportList_airport(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		parameters = CommonHelper.checkSearchDateByWeek(parameters);
		
		SimpleCode simpleCode = new SimpleCode();
		String index = "/report/reportList_airport.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
				
		DataModelAndView modelAndView = new DataModelAndView();
		
		//2016.12.27 by hjpark
	    List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    
	    SearchSearch search = new SearchSearch();
	    search.setAuth_idsList(list);
	    
	    HttpSession session = request.getSession();
	    String master = (String)session.getAttribute("master");
		modelAndView.addObject("masterflag", master);
		
		String ui_type = (String)session.getAttribute("ui_type");
		if (ui_type != null & ui_type.length() > 0) {
			modelAndView.addObject("ui_type", ui_type);
		}
		
		String mode_access_auth = (String)session.getAttribute("mode_access_auth");
		if (mode_access_auth != null & mode_access_auth.length() > 0) {
			modelAndView.addObject("mode_access_auth", mode_access_auth);
		}
		
		List<SystemMaster> systemList = reportSvc.findSystemMasterList(search);
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("systemList", systemList);
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName("reportList_airport");
		
		return modelAndView;
	}
	
	@RequestMapping(value = "reportDetail.html")
	public ModelAndView findReportDetail(@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date",required=false) String start_date,
			@RequestParam(value = "end_date",required=false) String end_date) {
		
		ModelAndView modelAndView = new ModelAndView();
		
		String tmpList[] = system_seq.split(",");
		
		List<String> syslist = new ArrayList<String>();
		
		for ( int i=0; i< tmpList.length; ++i ) {
			if ( !tmpList[i].equals("0")) {
				syslist.add(tmpList[i]);
			}
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setSysList(syslist);
		
		//List<SystemMaster> systemList = reportSvc.findSystemMasterDetailList(syslist);
		List<SystemMaster> systemList = reportSvc.findSystemMasterDetailList(pr);
		long time = System.currentTimeMillis(); 
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
		String date = dayTime.format(new Date(time));
		
		
		for ( int i=0; i<systemList.size(); ++i ) {
			SystemMaster smbean = systemList.get(i);
			
			PrivacyReport tmpBean = new PrivacyReport();
			tmpBean.setStart_date(start_date.replaceAll("-", ""));
			tmpBean.setEnd_date(end_date.replaceAll("-", ""));
			tmpBean.setSystem_seq(smbean.getSystem_seq());
			
			List<PrivacyReport> privCntList = reportSvc.findPrivacyReportCntList(tmpBean);
			
			int nTotCount=0;
			
			for ( int j=0; j<privCntList.size(); ++j ) {
				PrivacyReport bean = privCntList.get(j);
				if ( bean.getResult_type().equals("1")) {
					smbean.setResultType1(Integer.parseInt(bean.getCnt()));
					nTotCount=nTotCount+Integer.parseInt(bean.getCnt());
				} else if ( bean.getResult_type().equals("2")) {
					smbean.setResultType2(Integer.parseInt(bean.getCnt()));
					nTotCount=nTotCount+Integer.parseInt(bean.getCnt());
				} else if ( bean.getResult_type().equals("3")) {
					smbean.setResultType3(Integer.parseInt(bean.getCnt()));
					nTotCount=nTotCount+Integer.parseInt(bean.getCnt());
				} else if ( bean.getResult_type().equals("4")) {
					smbean.setResultType4(Integer.parseInt(bean.getCnt()));
					nTotCount=nTotCount+Integer.parseInt(bean.getCnt());
				} else if ( bean.getResult_type().equals("5")) {
					smbean.setResultType5(Integer.parseInt(bean.getCnt()));
					nTotCount=nTotCount+Integer.parseInt(bean.getCnt());
				} else if ( bean.getResult_type().equals("6")) {
					smbean.setResultType6(Integer.parseInt(bean.getCnt()));
					nTotCount=nTotCount+Integer.parseInt(bean.getCnt());
				} else if ( bean.getResult_type().equals("7")) {
					smbean.setResultType7(Integer.parseInt(bean.getCnt()));
					nTotCount=nTotCount+Integer.parseInt(bean.getCnt());
				} else if ( bean.getResult_type().equals("8")) {
					smbean.setResultType8(Integer.parseInt(bean.getCnt()));
					nTotCount=nTotCount+Integer.parseInt(bean.getCnt());
				} else if ( bean.getResult_type().equals("9")) {
					smbean.setResultType9(Integer.parseInt(bean.getCnt()));
					nTotCount=nTotCount+Integer.parseInt(bean.getCnt());
				} else if ( bean.getResult_type().equals("10")) {
					smbean.setResultType10(Integer.parseInt(bean.getCnt()));
					nTotCount=nTotCount+Integer.parseInt(bean.getCnt());
				} else if ( bean.getResult_type().equals("99")) {
					smbean.setResultType99(Integer.parseInt(bean.getCnt()));
					nTotCount=nTotCount+Integer.parseInt(bean.getCnt());
				} 
				smbean.setTot_count(nTotCount);
			}
			systemList.set(i, smbean);
		}
		
		//전체 개인정보 Top10
		
		for ( int i=0; i<systemList.size(); ++i ) {
			SystemMaster smbean = systemList.get(i);
			
			PrivacyReport tmpBean = new PrivacyReport();
			tmpBean.setStart_date(start_date.replaceAll("-", ""));
			tmpBean.setEnd_date(end_date.replaceAll("-", ""));
			tmpBean.setSystem_seq(smbean.getSystem_seq());
			
			List<PrivacyReport> privCntListEmp = reportSvc.findPrivacyReportCntbyEmp(tmpBean);
			
			for ( int j=0; j<privCntListEmp.size(); ++j ) {
				PrivacyReport tmp = privCntListEmp.get(j);
				
				String strContent = "";
				String emp_user_id = tmp.getEmp_user_id();
				/*String strDeptName= reportSvc.findDeptNameByEmpUserId(emp_user_id); 	
				String strName = reportSvc.findNameByEmpUserId(emp_user_id);*/
				
				String strDeptName= tmp.getDept_name();
				String strName = tmp.getEmp_user_name();
				
				strContent = strDeptName + "," +emp_user_id+","+strName+","+ tmp.getCnt();
				
				if ( j == 0 ) smbean.setTop1(strContent);
				else if ( j == 1 ) smbean.setTop2(strContent);
				else if ( j == 2 ) smbean.setTop3(strContent);
				else if ( j == 3 ) smbean.setTop4(strContent);
				else if ( j == 4 ) smbean.setTop5(strContent);
				else if ( j == 5 ) smbean.setTop6(strContent);
				else if ( j == 6 ) smbean.setTop7(strContent);
				else if ( j == 7 ) smbean.setTop8(strContent);
				else if ( j == 8 ) smbean.setTop9(strContent);
				else if ( j == 9 ) smbean.setTop10(strContent);
			}
			
			systemList.set(i, smbean);
		}
		
		PrivacyReport tmpBean = new PrivacyReport();
		tmpBean.setStart_date(start_date.replaceAll("-", ""));
		tmpBean.setEnd_date(end_date.replaceAll("-", ""));
		tmpBean.setList(syslist);
		
		List<PrivacyReport> systemListTop10 = reportSvc.findSystemMasterTop10(tmpBean);
		
		SystemMaster systemCnt = new SystemMaster();
		
		for ( int j=0; j<systemListTop10.size(); ++j ) {
			PrivacyReport tmp = systemListTop10.get(j);
			
			String strContent = "";
			String emp_user_id = tmp.getEmp_user_id();
			String strDeptName= tmp.getDept_name();
			String strName = tmp.getEmp_user_name();
			
			strContent = strDeptName + "," +emp_user_id+","+strName+","+ tmp.getCnt();
			
			if ( j == 0 ) systemCnt.setTop1(strContent);
			else if ( j == 1 ) systemCnt.setTop2(strContent);
			else if ( j == 2 ) systemCnt.setTop3(strContent);
			else if ( j == 3 ) systemCnt.setTop4(strContent);
			else if ( j == 4 ) systemCnt.setTop5(strContent);
			else if ( j == 5 ) systemCnt.setTop6(strContent);
			else if ( j == 6 ) systemCnt.setTop7(strContent);
			else if ( j == 7 ) systemCnt.setTop8(strContent);
			else if ( j == 8 ) systemCnt.setTop9(strContent);
			else if ( j == 9 ) systemCnt.setTop10(strContent);
		}
		
		
		modelAndView.addObject("systemCnt", systemCnt);
		modelAndView.addObject("start_date", start_date);
		modelAndView.addObject("end_date", end_date);
		modelAndView.addObject("date", date);
		modelAndView.addObject("systemList", systemList);
		modelAndView.setViewName("report");
		
		return modelAndView;
	}
	
	@RequestMapping(value = "reportDetail_new.html")
	public ModelAndView findReportDetail_new(HttpServletRequest request,
			@RequestParam(value = "type", required = false) int type,
			@RequestParam(value = "period_type", required = false) int period_type,
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date,
			@RequestParam(value = "menu_id", required = false) String menu_id,
			@RequestParam(value = "download_type", required = false) String download_type) {

		ModelAndView modelAndView = new ModelAndView();
		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
	    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
		String date = dayTime.format(new Date(time));
		String ui_type = commonDao.getUiType();
		modelAndView.addObject("ui_type", ui_type);
		
		// 마지막날짜가 현재보다 클 경우 어제날짜까지로 지정해준다.
		/*
		try {
			Date tmpDate = dayTime.parse(end_date);
			int compare = tmpDate.compareTo(new Date());
			if( compare >= 0) {
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DAY_OF_MONTH, -1);
				end_date = dayTime.format(cal.getTime());
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		*/
		String compareDate = "";
		
		PrivacyReport pr = new PrivacyReport();			//선택 기간
		PrivacyReport prCompare = new PrivacyReport();	//비교 기간(동년전월)
		PrivacyReport prCompare2 = new PrivacyReport();	//비교 기간(전년동월)
		
		
		HttpSession session = request.getSession();
	    String master = (String)session.getAttribute("master");
	    
	    String use_reportLogo = (String)session.getAttribute("use_reportLogo");
	    modelAndView.addObject("use_reportLogo", use_reportLogo);
	    if (use_reportLogo != null && use_reportLogo.equals("Y")) {
		    String rootPath = request.getSession().getServletContext().getRealPath("/");
			String attach_path = "resources/upload/";
			
			String filename = commonDao.selectCurrentImage_logo2();
			String savePath = rootPath + attach_path + filename;
			
			File file = new File(savePath);
			
			if(file.exists()) {
				modelAndView.addObject("filename", filename);
			}
	    }
	    
	    String use_reportLine = (String)session.getAttribute("use_reportLine");
	    modelAndView.addObject("use_reportLine", use_reportLine);
	    
		modelAndView.addObject("masterflag", master);
		modelAndView.addObject("report_type", type);
		modelAndView.addObject("period_type", period_type);
		modelAndView.addObject("start_date", start_date);
		modelAndView.addObject("end_date", end_date);
		modelAndView.addObject("date", date);
		modelAndView.addObject("system_seq", system_seq);
		modelAndView.addObject("use_studentId", use_studentId);
		modelAndView.addObject("logo_report_url", logo_report_url);
		//modelAndView.addObject("use_approval_line", use_approval_line);
		modelAndView.addObject("menu_id", menu_id);

		List<String> syslist = new ArrayList<String>();
		

		String tmpList[] = system_seq.split(",");
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}
		syslist.add("");
		
		String sDate[] = start_date.split("-");
		String eDate[] = end_date.split("-");

		int strYear = Integer.parseInt(sDate[0]);
		int strMonth = Integer.parseInt(sDate[1]);
		int endYear = Integer.parseInt(eDate[0]);
		int endMonth = Integer.parseInt(eDate[1]);

		int diffMon = (endYear - strYear)* 12 + (endMonth - strMonth);

		String stDate = start_date.replaceAll("-", "").substring(0,6);
		String edDate = end_date.replaceAll("-", "").substring(0,6);
		
		pr.setStart_date(stDate);
		pr.setEnd_date(edDate);
		pr.setSysList(syslist);
		
		//pr.setMonth(sDate[0] + sDate[1]);
		
		if(!pr.getSysList().isEmpty()) {
			List<SystemMaster> systems = reportSvc.findSystemMasterDetailList(pr);
			for (SystemMaster sm : systems) {
				PrivacyReport prt = new PrivacyReport();
				prt.setSystem_seq(sm.getSystem_seq());
				prt.setStart_date(stDate);
				prt.setEnd_date(edDate);
				List<PrivacyReport> res = reportDao.getMajorResultType(prt);
				String result_type = "";
				if (res.size() > 0) {
					for (PrivacyReport data : res) {
						String text = data.getPrivacy_desc()  + "(" + data.getCnt() + ") ";
						result_type += text;
					}
				} else {
					result_type = "데이터없음";
				}
				sm.setResult_type(result_type);
			}
			modelAndView.addObject("systems", systems);
		}
	    
	    Calendar cal = Calendar.getInstance();
	    cal.set(strYear, strMonth-1, 1);		//선택기간 시작월 1일
	    
	    cal.add(Calendar.MONTH, -1);    		// 비교 기간 끝(선택기간 한달전)
	    compareDate = dateFormatter.format(cal.getTime());
//		edDate = compareDate.substring(0,6)+"31";
	    edDate = compareDate.substring(0,6);

		cal.add(Calendar.MONTH, -diffMon);    	// 비교 기간 시작
		compareDate = dateFormatter.format(cal.getTime());
//		stDate = compareDate.substring(0,6)+"01";
		stDate = compareDate.substring(0,6);

		prCompare.setStart_date(stDate);
		prCompare.setEnd_date(edDate);
		prCompare.setSysList(syslist);
		
		cal.set(strYear, strMonth-1, 1);
		cal.add(Calendar.YEAR, -1);    		// 비교 기간 끝(선택기간 한달전)
	    compareDate = dateFormatter.format(cal.getTime());
		edDate = compareDate.substring(0,6)+"31";

		cal.add(Calendar.YEAR, -diffMon);    	// 비교 기간 시작
		compareDate = dateFormatter.format(cal.getTime());
		stDate = compareDate.substring(0,6)+"01";
		
		prCompare2.setStart_date(stDate);
		prCompare2.setEnd_date(edDate);
		prCompare2.setSysList(syslist);
		
		if(type == 4){
			int totCnt = 0;
			
			List<PrivacyReport> typeCnt = reportSvc.findTypeReportCount(pr);
			if(typeCnt.size()>0){
				modelAndView.addObject("topType", typeCnt.get(0).getPrivacy_desc());
				for(int i=0; i<typeCnt.size(); i++){
					totCnt = totCnt + Integer.parseInt(typeCnt.get(i).getCnt());
				}
				modelAndView.addObject("totCnt", totCnt);
				modelAndView.addObject("typeCnt", typeCnt);
			}
			
			
			List<PrivacyReport> systemCnt = reportSvc.findSystemReportCount(pr);
			if(systemCnt.size()>0) {
				modelAndView.addObject("topSystem", systemCnt.get(0).getSystem_name());
				modelAndView.addObject("systemCnt", systemCnt);
			}
			
			List<PrivacyReport> deptCnt = reportSvc.findDeptReportCount(pr);
			if(deptCnt.size()>0) {
				modelAndView.addObject("topDept", deptCnt.get(0).getDept_name());
				modelAndView.addObject("deptCnt", deptCnt);
			}

			List<PrivacyReport> indvCnt = reportSvc.findIndvReportCount(pr);
			if(indvCnt.size()>0) {
				modelAndView.addObject("topIndv", indvCnt.get(0).getEmp_user_name());
				modelAndView.addObject("indvCnt", indvCnt);
			}

			if(ui_type.equals("G")) {
				modelAndView.setViewName("report_new4_gj");
			} else {
				modelAndView.setViewName("report_new4");
			}
		}else{
			
			
/*	
			SetupSearch ss = new SetupSearch();
			ss.setUse_yn_search("Y");
			ss.setIs_realtime_extract_search("Y");
*/
			if(type == 1 || type == 3){

				/*int listEmpDetailCount1 = 0;
				int listEmpDetailCount2 = 0;
				int listEmpDetailCount3 = 0;
				int listEmpDetailCount4 = 0;
				int listEmpDetailCount5 = 0;*/
				
				//List<PrivacyReport> listEmpDetailCount = reportSvc.findEmpDetailListCount(pr);
				
				/*for ( int i=0; i<listEmpDetailCount.size(); ++i ) {
					PrivacyReport tmp = listEmpDetailCount.get(i);
					
					if (tmp.getRule_cd().substring(0, 2).equals("11")) {
						listEmpDetailCount1=Integer.parseInt(tmp.getCnt());
					}else if (tmp.getRule_cd().substring(0, 2).equals("12")) {
						listEmpDetailCount2=Integer.parseInt(tmp.getCnt());
					}else if (tmp.getRule_cd().substring(0, 2).equals("40")) {
						listEmpDetailCount3=Integer.parseInt(tmp.getCnt());
					}else if (tmp.getRule_cd().substring(0, 2).equals("30")) {
						listEmpDetailCount4=Integer.parseInt(tmp.getCnt());
					}else {
						listEmpDetailCount5=Integer.parseInt(tmp.getCnt());
					}
				}*/

				/*modelAndView.addObject("listEmpDetailCount1", listEmpDetailCount1);
				modelAndView.addObject("listEmpDetailCount2", listEm pDetailCount2);
				modelAndView.addObject("listEmpDetailCount3", listEmpDetailCount3);
				modelAndView.addObject("listEmpDetailCount4", listEmpDetailCount4);
				modelAndView.addObject("listEmpDetailCount5", listEmpDetailCount5);*/
				
				// 20180115 비정상행위 제거
				//modelAndView.addObject("empList", listEmpDetailCount);
				
				PrivacyReport findPrivacyReportBySystemTop1 = reportSvc.findPrivacyReportBySystemTop1(pr);
				if(findPrivacyReportBySystemTop1 != null)
					modelAndView.addObject("reportBySys", findPrivacyReportBySystemTop1.getSystem_name());
				
				PrivacyReport findPrivacyReportByDeptTop1 = reportSvc.findPrivacyReportByDeptTop1(pr);
				if(findPrivacyReportByDeptTop1 != null)
					modelAndView.addObject("reportByDept", findPrivacyReportByDeptTop1.getDept_name());
				
//				List<PrivacyReport> listByReqtypeCnt = reportSvc.findPrivacyReportByReqtypeCnt(pr);
//				modelAndView.addObject("listByReqtypeCnt", listByReqtypeCnt);
//		
//				if (!listByReqtypeCnt.isEmpty())
//					modelAndView.addObject("topReportByReqtype", listByReqtypeCnt.get(0).getReq_type());
		
				int diffMonth = getDiffMonth(pr);
				modelAndView.addObject("diffMonth", diffMonth);

				/*List list2 = getReportDetail2_chart2(pr);
				modelAndView.addObject("emp_detailList2", list2); 
				
				List list41 = findReportDetailbyIndividual(pr);
				modelAndView.addObject("emp_detailList4", list41);*/
				
				int logCnt = reportSvc.findPrivacyTotalLogCnt(pr);
				modelAndView.addObject("logCnt", logCnt);
				
				int privCnt = reportSvc.findPrivacyTotalCnt(pr);
				modelAndView.addObject("privCnt", privCnt);
		
				/*int empDetailCnt = reportSvc.findEmpDetailCount(pr);
				modelAndView.addObject("empDetailCnt", empDetailCnt);*/

				List<PrivacyReport> privTypeCount;
				int privTypeCountAvg = 0;

				if(period_type == 1){
					privTypeCount = reportSvc.findPrivTypeCountWeek(pr);
				
					String proc_date="";
					//Date dateT = new Date();
					int nConut = 0;
					int nPrivCount = 0;
					String tmpS="";
					
					for (int i = 0; i < privTypeCount.size(); ++i) {
						PrivacyReport tmp = privTypeCount.get(i);
						
						proc_date = strYear+"년 "+strMonth+"월 "+ tmp.getWeek() +"주차";
						
						tmp.setProc_date(proc_date);
		
						if (i == 0) {
							tmp.setTot_cnt("-");
						} else {
							nConut = Integer.parseInt(tmp.getCnt());
							nPrivCount = Integer.parseInt(privTypeCount.get(i - 1).getCnt());
							tmpS = Integer.toString(nConut - nPrivCount);
							if (tmpS.indexOf("-") >= 0)
								tmp.setbCheckPlus("0");
							else
								tmp.setbCheckPlus("1");
		
							tmpS = tmpS.replaceAll("-", "");
							tmp.setTot_cnt(tmpS);
						}
		
						privTypeCountAvg = privTypeCountAvg + Integer.parseInt(tmp.getCnt());
		
						privTypeCount.set(i, tmp);
					}
				}else{
					pr.setStart_date(pr.getStart_date().substring(0,6));
					pr.setEnd_date(pr.getEnd_date().substring(0,6));
					privTypeCount = reportSvc.findPrivacyReportDetailChart3_1(pr);
					
					String proc_date="";
					int nConut = 0;
					int nPrivCount = 0;
					String tmpS="";
					
					for (int i = 0; i < privTypeCount.size(); ++i) {
						PrivacyReport tmp = privTypeCount.get(i);
						
						proc_date = tmp.getMonth().substring(0,4)+"년 "+tmp.getMonth().substring(4,6)+"월";
						
						tmp.setProc_date(proc_date);
		
						if (i == 0) {
							tmp.setTot_cnt("-");
						} else {
							nConut = Integer.parseInt(tmp.getCnt());
							nPrivCount = Integer.parseInt(privTypeCount.get(i - 1).getCnt());
							tmpS = Integer.toString(nConut - nPrivCount);
							if (tmpS.indexOf("-") >= 0)
								tmp.setbCheckPlus("0");
							else
								tmp.setbCheckPlus("1");
		
							tmpS = tmpS.replaceAll("-", "");
							tmp.setTot_cnt(tmpS);
						}
		
						privTypeCountAvg = privTypeCountAvg + Integer.parseInt(tmp.getCnt());
		
						privTypeCount.set(i, tmp);
					}
				}
	
				// 개인정보 업무 행위별 결과(단위: 처리한 개인정보 건수)
				
				List<PrivacyReport> systemCurdCount = reportSvc.findSystemCurdCount(pr);
				modelAndView.addObject("systemCurdCount", systemCurdCount);
				/*List<PrivacyReport> systemList = reportSvc.findSystemMaster(pr);
	
				int systemCount1 = 0;
				int systemCount2 = 0;
				int systemCount3 = 0;
				int systemCount4 = 0;
	
				for (int i = 0; i < systemList.size(); ++i) {
					PrivacyReport tmp = systemList.get(i);
	
					tmp.setStart_date(start_date.replaceAll("-", ""));
					tmp.setEnd_date(end_date.replaceAll("-", ""));
	
					List<PrivacyReport> systemCurdCount = reportSvc.findSystemCurdCount(tmp);
	
					for (int j = 0; j < systemCurdCount.size(); ++j) {
	
						PrivacyReport tmpCount = systemCurdCount.get(j);
						if (tmpCount.getReq_type().equals("RD")) {
							tmp.setType1(Integer.parseInt(tmpCount.getCnt()));
							systemCount1 = systemCount1 + Integer.parseInt(tmpCount.getCnt());
						} else if (tmpCount.getReq_type().equals("CR")) {
							tmp.setType2(Integer.parseInt(tmpCount.getCnt()));
							systemCount2 = systemCount2 + Integer.parseInt(tmpCount.getCnt());
						} else if (tmpCount.getReq_type().equals("UD")) {
							tmp.setType3(Integer.parseInt(tmpCount.getCnt()));
							systemCount3 = systemCount3 + Integer.parseInt(tmpCount.getCnt());
						} else if (tmpCount.getReq_type().equals("DL")) {
							tmp.setType4(Integer.parseInt(tmpCount.getCnt()));
							systemCount4 = systemCount4 + Integer.parseInt(tmpCount.getCnt());
						}
					}
	
					systemList.set(i, tmp);
				}
	
				modelAndView.addObject("systemList", systemList);
	
				modelAndView.addObject("systemCount1", systemCount1);
				modelAndView.addObject("systemCount2", systemCount2);
				modelAndView.addObject("systemCount3", systemCount3);
				modelAndView.addObject("systemCount4", systemCount4);*/
				
				List<PrivacyReport> list4 = reportSvc.findPrivacylogCntBySystem(pr);
				List<PrivacyReport> list4_1 = reportSvc.findPrivacylogCntBySystem(pr);
				
				int nCount1, nCount2, nCount3;
				
				for(int i=0; i<list4.size(); i++) {
					
					PrivacyReport report = list4.get(i);

					report.setStart_date(pr.getStart_date());
					report.setEnd_date(pr.getEnd_date());
					//List<PrivacyReport> listTmp = reportSvc.findReportDetail_chart4_detail(report);\

					report.setStart_date(prCompare.getStart_date());
					report.setEnd_date(prCompare.getEnd_date());
					//List<PrivacyReport> listTmp2 = reportSvc.findReportDetail_chart4_detail(report);
					PrivacyReport prev_report = reportSvc.findPrivacylogCntBySystemCompare(report);
					
					nCount1=0;
					nCount2=0;
					nCount3=0;
					
					/*if(listTmp.size()>0){
						nCount1 = Integer.parseInt(listTmp.get(0).getCnt());
					}
					if(listTmp2.size()>0){
						nCount2 = Integer.parseInt(listTmp2.get(0).getCnt());
					}*/
					if(report != null)
						nCount1 = Integer.parseInt(report.getCnt());
					if(prev_report != null)
						nCount2 = Integer.parseInt(prev_report.getCnt());
/*
					for ( int j=0; j<listTmp.size(); ++j ) {
						
						if ( edDate.substring(0, 6).equals(listTmp.get(j).getProc_date()) ) nCount1 = Integer.parseInt(listTmp.get(j).getCnt());
						else if ( stDate.substring(0, 6).equals(listTmp.get(j).getProc_date()) ) nCount2 = Integer.parseInt(listTmp.get(j).getCnt());
					}
*/					
					nCount3 = nCount1 - nCount2;
					
					report.setType1(nCount1); //금월
					report.setType2(nCount2); //이전월	
					
					
					if ( nCount3 < 0 ) report.setbCheckPlus("0");
					else report.setbCheckPlus("1");
					
					if ( nCount3 < 0 ) nCount3 = -(nCount3);
					report.setType3(nCount3); //증감	
					
					list4.set(i, report);
				}
				
				modelAndView.addObject("listChart4Detail", list4);
				
				//1014
				for(int i=0; i<list4_1.size(); i++) {
					
					PrivacyReport report = list4_1.get(i);
					
					report.setStart_date(pr.getStart_date());
					report.setEnd_date(pr.getEnd_date());

					report.setStart_date(prCompare2.getStart_date());
					report.setEnd_date(prCompare2.getEnd_date());
										
					PrivacyReport prev_report = reportSvc.findPrivacylogCntBySystemCompare(report);
					
					
					nCount1=0;
					nCount2=0;
					nCount3=0;
					
					if(report != null)
						nCount1 = Integer.parseInt(report.getCnt());
					if(prev_report != null)
						nCount2 = Integer.parseInt(prev_report.getCnt());
					nCount3 = nCount1 - nCount2;
					
					report.setType1(nCount1); //금월
					report.setType2(nCount2); //이전월	
					
					
					if ( nCount3 < 0 ) report.setbCheckPlus("0");
					else report.setbCheckPlus("1");
					
					if ( nCount3 < 0 ) nCount3 = -(nCount3);
					report.setType3(nCount3); //증감	
					
					list4_1.set(i, report);
				}
				
				modelAndView.addObject("listChart4Detail_1", list4_1);
				//1014<				
				
				List<PrivacyReport> list = new ArrayList<>();
				List<PrivacyReport> list_1 = new ArrayList<>(); 
//				if (use_studentId.length() > 0 && use_studentId.equals("yes")) {
//					list = reportSvc.findReportDetail_chart5_useStudentId(pr);
//					list_1 = reportSvc.findReportDetail_chart5_useStudentId(pr);
//				} else {
//					list = reportSvc.findReportDetail_chart5(pr);
//					list_1 = reportSvc.findReportDetail_chart5_useStudentId(pr);
//				}
				list = reportSvc.findReportDetail_chart5(pr);
				list_1 = reportSvc.findReportDetail_chart5(pr);
				
				for(int i=0; i<list.size(); i++) {
					
					PrivacyReport report = list.get(i);
					report.setSysList(syslist);
					
					report.setStart_date(prCompare.getStart_date());
					report.setEnd_date(prCompare.getEnd_date());	
					PrivacyReport prev_report = reportSvc.findReportDetail_chart5_detail(report);
					
					nCount1=0;
					nCount2=0;
					nCount3=0;

					if(report != null)
						nCount1 = Integer.parseInt(report.getCnt());
					if(prev_report != null)
						nCount2 = Integer.parseInt(prev_report.getCnt());
					nCount3 = nCount1 - nCount2;
					
					report.setType1(nCount1); //금월
					report.setType2(nCount2); //이전월	
					
					
					if ( nCount3 < 0 ) report.setbCheckPlus("0");
					else report.setbCheckPlus("1");
					
					if ( nCount3 < 0 ) nCount3 = -(nCount3);
					report.setType3(nCount3); //증감
					
					list.set(i, report);
				}
				
				modelAndView.addObject("listChart5Detail", list);
				
				//1014
				for(int i=0; i<list_1.size(); i++) {
					
					PrivacyReport report = list_1.get(i);
					report.setSysList(syslist);
					
					report.setStart_date(prCompare2.getStart_date());
					report.setEnd_date(prCompare2.getEnd_date());	
					PrivacyReport prev_report = reportSvc.findReportDetail_chart5_detail(report);
					
					nCount1=0;
					nCount2=0;
					nCount3=0;
					
					if(report != null)
						nCount1 = Integer.parseInt(report.getCnt());
					if(prev_report != null)
						nCount2 = Integer.parseInt(prev_report.getCnt());
					nCount3 = nCount1 - nCount2;
					
					report.setType1(nCount1); //금월
					report.setType2(nCount2); //전년전월	
					
					
					if ( nCount3 < 0 ) report.setbCheckPlus("0");
					else report.setbCheckPlus("1");
					
					if ( nCount3 < 0 ) nCount3 = -(nCount3);
					report.setType3(nCount3); //증감
					
					list_1.set(i, report);
				}
				
				modelAndView.addObject("listChart5Detail_1", list_1);
	
				modelAndView.addObject("privTypeCount", privTypeCount);
		
				if (privTypeCount.size() > 0) {
					privTypeCountAvg = privTypeCountAvg / privTypeCount.size();
				}
				
				modelAndView.addObject("privTypeCountAvg", privTypeCountAvg);
				
				if(ui_type.equals("G")) {
					modelAndView.setViewName("report_new1_gj");
				} else {
					modelAndView.setViewName("report_new1");
				}
			    modelAndView.addObject("reportType", type);

			}else if (type == 2){
				
				List<PrivacyReport> listEmpDetail = reportSvc.findEmpDetailList(pr);
				
				String checkEmpNameMasking = commonDao.checkEmpNameMasking();
				if(checkEmpNameMasking.equals("Y")) {
					for (int i = 0; i < listEmpDetail.size(); i++) {
						if(!"".equals(listEmpDetail.get(i).getEmp_user_name())) {
							String empUserName = listEmpDetail.get(i).getEmp_user_name();
							StringBuilder builder = new StringBuilder(empUserName);
							builder.setCharAt(empUserName.length() - 2, '*');
							listEmpDetail.get(i).setEmp_user_name(builder.toString());
						}
					}
				}
				
				// 비정상행위 의심사례 검토 결과 비정상 행위 기준별 결과
				modelAndView.addObject("listEmpDetail", listEmpDetail);
				
				List list1 = getReportDetail2_chart1(start_date, end_date, syslist);
				modelAndView.addObject("emp_detailList", list1);
				
				List<Report> rule_list = new ArrayList<Report>();
				Report tmp1 = new Report();
				tmp1.setRule_nm("고유식별정보 과다사용");
				rule_list.add(tmp1);
				
				Report tmp2 = new Report();
				tmp2.setRule_nm("비정상 접근");
				rule_list.add(tmp2);
				
				Report tmp3 = new Report();
				tmp3.setRule_nm("과다처리");
				rule_list.add(tmp3);
				
				Report tmp4 = new Report();
				tmp4.setRule_nm("특정인 처리");
				rule_list.add(tmp4);
				
				Report tmp5 = new Report();
				tmp5.setRule_nm("특정시간대 처리");
				rule_list.add(tmp5);
				
				modelAndView.addObject("rule_list", rule_list);
				List<Map<String, Object>> map = getReportDetail2_chart3(pr, rule_list);
				modelAndView.addObject("emp_detailList3", map);
				/*modelAndView.addObject("emp_detailList3", map.get("array"));
				modelAndView.addObject("emp_detailList3_total", map.get("sum"));*/
				
				int empcnt = reportDao.findEmpDetailCount(pr);
				modelAndView.addObject("empcnt", empcnt);
				if(ui_type.equals("G")) {
					modelAndView.setViewName("report_new2_gj");
				} else {
					modelAndView.setViewName("report_new2");
				}
				modelAndView.addObject("reportType", type);
			}
		}
		modelAndView.addObject("download_type", download_type);
		String pdfFullPath = request.getRequestURL() +"?"+ request.getQueryString();
		modelAndView.addObject("pdfFullPath", pdfFullPath);
		modelAndView.addObject("ui_type", ui_type);
		return modelAndView;
	}
	
	//page : reportEmpLevel.html 수준진단보고
	@RequestMapping(value="reportEmpLevel.html")
	public ModelAndView findReportEmpLevel(HttpServletRequest request,
			@RequestParam(value = "type", required = false) int type,
			@RequestParam(value = "period_type", required = false) int period_type,
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date,
			@RequestParam(value = "menu_id", required = false) String menu_id,
			@RequestParam(value = "download_type", required = false) String download_type) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("authorize", reportSvc.reportAuthorizeInfo());
		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
	    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
		String date = dayTime.format(new Date(time));
		String ui_type = commonDao.getUiType();
		modelAndView.addObject("ui_type", ui_type);
		
		// 마지막날짜가 현재보다 클 경우 어제날짜까지로 지정해준다.
		/*
		try {
			Date tmpDate = dayTime.parse(end_date);
			int compare = tmpDate.compareTo(new Date());
			if( compare >= 0) {
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DAY_OF_MONTH, -1);
				end_date = dayTime.format(cal.getTime());
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		*/
		String compareDate = "";
		
		PrivacyReport pr = new PrivacyReport();			//선택 기간
		PrivacyReport prCompare = new PrivacyReport();	//비교 기간(동년전월)
		PrivacyReport prCompare2 = new PrivacyReport();	//비교 기간(전년동월)
		
		
		HttpSession session = request.getSession();
		String master = (String)session.getAttribute("master");
	    String use_reportLogo = (String)session.getAttribute("use_reportLogo");
	    String use_reportLine = (String)session.getAttribute("use_reportLine");
	    if (use_reportLogo != null && use_reportLogo.equals("Y")) {
		    String rootPath = request.getSession().getServletContext().getRealPath("/");
			String attach_path = "resources/upload/";
			String filename = commonDao.selectCurrentImage_logo2();
			String savePath = rootPath + attach_path + filename;
			File file = new File(savePath);
			if(file.exists()) {
				modelAndView.addObject("filename", filename);
			}
	    }
	    modelAndView.addObject("use_reportLogo", use_reportLogo);
	    modelAndView.addObject("use_reportLine", use_reportLine);
		modelAndView.addObject("masterflag", master);
		modelAndView.addObject("report_type", type);
		modelAndView.addObject("period_type", period_type);
		modelAndView.addObject("start_date", start_date);
		modelAndView.addObject("end_date", end_date);
		modelAndView.addObject("date", date);
		modelAndView.addObject("system_seq", system_seq);
		modelAndView.addObject("use_studentId", use_studentId);
		modelAndView.addObject("logo_report_url", logo_report_url);
		modelAndView.addObject("menu_id", menu_id);

		List<String> syslist = new ArrayList<String>();
		

		String tmpList[] = system_seq.split(",");
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}
		syslist.add("");
		
		String sDate[] = start_date.split("-");
		String eDate[] = end_date.split("-");

		int strYear = Integer.parseInt(sDate[0]);
		int strMonth = Integer.parseInt(sDate[1]);
		int endYear = Integer.parseInt(eDate[0]);
		int endMonth = Integer.parseInt(eDate[1]);
		int diffMon = (endYear - strYear)* 12 + (endMonth - strMonth);
		String stDate = start_date.replaceAll("-", "").substring(0,6);
		String edDate = end_date.replaceAll("-", "").substring(0,6);
		pr.setStart_date(stDate);
		pr.setEnd_date(edDate);
		pr.setSysList(syslist);
		
		if(!pr.getSysList().isEmpty()) {
			List<SystemMaster> systems = reportSvc.findSystemMasterDetailList(pr);
			for (SystemMaster sm : systems) {
				PrivacyReport prt = new PrivacyReport();
				prt.setSystem_seq(sm.getSystem_seq());
				prt.setStart_date(stDate);
				prt.setEnd_date(edDate);
				List<PrivacyReport> res = reportDao.getMajorResultType(prt);
				String result_type = "";
				if (res.size() > 0) {
					for (PrivacyReport data : res) {
						String text = data.getPrivacy_desc()  + "(" + data.getCnt() + ") ";
						result_type += text;
					}
				} else {
					result_type = "데이터없음";
				}
				sm.setResult_type(result_type);
			}
			modelAndView.addObject("systems", systems);
		}
	    
	    Calendar cal = Calendar.getInstance();
	    cal.set(strYear, strMonth-1, 1);		//선택기간 시작월 1일
	    
	    cal.add(Calendar.MONTH, -1);    		// 비교 기간 끝(선택기간 한달전)
	    compareDate = dateFormatter.format(cal.getTime());
	    edDate = compareDate.substring(0,6);

		cal.add(Calendar.MONTH, -diffMon);    	// 비교 기간 시작
		compareDate = dateFormatter.format(cal.getTime());
		stDate = compareDate.substring(0,6);

		prCompare.setStart_date(stDate);
		prCompare.setEnd_date(edDate);
		prCompare.setSysList(syslist);
		
		cal.set(strYear, strMonth-1, 1);
		cal.add(Calendar.YEAR, -1);    		// 비교 기간 끝(선택기간 한달전)
	    compareDate = dateFormatter.format(cal.getTime());
		edDate = compareDate.substring(0,6)+"31";

		cal.add(Calendar.YEAR, -diffMon);    	// 비교 기간 시작
		compareDate = dateFormatter.format(cal.getTime());
		stDate = compareDate.substring(0,6)+"01";
		
		prCompare2.setStart_date(stDate);
		prCompare2.setEnd_date(edDate);
		prCompare2.setSysList(syslist);
		
		PrivacyReport findPrivacyReportBySystemTop1 = reportSvc.findPrivacyReportBySystemTop1(pr);
		if(findPrivacyReportBySystemTop1 != null) {
			modelAndView.addObject("reportBySys", findPrivacyReportBySystemTop1.getSystem_name());
		}
		PrivacyReport findPrivacyReportByDeptTop1 = reportSvc.findPrivacyReportByDeptTop1(pr);
		if(findPrivacyReportByDeptTop1 != null) {
			modelAndView.addObject("reportByDept", findPrivacyReportByDeptTop1.getDept_name());
		}
		int diffMonth = getDiffMonth(pr);
		modelAndView.addObject("diffMonth", diffMonth);
		
		int logCnt = reportSvc.findPrivacyTotalLogCnt(pr);
		modelAndView.addObject("logCnt", logCnt);
		
		int privCnt = reportSvc.findPrivacyTotalCnt(pr);
		modelAndView.addObject("privCnt", privCnt);

		List<PrivacyReport> privTypeCount;
		int privTypeCountAvg = 0;

		if(period_type == 1) {
			privTypeCount = reportSvc.findPrivTypeCountWeek(pr);
		
			String proc_date="";
			int nConut = 0;
			int nPrivCount = 0;
			String tmpS="";
			
			for (int i = 0; i < privTypeCount.size(); ++i) {
				PrivacyReport tmp = privTypeCount.get(i);
				proc_date = strYear+"년 "+strMonth+"월 "+ tmp.getWeek() +"주차";
				tmp.setProc_date(proc_date);
				if (i == 0) {
					tmp.setTot_cnt("-");
				} else {
					nConut = Integer.parseInt(tmp.getCnt());
					nPrivCount = Integer.parseInt(privTypeCount.get(i - 1).getCnt());
					tmpS = Integer.toString(nConut - nPrivCount);
					if (tmpS.indexOf("-") >= 0)
						tmp.setbCheckPlus("0");
					else
						tmp.setbCheckPlus("1");

					tmpS = tmpS.replaceAll("-", "");
					tmp.setTot_cnt(tmpS);
				}
				privTypeCountAvg = privTypeCountAvg + Integer.parseInt(tmp.getCnt());
				privTypeCount.set(i, tmp);
			}
		} else {
			pr.setStart_date(pr.getStart_date().substring(0,6));
			pr.setEnd_date(pr.getEnd_date().substring(0,6));
			privTypeCount = reportSvc.findPrivacyReportDetailChart3_1(pr);
			
			String proc_date="";
			int nConut = 0;
			int nPrivCount = 0;
			String tmpS="";
			
			for (int i = 0; i < privTypeCount.size(); ++i) {
				PrivacyReport tmp = privTypeCount.get(i);
				
				proc_date = tmp.getMonth().substring(0,4)+"년 "+tmp.getMonth().substring(4,6)+"월";
				
				tmp.setProc_date(proc_date);

				if (i == 0) {
					tmp.setTot_cnt("-");
				} else {
					nConut = Integer.parseInt(tmp.getCnt());
					nPrivCount = Integer.parseInt(privTypeCount.get(i - 1).getCnt());
					tmpS = Integer.toString(nConut - nPrivCount);
					if (tmpS.indexOf("-") >= 0)
						tmp.setbCheckPlus("0");
					else
						tmp.setbCheckPlus("1");

					tmpS = tmpS.replaceAll("-", "");
					tmp.setTot_cnt(tmpS);
				}

				privTypeCountAvg = privTypeCountAvg + Integer.parseInt(tmp.getCnt());

				privTypeCount.set(i, tmp);
			}
		}

		// 개인정보 업무 행위별 결과(단위: 처리한 개인정보 건수)
		
		List<PrivacyReport> systemCurdCount = reportSvc.findSystemCurdCount(pr);
		modelAndView.addObject("systemCurdCount", systemCurdCount);
		
		List<PrivacyReport> list4 = reportSvc.findPrivacylogCntBySystem(pr);
		List<PrivacyReport> list4_1 = reportSvc.findPrivacylogCntBySystem(pr);
		
		int nCount1, nCount2, nCount3, nCount4, nCount5;
		
		for(int i=0; i<list4.size(); i++) {
			PrivacyReport report = list4.get(i);
			report.setStart_date(prCompare.getStart_date().substring(0,6));
			report.setEnd_date(prCompare.getEnd_date().substring(0,6));
			PrivacyReport prev_report = reportSvc.findPrivacylogCntBySystemCompare(report);
			report.setStart_date(prCompare2.getStart_date().substring(0,6));
			report.setEnd_date(prCompare2.getEnd_date().substring(0,6));
			PrivacyReport prevYear_report = reportSvc.findPrivacylogCntBySystemCompare(report);
			
			nCount1=0;
			nCount2=0;
			nCount3=0;
			nCount4=0;
			nCount5=0;
			
			if(report != null) {
				nCount1 = Integer.parseInt(report.getCnt());
			}
			if(prev_report != null) {
				nCount2 = Integer.parseInt(prev_report.getCnt());
			}
			if(prevYear_report != null) {
				nCount4 = Integer.parseInt(prevYear_report.getCnt());
			}
			nCount3 = nCount1 - nCount2;
			nCount5 = nCount1 - nCount4;
			report.setType1(nCount1); //금월
			report.setType2(nCount2); //이전월
			report.setType4(nCount4);
			if ( nCount3 < 0 ) {
				report.setbCheckPlus("0");
			}
			else {
				report.setbCheckPlus("1");
			}
			if ( nCount3 < 0 ) {
				nCount3 = -(nCount3);
			}
			if ( nCount5 < 0 ) {
				report.setbCheckPlus1("0");
			}
			else {
				report.setbCheckPlus1("1");
			}
			if ( nCount5 < 0 ) {
				nCount5 = -(nCount5);
			}
			report.setType3(nCount3); //증감
			report.setType5(nCount5);
			list4.set(i, report);
		}
		
		modelAndView.addObject("listChart4Detail", list4);
		for(int i=0; i<list4_1.size(); i++) {
			PrivacyReport report = list4_1.get(i);
			report.setStart_date(prCompare2.getStart_date());
			report.setEnd_date(prCompare2.getEnd_date());
								
			PrivacyReport prev_report = reportSvc.findPrivacylogCntBySystemCompare(report);
			
			nCount1=0;
			nCount2=0;
			nCount3=0;
			
			if(report != null) {
				nCount1 = Integer.parseInt(report.getCnt());
			}
			if(prev_report != null) {
				nCount2 = Integer.parseInt(prev_report.getCnt());
			}
			nCount3 = nCount1 - nCount2;
			
			report.setType1(nCount1); //금월
			report.setType2(nCount2); //이전월	
			if ( nCount3 < 0 ) {
				report.setbCheckPlus("0");
			}
			else {
				report.setbCheckPlus("1");
			}
			if ( nCount3 < 0 ) {
				nCount3 = -(nCount3);
			}
			report.setType3(nCount3); //증감	
			
			list4_1.set(i, report);
		}
		modelAndView.addObject("listChart4Detail_1", list4_1);
		
		
		//소속별 접속기록 현황 TOP10 표			
		List<PrivacyReport> list = new ArrayList<>();
		List<PrivacyReport> list_1 = new ArrayList<>();
		
		list = reportSvc.findReportDetail_chart5(pr);
		list_1 = reportSvc.findReportDetail_chart5(pr);
		if(list.size() < 1) {
			prCompare.setSysList(syslist);
			prCompare.setStart_date(prCompare.getStart_date().substring(0,6));
			prCompare.setEnd_date(prCompare.getEnd_date().substring(0,6));
			List<PrivacyReport> prevRepVoTempList = reportSvc.findReportDetail_chart5(prCompare);
			if(prevRepVoTempList.size() < 1) {
				prCompare2.setSysList(syslist);
				prCompare2.setStart_date(prCompare2.getStart_date().substring(0,6));
				prCompare2.setEnd_date(prCompare2.getEnd_date().substring(0,6));
				List<PrivacyReport> prevYearRepVoList = reportSvc.findReportDetail_chart5(prCompare2);
				for(int i=0; i<prevYearRepVoList.size(); i++) {
					
					PrivacyReport prevYearReport = prevYearRepVoList.get(i);
					
					nCount1=0;
					nCount2=0;
					nCount3=0;
					nCount4=0;
					nCount5=0;
					
					if(prevYearReport != null) {
						nCount4 = Integer.parseInt(prevYearReport.getCnt());
					}
					nCount3 = nCount1 - nCount2;
					nCount5 = nCount1 - nCount4;
					prevYearReport.setType1(nCount1);
					prevYearReport.setType2(nCount2);
					prevYearReport.setType4(nCount4);
					
					
					if ( nCount3 < 0 ) {
						prevYearReport.setbCheckPlus("0");
					}
					else {
						prevYearReport.setbCheckPlus("1");
					}
					if ( nCount3 < 0 ) {
						nCount3 = -(nCount3);
					}
					if ( nCount5 < 0 ) {
						prevYearReport.setbCheckPlus1("0");
					}
					else {
						prevYearReport.setbCheckPlus1("1");
					}
					if ( nCount5 < 0 ) {
						nCount5 = -(nCount5);
					}
					prevYearReport.setType3(nCount3);
					prevYearReport.setType5(nCount5);
					
					prevYearRepVoList.set(i, prevYearReport);
				}
				modelAndView.addObject("listChart5Detail", prevYearRepVoList);
			} else {
				PrivacyReport report = new PrivacyReport();
				for(int i=0; i<prevRepVoTempList.size(); i++) {
					PrivacyReport prevRepVo = prevRepVoTempList.get(i);
					report.setSysList(syslist);
					report.setStart_date(prCompare2.getStart_date().substring(0,6));
					report.setEnd_date(prCompare2.getEnd_date().substring(0,6));
					PrivacyReport prevYearRepVo = reportSvc.findReportDetail_chart5_detail(report);
					
					nCount1=0;
					nCount2=0;
					nCount3=0;
					nCount4=0;
					nCount5=0;
					
					if(prevRepVo != null) {
						nCount2 = Integer.parseInt(prevRepVo.getCnt());
					}
					if(prevYearRepVo != null) {
						nCount4 = Integer.parseInt(prevYearRepVo.getCnt());
					}
					nCount3 = nCount1 - nCount2;
					nCount5 = nCount1 - nCount4;
					prevRepVo.setType1(nCount1);
					prevRepVo.setType2(nCount2);
					prevRepVo.setType4(nCount4);
					
					 
					if ( nCount3 < 0 ) {
						prevRepVo.setbCheckPlus("0");
					}
					else {
						prevRepVo.setbCheckPlus("1");
					}
					if ( nCount3 < 0 ) {
						nCount3 = -(nCount3);
					}
					if ( nCount5 < 0 ) {
						prevRepVo.setbCheckPlus1("0");
					}
					else {
						prevRepVo.setbCheckPlus1("1");
					}
					if ( nCount5 < 0 ) {
						nCount5 = -(nCount5);
					}
					prevRepVo.setType3(nCount3);
					prevRepVo.setType5(nCount5);
					
					prevRepVoTempList.set(i, prevRepVo);
				}
				modelAndView.addObject("listChart5Detail", prevRepVoTempList);
			}
		} else {
			for(int i=0; i<list.size(); i++) {
				PrivacyReport report = list.get(i);
				report.setSysList(syslist);
				report.setStart_date(prCompare.getStart_date().substring(0,6));
				report.setEnd_date(prCompare.getEnd_date().substring(0,6));
				PrivacyReport prev_report = reportSvc.findReportDetail_chart5_detail(report);
				report.setStart_date(prCompare2.getStart_date().substring(0,6));
				report.setEnd_date(prCompare2.getEnd_date().substring(0,6));
				PrivacyReport prevYear_report = reportSvc.findReportDetail_chart5_detail(report);
				
				nCount1=0;
				nCount2=0;
				nCount3=0;
				nCount4=0;
				nCount5=0;
	
				if(report != null) {
					nCount1 = Integer.parseInt(report.getCnt());
				}
				if(prev_report != null) {
					nCount2 = Integer.parseInt(prev_report.getCnt());
				}
				if(prevYear_report != null) {
					nCount4 = Integer.parseInt(prevYear_report.getCnt());
				}
				nCount3 = nCount1 - nCount2;
				nCount5 = nCount1 - nCount4;
				report.setType1(nCount1);
				report.setType2(nCount2);
				report.setType4(nCount4);
				
				
				if ( nCount3 < 0 ) {
					report.setbCheckPlus("0");
				}
				else {
					report.setbCheckPlus("1");
				}
				if ( nCount3 < 0 ) {
					nCount3 = -(nCount3);
				}
				if ( nCount5 < 0 ) {
					report.setbCheckPlus1("0");
				}
				else {
					report.setbCheckPlus1("1");
				}
				if ( nCount5 < 0 ) {
					nCount5 = -(nCount5);
				}
				report.setType3(nCount3);
				report.setType5(nCount5);
				
				list.set(i, report);
			}
			modelAndView.addObject("listChart5Detail", list);
		}
		
		
		//1014
		for(int i=0; i<list_1.size(); i++) {
			
			PrivacyReport report = list_1.get(i);
			report.setSysList(syslist);
			
			report.setStart_date(prCompare2.getStart_date());
			report.setEnd_date(prCompare2.getEnd_date());	
			PrivacyReport prev_report = reportSvc.findReportDetail_chart5_detail(report);
			
			nCount1=0;
			nCount2=0;
			nCount3=0;
			
			if(report != null)
				nCount1 = Integer.parseInt(report.getCnt());
			if(prev_report != null)
				nCount2 = Integer.parseInt(prev_report.getCnt());
			nCount3 = nCount1 - nCount2;
			
			report.setType1(nCount1); //금월
			report.setType2(nCount2); //전년전월	
			
			
			if ( nCount3 < 0 ) report.setbCheckPlus("0");
			else report.setbCheckPlus("1");
			
			if ( nCount3 < 0 ) nCount3 = -(nCount3);
			report.setType3(nCount3); //증감
			
			list_1.set(i, report);
		}
		
		modelAndView.addObject("listChart5Detail_1", list_1);

		modelAndView.addObject("privTypeCount", privTypeCount);

		if (privTypeCount.size() > 0) {
			privTypeCountAvg = privTypeCountAvg / privTypeCount.size();
		}
		
		modelAndView.addObject("privTypeCountAvg", privTypeCountAvg);
		
		if(ui_type.equals("G")) {
			modelAndView.setViewName("report_new1_gj");
		} else {
			modelAndView.setViewName("report_new1");
		}
	    modelAndView.addObject("reportType", type);

		modelAndView.addObject("download_type", download_type);
		String pdfFullPath = request.getRequestURL() +"?"+ request.getQueryString();
		modelAndView.addObject("pdfFullPath", pdfFullPath);
		modelAndView.addObject("ui_type", ui_type);
		return modelAndView;
	}
	
	
	//CPO_오남용보고 reportCPOPop.jsp
	@RequestMapping(value="reportCPO.html", method={RequestMethod.GET, RequestMethod.POST})
	public ModelAndView findReportCPO(@RequestParam Map<String, String> param, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		int reportType = Integer.parseInt(param.get("type"));
		int dateType = Integer.parseInt(param.get("period_type"));
		String systemSeq = String.valueOf(param.get("system_seq"));
		String startDate = String.valueOf(param.get("start_date"));
		String endDate = String.valueOf(param.get("end_date"));
		String menuId = String.valueOf(param.get("menu_id"));
		String downType = String.valueOf(param.get("download_type"));
		String quarter_type = String.valueOf(param.get("quarter_type"));
		
		DateUtil dateUtil = new DateUtil();
		String date = dateUtil.getDateNow();
		endDate = dateUtil.getReportEndDate(endDate);
		
		HttpSession session = request.getSession();
		String ui_type = (String)session.getAttribute("ui_type");
	    String master = (String)session.getAttribute("master");
	    String use_reportLogo = (String)session.getAttribute("use_reportLogo");
	    String use_reportLine = (String)session.getAttribute("use_reportLine");
	    String pdfFullPath = request.getRequestURL() +"?"+ request.getQueryString();
	    
	    //로그 파일이 존재하면 생성
	    if (use_reportLogo != null && use_reportLogo.equals("Y")) {
			String filename = commonDao.selectCurrentImage_logo2();
			String savePath = request.getSession().getServletContext().getRealPath("/") + "resources/upload/" + filename;
			File file = new File(savePath);
			if(file.exists()) {
				mav.addObject("filename", filename);
			}
	    }
	    
	    //system_seq가져오기(시스템 리스트로 분리)
		String[] tmpList = systemSeq.split(",");
		List<String> sysList = new ArrayList<String>(Arrays.asList(tmpList));
		sysList.add("00");
		
		// yyyyMM ( start_date, end_date )
		PrivacyReport pr = dateUtil.getYyyyMmDatePriReportVO(startDate, endDate);
		pr.setSysList(sysList);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
		Calendar cal = Calendar.getInstance();
		String sd = pr.getStart_date();
		String ed = pr.getEnd_date();
		try {
			if(dateType==1) {// 월
				cal.setTime(sdf.parse(sd));
				cal.add(cal.MONTH, -1);
				pr.setCompare_start_date(sdf.format(cal.getTime()));
				pr.setCompare_end_date(sdf.format(cal.getTime()));
			} else if(dateType==2) {// 분기
				cal.setTime(sdf.parse(sd));
				cal.add(cal.MONTH, -3);
				pr.setCompare_start_date(sdf.format(cal.getTime()));
				cal.setTime(sdf.parse(ed));
				cal.add(cal.MONTH, -3);
				pr.setCompare_end_date(sdf.format(cal.getTime()));
			} else if(dateType==4) {// 연도
				cal.setTime(sdf.parse(sd));
				cal.add(cal.YEAR, -1);
				pr.setCompare_start_date(sdf.format(cal.getTime()));
				cal.setTime(sdf.parse(ed));
				cal.add(cal.YEAR, -1);
				pr.setCompare_end_date(sdf.format(cal.getTime()));
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		// 점검대상 ( 월간, 분기, 연간에 따라 다름 )
		List<PrivacyReport> systems = reportSvc.abnormalTotalListBySystem(pr, dateType, sd);
		mav.addObject("systems", systems);
		
		// 총 처리량
		int totalCnt = reportSvc.abnormalTotalCnt(pr);
		mav.addObject("totalCnt", totalCnt);
		// 시스템별 시나리오별 처리량
		List<PrivacyReport> systemByScenario = reportSvc.abnormalSystemList(pr);
		mav.addObject("systemByScenario", systemByScenario);
		// 소속별 시나리오별 처리량
		List<PrivacyReport> deptByScenario = reportSvc.abnormalDeptList(pr);
		mav.addObject("deptByScenario", deptByScenario);
		// 개인별 시나리오별 처리량
		List<PrivacyReport> userByScenario = reportSvc.abnormalUserList(pr);
		mav.addObject("userByScenario", userByScenario);
		
		// 시스템별 처리량 이용량 top5
		List<PrivacyReport> systemTop5 = reportSvc.abnormalSystemTop5(pr);
		mav.addObject("systemTop5", systemTop5);
		// 부서별 처리량 이용량 top5
		List<PrivacyReport> deptTop5 = reportSvc.abnormalDeptTop5(pr);
		mav.addObject("deptTop5", deptTop5);
		String deptNameTop5 = "";
		if(deptTop5.get(0) != null) {
			for(int i=0; i<deptTop5.size()-1; i++) {
				String dept_name = deptTop5.get(i).getDept_name();
				if(dept_name != null) {
					deptNameTop5 += dept_name + ", ";
				} else {
					deptNameTop5 = deptNameTop5.substring(0, deptNameTop5.length()-2);
					break;
				}
			}
		}
		mav.addObject("deptNameTop5", deptNameTop5);
		// 개인별 처리량 이용량 top5
		List<PrivacyReport> userTop5 = reportSvc.abnormalUserTop5(pr);
		mav.addObject("userTop5", userTop5);
		String empUserNameTop5 = "";
		if(userTop5.get(0) != null) {
			for(int i=0; i<userTop5.size()-1; i++) {
				String emp_user_name = userTop5.get(i).getEmp_user_name();
				String dept_name = userTop5.get(i).getDept_name();
				if(emp_user_name != null) {
					if(emp_user_name.equals("공통")) {
						empUserNameTop5 += emp_user_name + ", ";
					} else {
						empUserNameTop5 += dept_name + " " + emp_user_name + ", ";
					}
				} else {
					empUserNameTop5 = empUserNameTop5.substring(0, empUserNameTop5.length()-2);
					break;
				}
			}
		}
		mav.addObject("empUserNameTop5", empUserNameTop5);
		
		// report_option 정보
		Map<String, String> optionMap = new HashMap<String, String>();
		optionMap.put("code_id", "2");
		String proc_month = startDate.replace("-", "").substring(0, 6);
		optionMap.put("proc_month", proc_month);
		optionMap.put("period_type", param.get("period_type"));
		List<Map<String, String>> reportOption = reportDao.findReportOption(optionMap);
		mav.addObject("reportOption", reportOption);
		
		// top5 표 컬럼 ex. 전월/당월, 이전분기/1분기, 전년/금년
		String preText = "";
		String thisText = "";
		String reportDate = pr.getStart_date().substring(0, 4) +"년 ";
		if(dateType==1) {// 월
			preText = "전월";
			thisText = "당월";
			reportDate += pr.getStart_date().substring(4)+"월";
		} else if(dateType==2) {// 분기
			preText = "이전분기";
			thisText = quarter_type+"분기";
			reportDate += thisText;
		} else if(dateType==4) {// 연도
			preText = "전년";
			thisText = "금년";
		}
		mav.addObject("preText", preText);
		mav.addObject("thisText", thisText);
		mav.addObject("reportDate", reportDate);
		mav.addObject("start_date", startDate);
		mav.addObject("end_date", endDate);
		mav.addObject("date", date);
		
		mav.addObject("authorize", reportSvc.reportAuthorizeInfo()); // 결재라인
	    mav.addObject("use_reportLine", use_reportLine);
	    mav.addObject("use_reportLogo", use_reportLogo);
	    mav.addObject("masterflag", master);
	    mav.addObject("reportType", reportType);
	    mav.addObject("period_type", dateType);
	    mav.addObject("system_seq", systemSeq);
	    mav.addObject("use_studentId", use_studentId);
	    mav.addObject("logo_report_url", logo_report_url);
	    mav.addObject("menu_id", menuId);
	    mav.addObject("download_type", downType);
	    mav.addObject("pdfFullPath", pdfFullPath);
	    mav.addObject("ui_type", ui_type);
		
		mav.setViewName("report_new2");
		return mav;
	}
	@RequestMapping(value = "abnormalSystemChart.html")
	@ResponseBody
	public JSONArray abnormalSystemChart(
		@RequestParam(value = "system_seq",required=false) String system_seq,
		@RequestParam(value = "start_date", required = false) String start_date,
		@RequestParam(value = "end_date", required = false) String end_date) {

		String tmpList[] = system_seq.split(",");
		List<String> syslist = new ArrayList<String>(Arrays.asList(tmpList));
		syslist.add("00");
		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", "").substring(0, 6));
		pr.setEnd_date(end_date.replaceAll("-", "").substring(0, 6));
		pr.setSysList(syslist);
		
		JSONArray jArray = new JSONArray();
		List<PrivacyReport> systemTop5 = reportSvc.abnormalSystemTop5Chart(pr);
		for (int i = 0; i < systemTop5.size(); i++) {
			JSONObject jsonObj = new JSONObject();
			PrivacyReport report = systemTop5.get(i);
			jsonObj.put("system_name", report.getSystem_name());
			jsonObj.put("totalCnt", report.getTotalCnt());
			jsonObj.put("logcnt", report.getLogcnt());
			jArray.add(jsonObj);
		}
		return jArray;
	}
	@RequestMapping(value = "abnormalDeptChart.html")
	@ResponseBody
	public JSONArray abnormalDeptChart(
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {

		String tmpList[] = system_seq.split(",");
		List<String> syslist = new ArrayList<String>(Arrays.asList(tmpList));
		syslist.add("00");
		
		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", "").substring(0, 6));
		pr.setEnd_date(end_date.replaceAll("-", "").substring(0, 6));
		pr.setSysList(syslist);

		List<PrivacyReport> deptTop5 = reportSvc.abnormalDeptTop5Chart(pr);

		JSONArray jArray = new JSONArray();
		for (int i = 0; i < deptTop5.size(); i++) {
			JSONObject jsonObj = new JSONObject();
			PrivacyReport report = deptTop5.get(i);
			jsonObj.put("dept_name", report.getDept_name());
			jsonObj.put("totalCnt", report.getTotalCnt());
			jsonObj.put("logcnt", report.getLogcnt());
			jArray.add(jsonObj);
		}
		return jArray;
	}
	@RequestMapping(value = "abnormalUserChart.html")
	@ResponseBody
	public JSONArray abnormalUserChart(
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {

		String tmpList[] = system_seq.split(",");
		List<String> syslist = new ArrayList<String>(Arrays.asList(tmpList));
		syslist.add("00");
		
		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", "").substring(0, 6));
		pr.setEnd_date(end_date.replaceAll("-", "").substring(0, 6));
		pr.setSysList(syslist);

		List<PrivacyReport> userTop5 = reportSvc.abnormalUserTop5Chart(pr);

		JSONArray jArray = new JSONArray();
		for (int i = 0; i < userTop5.size(); i++) {
			JSONObject jsonObj = new JSONObject();
			PrivacyReport report = userTop5.get(i);
			String emp_user_name = report.getDept_name() + " " + report.getEmp_user_name();
			jsonObj.put("emp_user_name", emp_user_name);
			jsonObj.put("totalCnt", report.getTotalCnt());
			jsonObj.put("logcnt", report.getLogcnt());
			jArray.add(jsonObj);
		}
		return jArray;
	}
	
	/**
	 * @page : reportEmpSystem.html
	 * @desc : 담당자용_시스템별 수준진단보고(type_3)
	 * @auth : syjung
	 */
	@RequestMapping(value="reportEmpSystem.html")
	public ModelAndView findReportEmpSystem(HttpServletRequest request,
			@RequestParam(value = "type", required = false) int type,
			@RequestParam(value = "period_type", required = false) int period_type,
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date,
			@RequestParam(value = "menu_id", required = false) String menu_id,
			@RequestParam(value = "download_type", required = false) String download_type) {
		ModelAndView modelAndView = new ModelAndView();
		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
	    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
		String date = dayTime.format(new Date(time));
		String ui_type = commonDao.getUiType();
		modelAndView.addObject("ui_type", ui_type);
		
		// 마지막날짜가 현재보다 클 경우 어제날짜까지로 지정해준다.
		/*
		try {
			Date tmpDate = dayTime.parse(end_date);
			int compare = tmpDate.compareTo(new Date());
			if( compare >= 0) {
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DAY_OF_MONTH, -1);
				end_date = dayTime.format(cal.getTime());
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		*/
		String compareDate = "";
		
		PrivacyReport pr = new PrivacyReport();			//선택 기간
		PrivacyReport prCompare = new PrivacyReport();	//비교 기간(동년전월)
		PrivacyReport prCompare2 = new PrivacyReport();	//비교 기간(전년동월)
		
		
		HttpSession session = request.getSession();
	    String master = (String)session.getAttribute("master");
	    
	    String use_reportLogo = (String)session.getAttribute("use_reportLogo");
	    modelAndView.addObject("use_reportLogo", use_reportLogo);
	    if (use_reportLogo != null && use_reportLogo.equals("Y")) {
		    String rootPath = request.getSession().getServletContext().getRealPath("/");
			String attach_path = "resources/upload/";
			
			String filename = commonDao.selectCurrentImage_logo2();
			String savePath = rootPath + attach_path + filename;
			
			File file = new File(savePath);
			
			if(file.exists()) {
				modelAndView.addObject("filename", filename);
			}
	    }
	    
	    String use_reportLine = (String)session.getAttribute("use_reportLine");
	    modelAndView.addObject("use_reportLine", use_reportLine);
	    
		modelAndView.addObject("masterflag", master);
		modelAndView.addObject("report_type", type);
		modelAndView.addObject("period_type", period_type);
		modelAndView.addObject("start_date", start_date);
		modelAndView.addObject("end_date", end_date);
		modelAndView.addObject("date", date);
		modelAndView.addObject("system_seq", system_seq);
		modelAndView.addObject("use_studentId", use_studentId);
		modelAndView.addObject("logo_report_url", logo_report_url);
		modelAndView.addObject("menu_id", menu_id);

		List<String> syslist = new ArrayList<String>();
		

		String tmpList[] = system_seq.split(",");
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}
		syslist.add("");
		
		String sDate[] = start_date.split("-");
		String eDate[] = end_date.split("-");

		int strYear = Integer.parseInt(sDate[0]);
		int strMonth = Integer.parseInt(sDate[1]);
		int endYear = Integer.parseInt(eDate[0]);
		int endMonth = Integer.parseInt(eDate[1]);

		int diffMon = (endYear - strYear)* 12 + (endMonth - strMonth);

		String stDate = start_date.replaceAll("-", "").substring(0,6);
		String edDate = end_date.replaceAll("-", "").substring(0,6);
		
		pr.setStart_date(stDate);
		pr.setEnd_date(edDate);
		pr.setSysList(syslist);
		
		if(!pr.getSysList().isEmpty()) {
			List<SystemMaster> systems = reportSvc.findSystemMasterDetailList(pr);
			for (SystemMaster sm : systems) {
				PrivacyReport prt = new PrivacyReport();
				prt.setSystem_seq(sm.getSystem_seq());
				prt.setStart_date(stDate);
				prt.setEnd_date(edDate);
				List<PrivacyReport> res = reportDao.getMajorResultType(prt);
				String result_type = "";
				if (res.size() > 0) {
					for (PrivacyReport data : res) {
						String text = data.getPrivacy_desc()  + "(" + data.getCnt() + ") ";
						result_type += text;
					}
				} else {
					result_type = "데이터없음";
				}
				sm.setResult_type(result_type);
			}
			modelAndView.addObject("systems", systems);
		}
	    
	    Calendar cal = Calendar.getInstance();
	    cal.set(strYear, strMonth-1, 1);		//선택기간 시작월 1일
	    
	    cal.add(Calendar.MONTH, -1);    		// 비교 기간 끝(선택기간 한달전)
	    compareDate = dateFormatter.format(cal.getTime());
	    edDate = compareDate.substring(0,6);

		cal.add(Calendar.MONTH, -diffMon);    	// 비교 기간 시작
		compareDate = dateFormatter.format(cal.getTime());
		stDate = compareDate.substring(0,6);

		prCompare.setStart_date(stDate);
		prCompare.setEnd_date(edDate);
		prCompare.setSysList(syslist);
		
		cal.set(strYear, strMonth-1, 1);
		cal.add(Calendar.YEAR, -1);    		// 비교 기간 끝(선택기간 한달전)
	    compareDate = dateFormatter.format(cal.getTime());
		edDate = compareDate.substring(0,6)+"31";

		cal.add(Calendar.YEAR, -diffMon);    	// 비교 기간 시작
		compareDate = dateFormatter.format(cal.getTime());
		stDate = compareDate.substring(0,6)+"01";
		
		prCompare2.setStart_date(stDate);
		prCompare2.setEnd_date(edDate);
		prCompare2.setSysList(syslist);
		
		PrivacyReport findPrivacyReportBySystemTop1 = reportSvc.findPrivacyReportBySystemTop1(pr);
		if(findPrivacyReportBySystemTop1 != null)
			modelAndView.addObject("reportBySys", findPrivacyReportBySystemTop1.getSystem_name());
		
		PrivacyReport findPrivacyReportByDeptTop1 = reportSvc.findPrivacyReportByDeptTop1(pr);
		if(findPrivacyReportByDeptTop1 != null)
			modelAndView.addObject("reportByDept", findPrivacyReportByDeptTop1.getDept_name());

		int diffMonth = getDiffMonth(pr);
		modelAndView.addObject("diffMonth", diffMonth);
		
		int logCnt = reportSvc.findPrivacyTotalLogCnt(pr);
		modelAndView.addObject("logCnt", logCnt);
		
		int privCnt = reportSvc.findPrivacyTotalCnt(pr);
		modelAndView.addObject("privCnt", privCnt);

		List<PrivacyReport> privTypeCount;
		int privTypeCountAvg = 0;

		if(period_type == 1) {
			privTypeCount = reportSvc.findPrivTypeCountWeek(pr);
		
			String proc_date="";
			int nConut = 0;
			int nPrivCount = 0;
			String tmpS="";
			
			for (int i = 0; i < privTypeCount.size(); ++i) {
				PrivacyReport tmp = privTypeCount.get(i);
				
				proc_date = strYear+"년 "+strMonth+"월 "+ tmp.getWeek() +"주차";
				
				tmp.setProc_date(proc_date);

				if (i == 0) {
					tmp.setTot_cnt("-");
				} else {
					nConut = Integer.parseInt(tmp.getCnt());
					nPrivCount = Integer.parseInt(privTypeCount.get(i - 1).getCnt());
					tmpS = Integer.toString(nConut - nPrivCount);
					if (tmpS.indexOf("-") >= 0)
						tmp.setbCheckPlus("0");
					else
						tmp.setbCheckPlus("1");

					tmpS = tmpS.replaceAll("-", "");
					tmp.setTot_cnt(tmpS);
				}

				privTypeCountAvg = privTypeCountAvg + Integer.parseInt(tmp.getCnt());

				privTypeCount.set(i, tmp);
			}
		} else {
			pr.setStart_date(pr.getStart_date().substring(0,6));
			pr.setEnd_date(pr.getEnd_date().substring(0,6));
			privTypeCount = reportSvc.findPrivacyReportDetailChart3_1(pr);
			
			String proc_date="";
			int nConut = 0;
			int nPrivCount = 0;
			String tmpS="";
			
			for (int i = 0; i < privTypeCount.size(); ++i) {
				PrivacyReport tmp = privTypeCount.get(i);
				
				proc_date = tmp.getMonth().substring(0,4)+"년 "+tmp.getMonth().substring(4,6)+"월";
				
				tmp.setProc_date(proc_date);

				if (i == 0) {
					tmp.setTot_cnt("-");
				} else {
					nConut = Integer.parseInt(tmp.getCnt());
					nPrivCount = Integer.parseInt(privTypeCount.get(i - 1).getCnt());
					tmpS = Integer.toString(nConut - nPrivCount);
					if (tmpS.indexOf("-") >= 0)
						tmp.setbCheckPlus("0");
					else
						tmp.setbCheckPlus("1");

					tmpS = tmpS.replaceAll("-", "");
					tmp.setTot_cnt(tmpS);
				}

				privTypeCountAvg = privTypeCountAvg + Integer.parseInt(tmp.getCnt());

				privTypeCount.set(i, tmp);
			}
		}

		// 개인정보 업무 행위별 결과(단위: 처리한 개인정보 건수)
		
		List<PrivacyReport> systemCurdCount = reportSvc.findSystemCurdCount(pr);
		modelAndView.addObject("systemCurdCount", systemCurdCount);
		
		List<PrivacyReport> list4 = reportSvc.findPrivacylogCntBySystem(pr);
		List<PrivacyReport> list4_1 = reportSvc.findPrivacylogCntBySystem(pr);
		
		int nCount1, nCount2, nCount3, nCount4, nCount5;
		
		for(int i=0; i<list4.size(); i++) {
			
			PrivacyReport reportVo = list4.get(i);
			reportVo.setStart_date(prCompare.getStart_date());
			reportVo.setEnd_date(prCompare.getEnd_date());
			PrivacyReport prev_report = reportSvc.findPrivacylogCntBySystemCompare(reportVo);
			reportVo.setStart_date(prCompare2.getStart_date().substring(0,6));
			reportVo.setEnd_date(prCompare2.getEnd_date().substring(0,6));
			PrivacyReport prevYearReport = reportSvc.findPrivacylogCntBySystemCompare(reportVo);
			nCount1=0;
			nCount2=0;
			nCount3=0;
			nCount4=0;
			nCount5=0;
			
			if(reportVo != null) {
				nCount1 = Integer.parseInt(reportVo.getCnt());
			}
			if(prev_report != null) {
				nCount2 = Integer.parseInt(prev_report.getCnt());
			}
			if(prevYearReport != null) {
				nCount4 = Integer.parseInt(prevYearReport.getCnt());
			}
			nCount3 = nCount1 - nCount2;
			nCount5 = nCount1 - nCount4;
			reportVo.setType1(nCount1);
			reportVo.setType2(nCount2);
			reportVo.setType4(nCount4);
			
			
			if ( nCount3 < 0 ) {
				reportVo.setbCheckPlus("0");
			}
			else {
				reportVo.setbCheckPlus("1");
			}
			if ( nCount3 < 0 ) {
				nCount3 = -(nCount3);
			}
			if ( nCount5 < 0 ) {
				reportVo.setbCheckPlus1("0");
			}
			else {
				reportVo.setbCheckPlus1("1");
			}
			if ( nCount5 < 0 ) {
				nCount5 = -(nCount5);
			}
			reportVo.setType3(nCount3);
			reportVo.setType5(nCount5);
			
			list4.set(i, reportVo);
		}
		
		modelAndView.addObject("listChart4Detail", list4);
		
		//1014
		for(int i=0; i<list4_1.size(); i++) {
			
			PrivacyReport report = list4_1.get(i);
			
			report.setStart_date(pr.getStart_date());
			report.setEnd_date(pr.getEnd_date());

			report.setStart_date(prCompare2.getStart_date());
			report.setEnd_date(prCompare2.getEnd_date());
								
			PrivacyReport prev_report = reportSvc.findPrivacylogCntBySystemCompare(report);
			
			
			nCount1=0;
			nCount2=0;
			nCount3=0;
			
			if(report != null)
				nCount1 = Integer.parseInt(report.getCnt());
			if(prev_report != null)
				nCount2 = Integer.parseInt(prev_report.getCnt());
			nCount3 = nCount1 - nCount2;
			
			report.setType1(nCount1); //금월
			report.setType2(nCount2); //이전월	
			
			
			if ( nCount3 < 0 ) report.setbCheckPlus("0");
			else report.setbCheckPlus("1");
			
			if ( nCount3 < 0 ) nCount3 = -(nCount3);
			report.setType3(nCount3); //증감	
			
			list4_1.set(i, report);
		}
		
		modelAndView.addObject("listChart4Detail_1", list4_1);
		//1014<				
		// 소속별 접속기록 현황 Top10 표
		List<PrivacyReport> list = new ArrayList<>();
		List<PrivacyReport> list_1 = new ArrayList<>();
		
		list = reportSvc.findReportDetail_chart5(pr);
		list_1 = reportSvc.findReportDetail_chart5(pr);
		if(list.size() < 1) {
			prCompare.setSysList(syslist);
			prCompare.setStart_date(prCompare.getStart_date().substring(0,6));
			prCompare.setEnd_date(prCompare.getEnd_date().substring(0,6));
			List<PrivacyReport> prevRepVoTempList = reportSvc.findReportDetail_chart5(prCompare);
			if(prevRepVoTempList.size() < 1) {
				prCompare2.setSysList(syslist);
				prCompare2.setStart_date(prCompare2.getStart_date().substring(0,6));
				prCompare2.setEnd_date(prCompare2.getEnd_date().substring(0,6));
				List<PrivacyReport> prevYearRepVoList = reportSvc.findReportDetail_chart5(prCompare2);
				for(int i=0; i<prevYearRepVoList.size(); i++) {
					
					PrivacyReport prevYearReport = prevYearRepVoList.get(i);
					
					nCount1=0;
					nCount2=0;
					nCount3=0;
					nCount4=0;
					nCount5=0;
					
					if(prevYearReport != null) {
						nCount4 = Integer.parseInt(prevYearReport.getCnt());
					}
					nCount3 = nCount1 - nCount2;
					nCount5 = nCount1 - nCount4;
					prevYearReport.setType1(nCount1);
					prevYearReport.setType2(nCount2);
					prevYearReport.setType4(nCount4);
					
					
					if ( nCount3 < 0 ) {
						prevYearReport.setbCheckPlus("0");
					}
					else {
						prevYearReport.setbCheckPlus("1");
					}
					if ( nCount3 < 0 ) {
						nCount3 = -(nCount3);
					}
					if ( nCount5 < 0 ) {
						prevYearReport.setbCheckPlus1("0");
					}
					else {
						prevYearReport.setbCheckPlus1("1");
					}
					if ( nCount5 < 0 ) {
						nCount5 = -(nCount5);
					}
					prevYearReport.setType3(nCount3);
					prevYearReport.setType5(nCount5);
					
					prevYearRepVoList.set(i, prevYearReport);
				}
				modelAndView.addObject("listChart5Detail", prevYearRepVoList);
			} else {
				PrivacyReport report = new PrivacyReport();
				for(int i=0; i<prevRepVoTempList.size(); i++) {
					PrivacyReport prevRepVo = prevRepVoTempList.get(i);
					report.setSysList(syslist);
					report.setStart_date(prCompare2.getStart_date().substring(0,6));
					report.setEnd_date(prCompare2.getEnd_date().substring(0,6));
					PrivacyReport prevYearRepVo = reportSvc.findReportDetail_chart5_detail(report);
					
					nCount1=0;
					nCount2=0;
					nCount3=0;
					nCount4=0;
					nCount5=0;
					
					if(prevRepVo != null) {
						nCount2 = Integer.parseInt(prevRepVo.getCnt());
					}
					if(prevYearRepVo != null) {
						nCount4 = Integer.parseInt(prevYearRepVo.getCnt());
					}
					nCount3 = nCount1 - nCount2;
					nCount5 = nCount1 - nCount4;
					prevRepVo.setType1(nCount1);
					prevRepVo.setType2(nCount2);
					prevRepVo.setType4(nCount4);
					
					 
					if ( nCount3 < 0 ) {
						prevRepVo.setbCheckPlus("0");
					}
					else {
						prevRepVo.setbCheckPlus("1");
					}
					if ( nCount3 < 0 ) {
						nCount3 = -(nCount3);
					}
					if ( nCount5 < 0 ) {
						prevRepVo.setbCheckPlus1("0");
					}
					else {
						prevRepVo.setbCheckPlus1("1");
					}
					if ( nCount5 < 0 ) {
						nCount5 = -(nCount5);
					}
					prevRepVo.setType3(nCount3);
					prevRepVo.setType5(nCount5);
					
					prevRepVoTempList.set(i, prevRepVo);
				}
				modelAndView.addObject("listChart5Detail", prevRepVoTempList);
			}
		} else {
			for(int i=0; i<list.size(); i++) {
				PrivacyReport report = list.get(i);
				report.setSysList(syslist);
				report.setStart_date(prCompare.getStart_date().substring(0,6));
				report.setEnd_date(prCompare.getEnd_date().substring(0,6));
				PrivacyReport prev_report = reportSvc.findReportDetail_chart5_detail(report);
				report.setStart_date(prCompare2.getStart_date().substring(0,6));
				report.setEnd_date(prCompare2.getEnd_date().substring(0,6));
				PrivacyReport prevYear_report = reportSvc.findReportDetail_chart5_detail(report);
				
				nCount1=0;
				nCount2=0;
				nCount3=0;
				nCount4=0;
				nCount5=0;
	
				if(report != null) {
					nCount1 = Integer.parseInt(report.getCnt());
				}
				if(prev_report != null) {
					nCount2 = Integer.parseInt(prev_report.getCnt());
				}
				if(prevYear_report != null) {
					nCount4 = Integer.parseInt(prevYear_report.getCnt());
				}
				nCount3 = nCount1 - nCount2;
				nCount5 = nCount1 - nCount4;
				report.setType1(nCount1);
				report.setType2(nCount2);
				report.setType4(nCount4);
				
				
				if ( nCount3 < 0 ) {
					report.setbCheckPlus("0");
				}
				else {
					report.setbCheckPlus("1");
				}
				if ( nCount3 < 0 ) {
					nCount3 = -(nCount3);
				}
				if ( nCount5 < 0 ) {
					report.setbCheckPlus1("0");
				}
				else {
					report.setbCheckPlus1("1");
				}
				if ( nCount5 < 0 ) {
					nCount5 = -(nCount5);
				}
				report.setType3(nCount3);
				report.setType5(nCount5);
				
				list.set(i, report);
			}
			modelAndView.addObject("listChart5Detail", list);
		}
		
		//1014
		for(int i=0; i<list_1.size(); i++) {
			
			PrivacyReport report = list_1.get(i);
			report.setSysList(syslist);
			
			report.setStart_date(prCompare2.getStart_date());
			report.setEnd_date(prCompare2.getEnd_date());	
			PrivacyReport prev_report = reportSvc.findReportDetail_chart5_detail(report);
			
			nCount1=0;
			nCount2=0;
			nCount3=0;
			
			if(report != null) {
				nCount1 = Integer.parseInt(report.getCnt());
			}
			if(prev_report != null) {
				nCount2 = Integer.parseInt(prev_report.getCnt());
			}
			nCount3 = nCount1 - nCount2;
			
			report.setType1(nCount1); //금월
			report.setType2(nCount2); //전년전월	
			
			
			if ( nCount3 < 0 ) report.setbCheckPlus("0");
			else report.setbCheckPlus("1");
			
			if ( nCount3 < 0 ) nCount3 = -(nCount3);
			report.setType3(nCount3); //증감
			
			list_1.set(i, report);
		}
		
		modelAndView.addObject("listChart5Detail_1", list_1);

		modelAndView.addObject("privTypeCount", privTypeCount);

		if (privTypeCount.size() > 0) {
			privTypeCountAvg = privTypeCountAvg / privTypeCount.size();
		}
		
		modelAndView.addObject("privTypeCountAvg", privTypeCountAvg);
		
		if(ui_type.equals("G")) {
			modelAndView.setViewName("report_new1_gj");
		} else {
			modelAndView.setViewName("report_new1");
		}
	    modelAndView.addObject("reportType", type);

		modelAndView.addObject("download_type", download_type);
		String pdfFullPath = request.getRequestURL() +"?"+ request.getQueryString();
		modelAndView.addObject("pdfFullPath", pdfFullPath);
		modelAndView.addObject("ui_type", ui_type);
		
		return modelAndView;
	}
	
	/**
	 * @page : reportIdenUniq.html
	 * @desc : 고유식별정보 처리 현황 보고(type_4)
	 * @auth : syjung
	 */
	@RequestMapping(value="reportIdenUniq.html")
	public ModelAndView findReportIdenUniq(HttpServletRequest request,
			@RequestParam(value = "type", required = false) int type,
			@RequestParam(value = "period_type", required = false) int period_type,
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date,
			@RequestParam(value = "menu_id", required = false) String menu_id,
			@RequestParam(value = "download_type", required = false) String download_type) {


		ModelAndView modelAndView = new ModelAndView();
		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
	    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
		String date = dayTime.format(new Date(time));
		String ui_type = commonDao.getUiType();
		modelAndView.addObject("ui_type", ui_type);
		
		// 마지막날짜가 현재보다 클 경우 어제날짜까지로 지정해준다.
		/*
		try {
			Date tmpDate = dayTime.parse(end_date);
			int compare = tmpDate.compareTo(new Date());
			if( compare >= 0) {
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DAY_OF_MONTH, -1);
				end_date = dayTime.format(cal.getTime());
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		*/
		String compareDate = "";
		
		PrivacyReport pr = new PrivacyReport();			//선택 기간
		PrivacyReport prCompare = new PrivacyReport();	//비교 기간(동년전월)
		PrivacyReport prCompare2 = new PrivacyReport();	//비교 기간(전년동월)
		
		
		HttpSession session = request.getSession();
	    String master = (String)session.getAttribute("master");
	    
	    String use_reportLogo = (String)session.getAttribute("use_reportLogo");
	    modelAndView.addObject("use_reportLogo", use_reportLogo);
	    if (use_reportLogo != null && use_reportLogo.equals("Y")) {
		    String rootPath = request.getSession().getServletContext().getRealPath("/");
			String attach_path = "resources/upload/";
			
			String filename = commonDao.selectCurrentImage_logo2();
			String savePath = rootPath + attach_path + filename;
			
			File file = new File(savePath);
			
			if(file.exists()) {
				modelAndView.addObject("filename", filename);
			}
	    }
	    
	    String use_reportLine = (String)session.getAttribute("use_reportLine");
	    modelAndView.addObject("use_reportLine", use_reportLine);
		modelAndView.addObject("masterflag", master);
		modelAndView.addObject("report_type", type);
		modelAndView.addObject("period_type", period_type);
		modelAndView.addObject("start_date", start_date);
		modelAndView.addObject("end_date", end_date);
		modelAndView.addObject("date", date);
		modelAndView.addObject("system_seq", system_seq);
		modelAndView.addObject("use_studentId", use_studentId);
		modelAndView.addObject("logo_report_url", logo_report_url);
		modelAndView.addObject("menu_id", menu_id);

		List<String> syslist = new ArrayList<String>();
		

		String tmpList[] = system_seq.split(",");
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}
		syslist.add("");
		
		String sDate[] = start_date.split("-");
		String eDate[] = end_date.split("-");

		int strYear = Integer.parseInt(sDate[0]);
		int strMonth = Integer.parseInt(sDate[1]);
		int endYear = Integer.parseInt(eDate[0]);
		int endMonth = Integer.parseInt(eDate[1]);

		int diffMon = (endYear - strYear)* 12 + (endMonth - strMonth);

		String stDate = start_date.replaceAll("-", "").substring(0,6);
		String edDate = end_date.replaceAll("-", "").substring(0,6);
		
		pr.setStart_date(stDate);
		pr.setEnd_date(edDate);
		pr.setSysList(syslist);
		
		if(!pr.getSysList().isEmpty()) {
			List<SystemMaster> systems = reportSvc.findSystemMasterDetailList(pr);
			for (SystemMaster sm : systems) {
				PrivacyReport prt = new PrivacyReport();
				prt.setSystem_seq(sm.getSystem_seq());
				prt.setStart_date(stDate);
				prt.setEnd_date(edDate);
				List<PrivacyReport> res = reportDao.getMajorResultType(prt);
				String result_type = "";
				if (res.size() > 0) {
					for (PrivacyReport data : res) {
						String text = data.getPrivacy_desc()  + "(" + data.getCnt() + ") ";
						result_type += text;
					}
				} else {
					result_type = "데이터없음";
				}
				sm.setResult_type(result_type);
			}
			modelAndView.addObject("systems", systems);
		}
	    
	    Calendar cal = Calendar.getInstance();
	    cal.set(strYear, strMonth-1, 1);		//선택기간 시작월 1일
	    
	    cal.add(Calendar.MONTH, -1);    		// 비교 기간 끝(선택기간 한달전)
	    compareDate = dateFormatter.format(cal.getTime());
	    edDate = compareDate.substring(0,6);

		cal.add(Calendar.MONTH, -diffMon);    	// 비교 기간 시작
		compareDate = dateFormatter.format(cal.getTime());
		stDate = compareDate.substring(0,6);

		prCompare.setStart_date(stDate);
		prCompare.setEnd_date(edDate);
		prCompare.setSysList(syslist);
		
		cal.set(strYear, strMonth-1, 1);
		cal.add(Calendar.YEAR, -1);    		// 비교 기간 끝(선택기간 한달전)
	    compareDate = dateFormatter.format(cal.getTime());
		edDate = compareDate.substring(0,6)+"31";

		cal.add(Calendar.YEAR, -diffMon);    	// 비교 기간 시작
		compareDate = dateFormatter.format(cal.getTime());
		stDate = compareDate.substring(0,6)+"01";
		
		prCompare2.setStart_date(stDate);
		prCompare2.setEnd_date(edDate);
		prCompare2.setSysList(syslist);
		// 유형별 고유식별정보 처리 현황
		int totCnt = 0;
		List<PrivacyReport> typeCnt = reportSvc.findTypeReportCount(pr);
		if(typeCnt.size()>0){
			modelAndView.addObject("topType", typeCnt.get(0).getPrivacy_desc());
			for(int i=0; i<typeCnt.size(); i++){
				totCnt = totCnt + Integer.parseInt(typeCnt.get(i).getCnt());
			}
			modelAndView.addObject("totCnt", totCnt);
			modelAndView.addObject("typeCnt", typeCnt);
		}
		//시스템별 고유식별정보 처리 결과
		List<PrivacyReport> systemCnt = reportSvc.findSystemReportCount(pr);
		if(systemCnt.size()>0) {
			modelAndView.addObject("topSystem", systemCnt.get(0).getSystem_name());
			modelAndView.addObject("systemCnt", systemCnt);
		}
		//소속사별 고유식별정보 처리 결과
		List<PrivacyReport> deptCnt = reportSvc.findDeptReportCount(pr);
		if(deptCnt.size()>0) {
			modelAndView.addObject("topDept", deptCnt.get(0).getDept_name());
			modelAndView.addObject("deptCnt", deptCnt);
		}
		//개인별 고유식별정보 처리 결과
		List<PrivacyReport> indvCnt = reportSvc.findIndvReportCount(pr);
		if(indvCnt.size()>0) {
			modelAndView.addObject("topIndv", indvCnt.get(0).getEmp_user_name());
			modelAndView.addObject("indvCnt", indvCnt);
		}

		if(ui_type.equals("G")) {
			modelAndView.setViewName("report_new4_gj");
		} else {
			modelAndView.setViewName("report_new4");
		}
			
		modelAndView.addObject("download_type", download_type);
		String pdfFullPath = request.getRequestURL() +"?"+ request.getQueryString();
		modelAndView.addObject("pdfFullPath", pdfFullPath);
		modelAndView.addObject("ui_type", ui_type);
		return modelAndView;
	}
	
	/**
	 * @page : reportHalfDate.html
	 * @desc : 반기별_수준진단보고(type_5)
	 * @auth : syjung
	 */
	@RequestMapping(value = "reportHalfDate.html")
	public ModelAndView findReportHalfDate(HttpServletRequest request,
			@RequestParam(value = "type", required = false) int type,
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date,
			@RequestParam(value = "half_type", required = false) String half_type,
			@RequestParam(value = "download_type", required = false) String download_type) {
		
		ModelAndView modelAndView = new ModelAndView();
		String ui_type = commonDao.getUiType();
		String use_reportLogo = (String)request.getSession().getAttribute("use_reportLogo");
	    if (use_reportLogo != null && use_reportLogo.equals("Y")) {
			String rootPath = request.getSession().getServletContext().getRealPath("/");
			String attach_path = "resources/upload/";
			String filename = commonDao.selectCurrentImage_logo2();
			String savePath = rootPath + attach_path + filename;
			File file = new File(savePath);
			if(file.exists()) {
				modelAndView.addObject("filename", filename);
			}
	    }
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
		String date = dateFormatter.format(new Date());
		// 마지막날짜가 현재보다 클 경우 어제날짜까지로 지정해준다.
		/*
		try {
			Date tmpDate = dateFormatter.parse(end_date);
			int compare = tmpDate.compareTo(new Date());
			if( compare >= 0) {
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DAY_OF_MONTH, -1);
				end_date = dateFormatter.format(cal.getTime());
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		*/
		modelAndView.addObject("use_reportLogo", use_reportLogo);
		modelAndView.addObject("start_date", start_date);
		modelAndView.addObject("end_date", end_date);
		modelAndView.addObject("date", date);
		modelAndView.addObject("system_seq", system_seq);
		modelAndView.addObject("logo_report_url", logo_report_url);
		modelAndView.addObject("half_type", half_type);
		
		PrivacyReport pr = new PrivacyReport();
		String stDate = start_date.replaceAll("-", "");
		String edDate = end_date.replaceAll("-", "");
		pr.setStart_date(stDate);
		pr.setEnd_date(edDate);
		pr.setHalf_type(half_type);
		
		int diffMon = getDiffMonth(pr);
		modelAndView.addObject("diffMon", diffMon);
		
		List<String> syslist = new ArrayList<String>();
		String tmpList[] = system_seq.split(",");
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}
		pr.setSysList(syslist);
		
		// 점검대상
		List<SystemMaster> systems = reportSvc.findSystemMasterDetailList(pr);
		modelAndView.addObject("systems", systems);
		
		// 개인정보 접속기록 건수
		int logCnt = reportSvc.findPrivacyTotalLogCnt(pr);
		modelAndView.addObject("logCnt", logCnt);
		
		// 최다 개인정보 처리시스템
		PrivacyReport findPrivacyReportBySystemTop1 = reportSvc.findPrivacyReportBySystemTop1(pr);
		if(findPrivacyReportBySystemTop1 != null)
			modelAndView.addObject("reportBySys", findPrivacyReportBySystemTop1.getSystem_name());
		
		// 부서별 개인정보 월별 현황
		List<PrivacyReport> table5 = reportSvc.findReportHalf_table5(pr);
		modelAndView.addObject("table5", table5);
		if(table5 != null && table5.size() > 0) {
			String max_dept = table5.get(0).getDept_name();
			modelAndView.addObject("max_dept", max_dept);
		}
		
		if(ui_type.equals("G")) {
			modelAndView.setViewName("report_half_gj");
		} else {
			modelAndView.setViewName("report_half");
		}

		modelAndView.addObject("report_type", type);
		modelAndView.addObject("download_type", download_type);
		String pdfFullPath = request.getRequestURL() +"?"+ request.getQueryString();
		modelAndView.addObject("pdfFullPath", pdfFullPath);
		
		//table3 데이터 주기
		if(!pr.getSysList().isEmpty()) {
			List<SystemMaster> systemsLst = reportSvc.findSystemMasterDetailList(pr);
			for (SystemMaster sm : systemsLst) {
				PrivacyReport prt = new PrivacyReport();
				prt.setSystem_seq(sm.getSystem_seq());
				prt.setStart_date(stDate);
				prt.setEnd_date(edDate);
				List<PrivacyReport> res = reportDao.getMajorResultType(prt);
				String result_type = "";
				if (res.size() > 0) {
					for (PrivacyReport data : res) {
						String text = data.getPrivacy_desc()  + "(" + data.getCnt() + ") ";
						result_type += text;
					}
				} else {
					result_type = "데이터없음";
				}
				sm.setResult_type(result_type);
			}
			modelAndView.addObject("systemsLst", systemsLst);
		}
		
		//table4에 데이터 주기
		List<PrivacyReport> table4Lst = reportSvc.findReportHalf_chart4(pr);
		modelAndView.addObject("table4Lst", table4Lst);
		
		//table6에 데이터 주기
		PrivacyReport prVo = this.reportHalf_chart6(system_seq, start_date, end_date, half_type);
		if(prVo != null) {
			int cnt1 = prVo.getType1();
			int cnt2 = prVo.getType2();
			int diffCnt = cnt1 - cnt2;
			if(diffCnt < 0) {
				prVo.setbCheckPlus("0");
				prVo.setType3(-(diffCnt));
			} else if(diffCnt == 0) {
				prVo.setbCheckPlus("-");
				prVo.setType3(0);
			} else { 
				prVo.setbCheckPlus("1");
				prVo.setType3(diffCnt);
			}
			modelAndView.addObject("prVo", prVo);
		} else {
			PrivacyReport prVo2 = new PrivacyReport();
			int cnt1 = 0;
			int cnt2 = 0;
			prVo2.setType1(0);
			prVo2.setType2(0);
			int diffCnt = cnt1 - cnt2;
			if(diffCnt < 0) {
				prVo2.setbCheckPlus("0");
				prVo2.setType3(-(diffCnt));
			} else if(diffCnt == 0) {
				prVo2.setbCheckPlus("-");
				prVo2.setType3(0);
			} else { 
				prVo2.setbCheckPlus("1");
				prVo2.setType3(diffCnt);
			}
			modelAndView.addObject("prVo", prVo2);
		}
		//table1에 데이터 추가
		List<PrivacyReport> reportBySysVoLst = reportSvc.findPrivacyReportBySystem(pr);
		int totSystemCnt = 0;
		for (int i = 0; i < reportBySysVoLst.size(); i++) {
			PrivacyReport reportTemp = reportBySysVoLst.get(i);
			int tmpCnt = Integer.parseInt(reportTemp.getCnt());
			totSystemCnt += tmpCnt;
		}
		modelAndView.addObject("reportBySysVoLst", reportBySysVoLst);
		modelAndView.addObject("totSystemCnt", totSystemCnt);
		return modelAndView;
	}
	
	@RequestMapping(value = "reportDetail_access.html")
	public ModelAndView findReportDetail_access(HttpServletRequest request,
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date,
			@RequestParam(value = "accessDateType", required = false) String accessDateType) {
		
		ModelAndView modelAndView = new ModelAndView();
		
		String use_reportLogo = (String)request.getSession().getAttribute("use_reportLogo");
	    modelAndView.addObject("use_reportLogo", use_reportLogo);
	    if (use_reportLogo != null && use_reportLogo.equals("Y")) {
			String rootPath = request.getSession().getServletContext().getRealPath("/");
			String attach_path = "resources/upload/";
			
			String filename = commonDao.selectCurrentImage_logo2();
			String savePath = rootPath + attach_path + filename;
			
			File file = new File(savePath);
			
			if(file.exists()) {
				modelAndView.addObject("filename", filename);
			}
	    }
	    
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
		String date = dateFormatter.format(new Date());
		
		modelAndView.addObject("start_date", start_date);
		modelAndView.addObject("end_date", end_date);
		modelAndView.addObject("date", date);
		modelAndView.addObject("system_seq", system_seq);
		modelAndView.addObject("accessDateType", accessDateType);
		modelAndView.addObject("logo_report_url", logo_report_url);
		
		PrivacyReport pr = new PrivacyReport();
		String stDate = start_date.replaceAll("-", "");
		String edDate = end_date.replaceAll("-", "");
		pr.setStart_date(stDate);
		pr.setEnd_date(edDate);
		
		List<String> syslist = new ArrayList<String>();
		String tmpList[] = system_seq.split(",");
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}
		pr.setSysList(syslist);
		
		// 점검대상
		List<SystemMaster> systems = reportSvc.findSystemMasterDetailList(pr);
		modelAndView.addObject("systems", systems);
		
		AccessAuthSearch search = new AccessAuthSearch();
		search.setSearch_from(stDate);
		search.setSearch_to(edDate);
		search.setSystem_seq(system_seq);
		search.setUseExcel("true");
		search.setAccessDateType(accessDateType);
		List<AccessAuth> empUserList = accessAuthDao.findAccessAuthList(search);
		modelAndView.addObject("empUserList", empUserList);
		
		modelAndView.setViewName("report_access");
		
		return modelAndView;
	}
	
	@RequestMapping(value = "reportDetail_allogInq_access.html")
	public ModelAndView findReportDetail_allogInq_access(HttpServletRequest request,
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {
		
		ModelAndView modelAndView = new ModelAndView();
		
		String use_reportLogo = (String)request.getSession().getAttribute("use_reportLogo");
	    modelAndView.addObject("use_reportLogo", use_reportLogo);
	    if (use_reportLogo != null && use_reportLogo.equals("Y")) {
			String rootPath = request.getSession().getServletContext().getRealPath("/");
			String attach_path = "resources/upload/";
			
			String filename = commonDao.selectCurrentImage_logo2();
			String savePath = rootPath + attach_path + filename;
			
			File file = new File(savePath);
			
			if(file.exists()) {
				modelAndView.addObject("filename", filename);
			}
	    }
	    
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
		String date = dateFormatter.format(new Date());
		
		modelAndView.addObject("start_date", start_date);
		modelAndView.addObject("end_date", end_date);
		modelAndView.addObject("date", date);
		modelAndView.addObject("system_seq", system_seq);
//		modelAndView.addObject("accessDateType", accessDateType);
		modelAndView.addObject("logo_report_url", logo_report_url);
		
		PrivacyReport pr = new PrivacyReport();
		String stDate = start_date.replaceAll("-", "");
		String edDate = end_date.replaceAll("-", "");
		pr.setStart_date(stDate);
		pr.setEnd_date(edDate);
		
		List<String> syslist = new ArrayList<String>();
		List<String> syslist_tmp = new ArrayList<String>(syslist);
		String tmpList[] = system_seq.split(",");
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}
		pr.setSysList(syslist);
		
		// 점검대상
		List<SystemMaster> systems = reportSvc.findSystemMasterDetailList(pr);
		//modelAndView.addObject("systems", systems);
		
		pr.setSearch_from(stDate);
		pr.setSearch_to(edDate);
		
		HttpSession session = request.getSession();
		// 개인정보유형 뷰
		String result_type_view = (String) session.getAttribute("result_type");
		modelAndView.addObject("result_type_view", result_type_view);
		
		List<Map<String, Object>> resList = new ArrayList<>();
		
		for (SystemMaster system : systems) {
			syslist.clear();
			syslist.add(system.getSystem_seq());
			pr.setSysList(syslist);
			List<AllLogInq> allLogInq = reportSvc.findAllLogInqList(pr);
			
			Map<String, Object> map = new HashMap<>();
			map.put("system_name", system.getSystem_name());
			map.put("allLogInq", allLogInq);
			resList.add(map);
		}
		modelAndView.addObject("resList", resList);
		/*List<AllLogInq> allLogInq = reportSvc.findAllLogInqList(pr);
		modelAndView.addObject("allLogInq", allLogInq);*/
		
		AccessAuthSearch search = new AccessAuthSearch();
		search.setSearch_from(stDate);
		search.setSearch_to(edDate);
		search.setSystem_seq(system_seq);
		search.setUseExcel("true");
//		search.setAccessDateType(accessDateType);
		search.setSysList(syslist_tmp);
		List<AccessAuth> empUserList = reportSvc.findAccessAuthList(search);
		modelAndView.addObject("empUserList", empUserList);
		
		modelAndView.setViewName("report_allLogInq_access");
		
		return modelAndView;
	}
	
	@RequestMapping(value = "reportDetail_moef.html")
	public ModelAndView findReportDetail_moef(HttpServletRequest request,
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {
		
		ModelAndView modelAndView = new ModelAndView();
		
		String use_reportLogo = (String)request.getSession().getAttribute("use_reportLogo");
	    modelAndView.addObject("use_reportLogo", use_reportLogo);
	    if (use_reportLogo != null && use_reportLogo.equals("Y")) {
			String rootPath = request.getSession().getServletContext().getRealPath("/");
			String attach_path = "resources/upload/";
			
			String filename = commonDao.selectCurrentImage_logo2();
			String savePath = rootPath + attach_path + filename;
			
			File file = new File(savePath);
			
			if(file.exists()) {
				modelAndView.addObject("filename", filename);
			}
	    }
	    
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
		String date = dateFormatter.format(new Date());
		
		modelAndView.addObject("start_date", start_date);
		modelAndView.addObject("end_date", end_date);
		modelAndView.addObject("date", date);
		modelAndView.addObject("system_seq", system_seq);
		modelAndView.addObject("logo_report_url", logo_report_url);
		
		PrivacyReport pr = new PrivacyReport();
		String stDate = start_date.replaceAll("-", "");
		String edDate = end_date.replaceAll("-", "");
		pr.setStart_date(stDate);
		pr.setEnd_date(edDate);
		
		// 사용자
		pr.setType1(1);
		pr.setReq_type("RD");
		List<PrivacyReport> top10Select = reportDao.getLogTop10ByReqType(pr);
		List<PrivacyReport> top10Select_res = reportSvc.getLogByReqType(top10Select);
		modelAndView.addObject("top10Select", top10Select_res);
		pr.setReq_type("CR");
		List<PrivacyReport> top10Create = reportDao.getLogTop10ByReqType(pr);
		List<PrivacyReport> top10Create_res = reportSvc.getLogByReqType(top10Create);
		modelAndView.addObject("top10Create", top10Create_res);
		pr.setReq_type("UD");
		List<PrivacyReport> top10Update = reportDao.getLogTop10ByReqType(pr);
		List<PrivacyReport> top10Update_res = reportSvc.getLogByReqType(top10Update);
		modelAndView.addObject("top10Update", top10Update_res);
		pr.setReq_type("DL");
		List<PrivacyReport> top10Delete = reportDao.getLogTop10ByReqType(pr);
		List<PrivacyReport> top10Delete_res = reportSvc.getLogByReqType(top10Delete);
		modelAndView.addObject("top10Delete", top10Delete_res);
		pr.setReq_type("PR");
		List<PrivacyReport> top10Print = reportDao.getLogTop10ByReqType(pr);
		List<PrivacyReport> top10Print_res = reportSvc.getLogByReqType(top10Print);
		modelAndView.addObject("top10Print", top10Print_res);
		pr.setReq_type("DN");
		List<PrivacyReport> top10Download = reportDao.getLogTop10ByReqType(pr);
		List<PrivacyReport> top10Download_res = reportSvc.getLogByReqType(top10Download);
		modelAndView.addObject("top10Download", top10Download_res);
		
		// 운영자
		pr.setType1(2);
		pr.setReq_type("RD");
		top10Select = reportDao.getLogTop10ByReqType(pr);
		top10Select_res = reportSvc.getLogByReqType(top10Select);
		modelAndView.addObject("top10Select2", top10Select_res);
		pr.setReq_type("CR");
		top10Create = reportDao.getLogTop10ByReqType(pr);
		top10Create_res = reportSvc.getLogByReqType(top10Create);
		modelAndView.addObject("top10Create2", top10Create_res);
		pr.setReq_type("UD");
		top10Update = reportDao.getLogTop10ByReqType(pr);
		top10Update_res = reportSvc.getLogByReqType(top10Update);
		modelAndView.addObject("top10Update2", top10Update_res);
		pr.setReq_type("DL");
		top10Delete = reportDao.getLogTop10ByReqType(pr);
		top10Delete_res = reportSvc.getLogByReqType(top10Delete);
		modelAndView.addObject("top10Delete2", top10Delete_res);
		pr.setReq_type("PR");
		top10Print = reportDao.getLogTop10ByReqType(pr);
		top10Print_res = reportSvc.getLogByReqType(top10Print);
		modelAndView.addObject("top10Print2", top10Print_res);
		pr.setReq_type("DN");
		top10Download = reportDao.getLogTop10ByReqType(pr);
		top10Download_res = reportSvc.getLogByReqType(top10Download);
		modelAndView.addObject("top10Download2", top10Download_res);
		
		modelAndView.setViewName("report_moef");
		
		return modelAndView;
	}

	
	@RequestMapping(value = "reportDetail_chart1_1.html")
	@ResponseBody
	public JSONArray reportDetail_chart1_1(
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {

		JSONArray jArray = new JSONArray();

		String tmpList[] = system_seq.split(",");
		
		List<String> syslist = new ArrayList<String>();
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", ""));
		pr.setEnd_date(end_date.replaceAll("-", ""));
		pr.setSysList(syslist);
		List<PrivacyReport> reportBySys = reportSvc.findPrivacyReportBySystem(pr);
//		int totSystemCnt = 0;
		for (int i = 0; i < reportBySys.size(); i++) {
			JSONObject jsonObj = new JSONObject();
			PrivacyReport report = reportBySys.get(i);
			int tmpCnt = Integer.parseInt(report.getCnt());
//			totSystemCnt += tmpCnt;
			jsonObj.put("system_seq", report.getSystem_seq());
			jsonObj.put("system_name", report.getSystem_name());
			jsonObj.put("cnt", tmpCnt);
			jArray.add(jsonObj);
		}

		return jArray;
	}

	@RequestMapping(value = "reportDetail_chart1_2.html")
	@ResponseBody
	public JSONArray reportDetail_chart1_2(@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {

		JSONArray jArray = new JSONArray();

		String tmpList[] = system_seq.split(",");
		
		List<String> syslist = new ArrayList<String>();
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", ""));
		pr.setEnd_date(end_date.replaceAll("-", ""));
		pr.setSysList(syslist);
		List<PrivacyReport> reportByDept = new ArrayList<>();
		if (use_studentId.length() > 0 && use_studentId.equals("yes")) {
			reportByDept = reportSvc.findPrivacyReportByDeptUseStudentId(pr);
		} else {
			reportByDept = reportSvc.findPrivacyReportByDept(pr);
		}
				

		for (int i = 0; i < reportByDept.size(); i++) {
			JSONObject jsonObj = new JSONObject();
			PrivacyReport report = reportByDept.get(i);
			jsonObj.put("dept_name", report.getDept_name());
			jsonObj.put("cnt", report.getCnt());
			jArray.add(jsonObj);
		}

		return jArray;
	}

	@RequestMapping(value = "reportDetail_chart2.html")
	@ResponseBody
	public JSONArray reportDetail_chart2(
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {
		
		JSONArray jArray = new JSONArray();

		String tmpList[] = system_seq.split(",");
		
		List<String> syslist = new ArrayList<String>();
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}
		syslist.add("");

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", "").substring(0, 6));
		pr.setEnd_date(end_date.replaceAll("-", "").substring(0, 6));
		pr.setSysList(syslist);
			
		String strYear = pr.getEnd_date().substring(0,4);
		String strMonth= pr.getEnd_date().substring(4,6);
		//pr.setMonth(pr.getEnd_date());

		List<PrivacyReport> privTypeCount;

		if(start_date.substring(0,7).equals(end_date.substring(0,7))){
	
			privTypeCount = reportSvc.findPrivTypeCountWeek(pr);
			
			for (PrivacyReport report : privTypeCount) {
				JSONObject jsonObj = new JSONObject();
	
				jsonObj.put("proc_date", strYear+"년 "+strMonth+"월 "+ report.getWeek()+"주차");
				jsonObj.put("week", report.getWeek());
				jsonObj.put("cnt", report.getCnt());
				jArray.add(jsonObj);
			}
		}else{
			
			privTypeCount = reportSvc.findPrivacyReportDetailChart3_1(pr);
			
			for (int i = 0; i < privTypeCount.size(); i++) {
				JSONObject jsonObj = new JSONObject();
				PrivacyReport report = privTypeCount.get(i);
	
				jsonObj.put("proc_date", report.getMonth().substring(0,4)+"년 "+report.getMonth().substring(4,6)+"월");
				jsonObj.put("cnt", report.getCnt());
				jArray.add(jsonObj);
			}
		}
		
		return jArray;
	}

	@RequestMapping(value = "reportDetail_chart3_1.html")
	@ResponseBody
	public JSONArray reportDetail_chart3_1(@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {

		JSONArray jArray = new JSONArray();

		String tmpList[] = system_seq.split(",");
		
		List<String> syslist = new ArrayList<String>();
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", "").substring(0,6));
		pr.setEnd_date(end_date.replaceAll("-", "").substring(0,6));
		pr.setSysList(syslist);
		
		SimpleDateFormat format1 = new SimpleDateFormat("yyyyMM");
		SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM");
		
		List<PrivacyReport> reportByDept = reportSvc.findPrivacyReportDetailChart3_1(pr);

		try {
			for (int i = 0; i < reportByDept.size(); i++) {
				JSONObject jsonObj = new JSONObject();
				PrivacyReport report = reportByDept.get(i);
				
				String proc_date = report.getMonth();
				if (proc_date == null || proc_date.length() == 0)
					continue;
	
				Date date;
				date = format1.parse(proc_date);
				proc_date = format2.format(date);
				
				jsonObj.put("proc_date", proc_date);
				jsonObj.put("cnt", report.getCnt());
				jArray.add(jsonObj);
			}
		}catch (ParseException e) {
				e.printStackTrace();
		}

		return jArray;
	}

	@RequestMapping(value = "reportDetail_chart3_2.html")
	@ResponseBody
	public JSONArray reportDetail_chart3_2(
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {

		JSONArray jArray = new JSONArray();

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", ""));
		pr.setEnd_date(end_date.replaceAll("-", ""));

		return jArray;
	}

	@RequestMapping(value = "reportDetail_chart3_3.html")
	@ResponseBody
	public JSONArray reportDetail_chart3_3(
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {

		JSONArray jArray = new JSONArray();

		String tmpList[] = system_seq.split(",");
		
		List<String> syslist = new ArrayList<String>();
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", "").substring(0,6));
		pr.setEnd_date(end_date.replaceAll("-", "").substring(0,6));
		pr.setSysList(syslist);
		
//		List<PrivacyReport> reportByDept = reportSvc.findPrivacyReportDetailChart3_1(pr);
		List<PrivacyReport> reportByDeptAvgTime = reportSvc.findPrivacyReportDetailChart3_3(pr);
/*		
		long time = System.currentTimeMillis(); 
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyyMM");
		String str = dayTime.format(new Date(time));

		pr.setStart_date(str);
		pr.setEnd_date(str);
		
		List<PrivacyReport> reportByNowTime = reportSvc.findPrivacyReportDetailChart3_3(pr);
*/		
		//List<PrivacyReport> reportByWeekdayAvgTime = reportSvc.findPrivacyReportDetailChart3_4(pr);
		
		HashMap<String,String> tmpMapAvg = new HashMap<String,String>();
		
		for ( int i=0; i<reportByDeptAvgTime.size(); ++i ) {
			tmpMapAvg.put(reportByDeptAvgTime.get(i).getProc_time(), reportByDeptAvgTime.get(i).getCnt());
		}
		
//		HashMap<String,String> tmpMapNow = new HashMap<String,String>();
		HashMap<String,String> tmpMapWeekday = new HashMap<String,String>();
/*		
		for ( int i=0; i<reportByNowTime.size(); ++i ) {
			tmpMapNow.put(reportByNowTime.get(i).getProc_time(), reportByNowTime.get(i).getCnt());
		}
*/
		/*for ( int i=0; i<reportByWeekdayAvgTime.size(); ++i ) {
			tmpMapWeekday.put(reportByWeekdayAvgTime.get(i).getProc_time(), reportByWeekdayAvgTime.get(i).getCnt());
		}*/
		
		String sDate[] = start_date.split("-");
		String eDate[] = end_date.split("-");

		int strYear = Integer.parseInt(sDate[0]);
		int strMonth = Integer.parseInt(sDate[1]);
		int endYear = Integer.parseInt(eDate[0]);
		int endMonth = Integer.parseInt(eDate[1]);

		int diffMon = (endYear - strYear)* 12 + (endMonth - strMonth);
		int totDays = 0;
		int totWeekdays = 0;

		Calendar cal = Calendar.getInstance();
		cal.set(strYear, strMonth-1, 1);

		for(int i=0; i<=diffMon; i++){
			int maxDays = cal.getActualMaximum(Calendar.DATE);
			totDays = totDays + maxDays;
			for (int j=1; j<=maxDays; j++) {
				cal.set(Calendar.DATE, j);
				if(cal.get(Calendar.DAY_OF_WEEK) != 1 && cal.get(Calendar.DAY_OF_WEEK) != 7){
					totWeekdays++;
				}
			}
			
			cal.add(Calendar.MONTH, 1);
		}
		
		for ( int i=0; i<24; ++i ) {
			String strKey = String.format("%02d", i);
			String tmpMapAvgCnt = tmpMapAvg.get(strKey);
			String tmpMapWeekdayCnt = tmpMapWeekday.get(strKey);
//			String tmpMapNowCnt = tmpMapNow.get(strKey);
			
			if ( tmpMapAvgCnt == null ) tmpMapAvgCnt="0";
			else tmpMapAvgCnt=Integer.toString(Integer.parseInt(tmpMapAvgCnt)/totDays);
//			else tmpMapAvgCnt=Integer.toString(Integer.parseInt(tmpMapAvgCnt)/reportByDept.size());
			
//			if ( tmpMapNowCnt == null ) tmpMapNowCnt="0";
//			else tmpMapNowCnt=Integer.toString(Integer.parseInt(tmpMapNowCnt)/reportByDept.size());
			if ( tmpMapWeekdayCnt == null ) tmpMapWeekdayCnt="0";
			else tmpMapWeekdayCnt=Integer.toString(Integer.parseInt(tmpMapWeekdayCnt)/totWeekdays);
//			else tmpMapWeekdayCnt=Integer.toString(Integer.parseInt(tmpMapWeekdayCnt)/reportByDept.size());
			
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("proc_date", strKey+"시");
			jsonObj.put("proc_time", strKey);
			jsonObj.put("cnt1", Integer.parseInt(tmpMapAvgCnt));
			//jsonObj.put("cnt2", Integer.parseInt(tmpMapWeekdayCnt));
//			jsonObj.put("cnt2", Integer.parseInt(tmpMapNowCnt));
			
			jArray.add(jsonObj);
			
		}

		return jArray;
	}
	
	/**
	 * @auth : syjung
	 * @date : 2020.02.24
	 * @desc : char4 draw
	 * @return Map
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="reportDetail_chart4.html")
	@ResponseBody
	public Map reportDetail_chart4(
			@RequestParam(value = "period_type", required = false) int period_type,
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date",required=false) String start_date,
			@RequestParam(value = "end_date",required=false) String end_date) {
		
		String tmpList[] = system_seq.split(",");
		List<String> syslist = new ArrayList<String>();
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}

		DateUtil dateUtil = new DateUtil();
		PrivacyReport pr = dateUtil.getYyyyMmDatePriReportVO(start_date, end_date);
		pr.setSysList(syslist);
		PrivacyReport prCompare = dateUtil.getYyyyMmMonthPriReportVO(start_date, end_date);
		prCompare.setSysList(syslist);
		
		JSONArray jArray = new JSONArray();
		
		List<PrivacyReport> list = reportSvc.findPrivacylogCntBySystem(pr);
		
		for(int i=0; i<list.size(); i++) {
			PrivacyReport report = list.get(i);
			int nCount1 = 0;
			int nCount2 = 0;
			report.setStart_date(prCompare.getStart_date());
			report.setEnd_date(prCompare.getEnd_date());	
			PrivacyReport prevReport = reportSvc.findPrivacylogCntBySystemCompare(report);
			
			if(report != null) {
				nCount1 = Integer.parseInt(report.getCnt());
			}
			if(prevReport != null) {
				nCount2 = Integer.parseInt(prevReport.getCnt());
			}
			
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("dept_name", report.getSystem_name());
			jsonObj.put("system_seq", report.getSystem_seq());
			jsonObj.put("type0", nCount1);
			jsonObj.put("type1", nCount2);
			jArray.add(jsonObj);
		}
		Map res = new HashMap();
		res.put("arrData", jArray);
		
		String title1 = "";
		String title2 = "";
		int strMonth = Integer.parseInt(start_date.split("-")[1]);
		
		if(period_type == 1) {
			title1 = strMonth+"월";
			title2 = "이전월";
		} else if(period_type == 2) {
			if(strMonth==1){
				title1 = "1분기";
			}else if(strMonth == 4) {
				title1 = "2분기";
			}else if(strMonth == 7) {
				title1 = "3분기";
			}else if(strMonth == 10) {
				title1 = "4분기";
			}
			title2 = "이전 분기";
		} else if(period_type == 3) {
			if(strMonth==1) {
				title1 = "상반기";
			}else if(strMonth == 7) {
				title1 = "하반기";
			}
			title2 = "이전 반기";
		} else if(period_type==4) {
			title1 = start_date.split("-")[0]+"년";
			title2 = "이전 년도";
		} else if(period_type==5) {
			title1 = "기간";
			title2 = "이전 기간";
		}
		JSONObject jsonObj = new JSONObject();

		res.put("title1", title1);
		res.put("title2", title2);

		return res;
		/*
		if(period_type != 4 && period_type != 5) {
			DateUtil dateUtil = new DateUtil();
			PrivacyReport pr = dateUtil.getYyyyMmDatePriReportVO(start_date, end_date);
			pr.setSysList(syslist);
			PrivacyReport prCompare = dateUtil.getYyyyMmMonthPriReportVO(start_date, end_date);
			prCompare.setSysList(syslist);
			PrivacyReport prCompare2 = dateUtil.getBefYearPriReportVO(start_date, end_date);
			prCompare2.setSysList(syslist);
	
			JSONArray jArray = new JSONArray();
			List<PrivacyReport> list = reportSvc.findReportDetail_chart4(pr);
			System.out.println("seyoung : " + "list.size() ====start====== : " + list.size());
			for(int i=0; i<list.size(); i++) {
				System.out.println("seyoung : " + "from문 ====start====== : ");
				PrivacyReport report = list.get(i);
				int nCount1 = 0;
				int nCount2 = 0;
				int nCount3 = 0;
				report.setStart_date(prCompare.getStart_date());
				report.setEnd_date(prCompare.getEnd_date());	
				PrivacyReport prevReport = reportSvc.findReportDetail_chart4_detail(report);
				report.setStart_date(prCompare2.getStart_date());
				report.setEnd_date(prCompare2.getEnd_date());
				PrivacyReport prevYearReport = reportSvc.findReportDetail_chart4_detail(report);
				
				if(report != null) {
					nCount1 = Integer.parseInt(report.getCnt());
				}
				if(prevReport != null) {
					nCount2 = Integer.parseInt(prevReport.getCnt());
				}
				if(prevYearReport != null) {
					nCount3 = Integer.parseInt(prevYearReport.getCnt());
				}
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("dept_name", report.getSystem_name());
				jsonObj.put("system_seq", report.getSystem_seq());
				jsonObj.put("type0", nCount1);
				jsonObj.put("type1", nCount2);
				jsonObj.put("type2", nCount3);
				jArray.add(jsonObj);
			}
			Map<String, Object> hashMap = new HashMap<String, Object>();
			hashMap.put("arrData", jArray);
			int strMonth = Integer.parseInt(start_date.split("-")[1]);
			String title1 = "";
			String title2 = "";
			String title3 = "";
			if(period_type == 1) {
				title1 = strMonth+"월";
				title2 = "이전월";
				title3 = "전년 " + title1;
			} else if(period_type == 2) {
				if(strMonth==1){
					title1 = "1분기";
				}else if(strMonth == 4) {
					title1 = "2분기";
				}else if(strMonth == 7) {
					title1 = "3분기";
				}else if(strMonth == 10) {
					title1 = "4분기";
				}
				title2 = "이전 분기";
				title3 = "전년 " + title1;
			} else if(period_type == 3) {
				if(strMonth==1) {
					title1 = "상반기";
				}else if(strMonth == 7) {
					title1 = "하반기";
				}
				title2 = "이전 반기";
				title3 = "전년 " + title1;
			}
			JSONObject jsonObj = new JSONObject();
	
			hashMap.put("title1", title1);
			hashMap.put("title2", title2);
			hashMap.put("title3", title3);
	
			return hashMap;
		} else {
		*/
	}
	
	//1014
	@RequestMapping(value="reportDetail_chart4_1.html")
	@ResponseBody
	public Map reportDetail_chart4_1(@RequestParam(value = "period_type", required = false) int period_type,
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date",required=false) String start_date,
			@RequestParam(value = "end_date",required=false) String end_date) {
		
		JSONArray jArray = new JSONArray();
		String tmpList[] = system_seq.split(",");
		
		List<String> syslist = new ArrayList<String>();
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}

	    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
		String compareDate = "";
		String sDate[] = start_date.split("-");
		String eDate[] = end_date.split("-");

		int strYear = Integer.parseInt(sDate[0]);
		int strMonth = Integer.parseInt(sDate[1]);
		int endYear = Integer.parseInt(eDate[0]);
		int endMonth = Integer.parseInt(eDate[1]);

		int diffMon = (endYear - strYear)* 12 + (endMonth - strMonth);
		
		PrivacyReport pr = new PrivacyReport();			//선택 기간
		PrivacyReport prCompare = new PrivacyReport();	//비교 기간

		String stDate = start_date.replaceAll("-", "").substring(0,6);
		String edDate = end_date.replaceAll("-", "").substring(0,6);
		
		pr.setStart_date(stDate);
		pr.setEnd_date(edDate);
		pr.setSysList(syslist);
	    
	    Calendar cal = Calendar.getInstance();
	    cal.set(strYear, strMonth-1, 1);		//선택기간 시작월 1일
	    
	    cal.add(Calendar.YEAR, -1);    		// 비교 기간 끝(선택기간 전년동월)
	    compareDate = dateFormatter.format(cal.getTime());
		edDate = compareDate.substring(0,6);

		cal.add(Calendar.YEAR, -diffMon);    	// 비교 기간 시작
		compareDate = dateFormatter.format(cal.getTime());
		stDate = compareDate.substring(0,6);

		prCompare.setStart_date(stDate);
		prCompare.setEnd_date(edDate);
		prCompare.setSysList(syslist);

		Map res = new HashMap();
		List<PrivacyReport> list = reportSvc.findPrivacylogCntBySystem(pr);
		
		for(int i=0; i<list.size(); i++) {
			
			PrivacyReport report = list.get(i);
			
			
			int nCount1 = 0;
			int nCount2 = 0;
			

			report.setStart_date(prCompare.getStart_date());
			report.setEnd_date(prCompare.getEnd_date());	
			PrivacyReport prev_report = reportSvc.findPrivacylogCntBySystemCompare(report);
			
			if(report != null){
				nCount1 = Integer.parseInt(report.getCnt());
			}
			if(prev_report != null){
				nCount2 = Integer.parseInt(prev_report.getCnt());
			}
			
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("dept_name", report.getSystem_name());
			jsonObj.put("system_seq", report.getSystem_seq());
			jsonObj.put("type0", nCount1);		//금월
			jsonObj.put("type1", nCount2);		//전년동월

			jArray.add(jsonObj);
		}
		res.put("arrData", jArray);

		String title1 = "";
		String title2 = "";
		if(period_type==1){
			title1 = strMonth+"월";
			title2 = "이전월";
		} else if(period_type==2){
			if(strMonth==1){
				title1 = "1분기";
			}else if(strMonth==4){
				title1 = "2분기";
			}else if(strMonth==7){
				title1 = "3분기";
			}else if(strMonth==10){
				title1 = "4분기";
			}
			title2 = "이전 분기";
		} else if(period_type==3){
			if(strMonth==1){
				title1 = "상반기";
			}else if(strMonth==7){
				title1 = "하반기";
			}
			title2 = "이전 반기";
		} else if(period_type==4){
			title1 = strYear+"년";
			title2 = "이전 년도";
		} else if(period_type==5){
			title1 = "기간";
			title2 = "이전 기간";
		}
		JSONObject jsonObj = new JSONObject();

		res.put("title1", title1);
		res.put("title2", title2);

		return res;
	}
	
	@RequestMapping(value="reportDetail_chart5.html")
	@ResponseBody
	public Map reportDetail_chart5(
					@RequestParam(value = "period_type", required = false) int period_type,
					@RequestParam(value = "system_seq",required=false) String system_seq,
					@RequestParam(value = "start_date",required=false) String start_date,
					@RequestParam(value = "end_date",required=false) String end_date) {
		
		JSONArray jArray = new JSONArray();

		String tmpList[] = system_seq.split(",");
		
		List<String> syslist = new ArrayList<String>();
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}

	    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
		String compareDate = "";
		String sDate[] = start_date.split("-");
		String eDate[] = end_date.split("-");

		int strYear = Integer.parseInt(sDate[0]);
		int strMonth = Integer.parseInt(sDate[1]);
		int endYear = Integer.parseInt(eDate[0]);
		int endMonth = Integer.parseInt(eDate[1]);

		int diffMon = (endYear - strYear)* 12 + (endMonth - strMonth);
		
		PrivacyReport pr = new PrivacyReport();			//선택 기간
		PrivacyReport prCompare = new PrivacyReport();	//비교 기간

		String stDate = start_date.replaceAll("-", "").substring(0, 6);
		String edDate = end_date.replaceAll("-", "").substring(0, 6);
		
		pr.setStart_date(stDate);
		pr.setEnd_date(edDate);
		pr.setSysList(syslist);
	    
	    Calendar cal = Calendar.getInstance();
	    cal.set(strYear, strMonth-1, 1);		//선택기간 시작월 1일
	    
	    cal.add(Calendar.MONTH, -1);    		// 비교 기간 끝(선택기간 한달전)
	    compareDate = dateFormatter.format(cal.getTime());
		edDate = compareDate.substring(0,6);

		cal.add(Calendar.MONTH, -diffMon);    	// 비교 기간 시작
		compareDate = dateFormatter.format(cal.getTime());
		stDate = compareDate.substring(0,6);

		prCompare.setStart_date(stDate);
		prCompare.setEnd_date(edDate);
		prCompare.setSysList(syslist);

		
//		if (use_studentId.length() > 0 && use_studentId.equals("yes")) {
//			list = reportSvc.findReportDetail_chart5_useStudentId(pr);
//		} else {
//			list = reportSvc.findReportDetail_chart5(pr);
//		}
		List<PrivacyReport> userVoList = reportSvc.findReportDetail_chart5(pr);
		Map res = new HashMap();
		if(userVoList.size() < 1) {
			int nCount1 = 0;
			int nCount2 = 0;
			PrivacyReport reportUserVo = new PrivacyReport();
			reportUserVo.setStart_date(prCompare.getStart_date());
			reportUserVo.setEnd_date(prCompare.getEnd_date());
			reportUserVo.setSysList(syslist);
			List<PrivacyReport> userVoDetailList = reportSvc.findReportDetail_chart5(reportUserVo);
			for(int i=0; i<userVoDetailList.size(); i++) {
				if(0 < userVoDetailList.size()) {
					PrivacyReport report = userVoDetailList.get(i);
				
					if(report != null) {
						nCount2 = Integer.parseInt(report.getCnt());
					}
					JSONObject jsonObj = new JSONObject();			
					jsonObj.put("dept_name", report.getDept_name());
					jsonObj.put("type0", nCount1);		//금월
					jsonObj.put("type1", nCount2);		//전월

					jArray.add(jsonObj);
				}
			}
		} else {
			for(int i=0; i<userVoList.size(); i++) {
				int nCount1 = 0;
				int nCount2 = 0;
				PrivacyReport report = userVoList.get(i);
				report.setSysList(syslist);
				report.setStart_date(prCompare.getStart_date());
				report.setEnd_date(prCompare.getEnd_date());	
				PrivacyReport prev_report = reportSvc.findReportDetail_chart5_detail(report);

				if(report != null) {
					nCount1 = Integer.parseInt(report.getCnt());
				}
				if(prev_report != null) {
					nCount2 = Integer.parseInt(prev_report.getCnt());
				}
				JSONObject jsonObj = new JSONObject();			
				jsonObj.put("dept_name", report.getDept_name());
				jsonObj.put("type0", nCount1);		//금월
				jsonObj.put("type1", nCount2);		//전월

				jArray.add(jsonObj);
			}
		}
		res.put("arrData", jArray);

		String title1 = "";
		String title2 = "";
		if(period_type==1){
			title1 = strMonth+"월";
			title2 = "이전월";
		} else if(period_type==2){
			if(strMonth==1){
				title1 = "1분기";
			}else if(strMonth==4){
				title1 = "2분기";
			}else if(strMonth==7){
				title1 = "3분기";
			}else if(strMonth==10){
				title1 = "4분기";
			}
			title2 = "이전 분기";
		} else if(period_type==3){
			if(strMonth==1){
				title1 = "상반기";
			}else if(strMonth==7){
				title1 = "하반기";
			}
			title2 = "이전 반기";
		} else if(period_type==4){
			title1 = strYear+"년";
			title2 = "이전 년도";
		} else if(period_type==5){
			title1 = "기간";
			title2 = "이전 기간";
		}
		JSONObject jsonObj = new JSONObject();

		res.put("title1", title1);
		res.put("title2", title2);

		return res;
	}
	
	//1014
	@RequestMapping(value="reportDetail_chart5_1.html")
	@ResponseBody
	public Map reportDetail_chart5_1(
					@RequestParam(value = "period_type", required = false) int period_type,
					@RequestParam(value = "system_seq",required=false) String system_seq,
					@RequestParam(value = "start_date",required=false) String start_date,
					@RequestParam(value = "end_date",required=false) String end_date) {
		JSONArray jArray = new JSONArray();

		String tmpList[] = system_seq.split(",");
		
		List<String> syslist = new ArrayList<String>();
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}

	    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
		String compareDate = "";
		String sDate[] = start_date.split("-");
		String eDate[] = end_date.split("-");

		int strYear = Integer.parseInt(sDate[0]);
		int strMonth = Integer.parseInt(sDate[1]);
		int endYear = Integer.parseInt(eDate[0]);
		int endMonth = Integer.parseInt(eDate[1]);

		int diffMon = (endYear - strYear)* 12 + (endMonth - strMonth);
		
		PrivacyReport pr = new PrivacyReport();			//선택 기간
		PrivacyReport prCompare = new PrivacyReport();	//비교 기간

		String stDate = start_date.replaceAll("-", "");
		String edDate = end_date.replaceAll("-", "");
		
		pr.setStart_date(stDate);
		pr.setEnd_date(edDate);
		pr.setSysList(syslist);
	    
	    Calendar cal = Calendar.getInstance();
	    cal.set(strYear, strMonth-1, 1);		//선택기간 시작월 1일
	    
	    cal.add(Calendar.YEAR, -1);    		// 비교 기간 끝(선택기간 한달전)
	    compareDate = dateFormatter.format(cal.getTime());
		edDate = compareDate.substring(0,6)+"31";

		cal.add(Calendar.YEAR, -diffMon);    	// 비교 기간 시작
		compareDate = dateFormatter.format(cal.getTime());
		stDate = compareDate.substring(0,6)+"01";

		prCompare.setStart_date(stDate);
		prCompare.setEnd_date(edDate);
		prCompare.setSysList(syslist);

		
		List<PrivacyReport> list = new ArrayList<>();
		if (use_studentId.length() > 0 && use_studentId.equals("yes")) {
			list = reportSvc.findReportDetail_chart5_useStudentId(pr);
		} else {
			list = reportSvc.findReportDetail_chart5(pr);
		}
		
		Map res = new HashMap();
		for(int i=0; i<list.size(); i++) {
			
			PrivacyReport report = list.get(i);
			report.setSysList(syslist);

			int nCount1 = 0;
			int nCount2 = 0;

			report.setStart_date(prCompare.getStart_date());
			report.setEnd_date(prCompare.getEnd_date());	
			PrivacyReport prev_report = reportSvc.findReportDetail_chart5_detail(report);

			if(report != null)
				nCount1 = Integer.parseInt(report.getCnt());
			if(prev_report != null)
				nCount2 = Integer.parseInt(prev_report.getCnt());

			JSONObject jsonObj = new JSONObject();			
			jsonObj.put("dept_name", report.getDept_name());
			jsonObj.put("type0", nCount1);		//금월
			jsonObj.put("type1", nCount2);		//전월

			jArray.add(jsonObj);
		}
		res.put("arrData", jArray);

		String title1 = "";
		String title2 = "";
		if(period_type==1){
			title1 = strMonth+"월";
			title2 = "이전월";
		} else if(period_type==2){
			if(strMonth==1){
				title1 = "1분기";
			}else if(strMonth==4){
				title1 = "2분기";
			}else if(strMonth==7){
				title1 = "3분기";
			}else if(strMonth==10){
				title1 = "4분기";
			}
			title2 = "이전 분기";
		} else if(period_type==3){
			if(strMonth==1){
				title1 = "상반기";
			}else if(strMonth==7){
				title1 = "하반기";
			}
			title2 = "이전 반기";
		} else if(period_type==4){
			title1 = strYear+"년";
			title2 = "이전 년도";
		} else if(period_type==5){
			title1 = "기간";
			title2 = "이전 기간";
		}
		JSONObject jsonObj = new JSONObject();

		res.put("title1", title1);
		res.put("title2", title2);

		return res;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "reportDetail_chart6.html")
	@ResponseBody
	public JSONArray reportDetail_chart6(
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {

		JSONArray jArray = new JSONArray();

		String tmpList[] = system_seq.split(",");
		
		List<String> syslist = new ArrayList<String>();
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", "").substring(0,6));
		pr.setEnd_date(end_date.replaceAll("-", "").substring(0,6));
		pr.setSysList(syslist);

		List<PrivacyReport> listByReqtype = reportSvc.chart_findPrivacyReportByReqtype(pr);

		for (int i = 0; i < listByReqtype.size(); i++) {
			JSONObject jsonObj = new JSONObject();
			PrivacyReport report = listByReqtype.get(i);
			jsonObj.put("req_type", report.getReq_type());
			jsonObj.put("cnt", report.getCnt());
			jArray.add(jsonObj);
		}

		return jArray;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "download_reportDetail_chart4.html")
	@ResponseBody
	public JSONArray download_reportDetail_chart4(
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {

		JSONArray jArray = new JSONArray();

		String tmpList[] = system_seq.split(",");
		
		List<String> syslist = new ArrayList<String>();
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", "").substring(0,6));
		pr.setEnd_date(end_date.replaceAll("-", "").substring(0,6));
		pr.setSysList(syslist);

		List<PrivacyReport> listByReqtype = reportSvc.getFileExtension_chart(pr);
		
		int listSize = listByReqtype.size();
		if(listSize >= 10)
			listSize = 10;

		for (int i = 0; i < listSize; i++) {
			JSONObject jsonObj = new JSONObject();
			PrivacyReport report = listByReqtype.get(i);
			jsonObj.put("file_ext", report.getFile_ext());
			jsonObj.put("file_ext_cnt", report.getFile_ext_cnt());
			jArray.add(jsonObj);
		}

		return jArray;
	}
	
	
	@RequestMapping(value = "reportDetail_chart7.html")
	@ResponseBody
	public JSONArray reportDetail_chart7(
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {

		JSONArray jArray = new JSONArray();

		String tmpList[] = system_seq.split(",");
		
		List<String> syslist = new ArrayList<String>();
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", "").substring(0,6));
		pr.setEnd_date(end_date.replaceAll("-", "").substring(0,6));
		pr.setSysList(syslist);
//		List<SystemMaster> systemList = reportSvc.findSystemMasterList();
		List<PrivacyReport> listByResultType = reportSvc.findPrivacyReportByResultType(pr);
		for (int i = 0; i < listByResultType.size(); i++) {
			JSONObject jsonObj = new JSONObject();
			PrivacyReport report = listByResultType.get(i);

			jsonObj.put("result_type", report.getResult_type());
			jsonObj.put("category", report.getPrivacy_desc());
			jsonObj.put("value", report.getCnt());
//			jsonObj.put("systemCnt", systemList.size());
			jArray.add(jsonObj);
		}

		return jArray;
	}

	@RequestMapping(value = "reportDetail_chart8.html")
	@ResponseBody
	public JSONArray reportDetail_chart8(
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {

		JSONArray jArray = new JSONArray();

		String tmpList[] = system_seq.split(",");
		
		List<String> syslist = new ArrayList<String>();
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", "").substring(0,6));
		pr.setEnd_date(end_date.replaceAll("-", "").substring(0,6));
		pr.setSysList(syslist);

		List<PrivacyReport> systemTypeResultCountTop10 = reportSvc.findSystemTypeResultCountTop10(pr);

		for (int i = 0; i < systemTypeResultCountTop10.size(); ++i) {

			PrivacyReport report = systemTypeResultCountTop10.get(i);
			report.setStart_date(start_date.replaceAll("-", "").substring(0,6));
			report.setEnd_date(end_date.replaceAll("-", "").substring(0,6));
			report.setSysList(syslist);

			List<PrivacyReport> privTypeTemp = reportSvc.findSystemTypeResultCount(report);

			for (int j = 0; j < privTypeTemp.size(); ++j) {
				PrivacyReport reportT = privTypeTemp.get(j);
				if (reportT.getResult_type().equals("1"))
					report.setType1(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("2"))
					report.setType2(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("3"))
					report.setType3(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("4"))
					report.setType4(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("5"))
					report.setType5(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("6"))
					report.setType6(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("7"))
					report.setType7(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("8"))
					report.setType8(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("9"))
					report.setType9(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("10"))
					report.setType10(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("11"))
					report.setType11(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("12"))
					report.setType12(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("99"))
					report.setType99(Integer.parseInt(reportT.getCnt()));
			}

			systemTypeResultCountTop10.set(i, report);

			JSONObject jsonObj = new JSONObject();

			jsonObj.put("category", report.getSystem_name());
			jsonObj.put("type1", report.getType1());
			jsonObj.put("type2", report.getType2());
			jsonObj.put("type3", report.getType3());
			jsonObj.put("type4", report.getType4());
			jsonObj.put("type5", report.getType5());
			jsonObj.put("type6", report.getType6());
			jsonObj.put("type7", report.getType7());
			jsonObj.put("type8", report.getType8());
			jsonObj.put("type9", report.getType9());
			jsonObj.put("type10", report.getType10());
			jsonObj.put("type11", report.getType11());
			jsonObj.put("type12", report.getType12());
			jsonObj.put("type99", report.getType99());

			jArray.add(jsonObj);
		}

		return jArray;
	}
	
	@RequestMapping(value = "reportDetail_chart8_new.html")
	@ResponseBody
	public JSONObject reportDetail_chart8_new(
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {

		JSONObject jObj = new JSONObject();
		JSONArray dataProvider = new JSONArray();
		JSONArray graphs = new JSONArray();

		String tmpList[] = system_seq.split(",");
		
		List<String> syslist = new ArrayList<String>();
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", "").substring(0,6));
		pr.setEnd_date(end_date.replaceAll("-", "").substring(0,6));
		pr.setSysList(syslist);
		
		List<Code> resultTypeList = reportSvc.findResultTypeList();
		
		for(int i=0; i<resultTypeList.size(); i++) {
			Code code = resultTypeList.get(i);
			JSONObject obj = new JSONObject();
			obj.put("balloonText", "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>");
			obj.put("fillAlphas", 0.8);
			obj.put("labelText", "[[value]]");
			obj.put("title", code.getCode_name());
			obj.put("type", "column");
			obj.put("color", "#000000");
			obj.put("valueField", code.getCode_id());
			obj.put("descriptionField", "system_seq");
			
			graphs.add(obj);
		}
		
		List<PrivacyReport> systemTypeResultCountTop10 = reportSvc.findSystemTypeResultCountTop10(pr);

		for (int i = 0; i < systemTypeResultCountTop10.size(); ++i) {
			
			JSONObject obj = new JSONObject();
			
			PrivacyReport report = systemTypeResultCountTop10.get(i);
			report.setStart_date(start_date.replaceAll("-", "").substring(0,6));
			report.setEnd_date(end_date.replaceAll("-", "").substring(0,6));
			report.setSysList(syslist);
			
			obj.put("category", report.getSystem_name());
			obj.put("system_seq", report.getSystem_seq());
			
			List<PrivacyReport> privTypeTemp = reportSvc.findSystemTypeResultCount(report);
			for(int j=0; j<privTypeTemp.size(); j++) {
				PrivacyReport reportT = privTypeTemp.get(j);
				obj.put(reportT.getResult_type(), reportT.getCnt());
			}
			dataProvider.add(obj);
		}
		
		jObj.put("dataProvider", dataProvider);
		jObj.put("graphs", graphs);
		
		return jObj;
	}

	@RequestMapping(value = "reportDetail_chart9.html")
	@ResponseBody
	public JSONArray reportDetail_chart9(
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {

		JSONArray jArray = new JSONArray();

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", ""));
		pr.setEnd_date(end_date.replaceAll("-", ""));

		List<PrivacyReport> deptTypeResultCountTop10 = reportSvc.findDeptTypeResultCountTop10(pr);

		for (int i = 0; i < deptTypeResultCountTop10.size(); ++i) {

			PrivacyReport report = deptTypeResultCountTop10.get(i);
			report.setStart_date(start_date.replaceAll("-", ""));
			report.setEnd_date(end_date.replaceAll("-", ""));

			List<PrivacyReport> privTypeTemp = reportSvc.findDeptTypeResultCount(report);

			for (int j = 0; j < privTypeTemp.size(); ++j) {
				PrivacyReport reportT = privTypeTemp.get(j);
				if (reportT.getResult_type().equals("1"))
					report.setType1(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("2"))
					report.setType2(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("3"))
					report.setType3(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("4"))
					report.setType4(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("5"))
					report.setType5(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("6"))
					report.setType6(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("7"))
					report.setType7(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("8"))
					report.setType8(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("9"))
					report.setType9(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("10"))
					report.setType10(Integer.parseInt(reportT.getCnt()));
			}

			deptTypeResultCountTop10.set(i, report);

			JSONObject jsonObj = new JSONObject();

			jsonObj.put("category", report.getDept_name());
			jsonObj.put("type1", report.getType1());
			jsonObj.put("type2", report.getType2());
			jsonObj.put("type3", report.getType3());
			jsonObj.put("type4", report.getType4());
			jsonObj.put("type5", report.getType5());
			jsonObj.put("type6", report.getType6());
			jsonObj.put("type7", report.getType7());
			jsonObj.put("type8", report.getType8());
			jsonObj.put("type9", report.getType9());
			jsonObj.put("type10", report.getType10());

			jArray.add(jsonObj);
		}

		return jArray;
	}
	
	@RequestMapping(value = "reportDetail_chart9_new.html")
	@ResponseBody
	public JSONObject reportDetail_chart9_new(
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {

		JSONObject jObj = new JSONObject();
		JSONArray dataProvider = new JSONArray();
		JSONArray graphs = new JSONArray();

		String tmpList[] = system_seq.split(",");
		
		List<String> syslist = new ArrayList<String>();
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", "").substring(0,6));
		pr.setEnd_date(end_date.replaceAll("-", "").substring(0,6));
		pr.setSysList(syslist);
		
//		List<SystemMaster> systemList = agentMngtDao.findSystemMasterList();
		//List<SystemMaster> systemList = reportSvc.findSystemMasterDetailList(pr);
		List<Code> resultTypeList = reportSvc.findResultTypeList();
		
		for(int i=0; i<resultTypeList.size(); i++) {
			Code code = resultTypeList.get(i);
			JSONObject obj = new JSONObject();
			obj.put("balloonText", "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>");
			obj.put("fillAlphas", 0.8);
			obj.put("labelText", "[[value]]");
			obj.put("title", code.getCode_name());
			obj.put("type", "column");
			obj.put("color", "#000000");
			obj.put("valueField", code.getCode_id());
			
			graphs.add(obj);
		}
		
		List<PrivacyReport> deptTypeResultCountTop10 = reportSvc.findDeptTypeResultCountBySysTop10(pr);

		for (int i = 0; i < deptTypeResultCountTop10.size(); ++i) {
			
			JSONObject obj = new JSONObject();
			
			PrivacyReport report = deptTypeResultCountTop10.get(i);
			report.setStart_date(start_date.replaceAll("-", "").substring(0,6));
			report.setEnd_date(end_date.replaceAll("-", "").substring(0,6));
			report.setSysList(syslist);
			
			obj.put("category", report.getDept_name());
			
			List<PrivacyReport> privTypeTemp = reportSvc.findDeptTypeResultCountBySys(report);
			for(int j=0; j<privTypeTemp.size(); j++) {
				PrivacyReport reportT = privTypeTemp.get(j);
				obj.put(reportT.getResult_type(), reportT.getCnt());
			}
			dataProvider.add(obj);
		}
		
		jObj.put("dataProvider", dataProvider);
		jObj.put("graphs", graphs);
		
		return jObj;
	}

	@RequestMapping(value = "reportDetail_chart10.html")
	@ResponseBody
	public JSONArray reportDetail_chart10(
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {

		JSONArray jArray = new JSONArray();

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", ""));
		pr.setEnd_date(end_date.replaceAll("-", ""));

		List<PrivacyReport> privTypeResultCountTop10 = reportSvc.findPrivTypeResultCountTop10(pr);

		for (int i = 0; i < privTypeResultCountTop10.size(); ++i) {

			PrivacyReport report = privTypeResultCountTop10.get(i);
			report.setStart_date(start_date.replaceAll("-", ""));
			report.setEnd_date(end_date.replaceAll("-", ""));

			List<PrivacyReport> privTypeTemp = reportSvc.findPrivTypeResultCount(report);

			for (int j = 0; j < privTypeTemp.size(); ++j) {
				PrivacyReport reportT = privTypeTemp.get(j);
				if (reportT.getResult_type().equals("1"))
					report.setType1(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("2"))
					report.setType2(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("3"))
					report.setType3(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("4"))
					report.setType4(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("5"))
					report.setType5(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("6"))
					report.setType6(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("7"))
					report.setType7(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("8"))
					report.setType8(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("9"))
					report.setType9(Integer.parseInt(reportT.getCnt()));
				else if (reportT.getResult_type().equals("10"))
					report.setType10(Integer.parseInt(reportT.getCnt()));
			}

			privTypeResultCountTop10.set(i, report);

			JSONObject jsonObj = new JSONObject();

			jsonObj.put("category", report.getEmp_user_name());
			jsonObj.put("type1", report.getType1());
			jsonObj.put("type2", report.getType2());
			jsonObj.put("type3", report.getType3());
			jsonObj.put("type4", report.getType4());
			jsonObj.put("type5", report.getType5());
			jsonObj.put("type6", report.getType6());
			jsonObj.put("type7", report.getType7());
			jsonObj.put("type8", report.getType8());
			jsonObj.put("type9", report.getType9());
			jsonObj.put("type10", report.getType10());

			jArray.add(jsonObj);
		}

		return jArray;
	}
	
	@RequestMapping(value = "reportDetail_chart10_new.html")
	@ResponseBody
	public JSONObject reportDetail_chart10_new(
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {

		JSONObject jObj = new JSONObject();
		JSONArray dataProvider = new JSONArray();
		JSONArray graphs = new JSONArray();

		String tmpList[] = system_seq.split(",");
		
		List<String> syslist = new ArrayList<String>();
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", "").substring(0,6));
		pr.setEnd_date(end_date.replaceAll("-", "").substring(0,6));
		pr.setSysList(syslist);

		List<PrivacyReport> privTypeResultCountBySysTop10 = reportSvc.findPrivTypeResultCountBySysTop10(pr);
		//List<SystemMaster> systemList = agentMngtDao.findSystemMasterList();
		//List<SystemMaster> systemList = reportSvc.findSystemMasterDetailList(pr);
		List<Code> resultTypeList = reportSvc.findResultTypeList();
		
		for(int i=0; i<resultTypeList.size(); i++) {
			Code code = resultTypeList.get(i);
			JSONObject obj = new JSONObject();
			obj.put("balloonText", "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>");
			obj.put("fillAlphas", 0.8);
			obj.put("labelText", "[[value]]");
			obj.put("title", code.getCode_name());
			obj.put("type", "column");
			obj.put("color", "#000000");
			obj.put("valueField", code.getCode_id());
			obj.put("descriptionField", "emp_user_id");
			
			graphs.add(obj);
		}
	
		for (int i = 0; i < privTypeResultCountBySysTop10.size(); ++i) {
			
			JSONObject obj = new JSONObject();
			
			PrivacyReport report = privTypeResultCountBySysTop10.get(i);
			report.setStart_date(start_date.replaceAll("-", "").substring(0,6));
			report.setEnd_date(end_date.replaceAll("-", "").substring(0,6));
			report.setSysList(syslist);
			
			String checkEmpNameMasking = commonDao.checkEmpNameMasking();
			if(checkEmpNameMasking.equals("Y")) {
				if(!"".equals(report.getEmp_user_name())) {
					String empUserName = report.getEmp_user_name();
					StringBuilder builder = new StringBuilder(empUserName);
					builder.setCharAt(empUserName.length() - 2, '*');
					report.setEmp_user_name(builder.toString());
				}
			}
			
			String category = report.getEmp_user_name() + "(" + report.getDept_name() + ")";
			obj.put("category", category);
			obj.put("emp_user_id", report.getEmp_user_id());
			
			List<PrivacyReport> privTypeTemp = reportSvc.findPrivTypeResultCountBySys(report);

			for (int j = 0; j < privTypeTemp.size(); ++j) {
				PrivacyReport reportT = privTypeTemp.get(j);
				obj.put(reportT.getResult_type(), reportT.getCnt());
			}
			dataProvider.add(obj);
			//privTypeResultCountBySysTop10.set(i, report);			
		}
		jObj.put("dataProvider", dataProvider);
		jObj.put("graphs", graphs);

		return jObj;
	}

	// ----------------------------------------------------------------------
	// 오남용보고용 chart1
	//
	// ----------------------------------------------------------------------
	@RequestMapping(value = "reportDetail2_chart1.html")
	@ResponseBody
	public Map reportDetail2_chart1(
			@RequestParam(value = "period_type", required = false) int period_type,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date,
			@RequestParam(value = "system_seq", required = false) String system_seq) {
		
	    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");

	    String sDate[] = start_date.split("-");
		String eDate[] = end_date.split("-");

		List<String> syslist = new ArrayList<String>();
		String tmpList[] = system_seq.split(",");
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}
		
		String compareDate = "";

		int strYear = Integer.parseInt(sDate[0]);
		int strMonth = Integer.parseInt(sDate[1]);
		int endYear = Integer.parseInt(eDate[0]);
		int endMonth = Integer.parseInt(eDate[1]);

		int diffMon = (endYear - strYear)* 12 + (endMonth - strMonth);
		
		PrivacyReport pr = new PrivacyReport();			//선택 기간
		PrivacyReport prCompare = new PrivacyReport();	//비교 기간

		String stDate = start_date.replaceAll("-", "");
		String edDate = end_date.replaceAll("-", "");
		
		pr.setStart_date(stDate);
		pr.setEnd_date(edDate);
	    pr.setSysList(syslist);
		
	    Calendar cal = Calendar.getInstance();
	    cal.set(strYear, strMonth-1, 1);		//선택기간 시작월 1일
	    
	    cal.add(Calendar.MONTH, -1);    		// 비교 기간 끝(선택기간 한달전)
	    compareDate = dateFormatter.format(cal.getTime());
		edDate = compareDate.substring(0,6)+"31";

		cal.add(Calendar.MONTH, -diffMon);    	// 비교 기간 시작
		compareDate = dateFormatter.format(cal.getTime());
		stDate = compareDate.substring(0,6)+"01";

		prCompare.setStart_date(stDate);
		prCompare.setEnd_date(edDate);
		prCompare.setSysList(syslist);

		// todo: 같은 달이면 이번달만.. 아니면 이번달과 저번달
		Map res = new HashMap();
		JSONArray jArrPre = new JSONArray();
		JSONArray jArrCur = new JSONArray();

		//String[] arrStart = start_date.split("-");

		//PrivacyReport pr = new PrivacyReport();

		// 현재달
/*		Calendar cal = Calendar.getInstance();
		cal.set(Integer.parseInt(arrStart[0]), Integer.parseInt(arrStart[1]) - 1, Integer.parseInt(arrStart[2]));
		int endDay = cal.getActualMaximum(Calendar.DATE);
		String firstDate = arrStart[0] + "" + arrStart[1] + "" + "01";
		String endDate = arrStart[0] + "" + arrStart[1] + "" + endDay;
		pr.setStart_date(firstDate);
		pr.setEnd_date(endDate);*/
		List<PrivacyReport> data = reportSvc.findReportDetail2_chart1(pr);
		/*String preStr = "";
		int sum = 0;
		for (int i = 0; i < data.size(); i++) {
			PrivacyReport report = data.get(i);
			String rule_cd = report.getRule();
			if (rule_cd.substring(0, 1).equals(preStr)) {
				sum += Integer.parseInt(report.getCnt());
			} else {
				if (i != 0) {
					JSONObject obj = new JSONObject();
					String rule = data.get(i - 1).getRule();
					String resRule = getExtraBaseSetupStr(rule);
					obj.put("rule", resRule);
					obj.put("cnt", sum);
					jArrCur.add(obj);
				}
				sum = Integer.parseInt(report.getCnt());
				preStr = rule_cd.substring(0, 1);
			}

			if (i == data.size() - 1) {
				JSONObject obj = new JSONObject();
				String rule = data.get(i).getRule();
				String resRule = getExtraBaseSetupStr(rule);
				obj.put("rule", resRule);
				obj.put("cnt", sum);
				jArrCur.add(obj);
			}
		}*/
		res.put("cur", data);

		// 이전달
/*		cal.add(cal.MONTH, -1);
		endDay = cal.getActualMaximum(Calendar.DATE);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMM");
		String beforeYear = dateFormat.format(cal.getTime()).substring(0, 4);
		String beforeMonth = dateFormat.format(cal.getTime()).substring(4, 6);
		firstDate = beforeYear + "" + beforeMonth + "" + "01";
		endDate = beforeYear + "" + beforeMonth + "" + endDay;
		pr.setStart_date(firstDate);
		pr.setEnd_date(endDate);*/
		data = reportSvc.findReportDetail2_chart1(prCompare);
		/*preStr = "";
		sum = 0;
		for (int i = 0; i < data.size(); i++) {
			PrivacyReport report = data.get(i);
			String rule_cd = report.getRule();
			if (rule_cd.substring(0, 1).equals(preStr)) {
				sum += Integer.parseInt(report.getCnt());
			} else {
				if (i != 0) {
					JSONObject obj = new JSONObject();
					String rule = data.get(i - 1).getRule();
					String resRule = getExtraBaseSetupStr(rule);
					obj.put("rule", resRule);
					obj.put("cnt", sum);
					jArrPre.add(obj);
				}
				sum = Integer.parseInt(report.getCnt());
				preStr = rule_cd.substring(0, 1);
			}

			if (i == data.size() - 1) {
				JSONObject obj = new JSONObject();
				String rule = data.get(i).getRule();
				String resRule = getExtraBaseSetupStr(rule);
				obj.put("rule", resRule);
				obj.put("cnt", sum);
				jArrPre.add(obj);
			}
		}*/
		res.put("pre", data);

		String title1 = "";
		String title2 = "";
		if(period_type==1){
			title1 = strMonth+"월";
			title2 = "이전월";
		} else if(period_type==2){
			if(strMonth==1){
				title1 = "1분기";
			}else if(strMonth==4){
				title1 = "2분기";
			}else if(strMonth==7){
				title1 = "3분기";
			}else if(strMonth==10){
				title1 = "4분기";
			}
			title2 = "이전 분기";
		} else if(period_type==3){
			if(strMonth==1){
				title1 = "상반기";
			}else if(strMonth==7){
				title1 = "하반기";
			}
			title2 = "이전 반기";
		} else if(period_type==4){
			title1 = strYear+"년";
			title2 = "이전 년도";
		} else if(period_type==5){
			title1 = "기간";
			title2 = "이전 기간";
		}
		res.put("title1", title1);
		res.put("title2", title2);

		return res;
	}
	
	/**
	 * @page : getReportDetail2_chart1
	 * @auth : syjung
	 */
	public List getReportDetail2_chart1(String start_date, String end_date, List<String> syslist) {
		DateUtil dateUtil = new DateUtil();
		PrivacyReport pr = dateUtil.getInputDatePriReportVO(start_date, end_date);
		pr.setSysList(syslist);
		PrivacyReport prCompare = dateUtil.getBefMonthPriReportVO(start_date, end_date);
		prCompare.setSysList(syslist);

		List<PrivacyReport> data1 = reportSvc.findReportDetail2_chart1(pr);
		List<PrivacyReport> data2 = reportSvc.findReportDetail2_chart1(prCompare);

		List list = new ArrayList<>();
		for (int i = 0; i < data1.size(); i++) {
			HashMap map = new HashMap<>();
			PrivacyReport cur = data1.get(i);
			PrivacyReport pre = data2.get(i);
			map.put("rule_nm", cur.getRule_nm());
			map.put("cur", cur.getCnt());
			map.put("pre", pre.getCnt());
			
			list.add(map);
		}

		return list;
	}
	/**
	 * @page : getReportDetail2_chart1
	 * @auth : syjung
	 */
	public List getReportDetail2_chart1(String start_date, String end_date, List<String> syslist, int dateType) {
		DateUtil dateUtil = new DateUtil();
		PrivacyReport pr = dateUtil.getInputDatePriReportVO(start_date, end_date);
		pr.setSysList(syslist);
		PrivacyReport prCompare = dateUtil.getBefMonthPriReportVO(start_date, end_date);
		prCompare.setSysList(syslist);
		PrivacyReport prCompare2 = dateUtil.getBefYearPriReportVO(start_date, end_date);
		prCompare2.setSysList(syslist);

		List<PrivacyReport> data1 = reportSvc.findReportDetail2_chart1(pr);
		List<PrivacyReport> data2 = reportSvc.findReportDetail2_chart1(prCompare);
		List<PrivacyReport> data3 = reportSvc.findReportDetail2_chart1(prCompare2);

		List list = new ArrayList<>();
		for (int i = 0; i < data1.size(); i++) {
			HashMap map = new HashMap<>();
			PrivacyReport cur = data1.get(i);
			PrivacyReport pre = data2.get(i);
			PrivacyReport preYear = data3.get(i);
			map.put("rule_nm", cur.getRule_nm());
			map.put("cur", cur.getCnt());
			map.put("pre", pre.getCnt());
			map.put("preYear", preYear.getCnt());
			
			list.add(map);
		}

		return list;
	}

	// ----------------------------------------------------------------------
	// 오남용보고용 chart2
	//
	// ----------------------------------------------------------------------
	@RequestMapping(value = "reportDetail2_chart2.html")
	@ResponseBody
	public JSONArray reportDetail2_chart2(
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {

		JSONArray jArray = new JSONArray();

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", ""));
		pr.setEnd_date(end_date.replaceAll("-", ""));

		List<PrivacyReport> data = reportSvc.findReportDetail2_chart2(pr);
		for (int i = 0; i < data.size(); i++) {
			PrivacyReport report = data.get(i);
			JSONObject obj = new JSONObject();
			obj.put("rule", report.getRule());
			obj.put("cnt", report.getCnt());

			jArray.add(obj);
		}

		return jArray;
	}

	public List getReportDetail2_chart2(PrivacyReport pr) {

		List list = new ArrayList<>();
		List<PrivacyReport> data = reportSvc.findReportDetail2_chart2(pr);
		String strPre = "";
		int rowspan = 0;
		for (int i = 0; i < data.size(); i++) {
			PrivacyReport report = data.get(i);
			Map obj = new HashMap<>();
			String subPre = report.getRule_cd().substring(0, 1);
			if (subPre.equals(strPre)) {
				rowspan += 1;
			} else {
				if (i == 0) {
					rowspan += 1;
				} else {
					Map tmp = (Map) list.get(i - rowspan);
					tmp.put("rowspan", rowspan);
					rowspan = 1;
				}
				String resRule = getExtraBaseSetupStr(report.getRule_cd());
				obj.put("rule_cd", resRule);
				strPre = report.getRule_cd().substring(0, 1);
			}
			String resRule = getExtraBaseSetupStr(report.getRule());
			obj.put("rule", report.getRule());
			obj.put("cnt", report.getCnt());

			if (i == data.size() - 1) {
				if (rowspan > 1) {
					if( i < rowspan) {
						Map tmp = (Map)list.get(0);
						tmp.put("rowspan", rowspan);
					} else {
						Map tmp = (Map) list.get(i - rowspan);
						tmp.put("rowspan", rowspan);
					}
				} else {
					obj.put("rowspan", rowspan);
				}
			}
			list.add(obj);
		}

		return list;
	}

	@RequestMapping(value = "reportDetail2_chart3.html")
	@ResponseBody
	public Map reportDetail2_chart3(
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date,
			@RequestParam(value = "system_seq", required = false) String system_seq) {

		List<Report> rule_list = new ArrayList<Report>();
		
		String tmpList[] = system_seq.split(",");
		List<String> syslist = new ArrayList<String>();
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}
		
		Report tmp1 = new Report();
		tmp1.setRule_nm("고유식별정보 과다사용");
		rule_list.add(tmp1);
		
		Report tmp2 = new Report();
		tmp2.setRule_nm("비정상 접근");
		rule_list.add(tmp2);
		
		Report tmp3 = new Report();
		tmp3.setRule_nm("과다처리");
		rule_list.add(tmp3);
		
		Report tmp4 = new Report();
		tmp4.setRule_nm("특정인 처리");
		rule_list.add(tmp4);
		
		Report tmp5 = new Report();
		tmp5.setRule_nm("특정시간대 처리");
		rule_list.add(tmp5);
		
		
		Map map = new HashMap<>();
		map.put("start_date", start_date.replaceAll("-", ""));
		map.put("end_date", end_date.replaceAll("-", ""));
		map.put("rule_list", rule_list);
		map.put("sysList",syslist);

		List<Map<String, Object>> emp_list = reportDao.findReportDetail2_chart3(map);

		/*JSONArray jArray = new JSONArray();
		for (int i = 0; i < emp_list.size(); i++) {
			
			if ( i>4 ) break;
			
			Map data = emp_list.get(i);
			JSONObject obj = new JSONObject();
			
			data.put("start_date", start_date.replaceAll("-", ""));
			data.put("end_date", end_date.replaceAll("-", ""));
			
			List<Map> emp_listDetail = reportDao.findReportDetail2_chart3Detail(data);
			
			int nType1=0;
			int nType2=0;
			int nType3=0;
			int nType4=0;
			int nType5=0;
			
			for ( int j=0; j<emp_listDetail.size(); ++j ) {
				Map dataTemp = emp_listDetail.get(j);
				
				if (dataTemp.get("scen_seq").equals(1000)) {
					nType1=1;
					obj.put("nType1", dataTemp.get("cnt"));
				}
				else if (dataTemp.get("scen_seq").equals(2000)) {
					nType2=1;
					obj.put("nType2", dataTemp.get("cnt"));
				}
				else if (dataTemp.get("scen_seq").equals(3000)) {
					nType3=1;
					obj.put("nType3", dataTemp.get("cnt"));
				}
				else if (dataTemp.get("scen_seq").equals(4000)) {
					nType4=1;
					obj.put("nType4", dataTemp.get("cnt"));
				}
				else if (dataTemp.get("scen_seq").equals(5000)) {
					nType5=1;
					obj.put("nType5", dataTemp.get("cnt"));
				}
			}
			
			obj.put("dept_name", data.get("dept_name"));
			if ( nType1 == 0 ) obj.put("nType1", nType1);
			if ( nType2 == 0 ) obj.put("nType2", nType2);
			if ( nType3 == 0 ) obj.put("nType3", nType3);
			if ( nType4 == 0 ) obj.put("nType4", nType4);
			if ( nType5 == 0 ) obj.put("nType5", nType5);
			
			jArray.add(obj);
		}*/

		Map res = new HashMap<>();
		res.put("rule_list", rule_list);
		res.put("emp_list", emp_list);

		return res;
	}
	
	// cpo 오남용 개인별 top10 차트
	@RequestMapping(value = "reportDetail2_chart4.html")
	@ResponseBody
	public Map reportDetail2_chart4(
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date,
			@RequestParam(value = "system_seq", required = false) String system_seq) {
		
	    List<String> syslist = new ArrayList<String>();
		String tmpList[] = system_seq.split(",");
		for ( int i=0; i< tmpList.length; i++ ) {
			syslist.add(tmpList[i]);
		}

		DateUtil dateUtil = new DateUtil();
		PrivacyReport pr = dateUtil.getInputDatePriReportVO(start_date, end_date);
		pr.setSysList(syslist);
		
		List<PrivacyReport> listEmpDetail = reportSvc.findEmpDetailList(pr);
		
		Map res = new HashMap<>();
		res.put("listEmpDetail", listEmpDetail);

		return res;
	}
	
	

	public List<Map<String, Object>> getReportDetail2_chart3(PrivacyReport pr, List list) {
		Map map = new HashMap<>();
		map.put("start_date", pr.getStart_date());
		map.put("end_date", pr.getEnd_date());
		map.put("sysList", pr.getSysList());
		
		//map.put("rule_list", list);

		List<Map<String, Object>> emp_list = reportDao.findReportDetail2_chart3(map);

		//List array = new ArrayList();
		/*String total = "";
		int[] sum = new int[list.size()];
		
		for(int i=0; i<sum.length; i++) {
			sum[i] = 0;
		}
		
		JSONArray jArray = new JSONArray();
		for (int i = 0; i < emp_list.size(); i++) {
			
			if(i > 4)
				break;
			
			Map data = emp_list.get(i);
			JSONObject obj = new JSONObject();
			
			data.put("start_date", pr.getStart_date());
			data.put("end_date", pr.getEnd_date());
			
			
			List<Map> emp_listDetail = reportDao.findReportDetail2_chart3Detail(data);
			
			int nType1=0;
			int nType2=0;
			int nType3=0;
			int nType4=0;
			int nType5=0;
			
			for ( int j=0; j<emp_listDetail.size(); ++j ) {
				
				Map dataTemp = emp_listDetail.get(j);
				
				if (dataTemp.get("scen_seq").equals(1000)) {
					nType1=1;
					obj.put("nType1", dataTemp.get("cnt"));
				}
				else if (dataTemp.get("scen_seq").equals(2000)) {
					nType2=1;
					obj.put("nType2", dataTemp.get("cnt"));
				}
				else if (dataTemp.get("scen_seq").equals(3000)) {
					nType3=1;
					obj.put("nType3", dataTemp.get("cnt"));
				}
				else if (dataTemp.get("scen_seq").equals(4000)) {
					nType4=1;
					obj.put("nType4", dataTemp.get("cnt"));
				}
				else if (dataTemp.get("scen_seq").equals(5000)) {
					nType5=1;
					obj.put("nType5", dataTemp.get("cnt"));
				}
			}
			
			obj.put("dept_name", data.get("dept_name"));
			if ( nType1 == 0 ) obj.put("nType1", nType1);
			if ( nType2 == 0 ) obj.put("nType2", nType2);
			if ( nType3 == 0 ) obj.put("nType3", nType3);
			if ( nType4 == 0 ) obj.put("nType4", nType4);
			if ( nType5 == 0 ) obj.put("nType5", nType5);
			
			jArray.add(obj);
		}
		
		Map res = new HashMap<>();
		res.put("array", jArray);
		res.put("sum", sum);*/

		return emp_list;
	}
	
	@RequestMapping(value = "reportHalf_chart2.html")
	@ResponseBody
	public List<PrivacyReport> reportHalf_chart2(
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {
		
		//JSONArray jArray = new JSONArray();

		String tmpList[] = system_seq.split(",");
		
		List<String> syslist = new ArrayList<String>();
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", ""));
		pr.setEnd_date(end_date.replaceAll("-", ""));
		pr.setSysList(syslist);
			
		List<PrivacyReport> data = reportSvc.findReportHalf_chart2(pr);
			
		return data;
	}
	
	@RequestMapping(value = "reportHalf_chart3.html")
	@ResponseBody
	public List<PrivacyReport> reportHalf_chart3(
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {
		
		String tmpList[] = system_seq.split(",");
		
		List<String> syslist = new ArrayList<String>();
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", ""));
		pr.setEnd_date(end_date.replaceAll("-", ""));
		pr.setSysList(syslist);
			
		List<PrivacyReport> data = reportSvc.findReportHalf_chart3(pr);
			
		return data;
	}
	
	@RequestMapping(value = "reportHalf_chart4.html")
	@ResponseBody
	public List<PrivacyReport> reportHalf_chart4(@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {
		
		String tmpList[] = system_seq.split(",");
		
		List<String> syslist = new ArrayList<String>();
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", ""));
		pr.setEnd_date(end_date.replaceAll("-", ""));
		pr.setSysList(syslist);
			
		List<PrivacyReport> data = reportSvc.findReportHalf_chart4(pr);
			
		return data;
	}
	
	@RequestMapping(value = "reportHalf_chart5.html")
	@ResponseBody
	public List<PrivacyReport> reportHalf_chart5(
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {
		
		String tmpList[] = system_seq.split(",");
		
		List<String> syslist = new ArrayList<String>();
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", ""));
		pr.setEnd_date(end_date.replaceAll("-", ""));
		pr.setSysList(syslist);
			
		List<PrivacyReport> data = reportSvc.findReportHalf_chart5(pr);
			
		return data;
	}
	
	@RequestMapping(value = "reportHalf_chart6.html")
	@ResponseBody
	public PrivacyReport reportHalf_chart6(
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date,
			@RequestParam(value = "half_type", required = false) String half_type) {
		
		String tmpList[] = system_seq.split(",");
		
		List<String> syslist = new ArrayList<String>();
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", ""));
		pr.setEnd_date(end_date.replaceAll("-", ""));
		pr.setSysList(syslist);
		pr.setHalf_type(half_type);
			
		PrivacyReport data = reportSvc.findReportHalf_chart6(pr);
			
		return data;
	}

	public List findReportDetailbyIndividual(PrivacyReport pr) {
		List<PrivacyReport> list = reportSvc.findReportDetailbyIndividual(pr);
		for(int i=0; i<list.size(); i++) {
			PrivacyReport report = list.get(i);
			String ruleStr = getExtraBaseSetupStr(report.getRule_cd());
			report.setRule_cd(ruleStr);
		}
		
		return list;
	}
	
	public String getExtraBaseSetupStr(String rule) {
		String subRule = rule.substring(0, 1);
		String resRule = "";
		switch (subRule) {
		case "1":
			resRule = "특정인 처리";
			break;
		case "2":
			resRule = "과다처리";
			break;
		case "3":
			resRule = "특정시간대 처리";
			break;
		case "4":
			resRule = "비정상 접근";
			break;
		default:
			resRule = rule;
			break;
		}

		return resRule;
	}
	
	public int getDiffMonth(PrivacyReport pr) {
		int sYear = Integer.parseInt(pr.getStart_date().substring(0, 4));
		int sMonth = Integer.parseInt(pr.getStart_date().substring(4, 6));
		
		int eYear = Integer.parseInt(pr.getEnd_date().substring(0, 4));
		int eMonth = Integer.parseInt(pr.getEnd_date().substring(4, 6));
		
		return (eYear - sYear) * 12 + (eMonth - sMonth) +1;
	}
	
	public String getMonday(String yyyy,String mm, String wk){

 		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy.MM.dd");
 		Calendar c = Calendar.getInstance();
 		
 		int y=Integer.parseInt(yyyy);
 		int m=Integer.parseInt(mm)-1;
 		int w=Integer.parseInt(wk);

 		c.set(Calendar.YEAR,y);
 		c.set(Calendar.MONTH,m);
 		c.set(Calendar.WEEK_OF_MONTH,w);
 		c.set(Calendar.DAY_OF_WEEK,Calendar.MONDAY);
 		return formatter.format(c.getTime());
 	}
	
	
	
	// ----------------------------------------------------------------------
	// 고유식별정보 처리 현황 보고 chart
	//
	// ----------------------------------------------------------------------
	@RequestMapping(value = "reportDetail4_chart1_new.html")
	@ResponseBody
	public JSONArray reportDetail4_chart1_new(
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {

		JSONArray jArray = new JSONArray();
		List<String> syslist = new ArrayList<String>();
		String tmpList[] = system_seq.split(",");
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}
		
		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", "").substring(0, 6));
		pr.setEnd_date(end_date.replaceAll("-", "").substring(0, 6));
		pr.setSysList(syslist);
		
		List<PrivacyReport> listByResultType = reportSvc.findTypeReportCount(pr);
		for (int i = 0; i < listByResultType.size(); i++) {
			JSONObject jsonObj = new JSONObject();
			PrivacyReport chart4Report = listByResultType.get(i);

			jsonObj.put("category", chart4Report.getPrivacy_desc());
			jsonObj.put("value", chart4Report.getCnt());
			jArray.add(jsonObj);
		}

		return jArray;
	}

	@RequestMapping(value = "reportDetail4_chart2_new.html")
	@ResponseBody
	public JSONArray reportDetail4_chart2_new(
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {

		JSONArray jArray = new JSONArray();
		List<String> syslist = new ArrayList<String>();
		String tmpList[] = system_seq.split(",");
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", "").substring(0, 6));
		pr.setEnd_date(end_date.replaceAll("-", "").substring(0, 6));
		pr.setSysList(syslist);

		List<PrivacyReport> listBySystem = reportSvc.findSystemReportCount(pr);
		for (int i = 0; i < listBySystem.size(); i++) {
			JSONObject jsonObj = new JSONObject();
			PrivacyReport report = listBySystem.get(i);

			jsonObj.put("category", report.getSystem_name());
			jsonObj.put("type1", report.getType1());
			jsonObj.put("type2", report.getType2());
			jsonObj.put("type3", report.getType3());
			jsonObj.put("type10", report.getType10());
			jArray.add(jsonObj);
		}

		return jArray;
	}

	@RequestMapping(value = "reportDetail4_chart3_new.html")
	@ResponseBody
	public JSONArray reportDetail4_chart3_new(
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {

		JSONArray jArray = new JSONArray();
		List<String> syslist = new ArrayList<String>();
		String tmpList[] = system_seq.split(",");
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", "").substring(0, 6));
		pr.setEnd_date(end_date.replaceAll("-", "").substring(0, 6));
		pr.setSysList(syslist);

		List<PrivacyReport> listByDept = reportSvc.findDeptReportCount(pr);
		for (int i = 0; i < listByDept.size(); i++) {
			JSONObject jsonObj = new JSONObject();
			PrivacyReport report = listByDept.get(i);

			jsonObj.put("category", report.getDept_name());
			jsonObj.put("type1", report.getType1());
			jsonObj.put("type2", report.getType2());
			jsonObj.put("type3", report.getType3());
			jsonObj.put("type10", report.getType10());
			jArray.add(jsonObj);
		}

		return jArray;
	}

	@RequestMapping(value = "reportDetail4_chart4_new.html")
	@ResponseBody
	public JSONArray reportDetail4_chart4_new(
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {

		JSONArray jArray = new JSONArray();
		List<String> syslist = new ArrayList<String>();
		String tmpList[] = system_seq.split(",");
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", "").substring(0, 6));
		pr.setEnd_date(end_date.replaceAll("-", "").substring(0, 6));
		pr.setSysList(syslist);

		List<PrivacyReport> listByIndv = reportSvc.findIndvReportCount(pr);
		for (int i = 0; i < listByIndv.size(); i++) {
			JSONObject jsonObj = new JSONObject();
			PrivacyReport report = listByIndv.get(i);

			jsonObj.put("category", report.getEmp_user_name());
			jsonObj.put("type1", report.getType1());
			jsonObj.put("type2", report.getType2());
			jsonObj.put("type3", report.getType3());
			jsonObj.put("type10", report.getType10());
			jArray.add(jsonObj);
		}

		return jArray;
	}
	@ResponseBody
	@RequestMapping(value="getImages.html", method={RequestMethod.POST})
	public String getImages(@RequestParam Map<String, String> parameters,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		String rootPath = request.getSession().getServletContext().getRealPath("/");
		String attach_path = "";
		String report_type = parameters.get("type");
		String result="";
		if(report_type.equals("1"))
			attach_path = "resources/chartImages/";
		else if(report_type.equals("2"))
			attach_path = "resources/chartImages2/";
		else if(report_type.equals("3"))
			attach_path = "resources/chartImages3/";
		else if(report_type.equals("4"))
			attach_path = "resources/chartImages4/";
		else if(report_type.equals("5"))			//권한부여_보고
			attach_path = "resources/chartImages5/";
	    
		String savePath = rootPath + attach_path;
		
		File fi = new File(savePath);
		if(!fi.exists())
			fi.mkdirs();
		
		for(String key : parameters.keySet()) {
			if(key.equals("type"))
				continue;
			try {
				String data = parameters.get(key);
				if(data != null && data != "") {
					data = data.split(",")[1];
					
					byte[] imageBytes = DatatypeConverter.parseBase64Binary(data);
					BufferedImage bufImg = ImageIO.read(new ByteArrayInputStream(imageBytes));
					
					String filename = key + ".png";
					fi = new File(savePath + filename);
					
					result=savePath+filename;
					//
					 //BufferedImage bufImg = ImageIO.read(fi);
				        
				        // java-image-scaling 라이브러리
				        MultiStepRescaleOp rescale = new MultiStepRescaleOp(580, 200);
				        rescale.setUnsharpenMask(AdvancedResizeOp.UnsharpenMask.Soft);
				        
				        BufferedImage resizedImage = rescale.filter(bufImg, null);
					//
					
					//ImageIO.write(bufImg, "png", fi);
					ImageIO.write(resizedImage, "png", fi);
					result=savePath+filename;
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
	@RequestMapping(value="uploadImage1.html",method={RequestMethod.POST})
	public DataModelAndView uploadFile1(@RequestParam Map<String, String> parameters, 
			MultipartHttpServletRequest multi){
		
		DataModelAndView modelAndView = reportSvc.addFile(multi, 1);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
	    return modelAndView;
	}
	
	@RequestMapping(value="uploadImage2.html",method={RequestMethod.POST})
	public DataModelAndView uploadFile2(@RequestParam Map<String, String> parameters, 
			MultipartHttpServletRequest multi){
		
		DataModelAndView modelAndView = reportSvc.addFile(multi, 2);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
	    return modelAndView;
	}
	
	@MngtActHist(log_action = "SELECT", log_message = "[정기점검] 점검보고서 생성")
	@RequestMapping(value="report_inspection")
	public DataModelAndView report_inspection(
			@RequestParam(value = "search_fr", required = false) String search_fr
			,@RequestParam(value = "search_to", required = false) String search_to
			,@RequestParam(value = "report_seq", required = false) String report_seq
			) {
		DataModelAndView modelAndView = null;
		if(report_seq != null) {
			modelAndView = reportSvc.report_inspection(report_seq);
		}else {
			modelAndView = reportSvc.report_inspection(search_fr,search_to);
		}
	    modelAndView.setViewName("report_inspection_sw");
		
		return modelAndView;
	}
	
	@RequestMapping(value="report_inspection_hw")
	public DataModelAndView report_inspection_hw(@RequestParam String search_fr, @RequestParam String search_to) {
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		search_fr = search_fr.replaceAll("-", "");
		search_to = search_to.replaceAll("-", "");
		
		Calendar cal = sdf.getCalendar();
		try {
			cal.setTime(sdf.parse(search_fr));
			String frDate = sdf.format(cal.getTime());
			cal.add(Calendar.MONTH, -1);
			String frDate2 = sdf.format(cal.getTime());
			
			cal.setTime(sdf.parse(search_to));
			String toDate = sdf.format(cal.getTime());
			cal.add(Calendar.MONTH, -1);
			String toDate2 = sdf.format(cal.getTime());
			
			PrivacyReport pr = new PrivacyReport();
			pr.setStart_date(frDate);
			pr.setEnd_date(toDate);
			List<PrivacyReport> list = reportDao.findReport_inspection(pr);
			
			pr.setStart_date(frDate2);
			pr.setEnd_date(toDate2);
			List<PrivacyReport> list2 = reportDao.findReport_inspection(pr);
			
			for(int i=0; i<list.size(); i++) {
				PrivacyReport report = list.get(i);
				report.setType2(list2.get(i).getType1());
			}
			
			modelAndView.addObject("frDate", frDate);
			modelAndView.addObject("frDate2", frDate2);
			modelAndView.addObject("toDate", toDate);
			modelAndView.addObject("toDate2", toDate2);
			modelAndView.addObject("list", list);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		modelAndView.addObject("today", sdf.format(new Date()));
		//modelAndView.setViewName("report_inspection");
		modelAndView.setViewName("report_inspection_hw");
		
		return modelAndView;
	}
	
	@RequestMapping(value="download.html",method={RequestMethod.POST})
	public DataModelAndView addPrintReport(@RequestParam Map<String, String> parameters){
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
	    return modelAndView;
	}
	
	/**
	 * @page : reportDetail_download.html
	 * @desc : 개인정보 다운로드_보고(type_6)
	 * @auth : syjung
	 */
	@RequestMapping(value = "reportDetail_download.html")
	public ModelAndView findReportDetailDownload(HttpServletRequest request,
			@RequestParam(value = "type", required = false) int type,
			@RequestParam(value = "period_type", required = false) int period_type,
			@RequestParam(value = "system_seq", required = false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date,
			@RequestParam(value = "menu_id", required = false) String menu_id,
			@RequestParam(value = "download_type", required = false) String download_type) {
		
		HttpSession session = request.getSession();
		// 다운로드로그 풀스캔연동여부
		String use_fullscan = (String) session.getAttribute("use_fullscan");
		
		ModelAndView modelAndView = new ModelAndView();
		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
		String date = dayTime.format(new Date(time));
		String ui_type = commonDao.getUiType();

		// 마지막날짜가 현재보다 클 경우 어제날짜까지로 지정해준다.
		/*
		try {
			Date tmpDate = dayTime.parse(end_date);
			int compare = tmpDate.compareTo(new Date());
			if (compare >= 0) {
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DAY_OF_MONTH, -1);
				end_date = dayTime.format(cal.getTime());
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		*/
		String compareDate = "";

		PrivacyReport pr = new PrivacyReport(); // 선택 기간
		PrivacyReport prCompare = new PrivacyReport(); // 비교 기간

		
		String master = (String) session.getAttribute("master");

		String use_reportLogo = (String) session.getAttribute("use_reportLogo");
		modelAndView.addObject("use_reportLogo", use_reportLogo);
		if (use_reportLogo != null && use_reportLogo.equals("Y")) {
			String rootPath = request.getSession().getServletContext().getRealPath("/");
			String attach_path = "resources/upload/";

			String filename = commonDao.selectCurrentImage_logo2();
			String savePath = rootPath + attach_path + filename;

			File file = new File(savePath);

			if (file.exists()) {
				modelAndView.addObject("filename", filename);
			}
		}

		String use_reportLine = (String) session.getAttribute("use_reportLine");
		
		modelAndView.addObject("use_reportLine", use_reportLine);
		modelAndView.addObject("masterflag", master);
		modelAndView.addObject("report_type", type);
		modelAndView.addObject("period_type", period_type);
		modelAndView.addObject("start_date", start_date);
		modelAndView.addObject("end_date", end_date);
		modelAndView.addObject("date", date);
		modelAndView.addObject("system_seq", system_seq);
		modelAndView.addObject("use_studentId", use_studentId);
		modelAndView.addObject("logo_report_url", logo_report_url);
		modelAndView.addObject("menu_id", menu_id);

		List<String> syslist = new ArrayList<String>();
		String tmpList[] = system_seq.split(",");

		for (int i = 0; i < tmpList.length; ++i) {
			syslist.add(tmpList[i]);
		}

		String sDate[] = start_date.split("-");
		String eDate[] = end_date.split("-");

		int strYear = Integer.parseInt(sDate[0]);
		int strMonth = Integer.parseInt(sDate[1]);
		int endYear = Integer.parseInt(eDate[0]);
		int endMonth = Integer.parseInt(eDate[1]);

		int diffMon = (endYear - strYear) * 12 + (endMonth - strMonth);

		String stDate = start_date.replaceAll("-", "");
		String edDate = end_date.replaceAll("-", "");
		
		pr.setStart_date(stDate);
		pr.setEnd_date(edDate);
		pr.setSysList(syslist);
		
		//start, end date의 차이 개월
		int diffMonTemp = getDiffMonth(pr);
		modelAndView.addObject("diffMon", diffMonTemp);
		//다운로드 총 횟수
		int downCnt = reportSvc.findDownCountTot(pr);
		modelAndView.addObject("downCnt", downCnt);
		//시스템별 다운로드 총 횟수가 젤 많은 시스템명 가져옴
		String downBySysCntName = reportSvc.findDownBySysCountTot(pr);
		modelAndView.addObject("downBySysCntName", downBySysCntName);
		//점검 대상 다운로드 개인정보 처리시스템 가져오기
		if(!pr.getSysList().isEmpty()) {
			List<SystemMaster> systemsDownLst = reportSvc.findSystemMasterDetailDownList(pr);
			for(SystemMaster sm : systemsDownLst) {
				PrivacyReport prt = new PrivacyReport();
				prt.setSystem_seq(sm.getSystem_seq());
				prt.setStart_date(stDate);
				prt.setEnd_date(edDate);
				List<PrivacyReport> resDownLst = reportDao.getMajorDownResultType(prt);
				String result_type = "";
				if (resDownLst.size() > 0) {
					for(PrivacyReport prDownVo : resDownLst) {
						String text = prDownVo.getPrivacy_desc()  + "(" + prDownVo.getCnt() + ") ";
						result_type += text;
					}
				} else {
					result_type = "데이터없음";
				}
				sm.setResult_type(result_type);
			}
			modelAndView.addObject("systemsDownLst", systemsDownLst);
		}
		
		Calendar cal = Calendar.getInstance();
		cal.set(strYear, strMonth - 1, 1); // 선택기간 시작월 1일

		cal.add(Calendar.MONTH, -1); // 비교 기간 끝(선택기간 한달전)
		compareDate = dateFormatter.format(cal.getTime());
		edDate = compareDate.substring(0, 6) + "31";

		cal.add(Calendar.MONTH, -diffMon); // 비교 기간 시작
		compareDate = dateFormatter.format(cal.getTime());
		stDate = compareDate.substring(0, 6) + "01";

		prCompare.setStart_date(stDate);
		prCompare.setEnd_date(edDate);
		prCompare.setSysList(syslist);
		
		//1.시스템별 현황 TOP5
		List<PrivacyReport> findDownloadBySysTop5 = reportSvc.findDownloadCntBySysTop5(pr);
		modelAndView.addObject("findDownloadBySysTop5",findDownloadBySysTop5);
		//2.부서별 현황 TOP5
		List<PrivacyReport> findDownloadByDeptTop5 = reportSvc.findDownloadCntByDeptTop5(pr);
		modelAndView.addObject("findDownloadByDeptTop5",findDownloadByDeptTop5);
		//3.개인정보 유형별 현황 TOP5
		List<PrivacyReport> findDownloadCntByResultTypeTop5 = reportSvc.findDownloadCntByResultTypeTop5(pr);
		modelAndView.addObject("findDownloadCntByResultTypeTop5",findDownloadCntByResultTypeTop5);
		
		
		//상세결과
		//1.시스템별 개인정보 다운로드 현황
		List<PrivacyReport> findDownloadCntBySys = reportSvc.findDownloadCntBySys(pr);	//시스템별 개인정보 다운로드 횟수
		List<PrivacyReport> findResultCntBySys = reportSvc.findResultCntBySys(pr);		//시스템별 개인정보 건수
		
		for (int i = 0; i < findDownloadCntBySys.size(); i++){
			
			for(int j=0; j< findResultCntBySys.size(); j++){
				if(findDownloadCntBySys.get(i).getSystem_seq() == findResultCntBySys.get(j).getSystem_seq() ||
						findDownloadCntBySys.get(i).getSystem_seq().equals(findResultCntBySys.get(j).getSystem_seq())){
					findDownloadCntBySys.get(i).setCnt2(findResultCntBySys.get(j).getCnt2());
				}
			}
		}
		modelAndView.addObject("findDownloadCntBySys",findDownloadCntBySys);
		
		//2.주요 부서별 개인정보 다운로드 현황 TOP30
		List<PrivacyReport> findDownloadCntByDeptTop30 = reportSvc.findDownloadCntByDeptTop30(pr);	//주요 부서별 개인정보 다운로드 현황 TOP30
		List<PrivacyReport> findResultCntByDeptTop30 = reportSvc.findResultCntByDeptTop30(pr);		//주요 부서별 개인정보 건수 TOP30
		
		for (int i = 0; i < findDownloadCntByDeptTop30.size(); i++){
			
			for(int j=0; j< findResultCntByDeptTop30.size(); j++){
				if(findDownloadCntByDeptTop30.get(i).getDept_id() == findResultCntByDeptTop30.get(j).getDept_id() ||
						findDownloadCntByDeptTop30.get(i).getDept_id().equals(findResultCntByDeptTop30.get(j).getDept_id())){
					findDownloadCntByDeptTop30.get(i).setCnt2(findResultCntByDeptTop30.get(j).getCnt2());
				}
			}
		}
		modelAndView.addObject("findDownloadCntByDeptTop30",findDownloadCntByDeptTop30);
		
		
		//3.주요 사용자별 개인정보 다운로드 현황 TOP10
		List<PrivacyReport> findDownloadCntByUserTop10 = reportSvc.findDownloadCntByUserTop10(pr);	//주요 사용자별 개인정보 다운로드 현황 TOP10
		List<PrivacyReport> findResultCntByUserTop10 = reportSvc.findResultCntByUserTop10(pr);		//주요 사용자별 개인정보 건수 TOP10
		List<PrivacyReport> findSystemNameByUserTop10 = reportSvc.findSystemNameByUserTop10(pr);	//주요 사용자 접근 시스템
		
		
		for (int i = 0; i < findDownloadCntByUserTop10.size(); i++){
			
			for(int j=0; j< findResultCntByUserTop10.size(); j++){
				if(findDownloadCntByUserTop10.get(i).getEmp_user_id() == findResultCntByUserTop10.get(j).getEmp_user_id() ||
						findDownloadCntByUserTop10.get(i).getEmp_user_id().equals(findResultCntByUserTop10.get(j).getEmp_user_id())){
					findDownloadCntByUserTop10.get(i).setCnt2(findResultCntByUserTop10.get(j).getCnt2());
				}
			}
			
			for(int j=0; j< findSystemNameByUserTop10.size(); j++){
				if(findDownloadCntByUserTop10.get(i).getEmp_user_id() == findSystemNameByUserTop10.get(j).getEmp_user_id() ||
						findDownloadCntByUserTop10.get(i).getEmp_user_id().equals(findSystemNameByUserTop10.get(j).getEmp_user_id())){
					if(findDownloadCntByUserTop10.get(i).getSystem_name()=="" || findDownloadCntByUserTop10.get(i).getSystem_name().equals("")){
						findDownloadCntByUserTop10.get(i).setSystem_name(findSystemNameByUserTop10.get(j).getSystem_name());
					}else{
					findDownloadCntByUserTop10.get(i).setSystem_name(findDownloadCntByUserTop10.get(i).getSystem_name()+", "+findSystemNameByUserTop10.get(j).getSystem_name());
					}
				}
			}
		}
		modelAndView.addObject("findDownloadCntByUserTop10",findDownloadCntByUserTop10);
		
		//4.주요 URL별 다운로드 현황 TOP5
		List<PrivacyReport> findDownloadCntByUrlTop5 = reportSvc.findDownloadCntByUrlTop5(pr);	//주요 URL별 다운로드 현황 TOP5
		List<PrivacyReport> findResultCntByUrlTop5 = reportSvc.findResultCntByUrlTop5(pr);		//주요 URL별 개인정보 건수 TOP5
		
		for (int i = 0; i < findDownloadCntByUrlTop5.size(); i++){
			
			for(int j=0; j< findResultCntByUrlTop5.size(); j++){
				if(findDownloadCntByUrlTop5.get(i).getRequrl() == findResultCntByUrlTop5.get(j).getRequrl() ||
						findDownloadCntByUrlTop5.get(i).getRequrl().equals(findResultCntByUrlTop5.get(j).getRequrl())){
					findDownloadCntByUrlTop5.get(i).setCnt2(findResultCntByUrlTop5.get(j).getCnt2());
				}
			}
		}
		modelAndView.addObject("findDownloadCntByUrlTop5",findDownloadCntByUrlTop5);
		
		//5.다운로드한 주요 개인정보 유형 TOP10
		List<PrivacyReport> findDownloadCntByResultTop10 = reportSvc.findDownloadCntByResultTop10(pr);	//주요 개인정보 유형별 다운로드한 현황 TOP10
		List<PrivacyReport> findResultCntByResultTop10 = reportSvc.findResultCntByResultTop10(pr);		//주요 개인정보 유형별 개인정보 건수 TOP10 
		
		for (int i = 0; i < findDownloadCntByResultTop10.size(); i++){
			
			for(int j=0; j< findResultCntByResultTop10.size(); j++){
				if(findDownloadCntByResultTop10.get(i).getResult_type() == findResultCntByResultTop10.get(j).getResult_type() ||
						findDownloadCntByResultTop10.get(i).getResult_type().equals(findResultCntByResultTop10.get(j).getResult_type())){
					findDownloadCntByResultTop10.get(i).setCnt2(findResultCntByResultTop10.get(j).getCnt2());
				}
			}
		}
		modelAndView.addObject("findDownloadCntByResultTop10",findDownloadCntByResultTop10);
		
		//6.주요 사유별 개인정보 다운로드 건수 TOP10
		List<PrivacyReport> findDownloacCntByReasonTop10 = reportSvc.findDownloacCntByReasonTop10(pr);	//주요 사유별 개인정보 다운로드 현황 TOP10
		List<PrivacyReport> findResultCntByReasonTop10 = reportSvc.findResultCntByReasonTop10(pr);		//주요 사유별 개인정보 건수 TOP10
		
		for (int i = 0; i < findDownloacCntByReasonTop10.size(); i++){
			
			for(int j=0; j< findResultCntByReasonTop10.size(); j++){
				if(findDownloacCntByReasonTop10.get(i).getReason_code() == findResultCntByReasonTop10.get(j).getReason_code() ||
						findDownloacCntByReasonTop10.get(i).getReason_code().equals(findResultCntByReasonTop10.get(j).getReason_code())){
					findDownloacCntByReasonTop10.get(i).setCnt2(findResultCntByReasonTop10.get(j).getCnt2());
				}
			}
		}
		modelAndView.addObject("findDownloacCntByReasonTop10",findDownloacCntByReasonTop10);		
		
		modelAndView.addObject("download_type", download_type);
		String pdfFullPath = request.getRequestURL() +"?"+ request.getQueryString();
		modelAndView.addObject("pdfFullPath", pdfFullPath);
		
		modelAndView.addObject("use_fullscan", use_fullscan);
		modelAndView.setViewName("report_download");
		
		return modelAndView;
	}
	
	//시스템별 다운로드 현황 TOP5
	@RequestMapping(value = "reportDetail_chart11_1.html")
	@ResponseBody
	public JSONArray reportDetail_chart11_1(
			@RequestParam(value = "system_seq", required = false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {
		
		JSONArray jArray = new JSONArray();
		String tmpList[] = system_seq.split(",");
		List<String> syslist = new ArrayList<String>();

		for (int i = 0; i < tmpList.length; ++i) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", ""));
		pr.setEnd_date(end_date.replaceAll("-", ""));
		pr.setSysList(syslist);

		List<PrivacyReport> reportBySys = reportSvc.findDownloadCntBySysTop5(pr);		

		for (int i = 0; i < reportBySys.size(); i++) {
			JSONObject jsonObj = new JSONObject();
			PrivacyReport report = reportBySys.get(i);

			jsonObj.put("system_seq", report.getSystem_seq());
			jsonObj.put("system_name", report.getSystem_name());
			jsonObj.put("cnt", report.getCnt());
			jArray.add(jsonObj);
		}

		return jArray;
	}
	
	//부서별 개인정보 다운로드 현황Top5
	@RequestMapping(value = "reportDetail_chart11_2.html")
	@ResponseBody
	public JSONArray reportDetail_chart11_2(
			@RequestParam(value = "system_seq", required = false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {

		JSONArray jArray = new JSONArray();
		String tmpList[] = system_seq.split(",");
		List<String> syslist = new ArrayList<String>();

		for (int i = 0; i < tmpList.length; ++i) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", ""));
		pr.setEnd_date(end_date.replaceAll("-", ""));
		pr.setSysList(syslist);
		
		List<PrivacyReport> reportByDept = new ArrayList<>();
		reportByDept = reportSvc.findDownloadCntByDeptTop5(pr);

		for (int i = 0; i < reportByDept.size(); i++) {
			JSONObject jsonObj = new JSONObject();
			PrivacyReport report = reportByDept.get(i);
			
			jsonObj.put("dept_name", report.getDept_name());
			jsonObj.put("cnt", report.getCnt());
			jArray.add(jsonObj);
		}

		return jArray;
	}
	
	//개인정보 유형별 개인정보 다운로드 현황 Top5
	@RequestMapping(value = "reportDetail_chart11_3.html")
	@ResponseBody
	public JSONArray reportDetail_chart11_3(
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {

		JSONArray jArray = new JSONArray();
		String tmpList[] = system_seq.split(",");		
		List<String> syslist = new ArrayList<String>();
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", ""));
		pr.setEnd_date(end_date.replaceAll("-", ""));
		pr.setSysList(syslist);

		List<PrivacyReport> listByResultType = reportSvc.findDownloadCntByResultTypeTop5(pr);

		for (int i = 0; i < listByResultType.size(); i++) {
			JSONObject jsonObj = new JSONObject();
			PrivacyReport report = listByResultType.get(i);
			
			jsonObj.put("result_type", report.getResult_type());
			jsonObj.put("cnt", report.getCnt());
			jArray.add(jsonObj);
		}		
		
		return jArray;
	}
	
	@RequestMapping(value = "reportManagement.html", method = { RequestMethod.POST })
	public DataModelAndView findReportManagement(@RequestParam Map<String, String> parameters, @ModelAttribute("search") SearchSearch search, HttpServletRequest request) {

		parameters = CommonHelper.checkSearchDateByWeek(parameters);
		
		SimpleCode simpleCode = new SimpleCode();
		String index = "/report/reportManagement.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = reportSvc.findReportList(search, request);
		
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search",search);
		
		modelAndView.setViewName("reportManagement");
		
		return modelAndView;
	}
	@MngtActHist(log_action = "SELECT", log_message = "[통계보고] 정기점검보고서")
	@RequestMapping(value = "reportManagement_new.html", method = { RequestMethod.POST })
	public DataModelAndView findReportManagement_new(@RequestParam Map<String, String> parameters, @ModelAttribute("search") SearchSearch search, HttpServletRequest request) {

		parameters = CommonHelper.checkSearchDateByWeek(parameters);
		
		SimpleCode simpleCode = new SimpleCode();
		String index = "/report/reportManagement_new.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		Map<String, String> reportMap = reportSvc.findReportCode();
		
		DataModelAndView modelAndView = reportSvc.findReportList(search, request);
		
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("reportMap", reportMap);
		modelAndView.addObject("search",search);
		
		modelAndView.setViewName("reportManagement_new");
		
		return modelAndView;
	}
	
	@ResponseBody
	@RequestMapping(value = "reportListDelete.html", method = { RequestMethod.POST })
	public long ReportListDelete(@RequestParam Map<String, String> parameters, @ModelAttribute("search") SearchBase search) {
		
		parameters = CommonHelper.checkSearchDateByWeek(parameters);
		DataModelAndView modelAndView = new DataModelAndView();
		long report_seq = Long.parseLong(parameters.get("report_seq"));
		return reportSvc.removeReportDelete(report_seq);
	}
	
	@ResponseBody
	@RequestMapping(value="systemReportDelete.html", method=RequestMethod.POST)
	public int systemReportDelete(@RequestParam Map<String, String> parameters, @ModelAttribute("search") Report search) {
		int result = 1;
		parameters = CommonHelper.checkSearchDateByWeek(parameters);
		try {
 			reportSvc.systemReportDelete(search);
		} catch (Exception e) {
			result = 0;
			e.printStackTrace();
		}
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "reportDownload.html", method = { RequestMethod.POST })
	public String ReportDownload(@RequestParam Map<String, String> parameters, @ModelAttribute("search") SearchBase search) {
		
		parameters = CommonHelper.checkSearchDateByWeek(parameters);
		DataModelAndView modelAndView = new DataModelAndView();
		
		long report_seq = Long.parseLong(parameters.get("report_seq"));
		
		return reportSvc.saveReportDownloadTime(report_seq);
	}
	
	@RequestMapping(value="addWord.html", method={RequestMethod.POST})
	@ResponseBody
	public int addWord(@RequestParam Map<String, String> parameters,HttpServletRequest request) {
		
		int result = 0;
		String html_encode = RequestWrapper.decodeXSS(parameters.get("source"));
		String admin_user_id = parameters.get("admin_user_id");
		String log_message_title = RequestWrapper.decodeXSS(parameters.get("log_message_title"));
		String report_seq = parameters.get("report_seq");
		AdminUser adminUser = (AdminUser) request.getSession().getAttribute("userSession");
        String authType = (String) request.getSession().getAttribute("report_auth_type");
		if(admin_user_id == null) {
			admin_user_id = adminUser.getAdmin_user_id();
		}
		String make_report_auth = adminUserMngtDao.findAdminUserMngtOne(admin_user_id).getMake_report_auth();
		if (make_report_auth.equals("N")) {
			return 4;
		}
		File file = null;
		FileWriter fw = null;
		String filePath2 = "";
		Report report = new Report();
		
		
		try {
			String OS = "";
			String filePath = "";
			OS = System.getProperty("os.name").toLowerCase();
			if (OS.contains("win")) {
				filePath = report_path; // 폴더 경로
			} else if (OS.contains("nux")) {
				filePath = report_path;
			}
			System.out.println("  >>>>  reprt seq : " + parameters.get("report_seq"));
			System.out.println("  >>>>  system_seq : " + parameters.get("system_seq"));
			System.out.println("  >>>> period type : " + parameters.get("period_type"));
			String sysName = "";
			//시스템 보고서 -> 시스템 별 src 파일 생성
			if("Y".equals(parameters.get("system_report"))) {
				String tmpList[]= parameters.get("system_seq").split(",");
				for(int i=0;i<tmpList.length;i++) {
					report.setSystem_seq(tmpList[i]);
					report.setSystem_name(reportDao.getSystem_name(tmpList[i]));
					report.setProc_month(parameters.get("proc_date"));
					report.setReport_type(parameters.get("report_type"));
					filePath2 = filePath + parameters.get("proc_date") + File.separator 
							+ adminUser.getAdmin_user_id() + adminUser.getAuth_id() + authType + "__"+ log_message_title + "_" + report.getSystem_name() + ".doc.src";
					
					File file1 = new File(filePath + parameters.get("proc_date") + File.separator);
					
					if(!file1.exists()) {
						file1.mkdirs();
					}
					
					file = new File(filePath2);
					fw = new FileWriter(file);
					fw.write(html_encode);
					report.setHtml_encode(filePath2);
					report.setPeriod_type(parameters.get("period_type"));
					//report_Seq 값 가져오기
					//	- auth id 별 생성 system_seq 반환
					report.setAuth_id(adminUser.getAuth_id());
					
					report.setReport_seq(reportDao.getReportSeq(report));
					System.out.println("html_encode :" + report.getHtml_encode());
					System.out.println("report_seq :" + report.getReport_seq());
					System.out.println("proc_month :" + report.getProc_month());
					System.out.println("system_seq :" + report.getSystem_seq());
					System.out.println("report_type :" + report.getReport_type());
					reportDao.updateWord(report);
				}
				result = 3;
				return result;
			}
			
			filePath2 = filePath + parameters.get("proc_date") + File.separator + adminUser.getAdmin_user_id() + adminUser.getAuth_id() + authType + "__"+ log_message_title + ".doc.src";
			File file1 = new File(filePath + parameters.get("proc_date") + File.separator);
			
			if(!file1.exists()) {
				file1.mkdirs();
			}
			file = new File(filePath2);
			fw = new FileWriter(file);
			fw.write(html_encode);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				logger.debug("소스파일 생성 종료 : [ "+filePath2+" ] fileSize : "+ file.length());
				fw.close();
				} catch (IOException e) {e.printStackTrace();}
		}
		Date date = new Date();
		String fileName = "";
		
		report.setHtml_encode(filePath2);
		report.setReport_seq(report_seq);
		
		try {
			reportDao.updateWord(report);
			result = 3;
			//process = pb.start();
		} catch (Exception e) {
			e.printStackTrace();
			result = 0;
		}
		return result;
	}
	
	
	/*@RequestMapping(value="addWord.html", method={RequestMethod.POST})
	@ResponseBody
	public int addWord(@RequestParam Map<String, String> parameters,HttpServletRequest request) {
		
		int result = 0;
		String html_encode = RequestWrapper.decodeXSS(parameters.get("source"));
		String admin_user_id = parameters.get("admin_user_id");
		String log_message_title = RequestWrapper.decodeXSS(parameters.get("log_message_title"));
		String report_seq = parameters.get("report_seq");
		AdminUser adminUser = (AdminUser) request.getSession().getAttribute("userSession");
        String authType = (String) request.getSession().getAttribute("report_auth_type");
		if(admin_user_id == null) {
			admin_user_id = adminUser.getAdmin_user_id();
		}
		String make_report_auth = adminUserMngtDao.findAdminUserMngtOne(admin_user_id).getMake_report_auth();
		if (make_report_auth.equals("N")) {
			return 4;
		}
		File file = null;
		FileWriter fw = null;
		String filePath2 = "";
		try {
			String OS = "";
			String filePath = "";
			OS = System.getProperty("os.name").toLowerCase();
			if (OS.contains("win")) {
				filePath = report_path; // 폴더 경로
			} else if (OS.contains("nux")) {
				filePath = report_path;
			}
			
			
			filePath2 = filePath + parameters.get("proc_date") + File.separator + adminUser.getAdmin_user_id() + adminUser.getAuth_id() + authType + "__"+ log_message_title + ".doc.src";
			File file1 = new File(filePath + parameters.get("proc_date") + File.separator);
			
			if(!file1.exists()) {
				file1.mkdirs();
			}
			file = new File(filePath2);
			fw = new FileWriter(file);
			fw.write(html_encode);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				logger.debug("소스파일 생성 종료 : [ "+filePath2+" ] fileSize : "+ file.length());
				fw.close();
				} catch (IOException e) {e.printStackTrace();}
		}
		Date date = new Date();
		String fileName = "";
		
		Report report = new Report();
		report.setHtml_encode(filePath2);
		report.setReport_seq(report_seq);
		
		try {
			reportDao.updateWord(report);
			result = 3;
			//process = pb.start();
		} catch (Exception e) {
			e.printStackTrace();
			result = 0;
		}
		return result;
	}*/
	
	@RequestMapping(value="addReport.html", method={RequestMethod.POST})
	@ResponseBody
	public int addReport(@RequestParam Map<String, String> parameters,HttpServletRequest request) {
		
		int result = 0;
		System.out.println("[addReport.html]");
		String html_encode = RequestWrapper.decodeXSS(parameters.get("source"));
		String admin_user_id = parameters.get("admin_user_id");
		String log_message_title = RequestWrapper.decodeXSS(parameters.get("log_message_title"));
		String report_type = parameters.get("reportType");
		String proc_date = parameters.get("proc_date");
		String period_type = parameters.get("period_type");
		
        String pdfFullPath = RequestWrapper.decodeXSS(parameters.get("pdfFullPath"))+"&download_type=pdf";
        System.out.println("pdf full path : " + pdfFullPath);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMM");
		Date date = new Date();
		String fileName = "";
		
		if(admin_user_id == null) {
			admin_user_id = SessionManager.getInstance().getUserID(request.getSession()).split("/")[0];
		}
        pdfUtil.setPdfConfig(pdfFullPath, log_message_title, proc_date);
		pdfUtil.setRequest(request);
        
		Report report = new Report();
		report.setHtml_encode(html_encode);
		report.setProc_month(proc_date);
		report.setReport_type(report_type);
		report.setPeriod_type(period_type);
		
        report.setAdmin_user_id(admin_user_id);
        report.setLog_message_title(log_message_title);
//        report.setFile_path(pdf.getFilePath());
        report.setFile_path(pdfUtil.getFilePath());
        
		int report_title_flag = reportDao.getReportTitleFlag(report);
		try {
            if(report_title_flag == 0) {
                reportDao.addReport(report);
                result = 1;
            } else {
                reportDao.updateReport(report);
                report.setReport_seq(reportDao.findReportOne_Seq(report));
                result = 2;
            }
            pdfUtil.makePDF();
		} catch (Exception e) {
			e.printStackTrace();
			result = 0;
		}
		return result;
	}
	
	/*@RequestMapping(value="addReport.html", method={RequestMethod.POST})
	@ResponseBody
	public int addReport(@RequestParam Map<String, String> parameters,HttpServletRequest request) {
		
		int result = 0;
		
		String html_encode = RequestWrapper.decodeXSS(parameters.get("source"));
		String admin_user_id = parameters.get("admin_user_id");
		String log_message_title = RequestWrapper.decodeXSS(parameters.get("log_message_title"));
		String report_type = parameters.get("reportType");
		String proc_date = parameters.get("proc_date");
		String period_type = parameters.get("period_type");
		
        String pdfFullPath = RequestWrapper.decodeXSS(parameters.get("pdfFullPath"))+"&download_type=pdf";
        
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMM");
		Date date = new Date();
		String fileName = "";
		
		if(admin_user_id == null) {
			admin_user_id = SessionManager.getInstance().getUserID(request.getSession()).split("/")[0];
		}
        pdfUtil.setPdfConfig(pdfFullPath, log_message_title, proc_date);
		pdfUtil.setRequest(request);
        
		Report report = new Report();
		report.setHtml_encode(html_encode);
		report.setProc_month(proc_date);
		report.setReport_type(report_type);
		report.setPeriod_type(period_type);
		
        report.setAdmin_user_id(admin_user_id);
        report.setLog_message_title(log_message_title);
//        report.setFile_path(pdf.getFilePath());
        report.setFile_path(pdfUtil.getFilePath());
        
		int report_title_flag = reportDao.getReportTitleFlag(report);
		try {
            if(report_title_flag == 0) {
                reportDao.addReport(report);
                result = 1;
            } else {
                reportDao.updateReport(report);
                report.setReport_seq(reportDao.findReportOne_Seq(report));
                result = 2;
            }
			//process = pb.start();
            pdfUtil.makePDF();
		} catch (Exception e) {
			e.printStackTrace();
			result = 0;
		}
		return result;
	}*/
	
	@MngtActHist(log_action = "WORD DOWNLOAD", log_message = "[정기점검보고서] 워드 다운로드", l_category = "통계보고 > 정기점검보고서")
	@RequestMapping(value="report_Download.html", method={RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> report_Download(@RequestParam Map<String, String> parameters,HttpServletRequest request) {
		
		
		String report_seq = parameters.get("report_seq");
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		try {
			result = reportDao.getHtmlEncode(report_seq);
			String filePath = (String) result.get("html_encode");
			String filedata = "";
			Path path = Paths.get(filePath);
			List<String> list = Files.readAllLines(path);
			for (String string : list) {
				filedata += string;
			}
			result.put("html_encode", filedata);
			String fileName = (String) result.get("report_title");
			String[] nameArr = fileName.split("__");
			if(nameArr.length>1) {
				fileName = nameArr[1];
			}else {
				fileName = nameArr[0];
			}
			result.put("report_title", fileName);
			reportDao.updateReportMangementInfo(report_seq);
			
		} catch (Exception e) {
			result.put("error", "error");
		}
		
		return result;
	}
	
	@RequestMapping(value="systemReport_Download.html", method={RequestMethod.POST})
	@ResponseBody
	public Map<String, Object> systemReport_Download(@RequestParam Map<String, String> parameters,
			HttpServletRequest request, @ModelAttribute("report") Report report) {
		
		
		String report_seq = parameters.get("report_seq");
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		try {
			result = reportDao.getSystemHtmlEncode(report);
			String filePath = (String) result.get("html_encode");
			String filedata = "";
			Path path = Paths.get(filePath);
			List<String> list = Files.readAllLines(path);
			for (String string : list) {
				filedata += string;
			}
			result.put("html_encode", filedata);
			String fileName = (String) result.get("report_title");
			String[] nameArr = fileName.split("__");
			if(nameArr.length>1) {
				fileName = nameArr[1];
			}else {
				fileName = nameArr[0];
			}
			result.put("report_title", fileName);
			reportDao.updateSystemReportMangementInfo(report_seq);
			
		} catch (Exception e) {
			e.printStackTrace();
			result.put("error", "error");
		}
		
		return result;
	}
	
	@MngtActHist(log_action = "PDF DOWNLOAD", log_message = "[정기점검보고서] PDF 다운로드")
	@RequestMapping(value="report_pdf.html", method={RequestMethod.POST})
	public ModelAndView report_pdf(@RequestParam Map<String, String> parameters, HttpServletRequest request, HttpServletResponse response) {
		String report_seq = parameters.get("report_seq");
		Report report  = new Report();
		try {
			report = reportSvc.findReportPdfPath(report_seq);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		String path = report.getFile_path();
 		String filename = report.getReport_title()+".pdf";
		File down = new File(path);
		
		String fileSplit[] = down.getName().split("__");	//파일 이름은 사용자아이디__보고서이름.pdf 구조
		String realname = "";
		if(fileSplit.length > 1) {
			realname = fileSplit[1];
		}else {
			realname = fileSplit[0];
		}
		
		reportDao.updateReportMangementInfo(report_seq);
		ModelAndView result = new ModelAndView();
		result.addObject("downloadFile", down);
		result.addObject("orginName", realname);
		result.setViewName("download");
		return result;
	}
	
	@RequestMapping(value="systemReport_pdf.html")
	public ModelAndView systemReport_pdf(@RequestParam Map<String, String> parameters, HttpServletRequest request, HttpServletResponse response, @ModelAttribute("report") Report report) {
		ModelAndView result = new ModelAndView();
		System.out.println("system report download");
		
		//session에서 사용자 계정 auth를 가져온다
		HttpSession session = request.getSession();
		AdminUser user = (AdminUser) request.getSession().getAttribute("userSession");
		report.setAuth_id(user.getAuth_id());
		Report reportTmp = new Report();
		reportTmp = reportSvc.findSystemReportPath(report);
		
		String path = reportTmp.getFile_path();
		String filename = reportTmp.getReport_title();
		File down = new File(path);
		
		String fileSplit[] = down.getName().split("__");	//파일 이름은 사용자아이디__보고서이름.pdf 구조
		String realname = "";
		if(fileSplit.length > 1) {
			realname = fileSplit[1];
		}else {
			realname = fileSplit[0];
		}
		
		reportDao.updateSystemReportInfo(report);
		result.addObject("downloadFile", down);
		result.addObject("orginName", realname);
		result.setViewName("download");
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "reportFileValidation.html", method = { RequestMethod.POST })
	public int ReportFileValidation(@RequestParam Map<String, String> parameters) {
		String report_seq = parameters.get("num");
		Report report = reportSvc.findReportPdfPath(report_seq);
		
		return reportSvc.reportFileValidation(report);
	}
	
	// 보내온 주소를 바로 리포트로 만들어 다운받음
	// 테스트를 위한 임시 기능
	@RequestMapping(value = "reportMakeandDownload.html", method = { RequestMethod.POST })
	public ModelAndView reportMakeandDownload(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		String start_date = parameters.get("start_date");
		String end_date = parameters.get("end_date");
		String path = parameters.get("path");
		String title = parameters.get("title");
		
		String url = request.getRequestURL()+"";
		String uri = request.getRequestURI();
		url = url.replaceAll(uri, "");
		
		String fullpath = url + "/psm/report/reportDetail_new.html" + "?" + path+"&download_type=pdf";
		
		Date date = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			date = sdf.parse(start_date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		PdfUtil pdf = new PdfUtil(request);
		pdf.setPdfConfig(fullpath, title, start_date.replace("-", "").substring(0, 6));
		pdf.makePDF();
		
		String realPath = pdf.getFilePath();
		File down = new File(realPath);
		
		System.out.println(path);
		
		return new ModelAndView("download","downloadFile", down);
	}
	
	// 찾는 리포트가 있는지 제목으로 찾아오는 기능
	// 테스트를 위해 임시로 만듦
	@ResponseBody
	@RequestMapping(value = "report_find.html", method = { RequestMethod.POST })
	public int report_find(@RequestParam Map<String, String> parameters) {
		String year = parameters.get("year");
		String month = parameters.get("month");
		String title =  parameters.get("title");
		String path = PdfUtil.getPrimaryPath()+"/"+year+month+"/";
		File report = new File(path+year+"년_"+month+"월_"+title+".pdf");
		int returndate = 0;
		
		if(report.exists()) {
			returndate = 1;
		}
		System.out.println(report.getPath());
		return returndate;
	}
	
	@ResponseBody
	@RequestMapping(value = "reportMake.html", method = { RequestMethod.POST })
	public Map<String, String> reportMake(
				@RequestParam(value = "start_date", required = false) String start_date,
				@RequestParam(value = "end_date", required = false) String end_date,
				@RequestParam(value = "proc_date", required = false) String proc_date,
				@RequestParam(value = "period_type", required = false) String period_type,
				@RequestParam(value = "report_type", required = false) String report_type,
				@RequestParam(value = "title", required = false) String title,
				@RequestParam(value = "report_page", required = false) String report_page,
				@RequestParam(value = "admin_user_id", required = false) String admin_user_id,
				@RequestParam(value = "quarter_type", required = false) String quarter_type,
				@RequestParam(value = "system_seq", required = false) String system_seq,
				@RequestParam(value = "system_report", required = false) String system_report,
				HttpServletRequest request ) {
		System.out.println("[reportMake.html]");
		String url = request.getRequestURL()+"";
		String replaceUrl = "";
		String[] urlSplit = url.split("/");
		if(urlSplit.length>3) {
			replaceUrl = url.replace(urlSplit[2].split(":")[0], "127.0.0.1");
			logger.info("replaceUrl : " + replaceUrl);
		}
		String uri = request.getRequestURI();
		url = url.replaceAll(uri, "");
		replaceUrl = replaceUrl.replaceAll(uri, "");
		
		Map<String, String> resultMap = new HashMap<String, String>();
		if(admin_user_id == null) {
			admin_user_id = SessionManager.getInstance().getUserID(request.getSession()).split("/")[0];
		}
		String make_report_auth = adminUserMngtDao.findAdminUserMngtOne(admin_user_id).getMake_report_auth();
		if(make_report_auth.equals("N")) {
			resultMap.put("makeAuth", "none");
			return resultMap;	//보고서 생성 권한 없으면 리턴
		}
		String authType = (String) request.getSession().getAttribute("report_auth_type");
		AdminUser adminUser = (AdminUser) request.getSession().getAttribute("userSession");
		
		
		/**
		 * 시스템 보고서 생성
		 * 	- 시스템 report 생성 / 점검보고서 생성 구분
		 */
		
		pdfUtil.setRequest(request);
		pdfUtil.setFilePath(report_path);
		List<String> sysList = new ArrayList<String>();
		String seqlist = "";
		String titletmp = "";
		
		
		//report_page : 보고서 jsp 경로
		report_page = RequestWrapper.decodeXSS(report_page);
		String fullpath = "";
		String realPath = "";
		Report report = new Report();
		report.setHtml_encode("");
		report.setProc_month(proc_date);
		report.setReport_type(report_type);
		report.setPeriod_type(period_type);
		report.setAdmin_user_id(adminUser.getAdmin_user_id());
		report.setAuth_id(adminUser.getAuth_id());
		report.setAuth_type(authType);
		if("Y".equals(system_report)) { //시스템 일괄 보고서 생성
			report.setSystem_report(system_report);
			//시스템 code 분리
			system_seq = reportDao.getAuthSystemSeq(adminUser.getAuth_id());
			System.out.println("auth system seq : " + system_seq);
			String tmpList[] = system_seq.split(",");
			int report_title_flag = 0;
			for(int i=0;i<tmpList.length;i++) {
				fullpath = "";
				fullpath = report_page +"?"+"type="+report_type+"&period_type="+period_type +"&system_seq="+tmpList[i]+"&start_date="+start_date+"&end_date="+end_date+"&quarter_type="+quarter_type+"&system_report=" + system_report+ "&download_type=pdf";
				System.out.println("  >>> report full path :" + fullpath);
				report.setSystem_seq(tmpList[i]);
				report.setSystem_name(reportDao.getSystem_name(tmpList[i]));
				titletmp = adminUser.getAdmin_user_id()+adminUser.getAuth_id()+authType+"__"+title + "_" + report.getSystem_name();
				report.setLog_message_title(titletmp);
				pdfUtil.setPdfConfig(replaceUrl+fullpath, titletmp, start_date.replace("-", "").substring(0, 6));
				realPath = pdfUtil.getFilePath();
				report.setFile_path(realPath);
				report_title_flag = reportDao.getReportTitleFlag(report);
				try {
					if(report_title_flag == 0) {
						reportDao.addReport(report);
						resultMap.put("result", "1");
					} else {
						reportDao.updateReport(report);
		                report.setReport_seq(reportDao.findReportOne_Seq(report));
						resultMap.put("result", "2");
					}
					pdfUtil.makePDF();
				} catch (Exception e) {
					e.printStackTrace();
					resultMap.put("result", "3");
				}
				seqlist += report.getReport_seq() + ",";
			}//for end
			System.out.println("seq list : "  + seqlist);
			fullpath = report_page +"?"+"type="+report_type+"&period_type="+period_type +"&start_date="+start_date+"&end_date="+end_date+"&quarter_type="+quarter_type+"&download_type=pdf";
			resultMap.put("seqlist", seqlist.substring(0, seqlist.length()-1));
			resultMap.put("system_seq", system_seq);
		}else {
			title = adminUser.getAdmin_user_id()+adminUser.getAuth_id()+authType+"__"+title;
			report.setLog_message_title(title);
			fullpath = report_page +"?"+"type="+report_type+"&period_type="+period_type +"&system_seq="+system_seq+"&start_date="+start_date+"&end_date="+end_date+"&quarter_type="+quarter_type+"&system_report=" + system_report+ "&download_type=pdf";
			logger.info("fullPath : " + fullpath);
			pdfUtil.setPdfConfig(replaceUrl+fullpath, title, start_date.replace("-", "").substring(0, 6));
			realPath =pdfUtil.getFilePath();
			report.setFile_path(realPath);
			System.out.println("report file path : " + report.getFile_path());
			int report_title_flag = reportDao.getReportTitleFlag(report);
			try {
				if(report_title_flag == 0) {
					reportDao.addReport(report);
					resultMap.put("result", "1");
				} else {
					reportDao.updateReport(report);
	                report.setReport_seq(reportDao.findReportOne_Seq(report));
					resultMap.put("result", "2");
				}
				pdfUtil.makePDF();
			} catch (Exception e) {
				e.printStackTrace();
				resultMap.put("result", "3");
			}
			resultMap.put("seq", report.getReport_seq());
		}
		//resultMap.put("seq", report.getReport_seq());
		resultMap.put("fullpath", url+fullpath.replace("pdf", "word"));
		return resultMap;
	}
	
	@ResponseBody
	@RequestMapping(value = "report_upload.html", method = { RequestMethod.POST })
	public int report_upload(@RequestParam Map<String, String> parameters, HttpServletRequest request, HttpServletResponse response) {
		return reportSvc.report_upload(parameters, request);
	}
	
	@RequestMapping(value="report_userfile.html", method={RequestMethod.POST})
	public ModelAndView report_userfile(@RequestParam Map<String, String> parameters, HttpServletRequest request, HttpServletResponse response) {
		String report_seq = parameters.get("report_seq");
		Report report = reportDao.findReportPdfPath(report_seq);
		String realname = report.getUserfile_path();
		File file = new File(realname);
		
		realname = realname.split("__")[1];
		
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("downloadFile", file);
		modelAndView.addObject("orginName", realname);
		modelAndView.setViewName("download");
		
		return modelAndView;
	}	
	// 권한부여 보고서
		@RequestMapping(value = "reportDetail_authInfo.html")
		public ModelAndView findReportDetail_authInfo(HttpServletRequest request,
				@RequestParam(value = "type", required = false) int type,
				@RequestParam(value = "period_type", required = false) int period_type,
				@RequestParam(value = "system_seq", required = false) String system_seq,
				@RequestParam(value = "start_date", required = false) String start_date,
				@RequestParam(value = "end_date", required = false) String end_date,
				@RequestParam(value = "menu_id", required = false) String menu_id,
				@RequestParam(value = "download_type", required = false) String download_type)
		
		{

			ModelAndView modelAndView = new ModelAndView();
			long time = System.currentTimeMillis();
			SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
			String date = dayTime.format(new Date(time));
			String ui_type = commonDao.getUiType();
			modelAndView.addObject("download_type", download_type);
			String pdfFullPath = request.getRequestURL() +"?"+ request.getQueryString();
			modelAndView.addObject("pdfFullPath", pdfFullPath);
			// 마지막날짜가 현재보다 클 경우 어제날짜까지로 지정해준다.
			/*
			try {
				Date tmpDate = dayTime.parse(end_date);
				int compare = tmpDate.compareTo(new Date());
				if (compare >= 0) {
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.DAY_OF_MONTH, -1);
					end_date = dayTime.format(cal.getTime());
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
			*/
			String compareDate = "";

			PrivacyReport pr = new PrivacyReport(); // 선택 기간
			PrivacyReport prCompare = new PrivacyReport(); // 비교 기간

			HttpSession session = request.getSession();
			String master = (String) session.getAttribute("master");

			String use_reportLogo = (String) session.getAttribute("use_reportLogo");
			modelAndView.addObject("use_reportLogo", use_reportLogo);
			if (use_reportLogo != null && use_reportLogo.equals("Y")) {
				String rootPath = request.getSession().getServletContext().getRealPath("/");
				String attach_path = "resources/upload/";

				String filename = commonDao.selectCurrentImage_logo2();
				String savePath = rootPath + attach_path + filename;

				File file = new File(savePath);

				if (file.exists()) {
					modelAndView.addObject("filename", filename);
				}
			}

			String use_reportLine = (String) session.getAttribute("use_reportLine");
			modelAndView.addObject("use_reportLine", use_reportLine);

			modelAndView.addObject("masterflag", master);
			modelAndView.addObject("report_type", type);
			modelAndView.addObject("period_type", period_type);
			modelAndView.addObject("type", type);
			modelAndView.addObject("start_date", start_date);
			modelAndView.addObject("end_date", end_date);
			modelAndView.addObject("date", date);
			modelAndView.addObject("system_seq", system_seq);
			modelAndView.addObject("use_studentId", use_studentId);
			modelAndView.addObject("logo_report_url", logo_report_url);
			modelAndView.addObject("menu_id", menu_id);

			List<String> syslist = new ArrayList<String>();
			//

			String tmpList[] = system_seq.split(",");

			for (int i = 0; i < tmpList.length; ++i) {
				syslist.add(tmpList[i]);
			}

			String sDate[] = start_date.split("-");
			String eDate[] = end_date.split("-");

			int strYear = Integer.parseInt(sDate[0]);
			int strMonth = Integer.parseInt(sDate[1]);
			int endYear = Integer.parseInt(eDate[0]);
			int endMonth = Integer.parseInt(eDate[1]);

			int diffMon = (endYear - strYear) * 12 + (endMonth - strMonth);

			String stDate = start_date.replaceAll("-", "");
			String edDate = end_date.replaceAll("-", "");
			// String log_type = "31";

			String log_type = "";
			String report_name = "";

			modelAndView.addObject("log_type", log_type);
			

			pr.setStart_date(stDate);
			pr.setEnd_date(edDate);
			pr.setSysList(syslist);
			pr.setLog_type(log_type);

			// 점검대상
			List<SystemMaster> systems = reportSvc.findSystemMasterDetailList(pr);
			modelAndView.addObject("systems", systems);

			Calendar cal = Calendar.getInstance();
			cal.set(strYear, strMonth - 1, 1); // 선택기간 시작월 1일

			cal.add(Calendar.MONTH, -1); // 비교 기간 끝(선택기간 한달전)
			compareDate = dateFormatter.format(cal.getTime());
			edDate = compareDate.substring(0, 6) + "31";

			cal.add(Calendar.MONTH, -diffMon); // 비교 기간 시작
			compareDate = dateFormatter.format(cal.getTime());
			stDate = compareDate.substring(0, 6) + "01";

			prCompare.setStart_date(stDate);
			prCompare.setEnd_date(edDate);
			prCompare.setSysList(syslist);
			prCompare.setLog_type(log_type);

			// 접근권한신청현황 총 횟수
			int logCnt = reportSvc.findAuthInfoAllCount(pr);
			modelAndView.addObject("logCnt", logCnt);

			// 시스템별 행위
			List<PrivacyReport> authInfoAllCountByReqtype = reportSvc.findAuthInfoAllCountByReqType(pr);
			List<PrivacyReport> authInfoAllCountByReqtype_prev = reportSvc.findAuthInfoBySysCount(prCompare);
			// List<PrivacyReport> findAuthInfoBySysCount
			// =reportSvc.findAuthInfoBySysCount(prCompare);

			int nCount1, nCount2, nCount3;
			for (int i = 0; i < authInfoAllCountByReqtype.size(); i++) {

				nCount1 = 0;
				nCount2 = 0;
				nCount3 = 0;

				if (authInfoAllCountByReqtype.get(i) != null)
					nCount1 = authInfoAllCountByReqtype.get(i).getCnt1() + authInfoAllCountByReqtype.get(i).getCnt2()
							+ authInfoAllCountByReqtype.get(i).getCnt3();

				if (!authInfoAllCountByReqtype_prev.isEmpty()) {
					for (PrivacyReport a : authInfoAllCountByReqtype_prev) {
						if (a.getSystem_seq().equals(authInfoAllCountByReqtype.get(i).getSystem_seq())) {
							nCount2 = Integer.parseInt(a.getCnt());
						}
					}
				}

				nCount3 = nCount1 - nCount2;

				authInfoAllCountByReqtype.get(i).setType1(nCount1);// 금월
				authInfoAllCountByReqtype.get(i).setType2(nCount2);// 이전월
				authInfoAllCountByReqtype.get(i).setType1(nCount1);

				if (nCount3 < 0)
					authInfoAllCountByReqtype.get(i).setbCheckPlus("0");
				else
					authInfoAllCountByReqtype.get(i).setbCheckPlus("1");

				if (nCount3 < 0)
					nCount3 = -(nCount3);
				authInfoAllCountByReqtype.get(i).setType3(nCount3); // 증감

			}
			if (authInfoAllCountByReqtype != null)
				modelAndView.addObject("authInfoAllCountByReqtype", authInfoAllCountByReqtype);

			// 부서별 행위
			List<PrivacyReport> findAuthInfoByDeptCountByReqType = reportSvc.findAuthInfoByDeptCountByReqType(pr);
			List<PrivacyReport> findAuthInfoByDeptCountByReqType_prev = reportSvc.findAuthInfoAllCountByDept(prCompare);

			for (int i = 0; i < findAuthInfoByDeptCountByReqType.size(); i++) {

				nCount1 = 0;
				nCount2 = 0;
				nCount3 = 0;

				if (findAuthInfoByDeptCountByReqType.get(i) != null)
					nCount1 = findAuthInfoByDeptCountByReqType.get(i).getCnt1()
							+ findAuthInfoByDeptCountByReqType.get(i).getCnt2()
							+ findAuthInfoByDeptCountByReqType.get(i).getCnt3();

				if (!findAuthInfoByDeptCountByReqType_prev.isEmpty()) {
					for (PrivacyReport a : findAuthInfoByDeptCountByReqType_prev) {
						if (a.getDept_id().equals(findAuthInfoByDeptCountByReqType.get(i).getDept_id())) {
							nCount2 = Integer.parseInt(a.getCnt());
						}
					}
				}

				nCount3 = nCount1 - nCount2;

				findAuthInfoByDeptCountByReqType.get(i).setType1(nCount1);// 금월
				findAuthInfoByDeptCountByReqType.get(i).setType2(nCount2);// 이전월
				findAuthInfoByDeptCountByReqType.get(i).setType1(nCount1);

				if (nCount3 < 0)
					findAuthInfoByDeptCountByReqType.get(i).setbCheckPlus("0");
				else
					findAuthInfoByDeptCountByReqType.get(i).setbCheckPlus("1");

				if (nCount3 < 0)
					nCount3 = -(nCount3);
				findAuthInfoByDeptCountByReqType.get(i).setType3(nCount3); // 증감

			}
			if (findAuthInfoByDeptCountByReqType != null)
				modelAndView.addObject("findAuthInfoByDeptCountByReqType", findAuthInfoByDeptCountByReqType);

			// 권한별 행위
			List<PrivacyReport> findAuthInfoByAuthCountByReqType = reportSvc.findAuthInfoByAuthCountByReqType(pr);
			List<PrivacyReport> findAuthInfoByAuthCountByReqType_prev = reportSvc.findAuthInfoAllCountByAuth(prCompare);

			for (int i = 0; i < findAuthInfoByAuthCountByReqType.size(); i++) {

				nCount1 = 0;
				nCount2 = 0;
				nCount3 = 0;

				if (findAuthInfoByAuthCountByReqType.get(i) != null)
					nCount1 = findAuthInfoByAuthCountByReqType.get(i).getCnt1()
							+ findAuthInfoByAuthCountByReqType.get(i).getCnt2()
							+ findAuthInfoByAuthCountByReqType.get(i).getCnt3();

				if (!findAuthInfoByAuthCountByReqType_prev.isEmpty()) {
					for (PrivacyReport a : findAuthInfoByAuthCountByReqType_prev) {
						if (a.getTarget_auth().equals(findAuthInfoByAuthCountByReqType.get(i).getTarget_auth())) {
							nCount2 = Integer.parseInt(a.getCnt());
						}
					}
				}

				nCount3 = nCount1 - nCount2;

				findAuthInfoByAuthCountByReqType.get(i).setType1(nCount1);// 금월
				findAuthInfoByAuthCountByReqType.get(i).setType2(nCount2);// 이전월
				findAuthInfoByAuthCountByReqType.get(i).setType1(nCount1);

				if (nCount3 < 0)
					findAuthInfoByAuthCountByReqType.get(i).setbCheckPlus("0");
				else
					findAuthInfoByAuthCountByReqType.get(i).setbCheckPlus("1");

				if (nCount3 < 0)
					nCount3 = -(nCount3);
				findAuthInfoByAuthCountByReqType.get(i).setType3(nCount3); // 증감

			}
			if (findAuthInfoByAuthCountByReqType != null)
				modelAndView.addObject("findAuthInfoByAuthCountByReqType", findAuthInfoByAuthCountByReqType);

			List<PrivacyReport> findAuthInfoList2 = reportSvc.findAuthInfoList2(pr);
			modelAndView.addObject("findAuthInfoList2", findAuthInfoList2);

			modelAndView.setViewName("report_authInfo");

			return modelAndView;
		}
	
		// 접근권한신청현황 시스템별 총 횟수
		@RequestMapping(value = "reportDetail_authChart0.html")
		@ResponseBody
		public JSONArray reportDetail_authChart0(@RequestParam(value = "log_type", required = false) String log_type,
				@RequestParam(value = "system_seq", required = false) String system_seq,
				@RequestParam(value = "start_date", required = false) String start_date,
				@RequestParam(value = "end_date", required = false) String end_date) {

			JSONArray jArray = new JSONArray();
			String tmpList[] = system_seq.split(",");
			List<String> syslist = new ArrayList<String>();

			for (int i = 0; i < tmpList.length; ++i) {
				syslist.add(tmpList[i]);
			}

			PrivacyReport pr = new PrivacyReport();
			pr.setStart_date(start_date.replaceAll("-", ""));
			pr.setEnd_date(end_date.replaceAll("-", ""));
			pr.setSysList(syslist);
			pr.setLog_type(log_type);

			List<PrivacyReport> reportBySys = reportSvc.findAuthInfoBySysCount(pr);

			for (int i = 0; i < reportBySys.size(); i++) {
				JSONObject jsonObj = new JSONObject();
				PrivacyReport report = reportBySys.get(i);

				jsonObj.put("system_seq", report.getSystem_seq());
				jsonObj.put("system_name", report.getSystem_name());
				jsonObj.put("cnt", report.getCnt());
				jArray.add(jsonObj);
			}

			return jArray;
		}

		//
		@RequestMapping(value = "reportDetail_authChart1.html")
		@ResponseBody
		public JSONArray reportDetail_authChart1(@RequestParam(value = "system_seq", required = false) String system_seq,
				@RequestParam(value = "start_date", required = false) String start_date,
				@RequestParam(value = "end_date", required = false) String end_date) {

			JSONArray jArray = new JSONArray();
			String tmpList[] = system_seq.split(",");
			List<String> syslist = new ArrayList<String>();

			for (int i = 0; i < tmpList.length; ++i) {
				syslist.add(tmpList[i]);
			}

			PrivacyReport pr = new PrivacyReport();
			pr.setStart_date(start_date.replaceAll("-", ""));
			pr.setEnd_date(end_date.replaceAll("-", ""));
			pr.setSysList(syslist);

			List<PrivacyReport> privTypeTemp = reportSvc.findAuthInfoAllCountByReqType(pr);

			for (int i = 0; i < privTypeTemp.size(); i++) {
				JSONObject jsonObj = new JSONObject();
				PrivacyReport report = privTypeTemp.get(i);

				jsonObj.put("system_seq", report.getSystem_seq());
				jsonObj.put("system_name", report.getSystem_name());
				jsonObj.put("cnt1", report.getCnt1());
				jsonObj.put("cnt2", report.getCnt2());
				jsonObj.put("cnt3", report.getCnt3());
				jArray.add(jsonObj);
			}

			return jArray;
		}
		@MngtActHist(log_action = "SELECT", log_message = "보고서 생성")
		@RequestMapping(value = "report_summon.html")
		public DataModelAndView findReportSummon(HttpServletRequest request,
				@RequestParam(value = "search_from", required = false) String search_from,
				@RequestParam(value = "search_to", required = false) String search_to,
				@RequestParam(value = "dept_name", required = false) String dept_name) {

			DataModelAndView modelAndView = new DataModelAndView();

			Report report = new Report();
			report.setSearch_from(search_from);
			report.setSearch_to(search_to);

			Date dayFromDay = new Date();
			Date dayToDay = new Date();

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

			try {
				dayFromDay = sdf.parse(search_from);
				dayToDay = sdf.parse(search_to);
				sdf.applyPattern("yyyy년 MM월 dd일");

				search_from = sdf.format(dayFromDay);
				search_to = sdf.format(dayToDay);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			int systemCnt = reportSvc.findSystemCnt();
			int summaryLogCnt = reportSvc.findSummaryLogCnt(report);
			int summonReqCnt = reportSvc.findSummonReqCnt(report);
			int summonUserCnt = reportSvc.findSummonUserCnt(report);

			PrivacyReport summonSystem = reportSvc.findSummonSystem(report);
			PrivacyReport deptTop = reportSvc.finddeptTop(report);

			List<PrivacyReport> summonList = reportSvc.findSummonList(report);
			List<PrivacyReport> summonStatusList = reportSvc.findSummonStatusList(report);
			List<PrivacyReport> summonResultList = reportSvc.findSummonResultList(report);

			String ui_type = commonDao.getUiType();
			//20201222 hbjang 한국인삼공사 소명보고서 커스터마이징 UI코드 isks
			if(ui_type.equals("isks")) {
				modelAndView.setViewName("custom_report_summon_isks");
				modelAndView.addObject("totalSummonCntIsks", reportSvc.findTotalSummonCntIsks(report));
				modelAndView.addObject("totalExtrtCntIsks", reportSvc.findTotalExtrtCntIsks(report));
				modelAndView.addObject("orgRuleInfoListIsks", reportSvc.findOrgRuleInfoIsks());
				modelAndView.addObject("totalRuleSystemCntListIsks", reportSvc.findTotalRuleSystemCntIsks(report));
				modelAndView.addObject("systemList", commonDao.getSystemMasterList());
			} else {
				modelAndView.setViewName("report_summon");
			}
			
			
			
			modelAndView.addObject("search_from", search_from);
			modelAndView.addObject("search_to", search_to);
			modelAndView.addObject("systemCnt", systemCnt);
			modelAndView.addObject("summaryLogCnt", summaryLogCnt);
			modelAndView.addObject("summonReqCnt", summonReqCnt);
			modelAndView.addObject("summonUserCnt", summonUserCnt);
			modelAndView.addObject("summonSystem", summonSystem);
			modelAndView.addObject("deptTop", deptTop);
			modelAndView.addObject("summonList", summonList);
			modelAndView.addObject("summonStatusList", summonStatusList);
			modelAndView.addObject("summonResultList", summonResultList);

			return modelAndView;
		}

		@RequestMapping(value = "reportSummon_chart1.html")
		@ResponseBody
		public Map reportSummon_chart1(HttpServletRequest request,
				@RequestParam(value = "search_from", required = false) String search_from,
				@RequestParam(value = "search_to", required = false) String search_to) {

			Date dayFromDay = new Date();
			Date dayToDay = new Date();

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy년 MM월 dd일");

			try {
				dayFromDay = sdf.parse(search_from);
				dayToDay = sdf.parse(search_to);
				sdf.applyPattern("yyyyMMdd");

				search_from = sdf.format(dayFromDay);
				search_to = sdf.format(dayToDay);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			List<Report> rule_list = new ArrayList<Report>();

			Report tmp1 = new Report();
			tmp1.setRule_nm("소명요청");
			rule_list.add(tmp1);

			Report tmp2 = new Report();
			tmp2.setRule_nm("판정대기");
			rule_list.add(tmp2);

			Report tmp3 = new Report();
			tmp3.setRule_nm("재소명요청");
			rule_list.add(tmp3);

			Report tmp4 = new Report();
			tmp4.setRule_nm("판정완료");
			rule_list.add(tmp4);
			
			Report tmp5 = new Report();
			tmp5.setRule_nm("소명취소");
			rule_list.add(tmp5);

			Map map = new HashMap<>();
			map.put("search_from", search_from);
			map.put("search_to", search_to);
			map.put("rule_list", rule_list);

			List<Map> emp_list = reportDao.findReportSummon_chart1(map);

			Map res = new HashMap<>();
			res.put("rule_list", rule_list);
			res.put("emp_list", emp_list);

			return res;
		}

		@RequestMapping(value = "reportSummon_chart2.html")
		@ResponseBody
		public Map reportSummon_chart2(HttpServletRequest request,
				@RequestParam(value = "search_from", required = false) String search_from,
				@RequestParam(value = "search_to", required = false) String search_to) {

			Date dayFromDay = new Date();
			Date dayToDay = new Date();

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy년 MM월 dd일");

			try {
				dayFromDay = sdf.parse(search_from);
				dayToDay = sdf.parse(search_to);
				sdf.applyPattern("yyyyMMdd");

				search_from = sdf.format(dayFromDay);
				search_to = sdf.format(dayToDay);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			List<Report> rule_list = new ArrayList<Report>();

			Report tmp1 = new Report();
			tmp1.setRule_nm("미판정");
			rule_list.add(tmp1);

			Report tmp2 = new Report();
			tmp2.setRule_nm("적정");
			rule_list.add(tmp2);

			Report tmp3 = new Report();
			tmp3.setRule_nm("부적정");
			rule_list.add(tmp3);
			
			Report tmp4 = new Report();
			tmp4.setRule_nm("소명취소");
			rule_list.add(tmp4);

			Map map = new HashMap<>();
			map.put("search_from", search_from);
			map.put("search_to", search_to);
			map.put("rule_list", rule_list);

			List<Map> emp_list = reportDao.findReportSummon_chart2(map);

			Map res = new HashMap<>();
			res.put("rule_list", rule_list);
			res.put("emp_list", emp_list);

			return res;
		}

		@RequestMapping(value = "reportSummon_chart3.html")
		@ResponseBody
		public JSONArray reportSummon_chart3(HttpServletRequest request,
				@RequestParam(value = "search_from", required = false) String search_from,
				@RequestParam(value = "search_to", required = false) String search_to) {

			JSONArray jArray = new JSONArray();

			Date dayFromDay = new Date();
			Date dayToDay = new Date();

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy년 MM월 dd일");

			try {
				dayFromDay = sdf.parse(search_from);
				dayToDay = sdf.parse(search_to);
				sdf.applyPattern("yyyyMMdd");

				search_from = sdf.format(dayFromDay);
				search_to = sdf.format(dayToDay);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Report rp = new Report();
			rp.setSearch_from(search_from);
			rp.setSearch_to(search_to);

			List<PrivacyReport> listByReqtype = reportSvc.findPrivacytype(rp);

			for (int i = 0; i < listByReqtype.size(); i++) {
				JSONObject jsonObj = new JSONObject();
				PrivacyReport report = listByReqtype.get(i);
				jsonObj.put("req_type", report.getRule_cd());
				jsonObj.put("cnt", report.getCnt());
				jArray.add(jsonObj);
			}

			return jArray;
		}

		@RequestMapping(value = "reportSummon_chart4.html")
		@ResponseBody
		public JSONArray reportDetail_chart1_2(HttpServletRequest request,
				@RequestParam(value = "search_from", required = false) String search_from,
				@RequestParam(value = "search_to", required = false) String search_to) {

			JSONArray jArray = new JSONArray();

			Date dayFromDay = new Date();
			Date dayToDay = new Date();

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy년 MM월 dd일");

			try {
				dayFromDay = sdf.parse(search_from);
				dayToDay = sdf.parse(search_to);
				sdf.applyPattern("yyyyMMdd");

				search_from = sdf.format(dayFromDay);
				search_to = sdf.format(dayToDay);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Report rp = new Report();
			rp.setSearch_from(search_from);
			rp.setSearch_to(search_to);

			List<PrivacyReport> reportByDept = reportSvc.findSummonSystemRepot(rp);

			for (int i = 0; i < reportByDept.size(); i++) {
				JSONObject jsonObj = new JSONObject();
				PrivacyReport report = reportByDept.get(i);
				jsonObj.put("system_name", report.getSystem_name());
				jsonObj.put("cnt", report.getCnt());
				jArray.add(jsonObj);
			}

			return jArray;
		}
		
	@RequestMapping(value="report_Allcheck.html", method={RequestMethod.POST})
	@ResponseBody
	public List<ReportList> report_Allcheck(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		
		String year = parameters.get("year");
		String reportType = parameters.get("reportType");
		AdminUser user = (AdminUser) request.getSession().getAttribute("userSession");
		ReportList data = new ReportList();
		data.setProc_month(year);
		data.setReport_type(reportType);
		data.setUpdate_id(user.getAdmin_user_id());
		data.setAuth_id(user.getAuth_id());
		data.setAuth_type((String) request.getSession().getAttribute("report_auth_type"));
		System.out.println("system report flag : " + parameters.get("system_report"));
		data.setSystem_report(parameters.get("system_report"));
		
		List<ReportList> result = new ArrayList<ReportList>();
		try {
			result = reportDao.findReportAllCheck(data);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@RequestMapping(value = "reportDetail_combined.html")
	public ModelAndView findReportDetail_combined(HttpServletRequest request,
			@RequestParam(value = "type", required = false) int type,
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date,
			@RequestParam(value = "half_type", required = false) String half_type,
			@RequestParam(value = "period_type", required = false) int period_type,
			@RequestParam(value = "download_type", required = false) String download_type) {
		
		ModelAndView modelAndView = new ModelAndView();
		String ui_type = commonDao.getUiType();
		String use_reportLogo = (String)request.getSession().getAttribute("use_reportLogo");
	    modelAndView.addObject("use_reportLogo", use_reportLogo);
	    if (use_reportLogo != null && use_reportLogo.equals("Y")) {
			String rootPath = request.getSession().getServletContext().getRealPath("/");
			String attach_path = "resources/upload/";
			
			String filename = commonDao.selectCurrentImage_logo2();
			String savePath = rootPath + attach_path + filename;
			
			File file = new File(savePath);
			
			if(file.exists()) {
				modelAndView.addObject("filename", filename);
			}
	    }
	    
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
		String date = dateFormatter.format(new Date());
		
		// 마지막날짜가 현재보다 클 경우 어제날짜까지로 지정해준다.
		/*
		try {
			Date tmpDate = dateFormatter.parse(end_date);
			int compare = tmpDate.compareTo(new Date());
			if( compare >= 0) {
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DAY_OF_MONTH, -1);
				end_date = dateFormatter.format(cal.getTime());
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		*/
		modelAndView.addObject("start_date", start_date);
		modelAndView.addObject("end_date", end_date);
		modelAndView.addObject("date", date);
		modelAndView.addObject("system_seq", system_seq);
		modelAndView.addObject("period_type", period_type);
		modelAndView.addObject("logo_report_url", logo_report_url);
		modelAndView.addObject("half_type", half_type);
		
		List<String> syslist = new ArrayList<String>();
		String tmpList[] = system_seq.split(",");
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}
		
		
		PrivacyReport pr = new PrivacyReport();
		String stDate = start_date.replaceAll("-", "");
		String edDate = end_date.replaceAll("-", "");
		pr.setStart_date(stDate);
		pr.setEnd_date(edDate);
		pr.setHalf_type(half_type);
		pr.setSysList(syslist);
		
		String sDate[] = start_date.split("-");
		String eDate[] = end_date.split("-");

		int strYear = Integer.parseInt(sDate[0]);
		int strMonth = Integer.parseInt(sDate[1]);
		int endYear = Integer.parseInt(eDate[0]);
		int endMonth = Integer.parseInt(eDate[1]);
		String compareDate = "";
		Calendar cal = Calendar.getInstance();
	    cal.set(strYear, strMonth, 1);		//선택기간 시작월 1일
	    
		cal.add(Calendar.MONTH, -6);    	
		compareDate = dateFormatter.format(cal.getTime());
		modelAndView.addObject("compareDate", compareDate);
		stDate = compareDate.replaceAll("-", "");		// 비교 기간 시작

		PrivacyReport prCompare = new PrivacyReport();	//비교 기간(6개월 전~ 해당월 말)
		prCompare.setStart_date(stDate);
		prCompare.setEnd_date(edDate);
		prCompare.setSysList(syslist);
		
		
		modelAndView.addObject("stDate", stDate);
		modelAndView.addObject("edDate", edDate);
		
		List<String> monthList = new ArrayList<String>();
		for(int i = 0; i<6; i++) {
			monthList.add(dateFormatter.format(cal.getTime()).substring(0,7));
			cal.add(Calendar.MONTH, +1);
		}
		modelAndView.addObject("monthList", monthList);
		
		int diffMon = getDiffMonth(pr);
		modelAndView.addObject("diffMon", diffMon);
		int compareDiffMon = getDiffMonth(prCompare);
		modelAndView.addObject("compareDiffMon", compareDiffMon);
		
		// 점검대상
		List<SystemMaster> systems = reportSvc.findSystemMasterDetailList(pr);
		modelAndView.addObject("systems", systems);
		
		// 개인정보 접속기록 총 이용량
		int logCnt = reportSvc.findPrivacyTotalLogCnt(pr);
		modelAndView.addObject("logCnt", logCnt);
		
		// 개인정보 접속기록 이용량 시스템 Top1
		PrivacyReport findPrivacyReportBySystemTop1 = reportSvc.findPrivacyReportBySystemTop1(pr);
		if(findPrivacyReportBySystemTop1 != null)
			modelAndView.addObject("reportBySys", findPrivacyReportBySystemTop1.getSystem_name());
		
		// 개인정보 접속기록 이용량 부서 Top1
		PrivacyReport findPrivacyReportByDeptTop1 = reportSvc.findPrivacyReportByDeptTop1(pr);
		if(findPrivacyReportByDeptTop1 != null)
			modelAndView.addObject("reportByDept", findPrivacyReportByDeptTop1.getDept_name());
		
		//개인정보 접속기록 이용량 리스트
		List<PrivacyReport> reportBySysList = reportSvc.findPrivacyReportBySystem(pr);
		if(reportBySysList != null)
		modelAndView.addObject("reportBySysList", reportBySysList);		
		
		//개인정보 접속기록 시스템별 처리현황
		List<PrivacyReport> reporProcessBySysList = reportSvc.findReportHalf_chart3(pr);
		if(reporProcessBySysList != null)
		modelAndView.addObject("reporProcessBySysList", reporProcessBySysList);		
		
		//개인정보 접속기록 시스템별 총 처리량
		int privCnt = reportSvc.findPrivacyTotalCnt(pr);
		modelAndView.addObject("privCnt", privCnt);
		
		//월별 접속기록 이용량 리스트
		List<PrivacyReport> reportByMonthList = reportSvc.findReportHalf_chart2(prCompare);
		if(reportByMonthList != null)
		modelAndView.addObject("reportByMonthList", reportByMonthList);
		
		// 부서별 개인정보 월별 현황
		List<String> monthList2 = new ArrayList<String>();
		for(int i = 0; i<6; i++) {
			prCompare.setMonth1((monthList.get(i).substring(5,7)));
		}
		if(monthList!=null) {
			prCompare.setMonth1(monthList.get(0).substring(5,7));
			prCompare.setMonth2(monthList.get(1).substring(5,7));
			prCompare.setMonth3(monthList.get(2).substring(5,7));
			prCompare.setMonth4(monthList.get(3).substring(5,7));
			prCompare.setMonth5(monthList.get(4).substring(5,7));
			prCompare.setMonth6(monthList.get(5).substring(5,7));
		}
		
		List<PrivacyReport> table5 = reportSvc.findReportHalf_table5(prCompare);
		modelAndView.addObject("table5", table5);
		if(table5 != null && table5.size() > 0) {
			String max_dept = table5.get(0).getDept_name();
			modelAndView.addObject("max_dept", max_dept);
		}
		
		//고유식별 정보 처리 현황
		int chart7TotalCnt = 0;  
		List<PrivacyReport> typeCnt = reportSvc.findTypeReportCount(pr);
		if(typeCnt.size()>0){
			modelAndView.addObject("topType", typeCnt.get(0).getPrivacy_desc());
			for(int i=0; i<typeCnt.size(); i++){
				chart7TotalCnt = chart7TotalCnt + Integer.parseInt(typeCnt.get(i).getCnt());
			}
			modelAndView.addObject("chart7TotalCnt", chart7TotalCnt);
			modelAndView.addObject("typeCnt", typeCnt);
		}
		
		//상세 결과
		//개인정보 접속기록 전체 현황
		List<PrivacyReport> privTypeCount;
		int privTypeCountAvg = 0;

		if(period_type == 1){
			privTypeCount = reportSvc.findPrivTypeCountWeek(pr);
		
			String proc_date="";
			//Date dateT = new Date();
			int nConut = 0;
			int nPrivCount = 0;
			String tmpS="";
			
			for (int i = 0; i < privTypeCount.size(); ++i) {
				PrivacyReport tmp = privTypeCount.get(i);
				
				proc_date = strYear+"년 "+strMonth+"월 "+(i+1)+"주차";
				
				tmp.setProc_date(proc_date);

				if (i == 0) {
					tmp.setTot_cnt("-");
				} else {
					nConut = Integer.parseInt(tmp.getCnt());
					nPrivCount = Integer.parseInt(privTypeCount.get(i - 1).getCnt());
					tmpS = Integer.toString(nConut - nPrivCount);
					if (tmpS.indexOf("-") >= 0)
						tmp.setbCheckPlus("0");
					else
						tmp.setbCheckPlus("1");

					tmpS = tmpS.replaceAll("-", "");
					tmp.setTot_cnt(tmpS);
				}

				privTypeCountAvg = privTypeCountAvg + Integer.parseInt(tmp.getCnt());

				privTypeCount.set(i, tmp);
			}
		}else{
			privTypeCount = reportSvc.findPrivacyReportDetailChart3_1(pr);
			
			String proc_date="";
			int nConut = 0;
			int nPrivCount = 0;
			String tmpS="";
			
			for (int i = 0; i < privTypeCount.size(); ++i) {
				PrivacyReport tmp = privTypeCount.get(i);
				
				proc_date = tmp.getProc_date().substring(0,4)+"년 "+tmp.getProc_date().substring(4,6)+"월";
				
				tmp.setProc_date(proc_date);

				if (i == 0) {
					tmp.setTot_cnt("-");
				} else {
					nConut = Integer.parseInt(tmp.getCnt());
					nPrivCount = Integer.parseInt(privTypeCount.get(i - 1).getCnt());
					tmpS = Integer.toString(nConut - nPrivCount);
					if (tmpS.indexOf("-") >= 0)
						tmp.setbCheckPlus("0");
					else
						tmp.setbCheckPlus("1");

					tmpS = tmpS.replaceAll("-", "");
					tmp.setTot_cnt(tmpS);
				}

				privTypeCountAvg = privTypeCountAvg + Integer.parseInt(tmp.getCnt());

				privTypeCount.set(i, tmp);
			}
		}
		modelAndView.addObject("privTypeCount", privTypeCount);
		
		//개인정보 처리시스템별 접속기록 현황
		List<PrivacyReport> list4 = reportSvc.findPrivacylogCntBySystem(pr);
		List<PrivacyReport> list4_1 = reportSvc.findPrivacylogCntBySystem(pr);
		
		int nCount1, nCount2, nCount3;
		
		for(int i=0; i<list4.size(); i++) {
			
			PrivacyReport report = list4.get(i);

			report.setStart_date(pr.getStart_date());
			report.setEnd_date(pr.getEnd_date());

			report.setStart_date(prCompare.getStart_date());
			report.setEnd_date(prCompare.getEnd_date());
			PrivacyReport prev_report = reportSvc.findPrivacylogCntBySystemCompare(report);
			
			nCount1=0;
			nCount2=0;
			nCount3=0;
			
			if(report != null)
				nCount1 = Integer.parseInt(report.getCnt());
			if(prev_report != null)
				nCount2 = Integer.parseInt(prev_report.getCnt());
			nCount3 = nCount1 - nCount2;
			
			report.setType1(nCount1); //금월
			report.setType2(nCount2); //이전월	
			
			if ( nCount3 < 0 ) report.setbCheckPlus("0");
			else report.setbCheckPlus("1");
			
			if ( nCount3 < 0 ) nCount3 = -(nCount3);
			report.setType3(nCount3); //증감	
			
			list4.set(i, report);
		}
		
		modelAndView.addObject("listChart4Detail", list4);
		
		List<PrivacyReport> list = new ArrayList<>();
		List<PrivacyReport> list_1 = new ArrayList<>(); 
		if (use_studentId.length() > 0 && use_studentId.equals("yes")) {
			list = reportSvc.findReportDetail_chart5_useStudentId(pr);
			list_1 = reportSvc.findReportDetail_chart5_useStudentId(pr);
		} else {
			list = reportSvc.findReportDetail_chart5(pr);
			list_1 = reportSvc.findReportDetail_chart5_useStudentId(pr);
		}
		
		for(int i=0; i<list.size(); i++) {
			
			PrivacyReport report = list.get(i);
			report.setSysList(syslist);
			
			report.setStart_date(prCompare.getStart_date());
			report.setEnd_date(prCompare.getEnd_date());	
			PrivacyReport prev_report = reportSvc.findReportDetail_chart5_detail(report);
			
			nCount1=0;
			nCount2=0;
			nCount3=0;

			if(report != null)
				nCount1 = Integer.parseInt(report.getCnt());
			if(prev_report != null)
				nCount2 = Integer.parseInt(prev_report.getCnt());
			nCount3 = nCount1 - nCount2;
			
			report.setType1(nCount1); //금월
			report.setType2(nCount2); //이전월	
			
			
			if ( nCount3 < 0 ) report.setbCheckPlus("0");
			else report.setbCheckPlus("1");
			
			if ( nCount3 < 0 ) nCount3 = -(nCount3);
			report.setType3(nCount3); //증감
			
			list.set(i, report);
		}
		
		modelAndView.addObject("listChart5Detail", list);
		
		//개인정보 접속기록 상세점검 결과
		//개인정보 업무 행위별 결과
		List<PrivacyReport> systemCurdCount = reportSvc.findSystemCurdCount(pr);
		modelAndView.addObject("systemCurdCount", systemCurdCount);
		
		//고유식별정보 처리 상세 결과
		int totCnt4 = 0;
		
		List<PrivacyReport> typeCnt4 = reportSvc.findTypeReportCount(pr);
		if(typeCnt4.size()>0){
			modelAndView.addObject("topType", typeCnt4.get(0).getPrivacy_desc());
			for(int i=0; i<typeCnt4.size(); i++){
				totCnt4 = totCnt4 + Integer.parseInt(typeCnt4.get(i).getCnt());
			}
			modelAndView.addObject("totCnt4", totCnt4);
			modelAndView.addObject("typeCnt4", typeCnt4);
		}
		
		
		List<PrivacyReport> systemCnt = reportSvc.findSystemReportCount(pr);
		if(systemCnt.size()>0) {
			modelAndView.addObject("topSystem", systemCnt.get(0).getSystem_name());
			modelAndView.addObject("systemCnt", systemCnt);
		}
		
		List<PrivacyReport> deptCnt = reportSvc.findDeptReportCount(pr);
		if(deptCnt.size()>0) {
			modelAndView.addObject("topDept", deptCnt.get(0).getDept_name());
			modelAndView.addObject("deptCnt", deptCnt);
		}

		List<PrivacyReport> indvCnt = reportSvc.findIndvReportCount(pr);
		if(indvCnt.size()>0) {
			modelAndView.addObject("topIndv", indvCnt.get(0).getEmp_user_name());
			modelAndView.addObject("indvCnt", indvCnt);
		}
		
		modelAndView.addObject("report_type", type);
		modelAndView.addObject("download_type", download_type);
		String pdfFullPath = request.getRequestURL() +"?"+ request.getQueryString();
		modelAndView.addObject("pdfFullPath", pdfFullPath);
		
		modelAndView.setViewName("report_combined");
		return modelAndView;
	}
	
	/**
	 * @page : reportDetail_download.html
	 * @desc : 개인정보 다운로드_보고(type_6)
	 * @auth : syjung
	 */
	@RequestMapping(value = "reportDetail_download_new.html")
	public ModelAndView findReportDetailDownload_new(HttpServletRequest request,
			@RequestParam(value = "type", required = false) int type,
			@RequestParam(value = "period_type", required = false) int period_type,
			@RequestParam(value = "system_seq", required = false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date,
			@RequestParam(value = "menu_id", required = false) String menu_id,
			@RequestParam(value = "half_type", required = false) String half_type,
			@RequestParam(value = "quarter_type", required = false) String quarter_type,
			@RequestParam(value = "download_type", required = false) String download_type) throws Exception{
		
		ModelAndView modelAndView = new ModelAndView();
		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
	    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
		String date = dayTime.format(new Date(time));
		String ui_type = commonDao.getUiType();
		modelAndView.addObject("ui_type", ui_type);
		
		// 마지막날짜가 현재보다 클 경우 어제날짜까지로 지정해준다.
		
		Calendar cal = Calendar.getInstance();
		
		String compareDate = "";
				
		HttpSession session = request.getSession();
		String master = (String)session.getAttribute("master");
	    String use_reportLogo = (String)session.getAttribute("use_reportLogo");
	    String use_reportLine = (String)session.getAttribute("use_reportLine");
	    if (use_reportLogo != null && use_reportLogo.equals("Y")) {
		    String rootPath = request.getSession().getServletContext().getRealPath("/");
			String attach_path = "resources/upload/";
			String filename = commonDao.selectCurrentImage_logo2();
			String savePath = rootPath + attach_path + filename;
			File file = new File(savePath);
			if(file.exists()) {
				modelAndView.addObject("filename", filename);
			}
	    }
	    
	    //다운로드 보고서 > report_option 
	    Map<String, String> optionMap = new HashMap<String, String>();
		optionMap.put("code_id", "12");
		String proc_month = start_date.replace("-", "").substring(0, 6);
		optionMap.put("proc_month", proc_month);
		optionMap.put("period_type", String.valueOf(period_type));
		List<Map<String, String>> reportOption = reportDao.findReportOption(optionMap);
		
		//총평 데이터 반환
		String report_desc_str = reportDao.getReportDescStr(optionMap);
		
		modelAndView.addObject("report_desc_str", report_desc_str);
		modelAndView.addObject("reportOption", reportOption);
	    
	    modelAndView.addObject("use_reportLogo", use_reportLogo);
	    modelAndView.addObject("use_reportLine", use_reportLine);
		modelAndView.addObject("masterflag", master);
		modelAndView.addObject("report_type", type);
		modelAndView.addObject("period_type", period_type);
		modelAndView.addObject("start_date", start_date);
		modelAndView.addObject("end_date", end_date);
		modelAndView.addObject("date", date);
		modelAndView.addObject("system_seq", system_seq);
		modelAndView.addObject("logo_report_url", logo_report_url);
		modelAndView.addObject("menu_id", menu_id);
		modelAndView.addObject("half_type", half_type);
		modelAndView.addObject("quarter_type", quarter_type);

		//선택 시스템 목록
		List<String> syslist = new ArrayList<String>();
		String tmpList[] = system_seq.split(",");
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}
		syslist.add("");
		
		String sDate[] = start_date.split("-");
		String eDate[] = end_date.split("-");

		int strYear = Integer.parseInt(sDate[0]);
		int strMonth = Integer.parseInt(sDate[1]);
		int endYear = Integer.parseInt(eDate[0]);
		int endMonth = Integer.parseInt(eDate[1]);
		int diffMon = (endYear - strYear)* 12 + (endMonth - strMonth);
		String stDate = start_date.replaceAll("-", "").substring(0,6);
		String edDate = end_date.replaceAll("-", "").substring(0,6);
		
		PrivacyReport pr = new PrivacyReport();			//선택 기간
		PrivacyReport prCompare = new PrivacyReport();	//비교 기간 (동년전월, 동년 이전분기)
		PrivacyReport pr1 = new PrivacyReport();		//
	    
		String compare_type = "1";
		modelAndView.addObject("compare_type", compare_type);
		if (period_type == 1) {
			pr.setStart_date(stDate);
			pr.setEnd_date(edDate);
			pr.setSysList(syslist);
			
			if(compare_type == "1") {			//동년 전월 (compare_type =1)
				cal.set(strYear, strMonth-1, 1);
			    cal.add(Calendar.MONTH, -1);    	
			    compareDate = dateFormatter.format(cal.getTime());
				pr.setCompare_start_date(compareDate.substring(0,6));		
				pr.setCompare_end_date(compareDate.substring(0,6));			
				
				prCompare.setStart_date(compareDate.substring(0,6));
				prCompare.setEnd_date(compareDate.substring(0,6));
				prCompare.setSysList(syslist);
			}else if(compare_type == "2") {		//전년동월 (compare_type =2)
				pr.setCompare_start_date(strYear-1+stDate.substring(4, 6));
				pr.setCompare_end_date(strYear-1+edDate.substring(4, 6));
				
				//비교 기간1(전년동월)
				prCompare.setStart_date(strYear-1+stDate.substring(4, 6));
				prCompare.setEnd_date(strYear-1+edDate.substring(4, 6));
				prCompare.setSysList(syslist);
			}
			
			
			pr1.setStart_date(strYear+"01");		
			pr1.setEnd_date(edDate);				
		    pr1.setSysList(syslist);				
			pr1.setMonth(strMonth+"");
			pr1.setPeriod_type("1");
			
		}else if(period_type == 2) {	//분기
			pr1.setStart_date(stDate);
			pr1.setEnd_date(edDate);
			pr1.setSysList(syslist);
			pr1.setPeriod_type("2");
			pr1.setQuarter_type(quarter_type);
			
			pr.setStart_date(stDate);
			pr.setEnd_date(edDate);
			pr.setSysList(syslist);
			if(compare_type == "1") {			//동년 전분기 (compare_type =1)
				cal.set(strYear, strMonth-1, 1);		
			    cal.add(Calendar.MONTH, -3);    		
			    compareDate = dateFormatter.format(cal.getTime());
				pr.setCompare_start_date(compareDate.substring(0,6));
				prCompare.setStart_date(compareDate.substring(0,6));
				
				cal.set(strYear, strMonth-1, 1);		
			    cal.add(Calendar.MONTH, -1);    		
			    compareDate = dateFormatter.format(cal.getTime());
				pr.setCompare_end_date(compareDate.substring(0,6));
				prCompare.setEnd_date(compareDate.substring(0,6));
				prCompare.setSysList(syslist);
			}else if(compare_type == "2") {		//전년 동분기 (compare_type =2)
				pr.setCompare_start_date(strYear-1+stDate.substring(4, 6));
				pr.setCompare_end_date(strYear-1+edDate.substring(4, 6));
				//전년 동일분기
				prCompare.setStart_date(strYear-1+stDate.substring(4, 6));
				prCompare.setEnd_date(strYear-1+edDate.substring(4, 6));
				prCompare.setSysList(syslist);
			}
			
		}
		else if(period_type == 4) {	//년
			pr.setStart_date(stDate);
			pr.setEnd_date(edDate);
			pr.setSysList(syslist);
			pr.setCompare_start_date(strYear-1+stDate.substring(4, 6));
			pr.setCompare_end_date(strYear-1+edDate.substring(4, 6));
			
			pr1.setStart_date(stDate);
			pr1.setEnd_date(edDate);
			pr1.setSysList(syslist);
			pr1.setPeriod_type("4");
			
			//전년
			prCompare.setStart_date(strYear-1+stDate.substring(4, 6));
			prCompare.setEnd_date(strYear-1+edDate.substring(4, 6));
			prCompare.setSysList(syslist);
			
		}
	    
		List<SystemMaster> systems = reportSvc.findSystemMasterDetailList(pr);
		modelAndView.addObject("systems", systems);
		
		int diffMonth = getDiffMonth(pr);
		modelAndView.addObject("diffMonth", diffMonth);
		
		int logCnt = reportSvc.downloadTotalLogCnt(pr);				//다운로드 로그
		modelAndView.addObject("logCnt", logCnt);
		int prevLogCnt = reportSvc.downloadTotalLogCnt(prCompare);	//개인정보 접속기록 비교기간 총 처리량(로그건수)
		modelAndView.addObject("prevLogCnt", prevLogCnt);
		
		int compareLogcnt = logCnt - prevLogCnt;
		String checkPlue1 ="";
		String checkPlue2 ="";
		if ( compareLogcnt < 0 ) {
			checkPlue1 ="0";
		}
		else {
			checkPlue1 ="1";
		}
		if ( compareLogcnt < 0 ) {
			compareLogcnt = -(compareLogcnt);
		}
		modelAndView.addObject("compareLogcnt", compareLogcnt);
		modelAndView.addObject("checkPlue1", checkPlue1);
		
		int privacyCnt = reportSvc.downloadTotalCnt(pr);				//개인정보 접속기록 이용량(개인정보유형처리(이용)건수)
		modelAndView.addObject("privacyCnt", privacyCnt);
		
		int prevPrivacyCnt = reportSvc.downloadTotalCnt(prCompare);	//개인정보 접속기록 비교기간 이용량(개인정보유형처리(이용)건수)
		modelAndView.addObject("prevPrivacyCnt", prevPrivacyCnt);
		
		int comparePrivacyCnt = privacyCnt - prevPrivacyCnt;
		if ( comparePrivacyCnt < 0 ) {
			checkPlue2 ="0";
		}
		else {
			checkPlue2 ="1";
		}
		if ( comparePrivacyCnt < 0 ) {
			comparePrivacyCnt = -(comparePrivacyCnt);
		}
		modelAndView.addObject("comparePrivacyCnt", comparePrivacyCnt);
		modelAndView.addObject("checkPlue2", checkPlue2);
		
		
		List<PrivacyReport> privacyProcessCntBySystem = reportSvc.downloadProcessCntBySystem(pr1);
		modelAndView.addObject("privacyProcessCntBySystem", privacyProcessCntBySystem);
		
		List<PrivacyReport> privacyUseCntBySystem = reportSvc.downloadUseCntBySystem(pr1);			//2.시스템별 개인정보 접속기록 이용현황
		modelAndView.addObject("privacyUseCntBySystem", privacyUseCntBySystem);
		
		List<PrivacyReport> comparePrevCntBySystem = reportSvc.downloadcomparePrevCntBySystem(pr); 			//3.시스템별 개인정보 접속기록 현황(비교)
		
		
		//단위데이터 미사용
		modelAndView.addObject("compareUnit01", "");
		modelAndView.addObject("compareUnit01_num", "");
		modelAndView.addObject("comparePrevCntBySystem", comparePrevCntBySystem);
		
		List<PrivacyReport> comparePrevCntBySystemTop3 = reportSvc.downloadcomparePrevCntBySystemTop3(pr); 			//3.시스템별 개인정보 접속기록 현황(비교)
		
		//단위데이터 사용 / 미사용
		modelAndView.addObject("compareUnit03", "");
		modelAndView.addObject("comparePrevCntBySystemTop3", comparePrevCntBySystemTop3);
		
		//시스템 개인정보 top3 이하 데이터
		PrivacyReport comparePrevCntBySystemSum = reportSvc.downloadComparePrevCntBySystemSum(pr);
		modelAndView.addObject("comparePrevCntBySystemSum", comparePrevCntBySystemSum);
		
		List<PrivacyReport> privacyProcessCntByDept = reportSvc.downloadProcessCntByDept(pr1);		//4.소속별 개인정보 접속기록 처리현황
		if(privacyProcessCntByDept.size()==1){
			privacyProcessCntByDept = new ArrayList<PrivacyReport>();
		}
		modelAndView.addObject("privacyProcessCntByDept", privacyProcessCntByDept);
		
		List<PrivacyReport> privacyUseCntByDept = reportSvc.downloadUseCntByDept(pr1);				//5.부서별 개인정보 접속기록 이용현황
		if(privacyUseCntByDept.size()==1){
			privacyUseCntByDept = new ArrayList<PrivacyReport>();
		}
		modelAndView.addObject("privacyUseCntByDept", privacyUseCntByDept);
		
		List<PrivacyReport> comparePrevCntByDept = reportSvc.downloadComparePrevCntByDept(pr); 				//6.소속별 개인정보 접속기록 현황(비교)
		if(comparePrevCntByDept.size()==1){
			comparePrevCntByDept = new ArrayList<PrivacyReport>();
		}
		
		//단위데이터 미사용
		modelAndView.addObject("compareUnit02", "");
		modelAndView.addObject("comparePrevCntByDept", comparePrevCntByDept);
		
		//소속 top5
		List<PrivacyReport> comparePrevCntByDeptTop5 = reportSvc.downloadComparePrevCntByDeptTop5(pr); 				//6.소속별 개인정보 접속기록 현황(비교)
		
		//단위데이터 사용 / 미사용
		//modelAndView.addObject("compareUnit04", comparePrevCntByDeptTop5.get(0).getDataUnit());
		modelAndView.addObject("compareUnit04", "");
		modelAndView.addObject("comparePrevCntByDeptTop5", comparePrevCntByDeptTop5);
		
		
		List<PrivacyReport> comparePrevCntByResultType = new ArrayList<>();
		List<Code> resultTypeList = reportSvc.findResultTypeList();		
		
		for(int i=0; i<resultTypeList.size(); i++) {
			Code code = resultTypeList.get(i);
			pr1.setType("TYPE"+code.getCode_id());
			pr.setType("TYPE"+code.getCode_id());
			//PrivacyReport returnResult = reportSvc.privacyUseCntByResultType(pr1);					//7.개인정보 유형별 접속기록 이용현황
			PrivacyReport returnResult2 = reportSvc.comparePrevCntByResultType_download(pr);					//7.개인정보 유형별 접속기록 이용현황(비교)
			//returnResult.setCode_name(code.getCode_name());
			returnResult2.setCode_name(code.getCode_name());
			comparePrevCntByResultType.add(i, returnResult2);
		}
		
		//단위데이터 사용 / 미사용
		//modelAndView.addObject("compareUnit05", comparePrevCntByResultType.get(0).getDataUnit());
		modelAndView.addObject("compareUnit05", "");
		modelAndView.addObject("comparePrevCntByResultType", comparePrevCntByResultType);
		
		
		if(period_type == 4) {
			
			List<PrivacyReport> privacylogByResult = new ArrayList<>();
			//List<Code> resultTypeList = reportSvc.findResultTypeList();
			resultTypeList = reportSvc.findResultTypeList();
			for(int i=0; i<resultTypeList.size(); i++) {
				Code code = resultTypeList.get(i);
				pr.setType("TYPE"+code.getCode_id());
				PrivacyReport returnResult = reportSvc.downloadlogByResultType(pr);
				returnResult.setCode_name(code.getCode_name());
				privacylogByResult.add(i, returnResult);
			}
			modelAndView.addObject("privacylogByResult", privacylogByResult);
			
			//전월비교
		}else {

		}
		
		
		//차트 데이터 > grid 출력
		List<PrivacyReport> chart_privacyUseCntBySystem_dn = reportSvc.chart_privacyUseCntBySystem_download(pr);
		List<PrivacyReport> reportByDept_dn = reportSvc.chart_privacyUseCntByDept_download(pr);
		//PrivacyReport returnResult = reportSvc.chart_privacyUseCntByResultType_download(pr);
		
		List<PrivacyReport> chart_privacyUseCntByResultType = new ArrayList<>();
		//List<Code> resultTypeList = reportSvc.findResultTypeList();
		resultTypeList = reportSvc.findResultTypeList();
		for(int i=0; i<resultTypeList.size(); i++) {
			Code code = resultTypeList.get(i);
			pr.setType("TYPE"+code.getCode_id());
			PrivacyReport returnResult = reportSvc.chart_privacyUseCntByResultType_download(pr);
			returnResult.setCode_name(code.getCode_name());
			chart_privacyUseCntByResultType.add(i, returnResult);
		}
		
		ListComparator comp = new ListComparator();
		Collections.sort(chart_privacyUseCntByResultType, comp); //list sorting
		//Collections.sort(comparePrevCntByResultType, comp);//list sorting

		List<PrivacyReport> fileExtDatas_dn = reportSvc.getFileExtension_chart(pr1);
		modelAndView.addObject("ext_unit_01", "");
		
		List<PrivacyReport> fileExtData_dn_compare = reportSvc.getFileExtensionCompare_chart(pr);
		
		modelAndView.addObject("ext_unit_02", "");
		modelAndView.addObject("ext_unit_num_02", "");
		
		List<PrivacyReport> fileExtData_dn_compare_top5 = reportSvc.getFileExtensionCompare_chart_top5(pr);
		modelAndView.addObject("ext_unit_03", "");
		PrivacyReport fileExtData_after = reportSvc.getFileExtensionCompare_after(pr);
		
		modelAndView.addObject("fileExtData_dn_compare", fileExtData_dn_compare);
		modelAndView.addObject("fileExtData_dn_compare_top5", fileExtData_dn_compare_top5);
		modelAndView.addObject("fileExtData_after", fileExtData_after);
		
		
		//[다운로드보고서] 1. 시스템별 개인정보 다운로드 처리현황
		List<PrivacyReport> wordGrid = reportSvc.getWordReportData1(pr1);
		modelAndView.addObject("grid_unit_01", "");
		modelAndView.addObject("grid_unit_01_num", "");
		
		List<PrivacyReport> wordGrid02 = reportSvc.getWordReportData2(pr1);
		modelAndView.addObject("grid_unit_02", "");
		modelAndView.setViewName("report_download_new");
	    modelAndView.addObject("reportType", type);
	    modelAndView.addObject("authorize", reportSvc.reportAuthorizeInfo());
	    modelAndView.addObject("resultTypeList", chart_privacyUseCntByResultType);
		modelAndView.addObject("download_type", download_type);
		String pdfFullPath = request.getRequestURL() +"?"+ request.getQueryString();
		modelAndView.addObject("pdfFullPath", pdfFullPath);
		modelAndView.addObject("fileExtDatas_dn", fileExtDatas_dn);
		modelAndView.addObject("ui_type", ui_type);
		modelAndView.addObject("wordGrid", wordGrid);
		modelAndView.addObject("wordGrid02", wordGrid02);
		return modelAndView;
	}
	
	
	/*@RequestMapping(value = "reportDetail_download_new.html")
	public ModelAndView findReportDetailDownload_new(HttpServletRequest request,
			@RequestParam(value = "type", required = false) int type,
			@RequestParam(value = "period_type", required = false) int period_type,
			@RequestParam(value = "system_seq", required = false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date,
			@RequestParam(value = "menu_id", required = false) String menu_id,
			@RequestParam(value = "download_type", required = false) String download_type) {
		
		HttpSession session = request.getSession();
		// 다운로드로그 풀스캔연동여부
		String use_fullscan = (String) session.getAttribute("use_fullscan");
		
		ModelAndView modelAndView = new ModelAndView();
		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
		String date = dayTime.format(new Date(time));
		String ui_type = commonDao.getUiType();

		// 마지막날짜가 현재보다 클 경우 어제날짜까지로 지정해준다.
		
		try {
			Date tmpDate = dayTime.parse(end_date);
			int compare = tmpDate.compareTo(new Date());
			if (compare >= 0) {
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DAY_OF_MONTH, -1);
				end_date = dayTime.format(cal.getTime());
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		String compareDate = "";

		PrivacyReport pr = new PrivacyReport(); // 선택 기간
		PrivacyReport prCompare = new PrivacyReport(); // 비교 기간

		
		String master = (String) session.getAttribute("master");

		String use_reportLogo = (String) session.getAttribute("use_reportLogo");
		modelAndView.addObject("use_reportLogo", use_reportLogo);
		if (use_reportLogo != null && use_reportLogo.equals("Y")) {
			String rootPath = request.getSession().getServletContext().getRealPath("/");
			String attach_path = "resources/upload/";

			String filename = commonDao.selectCurrentImage_logo2();
			String savePath = rootPath + attach_path + filename;

			File file = new File(savePath);

			if (file.exists()) {
				modelAndView.addObject("filename", filename);
			}
		}

		String use_reportLine = (String) session.getAttribute("use_reportLine");
		
		modelAndView.addObject("use_reportLine", use_reportLine);
		modelAndView.addObject("masterflag", master);
		modelAndView.addObject("report_type", type);
		modelAndView.addObject("period_type", period_type);
		modelAndView.addObject("start_date", start_date);
		modelAndView.addObject("end_date", end_date);
		modelAndView.addObject("date", date);
		modelAndView.addObject("system_seq", system_seq);
		modelAndView.addObject("use_studentId", use_studentId);
		modelAndView.addObject("logo_report_url", logo_report_url);
		modelAndView.addObject("menu_id", menu_id);

		List<String> syslist = new ArrayList<String>();
		String tmpList[] = system_seq.split(",");

		for (int i = 0; i < tmpList.length; ++i) {
			syslist.add(tmpList[i]);
		}

		String sDate[] = start_date.split("-");
		String eDate[] = end_date.split("-");

		int strYear = Integer.parseInt(sDate[0]);
		int strMonth = Integer.parseInt(sDate[1]);
		int endYear = Integer.parseInt(eDate[0]);
		int endMonth = Integer.parseInt(eDate[1]);

		int diffMon = (endYear - strYear) * 12 + (endMonth - strMonth);

		String stDate = start_date.replaceAll("-", "");
		String edDate = end_date.replaceAll("-", "");
		
		pr.setStart_date(stDate);
		pr.setEnd_date(edDate);
		pr.setSysList(syslist);
		
		//start, end date의 차이 개월
		int diffMonTemp = getDiffMonth(pr);
		modelAndView.addObject("diffMon", diffMonTemp);
		//다운로드 총 횟수
		int downCnt = reportSvc.findDownCountTot(pr);
		modelAndView.addObject("downCnt", downCnt);
		//시스템별 다운로드 총 횟수가 젤 많은 시스템명 가져옴
		String downBySysCntName = reportSvc.findDownBySysCountTot(pr);
		modelAndView.addObject("downBySysCntName", downBySysCntName);
		//점검 대상 다운로드 개인정보 처리시스템 가져오기
		if(!pr.getSysList().isEmpty()) {
			List<SystemMaster> systemsDownLst = reportSvc.findSystemMasterDetailDownList(pr);
			for(SystemMaster sm : systemsDownLst) {
				PrivacyReport prt = new PrivacyReport();
				prt.setSystem_seq(sm.getSystem_seq());
				prt.setStart_date(stDate);
				prt.setEnd_date(edDate);
				List<PrivacyReport> resDownLst = reportDao.getMajorDownResultType(prt);
				String result_type = "";
				if (resDownLst.size() > 0) {
					for(PrivacyReport prDownVo : resDownLst) {
						String text = prDownVo.getPrivacy_desc()  + "(" + prDownVo.getCnt() + ") ";
						result_type += text;
					}
				} else {
					result_type = "데이터없음";
				}
				sm.setResult_type(result_type);
			}
			modelAndView.addObject("systemsDownLst", systemsDownLst);
		}
		
		Calendar cal = Calendar.getInstance();
		cal.set(strYear, strMonth - 1, 1); // 선택기간 시작월 1일

		cal.add(Calendar.MONTH, -1); // 비교 기간 끝(선택기간 한달전)
		compareDate = dateFormatter.format(cal.getTime());
		edDate = compareDate.substring(0, 6) + "31";

		cal.add(Calendar.MONTH, -diffMon); // 비교 기간 시작
		compareDate = dateFormatter.format(cal.getTime());
		stDate = compareDate.substring(0, 6) + "01";

		prCompare.setStart_date(stDate);
		prCompare.setEnd_date(edDate);
		prCompare.setSysList(syslist);
		
		//1.시스템별 현황 TOP5
		List<PrivacyReport> findDownloadBySysTop5 = reportSvc.findDownloadCntBySysTop5(pr);
		modelAndView.addObject("findDownloadBySysTop5",findDownloadBySysTop5);
		//2.부서별 현황 TOP5
		List<PrivacyReport> findDownloadByDeptTop5 = reportSvc.findDownloadCntByDeptTop5(pr);
		modelAndView.addObject("findDownloadByDeptTop5",findDownloadByDeptTop5);
		//3.개인정보 유형별 현황 TOP5
		List<PrivacyReport> findDownloadCntByResultTypeTop5 = reportSvc.findDownloadCntByResultTypeTop5(pr);
		modelAndView.addObject("findDownloadCntByResultTypeTop5",findDownloadCntByResultTypeTop5);
		
		
		//상세결과
		//1.시스템별 개인정보 다운로드 현황
		List<PrivacyReport> findDownloadCntBySys = reportSvc.findDownloadCntBySys(pr);	//시스템별 개인정보 다운로드 횟수
		List<PrivacyReport> findResultCntBySys = reportSvc.findResultCntBySys(pr);		//시스템별 개인정보 건수
		
		for (int i = 0; i < findDownloadCntBySys.size(); i++){
			
			for(int j=0; j< findResultCntBySys.size(); j++){
				if(findDownloadCntBySys.get(i).getSystem_seq() == findResultCntBySys.get(j).getSystem_seq() ||
						findDownloadCntBySys.get(i).getSystem_seq().equals(findResultCntBySys.get(j).getSystem_seq())){
					findDownloadCntBySys.get(i).setCnt2(findResultCntBySys.get(j).getCnt2());
				}
			}
		}
		modelAndView.addObject("findDownloadCntBySys",findDownloadCntBySys);
		
		//2.주요 부서별 개인정보 다운로드 현황 TOP30
		List<PrivacyReport> findDownloadCntByDeptTop30 = reportSvc.findDownloadCntByDeptTop30(pr);	//주요 부서별 개인정보 다운로드 현황 TOP30
		List<PrivacyReport> findResultCntByDeptTop30 = reportSvc.findResultCntByDeptTop30(pr);		//주요 부서별 개인정보 건수 TOP30
		
		for (int i = 0; i < findDownloadCntByDeptTop30.size(); i++){
			
			for(int j=0; j< findResultCntByDeptTop30.size(); j++){
				if(findDownloadCntByDeptTop30.get(i).getDept_id() == findResultCntByDeptTop30.get(j).getDept_id() ||
						findDownloadCntByDeptTop30.get(i).getDept_id().equals(findResultCntByDeptTop30.get(j).getDept_id())){
					findDownloadCntByDeptTop30.get(i).setCnt2(findResultCntByDeptTop30.get(j).getCnt2());
				}
			}
		}
		modelAndView.addObject("findDownloadCntByDeptTop30",findDownloadCntByDeptTop30);
		
		
		//3.주요 사용자별 개인정보 다운로드 현황 TOP10
		List<PrivacyReport> findDownloadCntByUserTop10 = reportSvc.findDownloadCntByUserTop10(pr);	//주요 사용자별 개인정보 다운로드 현황 TOP10
		List<PrivacyReport> findResultCntByUserTop10 = reportSvc.findResultCntByUserTop10(pr);		//주요 사용자별 개인정보 건수 TOP10
		List<PrivacyReport> findSystemNameByUserTop10 = reportSvc.findSystemNameByUserTop10(pr);	//주요 사용자 접근 시스템
		
		
		for (int i = 0; i < findDownloadCntByUserTop10.size(); i++){
			
			for(int j=0; j< findResultCntByUserTop10.size(); j++){
				if(findDownloadCntByUserTop10.get(i).getEmp_user_id() == findResultCntByUserTop10.get(j).getEmp_user_id() ||
						findDownloadCntByUserTop10.get(i).getEmp_user_id().equals(findResultCntByUserTop10.get(j).getEmp_user_id())){
					findDownloadCntByUserTop10.get(i).setCnt2(findResultCntByUserTop10.get(j).getCnt2());
				}
			}
			
			for(int j=0; j< findSystemNameByUserTop10.size(); j++){
				if(findDownloadCntByUserTop10.get(i).getEmp_user_id() == findSystemNameByUserTop10.get(j).getEmp_user_id() ||
						findDownloadCntByUserTop10.get(i).getEmp_user_id().equals(findSystemNameByUserTop10.get(j).getEmp_user_id())){
					if(findDownloadCntByUserTop10.get(i).getSystem_name()=="" || findDownloadCntByUserTop10.get(i).getSystem_name().equals("")){
						findDownloadCntByUserTop10.get(i).setSystem_name(findSystemNameByUserTop10.get(j).getSystem_name());
					}else{
					findDownloadCntByUserTop10.get(i).setSystem_name(findDownloadCntByUserTop10.get(i).getSystem_name()+", "+findSystemNameByUserTop10.get(j).getSystem_name());
					}
				}
			}
		}
		modelAndView.addObject("findDownloadCntByUserTop10",findDownloadCntByUserTop10);
		
		//4.주요 URL별 다운로드 현황 TOP5
		List<PrivacyReport> findDownloadCntByUrlTop5 = reportSvc.findDownloadCntByUrlTop5(pr);	//주요 URL별 다운로드 현황 TOP5
		List<PrivacyReport> findResultCntByUrlTop5 = reportSvc.findResultCntByUrlTop5(pr);		//주요 URL별 개인정보 건수 TOP5
		
		for (int i = 0; i < findDownloadCntByUrlTop5.size(); i++){
			
			for(int j=0; j< findResultCntByUrlTop5.size(); j++){
				if(findDownloadCntByUrlTop5.get(i).getRequrl() == findResultCntByUrlTop5.get(j).getRequrl() ||
						findDownloadCntByUrlTop5.get(i).getRequrl().equals(findResultCntByUrlTop5.get(j).getRequrl())){
					findDownloadCntByUrlTop5.get(i).setCnt2(findResultCntByUrlTop5.get(j).getCnt2());
				}
			}
		}
		modelAndView.addObject("findDownloadCntByUrlTop5",findDownloadCntByUrlTop5);
		
		//5.다운로드한 주요 개인정보 유형 TOP10
		List<PrivacyReport> findDownloadCntByResultTop10 = reportSvc.findDownloadCntByResultTop10(pr);	//주요 개인정보 유형별 다운로드한 현황 TOP10
		List<PrivacyReport> findResultCntByResultTop10 = reportSvc.findResultCntByResultTop10(pr);		//주요 개인정보 유형별 개인정보 건수 TOP10 
		
		for (int i = 0; i < findDownloadCntByResultTop10.size(); i++){
			
			for(int j=0; j< findResultCntByResultTop10.size(); j++){
				if(findDownloadCntByResultTop10.get(i).getResult_type() == findResultCntByResultTop10.get(j).getResult_type() ||
						findDownloadCntByResultTop10.get(i).getResult_type().equals(findResultCntByResultTop10.get(j).getResult_type())){
					findDownloadCntByResultTop10.get(i).setCnt2(findResultCntByResultTop10.get(j).getCnt2());
				}
			}
		}
		modelAndView.addObject("findDownloadCntByResultTop10",findDownloadCntByResultTop10);
		
		//6.주요 사유별 개인정보 다운로드 건수 TOP10
		List<PrivacyReport> findDownloacCntByReasonTop10 = reportSvc.findDownloacCntByReasonTop10(pr);	//주요 사유별 개인정보 다운로드 현황 TOP10
		List<PrivacyReport> findResultCntByReasonTop10 = reportSvc.findResultCntByReasonTop10(pr);		//주요 사유별 개인정보 건수 TOP10
		
		for (int i = 0; i < findDownloacCntByReasonTop10.size(); i++){
			
			for(int j=0; j< findResultCntByReasonTop10.size(); j++){
				if(findDownloacCntByReasonTop10.get(i).getReason_code() == findResultCntByReasonTop10.get(j).getReason_code() ||
						findDownloacCntByReasonTop10.get(i).getReason_code().equals(findResultCntByReasonTop10.get(j).getReason_code())){
					findDownloacCntByReasonTop10.get(i).setCnt2(findResultCntByReasonTop10.get(j).getCnt2());
				}
			}
		}
		modelAndView.addObject("findDownloacCntByReasonTop10",findDownloacCntByReasonTop10);		
		
		modelAndView.addObject("download_type", download_type);
		String pdfFullPath = request.getRequestURL() +"?"+ request.getQueryString();
		modelAndView.addObject("pdfFullPath", pdfFullPath);
		
		modelAndView.addObject("use_fullscan", use_fullscan);
		modelAndView.setViewName("report_download_new");
		
		return modelAndView;
	}*/
	
	
	//page : reportEmpLevel.html 수준진단보고
	@RequestMapping(value="reportEmpLevel_new.html")
	public ModelAndView findReportEmpLevel_new(HttpServletRequest request,
			@ModelAttribute("search") SearchSearch search, 
			@RequestParam(value = "type", required = false) int type,
			@RequestParam(value = "period_type", required = false) int period_type,
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date,
			@RequestParam(value = "menu_id", required = false) String menu_id,
			@RequestParam(value = "half_type", required = false) String half_type,
			@RequestParam(value = "quarter_type", required = false) String quarter_type,
			@RequestParam(value = "download_type", required = false) String download_type) {
		
		ModelAndView modelAndView = new ModelAndView();
		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
	    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
		String date = dayTime.format(new Date(time));
		String ui_type = commonDao.getUiType();
		modelAndView.addObject("ui_type", ui_type);
		
		// 마지막날짜가 현재보다 클 경우 어제날짜까지로 지정해준다.
		Calendar cal = Calendar.getInstance();
		String compareDate = "";
				
		HttpSession session = request.getSession();
		String master = (String)session.getAttribute("master");
	    String use_reportLogo = (String)session.getAttribute("use_reportLogo");
	    String use_reportLine = (String)session.getAttribute("use_reportLine");
	    if (use_reportLogo != null && use_reportLogo.equals("Y")) {
		    String rootPath = request.getSession().getServletContext().getRealPath("/");
			String attach_path = "resources/upload/";
			String filename = commonDao.selectCurrentImage_logo2();
			String savePath = rootPath + attach_path + filename;
			File file = new File(savePath);
			if(file.exists()) {
				modelAndView.addObject("filename", filename);
			}
	    }
	    
	    
	    //수준진단보고서 > report_option
	    Map<String, String> optionMap = new HashMap<String, String>();
		optionMap.put("code_id", "13");
		String proc_month = start_date.replace("-", "").substring(0, 6);
		optionMap.put("proc_month", proc_month);
		optionMap.put("period_type", String.valueOf(period_type));
		List<Map<String, String>> reportOption = reportDao.findReportOption(optionMap);
		
		//총평 데이터 반환
		String report_desc_str = reportDao.getReportDescStr(optionMap);
		
		modelAndView.addObject("report_desc_str", report_desc_str);
		modelAndView.addObject("reportOption", reportOption);
	    modelAndView.addObject("use_reportLogo", use_reportLogo);
	    modelAndView.addObject("use_reportLine", use_reportLine);
		modelAndView.addObject("masterflag", master);
		modelAndView.addObject("report_type", type);
		modelAndView.addObject("period_type", period_type);
		modelAndView.addObject("start_date", start_date);
		modelAndView.addObject("end_date", end_date);
		modelAndView.addObject("date", date);
		modelAndView.addObject("system_seq", system_seq);
		modelAndView.addObject("logo_report_url", logo_report_url);
		modelAndView.addObject("menu_id", menu_id);
		modelAndView.addObject("half_type", half_type);
		modelAndView.addObject("quarter_type", quarter_type);

		//선택 시스템 목록
		List<String> syslist = new ArrayList<String>();
		String tmpList[] = system_seq.split(",");
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}
		syslist.add("");
		
		String sDate[] = start_date.split("-");
		String eDate[] = end_date.split("-");

		int strYear = Integer.parseInt(sDate[0]);
		int strMonth = Integer.parseInt(sDate[1]);
		int endYear = Integer.parseInt(eDate[0]);
		int endMonth = Integer.parseInt(eDate[1]);
		int diffMon = (endYear - strYear)* 12 + (endMonth - strMonth);
		String stDate = start_date.replaceAll("-", "").substring(0,6);
		String edDate = end_date.replaceAll("-", "").substring(0,6);
		
		PrivacyReport pr = new PrivacyReport();			//선택 기간
		PrivacyReport prCompare = new PrivacyReport();	//비교 기간 (동년전월, 동년 이전분기)
		PrivacyReport pr1 = new PrivacyReport();		//
	    
		
		// 접속유형 default 세팅
		String mapping_id = (String) session.getAttribute("mapping_id");
		if(mapping_id.equals("Y")) {
			if ( search.getMapping_id() == null ) 
				search.setMapping_id("code1");
			if ( search.getMapping_id().equals("code2") ) 
				search.setMapping_id(null);
		}else {
			if(search.getMapping_id() == null || search.getMapping_id().equals("code2"))
				search.setMapping_id(null);
			else
				search.setMapping_id("code1");
		}
		
		
		String compare_type = "1";
		modelAndView.addObject("compare_type", compare_type);
		if (period_type == 1) {
			pr.setStart_date(stDate);
			pr.setEnd_date(edDate);
			pr.setSysList(syslist);
			
			if(compare_type == "1") {			//동년 전월 (compare_type =1)
				cal.set(strYear, strMonth-1, 1);
			    cal.add(Calendar.MONTH, -1);    	
			    compareDate = dateFormatter.format(cal.getTime());
				pr.setCompare_start_date(compareDate.substring(0,6));		
				pr.setCompare_end_date(compareDate.substring(0,6));			
				
				prCompare.setStart_date(compareDate.substring(0,6));
				prCompare.setEnd_date(compareDate.substring(0,6));
				prCompare.setSysList(syslist);
			}else if(compare_type == "2") {		//전년동월 (compare_type =2)
				pr.setCompare_start_date(strYear-1+stDate.substring(4, 6));
				pr.setCompare_end_date(strYear-1+edDate.substring(4, 6));
				
				//비교 기간1(전년동월)
				prCompare.setStart_date(strYear-1+stDate.substring(4, 6));
				prCompare.setEnd_date(strYear-1+edDate.substring(4, 6));
				prCompare.setSysList(syslist);
			}
			
			
			pr1.setStart_date(strYear+"01");		
			pr1.setEnd_date(edDate);				
		    pr1.setSysList(syslist);				
			pr1.setMonth(strMonth+"");
			pr1.setPeriod_type("1");
			
		}else if(period_type == 2) {	//분기
			pr1.setStart_date(stDate);
			pr1.setEnd_date(edDate);
			pr1.setSysList(syslist);
			pr1.setPeriod_type("2");
			pr1.setQuarter_type(quarter_type);
			
			pr.setStart_date(stDate);
			pr.setEnd_date(edDate);
			pr.setSysList(syslist);
			if(compare_type == "1") {			//동년 전분기 (compare_type =1)
				cal.set(strYear, strMonth-1, 1);		
			    cal.add(Calendar.MONTH, -3);    		
			    compareDate = dateFormatter.format(cal.getTime());
				pr.setCompare_start_date(compareDate.substring(0,6));
				prCompare.setStart_date(compareDate.substring(0,6));
				
				cal.set(strYear, strMonth-1, 1);		
			    cal.add(Calendar.MONTH, -1);    		
			    compareDate = dateFormatter.format(cal.getTime());
				pr.setCompare_end_date(compareDate.substring(0,6));
				prCompare.setEnd_date(compareDate.substring(0,6));
				prCompare.setSysList(syslist);
			}else if(compare_type == "2") {		//전년 동분기 (compare_type =2)
				pr.setCompare_start_date(strYear-1+stDate.substring(4, 6));
				pr.setCompare_end_date(strYear-1+edDate.substring(4, 6));
				//전년 동일분기
				prCompare.setStart_date(strYear-1+stDate.substring(4, 6));
				prCompare.setEnd_date(strYear-1+edDate.substring(4, 6));
				prCompare.setSysList(syslist);
			}
			
		}
		else if(period_type == 4) {	//년
			pr.setStart_date(stDate);
			pr.setEnd_date(edDate);
			pr.setSysList(syslist);
			pr.setCompare_start_date(strYear-1+stDate.substring(4, 6));
			pr.setCompare_end_date(strYear-1+edDate.substring(4, 6));
			
			pr1.setStart_date(stDate);
			pr1.setEnd_date(edDate);
			pr1.setSysList(syslist);
			pr1.setPeriod_type("4");
			
			//전년
			prCompare.setStart_date(strYear-1+stDate.substring(4, 6));
			prCompare.setEnd_date(strYear-1+edDate.substring(4, 6));
			prCompare.setSysList(syslist);
			
		}
	    
		List<SystemMaster> systems = reportSvc.findSystemMasterDetailList(pr);
		modelAndView.addObject("systems", systems);
		
		int diffMonth = getDiffMonth(pr);
		modelAndView.addObject("diffMonth", diffMonth);
		
		int logCnt = reportSvc.findPrivacyTotalLogCnt(pr);				//개인정보 접속기록 총 처리량(로그건수)
		modelAndView.addObject("logCnt", logCnt);
		int prevLogCnt = reportSvc.findPrivacyTotalLogCnt(prCompare);	//개인정보 접속기록 비교기간 총 처리량(로그건수)
		modelAndView.addObject("prevLogCnt", prevLogCnt);
		
		int compareLogcnt = logCnt - prevLogCnt;
		modelAndView.addObject("compareLogcnt", compareLogcnt);
		
		int privacyCnt = reportSvc.findPrivacyTotalCnt(pr);				//개인정보 접속기록 이용량(개인정보유형처리(이용)건수)
		modelAndView.addObject("privacyCnt", privacyCnt);
		
		int prevPrivacyCnt = reportSvc.findPrivacyTotalCnt(prCompare);	//개인정보 접속기록 비교기간 이용량(개인정보유형처리(이용)건수)
		modelAndView.addObject("prevPrivacyCnt", prevPrivacyCnt);
		
		int comparePrivacyCnt = privacyCnt - prevPrivacyCnt;
		modelAndView.addObject("comparePrivacyCnt", comparePrivacyCnt);
		
		List<PrivacyReport> privacyProcessCntBySystem = reportSvc.privacyProcessCntBySystem(pr1);	//1.시스템별 개인정보 접속기록 처리현황
		modelAndView.addObject("privacyProcessCntBySystem", privacyProcessCntBySystem);
		
		List<PrivacyReport> privacyUseCntBySystem = reportSvc.privacyUseCntBySystem(pr1);			//2.시스템별 개인정보 접속기록 이용현황
		modelAndView.addObject("privacyUseCntBySystem", privacyUseCntBySystem);
		
		List<PrivacyReport> comparePrevCntBySystem = reportSvc.comparePrevCntBySystem(pr); 			//3.시스템별 개인정보 접속기록 현황(비교)

		modelAndView.addObject("compUnit01", "");
		modelAndView.addObject("compUnit01_num", "");
		modelAndView.addObject("comparePrevCntBySystem", comparePrevCntBySystem);
		
		List<PrivacyReport> privacyProcessCntByDept = reportSvc.privacyProcessCntByDept(pr1);		//4.소속별 개인정보 접속기록 처리현황
		if(privacyProcessCntByDept.size()==1){
			privacyProcessCntByDept = new ArrayList<PrivacyReport>();
		}
		modelAndView.addObject("privacyProcessCntByDept", privacyProcessCntByDept);
		
		List<PrivacyReport> privacyUseCntByDept = reportSvc.privacyUseCntByDept(pr1);				//5.부서별 개인정보 접속기록 이용현황
		if(privacyUseCntByDept.size()==1){
			privacyUseCntByDept = new ArrayList<PrivacyReport>();
		}
		modelAndView.addObject("privacyUseCntByDept", privacyUseCntByDept);
		
		List<PrivacyReport> comparePrevCntByDept = reportSvc.comparePrevCntByDept(pr); 				//6.소속별 개인정보 접속기록 현황(비교)
		if(comparePrevCntByDept.size()==1){
			comparePrevCntByDept = new ArrayList<PrivacyReport>();
		}
		try {
			
			modelAndView.addObject("compUnit02", "");
			modelAndView.addObject("compUnit02_num", "");
			modelAndView.addObject("comparePrevCntByDept", comparePrevCntByDept);
		} catch (Exception e) {
		}
		
		
		List<PrivacyReport> privacyProcessCntByReqType = reportSvc.privacyProcessCntByReqType(pr1);	//9.수행업무별 접속기록 처리현황
		
		modelAndView.addObject("compUnit05", "");
		modelAndView.addObject("privacyProcessCntByReqType", privacyProcessCntByReqType);
		
		List<PrivacyReport> comparePrevCntByReqType = reportSvc.comparePrevCntByReqType(pr); 		//10.수행업무별 접속기록 처리현황(비교)
		modelAndView.addObject("compUnit06", "");
		modelAndView.addObject("compUnit06_num", "");
		modelAndView.addObject("comparePrevCntByReqType", comparePrevCntByReqType);
		
		List<PrivacyReport> comparePrevCntByUser = reportSvc.comparePrevCntByUser(pr);
		modelAndView.addObject("comparePrevCntByUser", comparePrevCntByUser);
		
		
		
		List<PrivacyReport> privacyUseCntByResultType = new ArrayList<>();
		List<PrivacyReport> comparePrevCntByResultType = new ArrayList<>();
		
		List<Code> resultTypeList = reportSvc.findResultTypeList();									
		ArrayList<PrivacyReport> listSort = new ArrayList<>(resultTypeList.size());
		ArrayList<PrivacyReport> listSort_tmp = new ArrayList<>(resultTypeList.size());
		for(int i=0; i<resultTypeList.size(); i++) {
			Code code = resultTypeList.get(i);
			pr1.setType("TYPE"+code.getCode_id());
			pr.setType("TYPE"+code.getCode_id());
			PrivacyReport returnResult = reportSvc.privacyUseCntByResultType(pr1);					//7.개인정보 유형별 접속기록 이용현황
			PrivacyReport returnResult2 = reportSvc.comparePrevCntByResultType(pr);					//7.개인정보 유형별 접속기록 이용현황(비교)
			returnResult.setCode_name(code.getCode_name());
			returnResult2.setCode_name(code.getCode_name());
			privacyUseCntByResultType.add(i, returnResult);
			comparePrevCntByResultType.add(i, returnResult2);
			
			listSort.add(i, returnResult);
			listSort_tmp.add(i, returnResult);
		}
		ListComparator comp = new ListComparator();
		Collections.sort(privacyUseCntByResultType, comp); //list sorting
		Collections.sort(comparePrevCntByResultType, comp);//list sorting
		
		
		modelAndView.addObject("compUnit03", "");
		modelAndView.addObject("privacyUseCntByResultType", privacyUseCntByResultType);

		modelAndView.addObject("compUnit04", "");
		modelAndView.addObject("compUnit04_num", "");
		modelAndView.addObject("comparePrevCntByResultType", comparePrevCntByResultType);
		
		
		/*List<PrivacyReport> privacyProcessCntByReqType = reportSvc.privacyProcessCntByReqType(pr1);	//9.수행업무별 접속기록 처리현황
		
		modelAndView.addObject("compUnit05", "");
		modelAndView.addObject("privacyProcessCntByReqType", privacyProcessCntByReqType);
		
		List<PrivacyReport> comparePrevCntByReqType = reportSvc.comparePrevCntByReqType(pr); 		//10.수행업무별 접속기록 처리현황(비교)
		modelAndView.addObject("compUnit06", "");
		modelAndView.addObject("compUnit06_num", "");
		modelAndView.addObject("comparePrevCntByReqType", comparePrevCntByReqType);*/
		
		//병합 쿼리
		List<PrivacyReport> empWordGrid01 = reportSvc.getEmpWordGrid01(pr1);
		
		modelAndView.addObject("wordUnit01", "");
		modelAndView.addObject("empWordGrid01", empWordGrid01);
		
		List<PrivacyReport> empWordGrid02 = reportSvc.getEmpWordGrid02(pr1);
		modelAndView.addObject("wordUnit02", "");
		modelAndView.addObject("empWordGrid02", empWordGrid02);
		
		
		modelAndView.setViewName("report_num01");
	    modelAndView.addObject("reportType", type);
	    modelAndView.addObject("authorize", reportSvc.reportAuthorizeInfo());
		modelAndView.addObject("download_type", download_type);
		String pdfFullPath = request.getRequestURL() +"?"+ request.getQueryString();
		modelAndView.addObject("pdfFullPath", pdfFullPath);
		modelAndView.addObject("ui_type", ui_type);
		return modelAndView;
	}
	
	@RequestMapping(value = "reportDetail_chart_new1.html")
	@ResponseBody
	public JSONArray reportDetail_chart_new1(
		@RequestParam(value = "system_seq",required=false) String system_seq,
		@RequestParam(value = "start_date", required = false) String start_date,
		@RequestParam(value = "end_date", required = false) String end_date) {

		JSONArray jArray = new JSONArray();

		String tmpList[] = system_seq.split(",");

		List<String> syslist = new ArrayList<String>();

		for (int i = 0; i < tmpList.length; ++i) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", "").substring(0, 6));
		pr.setEnd_date(end_date.replaceAll("-", "").substring(0, 6));
		pr.setSysList(syslist);
		
		List<PrivacyReport> chart_privacyUseCntBySystem = new ArrayList<>();
		chart_privacyUseCntBySystem = reportSvc.chart_privacyUseCntBySystem(pr);

		for (int i = 0; i < chart_privacyUseCntBySystem.size(); i++) {
		//for (int i = 0; i < 10; i++) {
			JSONObject jsonObj = new JSONObject();
			PrivacyReport report = chart_privacyUseCntBySystem.get(i);
			jsonObj.put("system_name", report.getSystem_name());
			jsonObj.put("logCnt", report.getLogcnt());
			jsonObj.put("privacyCnt", report.getPrivacyCnt());
			jArray.add(jsonObj);
		}

		return jArray;
	}
	
	@RequestMapping(value = "donwload_reportDetail_chart_new1.html")
	@ResponseBody
	public JSONArray donwload_reportDetail_chart_new1(
		@RequestParam(value = "system_seq",required=false) String system_seq,
		@RequestParam(value = "start_date", required = false) String start_date,
		@RequestParam(value = "end_date", required = false) String end_date) {

		JSONArray jArray = new JSONArray();

		String tmpList[] = system_seq.split(",");

		List<String> syslist = new ArrayList<String>();

		for (int i = 0; i < tmpList.length; ++i) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", "").substring(0, 6));
		pr.setEnd_date(end_date.replaceAll("-", "").substring(0, 6));
		pr.setSysList(syslist);
		
		List<PrivacyReport> chart_privacyUseCntBySystem = new ArrayList<>();
		chart_privacyUseCntBySystem = reportSvc.chart_privacyUseCntBySystem_download(pr);
		
		int listSize = chart_privacyUseCntBySystem.size();
		if(listSize >= 10)
			listSize = 10;

		for (int i = 0; i < listSize; i++) {
			JSONObject jsonObj = new JSONObject();
			PrivacyReport report = chart_privacyUseCntBySystem.get(i);
			jsonObj.put("system_name", report.getSystem_name());
			jsonObj.put("logCnt", report.getLogcnt());
			jsonObj.put("privacyCnt", report.getPrivacyCnt());
			jArray.add(jsonObj);
		}

		return jArray;
	}
	
	@RequestMapping(value = "download_reportDetail_chart_new1.html")
	@ResponseBody
	public JSONArray download_reportDetail_chart_new1(
		@RequestParam(value = "system_seq",required=false) String system_seq,
		@RequestParam(value = "start_date", required = false) String start_date,
		@RequestParam(value = "end_date", required = false) String end_date) {

		JSONArray jArray = new JSONArray();

		String tmpList[] = system_seq.split(",");

		List<String> syslist = new ArrayList<String>();

		for (int i = 0; i < tmpList.length; ++i) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", "").substring(0, 6));
		pr.setEnd_date(end_date.replaceAll("-", "").substring(0, 6));
		pr.setSysList(syslist);
		
		List<PrivacyReport> chart_privacyUseCntBySystem = new ArrayList<>();
		chart_privacyUseCntBySystem = reportSvc.chart_privacyUseCntBySystem_download(pr);

		for (int i = 0; i < chart_privacyUseCntBySystem.size(); i++) {
			JSONObject jsonObj = new JSONObject();
			PrivacyReport report = chart_privacyUseCntBySystem.get(i);
			jsonObj.put("system_name", report.getSystem_name());
			jsonObj.put("logCnt", report.getLogcnt());
			jsonObj.put("privacyCnt", report.getPrivacyCnt());
			jArray.add(jsonObj);
		}

		return jArray;
	}
	
	@RequestMapping(value = "reportDetail_chart_new2.html")
	@ResponseBody
	public JSONArray reportDetail_chart_new2(
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {

		JSONArray jArray = new JSONArray();

		String tmpList[] = system_seq.split(",");
		
		List<String> syslist = new ArrayList<String>();
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", "").substring(0, 6));
		pr.setEnd_date(end_date.replaceAll("-", "").substring(0, 6));
		pr.setSysList(syslist);

		List<PrivacyReport> reportByDept = reportSvc.chart_privacyUseCntByDept(pr);

		int size = reportByDept.size();
		
		for (int i = 0; i < size; i++) {
			JSONObject jsonObj = new JSONObject();
			PrivacyReport report = reportByDept.get(i);
			jsonObj.put("dept_name", report.getDept_name());
			jsonObj.put("logCnt", report.getLogcnt());
			jsonObj.put("privacyCnt", report.getPrivacyCnt());
			jArray.add(jsonObj);
		}

		return jArray;
	}
	
	
	
	@RequestMapping(value = "reportDetail_chart_new5.html")
	@ResponseBody
	public JSONArray reportDetail_chart_new5(
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {

		JSONArray jArray = new JSONArray();

		String tmpList[] = system_seq.split(",");
		
		List<String> syslist = new ArrayList<String>();
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", "").substring(0, 6));
		pr.setEnd_date(end_date.replaceAll("-", "").substring(0, 6));
		pr.setSysList(syslist);

		List<PrivacyReport> reportByDept = reportSvc.chart_privacyUseCntByUser(pr);

		int size = reportByDept.size();
		for (int i = 0; i < size; i++) {
			JSONObject jsonObj = new JSONObject();
			PrivacyReport report = reportByDept.get(i);

			jsonObj.put("emp_user_name", report.getEmp_user_name());
			jsonObj.put("logCnt", report.getLogcnt());
			jsonObj.put("privacyCnt", report.getPrivacyCnt());
			jArray.add(jsonObj);
		}

		return jArray;
	}
	
	
	
	
	
	
	
	@RequestMapping(value = "download_reportDetail_chart_new2.html")
	@ResponseBody
	public JSONArray download_reportDetail_chart_new2(
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {

		JSONArray jArray = new JSONArray();

		String tmpList[] = system_seq.split(",");
		
		List<String> syslist = new ArrayList<String>();
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", "").substring(0, 6));
		pr.setEnd_date(end_date.replaceAll("-", "").substring(0, 6));
		pr.setSysList(syslist);

		List<PrivacyReport> reportByDept = reportSvc.chart_privacyUseCntByDept_download(pr);

		int size = reportByDept.size();
		if(size >= 10)
			size = 10;
		
		for (int i = 0; i < size; i++) {
			JSONObject jsonObj = new JSONObject();
			PrivacyReport report = reportByDept.get(i);
			jsonObj.put("dept_name", report.getDept_name());
			jsonObj.put("logCnt", report.getLogcnt());
			jsonObj.put("privacyCnt", report.getPrivacyCnt());
			jArray.add(jsonObj);
		}

		return jArray;
	}
	
	@RequestMapping(value = "reportDetail_chart_new3.html")
	@ResponseBody
	public JSONArray reportDetail_chart_new3(
		@RequestParam(value = "system_seq",required=false) String system_seq,
		@RequestParam(value = "start_date", required = false) String start_date,
		@RequestParam(value = "end_date", required = false) String end_date) {

		JSONArray jArray = new JSONArray();

		String tmpList[] = system_seq.split(",");

		List<String> syslist = new ArrayList<String>();

		for (int i = 0; i < tmpList.length; ++i) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", "").substring(0, 6));
		pr.setEnd_date(end_date.replaceAll("-", "").substring(0, 6));
		pr.setSysList(syslist);
		
		List<PrivacyReport> chart_privacyUseCntByResultType = new ArrayList<>();
		
		List<Code> resultTypeList = reportSvc.findResultTypeList();
		
		for(int i=0; i<resultTypeList.size(); i++) {
			Code code = resultTypeList.get(i);
			pr.setType("TYPE"+code.getCode_id());
			PrivacyReport returnResult = reportSvc.chart_privacyUseCntByResultType(pr);
			returnResult.setCode_name(code.getCode_name());
			chart_privacyUseCntByResultType.add(i, returnResult);
		}
		
		ListComparator comp = new ListComparator();
		Collections.sort(chart_privacyUseCntByResultType, comp); //list sorting
		
		int dataSize = chart_privacyUseCntByResultType.size();
		if(dataSize >= 10)
			dataSize = 10;

		for (int i = 0; i < dataSize; i++) {
			JSONObject jsonObj = new JSONObject();
			PrivacyReport report = chart_privacyUseCntByResultType.get(i);
			if(report.getPrivacyCnt() != 0) {
				jsonObj.put("code_name", report.getCode_name());
				jsonObj.put("privacyCnt", report.getPrivacyCnt());
				jArray.add(jsonObj);
			}
		}

		return jArray;
	}
	
	
	
	@RequestMapping(value = "download_reportDetail_chart_new3.html")
	@ResponseBody
	public JSONArray download_reportDetail_chart_new3(
		@RequestParam(value = "system_seq",required=false) String system_seq,
		@RequestParam(value = "start_date", required = false) String start_date,
		@RequestParam(value = "end_date", required = false) String end_date) {

		JSONArray jArray = new JSONArray();

		String tmpList[] = system_seq.split(",");

		List<String> syslist = new ArrayList<String>();

		for (int i = 0; i < tmpList.length; ++i) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", "").substring(0, 6));
		pr.setEnd_date(end_date.replaceAll("-", "").substring(0, 6));
		pr.setSysList(syslist);
		
		List<PrivacyReport> chart_privacyUseCntByResultType = new ArrayList<>();
		List<Code> resultTypeList = reportSvc.findResultTypeList();
		for(int i=0; i<resultTypeList.size(); i++) {
			Code code = resultTypeList.get(i);
			pr.setType("TYPE"+code.getCode_id());
			//PrivacyReport returnResult = reportSvc.chart_privacyUseCntByResultType(pr);
			PrivacyReport returnResult = reportSvc.chart_privacyUseCntByResultType_download(pr);
			returnResult.setCode_name(code.getCode_name());
			chart_privacyUseCntByResultType.add(i, returnResult);
		}

		int dataSize = chart_privacyUseCntByResultType.size();
		if(dataSize >= 10)
			dataSize = 10;
		
		for (int i = 0; i < dataSize; i++) {
			JSONObject jsonObj = new JSONObject();
			PrivacyReport report = chart_privacyUseCntByResultType.get(i);
			if(report.getPrivacyCnt() != 0) {
				jsonObj.put("code_name", report.getCode_name());
				jsonObj.put("privacyCnt", report.getPrivacyCnt());
				jArray.add(jsonObj);
			}
		}

		return jArray;
	}
	
	@RequestMapping(value = "reportDetail_chart_new4.html")
	@ResponseBody
	public JSONArray reportDetail_chart_new4(
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date) {

		JSONArray jArray = new JSONArray();

		String tmpList[] = system_seq.split(",");
		
		List<String> syslist = new ArrayList<String>();
		
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}

		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(start_date.replaceAll("-", "").substring(0, 6));
		pr.setEnd_date(end_date.replaceAll("-", "").substring(0, 6));
		pr.setSysList(syslist);

		List<PrivacyReport> listByReqtype = reportSvc.findPrivacyReportByReqtype(pr);

		for (int i = 0; i < listByReqtype.size(); i++) {
			JSONObject jsonObj = new JSONObject();
			PrivacyReport report = listByReqtype.get(i);
			jsonObj.put("req_type", report.getReq_type());
			jsonObj.put("cnt", report.getCnt());
			jArray.add(jsonObj);
		}

		return jArray;
	}
	
	@RequestMapping(value = "uploadReportList.html")
	public DataModelAndView uploadReportList(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		UploadReport uploadReport = CommonHelper.convertMapToBean(parameters, UploadReport.class);
		return reportSvc.uploadReportList(uploadReport,request);
	}
	
	@RequestMapping(value = "uploadReportRemove.html")
	@ResponseBody
	public String uploadReportRemove(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		UploadReport uploadReport = CommonHelper.convertMapToBean(parameters, UploadReport.class);
		return reportSvc.uploadReportRemove(uploadReport,request);
	}
	
	
	@RequestMapping(value = "uploadReportOne.html")
	@ResponseBody
	public String uploadReportOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		return reportSvc.uploadReportOne(parameters);
	}
	
	@RequestMapping(value = "addReportOption.html")
	@ResponseBody
	public String addReportOption(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		String update_id = ((AdminUser)request.getSession().getAttribute("userSession")).getAdmin_user_id();
		parameters.put("update_id", update_id);
		return reportSvc.addReportOption(parameters);
	}
	
	@RequestMapping(value = "saveReportOption.html")
	@ResponseBody
	public String saveReportOption(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		String update_id = ((AdminUser)request.getSession().getAttribute("userSession")).getAdmin_user_id();
		parameters.put("update_id", update_id);
		return reportSvc.saveReportOption(parameters);
	}
	
	@RequestMapping(value = "findReportOption.html")
	@ResponseBody
	public List<Map<String,String>> findReportOption(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		String update_id = ((AdminUser)request.getSession().getAttribute("userSession")).getAdmin_user_id();
		parameters.put("update_id", update_id);
		return reportSvc.findReportOption(parameters);
	}
	
	@RequestMapping(value="checkBeforeReport.html",method={RequestMethod.POST})
	@ResponseBody
	public Integer checkBeforeReport(@RequestParam Map<String, String> parameters) throws Exception{
		return reportSvc.checkBeforeReport(parameters);
	}
	
	@MngtActHist(log_action = "EXCEL DOWNLOAD", log_message = "[정기점검보고서] 엑셀 다운로드", l_category = "통계보고 > 정기점검보고서")
	@RequestMapping(value="makeExcelReportAll.html", method={RequestMethod.POST})
	public DataModelAndView makeExcelReportAll(@RequestParam Map<String, String> param, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		reportSvc.makeExcelReportAll(modelAndView, param, request);
		return modelAndView;
	}
	
	@MngtActHist(log_action = "EXCEL DOWNLOAD", log_message = "[정기점검보고서] 엑셀 다운로드", l_category = "통계보고 > 정기점검보고서")
	@RequestMapping(value="makeExcelReportDown.html", method={RequestMethod.POST})
	public DataModelAndView makeExcelReportDown(@RequestParam Map<String, String> param, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		reportSvc.makeExcelReportDown(modelAndView, param, request);
		return modelAndView;
	}
	
	/*
	 * 정기점검 보고서 데이터 저장
	 */
	@ResponseBody
	@RequestMapping(value="inspectionDataSave.html", method={RequestMethod.POST})
	public String inspectionDataSave(@RequestParam Map<String, String> param, HttpServletRequest request) {
		return reportSvc.inspectionDataSave(param,request);
	}
	
	@ResponseBody
	@RequestMapping(value="inspectionDataLoad.html", method={RequestMethod.POST})
	public String inspectionDataLoad(@RequestParam Map<String, String> param, HttpServletRequest request) {
		return reportSvc.inspectionDataLoad(param);
	}
	
	@ResponseBody
	@RequestMapping(value="inspectionDataDelete.html", method={RequestMethod.POST})
	public String inspectionDataDelete(@RequestParam Map<String, String> param, HttpServletRequest request) {
		return reportSvc.inspectionDataDelete(param,request);
	}
	
	//통합보고서
	/**
	 * 20200701
	 * 한화토탈 통합 보고서 url 생성
	 * 
	 */
	@RequestMapping(value="reportTotalIntegration.html")
	public ModelAndView findTotalIntegrationReport(@RequestParam Map<String, String> param, HttpServletRequest request,
			@ModelAttribute("search") SearchSearch search, 
			@RequestParam(value = "type", required = false) int type,
			@RequestParam(value = "period_type", required = false) int period_type,
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date,
			@RequestParam(value = "menu_id", required = false) String menu_id,
			@RequestParam(value = "half_type", required = false) String half_type,
			@RequestParam(value = "quarter_type", required = false) String quarter_type,
			@RequestParam(value = "download_type", required = false) String download_type) {
		
		ModelAndView modelAndView = new ModelAndView();
		
		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
	    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
		String date = dayTime.format(new Date(time));
		String ui_type = commonDao.getUiType();
		modelAndView.addObject("ui_type", ui_type);
		
		// 마지막날짜가 현재보다 클 경우 어제날짜까지로 지정해준다.
		Calendar cal = Calendar.getInstance();
		String compareDate = "";
		
		HttpSession session = request.getSession();
		String master = (String)session.getAttribute("master");
	    String use_reportLogo = (String)session.getAttribute("use_reportLogo");
	    String use_reportLine = (String)session.getAttribute("use_reportLine");
	    if (use_reportLogo != null && use_reportLogo.equals("Y")) {
		    String rootPath = request.getSession().getServletContext().getRealPath("/");
			String attach_path = "resources/upload/";
			String filename = commonDao.selectCurrentImage_logo2();
			String savePath = rootPath + attach_path + filename;
			File file = new File(savePath);
			if(file.exists()) {
				modelAndView.addObject("filename", filename);
			}
	    }
		
	  //수준진단보고서 > report_option
	    Map<String, String> optionMap = new HashMap<String, String>();
		optionMap.put("code_id", "14");
		String proc_month = start_date.replace("-", "").substring(0, 6);
		
		//20200702 한화 토탈 보고서 조건 제거
		//optionMap.put("proc_month", proc_month); //
		//optionMap.put("period_type", String.valueOf(period_type));
		List<Map<String, String>> reportOption = reportDao.findReportOption(optionMap);
		
		//총평 데이터 반환
		String report_desc_str = reportDao.getReportDescStr(optionMap);
		
		modelAndView.addObject("report_desc_str", report_desc_str);
		modelAndView.addObject("reportOption", reportOption);
	    
	    modelAndView.addObject("use_reportLogo", use_reportLogo);
	    modelAndView.addObject("use_reportLine", use_reportLine);
		modelAndView.addObject("masterflag", master);
		modelAndView.addObject("report_type", type);
		modelAndView.addObject("period_type", period_type);
		modelAndView.addObject("start_date", start_date);
		modelAndView.addObject("end_date", end_date);
		modelAndView.addObject("date", date);
		modelAndView.addObject("system_seq", system_seq);
		modelAndView.addObject("logo_report_url", logo_report_url);
		modelAndView.addObject("menu_id", menu_id);
		modelAndView.addObject("half_type", half_type);
		modelAndView.addObject("quarter_type", quarter_type);
		
		
		//선택 시스템 목록
		List<String> syslist = new ArrayList<String>();
		String tmpList[] = system_seq.split(",");
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}
		syslist.add("");
		
		String sDate[] = start_date.split("-");
		String eDate[] = end_date.split("-");

		int strYear = Integer.parseInt(sDate[0]);
		int strMonth = Integer.parseInt(sDate[1]);
		int endYear = Integer.parseInt(eDate[0]);
		int endMonth = Integer.parseInt(eDate[1]);
		int diffMon = (endYear - strYear)* 12 + (endMonth - strMonth);
		String stDate = start_date.replaceAll("-", "").substring(0,6);
		String edDate = end_date.replaceAll("-", "").substring(0,6);
		
		PrivacyReport pr = new PrivacyReport();			//선택 기간
		PrivacyReport prCompare = new PrivacyReport();	//비교 기간 (동년전월, 동년 이전분기)
		PrivacyReport pr1 = new PrivacyReport();		//
	    
		
		// 접속유형 default 세팅
		String mapping_id = (String) session.getAttribute("mapping_id");
		if(mapping_id.equals("Y")) {
			if ( search.getMapping_id() == null ) 
				search.setMapping_id("code1");
			if ( search.getMapping_id().equals("code2") ) 
				search.setMapping_id(null);
		}else {
			if(search.getMapping_id() == null || search.getMapping_id().equals("code2"))
				search.setMapping_id(null);
			else
				search.setMapping_id("code1");
		}
		
		
		String compare_type = "1";
		modelAndView.addObject("compare_type", compare_type);
		if (period_type == 1) {
			pr.setStart_date(stDate);
			pr.setEnd_date(edDate);
			pr.setSysList(syslist);
			
			if(compare_type == "1") {			//동년 전월 (compare_type =1)
				cal.set(strYear, strMonth-1, 1);
			    cal.add(Calendar.MONTH, -1);    	
			    compareDate = dateFormatter.format(cal.getTime());
				pr.setCompare_start_date(compareDate.substring(0,6));		
				pr.setCompare_end_date(compareDate.substring(0,6));			
				
				prCompare.setStart_date(compareDate.substring(0,6));
				prCompare.setEnd_date(compareDate.substring(0,6));
				prCompare.setSysList(syslist);
			}else if(compare_type == "2") {		//전년동월 (compare_type =2)
				pr.setCompare_start_date(strYear-1+stDate.substring(4, 6));
				pr.setCompare_end_date(strYear-1+edDate.substring(4, 6));
				
				//비교 기간1(전년동월)
				prCompare.setStart_date(strYear-1+stDate.substring(4, 6));
				prCompare.setEnd_date(strYear-1+edDate.substring(4, 6));
				prCompare.setSysList(syslist);
			}
			
			
			pr1.setStart_date(strYear+"01");		
			pr1.setEnd_date(edDate);				
		    pr1.setSysList(syslist);				
			pr1.setMonth(strMonth+"");
			pr1.setPeriod_type("1");
			
		}else if(period_type == 2) {	//분기
			pr1.setStart_date(stDate);
			pr1.setEnd_date(edDate);
			pr1.setSysList(syslist);
			pr1.setPeriod_type("2");
			pr1.setQuarter_type(quarter_type);
			
			pr.setStart_date(stDate);
			pr.setEnd_date(edDate);
			pr.setSysList(syslist);
			if(compare_type == "1") {			//동년 전분기 (compare_type =1)
				cal.set(strYear, strMonth-1, 1);		
			    cal.add(Calendar.MONTH, -3);    		
			    compareDate = dateFormatter.format(cal.getTime());
				pr.setCompare_start_date(compareDate.substring(0,6));
				prCompare.setStart_date(compareDate.substring(0,6));
				
				cal.set(strYear, strMonth-1, 1);		
			    cal.add(Calendar.MONTH, -1);    		
			    compareDate = dateFormatter.format(cal.getTime());
				pr.setCompare_end_date(compareDate.substring(0,6));
				prCompare.setEnd_date(compareDate.substring(0,6));
				prCompare.setSysList(syslist);
			}else if(compare_type == "2") {		//전년 동분기 (compare_type =2)
				pr.setCompare_start_date(strYear-1+stDate.substring(4, 6));
				pr.setCompare_end_date(strYear-1+edDate.substring(4, 6));
				//전년 동일분기
				prCompare.setStart_date(strYear-1+stDate.substring(4, 6));
				prCompare.setEnd_date(strYear-1+edDate.substring(4, 6));
				prCompare.setSysList(syslist);
			}
			
		}
		else if(period_type == 4) {	//년
			pr.setStart_date(stDate);
			pr.setEnd_date(edDate);
			pr.setSysList(syslist);
			pr.setCompare_start_date(strYear-1+stDate.substring(4, 6));
			pr.setCompare_end_date(strYear-1+edDate.substring(4, 6));
			
			pr1.setStart_date(stDate);
			pr1.setEnd_date(edDate);
			pr1.setSysList(syslist);
			pr1.setPeriod_type("4");
			
			//전년
			prCompare.setStart_date(strYear-1+stDate.substring(4, 6));
			prCompare.setEnd_date(strYear-1+edDate.substring(4, 6));
			prCompare.setSysList(syslist);
			
		}
	    
		List<SystemMaster> systems = reportSvc.findSystemMasterDetailList(pr);
		modelAndView.addObject("systems", systems);
		
		int diffMonth = getDiffMonth(pr);
		modelAndView.addObject("diffMonth", diffMonth);
		
		int logCnt = reportSvc.findPrivacyTotalLogCnt(pr);				//개인정보 접속기록 총 처리량(로그건수)
		modelAndView.addObject("logCnt", logCnt);
		int prevLogCnt = reportSvc.findPrivacyTotalLogCnt(prCompare);	//개인정보 접속기록 비교기간 총 처리량(로그건수)
		modelAndView.addObject("prevLogCnt", prevLogCnt);
		
		int compareLogcnt = logCnt - prevLogCnt;
		modelAndView.addObject("compareLogcnt", compareLogcnt);
		
		int privacyCnt = reportSvc.findPrivacyTotalCnt(pr);				//개인정보 접속기록 이용량(개인정보유형처리(이용)건수)
		modelAndView.addObject("privacyCnt", privacyCnt);
		
		int prevPrivacyCnt = reportSvc.findPrivacyTotalCnt(prCompare);	//개인정보 접속기록 비교기간 이용량(개인정보유형처리(이용)건수)
		modelAndView.addObject("prevPrivacyCnt", prevPrivacyCnt);
		
		int comparePrivacyCnt = privacyCnt - prevPrivacyCnt;
		modelAndView.addObject("comparePrivacyCnt", comparePrivacyCnt);
		
		List<PrivacyReport> privacyProcessCntBySystem = reportSvc.privacyProcessCntBySystem(pr1);	//1.시스템별 개인정보 접속기록 처리현황
		modelAndView.addObject("privacyProcessCntBySystem", privacyProcessCntBySystem);
		
		List<PrivacyReport> privacyUseCntBySystem = reportSvc.privacyUseCntBySystem(pr1);			//2.시스템별 개인정보 접속기록 이용현황
		modelAndView.addObject("privacyUseCntBySystem", privacyUseCntBySystem);
		
		List<PrivacyReport> comparePrevCntBySystem = reportSvc.comparePrevCntBySystem(pr); 			//3.시스템별 개인정보 접속기록 현황(비교)

		modelAndView.addObject("compUnit01", "");
		modelAndView.addObject("compUnit01_num", "");
		modelAndView.addObject("comparePrevCntBySystem", comparePrevCntBySystem);
		
		List<PrivacyReport> privacyProcessCntByDept = reportSvc.privacyProcessCntByDept(pr1);		//4.소속별 개인정보 접속기록 처리현황
		if(privacyProcessCntByDept.size()==1){
			privacyProcessCntByDept = new ArrayList<PrivacyReport>();
		}
		modelAndView.addObject("privacyProcessCntByDept", privacyProcessCntByDept);
		
		List<PrivacyReport> privacyUseCntByDept = reportSvc.privacyUseCntByDept(pr1);				//5.부서별 개인정보 접속기록 이용현황
		if(privacyUseCntByDept.size()==1){
			privacyUseCntByDept = new ArrayList<PrivacyReport>();
		}
		modelAndView.addObject("privacyUseCntByDept", privacyUseCntByDept);
		
		List<PrivacyReport> comparePrevCntByDept = reportSvc.comparePrevCntByDept(pr); 				//6.소속별 개인정보 접속기록 현황(비교)
		if(comparePrevCntByDept.size()==1){
			comparePrevCntByDept = new ArrayList<PrivacyReport>();
		}
		try {
			
			modelAndView.addObject("compUnit02", "");
			modelAndView.addObject("compUnit02_num", "");
			modelAndView.addObject("comparePrevCntByDept", comparePrevCntByDept);
		} catch (Exception e) {
		}
		
		
		List<PrivacyReport> privacyUseCntByResultType = new ArrayList<>();
		List<PrivacyReport> comparePrevCntByResultType = new ArrayList<>();
		
		List<Code> resultTypeList = reportSvc.findResultTypeList();									
		ArrayList<PrivacyReport> listSort = new ArrayList<>(resultTypeList.size());
		ArrayList<PrivacyReport> listSort_tmp = new ArrayList<>(resultTypeList.size());
		for(int i=0; i<resultTypeList.size(); i++) {
			Code code = resultTypeList.get(i);
			pr1.setType("TYPE"+code.getCode_id());
			pr.setType("TYPE"+code.getCode_id());
			PrivacyReport returnResult = reportSvc.privacyUseCntByResultType(pr1);					//7.개인정보 유형별 접속기록 이용현황
			PrivacyReport returnResult2 = reportSvc.comparePrevCntByResultType(pr);					//7.개인정보 유형별 접속기록 이용현황(비교)
			returnResult.setCode_name(code.getCode_name());
			returnResult2.setCode_name(code.getCode_name());
			
			returnResult.setPeriod_type(Integer.toString(period_type));
			returnResult2.setPeriod_type(Integer.toString(period_type));
			privacyUseCntByResultType.add(i, returnResult);
			comparePrevCntByResultType.add(i, returnResult2);
			
			listSort.add(i, returnResult);
			listSort_tmp.add(i, returnResult);
		}
		ListComparator comp = new ListComparator();
		Collections.sort(privacyUseCntByResultType, comp); //list sorting
		Collections.sort(comparePrevCntByResultType, comp);//list sorting
		
		
		modelAndView.addObject("compUnit03", "");
		modelAndView.addObject("privacyUseCntByResultType", privacyUseCntByResultType);

		modelAndView.addObject("compUnit04", "");
		modelAndView.addObject("compUnit04_num", "");
		modelAndView.addObject("comparePrevCntByResultType", comparePrevCntByResultType);
		
		
		List<PrivacyReport> privacyProcessCntByReqType = reportSvc.privacyProcessCntByReqType(pr1);	//9.수행업무별 접속기록 처리현황
		
		modelAndView.addObject("compUnit05", "");
		modelAndView.addObject("privacyProcessCntByReqType", privacyProcessCntByReqType);
		
		List<PrivacyReport> comparePrevCntByReqType = reportSvc.comparePrevCntByReqType(pr); 		//10.수행업무별 접속기록 처리현황(비교)
		modelAndView.addObject("compUnit06", "");
		modelAndView.addObject("compUnit06_num", "");
		modelAndView.addObject("comparePrevCntByReqType", comparePrevCntByReqType);
		
		//병합 쿼리
		List<PrivacyReport> empWordGrid01 = reportSvc.getEmpWordGrid01(pr1);
		
		modelAndView.addObject("wordUnit01", "");
		modelAndView.addObject("empWordGrid01", empWordGrid01);
		
		List<PrivacyReport> empWordGrid02 = reportSvc.getEmpWordGrid02(pr1);
		modelAndView.addObject("wordUnit02", "");
		modelAndView.addObject("empWordGrid02", empWordGrid02);
		
		
		//==============================================================================================
		//==============================================================================================
		
		
		// CPO 오남용 데이터
		String startDate = String.valueOf(param.get("start_date"));
		String endDate = String.valueOf(param.get("end_date"));
		int dateType = Integer.parseInt(param.get("period_type"));
		DateUtil dateUtil = new DateUtil();
		date = dateUtil.getDateNow();
		endDate = dateUtil.getReportEndDate(endDate);
		
		//pr -> start_date, end_date : yyyyMMdd로 조회하게 끔 처리(선택 기간)
		pr = dateUtil.getInputDatePriReportVO(startDate, endDate);
		pr.setSysList(syslist);
		//prCompare -> start_date : 이전 달 1일, end_date : 이전 달 31일로 세팅(동년전월)
		prCompare = dateUtil.getBefMonthPriReportVO(startDate, endDate);
		prCompare.setSysList(syslist);
		//prCompare2 -> start_date : 이전 년도 1일, end_date : 이전 년도 31일로 세팅(전년동월)
		PrivacyReport prCompare2 = dateUtil.getBefYearPriReportVO(startDate, endDate);
		prCompare2.setSysList(syslist);
		
		
		
		// 비정상 행위한 Top10 직원의 id, name, deptName, count정보를 가져옴
		List<PrivacyReport> listEmpDetail = reportSvc.findEmpDetailList(pr);
		String checkEmpNameMasking = commonDao.checkEmpNameMasking();
		if(checkEmpNameMasking.equals("Y")) {
			for (int i = 0; i < listEmpDetail.size(); i++) {
				if(!"".equals(listEmpDetail.get(i).getEmp_user_name())) {
					String empUserName = listEmpDetail.get(i).getEmp_user_name();
					StringBuilder builder = new StringBuilder(empUserName);
					builder.setCharAt(empUserName.length() - 2, '*');
					listEmpDetail.get(i).setEmp_user_name(builder.toString());
				}
			}
		}
		
		
		List empRuleLst = null;
		//rule_nm, rule_cd, cnt : 비정상행위 룰명, scen_seq(시나리오명), 비정상행위 갯수를 가져옴
		if(dateType == 4 || dateType == 5) {
			empRuleLst = getReportDetail2_chart1(startDate, endDate, syslist);
		} else {
			empRuleLst = getReportDetail2_chart1(startDate, endDate, syslist, dateType);
		}
		
		
		List list1 = getReportDetail2_chart1(start_date, end_date, syslist);
		modelAndView.addObject("emp_detailList", list1);
		
		List<Report> rule_list = new ArrayList<Report>();
		Report tmp1 = new Report();
		tmp1.setRule_nm("고유식별정보 과다사용");
		rule_list.add(tmp1);
		
		Report tmp2 = new Report();
		tmp2.setRule_nm("비정상 접근");
		rule_list.add(tmp2);
		
		Report tmp3 = new Report();
		tmp3.setRule_nm("과다처리");
		rule_list.add(tmp3);
		
		Report tmp4 = new Report();
		tmp4.setRule_nm("특정인 처리");
		rule_list.add(tmp4);
		
		Report tmp5 = new Report();
		tmp5.setRule_nm("특정시간대 처리");
		rule_list.add(tmp5);
		
		modelAndView.addObject("rule_list", rule_list);
		List<Map<String, Object>> map = getReportDetail2_chart3(pr, rule_list);
		modelAndView.addObject("emp_detailList3", map);
		
		int empcnt = reportDao.findEmpDetailCount(pr);
		modelAndView.addObject("empcnt", empcnt);
		
		
		modelAndView.setViewName("report_totalIntegration");
		modelAndView.addObject("listEmpDetail", listEmpDetail);
		modelAndView.addObject("emp_detailList", empRuleLst);
		modelAndView.addObject("reportType", type);
	    modelAndView.addObject("authorize", reportSvc.reportAuthorizeInfo());
		modelAndView.addObject("download_type", download_type);
		String pdfFullPath = request.getRequestURL() +"?"+ request.getQueryString();
		modelAndView.addObject("pdfFullPath", pdfFullPath);
		modelAndView.addObject("ui_type", ui_type);
		return modelAndView;
	}
	
	@RequestMapping(value="system_empReport.html")
	public ModelAndView findReportEmpLevel_new(HttpServletRequest request,
			@ModelAttribute("search") SearchSearch search, 
			@RequestParam(value = "type", required = false) int type,
			@RequestParam(value = "period_type", required = false) int period_type,
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date,
			@RequestParam(value = "menu_id", required = false) String menu_id,
			@RequestParam(value = "half_type", required = false) String half_type,
			@RequestParam(value = "quarter_type", required = false) String quarter_type,
			@RequestParam(value = "download_type", required = false) String download_type,
			@RequestParam(value = "system_report", required = false) String system_report) {
		
		ModelAndView modelAndView = new ModelAndView();
		System.out.println(">>  ======================  수준진단보고서 ");
		System.out.println(">> ======================= download_type : " + download_type);
		System.out.println("==========================  system report : " + system_report);
		System.out.println("==========================  system seq : " + system_seq);
		
		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
	    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
		String date = dayTime.format(new Date(time));
		String ui_type = commonDao.getUiType();
		modelAndView.addObject("ui_type", ui_type);
		
		// 마지막날짜가 현재보다 클 경우 어제날짜까지로 지정해준다.
		Calendar cal = Calendar.getInstance();
		String compareDate = "";
				
		HttpSession session = request.getSession();
		String master = (String)session.getAttribute("master");
	    String use_reportLogo = (String)session.getAttribute("use_reportLogo");
	    String use_reportLine = (String)session.getAttribute("use_reportLine");
	    if (use_reportLogo != null && use_reportLogo.equals("Y")) {
		    String rootPath = request.getSession().getServletContext().getRealPath("/");
			String attach_path = "resources/upload/";
			String filename = commonDao.selectCurrentImage_logo2();
			String savePath = rootPath + attach_path + filename;
			File file = new File(savePath);
			if(file.exists()) {
				modelAndView.addObject("filename", filename);
			}
	    }
	    
	    
	    //수준진단보고서 > report_option
	    Map<String, String> optionMap = new HashMap<String, String>();
		optionMap.put("code_id", "13");
		String proc_month = start_date.replace("-", "").substring(0, 6);
		optionMap.put("proc_month", proc_month);
		optionMap.put("period_type", String.valueOf(period_type));
		List<Map<String, String>> reportOption = reportDao.findReportOption(optionMap);
		
		//총평 데이터 반환
		String report_desc_str = reportDao.getReportDescStr(optionMap);
		
		modelAndView.addObject("report_desc_str", report_desc_str);
		modelAndView.addObject("reportOption", reportOption);
	    modelAndView.addObject("use_reportLogo", use_reportLogo);
	    modelAndView.addObject("use_reportLine", use_reportLine);
		modelAndView.addObject("masterflag", master);
		modelAndView.addObject("report_type", type);
		modelAndView.addObject("period_type", period_type);
		modelAndView.addObject("start_date", start_date);
		modelAndView.addObject("end_date", end_date);
		modelAndView.addObject("date", date);
		modelAndView.addObject("system_seq", system_seq);
		modelAndView.addObject("logo_report_url", logo_report_url);
		modelAndView.addObject("menu_id", menu_id);
		modelAndView.addObject("half_type", half_type);
		modelAndView.addObject("quarter_type", quarter_type);

		List<String> syslist = new ArrayList<String>();
		String tmpList[] = system_seq.split(",");
		if("Y".equals(system_report)) {
			syslist.add(system_seq);
			modelAndView.addObject("authorize", reportSvc.reportAuthorizeInfo_system(system_seq));
		}else {
 			for ( int i=0; i< tmpList.length; ++i ) {
				syslist.add(tmpList[i]);
			}
 			modelAndView.addObject("authorize", reportSvc.reportAuthorizeInfo());
		}
		syslist.add("");
		
		String sDate[] = start_date.split("-");
		String eDate[] = end_date.split("-");

		int strYear = Integer.parseInt(sDate[0]);
		int strMonth = Integer.parseInt(sDate[1]);
		int endYear = Integer.parseInt(eDate[0]);
		int endMonth = Integer.parseInt(eDate[1]);
		int diffMon = (endYear - strYear)* 12 + (endMonth - strMonth);
		String stDate = start_date.replaceAll("-", "").substring(0,6);
		String edDate = end_date.replaceAll("-", "").substring(0,6);
		
		PrivacyReport pr = new PrivacyReport();			//선택 기간
		PrivacyReport prCompare = new PrivacyReport();	//비교 기간 (동년전월, 동년 이전분기)
		PrivacyReport pr1 = new PrivacyReport();		//
	    
		
		// 접속유형 default 세팅
		String mapping_id = (String) session.getAttribute("mapping_id");
		if(mapping_id.equals("Y")) {
			if ( search.getMapping_id() == null ) 
				search.setMapping_id("code1");
			if ( search.getMapping_id().equals("code2") ) 
				search.setMapping_id(null);
		}else {
			if(search.getMapping_id() == null || search.getMapping_id().equals("code2"))
				search.setMapping_id(null);
			else
				search.setMapping_id("code1");
		}
		
		
		String compare_type = "1";
		modelAndView.addObject("compare_type", compare_type);
		if (period_type == 1) {
			pr.setStart_date(stDate);
			pr.setEnd_date(edDate);
			pr.setSysList(syslist);
			
			if(compare_type == "1") {			//동년 전월 (compare_type =1)
				cal.set(strYear, strMonth-1, 1);
			    cal.add(Calendar.MONTH, -1);    	
			    compareDate = dateFormatter.format(cal.getTime());
				pr.setCompare_start_date(compareDate.substring(0,6));		
				pr.setCompare_end_date(compareDate.substring(0,6));			
				
				prCompare.setStart_date(compareDate.substring(0,6));
				prCompare.setEnd_date(compareDate.substring(0,6));
				prCompare.setSysList(syslist);
			}else if(compare_type == "2") {		//전년동월 (compare_type =2)
				pr.setCompare_start_date(strYear-1+stDate.substring(4, 6));
				pr.setCompare_end_date(strYear-1+edDate.substring(4, 6));
				
				//비교 기간1(전년동월)
				prCompare.setStart_date(strYear-1+stDate.substring(4, 6));
				prCompare.setEnd_date(strYear-1+edDate.substring(4, 6));
				prCompare.setSysList(syslist);
			}
			
			
			pr1.setStart_date(strYear+"01");		
			pr1.setEnd_date(edDate);				
		    pr1.setSysList(syslist);				
			pr1.setMonth(strMonth+"");
			pr1.setPeriod_type("1");
			
		}else if(period_type == 2) {	//분기
			pr1.setStart_date(stDate);
			pr1.setEnd_date(edDate);
			pr1.setSysList(syslist);
			pr1.setPeriod_type("2");
			pr1.setQuarter_type(quarter_type);
			
			pr.setStart_date(stDate);
			pr.setEnd_date(edDate);
			pr.setSysList(syslist);
			if(compare_type == "1") {			//동년 전분기 (compare_type =1)
				cal.set(strYear, strMonth-1, 1);		
			    cal.add(Calendar.MONTH, -3);    		
			    compareDate = dateFormatter.format(cal.getTime());
				pr.setCompare_start_date(compareDate.substring(0,6));
				prCompare.setStart_date(compareDate.substring(0,6));
				
				cal.set(strYear, strMonth-1, 1);		
			    cal.add(Calendar.MONTH, -1);    		
			    compareDate = dateFormatter.format(cal.getTime());
				pr.setCompare_end_date(compareDate.substring(0,6));
				prCompare.setEnd_date(compareDate.substring(0,6));
				prCompare.setSysList(syslist);
			}else if(compare_type == "2") {		//전년 동분기 (compare_type =2)
				pr.setCompare_start_date(strYear-1+stDate.substring(4, 6));
				pr.setCompare_end_date(strYear-1+edDate.substring(4, 6));
				//전년 동일분기
				prCompare.setStart_date(strYear-1+stDate.substring(4, 6));
				prCompare.setEnd_date(strYear-1+edDate.substring(4, 6));
				prCompare.setSysList(syslist);
			}
			
		}
		else if(period_type == 4) {	//년
			pr.setStart_date(stDate);
			pr.setEnd_date(edDate);
			pr.setSysList(syslist);
			pr.setCompare_start_date(strYear-1+stDate.substring(4, 6));
			pr.setCompare_end_date(strYear-1+edDate.substring(4, 6));
			
			pr1.setStart_date(stDate);
			pr1.setEnd_date(edDate);
			pr1.setSysList(syslist);
			pr1.setPeriod_type("4");
			
			//전년
			prCompare.setStart_date(strYear-1+stDate.substring(4, 6));
			prCompare.setEnd_date(strYear-1+edDate.substring(4, 6));
			prCompare.setSysList(syslist);
			
		}
	    
		List<SystemMaster> systems = reportSvc.findSystemMasterDetailList(pr);
		modelAndView.addObject("systems", systems);
		
		int diffMonth = getDiffMonth(pr);
		modelAndView.addObject("diffMonth", diffMonth);
		
		int logCnt = reportSvc.findPrivacyTotalLogCnt(pr);				//개인정보 접속기록 총 처리량(로그건수)
		modelAndView.addObject("logCnt", logCnt);
		int prevLogCnt = reportSvc.findPrivacyTotalLogCnt(prCompare);	//개인정보 접속기록 비교기간 총 처리량(로그건수)
		modelAndView.addObject("prevLogCnt", prevLogCnt);
		
		int compareLogcnt = logCnt - prevLogCnt;
		modelAndView.addObject("compareLogcnt", compareLogcnt);
		
		int privacyCnt = reportSvc.findPrivacyTotalCnt(pr);				//개인정보 접속기록 이용량(개인정보유형처리(이용)건수)
		modelAndView.addObject("privacyCnt", privacyCnt);
		
		int prevPrivacyCnt = reportSvc.findPrivacyTotalCnt(prCompare);	//개인정보 접속기록 비교기간 이용량(개인정보유형처리(이용)건수)
		modelAndView.addObject("prevPrivacyCnt", prevPrivacyCnt);
		
		int comparePrivacyCnt = privacyCnt - prevPrivacyCnt;
		modelAndView.addObject("comparePrivacyCnt", comparePrivacyCnt);
		
		List<PrivacyReport> privacyProcessCntBySystem = reportSvc.privacyProcessCntBySystem(pr1);	//1.시스템별 개인정보 접속기록 처리현황
		modelAndView.addObject("privacyProcessCntBySystem", privacyProcessCntBySystem);
		
		List<PrivacyReport> privacyUseCntBySystem = reportSvc.privacyUseCntBySystem(pr1);			//2.시스템별 개인정보 접속기록 이용현황
		modelAndView.addObject("privacyUseCntBySystem", privacyUseCntBySystem);
		
		List<PrivacyReport> comparePrevCntBySystem = reportSvc.comparePrevCntBySystem(pr); 			//3.시스템별 개인정보 접속기록 현황(비교)

		modelAndView.addObject("compUnit01", "");
		modelAndView.addObject("compUnit01_num", "");
		modelAndView.addObject("comparePrevCntBySystem", comparePrevCntBySystem);
		
		List<PrivacyReport> privacyProcessCntByDept = reportSvc.privacyProcessCntByDept(pr1);		//4.소속별 개인정보 접속기록 처리현황
		if(privacyProcessCntByDept.size()==1){
			privacyProcessCntByDept = new ArrayList<PrivacyReport>();
		}
		modelAndView.addObject("privacyProcessCntByDept", privacyProcessCntByDept);
		
		List<PrivacyReport> privacyUseCntByDept = reportSvc.privacyUseCntByDept(pr1);				//5.부서별 개인정보 접속기록 이용현황
		if(privacyUseCntByDept.size()==1){
			privacyUseCntByDept = new ArrayList<PrivacyReport>();
		}
		modelAndView.addObject("privacyUseCntByDept", privacyUseCntByDept);
		
		List<PrivacyReport> comparePrevCntByDept = reportSvc.comparePrevCntByDept(pr); 				//6.소속별 개인정보 접속기록 현황(비교)
		if(comparePrevCntByDept.size()==1){
			comparePrevCntByDept = new ArrayList<PrivacyReport>();
		}
		try {
			
			modelAndView.addObject("compUnit02", "");
			modelAndView.addObject("compUnit02_num", "");
			modelAndView.addObject("comparePrevCntByDept", comparePrevCntByDept);
		} catch (Exception e) {
		}
		
		
		List<PrivacyReport> privacyProcessCntByReqType = reportSvc.privacyProcessCntByReqType(pr1);	//9.수행업무별 접속기록 처리현황
		
		modelAndView.addObject("compUnit05", "");
		modelAndView.addObject("privacyProcessCntByReqType", privacyProcessCntByReqType);
		
		List<PrivacyReport> comparePrevCntByReqType = reportSvc.comparePrevCntByReqType(pr); 		//10.수행업무별 접속기록 처리현황(비교)
		modelAndView.addObject("compUnit06", "");
		modelAndView.addObject("compUnit06_num", "");
		modelAndView.addObject("comparePrevCntByReqType", comparePrevCntByReqType);
		
		List<PrivacyReport> comparePrevCntByUser = reportSvc.comparePrevCntByUser(pr);
		modelAndView.addObject("comparePrevCntByUser", comparePrevCntByUser);
		
		
		
		List<PrivacyReport> privacyUseCntByResultType = new ArrayList<>();
		List<PrivacyReport> comparePrevCntByResultType = new ArrayList<>();
		
		List<Code> resultTypeList = reportSvc.findResultTypeList();									
		ArrayList<PrivacyReport> listSort = new ArrayList<>(resultTypeList.size());
		ArrayList<PrivacyReport> listSort_tmp = new ArrayList<>(resultTypeList.size());
		for(int i=0; i<resultTypeList.size(); i++) {
			Code code = resultTypeList.get(i);
			pr1.setType("TYPE"+code.getCode_id());
			pr.setType("TYPE"+code.getCode_id());
			PrivacyReport returnResult = reportSvc.privacyUseCntByResultType(pr1);					//7.개인정보 유형별 접속기록 이용현황
			PrivacyReport returnResult2 = reportSvc.comparePrevCntByResultType(pr);					//7.개인정보 유형별 접속기록 이용현황(비교)
			returnResult.setCode_name(code.getCode_name());
			returnResult2.setCode_name(code.getCode_name());
			privacyUseCntByResultType.add(i, returnResult);
			comparePrevCntByResultType.add(i, returnResult2);
			
			listSort.add(i, returnResult);
			listSort_tmp.add(i, returnResult);
		}
		ListComparator comp = new ListComparator();
		Collections.sort(privacyUseCntByResultType, comp); //list sorting
		Collections.sort(comparePrevCntByResultType, comp);//list sorting
		
		
		modelAndView.addObject("compUnit03", "");
		modelAndView.addObject("privacyUseCntByResultType", privacyUseCntByResultType);

		modelAndView.addObject("compUnit04", "");
		modelAndView.addObject("compUnit04_num", "");
		modelAndView.addObject("comparePrevCntByResultType", comparePrevCntByResultType);
		
		
		/*List<PrivacyReport> privacyProcessCntByReqType = reportSvc.privacyProcessCntByReqType(pr1);	//9.수행업무별 접속기록 처리현황
		
		modelAndView.addObject("compUnit05", "");
		modelAndView.addObject("privacyProcessCntByReqType", privacyProcessCntByReqType);
		
		List<PrivacyReport> comparePrevCntByReqType = reportSvc.comparePrevCntByReqType(pr); 		//10.수행업무별 접속기록 처리현황(비교)
		modelAndView.addObject("compUnit06", "");
		modelAndView.addObject("compUnit06_num", "");
		modelAndView.addObject("comparePrevCntByReqType", comparePrevCntByReqType);*/
		
		//병합 쿼리
		List<PrivacyReport> empWordGrid01 = reportSvc.getEmpWordGrid01(pr1);
		
		modelAndView.addObject("wordUnit01", "");
		modelAndView.addObject("empWordGrid01", empWordGrid01);
		
		List<PrivacyReport> empWordGrid02 = reportSvc.getEmpWordGrid02(pr1);
		modelAndView.addObject("wordUnit02", "");
		modelAndView.addObject("empWordGrid02", empWordGrid02);
		
		
	    modelAndView.addObject("reportType", type);
	    //modelAndView.addObject("authorize", reportSvc.reportAuthorizeInfo());
		modelAndView.addObject("download_type", download_type);
		String pdfFullPath = request.getRequestURL() +"?"+ request.getQueryString();
		modelAndView.addObject("pdfFullPath", pdfFullPath);
		modelAndView.addObject("ui_type", ui_type);
		modelAndView.setViewName("system_empReport");
		return modelAndView;
	}
	
	@RequestMapping(value = "system_downReport.html")
	public ModelAndView findReportDetailDownload_system(HttpServletRequest request,
			@RequestParam(value = "type", required = false) int type,
			@RequestParam(value = "period_type", required = false) int period_type,
			@RequestParam(value = "system_seq", required = false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date,
			@RequestParam(value = "menu_id", required = false) String menu_id,
			@RequestParam(value = "half_type", required = false) String half_type,
			@RequestParam(value = "quarter_type", required = false) String quarter_type,
			@RequestParam(value = "download_type", required = false) String download_type,
			@RequestParam(value = "system_report", required = false) String system_report) throws Exception{
		
		ModelAndView modelAndView = new ModelAndView();
		System.out.println(">>  ======================  다운로드 보고서 ");
		System.out.println(">> ======================= download_type : " + download_type);
		System.out.println("==========================  system report : " + system_report);
		System.out.println("==========================  system seq : " + system_seq);
		
		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
	    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
		String date = dayTime.format(new Date(time));
		String ui_type = commonDao.getUiType();
		modelAndView.addObject("ui_type", ui_type);
		
		// 마지막날짜가 현재보다 클 경우 어제날짜까지로 지정해준다.
		
		Calendar cal = Calendar.getInstance();
		
		String compareDate = "";
				
		HttpSession session = request.getSession();
		String master = (String)session.getAttribute("master");
	    String use_reportLogo = (String)session.getAttribute("use_reportLogo");
	    String use_reportLine = (String)session.getAttribute("use_reportLine");
	    if (use_reportLogo != null && use_reportLogo.equals("Y")) {
		    String rootPath = request.getSession().getServletContext().getRealPath("/");
			String attach_path = "resources/upload/";
			String filename = commonDao.selectCurrentImage_logo2();
			String savePath = rootPath + attach_path + filename;
			File file = new File(savePath);
			if(file.exists()) {
				modelAndView.addObject("filename", filename);
			}
	    }
	    
	    //다운로드 보고서 > report_option 
	    Map<String, String> optionMap = new HashMap<String, String>();
		optionMap.put("code_id", "12");
		String proc_month = start_date.replace("-", "").substring(0, 6);
		optionMap.put("proc_month", proc_month);
		optionMap.put("period_type", String.valueOf(period_type));
		List<Map<String, String>> reportOption = reportDao.findReportOption(optionMap);
		
		//총평 데이터 반환
		String report_desc_str = reportDao.getReportDescStr(optionMap);
		
		modelAndView.addObject("report_desc_str", report_desc_str);
		modelAndView.addObject("reportOption", reportOption);
	    
	    modelAndView.addObject("use_reportLogo", use_reportLogo);
	    modelAndView.addObject("use_reportLine", use_reportLine);
		modelAndView.addObject("masterflag", master);
		modelAndView.addObject("report_type", type);
		modelAndView.addObject("period_type", period_type);
		modelAndView.addObject("start_date", start_date);
		modelAndView.addObject("end_date", end_date);
		modelAndView.addObject("date", date);
		modelAndView.addObject("system_seq", system_seq);
		modelAndView.addObject("logo_report_url", logo_report_url);
		modelAndView.addObject("menu_id", menu_id);
		modelAndView.addObject("half_type", half_type);
		modelAndView.addObject("quarter_type", quarter_type);

		//선택 시스템 목록
		/*List<String> syslist = new ArrayList<String>();
		String tmpList[] = system_seq.split(",");
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}*/
		List<String> syslist = new ArrayList<String>();
		String tmpList[] = system_seq.split(",");
		if("Y".equals(system_report)) {
			syslist.add(system_seq);
			modelAndView.addObject("authorize", reportSvc.reportAuthorizeInfo_system(system_seq));
		}else {
 			for ( int i=0; i< tmpList.length; ++i ) {
				syslist.add(tmpList[i]);
			}
 			modelAndView.addObject("authorize", reportSvc.reportAuthorizeInfo());
		}
		syslist.add("");
		
		String sDate[] = start_date.split("-");
		String eDate[] = end_date.split("-");

		int strYear = Integer.parseInt(sDate[0]);
		int strMonth = Integer.parseInt(sDate[1]);
		int endYear = Integer.parseInt(eDate[0]);
		int endMonth = Integer.parseInt(eDate[1]);
		int diffMon = (endYear - strYear)* 12 + (endMonth - strMonth);
		String stDate = start_date.replaceAll("-", "").substring(0,6);
		String edDate = end_date.replaceAll("-", "").substring(0,6);
		
		PrivacyReport pr = new PrivacyReport();			//선택 기간
		PrivacyReport prCompare = new PrivacyReport();	//비교 기간 (동년전월, 동년 이전분기)
		PrivacyReport pr1 = new PrivacyReport();		//
	    
		String compare_type = "1";
		modelAndView.addObject("compare_type", compare_type);
		if (period_type == 1) {
			pr.setStart_date(stDate);
			pr.setEnd_date(edDate);
			pr.setSysList(syslist);
			
			if(compare_type == "1") {			//동년 전월 (compare_type =1)
				cal.set(strYear, strMonth-1, 1);
			    cal.add(Calendar.MONTH, -1);    	
			    compareDate = dateFormatter.format(cal.getTime());
				pr.setCompare_start_date(compareDate.substring(0,6));		
				pr.setCompare_end_date(compareDate.substring(0,6));			
				
				prCompare.setStart_date(compareDate.substring(0,6));
				prCompare.setEnd_date(compareDate.substring(0,6));
				prCompare.setSysList(syslist);
			}else if(compare_type == "2") {		//전년동월 (compare_type =2)
				pr.setCompare_start_date(strYear-1+stDate.substring(4, 6));
				pr.setCompare_end_date(strYear-1+edDate.substring(4, 6));
				
				//비교 기간1(전년동월)
				prCompare.setStart_date(strYear-1+stDate.substring(4, 6));
				prCompare.setEnd_date(strYear-1+edDate.substring(4, 6));
				prCompare.setSysList(syslist);
			}
			
			
			pr1.setStart_date(strYear+"01");		
			pr1.setEnd_date(edDate);				
		    pr1.setSysList(syslist);				
			pr1.setMonth(strMonth+"");
			pr1.setPeriod_type("1");
			
		}else if(period_type == 2) {	//분기
			pr1.setStart_date(stDate);
			pr1.setEnd_date(edDate);
			pr1.setSysList(syslist);
			pr1.setPeriod_type("2");
			pr1.setQuarter_type(quarter_type);
			
			pr.setStart_date(stDate);
			pr.setEnd_date(edDate);
			pr.setSysList(syslist);
			if(compare_type == "1") {			//동년 전분기 (compare_type =1)
				cal.set(strYear, strMonth-1, 1);		
			    cal.add(Calendar.MONTH, -3);    		
			    compareDate = dateFormatter.format(cal.getTime());
				pr.setCompare_start_date(compareDate.substring(0,6));
				prCompare.setStart_date(compareDate.substring(0,6));
				
				cal.set(strYear, strMonth-1, 1);		
			    cal.add(Calendar.MONTH, -1);    		
			    compareDate = dateFormatter.format(cal.getTime());
				pr.setCompare_end_date(compareDate.substring(0,6));
				prCompare.setEnd_date(compareDate.substring(0,6));
				prCompare.setSysList(syslist);
			}else if(compare_type == "2") {		//전년 동분기 (compare_type =2)
				pr.setCompare_start_date(strYear-1+stDate.substring(4, 6));
				pr.setCompare_end_date(strYear-1+edDate.substring(4, 6));
				//전년 동일분기
				prCompare.setStart_date(strYear-1+stDate.substring(4, 6));
				prCompare.setEnd_date(strYear-1+edDate.substring(4, 6));
				prCompare.setSysList(syslist);
			}
			
		}
		else if(period_type == 4) {	//년
			pr.setStart_date(stDate);
			pr.setEnd_date(edDate);
			pr.setSysList(syslist);
			pr.setCompare_start_date(strYear-1+stDate.substring(4, 6));
			pr.setCompare_end_date(strYear-1+edDate.substring(4, 6));
			
			pr1.setStart_date(stDate);
			pr1.setEnd_date(edDate);
			pr1.setSysList(syslist);
			pr1.setPeriod_type("4");
			
			//전년
			prCompare.setStart_date(strYear-1+stDate.substring(4, 6));
			prCompare.setEnd_date(strYear-1+edDate.substring(4, 6));
			prCompare.setSysList(syslist);
			
		}
	    
		List<SystemMaster> systems = reportSvc.findSystemMasterDetailList(pr);
		modelAndView.addObject("systems", systems);
		
		int diffMonth = getDiffMonth(pr);
		modelAndView.addObject("diffMonth", diffMonth);
		
		int logCnt = reportSvc.downloadTotalLogCnt(pr);				//다운로드 로그
		modelAndView.addObject("logCnt", logCnt);
		int prevLogCnt = reportSvc.downloadTotalLogCnt(prCompare);	//개인정보 접속기록 비교기간 총 처리량(로그건수)
		modelAndView.addObject("prevLogCnt", prevLogCnt);
		
		int compareLogcnt = logCnt - prevLogCnt;
		String checkPlue1 ="";
		String checkPlue2 ="";
		if ( compareLogcnt < 0 ) {
			checkPlue1 ="0";
		}
		else {
			checkPlue1 ="1";
		}
		if ( compareLogcnt < 0 ) {
			compareLogcnt = -(compareLogcnt);
		}
		modelAndView.addObject("compareLogcnt", compareLogcnt);
		modelAndView.addObject("checkPlue1", checkPlue1);
		
		int privacyCnt = reportSvc.downloadTotalCnt(pr);				//개인정보 접속기록 이용량(개인정보유형처리(이용)건수)
		modelAndView.addObject("privacyCnt", privacyCnt);
		
		int prevPrivacyCnt = reportSvc.downloadTotalCnt(prCompare);	//개인정보 접속기록 비교기간 이용량(개인정보유형처리(이용)건수)
		modelAndView.addObject("prevPrivacyCnt", prevPrivacyCnt);
		
		int comparePrivacyCnt = privacyCnt - prevPrivacyCnt;
		if ( comparePrivacyCnt < 0 ) {
			checkPlue2 ="0";
		}
		else {
			checkPlue2 ="1";
		}
		if ( comparePrivacyCnt < 0 ) {
			comparePrivacyCnt = -(comparePrivacyCnt);
		}
		modelAndView.addObject("comparePrivacyCnt", comparePrivacyCnt);
		modelAndView.addObject("checkPlue2", checkPlue2);
		
		
		List<PrivacyReport> privacyProcessCntBySystem = reportSvc.downloadProcessCntBySystem(pr1);
		modelAndView.addObject("privacyProcessCntBySystem", privacyProcessCntBySystem);
		
		List<PrivacyReport> privacyUseCntBySystem = reportSvc.downloadUseCntBySystem(pr1);			//2.시스템별 개인정보 접속기록 이용현황
		modelAndView.addObject("privacyUseCntBySystem", privacyUseCntBySystem);
		
		List<PrivacyReport> comparePrevCntBySystem = reportSvc.downloadcomparePrevCntBySystem(pr); 			//3.시스템별 개인정보 접속기록 현황(비교)
		
		
		//단위데이터 미사용
		modelAndView.addObject("compareUnit01", "");
		modelAndView.addObject("compareUnit01_num", "");
		modelAndView.addObject("comparePrevCntBySystem", comparePrevCntBySystem);
		
		List<PrivacyReport> comparePrevCntBySystemTop3 = reportSvc.downloadcomparePrevCntBySystemTop3(pr); 			//3.시스템별 개인정보 접속기록 현황(비교)
		
		//단위데이터 사용 / 미사용
		modelAndView.addObject("compareUnit03", "");
		modelAndView.addObject("comparePrevCntBySystemTop3", comparePrevCntBySystemTop3);
		
		//시스템 개인정보 top3 이하 데이터
		PrivacyReport comparePrevCntBySystemSum = reportSvc.downloadComparePrevCntBySystemSum(pr);
		modelAndView.addObject("comparePrevCntBySystemSum", comparePrevCntBySystemSum);
		
		List<PrivacyReport> privacyProcessCntByDept = reportSvc.downloadProcessCntByDept(pr1);		//4.소속별 개인정보 접속기록 처리현황
		if(privacyProcessCntByDept.size()==1){
			privacyProcessCntByDept = new ArrayList<PrivacyReport>();
		}
		modelAndView.addObject("privacyProcessCntByDept", privacyProcessCntByDept);
		
		List<PrivacyReport> privacyUseCntByDept = reportSvc.downloadUseCntByDept(pr1);				//5.부서별 개인정보 접속기록 이용현황
		if(privacyUseCntByDept.size()==1){
			privacyUseCntByDept = new ArrayList<PrivacyReport>();
		}
		modelAndView.addObject("privacyUseCntByDept", privacyUseCntByDept);
		
		List<PrivacyReport> comparePrevCntByDept = reportSvc.downloadComparePrevCntByDept(pr); 				//6.소속별 개인정보 접속기록 현황(비교)
		if(comparePrevCntByDept.size()==1){
			comparePrevCntByDept = new ArrayList<PrivacyReport>();
		}
		
		//단위데이터 미사용
		modelAndView.addObject("compareUnit02", "");
		modelAndView.addObject("comparePrevCntByDept", comparePrevCntByDept);
		
		//소속 top5
		List<PrivacyReport> comparePrevCntByDeptTop5 = reportSvc.downloadComparePrevCntByDeptTop5(pr); 				//6.소속별 개인정보 접속기록 현황(비교)
		
		//단위데이터 사용 / 미사용
		//modelAndView.addObject("compareUnit04", comparePrevCntByDeptTop5.get(0).getDataUnit());
		modelAndView.addObject("compareUnit04", "");
		modelAndView.addObject("comparePrevCntByDeptTop5", comparePrevCntByDeptTop5);
		
		
		List<PrivacyReport> comparePrevCntByResultType = new ArrayList<>();
		List<Code> resultTypeList = reportSvc.findResultTypeList();		
		
		for(int i=0; i<resultTypeList.size(); i++) {
			Code code = resultTypeList.get(i);
			pr1.setType("TYPE"+code.getCode_id());
			pr.setType("TYPE"+code.getCode_id());
			//PrivacyReport returnResult = reportSvc.privacyUseCntByResultType(pr1);					//7.개인정보 유형별 접속기록 이용현황
			PrivacyReport returnResult2 = reportSvc.comparePrevCntByResultType_download(pr);					//7.개인정보 유형별 접속기록 이용현황(비교)
			//returnResult.setCode_name(code.getCode_name());
			returnResult2.setCode_name(code.getCode_name());
			comparePrevCntByResultType.add(i, returnResult2);
		}
		
		//단위데이터 사용 / 미사용
		//modelAndView.addObject("compareUnit05", comparePrevCntByResultType.get(0).getDataUnit());
		modelAndView.addObject("compareUnit05", "");
		modelAndView.addObject("comparePrevCntByResultType", comparePrevCntByResultType);
		
		
		if(period_type == 4) {
			
			List<PrivacyReport> privacylogByResult = new ArrayList<>();
			//List<Code> resultTypeList = reportSvc.findResultTypeList();
			resultTypeList = reportSvc.findResultTypeList();
			for(int i=0; i<resultTypeList.size(); i++) {
				Code code = resultTypeList.get(i);
				pr.setType("TYPE"+code.getCode_id());
				PrivacyReport returnResult = reportSvc.downloadlogByResultType(pr);
				returnResult.setCode_name(code.getCode_name());
				privacylogByResult.add(i, returnResult);
			}
			modelAndView.addObject("privacylogByResult", privacylogByResult);
			
			//전월비교
		}else {

		}
		
		
		//차트 데이터 > grid 출력
		List<PrivacyReport> chart_privacyUseCntBySystem_dn = reportSvc.chart_privacyUseCntBySystem_download(pr);
		List<PrivacyReport> reportByDept_dn = reportSvc.chart_privacyUseCntByDept_download(pr);
		//PrivacyReport returnResult = reportSvc.chart_privacyUseCntByResultType_download(pr);
		
		List<PrivacyReport> chart_privacyUseCntByResultType = new ArrayList<>();
		//List<Code> resultTypeList = reportSvc.findResultTypeList();
		resultTypeList = reportSvc.findResultTypeList();
		for(int i=0; i<resultTypeList.size(); i++) {
			Code code = resultTypeList.get(i);
			pr.setType("TYPE"+code.getCode_id());
			PrivacyReport returnResult = reportSvc.chart_privacyUseCntByResultType_download(pr);
			returnResult.setCode_name(code.getCode_name());
			chart_privacyUseCntByResultType.add(i, returnResult);
		}
		
		ListComparator comp = new ListComparator();
		Collections.sort(chart_privacyUseCntByResultType, comp); //list sorting
		//Collections.sort(comparePrevCntByResultType, comp);//list sorting

		List<PrivacyReport> fileExtDatas_dn = reportSvc.getFileExtension_chart(pr1);
		modelAndView.addObject("ext_unit_01", "");
		
		List<PrivacyReport> fileExtData_dn_compare = reportSvc.getFileExtensionCompare_chart(pr);
		
		modelAndView.addObject("ext_unit_02", "");
		modelAndView.addObject("ext_unit_num_02", "");
		
		List<PrivacyReport> fileExtData_dn_compare_top5 = reportSvc.getFileExtensionCompare_chart_top5(pr);
		modelAndView.addObject("ext_unit_03", "");
		PrivacyReport fileExtData_after = reportSvc.getFileExtensionCompare_after(pr);
		
		modelAndView.addObject("fileExtData_dn_compare", fileExtData_dn_compare);
		modelAndView.addObject("fileExtData_dn_compare_top5", fileExtData_dn_compare_top5);
		modelAndView.addObject("fileExtData_after", fileExtData_after);
		
		
		//[다운로드보고서] 1. 시스템별 개인정보 다운로드 처리현황
		List<PrivacyReport> wordGrid = reportSvc.getWordReportData1(pr1);
		modelAndView.addObject("grid_unit_01", "");
		modelAndView.addObject("grid_unit_01_num", "");
		
		List<PrivacyReport> wordGrid02 = reportSvc.getWordReportData2(pr1);
		modelAndView.addObject("grid_unit_02", "");
		modelAndView.setViewName("system_downReport");
	    modelAndView.addObject("reportType", type);
	    //modelAndView.addObject("authorize", reportSvc.reportAuthorizeInfo());
	    modelAndView.addObject("resultTypeList", chart_privacyUseCntByResultType);
		modelAndView.addObject("download_type", download_type);
		String pdfFullPath = request.getRequestURL() +"?"+ request.getQueryString();
		modelAndView.addObject("pdfFullPath", pdfFullPath);
		modelAndView.addObject("fileExtDatas_dn", fileExtDatas_dn);
		modelAndView.addObject("ui_type", ui_type);
		modelAndView.addObject("wordGrid", wordGrid);
		modelAndView.addObject("wordGrid02", wordGrid02);
		return modelAndView;
	}
	
	//CPO_오남용보고 reportCPOPop.jsp
	@RequestMapping(value="system_cpoReport.html", method={RequestMethod.GET, RequestMethod.POST})
	public ModelAndView findReportCPO_system(@RequestParam Map<String, String> param, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		int reportType = Integer.parseInt(param.get("type"));
		int dateType = Integer.parseInt(param.get("period_type"));
		String systemSeq = String.valueOf(param.get("system_seq"));
		String startDate = String.valueOf(param.get("start_date"));
		String endDate = String.valueOf(param.get("end_date"));
		String menuId = String.valueOf(param.get("menu_id"));
		String downType = String.valueOf(param.get("download_type"));
		String quarter_type = String.valueOf(param.get("quarter_type"));
		String system_report = String.valueOf(param.get("system_report"));
		
		System.out.println(">>  ======================  CPO_오남용 보고서 ");
		System.out.println(">> ======================= download_type : " + downType);
		System.out.println("==========================  system report : " + system_report);
		System.out.println("==========================  system seq : " + systemSeq);
		
		DateUtil dateUtil = new DateUtil();
		String date = dateUtil.getDateNow();
		endDate = dateUtil.getReportEndDate(endDate);
		
		HttpSession session = request.getSession();
		String ui_type = (String)session.getAttribute("ui_type");
	    String master = (String)session.getAttribute("master");
	    String use_reportLogo = (String)session.getAttribute("use_reportLogo");
	    String use_reportLine = (String)session.getAttribute("use_reportLine");
	    String pdfFullPath = request.getRequestURL() +"?"+ request.getQueryString();
	    
	    //로그 파일이 존재하면 생성
	    if (use_reportLogo != null && use_reportLogo.equals("Y")) {
			String filename = commonDao.selectCurrentImage_logo2();
			String savePath = request.getSession().getServletContext().getRealPath("/") + "resources/upload/" + filename;
			File file = new File(savePath);
			if(file.exists()) {
				mav.addObject("filename", filename);
			}
	    }
	    
	    //system_seq가져오기(시스템 리스트로 분리)
//		String[] tmpList = systemSeq.split(",");
//		List<String> sysList = new ArrayList<String>(Arrays.asList(tmpList));
//		sysList.add("00");
	    List<String> sysList = new ArrayList<String>();
		String tmpList[] = systemSeq.split(",");
		if("Y".equals(system_report)) {
			sysList.add(systemSeq);
			mav.addObject("authorize", reportSvc.reportAuthorizeInfo_system(systemSeq));
		}else {
 			for ( int i=0; i< tmpList.length; ++i ) {
 				sysList.add(tmpList[i]);
			}
 			mav.addObject("authorize", reportSvc.reportAuthorizeInfo());
		}
		sysList.add("");
		
		
		// yyyyMM ( start_date, end_date )
		PrivacyReport pr = dateUtil.getYyyyMmDatePriReportVO(startDate, endDate);
		pr.setSysList(sysList);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
		Calendar cal = Calendar.getInstance();
		String sd = pr.getStart_date();
		String ed = pr.getEnd_date();
		try {
			if(dateType==1) {// 월
				cal.setTime(sdf.parse(sd));
				cal.add(cal.MONTH, -1);
				pr.setCompare_start_date(sdf.format(cal.getTime()));
				pr.setCompare_end_date(sdf.format(cal.getTime()));
			} else if(dateType==2) {// 분기
				cal.setTime(sdf.parse(sd));
				cal.add(cal.MONTH, -3);
				pr.setCompare_start_date(sdf.format(cal.getTime()));
				cal.setTime(sdf.parse(ed));
				cal.add(cal.MONTH, -3);
				pr.setCompare_end_date(sdf.format(cal.getTime()));
			} else if(dateType==4) {// 연도
				cal.setTime(sdf.parse(sd));
				cal.add(cal.YEAR, -1);
				pr.setCompare_start_date(sdf.format(cal.getTime()));
				cal.setTime(sdf.parse(ed));
				cal.add(cal.YEAR, -1);
				pr.setCompare_end_date(sdf.format(cal.getTime()));
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		// 점검대상 ( 월간, 분기, 연간에 따라 다름 )
		List<PrivacyReport> systems = reportSvc.abnormalTotalListBySystem(pr, dateType, sd);
		mav.addObject("systems", systems);
		
		// 총 처리량
		int totalCnt = reportSvc.abnormalTotalCnt(pr);
		mav.addObject("totalCnt", totalCnt);
		// 시스템별 시나리오별 처리량
		List<PrivacyReport> systemByScenario = reportSvc.abnormalSystemList(pr);
		mav.addObject("systemByScenario", systemByScenario);
		// 소속별 시나리오별 처리량
		List<PrivacyReport> deptByScenario = reportSvc.abnormalDeptList(pr);
		mav.addObject("deptByScenario", deptByScenario);
		// 개인별 시나리오별 처리량
		List<PrivacyReport> userByScenario = reportSvc.abnormalUserList(pr);
		mav.addObject("userByScenario", userByScenario);
		
		// 시스템별 처리량 이용량 top5
		List<PrivacyReport> systemTop5 = reportSvc.abnormalSystemTop5(pr);
		mav.addObject("systemTop5", systemTop5);
		// 부서별 처리량 이용량 top5
		List<PrivacyReport> deptTop5 = reportSvc.abnormalDeptTop5(pr);
		mav.addObject("deptTop5", deptTop5);
		String deptNameTop5 = "";
		if(deptTop5.get(0) != null) {
			for(int i=0; i<deptTop5.size()-1; i++) {
				String dept_name = deptTop5.get(i).getDept_name();
				if(dept_name != null) {
					deptNameTop5 += dept_name + ", ";
				} else {
					deptNameTop5 = deptNameTop5.substring(0, deptNameTop5.length()-2);
					break;
				}
			}
		}
		mav.addObject("deptNameTop5", deptNameTop5);
		// 개인별 처리량 이용량 top5
		List<PrivacyReport> userTop5 = reportSvc.abnormalUserTop5(pr);
		mav.addObject("userTop5", userTop5);
		String empUserNameTop5 = "";
		if(userTop5.get(0) != null) {
			for(int i=0; i<userTop5.size()-1; i++) {
				String emp_user_name = userTop5.get(i).getEmp_user_name();
				String dept_name = userTop5.get(i).getDept_name();
				if(emp_user_name != null) {
					if(emp_user_name.equals("공통")) {
						empUserNameTop5 += emp_user_name + ", ";
					} else {
						empUserNameTop5 += dept_name + " " + emp_user_name + ", ";
					}
				} else {
					empUserNameTop5 = empUserNameTop5.substring(0, empUserNameTop5.length()-2);
					break;
				}
			}
		}
		mav.addObject("empUserNameTop5", empUserNameTop5);
		
		// report_option 정보
		Map<String, String> optionMap = new HashMap<String, String>();
		optionMap.put("code_id", "2");
		String proc_month = startDate.replace("-", "").substring(0, 6);
		optionMap.put("proc_month", proc_month);
		optionMap.put("period_type", param.get("period_type"));
		List<Map<String, String>> reportOption = reportDao.findReportOption(optionMap);
		mav.addObject("reportOption", reportOption);
		
		// top5 표 컬럼 ex. 전월/당월, 이전분기/1분기, 전년/금년
		String preText = "";
		String thisText = "";
		String reportDate = pr.getStart_date().substring(0, 4) +"년 ";
		if(dateType==1) {// 월
			preText = "전월";
			thisText = "당월";
			reportDate += pr.getStart_date().substring(4)+"월";
		} else if(dateType==2) {// 분기
			preText = "이전분기";
			thisText = quarter_type+"분기";
			reportDate += thisText;
		} else if(dateType==4) {// 연도
			preText = "전년";
			thisText = "금년";
		}
		mav.addObject("preText", preText);
		mav.addObject("thisText", thisText);
		mav.addObject("reportDate", reportDate);
		mav.addObject("start_date", startDate);
		mav.addObject("end_date", endDate);
		mav.addObject("date", date);
		
		mav.addObject("authorize", reportSvc.reportAuthorizeInfo()); // 결재라인
	    mav.addObject("use_reportLine", use_reportLine);
	    mav.addObject("use_reportLogo", use_reportLogo);
	    mav.addObject("masterflag", master);
	    mav.addObject("reportType", reportType);
	    mav.addObject("period_type", dateType);
	    mav.addObject("system_seq", systemSeq);
	    mav.addObject("use_studentId", use_studentId);
	    mav.addObject("logo_report_url", logo_report_url);
	    mav.addObject("menu_id", menuId);
	    mav.addObject("download_type", downType);
	    mav.addObject("pdfFullPath", pdfFullPath);
	    mav.addObject("ui_type", ui_type);
	    
	    List<SystemMaster> systems_name = reportSvc.findSystemMasterDetailList(pr);
		mav.addObject("systems_name", systems_name);
		
		mav.setViewName("system_cpoReport");
		return mav;
	}
	
}

