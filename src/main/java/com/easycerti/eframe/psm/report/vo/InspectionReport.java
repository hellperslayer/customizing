package com.easycerti.eframe.psm.report.vo;

public class InspectionReport {
	private String report_seq;
	private String report_data;
	private String proc_date;
	private String report_type;

	private String update_id;
	private String search_fr;
	private String search_to;
	
	private String update_time;
	
	private String report_title;
	
	public String getReport_title() {
		return report_title;
	}
	public void setReport_title(String report_title) {
		this.report_title = report_title;
	}
	public String getUpdate_time() {
		return update_time;
	}
	public void setUpdate_time(String update_time) {
		this.update_time = update_time;
	}
	public String getReport_seq() {
		return report_seq;
	}
	public void setReport_seq(String report_seq) {
		this.report_seq = report_seq;
	}

	public String getReport_data() {
		return report_data;
	}
	public void setReport_data(String report_data) {
		this.report_data = report_data;
	}
	public String getProc_date() {
		return proc_date;
	}
	public void setProc_date(String proc_date) {
		this.proc_date = proc_date;
	}
	public String getReport_type() {
		return report_type;
	}
	public void setReport_type(String report_type) {
		this.report_type = report_type;
	}
	public String getSearch_fr() {
		return search_fr;
	}
	public void setSearch_fr(String search_fr) {
		this.search_fr = search_fr;
	}
	public String getSearch_to() {
		return search_to;
	}
	public void setSearch_to(String search_to) {
		this.search_to = search_to;
	}
	public String getUpdate_id() {
		return update_id;
	}
	public void setUpdate_id(String update_id) {
		this.update_id = update_id;
	}
	
}
