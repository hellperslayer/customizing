package com.easycerti.eframe.psm.report.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.vo.SearchBase;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.report.vo.Report;
import com.easycerti.eframe.psm.report.vo.UploadReport;
import com.easycerti.eframe.psm.search.vo.AllLogInq;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.system_management.vo.AccessAuth;
import com.easycerti.eframe.psm.system_management.vo.AccessAuthSearch;
import com.easycerti.eframe.psm.system_management.vo.PrivacyReport;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;


public interface ReportSvc {
	List<SystemMaster> findSystemMasterList();
	List<SystemMaster> findSystemMasterDetailList(PrivacyReport pr);
	List<SystemMaster> findSystemMasterDetailDownList(PrivacyReport pr);
	List<PrivacyReport> findPrivacyReportCntList(PrivacyReport tmpBean);
	List<PrivacyReport> findPrivacyReportCntbyEmp(PrivacyReport tmpBean);
	String findDeptNameByEmpUserId(String emp_user_id);
	String findNameByEmpUserId(String emp_user_id);
	List<PrivacyReport> findSystemMasterTop10(PrivacyReport tmpBean);
	List<SystemMaster> findSystemMasterList(SearchSearch search);
	List<PrivacyReport> findPrivacyReportBySystem(PrivacyReport pr);
	List<PrivacyReport> findPrivacyReportByDept(PrivacyReport pr);
	List<PrivacyReport> findPrivacyReportByDeptUseStudentId(PrivacyReport pr);
	PrivacyReport findPrivacyReportBySystemTop1(PrivacyReport pr);
	PrivacyReport findPrivacyReportByDeptTop1(PrivacyReport pr);
	
	List<PrivacyReport> findPrivacyReportByReqtype(PrivacyReport pr);
	List<PrivacyReport> findPrivacyReportByReqtypeCnt(PrivacyReport pr);
	List<PrivacyReport> findPrivacyReportByResultType(PrivacyReport pr);
	int findEmpDetailCount(PrivacyReport pr);
	
	List<PrivacyReport> findPrivTypeCount(PrivacyReport pr);
	List<PrivacyReport> findEmpDetailList(PrivacyReport pr);
	List<PrivacyReport> findPrivTypeResultCountTop10(PrivacyReport pr);
	List<PrivacyReport> findPrivTypeResultCount(PrivacyReport report);
	List<PrivacyReport> findDeptTypeResultCountTop10(PrivacyReport pr);
	List<PrivacyReport> findPrivTypeResultCountBySys(PrivacyReport report);
	List<PrivacyReport> findPrivTypeResultCountBySysTop10(PrivacyReport pr);
	List<PrivacyReport> findDeptTypeResultCount(PrivacyReport report);
	List<PrivacyReport> findDeptTypeResultCountBySys(PrivacyReport report);
	List<PrivacyReport> findDeptTypeResultCountBySysTop10(PrivacyReport pr);
	List<PrivacyReport> findSystemTypeResultCountTop10(PrivacyReport pr);
	List<PrivacyReport> findSystemTypeResultCount(PrivacyReport report);
	List<PrivacyReport> findSystemMaster(PrivacyReport pr);
	List<PrivacyReport> findSystemCurdCount(PrivacyReport tmp);
	List<PrivacyReport> findPrivacyReportByDept_1(PrivacyReport pr);
	List<PrivacyReport> findPrivacyReportByDept_2(PrivacyReport report);
	List<PrivacyReport> findReportDetail2_chart1(PrivacyReport pr);
	List<PrivacyReport> findReportDetail2_chart2(PrivacyReport pr);
	List<PrivacyReport> findReportDetailbyIndividual(PrivacyReport pr);

	List<PrivacyReport> findReportDetail_chart5(PrivacyReport pr);
	List<PrivacyReport> findReportDetail_chart5_useStudentId(PrivacyReport pr);
	PrivacyReport findReportDetail_chart5_detail(PrivacyReport report);
	
	List<PrivacyReport> findPrivacyReportDetailChart3_1(PrivacyReport pr);
	List<PrivacyReport> findPrivacyReportDetailChart3_3(PrivacyReport pr);
	List<PrivacyReport> findPrivacyReportDetailChart3_4(PrivacyReport pr);
	List<PrivacyReport> findEmpDetailListCount(PrivacyReport pr);
	List<PrivacyReport> findPrivTypeCountWeek(PrivacyReport pr);

	List<PrivacyReport> findTypeReportCount(PrivacyReport pr);
	List<PrivacyReport> findSystemReportCount(PrivacyReport pr);
	List<PrivacyReport> findDeptReportCount(PrivacyReport pr);
	List<PrivacyReport> findIndvReportCount(PrivacyReport pr);
	
	List<PrivacyReport> findReportHalf_chart2(PrivacyReport pr);
	List<PrivacyReport> findReportHalf_chart3(PrivacyReport pr);
	List<PrivacyReport> findReportHalf_chart4(PrivacyReport pr);
	List<PrivacyReport> findReportHalf_chart5(PrivacyReport pr);
	List<PrivacyReport> findReportHalf_table5(PrivacyReport pr);
	PrivacyReport findReportHalf_chart6(PrivacyReport pr);
	
	public DataModelAndView addFile(MultipartHttpServletRequest multi, int type);
	
	public List<Code> findResultTypeList(); 
	
	public List<PrivacyReport> getLogByReqType(List<PrivacyReport> list);
	
	public List<AllLogInq> findAllLogInqList(PrivacyReport pr);
	public List<AccessAuth> findAccessAuthList(AccessAuthSearch search);
	
	//다운로드 보고서
	int findDownCountTot(PrivacyReport pr);
	String findDownBySysCountTot(PrivacyReport pr);
	List<PrivacyReport> findDownloadCntBySysTop5(PrivacyReport pr);
	List<PrivacyReport> findDownloadCntByDeptTop5(PrivacyReport pr);
	List<PrivacyReport> findDownloadCntByResultTypeTop5(PrivacyReport pr);
	
	List<PrivacyReport> findDownloadCntBySys(PrivacyReport pr);				//시스템별 개인정보 다운로드 현황
	List<PrivacyReport> findResultCntBySys(PrivacyReport pr);				//시스템별 개인정보 건수
	List<PrivacyReport> findDownloadCntByDeptTop30(PrivacyReport pr);		//주요 부서별 개인정보 다운로드 현황 TOP30
	List<PrivacyReport> findResultCntByDeptTop30(PrivacyReport pr);			//주요 부서별 개인정보 건수 TOP30
	List<PrivacyReport> findDownloadCntByUserTop10(PrivacyReport pr);		//주요 사용자별 개인정보 다운로드 현황 TOP10
	List<PrivacyReport> findResultCntByUserTop10(PrivacyReport pr);			//주요 사용자별 개인정보 건수 TOP10
	List<PrivacyReport> findDownloadCntByUrlTop5(PrivacyReport pr);			//주요 URL별 다운로드 현황 TOP5
	List<PrivacyReport> findResultCntByUrlTop5(PrivacyReport pr);			//주요 URL별 개인정보 건수 TOP5
	List<PrivacyReport> findDownloadCntByResultTop10(PrivacyReport pr);		//주요 개인정보 유형별 다운로드한 현황TOP10 
	List<PrivacyReport> findResultCntByResultTop10(PrivacyReport pr);		//주요 개인정보 유형별 개인정보 건수 TOP10 
	List<PrivacyReport> findSystemNameByUserTop10(PrivacyReport pr);		//주요 사용자 접근 시스템
	List<PrivacyReport> findDownloacCntByReasonTop10(PrivacyReport pr);		//주요 사유별 개인정보 다운로드 현황 TOP10
	List<PrivacyReport> findResultCntByReasonTop10(PrivacyReport pr);		//주요 사유별 개인정보 건수 TOP10
	
	List<PrivacyReport> findSystemTypeDownloadResultCountTop10(PrivacyReport pr);
	List<PrivacyReport> findSystemTypeDownloadResultCount(PrivacyReport pr);

	//점검보고서 관리 페이지
	long removeReportDelete(long report_seq);
	String saveReportDownloadTime(long report_seq);
	DataModelAndView findReportList(SearchBase search, HttpServletRequest request);
	Report findReportPdfPath(String report_seq);
	int reportFileValidation(Report report);
	
	//소명_보고서
	public int findSystemCnt();
	public int findSummaryLogCnt(Report report);
	public int findSummonReqCnt(Report report);
	public int findSummonUserCnt(Report report);
	PrivacyReport findSummonSystem(Report report);
	PrivacyReport finddeptTop(Report report);
	public List<PrivacyReport> findSummonList(Report report);
	public List<PrivacyReport> findSummonStatusList(Report report);
	public List<PrivacyReport> findSummonResultList(Report report);
	public List<PrivacyReport> findPrivacytype(Report report);
	public List<PrivacyReport> findSummonSystemRepot(Report report);
	
	//노홍현
	//권한관리_보고서
	int findAuthInfoAllCount(PrivacyReport pr);	
	List<PrivacyReport> findAuthInfoBySysCount(PrivacyReport pr);
	List<PrivacyReport> findAuthInfoAllCountByDept(PrivacyReport pr);
	List<PrivacyReport> findAuthInfoAllCountByAuth(PrivacyReport pr);
	List<PrivacyReport> findAuthInfoAllCountByReqType(PrivacyReport pr);	
	List<PrivacyReport> findAuthInfoByDeptCountByReqType(PrivacyReport pr);
	List<PrivacyReport> findAuthInfoByAuthCountByReqType(PrivacyReport pr);	
	List<PrivacyReport> findAuthInfoList2(PrivacyReport pr);
	
	//노홍현 2020.03.17
	//수준진단보고서 START
	int findPrivacyTotalLogCnt(PrivacyReport pr);							//개인정보  총 처리량(로그건수,다운로드건수)
	int findPrivacyTotalCnt(PrivacyReport pr);								//개인정보  총 이용량(개인정보유형처리(이용)건수)
	
	List<PrivacyReport> privacyProcessCntBySystem(PrivacyReport pr);		//1.시스템별 개인정보 접속기록 처리현황 
	List<PrivacyReport> privacyUseCntBySystem(PrivacyReport pr);			//2.시스템별 개인정보 접속기록 이용현황
	List<PrivacyReport> comparePrevCntBySystem(PrivacyReport pr);			//3.시스템별 개인정보 접속기록 현황(비교)
	List<PrivacyReport> privacyProcessCntByDept(PrivacyReport pr);			//4.부서별 개인정보 접속기록 처리현황
	List<PrivacyReport> privacyUseCntByDept(PrivacyReport pr);				//5.부서별 개인정보 접속기록 이용현황
	List<PrivacyReport> comparePrevCntByDept(PrivacyReport pr);				//6.소속별 개인정보 접속기록 현황(비교)
	
	PrivacyReport findPrivacylogByResultType(PrivacyReport pr);				//7.개인정보 유형별 접속기록 현황 (당월,누적)(년간)
	PrivacyReport privacyUseCntByResultType(PrivacyReport pr);				//7.개인정보 유형별 접속기록 이용현황
	PrivacyReport comparePrevCntByResultType(PrivacyReport pr);				//7.개인정보 유형별 접속기록 이용현황
		
	
	
	List<PrivacyReport> privacyProcessCntByReqType(PrivacyReport pr);		//9.수행업무별 접속기록 처리현황
	List<PrivacyReport> comparePrevCntByReqType(PrivacyReport pr);			//10.수행업무별 접속기록 처리현황
	
	
	//report_chart
	List<PrivacyReport> chart_privacyUseCntBySystem(PrivacyReport pr);		//1.시스템별 개인정보 접속기록 이용 현황(차트)
	List<PrivacyReport> chart_privacyUseCntByDept(PrivacyReport pr);		//2.소속별 개인정보 접속기록 이용 현황(차트)
	PrivacyReport chart_privacyUseCntByResultType(PrivacyReport pr);		//3.개인정보유형별 개인정보 접속기록 이용 현황(차트)
	
	
	
	List<PrivacyReport> sumLogCntByDeptYear(PrivacyReport pr);				//4.소속별 개인정보 접속기록 현황(분기,누적)(년간) 
	List<PrivacyReport> findLogCntByDept(PrivacyReport pr);					//5.소속별 개인정보 접속기록 현황(비교)(년간)
	
	
	
	List<PrivacyReport> findPrivacylogCntBySystem(PrivacyReport pr);		//시스템별  접속기록 총 처리량(로그건수)
	PrivacyReport findPrivacylogCntBySystemCompare(PrivacyReport report);	//시스템별  접속기록 총 처리량 비교(로그건수) 전월,전년비교
	
	
	List<PrivacyReport> findPrivacyCntBySystem(PrivacyReport pr);			//시스템별  접속기록 총 이용량(개인정보유형처리(이용)건수)
	PrivacyReport findPrivacyCntBySystemCompare(PrivacyReport report);		//시스템별  접속기록 총 이용량 비교(개인정보유형처리(이용)건수) 전월,전년비교
	
	List<PrivacyReport> findPrivacylogCntByDept(PrivacyReport pr);			//부서별  접속기록 총 처리량(로그건수)
	PrivacyReport findPrivacylogCntByDeptCompare(PrivacyReport report);		//부서별  접속기록 총 처리량 비교(로그건수) 전월,전년비교
	//List<PrivacyReport> findPrivacyCntByDept(PrivacyReport pr);				//부서별  접속기록 총 이용량(개인정보유형처리(이용)건수)
	PrivacyReport findPrivacyCntByDeptCompare(PrivacyReport report);		//부서별  접속기록 총 이용량 비교(개인정보유형처리(이용)건수) 전월,전년비교
	
	//보고서 코드로 제목 가져오기
	Map<String, String> findReportCode();
	
	//업로드 보고서 히스토리
	DataModelAndView uploadReportList(UploadReport uploadReport, HttpServletRequest request);
	String uploadReportRemove(UploadReport uploadReport, HttpServletRequest request);
	int downloadTotalLogCnt(PrivacyReport pr); //다운로드 보고서 > 시스템 별 처리 현황
	int downloadTotalCnt(PrivacyReport pr);
	List<PrivacyReport> downloadProcessCntBySystem(PrivacyReport pr1);
	List<PrivacyReport> downloadUseCntBySystem(PrivacyReport pr1);
	List<PrivacyReport> downloadcomparePrevCntBySystem(PrivacyReport pr);
	List<PrivacyReport> downloadProcessCntByDept(PrivacyReport pr1);
	List<PrivacyReport> downloadUseCntByDept(PrivacyReport pr1);
	List<PrivacyReport> downloadComparePrevCntByDept(PrivacyReport pr);
	PrivacyReport downloadlogByResultType(PrivacyReport pr);
	
	
	int report_upload(Map<String, String> parameters, HttpServletRequest request);
	List<PrivacyReport> chart_privacyUseCntBySystem_download(PrivacyReport pr);
	List<PrivacyReport> chart_privacyUseCntByDept_download(PrivacyReport pr);
	PrivacyReport chart_privacyUseCntByResultType_download(PrivacyReport pr);
	List<PrivacyReport> findPrivacyReportByReqtype_download(PrivacyReport pr);
	List<PrivacyReport> getFileExtension_chart(PrivacyReport pr);
	String uploadReportOne(Map<String, String> parameters);
	PrivacyReport downloadComparePrevCntBySystemSum(PrivacyReport pr);
	List<PrivacyReport> downloadcomparePrevCntBySystemTop3(PrivacyReport pr);
	List<PrivacyReport> downloadComparePrevCntByDeptTop5(PrivacyReport pr);
	PrivacyReport comparePrevCntByResultType_download(PrivacyReport pr);
	List<PrivacyReport> getFileExtensionCompare_chart(PrivacyReport pr);
	List<PrivacyReport> getFileExtensionCompare_chart_top5(PrivacyReport pr);
	PrivacyReport getFileExtensionCompare_after(PrivacyReport pr);
	
	String addReportOption(Map<String, String> parameters);
	String saveReportOption(Map<String, String> parameters);
	List<Map<String,String>> findReportOption(Map<String, String> parameters);
	List<PrivacyReport> getWordReportData1(PrivacyReport pr1);
	List<PrivacyReport> getWordReportData2(PrivacyReport pr1);
	List<PrivacyReport> getEmpWordGrid01(PrivacyReport pr1);
	List<PrivacyReport> getEmpWordGrid02(PrivacyReport pr1);
	Integer checkBeforeReport(Map<String, String> parameters);
	
	void makeExcelReportAll(DataModelAndView modelAndView, Map<String, String> map, HttpServletRequest request);
	void makeExcelReportDown(DataModelAndView modelAndView, Map<String, String> map, HttpServletRequest request);
	
	public String reportAuthorizeInfo();
	List<PrivacyReport> chart_findPrivacyReportByReqtype(PrivacyReport pr);
	
	String inspectionDataLoad(Map<String, String> param);
	String inspectionDataSave(Map<String, String> param, HttpServletRequest request);
	DataModelAndView report_inspection(String search_fr, String search_to);
	DataModelAndView report_inspection(String report_seq);
	String inspectionDataDelete(Map<String, String> param, HttpServletRequest request);
	
	// 오남용보고서
	public List<PrivacyReport> abnormalTotalListBySystem(PrivacyReport pr, int period_type, String start_date);
	
	public int abnormalTotalCnt(PrivacyReport pr);
	public List<PrivacyReport> abnormalSystemList(PrivacyReport pr);
	public List<PrivacyReport> abnormalDeptList(PrivacyReport pr);
	public List<PrivacyReport> abnormalUserList(PrivacyReport pr);
	
	public List<PrivacyReport> abnormalSystemTop5(PrivacyReport pr);
	public List<PrivacyReport> abnormalDeptTop5(PrivacyReport pr);
	public List<PrivacyReport> abnormalUserTop5(PrivacyReport pr);
	
	public List<PrivacyReport> abnormalSystemTop5Chart(PrivacyReport pr);
	public List<PrivacyReport> abnormalDeptTop5Chart(PrivacyReport pr);
	public List<PrivacyReport> abnormalUserTop5Chart(PrivacyReport pr);
	List<PrivacyReport> comparePrevCntByUser(PrivacyReport pr);
	List<PrivacyReport> chart_privacyUseCntByUser(PrivacyReport pr);
	int systemReportDelete(Report parameters);
	//시스템 report
	public Report findSystemReportPath(Report report);
	public String reportAuthorizeInfo_system(String system_seq);
	
	public List<PrivacyReport> findOrgRuleInfoIsks();
	public PrivacyReport findTotalSummonCntIsks(Report report);
	public PrivacyReport findTotalExtrtCntIsks(Report report);
	public List<PrivacyReport> findTotalRuleSystemCntIsks(Report report);
}
