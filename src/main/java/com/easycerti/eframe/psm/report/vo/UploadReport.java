package com.easycerti.eframe.psm.report.vo;

import java.util.Date;

public class UploadReport {
	private Integer report_id;
	private Integer report_fk_id;
	private String report_type;
	private String file_path;
	private Date upload_date;
	private Date delete_date;
	private String upload_user;
	
	public String getUpload_user() {
		return upload_user;
	}
	public void setUpload_user(String upload_user) {
		this.upload_user = upload_user;
	}
	public Integer getReport_id() {
		return report_id;
	}
	public void setReport_id(Integer report_id) {
		this.report_id = report_id;
	}
	public Integer getReport_fk_id() {
		return report_fk_id;
	}
	public void setReport_fk_id(Integer report_fk_id) {
		this.report_fk_id = report_fk_id;
	}
	public String getReport_type() {
		return report_type;
	}
	public void setReport_type(String report_type) {
		this.report_type = report_type;
	}
	public String getFile_path() {
		return file_path;
	}
	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}
	public Date getUpload_date() {
		return upload_date;
	}
	public void setUpload_date(Date upload_date) {
		this.upload_date = upload_date;
	}
	public Date getDelete_date() {
		return delete_date;
	}
	public void setDelete_date(Date delete_date) {
		this.delete_date = delete_date;
	}
	
}
