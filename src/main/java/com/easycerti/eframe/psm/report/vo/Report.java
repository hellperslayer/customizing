package com.easycerti.eframe.psm.report.vo;

import com.easycerti.eframe.common.vo.AbstractValueObject;

public class Report extends AbstractValueObject {

	private int rank;

	private int rule_cnt;
	private int log_cnt;

	private String emp_user_name;
	private String emp_user_id;
	private String dept_name;
	private String dept_id;

	private int dng_val;
	private String dng_status;

	private String occr_dt;
	private String proc_month;
	

	private int rule_seq;
	private String report_seq;
	private String report_title;
	private String system_seq;
	private String system_name;
	private String rule_nm;
	private String html_encode;
	private String admin_user_id;
	private String log_message_title;
	private String report_type;
	private String file_path;
	private String period_type;
	private String userfile_path;
	private String auth_id;
	
	private String auth_type;
	private String system_report;
	
	private String auth_ids;
	private String make_report_auth;
	
	public String getAuth_type() {
		return auth_type;
	}

	public void setAuth_type(String auth_type) {
		this.auth_type = auth_type;
	}

	public String getAuth_id() {
		return auth_id;
	}

	public void setAuth_id(String auth_id) {
		this.auth_id = auth_id;
	}

	public String getUserfile_path() {
		return userfile_path;
	}

	public void setUserfile_path(String userfile_path) {
		this.userfile_path = userfile_path;
	}

	public String getReport_title() {
		return report_title;
	}

	public void setReport_title(String report_title) {
		this.report_title = report_title;
	}
	
	public String getFile_path() {
		return file_path;
	}

	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}

	public String getReport_type() {
		return report_type;
	}

	public void setReport_type(String report_type) {
		this.report_type = report_type;
	}
	
	public String getProc_month() {
		return proc_month;
	}

	public void setProc_month(String proc_month) {
		this.proc_month = proc_month;
	}

	public String getHtml_encode() {
		return html_encode;
	}

	public void setHtml_encode(String html_encode) {
		this.html_encode = html_encode;
	}

	public String getAdmin_user_id() {
		return admin_user_id;
	}

	public void setAdmin_user_id(String admin_user_id) {
		this.admin_user_id = admin_user_id;
	}

	public String getLog_message_title() {
		return log_message_title;
	}

	public void setLog_message_title(String log_message_title) {
		this.log_message_title = log_message_title;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public int getRule_cnt() {
		return rule_cnt;
	}

	public void setRule_cnt(int rule_cnt) {
		this.rule_cnt = rule_cnt;
	}

	public int getLog_cnt() {
		return log_cnt;
	}

	public void setLog_cnt(int log_cnt) {
		this.log_cnt = log_cnt;
	}

	public String getEmp_user_name() {
		return emp_user_name;
	}

	public void setEmp_user_name(String emp_user_name) {
		this.emp_user_name = emp_user_name;
	}

	public String getEmp_user_id() {
		return emp_user_id;
	}

	public void setEmp_user_id(String emp_user_id) {
		this.emp_user_id = emp_user_id;
	}

	public String getDept_name() {
		return dept_name;
	}

	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}

	public int getDng_val() {
		return dng_val;
	}

	public void setDng_val(int dng_val) {
		this.dng_val = dng_val;
	}

	public String getDng_status() {
		return dng_status;
	}

	public void setDng_status(String dng_status) {
		this.dng_status = dng_status;
	}

	public String getOccr_dt() {
		return occr_dt;
	}

	public void setOccr_dt(String occr_dt) {
		this.occr_dt = occr_dt;
	}

	public int getRule_seq() {
		return rule_seq;
	}

	public void setRule_seq(int rule_seq) {
		this.rule_seq = rule_seq;
	}

	public String getSystem_seq() {
		return system_seq;
	}

	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}

	public String getSystem_name() {
		return system_name;
	}

	public void setSystem_name(String system_name) {
		this.system_name = system_name;
	}

	public String getDept_id() {
		return dept_id;
	}

	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}

	public String getRule_nm() {
		return rule_nm;
	}


	public void setRule_nm(String rule_nm) {
		this.rule_nm = rule_nm;
	}


	public String getPeriod_type() {
		return period_type;
	}

	public void setPeriod_type(String period_type) {
		this.period_type = period_type;
	}
	

	public String getReport_seq() {
		return report_seq;
	}

	public void setReport_seq(String report_seq) {
		this.report_seq = report_seq;
	}

	public String getSystem_report() {
		return system_report;
	}

	public void setSystem_report(String system_report) {
		this.system_report = system_report;
	}

	public String getAuth_ids() {
		return auth_ids;
	}

	public void setAuth_ids(String auth_ids) {
		this.auth_ids = auth_ids;
	}

	public String getMake_report_auth() {
		return make_report_auth;
	}

	public void setMake_report_auth(String make_report_auth) {
		this.make_report_auth = make_report_auth;
	}
}
