package com.easycerti.eframe.psm.history_management.vo;

import java.sql.Timestamp;

import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 관리자행위이력 VO
 * 
 * @author yjyoo
 * @since 2015. 4. 23.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 23.           yjyoo            최초 생성
 *
 * </pre>
 */

public class ManagerActHist extends AbstractValueObject {

	// 감사로그 ID
	private String log_auth_id;
	
	// 관리자 ID
	private String admin_user_id;
	
	// 관리자 IP 주소
	private String ip_address;
	
	// 로그 시간
	private Timestamp log_datetime;
	
	private String str_log_datetime;
	
	// 메뉴 ID
	private String menu_id;
	
	// 대분류
	private String l_category;
	
	// 중분류
	private String m_category;
	
	// 소분류
	private String s_category;
	
	// 로그 내용
	private String log_message;
	
	// 로그 종류
	private String log_action;
	
	/**
	 * etc
	 */
	// 관리자 이름
	private String admin_user_name;
	
	private String[] log_massage_action;

	
	public String[] getLog_massage_action() {
		return log_massage_action;
	}

	public void setLog_massage_action(String[] log_massage_action) {
		this.log_massage_action = log_massage_action;
	}

	public String getLog_auth_id() {
		return log_auth_id;
	}

	public void setLog_auth_id(String log_auth_id) {
		this.log_auth_id = log_auth_id;
	}

	public String getAdmin_user_id() {
		return admin_user_id;
	}

	public void setAdmin_user_id(String admin_user_id) {
		this.admin_user_id = admin_user_id;
	}

	public String getIp_address() {
		return ip_address;
	}

	public void setIp_address(String ip_address) {
		this.ip_address = ip_address;
	}

	public Timestamp getLog_datetime() {
		return log_datetime;
	}

	public void setLog_datetime(Timestamp log_datetime) {
		this.log_datetime = log_datetime;
	}
	
	public String getStr_log_datetime() {
		return str_log_datetime;
	}

	public void setStr_log_datetime(String str_log_datetime) {
		this.str_log_datetime = str_log_datetime;
	}

	public String getMenu_id() {
		return menu_id;
	}

	public void setMenu_id(String menu_id) {
		this.menu_id = menu_id;
	}

	public String getL_category() {
		return l_category;
	}

	public void setL_category(String l_category) {
		this.l_category = l_category;
	}

	public String getM_category() {
		return m_category;
	}

	public void setM_category(String m_category) {
		this.m_category = m_category;
	}

	public String getS_category() {
		return s_category;
	}

	public void setS_category(String s_category) {
		this.s_category = s_category;
	}

	public String getLog_message() {
		return log_message;
	}

	public void setLog_message(String log_message) {
		this.log_message = log_message;
	}

	public String getLog_action() {
		return log_action;
	}

	public void setLog_action(String log_action) {
		this.log_action = log_action;
	}

	public String getAdmin_user_name() {
		return admin_user_name;
	}

	public void setAdmin_user_name(String admin_user_name) {
		this.admin_user_name = admin_user_name;
	}

	// toString
	@Override
	public String toString() {
		return "ManagerActHist [log_auth_id=" + log_auth_id + ", admin_user_id="
				+ admin_user_id + ", ip_address=" + ip_address
				+ ", log_datetime=" + log_datetime + ", menu_id=" + menu_id
				+ ", l_category=" + l_category + ", m_category=" + m_category
				+ ", s_category=" + s_category + ", log_message=" + log_message
				+ ", log_action=" + log_action + ", admin_user_name="
				+ admin_user_name + "]";
	}
}
