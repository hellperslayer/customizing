package com.easycerti.eframe.psm.history_management.vo;

import java.util.Date;

import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 에이전트동작정지이력 VO
 * 
 * @author yjyoo
 * @since 2015. 4. 23.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 23.           yjyoo            최초 생성
 *
 * </pre>
 */

public class AgentMasterHist extends AbstractValueObject {
	
	// 에이전트 동작 ID
	private String action_log_id;

	// 에이전트 시퀀스
	private String agent_seq;
	
	// 서버 시퀀스
	private String server_seq;
	
	// 동작 시간
	private Date command_datetime;
	
	// 동작 종류 (START / STOP)
	private String action;
	
	// 동작 메시지 (성공 / 실패)
	private String message;

	
	public String getAction_log_id() {
		return action_log_id;
	}

	public void setAction_log_id(String action_log_id) {
		this.action_log_id = action_log_id;
	}

	public String getAgent_seq() {
		return agent_seq;
	}

	public void setAgent_seq(String agent_seq) {
		this.agent_seq = agent_seq;
	}

	public String getServer_seq() {
		return server_seq;
	}

	public void setServer_seq(String server_seq) {
		this.server_seq = server_seq;
	}

	public Date getCommand_datetime() {
		return command_datetime;
	}

	public void setCommand_datetime(Date command_datetime) {
		this.command_datetime = command_datetime;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
