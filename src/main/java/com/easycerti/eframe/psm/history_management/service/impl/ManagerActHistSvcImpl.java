package com.easycerti.eframe.psm.history_management.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Repository;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.history_management.dao.ManagerActHistDao;
import com.easycerti.eframe.psm.history_management.service.ManagerActHistSvc;
import com.easycerti.eframe.psm.history_management.vo.ManagerActHist;
import com.easycerti.eframe.psm.history_management.vo.ManagerActHistSearch;
import com.easycerti.eframe.psm.system_management.dao.MenuMngtDao;
import com.easycerti.eframe.psm.system_management.vo.Menu;

/**
 * 관리자행위이력 관련 Service Implements
 * 
 * @author yjyoo
 * @since 2015. 4. 23.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 23.           yjyoo            최초 생성
 *
 * </pre>
 */
@Repository
public class ManagerActHistSvcImpl implements ManagerActHistSvc {
	
	@Autowired
	private ManagerActHistDao managerActHistDao;
	
	@Autowired
	private MenuMngtDao menuMngtDao;
	
	@Autowired
	private CommonDao commonDao;
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;
	

	@Override
	public DataModelAndView findManagerActHistList(ManagerActHistSearch search) {
		int pageSize = Integer.valueOf(defaultPageSize);
		
		search.setSize(pageSize);
		search.setTotal_count(managerActHistDao.findManagerActHistOne_count(search));
		
		List<ManagerActHist> managerActHistList = managerActHistDao.findManagerActHistList(search);
		
		SimpleCode simpleCode = new SimpleCode("/managerActHist/list.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("managerActHistList", managerActHistList);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	@Override
	public void findManagerActHistList_download(DataModelAndView modelAndView, ManagerActHistSearch search, HttpServletRequest request) {
		String fileName = "PSM_감사이력_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "감사이력";
		
		search.setUseExcel("true");
		List<ManagerActHist> managerActHists = managerActHistDao.findManagerActHistList(search);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		for(ManagerActHist managerActHist : managerActHists) {
			String category = "";
			if(managerActHist.getS_category() != null && !managerActHist.getS_category().equals("")) {
				category = managerActHist.getL_category() + " > " + managerActHist.getM_category() + " > " + managerActHist.getS_category();
			}
			else if(managerActHist.getL_category() != null && !managerActHist.getL_category().equals("")) {
				if(managerActHist.getS_category() == null || managerActHist.getS_category().equals("")) {
					category = managerActHist.getL_category() + " > " + managerActHist.getM_category();
				}
			}
			managerActHist.setL_category(category);
			managerActHist.setStr_log_datetime(sdf.format(managerActHist.getLog_datetime()));
		}
		
		String[] columns = new String[] {"admin_user_id", "admin_user_name", "ip_address", "str_log_datetime", "l_category", "log_message", "log_action"};
		String[] heads = new String[] {"관리자ID", "관리자명", "IP", "일시", "분류", "로그메세지", "로그종류"};

		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", managerActHists);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}

	@Override
	public DataModelAndView findManagerActHistOne(String log_auth_id) {
		
		ManagerActHist managerActHistDetail = managerActHistDao.findManagerActHistOne(log_auth_id);
		
		String[] array = managerActHistDetail.getLog_message().split("/");
		managerActHistDetail.setLog_massage_action(array);
		
		SimpleCode simpleCode = new SimpleCode("/managerActHist/detail.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("managerActHistDetail", managerActHistDetail);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView leaveManagerActHistOne(Map<String, String> parameters) {
		ManagerActHist paramBean = CommonHelper.convertMapToBean(parameters, ManagerActHist.class);
		
		Menu menuParamBean = new Menu();
		menuParamBean.setMenu_id(paramBean.getMenu_id());
		List<Menu> menuList = menuMngtDao.findMenuMngtList_fullPathInfo(menuParamBean);
		
		try {
			paramBean.setLog_auth_id(CommonHelper.getGUID());
			
			// 넘어온 menu_id가 login일때 로그인
			if(paramBean.getMenu_id().equals("login")) {
				paramBean.setL_category("로그인");
			}
			// 넘어온 menu_id가 logout일때 로그아웃
			else if(paramBean.getMenu_id().equals("logout")) {
				paramBean.setL_category("로그아웃");
			}
			// DEPTH : 2 (리스트)
			else if(menuList.size() == 2) {
				paramBean.setL_category(menuList.get(0).getMenu_name());
				paramBean.setM_category(menuList.get(1).getMenu_name());
			}
			// DEPTH : 3 (상세)
			else if(menuList.size() == 3) {
				paramBean.setL_category(menuList.get(0).getMenu_name());
				paramBean.setM_category(menuList.get(1).getMenu_name());
				paramBean.setS_category(menuList.get(2).getMenu_name());
			}
			
			managerActHistDao.leaveManagerActHistOne(paramBean);
		} catch(DataAccessException e) {
		}

		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}

}
