package com.easycerti.eframe.psm.history_management.dao;

import java.util.List;

import com.easycerti.eframe.psm.history_management.vo.ManagerActHist;
import com.easycerti.eframe.psm.history_management.vo.ManagerActHistSearch;

/**
 * 관리자행위이력 관련 Dao Interface
 * 
 * @author yjyoo
 * @since 2015. 4. 23.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 23.           yjyoo            최초 생성
 *
 * </pre>
 */
public interface ManagerActHistDao {
	
	/**
	 * 관리자행위이력 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 23.
	 * @return List<ManagerActHist>
	 */
	public List<ManagerActHist> findManagerActHistList(ManagerActHistSearch managerActHist);

	/**
	 * 관리자행위이력 리스트 갯수
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 23.
	 * @return Integer
	 */
	public Integer findManagerActHistOne_count(ManagerActHistSearch managerActHist);
	
	/**
	 * 관리자행위이력 상세정보
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 23.
	 * @return ManagerActHist
	 */
	public ManagerActHist findManagerActHistOne(String log_auth_id);
	
	/**
	 * 관리자행위이력 추가
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 23.
	 * @return void
	 */
	public void leaveManagerActHistOne(ManagerActHist managerActHist);
	
}
