package com.easycerti.eframe.psm.history_management.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.history_management.vo.ManagerActHistSearch;

/**
 * 관리자행위이력 관련 Service Interface
 * 
 * @author yjyoo
 * @since 2015. 4. 23.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 23.           yjyoo            최초 생성
 *   2015. 5. 26.           yjyoo            엑셀 다운로드 추가
 *
 * </pre>
 */
public interface ManagerActHistSvc {
	
	/**
	 * 관리자행위이력 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 23.
	 * @return DataModelAndView
	 */
	public DataModelAndView findManagerActHistList(ManagerActHistSearch search);
	
	/**
	 * 관리자행위이력 리스트 엑셀 다운로드
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 26.
	 * @return void
	 */
	public void findManagerActHistList_download(DataModelAndView modelAndView, ManagerActHistSearch search, HttpServletRequest request);
	
	/**
	 * 관리자행위이력 상세정보
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 23.
	 * @return DataModelAndView
	 */
	public DataModelAndView findManagerActHistOne(String log_auth_id);
	
	/**
	 * 관리자행위이력 추가
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 23.
	 * @return DataModelAndView
	 */
	public DataModelAndView leaveManagerActHistOne(Map<String, String> parameters);
	
}
