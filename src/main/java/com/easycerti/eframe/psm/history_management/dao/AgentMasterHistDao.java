package com.easycerti.eframe.psm.history_management.dao;

import java.util.List;

import com.easycerti.eframe.psm.history_management.vo.AgentMasterHist;

/**
 * 에이전트동작정지이력 관련 Dao Interface
 * 
 * @author yjyoo
 * @since 2015. 4. 29.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 29.           yjyoo            최초 생성
 *
 * </pre>
 */
public interface AgentMasterHistDao {
	
	/**
	 * 에이전트동작정지이력 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 29.
	 * @return List<AgentMasterHist>
	 */
	public List<AgentMasterHist> findAgentMasterHistList(AgentMasterHist agentMasterHist);

	/**
	 * 에이전트동작정지이력 리스트 갯수
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 29.
	 * @return Integer
	 */
	public Integer findAgentMasterHistOne_count(AgentMasterHist agentMasterHist);
	
	/**
	 * 에이전트동작정지이력 추가
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 29.
	 * @return void
	 */
	public void addAgentMasterHistOne(AgentMasterHist agentMasterHist);
	
}
