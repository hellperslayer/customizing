package com.easycerti.eframe.psm.history_management.service;

import java.util.Map;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.history_management.vo.AgentMasterHist;

/**
 * 에이전트동작정지이력 관련 Service Interface
 * 
 * @author yjyoo
 * @since 2015. 4. 29.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 29.           yjyoo            최초 생성
 *
 * </pre>
 */
public interface AgentMasterHistSvc {
	
	/**
	 * 에이전트동작정지이력 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 29.
	 * @return DataModelAndView
	 */
	public DataModelAndView findAgentMasterHistList(Map<String, String> parameters);
	
	/**
	 * 에이전트동작정지이력 추가
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 29.
	 * @return DataModelAndView
	 */
	public DataModelAndView addAgentMasterHistOne(AgentMasterHist agentMasterHist);
	
}
