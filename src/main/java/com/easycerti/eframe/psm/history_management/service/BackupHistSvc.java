package com.easycerti.eframe.psm.history_management.service;

import javax.servlet.http.HttpServletRequest;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.history_management.vo.BackupHist;
import com.easycerti.eframe.psm.history_management.vo.BackupHistSearch;

/**
 * 백업이력 관련 Service Interface
 * 
 * @author yjyoo
 * @since 2015. 4. 23.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 23.           yjyoo            최초 생성
 *
 * </pre>
 */
public interface BackupHistSvc {
	
	/**
	 * 백업이력 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 23.
	 * @return DataModelAndView
	 */
	public DataModelAndView findBackupHistList(BackupHistSearch search);

	/**
	 * 백업이력 리스트 엑셀 다운로드
	 * @author yjyoo
	 * @since 2015. 6. 12.
	 * @return DataModelAndView
	 */
	public DataModelAndView findBackupHistList_download(DataModelAndView modelAndView, BackupHistSearch search, HttpServletRequest request);
	
	/**
	 * 백업이력 상세정보
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 23.
	 * @return DataModelAndView
	 */
	public DataModelAndView findBackupHistOne(String backup_log_id);
	
	public String checkIntegrity(BackupHistSearch search);
	public String restore(BackupHistSearch search);

	public String checkFile(BackupHistSearch search);
	public DataModelAndView checkLogCount(BackupHist backupHist);
}
