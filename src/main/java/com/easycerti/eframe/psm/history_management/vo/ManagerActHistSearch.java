package com.easycerti.eframe.psm.history_management.vo;


import com.easycerti.eframe.common.vo.SearchBase;

/**
 * 관리자행위이력 검색 VO
 * 
 * @author yjyoo
 * @since 2015. 6. 11.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 6. 11.           yjyoo            최초 생성
 *
 * </pre>
 */
public class ManagerActHistSearch extends SearchBase {
	
	//로그메세지
	private String log_message;
	
	// 관리자 ID
	private String admin_user_id_1;
	
	// 관리자 이름
	private String admin_user_name;
	
	public String getAdmin_user_id_1() {
		return admin_user_id_1;
	}

	public void setAdmin_user_id_1(String admin_user_id_1) {
		this.admin_user_id_1 = admin_user_id_1;
	}

	public String getAdmin_user_name() {
		return admin_user_name;
	}

	public void setAdmin_user_name(String admin_user_name) {
		this.admin_user_name = admin_user_name;
	}

	@Override
	public String toString() {
		return "AdminUserSearch [admin_user_id_1=" + admin_user_id_1
				+ ", admin_user_name=" + admin_user_name + "]";
	}

	public String getLog_message() {
		return log_message;
	}

	public void setLog_message(String log_message) {
		this.log_message = log_message;
	}
	
	
}
