package com.easycerti.eframe.psm.history_management.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.history_management.service.BackupHistSvc;
import com.easycerti.eframe.psm.history_management.vo.BackupHist;
import com.easycerti.eframe.psm.history_management.vo.BackupHistSearch;

/**
 * 백업이력 관련 Controller 
 * 
 * @author yjyoo
 * @since 2015. 4. 23.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 23.           yjyoo            최초 생성
 *
 * </pre>
 */
@Controller
@RequestMapping("/backupHist/*")
public class BackupHistCtrl {
	
	@Autowired
	private BackupHistSvc backupHistSvc;
	
	/**
	 * 백업이력 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 23.
	 * @return DataModelAndView
	 */
	
	@MngtActHist(log_action = "SELECT", log_message = "[접속기록] 백업이력조회")
	@RequestMapping(value={"list.html"}, method={RequestMethod.POST})
	public DataModelAndView findBackupHistList(@ModelAttribute("search") BackupHistSearch search, HttpServletRequest request) {
		search.initDatesWithDefaultIfEmptyByMonth(1);
		
		DataModelAndView modelAndView = backupHistSvc.findBackupHistList(search);
		
		modelAndView.addObject("search", search);
		modelAndView.setViewName("backupHistList");
		
		return modelAndView;
	}
	
	@RequestMapping(value="download.html",method={RequestMethod.POST})
	public DataModelAndView addAdminUserMngtList_download(@ModelAttribute("search") BackupHistSearch search, @RequestParam Map<String, String> parameters, HttpServletRequest request){
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);

		backupHistSvc.findBackupHistList_download(modelAndView, search, request);
		
		return modelAndView;
	}
	
	@MngtActHist(log_action = "SELECT", log_message = "[백업이력조회] 위변조 확인", l_category = "접속기록 > 백업이력조회")
	@RequestMapping(value="checkIntegrity.html",method={RequestMethod.POST})
	public DataModelAndView checkIntegrity(@ModelAttribute("search") BackupHistSearch search, @RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
	
		DataModelAndView modelAndView = new DataModelAndView();
		String result = backupHistSvc.checkIntegrity(search);
		
		modelAndView.addObject("result", result);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@RequestMapping(value="restore.html",method={RequestMethod.POST})
	public DataModelAndView restore(@ModelAttribute("search") BackupHistSearch search, @RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
	
		DataModelAndView modelAndView = new DataModelAndView();
		String restore_result = backupHistSvc.restore(search);
		
		modelAndView.addObject("restore_result", restore_result);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}

	/**
	 * 백업파일 유무 확인을 위해 대상파일 바이너리 데이터 일부 표기
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[백업이력조회] 암호화 확인", l_category = "접속기록 > 백업이력조회")
	@RequestMapping(value="checkFile.html",method={RequestMethod.POST})
	public DataModelAndView checkFile(@ModelAttribute("search") BackupHistSearch search, @RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
	
		DataModelAndView modelAndView = new DataModelAndView();
		String result = backupHistSvc.checkFile(search);
		
		modelAndView.addObject("result", result);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@MngtActHist(log_action = "SELECT", log_message = "[백업이력조회] 상세보기", l_category = "접속기록 > 백업이력조회")
	@RequestMapping(value={"checkLogCount.html"}, method={RequestMethod.POST})
	public DataModelAndView checkLogCount(String log_type, String proc_date) {
		BackupHist backupHist = new BackupHist();
		backupHist.setLog_type(log_type);
		backupHist.setBackup_datetime_str(proc_date);
		DataModelAndView modelAndView = backupHistSvc.checkLogCount(backupHist);
		modelAndView.setViewName("backupHistCount");
		return modelAndView;
	}
}
