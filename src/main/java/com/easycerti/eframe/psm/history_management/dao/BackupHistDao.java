package com.easycerti.eframe.psm.history_management.dao;

import java.util.List;

import com.easycerti.eframe.psm.history_management.vo.BackupHist;
import com.easycerti.eframe.psm.history_management.vo.BackupHistSearch;
import com.easycerti.eframe.psm.search.vo.AllLogInq;

/**
 * 백업이력 관련 Dao Interface
 * 
 * @author yjyoo
 * @since 2015. 4. 23.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 23.           yjyoo            최초 생성
 *
 * </pre>
 */
public interface BackupHistDao {
	
	/**
	 * 백업이력 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 23.
	 * @return List<BackupHist>
	 */
	public List<BackupHist> findBackupHistList(BackupHistSearch backupHist);

	/**
	 * 백업이력 리스트 갯수
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 23.
	 * @return Integer
	 */
	public Integer findBackupHistOne_count(BackupHistSearch backupHist);
	
	/**
	 * 백업이력 상세정보
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 23.
	 * @return BackupHist
	 */
	public BackupHist findBackupHistOne(String backup_log_id);
	
	/**
	 * 백업주기
	 * 
	 *  @author sysong
	 *  @since 2019. 09. 06.
	 *  @return String
	 */
	public String findBackupDate();
	
	public void restoreHashcode(BackupHist backupHist);
	
	public List<AllLogInq> backUpDetailCtList(BackupHist backupHist);
	public List<String> systemNameList();
}
