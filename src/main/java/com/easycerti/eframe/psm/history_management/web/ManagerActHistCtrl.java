package com.easycerti.eframe.psm.history_management.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.psm.history_management.service.ManagerActHistSvc;
import com.easycerti.eframe.psm.history_management.vo.ManagerActHistSearch;

/**
 * 관리자행위이력 관련 Controller 
 * 
 * @author yjyoo
 * @since 2015. 4. 23.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 23.           yjyoo            최초 생성
 *   2015. 5. 26.           yjyoo            엑셀 다운로드 추가
 *
 * </pre>
 */
@Controller
@RequestMapping("/managerActHist/*")
public class ManagerActHistCtrl {
	
	@Autowired
	private ManagerActHistSvc managerActHistSvc;
	
	/**
	 * 관리자행위이력 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 23.
	 * @return DataModelAndView
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[환경관리] 감사이력")
	@RequestMapping(value={"list.html"}, method={RequestMethod.POST})
	public DataModelAndView findManagerActHistList(@ModelAttribute("search") ManagerActHistSearch search, @RequestParam Map<String, String> parameters, HttpServletRequest request) {
		search.initDatesWithDefaultIfEmpty(1);
		
		DataModelAndView modelAndView = managerActHistSvc.findManagerActHistList(search);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("managerActHistList");
		
		return modelAndView;
	}
	
	/**
	 * 관리자행위이력 상세
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 23.
	 * @return DataModelAndView
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[환경관리] 감사이력 상세")
	@RequestMapping(value={"detail.html"}, method={RequestMethod.POST})
	public DataModelAndView findManagerActHistDetail(@ModelAttribute("search") ManagerActHistSearch search, @RequestParam Map<String, String> parameters, HttpServletRequest request) {
		String log_auth_id = parameters.get("log_auth_id");
		
		DataModelAndView modelAndView = managerActHistSvc.findManagerActHistOne(log_auth_id);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("managerActHistDetail");
		
		return modelAndView;
	}
	
	/**
	 * 관리자행위이력 리스트 엑셀 다운로드
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 26.
	 * @return DataModelAndView
	 */
	@RequestMapping(value={"download.html"}, method={RequestMethod.POST})
	public DataModelAndView addManagerActHistList_download(@ModelAttribute("search") ManagerActHistSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		managerActHistSvc.findManagerActHistList_download(modelAndView, search, request);
		
		return modelAndView;
	}

	/**
	 * 관리자행위이력 추가
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 23.
	 * @return void
	 */
	@RequestMapping(value={"add.html"}, method={RequestMethod.POST})
	public void leaveManagerActHistOne(@RequestParam Map<String, String> parameters, HttpServletRequest request, HttpServletResponse response) throws Exception {

		if(parameters.get("admin_user_id_temp") != null) // 로그아웃시
			parameters.put("admin_user_id", parameters.get("admin_user_id_temp"));
		else
			parameters.put("admin_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		parameters.put("ip_address", request.getRemoteAddr());
		
		managerActHistSvc.leaveManagerActHistOne(parameters);
	}
}
