package com.easycerti.eframe.psm.history_management.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Repository;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.psm.history_management.dao.AgentMasterHistDao;
import com.easycerti.eframe.psm.history_management.service.AgentMasterHistSvc;
import com.easycerti.eframe.psm.history_management.vo.AgentMasterHist;
import com.easycerti.eframe.psm.history_management.vo.AgentMasterHistList;

/**
 * 에이전트동작정지이력 관련 Service Implements
 * 
 * @author yjyoo
 * @since 2015. 4. 29.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 29.           yjyoo            최초 생성
 *
 * </pre>
 */
@Repository
public class AgentMasterHistSvcImpl implements AgentMasterHistSvc {
	
	@Autowired
	private AgentMasterHistDao agentMasterHistDao;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;
	

	@Override
	public DataModelAndView findAgentMasterHistList(Map<String, String> parameters) {
		parameters = CommonHelper.setPageParam(parameters, defaultPageSize);

		AgentMasterHist paramBean = CommonHelper.convertMapToBean(parameters, AgentMasterHist.class);
		List<AgentMasterHist> agentMasterHists = agentMasterHistDao.findAgentMasterHistList(paramBean);
		Integer totalCount = agentMasterHistDao.findAgentMasterHistOne_count(paramBean);
		
		AgentMasterHistList agentMasterHistListBean = new AgentMasterHistList(agentMasterHists, totalCount.toString());
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("agentMasterHistList", agentMasterHistListBean);
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView addAgentMasterHistOne(AgentMasterHist agentMasterHist) {
		agentMasterHistDao.addAgentMasterHistOne(agentMasterHist);

		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", agentMasterHist);
		
		return modelAndView;
	}
}
