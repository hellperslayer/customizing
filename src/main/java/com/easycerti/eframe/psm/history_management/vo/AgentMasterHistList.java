package com.easycerti.eframe.psm.history_management.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 에이전트동작정지이력 리스트 VO
 * 
 * @author yjyoo
 * @since 2015. 4. 23.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 23.           yjyoo            최초 생성
 *
 * </pre>
 */
public class AgentMasterHistList extends AbstractValueObject {

	// 에이전트동작정지이력 목록
	private List<AgentMasterHist> agentMasterHists = null;

	
	public AgentMasterHistList() {}
	
	public AgentMasterHistList(List<AgentMasterHist> agentMasterHists){
		this.agentMasterHists = agentMasterHists;
	}
	
	public AgentMasterHistList(List<AgentMasterHist> agentMasterHists, String page_total_count){
		this.agentMasterHists = agentMasterHists;
		setPage_total_count(page_total_count);
	}

	public List<AgentMasterHist> getAgentMasterHists() {
		return agentMasterHists;
	}

	public void setAgentMastertHists(List<AgentMasterHist> agentMasterHists) {
		this.agentMasterHists = agentMasterHists;
	}
}
