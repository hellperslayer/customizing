package com.easycerti.eframe.psm.history_management.vo;

import java.util.Date;

import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 백업이력 VO
 * 
 * @author yjyoo
 * @since 2015. 4. 23.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 23.           yjyoo            최초 생성
 *
 * </pre>
 */

public class BackupHist extends AbstractValueObject {

	// 감사로그 ID
	private String backup_log_id;
	
	// 백업시간
	private Date backup_datetime;
	private String backup_datetime_str;
	
	// 백업에이전트 ID
	private String backup_agent_id;
	
	// 백업분류
	private String backup_type;
	
	// 원본데이터형식
	private String origin_data;
	
	// 백업데이터형식
	private String backup_data;
	
	// 파일크기
	private String log_size;
	
	// 시스템시퀀스 (대상서버)
	private String system_seq;

	
	/**
	 * Etc
	 */
	// 백업분류명
	private String backup_type_name;
	
	// 대상서버명
	private String system_name;
	
	// 변경된 hash 값 저장
	private String file_hash;
	
	private String log_type;

	
	public String getLog_type() {
		return log_type;
	}

	public void setLog_type(String log_type) {
		this.log_type = log_type;
	}

	public String getFile_hash() {
		return file_hash;
	}

	public void setFile_hash(String file_hash) {
		this.file_hash = file_hash;
	}

	public String getBackup_log_id() {
		return backup_log_id;
	}

	public void setBackup_log_id(String backup_log_id) {
		this.backup_log_id = backup_log_id;
	}

	public Date getBackup_datetime() {
		return backup_datetime;
	}

	public void setBackup_datetime(Date backup_datetime) {
		this.backup_datetime = backup_datetime;
	}
	
	public String getBackup_datetime_str() {
		return backup_datetime_str;
	}

	public void setBackup_datetime_str(String backup_datetime_str) {
		this.backup_datetime_str = backup_datetime_str;
	}

	public String getBackup_agent_id() {
		return backup_agent_id;
	}

	public void setBackup_agent_id(String backup_agent_id) {
		this.backup_agent_id = backup_agent_id;
	}

	public String getBackup_type() {
		return backup_type;
	}

	public void setBackup_type(String backup_type) {
		this.backup_type = backup_type;
	}

	public String getOrigin_data() {
		return origin_data;
	}

	public void setOrigin_data(String origin_data) {
		this.origin_data = origin_data;
	}

	public String getBackup_data() {
		return backup_data;
	}

	public void setBackup_data(String backup_data) {
		this.backup_data = backup_data;
	}

	public String getLog_size() {
		return log_size;
	}

	public void setLog_size(String log_size) {
		this.log_size = log_size;
	}

	public String getSystem_seq() {
		return system_seq;
	}

	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}

	public String getBackup_type_name() {
		return backup_type_name;
	}

	public void setBackup_type_name(String backup_type_name) {
		this.backup_type_name = backup_type_name;
	}

	public String getSystem_name() {
		return system_name;
	}

	public void setSystem_name(String system_name) {
		this.system_name = system_name;
	}
}
