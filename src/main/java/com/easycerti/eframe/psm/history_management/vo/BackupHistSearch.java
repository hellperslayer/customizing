package com.easycerti.eframe.psm.history_management.vo;

import com.easycerti.eframe.common.vo.SearchBase;

/**
 * 백업이력 검색 VO
 * 
 * @author yjyoo
 * @since 2015. 6. 12.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 6. 12.           yjyoo            최초 생성
 *
 * </pre>
 */

public class BackupHistSearch extends SearchBase {

	// 백업분류
	private String backup_type;
	private String backup_log_id;
	private String backup_data;
	
	private String main_menu_id;
	private String sub_menu_id;
	private String system_seq;
	private String menuId;
	private String isSearch;
	
	private String log_type;
	
	public String getLog_type() {
		return log_type;
	}

	public void setLog_type(String log_type) {
		this.log_type = log_type;
	}

	public String getBackup_type() {
		return backup_type;
	}

	public void setBackup_type(String backup_type) {
		this.backup_type = backup_type;
	}

	public String getBackup_log_id() {
		return backup_log_id;
	}

	public void setBackup_log_id(String backup_log_id) {
		this.backup_log_id = backup_log_id;
	}

	public String getBackup_data() {
		return backup_data;
	}

	public void setBackup_data(String backup_data) {
		this.backup_data = backup_data;
	}

	public String getMain_menu_id() {
		return main_menu_id;
	}

	public void setMain_menu_id(String main_menu_id) {
		this.main_menu_id = main_menu_id;
	}

	public String getSub_menu_id() {
		return sub_menu_id;
	}

	public void setSub_menu_id(String sub_menu_id) {
		this.sub_menu_id = sub_menu_id;
	}

	public String getSystem_seq() {
		return system_seq;
	}

	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}

	public String getMenuId() {
		return menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public String getIsSearch() {
		return isSearch;
	}

	public void setIsSearch(String isSearch) {
		this.isSearch = isSearch;
	}
	
	
}
