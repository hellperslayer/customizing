package com.easycerti.eframe.psm.history_management.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.GetSHA256;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.history_management.dao.BackupHistDao;
import com.easycerti.eframe.psm.history_management.service.BackupHistSvc;
import com.easycerti.eframe.psm.history_management.vo.BackupHist;
import com.easycerti.eframe.psm.history_management.vo.BackupHistSearch;
import com.easycerti.eframe.psm.search.vo.AllLogInq;

/**
 * 백업이력 관련 Service Implements
 * 
 * @author yjyoo
 * @since 2015. 4. 23.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 23.           yjyoo            최초 생성
 *
 * </pre>
 */
@Repository
public class BackupHistSvcImpl implements BackupHistSvc {
	
	@Autowired
	private BackupHistDao backupHistDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;
	
	@Value("#{configProperties.backupHistPath}")
	private String backupHist_path;

	@Override
	public DataModelAndView findBackupHistList(BackupHistSearch search) {
		int pageSize = Integer.valueOf(defaultPageSize);
		
		search.setSize(pageSize);
		search.setTotal_count(backupHistDao.findBackupHistOne_count(search));
		List<BackupHist> backupHistList = backupHistDao.findBackupHistList(search);
		String backupDate = backupHistDao.findBackupDate();
		
		SimpleCode simpleCode = new SimpleCode("/backupHist/list.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("backupHistList", backupHistList);
		modelAndView.addObject("backupDate", backupDate);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView findBackupHistList_download(DataModelAndView modelAndView, BackupHistSearch search, HttpServletRequest request) {
		String fileName = "PSM_백업이력_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "백업이력";

		search.setUseExcel("true");
		List<BackupHist> backupHists = backupHistDao.findBackupHistList(search);
		for(int i=0; i<backupHists.size(); i++) {
			BackupHist bh = backupHists.get(i);
			Date date = bh.getBackup_datetime();
			String strDate = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss").format(date);
			bh.setBackup_datetime_str(strDate);
			/*String log_type = bh.getLog_type();
			if(log_type.equals("BA")) {
				bh.setLog_type("접속기록");
			} else {
				bh.setLog_type("다운로드접속기록");
			}*/
		}
		String[] columns = new String[] {"backup_datetime_str", "backup_type_name", "origin_data", "log_size"};
		String[] heads = new String[] {"일시", "분류", "로그", "파일크기"};

		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", backupHists);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
		
		return null;
	}

	@Override
	public DataModelAndView findBackupHistOne(String backup_log_id) {
		return null;
	}
	
	@Override
	public String checkIntegrity(BackupHistSearch search) {
		
		BackupHist backupHist = backupHistDao.findBackupHistOne(search.getBackup_log_id());
		if (backupHist != null) {
			String filepath = backupHist.getBackup_data();
			File file = new File(filepath);
			if( !file.exists() ) {
				System.out.println("FILE NOT EXISTS!!, CHECK FILE : "+filepath);
				return "NO_FILE";
			}
			
			String file_hash = null;
			try {
				file_hash = GetSHA256.getHashcode(filepath);
			} catch (NoSuchAlgorithmException e) {
				System.out.println("GET FILE HASH ERROR!!!");
				e.printStackTrace();
				return "HASH_ERROR";
			}
			
			if (backupHistDao.findBackupHistOne(file_hash) == null) {
				return "NO_HASH";
			}
			
			return "OK";
			
		} else {
			return "NO_DATA";
		}
	}
	
	@Override
	public String restore(BackupHistSearch search) {
		
		BackupHist backupHist = backupHistDao.findBackupHistOne(search.getBackup_log_id());
		if (backupHist != null) {
			String filepath = backupHist.getBackup_data(); // 기존 파일 경로
			File file = new File(filepath);
			
			String fileDirectory = file.getParentFile().toString(); // 파일명 제거, 디렉토리 가져오기
			String exHashFile = "";
			
			for (File info : new File(fileDirectory).listFiles()) {
				if (info.isFile()) {
					exHashFile = info.getName();
				}
			}
			
			String realFilepath = backupHist_path + "/" + exHashFile;
			
			try {
				FileInputStream fis = new FileInputStream(realFilepath); // 복사할 파일
				FileOutputStream fos = new FileOutputStream(filepath); // 기존 파일
				
				FileChannel fcin = fis.getChannel();
				FileChannel fcout = fos.getChannel();
				
				long size = 0;
				size = fcin.size();
				
				fcin.transferTo(0, size, fcout);
				
				fcout.close();
				fcin.close();

			}  catch (IOException e) {
				e.printStackTrace();
				return "HASH_ERROR";
			}
			
			return "OK";
			
		} else {
			return "NO_DATA";
		}
	}

	@Override
	public String checkFile(BackupHistSearch search) {
		BackupHist backupHist = backupHistDao.findBackupHistOne(search.getBackup_log_id());
		if (backupHist != null) {
			String filepath = backupHist.getBackup_data(); // 기존 파일 경로
			File file = new File(filepath);
			if( !file.exists() ) {
				System.out.println("FILE NOT EXISTS!!, CHECK FILE : "+filepath);
				return "NO_FILE";
			}
			try {
				FileInputStream fis = new FileInputStream(file);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				byte[] buf = new byte[64];
				int len = 0;
				/*
				 * while ((len = fis.read(buf)) != -1) { baos.write(buf,0,len); }
				 */
				len = fis.read(buf);
				baos.write(buf,0,len);
				byte[] fileArray = baos.toByteArray();
				String out = new String(Base64.getEncoder().encode(fileArray));
				fis.close();
				baos.close();
				return out;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "ERROR";
			}
		} else {
			return "NO_DATA";
		}
	}

	@Override
	public DataModelAndView checkLogCount(BackupHist backupHist) {
		DataModelAndView modelAndView = new DataModelAndView();
		
		/* 추후 안정화 되면 사용
		List<AllLogInq> backUpDetailCtList = backupHistDao.backUpDetailCtList(backupHist);
		modelAndView.addObject("backUpDetailCtList", backUpDetailCtList);*/
		
		List<String> systemName = backupHistDao.systemNameList();
		modelAndView.addObject("systemName", systemName);
		return modelAndView;
	}
	
	
}
