package com.easycerti.eframe.psm.search.vo;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import com.easycerti.eframe.common.util.DateUtil;
import com.easycerti.eframe.common.util.PageInfo;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SearchBase;
/**
 * 
 * 설명 : 로그수집,로그분석 SearchBean
 * @author tjlee
 * @since 2015. 5. 28.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 28.           tjlee            최초 생성
 *
 * </pre>
 */

public class SearchSearch extends SearchBase{

	// 기본적으로 PageInfo는 상속받은 SearchBase 안에 다시 상속을 받아 사용한다.
	
	// 각 페이지에 상세 페이징 객체생성(2dept 이상의 페이징 목록 유지)
	// 전체로그조회 상세
	PageInfo allLogInqDetail = new PageInfo();
	// 추출조건별조회 상세
	PageInfo extrtCondbyInqDetail = new PageInfo();
	// 위험도별조회 상세
	PageInfo empDetailInqDetail = new PageInfo();
	// 접속이력조회 상세
	PageInfo reqLogInqDetail = new PageInfo();
	
	PageInfo sqlPrivacyDetail = new PageInfo();
	
	private long log_seq;
	
	private String logSeqs;

	// 사번
	private String emp_user_id;
	
	private String login_user_id;
	
	private String login_auth_id;
	
	// 사용자명
	private String emp_user_name;
	
	// 부서명
	private String dept_name;
	
	//부서ID
	private String dept_id;
	
	// 기간선택
	private String daySelect = "";

	// 사용자IP
	private String user_ip;
	
	// 개인정보유형
	private String privacyType;
	
	// 수행업무
	private String req_type;

	// 개인정보
	private String privacy;
	
	// 대상서버
	private String system_seq;
	private String system_seq_temp;
	
	// URL
	private String req_url;
	
	// 상세로그번호
	private String detailLogSeq;
	
	// 상세날짜
	private String detailProcDate;
	
	// 추출조건 ( 추출조건별조회용 ) 
	private String rule_cd;
	
	// 위험도 ( 위험도별조회용 )
	private String dng_val;
	
	// 상세empCd 
	private String detailEmpCd;
	
	// 상세OccrDt 
	private String detailOccrDt;
	
	//상세EmpDetailSeq
	private String detailEmpDetailSeq;

	// 토폴로지용
	private String starthm;
	private String endhm;
	
	// 상세 로그 화면 분기를 위해 bbs_id 생성
	private String bbs_id;
	
	// 위험도별조회 페이지 상세 링크 조건 
	private String detailStartDay;
	private String detailEndDay;
	private String detailRuleCd;
	
	private Integer log_cnt;
	
	private String data1;

	private String data2;
	
	private String day_month_year;
	
	private String emp_detail_seq;
	private String summon_code;
	
	List<String> auth_idsList;
	
	private String reqLogInqDetail_user_id;
	
	//정렬 타입 
	private String check_type;
	
	//오름차순, 내림차순 정보 
	private int sort_flag;
	
	private String scen_seq;
	
	private String result_type;
	private String proc_date;
	private String proc_time;
	private String log_gubun;
	private String serverip;
	private String dept_cd;
	private String user_id;
	private String scrn_name;
	private String button_cd;
	private String threadid;
	private String threadid_1;
	private String sellertool;
	// 일, 월, 연도 체크 타입
	private String chk_date_type;
	
	/** 180314 소명판정 결제자 위임 **/
	private String admin_user_id;
	private String auth_ids;
	private String appoval_id;
	private String appoval_name;
	private String delegation_id;
	private String delegation_name;
	private String delegation_date;
	private String system_name;
	private String detect_div_search;
	private String detect_div;
	
	private String year;
	private String month;
	
	private String day;
	private String yearMonth;
	
	private String start_h;
	private String start_m;
	private String end_h;
	private String end_m;
	
	private String start_time;
	private String end_time;
	
	private String biz_log_result_seq;
	private String privacy_seq;
	private String result_content;
	
	private Boolean searchTime;
	
	private String query_id;
	private String rd_name;
	private String grid_name;
	private String request_data;
	private String check_file;
	
	private String ui_type;
	private String resp_body;
	
	private String detailResultType;
	
	//부적정 사용자 리스트 사용 y/n 쓰기 위해서
	private String use_adverse;
	
	private String h_type;
	//재소명 요청내역 여부
	private String resummon_yn;
	
	
	//소명 지연 
	private String summon_late_yn;
	private String file_name;
	private String reason;
	private String result_owner_flag;
	private String result_owner;
	
	private String startdate8;
	private String enddate8;
	
	private String startdate9;
	private String enddate9;
	
	private String emp_user_idD2;
	private String occr_dt;
	
	private String report_type;
	
	//
//	private String main_menu_id;
//	private String sub_menu_id;
	private String menuId;
	
	// 추가로그(ExtraBizLog) 관련
	private String log_type;
	private String is_login_id;
	
	// 추가로그(AuthInfoList) 관련
	private String target_auth;
	private String target_id;	 
	private String target_auth_name;
	
	
	private String threshold;
	private String threshold_use;
	private String threshold_total_inbox;
	
	private String menu_name;
	
	private String searchType;
	
	// 에이전트타입
	private String agent_type;
	private List<String> sysList;
	
	private String detail_summon_status;
	private String summon_status;
	private String detail_decision_status;
	private String detail_decision_status_second;
	private String decision_status;
	private String user_auth;
	private String decision_user_id;
	private String decision_user_name;
	
	private String crud_type;
	private String summonKey;
	
	//최종권한 사용여부
	private String summon_cfm_yn;
	private String log_filter;
	
	public List<String> getSysList() {
		return sysList;
	}
	public void setSysList(List<String> sysList) {
		this.sysList = sysList;
	}
	
	public void setSysList(String[] sysList) {
		this.sysList = new ArrayList<String>();
		for (String string : sysList) {
			this.sysList.add(string);
		}
	}
	
	public String getDecision_user_id() {
		return decision_user_id;
	}
	public void setDecision_user_id(String decision_user_id) {
		this.decision_user_id = decision_user_id;
	}
	public String getDecision_user_name() {
		return decision_user_name;
	}
	public void setDecision_user_name(String decision_user_name) {
		this.decision_user_name = decision_user_name;
	}
	public String getUser_auth() {
		return user_auth;
	}
	public void setUser_auth(String user_auth) {
		this.user_auth = user_auth;
	}
	public String getDetail_summon_status() {
		return detail_summon_status;
	}
	public void setDetail_summon_status(String detail_summon_status) {
		this.detail_summon_status = detail_summon_status;
	}
	public String getSummon_status() {
		return summon_status;
	}
	public void setSummon_status(String summon_status) {
		this.summon_status = summon_status;
	}
	public String getDecision_status() {
		return decision_status;
	}
	public void setDecision_status(String decision_status) {
		this.decision_status = decision_status;
	}
	
	public String getChk_date_type() {
		return chk_date_type;
	}
	public void setChk_date_type(String chk_date_type) {
		this.chk_date_type = chk_date_type;
	}
	public String getAgent_type() {
		return agent_type;
	}
	public void setAgent_type(String agent_type) {
		this.agent_type = agent_type;
	}
	private String logCtType;
	
	private String logSqlPageNum;
	private String logSqlResultPageNum;
	private String tab_flag;
	
	public String getLogSqlResultPageNum() {
		if(logSqlResultPageNum==null) {
			logSqlResultPageNum = "1";
		}
		return logSqlResultPageNum;
	}
	public void setLogSqlResultPageNum(String logSqlResultPageNum) {
		this.logSqlResultPageNum = logSqlResultPageNum;
	}
	public String getTab_flag() {
		if(tab_flag==null||tab_flag=="") {
			tab_flag = "1";
		}
		return tab_flag;
	}
	public void setTab_flag(String tab_flag) {
		this.tab_flag = tab_flag;
	}
	public String getLogSqlPageNum() {
		if(logSqlPageNum==null) {
			logSqlPageNum = "1";
		}
		return logSqlPageNum;
	}
	public void setLogSqlPageNum(String logSqlPageNum) {
		this.logSqlPageNum = logSqlPageNum;
	}
	public String getLogCtType() {
		return logCtType;
	}
	public void setLogCtType(String logCtType) {
		this.logCtType = logCtType;
	}
	public String getSearchType() {
		return searchType;
	}
	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}
	public String getMenu_name() {
		return menu_name;
	}
	public void setMenu_name(String menu_name) {
		this.menu_name = menu_name;
	}
	public String getLog_type() {
		return log_type;
	}
	public void setLog_type(String log_type) {
		this.log_type = log_type;
	}
	public String getIs_login_id() {
		return is_login_id;
	}
	public void setIs_login_id(String is_login_id) {
		this.is_login_id = is_login_id;
	}
	public String getTarget_auth() {
		return target_auth;
	}
	public void setTarget_auth(String target_auth) {
		this.target_auth = target_auth;
	}
	public String getTarget_id() {
		return target_id;
	}
	public void setTarget_id(String target_id) {
		this.target_id = target_id;
	}
	public String getTarget_auth_name() {
		return target_auth_name;
	}
	public void setTarget_auth_name(String target_auth_name) {
		this.target_auth_name = target_auth_name;
	}
	private String log_delimiter;
	
	public String getReport_type() {
		return report_type;
	}
	public void setReport_type(String report_type) {
		this.report_type = report_type;
	}
	public String getOccr_dt() {
		return occr_dt;
	}
	public void setOccr_dt(String occr_dt) {
		this.occr_dt = occr_dt;
	}
	public String getEmp_user_idD2() {
		return emp_user_idD2;
	}
	public void setEmp_user_idD2(String emp_user_idD2) {
		this.emp_user_idD2 = emp_user_idD2;
	}
	public String getStartdate8() {
		return startdate8;
	}
	public void setStartdate8(String startdate8) {
		this.startdate8 = startdate8;
	}
	public String getEnddate8() {
		return enddate8;
	}
	public void setEnddate8(String enddate8) {
		this.enddate8 = enddate8;
	}
	public String getStartdate9() {
		return startdate9;
	}
	public void setStartdate9(String startdate9) {
		this.startdate9 = startdate9;
	}
	public String getEnddate9() {
		return enddate9;
	}
	public void setEnddate9(String enddate9) {
		this.enddate9 = enddate9;
	}
	public String getResult_owner() {
		return result_owner;
	}
	public void setResult_owner(String result_owner) {
		this.result_owner = result_owner;
	}
	public String getResult_owner_flag() {
		return result_owner_flag;
	}
	public void setResult_owner_flag(String result_owner_flag) {
		this.result_owner_flag = result_owner_flag;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}

	//소명 요청일, 답변일, 판정일 타입 (1, 2, 3)
	private String dateType;
	
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public String getDetect_div_search() {
		return detect_div_search;
	}
	public void setDetect_div_search(String detect_div_search) {
		this.detect_div_search = detect_div_search;
	}
	public String getDetect_div() {
		return detect_div;
	}
	public void setDetect_div(String detect_div) {
		this.detect_div = detect_div;
	}
	public String getDetailResultType() {
		return detailResultType;
	}
	public void setDetailResultType(String detailResultType) {
		this.detailResultType = detailResultType;
	}
	public String getResp_body() {
		return resp_body;
	}
	public void setResp_body(String resp_body) {
		this.resp_body = resp_body;
	}
	public String getUi_type() {
		return ui_type;
	}
	public void setUi_type(String ui_type) {
		this.ui_type = ui_type;
	}
	public String getRequest_data() {
		return request_data;
	}
	public void setRequest_data(String request_data) {
		this.request_data = request_data;
	}
	public String getQuery_id() {
		return query_id;
	}
	public void setQuery_id(String query_id) {
		this.query_id = query_id;
	}
	public String getRd_name() {
		return rd_name;
	}
	public void setRd_name(String rd_name) {
		this.rd_name = rd_name;
	}
	public String getGrid_name() {
		return grid_name;
	}
	public void setGrid_name(String grid_name) {
		this.grid_name = grid_name;
	}
	public Boolean getSearchTime() {
		return searchTime;
	}
	public void setSearchTime(Boolean searchTime) {
		this.searchTime = searchTime;
	}
	public String getPrivacy_seq() {
		return privacy_seq;
	}
	public void setPrivacy_seq(String privacy_seq) {
		this.privacy_seq = privacy_seq;
	}
	public String getResult_content() {
		return result_content;
	}
	public void setResult_content(String result_content) {
		this.result_content = result_content;
	}
	public String getBiz_log_result_seq() {
		return biz_log_result_seq;
	}
	public void setBiz_log_result_seq(String biz_log_result_seq) {
		this.biz_log_result_seq = biz_log_result_seq;
	}
	public String getStart_time() {
		return start_time;
	}
	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}
	public String getEnd_time() {
		return end_time;
	}
	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}
	public String getStart_h() {
		return start_h;
	}
	public void setStart_h(String start_h) {
		this.start_h = start_h;
	}
	public String getStart_m() {
		return start_m;
	}
	public void setStart_m(String start_m) {
		this.start_m = start_m;
	}
	public String getEnd_h() {
		return end_h;
	}
	public void setEnd_h(String end_h) {
		this.end_h = end_h;
	}
	public String getEnd_m() {
		return end_m;
	}
	public void setEnd_m(String end_m) {
		this.end_m = end_m;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getYearMonth() {
		return yearMonth;
	}
	public void setYearMonth(String yearMonth) {
		this.yearMonth = yearMonth;
	}
	public String getAdmin_user_id() {
		return admin_user_id;
	}
	public void setAdmin_user_id(String admin_user_id) {
		this.admin_user_id = admin_user_id;
	}
	public String getAuth_ids() {
		return auth_ids;
	}
	public void setAuth_ids(String auth_ids) {
		this.auth_ids = auth_ids;
	}
	public String getAppoval_id() {
		return appoval_id;
	}
	public void setAppoval_id(String appoval_id) {
		this.appoval_id = appoval_id;
	}
	public String getAppoval_name() {
		return appoval_name;
	}
	public void setAppoval_name(String appoval_name) {
		this.appoval_name = appoval_name;
	}
	public String getDelegation_id() {
		return delegation_id;
	}
	public void setDelegation_id(String delegation_id) {
		this.delegation_id = delegation_id;
	}
	public String getDelegation_name() {
		return delegation_name;
	}
	public void setDelegation_name(String delegation_name) {
		this.delegation_name = delegation_name;
	}
	public String getDelegation_date() {
		return delegation_date;
	}
	public void setDelegation_date(String delegation_date) {
		this.delegation_date = delegation_date;
	}
	public String getSystem_name() {
		return system_name;
	}
	public void setSystem_name(String system_name) {
		this.system_name = system_name;
	}
	public String getLog_gubun() {
		return log_gubun;
	}
	public void setLog_gubun(String log_gubun) {
		this.log_gubun = log_gubun;
	}
	
	private String desc_status;	// 소명 진행상태
	private String desc_result;
	private String desc_seq;
	private String summon_req_emp;
	private String deci_nm;
	private String detailLogSqlSeq;
	private String isSearch;
	
	private String search_from_chart;
	private String search_to_chart;
	private String sql;
	
	
	public String getSql() {
		return sql;
	}
	public void setSql(String sql) {
		this.sql = sql;
	}
	public String getSearch_from_chart() {
		return search_from_chart;
	}
	public void setSearch_from_chart(String search_from_chart) {
		this.search_from_chart = search_from_chart;
	}
	
	public String getSearch_to_chart() {
		return search_to_chart;
	}
	public void setSearch_to_chart(String search_to_chart) {
		this.search_to_chart = search_to_chart;
	}
	public String getIsSearch() {
		return isSearch;
	}
	public void setIsSearch(String isSearch) {
		this.isSearch = isSearch;
	}
	public String getLogSeqs() {
		return logSeqs;
	}
	public void setLogSeqs(String logSeqs) {
		this.logSeqs = logSeqs;
	}
	public String getDeci_nm() {
		return deci_nm;
	}
	public void setDeci_nm(String deci_nm) {
		this.deci_nm = deci_nm;
	}
	public String getSummon_req_emp() {
		return summon_req_emp;
	}
	public void setSummon_req_emp(String summon_req_emp) {
		this.summon_req_emp = summon_req_emp;
	}
	public String getDesc_seq() {
		return desc_seq;
	}
	public void setDesc_seq(String desc_seq) {
		this.desc_seq = desc_seq;
	}
	public String getDesc_status() {
		return desc_status;
	}
	public void setDesc_status(String desc_status) {
		this.desc_status = desc_status;
	}
	
	public String getDesc_result() {
		return desc_result;
	}
	public void setDesc_result(String desc_result) {
		this.desc_result = desc_result;
	}
	public List<String> getAuth_idsList() {
		return auth_idsList;
	}
	public void setAuth_idsList(List<String> auth_idsList) {
		this.auth_idsList = auth_idsList;
	}
	
	

	public String getData1() {
		return data1;
	}

	public void setData1(String data1) {
		this.data1 = data1;
	}

	public String getData2() {
		return data2;
	}

	public void setData2(String data2) {
		this.data2 = data2;
	}

	public int getCnt1() {
		return cnt1;
	}

	public void setCnt1(int cnt1) {
		this.cnt1 = cnt1;
	}

	private int cnt1;
	
	public Integer getLog_cnt() {
		return log_cnt;
	}

	public void setLog_cnt(Integer log_cnt) {
		this.log_cnt = log_cnt;
	}

	//년월일 체크
	private String check_times;
	
	//ID 맵핑여부
	private String mapping_id;
	
	public String getMapping_id() {
		return mapping_id;
	}

	public void setMapping_id(String mapping_id) {
		this.mapping_id = mapping_id;
	}

	public long getLog_seq() {
		return log_seq;
	}

	public void setLog_seq(long log_seq) {
		this.log_seq = log_seq;
	}
	public String getCheck_times() {
		return check_times;
	}

	public void setCheck_times(String check_times) {
		this.check_times = check_times;
	}

	public String getDetailEmpCd() {
		return detailEmpCd;
	}

	public void setDetailEmpCd(String detailEmpCd) {
		this.detailEmpCd = detailEmpCd;
	}

	public String getDetailOccrDt() {
		return detailOccrDt;
	}

	public void setDetailOccrDt(String detailOccrDt) {
		this.detailOccrDt = detailOccrDt;
	}

	public String getDetailEmpDetailSeq() {
		return detailEmpDetailSeq;
	}

	public void setDetailEmpDetailSeq(String detailEmpDetailSeq) {
		this.detailEmpDetailSeq = detailEmpDetailSeq;
	}

	public String getStarthm() {
		return starthm;
	}

	public void setStarthm(String starthm) {
		this.starthm = starthm;
	}

	public String getEndhm() {
		return endhm;
	}

	public void setEndhm(String endhm) {
		this.endhm = endhm;
	}

	public String getEmp_user_id() {
		return emp_user_id;
	}

	public void setEmp_user_id(String emp_user_id) {
		this.emp_user_id = emp_user_id;
	}

	public String getEmp_user_name() {
		return emp_user_name;
	}

	public void setEmp_user_name(String emp_user_name) {
		this.emp_user_name = emp_user_name;
	}

	public String getDept_name() {
		return dept_name;
	}

	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}

	public String getDept_id() {
		return dept_id;
	}

	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}
	
	public String getDaySelect() {
		return daySelect;
	}

	public void setDaySelect(String daySelect) {
		this.daySelect = daySelect;
	}

	public String getUser_ip() {
		return user_ip;
	}

	public void setUser_ip(String user_ip) {
		this.user_ip = user_ip;
	}

	public String getPrivacyType() {
		return privacyType;
	}

	public void setPrivacyType(String privacyType) {
		this.privacyType = privacyType;
	}

	public String getPrivacy() {
		return privacy;
	}

	public void setPrivacy(String privacy) {
		this.privacy = privacy;
	}

	public String getSystem_seq() {
		return system_seq;
	}

	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}

	public String getSystem_seq_temp() {
		return system_seq_temp;
	}
	
	public void setSystem_seq_temp(String system_seq_temp) {
		this.system_seq_temp = system_seq_temp;
	}
	
	public String getReq_url() {
		return req_url;
	}

	public void setReq_url(String req_url) {
		this.req_url = req_url;
	}

	public String getDetailLogSeq() {
		return detailLogSeq;
	}

	public void setDetailLogSeq(String detailLogSeq) {
		this.detailLogSeq = detailLogSeq;
	}

	public String getDetailProcDate() {
		return detailProcDate;
	}

	public void setDetailProcDate(String detailProcDate) {
		this.detailProcDate = detailProcDate;
	}

	public String getRule_cd() {
		return rule_cd;
	}

	public void setRule_cd(String rule_cd) {
		this.rule_cd = rule_cd;
	}

	public String getDng_val() {
		return dng_val;
	}

	public void setDng_val(String dng_val) {
		this.dng_val = dng_val;
	}

	public String getBbs_id() {
		return bbs_id;
	}

	public void setBbs_id(String bbs_id) {
		this.bbs_id = bbs_id;
	}
	
	public String getDetailStartDay() {
		return detailStartDay;
	}

	public void setDetailStartDay(String detailStartDay) {
		this.detailStartDay = detailStartDay;
	}

	public String getDetailEndDay() {
		return detailEndDay;
	}

	public void setDetailEndDay(String detailEndDay) {
		this.detailEndDay = detailEndDay;
	}

	public String getDetailRuleCd() {
		return detailRuleCd;
	}

	public void setDetailRuleCd(String detailRuleCd) {
		this.detailRuleCd = detailRuleCd;
	}

	public PageInfo getAllLogInqDetail() {
		return allLogInqDetail;
	}

	public void setAllLogInqDetail(PageInfo allLogInqDetail) {
		this.allLogInqDetail = allLogInqDetail;
	}

	public PageInfo getExtrtCondbyInqDetail() {
		return extrtCondbyInqDetail;
	}

	public void setExtrtCondbyInqDetail(PageInfo extrtCondbyInqDetail) {
		this.extrtCondbyInqDetail = extrtCondbyInqDetail;
	}

	public PageInfo getEmpDetailInqDetail() {
		return empDetailInqDetail;
	}

	public void setEmpDetailInqDetail(PageInfo empDetailInqDetail) {
		this.empDetailInqDetail = empDetailInqDetail;
	}

	public String getDay_month_year() {
		return day_month_year;
	}

	public void setDay_month_year(String day_month_year) {
		this.day_month_year = day_month_year;
	}

	public String getEmp_detail_seq() {
		return emp_detail_seq;
	}

	public void setEmp_detail_seq(String emp_detail_seq) {
		this.emp_detail_seq = emp_detail_seq;
	}
	
	public PageInfo getReqLogInqDetail() {
		return reqLogInqDetail;
	}
	
	public void setReqLogInqDetail(PageInfo reqLogInqDetail) {
		this.reqLogInqDetail = reqLogInqDetail;
	}
	public String getReqLogInqDetail_user_id() {
		return reqLogInqDetail_user_id;
	}
	public void setReqLogInqDetail_user_id(String reqLogInqDetail_user_id) {
		this.reqLogInqDetail_user_id = reqLogInqDetail_user_id;
	}
	public String getReq_type() {
		return req_type;
	}
	public void setReq_type(String req_type) {
		this.req_type = req_type;
	}
	public String getCheck_type() {
		return check_type;
	}
	public void setCheck_type(String check_type) {
		this.check_type = check_type;
	}
	public int getSort_flag() {
		return sort_flag;
	}
	public void setSort_flag(int sort_flag) {
		this.sort_flag = sort_flag;
	}
	public String getScen_seq() {
		return scen_seq;
	}
	public void setScen_seq(String scen_seq) {
		this.scen_seq = scen_seq;
	}
	public String getResult_type() {
		return result_type;
	}
	public void setResult_type(String result_type) {
		this.result_type = result_type;
	}
	public String getProc_date() {
		return proc_date;
	}
	public void setProc_date(String proc_date) {
		this.proc_date = proc_date;
	}
	public String getProc_time() {
		return proc_time;
	}
	public void setProc_time(String proc_time) {
		this.proc_time = proc_time;
	}
	public String getServerip() {
		return serverip;
	}
	public void setServerip(String serverip) {
		this.serverip = serverip;
	}
	public String getDept_cd() {
		return dept_cd;
	}
	public void setDept_cd(String dept_cd) {
		this.dept_cd = dept_cd;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getScrn_name() {
		return scrn_name;
	}
	public void setScrn_name(String scrn_name) {
		this.scrn_name = scrn_name;
	}
	public String getButton_cd() {
		return button_cd;
	}
	public void setButton_cd(String button_cd) {
		this.button_cd = button_cd;
	}
	public String getThreadid() {
		return threadid;
	}
	public void setThreadid(String threadid) {
		this.threadid = threadid;
	}
	public String getThreadid_1() {
		return threadid_1;
	}
	public void setThreadid_1(String threadid_1) {
		this.threadid_1 = threadid_1;
	}
	public String getDetailLogSqlSeq() {
		return detailLogSqlSeq;
	}
	public void setDetailLogSqlSeq(String detailLogSqlSeq) {
		this.detailLogSqlSeq = detailLogSqlSeq;
	}
	public String getSellertool() {
		return sellertool;
	}
	public void setSellertool(String sellertool) {
		this.sellertool = sellertool;
	}
	
	public String getCheck_file() {
		return check_file;
	}
	public void setCheck_file(String check_file) {
		this.check_file = check_file;
	}
	public void initDatesWithDefaultIfEmpty_chart(int term) {
		if ("".equals(getSearch_from()) || "".equals(getSearch_to())) {
			search_to_chart = DateUtil.getDateToString();
			try {
				search_from_chart = DateUtil.addDate(search_to_chart, -term);
			} catch (ParseException e) {
				throw new ESException("SYS101J");
			}
		}
	}
	public void initDatesIfEmpty_week_chart(Date search_from, Date search_to, int amount) {
		if ("".equals(getSearch_from()) || "".equals(getSearch_to())) {
			
			Calendar cal = new GregorianCalendar(Locale.KOREA);
			cal.setTime(search_from);
			cal.add(Calendar.DATE, -7 * amount); 
			
			this.search_from_chart = DateUtil.getDateToString(cal.getTime(), "yyyyMMdd");
			this.search_to_chart = DateUtil.getDateToString(search_to, "yyyyMMdd");
		}
	}
	public void initDatesIfEmpty_month_chart(Date search_from, Date search_to, int amount) {
		if ("".equals(getSearch_from()) || "".equals(getSearch_to())) {
			
			Calendar cal = new GregorianCalendar(Locale.KOREA);
			cal.setTime(search_from);
			cal.add(Calendar.MONTH, -amount);
			
			this.search_from_chart = DateUtil.getDateToString(cal.getTime(), "yyyyMMdd");
			this.search_to_chart = DateUtil.getDateToString(search_to, "yyyyMMdd");
		}
	}
	public String getUse_adverse() {
		return use_adverse;
	}
	public void setUse_adverse(String use_adverse) {
		this.use_adverse = use_adverse;
	}
	public String getH_type() {
		return h_type;
	}
	public void setH_type(String h_type) {
		this.h_type = h_type;
	}
	public String getResummon_yn() {
		return resummon_yn;
	}
	public void setResummon_yn(String resummon_yn) {
		this.resummon_yn = resummon_yn;
	}
	public String getDateType() {
		return dateType;
	}
	public void setDateType(String dateType) {
		this.dateType = dateType;
	}
	public String getSummon_late_yn() {
		return summon_late_yn;
	}
	public void setSummon_late_yn(String summon_late_yn) {
		this.summon_late_yn = summon_late_yn;
	}

	/*
	 * public String getMain_menu_id() { return main_menu_id; } public void
	 * setMain_menu_id(String main_menu_id) { this.main_menu_id = main_menu_id; }
	 * public String getSub_menu_id() { return sub_menu_id; } public void
	 * setSub_menu_id(String sub_menu_id) { this.sub_menu_id = sub_menu_id; }
	 */
	public String getMenuId() {
		return menuId;
	}
	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
	public String getLog_delimiter() {
		return log_delimiter;
	}
	public void setLog_delimiter(String log_delimiter) {
		this.log_delimiter = log_delimiter;
	}
	public String getLogin_user_id() {
		return login_user_id;
	}
	public void setLogin_user_id(String login_user_id) {
		this.login_user_id = login_user_id;
	}
	public String getLogin_auth_id() {
		return login_auth_id;
	}
	public void setLogin_auth_id(String login_auth_id) {
		this.login_auth_id = login_auth_id;
	}
	public String getThreshold() {
		return threshold;
	}
	public void setThreshold(String threshold) {
		this.threshold = threshold;
	}
	public String getThreshold_use() {
		return threshold_use;
	}
	public void setThreshold_use(String threshold_use) {
		this.threshold_use = threshold_use;
	}
	public String getThreshold_total_inbox() {
		return threshold_total_inbox;
	}
	public void setThreshold_total_inbox(String threshold_total_inbox) {
		this.threshold_total_inbox = threshold_total_inbox;
	}
	public PageInfo getSqlPrivacyDetail() {
		return sqlPrivacyDetail;
	}
	public void setSqlPrivacyDetail(PageInfo sqlPrivacyDetail) {
		this.sqlPrivacyDetail = sqlPrivacyDetail;
	}
	public String getCrud_type() {
		return crud_type;
	}
	public void setCrud_type(String crud_type) {
		this.crud_type = crud_type;
	}
	public String getSummon_code() {
		return summon_code;
	}
	public void setSummon_code(String summon_code) {
		this.summon_code = summon_code;
	}

	public String getSummonKey() {
		return summonKey;
	}
	public void setSummonKey(String summonKey) {
		this.summonKey = summonKey;
	}
	public String getDetail_decision_status() {
		return detail_decision_status;
	}
	public void setDetail_decision_status(String detail_decision_status) {
		this.detail_decision_status = detail_decision_status;
	}
	public String getDetail_decision_status_second() {
		return detail_decision_status_second;
	}
	public void setDetail_decision_status_second(String detail_decision_status_second) {
		this.detail_decision_status_second = detail_decision_status_second;
	}
	public String getSummon_cfm_yn() {
		return summon_cfm_yn;
	}
	public void setSummon_cfm_yn(String summon_cfm_yn) {
		this.summon_cfm_yn = summon_cfm_yn;
	}
	public String getLog_filter() {
		return log_filter;
	}
	public void setLog_filter(String log_filter) {
		this.log_filter = log_filter;
	}
	
}
