package com.easycerti.eframe.psm.search.dao;

import java.util.List;

import com.easycerti.eframe.psm.search.vo.AllLogInq;
import com.easycerti.eframe.psm.search.vo.SearchSearch;

public interface OnrLogInqDao {

	public List<AllLogInq> findOnrLogInqList(SearchSearch search);
	public int findOnrLogInqList_count(SearchSearch search);
	
	public AllLogInq findOnrLogInqDetail(SearchSearch search);
	public List<AllLogInq> findOnrLogInqFileList(SearchSearch search);
}
