package com.easycerti.eframe.psm.search.vo;

import com.easycerti.eframe.common.vo.AbstractValueObject;
/**
 * 
 * 설명 : 추출조건별조회 VO
 * @author tjlee
 * @since 2015. 4. 22.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 22.           tjlee            최초 생성
 *
 * </pre>
 */
public class ExtrtCondbyInqTimeline extends AbstractValueObject{
	
	/**
	 * serialVersionUID
	 */
//	private static final long serialVersionUID = 3548522096551273127L;
	
	private String rule_nm;
	private int rule_cd;
	private String system_name;
	private String system_seq;
	private String proc_date;
	private String proc_time;
	private String user_id;
	private String user_ip;
	private String emp_user_name;
	private long log_seq;
	private String scen_name;
	// toString
	@Override
	public String toString() {
		return "ExtrtCondbyInq [" + (rule_nm != null ? "rule_nm=" + rule_nm + ", " : "") + (system_name != null ? "system_name=" + system_name + ", " : "")
				+ (proc_date != null ? "proc_date=" + proc_date + ", " : "") + (proc_time != null ? "proc_time=" + proc_time + ", " : "")
				+ (user_id != null ? "user_id=" + user_id + ", " : "") + (user_ip != null ? "user_ip=" + user_ip : "") + "]";
	}
	public long getLog_seq() {
		return log_seq;
	}
	public void setLog_seq(long log_seq) {
		this.log_seq = log_seq;
	}
	public String getEmp_user_name() {
		return emp_user_name;
	}
	public void setEmp_user_name(String emp_user_name) {
		this.emp_user_name = emp_user_name;
	}	
	public int getRule_cd() {
		return rule_cd;
	}
	public void setRule_cd(int rule_cd) {
		this.rule_cd = rule_cd;
	}
	public String getSystem_seq() {
		return system_seq;
	}
	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}
	public String getRule_nm() {
		return rule_nm;
	}


	public void setRule_nm(String rule_nm) {
		this.rule_nm = rule_nm;
	}


	public String getSystem_name() {
		return system_name;
	}


	public void setSystem_name(String system_name) {
		this.system_name = system_name;
	}


	public String getProc_date() {
		return proc_date;
	}


	public void setProc_date(String proc_date) {
		this.proc_date = proc_date;
	}


	public String getProc_time() {
		return proc_time;
	}


	public void setProc_time(String proc_time) {
		this.proc_time = proc_time;
	}


	public String getUser_id() {
		return user_id;
	}


	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}


	public String getUser_ip() {
		return user_ip;
	}


	public void setUser_ip(String user_ip) {
		this.user_ip = user_ip;
	}
	public String getScen_name() {
		return scen_name;
	}
	public void setScen_name(String scen_name) {
		this.scen_name = scen_name;
	}

//
//	public static long getSerialversionuid() {
//		return serialVersionUID;
//	}
	

	
}
