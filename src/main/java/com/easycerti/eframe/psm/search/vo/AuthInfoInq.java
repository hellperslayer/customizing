package com.easycerti.eframe.psm.search.vo;

import com.easycerti.eframe.common.util.PageInfo;

public class AuthInfoInq {
	private long log_seq;			//log_seq
	private String proc_date;		//날짜
	private String proc_time;		//시간
	private String dept_id;			//권한부여자_소속_id
	private String dept_name;		//권한부여자_소속명
	private String emp_user_id;		//권한부여자_id
	private String emp_user_name;	//권한부여자_이름
	
	private String user_ip;		//권한부여자_ip
	private String req_type;		//행위 
	private String target_auth;		//타겟권한
	private String target_id;		//타겟id
	
	private String target_auth_name;		//타겟이름
	private String system_seq;		//시스템_seq
	private String system_name;		//시스템명
	
	private String target_name;		//타겟이름
	private String allow_ip;		//허용ip
	private String req_url;			//접근경로
	
	
	private PageInfo pageInfo;
	
	public PageInfo getPageInfo() {
		return pageInfo;
	}
	public void setPageInfo(PageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}
	
	public long getLog_seq() {
		return log_seq;
	}
	public void setLog_seq(long log_seq) {
		this.log_seq = log_seq;
	}
	public String getProc_date() {
		return proc_date;
	}
	public void setProc_date(String proc_date) {
		this.proc_date = proc_date;
	}
	public String getProc_time() {
		return proc_time;
	}
	public void setProc_time(String proc_time) {
		this.proc_time = proc_time;
	}
	public String getEmp_user_id() {
		return emp_user_id;
	}
	public void setEmp_user_id(String emp_user_id) {
		this.emp_user_id = emp_user_id;
	}
	public String getEmp_user_name() {
		return emp_user_name;
	}
	public void setEmp_user_name(String emp_user_name) {
		this.emp_user_name = emp_user_name;
	}
	public String getDept_id() {
		return dept_id;
	}
	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}
	public String getDept_name() {
		return dept_name;
	}
	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}
	public String getUser_ip() {
		return user_ip;
	}
	public void setUser_ip(String user_ip) {
		this.user_ip = user_ip;
	}
	public String getReq_type() {
		return req_type;
	}
	public void setReq_type(String req_type) {
		this.req_type = req_type;
	}
	public String getTarget_auth() {
		return target_auth;
	}
	public void setTarget_auth(String target_auth) {
		this.target_auth = target_auth;
	}
	public String getTarget_id() {
		return target_id;
	}
	public void setTarget_id(String target_id) {
		this.target_id = target_id;
	}
	public String getSystem_seq() {
		return system_seq;
	}
	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}
	public String getSystem_name() {
		return system_name;
	}
	public void setSystem_name(String system_name) {
		this.system_name = system_name;
	}
	public String getTarget_auth_name() {
		return target_auth_name;
	}
	public void setTarget_auth_name(String target_auth_name) {
		this.target_auth_name = target_auth_name;
	}
	public String getTarget_name() {
		return target_name;
	}
	public void setTarget_name(String target_name) {
		this.target_name = target_name;
	}
	public String getAllow_ip() {
		return allow_ip;
	}
	public void setAllow_ip(String allow_ip) {
		this.allow_ip = allow_ip;
	}
	public String getReq_url() {
		return req_url;
	}
	public void setReq_url(String req_url) {
		this.req_url = req_url;
	}
}
