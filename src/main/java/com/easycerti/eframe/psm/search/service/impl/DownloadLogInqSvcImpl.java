package com.easycerti.eframe.psm.search.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.PrivacyType;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.search.dao.AllLogInqDao;
import com.easycerti.eframe.psm.search.dao.DownloadLogInqDao;
import com.easycerti.eframe.psm.search.dao.ExtrtCondbyInqDao;
import com.easycerti.eframe.psm.search.service.DownloadLogInqSvc;
import com.easycerti.eframe.psm.search.vo.AllLogInq;
import com.easycerti.eframe.psm.search.vo.AllLogInqList;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.system_management.dao.AgentMngtDao;
import com.easycerti.eframe.psm.system_management.vo.Misdetect;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

@Service
public class DownloadLogInqSvcImpl implements DownloadLogInqSvc {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired 
	private CommonDao commonDao;
	
	@Autowired 
	private DownloadLogInqDao downloadLogInqDao;
	
	@Autowired 
	private AgentMngtDao agentMngtDao;
	
	@Autowired 
	private AllLogInqDao allLogInqDao;
	
	@Autowired 
	private ExtrtCondbyInqDao extrtCondbyInqDao;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	@Override
	public DataModelAndView findDownloadLogInqList(SearchSearch search) {
		
		SimpleCode simpleCode = new SimpleCode();
		String index = "/downloadLogInq/list.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		if (search.getStart_h() != null && search.getStart_h() != "") {
			String start_time = search.getStart_h().substring(0);
			if (start_time.length() < 2)
				start_time = "0" + start_time;
			search.setStart_time(start_time);
		}
		if (search.getEnd_h() != null && search.getEnd_h() != "") {
			String end_time = search.getEnd_h().substring(0);
			if (end_time.length() < 2)
				end_time = "0" + end_time;
			search.setEnd_time(end_time);
		}
			
		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		int count = 0;
		List<AllLogInq> downloadLogInq = new ArrayList<AllLogInq>();
		
		if(search.getLogin_auth_id().equals("AUTH20000")) {
			count = downloadLogInqDao.findDownloadLogInqListAuth20_count(search);
			downloadLogInq = downloadLogInqDao.findDownloadLogInqAuth20List(search);
		}else {
			count = downloadLogInqDao.findDownloadLogInqList_count(search);
			downloadLogInq = downloadLogInqDao.findDownloadLogInqList(search);
		}
		
		for(int i=0; i<downloadLogInq.size(); i++) {
			AllLogInq ali = downloadLogInq.get(i);
			String result_type = ali.getResult_type();
			ali.setResultTypeMap(result_type);
			if(result_type != null) {
				String[] arrResult_type = result_type.split(",");
				Arrays.sort(arrResult_type);
				String res = "";
				for(int j=0; j<arrResult_type.length; j++) {
					if(j ==0)
						res += arrResult_type[j];
					else
						res += "," + arrResult_type[j];
				}
				ali.setResult_type(res);
			}
		}
		
		String checkEmpNameMasking = commonDao.checkEmpNameMasking();
		if(checkEmpNameMasking.equals("Y")) {
			for (int i = 0; i < downloadLogInq.size(); i++) {
				if(!"".equals(downloadLogInq.get(i).getEmp_user_name())) {
					String empUserName = downloadLogInq.get(i).getEmp_user_name();
					StringBuilder builder = new StringBuilder(empUserName);
					builder.setCharAt(empUserName.length() - 2, '*');
					downloadLogInq.get(i).setEmp_user_name(builder.toString());
				}
			}
		}
		
		search.setTotal_count(count);
		//List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList_Onnara();
		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList_byAdmin(search);
		
		AllLogInqList downloadLogInqList = new AllLogInqList(downloadLogInq);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("downloadLogInqList", downloadLogInqList);
		modelAndView.addObject("systemMasterList", systemMasterList);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView findDownloadLogInqDetail(SearchSearch search) {

		int pageSize = 5;
		search.getAllLogInqDetail().setSize(pageSize);
		
		int cnt = downloadLogInqDao.findDownloadLogInqDetailResultCount(search);
		search.getAllLogInqDetail().setTotal_count(cnt);
		
		//String ui_type = commonDao.getUiType();
		//search.setUi_type(ui_type+"_TYPE");
		
		List<AllLogInq> data2 = new ArrayList<>();
		AllLogInq downloadLogInq = new AllLogInq();
		List<AllLogInq> downloadLogInqDetailList = new ArrayList<AllLogInq>();
		
		try{
			downloadLogInq = downloadLogInqDao.findDownloadLogInqDetail(search);
			
			String result_type = downloadLogInq.getResult_type();
			if(result_type != null) {
				String[] arrResult_type = result_type.split(",");
				Arrays.sort(arrResult_type);
				String res = "";
				for(int j=0; j<arrResult_type.length; j++) {
					if(j ==0)
						res += arrResult_type[j];
					else
						res += "," + arrResult_type[j];
				}
				downloadLogInq.setResult_type(res);
			}
			
			String checkEmpNameMasking = commonDao.checkEmpNameMasking();
			if(checkEmpNameMasking.equals("Y")) {
				if(!"".equals(downloadLogInq.getEmp_user_name())) {
					String empUserName = downloadLogInq.getEmp_user_name();
					StringBuilder builder = new StringBuilder(empUserName);
					builder.setCharAt(empUserName.length() - 2, '*');
					downloadLogInq.setEmp_user_name(builder.toString());
				}
			}
			
			if(downloadLogInq.getEmp_user_id() != null) {
				//data2 = extrtCondbyInqDao.getExtrtCondbyInqDetail2_1(downloadLogInq.getEmp_user_id());
				data2 = downloadLogInqDao.findDownloadLogGroupBySystem(downloadLogInq.getEmp_user_id());
			}
			// 개인정보 마스킹 
			//search.setSystem_seq(allLogInq.getSystem_seq());
			search.setSystem_seq_temp(downloadLogInq.getSystem_seq());
			
			downloadLogInqDetailList = downloadLogInqDao.findDownloadLogInqDetailResult(search);
			
			for(AllLogInq v : downloadLogInqDetailList){
				if(v.getResult_content() != null && v.getResult_content() != ""){
					
					String pResult = v.getResult_content();
					
					if(v.getResult_type() != null && v.getPrivacy_masking() != null && v.getPrivacy_masking().equals("Y")) {
						pResult = AllLogInqSvcImpl.getResultContextByMasking(pResult, v.getResult_type());
					}
					
					v.setResult_content_masking(pResult);
				}
			}
		}catch(Exception e) {
			logger.error("[DownloadLogInqSvcImpl.findDownloadLogInqDetail] MESSAGE : " + e.getMessage());
			throw new ESException("SYS005V");
		}
		
		SimpleCode simpleCode = new SimpleCode();
		String index = "/downloadLogInq/detail.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		modelAndView.addObject("downloadLogInq", downloadLogInq);
		modelAndView.addObject("downloadLogInqDetailList", downloadLogInqDetailList);
		
		modelAndView.addObject("index_id", index_id);
		if ( data2.size() > 0 ) {
			modelAndView.addObject("data2", data2);
		}
		return modelAndView;
	}
	
	@Override
	public void findDownloadLogInqList_download(DataModelAndView modelAndView, SearchSearch search,
			HttpServletRequest request) throws Exception{
		search.setUseExcel("true");
		
		String fileName = "PSM_다운로드_로그조회_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "다운로드 로그조회";
		
		if(search.getStart_h() != null) {
			String start_time = search.getStart_h().substring(0);
			if(start_time.length() < 2)
				start_time = "0" + start_time;
			search.setStart_time(start_time);
		}
		if(search.getEnd_h() != null) {
			String end_time = search.getEnd_h().substring(0);
			if(end_time.length() < 2)
				end_time = "0" + end_time;
			search.setEnd_time(end_time);
		}
		List<AllLogInq> allLogInq = new ArrayList<AllLogInq>();
		
		if(search.getLogin_auth_id().equals("AUTH20000")) {
			allLogInq = downloadLogInqDao.findDownloadLogInqAuth20List(search);
		}else {
			allLogInq = downloadLogInqDao.findDownloadLogInqList(search);
		}
		
		List<PrivacyType> privacyTypeList = commonDao.getPrivacyTypesExceptRegular();
		SimpleDateFormat parseSdf = new SimpleDateFormat("yyyyMMddHHmmss");
		SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");

		// 다운로드로그 풀스캔연동여부
		HttpSession session = request.getSession();
		String use_fullscan = (String) session.getAttribute("use_fullscan");
		
		if(use_fullscan != null && use_fullscan.equals("Y")) {
			for(int i=0; i<allLogInq.size(); i++){
				AllLogInq ali = allLogInq.get(i);
				String result_type = ali.getResult_type();
				if(result_type != null) {
					String[] arrResult_type = result_type.split(",");
					Arrays.sort(arrResult_type);
					
					String result_type_string = "";
					String prev = "";
					int count = 1;
					for(int j=0; j<arrResult_type.length; j++) {
						if(prev.equals(arrResult_type[j])) {
							count++;
						}else {
							if(j != 0) {
								result_type_string += " " +count + ", ";
							}
							
							for(int k=0; k<privacyTypeList.size(); k++){
								if(privacyTypeList.get(k).getPrivacy_type().equals(arrResult_type[j])) { 
									result_type_string += privacyTypeList.get(k).getPrivacy_desc(); 
								}
							}
							count = 1;
							prev = arrResult_type[j];
						}
						
						if(j == arrResult_type.length - 1) {
							result_type_string += " " +count;
						}
					}
					
					allLogInq.get(i).setCnt(arrResult_type.length);
					allLogInq.get(i).setResult_type(result_type_string);
				}
				
				if(allLogInq.get(i).getDept_name() == null || allLogInq.get(i).getDept_name().equals("")) 
					allLogInq.get(i).setDept_name("시스템");
				if(allLogInq.get(i).getEmp_user_name() == null || allLogInq.get(i).getEmp_user_name().equals("")) 
					allLogInq.get(i).setEmp_user_name("시스템");
				
				try {
					allLogInq.get(i).setProc_date(sdf.format(parseSdf.parse(allLogInq.get(i).getProc_date() + allLogInq.get(i).getProc_time())));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				/*if(allLogInq.get(i).getScrn_name() == null) {
					allLogInq.get(i).setScrn_name(allLogInq.get(i).getReq_url());
				}*/
			}
		}
		

		String scrn_name_view = (String) session.getAttribute("scrn_name");
		String[] columns;
		String[] heads;
		if(use_fullscan != null && use_fullscan.equals("Y")) {
			if(scrn_name_view.equals("1")) {
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "result_type", "cnt", "scrn_name", "req_url", "file_name", "reason"};
				heads = new String[] {"일시", "소속", "취급자ID", "취급자명", "취급자IP", "시스템명", "정보주체", "합계", "메뉴명", "접근 경로", "파일명", "사유"};
			}else if(scrn_name_view.equals("2")){
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "result_type", "cnt", "req_url", "file_name", "reason"};
				heads = new String[] {"일시", "소속", "취급자ID", "취급자명", "취급자IP", "시스템명", "정보주체", "합계", "접근 경로", "파일명", "사유"};
			}else {
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "result_type", "cnt", "file_name", "reason"};
				heads = new String[] {"일시", "소속", "취급자ID", "취급자명", "취급자IP", "시스템명", "정보주체", "합계", "파일명", "사유"};
			}
		} else {
			if(scrn_name_view.equals("1")) {
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "scrn_name", "req_url", "file_name", "reason"};
				heads = new String[] {"일시", "소속", "취급자ID", "취급자명", "취급자IP", "시스템명", "메뉴명", "접근 경로", "파일명", "사유"};
			}else if(scrn_name_view.equals("2")){
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "req_url", "file_name", "reason"};
				heads = new String[] {"일시", "소속", "취급자ID", "취급자명", "취급자IP", "시스템명", "접근 경로", "파일명", "사유"};
			}else {
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "file_name", "reason"};
				heads = new String[] {"일시", "소속", "취급자ID", "취급자명", "취급자IP", "시스템명", "파일명", "사유"};
			}
		}
		
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", allLogInq);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}
	
	@Override
	public void findDownloadLogInqList_downloadCSV(DataModelAndView modelAndView, SearchSearch search,
			HttpServletRequest request) {
		
		search.setUseExcel("true");
		
		String fileName = "PSM_다운로드_로그조회_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		//String sheetName = "개인정보 로그조회";
		
		if(search.getStart_h() != null) {
			String start_time = search.getStart_h().substring(0);
			if(start_time.length() < 2)
				start_time = "0" + start_time;
			search.setStart_time(start_time);
		}
		if(search.getEnd_h() != null) {
			String end_time = search.getEnd_h().substring(0);
			if(end_time.length() < 2)
				end_time = "0" + end_time;
			search.setEnd_time(end_time);
		}

		List<AllLogInq> allLogInq = new ArrayList<AllLogInq>();
		
		if(search.getLogin_auth_id().equals("AUTH20000")) {
			allLogInq = downloadLogInqDao.findDownloadLogInqAuth20List(search);
		}else {
			allLogInq = downloadLogInqDao.findDownloadLogInqList(search);
		}
		List<PrivacyType> privacyTypeList = commonDao.getPrivacyTypesExceptRegular();
		
		SimpleDateFormat parseSdf = new SimpleDateFormat("yyyyMMddHHmmss");
		SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");

		// 다운로드로그 풀스캔연동여부
		HttpSession session = request.getSession();
		String use_fullscan = (String) session.getAttribute("use_fullscan");
		
		if(use_fullscan != null && use_fullscan.equals("Y")) {
			for(int i=0; i<allLogInq.size(); i++){
				AllLogInq ali = allLogInq.get(i);
				String result_type = ali.getResult_type();
				if(result_type != null) {
					String[] arrResult_type = result_type.split(",");
					Arrays.sort(arrResult_type);
					String result_type_string = "";
					String prev = "";
					int count = 1;
					for(int j=0; j<arrResult_type.length; j++) {
						if(prev.equals(arrResult_type[j])) {
							count++;
						}else {
							if(j != 0){
								result_type_string += " " +count + ", ";
							}
							
							for(int k=0; k<privacyTypeList.size(); k++){
								if(privacyTypeList.get(k).getPrivacy_type().equals(arrResult_type[j])) { 
									result_type_string += privacyTypeList.get(k).getPrivacy_desc(); 
								}
							}
							count = 1;
							prev = arrResult_type[j];
						}
						
						if(j == arrResult_type.length - 1){
							result_type_string += " " +count;
						}
					}
					
					allLogInq.get(i).setCnt(arrResult_type.length);
					allLogInq.get(i).setResult_type(result_type_string);
				}
				
				if(allLogInq.get(i).getDept_name() == null || allLogInq.get(i).getDept_name().trim().equals("")) { 
					allLogInq.get(i).setDept_name("-");
				}
				if(allLogInq.get(i).getEmp_user_name() == null || allLogInq.get(i).getEmp_user_name().equals("")) { 
					allLogInq.get(i).setEmp_user_name("-");
				}
				try {
					allLogInq.get(i).setProc_date(sdf.format(parseSdf.parse(allLogInq.get(i).getProc_date() + allLogInq.get(i).getProc_time())));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		}
		
		List<Map<String, Object>> list = new ArrayList<>();
		
		if(use_fullscan != null && use_fullscan.equals("Y")) {
			for(AllLogInq ali : allLogInq) {
				Map<String, Object> map = new HashMap<>();
				map.put("proc_date", ali.getProc_date());
				map.put("dept_name", ali.getDept_name());
				map.put("emp_user_id", ali.getEmp_user_id());
				map.put("emp_user_name", ali.getEmp_user_name());
				map.put("user_ip", ali.getUser_ip());
				map.put("system_name", ali.getSystem_name());
				map.put("result_type", ali.getResult_type());
				map.put("cnt", ali.getCnt());
				map.put("scrn_name", ali.getScrn_name());
				map.put("req_url", ali.getReq_url());
				map.put("file_name", ali.getFile_name());
				map.put("reason", ali.getReason());
				
				list.add(map);
			}
		} else {
			for(AllLogInq ali : allLogInq) {
				Map<String, Object> map = new HashMap<>();
				map.put("proc_date", ali.getProc_date());
				map.put("dept_name", ali.getDept_name());
				map.put("emp_user_id", ali.getEmp_user_id());
				map.put("emp_user_name", ali.getEmp_user_name());
				map.put("user_ip", ali.getUser_ip());
				map.put("system_name", ali.getSystem_name());
				map.put("scrn_name", ali.getScrn_name());
				map.put("req_url", ali.getReq_url());
				map.put("file_name", ali.getFile_name());
				map.put("reason", ali.getReason());
				
				list.add(map);
			}
		}
		
		String scrn_name_view = (String) session.getAttribute("scrn_name");
		String[] columns;
		String[] heads;
		if(use_fullscan != null && use_fullscan.equals("Y")) {
			if(scrn_name_view.equals("1")) {
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "result_type", "cnt", "scrn_name", "req_url", "file_name", "reason"};
				heads = new String[] {"일시", "소속", "취급자ID", "취급자명", "취급자IP", "시스템명", "정보주체", "합계", "메뉴명", "접근 경로", "파일명", "사유"};
			}else if(scrn_name_view.equals("2")){
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "result_type", "cnt", "req_url", "file_name", "reason"};
				heads = new String[] {"일시", "소속", "취급자ID", "취급자명", "취급자IP", "시스템명", "정보주체", "합계", "접근 경로", "파일명", "사유"};
			}else {
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "result_type", "cnt", "file_name", "reason"};
				heads = new String[] {"일시", "소속", "취급자ID", "취급자명", "취급자IP", "시스템명", "정보주체", "합계", "파일명", "사유"};
			}
		} else {
			if(scrn_name_view.equals("1")) {
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "scrn_name", "req_url", "file_name", "reason"};
				heads = new String[] {"일시", "소속", "취급자ID", "취급자명", "취급자IP", "시스템명", "메뉴명", "접근 경로", "파일명", "사유"};
			}else if(scrn_name_view.equals("2")){
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "req_url", "file_name", "reason"};
				heads = new String[] {"일시", "소속", "취급자ID", "취급자명", "취급자IP", "시스템명", "접근 경로", "파일명", "사유"};
			}else {
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "file_name", "reason"};
				heads = new String[] {"일시", "소속", "취급자ID", "취급자명", "취급자IP", "시스템명", "파일명", "사유"};
			}
		}
		
		modelAndView.addObject("excelData", list);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
		modelAndView.addObject("fileName", fileName);
	}
	
	@Override
	public DataModelAndView saveDownloadLogReason(Map<String, String> parameters) {
		SearchSearch paramBean = CommonHelper.convertMapToBean(parameters, SearchSearch.class);

		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);

		try {
			downloadLogInqDao.saveDownloadLogReason(paramBean);
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			logger.error("[ExtrtCondbyInqSvcImpl.saveDownloadLogReason] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS601J");
		}

		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);

		return modelAndView;
	}
	
	@Override
	public void findDownloadLogInqDetail_download(DataModelAndView modelAndView, SearchSearch search,
			HttpServletRequest request) {
		
		search.setUseExcel("true");
		
		String fileName = "PSM_개인정보_다운로드_로그조회_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "접속기록상세 개인정보 리스트";
		System.out.println(fileName+sheetName);
		AllLogInq downloadLogInq = downloadLogInqDao.findDownloadLogInqDetail(search);
		search.setSystem_seq_temp(downloadLogInq.getSystem_seq());
		List<AllLogInq> downloadLogInqDetailList = downloadLogInqDao.findDownloadLogInqDetailResult(search);
		
		int num = 1;
		for(AllLogInq v : downloadLogInqDetailList){
			if(v.getResult_content() != null && v.getResult_content() != ""){
				
				String pResult = v.getResult_content();
				
				if(v.getResult_type() != null && v.getPrivacy_masking() != null && v.getPrivacy_masking().equals("Y")) {
					pResult = getResultContextByMasking(pResult, v.getResult_type());
				}
				
				v.setResult_content_masking(pResult);
				v.setData1(num);
				
				num++;
			}
		}
		String userProfile ="이름 : " + downloadLogInq.getEmp_user_name()+
							" 소속 : " + downloadLogInq.getDept_name()+
							" 시스템 : " + downloadLogInq.getSystem_name()+
							" 날짜 : " + downloadLogInq.getProc_date().toString();
		
		String[] columns;
		String[] heads;
		columns = new String[] {"data1", "result_type", "result_content_masking"};
		heads = new String[] {"No.", "개인정보유형", "개인정보내용"};
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", downloadLogInqDetailList);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
		modelAndView.addObject("userProfile", userProfile);
	}
	
	@Override
	public void findDownloadLoginqDetail_downloadCSV(DataModelAndView modelAndView, SearchSearch search,
			HttpServletRequest request) {
		
		search.setUseExcel("true");
		
		String fileName = "PSM_개인정보_다운로드_로그조회_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		
		AllLogInq downloadLogInq = downloadLogInqDao.findDownloadLogInqDetail(search);
		List<AllLogInq> allLogInqDetailList = downloadLogInqDao.findDownloadLogInqDetailResult(search);
		
		int num = 1;
		for(AllLogInq v : allLogInqDetailList){
			if(v.getResult_content() != null && v.getResult_content() != ""){
				
				String pResult = v.getResult_content();
				
				if(v.getResult_type() != null && v.getPrivacy_masking() != null && v.getPrivacy_masking().equals("Y")) {
					pResult = getResultContextByMasking(pResult, v.getResult_type());
				}
				
				v.setResult_content_masking(pResult);
				v.setData1(num);
				
				num++;
			}
		}
		
		List<Map<String, Object>> excelDataList = new ArrayList<>();
		
		for (AllLogInq alllog : allLogInqDetailList) {
			Map<String, Object> map = new HashMap<>();
			map.put("data1", alllog.getData1());
			map.put("result_type", alllog.getResult_type());
			map.put("result_content_masking", alllog.getResult_content_masking());
			excelDataList.add(map);
		}
		
		String[] columns;
		String[] heads;
		columns = new String[] {"data1", "result_type", "result_content_masking"};
		heads = new String[] {"No.", "개인정보유형", "개인정보내용"};
		
		modelAndView.addObject("excelData", excelDataList);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
		modelAndView.addObject("fileName", fileName);
	}
	
	@Override
	public DataModelAndView addPrivacy(Map<String, String> parameters){
		Misdetect paramBean = CommonHelper.convertMapToBean(parameters, Misdetect.class);
		
		if("1".equals(paramBean.getPrivacy_type())){
			String privPattern = downloadLogInqDao.checkPattern(paramBean); 
			paramBean.setMisdetect_pattern(privPattern);
		}else {
			String privPattern = downloadLogInqDao.checkPattern(paramBean); 
			paramBean.setMisdetect_pattern(privPattern);
		}	
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			downloadLogInqDao.addPrivacy(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[AllLogInqSvcImpl.addPrivacy] MESSAGE : " + e.getMessage());
			throw new ESException("SYS030J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView removePrivacy(Map<String, String> parameters) {
		Misdetect paramBean = CommonHelper.convertMapToBean(parameters, Misdetect.class);
		
		if("1".equals(paramBean.getPrivacy_type())){
			String privPattern = downloadLogInqDao.checkPattern(paramBean); 
			paramBean.setMisdetect_pattern(privPattern);
		}else {
			String privPattern = downloadLogInqDao.checkPattern(paramBean); 
			paramBean.setMisdetect_pattern(privPattern);
		}
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			downloadLogInqDao.removePrivacy(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[DownloadLogInqSvcImpl.removePrivacy] MESSAGE : " + e.getMessage());
			throw new ESException("SYS030J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	public static String getResultContextByMasking(String result, String type) {
		int len = result.length();
		switch(type) {
			case "주민등록번호":
			case "외국인등록번호":
				result = result.substring(0,6)+"-*******";
				break;
			case "운전면허번호":
				result = result.substring(0,6);
				for(int i=0; i<len-6; i++)
					result += "*";
				break;
			case "여권번호":
				result = result.substring(0,5);
				for(int i=0; i<len-5; i++)
					result += "*";
				break;
			case "신용카드번호":
				result = result.substring(0,10)+"****-****";
				break;
			case "건강보험번호":
				result = result.substring(0,6);
				for(int i=0; i<len-6; i++)
					result += "*";
				break;
			case "전화번호":
			case "휴대폰번호":
				String[] arr = result.split("-");
				if(arr.length <= 1) {
					result = result.substring(0, len - 4) + "****";
				}else {
					result = arr[0] + "-" + arr[1] + "-****";
				}
				break;
			case "이메일":
				result = result.replaceAll("(?<=.{3}).(?=.*@)", "*");
				break;
			case "계좌번호":
				String[] arr2 = result.split("-");
				if(arr2.length <= 2) {
					result = result.substring(0, len - 6) + "******";
				}else {
					result = arr2[0] + "-" + arr2[1] + "-";
					if(arr2[2] != null) {
						int l = arr2[2].length();
						for(int i=0; i<l; i++)
							result += "*";
					}
				}
				break;
			/*case "외국인등록번호":
				result = result.substring(0,5);
				for(int i=0; i<len-5; i++)
					result += "*";
				break;*/
		}
		
		return result;
	}

	
	
}
