package com.easycerti.eframe.psm.search.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.search.vo.AllLogSql;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;
/**
 * 
 * 설명 : 전체로그조회 Service Interface
 * @author tjlee
 * @since 2015. 4. 22.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 22.           tjlee            최초 생성
 *
 * </pre>
 */

public interface AllLogInqSvc {

	/**
	 * 설명 : 전체로그조회 리스트 
	 * @author tjlee
	 * @since 2015. 6. 8.
	 * @return DataModelAndView
	 */
	public DataModelAndView findAllLogInqList(SearchSearch search);
	
	/**
	 * 설명 : 전체로그조회 상세 
	 * @author tjlee
	 * @since 2015. 6. 8.
	 * @return DataModelAndView
	 */
	public DataModelAndView findAllLogInqDetail(SearchSearch search, HttpServletRequest request);
	
	/**
	 * 설명 : 소명조회 상세 
	 * @author tjlee
	 * @since 2015. 6. 8.
	 * @return DataModelAndView
	 */
	public DataModelAndView findSummonAllLogInqDetail(SearchSearch search);
	
	/**
	 * 설명 : 전체로그조회 상세 개인정보 탐지 제외 
	 * @author ehchoi
	 * @since 2016. 8. 30.
	 * @return DataModelAndView
	 */
	public DataModelAndView addPrivacy(Map<String, String> parameters);
	public DataModelAndView removePrivacy(Map<String, String> parameters);
	
	
	public DataModelAndView addGoogle(Map<String, String> parameters);
	
	public DataModelAndView updateGoogle(Map<String, String> parameters);
	
	public DataModelAndView deleteGoogle(Map<String, String> parameters);
	
	public DataModelAndView findDashPopupList(Map<String, String> parameters);
	
	/**
	 * 설명 : 전체로그조회 엑셀다운로드 
	 * @author tjlee
	 * @since 2015. 6. 8.
	 * @return void
	 */
	public void findAllLogInqList_download(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);
	public void findAllLogInqList_download_extend(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);
	public void findAllLogInqList_downloadCSV(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);
	public void findAllLogInqList_downloadCSV_extend(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);
	
	public int findAllLogInqDetailLogSeq(SearchSearch search);
	
	public DataModelAndView findReadInfoList(SearchSearch search);
	
	public void findAllLoginqDetail_download(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);
	public void findAllLoginqDetail_downloadCSV(DataModelAndView modelAndView, SearchSearch search,
			HttpServletRequest request);
	
	public DataModelAndView findAllLogInqListByReqType(SearchSearch search);
	public void findAllLogInqList_downloadByReqType(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);
	

	public JSONObject checkPrivacyInfo(SearchSearch search);
	public DataModelAndView findPrivacyInfo(SearchSearch search);
	public int addCenterLogInfo(List<String[]> logList);

	public DataModelAndView findBizLog80Popup(SearchSearch search);
	public DataModelAndView getResultType(SearchSearch search);

	//개인정보 임계치
	public DataModelAndView thresholdSetting(SearchSearch search);

	//개인정보 임계치 수정
	public DataModelAndView updateThreshold(SearchSearch search);
	
	public String updateSqlCollectYn(AllLogSql allLogSql);
	public void logSqlDownloadExcel(DataModelAndView modelAndView, AllLogSql allLogSql);
	public void logSqlDownloadCsv(DataModelAndView modelAndView, AllLogSql allLogSql);

	public DataModelAndView findAllLogInqList_kdic(SearchSearch search);

	public DataModelAndView findAllLogInqDetail_kdic(SearchSearch search, HttpServletRequest request);
	
}
