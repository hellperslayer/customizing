package com.easycerti.eframe.psm.search.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.search.dao.EmpDetailInqDao;
import com.easycerti.eframe.psm.search.service.EmpDetailInqSvc;
import com.easycerti.eframe.psm.search.vo.EmpDetailInq;
import com.easycerti.eframe.psm.search.vo.EmpDetailInqList;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.system_management.dao.MenuMngtDao;
import com.easycerti.eframe.psm.system_management.vo.EmpUser;
/**
 * 
 * 설명 : 위험도별조회 Service Implements
 * @author tjlee
 * @since 2015. 4. 22.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 22.           tjlee            최초 생성
 *
 * </pre>
 */
@Service
public class EmpDetailInqSvcImpl implements EmpDetailInqSvc {

	@Autowired
	private EmpDetailInqDao empDetailInqDao;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;

	@Autowired
	private MenuMngtDao menuDao;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;
	
	@Override
	public DataModelAndView findEmpDetailInqList(SearchSearch search) {

		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		search.setTotal_count(empDetailInqDao.findEmpDetailInqOne_count(search));
		
		List<EmpDetailInq> empDetailInq = empDetailInqDao.findEmpDetailInqList(search);
		EmpDetailInqList empDetailInqList = new EmpDetailInqList(empDetailInq);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("empDetailInqList", empDetailInqList);
		
		return modelAndView;
	}
	
	
	@Override
	public DataModelAndView findEmpDetailInqDetail(SearchSearch search) {
		
		int pageSize = Integer.valueOf(defaultPageSize);
		search.getEmpDetailInqDetail().setSize(pageSize);
		search.getEmpDetailInqDetail().setTotal_count(empDetailInqDao.findEmpDetailInqDetailOne_count(search));
		
		EmpUser empUser = empDetailInqDao.findEmpUser(search);
		List<EmpDetailInq> empDetailInq = empDetailInqDao.findEmpDetailInqListByEmpCd(search);
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		modelAndView.addObject("empUser", empUser);
		modelAndView.addObject("empDetailInq", empDetailInq);
		
		return modelAndView;
	}


	@Override
	public void findEmpDetailInqList_download(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request) {

		search.setUseExcel("true");
		
		String fileName = "PSM_위험도별조회_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "위험도별조회";
		
		List<EmpDetailInq> empDetailInq = empDetailInqDao.findEmpDetailInqList(search);
		
		SimpleDateFormat parseSdf = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		for(int i=0; i<empDetailInq.size(); i++){
			try {
				empDetailInq.get(i).setOccr_dt(
						sdf.format(parseSdf.parse(empDetailInq.get(i).getStartday()))
						+ " ~ " 
						+  sdf.format(parseSdf.parse(empDetailInq.get(i).getEndday()))
				);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			switch(empDetailInq.get(i).getSum_dng_val_code()){
				case 1 : empDetailInq.get(i).setSum_dng_val_string("심각"); break;
				case 2 : empDetailInq.get(i).setSum_dng_val_string("경계"); break;
				case 3 : empDetailInq.get(i).setSum_dng_val_string("주의"); break;
				case 4 : empDetailInq.get(i).setSum_dng_val_string("관심"); break;
				case 5 : empDetailInq.get(i).setSum_dng_val_string("정상"); break;
				default : empDetailInq.get(i).setSum_dng_val_string(null); break;
			}
		}
		
		String[] columns = new String[] {"occr_dt", "emp_user_id", "emp_user_name", "dept_name" , "sum_dng_val", "sum_dng_val_string"};
		String[] heads = new String[] {"기간", "사번", "사용자명", "부서", "위험지수/기간", "위험도"};
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", empDetailInq);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}
}
