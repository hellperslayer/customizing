package com.easycerti.eframe.psm.search.vo;

public class PatchHist {
	private String seq;
	private String proc_date;
	private String patch_type;
	private String patch_version;
	private String patch_date;
	private String script;
	private String description;
	private String patch_sort;
	
	public String getPatch_sort() {
		return patch_sort;
	}
	public void setPatch_sort(String patch_sort) {
		this.patch_sort = patch_sort;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getProc_date() {
		return proc_date;
	}
	public void setProc_date(String proc_date) {
		this.proc_date = proc_date;
	}
	public String getPatch_type() {
		return patch_type;
	}
	public void setPatch_type(String patch_type) {
		this.patch_type = patch_type;
	}
	public String getPatch_version() {
		return patch_version;
	}
	public void setPatch_version(String patch_version) {
		this.patch_version = patch_version;
	}
	public String getPatch_date() {
		return patch_date;
	}
	public void setPatch_date(String patch_date) {
		this.patch_date = patch_date;
	}
	public String getScript() {
		return script;
	}
	public void setScript(String script) {
		this.script = script;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
