package com.easycerti.eframe.psm.search.dao;

import java.util.List;

import com.easycerti.eframe.psm.search.vo.AllLogInq;
import com.easycerti.eframe.psm.search.vo.SearchSearch;

public interface FileManageDao {

	//파일 대장 등록 리스트 갯수
	Integer getRegisteredListCnt(SearchSearch search);

	//파일 대장 리스트 
	List<AllLogInq> getRegisteredList(SearchSearch search);

}
