package com.easycerti.eframe.psm.search.web;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.psm.search.dao.AllLogInqDao;
import com.easycerti.eframe.psm.search.service.AllLogInqSvc;
import com.easycerti.eframe.psm.search.service.FileManageSvc;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.system_management.dao.OptionSettingDao;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;

@Controller
@RequestMapping("/filemanagement/*")
public class FileManageCtrl {

	@Autowired
	private FileManageSvc fileManageSvc;
	
	@Autowired
	private OptionSettingDao optionSettingDao;
	
	@Autowired
	private AllLogInqDao allLogInqDao;
	
	@MngtActHist(log_action = "SELECT", log_message = "[접속기록] 파일대장접속기록관리")
	@RequestMapping(value="registeredList.html", method=RequestMethod.POST)
	public DataModelAndView findDownloadLogInqList(@ModelAttribute("search") SearchSearch search, @RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		HttpSession session = request.getSession();
		// 개인정보유형 뷰
		String result_type_view = (String) session.getAttribute("result_type");
		// 메뉴명 유무
		String scrn_name_view = (String) session.getAttribute("scrn_name");
		// 접속유형 default 세팅
		String mapping_id = (String) session.getAttribute("mapping_id");
		// 다운로드로그 풀스캔연동여부
		String use_fullscan = (String) session.getAttribute("use_fullscan");
		if(mapping_id.equals("Y")) { if ( search.getMapping_id() == null ) search.setMapping_id("code1");
			//if ( search.getMapping_id().equals("codeAll") ) search.setMapping_id("");
			if ( search.getMapping_id().equals("code2") ) search.setMapping_id(null);
		}else {
			if(search.getMapping_id() == null || search.getMapping_id().equals("code2"))
				search.setMapping_id(null);
			else
				search.setMapping_id("code1");
		}
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
	    
	    if (parameters.get("search_from_rp") == null && parameters.get("search_to_rp") == null) {
			Date today = new Date();
			search.initDatesIfEmpty(today, today);
		}
		
		DataModelAndView modelAndView = fileManageSvc.setRegisteredList(search);
		
		modelAndView.addObject("search", search);
		modelAndView.addObject("use_fullscan", use_fullscan);
		modelAndView.addObject("result_type_view", result_type_view);
		modelAndView.addObject("scrn_name_view", scrn_name_view);
		modelAndView.setViewName("registeredList");
		return modelAndView;
	}
	@MngtActHist(log_action = "SELECT", log_message = "[파일대장접속기록관리] 파일대장 접속기록 상세")
	@RequestMapping(value="registeredDetailList.html", method=RequestMethod.POST)
	public DataModelAndView registeredDetailList(@ModelAttribute("search") SearchSearch search, @RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		try{
			String emp_user_name = parameters.get("emp_user_name");
			if(emp_user_name.isEmpty())
				search.setIsSearch("N");
			else
				search.setIsSearch("Y");
		} catch (Exception e) {
			search.setIsSearch("N");
		}
		
		HttpSession session = request.getSession();
		String master = (String)session.getAttribute("master");
		String scrn_name_view = (String) session.getAttribute("scrn_name");
		String use_fullscan = (String) session.getAttribute("use_fullscan");
		
		DataModelAndView modelAndView = fileManageSvc.setRegisteredDetailList(search);
		
		
		modelAndView.addObject("search", search);
		modelAndView.addObject("masterflag", master);
		modelAndView.addObject("scrn_name_view", scrn_name_view);
		modelAndView.addObject("use_fullscan", use_fullscan);
		modelAndView.setViewName("registeredDetailList");
		return modelAndView;
	}
	
	//파일 대장 엑셀 다운로드
	@RequestMapping(value="registeredListExcelDown.html", method={RequestMethod.POST})
	public DataModelAndView addregisteredListExcelDown(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		if ( search.getMapping_id() == null ) search.setMapping_id("code1");
	    if ( search.getMapping_id().equals("code2") ) search.setMapping_id(null);
	    
	    List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_onnara");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
	    AdminUser adminUser = SystemHelper.getAdminUserInfo(request);
		
		search.setLogin_user_id(adminUser.getAdmin_user_id());
		search.setLogin_auth_id(adminUser.getAuth_id());
		
		fileManageSvc.findDownloadLogInqDetail_download(modelAndView, search, request);
	    
		return modelAndView;
	}
	
	@RequestMapping(value="registeredListCSVDown.html", method={RequestMethod.POST})
	public DataModelAndView addregisteredListCSVDown(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.CSV_DOWNLOAD_VIEW);
		
		if ( search.getMapping_id() == null ) search.setMapping_id("code1");
	    if ( search.getMapping_id().equals("code2") ) search.setMapping_id(null);
	    
	    List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_onnara");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
	    AdminUser adminUser = SystemHelper.getAdminUserInfo(request);
		
		search.setLogin_user_id(adminUser.getAdmin_user_id());
		search.setLogin_auth_id(adminUser.getAuth_id());
	    
		fileManageSvc.findDownloadLoginqDetail_downloadCSV(modelAndView, search, request);
		
		return modelAndView;
	}
	
	
	@RequestMapping(value="registeredDetailListExcelDown.html", method={RequestMethod.POST})
	public DataModelAndView addregisteredDetailListExcelDown(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		fileManageSvc.registeredDetailListExcelDown(modelAndView, search, request);
	    
		return modelAndView;
	}
	
	@RequestMapping(value="registeredDetailListCSVDown.html", method={RequestMethod.POST})
	public DataModelAndView addregisteredDetailListCSVDown(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.CSV_DOWNLOAD_VIEW);
		
		fileManageSvc.registeredDetailListCSVDown(modelAndView, search, request);
	    
		return modelAndView;
	}
	
	
	
	
	
	
	
	
}
