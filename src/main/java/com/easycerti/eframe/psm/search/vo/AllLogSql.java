package com.easycerti.eframe.psm.search.vo;

import java.util.List;

import com.easycerti.eframe.common.util.PageInfo;
import com.easycerti.eframe.common.vo.AbstractValueObject;

public class AllLogSql extends AbstractValueObject {
	
	// sql_info
	private Integer sql_seq;
	private String sql;
	private String columns;
	private List<String> columnsList;
	private String collect_yn;
	
	// log_sql
	private Integer log_sql_seq;
	private long log_seq;
	private String params;
	private List<String> paramsList;
	private String proc_datetime;
	
	// sql_result
	private Integer result_seq;
	private String result;
	private List<String> resultList;
	
	private int logSqlPageNum;
	private String proc_datetime_start;
	private String proc_datetime_end;
	private int offsetNum;

	PageInfo logSqlResult = new PageInfo();
	
	
	
	public String getCollect_yn() {
		return collect_yn;
	}

	public void setCollect_yn(String collect_yn) {
		this.collect_yn = collect_yn;
	}

	public PageInfo getLogSqlResult() {
		return logSqlResult;
	}

	public void setLogSqlResult(PageInfo logSqlResult) {
		this.logSqlResult = logSqlResult;
	}

	public List<String> getResultList() {
		return resultList;
	}

	public void setResultList(List<String> resultList) {
		this.resultList = resultList;
	}

	public List<String> getColumnsList() {
		return columnsList;
	}

	public void setColumnsList(List<String> columnsList) {
		this.columnsList = columnsList;
	}

	public List<String> getParamsList() {
		return paramsList;
	}

	public void setParamsList(List<String> paramsList) {
		this.paramsList = paramsList;
	}

	public Integer getSql_seq() {
		return sql_seq;
	}

	public void setSql_seq(Integer sql_seq) {
		this.sql_seq = sql_seq;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public String getColumns() {
		return columns;
	}

	public void setColumns(String columns) {
		this.columns = columns;
	}

	public Integer getLog_sql_seq() {
		return log_sql_seq;
	}

	public void setLog_sql_seq(Integer log_sql_seq) {
		this.log_sql_seq = log_sql_seq;
	}

	public long getLog_seq() {
		return log_seq;
	}

	public void setLog_seq(long log_seq) {
		this.log_seq = log_seq;
	}

	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}

	public String getProc_datetime() {
		return proc_datetime;
	}

	public void setProc_datetime(String proc_datetime) {
		this.proc_datetime = proc_datetime;
	}

	public Integer getResult_seq() {
		return result_seq;
	}

	public void setResult_seq(Integer result_seq) {
		this.result_seq = result_seq;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public int getLogSqlPageNum() {
		return logSqlPageNum;
	}

	public void setLogSqlPageNum(int logSqlPageNum) {
		this.logSqlPageNum = logSqlPageNum;
	}

	public String getProc_datetime_start() {
		proc_datetime_start = proc_datetime+" 000000";
		return proc_datetime_start;
	}

	public String getProc_datetime_end() {
		proc_datetime_end = proc_datetime+" 232323";
		return proc_datetime_end;
	}

	public int getOffsetNum() {
		offsetNum = logSqlPageNum-1;
		return offsetNum;
	}
	
	
	
	
}
