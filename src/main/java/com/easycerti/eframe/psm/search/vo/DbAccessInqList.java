package com.easycerti.eframe.psm.search.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.AbstractValueObject;
/**
 * 
 * 설명 : DB접근로그조회 리스트 Bean
 * @author tjlee
 * @since 2015. 4. 22.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 22.           tjlee            최초 생성
 *
 * </pre>
 */
public class DbAccessInqList extends AbstractValueObject{

	// DB접근로그조회 리스트
	private List<DbAccessInq> dbAccessInq = null;

	
	public DbAccessInqList(){
		
	}
	
	public DbAccessInqList(List<DbAccessInq> dbAccessInq){
		this.dbAccessInq = dbAccessInq;
	}
	
	public DbAccessInqList(List<DbAccessInq> dbAccessInq, String page_total_count){
		this.dbAccessInq = dbAccessInq;
		setPage_total_count(page_total_count);
	}

	public List<DbAccessInq> getDbAccessInq() {
		return dbAccessInq;
	}

	public void setDbAccessInq(List<DbAccessInq> dbAccessInq) {
		this.dbAccessInq = dbAccessInq;
	}
}
