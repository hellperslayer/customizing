package com.easycerti.eframe.psm.search.web;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.core.web.RequestWrapper;
import com.easycerti.eframe.psm.search.dao.ExtrtCondbyInqDao;
import com.easycerti.eframe.psm.search.service.AllLogInqSvc;
import com.easycerti.eframe.psm.search.vo.AllLogSql;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.system_management.dao.OptionSettingDao;
/**
 * 
 * 설명 : 전체로그조회 Controller
 * @author tjlee
 * @since 2015. 4. 22.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 22.           tjlee            최초 생성
 *
 * </pre>
 */
@Controller
@RequestMapping("/allLogInq/*")
public class AllLogInqCtrl {

	@Autowired
	private AllLogInqSvc allLogInqSvc;
	
	@Autowired
	private ExtrtCondbyInqDao extrtCondbyInqDao;
	
	@Autowired
	private OptionSettingDao optionSettingDao;
	
	@Value("#{configProperties.excel_type}")
	private String excel_type;
	/**
	 * 전체로그조회 List
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[접속기록] 접속기록조회")
	@RequestMapping(value={"list.html"}, method={RequestMethod.POST})
	public DataModelAndView findAllLogInqList(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request,
			@RequestParam(value = "sub_menu_id", defaultValue = "") String sub_menu_id,
			@RequestParam(value = "menutitle",defaultValue="")String menutitle) 
	{
		// 보고서에서 차트 클릭시 매핑
		if(parameters.get("search_from_rp") != null && search.getSearch_from() == null && !search.getSearch_from().equals("")) {search.setSearch_from(parameters.get("search_from_rp"));}
		if(parameters.get("search_to_rp") != null && search.getSearch_to() == null && !search.getSearch_to().equals("")) {search.setSearch_to(parameters.get("search_to_rp"));}
		if(parameters.get("system_seq_rp") != null && search.getSystem_seq() == null && !search.getSystem_seq().equals("")) {search.setSystem_seq(parameters.get("system_seq_rp"));}
		if(parameters.get("dept_name_rp") != null  && search.getDept_name() == null && !search.getDept_name().equals("")) {search.setDept_name(parameters.get("dept_name_rp"));}
		if(parameters.get("privacyType_rp") != null && search.getPrivacyType() == null && !search.getPrivacyType().equals("")) {search.setPrivacyType(parameters.get("privacyType_rp"));}
		if(parameters.get("req_type_rp") != null && search.getReq_type() == null && !search.getReq_type().equals("")) {search.setReq_type(parameters.get("req_type_rp"));}
		if(parameters.get("emp_user_id_rp") != null && search.getEmp_user_id() == null && !search.getEmp_user_id().equals("")) {search.setEmp_user_id(parameters.get("emp_user_id_rp"));}
		
		if(parameters.get("main_menu_id") != null && search.getMain_menu_id() == null && !search.getMain_menu_id().equals("")) { search.setMain_menu_id(parameters.get("main_menu_id"));}
		if(parameters.get("sub_menu_id") != null && search.getSub_menu_id() == null && !search.getSub_menu_id().equals("")) { search.setSub_menu_id(parameters.get("sub_menu_id"));}
		if(parameters.get("system_seq") != null && search.getSystem_seq() == null && !search.getSystem_seq().equals("")) { search.setSystem_seq(parameters.get("system_seq"));}
		if(parameters.get("menuId") != null && search.getMenuId() == null && !search.getMenuId().equals("")) { search.setMenuId(parameters.get("menuId"));}
		if(parameters.get("isSearch") != null && search.getIsSearch() == null && !search.getIsSearch().equals("")) { search.setIsSearch(parameters.get("isSearch"));}
		
		if(search.getReq_url() != null) {
			search.setReq_url(RequestWrapper.decodeXSS(search.getReq_url()));
		}
		HttpSession session = request.getSession();
		// 개인정보유형 뷰
		String result_type_view = (String) session.getAttribute("result_type");
		// 메뉴명 유무
		String scrn_name_view = (String) session.getAttribute("scrn_name");
		// 접속유형 default 세팅
		String mapping_id = (String) session.getAttribute("mapping_id");
		if(mapping_id.equals("Y")) {
			if ( search.getMapping_id() == null ) search.setMapping_id("code1");
			//if ( search.getMapping_id().equals("codeAll") ) search.setMapping_id("");
			if ( search.getMapping_id().equals("code2") ) search.setMapping_id(null);
		}else {
			if(search.getMapping_id() == null || search.getMapping_id().equals("code2"))
				search.setMapping_id(null);
			else
				search.setMapping_id("code1");
		}
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
	    
		if (parameters.get("search_from_rp") == null && parameters.get("search_to_rp") == null) {
			Date today = new Date();
			search.initDatesIfEmpty(today, today);
		}
		
		DataModelAndView modelAndView = allLogInqSvc.findAllLogInqList(search);
		
		String ui_type = (String)session.getAttribute("ui_type");
		if (ui_type != null && ui_type.length() > 0) {
			modelAndView.addObject("ui_type", ui_type);
		}
		
		//modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("menutitle",menutitle);
		modelAndView.addObject("sub_menu_id", sub_menu_id);
		modelAndView.addObject("search", search);
		modelAndView.addObject("result_type_view", result_type_view);
		modelAndView.addObject("scrn_name_view", scrn_name_view);
		
		String biz_log_file = (String) session.getAttribute("sbiz_log_file");
		modelAndView.addObject("biz_log_file", biz_log_file);
		
		modelAndView.setViewName("allLogInqList");
		
		return modelAndView;
	}
	
	
	
	/**
	 * 전체로그조회상세List
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[접속기록조회] 전체로그상세")
	@RequestMapping(value={"detail.html"}, method={RequestMethod.POST,RequestMethod.GET})
	public DataModelAndView findAllLogInqDetail(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		//전체로그조회 상세 리스트 페이징 page_num 세팅
		if(parameters.get("allLogInqDetail_page_num")!=null){ search.getAllLogInqDetail().setPage_num(Integer.valueOf(parameters.get("allLogInqDetail_page_num")));	}
		//추출조건별조회 상세 리스트 페이징 page_num 세팅
		if(parameters.get("extrtCondbyInqDetail_page_num")!=null){ search.getExtrtCondbyInqDetail().setPage_num(Integer.valueOf(parameters.get("extrtCondbyInqDetail_page_num")));	}
		//위험도별조회 상세 리스트 페이징 page_num 세팅
		if(parameters.get("empDetailInqDetail_page_num")!=null){ search.getEmpDetailInqDetail().setPage_num(Integer.valueOf(parameters.get("empDetailInqDetail_page_num")));}
		
		if(parameters.get("main_menu_id") != null && search.getMain_menu_id() == null && !search.getMain_menu_id().equals("")) { search.setMain_menu_id(parameters.get("main_menu_id"));}
		if(parameters.get("sub_menu_id") != null && search.getSub_menu_id() == null && !search.getSub_menu_id().equals("")) { search.setSub_menu_id(parameters.get("sub_menu_id"));}
		if(parameters.get("system_seq") != null && search.getSystem_seq() == null && !search.getSystem_seq().equals("")) { search.setSystem_seq(parameters.get("system_seq"));}
		if(parameters.get("menuId") != null && search.getMenuId() == null && !search.getMenuId().equals("")) { search.setMenuId(parameters.get("menuId"));}
		if(parameters.get("isSearch") != null && search.getIsSearch() == null && !search.getIsSearch().equals("")) { search.setIsSearch(parameters.get("isSearch"));}
					
		//List<ExtrtCondbyInqDetailChart> data1 = new ArrayList<>();		
		
		//2017.06.29
		try{
			String emp_user_name = parameters.get("emp_user_name");
			if(emp_user_name.isEmpty())
				search.setIsSearch("N");
			else
				search.setIsSearch("Y");
		} catch (Exception e) {
			search.setIsSearch("N");
		}
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
		/*search.setProc_date(parameters.get("detailProcDate"));
		search.setProc_time(parameters.get("detailProcTime"));
		search.setResult_type(parameters.get("result_type"));
		search.setEmp_user_name(parameters.get("user_name"));
		
		long log_seq = allLogInqSvc.findAllLogInqDetailLogSeq(search);
		search.setLog_seq(log_seq);
		search.setDetailLogSeq(Integer.toString(log_seq));*/
		
		
		String result_owner_flag = optionSettingDao.getResultOwnerFlag();
		  
		search.setResult_owner_flag(result_owner_flag);
		 
		DataModelAndView modelAndView = allLogInqSvc.findAllLogInqDetail(search, request);
			
		//modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		
		HttpSession session = request.getSession();
		String master = (String)session.getAttribute("master");
		modelAndView.addObject("masterflag", master);
		
		String scrn_name_view = (String) session.getAttribute("scrn_name");
		modelAndView.addObject("scrn_name_view", scrn_name_view);
		
		modelAndView.setViewName("allLogInqDetail");

		return modelAndView;
	}

	/**
	 * 20201216 신한카드 비인가 전체로그조회상세List
	 */
//	@MngtActHist(log_action = "SELECT", log_message = "[접속기록조회] 전체로그상세")
	@RequestMapping(value={"detail_shcd.html"}, method={RequestMethod.POST,RequestMethod.GET})
	public DataModelAndView findAllLogInqDetail_shcd(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		//전체로그조회 상세 리스트 페이징 page_num 세팅
		if(parameters.get("allLogInqDetail_page_num")!=null){ search.getAllLogInqDetail().setPage_num(Integer.valueOf(parameters.get("allLogInqDetail_page_num")));	}
		//추출조건별조회 상세 리스트 페이징 page_num 세팅
		if(parameters.get("extrtCondbyInqDetail_page_num")!=null){ search.getExtrtCondbyInqDetail().setPage_num(Integer.valueOf(parameters.get("extrtCondbyInqDetail_page_num")));	}
		//위험도별조회 상세 리스트 페이징 page_num 세팅
		if(parameters.get("empDetailInqDetail_page_num")!=null){ search.getEmpDetailInqDetail().setPage_num(Integer.valueOf(parameters.get("empDetailInqDetail_page_num")));}
		
		if(parameters.get("main_menu_id") != null && search.getMain_menu_id() == null && !search.getMain_menu_id().equals("")) { search.setMain_menu_id(parameters.get("main_menu_id"));}
		if(parameters.get("sub_menu_id") != null && search.getSub_menu_id() == null && !search.getSub_menu_id().equals("")) { search.setSub_menu_id(parameters.get("sub_menu_id"));}
		if(parameters.get("system_seq") != null && search.getSystem_seq() == null && !search.getSystem_seq().equals("")) { search.setSystem_seq(parameters.get("system_seq"));}
		if(parameters.get("menuId") != null && search.getMenuId() == null && !search.getMenuId().equals("")) { search.setMenuId(parameters.get("menuId"));}
		if(parameters.get("isSearch") != null && search.getIsSearch() == null && !search.getIsSearch().equals("")) { search.setIsSearch(parameters.get("isSearch"));}
		
		//List<ExtrtCondbyInqDetailChart> data1 = new ArrayList<>();		
		
		//2017.06.29
		try{
			String emp_user_name = parameters.get("emp_user_name");
			if(emp_user_name.isEmpty())
				search.setIsSearch("N");
			else
				search.setIsSearch("Y");
		} catch (Exception e) {
			search.setIsSearch("N");
		}
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
		if(list != null) {
			search.setAuth_idsList(list);
		}else {
			list = (List<String>) request.getSession().getAttribute("auth_ids_list");
			search.setAuth_idsList(list);
		}
		/*search.setProc_date(parameters.get("detailProcDate"));
		search.setProc_time(parameters.get("detailProcTime"));
		search.setResult_type(parameters.get("result_type"));
		search.setEmp_user_name(parameters.get("user_name"));
		
		long log_seq = allLogInqSvc.findAllLogInqDetailLogSeq(search);
		search.setLog_seq(log_seq);
		search.setDetailLogSeq(Integer.toString(log_seq));*/
		
		
		String result_owner_flag = optionSettingDao.getResultOwnerFlag();
		
		search.setResult_owner_flag(result_owner_flag);
		
		DataModelAndView modelAndView = allLogInqSvc.findAllLogInqDetail(search, request);
		
		//modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		
		HttpSession session = request.getSession();
		String master = (String)session.getAttribute("master");
		modelAndView.addObject("masterflag", master);
		
		String scrn_name_view = (String) session.getAttribute("scrn_name");
		modelAndView.addObject("scrn_name_view", scrn_name_view);
		
		modelAndView.setViewName("allLogInqDetail_shcd");
		
		return modelAndView;
	}
	
	
	/**
	 * 소명조회상세List -> view
	 */
	@RequestMapping(value={"allLogInqSummonDetailView.html"}, method={RequestMethod.POST,RequestMethod.GET})
	public DataModelAndView findSummonAllLogInqDetail(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {

		if(parameters.get("main_menu_id") != null && search.getMain_menu_id() == null && !search.getMain_menu_id().equals("")) { search.setMain_menu_id(parameters.get("main_menu_id"));}
		if(parameters.get("sub_menu_id") != null && search.getSub_menu_id() == null && !search.getSub_menu_id().equals("")) { search.setSub_menu_id(parameters.get("sub_menu_id"));}
		if(parameters.get("system_seq") != null && search.getSystem_seq() == null && !search.getSystem_seq().equals("")) { search.setSystem_seq(parameters.get("system_seq"));}
		if(parameters.get("menuId") != null && search.getMenuId() == null && !search.getMenuId().equals("")) { search.setMenuId(parameters.get("menuId"));}
		if(parameters.get("isSearch") != null && search.getIsSearch() == null && !search.getIsSearch().equals("")) { search.setIsSearch(parameters.get("isSearch"));}
		
		//전체로그조회 상세 리스트 페이징 page_num 세팅
		if(parameters.get("allLogInqDetail_page_num")!=null){
			search.getAllLogInqDetail().setPage_num(Integer.valueOf(parameters.get("allLogInqDetail_page_num")));
		}
		
		//추출조건별조회 상세 리스트 페이징 page_num 세팅
		if(parameters.get("extrtCondbyInqDetail_page_num")!=null){
			search.getExtrtCondbyInqDetail().setPage_num(Integer.valueOf(parameters.get("extrtCondbyInqDetail_page_num")));
		}
		
		//위험도별조회 상세 리스트 페이징 page_num 세팅
		if(parameters.get("empDetailInqDetail_page_num")!=null){
			search.getEmpDetailInqDetail().setPage_num(Integer.valueOf(parameters.get("empDetailInqDetail_page_num")));
		}
					
		DataModelAndView modelAndView = allLogInqSvc.findSummonAllLogInqDetail(search);
			

		modelAndView.setViewName("allLogInqDetail");
		
		
		//modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("allLogInqSummonDetailView");
	
		return modelAndView;
	}
	
	/*개인정보 제외 추가*/
	@RequestMapping(value="add.html",method={RequestMethod.POST})
	public DataModelAndView addPrivacy(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		String[] args = {"misdetect_pattern", "privacy_type"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
				
		DataModelAndView modelAndView = allLogInqSvc.addPrivacy(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	/*개인정보 제외 복원*/
	@RequestMapping(value="remove.html",method={RequestMethod.POST})
	public DataModelAndView removePrivacy(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		String[] args = {"misdetect_pattern", "privacy_type"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
				
		DataModelAndView modelAndView = allLogInqSvc.removePrivacy(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@RequestMapping(value="addGoogle.html",method={RequestMethod.POST})
	public DataModelAndView addGoogle(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
				
		DataModelAndView modelAndView = allLogInqSvc.addGoogle(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@RequestMapping(value="updateGoogle.html",method={RequestMethod.POST})
	public DataModelAndView updateGoogle(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
				
		DataModelAndView modelAndView = allLogInqSvc.updateGoogle(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@RequestMapping(value="deleteGoogle.html",method={RequestMethod.POST})
	public DataModelAndView deleteGoogle(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
				
		DataModelAndView modelAndView = allLogInqSvc.deleteGoogle(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@RequestMapping(value="popupList.html",method={RequestMethod.POST})
	public DataModelAndView findDashPopupList(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		DataModelAndView modelAndView = allLogInqSvc.findDashPopupList(parameters);
		modelAndView.setViewName("dashboard_list_popup");
		
		return modelAndView;
	}
	
	

	/**
	 * 전체로그조회 엑셀 다운로드
	 */
	@RequestMapping(value="download.html", method={RequestMethod.POST})
	public DataModelAndView addAllLogInqList_download(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		if ( search.getMapping_id() == null ) search.setMapping_id("code1");
	    if ( search.getMapping_id().equals("code2") ) search.setMapping_id(null);
	    
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    search.setAuth_idsList(list);
	    
	    if(excel_type.equals("basic"))
	    	allLogInqSvc.findAllLogInqList_download(modelAndView, search, request);
	    else if(excel_type.equals("extend"))
	    	allLogInqSvc.findAllLogInqList_download_extend(modelAndView, search, request);
		
		return modelAndView;
	}
	
	/**
	 * 전체로그조회 엑셀(CSV) 다운로드
	 */
	@RequestMapping(value="downloadCSV.html", method={RequestMethod.POST})
	public DataModelAndView addAllLogInqList_downloadCSV(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.CSV_DOWNLOAD_VIEW);
		
		if ( search.getMapping_id() == null ) search.setMapping_id("code1");
	    if ( search.getMapping_id().equals("code2") ) search.setMapping_id(null);
	    
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    search.setAuth_idsList(list);
	    
	    if(excel_type.equals("basic"))
	    	allLogInqSvc.findAllLogInqList_downloadCSV(modelAndView, search, request);
	    else if(excel_type.equals("extend"))
	    	allLogInqSvc.findAllLogInqList_downloadCSV_extend(modelAndView, search, request);
		
		return modelAndView;
	}
	@RequestMapping(value="downloadDetail.html", method={RequestMethod.POST})
	public DataModelAndView addAllLogInqList_downloadDetail(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		allLogInqSvc.findAllLoginqDetail_download(modelAndView, search, request);
	    
		return modelAndView;
	}
	@RequestMapping(value="downloadDetailCSV.html", method={RequestMethod.POST})
	public DataModelAndView addAllLogInqList_downloadDetailCSV(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.CSV_DOWNLOAD_VIEW);
		
		allLogInqSvc.findAllLoginqDetail_downloadCSV(modelAndView, search, request);
	    
		return modelAndView;
	}
	
	@MngtActHist(log_action = "SELECT", log_message = "[접속기록] 정보주체자 조회")
	@RequestMapping(value={"readInfoList.html"}, method={RequestMethod.POST})
	public DataModelAndView findReadInfoList(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request,
			@RequestParam(value = "sub_menu_id", defaultValue = "") String sub_menu_id,
			@RequestParam(value = "menutitle",defaultValue="")String menutitle) 
	{
		if(parameters.get("main_menu_id") != null && search.getMain_menu_id() == null && !search.getMain_menu_id().equals("")) { search.setMain_menu_id(parameters.get("main_menu_id"));}
		if(parameters.get("sub_menu_id") != null && search.getSub_menu_id() == null && !search.getSub_menu_id().equals("")) { search.setSub_menu_id(parameters.get("sub_menu_id"));}
		if(parameters.get("system_seq") != null && search.getSystem_seq() == null && !search.getSystem_seq().equals("")) { search.setSystem_seq(parameters.get("system_seq"));}
		if(parameters.get("menuId") != null && search.getMenuId() == null && !search.getMenuId().equals("")) { search.setMenuId(parameters.get("menuId"));}
		if(parameters.get("isSearch") != null && search.getIsSearch() == null && !search.getIsSearch().equals("")) { search.setIsSearch(parameters.get("isSearch"));}
		HttpSession session = request.getSession();
		// 접속유형 default 세팅
		String mapping_id = (String) session.getAttribute("mapping_id");
		if(mapping_id.equals("Y")) {
			if ( search.getMapping_id() == null ) search.setMapping_id("code1");
			//if ( search.getMapping_id().equals("codeAll") ) search.setMapping_id("");
			if ( search.getMapping_id().equals("code2") ) search.setMapping_id(null);
		}else {
			if(search.getMapping_id() == null || search.getMapping_id().equals("code2"))
				search.setMapping_id(null);
			else
				search.setMapping_id("code1");
		}
		search.initDatesWithDefaultIfEmpty(7);
		search.setPrivacy(parameters.get("privacy"));
 		DataModelAndView modelAndView = allLogInqSvc.findReadInfoList(search);
		
		//modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("menutitle",menutitle);
		modelAndView.addObject("sub_menu_id", sub_menu_id);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("readInfoList");

		return modelAndView;
	}
	
	@RequestMapping(value={"listByReqType.html"}, method={RequestMethod.POST})
	public DataModelAndView findAllLogInqListByReqType(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request,
			@RequestParam(value = "sub_menu_id", defaultValue = "") String sub_menu_id,
			@RequestParam(value = "menutitle",defaultValue="")String menutitle) 
	{
		if(parameters.get("main_menu_id") != null && search.getMain_menu_id() == null && !search.getMain_menu_id().equals("")) { search.setMain_menu_id(parameters.get("main_menu_id"));}
		if(parameters.get("sub_menu_id") != null && search.getSub_menu_id() == null && !search.getSub_menu_id().equals("")) { search.setSub_menu_id(parameters.get("sub_menu_id"));}
		if(parameters.get("system_seq") != null && search.getSystem_seq() == null && !search.getSystem_seq().equals("")) { search.setSystem_seq(parameters.get("system_seq"));}
		if(parameters.get("menuId") != null && search.getMenuId() == null && !search.getMenuId().equals("")) { search.setMenuId(parameters.get("menuId"));}
		if(parameters.get("isSearch") != null && search.getIsSearch() == null && !search.getIsSearch().equals("")) { search.setIsSearch(parameters.get("isSearch"));}
		
		Date today = new Date();
		
		search.initDatesIfEmpty(today, today);
		
		DataModelAndView modelAndView = allLogInqSvc.findAllLogInqListByReqType(search);
		
		//modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("menutitle",menutitle);
		modelAndView.addObject("sub_menu_id", sub_menu_id);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("allLogInqListByReqType");

		return modelAndView;
	}
	
	@RequestMapping(value="downloadByReqType.html", method={RequestMethod.POST})
	public DataModelAndView findAllLogInqList_downloadByReqType(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		allLogInqSvc.findAllLogInqList_downloadByReqType(modelAndView, search, request);
		
		return modelAndView;
	}

	@RequestMapping(value={"checkPrivacyInfo.html"}, method={RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public JSONArray checkPrivacyInfo(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {

		JSONArray jArray = new JSONArray();
		search.setPrivacy(parameters.get("privacy"));
		try {
			JSONObject modelAndView = allLogInqSvc.checkPrivacyInfo(search);
			jArray.add(modelAndView);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new ESException("SYS501J");
		}
		
		
		

		return jArray;
	}
	
	@RequestMapping(value={"findPrivacyInfo.html"}, method={RequestMethod.POST,RequestMethod.GET})
	public DataModelAndView findPrivacyInfo(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = allLogInqSvc.findPrivacyInfo(search);
		try {
			String privacy = new String(parameters.get("privacy").getBytes("iso-8859-1"), "utf-8");
			String name = new String(parameters.get("name").getBytes("iso-8859-1"), "utf-8");
			parameters.put("privacy", privacy);
			parameters.put("name", name);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("allLogInqPrivacyInfo");

		return modelAndView;
	}
	
	@RequestMapping(value={"findBizLog80Popup.html"}, method={RequestMethod.POST,RequestMethod.GET})
	public DataModelAndView findBizLog80Popup(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		search.setLog_seq(Integer.parseInt(parameters.get("log_seq")));
		
		DataModelAndView modelAndView = allLogInqSvc.findBizLog80Popup(search);

		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("bizLog80Popup");

		return modelAndView;
	}
	
	@RequestMapping(value="fileDownload.html",method={RequestMethod.POST})
	public DataModelAndView fileDownload(@RequestParam Map<String, String> parameters){
		
		DataModelAndView modelAndView = new DataModelAndView();
		//String path = "c:\\fileDownload\\" + parameters.get("fileName");
		String path = parameters.get("file_path");
		File file = new File(path);
		modelAndView.addObject("downloadFile", file);
		modelAndView.addObject("orginName", parameters.get("file_name"));
		
		modelAndView.setViewName(CommonResource.FILE_DOWNLOAD_VIEW);
		return modelAndView;
	}
	
	@RequestMapping(value="getResultType.html", method={RequestMethod.POST})
	public DataModelAndView getResultType(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "log_seq") String log_seq,
			@RequestParam(value = "proc_date") String proc_date,
			@RequestParam(value = "system_seq") String system_seq,
			@RequestParam(value = "result_owner") String result_owner) throws Exception {

		SearchSearch search = new SearchSearch();
		search.setDetailLogSeq(log_seq);
		search.setDetailProcDate(proc_date);
		search.setSystem_seq(system_seq);
		search.setResult_owner(result_owner);
		DataModelAndView modelAndView = allLogInqSvc.getResultType(search);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	} 
	
	//개인정보 임계치
	@RequestMapping(value="thresholdSetting.html", method=RequestMethod.POST)
	public DataModelAndView thresholdSetting(@ModelAttribute("search") SearchSearch search, @RequestParam Map<String, String>parameters, HttpServletRequest request) {
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
		if(list != null)
			search.setAuth_idsList(list);
		else {
			list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
		}	
		
		DataModelAndView modelAndView = allLogInqSvc.thresholdSetting(search);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("thresholdSetting");
		return modelAndView;
	}
	
	//개인정보 임계치 설정
	@RequestMapping(value="updateThreshold.html", method=RequestMethod.POST)
	public DataModelAndView updateThreshold(@ModelAttribute("search") SearchSearch search, @RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		//개인정보 임계치 설정 변경
		//int updateResult = thresholdSettingSvc.updateThreshold(search, param);
		DataModelAndView modelAndView = allLogInqSvc.updateThreshold(search);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	@RequestMapping(value="logSqlDownloadExcel.html", method=RequestMethod.POST)
	public DataModelAndView logSqlDownloadExcel(String log_seq, String log_sql_seq, String proc_date) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW2);
		AllLogSql allLogSql = new AllLogSql();
		allLogSql.setLog_seq(Long.parseLong(log_seq));
		allLogSql.setProc_datetime(proc_date);
		allLogInqSvc.logSqlDownloadExcel(modelAndView, allLogSql);
		
		return modelAndView;
	}
	
	@RequestMapping(value="logSqlDownloadCsv.html", method=RequestMethod.POST)
	public DataModelAndView logSqlDownloadCsv(String log_seq, String log_sql_seq, String proc_date) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.CSV_DOWNLOAD_VIEW);
		AllLogSql allLogSql = new AllLogSql();
		allLogSql.setLog_seq(Long.parseLong(log_seq));
		allLogSql.setLog_sql_seq(Integer.parseInt(log_sql_seq));
		allLogSql.setProc_datetime(proc_date);
		allLogInqSvc.logSqlDownloadCsv(modelAndView, allLogSql);
		return modelAndView;
	}
	
	@RequestMapping(value="updateSqlCollectYn.html", method=RequestMethod.POST)
	@ResponseBody
	public String updateSqlCollectYn(String sql_seq, String collect_yn) {
		AllLogSql allLogSql = new AllLogSql();
		allLogSql.setSql_seq(Integer.parseInt(sql_seq));;
		allLogSql.setCollect_yn(collect_yn);
		return allLogInqSvc.updateSqlCollectYn(allLogSql);
	}
	
	@RequestMapping(value={"list_kdic.html"}, method={RequestMethod.POST})
	public DataModelAndView findAllLogInqList_kdic(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request,
			@RequestParam(value = "sub_menu_id", defaultValue = "") String sub_menu_id,
			@RequestParam(value = "menutitle",defaultValue="")String menutitle) 
	{
		// 보고서에서 차트 클릭시 매핑
		if(parameters.get("search_from_rp") != null && search.getSearch_from() == null && !search.getSearch_from().equals("")) {search.setSearch_from(parameters.get("search_from_rp"));}
		if(parameters.get("search_to_rp") != null && search.getSearch_to() == null && !search.getSearch_to().equals("")) {search.setSearch_to(parameters.get("search_to_rp"));}
		if(parameters.get("system_seq_rp") != null && search.getSystem_seq() == null && !search.getSystem_seq().equals("")) {search.setSystem_seq(parameters.get("system_seq_rp"));}
		if(parameters.get("dept_name_rp") != null  && search.getDept_name() == null && !search.getDept_name().equals("")) {search.setDept_name(parameters.get("dept_name_rp"));}
		if(parameters.get("privacyType_rp") != null && search.getPrivacyType() == null && !search.getPrivacyType().equals("")) {search.setPrivacyType(parameters.get("privacyType_rp"));}
		if(parameters.get("req_type_rp") != null && search.getReq_type() == null && !search.getReq_type().equals("")) {search.setReq_type(parameters.get("req_type_rp"));}
		if(parameters.get("emp_user_id_rp") != null && search.getEmp_user_id() == null && !search.getEmp_user_id().equals("")) {search.setEmp_user_id(parameters.get("emp_user_id_rp"));}
		
		if(parameters.get("main_menu_id") != null && search.getMain_menu_id() == null && !search.getMain_menu_id().equals("")) { search.setMain_menu_id(parameters.get("main_menu_id"));}
		if(parameters.get("sub_menu_id") != null && search.getSub_menu_id() == null && !search.getSub_menu_id().equals("")) { search.setSub_menu_id(parameters.get("sub_menu_id"));}
		if(parameters.get("system_seq") != null && search.getSystem_seq() == null && !search.getSystem_seq().equals("")) { search.setSystem_seq(parameters.get("system_seq"));}
		if(parameters.get("menuId") != null && search.getMenuId() == null && !search.getMenuId().equals("")) { search.setMenuId(parameters.get("menuId"));}
		if(parameters.get("isSearch") != null && search.getIsSearch() == null && !search.getIsSearch().equals("")) { search.setIsSearch(parameters.get("isSearch"));}
		
		if(search.getReq_url() != null) {
			search.setReq_url(RequestWrapper.decodeXSS(search.getReq_url()));
		}
		HttpSession session = request.getSession();
		// 개인정보유형 뷰
		String result_type_view = (String) session.getAttribute("result_type");
		// 메뉴명 유무
		String scrn_name_view = (String) session.getAttribute("scrn_name");
		// 접속유형 default 세팅
		String mapping_id = (String) session.getAttribute("mapping_id");
		if(mapping_id.equals("Y")) {
			if ( search.getMapping_id() == null ) search.setMapping_id("code1");
			//if ( search.getMapping_id().equals("codeAll") ) search.setMapping_id("");
			if ( search.getMapping_id().equals("code2") ) search.setMapping_id(null);
		}else {
			if(search.getMapping_id() == null || search.getMapping_id().equals("code2"))
				search.setMapping_id(null);
			else
				search.setMapping_id("code1");
		}
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
	    
		if (parameters.get("search_from_rp") == null && parameters.get("search_to_rp") == null) {
			Date today = new Date();
			search.initDatesIfEmpty(today, today);
		}
		
		DataModelAndView modelAndView = allLogInqSvc.findAllLogInqList_kdic(search);
		
		String ui_type = (String)session.getAttribute("ui_type");
		if (ui_type != null && ui_type.length() > 0) {
			modelAndView.addObject("ui_type", ui_type);
		}
		
		//modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("menutitle",menutitle);
		modelAndView.addObject("sub_menu_id", sub_menu_id);
		modelAndView.addObject("search", search);
		modelAndView.addObject("result_type_view", result_type_view);
		modelAndView.addObject("scrn_name_view", scrn_name_view);
		
		String biz_log_file = (String) session.getAttribute("sbiz_log_file");
		modelAndView.addObject("biz_log_file", biz_log_file);
		
		modelAndView.setViewName("allLogInqList_kdic");
		
		return modelAndView;
	}
	
	@RequestMapping(value={"detail_kdic.html"}, method={RequestMethod.POST,RequestMethod.GET})
	public DataModelAndView findAllLogInqDetail_kdic(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		//전체로그조회 상세 리스트 페이징 page_num 세팅
		if(parameters.get("allLogInqDetail_page_num")!=null){ search.getAllLogInqDetail().setPage_num(Integer.valueOf(parameters.get("allLogInqDetail_page_num")));	}
		//추출조건별조회 상세 리스트 페이징 page_num 세팅
		if(parameters.get("extrtCondbyInqDetail_page_num")!=null){ search.getExtrtCondbyInqDetail().setPage_num(Integer.valueOf(parameters.get("extrtCondbyInqDetail_page_num")));	}
		//위험도별조회 상세 리스트 페이징 page_num 세팅
		if(parameters.get("empDetailInqDetail_page_num")!=null){ search.getEmpDetailInqDetail().setPage_num(Integer.valueOf(parameters.get("empDetailInqDetail_page_num")));}
		
		if(parameters.get("main_menu_id") != null && search.getMain_menu_id() == null && !search.getMain_menu_id().equals("")) { search.setMain_menu_id(parameters.get("main_menu_id"));}
		if(parameters.get("sub_menu_id") != null && search.getSub_menu_id() == null && !search.getSub_menu_id().equals("")) { search.setSub_menu_id(parameters.get("sub_menu_id"));}
		if(parameters.get("system_seq") != null && search.getSystem_seq() == null && !search.getSystem_seq().equals("")) { search.setSystem_seq(parameters.get("system_seq"));}
		if(parameters.get("menuId") != null && search.getMenuId() == null && !search.getMenuId().equals("")) { search.setMenuId(parameters.get("menuId"));}
		if(parameters.get("isSearch") != null && search.getIsSearch() == null && !search.getIsSearch().equals("")) { search.setIsSearch(parameters.get("isSearch"));}
					
		//List<ExtrtCondbyInqDetailChart> data1 = new ArrayList<>();		
		
		//2017.06.29
		try{
			String emp_user_name = parameters.get("emp_user_name");
			if(emp_user_name.isEmpty())
				search.setIsSearch("N");
			else
				search.setIsSearch("Y");
		} catch (Exception e) {
			search.setIsSearch("N");
		}
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
		/*search.setProc_date(parameters.get("detailProcDate"));
		search.setProc_time(parameters.get("detailProcTime"));
		search.setResult_type(parameters.get("result_type"));
		search.setEmp_user_name(parameters.get("user_name"));
		
		long log_seq = allLogInqSvc.findAllLogInqDetailLogSeq(search);
		search.setLog_seq(log_seq);
		search.setDetailLogSeq(Integer.toString(log_seq));*/
		
		
		String result_owner_flag = optionSettingDao.getResultOwnerFlag();
		  
		search.setResult_owner_flag(result_owner_flag);
		 
		DataModelAndView modelAndView = allLogInqSvc.findAllLogInqDetail_kdic(search, request);
			
		//modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		
		HttpSession session = request.getSession();
		String master = (String)session.getAttribute("master");
		modelAndView.addObject("masterflag", master);
		
		String scrn_name_view = (String) session.getAttribute("scrn_name");
		modelAndView.addObject("scrn_name_view", scrn_name_view);
		
		modelAndView.setViewName("allLogInqDetail_kdic");

		return modelAndView;
	}
}
