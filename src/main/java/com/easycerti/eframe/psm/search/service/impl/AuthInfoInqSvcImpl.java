package com.easycerti.eframe.psm.search.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.search.dao.ExtrtCondbyInqDao;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.search.dao.AuthInfoInqDao;
import com.easycerti.eframe.psm.search.service.AuthInfoInqSvc;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInqDetailChart;
import com.easycerti.eframe.psm.search.vo.AllLogInq;
import com.easycerti.eframe.psm.search.vo.AuthInfoInq;
import com.easycerti.eframe.psm.search.vo.AuthInfoInqList;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.system_management.dao.AgentMngtDao;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

@Service
public class AuthInfoInqSvcImpl implements AuthInfoInqSvc{
	
	@Autowired
	private AuthInfoInqDao authInfoInqDao;
	
	@Autowired
	private AgentMngtDao agentMngtDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private ExtrtCondbyInqDao extrtCondbyInqDao;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;
	
	@Override
	public DataModelAndView findAuthInfoInqList(SearchSearch search) {

		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		search.setTotal_count(authInfoInqDao.findAuthInfoInqOne_count(search));
		
		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList();
		List<AuthInfoInq> authInfoInq = authInfoInqDao.findAuthInfoInqList(search);
		List<AuthInfoInq> authInfoToDepartment = authInfoInqDao.findAuthInfoToDepartment();
		AuthInfoInqList authInfoInqList = new AuthInfoInqList(authInfoInq);
		
		SimpleCode simpleCode = new SimpleCode("/authInfoInq/list.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("authInfoInqList", authInfoInqList);
		modelAndView.addObject("systemMasterList", systemMasterList);
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("authInfoToDepartment", authInfoToDepartment);
		
		return modelAndView;
		 	
	}
	
	@Override
	public void findAuthInfoInqList_download(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request) {
		
		search.setUseExcel("true");
		
		String fileName = "PSM_권한관리조회_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "권한관리조회";
		
		List<AuthInfoInq> authInfoInq = authInfoInqDao.findAuthInfoInqList(search);
		
		SimpleDateFormat parseSdf = new SimpleDateFormat("yyyyMMddHHmmss");
		SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
		
		for(int i=0; i<authInfoInq.size(); i++){
			try {
				authInfoInq.get(i).setProc_date(sdf.format(parseSdf.parse(authInfoInq.get(i).getProc_date() + authInfoInq.get(i).getProc_time())));
			
			} catch (ParseException e) {
				e.printStackTrace();
			}
			if(authInfoInq.get(i).getEmp_user_id()==null || authInfoInq.get(i).getEmp_user_id().equals("") || authInfoInq.get(i).getEmp_user_id().equals("null"))
				authInfoInq.get(i).setEmp_user_id("시스템");
			if(authInfoInq.get(i).getSystem_name()==null || authInfoInq.get(i).getSystem_name().equals("") || authInfoInq.get(i).getSystem_name().equals("null"))
				authInfoInq.get(i).setSystem_name("시스템");
			if(authInfoInq.get(i).getDept_name()==null || authInfoInq.get(i).getDept_name().equals("") || authInfoInq.get(i).getDept_name().equals("null"))
				authInfoInq.get(i).setDept_name("시스템");
			if(authInfoInq.get(i).getEmp_user_name()==null || authInfoInq.get(i).getEmp_user_name().equals("") || authInfoInq.get(i).getEmp_user_name().equals("null"))
				authInfoInq.get(i).setEmp_user_name("시스템");
			if(authInfoInq.get(i).getUser_ip()==null || authInfoInq.get(i).getUser_ip().equals("") || authInfoInq.get(i).getUser_ip().equals("null"))
				authInfoInq.get(i).setUser_ip("시스템");
			
			if(authInfoInq.get(i).getTarget_id()==null || authInfoInq.get(i).getTarget_id().equals("") || authInfoInq.get(i).getTarget_id().equals("null"))
				authInfoInq.get(i).setTarget_id("시스템");
			if(authInfoInq.get(i).getTarget_auth_name()==null || authInfoInq.get(i).getTarget_auth_name().equals("") || authInfoInq.get(i).getTarget_auth_name().equals("null"))
				authInfoInq.get(i).setTarget_auth_name("시스템");
			
			String req_type_en= authInfoInq.get(i).getReq_type();
			switch(req_type_en){
				case "CR": authInfoInq.get(i).setReq_type("등록");break;
				case "UD": authInfoInq.get(i).setReq_type("수정");break;
				case "DL": authInfoInq.get(i).setReq_type("삭제");break;
			}
			
			
		}
		
		String[] columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "target_id", "target_auth_name", "req_type"};
		String[] heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "사용자IP", "시스템명", "타겟ID", "타겟권한", "행위"};
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", authInfoInq);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}
	
	@Override
	public void findAuthInfoInqList_downloadCSV(DataModelAndView modelAndView, SearchSearch search,
			HttpServletRequest request) {
		
		search.setUseExcel("true");
		
		String fileName = "PSM_권한관리조회_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "권한관리조회";
		
		List<AuthInfoInq> authInfoInq = authInfoInqDao.findAuthInfoInqList(search);
		
		SimpleDateFormat parseSdf = new SimpleDateFormat("yyyyMMddHHmmss");
		SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
		
		for(int i=0; i<authInfoInq.size(); i++){
			try {
				authInfoInq.get(i).setProc_date(sdf.format(parseSdf.parse(authInfoInq.get(i).getProc_date() + authInfoInq.get(i).getProc_time())));
			
			} catch (ParseException e) {
				e.printStackTrace();
			}
			if(authInfoInq.get(i).getEmp_user_id()==null || authInfoInq.get(i).getEmp_user_id().equals("") || authInfoInq.get(i).getEmp_user_id().equals("null"))
				authInfoInq.get(i).setEmp_user_id("시스템");
			if(authInfoInq.get(i).getSystem_name()==null || authInfoInq.get(i).getSystem_name().equals("") || authInfoInq.get(i).getSystem_name().equals("null"))
				authInfoInq.get(i).setSystem_name("시스템");
			if(authInfoInq.get(i).getDept_name()==null || authInfoInq.get(i).getDept_name().equals("") || authInfoInq.get(i).getDept_name().equals("null"))
				authInfoInq.get(i).setDept_name("시스템");
			if(authInfoInq.get(i).getEmp_user_name()==null || authInfoInq.get(i).getEmp_user_name().equals("") || authInfoInq.get(i).getEmp_user_name().equals("null"))
				authInfoInq.get(i).setEmp_user_name("시스템");
			if(authInfoInq.get(i).getUser_ip()==null || authInfoInq.get(i).getUser_ip().equals("") || authInfoInq.get(i).getUser_ip().equals("null"))
				authInfoInq.get(i).setUser_ip("시스템");
			
			if(authInfoInq.get(i).getTarget_id()==null || authInfoInq.get(i).getTarget_id().equals("") || authInfoInq.get(i).getTarget_id().equals("null"))
				authInfoInq.get(i).setTarget_id("시스템");
			if(authInfoInq.get(i).getTarget_auth_name()==null || authInfoInq.get(i).getTarget_auth_name().equals("") || authInfoInq.get(i).getTarget_auth_name().equals("null"))
				authInfoInq.get(i).setTarget_auth_name("시스템");
			String req_type_en= authInfoInq.get(i).getReq_type();
			switch(req_type_en){
				case "CR": authInfoInq.get(i).setReq_type("등록");break;
				case "UD": authInfoInq.get(i).setReq_type("수정");break;
				case "DL": authInfoInq.get(i).setReq_type("삭제");break;
			}
		}
		
		List<Map<String, Object>> list = new ArrayList<>();

		for(AuthInfoInq req : authInfoInq) {
			Map<String, Object> map = new HashMap<>();
			map.put("proc_date", req.getProc_date());
			map.put("dept_name", req.getDept_name());
			map.put("emp_user_id", req.getEmp_user_id());
			map.put("emp_user_name", req.getEmp_user_name());
			map.put("user_ip", req.getUser_ip());
			map.put("system_name", req.getSystem_name());
			map.put("target_id", req.getTarget_id());
			map.put("target_auth_name", req.getTarget_auth_name());
			map.put("req_type", req.getReq_type());
			
			
			list.add(map);
		}
		
		String[] columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name","target_id", "target_auth_name", "req_type"};
		String[] heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "사용자IP", "시스템명", "타겟ID", "타겟권한", "행위"};
		
		modelAndView.addObject("excelData", list);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
		modelAndView.addObject("fileName", fileName);
	}
	
	
	//
	@Override
	public DataModelAndView findAuthInfoInqDetail(SearchSearch search) {

		DataModelAndView modelAndView = new DataModelAndView();

		AuthInfoInq authInfoInq = new AuthInfoInq();
		List<ExtrtCondbyInqDetailChart> userinfo = new ArrayList<>();
		AuthInfoInq authInfoDetailList = new AuthInfoInq();

		authInfoInq = authInfoInqDao.findAuthInfoInqDetail(search);
		modelAndView.addObject("authInfoInq", authInfoInq);

		SimpleCode simpleCode = new SimpleCode();
		String index = "/authInfoInq/detail.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);

		return modelAndView;
	}

	@Override
	public List<AuthInfoInq> findAuthInfoToDepartment() {
		
		return authInfoInqDao.findAuthInfoToDepartment();
	}

}
