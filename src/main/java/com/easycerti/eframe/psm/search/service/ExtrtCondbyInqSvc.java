package com.easycerti.eframe.psm.search.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.dashboard.vo.Dashboard;
import com.easycerti.eframe.psm.search.vo.EmpDetailInq;
import com.easycerti.eframe.psm.search.vo.EmpDetailInqList;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInq;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInqTimeline;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInqTimelineCond;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.setup.vo.ExtrtBaseSetup;
import com.easycerti.eframe.psm.statistics.vo.Statistics;
/**
 * 
 * 설명 : 추출조건별조회 Service Interface
 * @author tjlee
 * @since 2015. 4. 22.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일           			 수정자          		 수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 22.           tjlee            최초 생성
 *   2017. 5. 24.			sylee			detailCart2_5관련내용 추가
 *
 * </pre>
 */
public interface ExtrtCondbyInqSvc {

	/**
	 * 설명 : 추출조건별조회 리스트 
	 * @author tjlee
	 * @since 2015. 6. 8.
	 * @return DataModelAndView
	 */
	public DataModelAndView findExtrtCondbyInqList(SearchSearch search);
	
	public DataModelAndView extrtCondbyInqDaily(SearchSearch search);
	public DataModelAndView extrtCondbyInqDept(SearchSearch search);
	public DataModelAndView extrtCondbyInqUser(SearchSearch search);
	public DataModelAndView extrtCondbyInqSystem(SearchSearch search);
	
	public void extrtCondbyInqDaliyDownload(DataModelAndView modelAndView, SearchSearch search);
	public void extrtCondbyInqDeptDownload(DataModelAndView modelAndView, SearchSearch search);
	public void extrtCondbyInqUserDownload(DataModelAndView modelAndView, SearchSearch search);
	public void extrtCondbyInqSystemDownload(DataModelAndView modelAndView, SearchSearch search);
	
	/**
	 * 설명 : 소명조회 리스트 
	 */
	public DataModelAndView findSummonList(SearchSearch search);
	
	
	/**
	 * 설명 : 소명조회 리스트 
	 */
	public DataModelAndView findSummonManageList(SearchSearch search);
	
	
	public List<ExtrtCondbyInqTimeline> findExtrtCondbyInqTimelineAll(ExtrtCondbyInqTimelineCond condition);
	public List<ExtrtCondbyInqTimeline> findExtrtCondbyInqTimeline_rule(ExtrtCondbyInqTimelineCond condition);
	/**
	 * 설명 : 추출조건별조회 상세 
	 * @author tjlee
	 * @since 2015. 6. 8.
	 * @return DataModelAndView
	 */

	/**
	 * 설명 : 소명조회 상세 
	 * @author tjlee
	 * @since 2015. 6. 8.
	 * @return DataModelAndView
	 */
	public DataModelAndView findSummonDetail(SearchSearch search);
	
	public DataModelAndView findSummonManageDetail(SearchSearch search, HttpServletRequest request);
	
	

//	비정상위험분석 상단바 데이터
	public Map CountOfTopBar(Map<String, String> parameters, SearchSearch search);
	
	
	public DataModelAndView findExtrtCondbyInqDetail(SearchSearch search);
	/**
	 * 설명 : 소명조회 상세 
	 * (단건)
	 */

	public DataModelAndView addSummonOne(Map<String, String> parameters, HttpServletRequest request);
	public DataModelAndView reSummonOne(Map<String, String> parameters, HttpServletRequest request);
	public DataModelAndView addSummonResult(Map<String, String> parameters);
	public DataModelAndView removeSummonResult(Map<String, String> parameters);
	public DataModelAndView updateSummonResult(Map<String, String> parameters);
	public DataModelAndView addReSummonResult(Map<String, String> parameters);
	public DataModelAndView SummonResultConfirm(Map<String, String> parameters);
	
	
	public DataModelAndView findLineChart(Map<String, String> parameters);
	/**
	 * 설명 : 추출조건별조회 엑셀다운로드
	 * @author tjlee
	 * @since 2015. 6. 8.
	 * @return void
	 */
	public void findExtrtCondbyInqList_download(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);
	
	public void summonListExcel(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);
	public void summonDetailExcel(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);
	public void summonManageListExcel(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);
	public void abuseInfoDownloadExcel(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);
	
	public DataModelAndView sendRequest(Map<String, String> parameters);
	
	public DataModelAndView detailPieChart(Map<String, String> parameters);

	public DataModelAndView detailBarChart(Map<String, String> parameters);

	public DataModelAndView getChart0(Map<String, String> parameters);

	public DataModelAndView getChart1(Map<String, String> parameters);

	public DataModelAndView getChart2(Map<String, String> parameters);

	public String getDeptName(SearchSearch search);

	public DataModelAndView findAbnormalList(SearchSearch search);

	public List<ExtrtCondbyInq> findAbnormalChart0(SearchSearch search);

	public List<ExtrtCondbyInq> findAbnormalChart1(SearchSearch search);

	public DataModelAndView findExtrtCondbyInqDetailChart(SearchSearch search);
	
	public int findAbnormalDetailDngChart(ExtrtCondbyInq param);
	
	public List<ExtrtCondbyInq> findAbnormalDetailChart1(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDetailChart2(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDetailChart3(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDetailChart4(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDetailChart5(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDetailChart20(ExtrtCondbyInq param);

	public List<ExtrtCondbyInq> findAbnormalDetailChart1_1(String emp_user_name);
	public List<ExtrtCondbyInq> findAbnormalDetailChart2_1(String emp_user_name);
	public List<ExtrtCondbyInq> findAbnormalDetailChart3_1(String emp_user_name);
	public List<ExtrtCondbyInq> findAbnormalDetailChart4_1(String emp_user_name);
	public List<ExtrtCondbyInq> findAbnormalDetailChart5_1(String emp_user_name);

	public List<ExtrtCondbyInq> findAbnormalDetailChart1_2(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDetailChart2_2(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDetailChart3_2(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDetailChart4_2(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDetailChart5_2(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDetailChart20_2(ExtrtCondbyInq param);

	public List<ExtrtCondbyInq> findAbnormalDetailChart1_3(String dept_id);
	public List<ExtrtCondbyInq> findAbnormalDetailChart2_3(String dept_id);
	public List<ExtrtCondbyInq> findAbnormalDetailChart3_3(String dept_id);
	public List<ExtrtCondbyInq> findAbnormalDetailChart4_3(String dept_id);
	public List<ExtrtCondbyInq> findAbnormalDetailChart5_3(String dept_id);
	
	public List<ExtrtCondbyInq> findAbnormalDetailChart6(SearchSearch search);
	public List<ExtrtCondbyInq> findAbnormalDetailChart7(SearchSearch search);
	public List<ExtrtCondbyInq> findAbnormalDetailChart7_1(SearchSearch search);
	public List<ExtrtCondbyInq> findAbnormalDetailChart8(SearchSearch search);
	public List<ExtrtCondbyInq> findAbnormalDetailChart9(SearchSearch search);
	public List<ExtrtCondbyInq> findAbnormalDetailChart9_1(SearchSearch search);
	public List<ExtrtCondbyInq> findAbnormalDetailChart10(SearchSearch search);
	public List<ExtrtCondbyInq> findAbnormalDetailChart10_1(SearchSearch search);
	
	/**
	 * 설명 : 개인정보 취급패턴:취급유형 
	 * @author sylee
	 * @since 2017. 5. 24.
	 * @return DataModelAndView
	 */
	public List<ExtrtCondbyInq> findAbnormalDetailChart11(ExtrtCondbyInq eci);
	public Statistics findAbnormalDetailChart11_new(ExtrtCondbyInq eci);
	public List<Statistics> findAbnormalDetailChart12(ExtrtCondbyInq eci);
	public List<ExtrtCondbyInq> findAbnormalDetailChart12_1(ExtrtCondbyInq eci);
	public List<ExtrtCondbyInq> findAbnormalDetailChart13(ExtrtCondbyInq eci);
	public List<ExtrtCondbyInq> findAbnormalDetailChart14(ExtrtCondbyInq eci);

	public Object findAbnormalDetailChart6_TOT(SearchSearch search);
	public Object findAbnormalDetailChart7_TOT(SearchSearch search);
	public Object findAbnormalDetailChart8_TOT(SearchSearch search);
	public Object findAbnormalDetailChart9_TOT(SearchSearch search);
	public Object findAbnormalDetailChart10_TOT(SearchSearch search);

	
	/**
	 *부서별 비정상위험분석 상세차트
	 */
	public DataModelAndView findExtrtCondbyInqDeptDetailChart(SearchSearch search);
	
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart1(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart2(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart3(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart4(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart5(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart20(ExtrtCondbyInq param);

	public int findAbnormalDeptDetailDngChart(ExtrtCondbyInq param);
	
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart1_1(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart2_1(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart3_1(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart4_1(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart5_1(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart20_1(ExtrtCondbyInq param);
	
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart6(SearchSearch search);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart7(SearchSearch search);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart7_1(SearchSearch search);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart8(SearchSearch search);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart9(SearchSearch search);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart9_1(SearchSearch search);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart10(SearchSearch search);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart10_1(SearchSearch search);
	public Statistics findAbnormalDeptDetailChart11(ExtrtCondbyInq eci);
	public List<Statistics> findAbnormalDeptDetailChart12(ExtrtCondbyInq eci);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart12_1(ExtrtCondbyInq eci);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart13(ExtrtCondbyInq eci);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart14(ExtrtCondbyInq eci);

	public Object findAbnormalDeptDetailChart6_TOT(SearchSearch search);
	public Object findAbnormalDeptDetailChart7_TOT(SearchSearch search);
	public Object findAbnormalDeptDetailChart8_TOT(SearchSearch search);
	public Object findAbnormalDeptDetailChart9_TOT(SearchSearch search);
	public Object findAbnormalDeptDetailChart10_TOT(SearchSearch search);

	public DataModelAndView getResultType(SearchSearch search);
	public DataModelAndView setCheckExtrtDetail(Map<String, String> parameters);
	public List<ExtrtBaseSetup> getExtrtCondByAvgCnt(String emp_user_id);
	
	/** 180314 sysong 소명판정 판정자 위임 **/
	public DataModelAndView findSummonDelegation(SearchSearch search);
	
	/** 180314 sysong 소명판정 판정자 위임 **/
	public DataModelAndView summonUserRemove(Map<String, String> parameters);
	
	/** 180314 sysong 월별 소명 현황 **/
	public DataModelAndView findsummonPerby(SearchSearch search);
	
	/** 180314 sysong 월별 소명 현황 엑셀 다운로드 **/
	public void summonPerby_download(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);
	
	/** 180314 sysong 소속별 소명 현황 **/
	public DataModelAndView findsummonDeptby(SearchSearch search);
	
	/** 180314 sysong 소속별 소명 현황 엑셀 다운로드 **/
	public void summonDeptby_download(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);
	
	/** 180314 sysong 개인별 소명 현황 **/
	public DataModelAndView findsummonIndvby(SearchSearch search);
	
	/** 180314 sysong 개인별 소명 현황 엑셀 다운로드 **/
	public void summonIndvby_download(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);
	
	/** 180314 sysong 소명요청 및 판정 현황 **/
	public DataModelAndView findsummonProcessby(SearchSearch search);
	
	/** 180314 sysong 개인별 소명요청 및 승인 현황 엑셀 다운로드 **/
	public void summonProcessby_download(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);
	
	public List<ExtrtCondbyInq> findScenarioByScenSeq(SearchSearch search);
	
	public DataModelAndView saveFollowUp(Map<String, String> parameters);

	public DataModelAndView findSummonUpdate(SearchSearch search);
	
	public DataModelAndView findAbuseInfoList(SearchSearch search);
	public DataModelAndView findAbuseInfoDetail(SearchSearch search);
	
	public DataModelAndView saveAbuseInfo(Map<String, String> parameters);
	
	public DataModelAndView findRuleList(SearchSearch search);
	public DataModelAndView findRuleScript(int rule_seq);

	public void truncateExtractTemp();
	public void executeSql(ExtrtCondbyInq extrtCondbyInq);
	public void executeSqlSecond(String sql);
	public DataModelAndView findBizLogExtractTemp();
	

	public DataModelAndView findCenterSummonList(SearchSearch search);
	public String findCenterApprReturnUrl(long desc_seq, String admin_user_id);
	
	public DataModelAndView findTimeLineChart(SearchSearch search);
	public DataModelAndView timeLineChartAppend(SearchSearch search);
	
	public DataModelAndView timelineAppendByUser(SearchSearch search);

	public Map<String, String> summonListAdd(Map<String, String> parameters, HttpServletRequest request);
	public DataModelAndView summonDelegatePersonal(Map<String, String> parameters);

	public List findEmpUserList(Map<String, String> parameters);

	public String summonDelegatePersonalAdd(Map<String, String> parameters, HttpServletRequest request);

	public String summonDelegatePersonalDelete(Map<String, String> parameters, HttpServletRequest request);

	public DataModelAndView summonDelegateDept(Map<String, String> parameters);

	public String summonDelegateDeptAdd(Map<String, String> parameters, HttpServletRequest request);

	public String summonDelegateDeptDelete(Map<String, String> parameters, HttpServletRequest request);

	public DataModelAndView approvalDelegatePersonal(Map<String, String> parameters);

	public String approvalDelegatePersonalAdd(Map<String, String> parameters, HttpServletRequest request);

	public String approvalDelegatePersonalDelete(Map<String, String> parameters, HttpServletRequest request);

	public DataModelAndView approvalDelegateDept(Map<String, String> parameters);

	public String approvalDelegateDeptAdd(Map<String, String> parameters, HttpServletRequest request);

	public String approvalDelegateDeptDelete(Map<String, String> parameters, HttpServletRequest request);
	
	public String upLoadSystemMngtOne(Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response)throws Exception;	//소명 응답excel업로드
	public DataModelAndView addResponseSummonOne(Map<String, String> parameters, HttpServletRequest request);
	//20201222 hbjang 명지전문대 비정상위험분석 보고서 커스터마이징
	public DataModelAndView findExtrtReport(int emp_detail_seq, String occr_dt);
	public void extrtExcelDownload(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);
	
	//소명요청 상세 > 소명 삭제
	public DataModelAndView removeSummonOne(Map<String, String> parameters, HttpServletRequest request);
}
