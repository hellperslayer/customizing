package com.easycerti.eframe.psm.search.service;

import javax.servlet.http.HttpServletRequest;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.search.vo.SearchSearch;

public interface FileManageSvc {

	//파일 대장 관리
	DataModelAndView setRegisteredList(SearchSearch search);

	//파일 대장 관리 상세
	DataModelAndView setRegisteredDetailList(SearchSearch search);

	//엑셀 다운로드
	void findDownloadLogInqDetail_download(DataModelAndView modelAndView, SearchSearch search,
			HttpServletRequest request);

	//CSV 다운로드
	void findDownloadLoginqDetail_downloadCSV(DataModelAndView modelAndView, SearchSearch search,
			HttpServletRequest request);

	//상세 엑셀 다운로드
	void registeredDetailListExcelDown(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);

	//상세 CSV 다운로드
	void registeredDetailListCSVDown(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);

}
