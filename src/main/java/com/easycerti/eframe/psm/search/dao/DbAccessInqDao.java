package com.easycerti.eframe.psm.search.dao;

import java.util.List;

import com.easycerti.eframe.psm.search.vo.DbAccessInq;
import com.easycerti.eframe.psm.search.vo.SearchSearch;

/**
 * 
 * 설명 : DB접근로그조회 Dao Interface
 * @author tjlee
 * @since 2015. 4. 22.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 22.           tjlee            최초 생성
 *
 * </pre>
 */
public interface DbAccessInqDao {
	/**
	 * 
	 * 설명 : DB접근로그조회 리스트
	 * @author tjlee
	 * @since 2015. 4. 22.
	 * @return List<DbAccessInq>
	 */
	public List<DbAccessInq> findDbAccessInqList(SearchSearch search);
	
	/**
	 * 설명 : DB접근로그조회 리스트 카운트 
	 * @author tjlee
	 * @since 2015. 4. 22.
	 * @return int
	 */
	public int findDbAccessInqOne_count(SearchSearch search);
	
	/**
	 * 
	 * 설명 : DB접근로그조회 상세
	 * @author tjlee
	 * @since 2015. 4. 22.
	 * @return DbAccessInq
	 */
	public List<DbAccessInq> findDbAccessInqDetail(SearchSearch search);

	/**
	 * 설명 : DB접근로그조회 상세 정보 
	 * @author tjlee
	 * @since 2015. 5. 14.
	 * @return DbAccessInq
	 */
	public DbAccessInq findDbAccessInqOne(SearchSearch search);
	
	public List<DbAccessInq> findDbAccessInqResult(SearchSearch search);
	
	public DbAccessInq findColumnInfo(String col_no);
	
}
