package com.easycerti.eframe.psm.search.dao;

import java.util.List;

import com.easycerti.eframe.psm.search.vo.EmpDetailInq;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.system_management.vo.EmpUser;

/**
 * 
 * 설명 : 위험도별조회 Dao Interface
 * @author tjlee
 * @since 2015. 4. 22.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 22.           tjlee            최초 생성
 *
 * </pre>
 */
public interface EmpDetailInqDao {
	/**
	 *위험도별조회 리스트
	 */
	public List<EmpDetailInq> findEmpDetailInqList(SearchSearch search);
	public int findEmpDetailInqOne_count(SearchSearch search);
	
	/**
	 *위험도별조회 상세
	 */
	public EmpUser findEmpUser(SearchSearch search);
	public EmpDetailInq findEmpDetailInqDetail(EmpDetailInq param);
	public int findEmpDetailInqDetailOne_count(SearchSearch search);
	public List<EmpDetailInq> findEmpDetailInqListByEmpCd(SearchSearch search);
	public List<EmpDetailInq> findEmpDetailInqListByLogList(EmpDetailInq param);
	public int findEmpDetailInqListByLogListCount(EmpDetailInq param);
	
}
