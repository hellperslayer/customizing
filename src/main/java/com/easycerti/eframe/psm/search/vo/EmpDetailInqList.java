package com.easycerti.eframe.psm.search.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.AbstractValueObject;
/**
 * 
 * 설명 : 위험도별조회 리스트 Bean
 * @author tjlee
 * @since 2015. 4. 22.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 22.           tjlee            최초 생성
 *
 * </pre>
 */
public class EmpDetailInqList extends AbstractValueObject{

	// 위험도별조회 리스트
	private List<EmpDetailInq> empDetailInq = null;

	
	public EmpDetailInqList(){
		
	}
	
	public EmpDetailInqList(List<EmpDetailInq> empDetailInq){
		this.empDetailInq = empDetailInq;
	}
	
	public EmpDetailInqList(List<EmpDetailInq> empDetailInq, String page_total_count){
		this.empDetailInq = empDetailInq;
		setPage_total_count(page_total_count);
	}

	public List<EmpDetailInq> getEmpDetailInq() {
		return empDetailInq;
	}

	public void setEmpDetailInq(List<EmpDetailInq> empDetailInq) {
		this.empDetailInq = empDetailInq;
	}
}
