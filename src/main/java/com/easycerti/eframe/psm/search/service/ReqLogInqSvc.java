package com.easycerti.eframe.psm.search.service;

import javax.servlet.http.HttpServletRequest;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.search.vo.SearchSearch;

public interface ReqLogInqSvc {
	public DataModelAndView findReqLogInqList(SearchSearch search);

	public void findReqLogInqList_download(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);
	public void findReqLogInqList_downloadCSV(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);
	
	public DataModelAndView findReqLogIngDetail(SearchSearch search);
}
