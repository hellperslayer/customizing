package com.easycerti.eframe.psm.search.vo;

import java.util.List;
import com.easycerti.eframe.common.vo.AbstractValueObject;

public class AuthInfoInqList extends AbstractValueObject {

	private List<AuthInfoInq> authInfoInq = null;

	public AuthInfoInqList() {

	}

	public AuthInfoInqList(List<AuthInfoInq> authInfoInq) {
		this.authInfoInq = authInfoInq;
	}

	public AuthInfoInqList(List<AuthInfoInq> authInfoInq, String page_total_count) {
		this.authInfoInq = authInfoInq;
		setPage_total_count(page_total_count);
	}

	public List<AuthInfoInq> getAuthInfoInq() {
		return authInfoInq;
	}

	public void setAuthInfoInq(List<AuthInfoInq> authInfoInq) {
		this.authInfoInq = authInfoInq;
	}

}
