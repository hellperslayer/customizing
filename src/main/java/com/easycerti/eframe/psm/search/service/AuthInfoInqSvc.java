package com.easycerti.eframe.psm.search.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.search.vo.AuthInfoInq;
import com.easycerti.eframe.psm.search.vo.SearchSearch;

public interface AuthInfoInqSvc {
	
	public DataModelAndView findAuthInfoInqList(SearchSearch search);	
	public void findAuthInfoInqList_download(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);
	public void findAuthInfoInqList_downloadCSV(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);
	
	public DataModelAndView findAuthInfoInqDetail(SearchSearch search);
	
	public List<AuthInfoInq> findAuthInfoToDepartment();

}
