package com.easycerti.eframe.psm.search.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.search.dao.ExtrtCondbyInqDao;
import com.easycerti.eframe.psm.search.dao.ReqLogInqDao;
import com.easycerti.eframe.psm.search.service.ReqLogInqSvc;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInqDetailChart;
import com.easycerti.eframe.psm.search.vo.ReqLogInq;
import com.easycerti.eframe.psm.search.vo.ReqLogInqList;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.system_management.dao.AgentMngtDao;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

@Service
public class ReqLogInqSvcImpl implements ReqLogInqSvc {
	
	@Autowired
	private ReqLogInqDao reqLogInqDao;
	
	@Autowired
	private AgentMngtDao agentMngtDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private ExtrtCondbyInqDao extrtCondbyInqDao;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;
	
	@Override
	public DataModelAndView findReqLogInqList(SearchSearch search) {
		
		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		search.setTotal_count(reqLogInqDao.findReqLogInqOne_count(search));
		
		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList_byAdmin(search);
		//List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList();
		List<ReqLogInq> reqLogInq = reqLogInqDao.findReqLogInqList(search);
		String checkEmpNameMasking = commonDao.checkEmpNameMasking();
		if(checkEmpNameMasking.equals("Y")) {
			for (int i = 0; i < reqLogInq.size(); i++) {
				if(!"".equals(reqLogInq.get(i).getEmp_user_name())) {
					String empUserName = reqLogInq.get(i).getEmp_user_name();
					StringBuilder builder = new StringBuilder(empUserName);
					builder.setCharAt(empUserName.length() - 2, '*');
					reqLogInq.get(i).setEmp_user_name(builder.toString());
				}
			}
		}
		ReqLogInqList reqLogInqList = new ReqLogInqList(reqLogInq);
		
		SimpleCode simpleCode = new SimpleCode("/reqLogInq/list.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("reqLogInqList", reqLogInqList);
		modelAndView.addObject("systemMasterList", systemMasterList);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	@Override
	public void findReqLogInqList_download(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request) {
		
		search.setUseExcel("true");
		
		String fileName = "PSM_로그조회_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "로그조회";
		
		List<ReqLogInq> reqLogInq = reqLogInqDao.findReqLogInqList(search);
		
		SimpleDateFormat parseSdf = new SimpleDateFormat("yyyyMMddHHmmss");
		SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
		
		for(int i=0; i<reqLogInq.size(); i++){
			try {
				reqLogInq.get(i).setProc_date(sdf.format(parseSdf.parse(reqLogInq.get(i).getProc_date() + reqLogInq.get(i).getProc_time())));
			
			} catch (ParseException e) {
				e.printStackTrace();
			}
			if(reqLogInq.get(i).getEmp_user_id()==null || reqLogInq.get(i).getEmp_user_id().equals("") || reqLogInq.get(i).getEmp_user_id().equals("null"))
				reqLogInq.get(i).setEmp_user_id("시스템");
			if(reqLogInq.get(i).getSystem_name()==null || reqLogInq.get(i).getSystem_name().equals("") || reqLogInq.get(i).getSystem_name().equals("null"))
				reqLogInq.get(i).setSystem_name("시스템");
			if(reqLogInq.get(i).getDept_name()==null || reqLogInq.get(i).getDept_name().equals("") || reqLogInq.get(i).getDept_name().equals("null"))
				reqLogInq.get(i).setDept_name("시스템");
			if(reqLogInq.get(i).getEmp_user_name()==null || reqLogInq.get(i).getEmp_user_name().equals("") || reqLogInq.get(i).getEmp_user_name().equals("null"))
				reqLogInq.get(i).setEmp_user_name("시스템");
			if(reqLogInq.get(i).getUser_ip()==null || reqLogInq.get(i).getUser_ip().equals("") || reqLogInq.get(i).getUser_ip().equals("null"))
				reqLogInq.get(i).setUser_ip("시스템");
			
		}
		
		String[] columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "req_url"};
		String[] heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "사용자IP", "시스템명", "접근 경로"};
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", reqLogInq);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}
	
	@Override
	public void findReqLogInqList_downloadCSV(DataModelAndView modelAndView, SearchSearch search,
			HttpServletRequest request) {
		
		search.setUseExcel("true");
		
		String fileName = "PSM_로그조회_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "로그조회";
		
		List<ReqLogInq> reqLogInq = reqLogInqDao.findReqLogInqList(search);
		
		SimpleDateFormat parseSdf = new SimpleDateFormat("yyyyMMddHHmmss");
		SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
		
		for(int i=0; i<reqLogInq.size(); i++){
			try {
				reqLogInq.get(i).setProc_date(sdf.format(parseSdf.parse(reqLogInq.get(i).getProc_date() + reqLogInq.get(i).getProc_time())));
			
			} catch (ParseException e) {
				e.printStackTrace();
			}
			if(reqLogInq.get(i).getEmp_user_id()==null || reqLogInq.get(i).getEmp_user_id().equals("") || reqLogInq.get(i).getEmp_user_id().equals("null"))
				reqLogInq.get(i).setEmp_user_id("시스템");
			if(reqLogInq.get(i).getSystem_name()==null || reqLogInq.get(i).getSystem_name().equals("") || reqLogInq.get(i).getSystem_name().equals("null"))
				reqLogInq.get(i).setSystem_name("시스템");
			if(reqLogInq.get(i).getDept_name()==null || reqLogInq.get(i).getDept_name().equals("") || reqLogInq.get(i).getDept_name().equals("null"))
				reqLogInq.get(i).setDept_name("시스템");
			if(reqLogInq.get(i).getEmp_user_name()==null || reqLogInq.get(i).getEmp_user_name().equals("") || reqLogInq.get(i).getEmp_user_name().equals("null"))
				reqLogInq.get(i).setEmp_user_name("시스템");
			if(reqLogInq.get(i).getUser_ip()==null || reqLogInq.get(i).getUser_ip().equals("") || reqLogInq.get(i).getUser_ip().equals("null"))
				reqLogInq.get(i).setUser_ip("시스템");
		}
		
		List<Map<String, Object>> list = new ArrayList<>();

		for(ReqLogInq req : reqLogInq) {
			Map<String, Object> map = new HashMap<>();
			map.put("proc_date", req.getProc_date());
			map.put("dept_name", req.getDept_name());
			map.put("emp_user_id", req.getEmp_user_id());
			map.put("emp_user_name", req.getEmp_user_name());
			map.put("user_ip", req.getUser_ip());
			map.put("system_name", req.getSystem_name());
			map.put("req_url", req.getReq_url());
			
			list.add(map);
		}
		
		String[] columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "req_url"};
		String[] heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "사용자IP", "시스템명", "접근 경로"};
		
		modelAndView.addObject("excelData", list);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
		modelAndView.addObject("fileName", fileName);
	}
	
	@Override
	public DataModelAndView findReqLogIngDetail(SearchSearch search) {
		
		int pageSize = Integer.valueOf(defaultPageSize);
		
		ReqLogInq reqLogInq = reqLogInqDao.findReqLogIngDetail(search);
		//search.setEmp_user_id(reqLogInq.getEmp_user_id());
		
		List<ExtrtCondbyInqDetailChart> systemList = new ArrayList<>();
		if(reqLogInq.getEmp_user_id() != null) {
			search.setEmp_user_idD2(reqLogInq.getEmp_user_id());
			systemList = extrtCondbyInqDao.getExtrtCondbyInqDetail2_1(search);
		}
		
		String checkEmpNameMasking = commonDao.checkEmpNameMasking();
		if(checkEmpNameMasking.equals("Y")) {
			if(!"".equals(reqLogInq.getEmp_user_name())) {
				String empUserName = reqLogInq.getEmp_user_name();
				StringBuilder builder = new StringBuilder(empUserName);
				builder.setCharAt(empUserName.length() - 2, '*');
				reqLogInq.setEmp_user_name(builder.toString());
			}
		}
		
		search.getReqLogInqDetail().setSize(pageSize);
		int cnt = reqLogInqDao.findReqLogInqDetailResultCount(search);
		search.getReqLogInqDetail().setTotal_count(cnt);
		
		List<ReqLogInq> reqLogInqDetailList = reqLogInqDao.findReqLogIngDetailResult(search);
		
		SimpleCode simpleCode = new SimpleCode("/reqLogInq/detail.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("reqLogInq", reqLogInq);
		modelAndView.addObject("systemList", systemList);
		modelAndView.addObject("reqLogInqDetailList", reqLogInqDetailList);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
}
