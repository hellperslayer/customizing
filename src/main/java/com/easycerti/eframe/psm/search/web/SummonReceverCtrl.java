package com.easycerti.eframe.psm.search.web;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.easycerti.eframe.psm.search.service.AllLogInqSvc;

@Controller
@RequestMapping("/remote/*")
public class SummonReceverCtrl {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private AllLogInqSvc allLogInqSvc;

	@RequestMapping(value={"RemoteSummonReceiver.do"}, method={RequestMethod.POST,RequestMethod.GET})
	public ModelAndView findReqLogInqList(HttpServletRequest request
			, HttpServletResponse response
			, @RequestParam("values") String[] values) {
//		values[0] = "20190521	RC	2	312944	20190521135353	R2003204	류성열	R2003204	192.1.19.159	60100313	혈액관리본부 혈액안전국 품질평가관리팀	601601	혈액관리본부	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	7501141	박종민	1	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	R	null	null	null	resrelease.jsp	null	W	null	null	null	null	null	null	null	null	null	null	null	null	null	6376412027	null	null	312944	BIMS	RC	20190617	20190707	null	N	0	4	동일IP 다수ID접속(타부서)";	
//		values[1] = "20190521	RC	2	299179	20190521134444	R2003204	류성열	R2003204	192.1.19.159	60100313	혈액관리본부 혈액안전국 품질평가관리팀	601601	혈액관리본부	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	9505181	전태양	1	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	R	null	null	null	resrelease.jsp	null	W	null	null	null	null	null	null	null	null	null	null	null	null	null	6376401083	null	null	299179	BIMS	RC	20190617	20190707	null	N	0	4	동일IP 다수ID접속(타부서)";	
//		values[2] = "20190521	RC	2	312948	20190521135353	R2003204	류성열	R2003204	192.1.19.159	60100313	혈액관리본부 혈액안전국 품질평가관리팀	601601	혈액관리본부	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	7811271	임재동	1	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	R	null	null	null	resrelease.jsp	null	W	null	null	null	null	null	null	null	null	null	null	null	null	null	6376412005	null	null	312948	BIMS	RC	20190617	20190707	null	N	0	4	동일IP 다수ID접속(타부서)";
//		values[3] = "20190521	RC	2	299162	20190521134444	R2003204	류성열	R2003204	192.1.19.159	60100313	혈액관리본부 혈액안전국 품질평가관리팀	601601	혈액관리본부	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	8010182	정진희	1	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	R	null	null	null	resrelease.jsp	null	W	null	null	null	null	null	null	null	null	null	null	null	null	null	6376401147	null	null	299162	BIMS	RC	20190617	20190707	null	N	0	4	동일IP 다수ID접속(타부서)";
//		values[4] = "20190521	RC	2	312980	20190521135354	R2003204	류성열	R2003204	192.1.19.159	60100313	혈액관리본부 혈액안전국 품질평가관리팀	601601	혈액관리본부	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	5505051	최성봉	1	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	null	R	null	null	null	resrelease.jsp	null	W	null	null	null	null	null	null	null	null	null	null	null	null	null	6376412253	null	null	312980	BIMS	RC	20190617	20190707	null	N	0	4	동일IP 다수ID접속(타부서)";
		int result = 0;
		
		/*
		try {
			System.out.println("================================== Before Encode Data Start ==================================");
			for (int i = 0; i < values.length; i++) {
				System.out.println(i + " : " + values[i]);
			}
			System.out.println("================================== Before Encode Data End ==================================");
			System.out.println("================================== After Encode Data Start ==================================");
			String[] encodings = {"UTF-8", "CP949", "EUC-KR", "ISO8859-1"};
			for (int i = 0; i < values.length; i++) {
				String a = values[i];
				for( String e1 : encodings ) {
					for( String e2 : encodings ) {
						System.out.println(String.format("%s:%s > %s", e1,e2,new String(a.getBytes(e1), e2)));
					}
				}
				System.out.println("================================== NEXT DATA !! ==================================");
			}
			System.out.println("================================== After Encode Data End ==================================");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		*/
		
		List<String[]> list = new ArrayList<String[]>();

		for (int i = 0; i < values.length; i++) {
			try {
				String a = new String(values[i].getBytes("ISO8859-1"), "EUC-KR");
				values[i] = a;
			} catch (Exception e) {
				logger.error("",e);
			}
			String[] tmp = values[i].split("\t");
			for(int j = 0; j < tmp.length; j++) {
				if(tmp[j] != null && tmp[j].equals("null")) {
					tmp[j] = "";
				}
			}
			
			if(tmp.length == 91) {
				String[] aa = new String[92];
				System.arraycopy(tmp, 0, aa, 0, 90);
				aa[90] = tmp[90];
				aa[91] = tmp[90];
				list.add(aa);
				continue;
			}
			else if(tmp.length != 92) {
				ModelAndView modelAndView = new ModelAndView();
				modelAndView.setViewName("messageView");
				modelAndView.addObject("message", result);
				return modelAndView;
			}
			list.add(tmp);
		}
//		result = 1;
		result = allLogInqSvc.addCenterLogInfo(list);
		// result = dao.insertDescInfo(list);

		// PrintWriter out = response.getWriter();
		// out.println(result);

		logger.debug("전송 완료 건수 : " + list.size());
		logger.debug("소명 데이터 받기 끝 ===================================================");

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("messageView");
		modelAndView.addObject("message", result);
		return modelAndView;
	}
	
}
