package com.easycerti.eframe.psm.search.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.AbstractValueObject;

public class ReqLogInqList extends AbstractValueObject{

	private List<ReqLogInq> reqLogInq = null;

	public ReqLogInqList(){
		
	}
	
	public ReqLogInqList(List<ReqLogInq> reqLogInq){
		this.reqLogInq = reqLogInq;
	}
	
	public ReqLogInqList(List<ReqLogInq> reqLogInq, String page_total_count){
		this.reqLogInq = reqLogInq;
		setPage_total_count(page_total_count);
	}
	
	public List<ReqLogInq> getReqLogInq() {
		return reqLogInq;
	}

	public void setReqLogInq(List<ReqLogInq> reqLogInq) {
		this.reqLogInq = reqLogInq;
	}
	
}
