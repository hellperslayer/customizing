package com.easycerti.eframe.psm.search.web;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.psm.search.service.DownloadLogInqSvc;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;

@Controller
@RequestMapping("/downloadLogInq/*")
public class DownloadLogInqCtrl {
	
	@Autowired DownloadLogInqSvc downloadLogInqSvc;
	
	@MngtActHist(log_action = "SELECT", log_message = "[접속기록] 다운로드접속기록")
	@RequestMapping(value={"list.html"}, method={RequestMethod.POST})
	public DataModelAndView findDownloadLogInqList(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request,
			@RequestParam(value = "sub_menu_id", defaultValue = "") String sub_menu_id,
			@RequestParam(value = "menutitle",defaultValue="")String menutitle) 
	{
		
		HttpSession session = request.getSession();
		// 개인정보유형 뷰
		String result_type_view = (String) session.getAttribute("result_type");
		// 메뉴명 유무
		String scrn_name_view = (String) session.getAttribute("scrn_name");
		// 접속유형 default 세팅
		String mapping_id = (String) session.getAttribute("mapping_id");
		// 다운로드로그 풀스캔연동여부
		String use_fullscan = (String) session.getAttribute("use_fullscan");
		if(mapping_id.equals("Y")) { if ( search.getMapping_id() == null ) search.setMapping_id("code1");
			//if ( search.getMapping_id().equals("codeAll") ) search.setMapping_id("");
			if ( search.getMapping_id().equals("code2") ) search.setMapping_id(null);
		}else {
			if(search.getMapping_id() == null || search.getMapping_id().equals("code2"))
				search.setMapping_id(null);
			else
				search.setMapping_id("code1");
		}
		
		if(parameters.get("main_menu_id") != null && search.getMain_menu_id() == null && !search.getMain_menu_id().equals("")) { search.setMain_menu_id(parameters.get("main_menu_id"));}
		if(parameters.get("sub_menu_id") != null && search.getSub_menu_id() == null && !search.getSub_menu_id().equals("")) { search.setSub_menu_id(parameters.get("sub_menu_id"));}
		if(parameters.get("system_seq") != null && search.getSystem_seq() == null && !search.getSystem_seq().equals("")) { search.setSystem_seq(parameters.get("system_seq"));}
		if(parameters.get("menuId") != null && search.getMenuId() == null && !search.getMenuId().equals("")) { search.setMenuId(parameters.get("menuId"));}
		if(parameters.get("isSearch") != null && search.getIsSearch() == null && !search.getIsSearch().equals("")) { search.setIsSearch(parameters.get("isSearch"));}
    
	    List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_onnara");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
	    
		Date today = new Date();
		
		search.initDatesIfEmpty(today, today);
		
		AdminUser adminUser = SystemHelper.getAdminUserInfo(request);
		
		search.setLogin_user_id(adminUser.getAdmin_user_id());
		search.setLogin_auth_id(adminUser.getAuth_id());
		
		DataModelAndView modelAndView = downloadLogInqSvc.findDownloadLogInqList(search);

		//modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("use_fullscan", use_fullscan);
		modelAndView.addObject("menutitle",menutitle);
		modelAndView.addObject("sub_menu_id", sub_menu_id);
		modelAndView.addObject("search", search);
		modelAndView.addObject("result_type_view", result_type_view);
		modelAndView.addObject("scrn_name_view", scrn_name_view);
		modelAndView.setViewName("downloadLogInqList");

		return modelAndView;
	}
	@MngtActHist(log_action = "SELECT", log_message = "[다운로드접속기록] 다운로드 로그조회 상세")
	@RequestMapping(value={"detail.html"}, method={RequestMethod.POST,RequestMethod.GET})
	public DataModelAndView findDownloadLogInqDetail(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		//전체로그조회 상세 리스트 페이징 page_num 세팅
		if(parameters.get("downloadLogInqDetail_page_num")!=null){
			search.getAllLogInqDetail().setPage_num(Integer.valueOf(parameters.get("downloadLogInqDetail_page_num")));
		}
		
		//추출조건별조회 상세 리스트 페이징 page_num 세팅
		if(parameters.get("extrtCondbyInqDetail_page_num")!=null){
			search.getExtrtCondbyInqDetail().setPage_num(Integer.valueOf(parameters.get("extrtCondbyInqDetail_page_num")));
		}
		
		//위험도별조회 상세 리스트 페이징 page_num 세팅
		if(parameters.get("empDetailInqDetail_page_num")!=null){
			search.getEmpDetailInqDetail().setPage_num(Integer.valueOf(parameters.get("empDetailInqDetail_page_num")));
		}
					
		//List<ExtrtCondbyInqDetailChart> data1 = new ArrayList<>();
		
		if(parameters.get("main_menu_id") != null && search.getMain_menu_id() == null && !search.getMain_menu_id().equals("")) { search.setMain_menu_id(parameters.get("main_menu_id"));}
		if(parameters.get("sub_menu_id") != null && search.getSub_menu_id() == null && !search.getSub_menu_id().equals("")) { search.setSub_menu_id(parameters.get("sub_menu_id"));}
		if(parameters.get("system_seq") != null && search.getSystem_seq() == null && !search.getSystem_seq().equals("")) { search.setSystem_seq(parameters.get("system_seq"));}
		if(parameters.get("menuId") != null && search.getMenuId() == null && !search.getMenuId().equals("")) { search.setMenuId(parameters.get("menuId"));}
		if(parameters.get("isSearch") != null && search.getIsSearch() == null && !search.getIsSearch().equals("")) { search.setIsSearch(parameters.get("isSearch"));}

		
		
		//2017.06.29
		
		try{
			String emp_user_name = parameters.get("emp_user_name");
			if(emp_user_name.isEmpty())
				search.setIsSearch("N");
			else
				search.setIsSearch("Y");
		} catch (Exception e) {
			search.setIsSearch("N");
		}
				
		DataModelAndView modelAndView = downloadLogInqSvc.findDownloadLogInqDetail(search);
			
		//modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		
		
		HttpSession session = request.getSession();
		String master = (String)session.getAttribute("master");
		modelAndView.addObject("masterflag", master);
		
		String scrn_name_view = (String) session.getAttribute("scrn_name");
		modelAndView.addObject("scrn_name_view", scrn_name_view);

		// 다운로드로그 풀스캔연동여부
		String use_fullscan = (String) session.getAttribute("use_fullscan");
		modelAndView.addObject("use_fullscan", use_fullscan);
		
		modelAndView.setViewName("downloadLogInqDetail");

		return modelAndView;
	}
	
	/**
	 * 20201216 신한카드 비인가 다운로드 로그조회 상세 페이지
	 */
//	@MngtActHist(log_action = "SELECT", log_message = "[다운로드접속기록] 다운로드 로그조회 상세")
	@RequestMapping(value={"detail_shcd.html"}, method={RequestMethod.POST,RequestMethod.GET})
	public DataModelAndView findDownloadLogInqDetail_shcd(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		//전체로그조회 상세 리스트 페이징 page_num 세팅
		if(parameters.get("downloadLogInqDetail_page_num")!=null){
			search.getAllLogInqDetail().setPage_num(Integer.valueOf(parameters.get("downloadLogInqDetail_page_num")));
		}
		
		//추출조건별조회 상세 리스트 페이징 page_num 세팅
		if(parameters.get("extrtCondbyInqDetail_page_num")!=null){
			search.getExtrtCondbyInqDetail().setPage_num(Integer.valueOf(parameters.get("extrtCondbyInqDetail_page_num")));
		}
		
		//위험도별조회 상세 리스트 페이징 page_num 세팅
		if(parameters.get("empDetailInqDetail_page_num")!=null){
			search.getEmpDetailInqDetail().setPage_num(Integer.valueOf(parameters.get("empDetailInqDetail_page_num")));
		}
		
		//List<ExtrtCondbyInqDetailChart> data1 = new ArrayList<>();
		
		if(parameters.get("main_menu_id") != null && search.getMain_menu_id() == null && !search.getMain_menu_id().equals("")) { search.setMain_menu_id(parameters.get("main_menu_id"));}
		if(parameters.get("sub_menu_id") != null && search.getSub_menu_id() == null && !search.getSub_menu_id().equals("")) { search.setSub_menu_id(parameters.get("sub_menu_id"));}
		if(parameters.get("system_seq") != null && search.getSystem_seq() == null && !search.getSystem_seq().equals("")) { search.setSystem_seq(parameters.get("system_seq"));}
		if(parameters.get("menuId") != null && search.getMenuId() == null && !search.getMenuId().equals("")) { search.setMenuId(parameters.get("menuId"));}
		if(parameters.get("isSearch") != null && search.getIsSearch() == null && !search.getIsSearch().equals("")) { search.setIsSearch(parameters.get("isSearch"));}
		
		
		//2017.06.29
		
		try{
			String emp_user_name = parameters.get("emp_user_name");
			if(emp_user_name.isEmpty())
				search.setIsSearch("N");
			else
				search.setIsSearch("Y");
		} catch (Exception e) {
			search.setIsSearch("N");
		}
		
		DataModelAndView modelAndView = downloadLogInqSvc.findDownloadLogInqDetail(search);
		
		//modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		
		
		HttpSession session = request.getSession();
		String master = (String)session.getAttribute("master");
		modelAndView.addObject("masterflag", master);
		
		String scrn_name_view = (String) session.getAttribute("scrn_name");
		modelAndView.addObject("scrn_name_view", scrn_name_view);
		
		// 다운로드로그 풀스캔연동여부
		String use_fullscan = (String) session.getAttribute("use_fullscan");
		modelAndView.addObject("use_fullscan", use_fullscan);
		
		modelAndView.setViewName("downloadLogInqDetail_shcd");
		
		return modelAndView;
	}
	
	@RequestMapping(value="download.html", method={RequestMethod.POST})
	public DataModelAndView addDownloadLogInqList_download(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		if ( search.getMapping_id() == null ) search.setMapping_id("code1");
	    if ( search.getMapping_id().equals("code2") ) search.setMapping_id(null);
	    
	    List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_onnara");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
	    AdminUser adminUser = SystemHelper.getAdminUserInfo(request);
		
		search.setLogin_user_id(adminUser.getAdmin_user_id());
		search.setLogin_auth_id(adminUser.getAuth_id());
	    try {
			downloadLogInqSvc.findDownloadLogInqList_download(modelAndView, search, request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
		return modelAndView;
	}
	
	/**
	 * 전체로그조회 엑셀(CSV) 다운로드
	 */
	@RequestMapping(value="downloadCSV.html", method={RequestMethod.POST})
	public DataModelAndView addDownloadLogInqList_downloadCSV(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.CSV_DOWNLOAD_VIEW);
		
		if ( search.getMapping_id() == null ) search.setMapping_id("code1");
	    if ( search.getMapping_id().equals("code2") ) search.setMapping_id(null);
	    
	    List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_onnara");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
	    AdminUser adminUser = SystemHelper.getAdminUserInfo(request);
		
		search.setLogin_user_id(adminUser.getAdmin_user_id());
		search.setLogin_auth_id(adminUser.getAuth_id());
	    
	    downloadLogInqSvc.findDownloadLogInqList_downloadCSV(modelAndView, search, request);
		
		return modelAndView;
	}
	
	@RequestMapping(value={"saveDownloadLogReason.html"}, method={RequestMethod.POST})
	public DataModelAndView saveDownloadLogReason(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = downloadLogInqSvc.saveDownloadLogReason(parameters); 
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	@RequestMapping(value="downloadDetail.html", method={RequestMethod.POST})
	public DataModelAndView addAllLogInqList_downloadDetail(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		downloadLogInqSvc.findDownloadLogInqDetail_download(modelAndView, search, request);
	    
		return modelAndView;
	}
	
	@RequestMapping(value="downloadDetailCSV.html", method={RequestMethod.POST})
	public DataModelAndView addAllLogInqList_downloadDetailCSV(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.CSV_DOWNLOAD_VIEW);
		
		downloadLogInqSvc.findDownloadLoginqDetail_downloadCSV(modelAndView, search, request);
	    
		return modelAndView;
	}
	
	/*개인정보 제외 추가*/
	@RequestMapping(value="add.html",method={RequestMethod.POST})
	public DataModelAndView addPrivacy(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		String[] args = {"misdetect_pattern", "privacy_type"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
				
		DataModelAndView modelAndView = downloadLogInqSvc.addPrivacy(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	/*개인정보 제외 복원*/
	@RequestMapping(value="remove.html",method={RequestMethod.POST})
	public DataModelAndView removePrivacy(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		String[] args = {"misdetect_pattern", "privacy_type"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
				
		DataModelAndView modelAndView = downloadLogInqSvc.removePrivacy(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
}
