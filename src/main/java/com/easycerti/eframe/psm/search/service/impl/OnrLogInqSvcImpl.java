package com.easycerti.eframe.psm.search.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.PrivacyType;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.search.dao.AllLogInqDao;
import com.easycerti.eframe.psm.search.dao.ExtrtCondbyInqDao;
import com.easycerti.eframe.psm.search.dao.OnrLogInqDao;
import com.easycerti.eframe.psm.search.service.OnrLogInqSvc;
import com.easycerti.eframe.psm.search.vo.AllLogInq;
import com.easycerti.eframe.psm.search.vo.AllLogInqList;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInqDetailChart;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.system_management.dao.AgentMngtDao;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

@Service
public class OnrLogInqSvcImpl implements OnrLogInqSvc {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired 
	private CommonDao commonDao;
	
	@Autowired 
	private OnrLogInqDao onrLogInqDao;
	
	@Autowired 
	private AgentMngtDao agentMngtDao;
	
	@Autowired 
	private AllLogInqDao allLogInqDao;
	
	@Autowired 
	private ExtrtCondbyInqDao extrtCondbyInqDao;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;
	
	
	
	@Override
	public DataModelAndView findOnrLogInqList(SearchSearch search) {
		
		SimpleCode simpleCode = new SimpleCode();
		String index = "/onrLogInq/list.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		int count = onrLogInqDao.findOnrLogInqList_count(search);
		search.setTotal_count(count);
		
		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList_Onnara();
		List<AllLogInq> onrLogInq = onrLogInqDao.findOnrLogInqList(search);
		
		AllLogInqList onrLogInqList = new AllLogInqList(onrLogInq);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("onrLogInqList", onrLogInqList);
		modelAndView.addObject("systemMasterList", systemMasterList);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView findOnrLogInqDetail(SearchSearch search) {

		int pageSize = 5;
		search.getAllLogInqDetail().setSize(pageSize);
		
		int cnt = allLogInqDao.findAllLogInqDetailResultCount(search);
		search.getAllLogInqDetail().setTotal_count(cnt);
		
		//String ui_type = commonDao.getUiType();
		//search.setUi_type(ui_type+"_TYPE");
		
		List<ExtrtCondbyInqDetailChart> data2 = new ArrayList<>();
		AllLogInq onrLogInq = new AllLogInq();
		List<AllLogInq> onrLogInqDetailList = new ArrayList<AllLogInq>();
		List<AllLogInq> onrLogInqFileList = new ArrayList<AllLogInq>();
		
		try{
			onrLogInq = onrLogInqDao.findOnrLogInqDetail(search);
			
			if(onrLogInq.getEmp_user_id() != null) {
				search.setEmp_user_idD2(onrLogInq.getEmp_user_id());
				data2 = extrtCondbyInqDao.getExtrtCondbyInqDetail2_1(search);
			}
			// 개인정보 마스킹 
			//search.setSystem_seq(allLogInq.getSystem_seq());
			search.setSystem_seq_temp(onrLogInq.getSystem_seq());
			
			onrLogInqFileList = onrLogInqDao.findOnrLogInqFileList(search);
			onrLogInqDetailList = allLogInqDao.findAllLogInqDetailResult(search);
			
			for(AllLogInq v : onrLogInqDetailList){
				if(v.getResult_content() != null && v.getResult_content() != ""){
					
					String pResult = v.getResult_content();
					
					if(v.getResult_type() != null && v.getPrivacy_masking() != null && v.getPrivacy_masking().equals("Y")) {
						pResult = AllLogInqSvcImpl.getResultContextByMasking(pResult, v.getResult_type());
					}
					
					v.setResult_content_masking(pResult);
				}
			}
		}catch(Exception e) {
			logger.error("[OnrLogInqSvcImpl.findOnrLogInqDetail] MESSAGE : " + e.getMessage());
			throw new ESException("SYS005V");
		}
		
		SimpleCode simpleCode = new SimpleCode();
		String index = "/onrLogInq/detail.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		modelAndView.addObject("onrLogInq", onrLogInq);
		modelAndView.addObject("onrLogInqDetailList", onrLogInqDetailList);
		modelAndView.addObject("onrLogInqFileList", onrLogInqFileList);
		
		modelAndView.addObject("index_id", index_id);
		if ( data2.size() > 0 ) {
			modelAndView.addObject("data2", data2);
		}
		return modelAndView;
	}
	
	@Override
	public void findOnrLogInqList_download(DataModelAndView modelAndView, SearchSearch search,
			HttpServletRequest request) {
		
		search.setUseExcel("true");
		
		String fileName = "PSM_개인정보_접속기록조회(온나라)_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "개인정보 접속기록조회(온나라)";
		
		List<AllLogInq> allLogInq = onrLogInqDao.findOnrLogInqList(search);
		List<PrivacyType> privacyTypeList = commonDao.getPrivacyTypesExceptRegular();
		SimpleDateFormat parseSdf = new SimpleDateFormat("yyyyMMddHHmmss");
		SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
		
		for(int i=0; i<allLogInq.size(); i++){
			AllLogInq ali = allLogInq.get(i);
			String result_type = ali.getResult_type();
			if(result_type != null) {
				String[] arrResult_type = result_type.split(",");
				Arrays.sort(arrResult_type);
				
				String result_type_string = "";
				String prev = "";
				int count = 1;
				for(int j=0; j<arrResult_type.length; j++) {
					if(prev.equals(arrResult_type[j])) {
						count++;
					}else {
						if(j != 0) {
							result_type_string += " " +count + ", ";
						}
						
						for(int k=0; k<privacyTypeList.size(); k++){
							if(privacyTypeList.get(k).getPrivacy_type().equals(arrResult_type[j])) { 
								result_type_string += privacyTypeList.get(k).getPrivacy_desc(); 
							}
						}
						count = 1;
						prev = arrResult_type[j];
					}
					
					if(j == arrResult_type.length - 1) {
						result_type_string += " " +count;
					}
				}
				
				allLogInq.get(i).setCnt(arrResult_type.length);
				allLogInq.get(i).setResult_type(result_type_string);
			}
			
			String req_type_en=allLogInq.get(i).getReq_type();
			switch(req_type_en){
				case "RD": allLogInq.get(i).setReq_type("조회");break;
				case "CR": allLogInq.get(i).setReq_type("등록");break;
				case "UD": allLogInq.get(i).setReq_type("수정");break;
				case "DL": allLogInq.get(i).setReq_type("삭제");break;
				case "EX": allLogInq.get(i).setReq_type("다운로드");break;
				case "PR": allLogInq.get(i).setReq_type("출력");break;
				case "CO": allLogInq.get(i).setReq_type("수집");break;
				case "NE": allLogInq.get(i).setReq_type("생성");break;
				case "BE": allLogInq.get(i).setReq_type("연계");break;
				case "IN": allLogInq.get(i).setReq_type("연동");break;
				case "WR": allLogInq.get(i).setReq_type("기록");break;
				case "SA": allLogInq.get(i).setReq_type("저장");break;
				case "SU": allLogInq.get(i).setReq_type("보유");break;
				case "FI": allLogInq.get(i).setReq_type("가공");break;
				case "UP": allLogInq.get(i).setReq_type("편집");break;
				case "SC": allLogInq.get(i).setReq_type("검색");break;
				case "CT": allLogInq.get(i).setReq_type("정정");break;
				case "RE": allLogInq.get(i).setReq_type("복구");break;
				case "US": allLogInq.get(i).setReq_type("이용");break;
				case "OF": allLogInq.get(i).setReq_type("제공");break;
				case "OP": allLogInq.get(i).setReq_type("공개");break;
				case "AN": allLogInq.get(i).setReq_type("파기");break;
			}
			
			if(allLogInq.get(i).getDept_name().equals("") || allLogInq.get(i).getDept_name().equals(null)) 
				allLogInq.get(i).setDept_name("시스템");
			if(allLogInq.get(i).getEmp_user_name().equals("") || allLogInq.get(i).getEmp_user_name().equals(null)) 
				allLogInq.get(i).setEmp_user_name("시스템");
			
			try {
				allLogInq.get(i).setProc_date(sdf.format(parseSdf.parse(allLogInq.get(i).getProc_date() + allLogInq.get(i).getProc_time())));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			/*if(allLogInq.get(i).getScrn_name() == null) {
				allLogInq.get(i).setScrn_name(allLogInq.get(i).getReq_url());
			}*/
		}
		
		HttpSession session = request.getSession();
		String scrn_name_view = (String) session.getAttribute("scrn_name");
		String[] columns;
		String[] heads;
		if(scrn_name_view.equals("1")) {
			columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "result_type", "cnt", "req_type", "scrn_name", "req_url"};
			heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "사용자IP", "시스템명", "개인정보유형", "합계", "수행업무", "메뉴명", "접근 경로"};
		}else if(scrn_name_view.equals("2")){
			columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "result_type", "cnt", "req_type", "req_url"};
			heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "사용자IP", "시스템명", "개인정보유형", "합계", "수행업무", "접근 경로"};
		}else {
			columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "result_type", "cnt", "req_type"};
			heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "사용자IP", "시스템명", "개인정보유형", "합계", "수행업무"};
		}
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", allLogInq);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}
	
	@Override
	public void findOnrLogInqList_downloadCSV(DataModelAndView modelAndView, SearchSearch search,
			HttpServletRequest request) {
		
		search.setUseExcel("true");
		
		String fileName = "PSM_개인정보_접속기록조회(온나라)_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		//String sheetName = "개인정보 로그조회";
		
		List<AllLogInq> allLogInq = onrLogInqDao.findOnrLogInqList(search);
		List<PrivacyType> privacyTypeList = commonDao.getPrivacyTypesExceptRegular();
		
		SimpleDateFormat parseSdf = new SimpleDateFormat("yyyyMMddHHmmss");
		SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
		
		for(int i=0; i<allLogInq.size(); i++){
			/*String result_type[] = allLogInq.get(i).getResult_type().split(",");
			
			String result_type_string = "";
			for(int j=0; j<result_type.length; j++){
				for(int k=0; k<privacyTypeList.size(); k++){
					if(privacyTypeList.get(k).getPrivacy_type().equals(result_type[j]) ) result_type_string += privacyTypeList.get(k).getPrivacy_desc(); 
				}
			}*/
			AllLogInq ali = allLogInq.get(i);
			String result_type = ali.getResult_type();
			if(result_type != null) {
				String[] arrResult_type = result_type.split(",");
				Arrays.sort(arrResult_type);
				String result_type_string = "";
				String prev = "";
				int count = 1;
				for(int j=0; j<arrResult_type.length; j++) {
					if(prev.equals(arrResult_type[j])) {
						count++;
					}else {
						if(j != 0){
							result_type_string += " " +count + ", ";
						}
						
						for(int k=0; k<privacyTypeList.size(); k++){
							if(privacyTypeList.get(k).getPrivacy_type().equals(arrResult_type[j])) { 
								result_type_string += privacyTypeList.get(k).getPrivacy_desc(); 
							}
						}
						count = 1;
						prev = arrResult_type[j];
					}
					
					if(j == arrResult_type.length - 1){
						result_type_string += " " +count;
					}
				}
				
				allLogInq.get(i).setCnt(arrResult_type.length);
				allLogInq.get(i).setResult_type(result_type_string);
			}
			
			String req_type_en=allLogInq.get(i).getReq_type();
			switch(req_type_en){
				case "RD": allLogInq.get(i).setReq_type("조회");break;
				case "CR": allLogInq.get(i).setReq_type("등록");break;
				case "UD": allLogInq.get(i).setReq_type("수정");break;
				case "DL": allLogInq.get(i).setReq_type("삭제");break;
				case "EX": allLogInq.get(i).setReq_type("다운로드");break;
				case "PR": allLogInq.get(i).setReq_type("출력");break;
				case "CO": allLogInq.get(i).setReq_type("수집");break;
				case "NE": allLogInq.get(i).setReq_type("생성");break;
				case "BE": allLogInq.get(i).setReq_type("연계");break;
				case "IN": allLogInq.get(i).setReq_type("연동");break;
				case "WR": allLogInq.get(i).setReq_type("기록");break;
				case "SA": allLogInq.get(i).setReq_type("저장");break;
				case "SU": allLogInq.get(i).setReq_type("보유");break;
				case "FI": allLogInq.get(i).setReq_type("가공");break;
				case "UP": allLogInq.get(i).setReq_type("편집");break;
				case "SC": allLogInq.get(i).setReq_type("검색");break;
				case "CT": allLogInq.get(i).setReq_type("정정");break;
				case "RE": allLogInq.get(i).setReq_type("복구");break;
				case "US": allLogInq.get(i).setReq_type("이용");break;
				case "OF": allLogInq.get(i).setReq_type("제공");break;
				case "OP": allLogInq.get(i).setReq_type("공개");break;
				case "AN": allLogInq.get(i).setReq_type("파기");break;
			}
			
			if(allLogInq.get(i).getDept_name().equals("") || allLogInq.get(i).getDept_name().equals(null)) 
				allLogInq.get(i).setDept_name("-");
			if(allLogInq.get(i).getEmp_user_name().equals("") || allLogInq.get(i).getEmp_user_name().equals(null)) 
				allLogInq.get(i).setEmp_user_name("-");
			
			try {
				allLogInq.get(i).setProc_date(sdf.format(parseSdf.parse(allLogInq.get(i).getProc_date() + allLogInq.get(i).getProc_time())));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			/*if(allLogInq.get(i).getScrn_name() == null) {
				allLogInq.get(i).setScrn_name(allLogInq.get(i).getReq_url());
			}*/
		}
		
		List<Map<String, Object>> list = new ArrayList<>();

		for(AllLogInq ali : allLogInq) {
			Map<String, Object> map = new HashMap<>();
			map.put("proc_date", ali.getProc_date());
			map.put("dept_name", ali.getDept_name());
			map.put("emp_user_id", ali.getEmp_user_id());
			map.put("emp_user_name", ali.getEmp_user_name());
			map.put("user_ip", ali.getUser_ip());
			map.put("system_name", ali.getSystem_name());
			map.put("result_type", ali.getResult_type());
			map.put("cnt", ali.getCnt());
			map.put("req_type", ali.getReq_type());
			map.put("scrn_name", ali.getScrn_name());
			map.put("req_url", ali.getReq_url());
			
			list.add(map);
		}
		
		HttpSession session = request.getSession();
		String scrn_name_view = (String) session.getAttribute("scrn_name");
		String[] columns;
		String[] heads;
		if(scrn_name_view.equals("1")) {
			columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "result_type", "cnt", "req_type", "scrn_name", "req_url"};
			heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "사용자IP", "시스템명", "개인정보유형", "합계", "수행업무", "메뉴명", "접근 경로"};
		}else if(scrn_name_view.equals("2")){
			columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "result_type", "cnt", "req_type", "req_url"};
			heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "사용자IP", "시스템명", "개인정보유형", "합계", "수행업무", "접근 경로"};
		}else {
			columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "result_type", "cnt", "req_type"};
			heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "사용자IP", "시스템명", "개인정보유형", "합계", "수행업무"};
		}
		
		modelAndView.addObject("excelData", list);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
		modelAndView.addObject("fileName", fileName);
	}
}
