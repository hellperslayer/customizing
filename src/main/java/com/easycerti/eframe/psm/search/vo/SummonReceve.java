package com.easycerti.eframe.psm.search.vo;

import com.easycerti.eframe.common.vo.AbstractValueObject;
/**
 * 
 * 설명 : 전체로그조회 VO
 * @author tjlee
 * @since 2015. 4. 22.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 22.           tjlee            최초 생성
 *
 * </pre>
 */
public class SummonReceve extends AbstractValueObject{
	/*
	biz_log
	*/
	private String proc_date;
	private String proc_time;
	private String agency_cd;
	private String desc_div;
	private long log_seq;
	private String proc_datetime;
	private String emp_cd;
	private String emp_nm;
	private String user_id;
	private String user_ip;
	private String dept_cd;
	private String dept_nm;
	private String org_cd;
	private String org_nm;
	private String hq_cd;
	private String hq_nm;
	private String posit_gu;
	private String acc_org_cd;
	private String acc_org_nm;
	private String acc_hq_cd;
	private String acc_hq_nm;
	private String bran_cd;
	private String bran_nm;
	private String nh_dept_cd;
	private String nh_dept_nm;
	private String cd_org_cd;
	private String cd_org_nm;
	private String car_cd;
	private String site_cd;
	private String member_div;
	private String join_ssn;
	private String ssn_name;
	private String sear_cont;
	private String sear_val;
	private String inq_seq;
	private String cert_num;
	private String sear_log24;
	private String per_inf_cd;
	private String esta_sym;
	private String firm_nm;
	private String assu_ssn;
	private String assu_nm;
	private String contact_condition;
	private String psnl_inf_qry_hed_colnm;
	private String psnl_inf_cnts;
	private String inq_reason;
	private String ykiho_cd;
	private String ykiho_nm;
	private String recv_no;
	private String recv_yyyy;
	private String output_pgm_id;
	private String busi_cd;
	private String busi_nm;
	private String busi_dtl_contn;
	private String inq_db;
	private String patient_cd;
	private String patient_nm;
	private String bloodno;
	private String result;
	private String button_cd;
	private String scrn_id;
	private String scrn_nm;
	private String prg_id;
	private String prg_nm;
	private String req_url;
	private String vcls;
	private String ssn_org_cd;
	private String ssn_org_nm;
	private String ssn_hq_cd;
	private String ssn_hq_nm;
	private String juri_out;
	private String firm_cd;
	private String firm_cd_nm;
	private String firm_hq_cd;
	private String firm_hq_nm;
	private String assu_org_cd;
	private String assu_org_nm;
	private String assu_hq_cd;
	private String assu_hq_nm;
	private String audit_no;
	private String grade;
	private String status;
	private String sys_cd;
	private String log_agency_cd;
	private String req_date;
	private String ans_date;
	private String summon_req_emp;
	private String resummon_yn;
	private String desc_status;
	private String desc_result;
	private String rule_nm;
	private String summon_reason;
	private String full_proc_date;
	private long biz_log_seq;
	private String detect_div;
	private String result_type;
	private String result_content;
	
	public String getResult_content() {
		return result_content;
	}
	public void setResult_content(String result_content) {
		this.result_content = result_content;
	}
	public String getResult_type() {
		return result_type;
	}
	public void setResult_type(String result_type) {
		this.result_type = result_type;
	}
	public String getDetect_div() {
		return detect_div;
	}
	public void setDetect_div(String detect_div) {
		this.detect_div = detect_div;
	}
	public String getFull_proc_date() {
		return full_proc_date;
	}
	public void setFull_proc_date(String full_proc_date) {
		this.full_proc_date = full_proc_date;
	}
	public long getBiz_log_seq() {
		return biz_log_seq;
	}
	public void setBiz_log_seq(long l) {
		this.biz_log_seq = l;
	}
	public String getProc_date() {
		return proc_date;
	}
	public void setProc_date(String proc_date) {
		this.proc_date = proc_date;
	}
	public String getAgency_cd() {
		return agency_cd;
	}
	public void setAgency_cd(String agency_cd) {
		this.agency_cd = agency_cd;
	}
	public String getDesc_div() {
		return desc_div;
	}
	public void setDesc_div(String desc_div) {
		this.desc_div = desc_div;
	}
	public long getLog_seq() {
		return log_seq;
	}
	public void setLog_seq(long log_seq) {
		this.log_seq = log_seq;
	}
	public String getProc_datetime() {
		return proc_datetime;
	}
	public void setProc_datetime(String proc_datetime) {
		this.proc_datetime = proc_datetime;
	}
	public String getEmp_cd() {
		return emp_cd;
	}
	public void setEmp_cd(String emp_cd) {
		this.emp_cd = emp_cd;
	}
	public String getEmp_nm() {
		return emp_nm;
	}
	public void setEmp_nm(String emp_nm) {
		this.emp_nm = emp_nm;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getUser_ip() {
		return user_ip;
	}
	public void setUser_ip(String user_ip) {
		this.user_ip = user_ip;
	}
	public String getDept_cd() {
		return dept_cd;
	}
	public void setDept_cd(String dept_cd) {
		this.dept_cd = dept_cd;
	}
	public String getDept_nm() {
		return dept_nm;
	}
	public void setDept_nm(String dept_nm) {
		this.dept_nm = dept_nm;
	}
	public String getOrg_cd() {
		return org_cd;
	}
	public void setOrg_cd(String org_cd) {
		this.org_cd = org_cd;
	}
	public String getOrg_nm() {
		return org_nm;
	}
	public void setOrg_nm(String org_nm) {
		this.org_nm = org_nm;
	}
	public String getHq_cd() {
		return hq_cd;
	}
	public void setHq_cd(String hq_cd) {
		this.hq_cd = hq_cd;
	}
	public String getHq_nm() {
		return hq_nm;
	}
	public void setHq_nm(String hq_nm) {
		this.hq_nm = hq_nm;
	}
	public String getPosit_gu() {
		return posit_gu;
	}
	public void setPosit_gu(String posit_gu) {
		this.posit_gu = posit_gu;
	}
	public String getAcc_org_cd() {
		return acc_org_cd;
	}
	public void setAcc_org_cd(String acc_org_cd) {
		this.acc_org_cd = acc_org_cd;
	}
	public String getAcc_org_nm() {
		return acc_org_nm;
	}
	public void setAcc_org_nm(String acc_org_nm) {
		this.acc_org_nm = acc_org_nm;
	}
	public String getAcc_hq_cd() {
		return acc_hq_cd;
	}
	public void setAcc_hq_cd(String acc_hq_cd) {
		this.acc_hq_cd = acc_hq_cd;
	}
	public String getAcc_hq_nm() {
		return acc_hq_nm;
	}
	public void setAcc_hq_nm(String acc_hq_nm) {
		this.acc_hq_nm = acc_hq_nm;
	}
	public String getBran_cd() {
		return bran_cd;
	}
	public void setBran_cd(String bran_cd) {
		this.bran_cd = bran_cd;
	}
	public String getBran_nm() {
		return bran_nm;
	}
	public void setBran_nm(String bran_nm) {
		this.bran_nm = bran_nm;
	}
	public String getNh_dept_cd() {
		return nh_dept_cd;
	}
	public void setNh_dept_cd(String nh_dept_cd) {
		this.nh_dept_cd = nh_dept_cd;
	}
	public String getNh_dept_nm() {
		return nh_dept_nm;
	}
	public void setNh_dept_nm(String nh_dept_nm) {
		this.nh_dept_nm = nh_dept_nm;
	}
	public String getCd_org_cd() {
		return cd_org_cd;
	}
	public void setCd_org_cd(String cd_org_cd) {
		this.cd_org_cd = cd_org_cd;
	}
	public String getCd_org_nm() {
		return cd_org_nm;
	}
	public void setCd_org_nm(String cd_org_nm) {
		this.cd_org_nm = cd_org_nm;
	}
	public String getCar_cd() {
		return car_cd;
	}
	public void setCar_cd(String car_cd) {
		this.car_cd = car_cd;
	}
	public String getSite_cd() {
		return site_cd;
	}
	public void setSite_cd(String site_cd) {
		this.site_cd = site_cd;
	}
	public String getMember_div() {
		return member_div;
	}
	public void setMember_div(String member_div) {
		this.member_div = member_div;
	}
	public String getJoin_ssn() {
		return join_ssn;
	}
	public void setJoin_ssn(String join_ssn) {
		this.join_ssn = join_ssn;
	}
	public String getSsn_name() {
		return ssn_name;
	}
	public void setSsn_name(String ssn_name) {
		this.ssn_name = ssn_name;
	}
	public String getSear_cont() {
		return sear_cont;
	}
	public void setSear_cont(String sear_cont) {
		this.sear_cont = sear_cont;
	}
	public String getSear_val() {
		return sear_val;
	}
	public void setSear_val(String sear_val) {
		this.sear_val = sear_val;
	}
	public String getInq_seq() {
		return inq_seq;
	}
	public void setInq_seq(String inq_seq) {
		this.inq_seq = inq_seq;
	}
	public String getCert_num() {
		return cert_num;
	}
	public void setCert_num(String cert_num) {
		this.cert_num = cert_num;
	}
	public String getSear_log24() {
		return sear_log24;
	}
	public void setSear_log24(String sear_log24) {
		this.sear_log24 = sear_log24;
	}
	public String getPer_inf_cd() {
		return per_inf_cd;
	}
	public void setPer_inf_cd(String per_inf_cd) {
		this.per_inf_cd = per_inf_cd;
	}
	public String getEsta_sym() {
		return esta_sym;
	}
	public void setEsta_sym(String esta_sym) {
		this.esta_sym = esta_sym;
	}
	public String getFirm_nm() {
		return firm_nm;
	}
	public void setFirm_nm(String firm_nm) {
		this.firm_nm = firm_nm;
	}
	public String getAssu_ssn() {
		return assu_ssn;
	}
	public void setAssu_ssn(String assu_ssn) {
		this.assu_ssn = assu_ssn;
	}
	public String getAssu_nm() {
		return assu_nm;
	}
	public void setAssu_nm(String assu_nm) {
		this.assu_nm = assu_nm;
	}
	public String getContact_condition() {
		return contact_condition;
	}
	public void setContact_condition(String contact_condition) {
		this.contact_condition = contact_condition;
	}
	public String getPsnl_inf_qry_hed_colnm() {
		return psnl_inf_qry_hed_colnm;
	}
	public void setPsnl_inf_qry_hed_colnm(String psnl_inf_qry_hed_colnm) {
		this.psnl_inf_qry_hed_colnm = psnl_inf_qry_hed_colnm;
	}
	public String getPsnl_inf_cnts() {
		return psnl_inf_cnts;
	}
	public void setPsnl_inf_cnts(String psnl_inf_cnts) {
		this.psnl_inf_cnts = psnl_inf_cnts;
	}
	public String getInq_reason() {
		return inq_reason;
	}
	public void setInq_reason(String inq_reason) {
		this.inq_reason = inq_reason;
	}
	public String getYkiho_cd() {
		return ykiho_cd;
	}
	public void setYkiho_cd(String ykiho_cd) {
		this.ykiho_cd = ykiho_cd;
	}
	public String getYkiho_nm() {
		return ykiho_nm;
	}
	public void setYkiho_nm(String ykiho_nm) {
		this.ykiho_nm = ykiho_nm;
	}
	public String getRecv_no() {
		return recv_no;
	}
	public void setRecv_no(String recv_no) {
		this.recv_no = recv_no;
	}
	public String getRecv_yyyy() {
		return recv_yyyy;
	}
	public void setRecv_yyyy(String recv_yyyy) {
		this.recv_yyyy = recv_yyyy;
	}
	public String getOutput_pgm_id() {
		return output_pgm_id;
	}
	public void setOutput_pgm_id(String output_pgm_id) {
		this.output_pgm_id = output_pgm_id;
	}
	public String getBusi_cd() {
		return busi_cd;
	}
	public void setBusi_cd(String busi_cd) {
		this.busi_cd = busi_cd;
	}
	public String getBusi_nm() {
		return busi_nm;
	}
	public void setBusi_nm(String busi_nm) {
		this.busi_nm = busi_nm;
	}
	public String getBusi_dtl_contn() {
		return busi_dtl_contn;
	}
	public void setBusi_dtl_contn(String busi_dtl_contn) {
		this.busi_dtl_contn = busi_dtl_contn;
	}
	public String getInq_db() {
		return inq_db;
	}
	public void setInq_db(String inq_db) {
		this.inq_db = inq_db;
	}
	public String getPatient_cd() {
		return patient_cd;
	}
	public void setPatient_cd(String patient_cd) {
		this.patient_cd = patient_cd;
	}
	public String getPatient_nm() {
		return patient_nm;
	}
	public void setPatient_nm(String patient_nm) {
		this.patient_nm = patient_nm;
	}
	public String getBloodno() {
		return bloodno;
	}
	public void setBloodno(String bloodno) {
		this.bloodno = bloodno;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getButton_cd() {
		return button_cd;
	}
	public void setButton_cd(String button_cd) {
		this.button_cd = button_cd;
	}
	public String getScrn_id() {
		return scrn_id;
	}
	public void setScrn_id(String scrn_id) {
		this.scrn_id = scrn_id;
	}
	public String getScrn_nm() {
		return scrn_nm;
	}
	public void setScrn_nm(String scrn_nm) {
		this.scrn_nm = scrn_nm;
	}
	public String getPrg_id() {
		return prg_id;
	}
	public void setPrg_id(String prg_id) {
		this.prg_id = prg_id;
	}
	public String getPrg_nm() {
		return prg_nm;
	}
	public void setPrg_nm(String prg_nm) {
		this.prg_nm = prg_nm;
	}
	public String getReq_url() {
		return req_url;
	}
	public void setReq_url(String req_url) {
		this.req_url = req_url;
	}
	public String getVcls() {
		return vcls;
	}
	public void setVcls(String vcls) {
		this.vcls = vcls;
	}
	public String getSsn_org_cd() {
		return ssn_org_cd;
	}
	public void setSsn_org_cd(String ssn_org_cd) {
		this.ssn_org_cd = ssn_org_cd;
	}
	public String getSsn_org_nm() {
		return ssn_org_nm;
	}
	public void setSsn_org_nm(String ssn_org_nm) {
		this.ssn_org_nm = ssn_org_nm;
	}
	public String getSsn_hq_cd() {
		return ssn_hq_cd;
	}
	public void setSsn_hq_cd(String ssn_hq_cd) {
		this.ssn_hq_cd = ssn_hq_cd;
	}
	public String getSsn_hq_nm() {
		return ssn_hq_nm;
	}
	public void setSsn_hq_nm(String ssn_hq_nm) {
		this.ssn_hq_nm = ssn_hq_nm;
	}
	public String getJuri_out() {
		return juri_out;
	}
	public void setJuri_out(String juri_out) {
		this.juri_out = juri_out;
	}
	public String getFirm_cd() {
		return firm_cd;
	}
	public void setFirm_cd(String firm_cd) {
		this.firm_cd = firm_cd;
	}
	public String getFirm_cd_nm() {
		return firm_cd_nm;
	}
	public void setFirm_cd_nm(String firm_cd_nm) {
		this.firm_cd_nm = firm_cd_nm;
	}
	public String getFirm_hq_cd() {
		return firm_hq_cd;
	}
	public void setFirm_hq_cd(String firm_hq_cd) {
		this.firm_hq_cd = firm_hq_cd;
	}
	public String getFirm_hq_nm() {
		return firm_hq_nm;
	}
	public void setFirm_hq_nm(String firm_hq_nm) {
		this.firm_hq_nm = firm_hq_nm;
	}
	public String getAssu_org_cd() {
		return assu_org_cd;
	}
	public void setAssu_org_cd(String assu_org_cd) {
		this.assu_org_cd = assu_org_cd;
	}
	public String getAssu_org_nm() {
		return assu_org_nm;
	}
	public void setAssu_org_nm(String assu_org_nm) {
		this.assu_org_nm = assu_org_nm;
	}
	public String getAssu_hq_cd() {
		return assu_hq_cd;
	}
	public void setAssu_hq_cd(String assu_hq_cd) {
		this.assu_hq_cd = assu_hq_cd;
	}
	public String getAssu_hq_nm() {
		return assu_hq_nm;
	}
	public void setAssu_hq_nm(String assu_hq_nm) {
		this.assu_hq_nm = assu_hq_nm;
	}
	public String getAudit_no() {
		return audit_no;
	}
	public void setAudit_no(String audit_no) {
		this.audit_no = audit_no;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSys_cd() {
		return sys_cd;
	}
	public void setSys_cd(String sys_cd) {
		this.sys_cd = sys_cd;
	}
	public String getLog_agency_cd() {
		return log_agency_cd;
	}
	public void setLog_agency_cd(String log_agency_cd) {
		this.log_agency_cd = log_agency_cd;
	}
	public String getReq_date() {
		return req_date;
	}
	public void setReq_date(String req_date) {
		this.req_date = req_date;
	}
	public String getAns_date() {
		return ans_date;
	}
	public void setAns_date(String ans_date) {
		this.ans_date = ans_date;
	}
	public String getSummon_req_emp() {
		return summon_req_emp;
	}
	public void setSummon_req_emp(String summon_req_emp) {
		this.summon_req_emp = summon_req_emp;
	}
	public String getResummon_yn() {
		return resummon_yn;
	}
	public void setResummon_yn(String resummon_yn) {
		this.resummon_yn = resummon_yn;
	}
	public String getDesc_status() {
		return desc_status;
	}
	public void setDesc_status(String desc_status) {
		this.desc_status = desc_status;
	}
	public String getDesc_result() {
		return desc_result;
	}
	public void setDesc_result(String desc_result) {
		this.desc_result = desc_result;
	}
	public String getRule_nm() {
		return rule_nm;
	}
	public void setRule_nm(String rule_nm) {
		this.rule_nm = rule_nm;
	}
	public String getSummon_reason() {
		return summon_reason;
	}
	public void setSummon_reason(String summon_reason) {
		this.summon_reason = summon_reason;
	}
	public String getProc_time() {
		return proc_time;
	}
	public void setProc_time(String proc_time) {
		this.proc_time = proc_time;
	}
	
	
	
}
