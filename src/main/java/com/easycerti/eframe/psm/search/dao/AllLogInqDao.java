package com.easycerti.eframe.psm.search.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.easycerti.eframe.psm.dashboard.vo.Dashboard;
import com.easycerti.eframe.psm.search.vo.AllLogInq;
import com.easycerti.eframe.psm.search.vo.AllLogSql;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInq;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.search.vo.SummonReceve;
import com.easycerti.eframe.psm.setup.vo.DLogMenuMappSetup;
import com.easycerti.eframe.psm.system_management.vo.Misdetect;

/**
 * 
 * 설명 : 전체로그조회 리스트
 * @author tjlee
 * @since 2015. 4. 22.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 22.           tjlee            최초 생성
 *
 * </pre>
 */

public interface AllLogInqDao {
	/**
	 * 전체로그조회 리스트
	 */
	public List<AllLogInq> findAllLogInqList(SearchSearch search);
	public long findAllLogInqOne_count(SearchSearch search);

	/**
	 * 전체로그조회 상세
	 */
	public AllLogInq findAllLogInqDetail(SearchSearch search);
	public List<AllLogInq> findAllLogInqDetailResult(SearchSearch search);
	public List<AllLogInq> findAllLogInqDetailResult2(SearchSearch search); //탐지예외처리 개인정보 평문노출 이슈
	public List<AllLogInq> findAllLogInqDetailResultOwner(SearchSearch search);	//[재정정보원] 접속기록조회 상세 화면 커스텀(정보주체자)
	public int findAllLogInqDetailResultCount(SearchSearch search);	
	public String checkPattern(Misdetect misdetect);
	public void addPrivacy(Misdetect misdetect);
	public void removePrivacy(Misdetect misdetect);
	public void addGoogle(Dashboard dashboard);
	public void updateGoogle(Dashboard dashboard);	
	public void deleteGoogle(Dashboard dashboard);	
	public List<Dashboard> findDashboardList();
	public int findAllLogInqDetailLogSeq(SearchSearch search);
	
	
	public ExtrtCondbyInq finallLogInqListByEmpCd(SearchSearch search);
	
	public String findSummon(String log_seq);
	
	public int findReadInfoList_count(SearchSearch search);
	public List<AllLogInq> findReadInfoList(SearchSearch search);
	public String getResultContentByAes(SearchSearch privacy);
	
	public AllLogInq findAllLogInqDetailBody(SearchSearch search);
	public AllLogInq findAllLogInqDetailQuery(SearchSearch search);
	public AllLogInq findAllLogInqDetailQuery_YJ(SearchSearch search);
	
	public List<AllLogInq> findLogInqFileList(SearchSearch search);
	public List<AllLogInq> findLogInqApprovalList(SearchSearch search);
	
	public List<AllLogInq> findAllLogInqListByReqType(SearchSearch search);
	
	public List<AllLogInq> getResultTypeListByLogseq(SearchSearch search);
	public AllLogInq findPrivacyInfo(SearchSearch search);

	public int addSummonBizLogSummary(SummonReceve summonReceve);
	public void addSummonBizLog(SummonReceve summonReceve);
	public void addSummonBizLogResult(SummonReceve summonReceve);
	
	public List<SummonReceve> findBizLog80(long l);
	public List<AllLogInq> getResultTypeByOne(SearchSearch search);
	public AllLogInq getThresholdInfo(SearchSearch search);
	public int systemExistChk(SearchSearch search);
	public int insertThreshold(SearchSearch search);
	public int updateThreshold(SearchSearch search);
	public List<AllLogInq> findAllLogInqRegisteredDetail(SearchSearch search);
	public List<DLogMenuMappSetup> finddLogMenuMappListBySystem(SearchSearch search);
	
	public List<String> allLogSqlCtAndHasPrivacyInfo(AllLogSql allLogSql);
	public AllLogSql allLogSqlInfo(AllLogSql allLogSql);
	public List<AllLogSql> allLogSqlResultList(AllLogSql allLogSql);
	public int allLogSqlResultListCt(AllLogSql allLogSql);
	public void updateallLogSqlCollectYn(AllLogSql allLogSql);
	public int allLogSqlResultCt(AllLogSql allLogSql);
	
	public List<AllLogSql> logSqlSeqList(AllLogSql allLogSql);
	
	public List<AllLogInq> findLogCountBySystem(SearchSearch search);
	public String emailChk(String id);
	public int updateTmpPwd(@Param("id")String id, @Param("tmpPwd")String tmpPwd);
	
	public int findAllLogInqOne_count_kdic(SearchSearch search);
	public List<AllLogInq> findAllLogInqList_kdic(SearchSearch search);
	public List<AllLogInq> findAllLogInqDetail_sqlPrivacyInfo(SearchSearch search);
	public int findAllLogInqDetail_sqlPrivacyInfo_count(SearchSearch search);
	public List<AllLogInq> findAllLogInqDetailResult_kdic(SearchSearch search);
}
