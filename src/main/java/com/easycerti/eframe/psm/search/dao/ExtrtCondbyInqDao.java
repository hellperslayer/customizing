package com.easycerti.eframe.psm.search.dao;

import java.util.List;
import java.util.Map;

import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.dashboard.vo.Dashboard;
import com.easycerti.eframe.psm.search.vo.AllLogInq;
import com.easycerti.eframe.psm.search.vo.EmpDetailInq;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInq;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInqDetailChart;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInqTimeline;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInqTimelineCond;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.setup.vo.ExtrtBaseSetup;
import com.easycerti.eframe.psm.statistics.vo.Statistics;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;
import com.easycerti.eframe.psm.system_management.vo.Delegation;
import com.easycerti.eframe.psm.system_management.vo.EmpUser;

/**
 * 
 * 설명 : 추출조건별조회 Dao Interface
 * @author tjlee
 * @since 2015. 4. 22.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 22.           tjlee            최초 생성
 *   2017. 5. 24.			sylee			detailCart2_5관련내용 추가	
 *
 * </pre>
 */
public interface ExtrtCondbyInqDao {
	/**
	 *추출조건별조회 리스트
	 */
	public List<ExtrtCondbyInq> findExtrtCondbyInqList(SearchSearch search);
	public int findExtrtCondbyInqOne_count(SearchSearch search);
	public List<ExtrtCondbyInq> findSummonList(SearchSearch search);
	public int findSummon_count(SearchSearch search);
	
	public List<ExtrtCondbyInq> findExtrtCondbyInqDaily(SearchSearch search);
	public List<ExtrtCondbyInq> findExtrtCondbyInqDept(SearchSearch search);
	public List<ExtrtCondbyInq> findExtrtCondbyInqUser(SearchSearch search);
	public List<ExtrtCondbyInq> findExtrtCondbyInqSystem(SearchSearch search);
	
	public ExtrtCondbyInq findExtrtCondbyInqDailyMax(SearchSearch search);
	public ExtrtCondbyInq findExtrtCondbyInqDeptMax(SearchSearch search);
	public ExtrtCondbyInq findExtrtCondbyInqUserMax(SearchSearch search);
	public ExtrtCondbyInq findExtrtCondbyInqSystemMax(SearchSearch search);
	
	public ExtrtCondbyInq findExtrtCondbyInqDailySum(SearchSearch search);
	public ExtrtCondbyInq findExtrtCondbyInqDeptSum(SearchSearch search);
	public ExtrtCondbyInq findExtrtCondbyInqUserSum(SearchSearch search);
	public ExtrtCondbyInq findExtrtCondbyInqSystemSum(SearchSearch search);
	
	public List<ExtrtCondbyInq> extrtCondbyInqList(SearchSearch search);
	public int extrtCondbyInqListCt(SearchSearch search);
	
	public List<ExtrtCondbyInq> extrtCondbyInqDetailList(SearchSearch search);
	//상세시나리오 - 파일 다운로드인 경우 파일명 정보 조회
	public List<ExtrtCondbyInq> extrtCondbyInqFileNameList(SearchSearch search);
	public int extrtCondbyInqDetailListCt(SearchSearch search);
	public String resultTypeName(String result_type);
	
	/**
	 *소명판정 리스트
	 */
	public List<ExtrtCondbyInq> findSummonManageList(SearchSearch search);
	public int findSummonManage_count(SearchSearch search);
	
	/**
	 *추출조건별조회 상세
	 */
	public EmpUser findEmpUser(ExtrtCondbyInq param);
	public ExtrtCondbyInq findExtrtCondbyInqDetail(ExtrtCondbyInq param);
//	비정상 위험현황 top10테이블 데이터
	public List<Dashboard> findAbnormalEmpTop10Info(Dashboard dashboard);
	public List<Dashboard> findAbnormalEmpTop10List(Dashboard dashboard);
	
//	타임라인 데이터
	public List<ExtrtCondbyInqTimeline> rule_list();
	public List<ExtrtCondbyInqTimeline> findExtrtCondbyInqTimeline_rule(ExtrtCondbyInqTimelineCond condition);	
	public List<ExtrtCondbyInqTimeline> findExtrtCondbyInqTimelineAll(ExtrtCondbyInqTimelineCond condition);
	
//	비정상 위험분석 상단 카운트 데이터
	public int countOfemp(SearchSearch search);
	public int countOfdept(SearchSearch search);
	public int countOfempByWeek(SearchSearch search);
	public int countOfdeptByWeek(SearchSearch search);
	public ExtrtCondbyInq empDetailCountByWeek(SearchSearch search);
	
	public ExtrtCondbyInq findExtrtCondbyInqListByEmpCd(SearchSearch search);
	public ExtrtCondbyInq findExtrtCondbyInqListByDescSeq(SearchSearch search);
	public ExtrtCondbyInq findExtrtCondbyInqListByDescSeqCenter(SearchSearch search);
	public List<AllLogInq> findExtrtCondbyInqListByLogList(SearchSearch search);
	public List<AllLogInq> findExtrtCondbyInqListByDownloadLogList(SearchSearch search);
	public List<AllLogInq> findExtrtCondbyInqListByLogList_summon(SearchSearch search);
	public List<AllLogInq> findExtrtCondbyInqDownloadLogListByLogList_summon(SearchSearch search);
	public List<AllLogInq> findBizLog(List logSeqs);
	public List<ExtrtCondbyInq> findSummonMemo(String desc_seq);
	public ExtrtCondbyInq findSummonRaw(String desc_seq);
	public ExtrtCondbyInq findSummonResult(String desc_seq);
	public ExtrtCondbyInq findSummonResult2(String desc_seq);
	public int findExtrtCondbyInqListByLogListCount(SearchSearch search);
	public int findExtrtCondbyInqListByDownloadLogListCount(SearchSearch search);
	public int findLogSeq(SearchSearch search);
	public List<ExtrtBaseSetup> findRuleTblList();
	public List<ExtrtBaseSetup> findRuleTblListByScenSeq(SearchSearch search);
	public List<ExtrtCondbyInq> getBizLogSeq(SearchSearch search);
	public List<EmpUser> findEmpUserCond(SearchSearch search);
	public int findEmpUserCondCount(SearchSearch search);
	public int findEmpUserCondCount_downloadLog(SearchSearch search);
	public void sendRequest(SearchSearch search);
	public void setBizLogStatus(ExtrtCondbyInq param);
	public List<String> getLogSeqBySummonList(SearchSearch search);
	public void setCheckExtrtCondbyInqDetail(SearchSearch search);
	
	public List<Dashboard> findLineChartDay(SearchSearch search);
	public List<Dashboard> findLineChartMonth(SearchSearch paramBean);
	public List<Dashboard> findLineChartYear(SearchSearch paramBean);
	public List<AllLogInq> detailPieChart(SearchSearch search);
	public List<AllLogInq> detailBarChart(SearchSearch paramBean);
	public List<ExtrtCondbyInqDetailChart> getChart0(SearchSearch paramBean);
	public List<ExtrtCondbyInqDetailChart> getChart1(SearchSearch paramBean);
	public List<ExtrtCondbyInqDetailChart> getChart2(SearchSearch paramBean);
	public String getDeptName(SearchSearch search);
	public List<ExtrtCondbyInq> findAbnormalList(SearchSearch search);
	public List<ExtrtCondbyInq> findAbnormalChart0(SearchSearch search);
	public List<ExtrtCondbyInq> findAbnormalChart1(SearchSearch search);
	
	public List<EmpDetailInq> findAbnormalDeptDonutChart(SearchSearch search);
	public int findAbnormalDeptDonutChartAll(SearchSearch search);
	public List<EmpDetailInq> findAbnormalUserDonutChart(SearchSearch search);
	public int findAbnormalUserDonutChartAll(SearchSearch search);
	
	public ExtrtCondbyInqDetailChart getExtrtCondbyInqDetail1(SearchSearch search);
	public List<ExtrtCondbyInqDetailChart> getExtrtCondbyInqDetail2(SearchSearch search);
	public List<ExtrtCondbyInqDetailChart> getExtrtCondbyInqDetail1_1(String user_id);
	public List<ExtrtCondbyInqDetailChart> getExtrtCondbyInqDetail2_1(SearchSearch search);
	
	public int findAbnormalDetailDngChart(ExtrtCondbyInq param);
	
	public List<ExtrtCondbyInq> findAbnormalDetailChart1(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDetailChart2(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDetailChart3(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDetailChart4(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDetailChart5(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDetailChart20(ExtrtCondbyInq param); //
	
	public List<ExtrtCondbyInq> findAbnormalDetailChart1_1(String emp_user_name);
	public List<ExtrtCondbyInq> findAbnormalDetailChart2_1(String emp_user_name);
	public List<ExtrtCondbyInq> findAbnormalDetailChart3_1(String emp_user_name);
	public List<ExtrtCondbyInq> findAbnormalDetailChart4_1(String emp_user_name);
	public List<ExtrtCondbyInq> findAbnormalDetailChart5_1(String emp_user_name);
	
	public List<ExtrtCondbyInq> findAbnormalDetailChart1_2(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDetailChart2_2(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDetailChart3_2(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDetailChart4_2(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDetailChart5_2(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDetailChart20_2(ExtrtCondbyInq param); //
	
	public List<ExtrtCondbyInq> findAbnormalDetailChart1_3(String dept_id);
	public List<ExtrtCondbyInq> findAbnormalDetailChart2_3(String dept_id);
	public List<ExtrtCondbyInq> findAbnormalDetailChart3_3(String dept_id);
	public List<ExtrtCondbyInq> findAbnormalDetailChart4_3(String dept_id);
	public List<ExtrtCondbyInq> findAbnormalDetailChart5_3(String dept_id);
	
	/* 비정상위험분석 시나리오별 카운트 */
	public List<ExtrtCondbyInq> countByScenario(SearchSearch search);
	public List<ExtrtCondbyInq> countByScenarioByday(SearchSearch search);
	public List<EmpDetailInq> scenarioList();
	/**
	 *비정상위험분석 상세차트:개인정보 취급량
	 */
	public List<ExtrtCondbyInq> findAbnormalDetailChart6(SearchSearch search);
	public List<ExtrtCondbyInq> findAbnormalDetailChart7(SearchSearch search);
	public List<ExtrtCondbyInq> findAbnormalDetailChart7_1(SearchSearch search);
	/**
	 *비정상위험분석 상세차트:월별 누적량 비교
	 */
	public List<ExtrtCondbyInq> findAbnormalDetailChart8(SearchSearch search);
	/**
	 *비정상위험분석 상세차트:요일별 패턴
	 */
	public List<ExtrtCondbyInq> findAbnormalDetailChart9(SearchSearch search);
	public List<ExtrtCondbyInq> findAbnormalDetailChart9_1(SearchSearch search);
	/**
	 *비정상위험분석 상세차트:24시간 패턴
	 */
	public List<ExtrtCondbyInq> findAbnormalDetailChart10(SearchSearch search);
	public List<ExtrtCondbyInq> findAbnormalDetailChart10_1(SearchSearch search);
	/**
	 *비정상위험분석 상세차트:취급유형
	 */
	public List<ExtrtCondbyInq> findAbnormalDetailChart11(ExtrtCondbyInq eci);
	public List<Statistics> findAbnormalDetailChart12(ExtrtCondbyInq eci);
	public List<ExtrtCondbyInq> findAbnormalDetailChart12_1(ExtrtCondbyInq eci);
	public List<ExtrtCondbyInq> findAbnormalDetailChart13(ExtrtCondbyInq eci);
	public List<ExtrtCondbyInq> findAbnormalDetailChart14(ExtrtCondbyInq eci);

	public Object findAbnormalDetailChart6_TOT(SearchSearch search);
	public Object findAbnormalDetailChart7_TOT(SearchSearch search);
	public Object findAbnormalDetailChart8_TOT(SearchSearch search);
	public Object findAbnormalDetailChart9_TOT(SearchSearch search);
	public Object findAbnormalDetailChart10_TOT(SearchSearch search);
	public List<ExtrtBaseSetup> getExtrtCondByAvgCnt(String emp_user_name);
	
	/**
	 *부서별 비정상위험분석 상세차트
	 */
	public List<ExtrtCondbyInqDetailChart> getExtrtCondbyInqDeptDetail1(SearchSearch search);
	public List<ExtrtCondbyInqDetailChart> getExtrtCondbyInqDeptDetail2(SearchSearch search);
	
	public List<ExtrtCondbyInq> getUserByDeptId(SearchSearch search);
	public List<ExtrtCondbyInq> getDeptLank(SearchSearch search);
	public List<ExtrtCondbyInq> getDeptLank_new(SearchSearch search);

	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart1(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart2(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart3(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart4(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart5(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart20(ExtrtCondbyInq param);
	
	public int findAbnormalDeptDetailDngChart(ExtrtCondbyInq param);
	
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart1_1(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart2_1(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart3_1(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart4_1(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart5_1(ExtrtCondbyInq param);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart20_1(ExtrtCondbyInq param);
	
	/**
	 *부서별 비정상위험분석 상세차트:개인정보 취급량
	 */
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart6(SearchSearch search);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart7(SearchSearch search);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart7_1(SearchSearch search);
	/**
	 *부서별 비정상위험분석 상세차트:월별 누적량 비교
	 */
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart8(SearchSearch search);
	/**
	 *부서별 비정상위험분석 상세차트:요일별 패턴
	 */
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart9(SearchSearch search);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart9_1(SearchSearch search);
	/**
	 *부서별 비정상위험분석 상세차트:24시간 패턴
	 */
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart10(SearchSearch search);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart10_1(SearchSearch search);
	/**
	 *부서별 비정상위험분석 상세차트:취급유형
	 */
	public Statistics findAbnormalDeptDetailChart11(ExtrtCondbyInq eci);
	public List<Statistics> findAbnormalDeptDetailChart12(ExtrtCondbyInq eci);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart12_1(ExtrtCondbyInq eci);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart13(ExtrtCondbyInq eci);
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart14(ExtrtCondbyInq eci);
	
	public Object findAbnormalDeptDetailChart6_TOT(SearchSearch search);
	public Object findAbnormalDeptDetailChart7_TOT(SearchSearch search);
	public Object findAbnormalDeptDetailChart8_TOT(SearchSearch search);
	public Object findAbnormalDeptDetailChart9_TOT(SearchSearch search);
	public Object findAbnormalDeptDetailChart10_TOT(SearchSearch search);

	/**
	 * 소명 요청 (단건)
	 * 
	 */
	public void addSummonOne(AdminUser adminUser);
	public void addSummonOne2(AdminUser adminUser);
	
	public void reSummonOne(AdminUser adminUser);
	public void reSummonOne2(AdminUser adminUser);
	public void reSummonOne3(AdminUser adminUser);
	
	public void addSummonDecision(AdminUser adminUser);
	public void addSummonResult(AdminUser adminUser);
	
	public void setSummonStatusEnd(Map map);
	public void addSummonMemo(AdminUser adminUser);
	public int getTotalPrivCount(ExtrtCondbyInq summon);
	
	public List<AllLogInq> getResultTypeByOne(SearchSearch search);
	public List<AllLogInq> getResultTypeByOne_DownloadLog(SearchSearch search);
	public List<String> getPrivacyTypes();
	
	public String getMaxDescSeqBySummon();
	public void updateSummonRaw(AdminUser adminUser);
	public String findDescLogType(SearchSearch search);
	public List<AllLogInq> findDownloadLog(List logSeqs);
	
	
	/** 20180314 sysong 소명판정 판정자 위힘  **/
	public String auth_ids(AdminUser adminUser);
	public List<ExtrtCondbyInq> findSummonDelegation(SearchSearch search);
	public List<Delegation> findDelegation(SearchSearch search);
	
	/** 20180314 sysong 소명판정 판정자 위임 판정자 추가  **/
	public int checkUserAdmin(AdminUser adminUser);
	public Map<String, String> findUserAdmin(AdminUser adminUser);
	public void updateDelegate(Delegation delegation);
	public void addUserAdmin(AdminUser adminUser);
	public void addDelegate(Delegation delegation);
	
	/** 20180314 sysong 소명판정 판정자 위임 판정자 삭제 **/
	public void summonUserRemove(Delegation delegation);
	public void summonAdminUserRemove(Delegation delegation);
	
	/** 20180314 sysong 월별 소명 현황 **/
	public List<ExtrtBaseSetup> findSystemList();
	public List<ExtrtCondbyInq> findsummonPerby(SearchSearch search);
	
	/** 20180314 sysong 소속별 소명 현황 **/
	public List<ExtrtCondbyInq> findsummonDeptby(SearchSearch search);
	public int findsummonDeptby_Count(SearchSearch search);
	
	/** 20180314 sysong 개인별 소명 현황 **/
	public List<ExtrtCondbyInq> findsummonIndvby(SearchSearch search);
	public int findsummonIndvby_Count(SearchSearch search);
	
	/** 20180314 sysong 소명요청 및 판정 현황 **/
	public List<ExtrtCondbyInq> findsummonProcessby(SearchSearch search);
	
	public List<ExtrtCondbyInq> findExtrtCondbyInqExcel(SearchSearch search);
	public String findOtionSettingValue();
	
	public List<ExtrtCondbyInq> findScenarioByScenSeq(SearchSearch search);
	
	public List<ExtrtCondbyInq> findExtrtCondbyInqListByList_info(SearchSearch search);
	public int findExtrtCondbyInqListByListCount_info(SearchSearch search);
	
	public void addAutoSummonOne(AdminUser adminUser);
	public void addAutoSummonOne2(AdminUser adminUser);
	
	public void saveFollowUp(ExtrtCondbyInq extrtCondbyInq);
	public void updateExtractResultSummon(long extract_result_seq);
	public void updateExtractResultSummonDelete(long l);
	
	public List<String> findDescLogSeq(SearchSearch search);
	public List<AdminUser> findLogseqsGroupById(AdminUser adminUser);
	
	
	/**
	 * 소명 요청 (단건), 스키마 변경
	 */
	public int addDescInfo(AdminUser adminUser);
	public void addDescLog(AdminUser adminUser);
	public void addDescHistory(AdminUser adminUser);
	
	public long getDescSeq();
	public void updateDescInfoForDecision(AdminUser adminUser);
	public void updateDescInfoForReDecision(AdminUser adminUser);
	public void removeSummonResult(AdminUser adminUser);
	public void updateSummonResult(AdminUser adminUser);
	public void updateDescInfoForDecisionResult2Clear(AdminUser adminUser);
	
	//소명요청 상세 > 소명요청 삭제(emp_detail)
	public void removeSummonOne_emp_detail(AdminUser paramBean);
	//소명요청 상세 > 소명요청 삭제(rule_biz)
	public void removeSummonOne_rule_biz(AdminUser paramBean);
		
	/**
	 * 작업 내용 : 소명 프로세스 
	 * 작업자 : 노홍현 
	 * 작업 기간 : 2020.08.25 ~
	 */
	public String checkSelfSummon(AdminUser adminUser);		//소명대리인 사용 유무
	public AdminUser findSummon_user(AdminUser adminUser);	//소명대리인 추출(개인 옵션)
	public AdminUser findSummon_dept(AdminUser adminUser);	//소명대리인 추출(부서 옵션)
	public void addSummonInfo(AdminUser adminUser);			//소명이력 생성
	public void addSummonHistory(AdminUser adminUser);		//소명이력 생성
	public void reSummonInfo(AdminUser adminUser);			//재소명 업데이트
	public void decisionSummonInfo(AdminUser adminUser);	//소명판정 업데이트
	public List<ExtrtCondbyInq> findSummonHistory(SearchSearch search);//소명이력 조회
	public ExtrtCondbyInq findSummon_response(SearchSearch search);	//소명응답 내용
	public ExtrtCondbyInq findSummon_decision(SearchSearch search);	//소명판정 내용 
	public AdminUser findSummonInfoOne(AdminUser adminUser);//소명메일 추출조건정보
	public EmpUser findEmail_addr(AdminUser adminUser);		//emp_user테이블에서 메일 추출
	public String encodeSummon_param(AdminUser adminUser);	//emp_detail_seq 암호화
	public String findSummonStatus(String emp_detail_seq);	//소명답변 업로드 기능 유효성검사
	public String decryptSummonKey(String summonKey);	//소명답변 업로드 기능 유효성검사
	
	/**
	 * 소명 알람관련
	 */
	public List<AdminUser> findDescAppr();
	public List<AdminUser> findSummonManagerList(AdminUser adminUser);
	
	public void updateDescStatus(AdminUser adminUser);	
	public void updateDescApprCheck(AdminUser adminUser);
	public void updateDescInfoResponseClear(AdminUser adminUser);
	public void addDescAppr(AdminUser adminUser);
	
	public String findSummonTotalUserList();
	public List<AdminUser> findAlarmInfo();
	public List<AdminUser> findAlarmRequestInfo();
	public List<AdminUser> findAlarmResponseInfo();
	public List<AdminUser> findAlarmJudgementInfo();
	public List<AdminUser> findAlarmCancelInfo();
	public List<AdminUser> findAlarmSenderInfo();
	public AdminUser findLastBizLog(long log_seq);
	public AdminUser findLastDownloadLog(long log_seq);
	public AdminUser findLastDbAccessLog(long log_seq);	
	public AdminUser findApprSummonHtml();
	public AllLogInq findApprSummonInfo(long logSeq);
	public String findDeptNameByTargetUserId(AdminUser adminUser);
	
	public List<ExtrtCondbyInq> findAbuseInfoList(SearchSearch search);
	public List<ExtrtCondbyInq> findAbuseInfoListExcel(SearchSearch search);
	public int findAbuseInfoListCount(SearchSearch search);
	public ExtrtCondbyInq findAbuseInfoDetail(SearchSearch search);
	public int findAbuseInfoDetailCount(SearchSearch search);
	
	public void addAbuseInfoDummy(AdminUser adminUser);
	public void saveAbuseInfo(AdminUser adminUser);
	public List<ExtrtCondbyInq> findCodeInfo(String group_code_id);
	
	public List<ExtrtBaseSetup> findRuleList(SearchSearch search);
	public int findRuleListCount(SearchSearch search);
	public ExtrtCondbyInq findRuleScript(int rule_seq);

	public void truncateSimulateEmpDetail();
	public void truncateRuleExtractTemp();
	public void executeSql(ExtrtCondbyInq extrtCondbyInq);
	public List<ExtrtCondbyInq> findBizLogExtractTemp();
	public List<String> findRuleExtractTempDate();
	public String findDescApprMode();

	public List<ExtrtCondbyInq> findCenterSummonList(SearchSearch search);
	public int findCenterSummonListCount(SearchSearch search);
	public long findApprSeq(long desc_seq);
	public int findEmpUserCondCount_dbacLog(SearchSearch search);
	public List<AllLogInq> findExtrtCondbyInqDbacLogListByLogList_summon(SearchSearch search);
	
	
	public List<AllLogInq> timeLineList(SearchSearch search);
	public Integer timeLineListCt(SearchSearch search);
	public List<AllLogInq> timeLineListByUser(SearchSearch search);
	
	public String empUserIDExample();
	public ExtrtCondbyInqDetailChart empUserInfo(String emp_user_id);
	public ExtrtCondbyInqDetailChart findEmpUserInfo(SearchSearch search);
	
	public Statistics findAbnormalDetailChart11_new(ExtrtCondbyInq eci);
	public List<Code> resultTypeList_chart();
	
	public List<EmpUser> extractEmpUserInfo(SearchSearch search);
	public List<EmpUser> extractDnEmpUserInfo(SearchSearch search);
	public ExtrtCondbyInq extractDetailScenarioInfo(SearchSearch search);
	public int extractLogCount(SearchSearch search);
	public List<AllLogInq> extractLogList(SearchSearch search);
	public String privacySeqInfo(String rule_cd);
	
	//소명위임, 판정위임
	public int addSummonDelegatePersonal(ExtrtCondbyInq search);
	public void addSummonDelegateHist(ExtrtCondbyInq summonUser);
	public List<ExtrtCondbyInq> findDelegateList(Map<String, String> parameters);
	public List<ExtrtCondbyInq> findDelegateHistList(Map<String, String> parameters);
	public void deleteSummonDelegatePersonal(ExtrtCondbyInq summonUser);
	public ExtrtCondbyInq findDelegateOne(Map<String, String> parameters);
	public List<ExtrtCondbyInq> findDelegateList_dept(Map<String, String> parameters);
	public List<ExtrtCondbyInq> findDelegateHistList_dept(Map<String, String> parameters);
	public int addSummonDelegateDept(ExtrtCondbyInq search);
	public ExtrtCondbyInq findDelegateOne_dept(Map<String, String> parameters);
	public void addSummonDelegateHist_dept(ExtrtCondbyInq summonUser);
	public void deleteSummonDelegateDept(ExtrtCondbyInq summonUser);
	public int addApprovalDelegatePersonal(ExtrtCondbyInq search);
	public ExtrtCondbyInq findAppDelegateOne(Map<String, String> parameters);
	public void addApprovalDelegateHist(ExtrtCondbyInq summonUser);
	public void deleteApprovalDelegatePersonal(ExtrtCondbyInq summonUser);
	public List<ExtrtCondbyInq> findAppDelegateList(Map<String, String> parameters);
	public List<ExtrtCondbyInq> findAppDelegateHistList(Map<String, String> parameters);
	public int addApprovalDelegateDept(ExtrtCondbyInq search);
	public ExtrtCondbyInq findAppDelegateOne_dept(Map<String, String> parameters);
	public void addApprovalDelegateHist_dept(ExtrtCondbyInq summonUser);
	public void deleteApprovalDelegateDept(ExtrtCondbyInq summonUser);
	public List<ExtrtCondbyInq> findAppDelegateList_dept(Map<String, String> parameters);
	public List<ExtrtCondbyInq> findAppDelegateHistList_dept(Map<String, String> parameters);
	//소명위임, 판정위임 끝
	public void updateSummonResponse(AdminUser summonInfo);
	
	//신한카드 커스텀 (소명파일 생성 관련)
	public ExtrtCondbyInq findShcard_summonInfo(AdminUser adminUser);
	public void addShcard_summmonInfo(AdminUser adminUser);
	public void decision2SummonInfo(AdminUser paramBean);
	public void addSummon2History(AdminUser paramBean);
	
	//20201222 hbjang 명지전문대 비정상위험분석 보고서 커스터마이징
	public ExtrtCondbyInq findExtrtReport(SearchSearch search);
	public Map<String, String> findSummonSmsInfo(String emp_user_id);
	public void saveSummonSms(Map<String, String> sms_info);
	public List<ExtrtCondbyInq> findExtrtExcelDownload(SearchSearch search);
	
	
}
