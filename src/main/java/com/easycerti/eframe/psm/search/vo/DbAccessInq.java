package com.easycerti.eframe.psm.search.vo;

import com.easycerti.eframe.common.util.PageInfo;
import com.easycerti.eframe.common.vo.AbstractValueObject;
/**
 * 
 * 설명 : DB접근로그조회 VO
 * @author tjlee
 * @since 2015. 4. 22.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 22.           tjlee            최초 생성
 *
 * </pre>
 */
public class DbAccessInq extends AbstractValueObject{
	private long log_seq;
	private String proc_date;
	private String proc_time;
	private String emp_user_id;
	private String emp_user_name;
	private String dept_id;
	private String dept_name;
	private String user_ip;
	private String user_id;
	private String scrn_id;
	private String scrn_name;
	private String req_type;
	private String system_seq;
	private String system_name;
	private String server_seq;
	private String req_context;
	private String req_url;
	private String req_end_time;
	private String result_type;
	private String result_content;
	
	// dbac_log_sql 용 
	private long log_sql_seq;
	private String reqsql;
	private String result_cut_flag;
	private int result_cut_count;
	private int result_total_count;
	
	private PageInfo pageInfo;
	
	private String col_name;
	private String col_name_kor;
	
	public PageInfo getPageInfo() {
		return pageInfo;
	}
	public void setPageInfo(PageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}
	public String getProc_date() {
		return proc_date;
	}
	public void setProc_date(String proc_date) {
		this.proc_date = proc_date;
	}
	public String getProc_time() {
		return proc_time;
	}
	public void setProc_time(String proc_time) {
		this.proc_time = proc_time;
	}
	public String getEmp_user_id() {
		return emp_user_id;
	}
	public void setEmp_user_id(String emp_user_id) {
		this.emp_user_id = emp_user_id;
	}
	public String getEmp_user_name() {
		return emp_user_name;
	}
	public void setEmp_user_name(String emp_user_name) {
		this.emp_user_name = emp_user_name;
	}
	public String getDept_id() {
		return dept_id;
	}
	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}
	public String getDept_name() {
		return dept_name;
	}
	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}
	public String getUser_ip() {
		return user_ip;
	}
	public void setUser_ip(String user_ip) {
		this.user_ip = user_ip;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getScrn_id() {
		return scrn_id;
	}
	public void setScrn_id(String scrn_id) {
		this.scrn_id = scrn_id;
	}
	public String getScrn_name() {
		return scrn_name;
	}
	public void setScrn_name(String scrn_name) {
		this.scrn_name = scrn_name;
	}
	public String getReq_type() {
		return req_type;
	}
	public void setReq_type(String req_type) {
		this.req_type = req_type;
	}
	public String getSystem_seq() {
		return system_seq;
	}
	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}
	public String getServer_seq() {
		return server_seq;
	}
	public void setServer_seq(String server_seq) {
		this.server_seq = server_seq;
	}
	public String getReq_context() {
		return req_context;
	}
	public void setReq_context(String req_context) {
		this.req_context = req_context;
	}
	public String getReq_url() {
		return req_url;
	}
	public void setReq_url(String req_url) {
		this.req_url = req_url;
	}
	public String getReq_end_time() {
		return req_end_time;
	}
	public void setReq_end_time(String req_end_time) {
		this.req_end_time = req_end_time;
	}
	public String getResult_type() {
		return result_type;
	}
	public void setResult_type(String result_type) {
		this.result_type = result_type;
	}
	public String getResult_content() {
		return result_content;
	}
	public void setResult_content(String result_content) {
		this.result_content = result_content;
	}
	public long getLog_seq() {
		return log_seq;
	}
	public void setLog_seq(long log_seq) {
		this.log_seq = log_seq;
	}
	public String getSystem_name() {
		return system_name;
	}
	public void setSystem_name(String system_name) {
		this.system_name = system_name;
	}
	public long getLog_sql_seq() {
		return log_sql_seq;
	}
	public void setLog_sql_seq(long log_sql_seq) {
		this.log_sql_seq = log_sql_seq;
	}
	public String getReqsql() {
		return reqsql;
	}
	public void setReqsql(String reqsql) {
		this.reqsql = reqsql;
	}
	public String getResult_cut_flag() {
		return result_cut_flag;
	}
	public void setResult_cut_flag(String result_cut_flag) {
		this.result_cut_flag = result_cut_flag;
	}
	public int getResult_cut_count() {
		return result_cut_count;
	}
	public void setResult_cut_count(int result_cut_count) {
		this.result_cut_count = result_cut_count;
	}
	public int getResult_total_count() {
		return result_total_count;
	}
	public void setResult_total_count(int result_total_count) {
		this.result_total_count = result_total_count;
	}
	public String getCol_name() {
		return col_name;
	}
	public void setCol_name(String col_name) {
		this.col_name = col_name;
	}
	public String getCol_name_kor() {
		return col_name_kor;
	}
	public void setCol_name_kor(String col_name_kor) {
		this.col_name_kor = col_name_kor;
	}
	
	
}
