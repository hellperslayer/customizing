package com.easycerti.eframe.psm.search.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;

import com.easycerti.eframe.common.dao.BootInitialDao;
import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.JDBCConnectionUtil;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.PrivacyType;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.search.dao.AllLogInqDao;
import com.easycerti.eframe.psm.search.dao.DownloadLogInqDao;
import com.easycerti.eframe.psm.search.dao.ExtrtCondbyInqDao;
import com.easycerti.eframe.psm.search.dao.FileManageDao;
import com.easycerti.eframe.psm.search.service.FileManageSvc;
import com.easycerti.eframe.psm.search.vo.AllLogInq;
import com.easycerti.eframe.psm.search.vo.AllLogInqList;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInqDetailChart;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.system_management.dao.AgentMngtDao;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

@Service
public class FileManageSvcImpl implements FileManageSvc {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired 
	private CommonDao commonDao;
	
	@Autowired
	private FileManageDao fileManageDao;
	
	@Autowired 
	private AgentMngtDao agentMngtDao;
	
	@Autowired 
	private DownloadLogInqDao downloadLogInqDao;
	
	@Autowired
	private ExtrtCondbyInqDao extrtCondbyInqDao;
	
	@Autowired
	BootInitialDao bDao;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;
	
	@Value("#{configProperties.sub_className}")
	private String sub_className;
	
	@Value("#{configProperties.sub_url}")
	private String sub_url;
	
	@Value("#{configProperties.sub_id}")
	private String sub_id;
	
	@Value("#{configProperties.sub_pw}")
	private String sub_pw;
	
	@Value("#{configProperties.sub_sql}")
	private String sub_sql;
	
	@Value("#{configProperties.pacs_className}")
	private String pacs_className;
	
	@Value("#{configProperties.pacs_url}")
	private String pacs_url;
	
	@Value("#{configProperties.pacs_id}")
	private String pacs_id;
	
	@Value("#{configProperties.pacs_pw}")
	private String pacs_pw;
	
	@Value("#{configProperties.pacs_sql}")
	private String pacs_sql;
	
	@Value("#{dbmsProperties.url}")
	private String psm_url;
	
	@Value("#{dbmsProperties.username}")
	private String psm_id;
	
	@Value("#{dbmsProperties.password}")
	private String psm_pw;
	
	@Value("#{dbmsProperties.driverClassName}")
	private String psm_className;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	//파일 대장 관리
	@Override
	public DataModelAndView setRegisteredList(SearchSearch search) {
		SimpleCode simpleCode = new SimpleCode();
		String index = "/filemanagement/registeredList.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		if (search.getStart_h() != null && search.getStart_h() != "") {
			String start_time = search.getStart_h().substring(0);
			if (start_time.length() < 2)
				start_time = "0" + start_time;
			search.setStart_time(start_time);
		}
		if (search.getEnd_h() != null && search.getEnd_h() != "") {
			String end_time = search.getEnd_h().substring(0);
			if (end_time.length() < 2)
				end_time = "0" + end_time;
			search.setEnd_time(end_time);
		}
		
		//시스템 목록
		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList_byAdmin(search);
		
		
		int registerdListCnt = fileManageDao.getRegisteredListCnt(search);
		search.setTotal_count(registerdListCnt);
		List<AllLogInq> registerdList = fileManageDao.getRegisteredList(search);
		for (AllLogInq allLogInq : registerdList) {
			allLogInq.setResultTypeMap(allLogInq.getResult_type());
		}
		AllLogInqList registeredFileList = new AllLogInqList(registerdList);
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("registerdListCnt", registerdListCnt);
		modelAndView.addObject("systemMasterList", systemMasterList);
		modelAndView.addObject("registeredFileList", registeredFileList);
		return modelAndView;
	}
	
	//파일 대장 관리 상세
	@Override
	public DataModelAndView setRegisteredDetailList(SearchSearch search) {
		int pageSize = 5;
		search.getAllLogInqDetail().setSize(pageSize);
		
		int cnt = downloadLogInqDao.findDownloadLogInqDetailResultCount(search);
		search.getAllLogInqDetail().setTotal_count(cnt);
		
		List<AllLogInq> data2 = new ArrayList<>();
		AllLogInq downloadLogInq = new AllLogInq();
		List<AllLogInq> downloadLogInqDetailList = new ArrayList<AllLogInq>();
		
		try{
			downloadLogInq = downloadLogInqDao.findDownloadLogInqDetail(search);
			
			String checkEmpNameMasking = commonDao.checkEmpNameMasking();
			if(checkEmpNameMasking.equals("Y")) {
				if(!"".equals(downloadLogInq.getEmp_user_name())) {
					String empUserName = downloadLogInq.getEmp_user_name();
					StringBuilder builder = new StringBuilder(empUserName);
					builder.setCharAt(empUserName.length() - 2, '*');
					downloadLogInq.setEmp_user_name(builder.toString());
				}
			}
			
			if(downloadLogInq.getEmp_user_id() != null) {
				//data2 = extrtCondbyInqDao.getExtrtCondbyInqDetail2_1(downloadLogInq.getEmp_user_id());
				data2 = downloadLogInqDao.findDownloadLogGroupBySystem(downloadLogInq.getEmp_user_id());
			}
			// 개인정보 마스킹 
			//search.setSystem_seq(allLogInq.getSystem_seq());
			search.setSystem_seq_temp(downloadLogInq.getSystem_seq());
			
			downloadLogInqDetailList = downloadLogInqDao.findDownloadLogInqDetailResult(search);
			
			for(AllLogInq v : downloadLogInqDetailList){
				if(v.getResult_content() != null && v.getResult_content() != ""){
					
					String pResult = v.getResult_content();
					
					if(v.getResult_type() != null && v.getPrivacy_masking() != null && v.getPrivacy_masking().equals("Y")) {
						pResult = AllLogInqSvcImpl.getResultContextByMasking(pResult, v.getResult_type());
					}
					
					v.setResult_content_masking(pResult);
				}
			}
		}catch(Exception e) {
			logger.error("[DownloadLogInqSvcImpl.findDownloadLogInqDetail] MESSAGE : " + e.getMessage());
			throw new ESException("SYS005V");
		}
		
		SimpleCode simpleCode = new SimpleCode();
		String index = "/filemanagement/registeredDetailList.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		modelAndView.addObject("downloadLogInq", downloadLogInq);
		modelAndView.addObject("downloadLogInqDetailList", downloadLogInqDetailList);
		
		modelAndView.addObject("index_id", index_id);
		if ( data2.size() > 0 ) {
			modelAndView.addObject("data2", data2);
		}
		return modelAndView;
	}
	
	@Override
	public void findDownloadLogInqDetail_download(DataModelAndView modelAndView, SearchSearch search,
			HttpServletRequest request) {
		search.setUseExcel("true");
		
		String fileName = "PSM_파일대장개인정보로그조회_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "파일대장접속기록 개인정보 리스트";
		System.out.println(fileName+sheetName);
		
		if(search.getStart_h() != null) {
			String start_time = search.getStart_h().substring(0);
			if(start_time.length() < 2)
				start_time = "0" + start_time;
			search.setStart_time(start_time);
		}
		if(search.getEnd_h() != null) {
			String end_time = search.getEnd_h().substring(0);
			if(end_time.length() < 2)
				end_time = "0" + end_time;
			search.setEnd_time(end_time);
		}
		List<AllLogInq> allLogInq = new ArrayList<AllLogInq>();
		
		if(search.getLogin_auth_id().equals("AUTH20000")) {
			allLogInq = downloadLogInqDao.findDownloadLogInqAuth20List(search);
		}else {
			//allLogInq = downloadLogInqDao.findDownloadLogInqList(search);
			allLogInq = fileManageDao.getRegisteredList(search);
		}
		
		List<PrivacyType> privacyTypeList = commonDao.getPrivacyTypesExceptRegular();
		SimpleDateFormat parseSdf = new SimpleDateFormat("yyyyMMddHHmmss");
		SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");

		// 다운로드로그 풀스캔연동여부
		HttpSession session = request.getSession();
		String use_fullscan = (String) session.getAttribute("use_fullscan");
		
		for(int i=0; i<allLogInq.size(); i++){
			if(use_fullscan != null && use_fullscan.equals("Y")) {
				AllLogInq ali = allLogInq.get(i);
				String result_type = ali.getResult_type();
				if(result_type != null) {
					String[] arrResult_type = result_type.split(",");
					Arrays.sort(arrResult_type);
					
					String result_type_string = "";
					String prev = "";
					int count = 1;
					for(int j=0; j<arrResult_type.length; j++) {
						if(prev.equals(arrResult_type[j])) {
							count++;
						}else {
							if(j != 0) {
								result_type_string += " " +count + ", ";
							}
							
							for(int k=0; k<privacyTypeList.size(); k++){
								if(privacyTypeList.get(k).getPrivacy_type().equals(arrResult_type[j])) { 
									result_type_string += privacyTypeList.get(k).getPrivacy_desc(); 
								}
							}
							count = 1;
							prev = arrResult_type[j];
						}
						
						if(j == arrResult_type.length - 1) {
							result_type_string += " " +count;
						}
					}
					
					allLogInq.get(i).setCnt(arrResult_type.length);
					allLogInq.get(i).setResult_type(result_type_string);
				}
				if(allLogInq.get(i).getDept_name().equals("") || allLogInq.get(i).getDept_name().equals(null)) 
					allLogInq.get(i).setDept_name("시스템");
				if(allLogInq.get(i).getEmp_user_name().equals("") || allLogInq.get(i).getEmp_user_name().equals(null)) 
					allLogInq.get(i).setEmp_user_name("시스템");
				
				try {
					allLogInq.get(i).setProc_date(sdf.format(parseSdf.parse(allLogInq.get(i).getProc_date() + allLogInq.get(i).getProc_time())));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			if(allLogInq.get(i).getRegister_status().equals("N") || allLogInq.get(i).getRegister_status() == null)
				allLogInq.get(i).setRegister_status("미등록");
			else
				allLogInq.get(i).setRegister_status("등록");
			
		}
		

		String scrn_name_view = (String) session.getAttribute("scrn_name");
		String[] columns;
		String[] heads;
		if(use_fullscan != null && use_fullscan.equals("Y")) {
			if(scrn_name_view.equals("1")) {
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "menu_name", "system_name", "result_type", "cnt", "scrn_name", "req_url", "file_name", "reason", "register_status"};
				heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "파일대장명", "시스템명", "정보주체", "합계", "메뉴명", "접근 경로", "파일명", "사유", "파일대장등록여부"};
			}else if(scrn_name_view.equals("2")){
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "menu_name", "system_name", "result_type", "cnt", "req_url", "file_name", "reason"};
				heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "파일대장명", "시스템명", "정보주체", "합계", "접근 경로", "파일명", "사유"};
			}else {
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "menu_name", "system_name", "result_type", "cnt", "file_name", "reason"};
				heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "파일대장명", "시스템명", "정보주체", "합계", "파일명", "사유"};
			}
		} else {
			if(scrn_name_view.equals("1")) {
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "menu_name", "system_name", "scrn_name", "req_url", "file_name", "reason", "register_status", "파일대장등록여부"};
				heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "파일대장명", "시스템명", "메뉴명", "접근 경로", "파일명", "사유"};
			}else if(scrn_name_view.equals("2")){
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "menu_name", "system_name", "req_url", "file_name", "reason"};
				heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "파일대장명", "시스템명", "접근 경로", "파일명", "사유"};
			}else {
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "menu_name", "system_name", "file_name", "reason"};
				heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "파일대장명", "시스템명", "파일명", "사유"};
			}
		}
		
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", allLogInq);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
		
	}
	
	
	
	//CSV 다운로드
	@Override
	public void findDownloadLoginqDetail_downloadCSV(DataModelAndView modelAndView, SearchSearch search,
			HttpServletRequest request) {
		search.setUseExcel("true");
		
		String fileName = "PSM_파일대장개인정보로그조회_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		
		if(search.getStart_h() != null) {
			String start_time = search.getStart_h().substring(0);
			if(start_time.length() < 2)
				start_time = "0" + start_time;
			search.setStart_time(start_time);
		}
		if(search.getEnd_h() != null) {
			String end_time = search.getEnd_h().substring(0);
			if(end_time.length() < 2)
				end_time = "0" + end_time;
			search.setEnd_time(end_time);
		}

		List<AllLogInq> allLogInq = new ArrayList<AllLogInq>();
		
		if(search.getLogin_auth_id().equals("AUTH20000")) {
			allLogInq = downloadLogInqDao.findDownloadLogInqAuth20List(search);
		}else {
			//allLogInq = downloadLogInqDao.findDownloadLogInqList(search);
			allLogInq = fileManageDao.getRegisteredList(search);
		}
		List<PrivacyType> privacyTypeList = commonDao.getPrivacyTypesExceptRegular();
		
		SimpleDateFormat parseSdf = new SimpleDateFormat("yyyyMMddHHmmss");
		SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");

		// 다운로드로그 풀스캔연동여부
		HttpSession session = request.getSession();
		String use_fullscan = (String) session.getAttribute("use_fullscan");
		
		
		for(int i=0; i<allLogInq.size(); i++){
			if(use_fullscan != null && use_fullscan.equals("Y")) {
				AllLogInq ali = allLogInq.get(i);
				String result_type = ali.getResult_type();
				if(result_type != null) {
					String[] arrResult_type = result_type.split(",");
					Arrays.sort(arrResult_type);
					String result_type_string = "";
					String prev = "";
					int count = 1;
					for(int j=0; j<arrResult_type.length; j++) {
						if(prev.equals(arrResult_type[j])) {
							count++;
						}else {
							if(j != 0){
								result_type_string += " " +count + ", ";
							}
							
							for(int k=0; k<privacyTypeList.size(); k++){
								if(privacyTypeList.get(k).getPrivacy_type().equals(arrResult_type[j])) { 
									result_type_string += privacyTypeList.get(k).getPrivacy_desc(); 
								}
							}
							count = 1;
							prev = arrResult_type[j];
						}
						
						if(j == arrResult_type.length - 1){
							result_type_string += " " +count;
						}
					}
					
					allLogInq.get(i).setCnt(arrResult_type.length);
					allLogInq.get(i).setResult_type(result_type_string);
				}
				
				if(allLogInq.get(i).getDept_name().equals("") || allLogInq.get(i).getDept_name().equals(null)) 
					allLogInq.get(i).setDept_name("-");
				if(allLogInq.get(i).getEmp_user_name().equals("") || allLogInq.get(i).getEmp_user_name().equals(null)) 
					allLogInq.get(i).setEmp_user_name("-");
				
				try {
					allLogInq.get(i).setProc_date(sdf.format(parseSdf.parse(allLogInq.get(i).getProc_date() + allLogInq.get(i).getProc_time())));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			
			if(allLogInq.get(i).getRegister_status().equals("N") || allLogInq.get(i).getRegister_status() == null)
				allLogInq.get(i).setRegister_status("미등록");
			else
				allLogInq.get(i).setRegister_status("등록");
		}
		
		
		
		List<Map<String, Object>> list = new ArrayList<>();
		
		if(use_fullscan != null && use_fullscan.equals("Y")) {
			for(AllLogInq ali : allLogInq) {
				Map<String, Object> map = new HashMap<>();
				map.put("proc_date", ali.getProc_date());
				map.put("dept_name", ali.getDept_name());
				map.put("emp_user_id", ali.getEmp_user_id());
				map.put("emp_user_name", ali.getEmp_user_name());
				map.put("menu_name", ali.getMenu_name());
				map.put("system_name", ali.getSystem_name());
				map.put("result_type", ali.getResult_type());
				map.put("cnt", ali.getCnt());
				map.put("scrn_name", ali.getScrn_name());
				map.put("req_url", ali.getReq_url());
				map.put("file_name", ali.getFile_name());
				map.put("reason", ali.getReason());
				map.put("register_status", ali.getRegister_status());
				list.add(map);
			}
		} else {
			for(AllLogInq ali : allLogInq) {
				Map<String, Object> map = new HashMap<>();
				map.put("proc_date", ali.getProc_date());
				map.put("dept_name", ali.getDept_name());
				map.put("emp_user_id", ali.getEmp_user_id());
				map.put("emp_user_name", ali.getEmp_user_name());
				map.put("menu_name", ali.getMenu_name());
				map.put("system_name", ali.getSystem_name());
				map.put("scrn_name", ali.getScrn_name());
				map.put("req_url", ali.getReq_url());
				map.put("file_name", ali.getFile_name());
				map.put("reason", ali.getReason());
				map.put("register_status", ali.getRegister_status());
				list.add(map);
			}
		}
		
		String scrn_name_view = (String) session.getAttribute("scrn_name");
		String[] columns;
		String[] heads;
		if(use_fullscan != null && use_fullscan.equals("Y")) {
			if(scrn_name_view.equals("1")) {
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "menu_name", "system_name", "result_type", "cnt", "scrn_name", "req_url", "file_name", "reason", "register_status"};
				heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "파일대장명", "시스템명", "정보주체", "합계", "메뉴명", "접근 경로", "파일명", "사유","파일대장등록여부"};
			}else if(scrn_name_view.equals("2")){
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "menu_name", "system_name", "result_type", "cnt", "req_url", "file_name", "reason"};
				heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "파일대장명", "시스템명", "정보주체", "합계", "접근 경로", "파일명", "사유"};
			}else {
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "menu_name", "system_name", "result_type", "cnt", "file_name", "reason"};
				heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "파일대장명", "시스템명", "정보주체", "합계", "파일명", "사유"};
			}
		} else {
			if(scrn_name_view.equals("1")) {
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "menu_name", "system_name", "scrn_name", "req_url", "file_name", "reason", "register_status"};
				heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "파일대장명", "시스템명", "메뉴명", "접근 경로", "파일명", "사유", "파일대장등록여부"};
			}else if(scrn_name_view.equals("2")){
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "menu_name", "system_name", "req_url", "file_name", "reason"};
				heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "파일대장명", "시스템명", "접근 경로", "파일명", "사유"};
			}else {
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "menu_name", "system_name", "file_name", "reason"};
				heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "파일대장명", "시스템명", "파일명", "사유"};
			}
		}
		
		modelAndView.addObject("excelData", list);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
		modelAndView.addObject("fileName", fileName);
	}
	
	
	//파일대장 상세 > 엑셀 다운로드
	@Override
	public void registeredDetailListExcelDown(DataModelAndView modelAndView, SearchSearch search,
			HttpServletRequest request) {
		search.setUseExcel("true");
		
		String fileName = "PSM_파일대장_상세_로그조회_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "파일대장 상세 개인정보 리스트";
		System.out.println(fileName+sheetName);
		AllLogInq downloadLogInq = downloadLogInqDao.findDownloadLogInqDetail(search);
		search.setSystem_seq_temp(downloadLogInq.getSystem_seq());
		List<AllLogInq> downloadLogInqDetailList = downloadLogInqDao.findDownloadLogInqDetailResult(search);
		
		int num = 1;
		for(AllLogInq v : downloadLogInqDetailList){
			if(v.getResult_content() != null && v.getResult_content() != ""){
				
				String pResult = v.getResult_content();
				
				if(v.getResult_type() != null && v.getPrivacy_masking() != null && v.getPrivacy_masking().equals("Y")) {
					pResult = DownloadLogInqSvcImpl.getResultContextByMasking(pResult, v.getResult_type());
				}
				
				v.setResult_content_masking(pResult);
				v.setData1(num);
				
				num++;
			}
		}
		String userProfile ="이름 : " + downloadLogInq.getEmp_user_name()+
							" 소속 : " + downloadLogInq.getDept_name()+
							" 시스템 : " + downloadLogInq.getSystem_name()+
							" 날짜 : " + downloadLogInq.getProc_date().toString();
		
		String[] columns;
		String[] heads;
		columns = new String[] {"data1", "result_type", "result_content_masking"};
		heads = new String[] {"No.", "개인정보유형", "개인정보내용"};
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", downloadLogInqDetailList);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
		modelAndView.addObject("userProfile", userProfile);
	}
	
	@Override
	public void registeredDetailListCSVDown(DataModelAndView modelAndView, SearchSearch search,
			HttpServletRequest request) {
		search.setUseExcel("true");
		
		String fileName = "PSM_파일대장_상세_로그조회_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		
		AllLogInq downloadLogInq = downloadLogInqDao.findDownloadLogInqDetail(search);
		List<AllLogInq> allLogInqDetailList = downloadLogInqDao.findDownloadLogInqDetailResult(search);
		
		int num = 1;
		for(AllLogInq v : allLogInqDetailList){
			if(v.getResult_content() != null && v.getResult_content() != ""){
				
				String pResult = v.getResult_content();
				
				if(v.getResult_type() != null && v.getPrivacy_masking() != null && v.getPrivacy_masking().equals("Y")) {
					pResult = DownloadLogInqSvcImpl.getResultContextByMasking(pResult, v.getResult_type());
				}
				
				v.setResult_content_masking(pResult);
				v.setData1(num);
				
				num++;
			}
		}
		
		List<Map<String, Object>> excelDataList = new ArrayList<>();
		
		for (AllLogInq alllog : allLogInqDetailList) {
			Map<String, Object> map = new HashMap<>();
			map.put("data1", alllog.getData1());
			map.put("result_type", alllog.getResult_type());
			map.put("result_content_masking", alllog.getResult_content_masking());
			excelDataList.add(map);
		}
		
		String[] columns;
		String[] heads;
		columns = new String[] {"data1", "result_type", "result_content_masking"};
		heads = new String[] {"No.", "개인정보유형", "개인정보내용"};
		
		modelAndView.addObject("excelData", excelDataList);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
		modelAndView.addObject("fileName", fileName);
	}
}
