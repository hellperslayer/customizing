package com.easycerti.eframe.psm.search.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.search.service.DbAccessInqSvc;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
/**
 * 
 * 설명 : DB접근로그조회 Controller
 * @author tjlee
 * @since 2015. 4. 22.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 22.           tjlee            최초 생성
 *
 * </pre>
 */
@Controller
@RequestMapping("/dbAccessInq/*")
public class DbAccessInqCtrl {

	@Autowired
	private DbAccessInqSvc dbAccessInqSvc;
	
	/**
	 * DB접근로그조회 List
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[접속기록] DB접근조회")
	@RequestMapping(value={"list.html"}, method={RequestMethod.POST})
	public DataModelAndView findDbAccessInqList(@ModelAttribute("search") SearchSearch search, @RequestParam Map<String, String> parameters, HttpServletRequest request) {

		//Date today = new Date();
		search.initDatesWithTerm(1);
		
		DataModelAndView modelAndView = dbAccessInqSvc.findDbAccessInqList(search);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("dbAccessInqList");

		return modelAndView;
	}

	/**
	 * DB접근로그조회상세List
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[접속기록] DB접근조회 상세")
	@RequestMapping(value={"detail.html"}, method={RequestMethod.POST,RequestMethod.GET})
	public DataModelAndView findDbAccessInqDetail(@ModelAttribute("search") SearchSearch search, @RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = dbAccessInqSvc.findDbAccessInqDetail(search);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("dbAccessInqDetail");

		return modelAndView;
	}
	

	/**
	 * DB접근로그조회 엑셀 다운로드
	 */
	@RequestMapping(value="download.html", method={RequestMethod.POST})
	public DataModelAndView addDbAccessInqList_download(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		dbAccessInqSvc.findDbAccessInqList_download(modelAndView, search, request);
		
		return modelAndView;
	}
}
