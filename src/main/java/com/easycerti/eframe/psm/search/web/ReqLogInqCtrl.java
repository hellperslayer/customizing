package com.easycerti.eframe.psm.search.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.search.dao.ExtrtCondbyInqDao;
import com.easycerti.eframe.psm.search.service.ReqLogInqSvc;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInqDetailChart;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.system_management.service.AuthMngtSvc;

@Controller
@RequestMapping("/reqLogInq/*")
public class ReqLogInqCtrl {
	@Autowired
	private ReqLogInqSvc reqLogInqSvc;
	
	@Autowired
	private AuthMngtSvc authMngtSvc;
	
	@Autowired
	private ExtrtCondbyInqDao extrtCondbyInqDao;

	@MngtActHist(log_action = "SELECT", log_message = "[접근관리] 접근이력조회")
	@RequestMapping(value={"list.html"}, method={RequestMethod.POST})
	public DataModelAndView findReqLogInqList(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request,
			@RequestParam(value = "sub_menu_id", defaultValue = "") String sub_menu_id,
			@RequestParam(value = "menutitle",defaultValue="")String menutitle) {
		

		Date today = new Date();
		search.initDatesIfEmpty(today, today);
	
		/*if(search.getEmp_user_id() != null) {
			String id = search.getEmp_user_id();
			String upperId = id.toUpperCase();
			search.setEmp_user_id(upperId);
		}*/
		
		// 접속유형 default 세팅
		HttpSession session = request.getSession();
		String mapping_id = (String) session.getAttribute("mapping_id");
		if(mapping_id.equals("Y")) {
			if ( search.getMapping_id() == null ) search.setMapping_id("code1");
		    //if ( search.getMapping_id().equals("codeAll") ) search.setMapping_id("");
		    if ( search.getMapping_id().equals("code2") ) search.setMapping_id(null);
		}else {
			if(search.getMapping_id() == null || search.getMapping_id().equals("code2"))
				search.setMapping_id(null);
			else
				search.setMapping_id("code1");
		}
		
		if(parameters.get("main_menu_id") != null && search.getMain_menu_id() == null && !search.getMain_menu_id().equals("")) { search.setMain_menu_id(parameters.get("main_menu_id"));}
		if(parameters.get("sub_menu_id") != null && search.getSub_menu_id() == null && !search.getSub_menu_id().equals("")) { search.setSub_menu_id(parameters.get("sub_menu_id"));}
		if(parameters.get("system_seq") != null && search.getSystem_seq() == null && !search.getSystem_seq().equals("")) { search.setSystem_seq(parameters.get("system_seq"));}
		if(parameters.get("menuId") != null && search.getMenuId() == null && !search.getMenuId().equals("")) { search.setMenuId(parameters.get("menuId"));}
		if(parameters.get("isSearch") != null && search.getIsSearch() == null && !search.getIsSearch().equals("")) { search.setIsSearch(parameters.get("isSearch"));}
		
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    search.setAuth_idsList(list);
	    
		DataModelAndView modelAndView = reqLogInqSvc.findReqLogInqList(search);
		int menuNum= 0;
		String menuCh = "";
	
		//modelAndView.addObject("paramBean", parameters);
		
		if(!",".equals(sub_menu_id) && !"".equals(sub_menu_id)){
		menuNum = Integer.parseInt(sub_menu_id.substring(4,9)) + 16 ;
		menuCh = sub_menu_id.substring(0,4) + "00" ;
		modelAndView.addObject("menuCh",menuCh);
		modelAndView.addObject("menuNum",menuNum);
		}
		modelAndView.addObject("menutitle",menutitle);
		modelAndView.addObject("search", search);
		modelAndView.addObject("sub_menu_id", sub_menu_id);
		modelAndView.setViewName("reqLogInqList");

		return modelAndView;
	}
	@MngtActHist(log_action = "SELECT", log_message = "[접근관리] 접근이력조회 상세")
	@RequestMapping(value={"detail.html"}, method={RequestMethod.POST,RequestMethod.GET})
	public DataModelAndView findReqLogInqDetail(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		if(parameters.get("reqLogInqDetail_page_num")!=null){
			search.getReqLogInqDetail().setPage_num(Integer.valueOf(parameters.get("reqLogInqDetail_page_num")));
		}
		
		if(parameters.get("main_menu_id") != null && search.getMain_menu_id() == null && !search.getMain_menu_id().equals("")) { search.setMain_menu_id(parameters.get("main_menu_id"));}
		if(parameters.get("sub_menu_id") != null && search.getSub_menu_id() == null && !search.getSub_menu_id().equals("")) { search.setSub_menu_id(parameters.get("sub_menu_id"));}
		if(parameters.get("system_seq") != null && search.getSystem_seq() == null && !search.getSystem_seq().equals("")) { search.setSystem_seq(parameters.get("system_seq"));}
		if(parameters.get("menuId") != null && search.getMenuId() == null && !search.getMenuId().equals("")) { search.setMenuId(parameters.get("menuId"));}
		if(parameters.get("isSearch") != null && search.getIsSearch() == null && !search.getIsSearch().equals("")) { search.setIsSearch(parameters.get("isSearch"));}
		
		List<ExtrtCondbyInqDetailChart> data1 = new ArrayList<>();
		List<ExtrtCondbyInqDetailChart> data2 = new ArrayList<>();
		if(parameters.get("reqLogInqDetail_user_id") != null) {
			search.setEmp_user_idD2(parameters.get("reqLogInqDetail_user_id"));
			data1 = extrtCondbyInqDao.getExtrtCondbyInqDetail1_1(parameters.get("reqLogInqDetail_user_id"));
			data2 = extrtCondbyInqDao.getExtrtCondbyInqDetail2_1(search);
		}
		
		DataModelAndView modelAndView = reqLogInqSvc.findReqLogIngDetail(search);
		
		//modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		if ( data1.size() > 0 ) {
			modelAndView.addObject("data1", data1.get(0));
			modelAndView.addObject("data2", data2);
		}
		
		HttpSession session = request.getSession();
		String master = (String)session.getAttribute("masterflag");
		modelAndView.addObject("masterflag", master);
		
		modelAndView.setViewName("reqLogInqDetail");

		return modelAndView;
	}
	@RequestMapping(value="download.html", method={RequestMethod.POST})
	public DataModelAndView addReqLogInqList_download(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		HttpSession session = request.getSession();
		String mapping_id = (String) session.getAttribute("mapping_id");
		if(mapping_id.equals("Y")) {
			if ( search.getMapping_id() == null ) search.setMapping_id("code1");
		    //if ( search.getMapping_id().equals("codeAll") ) search.setMapping_id("");
		    if ( search.getMapping_id().equals("code2") ) search.setMapping_id(null);
		}else {
			if(search.getMapping_id() == null || search.getMapping_id().equals("code2"))
				search.setMapping_id(null);
			else
				search.setMapping_id("code1");
		}
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    search.setAuth_idsList(list);
		reqLogInqSvc.findReqLogInqList_download(modelAndView, search, request);
		
		return modelAndView;
	}
	
	
	@RequestMapping(value="downloadCSV.html", method={RequestMethod.POST})
	public DataModelAndView addReqLogInqList_downloadCSV(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.CSV_DOWNLOAD_VIEW);
		
		HttpSession session = request.getSession();
		String mapping_id = (String) session.getAttribute("mapping_id");
		if(mapping_id.equals("Y")) {
			if ( search.getMapping_id() == null ) search.setMapping_id("code1");
		    //if ( search.getMapping_id().equals("codeAll") ) search.setMapping_id("");
		    if ( search.getMapping_id().equals("code2") ) search.setMapping_id(null);
		}else {
			if(search.getMapping_id() == null || search.getMapping_id().equals("code2"))
				search.setMapping_id(null);
			else
				search.setMapping_id("code1");
		}
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    search.setAuth_idsList(list);
		reqLogInqSvc.findReqLogInqList_downloadCSV(modelAndView, search, request);
		
		return modelAndView;
	}
	
	//���� 
	@RequestMapping(value="help_manager.html", method={RequestMethod.POST})
	public DataModelAndView setHelpView_manager() {
		DataModelAndView modelAndView = new DataModelAndView();
		
		modelAndView.setViewName("help_manager");
				
		return modelAndView;
	}
	@RequestMapping(value="help_master.html", method={RequestMethod.POST})
	public DataModelAndView setHelpView_master() {
		DataModelAndView modelAndView = new DataModelAndView();
		
		modelAndView.setViewName("help_master");
				
		return modelAndView;
	}
	
	
}
