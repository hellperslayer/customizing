package com.easycerti.eframe.psm.search.web;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.extract.ExtractSQLCreate_lib;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.service.CommonService;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.DateUtil;
import com.easycerti.eframe.common.util.GetStatisticsTypeData;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.dashboard.vo.Dashboard;
import com.easycerti.eframe.psm.scheduler.web.Scheduleder.MyAuthentication;
import com.easycerti.eframe.psm.search.dao.ExtrtCondbyInqDao;
import com.easycerti.eframe.psm.search.service.ExtrtCondbyInqSvc;
import com.easycerti.eframe.psm.search.vo.EmpDetailInq;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInq;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInqTimeline;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInqTimelineCond;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.setup.dao.ExtrtBaseSetupDao;
import com.easycerti.eframe.psm.setup.dao.IndvinfoTypeSetupDao;
import com.easycerti.eframe.psm.setup.vo.ExtrtBaseSetup;
import com.easycerti.eframe.psm.statistics.vo.Statistics;
import com.easycerti.eframe.psm.system_management.dao.AgentMngtDao;
import com.easycerti.eframe.psm.system_management.dao.EmpUserMngtDao;
import com.easycerti.eframe.psm.system_management.dao.SystemMngtDao;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;
import com.easycerti.eframe.psm.system_management.vo.Delegation;
import com.easycerti.eframe.psm.system_management.vo.System;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;
import com.sun.media.jfxmedia.logging.Logger;

import sun.java2d.pipe.SpanShapeRenderer.Simple;
/**
 * 
 * 설명 : 추출조건별조회 Controller
 * @author tjlee
 * @since 2015. 4. 22.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 22.           tjlee            최초 생성
 *
 * </pre>
 */
@Controller
@RequestMapping("/extrtCondbyInq/*")
public class ExtrtCondbyInqCtrl {

	@Autowired
	private ExtrtCondbyInqSvc extrtCondbyInqSvc;
	@Autowired
	private CommonService commonService;
	@Autowired
	private IndvinfoTypeSetupDao indvinfoTypeSetupDao;
	@Autowired
	private ExtrtCondbyInqDao extrtCondbyInqDao;
	@Autowired
	private ExtrtBaseSetupDao extrtBaseSetupDao;
	@Autowired
	private CommonDao commonDao;
	@Autowired
	private AgentMngtDao agentMngtDao;
	@Autowired
	private SystemMngtDao systemMngtDao;
	
	@Value("#{configProperties.use_detailchart}")
	private String use_detailchart;
	@Value("#{configProperties.abnormalList_top10_period_isWeek}")
	private String abnormalList_top10_period_isWeek;
	@Value("#{configProperties.abnormalList_top10_period_amount}")
	private int abnormalList_top10_period_amount;
	
	@Value("#{configProperties.summon_email_yn}")
	private String summon_email_yn;
	@Value("#{configProperties.smtp_host}")
	private String smtp_host;
	@Value("#{configProperties.smtp_port}")
	private String smtp_port;
	@Value("#{configProperties.smtp_id}")
	private String smtp_id;
	@Value("#{configProperties.smtp_pw}")
	private String smtp_pw;
	@Value("#{configProperties.smtp_fromName}")
	private String smtp_fromName;
	@Value("#{configProperties.context_url}")
	private String context_url;
	
	/**
	 * 추출조건별조회 List
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[빅데이터분석] 비정상위험분석 리스트")
	@RequestMapping(value={"list.html"}, method={RequestMethod.POST})
	public DataModelAndView findExtrtCondbyInqList(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {

        if(parameters.get("search_from_rp") != null && search.getSearch_from() == null && !search.getSearch_from().equals("")) {search.setSearch_from(parameters.get("search_from_rp"));}
        if(parameters.get("search_to_rp") != null && search.getSearch_to() == null && !search.getSearch_to().equals("")) {search.setSearch_to(parameters.get("search_to_rp"));}
        if(parameters.get("dept_name_rp") != null  && search.getDept_name() == null && !search.getDept_name().equals("")) {search.setDept_name(parameters.get("dept_name_rp"));}
        if(parameters.get("emp_user_id_rp") != null && search.getEmp_user_id() == null && !search.getEmp_user_id().equals("")) {search.setEmp_user_id(parameters.get("emp_user_id_rp"));}
        if(parameters.get("scen_seq_rp") != null && search.getScen_seq() == null && !search.getScen_seq().equals("") ) {search.setScen_seq(parameters.get("scen_seq_rp"));}
        
        if(parameters.get("main_menu_id") != null && search.getMain_menu_id() == null && !search.getMain_menu_id().equals("")) { search.setMain_menu_id(parameters.get("main_menu_id"));}
        if(parameters.get("sub_menu_id") != null && search.getSub_menu_id() == null && !search.getSub_menu_id().equals("")) { search.setSub_menu_id(parameters.get("sub_menu_id"));}
        if(parameters.get("system_seq") != null && search.getSystem_seq() == null && !search.getSystem_seq().equals("")) { search.setSystem_seq(parameters.get("system_seq"));}
        if(parameters.get("menuId") != null && search.getMenuId() == null && !search.getMenuId().equals("")) { search.setMenuId(parameters.get("menuId"));}
        if(parameters.get("isSearch") != null && search.getIsSearch() == null && !search.getIsSearch().equals("")) { search.setIsSearch(parameters.get("isSearch"));}
		
		if ( search.getSearch_from() != null && search.getSearch_from().length() == 2 ) {
			Calendar oCalendar = Calendar.getInstance( ); 
			String date = Integer.toString(oCalendar.get(Calendar.YEAR))+String.format("%2d", (oCalendar.get(Calendar.MONTH) + 1));
			date=date+search.getSearch_from();
			date=date.replaceAll(" ", "0");
			search.setSearch_from(date);
			search.setSearch_to(date);
		} else {
			try {
				search.initDatesWithTerm(1);

				String isMonth = parameters.get("isMonth");
				if(isMonth != null && isMonth.equals("2")) {
					String search_from ="";
					String search_to ="";
					Calendar cal = new GregorianCalendar(Locale.KOREA);
					cal.add(Calendar.DATE,-1);
					search_to = DateUtil.getDateToString(cal.getTime(), "yyyyMMdd");
					search_from = search_to.substring(0, 6)+"01";
					search.setSearch_to(search_to);
					search.setSearch_from(search_from);
				}
				search.setScen_seq(parameters.get("scen_seq"));
			} catch (Exception e) {
				
				e.printStackTrace();
			}
		}
		
		/*if(search.getEmp_user_id() != null) {
			String id = search.getEmp_user_id();
			String upperId = id.toUpperCase();
			search.setEmp_user_id(upperId);
		}*/
		HttpSession session = request.getSession();
		String use_systemSeq = (String) session.getAttribute("use_systemSeq");
		//부적정 사용자 리스트 출력 사용 여부
		String use_adverse = (String) session.getAttribute("use_adverse");
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list == null) {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    }
	    list.add("00");
	    search.setAuth_idsList(list);
	    
		DataModelAndView modelAndView = extrtCondbyInqSvc.findExtrtCondbyInqList(search);
		AdminUser adminUser = SystemHelper.getAdminUserInfo(request);
		
		//modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("adminUser", adminUser);
		modelAndView.addObject("search", search);
		modelAndView.addObject("use_systemSeq", use_systemSeq);
		modelAndView.addObject("use_adverse", use_adverse);
		modelAndView.setViewName("extrtCondbyInqList");
		modelAndView.addObject("adminUsers", commonService.getAdminUsers());
		return modelAndView;
	}
	
	@RequestMapping(value={"extrtCondbyInqDaliyList.html"}, method={RequestMethod.POST})
	public DataModelAndView extrtCondbyInqDaily(@ModelAttribute("search") SearchSearch search, HttpServletRequest request) {
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list == null) {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    }
	    list.add("00"); // 공통 시스템코드 추가
	    search.setAuth_idsList(list);
		DataModelAndView modelAndView = extrtCondbyInqSvc.extrtCondbyInqDaily(search);
		modelAndView.setViewName("extrtCondbyInqDaliyList");
		return modelAndView;
	}
	@RequestMapping(value={"extrtCondbyInqDeptList.html"}, method={RequestMethod.POST})
	public DataModelAndView extrtCondbyInqDept(@ModelAttribute("search") SearchSearch search, HttpServletRequest request) {
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list == null) {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    }
	    list.add("00"); // 공통 시스템코드 추가
	    search.setAuth_idsList(list);
		DataModelAndView modelAndView = extrtCondbyInqSvc.extrtCondbyInqDept(search);
		modelAndView.setViewName("extrtCondbyInqDeptList");
		return modelAndView;
	}
	@RequestMapping(value={"extrtCondbyInqUserList.html"}, method={RequestMethod.POST})
	public DataModelAndView extrtCondbyInqUser(@ModelAttribute("search") SearchSearch search, HttpServletRequest request) {
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list == null) {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    }
	    list.add("00"); // 공통 시스템코드 추가
	    search.setAuth_idsList(list);
		DataModelAndView modelAndView = extrtCondbyInqSvc.extrtCondbyInqUser(search);
		modelAndView.setViewName("extrtCondbyInqUserList");
		return modelAndView;
	}

	@RequestMapping(value={"extrtCondbyInqSystemList.html"}, method={RequestMethod.POST})
	public DataModelAndView extrtCondbyInqSystem(@ModelAttribute("search") SearchSearch search, HttpServletRequest request) {
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list == null) {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    }
	    list.add("00"); // 공통 시스템코드 추가
	    search.setAuth_idsList(list);
		DataModelAndView modelAndView = extrtCondbyInqSvc.extrtCondbyInqSystem(search);
		modelAndView.setViewName("extrtCondbyInqSystemList");
		return modelAndView;
	}
	@RequestMapping(value="extrtCondbyInqDaliyDownload.html", method={RequestMethod.POST})
	public DataModelAndView extrtCondbyInqDaliyDownload(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list == null) {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    }
	    list.add("00"); // 공통 시스템코드 추가
	    search.setAuth_idsList(list);
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		extrtCondbyInqSvc.extrtCondbyInqDaliyDownload(modelAndView, search);
		return modelAndView;
	}
	@RequestMapping(value="extrtCondbyInqDeptDownload.html", method={RequestMethod.POST})
	public DataModelAndView extrtCondbyInqDeptDownload(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list == null) {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    }
	    list.add("00"); // 공통 시스템코드 추가
	    search.setAuth_idsList(list);
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		extrtCondbyInqSvc.extrtCondbyInqDeptDownload(modelAndView, search);
		return modelAndView;
	}
	@RequestMapping(value="extrtCondbyInqUserDownload.html", method={RequestMethod.POST})
	public DataModelAndView extrtCondbyInqUserDownload(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list == null) {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    }
	    list.add("00"); // 공통 시스템코드 추가
	    search.setAuth_idsList(list);
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		extrtCondbyInqSvc.extrtCondbyInqUserDownload(modelAndView, search);
		return modelAndView;
	}
	@RequestMapping(value="extrtCondbyInqSystemDownload.html", method={RequestMethod.POST})
	public DataModelAndView extrtCondbyInqSystemDownload(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list == null) {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    }
	    list.add("00"); // 공통 시스템코드 추가
	    search.setAuth_idsList(list);
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		extrtCondbyInqSvc.extrtCondbyInqSystemDownload(modelAndView, search);
		return modelAndView;
	}
	/**
	 * 소명 List
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[소명관리] 소명요청")
	@RequestMapping(value={"summonList.html"}, method={RequestMethod.POST})
	public DataModelAndView summonList(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {

        if(parameters.get("main_menu_id") != null && search.getMain_menu_id() == null && !search.getMain_menu_id().equals("")) { search.setMain_menu_id(parameters.get("main_menu_id"));}
        if(parameters.get("sub_menu_id") != null && search.getSub_menu_id() == null && !search.getSub_menu_id().equals("")) { search.setSub_menu_id(parameters.get("sub_menu_id"));}
        if(parameters.get("system_seq") != null && search.getSystem_seq() == null && !search.getSystem_seq().equals("")) { search.setSystem_seq(parameters.get("system_seq"));}
        if(parameters.get("menuId") != null && search.getMenuId() == null && !search.getMenuId().equals("")) { search.setMenuId(parameters.get("menuId"));}
        if(parameters.get("isSearch") != null && search.getIsSearch() == null && !search.getIsSearch().equals("")) { search.setIsSearch(parameters.get("isSearch"));}
		
		if ( search.getSearch_from() != null && search.getSearch_from().length() == 2 ) {
			Calendar oCalendar = Calendar.getInstance( ); 
			String date = Integer.toString(oCalendar.get(Calendar.YEAR))+String.format("%2d", (oCalendar.get(Calendar.MONTH) + 1));
			date=date+search.getSearch_from();
			date=date.replaceAll(" ", "0");
			search.setSearch_from(date);
			search.setSearch_to(date);
		} else {
			search.initDatesWithTerm(1);

			String isMonth = parameters.get("isMonth");
			if(isMonth != null && isMonth.equals("2")) {
				String search_from ="";
				String search_to ="";
				Calendar cal = new GregorianCalendar(Locale.KOREA);
				cal.add(Calendar.DATE,-1);
				search_to = DateUtil.getDateToString(cal.getTime(), "yyyyMMdd");
				search_from = search_to.substring(0, 6)+"01";
				search.setSearch_to(search_to);
				search.setSearch_from(search_from);
			}
		}
		
		/*if(search.getEmp_user_id() != null) {
			String id = search.getEmp_user_id();
			String upperId = id.toUpperCase();
			search.setEmp_user_id(upperId);
		}*/
		
//		DataModelAndView modelAndView = extrtCondbyInqSvc.findExtrtCondbyInqList(search);
//		AdminUser adminUser = SystemHelper.getAdminUserInfo(request);
		String ui_type = commonDao.getUiType();
		search.setUi_type(ui_type);
		
		DataModelAndView modelAndView = extrtCondbyInqSvc.findSummonList(search);
		AdminUser adminUser = SystemHelper.getAdminUserInfo(request);
		
		//modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("adminUser", adminUser);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("summonList");
		modelAndView.addObject("adminUsers", commonService.getAdminUsers());
		return modelAndView;
	}
	
	/**
	 * 소명관리 List
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[소명관리] 소명관리")
	@RequestMapping(value={"summonManageList.html"}, method={RequestMethod.POST})
	public DataModelAndView summonManageList(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
        if(parameters.get("main_menu_id") != null && search.getMain_menu_id() == null && !search.getMain_menu_id().equals("")) { search.setMain_menu_id(parameters.get("main_menu_id"));}
        if(parameters.get("sub_menu_id") != null && search.getSub_menu_id() == null && !search.getSub_menu_id().equals("")) { search.setSub_menu_id(parameters.get("sub_menu_id"));}
        if(parameters.get("system_seq") != null && search.getSystem_seq() == null && !search.getSystem_seq().equals("")) { search.setSystem_seq(parameters.get("system_seq"));}
        if(parameters.get("menuId") != null && search.getMenuId() == null && !search.getMenuId().equals("")) { search.setMenuId(parameters.get("menuId"));}
        if(parameters.get("isSearch") != null && search.getIsSearch() == null && !search.getIsSearch().equals("")) { search.setIsSearch(parameters.get("isSearch"));}
		
		if ( search.getSearch_from() != null && search.getSearch_from().length() == 2 ) {
			Calendar oCalendar = Calendar.getInstance();
			String date = Integer.toString(oCalendar.get(Calendar.YEAR))+String.format("%2d", (oCalendar.get(Calendar.MONTH) + 1));
			date=date+search.getSearch_from();
			date=date.replaceAll(" ", "0");
			search.setSearch_from(date);
			search.setSearch_to(date);
		} else {
			Date today = new Date();
			search.initDatesIfEmpty_2week(today, today);
			//search.initDatesWithDefaultIfEmpty(0);
		}
		
		/*if(search.getEmp_user_id() != null) {
			String id = search.getEmp_user_id();
			String upperId = id.toUpperCase();
			search.setEmp_user_id(upperId);
		}*/
		HttpSession session = request.getSession();
		search.setAdmin_user_id(((AdminUser)request.getSession().getAttribute("userSession")).getAdmin_user_id());
		search.setUser_auth(((AdminUser)request.getSession().getAttribute("userSession")).getAuth_id());
		DataModelAndView modelAndView = extrtCondbyInqSvc.findSummonManageList(search);
		AdminUser adminUser = SystemHelper.getAdminUserInfo(request);
		
		String ui_type = commonDao.getUiType();
		search.setUi_type(ui_type);
		
		//modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("adminUser", adminUser);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("summonManageList");
		modelAndView.addObject("adminUsers", commonService.getAdminUsers());
		return modelAndView;
	}

	/**
	 * 소명조회상세List
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[소명관리] 소명요청 상세")
	@RequestMapping(value={"summonDetail.html"}, method={RequestMethod.POST})
	public DataModelAndView findSummonDetail(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
        if(parameters.get("main_menu_id") != null && search.getMain_menu_id() == null && !search.getMain_menu_id().equals("")) { search.setMain_menu_id(parameters.get("main_menu_id"));}
        if(parameters.get("sub_menu_id") != null && search.getSub_menu_id() == null && !search.getSub_menu_id().equals("")) { search.setSub_menu_id(parameters.get("sub_menu_id"));}
        if(parameters.get("system_seq") != null && search.getSystem_seq() == null && !search.getSystem_seq().equals("")) { search.setSystem_seq(parameters.get("system_seq"));}
        if(parameters.get("menuId") != null && search.getMenuId() == null && !search.getMenuId().equals("")) { search.setMenuId(parameters.get("menuId"));}
        if(parameters.get("isSearch") != null && search.getIsSearch() == null && !search.getIsSearch().equals("")) { search.setIsSearch(parameters.get("isSearch"));}
		
        //위험도별조회 상세 리스트 페이징 page_num 세팅
		/*if(parameters.get("empDetailInqDetail_page_num")!=null){
			search.getEmpDetailInqDetail().setPage_num(Integer.valueOf(parameters.get("empDetailInqDetail_page_num")));
		}

		//추출조건별조회 상세 리스트 페이징 page_num 세팅
		if(parameters.get("extrtCondbyInqDetail_page_num")!=null){
			search.getExtrtCondbyInqDetail().setPage_num(Integer.valueOf(parameters.get("extrtCondbyInqDetail_page_num")));
		}*/
		
		HttpSession session = request.getSession();
		String use_fullscan = (String) session.getAttribute("use_fullscan");
		
		String ui_type = commonDao.getUiType();
		search.setUi_type(ui_type);
		
		DataModelAndView modelAndView = extrtCondbyInqSvc.findSummonDetail(search);
		
		//modelAndView.addObject("paramBean", parameters);
		//modelAndView.addObject("search", search);
		modelAndView.setViewName("summonDetail");

		AdminUser adminUser = SystemHelper.getAdminUserInfo(request);
		modelAndView.addObject("adminUsers", commonService.getAdminUsers());
		modelAndView.addObject("use_fullscan", use_fullscan);

		return modelAndView;
	}
	
	/**
	 * 소명 삭제
	 * * 
	 */
	@RequestMapping(value="summonRemove.html",method={RequestMethod.POST})
	public DataModelAndView summonRemove(@RequestParam Map<String, String> parameters,HttpServletRequest request) throws Exception{
		
		DataModelAndView modelAndView = extrtCondbyInqSvc.removeSummonOne(parameters,request);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	/**
	 * 소명관리상세List
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[소명관리] 소명관리 상세")
	@RequestMapping(value={"summonManageDetail.html"}, method={RequestMethod.POST})
	public DataModelAndView findSummonManageDetail(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
        if(parameters.get("main_menu_id") != null && search.getMain_menu_id() == null && !search.getMain_menu_id().equals("")) { search.setMain_menu_id(parameters.get("main_menu_id"));}
        if(parameters.get("sub_menu_id") != null && search.getSub_menu_id() == null && !search.getSub_menu_id().equals("")) { search.setSub_menu_id(parameters.get("sub_menu_id"));}
        if(parameters.get("system_seq") != null && search.getSystem_seq() == null && !search.getSystem_seq().equals("")) { search.setSystem_seq(parameters.get("system_seq"));}
        if(parameters.get("menuId") != null && search.getMenuId() == null && !search.getMenuId().equals("")) { search.setMenuId(parameters.get("menuId"));}
        if(parameters.get("isSearch") != null && search.getIsSearch() == null && !search.getIsSearch().equals("")) { search.setIsSearch(parameters.get("isSearch"));}
		
		//추출조건별조회 상세 리스트 페이징 page_num 세팅
		if(parameters.get("extrtCondbyInqDetail_page_num")!=null){
			search.getExtrtCondbyInqDetail().setPage_num(Integer.valueOf(parameters.get("extrtCondbyInqDetail_page_num")));
		}
		search.setUser_auth(((AdminUser)request.getSession().getAttribute("userSession")).getAuth_id());
		
		String ui_type = commonDao.getUiType();
		search.setUi_type(ui_type);
		
		DataModelAndView modelAndView = extrtCondbyInqSvc.findSummonManageDetail(search,request);
		
		//modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("summonManageDetail");

		AdminUser adminUser = SystemHelper.getAdminUserInfo(request);
		modelAndView.addObject("adminUsers", commonService.getAdminUsers());

		return modelAndView;
	}

	/**
	 * 소명 요청
	 * * 관리자 추가 참조
	 */
	@RequestMapping(value="summonadd.html",method={RequestMethod.POST})
	public DataModelAndView summonadd(@RequestParam Map<String, String> parameters,HttpServletRequest request) throws Exception{
		/*
		String[] args = {"admin_user_id", "password", "admin_user_name"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		parameters.put("insert_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		*/
		
		DataModelAndView modelAndView = extrtCondbyInqSvc.addSummonOne(parameters,request);
		// 이메일 보내기
		/*
		if("Y".equals(summon_email_yn)){
			sendEmail(parameters);
		}
		*/
		
	//위로이동	DataModelAndView modelAndView = extrtCondbyInqSvc.addSummonOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
	//	System.out.println("CommonResource.JSON_VIEW           ="+CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	@RequestMapping(value="summonre.html",method={RequestMethod.POST})
	public DataModelAndView summonre(@RequestParam Map<String, String> parameters,HttpServletRequest request) throws Exception{
		
		DataModelAndView modelAndView = extrtCondbyInqSvc.reSummonOne(parameters,request);
		//DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	@RequestMapping(value="addSummonResult.html",method={RequestMethod.POST})
	public DataModelAndView addSummonResult(@RequestParam Map<String, String> parameters,HttpServletRequest request) throws Exception{
		
		DataModelAndView modelAndView = extrtCondbyInqSvc.addSummonResult(parameters);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	//소명 최종판정 등록 
	@RequestMapping(value="SummonResultConfirm.html",method={RequestMethod.POST})
	public DataModelAndView ResultConfirmSummon(@RequestParam Map<String, String> parameters,HttpServletRequest request) throws Exception{
		
		DataModelAndView modelAndView = extrtCondbyInqSvc.SummonResultConfirm(parameters);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	@RequestMapping(value="addReSummonResult.html",method={RequestMethod.POST})
	public DataModelAndView addReSummonResult(@RequestParam Map<String, String> parameters,HttpServletRequest request) throws Exception{
		parameters.put("admin_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		parameters.put("result2_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		DataModelAndView modelAndView = extrtCondbyInqSvc.addReSummonResult(parameters);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	@RequestMapping(value="removeSummonResult.html",method={RequestMethod.POST})
	public DataModelAndView removeSummonResult(@RequestParam Map<String, String> parameters,HttpServletRequest request) throws Exception{
		DataModelAndView modelAndView = extrtCondbyInqSvc.removeSummonResult(parameters);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	@RequestMapping(value="updateSummonResult.html",method={RequestMethod.POST})
	public DataModelAndView updateSummonResult(@RequestParam Map<String, String> parameters,HttpServletRequest request) throws Exception{
		DataModelAndView modelAndView = extrtCondbyInqSvc.updateSummonResult(parameters);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	// 메일 보내기 (TMON 전용)
	public void sendEmail(@RequestParam Map<String, String> parameters) {
	//	Map<String, Object> paramMapList = parseStr2Map(reqStr);
		
	//	MngRefTblControler mrtc = new MngRefTblControler();

		// 구글 SMTP 사용시
//		String host = "smtp.gmail.com";
//		String port = "465";
		
		// HOST 정보
//		String host = "182.162.7.11";
		//email 바뀐 ip
		String host = smtp_host;
		// PORT 정보
		String port = smtp_port;
		// 보내는 사람
		String fromName = smtp_fromName;
		// 보내는 사람 메일 주소
		String fromMail = smtp_id;
		// 받는 사람 이름
		String user_name = (String) parameters.get("emp_user_name");
		// 받는 사람 ID
		String user_id = (String) parameters.get("emp_user_id");
		// 받는 사람 메일 정보
		String toMail = (String) parameters.get("email_address");
		//String toMail = "hhroh@easycerti.com";
		
		//시스템 명
		String system_name = parameters.get("system_name");
		
		// 메일 제목
		String subject = "["+system_name+"] 개인정보처리시스템 오남용 사용이력에 대한 사유 요청";
		
		// 추출조건명
		String rule_nm = (String) parameters.get("rule_nm");
		// 요청사항
		//String requestMsg = (String) parameters.get("description1");
		// 소명 응답 페이지 URL
		
		// 이관 전 		String replyUrl = "http://10.10.14.33/context";
		
		/*요청사유
		request
		받는사람이메일
		to_emailAddrInput toEmail
		내용
		$("#to_emailContentInput").val().replace(/\n/g, "<br>");
		to_emailContentInput emailContent
		*/
		
		//이관후
		String replyUrl = context_url;
		//String replyUrl = "http://10.11.14.45/context";
		//String replyUrl = "http://10.11.14.46/context";
		
        String content = "<br>" + "- 개인정보보호법 제29조 (안전조치의무)<br>" 
        		+ "- 개인정보의 안정성 확보조치기준 제8조 (접속기록의 보관 및 점검)<br>"
                + "- 정보통신망법 제28조 (개인정보의 보호조치)<br>" 
        		+ "- 기술적·관리적 보호조치 기준 제5조 (접속기록의 위·변조방지)<br><br>"
                + "관련근거에 의거하여, 개인정보 유출 및 오남용 방지를 위한 [개인정보처리시스템 모니터링]을 수행하고 있습니다.<br>" 
        		+ "<b>수신 대상자["+user_name+"("+user_id +")]님은 해당 시스템 ["+ system_name + "]의 개인정보 열람에 대한 비정상 의심 행위("+rule_nm+")가 탐지되어 정당한 이유를 확인하고자 하오니 아래의 URL을 통하여 답변 부탁드립니다.</b><br>"
        		+"<br>"
        		+ "접속기록 소명페이지 바로가기(URL클릭) → <a href='" + replyUrl+ "'>" + replyUrl + "</a><br><br>"
                + "※ 해당 메일은 발신전용메일이며, 문의사항이 있으시면 담당자에게 연락 주시기 바랍니다.<br>"
                + "※ 문의 : "+smtp_fromName;
                //+ "<span style=\"color:red\">※ 메일 수신일 기준 7일 이내 소명 등록하여 주세요.</span><br>";
        // }
		try {
			Properties props = new Properties();
			props.put("mail.smtp.host", host);
			props.put("mail.smtp.port", port);
			props.put("mail.smtp.auth", "true");
			Authenticator auth = new MyAuthentication(smtp_id,smtp_pw);
			Session mailSession = null;
			
			// 구글 SMTP 사용시
//			props.put("mail.smtp.starttls.enable", "true");
//			props.put("mail.transport.protocol", "smtp");
//			props.put("mail.smtp.auth", "true");
//			props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

			try {
				mailSession = Session.getInstance(props, auth );
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			// 구글 SMTP 사용시
//			Session mailSession = Session.getInstance(props, new Authenticator() {
//				protected PasswordAuthentication getPasswordAuthentication() {
//					return new PasswordAuthentication("", "");
//				}
//			});
			
			if(toMail != null && !toMail.equals("")) {
				InternetAddress[] address1 = { new InternetAddress(toMail) };
				Message msg = new MimeMessage(mailSession);
				msg.setFrom(new InternetAddress(fromMail, MimeUtility.encodeText(fromName, "UTF-8", "B")));
				msg.setRecipients(Message.RecipientType.TO, address1);
				msg.setSubject(subject);
				msg.setSentDate(new java.util.Date());
				msg.setContent(content, "text/html;charset=euc-kr");
				
				Transport.send(msg);
			}
			
		} catch(MessagingException e) {
			e.printStackTrace();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@MngtActHist(log_action = "SELECT", log_message = "[빅데이터분석] 비정상위험분석")
	@RequestMapping(value={"abnormalList.html"}, method={RequestMethod.POST})
	public DataModelAndView findAbnormalList(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception {
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list == null) {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    }
	    list.add("00");
    	search.setAuth_idsList(list);

	    String searchType = search.getSearchType();
	    String isSearch = search.getIsSearch();
		if(searchType == null || searchType.equals("today")) {
			search.setSearchType("today");
			search.setSearch_from("");
			search.setSearch_to("");
			search.initDatesWithTerm(1);
		} else if(searchType.equals("day") && isSearch.equals("N")) {
			search.setSearch_from("");
			search.setSearch_to("");
			search.initDatesWithTerm(1);
		} else if (searchType.equals("month") && isSearch.equals("N")) {
			Calendar cal = Calendar.getInstance();
			String year = String.valueOf(cal.get(Calendar.YEAR));
			String month = String.valueOf(cal.get(Calendar.MONTH)+1);
			search.setYear(year);
			search.setMonth(month);
			search.initDateByMonth(year, month);
		} else if (searchType.equals("month") && isSearch.equals("Y")) {
			search.initDateByMonth(search.getYear(), search.getMonth());
		}
		
		DataModelAndView modelAndView = extrtCondbyInqSvc.findAbnormalList(search);
		AdminUser adminUser = SystemHelper.getAdminUserInfo(request);
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("adminUser", adminUser);
		modelAndView.setViewName("abnormalList");
		modelAndView.addObject("adminUsers", commonService.getAdminUsers());
		return modelAndView;
	}

	/**
	 * 추출조건별조회상세List
	 */
	@RequestMapping(value={"detail.html"}, method={RequestMethod.POST})
	public DataModelAndView findExtrtCondbyInqDetail(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
        if(parameters.get("main_menu_id") != null && search.getMain_menu_id() == null && !search.getMain_menu_id().equals("")) { search.setMain_menu_id(parameters.get("main_menu_id"));}
        if(parameters.get("sub_menu_id") != null && search.getSub_menu_id() == null && !search.getSub_menu_id().equals("")) { search.setSub_menu_id(parameters.get("sub_menu_id"));}
        if(parameters.get("system_seq") != null && search.getSystem_seq() == null && !search.getSystem_seq().equals("")) { search.setSystem_seq(parameters.get("system_seq"));}
        if(parameters.get("menuId") != null && search.getMenuId() == null && !search.getMenuId().equals("")) { search.setMenuId(parameters.get("menuId"));}
        if(parameters.get("isSearch") != null && search.getIsSearch() == null && !search.getIsSearch().equals("")) { search.setIsSearch(parameters.get("isSearch"));}
		
		//위험도별조회 상세 리스트 페이징 page_num 세팅
		if(parameters.get("empDetailInqDetail_page_num")!=null){
			search.getEmpDetailInqDetail().setPage_num(Integer.valueOf(parameters.get("empDetailInqDetail_page_num")));
		}

		//추출조건별조회 상세 리스트 페이징 page_num 세팅
		if(parameters.get("extrtCondbyInqDetail_page_num")!=null){
			search.getExtrtCondbyInqDetail().setPage_num(Integer.valueOf(parameters.get("extrtCondbyInqDetail_page_num")));
		}
		
		HttpSession session = request.getSession();
		String use_fullscan = (String) session.getAttribute("use_fullscan");
		
		DataModelAndView modelAndView = extrtCondbyInqSvc.findExtrtCondbyInqDetail(search);
		
		String ui_type = commonDao.getUiType();
		search.setUi_type(ui_type+"_TYPE");
		
		//modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("extrtCondbyInqDetail");
		modelAndView.addObject("use_fullscan", use_fullscan);

		return modelAndView;
	}
	/**
	 * 20201216 신한카드 비인가 추출조건별조회상세List
	 */
	@RequestMapping(value={"detail_shcd.html"}, method={RequestMethod.POST,RequestMethod.GET})
	public DataModelAndView findExtrtCondbyInqDetail_shcd(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
        if(parameters.get("main_menu_id") != null && search.getMain_menu_id() == null && !search.getMain_menu_id().equals("")) { search.setMain_menu_id(parameters.get("main_menu_id"));}
        if(parameters.get("sub_menu_id") != null && search.getSub_menu_id() == null && !search.getSub_menu_id().equals("")) { search.setSub_menu_id(parameters.get("sub_menu_id"));}
        if(parameters.get("system_seq") != null && search.getSystem_seq() == null && !search.getSystem_seq().equals("")) { search.setSystem_seq(parameters.get("system_seq"));}
        if(parameters.get("menuId") != null && search.getMenuId() == null && !search.getMenuId().equals("")) { search.setMenuId(parameters.get("menuId"));}
        if(parameters.get("isSearch") != null && search.getIsSearch() == null && !search.getIsSearch().equals("")) { search.setIsSearch(parameters.get("isSearch"));}
		
		//위험도별조회 상세 리스트 페이징 page_num 세팅
		if(parameters.get("empDetailInqDetail_page_num")!=null){
			search.getEmpDetailInqDetail().setPage_num(Integer.valueOf(parameters.get("empDetailInqDetail_page_num")));
		}
		
		//추출조건별조회 상세 리스트 페이징 page_num 세팅
		if(parameters.get("extrtCondbyInqDetail_page_num")!=null){
			search.getExtrtCondbyInqDetail().setPage_num(Integer.valueOf(parameters.get("extrtCondbyInqDetail_page_num")));
		}
		
		String emp_detail_seq = extrtCondbyInqDao.decryptSummonKey(search.getSummonKey());
		search.setEmp_detail_seq(emp_detail_seq);
		HttpSession session = request.getSession();
		String use_fullscan = (String) session.getAttribute("use_fullscan");
		
		DataModelAndView modelAndView = extrtCondbyInqSvc.findExtrtCondbyInqDetail(search);
		
		String ui_type = commonDao.getUiType();
		search.setUi_type(ui_type+"_TYPE");
		
		//modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("extrtCondbyInqDetail_shcd");
		modelAndView.addObject("use_fullscan", use_fullscan);
		
		return modelAndView;
	}
	/**
	 * 추출조건별조회 엑셀 다운로드
	 */
	@RequestMapping(value="download.html", method={RequestMethod.POST})
	public DataModelAndView addExtrtCondbyInqList_download(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		extrtCondbyInqSvc.findExtrtCondbyInqList_download(modelAndView, search, request);
		
		return modelAndView;
	}
	
	/** 20190612 소명요청 리스트 엑셀 다운로드 **/
	@RequestMapping(value="summonListExcel.html", method={RequestMethod.POST})
	public DataModelAndView summonListExcel(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		if ( search.getSearch_from() != null && search.getSearch_from().length() == 2 ) {
			Calendar oCalendar = Calendar.getInstance( ); 
			String date = Integer.toString(oCalendar.get(Calendar.YEAR))+String.format("%2d", (oCalendar.get(Calendar.MONTH) + 1));
			date=date+search.getSearch_from();
			date=date.replaceAll(" ", "0");
			search.setSearch_from(date);
			search.setSearch_to(date);
		} else {
			Date today = new Date();
			search.initDatesWithDefaultIfEmpty(0);
		}
		
		String ui_type = commonDao.getUiType();
		search.setUi_type(ui_type);
		//extrtCondbyInqSvc.findExtrtCondbyInqList_download(modelAndView, search, request);
		extrtCondbyInqSvc.summonListExcel(modelAndView, search, request);
		
		return modelAndView;
	}
	
	@RequestMapping(value="summonDetailExcel.html", method={RequestMethod.POST})
	public DataModelAndView summonDetailExcel(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		if ( search.getSearch_from() != null && search.getSearch_from().length() == 2 ) {
			Calendar oCalendar = Calendar.getInstance( ); 
			String date = Integer.toString(oCalendar.get(Calendar.YEAR))+String.format("%2d", (oCalendar.get(Calendar.MONTH) + 1));
			date=date+search.getSearch_from();
			date=date.replaceAll(" ", "0");
			search.setSearch_from(date);
			search.setSearch_to(date);
		} else {
			Date today = new Date();
			search.initDatesWithDefaultIfEmpty(0);
		}
		
		String ui_type = commonDao.getUiType();
		search.setUi_type(ui_type);
		
		//extrtCondbyInqSvc.findExtrtCondbyInqList_download(modelAndView, search, request);
		extrtCondbyInqSvc.summonDetailExcel(modelAndView, search, request);
		
		return modelAndView;
	}
	
	/** 20190612 소명관리 리스트 엑셀 다운로드 **/
	@RequestMapping(value="summonManageListExcel.html", method={RequestMethod.POST})
	public DataModelAndView addsummonManageListExcel(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		if ( search.getSearch_from() != null && search.getSearch_from().length() == 2 ) {
			Calendar oCalendar = Calendar.getInstance( ); 
			String date = Integer.toString(oCalendar.get(Calendar.YEAR))+String.format("%2d", (oCalendar.get(Calendar.MONTH) + 1));
			date=date+search.getSearch_from();
			date=date.replaceAll(" ", "0");
			search.setSearch_from(date);
			search.setSearch_to(date);
		} else {
			Date today = new Date();
			search.initDatesIfEmpty_2week(today, today);
			//search.initDatesWithDefaultIfEmpty(0);
		}
		
		search.setAdmin_user_id(((AdminUser)request.getSession().getAttribute("userSession")).getAdmin_user_id());
		search.setUser_auth(((AdminUser)request.getSession().getAttribute("userSession")).getAuth_id());
		String ui_type = commonDao.getUiType();
		search.setUi_type(ui_type);
		
		//extrtCondbyInqSvc.findExtrtCondbyInqList_download(modelAndView, search, request);
		//extrtCondbyInqSvc.summonListExcel(modelAndView, search, request);
		extrtCondbyInqSvc.summonManageListExcel(modelAndView, search, request);
		
		return modelAndView;
	}
	
	/** 20190613 부적정판정자 관리 리스트 엑셀 다운로드 **/
	@RequestMapping(value=" abuseInfoDownload.html", method={RequestMethod.POST})
	public DataModelAndView addabuseInfoDownloadExcel(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		if(parameters.get("search_from_rp") != null) {search.setSearch_from(parameters.get("search_from_rp"));}
		if(parameters.get("search_to_rp") != null) {search.setSearch_to(parameters.get("search_to_rp"));}
		if(parameters.get("dept_name_rp") != null) {search.setDept_name(parameters.get("dept_name_rp"));}
		if(parameters.get("emp_user_id_rp") != null) {search.setEmp_user_id(parameters.get("emp_user_id_rp"));}
		if(parameters.get("scen_seq_rp") != null) {search.setScen_seq(parameters.get("scen_seq_rp"));}
		
		if ( search.getSearch_from() != null && search.getSearch_from().length() == 2 ) {
			Calendar oCalendar = Calendar.getInstance( ); 
			String date = Integer.toString(oCalendar.get(Calendar.YEAR))+String.format("%2d", (oCalendar.get(Calendar.MONTH) + 1));
			date=date+search.getSearch_from();
			date=date.replaceAll(" ", "0");
			search.setSearch_from(date);
			search.setSearch_to(date);
		} else {
			Date today = new Date();
			//search.initDatesIfEmpty_2week(today, today);
			//search.initDatesWithDefaultIfEmpty(0);
			// 비정상위험분석에서 상단에 있는 카운팅 링크
			try {
				if(parameters.get("isWeekend").equals("YES")) {
					search.initDatesIfEmpty_week(today, today);
				}else {
					search.initDatesWithDefaultIfEmpty(0);
				}
			} catch (Exception e) {
				search.initDatesWithDefaultIfEmpty(0);
				//e.printStackTrace();
			}
		}
		
		HttpSession session = request.getSession();
		String use_systemSeq = (String) session.getAttribute("use_systemSeq");
		if (use_systemSeq.equals("Y")) {
			List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
		    if(list != null) {
		    	search.setAuth_idsList(list);
		    }else {
		    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
		    	search.setAuth_idsList(list);
		    }
		}
	    
		extrtCondbyInqSvc.findAbuseInfoList(search);
		
		extrtCondbyInqSvc.abuseInfoDownloadExcel(modelAndView, search, request);
		
		return modelAndView;
	}
	
	/** 180314 sysong 월별 소명 현황 엑셀 다운로드 **/
	@RequestMapping(value="summonPerbyDownload.html", method={RequestMethod.POST})
	public DataModelAndView addsummonPerby_download(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		extrtCondbyInqSvc.summonPerby_download(modelAndView, search, request);
		
		return modelAndView;
	}
	
	/** 180314 sysong 소속별 소명 현황 엑셀 다운로드 **/
	@RequestMapping(value="summonDeptbyDownload.html", method={RequestMethod.POST})
	public DataModelAndView addsummonDeptby_download(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		extrtCondbyInqSvc.summonDeptby_download(modelAndView, search, request);
		
		return modelAndView;
	}
	
	/** 180314 sysong 개인별 소명 현황 엑셀 다운로드 **/
	@RequestMapping(value="summonIndvbyDownload.html", method={RequestMethod.POST})
	public DataModelAndView addsummonIndvby_download(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		extrtCondbyInqSvc.summonIndvby_download(modelAndView, search, request);
		
		return modelAndView;
	}
	
	/** 180314 sysong 소명 요청 및 판정 엑셀 다운로드 **/
	@RequestMapping(value="summonProcessbyDownload.html", method={RequestMethod.POST})
	public DataModelAndView addsummonProcessby_download(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		extrtCondbyInqSvc.summonProcessby_download(modelAndView, search, request);
		
		return modelAndView;
	}
	
	@RequestMapping(value="sendrequest.html", method={RequestMethod.POST})
	public DataModelAndView sendRequest(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters) {
		
		DataModelAndView modelAndView = extrtCondbyInqSvc.sendRequest(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@RequestMapping(value="linechart.html", method={RequestMethod.POST})
	public DataModelAndView findLineChart(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = extrtCondbyInqSvc.findLineChart(parameters);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@RequestMapping(value="piechart.html", method={RequestMethod.POST})
	public DataModelAndView detailPieChart(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = extrtCondbyInqSvc.detailPieChart(parameters);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@RequestMapping(value="barchart.html", method={RequestMethod.POST})
	public DataModelAndView detailBarChart(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = extrtCondbyInqSvc.detailBarChart(parameters);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	
	/**
	 * 2017-01-18 by hjpark
	 * 
	 * 추출조건별조회상세 chart
	 */
	@MngtActHist(log_action = "SELECT", log_message = "비정상위험분석 상세차트")
	@RequestMapping(value={"detailChart.html"}, method={RequestMethod.POST})
	public DataModelAndView findExtrtCondbyInqDetailChart(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {

		// 2017.10.16 
		// 현재날짜가 아닌 선택날짜로 변경 
		String occr_dt = parameters.get("detailOccrDt");
		String occr_dt_to = parameters.get("detailOccrDt");
		if(occr_dt != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String now = sdf.format(new Date());
			if (now.equals(occr_dt)) {
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DATE, -1);
				occr_dt = sdf.format(cal.getTime());
			}
			
			search.setSearch_from(occr_dt);
			search.setSearch_to(occr_dt_to);
			
			search.setSearch_from_chart(occr_dt);
			search.setSearch_to_chart(occr_dt_to);
		} else {
			Date today = new Date();
			if(abnormalList_top10_period_isWeek.equals("yes")) {
				search.initDatesIfEmpty_week(today, today, abnormalList_top10_period_amount);
				search.initDatesIfEmpty_week_chart(today, today, abnormalList_top10_period_amount);
			}else {
				search.initDatesIfEmpty_month(today, today, abnormalList_top10_period_amount);
				search.initDatesIfEmpty_month_chart(today, today, abnormalList_top10_period_amount);
			}
		}
		
		String emp_user_id = parameters.get("userId");
		if(emp_user_id!=null || !emp_user_id.equals("")){
			search.setEmp_user_id(emp_user_id);
		}
//		System.out.println("emp_user_id:"+emp_user_id);
		// findAbnormalDeptDetailChart6_TOT 관련 (지난달 날짜)
		Calendar oCalendar = Calendar.getInstance( ); 
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		oCalendar.add(Calendar.MONTH, -1);
		
		String perMonth = sdf.format(oCalendar.getTime());
		String year = perMonth.substring(0, 4);
		String month = perMonth.substring(4, 6);
		String fr = year + month + "01";
		String to = year + month + "31";
		search.setStarthm(fr);
		search.setEndhm(to);
		
		// findAbnormalDeptDetailChart9_TOT 관련 (지난주 날짜)
		Date lastweek = DateUtil.getLastWeek();
		String enddate = sdf.format(lastweek);
		oCalendar.setTime(lastweek);
		oCalendar.add(Calendar.DATE, -6);
		String startdate = sdf.format(oCalendar.getTime());
		search.setStartdate9(startdate);
		search.setEnddate9(enddate);
		
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
		
		DataModelAndView modelAndView = extrtCondbyInqSvc.findExtrtCondbyInqDetailChart(search);
		String tab_flag;
		if(parameters.get("tab_flag") == null || parameters.get("tab_flag") =="") {
			tab_flag = "tab1";
		} else {
			tab_flag = parameters.get("tab_flag");
		}
		
		modelAndView.addObject("tabFlag", tab_flag);
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.addObject("chart6Tot", extrtCondbyInqSvc.findAbnormalDetailChart6_TOT(search));
		modelAndView.addObject("chart7Tot", extrtCondbyInqSvc.findAbnormalDetailChart7_TOT(search));
		modelAndView.addObject("chart8Tot", extrtCondbyInqSvc.findAbnormalDetailChart8_TOT(search));
		modelAndView.addObject("chart9Tot", extrtCondbyInqSvc.findAbnormalDetailChart9_TOT(search));
		modelAndView.addObject("chart10Tot", extrtCondbyInqSvc.findAbnormalDetailChart10_TOT(search));
		modelAndView.addObject("use_detailchart", use_detailchart);
		
		/*//개인누적 평균건수
		List<ExtrtBaseSetup> avgList = extrtCondbyInqSvc.getExtrtCondByAvgCnt(emp_user_id);
		for(int i=0; i<avgList.size(); i++) {
			ExtrtBaseSetup ebs = avgList.get(i);
			BigDecimal prct = new BigDecimal(ebs.getPrct_val());
			BigDecimal avg = new BigDecimal(ebs.getAvg_val());
			avg = avg.multiply(prct);
			BigDecimal tmp = new BigDecimal(100);
			prct = prct.multiply(tmp);
			ebs.setLimit_val(avg.intValue());
			ebs.setPrct_int(prct.intValue());
		}
		modelAndView.addObject("avgList", avgList);*/
		
		modelAndView.setViewName("extrtCondbyInqDetailChart_main");

		return modelAndView;
	}
	
	/**
	 * 2017-06-19
	 * 
	 * 부서별 비정상위험분석 상세차트 추출조건별조회상세 chart
	 */
	@MngtActHist(log_action = "SELECT", log_message = "부서별 비정상위험분석 상세차트")
	@RequestMapping(value={"deptDetailChart.html"}, method={RequestMethod.POST})
	public DataModelAndView findExtrtCondbyInqDeptDetailChart(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {

		// 2017.10.16 
		// 현재날짜가 아닌 선택날짜로 변경 
		String occr_dt = parameters.get("detailOccrDt");
		if(occr_dt != null && occr_dt != "") {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String now = sdf.format(new Date());
			if (now.equals(occr_dt)) {
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DATE, -1);
				occr_dt = sdf.format(cal.getTime());
			}
			
			search.setSearch_from(occr_dt);
			search.setSearch_to(occr_dt);
			
			search.setSearch_from_chart(occr_dt);
			search.setSearch_to_chart(occr_dt);
		} else {
			Date today = new Date();
			if(abnormalList_top10_period_isWeek.equals("yes")) {
				search.initDatesIfEmpty_week(today, today, abnormalList_top10_period_amount);
				search.initDatesIfEmpty_week_chart(today, today, abnormalList_top10_period_amount);
			}else {
				search.initDatesIfEmpty_month(today, today, abnormalList_top10_period_amount);
				search.initDatesIfEmpty_month_chart(today, today, abnormalList_top10_period_amount);
			}
		}
				
		String dept_id = parameters.get("dept_id");
		search.setDept_id(dept_id);
		
		Calendar oCalendar = Calendar.getInstance( ); 
		String date = Integer.toString(oCalendar.get(Calendar.YEAR))+String.format("%02d", (oCalendar.get(Calendar.MONTH) + 1))+String.format("%02d", (oCalendar.get(Calendar.DATE)));
		// findAbnormalDeptDetailChart6_TOT 관련 (지난달 날짜)
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		oCalendar.add(Calendar.MONTH, -1);
		
		String perMonth = sdf.format(oCalendar.getTime());
		String year = perMonth.substring(0, 4);
		String month = perMonth.substring(4, 6);
		String fr = year + month + "01";
		String to = year + month + "31";
		search.setStarthm(fr);
		search.setEndhm(to);
		
		// findAbnormalDeptDetailChart9_TOT 관련 (지난주 날짜)
		Date lastweek = DateUtil.getLastWeek();
		String enddate = sdf.format(lastweek);
		oCalendar.setTime(lastweek);
		oCalendar.add(Calendar.DATE, -6);
		String startdate = sdf.format(oCalendar.getTime());
		//Map<String, String> map = new HashMap<String, String>();
		//map.put("dept_id", dept_id);
		//map.put("startdate", startdate);
		//map.put("enddate", enddate);
		search.setStartdate9(startdate);
		search.setEnddate9(enddate);
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
				
		DataModelAndView modelAndView = extrtCondbyInqSvc.findExtrtCondbyInqDeptDetailChart(search);
		modelAndView.addObject("tabFlag", "tab1");
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.addObject("chart6Tot", extrtCondbyInqSvc.findAbnormalDeptDetailChart6_TOT(search));
		modelAndView.addObject("chart7Tot", extrtCondbyInqSvc.findAbnormalDeptDetailChart7_TOT(search));
		modelAndView.addObject("chart8Tot", extrtCondbyInqSvc.findAbnormalDeptDetailChart8_TOT(search));
		modelAndView.addObject("chart9Tot", extrtCondbyInqSvc.findAbnormalDeptDetailChart9_TOT(search));
		modelAndView.addObject("chart10Tot", extrtCondbyInqSvc.findAbnormalDeptDetailChart10_TOT(search));
		modelAndView.addObject("use_detailchart", use_detailchart);
		
		modelAndView.setViewName("extrtCondbyInqDeptDetailChart_main");

		return modelAndView;
	}

	@MngtActHist(log_action = "SELECT", log_message = "[접근관리] 취급자별타임라인")
	@RequestMapping(value={"findTimeLineChart.html"}, method={RequestMethod.POST})
	public DataModelAndView findTimeLineChart(@ModelAttribute("search") SearchSearch search, 
			HttpServletRequest request,
			@RequestParam(value = "sub_menu_id", defaultValue = "") String sub_menu_id,
			@RequestParam(value = "menutitle",defaultValue="")String menutitle) {
		
		DataModelAndView modelAndView = extrtCondbyInqSvc.findTimeLineChart(search);
		modelAndView.addObject("menutitle",menutitle);
		modelAndView.addObject("sub_menu_id", sub_menu_id);
		modelAndView.setViewName("timeLineChart");
		
		return modelAndView;
	}
	@RequestMapping(value={"timeLineChartAppend.html"}, method={RequestMethod.POST})
	public DataModelAndView timeLineChartAppend(@ModelAttribute("search") SearchSearch search, 
			HttpServletRequest request) {
		DataModelAndView modelAndView = extrtCondbyInqSvc.timeLineChartAppend(search);
		modelAndView.setViewName("timeLineChartAppend");
		return modelAndView;
	}
	
	//@MngtActHist(log_action = "SELECT", log_message = "시스템접근 타임라인")
	@RequestMapping(value={"timeline.html"}, method={RequestMethod.POST})
	public DataModelAndView findExtrtCondbyInqTimeline(@ModelAttribute("search") SearchSearch search, 
			HttpServletRequest request) {
		DataModelAndView modelAndView = extrtCondbyInqSvc.timelineAppendByUser(search);
		modelAndView.setViewName("timeLineByUserAppend");
		return modelAndView;
	}

	
	@RequestMapping(value="getChart0.html", method={RequestMethod.POST})
	public DataModelAndView detailChart0(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
	
		DataModelAndView modelAndView = extrtCondbyInqSvc.getChart0(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@RequestMapping(value="getChart1.html", method={RequestMethod.POST})
	public DataModelAndView detailChart1(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
	
		DataModelAndView modelAndView = extrtCondbyInqSvc.getChart1(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@RequestMapping(value="getChart2.html", method={RequestMethod.POST})
	public DataModelAndView detailChart2(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
	
		DataModelAndView modelAndView = extrtCondbyInqSvc.getChart2(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	
	@RequestMapping(value="findAbnormalChart0.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalChart0(@RequestParam Map<String, String> parameters, 
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		SearchSearch search = new SearchSearch();
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list == null) {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    }
	    list.add("00");
    	search.setAuth_idsList(list);
	    
	    search.setSearch_from(parameters.get("search_from"));
	    search.setSearch_to(parameters.get("search_to"));
	    search.setSystem_seq(parameters.get("system_seq"));
		List<EmpDetailInq> scenarioList = extrtCondbyInqDao.scenarioList();
		List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalChart0(search);
		
		JSONArray graphs = new JSONArray();
		String[] color = {"#3598DC", "#E7505A", "#8E44AD", "#C49F47", "#32C5D2"};
		int index = 0;
		for(EmpDetailInq e : scenarioList) {
			JSONObject graph = new JSONObject();
			graph.put("balloonText","[[title]]: [[value]]");
			graph.put("bullet","round");
			graph.put("title", e.getScen_name());
			graph.put("valueField", e.getScen_seq());
			graph.put("lineColor", color[index]);
			graph.put("fillAlphas",0);
			graph.put("showHandOnHover", true);
			graphs.add(graph);
			index ++;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	    Calendar cal = Calendar.getInstance();
		cal.setTime(sdf.parse(search.getSearch_from()));
		cal.add(Calendar.DAY_OF_MONTH, -1);
		String occr_dt = sdf.format(cal.getTime());
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj = new JSONObject();
		
		while (!occr_dt.equals(search.getSearch_to())) {
			cal.add(Calendar.DAY_OF_MONTH, 1);
			occr_dt = sdf.format(cal.getTime());
			jsonObj = new JSONObject();
			jsonObj.put("occr_dt", occr_dt);
			JSONObject dataProvider = new JSONObject();
			dataProvider.put("occr_dt", occr_dt);
			int scen1 = 0;
			int scen2 = 0;
			int scen3 = 0;
			int scen4 = 0;
			int scen5 = 0;
			for(ExtrtCondbyInq e : beanList) {
				if(occr_dt.equals(e.getOccr_dt())) {
					scen1 = e.getScenario1();
					scen2 = e.getScenario2();
					scen3 = e.getScenario3();
					scen4 = e.getScenario4();
					scen5 = e.getScenario5();
					break;
				}
			}
			dataProvider.put("1000", scen1);
			dataProvider.put("2000", scen2);
			dataProvider.put("3000", scen3);
			dataProvider.put("4000", scen4);
			dataProvider.put("5000", scen5);
			jsonObj.put("dataProvider", dataProvider);
			jsonObj.put("graphs", graphs);
			jArray.add(jsonObj);
		}
		return jArray;
	}
	
	@RequestMapping(value="findAbnormalChart1.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalChart1(@RequestParam Map<String, String> parameters,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		SearchSearch search = new SearchSearch();
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list == null) {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    }
	    list.add("00");
    	search.setAuth_idsList(list);
    	String search_to = parameters.get("search_to").substring(0, 7);
    	search_to = search_to.replace("-", "");
	    search.setSearch_to(search_to);
	    search.setSystem_seq(parameters.get("system_seq"));
		List<EmpDetailInq> scenarioList = extrtCondbyInqDao.scenarioList();
		List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalChart1(search);
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
		cal.setTime(sdf.parse(search_to));
		cal.add(Calendar.MONTH, -3);
		String occr_dt = sdf.format(cal.getTime());

		JSONArray jArray = new JSONArray();
		JSONArray dataProviders = new JSONArray();
		for(int i=0; i<3; i++) {
			JSONObject dataProvider=new JSONObject();
			cal.add(Calendar.MONTH, 1);
			occr_dt = sdf.format(cal.getTime());
			int scen1 = 0;
			int scen2 = 0;
			int scen3 = 0;
			int scen4 = 0;
			int scen5 = 0;
			dataProvider.put("occr_dt", occr_dt);
			if(beanList.size()>0) {
				for(int j=0; j<beanList.size(); j++) {
					ExtrtCondbyInq b = beanList.get(j);
					if(occr_dt.equals(b.getOccr_dt())) {
						scen1 = b.getScenario1();
						scen2 = b.getScenario2();
						scen3 = b.getScenario3();
						scen4 = b.getScenario4();
						scen5 = b.getScenario5();
						break;
					}
				}
			}
			dataProvider.put("1000", scen1);
			dataProvider.put("2000", scen2);
			dataProvider.put("3000", scen3);
			dataProvider.put("4000", scen4);
			dataProvider.put("5000", scen5);
			dataProviders.add(dataProvider);
		}
		JSONArray graphs = new JSONArray();
		String[] color = {"#3598DC", "#E7505A", "#8E44AD", "#C49F47", "#32C5D2"};
		int index = 0;
		for(EmpDetailInq s : scenarioList) {
			JSONObject graph = new JSONObject();
			graph.put("balloonText", "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>");
			graph.put("fillAlphas", "0.8");
			graph.put("labelText", "[[value]]");
			graph.put("lineAlpha", "0.3");
			graph.put("title", s.getScen_name());
			graph.put("type", "column");
			graph.put("fillColors", color[index]);
			graph.put("valueField", s.getScen_seq());
			graph.put("showHandOnHover", true);
			graphs.add(graph);
			index ++;
		}
		jArray.add(dataProviders);
		jArray.add(graphs);
		
		return jArray;
	}
	@RequestMapping(value="findAbnormalDeptDonutChart.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDeptDonutChart(@RequestParam Map<String, String> parameters,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		SearchSearch search = new SearchSearch();
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list == null) {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    }
	    list.add("00");
    	search.setAuth_idsList(list);
	    search.setSearch_from(parameters.get("search_from"));
	    search.setSearch_to(parameters.get("search_to"));
		search.setSystem_seq(parameters.get("system_seq"));
		List<EmpDetailInq> beanList = extrtCondbyInqDao.findAbnormalDeptDonutChart(search);
		int totalCnt = extrtCondbyInqDao.findAbnormalDeptDonutChartAll(search);
		JSONArray jArray = new JSONArray();
		JSONArray dataProviders = new JSONArray();
		if(beanList.size()>0) {
			for(EmpDetailInq b : beanList) {
				String dept_name = b.getDept_name();
				if(dept_name.equals("DEPT99999")) {
					dept_name = "공통";
				}
				dataProviders = new JSONArray();
				JSONObject dataProvider = new JSONObject();
				dataProvider.put("cnt", b.getDay_cnt());
				dataProvider.put("dept_name", dept_name);
				dataProvider.put("dept_id", b.getDept_id());
				dataProviders.add(dataProvider);
				dataProvider = new JSONObject();
				dataProvider.put("cnt", totalCnt);
				dataProvider.put("dept_name", "");
				dataProviders.add(dataProvider);
				jArray.add(dataProviders);
			}
		}
		return jArray;
	}
	@RequestMapping(value="findAbnormalUserDonutChart.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalUserDonutChart(@RequestParam Map<String, String> parameters,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		SearchSearch search = new SearchSearch();
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list == null) {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    }
	    list.add("00");
    	search.setAuth_idsList(list);
		search.setSearch_from(parameters.get("search_from"));
	    search.setSearch_to(parameters.get("search_to"));
		search.setSystem_seq(parameters.get("system_seq"));
		List<EmpDetailInq> beanList = extrtCondbyInqDao.findAbnormalUserDonutChart(search);
		int totalCnt = extrtCondbyInqDao.findAbnormalUserDonutChartAll(search);
		JSONArray jArray = new JSONArray();
		JSONArray dataProviders = new JSONArray();
		if(beanList.size()>0) {
			for(EmpDetailInq b : beanList) {
				String emp_user_name = b.getEmp_user_name();
				if(emp_user_name.equals("ZZZZ99999")) {
					emp_user_name = "공통";
				}
				dataProviders = new JSONArray();
				JSONObject dataProvider = new JSONObject();
				dataProvider.put("cnt", b.getDay_cnt());
				dataProvider.put("user_name", emp_user_name);
				dataProvider.put("user_id", b.getEmp_user_id());
				dataProviders.add(dataProvider);
				dataProvider = new JSONObject();
				dataProvider.put("cnt", totalCnt);
				dataProvider.put("user_name", "");
				dataProviders.add(dataProvider);
				jArray.add(dataProviders);
			}
		}
		return jArray;
	}
	
	@RequestMapping(value="findAbnormalDetailDngChart.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDetailDngChart(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "emp_user_name", defaultValue = "") String emp_user_name,
			@RequestParam(value = "dept_id", defaultValue = "") String dept_id,
			@RequestParam(value = "search_from", defaultValue = "") String search_from,
			@RequestParam(value = "search_to", defaultValue = "") String search_to,
			@RequestParam(value = "scen_seq", defaultValue = "") String scen_seq) throws Exception {	
		
		ExtrtCondbyInq param = new ExtrtCondbyInq();
		param.setSearch_from(search_from);
		param.setSearch_to(search_to);
		param.setScen_seq(scen_seq);
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list == null) {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    }
	    list.add("00"); // 공통 시스템코드 추가
		param.setAuth_idsList(list);
		
		int cnt = 0;
		int total = 0;
		param.setDept_id(dept_id);
		try {
			total = extrtCondbyInqSvc.findAbnormalDeptDetailDngChart(param); // 취급자 부서
			param.setEmp_user_id(emp_user_name);
			cnt = extrtCondbyInqSvc.findAbnormalDetailDngChart(param); // 해당 취급자
		} catch (Exception e) {
			e.printStackTrace();
		}
		JSONArray jArray = new JSONArray();
		JSONObject jsonObj=new JSONObject();
		jsonObj.put("data1", cnt);
		jsonObj.put("data2", total);
		jArray.add(jsonObj);
		
		return jArray;
	}
	
	@RequestMapping(value="findAbnormalDetailChart1.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDetailChart1(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "emp_user_name", defaultValue = "") String emp_user_name,
			@RequestParam(value = "dept_id", defaultValue = "") String dept_id,
			@RequestParam(value = "search_from", defaultValue = "") String search_from,
			@RequestParam(value = "search_to", defaultValue = "") String search_to) throws Exception {
		
		JSONArray jArray = new JSONArray();
		
		ExtrtCondbyInq param = new ExtrtCondbyInq();
		param.setEmp_user_id(emp_user_name);
		param.setSearch_from(search_from);
		param.setSearch_to(search_to);
		param.setDept_id(dept_id);
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	param.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	param.setAuth_idsList(list);
	    }
		
		List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalDetailChart1(param);
		List<ExtrtCondbyInq> beanList2 = extrtCondbyInqSvc.findAbnormalDetailChart1_2(param);
		//beanList3 = extrtCondbyInqSvc.findAbnormalDetailChart1_3(beanList1.get(0).getDept_id());
		
		if ( beanList == null ) {
			JSONObject jsonObj=new JSONObject();
			jsonObj.put("data1", 0);
			jsonObj.put("data2", 0);
			jArray.add(jsonObj);
		} else {
			//for ( int i=0; i<beanList.size(); ++i ) {
				//ExtrtCondbyInq temp = beanList.get(i);
				//ExtrtCondbyInq temp1 = beanList2.get(i);
				ExtrtCondbyInq temp = beanList.get(0);
				ExtrtCondbyInq temp1 = beanList2.get(0);
				
				//int nCount = 0;
				//if ( temp1 != null && temp !=null && beanList3.size() > 0 ) {
				if ( temp1 != null && temp !=null ) {
					//nCount = temp1.getCnt()/beanList3.size();
					JSONObject jsonObj=new JSONObject();
					jsonObj.put("data1", temp.getCnt());
					//jsonObj.put("data2", nCount);
					jsonObj.put("data2", temp1.getCnt());
					jArray.add(jsonObj);
				}
				else {
					JSONObject jsonObj=new JSONObject();
					jsonObj.put("data1", 0);
					jsonObj.put("data2", 0);
					jArray.add(jsonObj);
				}
			}
		//}
		
		return jArray;
	}
	
	@RequestMapping(value="findAbnormalDetailChart2.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDetailChart2(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "emp_user_name", defaultValue = "") String emp_user_name,
			@RequestParam(value = "dept_id", defaultValue = "") String dept_id,
			@RequestParam(value = "search_from", defaultValue = "") String search_from,
			@RequestParam(value = "search_to", defaultValue = "") String search_to) throws Exception {
		
		JSONArray jArray = new JSONArray();
		
		ExtrtCondbyInq param = new ExtrtCondbyInq();
		param.setEmp_user_id(emp_user_name);
		param.setSearch_from(search_from);
		param.setSearch_to(search_to);
		param.setDept_id(dept_id);
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	param.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	param.setAuth_idsList(list);
	    }
		
		List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalDetailChart2(param);
		//List<ExtrtCondbyInq> beanList1 = extrtCondbyInqSvc.findAbnormalDetailChart2_1(emp_user_name);
		List<ExtrtCondbyInq> beanList2 = extrtCondbyInqSvc.findAbnormalDetailChart2_2(param);
		//beanList3 = extrtCondbyInqSvc.findAbnormalDetailChart2_3(beanList1.get(0).getDept_id());
		
		if ( beanList == null ) {
			JSONObject jsonObj=new JSONObject();
			jsonObj.put("data1", 0);
			jsonObj.put("data2", 0);
			jArray.add(jsonObj);
		} else {
			for ( int i=0; i<beanList.size(); ++i ) {
				ExtrtCondbyInq temp = beanList.get(i);
				ExtrtCondbyInq temp1 = beanList2.get(i);
				
				//int nCount = 0;
				//if ( temp1 != null && temp !=null && beanList3.size() > 0 ) {
				if ( temp1 != null && temp !=null ) {
					//nCount = temp1.getCnt()/beanList3.size();
					JSONObject jsonObj=new JSONObject();
					jsonObj.put("data1", temp.getCnt());
					//jsonObj.put("data2", nCount);
					jsonObj.put("data2", temp1.getCnt());
					jArray.add(jsonObj);
				}
				else {
					JSONObject jsonObj=new JSONObject();
					jsonObj.put("data1", 0);
					jsonObj.put("data2", 0);
					jArray.add(jsonObj);
				}
			}
		}
		
		return jArray;
	}
	
	@RequestMapping(value="findAbnormalDetailChart3.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDetailChart3(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "emp_user_name", defaultValue = "") String emp_user_name,
			@RequestParam(value = "dept_id", defaultValue = "") String dept_id,
			@RequestParam(value = "search_from", defaultValue = "") String search_from,
			@RequestParam(value = "search_to", defaultValue = "") String search_to) throws Exception {	
		
		JSONArray jArray = new JSONArray();
		
		ExtrtCondbyInq param = new ExtrtCondbyInq();
		param.setEmp_user_id(emp_user_name);
		param.setSearch_from(search_from);
		param.setSearch_to(search_to);
		param.setDept_id(dept_id);
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	param.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	param.setAuth_idsList(list);
	    }
		
		List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalDetailChart3(param);
		//List<ExtrtCondbyInq> beanList1 = extrtCondbyInqSvc.findAbnormalDetailChart3_1(emp_user_name);
		List<ExtrtCondbyInq> beanList2 = extrtCondbyInqSvc.findAbnormalDetailChart3_2(param);
		//beanList3 = extrtCondbyInqSvc.findAbnormalDetailChart3_3(beanList1.get(0).getDept_id());
		
		if ( beanList == null ) {
			JSONObject jsonObj=new JSONObject();
			jsonObj.put("data1", 0);
			jsonObj.put("data2", 0);
			jArray.add(jsonObj);
		} else {
			for ( int i=0; i<beanList.size(); ++i ) {
				ExtrtCondbyInq temp = beanList.get(i);
				ExtrtCondbyInq temp1 = beanList2.get(i);
				
				//int nCount = 0;
				//if ( temp1 != null && temp !=null && beanList3.size() > 0 ) {
				if ( temp1 != null && temp !=null ) {
					//nCount = temp1.getCnt()/beanList3.size();
					JSONObject jsonObj=new JSONObject();
					jsonObj.put("data1", temp.getCnt());
					//jsonObj.put("data2", nCount);
					jsonObj.put("data2", temp1.getCnt());
					jArray.add(jsonObj);
				}
				else {
					JSONObject jsonObj=new JSONObject();
					jsonObj.put("data1", 0);
					jsonObj.put("data2", 0);
					jArray.add(jsonObj);
				}
			}
		}
		
		return jArray;
	}
	
	@RequestMapping(value="findAbnormalDetailChart4.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDetailChart4(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "emp_user_name", defaultValue = "") String emp_user_name,
			@RequestParam(value = "dept_id", defaultValue = "") String dept_id,
			@RequestParam(value = "search_from", defaultValue = "") String search_from,
			@RequestParam(value = "search_to", defaultValue = "") String search_to) throws Exception {	
		
		JSONArray jArray = new JSONArray();
		
		ExtrtCondbyInq param = new ExtrtCondbyInq();
		param.setEmp_user_id(emp_user_name);
		param.setSearch_from(search_from);
		param.setSearch_to(search_to);
		param.setDept_id(dept_id);
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	param.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	param.setAuth_idsList(list);
	    }
		
		List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalDetailChart4(param);
		//List<ExtrtCondbyInq> beanList1 = extrtCondbyInqSvc.findAbnormalDetailChart4_1(emp_user_name);
		List<ExtrtCondbyInq> beanList2 = extrtCondbyInqSvc.findAbnormalDetailChart4_2(param);
			//beanList3 = extrtCondbyInqSvc.findAbnormalDetailChart4_3(beanList1.get(0).getDept_id());
		
		if ( beanList == null ) {
			JSONObject jsonObj=new JSONObject();
			jsonObj.put("data1", 0);
			jsonObj.put("data2", 0);
			jArray.add(jsonObj);
		} else {
			for ( int i=0; i<beanList.size(); ++i ) {
				ExtrtCondbyInq temp = beanList.get(i);
				ExtrtCondbyInq temp1 = beanList2.get(i);
				
				//int nCount = 0;
				//if ( temp1 != null && temp !=null && beanList3.size() > 0 ) {
				if ( temp1 != null && temp !=null ) {
					//nCount = temp1.getCnt()/beanList3.size();
					JSONObject jsonObj=new JSONObject();
					jsonObj.put("data1", temp.getCnt());
					//jsonObj.put("data2", nCount);
					jsonObj.put("data2", temp1.getCnt());
					jArray.add(jsonObj);
				}
				else {
					JSONObject jsonObj=new JSONObject();
					jsonObj.put("data1", 0);
					jsonObj.put("data2", 0);
					jArray.add(jsonObj);
				}
			}

		}
		
		return jArray;
	}
	
	@RequestMapping(value="findAbnormalDetailChart5.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDetailChart5(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "emp_user_name", defaultValue = "") String emp_user_name,
			@RequestParam(value = "dept_id", defaultValue = "") String dept_id,
			@RequestParam(value = "search_from", defaultValue = "") String search_from,
			@RequestParam(value = "search_to", defaultValue = "") String search_to) throws Exception {	
		
		JSONArray jArray = new JSONArray();
		
		ExtrtCondbyInq param = new ExtrtCondbyInq();
		param.setEmp_user_id(emp_user_name);
		param.setSearch_from(search_from);
		param.setSearch_to(search_to);
		param.setDept_id(dept_id);
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	param.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	param.setAuth_idsList(list);
	    }
		
		List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalDetailChart5(param);
		//List<ExtrtCondbyInq> beanList1 = extrtCondbyInqSvc.findAbnormalDetailChart5_1(emp_user_name);
		List<ExtrtCondbyInq> beanList2 = extrtCondbyInqSvc.findAbnormalDetailChart5_2(param);
		//beanList3 = extrtCondbyInqSvc.findAbnormalDetailChart5_3(beanList1.get(0).getDept_id());
		
		if ( beanList == null ) {
			JSONObject jsonObj=new JSONObject();
			jsonObj.put("data1", 0);
			jsonObj.put("data2", 0);
			jArray.add(jsonObj);
		} else {
			for ( int i=0; i<beanList.size(); ++i ) {
				ExtrtCondbyInq temp = beanList.get(i);
				ExtrtCondbyInq temp1 = beanList2.get(i);
				
				//int nCount = 0;
				//if ( temp1 != null && temp !=null && beanList3.size() > 0 ) {
				if ( temp1 != null && temp !=null ) {
					//nCount = temp1.getCnt()/beanList3.size();
					JSONObject jsonObj=new JSONObject();
					jsonObj.put("data1", temp.getCnt());
					//jsonObj.put("data2", nCount);
					jsonObj.put("data2", temp1.getCnt());
					jArray.add(jsonObj);
				}
				else {
					JSONObject jsonObj=new JSONObject();
					jsonObj.put("data1", 0);
					jsonObj.put("data2", 0);
					jArray.add(jsonObj);
				}
			}

		}
		
		return jArray;
	}
	@RequestMapping(value="findAbnormalDetailChart6.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDetailChart6(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "emp_user_name", defaultValue = "") String emp_user_name) throws Exception {
		
		JSONArray jArray = new JSONArray();

		HashMap<String,Long> mapTemp = new HashMap<String,Long>();
		Calendar oCalendar = Calendar.getInstance( );
		Calendar lastCal = Calendar.getInstance();

		int prevMonth = lastCal.get(Calendar.MONTH);
		lastCal.add(Calendar.MONTH, -1);
		int lastDayPrevMonth = lastCal.getActualMaximum(lastCal.DAY_OF_MONTH);

		for ( int i=1; i<=lastDayPrevMonth; ++i ) {
			String date = Integer.toString(oCalendar.get(Calendar.YEAR))+String.format("%2d", (oCalendar.get(Calendar.MONTH)));
			String s = String.format("%02d", i);
			date=date+s;
			date=date.replaceAll(" ", "0");
			mapTemp.put(date, (long) 0);
		}
		
		SearchSearch search = new SearchSearch();
		
		search.setEmp_user_id(emp_user_name);
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
	    
		List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalDetailChart6(search);
		
		for ( int i=0; i<beanList.size(); ++i ) {
			ExtrtCondbyInq tmp = beanList.get(i);
			mapTemp.put(tmp.getData(), tmp.getCnt());
		}
		
		JSONObject dataProvider=new JSONObject();
		for ( int i=1; i<=lastDayPrevMonth; i++ ) {

			dataProvider=new JSONObject();
			
			String date = Integer.toString(oCalendar.get(Calendar.YEAR))+String.format("%2d", (oCalendar.get(Calendar.MONTH)));
			String s = String.format("%02d", i);
			date=date+s;
			date=date.replaceAll(" ", "0");
			long nCount = mapTemp.get(date);

			String proc_date = prevMonth + "/" + i;

			Calendar cal = Calendar.getInstance() ;
		    cal.set(lastCal.get(Calendar.YEAR), lastCal.get(Calendar.MONTH),i);
			
			int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
			if(dayOfWeek == 1){
				proc_date = proc_date + "(일)";
			}else if(dayOfWeek == 2){
				proc_date = proc_date + "(월)";
			}else if(dayOfWeek == 3){
				proc_date = proc_date + "(화)";
			}else if(dayOfWeek == 4){
				proc_date = proc_date + "(수)";
			}else if(dayOfWeek == 5){
				proc_date = proc_date + "(목)";
			}else if(dayOfWeek == 6){
				proc_date = proc_date + "(금)";
			}else if(dayOfWeek == 7){
				proc_date = proc_date + "(토)";
			}

			dataProvider.put("proc_date", proc_date);
			dataProvider.put("value1", nCount);
			jArray.add(dataProvider);
		}
		return jArray;
	}
	
	@RequestMapping(value="findAbnormalDetailChart7.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDetailChart7(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "emp_user_name", defaultValue = "") String emp_user_name) throws Exception {
		
		JSONArray jArray = new JSONArray();
		JSONObject dataProvider=new JSONObject();
		
		Calendar cal = Calendar.getInstance( );
		int daysOfMonth = 0;
		for(int i=0; i<3; i++){
			cal.add(Calendar.MONTH, -1);
			daysOfMonth = daysOfMonth + cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		}
		
		SearchSearch search = new SearchSearch();
		
		search.setEmp_user_id(emp_user_name);
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
	    
		List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalDetailChart7(search);
		List<ExtrtCondbyInq> beanList2 = extrtCondbyInqSvc.findAbnormalDetailChart7_1(search);
		
		for ( int i=0; i<beanList.size(); ++i ) {
			ExtrtCondbyInq tmp = beanList.get(i);
			ExtrtCondbyInq tmp2 = beanList2.get(i);
	
			dataProvider=new JSONObject();

			dataProvider.put("data1", tmp.getData());
			dataProvider.put("cnt1", tmp.getCnt()/daysOfMonth);
			dataProvider.put("cnt2", tmp2.getCnt());
			jArray.add(dataProvider);
		}
		
		return jArray;
	}
	
	@RequestMapping(value="findAbnormalDetailChart8.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDetailChart8(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "emp_user_name", defaultValue = "") String emp_user_name) throws Exception {
			
			JSONArray jArray = new JSONArray();
			JSONObject dataProvider=new JSONObject();
			
			String data[] = {"M-6", "M-5", "M-4", "M-3", "M-2", "M-1", "M"};
			
			Calendar cal = Calendar.getInstance( );
			cal.add(Calendar.MONTH, 1);
			int mm = cal.get(Calendar.MONTH);
			
			for(int i=6; i>=0; i--){
				if(mm==0) mm=12;

				data[i] = Integer.toString(mm)+"월";
				mm--;
			}
			
			SearchSearch search = new SearchSearch();
			
			search.setEmp_user_id(emp_user_name);
			
			List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
		    if(list != null) {
		    	search.setAuth_idsList(list);
		    }else {
		    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
		    	search.setAuth_idsList(list);
		    }

			List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalDetailChart8(search);
			
			for ( int i=0; i<beanList.size(); ++i ) {
				ExtrtCondbyInq tmp = beanList.get(i);
				dataProvider=new JSONObject();
				dataProvider.put("date", data[i]);
				dataProvider.put("cnt", tmp.getCnt());
				jArray.add(dataProvider);
			}
			
			return jArray;
	}
	
	@RequestMapping(value="findAbnormalDetailChart9.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDetailChart9(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "emp_user_name", defaultValue = "") String emp_user_name) throws Exception {
			
			JSONArray jArray = new JSONArray();
			JSONObject dataProvider=new JSONObject();

			long sunCnt=0, monCnt=0, tueCnt=0, wedCnt=0, thuCnt=0, friCnt=0, satCnt=0;
			
			for(int i=3; i>0; i--){
				Calendar lastCal = Calendar.getInstance();
				lastCal.add(Calendar.MONTH, -i);
				int lastDayPrevMonth = lastCal.getActualMaximum(lastCal.DAY_OF_MONTH);
	
				for ( int j=1; j<=lastDayPrevMonth; j++ ) {
	
					Calendar cal = Calendar.getInstance() ;
				    cal.set(lastCal.get(Calendar.YEAR), lastCal.get(Calendar.MONTH),j);
					
					int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
					if(dayOfWeek == 1){
						sunCnt++;
					}else if(dayOfWeek == 2){
						monCnt++;
					}else if(dayOfWeek == 3){
						tueCnt++;
					}else if(dayOfWeek == 4){
						wedCnt++;
					}else if(dayOfWeek == 5){
						thuCnt++;
					}else if(dayOfWeek == 6){
						friCnt++;
					}else if(dayOfWeek == 7){
						satCnt++;
					}
				}
			}
			String data[] = {"일", "월", "화", "수", "목", "금", "토"};
			
			SearchSearch search = new SearchSearch();
			
			search.setEmp_user_id(emp_user_name);
			
			List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
		    if(list != null) {
		    	search.setAuth_idsList(list);
		    }else {
		    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
		    	search.setAuth_idsList(list);
		    }

			List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalDetailChart9(search);
			List<ExtrtCondbyInq> beanList2 = extrtCondbyInqSvc.findAbnormalDetailChart9_1(search);

		    long sun=0, mon=0, tue=0, wed=0, thu=0, fri=0, sat=0;

		    for ( int i=0; i<beanList2.size(); ++i ) {
				ExtrtCondbyInq tmp = beanList2.get(i);
				String dt = tmp.getData();
				int yyyy = Integer.parseInt(dt.substring(0,4));
				int mm = Integer.parseInt(dt.substring(4,6))-1;
				int dd = Integer.parseInt(dt.substring(6));
				
				Calendar cal = Calendar.getInstance();
			    cal.set(yyyy, mm, dd);
			    
				int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
				if(dayOfWeek == 1){
					sun = sun + tmp.getCnt();
				}else if(dayOfWeek == 2){
					mon = mon + tmp.getCnt();
				}else if(dayOfWeek == 3){
					tue = tue + tmp.getCnt();
				}else if(dayOfWeek == 4){
					wed = wed + tmp.getCnt();
				}else if(dayOfWeek == 5){
					thu = thu + tmp.getCnt();
				}else if(dayOfWeek == 6){
					fri = fri + tmp.getCnt();
				}else if(dayOfWeek == 7){
					sat = sat + tmp.getCnt();
				}
			}
			long cnt[] = {sun/sunCnt, mon/monCnt, tue/tueCnt, wed/wedCnt, thu/thuCnt, fri/friCnt, sat/satCnt};
			
			for ( int i=0; i<beanList.size(); ++i ) {
				ExtrtCondbyInq tmp = beanList.get(i);
				dataProvider=new JSONObject();
				dataProvider.put("date", data[i]);
				dataProvider.put("cnt1", tmp.getCnt());
				dataProvider.put("cnt2", cnt[i]);
				jArray.add(dataProvider);
			}
			
			return jArray;
	}
	
	@RequestMapping(value="findAbnormalDetailChart10.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDetailChart10(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "emp_user_name", defaultValue = "") String emp_user_name) throws Exception {
		
		JSONArray jArray = new JSONArray();
		JSONObject dataProvider=new JSONObject();
		
		Calendar cal = Calendar.getInstance( );
		int daysOfMonth = 0;
		for(int i=0; i<3; i++){
			cal.add(Calendar.MONTH, -1);
			daysOfMonth = daysOfMonth + cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		}
		String data[]=new String[24];
		
		for(int i=0; i<data.length; ++i){
			data[i] = String.format("%02d", i);
		}
		
		SearchSearch search = new SearchSearch();
		
		search.setEmp_user_id(emp_user_name);
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }

		List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalDetailChart10(search);
		List<ExtrtCondbyInq> beanList2 = extrtCondbyInqSvc.findAbnormalDetailChart10_1(search);
		
		for ( int i=0; i<data.length; ++i ) {
			dataProvider=new JSONObject();
			long cnt1=0;
			long cnt2=0;
			for(int j=0; j<beanList.size(); j++){
				ExtrtCondbyInq tmp = beanList.get(j);
				if(tmp.getProc_time().equals(data[i])){
					cnt1 = tmp.getCnt();
					break;
				}
			}
			
			for(int j=0; j<beanList2.size(); j++){
				ExtrtCondbyInq tmp2 = beanList2.get(j);
				if(tmp2.getProc_time().equals(data[i])){
					cnt2 = tmp2.getCnt()/daysOfMonth;
					break;
				}
			}
			dataProvider.put("date", data[i]);
			dataProvider.put("cnt1", cnt1);
			dataProvider.put("cnt2", cnt2);
			jArray.add(dataProvider);
		}
		return jArray;
	}
	
	@RequestMapping(value="findAbnormalDetailChart10_noAverage.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDetailChart10_noAverage(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "emp_user_name", defaultValue = "") String emp_user_name) throws Exception {
		
		JSONArray jArray = new JSONArray();
		JSONObject dataProvider=new JSONObject();
		
		Calendar cal = Calendar.getInstance( );
		int daysOfMonth = 0;
		for(int i=0; i<3; i++){
			cal.add(Calendar.MONTH, -1);
			daysOfMonth = daysOfMonth + cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		}
		String data[]=new String[24];
		
		for(int i=0; i<data.length; ++i){
			data[i] = String.format("%02d", i);
		}

		SearchSearch search = new SearchSearch();
		
		search.setEmp_user_id(emp_user_name);
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
		
		List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalDetailChart10(search);
		
		for ( int i=0; i<data.length; ++i ) {
			dataProvider=new JSONObject();
			long cnt1=0;
			for(int j=0; j<beanList.size(); j++){
				ExtrtCondbyInq tmp = beanList.get(j);
				if(tmp.getProc_time().equals(data[i])){
					cnt1 = tmp.getCnt();
					break;
				}
			}
			
			dataProvider.put("date", data[i]);
			dataProvider.put("cnt1", cnt1);
			jArray.add(dataProvider);
		}
		return jArray;
	}
	
	@RequestMapping(value="findAbnormalDetailChart11.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDetailChart11(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "emp_user_name", defaultValue = "") String emp_user_name,
			@RequestParam(value = "search_from", required = false) String search_from,
			@RequestParam(value = "search_to", required = false) String search_to) throws Exception {

		JSONArray jArray = new JSONArray();
		JSONObject dataProvider=new JSONObject();		
		ExtrtCondbyInq eci = new ExtrtCondbyInq();
		eci.setEmp_user_id(emp_user_name);	
		
		Calendar oCalendar = Calendar.getInstance( );
		String date = Integer.toString(oCalendar.get(Calendar.YEAR))+String.format("%02d", (oCalendar.get(Calendar.MONTH))+1)+String.format("%02d", (oCalendar.get(Calendar.DATE)));

		if(search_from == null){
			eci.setSearch_from(date);
			eci.setSearch_to(date);
		} else {
			eci.setSearch_from(search_from.replaceAll("-", ""));
			eci.setSearch_to(search_to.replaceAll("-", ""));
		}
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	eci.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	eci.setAuth_idsList(list);
	    }
		Statistics beanList = extrtCondbyInqSvc.findAbnormalDetailChart11_new(eci);
		List<Code> codeList = extrtCondbyInqDao.resultTypeList_chart();
		for ( int i=0; i<codeList.size(); ++i ) {
			Code code = codeList.get(i);
			String code_id = code.getCode_id();
			dataProvider=new JSONObject();
			dataProvider.put("data", code.getCode_name());
			int cnt = GetStatisticsTypeData.getType(beanList, "type"+code.getResult_type_order());
			dataProvider.put("cnt", cnt);
			jArray.add(dataProvider);
		}
		
		return jArray;
	}
	
	@RequestMapping(value="findAbnormalDetailChart12.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDetailChart12(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "emp_user_name", defaultValue = "") String emp_user_name,
			@RequestParam(value = "search_from", required = false) String search_from,
			@RequestParam(value = "search_to", required = false) String search_to) throws Exception {
		JSONArray jArray = new JSONArray();
		JSONObject dataProvider=new JSONObject();		
		ExtrtCondbyInq eci = new ExtrtCondbyInq();
		eci.setEmp_user_id(emp_user_name);	
		Calendar oCalendar = Calendar.getInstance( );
		String date = Integer.toString(oCalendar.get(Calendar.YEAR))+String.format("%02d", (oCalendar.get(Calendar.MONTH))+1)+String.format("%02d", (oCalendar.get(Calendar.DATE)));
		if(search_from == null){
			eci.setSearch_from(date);
			eci.setSearch_to(date);
		} else {
			eci.setSearch_from(search_from.replaceAll("-", ""));
			eci.setSearch_to(search_to.replaceAll("-", ""));
		}
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	eci.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	eci.setAuth_idsList(list);
	    }
	    SearchSearch search = new SearchSearch();
	    search.setAuth_idsList(eci.getAuth_idsList());
		List<SystemMaster> SystemMasterList = indvinfoTypeSetupDao.getSystemMasterListByAuth(search);
		List<Statistics> beanList = extrtCondbyInqSvc.findAbnormalDetailChart12(eci);
		List<ExtrtCondbyInq> beanList2 = extrtCondbyInqSvc.findAbnormalDetailChart12_1(eci);
		for(int k=0; k<SystemMasterList.size();k++) {
			String system_name = SystemMasterList.get(k).getSystem_name();
			dataProvider=new JSONObject();
			dataProvider.put("system_name", system_name);
			for(int i=0; i<beanList.size(); i++) {
				Statistics st = beanList.get(i);
				if(system_name.equals(st.getSystem_name())) {
					dataProvider.put("cnt1", st.getType1());
					dataProvider.put("cnt2", st.getType2());
					dataProvider.put("cnt3", st.getType3());
					dataProvider.put("cnt10", st.getType10());
				}
			}
			for(int j=0;j<beanList2.size();j++) {
				ExtrtCondbyInq ex = beanList2.get(j);
				if(system_name.equals(ex.getSystem_name())) {
					dataProvider.put("cnt", ex.getCnt());
				}
			}
			jArray.add(dataProvider);
		}
		return jArray;
	}
	
	@RequestMapping(value="findAbnormalDetailChart13.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDetailChart13(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "emp_user_name", defaultValue = "") String emp_user_name,
			@RequestParam(value = "search_from", required = false) String search_from,
			@RequestParam(value = "search_to", required = false) String search_to) throws Exception {

		JSONArray jArray = new JSONArray();
		JSONObject dataProvider=new JSONObject();		
		ExtrtCondbyInq eci = new ExtrtCondbyInq();
		eci.setEmp_user_id(emp_user_name);	
		
		Calendar oCalendar = Calendar.getInstance( );
		String date = Integer.toString(oCalendar.get(Calendar.YEAR))+String.format("%02d", (oCalendar.get(Calendar.MONTH))+1)+String.format("%02d", (oCalendar.get(Calendar.DATE)));

		if(search_from == null){
			eci.setSearch_from(date);
			eci.setSearch_to(date);
		} else {
			eci.setSearch_from(search_from.replaceAll("-", ""));
			eci.setSearch_to(search_to.replaceAll("-", ""));
		}
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	eci.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	eci.setAuth_idsList(list);
	    }

		List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalDetailChart13(eci);
		
		for ( int i=0; i<beanList.size(); ++i ) {
			ExtrtCondbyInq tmp = beanList.get(i);
	
			dataProvider=new JSONObject();

			dataProvider.put("data", tmp.getData());
			dataProvider.put("cnt", tmp.getCnt());
			jArray.add(dataProvider);
		}
		
		return jArray;
	}
	
	@RequestMapping(value="findAbnormalDetailChart14.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDetailChart14(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "emp_user_name", defaultValue = "") String emp_user_name,
			@RequestParam(value = "search_from", required = false) String search_from,
			@RequestParam(value = "search_to", required = false) String search_to) throws Exception {

		JSONArray jArray = new JSONArray();
		JSONObject dataProvider=new JSONObject();		
		ExtrtCondbyInq eci = new ExtrtCondbyInq();
		eci.setEmp_user_id(emp_user_name);	
		
		Calendar oCalendar = Calendar.getInstance( );
		String date = Integer.toString(oCalendar.get(Calendar.YEAR))+String.format("%02d", (oCalendar.get(Calendar.MONTH))+1)+String.format("%02d", (oCalendar.get(Calendar.DATE)));

		if(search_from == null){
			eci.setSearch_from(date);
			eci.setSearch_to(date);
		} else {
			eci.setSearch_from(search_from.replaceAll("-", ""));
			eci.setSearch_to(search_to.replaceAll("-", ""));
		}
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	eci.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	eci.setAuth_idsList(list);
	    }

		List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalDetailChart14(eci);
		
		for ( int i=0; i<beanList.size(); ++i ) {
			ExtrtCondbyInq tmp = beanList.get(i);
	
			dataProvider=new JSONObject();

			dataProvider.put("data", tmp.getData());
			dataProvider.put("cnt", tmp.getCnt());
			jArray.add(dataProvider);
		}
		
		return jArray;
	}
	
	
	/*부서별 비정상위험분석 상세차트*/
	@RequestMapping(value="findAbnormalDeptDetailDngChart.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDeptDetailDngChart(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "dept_id", defaultValue = "") String dept_id,
			@RequestParam(value = "search_from", required = false) String search_from,
			@RequestParam(value = "search_to", required = false) String search_to,
			@RequestParam(value = "scen_seq", required = false) String scen_seq) throws Exception {	JSONArray jArray = new JSONArray();
		
		ExtrtCondbyInq param = new ExtrtCondbyInq();
		param.setSearch_from(search_from);
		param.setSearch_to(search_to);
		param.setScen_seq(scen_seq);
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list == null) {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    }
	    list.add("00"); // 공통 시스템코드 추가
		param.setAuth_idsList(list);
		
		long nCount = 0;
		long nCount1 = 0;
		try {
			nCount1 = extrtCondbyInqSvc.findAbnormalDeptDetailDngChart(param); // 전체
			param.setDept_id(dept_id);
			nCount = extrtCondbyInqSvc.findAbnormalDeptDetailDngChart(param); // 해당 부서
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		JSONObject jsonObj=new JSONObject();
		jsonObj.put("data", nCount);
		jsonObj.put("data1", nCount1);
		jArray.add(jsonObj);

		return jArray;
	}
	
	
	
	@RequestMapping(value="findAbnormalDeptDetailChart1.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDeptDetailChart1(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "dept_id", defaultValue = "") String dept_id,
			@RequestParam(value = "search_from", required = false) String search_from,
			@RequestParam(value = "search_to", required = false) String search_to) throws Exception {
		JSONArray jArray = new JSONArray();
		
		ExtrtCondbyInq param = new ExtrtCondbyInq();
		param.setDept_id(dept_id);
		param.setSearch_from(search_from);
		param.setSearch_to(search_to);
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	param.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	param.setAuth_idsList(list);
	    }
		
		List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalDeptDetailChart1_1(param);
		List<ExtrtCondbyInq> beanList1 = extrtCondbyInqSvc.findAbnormalDeptDetailChart1(param);

		long nCount = 0;
		long nCount1 = 0;
		if ( beanList != null ) {
			nCount = beanList.get(0).getCnt();
			nCount1 = beanList1.get(0).getCnt();
		}
		
		JSONObject jsonObj=new JSONObject();
		jsonObj.put("data", nCount);
		jsonObj.put("data1", nCount1);
		jArray.add(jsonObj);

		return jArray;
	}

	@RequestMapping(value="findAbnormalDeptDetailChart2.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDeptDetailChart2(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "dept_id", defaultValue = "") String dept_id,
			@RequestParam(value = "search_from", required = false) String search_from,
			@RequestParam(value = "search_to", required = false) String search_to) throws Exception {
		JSONArray jArray = new JSONArray();
		
		ExtrtCondbyInq param = new ExtrtCondbyInq();
		param.setDept_id(dept_id);
		param.setSearch_from(search_from);
		param.setSearch_to(search_to);
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	param.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	param.setAuth_idsList(list);
	    }
		
		List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalDeptDetailChart2_1(param);
		List<ExtrtCondbyInq> beanList1 = extrtCondbyInqSvc.findAbnormalDeptDetailChart2(param);

		long nCount = 0;
		long nCount1 = 0;
		if ( beanList != null ) {
			nCount = beanList.get(0).getCnt();
			nCount1 = beanList1.get(0).getCnt();
		}
		
		JSONObject jsonObj=new JSONObject();
		jsonObj.put("data", nCount);
		jsonObj.put("data1", nCount1);
		jArray.add(jsonObj);

		return jArray;
	}
	@RequestMapping(value="findAbnormalDeptDetailChart3.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDeptDetailChart3(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "dept_id", defaultValue = "") String dept_id,
			@RequestParam(value = "search_from", required = false) String search_from,
			@RequestParam(value = "search_to", required = false) String search_to) throws Exception {	JSONArray jArray = new JSONArray();
		
		ExtrtCondbyInq param = new ExtrtCondbyInq();
		param.setDept_id(dept_id);
		param.setSearch_from(search_from);
		param.setSearch_to(search_to);
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	param.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	param.setAuth_idsList(list);
	    }
			
		List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalDeptDetailChart3_1(param);
		List<ExtrtCondbyInq> beanList1 = extrtCondbyInqSvc.findAbnormalDeptDetailChart3(param);

		long nCount = 0;
		long nCount1 = 0;
		if ( beanList != null ) {
			nCount = beanList.get(0).getCnt();
			nCount1 = beanList1.get(0).getCnt();
		}
		
		JSONObject jsonObj=new JSONObject();
		jsonObj.put("data", nCount);
		jsonObj.put("data1", nCount1);
		jArray.add(jsonObj);

		return jArray;
	}
	
	@RequestMapping(value="findAbnormalDeptDetailChart4.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDeptDetailChart4(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "dept_id", defaultValue = "") String dept_id,
			@RequestParam(value = "search_from", required = false) String search_from,
			@RequestParam(value = "search_to", required = false) String search_to) throws Exception {	JSONArray jArray = new JSONArray();
		
		ExtrtCondbyInq param = new ExtrtCondbyInq();
		param.setDept_id(dept_id);
		param.setSearch_from(search_from);
		param.setSearch_to(search_to);
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	param.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	param.setAuth_idsList(list);
	    }
			
		List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalDeptDetailChart4_1(param);
		List<ExtrtCondbyInq> beanList1 = extrtCondbyInqSvc.findAbnormalDeptDetailChart4(param);

		long nCount = 0;
		long nCount1 = 0;
		if ( beanList != null ) {
			nCount = beanList.get(0).getCnt();
			nCount1 = beanList1.get(0).getCnt();
		}
		
		JSONObject jsonObj=new JSONObject();
		jsonObj.put("data", nCount);
		jsonObj.put("data1", nCount1);
		jArray.add(jsonObj);

		return jArray;
	}
	
	@RequestMapping(value="findAbnormalDeptDetailChart5.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDeptDetailChart5(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "dept_id", defaultValue = "") String dept_id,
			@RequestParam(value = "search_from", required = false) String search_from,
			@RequestParam(value = "search_to", required = false) String search_to) throws Exception {	JSONArray jArray = new JSONArray();
		
		ExtrtCondbyInq param = new ExtrtCondbyInq();
		param.setDept_id(dept_id);
		param.setSearch_from(search_from);
		param.setSearch_to(search_to);
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	param.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	param.setAuth_idsList(list);
	    }
			
		List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalDeptDetailChart5_1(param);
		List<ExtrtCondbyInq> beanList1 = extrtCondbyInqSvc.findAbnormalDeptDetailChart5(param);

		long nCount = 0;
		long nCount1 = 0;
		if ( beanList != null ) {
			nCount = beanList.get(0).getCnt();
			nCount1 = beanList1.get(0).getCnt();
		}
		
		JSONObject jsonObj=new JSONObject();
		jsonObj.put("data", nCount);
		jsonObj.put("data1", nCount1);
		jArray.add(jsonObj);

		return jArray;
	}
	
	@RequestMapping(value="findAbnormalDeptDetailChart20.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDeptDetailChart20(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "dept_id", defaultValue = "") String dept_id,
			@RequestParam(value = "search_from", required = false) String search_from,
			@RequestParam(value = "search_to", required = false) String search_to) throws Exception {	JSONArray jArray = new JSONArray();
		
		ExtrtCondbyInq param = new ExtrtCondbyInq();
		param.setDept_id(dept_id);
		param.setSearch_from(search_from);
		param.setSearch_to(search_to);
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	param.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	param.setAuth_idsList(list);
	    }
			
		List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalDeptDetailChart20_1(param);
		List<ExtrtCondbyInq> beanList1 = extrtCondbyInqSvc.findAbnormalDeptDetailChart20(param);

		long nCount = 0;
		long nCount1 = 0;
		if ( beanList != null ) {
			nCount = beanList.get(0).getCnt();
			nCount1 = beanList1.get(0).getCnt();
		}
		
		JSONObject jsonObj=new JSONObject();
		jsonObj.put("data", nCount);
		jsonObj.put("data1", nCount1);
		jArray.add(jsonObj);

		return jArray;
	}
	
	@RequestMapping(value="findAbnormalDeptDetailChart6.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDeptDetailChart6(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "dept_id", defaultValue = "") String dept_id) throws Exception {
		
		JSONArray jArray = new JSONArray();

		HashMap<String,Long> mapTemp = new HashMap<String,Long>();
		Calendar oCalendar = Calendar.getInstance( );
		Calendar lastCal = Calendar.getInstance();

		int prevMonth = lastCal.get(Calendar.MONTH);
		lastCal.add(Calendar.MONTH, -1);
		int lastDayPrevMonth = lastCal.getActualMaximum(lastCal.DAY_OF_MONTH);

		for ( int i=1; i<=lastDayPrevMonth; ++i ) {
			String date = Integer.toString(oCalendar.get(Calendar.YEAR))+String.format("%2d", (oCalendar.get(Calendar.MONTH)));
			String s = String.format("%02d", i);
			date=date+s;
			date=date.replaceAll(" ", "0");
			mapTemp.put(date, (long) 0);
		}
		
		SearchSearch search = new SearchSearch();
		search.setDept_id(dept_id);
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
		
		List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalDeptDetailChart6(search);
		
		for ( int i=0; i<beanList.size(); ++i ) {
			ExtrtCondbyInq tmp = beanList.get(i);
			mapTemp.put(tmp.getData(), tmp.getCnt());
		}
		
		JSONObject dataProvider=new JSONObject();

		for ( int i=1; i<=lastDayPrevMonth; i++ ) {

			dataProvider=new JSONObject();
			
			String date = Integer.toString(oCalendar.get(Calendar.YEAR))+String.format("%2d", (oCalendar.get(Calendar.MONTH)));
			String s = String.format("%02d", i);
			date=date+s;
			date=date.replaceAll(" ", "0");
			long nCount = mapTemp.get(date);

			String proc_date = prevMonth + "/" + i;

			Calendar cal = Calendar.getInstance() ;
		    cal.set(lastCal.get(Calendar.YEAR), lastCal.get(Calendar.MONTH),i);
			
			int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
			if(dayOfWeek == 1){
				proc_date = proc_date + "(일)";
			}else if(dayOfWeek == 2){
				proc_date = proc_date + "(월)";
			}else if(dayOfWeek == 3){
				proc_date = proc_date + "(화)";
			}else if(dayOfWeek == 4){
				proc_date = proc_date + "(수)";
			}else if(dayOfWeek == 5){
				proc_date = proc_date + "(목)";
			}else if(dayOfWeek == 6){
				proc_date = proc_date + "(금)";
			}else if(dayOfWeek == 7){
				proc_date = proc_date + "(토)";
			}

			dataProvider.put("proc_date", proc_date);
			dataProvider.put("value1", nCount);
			jArray.add(dataProvider);
		}
		return jArray;
	}
	
	@RequestMapping(value="findAbnormalDeptDetailChart7.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDeptDetailChart7(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "dept_id", defaultValue = "") String dept_id) throws Exception {
		
		JSONArray jArray = new JSONArray();
		JSONObject dataProvider=new JSONObject();
		
		Calendar cal = Calendar.getInstance( );
		int daysOfMonth = 0;
		for(int i=0; i<3; i++){
			cal.add(Calendar.MONTH, -1);
			daysOfMonth = daysOfMonth + cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		}
		
		SearchSearch search = new SearchSearch();
		search.setDept_id(dept_id);
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }

		List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalDeptDetailChart7(search);
		List<ExtrtCondbyInq> beanList2 = extrtCondbyInqSvc.findAbnormalDeptDetailChart7_1(search);
		
		for ( int i=0; i<beanList.size(); ++i ) {
			ExtrtCondbyInq tmp = beanList.get(i);
			ExtrtCondbyInq tmp2 = beanList2.get(i);
	
			dataProvider=new JSONObject();

			dataProvider.put("data1", tmp.getData());
			dataProvider.put("cnt1", tmp.getCnt()/daysOfMonth);
			dataProvider.put("cnt2", tmp2.getCnt());
			jArray.add(dataProvider);
		}
		
		return jArray;
	}
	
	@RequestMapping(value="findAbnormalDeptDetailChart8.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDeptDetailChart8(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "dept_id", defaultValue = "") String dept_id) throws Exception {
			
			JSONArray jArray = new JSONArray();
			JSONObject dataProvider=new JSONObject();
			
			String data[] = {"M-6", "M-5", "M-4", "M-3", "M-2", "M-1", "M"};
			
			Calendar cal = Calendar.getInstance( );
			cal.add(Calendar.MONTH, 1);
			int mm = cal.get(Calendar.MONTH);
			
			for(int i=6; i>=0; i--){
				if(mm==0) mm=12;

				data[i] = Integer.toString(mm)+"월";
				mm--;
			}
			
			SearchSearch search = new SearchSearch();
			search.setDept_id(dept_id);
			
			List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
		    if(list != null) {
		    	search.setAuth_idsList(list);
		    }else {
		    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
		    	search.setAuth_idsList(list);
		    }

			List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalDeptDetailChart8(search);
			
			for ( int i=0; i<beanList.size(); ++i ) {
				ExtrtCondbyInq tmp = beanList.get(i);
				dataProvider=new JSONObject();
				dataProvider.put("date", data[i]);
				dataProvider.put("cnt", tmp.getCnt());
				jArray.add(dataProvider);
			}
			
			return jArray;
	}
	
	@RequestMapping(value="findAbnormalDeptDetailChart9.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDeptDetailChart9(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "dept_id", defaultValue = "") String dept_id) throws Exception {
			
			JSONArray jArray = new JSONArray();
			JSONObject dataProvider=new JSONObject();

			long sunCnt=0, monCnt=0, tueCnt=0, wedCnt=0, thuCnt=0, friCnt=0, satCnt=0;
			
			for(int i=3; i>0; i--){
				Calendar lastCal = Calendar.getInstance();
				lastCal.add(Calendar.MONTH, -i);
				int lastDayPrevMonth = lastCal.getActualMaximum(lastCal.DAY_OF_MONTH);
	
				for ( int j=1; j<=lastDayPrevMonth; j++ ) {
	
					Calendar cal = Calendar.getInstance() ;
				    cal.set(lastCal.get(Calendar.YEAR), lastCal.get(Calendar.MONTH),j);
					
					int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
					if(dayOfWeek == 1){
						sunCnt++;
					}else if(dayOfWeek == 2){
						monCnt++;
					}else if(dayOfWeek == 3){
						tueCnt++;
					}else if(dayOfWeek == 4){
						wedCnt++;
					}else if(dayOfWeek == 5){
						thuCnt++;
					}else if(dayOfWeek == 6){
						friCnt++;
					}else if(dayOfWeek == 7){
						satCnt++;
					}
				}
			}
			String data[] = {"일", "월", "화", "수", "목", "금", "토"};
			
			SearchSearch search = new SearchSearch();
			search.setDept_id(dept_id);
			
			List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
		    if(list != null) {
		    	search.setAuth_idsList(list);
		    }else {
		    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
		    	search.setAuth_idsList(list);
		    }

			List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalDeptDetailChart9(search);
			List<ExtrtCondbyInq> beanList2 = extrtCondbyInqSvc.findAbnormalDeptDetailChart9_1(search);

		    long sun=0, mon=0, tue=0, wed=0, thu=0, fri=0, sat=0;

		    for ( int i=0; i<beanList2.size(); ++i ) {
				ExtrtCondbyInq tmp = beanList2.get(i);
				String dt = tmp.getData();
				int yyyy = Integer.parseInt(dt.substring(0,4));
				int mm = Integer.parseInt(dt.substring(4,6))-1;
				int dd = Integer.parseInt(dt.substring(6));
				
				Calendar cal = Calendar.getInstance();
			    cal.set(yyyy, mm, dd);
			    
				int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
				if(dayOfWeek == 1){
					sun = sun + tmp.getCnt();
				}else if(dayOfWeek == 2){
					mon = mon + tmp.getCnt();
				}else if(dayOfWeek == 3){
					tue = tue + tmp.getCnt();
				}else if(dayOfWeek == 4){
					wed = wed + tmp.getCnt();
				}else if(dayOfWeek == 5){
					thu = thu + tmp.getCnt();
				}else if(dayOfWeek == 6){
					fri = fri + tmp.getCnt();
				}else if(dayOfWeek == 7){
					sat = sat + tmp.getCnt();
				}
			}
			long cnt[] = {sun/sunCnt, mon/monCnt, tue/tueCnt, wed/wedCnt, thu/thuCnt, fri/friCnt, sat/satCnt};
			
			for ( int i=0; i<beanList.size(); ++i ) {
				ExtrtCondbyInq tmp = beanList.get(i);
				dataProvider=new JSONObject();
				dataProvider.put("date", data[i]);
				dataProvider.put("cnt1", tmp.getCnt());
				dataProvider.put("cnt2", cnt[i]);
				jArray.add(dataProvider);
			}
			
			return jArray;
	}
	
	@RequestMapping(value="findAbnormalDeptDetailChart10.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDeptDetailChart10(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "dept_id", defaultValue = "") String dept_id) throws Exception {
		
		JSONArray jArray = new JSONArray();
		JSONObject dataProvider=new JSONObject();
		
		Calendar cal = Calendar.getInstance( );
		int daysOfMonth = 0;
		for(int i=0; i<3; i++){
			cal.add(Calendar.MONTH, -1);
			daysOfMonth = daysOfMonth + cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		}
		String data[]=new String[24];
		
		for(int i=0; i<data.length; ++i){
			data[i] = String.format("%02d", i);
		}
		
		SearchSearch search = new SearchSearch();
		search.setDept_id(dept_id);
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }

		List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalDeptDetailChart10(search);
		List<ExtrtCondbyInq> beanList2 = extrtCondbyInqSvc.findAbnormalDeptDetailChart10_1(search);
		
		for ( int i=0; i<data.length; ++i ) {
			dataProvider=new JSONObject();
			long cnt1=0;
			long cnt2=0;
			for(int j=0; j<beanList.size(); j++){
				ExtrtCondbyInq tmp = beanList.get(j);
				if(tmp.getProc_time().equals(data[i])){
					cnt1 = tmp.getCnt();
					break;
				}
			}
			
			for(int j=0; j<beanList2.size(); j++){
				ExtrtCondbyInq tmp2 = beanList2.get(j);
				if(tmp2.getProc_time().equals(data[i])){
					cnt2 = tmp2.getCnt()/daysOfMonth;
					break;
				}
			}
			dataProvider.put("date", data[i]);
			dataProvider.put("cnt1", cnt1);
			dataProvider.put("cnt2", cnt2);
			jArray.add(dataProvider);
		}
		return jArray;
	}
	
	@RequestMapping(value="findAbnormalDeptDetailChart10_noAverage.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDeptDetailChart10_noAverage(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "dept_id", defaultValue = "") String dept_id) throws Exception {
		
		JSONArray jArray = new JSONArray();
		JSONObject dataProvider=new JSONObject();
		
		Calendar cal = Calendar.getInstance( );
		int daysOfMonth = 0;
		for(int i=0; i<3; i++){
			cal.add(Calendar.MONTH, -1);
			daysOfMonth = daysOfMonth + cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		}
		String data[]=new String[24];
		
		for(int i=0; i<data.length; ++i){
			data[i] = String.format("%02d", i);
		}


		SearchSearch search = new SearchSearch();
		search.setDept_id(dept_id);
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
	    
		List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalDeptDetailChart10(search);
		
		for ( int i=0; i<data.length; ++i ) {
			dataProvider=new JSONObject();
			long cnt1=0;
			for(int j=0; j<beanList.size(); j++){
				ExtrtCondbyInq tmp = beanList.get(j);
				if(tmp.getProc_time().equals(data[i])){
					cnt1 = tmp.getCnt();
					break;
				}
			}
			
			dataProvider.put("date", data[i]);
			dataProvider.put("cnt1", cnt1);
			jArray.add(dataProvider);
		}
		return jArray;
	}
	
	@RequestMapping(value="findAbnormalDeptDetailChart11.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDeptDetailChart11(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "dept_id", defaultValue = "") String dept_id,
			@RequestParam(value = "search_from", required = false) String search_from,
			@RequestParam(value = "search_to", required = false) String search_to) throws Exception {

		JSONArray jArray = new JSONArray();
		JSONObject dataProvider=new JSONObject();		
		ExtrtCondbyInq eci = new ExtrtCondbyInq();
		eci.setDept_id(dept_id);	
		
		Calendar oCalendar = Calendar.getInstance( );
		String date = Integer.toString(oCalendar.get(Calendar.YEAR))+String.format("%02d", (oCalendar.get(Calendar.MONTH))+1)+String.format("%02d", (oCalendar.get(Calendar.DATE)));

		if(search_from == null){
			eci.setSearch_from(date);
			eci.setSearch_to(date);
		} else {
			eci.setSearch_from(search_from.replaceAll("-", ""));
			eci.setSearch_to(search_to.replaceAll("-", ""));
		}
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	eci.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	eci.setAuth_idsList(list);
	    }
	    
	    Statistics beanList = extrtCondbyInqSvc.findAbnormalDeptDetailChart11(eci);
		List<Code> codeList = extrtCondbyInqDao.resultTypeList_chart();
		for ( int i=0; i<codeList.size(); ++i ) {
			Code code = codeList.get(i);
			String code_id = code.getCode_id();
			dataProvider=new JSONObject();
			dataProvider.put("data", code.getCode_name());
			int cnt = GetStatisticsTypeData.getType(beanList, "type"+code.getResult_type_order());
			dataProvider.put("cnt", cnt);
			jArray.add(dataProvider);
		}

		return jArray;
	}
	
	@RequestMapping(value="findAbnormalDeptDetailChart12.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDeptDetailChart12(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "dept_id", defaultValue = "") String dept_id,
			@RequestParam(value = "search_from", required = false) String search_from,
			@RequestParam(value = "search_to", required = false) String search_to) throws Exception {

		JSONArray jArray = new JSONArray();
		JSONObject dataProvider=new JSONObject();		
		ExtrtCondbyInq eci = new ExtrtCondbyInq();
		eci.setDept_id(dept_id);	
		
		Calendar oCalendar = Calendar.getInstance( );
		String date = Integer.toString(oCalendar.get(Calendar.YEAR))+String.format("%02d", (oCalendar.get(Calendar.MONTH))+1)+String.format("%02d", (oCalendar.get(Calendar.DATE)));

		if(search_from == null){
			eci.setSearch_from(date);
			eci.setSearch_to(date);
		} else {
			eci.setSearch_from(search_from.replaceAll("-", ""));
			eci.setSearch_to(search_to.replaceAll("-", ""));
		}
		
		SearchSearch search = new SearchSearch();
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	eci.setAuth_idsList(list);
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	eci.setAuth_idsList(list);
	    	search.setAuth_idsList(list);
	    }
	    
	    
	    List<SystemMaster> SystemMasterList = indvinfoTypeSetupDao.getSystemMasterListByAuth(search);
		List<Statistics> beanList = extrtCondbyInqSvc.findAbnormalDeptDetailChart12(eci);
		List<ExtrtCondbyInq> beanList2 = extrtCondbyInqSvc.findAbnormalDeptDetailChart12_1(eci);
		for(int k=0; k<SystemMasterList.size();k++) {
			String system_name = SystemMasterList.get(k).getSystem_name();
			dataProvider=new JSONObject();
			dataProvider.put("system_name", system_name);
			for(int i=0; i<beanList.size(); i++) {
				Statistics st = beanList.get(i);
				if(system_name.equals(st.getSystem_name())) {
					dataProvider.put("cnt1", st.getType1());
					dataProvider.put("cnt2", st.getType2());
					dataProvider.put("cnt3", st.getType3());
					dataProvider.put("cnt10", st.getType10());
				}
			}
			for(int j=0;j<beanList2.size();j++) {
				ExtrtCondbyInq ex = beanList2.get(j);
				if(system_name.equals(ex.getSystem_name())) {
					dataProvider.put("cnt", ex.getCnt());
				}
			}
			jArray.add(dataProvider);
		}
	    
	    
	    
	    
		return jArray;
	}
	
	@RequestMapping(value="findAbnormalDeptDetailChart13.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDeptDetailChart13(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "dept_id", defaultValue = "") String dept_id,
			@RequestParam(value = "search_from", required = false) String search_from,
			@RequestParam(value = "search_to", required = false) String search_to) throws Exception {

		JSONArray jArray = new JSONArray();
		JSONObject dataProvider=new JSONObject();		
		ExtrtCondbyInq eci = new ExtrtCondbyInq();
		eci.setDept_id(dept_id);	
		
		Calendar oCalendar = Calendar.getInstance( );
		String date = Integer.toString(oCalendar.get(Calendar.YEAR))+String.format("%02d", (oCalendar.get(Calendar.MONTH))+1)+String.format("%02d", (oCalendar.get(Calendar.DATE)));

		if(search_from == null){
			eci.setSearch_from(date);
			eci.setSearch_to(date);
		} else {
			eci.setSearch_from(search_from.replaceAll("-", ""));
			eci.setSearch_to(search_to.replaceAll("-", ""));
		}
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	eci.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	eci.setAuth_idsList(list);
	    }

		List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalDeptDetailChart13(eci);
		
		for ( int i=0; i<beanList.size(); ++i ) {
			ExtrtCondbyInq tmp = beanList.get(i);
	
			dataProvider=new JSONObject();

			dataProvider.put("data", tmp.getData());
			dataProvider.put("cnt", tmp.getCnt());
			jArray.add(dataProvider);
		}
		
		return jArray;
	}
	
	@RequestMapping(value="findAbnormalDeptDetailChart14.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDeptDetailChart14(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "dept_id", defaultValue = "") String dept_id,
			@RequestParam(value = "search_from", required = false) String search_from,
			@RequestParam(value = "search_to", required = false) String search_to) throws Exception {

		JSONArray jArray = new JSONArray();
		JSONObject dataProvider=new JSONObject();		
		ExtrtCondbyInq eci = new ExtrtCondbyInq();
		eci.setDept_id(dept_id);	
		
		Calendar oCalendar = Calendar.getInstance( );
		String date = Integer.toString(oCalendar.get(Calendar.YEAR))+String.format("%02d", (oCalendar.get(Calendar.MONTH))+1)+String.format("%02d", (oCalendar.get(Calendar.DATE)));

		if(search_from == null){
			eci.setSearch_from(date);
			eci.setSearch_to(date);
		} else {
			eci.setSearch_from(search_from.replaceAll("-", ""));
			eci.setSearch_to(search_to.replaceAll("-", ""));
		}
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	eci.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	eci.setAuth_idsList(list);
	    }

		List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalDeptDetailChart14(eci);
		
		for ( int i=0; i<beanList.size(); ++i ) {
			ExtrtCondbyInq tmp = beanList.get(i);
	
			dataProvider=new JSONObject();

			dataProvider.put("data", tmp.getData());
			dataProvider.put("cnt", tmp.getCnt());
			jArray.add(dataProvider);
		}
		
		return jArray;
	}
	
	@RequestMapping(value="findAbnormalDetailChart20.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findAbnormalDetailChart20(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "emp_user_name", defaultValue = "") String emp_user_name,
			@RequestParam(value = "dept_id", defaultValue = "") String dept_id,
			@RequestParam(value = "search_from", defaultValue = "") String search_from,
			@RequestParam(value = "search_to", defaultValue = "") String search_to) throws Exception {	
		
		JSONArray jArray = new JSONArray();
		
		ExtrtCondbyInq param = new ExtrtCondbyInq();
		param.setEmp_user_id(emp_user_name);
		param.setSearch_from(search_from);
		param.setSearch_to(search_to);
		param.setDept_id(dept_id);
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	param.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	param.setAuth_idsList(list);
	    }
		
		List<ExtrtCondbyInq> beanList = extrtCondbyInqSvc.findAbnormalDetailChart20(param);
		List<ExtrtCondbyInq> beanList2 = extrtCondbyInqSvc.findAbnormalDetailChart20_2(param);
		if ( beanList == null ) {
			JSONObject jsonObj=new JSONObject();
			jsonObj.put("data1", 0);
			jsonObj.put("data2", 0);
			jArray.add(jsonObj);
		} else {
			for ( int i=0; i<beanList.size(); ++i ) {
				ExtrtCondbyInq temp = beanList.get(i);
				ExtrtCondbyInq temp1 = beanList2.get(i);
				
				if ( temp1 != null && temp !=null ) {
					JSONObject jsonObj=new JSONObject();
					jsonObj.put("data1", temp.getCnt());
					jsonObj.put("data2", temp1.getCnt());
					jArray.add(jsonObj);
				}
				else {
					JSONObject jsonObj=new JSONObject();
					jsonObj.put("data1", 0);
					jsonObj.put("data2", 0);
					jArray.add(jsonObj);
				}
			}

		}
		return jArray;
	}
	
	@RequestMapping(value="getResultType.html", method={RequestMethod.POST})
	public DataModelAndView getResultType(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "log_seq") String log_seq,
			@RequestParam(value = "proc_date") String proc_date,
			@RequestParam(value = "system_seq") String system_seq,
			@RequestParam(value = "log_delimiter") String log_delimiter) throws Exception {

		SearchSearch search = new SearchSearch();
		search.setDetailLogSeq(log_seq);
		search.setDetailProcDate(proc_date);
		search.setSystem_seq_temp(system_seq);
		search.setLog_delimiter(log_delimiter);
		DataModelAndView modelAndView = extrtCondbyInqSvc.getResultType(search);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	} 
	
	@RequestMapping(value="setCheckExtrtDetail.html",method={RequestMethod.POST})
	public DataModelAndView setCheckExtrtDetail(@RequestParam Map<String, String> parameters){
		
		DataModelAndView modelAndView = extrtCondbyInqSvc.setCheckExtrtDetail(parameters);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	@RequestMapping(value="fileDownload.html",method={RequestMethod.POST})
	public DataModelAndView fileDownload(@RequestParam Map<String, String> parameters){
		
		DataModelAndView modelAndView = new DataModelAndView();
		String path = "c:\\upload\\" + parameters.get("fileName");
//		String path = "/usr/local/PSM/filedownload/" + parameters.get("fileName");
		File file = new File(path);
		modelAndView.addObject("downloadFile", file);
		modelAndView.addObject("orginName", parameters.get("orginName"));
		
		modelAndView.setViewName(CommonResource.FILE_DOWNLOAD_VIEW);
		return modelAndView;
	}
	
	/** 180314 sysong 소명판정 판정자 위임 **/
	@MngtActHist(log_action = "SELECT", log_message = "[소명관리] 소명판정 판정자 위임")
	@RequestMapping(value="summmonDelegation.html",method={RequestMethod.POST})
	public DataModelAndView findSummonDelegation(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request){
		
		if ( search.getSearch_from() != null && search.getSearch_from().length() == 2 ) {
			Calendar oCalendar = Calendar.getInstance( ); 
			String date = Integer.toString(oCalendar.get(Calendar.YEAR))+String.format("%2d", (oCalendar.get(Calendar.MONTH) + 1));
			date=date+search.getSearch_from();
			date=date.replaceAll(" ", "0");
			search.setSearch_from(date);
			search.setSearch_to(date);
		} else {
			Date today = new Date();
			//search.initDatesIfEmpty_2week(today, today);
			search.initDatesWithDefaultIfEmpty(0);
		}
		
		AdminUser adminUser = SystemHelper.getAdminUserInfo(request);
		String auth_id = adminUser.getAuth_id();
		String admin_user_id = adminUser.getAdmin_user_id();
		
		String auth_ids = extrtCondbyInqDao.auth_ids(adminUser);
		
		// 사용자(팀장)일 경우 해당 부서만 보여준다.
		if(auth_id.equals("AUTH00004")) {
			search.setDept_id(adminUser.getDept_id());
			search.setAuth_ids(auth_ids);
		}
		
		DataModelAndView modelAndView =  extrtCondbyInqSvc.findSummonDelegation(search);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName("summmonDelegation");
	
		return modelAndView;
	}
	
	/** 20180314 sysong 소명판정 판정자 위임 판정자 추가 **/
	
	@MngtActHist(log_action = "UPDATE", log_message = "[소명관리] 판정자 추가")
	@RequestMapping(value="summonUserAdd.html" ,method={RequestMethod.POST})
	@ResponseBody
	public int summonUserAdd(@RequestParam Map<String, String> parameters, 
			HttpServletRequest request) throws Exception {
		
		int result = 0; 
		
		String userId = parameters.get("user_Id");
		String auth_ids = parameters.get("auth_ids");
		String search_to = parameters.get("search_to");
		String search_from = parameters.get("search_from");
		
		// 위임자 정보
		AdminUser adminUser = new AdminUser();
		adminUser.setAdmin_user_id(userId); 
		
		Map<String, String> userInfo = extrtCondbyInqDao.findUserAdmin(adminUser);
		
		String pw = userInfo.get("emp_user_login_pw");
		String id = userInfo.get("emp_user_id");
		String dept_id = userInfo.get("dept_id");
		String user_name = userInfo.get("emp_user_name");
		
		Calendar calendar = Calendar.getInstance();
		Date last_pwd_change_datetime = calendar.getTime();
		
		adminUser.setPassword(pw);
		adminUser.setAdmin_user_id(id);
		adminUser.setDept_id(dept_id);
		adminUser.setAdmin_user_name(user_name);
		adminUser.setLast_pwd_change_datetime(last_pwd_change_datetime);
		adminUser.setAuth_ids(auth_ids);
		
		// 판정자 정보
		AdminUser loginUser = SystemHelper.getAdminUserInfo(request);
		
		Delegation delegation = new Delegation();
		
		delegation.setDelegation_date(search_from + " ~ " + search_to);
		
		search_to = search_to.replaceAll("-", "");
		search_from = search_from.replaceAll("-", "");
		
		delegation.setApproval_id(loginUser.getAdmin_user_id());
		delegation.setApproval_name(loginUser.getAdmin_user_name());
		delegation.setDelegation_id(adminUser.getAdmin_user_id());
		delegation.setDelegation_name(adminUser.getAdmin_user_name());
		delegation.setDelegation_date_to(search_to);
		delegation.setDelegation_date_from(search_from);
		delegation.setDept_id(loginUser.getDept_id());
		
		int res = extrtCondbyInqDao.checkUserAdmin(adminUser);
		
		if (res == 1) {
			extrtCondbyInqDao.updateDelegate(delegation);
			result = 2;
		} else if (res <= 0) {
			extrtCondbyInqDao.addUserAdmin(adminUser);
			extrtCondbyInqDao.addDelegate(delegation);
			result = 1;
		}
		 
 		return result;
	}
	
	/** 20180314 sysong 소명판정 판정자 위임 판정자 삭제 **/
	@RequestMapping(value="summonUserRemove.html", method={RequestMethod.POST})
	@ResponseBody
	public DataModelAndView summonUserRemove(@RequestParam Map<String, String> parameters, 
			HttpServletRequest request) throws Exception {
		
		DataModelAndView modelAndView = extrtCondbyInqSvc.summonUserRemove(parameters);
		
		return modelAndView; 
	}
	
	/** 20180314 sysong 월별 소명 현황 **/
	@MngtActHist(log_action = "SELECT", log_message = "[소명관리] 월별 소명 현황")
	@RequestMapping(value={"summonPerby.html"}, method={RequestMethod.POST})
	public DataModelAndView findsummonPerby(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = extrtCondbyInqSvc.findsummonPerby(search);
		
		modelAndView.setViewName("summonPerby");
		modelAndView.addObject("adminUsers", commonService.getAdminUsers());
		return modelAndView;
	}
	
	/** 20180314 sysong 부서별 소명 현황 **/
	@MngtActHist(log_action = "SELECT", log_message = "[소명관리] 소속별 소명 현황")
	@RequestMapping(value={"summonDeptby.html"}, method={RequestMethod.POST})
	public DataModelAndView findsummonDeptby(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {

		DataModelAndView modelAndView = extrtCondbyInqSvc.findsummonDeptby(search);
		
		modelAndView.setViewName("summonDeptby");
		modelAndView.addObject("adminUsers", commonService.getAdminUsers());
		return modelAndView;
	}
	
	/** 20180314 sysong 개인별 소명 현황 **/
	@MngtActHist(log_action = "SELECT", log_message = "[소명관리] 개인별 소명 현황")
	@RequestMapping(value={"summonIndvby.html"}, method={RequestMethod.POST})
	public DataModelAndView findsummonIndvby(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {

		DataModelAndView modelAndView = extrtCondbyInqSvc.findsummonIndvby(search);
		
		modelAndView.setViewName("summonIndvby");
		modelAndView.addObject("adminUsers", commonService.getAdminUsers());
		return modelAndView;
	}
	
	/** 20180314 sysong 소명요청 및 판정 현황 **/
	@MngtActHist(log_action = "SELECT", log_message = "[소명관리] 소명요청 및 판정 현황")
	@RequestMapping(value={"summonProcessby.html"}, method={RequestMethod.POST})
	public DataModelAndView findsummonProcessby(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		search.setSummon_cfm_yn((String)request.getSession().getAttribute("summon_cfm_yn"));
		
		DataModelAndView modelAndView = extrtCondbyInqSvc.findsummonProcessby(search);
		
		modelAndView.setViewName("summonProcessby");
		modelAndView.addObject("adminUsers", commonService.getAdminUsers());
		return modelAndView;
	}
	
	@RequestMapping(value={"findScenarioByScenSeq.html"}, method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findScenarioByScenSeq(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		JSONArray array = new JSONArray();
		
		List<ExtrtCondbyInq> list = extrtCondbyInqSvc.findScenarioByScenSeq(search);
		for(ExtrtCondbyInq data : list) {
			JSONObject object = new JSONObject();
			object.put("rule_cd", data.getRule_cd());
			object.put("rule_nm", data.getRule_nm());
			array.add(object);
		}
		
		return array;
	}
	
	@RequestMapping(value={"saveFollowUp.html"}, method={RequestMethod.POST})
	public DataModelAndView saveFollowUp(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = extrtCondbyInqSvc.saveFollowUp(parameters); 
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	@RequestMapping(value={"findSummonUpdate.html"}, method={RequestMethod.POST,RequestMethod.GET})
	public DataModelAndView findSummonUpdate(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = extrtCondbyInqSvc.findSummonUpdate(search);
		try {
			String expect_date = new String(parameters.get("expect_date").getBytes("iso-8859-1"), "utf-8");
			
			parameters.put("expect_date", expect_date);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("findSummonUpdate");

		return modelAndView;
	}
	
	@RequestMapping(value={"saveAbuseInfo.html"}, method={RequestMethod.POST})
	public DataModelAndView saveAbuseInfo(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = extrtCondbyInqSvc.saveAbuseInfo(parameters); 
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	@MngtActHist(log_action = "SELECT", log_message = "[소명관리] 부적정판정자처리")
	@RequestMapping(value={"abuseInfoList.html"}, method={RequestMethod.POST})
	public DataModelAndView findAbuseInfoList(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {

		if(parameters.get("search_from_rp") != null) {search.setSearch_from(parameters.get("search_from_rp"));}
		if(parameters.get("search_to_rp") != null) {search.setSearch_to(parameters.get("search_to_rp"));}
		if(parameters.get("dept_name_rp") != null) {search.setDept_name(parameters.get("dept_name_rp"));}
		if(parameters.get("emp_user_id_rp") != null) {search.setEmp_user_id(parameters.get("emp_user_id_rp"));}
		if(parameters.get("scen_seq_rp") != null) {search.setScen_seq(parameters.get("scen_seq_rp"));}
		
		if ( search.getSearch_from() != null && search.getSearch_from().length() == 2 ) {
			Calendar oCalendar = Calendar.getInstance( ); 
			String date = Integer.toString(oCalendar.get(Calendar.YEAR))+String.format("%2d", (oCalendar.get(Calendar.MONTH) + 1));
			date=date+search.getSearch_from();
			date=date.replaceAll(" ", "0");
			search.setSearch_from(date);
			search.setSearch_to(date);
		} else {
			Date today = new Date();
			//search.initDatesIfEmpty_2week(today, today);
			//search.initDatesWithDefaultIfEmpty(0);
			// 비정상위험분석에서 상단에 있는 카운팅 링크
			try {
				if(parameters.get("isWeekend").equals("YES")) {
					search.initDatesIfEmpty_week(today, today);
				}else {
					search.initDatesWithDefaultIfEmpty(0);
				}
			} catch (Exception e) {
				search.initDatesWithDefaultIfEmpty(0);
				//e.printStackTrace();
			}
		}
		
		/*if(search.getEmp_user_id() != null) {
			String id = search.getEmp_user_id();
			String upperId = id.toUpperCase();
			search.setEmp_user_id(upperId);
		}*/
		HttpSession session = request.getSession();
		
		String use_systemSeq = (String) session.getAttribute("use_systemSeq");
		
		if (use_systemSeq.equals("Y")) {
			List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
		    if(list != null) {
		    	search.setAuth_idsList(list);
		    }else {
		    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
		    	search.setAuth_idsList(list);
		    }
		}
	    
		DataModelAndView modelAndView = extrtCondbyInqSvc.findAbuseInfoList(search);
		AdminUser adminUser = SystemHelper.getAdminUserInfo(request);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("adminUser", adminUser);
		modelAndView.addObject("search", search);
		modelAndView.addObject("use_systemSeq", use_systemSeq);
		modelAndView.setViewName("abuseInfoList");
		return modelAndView;
	}
	
	/**
	 * 추출조건별조회상세Detail
	 */
	@RequestMapping(value={"abuseInfoDetail.html"}, method={RequestMethod.POST})
	public DataModelAndView findAbuseInfoDetail(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = extrtCondbyInqSvc.findAbuseInfoDetail(search);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("abuseInfoDetail");

		return modelAndView;
	}
	
	/**
	 * 비정상위험분석 시뮬레이션
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[빅데이터분석] 비정상위험분석 시뮬레이션")
	@RequestMapping(value={"ruleSimulate.html"}, method={RequestMethod.POST})
	public DataModelAndView ruleSimulate(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {

		search.initDatesWithDefaultIfEmpty(0);
		DataModelAndView modelAndView = extrtCondbyInqSvc.findRuleList(search);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("ruleSimulate");

		return modelAndView;
	}
	
	/**
	 * 비정상위험분석 시뮬레이션
	 */
	@RequestMapping(value={"findRuleScript.html"}, method={RequestMethod.POST,RequestMethod.GET})
	public DataModelAndView findRuleScript(
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		int rule_seq = Integer.parseInt(parameters.get("rule_seq"));
		DataModelAndView modelAndView = extrtCondbyInqSvc.findRuleScript(rule_seq);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	/**
	 * 비정상위험분석 시뮬레이션
	 */
	@RequestMapping(value={"runScript.html"}, method={RequestMethod.POST,RequestMethod.GET})
	public DataModelAndView runScript(
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		ExtrtBaseSetup extrtBaseSetup = CommonHelper.convertMapToBean(parameters, ExtrtBaseSetup.class);
		String search_from = extrtBaseSetup.getSearch_from().replace("-", "");
		extrtBaseSetup.setSearch_from(search_from);
		if(extrtBaseSetup.getIndv_yn().equals("Y")) {
			String system_arr = extrtBaseSetupDao.systemSeqArr();
			extrtBaseSetup.setSystem_seq(system_arr);
		}
		DataModelAndView modelAndView = null;
		try {
			extrtCondbyInqSvc.truncateExtractTemp();
			String firstSql = ExtractSQLCreate_lib.sqlCreator(extrtBaseSetup);
			firstSql = firstSql.replace("system_seq::integer","system_seq");
			
			String secondSql = ExtractSQLCreate_lib.empDetailsqlCreator(extrtBaseSetup);
			ExtrtCondbyInq sql = new ExtrtCondbyInq();
			sql.setSql(firstSql);
			extrtCondbyInqSvc.executeSql(sql);
			sql.setSql(secondSql);
			extrtCondbyInqSvc.executeSql(sql);
			modelAndView = extrtCondbyInqSvc.findBizLogExtractTemp();
		} catch (Exception e) {
			e.printStackTrace();
			throw new ESException("SYS601J");
		}
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	/**
	 * 소명관리 List
	 */
	@RequestMapping(value={"centerSummonList.html"}, method={RequestMethod.POST})
	public DataModelAndView centerSummonList(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		if ( search.getSearch_from() != null && search.getSearch_from().length() == 2 ) {
			Calendar oCalendar = Calendar.getInstance();
			String date = Integer.toString(oCalendar.get(Calendar.YEAR))+String.format("%2d", (oCalendar.get(Calendar.MONTH) + 1));
			date=date+search.getSearch_from();
			date=date.replaceAll(" ", "0");
			search.setSearch_from(date);
			search.setSearch_to(date);
		} else {
			Date today = new Date();
			search.initDatesIfEmpty_2week(today, today);
			//search.initDatesWithDefaultIfEmpty(0);
		}
		
		/*if(search.getEmp_user_id() != null) {
			String id = search.getEmp_user_id();
			String upperId = id.toUpperCase();
			search.setEmp_user_id(upperId);
		}*/
		
		DataModelAndView modelAndView = extrtCondbyInqSvc.findCenterSummonList(search);
		AdminUser adminUser = SystemHelper.getAdminUserInfo(request);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("adminUser", adminUser);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("centerSummonList");
		modelAndView.addObject("adminUsers", commonService.getAdminUsers());
		return modelAndView;
	}
	
	/** 20180314 sysong 소명판정 판정자 위임 판정자 추가 **/
	@RequestMapping(value="findCenterDescApprInfo.html" ,method={RequestMethod.POST})
	@ResponseBody
	public String findCenterDescApprInfo(@RequestParam Map<String, String> parameters, 
			HttpServletRequest request) throws Exception {
		
		long desc_seq = Long.parseLong(parameters.get("desc_seq"));
		String admin_user_id = SystemHelper.getAdminUserIdFromRequest(request);
		
		String returnUrl = extrtCondbyInqSvc.findCenterApprReturnUrl(desc_seq,admin_user_id);
		
 		return returnUrl;
	}
	
	@ResponseBody
	@RequestMapping(value = "summonListAdd.html", method = {RequestMethod.POST})
	public Map summonListAdd(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		Map<String,String> resultMap = extrtCondbyInqSvc.summonListAdd(parameters,request);
		
		return resultMap;
	}
	
	@RequestMapping(value="summonDelegatePersonal.html" ,method={RequestMethod.POST})
	public DataModelAndView summonDelegatePersonal(@RequestParam Map<String, String> parameters, 
			HttpServletRequest request) throws Exception {
		String admin_user_id = SystemHelper.getAdminUserIdFromRequest(request);
		DataModelAndView modelAndView = extrtCondbyInqSvc.summonDelegatePersonal(parameters);
		modelAndView.setViewName("summonDelegatePersonal");
		return modelAndView;
	}
	
	@RequestMapping(value="summonDelegateDept.html" ,method={RequestMethod.POST})
	public DataModelAndView summonDelegateDept(@RequestParam Map<String, String> parameters, 
			HttpServletRequest request) throws Exception {
		String admin_user_id = SystemHelper.getAdminUserIdFromRequest(request);
		DataModelAndView modelAndView = extrtCondbyInqSvc.summonDelegateDept(parameters);
		modelAndView.setViewName("summonDelegateDept");
		return modelAndView;
	}
	
	@ResponseBody
	@RequestMapping(value = "findEmpUserList.html", method = {RequestMethod.POST})
	public List findEmpUserList(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		return extrtCondbyInqSvc.findEmpUserList(parameters);
	}
	
	@ResponseBody
	@RequestMapping(value = "summonDelegatePersonalAdd.html", method = {RequestMethod.POST})
	public String summonDelegatePersonalAdd(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		return extrtCondbyInqSvc.summonDelegatePersonalAdd(parameters,request);
	}
	
	@ResponseBody
	@RequestMapping(value = "summonDelegatePersonalDelete.html", method = {RequestMethod.POST})
	public String summonDelegatePersonalDelete(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		return extrtCondbyInqSvc.summonDelegatePersonalDelete(parameters,request);
	}	
	
	@ResponseBody
	@RequestMapping(value = "summonDelegateDeptAdd.html", method = {RequestMethod.POST})
	public String summonDelegateDeptAdd(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		return extrtCondbyInqSvc.summonDelegateDeptAdd(parameters,request);
	}
	
	@ResponseBody
	@RequestMapping(value = "summonDelegateDeptDelete.html", method = {RequestMethod.POST})
	public String summonDelegateDeptDelete(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		return extrtCondbyInqSvc.summonDelegateDeptDelete(parameters,request);
	}	
	
	
	@RequestMapping(value="approvalDelegatePersonal.html" ,method={RequestMethod.POST})
	public DataModelAndView approvalDelegatePersonal(@RequestParam Map<String, String> parameters, 
			HttpServletRequest request) throws Exception {
		String admin_user_id = SystemHelper.getAdminUserIdFromRequest(request);
		DataModelAndView modelAndView = extrtCondbyInqSvc.approvalDelegatePersonal(parameters);
		modelAndView.setViewName("approvalDelegatePersonal");
		return modelAndView;
	}	
	
	@ResponseBody
	@RequestMapping(value = "approvalDelegatePersonalAdd.html", method = {RequestMethod.POST})
	public String approvalDelegatePersonalAdd(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		return extrtCondbyInqSvc.approvalDelegatePersonalAdd(parameters,request);
	}
	
	@ResponseBody
	@RequestMapping(value = "approvalDelegatePersonalDelete.html", method = {RequestMethod.POST})
	public String approvalDelegatePersonalDelete(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		return extrtCondbyInqSvc.approvalDelegatePersonalDelete(parameters,request);
	}	
	
	@RequestMapping(value="approvalDelegateDept.html" ,method={RequestMethod.POST})
	public DataModelAndView approvalDelegateDept(@RequestParam Map<String, String> parameters, 
			HttpServletRequest request) throws Exception {
		String admin_user_id = SystemHelper.getAdminUserIdFromRequest(request);
		DataModelAndView modelAndView = extrtCondbyInqSvc.approvalDelegateDept(parameters);
		modelAndView.setViewName("approvalDelegateDept");
		return modelAndView;
	}	
	
	@ResponseBody
	@RequestMapping(value = "approvalDelegateDeptAdd.html", method = {RequestMethod.POST})
	public String approvalDelegateDeptAdd(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		return extrtCondbyInqSvc.approvalDelegateDeptAdd(parameters,request);
	}
	
	@ResponseBody
	@RequestMapping(value = "approvalDelegateDeptDelete.html", method = {RequestMethod.POST})
	public String approvalDelegateDeptDelete(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		return extrtCondbyInqSvc.approvalDelegateDeptDelete(parameters,request);
	}	
	
	//소명응답 - 엑셀 업로드
	@RequestMapping(value="upload.html", method={RequestMethod.POST})
	@ResponseBody
	public String upSummonResponse(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		
		String result = extrtCondbyInqSvc.upLoadSystemMngtOne(parameters,request, response);
		
		/*ModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("result", result);
		modelAndView.setViewName(CommonResource.JSON_VIEW);*/
		
		return result;
	}
	
	//소명응답 httpConnection
	@RequestMapping(value="responseSummon.html", method={RequestMethod.POST})
	@ResponseBody
	public DataModelAndView responseSummon(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		DataModelAndView modelAndView = extrtCondbyInqSvc.addResponseSummonOne(parameters,request);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	@RequestMapping("exdownload.html")
	public ModelAndView download(HttpServletRequest request)throws Exception{
		
		String path = request.getSession().getServletContext().getRealPath("/")+"WEB-INF/";
		 //System.out.println(path);
		File down = new File(path + "excelExam/소명응답_업로드양식.xlsx");
		return new ModelAndView("download","downloadFile",down);
	}
	
	//20201222 hbjang 명지전문대 비정상위험분석 보고서 커스터마이징 UI코드 MJJ
	@RequestMapping(value = "showExtrtReport.html")
	public DataModelAndView showExtrtReport(HttpServletRequest request,
			@RequestParam(value = "emp_detail_seq", required = true) int emp_detail_seq,
			@RequestParam(value = "occr_dt", required = true) String occr_dt) {
		
		DataModelAndView mv = extrtCondbyInqSvc.findExtrtReport(emp_detail_seq, occr_dt);
		
		
		mv.setViewName("extrt_report");
		
		return mv;
	}
	
	//20210312 hbjang 명지전문대 비정상위험분석 엑셀 커스터마이징
	@RequestMapping(value="extrtExcelDownload.html", method={RequestMethod.POST})
	public DataModelAndView extrtExcelDownload(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		extrtCondbyInqSvc.extrtExcelDownload(modelAndView, search, request);
		
		return modelAndView;
	}
}
