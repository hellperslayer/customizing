package com.easycerti.eframe.psm.search.web;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.search.service.OnrLogInqSvc;
import com.easycerti.eframe.psm.search.vo.SearchSearch;

@Controller
@RequestMapping("/onrLogInq/*")
public class OnrLogInqCtrl {
	
	@Autowired OnrLogInqSvc onrLogInqSvc;
	
	@RequestMapping(value={"list.html"}, method={RequestMethod.POST})
	public DataModelAndView findOnrLogInqList(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request,
			@RequestParam(value = "sub_menu_id", defaultValue = "") String sub_menu_id,
			@RequestParam(value = "menutitle",defaultValue="")String menutitle) 
	{
		
		HttpSession session = request.getSession();
		// 개인정보유형 뷰
		String result_type_view = (String) session.getAttribute("result_type");
		// 메뉴명 유무
		String scrn_name_view = (String) session.getAttribute("scrn_name");
		// 접속유형 default 세팅
		String mapping_id = (String) session.getAttribute("mapping_id");
		if(mapping_id.equals("Y")) {
			if ( search.getMapping_id() == null ) search.setMapping_id("code1");
			//if ( search.getMapping_id().equals("codeAll") ) search.setMapping_id("");
			if ( search.getMapping_id().equals("code2") ) search.setMapping_id(null);
		}else {
			if(search.getMapping_id() == null || search.getMapping_id().equals("code2"))
				search.setMapping_id(null);
			else
				search.setMapping_id("code1");
		}
    
	    List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_onnara");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
	    
		Date today = new Date();
		
		search.initDatesIfEmpty(today, today);
		
		DataModelAndView modelAndView = onrLogInqSvc.findOnrLogInqList(search);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("menutitle",menutitle);
		modelAndView.addObject("sub_menu_id", sub_menu_id);
		modelAndView.addObject("search", search);
		modelAndView.addObject("result_type_view", result_type_view);
		modelAndView.addObject("scrn_name_view", scrn_name_view);
		modelAndView.setViewName("onrLogInqList");

		return modelAndView;
	}
	
	@RequestMapping(value={"detail.html"}, method={RequestMethod.POST,RequestMethod.GET})
	public DataModelAndView findOnrLogInqDetail(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		//전체로그조회 상세 리스트 페이징 page_num 세팅
		if(parameters.get("onrLogInqDetail_page_num")!=null){
			search.getAllLogInqDetail().setPage_num(Integer.valueOf(parameters.get("onrLogInqDetail_page_num")));
		}
		
		//추출조건별조회 상세 리스트 페이징 page_num 세팅
		if(parameters.get("extrtCondbyInqDetail_page_num")!=null){
			search.getExtrtCondbyInqDetail().setPage_num(Integer.valueOf(parameters.get("extrtCondbyInqDetail_page_num")));
		}
		
		//위험도별조회 상세 리스트 페이징 page_num 세팅
		if(parameters.get("empDetailInqDetail_page_num")!=null){
			search.getEmpDetailInqDetail().setPage_num(Integer.valueOf(parameters.get("empDetailInqDetail_page_num")));
		}
					
		//List<ExtrtCondbyInqDetailChart> data1 = new ArrayList<>();
		
		
		
		//2017.06.29
		
		try{
			String emp_user_name = parameters.get("emp_user_name");
			if(emp_user_name.isEmpty())
				search.setIsSearch("N");
			else
				search.setIsSearch("Y");
		} catch (Exception e) {
			search.setIsSearch("N");
		}
				
		DataModelAndView modelAndView = onrLogInqSvc.findOnrLogInqDetail(search);
			
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		
		
		HttpSession session = request.getSession();
		String master = (String)session.getAttribute("master");
		modelAndView.addObject("masterflag", master);
		
		String scrn_name_view = (String) session.getAttribute("scrn_name");
		modelAndView.addObject("scrn_name_view", scrn_name_view);
		
		modelAndView.setViewName("onrLogInqDetail");

		return modelAndView;
	}
	
	@RequestMapping(value="download.html", method={RequestMethod.POST})
	public DataModelAndView findOnrLogInqList_download(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		if ( search.getMapping_id() == null ) search.setMapping_id("code1");
	    if ( search.getMapping_id().equals("code2") ) search.setMapping_id(null);
	    
	    List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_onnara");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
	    
	    onrLogInqSvc.findOnrLogInqList_download(modelAndView, search, request);
	    
		return modelAndView;
	}
	
	/**
	 * 전체로그조회 엑셀(CSV) 다운로드
	 */
	@RequestMapping(value="downloadCSV.html", method={RequestMethod.POST})
	public DataModelAndView findOnrLogInqList_downloadCSV(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.CSV_DOWNLOAD_VIEW);
		
		if ( search.getMapping_id() == null ) search.setMapping_id("code1");
	    if ( search.getMapping_id().equals("code2") ) search.setMapping_id(null);
	    
	    List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_onnara");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
	    
	    onrLogInqSvc.findOnrLogInqList_downloadCSV(modelAndView, search, request);
		
		return modelAndView;
	}
}
