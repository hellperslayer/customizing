package com.easycerti.eframe.psm.search.web;

import java.util.ArrayList;	//
import java.util.Date;	//
import java.util.List;	//
import java.util.Map;	//

import javax.servlet.http.HttpServletRequest;	//
import javax.servlet.http.HttpSession;			//

import org.springframework.beans.factory.annotation.Autowired;	//
import org.springframework.stereotype.Controller;	//
import org.springframework.web.bind.annotation.ModelAttribute;	//
import org.springframework.web.bind.annotation.RequestMapping;	//
import org.springframework.web.bind.annotation.RequestMethod;	//
import org.springframework.web.bind.annotation.RequestParam;	//
import org.springframework.web.servlet.ModelAndView;	//

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;	//
import com.easycerti.eframe.common.spring.DataModelAndView;	//
import com.easycerti.eframe.psm.search.dao.ExtrtCondbyInqDao;	//
import com.easycerti.eframe.psm.search.service.AuthInfoInqSvc;	//
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInqDetailChart;	//
import com.easycerti.eframe.psm.search.vo.SearchSearch;	//
import com.easycerti.eframe.psm.system_management.service.AuthMngtSvc;	//

@Controller
@RequestMapping("/authInfoInq/*")
public class AuthInfoInqCtrl {
	@Autowired
	private AuthInfoInqSvc authInfoInqSvc;

	@Autowired
	private AuthMngtSvc authMngtSvc;

	@Autowired
	private ExtrtCondbyInqDao extrtCondbyInqDao;

	@MngtActHist(log_action = "SELECT", log_message = "[접근관리] 관리자권한이력조회")
	@RequestMapping(value = { "list.html" }, method = { RequestMethod.POST })
	public DataModelAndView findAuthInfoInqList(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request,
			@RequestParam(value = "sub_menu_id", defaultValue = "") String sub_menu_id,
			@RequestParam(value = "menutitle", defaultValue = "") String menutitle) {

		Date today = new Date();
		search.initDatesIfEmpty(today, today);
		
		// 접속유형 default 세팅
		HttpSession session = request.getSession();
		String mapping_id = (String) session.getAttribute("mapping_id");
		if (mapping_id.equals("Y")) {
			if (search.getMapping_id() == null)
				search.setMapping_id("code1");
			// if ( search.getMapping_id().equals("codeAll") )
			// search.setMapping_id("");
			if (search.getMapping_id().equals("code2"))
				search.setMapping_id(null);
		} else {
			if (search.getMapping_id() == null || search.getMapping_id().equals("code2"))
				search.setMapping_id(null);
			else
				search.setMapping_id("code1");
		}

		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list");
		search.setAuth_idsList(list);

		DataModelAndView modelAndView = authInfoInqSvc.findAuthInfoInqList(search);
		int menuNum = 0;
		String menuCh = "";
		
		modelAndView.addObject("paramBean", parameters);

		if (!",".equals(sub_menu_id) && !"".equals(sub_menu_id)) {
			menuNum = Integer.parseInt(sub_menu_id.substring(4, 9)) + 16;
			menuCh = sub_menu_id.substring(0, 4) + "00";
			modelAndView.addObject("menuCh", menuCh);
			modelAndView.addObject("menuNum", menuNum);
		}
		modelAndView.addObject("menutitle", menutitle);
		modelAndView.addObject("search", search);
		modelAndView.addObject("sub_menu_id", sub_menu_id);
		modelAndView.setViewName("authInfoList");
		
		return modelAndView;
		
	}
	
	
	
	@RequestMapping(value="download.html", method={RequestMethod.POST})
	public DataModelAndView addAuthInfoInqList_download(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		HttpSession session = request.getSession();
		String mapping_id = (String) session.getAttribute("mapping_id");
		if(mapping_id.equals("Y")) {
			if ( search.getMapping_id() == null ) search.setMapping_id("code1");
		    //if ( search.getMapping_id().equals("codeAll") ) search.setMapping_id("");
		    if ( search.getMapping_id().equals("code2") ) search.setMapping_id(null);
		}else {
			if(search.getMapping_id() == null || search.getMapping_id().equals("code2"))
				search.setMapping_id(null);
			else
				search.setMapping_id("code1");
		}
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    search.setAuth_idsList(list);
		authInfoInqSvc.findAuthInfoInqList_download(modelAndView, search, request);
		
		return modelAndView;
	}
	
	@RequestMapping(value="downloadCSV.html", method={RequestMethod.POST})
	public DataModelAndView addAuthInfoInqList_downloadCSV(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.CSV_DOWNLOAD_VIEW);
		
		HttpSession session = request.getSession();
		String mapping_id = (String) session.getAttribute("mapping_id");
		if(mapping_id.equals("Y")) {
			if ( search.getMapping_id() == null ) search.setMapping_id("code1");
		    //if ( search.getMapping_id().equals("codeAll") ) search.setMapping_id("");
		    if ( search.getMapping_id().equals("code2") ) search.setMapping_id(null);
		}else {
			if(search.getMapping_id() == null || search.getMapping_id().equals("code2"))
				search.setMapping_id(null);
			else
				search.setMapping_id("code1");
		}
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    search.setAuth_idsList(list);
		authInfoInqSvc.findAuthInfoInqList_downloadCSV(modelAndView, search, request);
		
		return modelAndView;
	}
	
	@RequestMapping(value="help_manager.html", method={RequestMethod.POST})
	public DataModelAndView setHelpView_manager() {
		DataModelAndView modelAndView = new DataModelAndView();
		
		modelAndView.setViewName("help_manager");
				
		return modelAndView;
	}
	
	@RequestMapping(value="help_master.html", method={RequestMethod.POST})
	public DataModelAndView setHelpView_master() {
		DataModelAndView modelAndView = new DataModelAndView();
		
		modelAndView.setViewName("help_master");
				
		return modelAndView;
	}
	
	@MngtActHist(log_action = "SELECT", log_message = "[접근관리] 관리자권한관리 상세")
	@RequestMapping(value={"detail.html"}, method={RequestMethod.POST})
	public DataModelAndView findExcelDownLoadDetail(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = authInfoInqSvc.findAuthInfoInqDetail(search);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		
		modelAndView.setViewName("authInfoListDetail");
		
		return modelAndView;
	}
	
	
}
