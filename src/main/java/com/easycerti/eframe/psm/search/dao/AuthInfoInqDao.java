package com.easycerti.eframe.psm.search.dao;

import java.util.List;

import com.easycerti.eframe.psm.search.vo.AuthInfoInq;
import com.easycerti.eframe.psm.search.vo.SearchSearch;

public interface AuthInfoInqDao {
	
	public List<AuthInfoInq> findAuthInfoInqList(SearchSearch search);		//권한관리 리스트
	public int findAuthInfoInqOne_count(SearchSearch search);
					   
	public AuthInfoInq findAuthInfoInqDetail(SearchSearch search);			//권한관리 리스트 (로그타입 조회) 상세페이지
	public AuthInfoInq findAuthInfoInqDetailResult(SearchSearch search);
	
	public List<AuthInfoInq> findAuthInfoToDepartment();
	
}