package com.easycerti.eframe.psm.search.vo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.easycerti.eframe.common.util.PageInfo;
import com.easycerti.eframe.common.vo.AbstractValueObject;
import com.easycerti.eframe.psm.calling_management.type.Status;
import com.easycerti.eframe.psm.calling_management.vo.CallingBizLog;
/**
 * 
 * 설명 : 추출조건별조회 VO
 * @author tjlee
 * @since 2015. 4. 22.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 22.           tjlee            최초 생성
 *
 * </pre>
 */
public class ExtrtCondbyInq extends AbstractValueObject implements Serializable {
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 3548522096551273127L;
	
	private String occr_dt;
	private String dept_id;
	private String emp_user_id;
	private String emp_user_name;
	private String rule_cd;
	private int rule_cnt;
	
	
	private int dng_val;
	private String dept_name;
	private String rule_nm;
	private String log_delimiter;
	private int sum_dng_val;
	private String string_agg_rule_nm;
	private String user_ip;
	private long log_seq;
	private String logSeqs;
	private String proc_time;
	private String proc_date;
	private String system_seq;
	private String file_name; //다운로드시 - 파일명
	private String req_url;
	private String result_type;
	private long emp_detail_seq; // 시퀀스
	private String summon_code;
	private int cll_dmnd_id; // 판정값
	private String statusValue;
	private String summon_dt;
	private Status desc_status;
	private String summon_req_emp;	// 소명 요청자
	private String emp_cd;
	private String desc_seq;
	private long desc_seqs;
	private String deci_nm;
	private String decision;
	private String log_gubun;
	private String remark;
	private String remark2;
	private String desc_result;
	private String file_nm;
	private String file_nm2;
	
	private int totalPrivCount=0;
	private String use_yn;
	
	private String system_name;
	private int summon_cnt;
	private int pre_cnt;
	private int this_cnt;
	private int gap;
	private int s_nm2;
	private int snm_4;
	private int snm_5;
	private int snm_8;
	private int snm_10;
	private int log_cnt;
	private String recalling_yn;
	private String desc_result_dt;
	private String bclass_ip;
	private String result_content;
	private String req_type;
	private String followup;
	private String h_body;
	private String h_type;
	private String h_user_id;
	private String h_timestamp;
	private int server_seq;

	private String abuse_purpose;
	private String abuse_type;
	private String fault_degree;
	
	private String abuse_content;
	private String administrative_dispo_day;
	private String administrative_dispo_type;
	private String criminal_dispo_day;
	private String criminal_dispo_type;
	private String etc_dispo_day;
	private String etc_dispo_type;
	private String response_user_name;
	private String result_user_name;
	private String detect_div;
	private int rule_seq;
	private String rule_desc;
	private int limit_cnt;
	private String is_realtime_extract;
	private String script;
	private String sql;
	private String collect_dt;
	List<String> auth_idsList;
	private String log_seqs;
	
	// 고도화
	private String rule_view_type;
	private String rule_result_type;
	private int scenario1 = 0;
	private int scenario2 = 0;
	private int scenario3 = 0;
	private int scenario4 = 0;
	private int scenario5 = 0;
	private int logcnt = 0;
	private String ip;
	private String time_view_yn;
	
	// 소명 판정
	private String summon_id;
	private String summon_system_seq;
	private String emp_system_seq;
	private String delegation_date;
	private String delegation_from;
	private String delegation_to;
	private String delegation_act;
	private String delegation_act_date;
	
	private String summon_seq;
	private String summon_hist_seq;
	
	private String summon_name;
	private String summon_system_name;
	private String emp_system_name;
	
	private String approval_id;
	private String approval_name;	
	private String approval_seq;
	
	private int type1=0;
	private int type2=0;
	private int type3=0;
	private int type4=0;
	private int type5=0;
	private int type6=0;
	private int type7=0;
	private int type8=0;
	private int type9=0;
	private int type10=0;
	private int type11=0;
	private int type12=0;
	private int type13=0;
	private int type14=0;
	private int type15=0;
	private int type16=0;
	private int type17=0;
	private int type18=0;
	
	
	public void setType1(int type1) {
		this.type1 = type1;
	}
	public void setType2(int type2) {
		this.type2 = type2;
	}
	public void setType3(int type3) {
		this.type3 = type3;
	}
	public void setType4(int type4) {
		this.type4 = type4;
	}
	public void setType5(int type5) {
		this.type5 = type5;
	}
	public void setType6(int type6) {
		this.type6 = type6;
	}
	public void setType7(int type7) {
		this.type7 = type7;
	}
	public void setType8(int type8) {
		this.type8 = type8;
	}
	public void setType9(int type9) {
		this.type9 = type9;
	}
	public void setType10(int type10) {
		this.type10 = type10;
	}
	public void setType11(int type11) {
		this.type11 = type11;
	}
	public void setType12(int type12) {
		this.type12 = type12;
	}
	public void setType13(int type13) {
		this.type13 = type13;
	}
	public void setType14(int type14) {
		this.type14 = type14;
	}
	public void setType15(int type15) {
		this.type15 = type15;
	}
	public void setType16(int type16) {
		this.type16 = type16;
	}
	public void setType17(int type17) {
		this.type17 = type17;
	}
	public void setType18(int type18) {
		this.type18 = type18;
	}
	public int getType1() {
		return type1;
	}
	public void countType1() {
		this.type1++;
	}
	public int getType2() {
		return type2;
	}
	public void countType2() {
		this.type2++;
	}
	public int getType3() {
		return type3;
	}
	public void countType3() {
		this.type3++;
	}
	public int getType4() {
		return type4;
	}
	public void countType4() {
		this.type4++;
	}
	public int getType5() {
		return type5;
	}
	public void countType5() {
		this.type5++;
	}
	public int getType6() {
		return type6;
	}
	public void countType6() {
		this.type6++;
	}
	public int getType7() {
		return type7;
	}
	public void countType7() {
		this.type7++;
	}
	public int getType8() {
		return type8;
	}
	public void countType8() {
		this.type8++;
	}
	public int getType9() {
		return type9;
	}
	public void countType9() {
		this.type9++;
	}
	public int getType10() {
		return type10;
	}
	public void countType10() {
		this.type10++;
	}
	public int getType11() {
		return type11;
	}
	public void countType11() {
		this.type11++;
	}
	public int getType12() {
		return type12;
	}
	public void countType12() {
		this.type12++;
	}
	public int getType13() {
		return type13;
	}
	public void countType13() {
		this.type13++;
	}
	public int getType14() {
		return type14;
	}
	public void countType14() {
		this.type14++;
	}
	public int getType15() {
		return type15;
	}
	public void countType15() {
		this.type15++;
	}
	
	
	public int getType16() {
		return type16;
	}
	public void countType16() {
		this.type16++;
	}
	public int getType17() {
		return type17;
	}
	public void countType17() {
		this.type17++;
	}
	public int getType18() {
		return type18;
	}
	public void countType18() {
		this.type18++;
	}
	
	private String summonKey;
	private String parameter;
	
	// DGB 전용 
		//과다처리건
		private int rule_cnt2;
		//평균
		private int avg_cnt;
	
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public String getSummonKey() {
		return summonKey;
	}
	public void setSummonKey(String summonKey) {
		this.summonKey = summonKey;
	}
	public String getParameter() {
		return parameter;
	}
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}
	public String getApproval_seq() {
		return approval_seq;
	}
	public void setApproval_seq(String approval_seq) {
		this.approval_seq = approval_seq;
	}
	public String getApproval_name() {
		return approval_name;
	}
	public void setApproval_name(String approval_name) {
		this.approval_name = approval_name;
	}
	public String getApproval_id() {
		return approval_id;
	}
	public void setApproval_id(String approval_id) {
		this.approval_id = approval_id;
	}
	public String getSummon_name() {
		return summon_name;
	}
	public void setSummon_name(String summon_name) {
		this.summon_name = summon_name;
	}
	public String getSummon_system_name() {
		return summon_system_name;
	}
	public void setSummon_system_name(String summon_system_name) {
		this.summon_system_name = summon_system_name;
	}
	public String getEmp_system_name() {
		return emp_system_name;
	}
	public void setEmp_system_name(String emp_system_name) {
		this.emp_system_name = emp_system_name;
	}
	public String getSummon_seq() {
		return summon_seq;
	}
	public void setSummon_seq(String summon_seq) {
		this.summon_seq = summon_seq;
	}
	public String getSummon_hist_seq() {
		return summon_hist_seq;
	}
	public void setSummon_hist_seq(String summon_hist_seq) {
		this.summon_hist_seq = summon_hist_seq;
	}
	public String getSummon_id() {
		return summon_id;
	}
	public void setSummon_id(String summon_id) {
		this.summon_id = summon_id;
	}
	public String getSummon_system_seq() {
		return summon_system_seq;
	}
	public void setSummon_system_seq(String summon_system_seq) {
		this.summon_system_seq = summon_system_seq;
	}
	public String getEmp_system_seq() {
		return emp_system_seq;
	}
	public void setEmp_system_seq(String emp_system_seq) {
		this.emp_system_seq = emp_system_seq;
	}
	public String getDelegation_date() {
		return delegation_date;
	}
	public void setDelegation_date(String delegation_date) {
		this.delegation_date = delegation_date;
	}
	public String getDelegation_from() {
		return delegation_from;
	}
	public void setDelegation_from(String delegation_from) {
		this.delegation_from = delegation_from;
	}
	public String getDelegation_to() {
		return delegation_to;
	}
	public void setDelegation_to(String delegation_to) {
		this.delegation_to = delegation_to;
	}
	public String getDelegation_act() {
		return delegation_act;
	}
	public void setDelegation_act(String delegation_act) {
		this.delegation_act = delegation_act;
	}
	public String getDelegation_act_date() {
		return delegation_act_date;
	}
	public void setDelegation_act_date(String delegation_act_date) {
		this.delegation_act_date = delegation_act_date;
	}
	
	private String req_dt;
	private String res_dt;
	private String decision_dt;
	private String summon_status;
	private String decision_status;
	//DGB 최종판정 
	private String decision_status_second;
	//
	private String datetime;
	private String user_id;
	private String user_name;
	private String target_id;
	private String target_name;
	private String res_user_id;
	private String res_user_name;
	private String decision_user_id;
	private String decision_user_name;
	private String status;
	private String msg;
	private String responseMsg;
	
	private Map<String, String> resultTypeMap;
	
	private String proc_month;
	
	
	
	public String getProc_month() {
		return proc_month;
	}
	public void setProc_month(String proc_month) {
		this.proc_month = proc_month;
	}
	public String getDecision_user_id() {
		return decision_user_id;
	}
	public void setDecision_user_id(String decision_user_id) {
		this.decision_user_id = decision_user_id;
	}
	public String getDecision_user_name() {
		return decision_user_name;
	}
	public void setDecision_user_name(String decision_user_name) {
		this.decision_user_name = decision_user_name;
	}
	public String getRes_user_id() {
		return res_user_id;
	}
	public void setRes_user_id(String res_user_id) {
		this.res_user_id = res_user_id;
	}
	public String getRes_user_name() {
		return res_user_name;
	}
	public void setRes_user_name(String res_user_name) {
		this.res_user_name = res_user_name;
	}
	public String getResponseMsg() {
		return responseMsg;
	}
	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getDatetime() {
		return datetime;
	}
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getTarget_id() {
		return target_id;
	}
	public void setTarget_id(String target_id) {
		this.target_id = target_id;
	}
	public String getTarget_name() {
		return target_name;
	}
	public void setTarget_name(String target_name) {
		this.target_name = target_name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSummon_status() {
		return summon_status;
	}
	public void setSummon_status(String summon_status) {
		this.summon_status = summon_status;
	}
	public String getDecision_status() {
		return decision_status;
	}
	public void setDecision_status(String decision_status) {
		this.decision_status = decision_status;
	}
	public String getReq_dt() {
		return req_dt;
	}
	public void setReq_dt(String req_dt) {
		this.req_dt = req_dt;
	}
	public String getRes_dt() {
		return res_dt;
	}
	public void setRes_dt(String res_dt) {
		this.res_dt = res_dt;
	}
	public String getDecision_dt() {
		return decision_dt;
	}
	public void setDecision_dt(String decision_dt) {
		this.decision_dt = decision_dt;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getTime_view_yn() {
		return time_view_yn;
	}
	public void setTime_view_yn(String time_view_yn) {
		this.time_view_yn = time_view_yn;
	}
	public String getRule_view_type() {
		return rule_view_type;
	}
	public void setRule_view_type(String rule_view_type) {
		this.rule_view_type = rule_view_type;
	}
	public String getRule_result_type() {
		return rule_result_type;
	}
	public void setRule_result_type(String rule_result_type) {
		this.rule_result_type = rule_result_type;
	}
	public int getScenario1() {
		return scenario1;
	}
	public void setScenario1(int scenario1) {
		this.scenario1 = scenario1;
	}
	public int getScenario2() {
		return scenario2;
	}
	public void setScenario2(int scenario2) {
		this.scenario2 = scenario2;
	}
	public int getScenario3() {
		return scenario3;
	}
	public void setScenario3(int scenario3) {
		this.scenario3 = scenario3;
	}
	public int getScenario4() {
		return scenario4;
	}
	public void setScenario4(int scenario4) {
		this.scenario4 = scenario4;
	}
	public int getScenario5() {
		return scenario5;
	}
	public void setScenario5(int scenario5) {
		this.scenario5 = scenario5;
	}
	public int getLogcnt() {
		return logcnt;
	}
	public void setLogcnt(int logcnt) {
		this.logcnt = logcnt;
	}
	public String getLog_seqs() {
		return log_seqs;
	}
	public void setLog_seqs(String log_seqs) {
		this.log_seqs = log_seqs;
	}
	public List<String> getAuth_idsList() {
		return auth_idsList;
	}
	public void setAuth_idsList(List<String> auth_idsList) {
		this.auth_idsList = auth_idsList;
	}
	public long getDesc_seqs() {
		return desc_seqs;
	}
	public void setDesc_seqs(long desc_seqs) {
		this.desc_seqs = desc_seqs;
	}
	public String getCollect_dt() {
		return collect_dt;
	}
	public void setCollect_dt(String collect_dt) {
		this.collect_dt = collect_dt;
	}
	public String getSql() {
		return sql;
	}
	public void setSql(String sql) {
		this.sql = sql;
	}
	public int getRule_seq() {
		return rule_seq;
	}
	public void setRule_seq(int rule_seq) {
		this.rule_seq = rule_seq;
	}
	public String getRule_desc() {
		return rule_desc;
	}
	public void setRule_desc(String rule_desc) {
		this.rule_desc = rule_desc;
	}
	public int getLimit_cnt() {
		return limit_cnt;
	}
	public void setLimit_cnt(int limit_cnt) {
		this.limit_cnt = limit_cnt;
	}
	public String getIs_realtime_extract() {
		return is_realtime_extract;
	}
	public void setIs_realtime_extract(String is_realtime_extract) {
		this.is_realtime_extract = is_realtime_extract;
	}
	public String getScript() {
		return script;
	}
	public void setScript(String script) {
		this.script = script;
	}
	public String getDetect_div() {
		return detect_div;
	}
	public void setDetect_div(String detect_div) {
		this.detect_div = detect_div;
	}
	public String getAbuse_purpose() {
		return abuse_purpose;
	}
	public void setAbuse_purpose(String abuse_purpose) {
		this.abuse_purpose = abuse_purpose;
	}
	public String getAbuse_type() {
		return abuse_type;
	}
	public void setAbuse_type(String abuse_type) {
		this.abuse_type = abuse_type;
	}
	public String getFault_degree() {
		return fault_degree;
	}
	public void setFault_degree(String fault_degree) {
		this.fault_degree = fault_degree;
	}
	public String getAbuse_content() {
		return abuse_content;
	}
	public void setAbuse_content(String abuse_content) {
		this.abuse_content = abuse_content;
	}
	public String getAdministrative_dispo_day() {
		return administrative_dispo_day;
	}
	public void setAdministrative_dispo_day(String administrative_dispo_day) {
		this.administrative_dispo_day = administrative_dispo_day;
	}
	public String getAdministrative_dispo_type() {
		return administrative_dispo_type;
	}
	public void setAdministrative_dispo_type(String administrative_dispo_type) {
		this.administrative_dispo_type = administrative_dispo_type;
	}
	public String getCriminal_dispo_day() {
		return criminal_dispo_day;
	}
	public void setCriminal_dispo_day(String criminal_dispo_day) {
		this.criminal_dispo_day = criminal_dispo_day;
	}
	public String getCriminal_dispo_type() {
		return criminal_dispo_type;
	}
	public void setCriminal_dispo_type(String criminal_dispo_type) {
		this.criminal_dispo_type = criminal_dispo_type;
	}
	public String getEtc_dispo_day() {
		return etc_dispo_day;
	}
	public void setEtc_dispo_day(String etc_dispo_day) {
		this.etc_dispo_day = etc_dispo_day;
	}
	public String getEtc_dispo_type() {
		return etc_dispo_type;
	}
	public void setEtc_dispo_type(String etc_dispo_type) {
		this.etc_dispo_type = etc_dispo_type;
	}
	public String getResponse_user_name() {
		return response_user_name;
	}
	public void setResponse_user_name(String response_user_name) {
		this.response_user_name = response_user_name;
	}
	public String getResult_user_name() {
		return result_user_name;
	}
	public void setResult_user_name(String result_user_name) {
		this.result_user_name = result_user_name;
	}
	public int getServer_seq() {
		return server_seq;
	}
	public void setServer_seq(int server_seq) {
		this.server_seq = server_seq;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getH_body() {
		return h_body;
	}
	public void setH_body(String h_body) {
		this.h_body = h_body;
	}
	public String getH_type() {
		return h_type;
	}
	public void setH_type(String h_type) {
		this.h_type = h_type;
	}
	public String getH_user_id() {
		return h_user_id;
	}
	public void setH_user_id(String h_user_id) {
		this.h_user_id = h_user_id;
	}
	public String getH_timestamp() {
		return h_timestamp;
	}
	public void setH_timestamp(String h_timestamp) {
		this.h_timestamp = h_timestamp;
	}
	public String getReq_type() {
		return req_type;
	}
	public void setReq_type(String req_type) {
		this.req_type = req_type;
	}
	public String getResult_content() {
		return result_content;
	}
	public void setResult_content(String result_content) {
		this.result_content = result_content;
	}
	public String getBclass_ip() {
		return bclass_ip;
	}
	public void setBclass_ip(String bclass_ip) {
		this.bclass_ip = bclass_ip;
	}
	public String getSystem_name() {
		return system_name;
	}
	public void setSystem_name(String system_name) {
		this.system_name = system_name;
	}
	public int getSummon_cnt() {
		return summon_cnt;
	}
	public void setSummon_cnt(int summon_cnt) {
		this.summon_cnt = summon_cnt;
	}
	public int getAvg_cnt() {
		return avg_cnt;
	}
	public void setAvg_cnt(int avg_cnt) {
		this.avg_cnt = avg_cnt;
	}
	public int getPre_cnt() {
		return pre_cnt;
	}
	public void setPre_cnt(int pre_cnt) {
		this.pre_cnt = pre_cnt;
	}
	public int getThis_cnt() {
		return this_cnt;
	}
	public void setThis_cnt(int this_cnt) {
		this.this_cnt = this_cnt;
	}
	public int getGap() {
		return gap;
	}
	public void setGap(int gap) {
		this.gap = gap;
	}
	public int getS_nm2() {
		return s_nm2;
	}
	public void setS_nm2(int s_nm2) {
		this.s_nm2 = s_nm2;
	}
	public int getSnm_4() {
		return snm_4;
	}
	public void setSnm_4(int snm_4) {
		this.snm_4 = snm_4;
	}
	public int getSnm_5() {
		return snm_5;
	}
	public void setSnm_5(int snm_5) {
		this.snm_5 = snm_5;
	}
	public int getSnm_8() {
		return snm_8;
	}
	public void setSnm_8(int snm_8) {
		this.snm_8 = snm_8;
	}
	public int getLog_cnt() {
		return log_cnt;
	}
	public void setLog_cnt(int log_cnt) {
		this.log_cnt = log_cnt;
	}
	public String getRecalling_yn() {
		return recalling_yn;
	}
	public void setRecalling_yn(String recalling_yn) {
		this.recalling_yn = recalling_yn;
	}
	public String getDesc_result_dt() {
		return desc_result_dt;
	}
	public void setDesc_result_dt(String desc_result_dt) {
		this.desc_result_dt = desc_result_dt;
	}
	public String getDesc_result() {
		return desc_result;
	}
	public void setDesc_result(String desc_result) {
		this.desc_result = desc_result;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public int getTotalPrivCount() {
		return totalPrivCount;
	}
	public void setTotalPrivCount(int totalPrivCount) {
		this.totalPrivCount = totalPrivCount;
	}
	
	private List<CallingBizLog> bizLogList;
	
	private PageInfo pageInfo;
	
	private String scen_name;
	private String scen_seq;
	
	public String getReq_url() {
		return req_url;
	}
	public void setReq_url(String req_url) {
		this.req_url = req_url;
	}
	public String getResult_type() {
		return result_type;
	}
	public void setResult_type(String result_type) {
		this.result_type = result_type;
	}
	public long getLog_seq() {
		return log_seq;
	}
	public void setLog_seq(long log_seq) {
		this.log_seq = log_seq;
	}
	
	public String getLogSeqs() {
		return logSeqs;
	}
	public void setLogSeqs(String logSeqs) {
		this.logSeqs = logSeqs;
	}
	public String getProc_time() {
		return proc_time;
	}
	public void setProc_time(String proc_time) {
		this.proc_time = proc_time;
	}
	
	public String getProc_date() {
		return proc_date;
	}
	public void setProc_date(String proc_date) {
		this.proc_date = proc_date;
	}
	public String getSystem_seq() {
		return system_seq;
	}
	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}
	public String getUser_ip() {
		return user_ip;
	}
	public void setUser_ip(String user_ip) {
		this.user_ip = user_ip;
	}
	
	public String getOccr_dt() {
		return occr_dt;
	}

	public void setOccr_dt(String occr_dt) {
		this.occr_dt = occr_dt;
	}
	public String getRule_cd() {
		return rule_cd;
	}
	public void setRule_cd(String rule_cd) {
		this.rule_cd = rule_cd;
	}
	public int getRule_cnt() {
		return rule_cnt;
	}

	public void setRule_cnt(int rule_cnt) {
		this.rule_cnt = rule_cnt;
	}

	public int getDng_val() {
		return dng_val;
	}

	public void setDng_val(int dng_val) {
		this.dng_val = dng_val;
	}

	public String getRule_nm() {
		return rule_nm;
	}

	public void setRule_nm(String rule_nm) {
		this.rule_nm = rule_nm;
	}

	public String getLog_delimiter() {
		return log_delimiter;
	}

	public void setLog_delimiter(String log_delimiter) {
		this.log_delimiter = log_delimiter;
	}

	public PageInfo getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(PageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}

	public String getDept_id() {
		return dept_id;
	}

	public void setDept_id(String dept_id) {
		if(dept_id.equals("DEPT99999")) {
			this.dept_id = "공통";
		}else {
			this.dept_id = dept_id;
		}
	}

	public String getEmp_user_id() {
		return emp_user_id;
	}

	public void setEmp_user_id(String emp_user_id) {
		if(emp_user_id.equals("ZZZZ99999")) {
			this.emp_user_id = "공통";
		}else {
			this.emp_user_id = emp_user_id;
		}
	}

	public String getEmp_user_name() {
		return emp_user_name;
	}

	public void setEmp_user_name(String emp_user_name) {
		if(emp_user_name.equals("ZZZZ99999")) {
			this.emp_user_name = "공통";
		}else {
			this.emp_user_name = emp_user_name;
		}
	}

	public String getDept_name() {
		return dept_name;
	}

	public void setDept_name(String dept_name) {
		if(dept_name.equals("DEPT99999")) {
			this.dept_name = "공통";
		}else {
			this.dept_name = dept_name;
		}
	}

	public int getSum_dng_val() {
		return sum_dng_val;
	}

	public void setSum_dng_val(int sum_dng_val) {
		this.sum_dng_val = sum_dng_val;
	}

	public String getString_agg_rule_nm() {
		return string_agg_rule_nm;
	}

	public void setString_agg_rule_nm(String string_agg_rule_nm) {
		this.string_agg_rule_nm = string_agg_rule_nm;
	}
	public long getEmp_detail_seq() {
		return emp_detail_seq;
	}
	public void setEmp_detail_seq(long emp_detail_seq) {
		this.emp_detail_seq = emp_detail_seq;
	}
	public int getCll_dmnd_id() {
		return cll_dmnd_id;
	}
	public void setCll_dmnd_id(int cll_dmnd_id) {
		this.cll_dmnd_id = cll_dmnd_id;
	}
	public List<CallingBizLog> getBizLogList() {
		return bizLogList;
	}
	public void setBizLogList(List<CallingBizLog> bizLogList) {
		this.bizLogList = bizLogList;
	}
	public String getStatusValue() {
		return statusValue;
	}
	public void setStatusValue(String statusValue) {
		this.statusValue = statusValue;
	}
	public String getSummon_dt() {
		return summon_dt;
	}
	public void setSummon_dt(String summon_dt) {
		this.summon_dt = summon_dt;
	}
	
	public Status getDesc_status() {
		return desc_status;
	}
	public void setDesc_status(Status desc_status) {
		this.desc_status = desc_status;
	}
	
	public String getSummon_req_emp() {
		return summon_req_emp;
	}
	public void setSummon_req_emp(String summon_req_emp) {
		this.summon_req_emp = summon_req_emp;
	}
	// toString
	@Override
	public String toString() {
		return "ExtrtCondbyInq [" + (occr_dt != null ? "occr_dt=" + occr_dt + ", " : "") + (dept_id != null ? "dept_id=" + dept_id + ", " : "")
				+ (emp_user_id != null ? "emp_user_id=" + emp_user_id + ", " : "") + (emp_user_name != null ? "emp_user_name=" + emp_user_name + ", " : "")
				+ (rule_cd != null ? "rule_cd=" + rule_cd + ", " : "") + "rule_cnt=" + rule_cnt + ", dng_val=" + dng_val + ", " + (dept_name != null ? "dept_name=" + dept_name + ", " : "")
				+ (rule_nm != null ? "rule_nm=" + rule_nm + ", " : "") + (log_delimiter != null ? "log_delimiter=" + log_delimiter + ", " : "") + "sum_dng_val=" + sum_dng_val + ", "
				+ (string_agg_rule_nm != null ? "string_agg_rule_nm=" + string_agg_rule_nm + ", " : "") + (user_ip != null ? "user_ip=" + user_ip + ", " : "") + "log_seq=" + log_seq + ", "
				+ (proc_time != null ? "proc_time=" + proc_time + ", " : "") + (system_seq != null ? "system_seq=" + system_seq + ", " : "") + (req_url != null ? "req_url=" + req_url + ", " : "")
				+ (result_type != null ? "result_type=" + result_type + ", " : "") + "emp_detail_seq=" + emp_detail_seq + ", cll_dmnd_id=" + cll_dmnd_id + ", "
				+ (status != null ? "status=" + status + ", " : "") + (statusValue != null ? "statusValue=" + statusValue + ", " : "") + (bizLogList != null ? "bizLogList=" + bizLogList + ", " : "")
				+ (pageInfo != null ? "pageInfo=" + pageInfo : "") + "]";
	}
	
	
	private String code_name;
	private int code_id;
	private String group_code_id;
	private int dng_ct;
	private int dng_per;
	private String className;
	private long cnt;
	private String data;
	private String is_check;
	private String scrn_id;
	private String scrn_name;
	
	public String getScrn_id() {
		return scrn_id;
	}
	public void setScrn_id(String scrn_id) {
		this.scrn_id = scrn_id;
	}
	public String getScrn_name() {
		return scrn_name;
	}
	public void setScrn_name(String scrn_name) {
		this.scrn_name = scrn_name;
	}
	public String getGroup_code_id() {
		return group_code_id;
	}
	public void setGroup_code_id(String group_code_id) {
		this.group_code_id = group_code_id;
	}
	public long getCnt() {
		return cnt;
	}
	public void setCnt(long cnt) {
		this.cnt = cnt;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public int getDng_per() {
		return dng_per;
	}
	public void setDng_per(int dng_per) {
		this.dng_per = dng_per;
	}
	public String getCode_name() {
		return code_name;
	}
	public void setCode_name(String code_name) {
		this.code_name = code_name;
	}
	public int getCode_id() {
		return code_id;
	}
	public void setCode_id(int code_id) {
		this.code_id = code_id;
	}
	public int getDng_ct() {
		return dng_ct;
	}
	public void setDng_ct(int dng_ct) {
		this.dng_ct = dng_ct;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getScen_name() {
		return scen_name;
	}
	public void setScen_name(String scen_name) {
		this.scen_name = scen_name;
	}
	
	public String getScen_seq() {
		return scen_seq;
	}
	public void setScen_seq(String scen_seq) {
		this.scen_seq = scen_seq;
	}
	public String getEmp_cd() {
		return emp_cd;
	}
	public void setEmp_cd(String emp_cd) {
		this.emp_cd = emp_cd;
	}
	public String getDesc_seq() {
		return desc_seq;
	}
	public void setDesc_seq(String desc_seq) {
		this.desc_seq = desc_seq;
	}
	public String getDeci_nm() {
		return deci_nm;
	}
	public void setDeci_nm(String deci_nm) {
		this.deci_nm = deci_nm;
	}
	public String getDecision() {
		return decision;
	}
	public void setDecision(String decision) {
		this.decision = decision;
	}
	public String getLog_gubun() {
		return log_gubun;
	}
	public void setLog_gubun(String log_gubun) {
		this.log_gubun = log_gubun;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getRemark2() {
		return remark2;
	}
	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}
	public String getIs_check() {
		return is_check;
	}
	public void setIs_check(String is_check) {
		this.is_check = is_check;
	}
	public String getUse_yn() {
		return use_yn;
	}
	public void setUse_yn(String use_yn) {
		this.use_yn = use_yn;
	}
	public String getFile_nm() {
		return file_nm;
	}
	public void setFile_nm(String file_nm) {
		this.file_nm = file_nm;
	}
	public String getFile_nm2() {
		return file_nm2;
	}
	public void setFile_nm2(String file_nm2) {
		this.file_nm2 = file_nm2;
	}
	public String getFollowup() {
		return followup;
	}
	public void setFollowup(String followup) {
		this.followup = followup;
	}
	
	
	/*소명테이블 재설계*/
	private String expect_dt;
	private String ruld_cd;
	private String log_type;
	private String request_body;
	private String request_body_summary;
	private String request_dt;
	private String request_timestamp;
	private String request_user_id;
	private String request_user_name;
	private String request_target_user_id;
	private String request_target_user_name;
	private String response_body;
	private String response_dt;
	private String response_timestamp;
	private String response_user_id;
	private String file_path1;
	private String file_path2;
	private String file_path3;
	private String file_path4;
	private String result_body;
	private String result_dt;
	private String result_timestamp;
	private String result_user_id;
	private String result2_body;
	private String result2_dt;
	private String result2_timestamp;
	private String result2_user_id;
	private String desc_result2;
	private String result2_user_name;
	private String desc_hq_cd;
	private String checkReResult;
	//소명 응답 내용
	private String msg_reply;
	//소명 승인내용
	private String msg_decision;
	//소명 최종승인내용(DGB전용)
	private String msg_decision2;

	public String getCheckReResult() {
		return checkReResult;
	}
	public void setCheckReResult(String checkReResult) {
		this.checkReResult = checkReResult;
	}
	public String getResult2_user_name() {
		return result2_user_name;
	}
	public void setResult2_user_name(String result2_user_name) {
		this.result2_user_name = result2_user_name;
	}
	public String getResult2_body() {
		return result2_body;
	}
	public void setResult2_body(String result2_body) {
		this.result2_body = result2_body;
	}
	public String getResult2_dt() {
		return result2_dt;
	}
	public void setResult2_dt(String result2_dt) {
		this.result2_dt = result2_dt;
	}
	public String getResult2_timestamp() {
		return result2_timestamp;
	}
	public void setResult2_timestamp(String result2_timestamp) {
		this.result2_timestamp = result2_timestamp;
	}
	public String getResult2_user_id() {
		return result2_user_id;
	}
	public void setResult2_user_id(String result2_user_id) {
		this.result2_user_id = result2_user_id;
	}
	public String getDesc_result2() {
		return desc_result2;
	}
	public void setDesc_result2(String desc_result2) {
		this.desc_result2 = desc_result2;
	}
	public String getExpect_dt() {
		return expect_dt;
	}
	public void setExpect_dt(String expect_dt) {
		this.expect_dt = expect_dt;
	}
	public String getRuld_cd() {
		return ruld_cd;
	}
	public void setRuld_cd(String ruld_cd) {
		this.ruld_cd = ruld_cd;
	}
	public String getLog_type() {
		return log_type;
	}
	public void setLog_type(String log_type) {
		this.log_type = log_type;
	}
	public String getRequest_body() {
		return request_body;
	}
	public void setRequest_body(String request_body) {
		this.request_body = request_body;
	}
	public String getRequest_body_summary() {
		return request_body_summary;
	}
	public void setRequest_body_summary(String request_body_summary) {
		this.request_body_summary = request_body_summary;
	}
	public String getRequest_dt() {
		return request_dt;
	}
	public void setRequest_dt(String request_dt) {
		this.request_dt = request_dt;
	}
	public String getRequest_timestamp() {
		return request_timestamp;
	}
	public void setRequest_timestamp(String request_timestamp) {
		this.request_timestamp = request_timestamp;
	}
	public String getRequest_user_id() {
		return request_user_id;
	}
	public void setRequest_user_id(String request_user_id) {
		this.request_user_id = request_user_id;
	}
	public String getRequest_user_name() {
		return request_user_name;
	}
	public void setRequest_user_name(String request_user_name) {
		this.request_user_name = request_user_name;
	}
	public String getRequest_target_user_id() {
		return request_target_user_id;
	}
	public void setRequest_target_user_id(String request_target_user_id) {
		this.request_target_user_id = request_target_user_id;
	}
	public String getRequest_target_user_name() {
		return request_target_user_name;
	}
	public void setRequest_target_user_name(String request_target_user_name) {
		this.request_target_user_name = request_target_user_name;
	}
	public String getResponse_body() {
		return response_body;
	}
	public void setResponse_body(String response_body) {
		this.response_body = response_body;
	}
	public String getResponse_dt() {
		return response_dt;
	}
	public void setResponse_dt(String response_dt) {
		this.response_dt = response_dt;
	}
	public String getResponse_timestamp() {
		return response_timestamp;
	}
	public void setResponse_timestamp(String response_timestamp) {
		this.response_timestamp = response_timestamp;
	}
	public String getResponse_user_id() {
		return response_user_id;
	}
	public void setResponse_user_id(String response_user_id) {
		this.response_user_id = response_user_id;
	}
	public String getFile_path1() {
		return file_path1;
	}
	public void setFile_path1(String file_path1) {
		this.file_path1 = file_path1;
	}
	public String getFile_path2() {
		return file_path2;
	}
	public void setFile_path2(String file_path2) {
		this.file_path2 = file_path2;
	}
	public String getFile_path3() {
		return file_path3;
	}
	public void setFile_path3(String file_path3) {
		this.file_path3 = file_path3;
	}
	public String getFile_path4() {
		return file_path4;
	}
	public void setFile_path4(String file_path4) {
		this.file_path4 = file_path4;
	}
	public String getResult_body() {
		return result_body;
	}
	public void setResult_body(String result_body) {
		this.result_body = result_body;
	}
	public String getResult_dt() {
		return result_dt;
	}
	public void setResult_dt(String result_dt) {
		this.result_dt = result_dt;
	}
	public String getResult_timestamp() {
		return result_timestamp;
	}
	public void setResult_timestamp(String result_timestamp) {
		this.result_timestamp = result_timestamp;
	}
	public String getResult_user_id() {
		return result_user_id;
	}
	public void setResult_user_id(String result_user_id) {
		this.result_user_id = result_user_id;
	}
	public String getDesc_hq_cd() {
		return desc_hq_cd;
	}
	public void setDesc_hq_cd(String desc_hq_cd) {
		this.desc_hq_cd = desc_hq_cd;
	}
	
	private long extract_result_seq;
	private String extract_dt;
	private String extract_key;
	private String is_summon_yn;

	public long getExtract_result_seq() {
		return extract_result_seq;
	}
	public void setExtract_result_seq(long extract_result_seq) {
		this.extract_result_seq = extract_result_seq;
	}
	public String getExtract_dt() {
		return extract_dt;
	}
	public void setExtract_dt(String extract_dt) {
		this.extract_dt = extract_dt;
	}
	public String getExtract_key() {
		return extract_key;
	}
	public void setExtract_key(String extract_key) {
		this.extract_key = extract_key;
	}
	public String getIs_summon_yn() {
		return is_summon_yn;
	}
	public void setIs_summon_yn(String is_summon_yn) {
		this.is_summon_yn = is_summon_yn;
	}
	public String getSummon_code() {
		return summon_code;
	}
	public void setSummon_code(String summon_code) {
		this.summon_code = summon_code;
	}
	
	public void setResultTypeMap(String resultType) {
		if(resultType == null)return;
		this.resultTypeMap = new HashMap<String, String>();
		int newResultTypeCheck = resultType.indexOf(":");
		String resultArr[] = resultType.split(",");
		if(newResultTypeCheck<1) {
			for (String string : resultArr) {
				try {
					this.getClass().getMethod("countType"+Integer.parseInt(string)).invoke(this);
				} catch (Exception e) {
					e.printStackTrace();
				} 
			}
		}else {
			for (String string : resultArr) {
				String typeValue[] = string.split(":");
				if(typeValue.length<2) continue;
				try {
					this.getClass().getMethod("setType"+Integer.parseInt(typeValue[0]),int.class).invoke(this,Integer.parseInt(typeValue[1]));
				} catch (Exception e) {
					e.printStackTrace();
				} 
			}
		}
		for (int i = 1; i < 19; i++) {
			try {
				Integer resultCount = (Integer) this.getClass().getMethod("getType"+i).invoke(this);
				if(resultCount>0) {
					resultTypeMap.put(String.valueOf(i), String.valueOf(resultCount));
				}
			} catch (Exception e) {
				e.printStackTrace();
			} 
		}
	}
	public Map<String, String> getResultTypeMap() {
		return this.resultTypeMap;
	}
	public String getDecision_status_second() {
		return decision_status_second;
	}
	public void setDecision_status_second(String decision_status_second) {
		this.decision_status_second = decision_status_second;
	}
	public String getMsg_reply() {
		return msg_reply;
	}
	public void setMsg_reply(String msg_reply) {
		this.msg_reply = msg_reply;
	}
	public String getMsg_decision() {
		return msg_decision;
	}
	public void setMsg_decision(String msg_decision) {
		this.msg_decision = msg_decision;
	}
	public String getMsg_decision2() {
		return msg_decision2;
	}
	public void setMsg_decision2(String msg_decision2) {
		this.msg_decision2 = msg_decision2;
	}
	public int getRule_cnt2() {
		return rule_cnt2;
	}
	public void setRule_cnt2(int rule_cnt2) {
		this.rule_cnt2 = rule_cnt2;
	}
	public int getSnm_10() {
		return snm_10;
	}
	public void setSnm_10(int snm_10) {
		this.snm_10 = snm_10;
	}

}

