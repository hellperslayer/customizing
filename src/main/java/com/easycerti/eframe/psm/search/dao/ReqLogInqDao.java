package com.easycerti.eframe.psm.search.dao;

import java.util.List;

import com.easycerti.eframe.psm.search.vo.ReqLogInq;
import com.easycerti.eframe.psm.search.vo.SearchSearch;

public interface ReqLogInqDao {
	
	public List<ReqLogInq> findReqLogInqList(SearchSearch search);
	public int findReqLogInqOne_count(SearchSearch search);
	public ReqLogInq findReqLogIngDetail(SearchSearch search);
	public List<ReqLogInq> findReqLogIngDetailResult(SearchSearch search);
	public int findReqLogInqDetailResultCount(SearchSearch search);
}
