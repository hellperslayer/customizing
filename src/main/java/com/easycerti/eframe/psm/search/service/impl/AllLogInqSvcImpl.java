package com.easycerti.eframe.psm.search.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.xmlbeans.impl.xb.xsdschema.All;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import com.easycerti.eframe.common.dao.BootInitialDao;
import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.BeanUtils;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.EncryptStrGenerator;
import com.easycerti.eframe.common.util.JDBCConnectionUtil;
import com.easycerti.eframe.common.util.PageInfo;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.PrivacyType;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.core.web.RequestWrapper;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.dashboard.vo.Dashboard;
import com.easycerti.eframe.psm.search.dao.AllLogInqDao;
import com.easycerti.eframe.psm.search.dao.ExtrtCondbyInqDao;
import com.easycerti.eframe.psm.search.service.AllLogInqSvc;
import com.easycerti.eframe.psm.search.vo.AllLogInq;
import com.easycerti.eframe.psm.search.vo.AllLogInqList;
import com.easycerti.eframe.psm.search.vo.AllLogSql;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInq;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInqDetailChart;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.search.vo.SummonReceve;
import com.easycerti.eframe.psm.setup.dao.ExtrtBaseSetupDao;
import com.easycerti.eframe.psm.setup.dao.IndvinfoTypeSetupDao;
import com.easycerti.eframe.psm.setup.dao.SqlMappSetupDao;
import com.easycerti.eframe.psm.setup.vo.DLogMenuMappSetup;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;
import com.easycerti.eframe.psm.setup.vo.SqlMappSetup;
import com.easycerti.eframe.psm.system_management.dao.AgentMngtDao;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;
import com.easycerti.eframe.psm.system_management.vo.Misdetect;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

/**
 * 
 * 설명 : 전체로그조회 Service Implements
 * @author tjlee
 * @since 2015. 4. 22.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 22.           tjlee            최초 생성
 *
 * </pre>
 */
@Service
public class AllLogInqSvcImpl implements AllLogInqSvc {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private AllLogInqDao allLogInqDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;

	@Autowired
	private AgentMngtDao agentMngtDao;
	
	@Autowired
	private ExtrtCondbyInqDao extrtCondbyInqDao;

	@Autowired
	private ExtrtBaseSetupDao extrtBaseSetupDao;
	
	@Autowired
	BootInitialDao bDao;
	
	@Autowired
	private IndvinfoTypeSetupDao indvinfoTypeSetupDao;
	
	@Autowired
	private SqlMappSetupDao sqlMappSetupDao;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;
	
	@Value("#{configProperties.sub_className}")
	private String sub_className;
	
	@Value("#{configProperties.sub_url}")
	private String sub_url;
	
	@Value("#{configProperties.sub_id}")
	private String sub_id;
	
	@Value("#{configProperties.sub_pw}")
	private String sub_pw;
	
	@Value("#{configProperties.sub_sql}")
	private String sub_sql;
	
	@Value("#{configProperties.pacs_className}")
	private String pacs_className;
	
	@Value("#{configProperties.pacs_url}")
	private String pacs_url;
	
	@Value("#{configProperties.pacs_id}")
	private String pacs_id;
	
	@Value("#{configProperties.pacs_pw}")
	private String pacs_pw;
	
	@Value("#{configProperties.pacs_sql}")
	private String pacs_sql;
	
	@Value("#{dbmsProperties.url}")
	private String psm_url;
	
	@Value("#{dbmsProperties.username}")
	private String psm_id;
	
	@Value("#{dbmsProperties.password}")
	private String psm_pw;
	
	@Value("#{dbmsProperties.driverClassName}")
	private String psm_className;
	
	@Value("#{configProperties.log_sql_system}")
	private String log_sql_system;

	@Override
	public DataModelAndView findAllLogInqList(SearchSearch search) {
		/*
		 * 통계에서 년별,월별,일별 날짜가 들어오면
		 * 해당 날짜에 맞춰 년월일이 전부 나오도록 한다.
		 * ex) 2016 -> 20160101 , 20161231
		 */
		String time_check = search.getCheck_times();
		String date_sub = search.getSearch_from();
		
		String emp_user_name = search.getEmp_user_name();
		if (emp_user_name != null) {
			emp_user_name = emp_user_name.replaceAll("& #40;", "(").replaceAll("& #41;", ")");
			search.setEmp_user_name(emp_user_name);
		}
		
		String dept_name = search.getDept_name();
		if (dept_name != null) {
			dept_name = dept_name.replaceAll("& #40;", "(").replaceAll("& #41;", ")");
			search.setDept_name(dept_name);
		}
		
		if(search.getReq_url() != null) {
			search.setReq_url(RequestWrapper.decodeXSS(search.getReq_url()));
		}
		
		Calendar calendar = Calendar.getInstance();

		if("MONTH".equals(time_check))
		{ 
			
			int year =  Integer.parseInt(date_sub.substring(0, 4));
			int month = Integer.parseInt(date_sub.substring(5, date_sub.length()))-1;
			int date = 1;
			String realmonth = date_sub.substring(4, date_sub.length());
			String realdate = "01";			
			calendar.set(year, month, date);

			int lastDay = calendar.getActualMaximum(Calendar.DATE);
			
			String start_date = Integer.toString(year) + realmonth + realdate;
			String end_date = Integer.toString(year) + realmonth + Integer.toString(lastDay);
			search.setSearch_from(start_date);
			search.setSearch_to(end_date);	
		}
		else if("YEAR".equals(time_check))
		{
		
			date_sub = search.getSearch_from();
				
			int year =  Integer.parseInt(date_sub);
			String month = "01";
			int date = 1;
			String realdate = "01";			
			calendar.set(year, 11, date);
			int lastDay = calendar.getActualMaximum(Calendar.DATE);
			
			String start_date = Integer.toString(year) + month + realdate;
			String end_date = Integer.toString(year) + "12" + Integer.toString(lastDay);
			search.setSearch_from(start_date);
			search.setSearch_to(end_date);				
		}
		
		if(search.getStart_h() != null) {
			String start_time = search.getStart_h().substring(0);
			if(start_time.length() < 2) {
				start_time = "0" + start_time;
			}
			search.setStart_time(start_time);
		}
		if(search.getEnd_h() != null) {
			String end_time = search.getEnd_h().substring(0);
			if(end_time.length() < 2) {
				end_time = "0" + end_time;
			}
			search.setEnd_time(end_time);
		}

		SimpleCode simpleCode = new SimpleCode();
		String index = "/allLogInq/list.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		//2016_12_27 by hjpark
		
		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		long count = allLogInqDao.findAllLogInqOne_count(search);
		search.setCount(count);
		

		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList_byAdmin(search);
		List<AllLogInq> allLogInq = allLogInqDao.findAllLogInqList(search);
		
		for(int i=0; i<allLogInq.size(); i++) {
			AllLogInq ali = allLogInq.get(i);
			String result_type = ali.getResult_type();
			ali.setResultTypeMap(result_type);
			if(result_type != null) {
				String[] arrResult_type = result_type.split(",");
				Arrays.sort(arrResult_type);
				String res = "";
				for(int j=0; j<arrResult_type.length; j++) {
					if(j ==0)
						res += arrResult_type[j];
					else
						res += "," + arrResult_type[j];
				}
				ali.setResult_type(res);
			}
		}
		
		String checkEmpNameMasking = commonDao.checkEmpNameMasking();
		if(checkEmpNameMasking.equals("Y")) {
			for (int i = 0; i < allLogInq.size(); i++) {
				if(!"".equals(allLogInq.get(i).getEmp_user_name())) {
					String empUserName = allLogInq.get(i).getEmp_user_name();
					StringBuilder builder = new StringBuilder(empUserName);
					builder.setCharAt(empUserName.length() - 2, '*');
					allLogInq.get(i).setEmp_user_name(builder.toString());
				}
			}
		}
		
		AllLogInqList allLogInqList = new AllLogInqList(allLogInq);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("allLogInqList", allLogInqList);
		modelAndView.addObject("systemMasterList", systemMasterList);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView findAllLogInqDetail(SearchSearch search, HttpServletRequest request) {

		int pageSize = 5;
		search.getAllLogInqDetail().setSize(pageSize);
		
		int cnt = allLogInqDao.findAllLogInqDetailResultCount(search);
		search.getAllLogInqDetail().setTotal_count(cnt);
		
		String ui_type = commonDao.getUiType();
		search.setUi_type(ui_type+"_TYPE");
		
		List<ExtrtCondbyInqDetailChart> data2 = new ArrayList<>();
		AllLogInq allLogInq = new AllLogInq();
		List<AllLogInq> allLogInqDetailList = new ArrayList<AllLogInq>();
		List<AllLogInq> allLogInqDetailList2 = new ArrayList<AllLogInq>();
		
		HttpSession session = request.getSession();
		String biz_log_file = (String) session.getAttribute("sbiz_log_file");
		if (biz_log_file != null && biz_log_file.equals("Y")) {
			search.setCheck_file("true");
		}
		String insert_biz_log_result = (String) session.getAttribute("insert_biz_log_result");
		if(search.getResult_owner_flag() != null && search.getResult_owner_flag().equals("Y")) {
			search.setResult_owner("true");
		}
		try{
			allLogInq = allLogInqDao.findAllLogInqDetail(search);
			String result_type = allLogInq.getResult_type();
			if(result_type != null) {
				String[] arrResult_type = result_type.split(",");
				Arrays.sort(arrResult_type);
				String res = "";
				for(int j=0; j<arrResult_type.length; j++) {
					if(j ==0)
						res += arrResult_type[j];
					else
						res += "," + arrResult_type[j];
				}
				allLogInq.setResult_type(res);
			}
			String checkEmpNameMasking = commonDao.checkEmpNameMasking();
			if(checkEmpNameMasking.equals("Y")) {
				if(!"".equals(allLogInq.getEmp_user_name())) {
					String empUserName = allLogInq.getEmp_user_name();
					StringBuilder builder = new StringBuilder(empUserName);
					builder.setCharAt(empUserName.length() - 2, '*');
					allLogInq.setEmp_user_name(builder.toString());
				}
			}
			if(allLogInq.getEmp_user_id() != null) {
				search.setEmp_user_idD2(allLogInq.getEmp_user_id());
				data2 = extrtCondbyInqDao.getExtrtCondbyInqDetail2_1(search);
			}
			search.setSystem_seq_temp(allLogInq.getSystem_seq());
			if ("M".equals(ui_type)) {
				allLogInqDetailList = allLogInqDao.findAllLogInqDetailResultOwner(search);
			} else {
				allLogInqDetailList = allLogInqDao.findAllLogInqDetailResult(search);
				for(AllLogInq v : allLogInqDetailList){
					if(v.getResult_content() != null && v.getResult_content() != ""){
						// 보훈병원
						if (ui_type.equals("B") && sub_className.length() > 0) {
							if (v.getResult_type() != null && v.getPrivacy_seq() == 11) {
								Object[] param = {v.getResult_content()};
								Map findColumn = new HashMap<>();
								findColumn.put("pt_name", "string");
								List res = JDBCConnectionUtil.ConnectionResult(sub_className, sub_url, sub_id, sub_pw, sub_sql, param, findColumn);
								if (res.size() > 0) {
									Map resMap = (Map) res.get(0);
									if (resMap.get("pt_name") != null)
										v.setEmp_user_name((String)resMap.get("pt_name"));
								}
							} else if (v.getResult_type() != null && v.getPrivacy_seq() == 12) {
									Object[] param = {v.getResult_content()};
									Map findColumn = new HashMap<>();
									findColumn.put("patId", "string");
									List res = JDBCConnectionUtil.ConnectionResult(pacs_className, pacs_url, pacs_id, pacs_pw, pacs_sql, param, findColumn);
									if (res.size() > 0) {
										Map resMap = (Map) res.get(0);
										if (resMap.get("patId") != null) {
											Object[] param1 = {resMap.get("patId")};
											Map findColumn2 = new HashMap<>();
											findColumn2.put("pt_name", "string");
											List res1 = JDBCConnectionUtil.ConnectionResult(sub_className, sub_url, sub_id, sub_pw, sub_sql, param1, findColumn2);
											if (res1.size() > 0) {
												Map resMap1 = (Map) res1.get(0);
												if (resMap1.get("pt_name") != null)
													v.setEmp_user_name((String)resMap1.get("pt_name"));
											}
										}
									}
								}
						}
						
						String pResult = v.getResult_content();
						if(v.getResult_type() != null && v.getPrivacy_masking() != null && v.getPrivacy_masking().equals("Y") && insert_biz_log_result.equals("Y")) {
							pResult = getResultContextByMasking(pResult, v.getResult_type());
						}
						v.setResult_content_masking(pResult);
					}
				}
			}
			allLogInqDetailList2 = allLogInqDao.findAllLogInqDetailResult2(search);
		}catch(Exception e) {
			logger.error("[AllLogInqSvcImpl.findAllLogInqDetail] MESSAGE : " + e.getMessage());
			throw new ESException("SYS005V");
		}
		
		SimpleCode simpleCode = new SimpleCode();
		String index = "/allLogInq/detail.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("allLogInq", allLogInq);
		modelAndView.addObject("allLogInqDetailList", allLogInqDetailList);
		modelAndView.addObject("allLogInqDetailList2", allLogInqDetailList2);
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("ui_type", ui_type);
		modelAndView.addObject("biz_log_file", biz_log_file);
		modelAndView.addObject("insert_biz_log_result", insert_biz_log_result);
		if ( data2.size() > 0 ) {
			modelAndView.addObject("data2", data2);
		}
		if (search.getResult_owner_flag() != null && search.getResult_owner_flag().equals("Y")) { 
			  search.setResult_owner("true");
			  modelAndView.addObject("result_owner_falg", search.getResult_owner_flag());
		}
		String biz_log_sql = (String) session.getAttribute("sbiz_log_sql");
		if(biz_log_sql != null && biz_log_sql.equals("Y")) {
			// 여주대학교 커스터마이징
			if (ui_type.equals("Y") && allLogInq.getSystem_seq().equals("04")) {
				AllLogInq detail_query = allLogInqDao.findAllLogInqDetailQuery_YJ(search);
				modelAndView.addObject("detail_query", detail_query);
			} else {
				AllLogInq detail_query = allLogInqDao.findAllLogInqDetailQuery(search);
				if(detail_query != null) {
					String tmpList[] = detail_query.getQuery_id().split("\r\n");
					List<String> querylist = new ArrayList<String>();
					List<String> querylist2 = new ArrayList<String>();
					for ( int i=0; i< tmpList.length; ++i ) {
						if ( !tmpList[i].equals("0")) {
							querylist.add(tmpList[i]);
						}
					}
					for(int i=0; i<querylist.size(); i++) {
						if(querylist.get(i).length() >0) {
							querylist2.add(querylist.get(i));
						}
					}
					modelAndView.addObject("querylist2", querylist2);
					modelAndView.addObject("querylistSize", querylist2.size());
					modelAndView.addObject("detail_query", detail_query);
				}
			}
		}
		
		String biz_log_body_req = (String) session.getAttribute("sbiz_log_body_req");
		String biz_log_body_res = (String) session.getAttribute("sbiz_log_body_res");
		if((biz_log_body_req != null && biz_log_body_req.equals("Y")) || (biz_log_body_res != null && biz_log_body_res.equals("Y"))) {
			AllLogInq detail_body = allLogInqDao.findAllLogInqDetailBody(search);
			modelAndView.addObject("detail_body", detail_body);
		}
		
		if(biz_log_file != null && biz_log_file.equals("Y")) {
			List<AllLogInq> allLogInqFileList = allLogInqDao.findLogInqFileList(search);
			modelAndView.addObject("allLogInqFileList", allLogInqFileList);
			List<AllLogInq> allLogInqApprovalList = allLogInqDao.findLogInqApprovalList(search);
			modelAndView.addObject("allLogInqApprovalList", allLogInqApprovalList);
		}
		modelAndView.addObject("biz_log_sql", biz_log_sql);
		modelAndView.addObject("biz_log_body_req", biz_log_body_req);
		modelAndView.addObject("biz_log_body_res", biz_log_body_res);
		modelAndView.addObject("mode_info_viewer", commonDao.getInfoViewerUse());
		modelAndView.addObject("resultTypeList", allLogInqDao.getResultTypeListByLogseq(search));
		String sbiz_log_filedownload = (String) session.getAttribute("sbiz_log_filedownload");
		modelAndView.addObject("sbiz_log_filedownload", sbiz_log_filedownload);
		
		
		String[] logSqlSystem = log_sql_system.split(",");
		String system_seq = allLogInq.getSystem_seq();
		String log_sql_yn = "N";
		for(String seq : logSqlSystem) {
			if(system_seq.equals(seq.trim())) {
				log_sql_yn = "Y";
			}
		}
		modelAndView.addObject("log_sql_yn", log_sql_yn);
		if(log_sql_yn.equals("Y")) {
			// sql
			modelAndView.addObject("tab_flag", search.getTab_flag());
			AllLogSql allLogSql = new AllLogSql();
			allLogSql.setLog_seq(Long.parseLong(search.getDetailLogSeq()));
			allLogSql.setProc_datetime(allLogInq.getProc_date());
			// sql count
			int logSqlResultCt = allLogInqDao.allLogSqlResultCt(allLogSql);
			modelAndView.addObject("logSqlResultCt", logSqlResultCt);
			List<String> sqlPrivacyInfo = allLogInqDao.allLogSqlCtAndHasPrivacyInfo(allLogSql);
			modelAndView.addObject("sqlPrivacyInfo", sqlPrivacyInfo);
			if(sqlPrivacyInfo.size()!=0) {
				// SQL 정보 가져옴
				allLogSql.setLogSqlPageNum(Integer.parseInt(search.getLogSqlPageNum()));
				AllLogSql logSqlInfo = allLogInqDao.allLogSqlInfo(allLogSql);
				String[] params = logSqlInfo.getParams().split("\\|");
				logSqlInfo.setParamsList(Arrays.asList(params));
				String[] columns = logSqlInfo.getColumns().split("\\|");
				logSqlInfo.setColumnsList(Arrays.asList(columns));
				modelAndView.addObject("logSqlInfo", logSqlInfo);
				// SQL 결과 가져옴
				allLogSql.setLog_sql_seq(logSqlInfo.getLog_sql_seq());
				PageInfo logSqlResult = allLogSql.getLogSqlResult();
				logSqlResult.setPage_num(Integer.parseInt(search.getLogSqlResultPageNum()));
				logSqlResult.setSize(10);
				logSqlResult.setTotal_count(allLogInqDao.allLogSqlResultListCt(allLogSql));
				List<AllLogSql> logSqlInfoResult = allLogInqDao.allLogSqlResultList(allLogSql);
				for(AllLogSql als : logSqlInfoResult) {
					String[] result = als.getResult().split("\\|");
					als.setResultList(Arrays.asList(result));
				}
				modelAndView.addObject("logSqlInfoResult", logSqlInfoResult);
			}
			modelAndView.addObject("allLogSql", allLogSql);
		}
		
		return modelAndView;
	}
	
	
	
	@Override
	public String updateSqlCollectYn(AllLogSql allLogSql) {
		try {
			allLogInqDao.updateallLogSqlCollectYn(allLogSql);
			return "success";
		} catch (Exception e) {
			return "error";
		}
	}


	@Override
	public void logSqlDownloadExcel(DataModelAndView modelAndView, AllLogSql allLogSql) {
		allLogSql.getLogSqlResult().setUseExcel("true");
		String fileName = "PSM_접속기록참조정보_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		fileName += "("+allLogSql.getLog_seq() + ")";
		List<AllLogSql> logSqlSeqList = allLogInqDao.logSqlSeqList(allLogSql);
		
		List<Map<String, Object>> modelList = new ArrayList<>();
		for(int k=0; k<logSqlSeqList.size();k++) {
			int log_sql_seq = logSqlSeqList.get(k).getLog_sql_seq();
			
			int noVal = k+1;
			String sheetName = "SQL결과_"+noVal;
			allLogSql.setLog_sql_seq(log_sql_seq);
			AllLogSql logSqlInfo = allLogInqDao.allLogSqlInfo(allLogSql);
			String[] summary = new String[4];
			summary[0] = "SQL";
			summary[1] = "파라미터";
			summary[2] = logSqlInfo.getSql();
			String[] param = logSqlInfo.getParams().split("\\|");
			String paramTemp = "";
			int paramLength = param.length;
			if(paramLength==1) {
				if(param[0]==null||param[0].equals("")) {
					paramTemp = "※ 파라미터 없음";
				} else {
					paramTemp += "1 : "+param[0];
				}
			} else {
				for(int i=0; i<paramLength;i++) {
					int no = i+1;
					if(no==param.length) {
						paramTemp += no+" : "+param[i];
					} else {
						paramTemp += no+" : "+param[i]+", ";
					}
				}
			}
			summary[3] = paramTemp;
			
			List<AllLogSql> logSqlInfoResult = allLogInqDao.allLogSqlResultList(allLogSql);
			String[] columns = logSqlInfo.getColumns().split("\\|");
			String[] heads = logSqlInfo.getColumns().split("\\|");
			List<Map<String, String>> excelData = new ArrayList<>();
			for(AllLogSql als : logSqlInfoResult) {
				String[] result = als.getResult().split("\\|");
				Map<String, String> map = new HashMap<>();
				for(int i=0; i<result.length; i++) {
					map.put(columns[i], result[i]);
				}
				excelData.add(map);
			}
			Map<String, Object> temp = new HashMap<>();
			temp.put("sheetName", sheetName);
			temp.put("excelData", excelData);
			temp.put("columns", columns);
			temp.put("heads", heads);
			temp.put("summary", summary);
			temp.put("sheetNum", k);
			modelList.add(temp);
		}
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("modelList", modelList);
		
	}


	@Override
	public void logSqlDownloadCsv(DataModelAndView modelAndView, AllLogSql allLogSql) {
		allLogSql.getLogSqlResult().setUseExcel("true");
		String fileName = "PSM_SQL결과정보_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		AllLogSql logSqlInfo = allLogInqDao.allLogSqlInfo(allLogSql);
		List<AllLogSql> logSqlInfoResult = allLogInqDao.allLogSqlResultList(allLogSql);
		String[] columns = logSqlInfo.getColumns().split("\\|");
		String[] heads = logSqlInfo.getColumns().split("\\|");
		List<Map<String, String>> excelData = new ArrayList<>();
		for(AllLogSql als : logSqlInfoResult) {
			String[] result = als.getResult().split("\\|");
			Map<String, String> map = new HashMap<>();
			for(int i=0; i<result.length; i++) {
				map.put(columns[i], result[i]);
			}
			excelData.add(map);
		}
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("excelData", excelData);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}


	@Override
	public DataModelAndView findSummonAllLogInqDetail(SearchSearch search) {

		int pageSize = Integer.valueOf(defaultPageSize);
		search.getAllLogInqDetail().setSize(pageSize);
		search.getAllLogInqDetail().setTotal_count(allLogInqDao.findAllLogInqDetailResultCount(search));
		
		AllLogInq allLogInq = allLogInqDao.findAllLogInqDetail(search);
		
		List<AllLogInq> allLogInqDetailList = allLogInqDao.findAllLogInqDetailResult(search);
		
		for(AllLogInq v : allLogInqDetailList){
			if(v.getResult_content() != null && v.getResult_content() != ""){
				
				String pResult = v.getResult_content();
				if( v.getResult_type() != null && "주민등록번호".equals(v.getResult_type()) ) {
					pResult = pResult.substring(0,7)+"*******";
				}
				
				v.setResult_content(pResult);
			}
		}

		ExtrtCondbyInq summon = allLogInqDao.finallLogInqListByEmpCd(search);
		SimpleCode simpleCode = new SimpleCode();
		String index = "/allLogInq/allLogInqSummonDetailView.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		// 소명요청했는지 확인.. 있으면 소명요청 안되게
		String exist = allLogInqDao.findSummon(String.valueOf(search.getDetailLogSeq()));
		DataModelAndView modelAndView = new DataModelAndView();
		
		String responseSampleText = " [TMON] 개인정보 처리시스템 접근 기록에 대한 소명 응답 요청 \r\n \r\n    · 요청 항목 : "+ summon.getRule_nm() +"\r\n    · 요청 사유 : " ;
		modelAndView.addObject("responseSampleText", responseSampleText);
		
		
		modelAndView.addObject("allLogInq", allLogInq);
		modelAndView.addObject("allLogInqDetailList", allLogInqDetailList);
		modelAndView.addObject("index_id", index_id);
		
		modelAndView.addObject("summon", summon);
		modelAndView.addObject("exist", exist);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView addPrivacy(Map<String, String> parameters){
		Misdetect paramBean = CommonHelper.convertMapToBean(parameters, Misdetect.class);
		
		if("1".equals(paramBean.getPrivacy_type())){
			String privPattern = allLogInqDao.checkPattern(paramBean); 
			paramBean.setMisdetect_pattern(privPattern);
		}else {
			String privPattern = allLogInqDao.checkPattern(paramBean); 
			paramBean.setMisdetect_pattern(privPattern);
		}	
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			allLogInqDao.addPrivacy(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[AllLogInqSvcImpl.addPrivacy] MESSAGE : " + e.getMessage());
			throw new ESException("SYS030J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView removePrivacy(Map<String, String> parameters) {
		Misdetect paramBean = CommonHelper.convertMapToBean(parameters, Misdetect.class);
		
		if("1".equals(paramBean.getPrivacy_type())){
			String privPattern = allLogInqDao.checkPattern(paramBean); 
			paramBean.setMisdetect_pattern(privPattern);
		}else {
			String privPattern = allLogInqDao.checkPattern(paramBean); 
			paramBean.setMisdetect_pattern(privPattern);
		}
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			allLogInqDao.removePrivacy(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[AllLogInqSvcImpl.removePrivacy] MESSAGE : " + e.getMessage());
			throw new ESException("SYS030J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView addGoogle(Map<String, String> parameters){
		Dashboard paramBean = CommonHelper.convertMapToBean(parameters, Dashboard.class);
				
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			allLogInqDao.addGoogle(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[IpPermSvcimpl.addIpPerm] MESSAGE : " + e.getMessage());
			throw new ESException("SYS030J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView updateGoogle(Map<String, String> parameters){
		Dashboard paramBean = CommonHelper.convertMapToBean(parameters, Dashboard.class);
				
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			allLogInqDao.updateGoogle(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[IpPermSvcimpl.addIpPerm] MESSAGE : " + e.getMessage());
			throw new ESException("SYS030J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView deleteGoogle(Map<String, String> parameters){
		Dashboard paramBean = CommonHelper.convertMapToBean(parameters, Dashboard.class);
				
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			allLogInqDao.deleteGoogle(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[IpPermSvcimpl.addIpPerm] MESSAGE : " + e.getMessage());
			throw new ESException("SYS030J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView findDashPopupList(Map<String, String> parameters) {
		
		List<Dashboard> items = allLogInqDao.findDashboardList();
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		modelAndView.addObject("items", items);
		
		return modelAndView;
	}
	
	@Override
	public void findAllLogInqList_download(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request) {

		search.setUseExcel("true");
		
		String fileName = "PSM_개인정보_접속기록조회_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "개인정보 접속기록조회";
		
		if(search.getStart_h() != null) {
			String start_time = search.getStart_h().substring(0);
			if(start_time.length() < 2)
				start_time = "0" + start_time;
			search.setStart_time(start_time);
		}
		if(search.getEnd_h() != null) {
			String end_time = search.getEnd_h().substring(0);
			if(end_time.length() < 2)
				end_time = "0" + end_time;
			search.setEnd_time(end_time);
		}

		List<AllLogInq> allLogInq = allLogInqDao.findAllLogInqList(search);
		List<PrivacyType> privacyTypeList = commonDao.getPrivacyTypesExceptRegular();
		SimpleDateFormat parseSdf = new SimpleDateFormat("yyyyMMddHHmmss");
		SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
		
		HttpSession session = request.getSession();
		String scrn_name_view = (String) session.getAttribute("scrn_name");
		String ui_type = (String) session.getAttribute("ui_type");
		
		for(int i=0; i<allLogInq.size(); i++){
			/*String result_type[] = allLogInq.get(i).getResult_type().split(",");
			
			String result_type_string = "";
			for(int j=0; j<result_type.length; j++){
				for(int k=0; k<privacyTypeList.size(); k++){
					if(privacyTypeList.get(k).getPrivacy_type().equals(result_type[j]) ) result_type_string += privacyTypeList.get(k).getPrivacy_desc(); 
				}
			}*/
			AllLogInq ali = allLogInq.get(i);
			String result_type = ali.getResult_type();
			if(result_type != null) {
				String[] arrResult_type = result_type.split(",");
				Arrays.sort(arrResult_type);
				
				String result_type_string = "";
				String prev = "";
				int count = 1;
				for(int j=0; j<arrResult_type.length; j++) {
					if(prev.equals(arrResult_type[j])) {
						count++;
					}else {
						if(j != 0) {
							result_type_string += " " +count + ", ";
						}
						
						for(int k=0; k<privacyTypeList.size(); k++){
							if(privacyTypeList.get(k).getPrivacy_type().equals(arrResult_type[j])) { 
								result_type_string += privacyTypeList.get(k).getPrivacy_desc(); 
							}
						}
						count = 1;
						prev = arrResult_type[j];
					}
					
					if(j == arrResult_type.length - 1) {
						result_type_string += " " +count;
					}
				}
				
				allLogInq.get(i).setCnt(arrResult_type.length);
				allLogInq.get(i).setResult_type(result_type_string);
			}
			
			String req_type_en=allLogInq.get(i).getReq_type();
			switch(req_type_en){
				case "RD": allLogInq.get(i).setReq_type("조회");break;
				case "CR": allLogInq.get(i).setReq_type("등록");break;
				case "UD": allLogInq.get(i).setReq_type("수정");break;
				case "DL": allLogInq.get(i).setReq_type("삭제");break;
				case "DN": allLogInq.get(i).setReq_type("다운로드");break;
				case "PR": allLogInq.get(i).setReq_type("출력");break;
				case "CO": allLogInq.get(i).setReq_type("수집");break;
				case "NE": allLogInq.get(i).setReq_type("생성");break;
				case "BE": allLogInq.get(i).setReq_type("연계");break;
				case "IN": allLogInq.get(i).setReq_type("연동");break;
				case "WR": allLogInq.get(i).setReq_type("기록");break;
				case "SA":
				case "SV": allLogInq.get(i).setReq_type("저장");break;
				case "SU": allLogInq.get(i).setReq_type("보유");break;
				case "FI": allLogInq.get(i).setReq_type("가공");break;
				case "UP": allLogInq.get(i).setReq_type("편집");break;
				case "SC": allLogInq.get(i).setReq_type("검색");break;
				case "CT": allLogInq.get(i).setReq_type("정정");break;
				case "RE": allLogInq.get(i).setReq_type("복구");break;
				case "US": allLogInq.get(i).setReq_type("이용");break;
				case "OF": allLogInq.get(i).setReq_type("제공");break;
				case "OP": allLogInq.get(i).setReq_type("공개");break;
				case "AN": allLogInq.get(i).setReq_type("파기");break;
				case "EX": allLogInq.get(i).setReq_type("실행");break;
				case "PRRD": allLogInq.get(i).setReq_type("출력(RD)");break;
				case "PREX": allLogInq.get(i).setReq_type("출력(Excel)");break;
			}
			
			if(allLogInq.get(i).getDept_name().equals("") || allLogInq.get(i).getDept_name().equals(null)) 
				allLogInq.get(i).setDept_name("시스템");
			if(allLogInq.get(i).getEmp_user_name().equals("") || allLogInq.get(i).getEmp_user_name().equals(null)) 
				allLogInq.get(i).setEmp_user_name("시스템");
			
			try {
				allLogInq.get(i).setProc_date(sdf.format(parseSdf.parse(allLogInq.get(i).getProc_date() + allLogInq.get(i).getProc_time())));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			/*if(allLogInq.get(i).getScrn_name() == null) {
				allLogInq.get(i).setScrn_name(allLogInq.get(i).getReq_url());
			}*/
			
			if(scrn_name_view.equals("2")) {
				if (allLogInq.get(i).getScrn_name() != null && allLogInq.get(i).getScrn_name().length() > 0) {
					String tmpReqUrl = allLogInq.get(i).getScrn_name();
					if(ui_type.equals("K")) {
						tmpReqUrl = allLogInq.get(i).getScrn_name() + "(" + allLogInq.get(i).getScrn_id() + ")";
					} 
					allLogInq.get(i).setReq_url(tmpReqUrl);
				}
			}
		}
		
		String[] columns;
		String[] heads;
		if(scrn_name_view.equals("1")) {
			columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "result_type", "cnt", "req_type", "scrn_name", "req_url"};
			heads = new String[] {"일시", "소속", "취급자ID", "취급자명", "취급자IP", "시스템명", "정보주체식별정보", "합계", "수행업무", "메뉴명", "접근 경로"};
		}else if(scrn_name_view.equals("2")){
			columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "result_type", "cnt", "req_type", "req_url"};
			String tmpName = "접근경로";
			if(ui_type.equals("K")) {
				tmpName = "메뉴명";
			} 
			heads = new String[] {"일시", "소속", "취급자ID", "취급자명", "취급자IP", "시스템명", "정보주체식별정보", "합계", "수행업무", tmpName};
		}else {
			columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "result_type", "cnt", "req_type"};
			heads = new String[] {"일시", "소속", "취급자ID", "취급자명", "취급자IP", "시스템명", "정보주체식별정보", "합계", "수행업무"};
		}
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", allLogInq);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}
	
	@Override
	public void findAllLogInqList_download_extend(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request) {

		search.setUseExcel("true");
		
		String fileName = "PSM_개인정보_접속기록조회_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "개인정보 접속기록조회";
		
		if(search.getStart_h() != null) {
			String start_time = search.getStart_h().substring(0);
			if(start_time.length() < 2)
				start_time = "0" + start_time;
			search.setStart_time(start_time);
		}
		if(search.getEnd_h() != null) {
			String end_time = search.getEnd_h().substring(0);
			if(end_time.length() < 2)
				end_time = "0" + end_time;
			search.setEnd_time(end_time);
		}
		
		List<AllLogInq> allLogInq = allLogInqDao.findAllLogInqList(search);
		List<PrivacyType> privacyTypeList = commonDao.getPrivacyTypesExceptRegular();
		List<Code> result_type_list = commonDao.getResultTypesByCode();
		SimpleDateFormat parseSdf = new SimpleDateFormat("yyyyMMddHHmmss");
		SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
		
		for(int i=0; i<allLogInq.size(); i++){
			/*String result_type[] = allLogInq.get(i).getResult_type().split(",");
			
			String result_type_string = "";
			for(int j=0; j<result_type.length; j++){
				for(int k=0; k<privacyTypeList.size(); k++){
					if(privacyTypeList.get(k).getPrivacy_type().equals(result_type[j]) ) result_type_string += privacyTypeList.get(k).getPrivacy_desc(); 
				}
			}*/
			AllLogInq ali = allLogInq.get(i);
			List<AllLogInq> result_types = commonDao.getResultTypesByLogSeq(ali);
			
			String result_type_string = "";
			String prev = "";
			int count = 1;
			for(int j=0; j<result_types.size(); j++) {
				if(prev.equals(result_types.get(j).getResult_type())) {
					count++;
				}else {
					if(j != 0) {
						result_type_string += " " +count + ", ";
						setData(ali, Integer.parseInt(prev), count);
					}
					
					for(int k=0; k<privacyTypeList.size(); k++){
						if(privacyTypeList.get(k).getPrivacy_type().equals(result_types.get(j).getResult_type())) { 
							result_type_string += privacyTypeList.get(k).getPrivacy_desc(); 
						}
					}
					count = 1;
					prev = result_types.get(j).getResult_type();
				}
				
				if(j == result_types.size() - 1) {
					result_type_string += " " +count;
					setData(ali, Integer.parseInt(result_types.get(j).getResult_type()), count);
				}
			}
			
			allLogInq.get(i).setCnt(result_types.size());
			String req_type_en=allLogInq.get(i).getReq_type();
			switch(req_type_en){
				case "RD": allLogInq.get(i).setReq_type("조회");break;
				case "CR": allLogInq.get(i).setReq_type("등록");break;
				case "UD": allLogInq.get(i).setReq_type("수정");break;
				case "DL": allLogInq.get(i).setReq_type("삭제");break;
				case "DN": allLogInq.get(i).setReq_type("다운로드");break;
				case "PR": allLogInq.get(i).setReq_type("출력");break;
				case "CO": allLogInq.get(i).setReq_type("수집");break;
				case "NE": allLogInq.get(i).setReq_type("생성");break;
				case "BE": allLogInq.get(i).setReq_type("연계");break;
				case "IN": allLogInq.get(i).setReq_type("연동");break;
				case "WR": allLogInq.get(i).setReq_type("기록");break;
				case "SA":
				case "SV": allLogInq.get(i).setReq_type("저장");break;
				case "SU": allLogInq.get(i).setReq_type("보유");break;
				case "FI": allLogInq.get(i).setReq_type("가공");break;
				case "UP": allLogInq.get(i).setReq_type("편집");break;
				case "SC": allLogInq.get(i).setReq_type("검색");break;
				case "CT": allLogInq.get(i).setReq_type("정정");break;
				case "RE": allLogInq.get(i).setReq_type("복구");break;
				case "US": allLogInq.get(i).setReq_type("이용");break;
				case "OF": allLogInq.get(i).setReq_type("제공");break;
				case "OP": allLogInq.get(i).setReq_type("공개");break;
				case "AN": allLogInq.get(i).setReq_type("파기");break;
				case "EX": allLogInq.get(i).setReq_type("실행");break;
				case "PRRD": allLogInq.get(i).setReq_type("출력(RD)");break;
				case "PREX": allLogInq.get(i).setReq_type("출력(Excel)");break;
			}
			
			if(allLogInq.get(i).getDept_name().equals("") || allLogInq.get(i).getDept_name().equals(null)) 
				allLogInq.get(i).setDept_name("시스템");
			if(allLogInq.get(i).getEmp_user_name().equals("") || allLogInq.get(i).getEmp_user_name().equals(null)) 
				allLogInq.get(i).setEmp_user_name("시스템");
			allLogInq.get(i).setResult_type(result_type_string);
			try {
				allLogInq.get(i).setProc_date(sdf.format(parseSdf.parse(allLogInq.get(i).getProc_date() + allLogInq.get(i).getProc_time())));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			/*if(allLogInq.get(i).getScrn_name() == null) {
				allLogInq.get(i).setScrn_name(allLogInq.get(i).getReq_url());
			}*/
			
			if (allLogInq.get(i).getScrn_name() != null && allLogInq.get(i).getScrn_name().length() > 0) {
				allLogInq.get(i).setReq_url(allLogInq.get(i).getScrn_name());
			}
		}
		
		String[] columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "result_type", "cnt", "req_type", "req_url"};
		String[] columns2 = new String[result_type_list.size() + 10];
		for(int i=0; i<columns.length; i++) 
			columns2[i] = columns[i];
		for(int i=0 ; i<result_type_list.size(); i++) 
			columns2[i+10] = "data" + (i + 1);
		
		String[] heads = new String[] {"일시", "소속", "취급자ID", "취급자명", "취급자IP", "시스템명", "정보주체식별정보", "합계", "수행업무", "접근 경로"};
		String[] heads2 = new String[result_type_list.size() + 10];
		for(int i=0; i<heads.length; i++) 
			heads2[i] = heads[i];
		for(int i=0 ; i<result_type_list.size(); i++) 
			heads2[i+10] = result_type_list.get(i).getCode_name();
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", allLogInq);
		modelAndView.addObject("columns", columns2);
		modelAndView.addObject("heads", heads2);
	}
	
	@Override
	public void findAllLogInqList_downloadCSV(DataModelAndView modelAndView, SearchSearch search,
			HttpServletRequest request) {
		
		search.setUseExcel("true");
		
		String fileName = "PSM_개인정보_접속기록조회_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		//String sheetName = "개인정보 로그조회";
		
		if(search.getStart_h() != null) {
			String start_time = search.getStart_h().substring(0);
			if(start_time.length() < 2)
				start_time = "0" + start_time;
			search.setStart_time(start_time);
		}
		if(search.getEnd_h() != null) {
			String end_time = search.getEnd_h().substring(0);
			if(end_time.length() < 2)
				end_time = "0" + end_time;
			search.setEnd_time(end_time);
		}

		List<AllLogInq> allLogInq = allLogInqDao.findAllLogInqList(search);
		List<PrivacyType> privacyTypeList = commonDao.getPrivacyTypesExceptRegular();
		
		SimpleDateFormat parseSdf = new SimpleDateFormat("yyyyMMddHHmmss");
		SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
		
		HttpSession session = request.getSession();
		String scrn_name_view = (String) session.getAttribute("scrn_name");
		String ui_type = (String) session.getAttribute("ui_type");
		
		for(int i=0; i<allLogInq.size(); i++){
			/*String result_type[] = allLogInq.get(i).getResult_type().split(",");
			
			String result_type_string = "";
			for(int j=0; j<result_type.length; j++){
				for(int k=0; k<privacyTypeList.size(); k++){
					if(privacyTypeList.get(k).getPrivacy_type().equals(result_type[j]) ) result_type_string += privacyTypeList.get(k).getPrivacy_desc(); 
				}
			}*/
			AllLogInq ali = allLogInq.get(i);
			String result_type = ali.getResult_type();
			if(result_type != null) {
				String[] arrResult_type = result_type.split(",");
				Arrays.sort(arrResult_type);
				String result_type_string = "";
				String prev = "";
				int count = 1;
				for(int j=0; j<arrResult_type.length; j++) {
					if(prev.equals(arrResult_type[j])) {
						count++;
					}else {
						if(j != 0){
							result_type_string += " " +count + ", ";
						}
						
						for(int k=0; k<privacyTypeList.size(); k++){
							if(privacyTypeList.get(k).getPrivacy_type().equals(arrResult_type[j])) { 
								result_type_string += privacyTypeList.get(k).getPrivacy_desc(); 
							}
						}
						count = 1;
						prev = arrResult_type[j];
					}
					
					if(j == arrResult_type.length - 1){
						result_type_string += " " +count;
					}
				}
				
				allLogInq.get(i).setCnt(arrResult_type.length);
				allLogInq.get(i).setResult_type(result_type_string);
			}
			
			String req_type_en=allLogInq.get(i).getReq_type();
			switch(req_type_en){
				case "RD": allLogInq.get(i).setReq_type("조회");break;
				case "CR": allLogInq.get(i).setReq_type("등록");break;
				case "UD": allLogInq.get(i).setReq_type("수정");break;
				case "DL": allLogInq.get(i).setReq_type("삭제");break;
				case "DN": allLogInq.get(i).setReq_type("다운로드");break;
				case "PR": allLogInq.get(i).setReq_type("출력");break;
				case "CO": allLogInq.get(i).setReq_type("수집");break;
				case "NE": allLogInq.get(i).setReq_type("생성");break;
				case "BE": allLogInq.get(i).setReq_type("연계");break;
				case "IN": allLogInq.get(i).setReq_type("연동");break;
				case "WR": allLogInq.get(i).setReq_type("기록");break;
				case "SA":
				case "SV": allLogInq.get(i).setReq_type("저장");break;
				case "SU": allLogInq.get(i).setReq_type("보유");break;
				case "FI": allLogInq.get(i).setReq_type("가공");break;
				case "UP": allLogInq.get(i).setReq_type("편집");break;
				case "SC": allLogInq.get(i).setReq_type("검색");break;
				case "CT": allLogInq.get(i).setReq_type("정정");break;
				case "RE": allLogInq.get(i).setReq_type("복구");break;
				case "US": allLogInq.get(i).setReq_type("이용");break;
				case "OF": allLogInq.get(i).setReq_type("제공");break;
				case "OP": allLogInq.get(i).setReq_type("공개");break;
				case "AN": allLogInq.get(i).setReq_type("파기");break;
				case "EX": allLogInq.get(i).setReq_type("실행");break;
				case "PRRD": allLogInq.get(i).setReq_type("출력(RD)");break;
				case "PREX": allLogInq.get(i).setReq_type("출력(Excel)");break;
			}
			
			if(allLogInq.get(i).getDept_name().equals("") || allLogInq.get(i).getDept_name().equals(null)) 
				allLogInq.get(i).setDept_name("-");
			if(allLogInq.get(i).getEmp_user_name().equals("") || allLogInq.get(i).getEmp_user_name().equals(null)) 
				allLogInq.get(i).setEmp_user_name("-");
			
			try {
				allLogInq.get(i).setProc_date(sdf.format(parseSdf.parse(allLogInq.get(i).getProc_date() + allLogInq.get(i).getProc_time())));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			/*if(allLogInq.get(i).getScrn_name() == null) {
				allLogInq.get(i).setScrn_name(allLogInq.get(i).getReq_url());
			}*/
			
			if(scrn_name_view.equals("2")) {
				if (allLogInq.get(i).getScrn_name() != null && allLogInq.get(i).getScrn_name().length() > 0) {
					String tmpReqUrl = allLogInq.get(i).getScrn_name();
					if(ui_type.equals("K")) {
						tmpReqUrl = allLogInq.get(i).getScrn_name() + "(" + allLogInq.get(i).getScrn_id() + ")";
					} 
					allLogInq.get(i).setReq_url(tmpReqUrl);
				}
			}
		}
		
		List<Map<String, Object>> list = new ArrayList<>();

		for(AllLogInq ali : allLogInq) {
			Map<String, Object> map = new HashMap<>();
			map.put("proc_date", ali.getProc_date());
			map.put("dept_name", ali.getDept_name());
			map.put("emp_user_id", ali.getEmp_user_id());
			map.put("emp_user_name", ali.getEmp_user_name());
			map.put("user_ip", ali.getUser_ip());
			map.put("system_name", ali.getSystem_name());
			map.put("result_type", ali.getResult_type());
			map.put("cnt", ali.getCnt());
			map.put("req_type", ali.getReq_type());
			map.put("scrn_name", ali.getScrn_name());
			map.put("req_url", ali.getReq_url());
			
			list.add(map);
		}
		
		String[] columns;
		String[] heads;
		if(scrn_name_view.equals("1")) {
			columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "result_type", "cnt", "req_type", "scrn_name", "req_url"};
			heads = new String[] {"일시", "소속", "취급자ID", "취급자명", "취급자IP", "시스템명", "정보주체식별정보", "합계", "수행업무", "메뉴명", "접근 경로"};
		}else if(scrn_name_view.equals("2")){
			String tmpName = "접근경로";
			if(ui_type.equals("K")) {
				tmpName = "메뉴명";
			} 
			columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "result_type", "cnt", "req_type", "req_url"};
			heads = new String[] {"일시", "소속", "취급자ID", "취급자명", "취급자IP", "시스템명", "정보주체식별정보", "합계", "수행업무", tmpName};
		}else {
			columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "result_type", "cnt", "req_type"};
			heads = new String[] {"일시", "소속", "취급자ID", "취급자명", "취급자IP", "시스템명", "정보주체식별정보", "합계", "수행업무"};
		}
		
		modelAndView.addObject("excelData", list);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
		modelAndView.addObject("fileName", fileName);
	}
	
	@Override
	public void findAllLogInqList_downloadCSV_extend(DataModelAndView modelAndView, SearchSearch search,
			HttpServletRequest request) {
		
		search.setUseExcel("true");
		
		String fileName = "PSM_개인정보로그조회_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		//String sheetName = "개인정보 로그조회";
		
		if(search.getStart_h() != null) {
			String start_time = search.getStart_h().substring(0);
			if(start_time.length() < 2)
				start_time = "0" + start_time;
			search.setStart_time(start_time);
		}
		if(search.getEnd_h() != null) {
			String end_time = search.getEnd_h().substring(0);
			if(end_time.length() < 2)
				end_time = "0" + end_time;
			search.setEnd_time(end_time);
		}

		List<AllLogInq> allLogInq = allLogInqDao.findAllLogInqList(search);
		List<PrivacyType> privacyTypeList = commonDao.getPrivacyTypesExceptRegular();
		List<Code> result_type_list = commonDao.getResultTypesByCode();
		
		SimpleDateFormat parseSdf = new SimpleDateFormat("yyyyMMddHHmmss");
		SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
		
		for(int i=0; i<allLogInq.size(); i++){
			/*String result_type[] = allLogInq.get(i).getResult_type().split(",");
			
			String result_type_string = "";
			for(int j=0; j<result_type.length; j++){
				for(int k=0; k<privacyTypeList.size(); k++){
					if(privacyTypeList.get(k).getPrivacy_type().equals(result_type[j]) ) result_type_string += privacyTypeList.get(k).getPrivacy_desc(); 
				}
			}*/
			AllLogInq ali = allLogInq.get(i);
			String result_type = ali.getResult_type();
			String[] arrResult_type = result_type.split(",");
			Arrays.sort(arrResult_type);
			String result_type_string = "";
			String prev = "";
			int count = 1;
			for(int j=0; j<arrResult_type.length; j++) {
				if(prev.equals(arrResult_type[j])) {
					count++;
				}else {
					if(j != 0){
						result_type_string += " " +count + ", ";
						setData(ali, Integer.parseInt(prev), count);
					}
					
					for(int k=0; k<privacyTypeList.size(); k++){
						if(privacyTypeList.get(k).getPrivacy_type().equals(arrResult_type[j])) { 
							result_type_string += privacyTypeList.get(k).getPrivacy_desc(); 
						}
					}
					count = 1;
					prev = arrResult_type[j];
				}
				
				if(j == arrResult_type.length - 1){
					result_type_string += " " +count;
					setData(ali, Integer.parseInt(arrResult_type[j]), count);
				}
			}
			
			allLogInq.get(i).setCnt(arrResult_type.length);
			String req_type_en=allLogInq.get(i).getReq_type();
			switch(req_type_en){
				case "RD": allLogInq.get(i).setReq_type("조회");break;
				case "CR": allLogInq.get(i).setReq_type("등록");break;
				case "UD": allLogInq.get(i).setReq_type("수정");break;
				case "DL": allLogInq.get(i).setReq_type("삭제");break;
				case "DN": allLogInq.get(i).setReq_type("다운로드");break;
				case "PR": allLogInq.get(i).setReq_type("출력");break;
				case "CO": allLogInq.get(i).setReq_type("수집");break;
				case "NE": allLogInq.get(i).setReq_type("생성");break;
				case "BE": allLogInq.get(i).setReq_type("연계");break;
				case "IN": allLogInq.get(i).setReq_type("연동");break;
				case "WR": allLogInq.get(i).setReq_type("기록");break;
				case "SA":
				case "SV": allLogInq.get(i).setReq_type("저장");break;
				case "SU": allLogInq.get(i).setReq_type("보유");break;
				case "FI": allLogInq.get(i).setReq_type("가공");break;
				case "UP": allLogInq.get(i).setReq_type("편집");break;
				case "SC": allLogInq.get(i).setReq_type("검색");break;
				case "CT": allLogInq.get(i).setReq_type("정정");break;
				case "RE": allLogInq.get(i).setReq_type("복구");break;
				case "US": allLogInq.get(i).setReq_type("이용");break;
				case "OF": allLogInq.get(i).setReq_type("제공");break;
				case "OP": allLogInq.get(i).setReq_type("공개");break;
				case "AN": allLogInq.get(i).setReq_type("파기");break;
				case "EX": allLogInq.get(i).setReq_type("실행");break;
				case "PRRD": allLogInq.get(i).setReq_type("출력(RD)");break;
				case "PREX": allLogInq.get(i).setReq_type("출력(Excel)");break;
			}
			
			if(allLogInq.get(i).getDept_name().equals("") || allLogInq.get(i).getDept_name().equals(null)) 
				allLogInq.get(i).setDept_name("-");
			if(allLogInq.get(i).getEmp_user_name().equals("") || allLogInq.get(i).getEmp_user_name().equals(null)) 
				allLogInq.get(i).setEmp_user_name("-");
			allLogInq.get(i).setResult_type(result_type_string);
			try {
				allLogInq.get(i).setProc_date(sdf.format(parseSdf.parse(allLogInq.get(i).getProc_date() + allLogInq.get(i).getProc_time())));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		List<Map<String, Object>> list = new ArrayList<>();

		for(AllLogInq ali : allLogInq) {
			Map<String, Object> map = new HashMap<>();
			map.put("proc_date", ali.getProc_date());
			map.put("dept_name", ali.getDept_name());
			map.put("emp_user_id", ali.getEmp_user_id());
			map.put("emp_user_name", ali.getEmp_user_name());
			map.put("user_ip", ali.getUser_ip());
			map.put("system_name", ali.getSystem_name());
			map.put("result_type", ali.getResult_type());
			map.put("cnt", ali.getCnt());
			map.put("req_type", ali.getReq_type());
			map.put("req_url", ali.getReq_url());
			if(!String.valueOf(ali.getData1()).isEmpty()) map.put("data1", ali.getData1());
			if(!String.valueOf(ali.getData2()).isEmpty()) map.put("data2", ali.getData2());
			if(!String.valueOf(ali.getData3()).isEmpty()) map.put("data3", ali.getData3());
			if(!String.valueOf(ali.getData4()).isEmpty()) map.put("data4", ali.getData4());
			if(!String.valueOf(ali.getData5()).isEmpty()) map.put("data5", ali.getData5());
			if(!String.valueOf(ali.getData6()).isEmpty()) map.put("data6", ali.getData6());
			if(!String.valueOf(ali.getData7()).isEmpty()) map.put("data7", ali.getData7());
			if(!String.valueOf(ali.getData8()).isEmpty()) map.put("data8", ali.getData8());
			if(!String.valueOf(ali.getData9()).isEmpty()) map.put("data9", ali.getData9());
			if(!String.valueOf(ali.getData10()).isEmpty()) map.put("data10", ali.getData10());
			if(!String.valueOf(ali.getData11()).isEmpty()) map.put("data11", ali.getData11());
			if(!String.valueOf(ali.getData12()).isEmpty()) map.put("data12", ali.getData12());
			if(!String.valueOf(ali.getData13()).isEmpty()) map.put("data13", ali.getData13());
			if(!String.valueOf(ali.getData14()).isEmpty()) map.put("data14", ali.getData14());
			if(!String.valueOf(ali.getData15()).isEmpty()) map.put("data15", ali.getData15());
			
			list.add(map);
		}
		
		String[] columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "result_type", "cnt", "req_type", "req_url"};
		String[] columns2 = new String[result_type_list.size() + 10];
		for(int i=0; i<columns.length; i++) 
			columns2[i] = columns[i];
		for(int i=0 ; i<result_type_list.size(); i++) 
			columns2[i+10] = "data" + (i + 1);
		String[] heads = new String[] {"일시", "소속", "취급자ID", "취급자명", "취급자IP", "시스템명", "정보주체식별정보", "합계", "수행업무", "접근 경로"};
		String[] heads2 = new String[result_type_list.size() + 10];
		for(int i=0; i<heads.length; i++) 
			heads2[i] = heads[i];
		for(int i=0 ; i<result_type_list.size(); i++) 
			heads2[i+10] = result_type_list.get(i).getCode_name();
		
		modelAndView.addObject("excelData", list);
		modelAndView.addObject("columns", columns2);
		modelAndView.addObject("heads", heads2);
		modelAndView.addObject("fileName", fileName);
	}
	
	@Override
	public void findAllLoginqDetail_download(DataModelAndView modelAndView, SearchSearch search,
			HttpServletRequest request) {
		
		search.setUseExcel("true");
		
		String fileName = "PSM_개인정보_로그조회_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "접속기록상세 개인정보 리스트";
		
		AllLogInq allLogInq = allLogInqDao.findAllLogInqDetail(search);
		search.setSystem_seq_temp(allLogInq.getSystem_seq());
		List<AllLogInq> allLogInqDetailList = allLogInqDao.findAllLogInqDetailResult(search);
		
		int num = 1;
		for(AllLogInq v : allLogInqDetailList){
			if(v.getResult_content() != null && v.getResult_content() != ""){
				
				String pResult = v.getResult_content();
				
				if(v.getResult_type() != null && v.getPrivacy_masking() != null && v.getPrivacy_masking().equals("Y")) {
					pResult = getResultContextByMasking(pResult, v.getResult_type());
				}
				
				v.setResult_content_masking(pResult);
				v.setData1(num);
				
				num++;
			}
		}
		String userProfile ="이름 : " + allLogInq.getEmp_user_name()+
							" 소속 : " + allLogInq.getDept_name()+
							" 시스템 : " + allLogInq.getSystem_name()+
							" 날짜 : " + allLogInq.getProc_date().toString();
		
		String[] columns;
		String[] heads;
		columns = new String[] {"data1", "result_type", "result_content_masking"};
		heads = new String[] {"No.", "개인정보유형", "개인정보내용"};
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", allLogInqDetailList);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
		modelAndView.addObject("userProfile", userProfile);
	}
	
	@Override
	public void findAllLoginqDetail_downloadCSV(DataModelAndView modelAndView, SearchSearch search,
			HttpServletRequest request) {
		
		search.setUseExcel("true");
		
		String fileName = "PSM_개인정보_로그조회_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		
		AllLogInq allLogInq = allLogInqDao.findAllLogInqDetail(search);
		List<AllLogInq> allLogInqDetailList = allLogInqDao.findAllLogInqDetailResult(search);
		
		int num = 1;
		for(AllLogInq v : allLogInqDetailList){
			if(v.getResult_content() != null && v.getResult_content() != ""){
				
				String pResult = v.getResult_content();
				
				if(v.getResult_type() != null && v.getPrivacy_masking() != null && v.getPrivacy_masking().equals("Y")) {
					pResult = getResultContextByMasking(pResult, v.getResult_type());
				}
				
				v.setResult_content_masking(pResult);
				v.setData1(num);
				
				num++;
			}
		}
		
		List<Map<String, Object>> excelDataList = new ArrayList<>();
		
		for (AllLogInq alllog : allLogInqDetailList) {
			Map<String, Object> map = new HashMap<>();
			map.put("data1", alllog.getData1());
			map.put("result_type", alllog.getResult_type());
			map.put("result_content_masking", alllog.getResult_content_masking());
			excelDataList.add(map);
		}
		
		String[] columns;
		String[] heads;
		columns = new String[] {"data1", "result_type", "result_content_masking"};
		heads = new String[] {"No.", "개인정보유형", "개인정보내용"};
		
		modelAndView.addObject("excelData", excelDataList);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
		modelAndView.addObject("fileName", fileName);
	}
	
	@Override
	public int findAllLogInqDetailLogSeq(SearchSearch search) {
		return allLogInqDao.findAllLogInqDetailLogSeq(search);
	}
	
	public static String getResultContextByMasking(String result, String type) {
		Pattern[] patternArr = new Pattern[10];/*주민번호, 운전번호, 여권번호, 신용카드번호, 건강보험번호, 전화번호, 이메일, 휴대폰번호, 계좌번호, 외국인등록번호*/
		patternArr[0]=Pattern.compile("\\b(?:[0-9]{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[1,2][0-9]|3[0,1]))-[1-4][0-9]{6}\\b|\\b(?:[0-9]{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[1,2][0-9]|3[0,1]))[1-4][0-9]{6}\\b");
		patternArr[1]=Pattern.compile("(?<!\\d)((1[1-9])|(2[0-68]))([ \\-]{1,3})\\d{6}([ \\-]{1,3})\\d{2}(?!\\d+)");
		patternArr[2]=Pattern.compile("([a-zA-Z]{1}|[a-zA-Z]{2})\\d{8}");
		patternArr[3]=Pattern.compile("(?<!\\d)(30[0-5]\\d{1}|3095|35(2[89]|[3-8]\\d{1}|92)|4\\d{3}|60(11|48|60)|5(021|409)|51(00|55)|53(10|77|88)|55(21|51|96|8[59])|51(2[0346]|4[089]|6[034]|7[6-9])|62(0[0-28]|1[0246]|2[1-9]|[56]\\d{1}|8[1-8])|63(6[01]|96)|64[4-9]\\d{1}|65\\d{2}|9(035|289|350|490|5(30|58)|720|806|999)|94(0[0379]|1[0-25]|2[0156]|3[012569]|4[013-6]|5[025-8]|6[0-578]|[78][05]))( ?\\- ?| |­)\\d{4}\\14\\d{4}\\14\\d{4}(?!\\d+)|(?<!\\d)(30([0-5]\\d{1}|95)|3[689]\\d{2})( ?\\- ?| |­)\\d{6}\\15\\d{4}(?!\\d+)|(?<!\\d)(34|37)\\d{2}( ?\\- ?| |­)\\d{6}\\16\\d{5}(?!\\d+)|(?<!\\d)\\d{4}( ?- ?| )\\d{6}( ?- ?| )\\d{5}(?!\\d+)");
		patternArr[4]=Pattern.compile("(?<!\\d)[1-8]( ?- ?| )\\d{10}(?!\\d+)");
		patternArr[5]=Pattern.compile("(?<!\\d)0(2|31|32|33|41|42|43|44|63|51|52|53|55|61|62|54|64)([ )\\-]{1,3})(\\d{3,4})([ \\-]{0,3})(\\d{4})(?!\\d+)");
		patternArr[6]=Pattern.compile("\\b[\\w\\~\\-\\.]{3,}@[\\w\\~\\-]{2,}\\.(?:kr|co.kr|com|net|asia|co|org|pro|cn|jp|biz|me|in|eu|info|tv|nameso|or.kr|pw|ru|sx|mobi|ph|tw|go.kr|ac.kr|ac)\\b");
		patternArr[7]=Pattern.compile("(?<!\\d)((010[ \\-]{0,3}\\d{4}[ \\-]{0,3}\\d{4})|(01[16789][ \\-]{0,3}\\d{3,4}[ \\-]{0,3}\\d{4}))(?!\\d+)");
		patternArr[8]=Pattern.compile("(?<!\\d)(\\d{3} ?- ?\\d{2} ?- ?\\d{5} ?- ?\\d{1}|\\d{3} ?- ?\\d{7} ?- ?\\d{1} ?- ?\\d{3}|\\d{8} ?- ?\\d{2}|\\d{3} ?- ?\\d{8}|\\d{3} ?- ?\\d{2} ?- ?\\d{6} ?- ?\\d{1}|\\d{3} ?- ?\\d{6} ?- ?\\d{2} ?- ?\\d{2} ?- ?\\d{1}|\\d{3} ?- ?\\d{2} ?- ?\\d{5} ?- ?\\d{3}|\\d{6} ?- ?\\d{2} ?- ?\\d{4}|\\d{6} ?- ?\\d{2} ?- ?\\d{6}|\\d{3} ?- ?\\d{6} ?- ?\\d{3}|\\d{6} ?- ?\\d{2} ?- ?\\d{3} ?- ?\\d{1}|\\d{4} ?- ?\\d{3} ?- ?\\d{6}|\\d{3} ?- ?\\d{2} ?- ?\\d{6}|\\d{3} ?- ?\\d{8} ?- ?\\d{1}|\\d{3} ?- ?\\d{3} ?- ?\\d{8}|\\d{3} ?- ?\\d{3} ?- ?\\d{6}|\\d{3} ?- ?\\d{2,3} ?- ?\\d{7} ?- ?\\d{1}|\\d{3} ?- ?\\d{2} ?- ?\\d{7,8}|\\d{3} ?- ?\\d{5} ?- ?\\d{2} ?- ?\\d{1} ?- ?\\d{2}|\\d{3} ?- ?\\d{5} ?- ?\\d{2} ?- ?\\d{1}|\\d{1} ?- ?\\d{6} ?- ?\\d{2} ?- ?\\d{1}|\\d{1} ?- ?\\d{6} ?- ?\\d{2} ?- ?\\d{1}|\\d{2} ?- ?\\d{2} ?- ?\\d{5} ?- ?\\d{1}|\\d{3} ?- ?\\d{2} ?- ?\\d{6} ?- ?\\d{3}|\\d{3} ?- ?\\d{3} ?- ?\\d{5} ?- ?\\d{1}|\\d{2} ?- ?\\d{2} ?- ?\\d{6}|\\d{3} ?- ?\\d{9} ?- ?\\d{2}|\\d{3} ?- ?\\d{2} ?- ?\\d{2} ?- ?\\d{6} ?- ?\\d{1}|\\d{4} ?- ?\\d{2,3} ?- ?\\d{6} ?- ?\\d{1}|\\d{5} ?- ?\\d{2} ?- ?\\d{5} ?- ?\\d{1}|\\d{3} ?- ?\\d{5} ?- ?\\d{1} ?- ?\\d{3}|\\d{3} ?- ?\\d{2} ?- ?\\d{4} ?- ?\\d{3}|\\d{6} ?- ?\\d{2} ?- ?\\d{5} ?- ?\\d{1}|\\d{3} ?- ?\\d{2} ?- ?\\d{4} ?- ?\\d{3}|\\d{6} ?- ?\\d{2} ?- ?\\d{5} ?- ?\\d{1}|\\d{3} ?- ?\\d{5} ?- ?\\d{2} ?- ?\\d{2} ?- ?\\d{1})(?!\\d)");
		patternArr[9]=Pattern.compile("\\b(?:[0-9]{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[1,2][0-9]|3[0,1]))-[5-8][0-9]{6}\\b|\\b(?:[0-9]{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[1,2][0-9]|3[0,1]))-[5-8][0-9]{6}\\b");
		
		 String resultType[] = {"주민번호","운전번호", "여권번호", "신용번호", "건강보험번호", "전화번호", "이메일", "휴대폰번호", "계좌번호", "외국인등록번호"};
		
		for (int i = 0; i < patternArr.length; i++) {
			if(patternArr[i].matcher(result).find()) {
				type = resultType[i];
				break;
			}
		}
		
		int len = result.length();
		switch(type) {
			case "주민번호":
			case "외국인등록번호":
				result = result.substring(0,6)+"-*******";
				break;
			case "운전번호":
				result = result.substring(0,6);
				for(int i=0; i<len-6; i++)
					result += "*";
				break;
			case "여권번호":
				result = result.substring(0,5);
				for(int i=0; i<len-5; i++)
					result += "*";
				break;
			case "신용번호":
				result = result.substring(0,10)+"****-****";
				break;
			case "건강보험번호":
				result = result.substring(0,6);
				for(int i=0; i<len-6; i++)
					result += "*";
				break;
			case "전화번호":
			case "휴대폰번호":
				String[] arr = result.split("-");
				if(arr.length <= 1) {
					result = result.substring(0, len - 4) + "****";
				}else {
					result = arr[0] + "-" + arr[1] + "-****";
				}
				break;
			case "이메일":
				result = result.replaceAll("(?<=.{3}).(?=.*@)", "*");
				break;
			case "계좌번호":
				String[] arr2 = result.split("-");
				if(arr2.length <= 2) {
					result = result.substring(0, len - 6) + "******";
				}else {
					result = arr2[0] + "-" + arr2[1] + "-";
					if(arr2[2] != null) {
						int l = arr2[2].length();
						for(int i=0; i<l; i++)
							result += "*";
					}
				}
				break;
			default:
				result = result.substring(0, result.length()-6)+"******";
				break;
		}
		return result;
	}
	
	@Override
	public DataModelAndView findReadInfoList(SearchSearch search) {

		SimpleCode simpleCode = new SimpleCode();
		String index = "/allLogInq/readInfoList.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList_byAdmin(search);
		
		String privacy = search.getPrivacy();
		if(privacy != null && privacy != "") {
			search.setPrivacy(privacy.trim());
			int pageSize = Integer.valueOf(defaultPageSize);
			search.setSize(pageSize);
			
			/*String priTemp = allLogInqDao.getResultContentByAes(search);
			search.setPrivacy(priTemp);*/
			int count = allLogInqDao.findReadInfoList_count(search);
			search.setTotal_count(count);
			List<AllLogInq> allLogInq = allLogInqDao.findReadInfoList(search);
			String checkEmpNameMasking = commonDao.checkEmpNameMasking();
			if(checkEmpNameMasking.equals("Y")) {
				for (int i = 0; i < allLogInq.size(); i++) {
					if(!"".equals(allLogInq.get(i).getEmp_user_name())) {
						String empUserName = allLogInq.get(i).getEmp_user_name();
						StringBuilder builder = new StringBuilder(empUserName);
						builder.setCharAt(empUserName.length() - 2, '*');
						allLogInq.get(i).setEmp_user_name(builder.toString());
					}
				}
			}
			AllLogInqList allLogInqList = new AllLogInqList(allLogInq);
			
			modelAndView.addObject("allLogInqList", allLogInqList);
		}	
		
		modelAndView.addObject("search", search);
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("systemMasterList", systemMasterList);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView findAllLogInqListByReqType(SearchSearch search) {
		
		SimpleCode simpleCode = new SimpleCode();
		String index = "/allLogInq/listByReqType.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);

		List<AllLogInq> allLogInq = allLogInqDao.findAllLogInqListByReqType(search);
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("allLogInq", allLogInq);
		modelAndView.addObject("log_count", allLogInq.size());
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	@Override
	public void findAllLogInqList_downloadByReqType(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request) {

		search.setUseExcel("true");
		
		String fileName = "PSM_접근행위별 접속기록조회_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "접근행위별 접속기록조회";

		List<AllLogInq> allLogInq = allLogInqDao.findAllLogInqListByReqType(search);
		
		for(int i=0; i<allLogInq.size(); i++){
			//AllLogInq ali = allLogInq.get(i);
			
			String req_type_en=allLogInq.get(i).getReq_type();
			switch(req_type_en){
				case "RD": allLogInq.get(i).setReq_type("조회");break;
				case "CR": allLogInq.get(i).setReq_type("등록");break;
				case "UD": allLogInq.get(i).setReq_type("수정");break;
				case "DL": allLogInq.get(i).setReq_type("삭제");break;
				case "DN": allLogInq.get(i).setReq_type("다운로드");break;
				case "PR": allLogInq.get(i).setReq_type("출력");break;
				case "CO": allLogInq.get(i).setReq_type("수집");break;
				case "NE": allLogInq.get(i).setReq_type("생성");break;
				case "BE": allLogInq.get(i).setReq_type("연계");break;
				case "IN": allLogInq.get(i).setReq_type("연동");break;
				case "WR": allLogInq.get(i).setReq_type("기록");break;
				case "SA": allLogInq.get(i).setReq_type("저장");break;
				case "SU": allLogInq.get(i).setReq_type("보유");break;
				case "FI": allLogInq.get(i).setReq_type("가공");break;
				case "UP": allLogInq.get(i).setReq_type("편집");break;
				case "SC": allLogInq.get(i).setReq_type("검색");break;
				case "CT": allLogInq.get(i).setReq_type("정정");break;
				case "RE": allLogInq.get(i).setReq_type("복구");break;
				case "US": allLogInq.get(i).setReq_type("이용");break;
				case "OF": allLogInq.get(i).setReq_type("제공");break;
				case "OP": allLogInq.get(i).setReq_type("공개");break;
				case "AN": allLogInq.get(i).setReq_type("파기");break;
			}
			
			if(allLogInq.get(i).getDept_name().equals("") || allLogInq.get(i).getDept_name().equals(null)) 
				allLogInq.get(i).setDept_name("-");
			if(allLogInq.get(i).getEmp_user_name().equals("") || allLogInq.get(i).getEmp_user_name().equals(null)) 
				allLogInq.get(i).setEmp_user_name("-");
		}
		
		String[] columns = new String[] {"data1", "req_type", "cnt", "emp_user_id", "emp_user_name", "dept_name", "privacy_desc", "cnt2"};
		String[] heads = new String[] {"rank", "접근행위", "로그 조회 건수", "사용자ID", "사용자명", "소속", "개인정보유형", "개인정보 유형별 조회 건수"};
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", allLogInq);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}
	
	private void setData(AllLogInq allLogInq, int result_type, int data) {
		if(result_type == 1)
			allLogInq.setData1(data);
		else if(result_type == 2)
			allLogInq.setData2(data);
		else if(result_type == 3)
			allLogInq.setData3(data);
		else if(result_type == 4)
			allLogInq.setData4(data);
		else if(result_type == 5)
			allLogInq.setData5(data);
		else if(result_type == 6)
			allLogInq.setData6(data);
		else if(result_type == 7)
			allLogInq.setData7(data);
		else if(result_type == 8)
			allLogInq.setData8(data);
		else if(result_type == 9)
			allLogInq.setData9(data);
		else if(result_type == 10)
			allLogInq.setData10(data);
		else if(result_type == 11)
			allLogInq.setData11(data);
		else if(result_type == 12)
			allLogInq.setData12(data);
	}
	
	@Override
	public JSONObject checkPrivacyInfo(SearchSearch search) {
		AllLogInq dbInfo = new AllLogInq();
		AllLogInq privacyInfoList = new AllLogInq();
		String ui_type = commonDao.getUiType();
		String errorCode = "";
		try{
			String sql = "SELECT DRIVER_NAME, URL, ID, PASSWORD, SQL, #{privacyType} as privacy_column FROM SYSTEM_DB WHERE SYSTEM_SEQ = #{system_seq}";
			if(ui_type.equals("518a")) {
				String type = search.getPrivacy().substring(0, 2).toUpperCase();
				if(type.equals("RF")) {
					search.setSystem_seq("03");
				} else if(type.equals("IT")) {
					search.setSystem_seq("04");
				} else {
					search.setSystem_seq("02");
				}
			}
			sql = sql.replace("#{privacyType}", search.getPrivacyType());
			sql = sql.replace("#{system_seq}", "'"+search.getSystem_seq()+"'");
			//search.setSql(sql);
			Object[] psmParam = {};
			Map psmFindColumn = new HashMap<>();
			psmFindColumn.put("driver_name", "string");
			psmFindColumn.put("url", "string");
			psmFindColumn.put("id", "string");
			psmFindColumn.put("password", "string");
			psmFindColumn.put("sql", "string");
			psmFindColumn.put("privacy_column", "string");
			
			EncryptStrGenerator en = new EncryptStrGenerator();
			
			String realPsmId = en.getDecryptedMessage(psm_id);
			String realPsmPw = en.getDecryptedMessage(psm_pw);
			
			List psmRes = JDBCConnectionUtil.ConnectionResult(psm_className, psm_url, realPsmId, realPsmPw, sql, psmParam, psmFindColumn);
			if (psmRes.size() > 0) {
				Map psmResMap = (Map) psmRes.get(0);
				if(psmResMap.get("privacy_column") != null) {
					dbInfo.setDriver_name((String)psmResMap.get("driver_name"));
					dbInfo.setUrl((String)psmResMap.get("url"));
					dbInfo.setId((String)psmResMap.get("id"));
					dbInfo.setPassword((String)psmResMap.get("password"));
					dbInfo.setPrivacy_column((String)psmResMap.get("privacy_column"));
					dbInfo.setSql((String)psmResMap.get("sql"));
					
					dbInfo.setSql(dbInfo.getSql().replace("#{privacy_column}", dbInfo.getPrivacy_column()).replace("#{privacy}", "'"+search.getPrivacy()+"'"));
					
					Object[] param = {};
					Map findColumn = new HashMap<>();
					findColumn.put("name", "string");
					List res = JDBCConnectionUtil.ConnectionResult(dbInfo.getDriver_name(), dbInfo.getUrl(), dbInfo.getId(), dbInfo.getPassword(), dbInfo.getSql(), param, findColumn);
					if (!res.isEmpty()) {
						String name = null;
						for(int a = 0; a < res.size(); a++) {
							Map resMap = (Map) res.get(a);
							if (resMap.get("name") != null) {
								if(a > 0) {
									name = name + ", ";
									name = name + (String)resMap.get("name");
								} else {
									name = (String)resMap.get("name");
								}
							}
						}
						privacyInfoList.setEmp_user_name(name);
						privacyInfoList.setPrivacy(search.getPrivacy());
					} else {
						errorCode = "err2";
					}
				} else {
					errorCode = "err3";
				}
			} else {
				errorCode = "err1";
			}
		}catch(Exception e) {
			logger.error("[AllLogInqSvcImpl.findAllLogInqDetail] MESSAGE : " + e.getMessage());
			errorCode = "err4";
		}

		SimpleCode simpleCode = new SimpleCode();
		String index = "/allLogInq/privacyInfo.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		modelAndView.addObject("privacyInfoList", privacyInfoList);

		JSONObject jsonObj=new JSONObject();
		jsonObj.put("name", privacyInfoList.getEmp_user_name());
		jsonObj.put("privacy", privacyInfoList.getPrivacy());
		jsonObj.put("errorCode", errorCode);
		
		return jsonObj;
	}
	
	@Override
	public DataModelAndView findPrivacyInfo(SearchSearch search) {
		SimpleCode simpleCode = new SimpleCode();
		String index = "/allLogInq/findBizLog80Popup.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	@SuppressWarnings("unused")
	@Override
	public int addCenterLogInfo(List<String[]> logList) {
		int result = 1;
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
			Date now = new Date();
			System.out.println("ADD CENTER SUMMON LOG : "+sdf.format(now));
			long descLogSeq = 0;
			int i = 0;
			for(String[] strArr : logList) {
				//biz_log insert
				System.out.println("LOG INDEX = "+i);
				SummonReceve ali = new SummonReceve();
				ali.setProc_date(strArr[4].substring(0,8));
				ali.setProc_time(strArr[4].substring(8));
				ali.setEmp_cd(strArr[5]);
				ali.setEmp_nm(strArr[6]);
				ali.setUser_id(strArr[7]);
				ali.setUser_ip(strArr[8]);
				ali.setDept_cd(strArr[9]);
				ali.setDept_nm(strArr[10]);
				ali.setOrg_cd(strArr[11]);
				ali.setOrg_nm(strArr[12]);
				ali.setHq_cd(strArr[13]);
				ali.setHq_nm(strArr[14]);
				ali.setPosit_gu(strArr[15]);
				ali.setAcc_org_cd(strArr[16]);
				ali.setAcc_org_nm(strArr[17]);
				ali.setAcc_hq_cd(strArr[18]);
				ali.setAcc_hq_nm(strArr[19]);
				ali.setBran_cd(strArr[20]);
				ali.setBran_nm(strArr[21]);
				ali.setNh_dept_cd(strArr[22]);
				ali.setNh_dept_nm(strArr[23]);
				ali.setCd_org_cd(strArr[24]);
				ali.setCd_org_nm(strArr[25]);
				ali.setCar_cd(strArr[26]);
				ali.setSite_cd(strArr[27]);
				ali.setMember_div(strArr[28]);
				ali.setJoin_ssn(strArr[29]);
				ali.setSsn_name(strArr[30]);
				ali.setSear_cont(strArr[31]);
				ali.setSear_val(strArr[32]);
				ali.setInq_seq(strArr[33]);
				ali.setCert_num(strArr[34]);
				ali.setSear_log24(strArr[35]);
				ali.setPer_inf_cd(strArr[36]);
				ali.setEsta_sym(strArr[37]);
				ali.setFirm_nm(strArr[38]);
				ali.setAssu_ssn(strArr[39]);
				ali.setAssu_nm(strArr[40]);
				ali.setContact_condition(strArr[41]);
				ali.setPsnl_inf_qry_hed_colnm(strArr[42]);
				ali.setPsnl_inf_cnts(strArr[43]);
				ali.setInq_reason(strArr[44]);
				ali.setYkiho_cd(strArr[45]);
				ali.setYkiho_nm(strArr[46]);
				ali.setRecv_no(strArr[47]);
				ali.setRecv_yyyy(strArr[48]);
				ali.setOutput_pgm_id(strArr[49]);
				ali.setBusi_cd(strArr[50]);
				ali.setBusi_nm(strArr[51]);
				ali.setBusi_dtl_contn(strArr[52]);
				ali.setInq_db(strArr[53]);
				ali.setPatient_cd(strArr[54]);
				ali.setPatient_nm(strArr[55]);
				ali.setBloodno(strArr[56]);
				ali.setResult(strArr[57]);
				ali.setButton_cd(strArr[58]);
				ali.setScrn_id(strArr[59]);
				ali.setScrn_nm(strArr[60]);
				ali.setPrg_id(strArr[61]);
				ali.setPrg_nm(strArr[62]);
				ali.setReq_url(strArr[63]);
				ali.setVcls(strArr[64]);
				ali.setSsn_org_cd(strArr[65]);
				ali.setSsn_org_nm(strArr[66]);
				ali.setSsn_hq_cd(strArr[67]);
				ali.setSsn_hq_nm(strArr[68]);
				ali.setJuri_out(strArr[69]);
				ali.setFirm_cd(strArr[70]);
				ali.setFirm_cd_nm(strArr[71]);
				ali.setFirm_hq_cd(strArr[72]);
				ali.setFirm_hq_nm(strArr[73]);
				ali.setAssu_org_cd(strArr[74]);
				ali.setAssu_org_nm(strArr[75]);
				ali.setAssu_hq_cd(strArr[76]);
				ali.setAssu_hq_nm(strArr[77]);
				ali.setAudit_no(strArr[78]);
				ali.setGrade(strArr[79]);
				ali.setStatus(strArr[80]);
				ali.setLog_seq(Integer.parseInt(strArr[81]));
				ali.setSys_cd(strArr[82]);
				ali.setLog_agency_cd(strArr[83]);
				ali.setFull_proc_date(strArr[4]);
				ali.setDetect_div("9");
				System.out.println("SET BIZ_LOG DATA");
				//ibatis의 selectKey로 biz_log_summary 인서트시 생성한 키를 biz_log_seq에 set함
				allLogInqDao.addSummonBizLogSummary(ali);
				System.out.println("INSERT BIZ_LOG_SUMMARY");
				ali.setBiz_log_seq(ali.getLog_seq());
				
				//biz_log insert
				allLogInqDao.addSummonBizLog(ali);
				System.out.println("INSERT BIZ_LOG");
				
				ali.setResult_content(ali.getJoin_ssn()+":"+ali.getSsn_name());
				allLogInqDao.addSummonBizLogResult(ali);
				System.out.println("INSERT BIZ_LOG_RESULT");
				
				/*
				ali.setReq_date(strArr[84]);
				ali.setAns_date(strArr[85]);
				ali.setSummon_req_emp(strArr[86]);
				ali.setResummon_yn(strArr[87]);
				ali.setDesc_status(strArr[88]);
				ali.setDesc_result(strArr[89]);
				ali.setRule_nm(strArr[90]);
				ali.setSummon_reason(strArr[91]);*/
				
				AdminUser adminUser = new AdminUser();
				adminUser.setOccr_dt(ali.getProc_date());
				adminUser.setExpect_dt(strArr[85]);
				adminUser.setLog_type("BA");
				adminUser.setDescription2(strArr[91]);
				adminUser.setAdmin_user_id(strArr[86]);
				adminUser.setInsert_user_id(ali.getEmp_cd());
				adminUser.setDetailEmpDetailSeq("0");
				adminUser.setDetect_div("9");
				adminUser.setRule_nm(strArr[90]);
				adminUser.setCollect_dt(ali.getProc_date());
				adminUser.setSystem_seq(ali.getSys_cd());
				System.out.println("SET DESC DATA");
				if(i == 0) {
					
					extrtCondbyInqDao.addDescInfo(adminUser);
					System.out.println("INSERT DESC_INFO");
					descLogSeq = adminUser.getDesc_seq();
					
					adminUser.setH_type("1");
					adminUser.setH_body("소명요청 : "+adminUser.getInsert_user_id()+"//"+adminUser.getDescription2());
					extrtCondbyInqDao.addDescHistory(adminUser);
					System.out.println("INSERT DESC_HISTORY");
					String procDate = ali.getProc_date();
					String procTime = ali.getProc_time();
					
					AdminUser apprHtml = extrtCondbyInqDao.findApprSummonHtml();
					String html = apprHtml.getParam_value();
					html = html.replaceFirst("requestTargetDeptName", extrtCondbyInqDao.findDeptNameByTargetUserId(adminUser));
					html = html.replaceFirst("requestTargetName", agentMngtDao.findNameByEmpUserId(adminUser.getInsert_user_id()));
					html = html.replaceFirst("procDate", procDate.substring(0, 4)+"-"+procDate.substring(4, 6)+"-"+procDate.substring(6));
					html = html.replaceFirst("procTime", procTime.substring(0, 2)+":"+procTime.substring(2, 4)+":"+procTime.substring(4));
					html = html.replaceFirst("userIp", ali.getUser_ip());
					html = html.replaceFirst("scrnName", ali.getScrn_nm());
					html = html.replaceFirst("ruleName", adminUser.getRule_nm());
					html = html.replaceFirst("resultContent", ali.getResult_content());
					html = html.replaceFirst("requestBody", adminUser.getDescription2());
					html = html.replaceFirst("etcBody", "");
					adminUser.setAppr_type("10");
					adminUser.setAppr_body(html);
					extrtCondbyInqDao.addDescAppr(adminUser);
					System.out.println("INSERT DESC_APPR");
				}
				
				//descseq 값보존을 위해서 값따로 뺀후에 넣음
				adminUser.setDesc_seq(descLogSeq);
				adminUser.setLog_seq(ali.getBiz_log_seq());
				extrtCondbyInqDao.addDescLog(adminUser);
				System.out.println("INSERT DESC_LOG");
				
				i++;
			}
			transactionManager.commit(transactionStatus);
		} catch (Exception e) {
			e.printStackTrace();
			transactionManager.rollback(transactionStatus);
			return 0;
		}
		
		return result;
	}
	

	@Override
	public DataModelAndView findBizLog80Popup(SearchSearch search) {
		SimpleCode simpleCode = new SimpleCode();
		String index = "/allLogInq/findBizLog80Popup.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		List<SummonReceve> bizLog80 = allLogInqDao.findBizLog80(search.getLog_seq());
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		modelAndView.addObject("bizLog80", bizLog80);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView getResultType(SearchSearch search) {
		DataModelAndView modelAndView = new DataModelAndView();
		List<AllLogInq> list = new ArrayList<>();
		List<AllLogInq> result = null;
			result = allLogInqDao.getResultTypeByOne(search);
		for (AllLogInq v : result) {

			String pResult = v.getResult_content();
			/*
			 * if( v.getResult_type() != null && "주민등록번호".equals(v.getResult_type()) ) {
			 * pResult = pResult.substring(0,7)+"*******"; }
			 */

			if (v.getResult_type() != null && v.getPrivacy_masking() != null
					&& v.getPrivacy_masking().equals("Y")) {
				pResult = getResultContextByMasking(pResult, v.getResult_type());
			}

			v.setResult_content(pResult);
		}
		list.addAll(result);

		modelAndView.addObject("list", list);
		return modelAndView;
	}
	
	//개인정보 임계치 설정
	@Override
	public DataModelAndView thresholdSetting(SearchSearch search) {
		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		
		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList_byAdmin(search);
		
		AllLogInq thresholdInfo = new AllLogInq();
		if(search.getSystem_seq()!=null && search.getSystem_seq()!="") {
			thresholdInfo = allLogInqDao.getThresholdInfo(search);
		}
		
		List<DLogMenuMappSetup> dLogMenuMappList = allLogInqDao.finddLogMenuMappListBySystem(search);
		
		SimpleCode simpleCode = new SimpleCode("/allLogInq/thresholdSetting.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("systemMasterList", systemMasterList);
		modelAndView.addObject("thresholdInfo", thresholdInfo);
		modelAndView.addObject("dLogMenuMappList", dLogMenuMappList);
		modelAndView.addObject("index_id", index_id);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView updateThreshold(SearchSearch search) {
		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		//기타 -> 사용자가 직접 값을 입력한 경우 
		if(search.getThreshold() != null &&  search.getThreshold().equals("etc")) {
			search.setThreshold(search.getThreshold_total_inbox().trim());
		}
		
		//사용 안함으로 체크한 경우 -> total 값을 0으로 저장
		System.out.println("param use flag : " + search.getThreshold_use());
		if(search.getThreshold_use().equals("N")) { 
			search.setThreshold("0");
		}
		
		//api_threshold테이블에 시스템이 존재하는지 확인
		int existChk = allLogInqDao.systemExistChk(search);
		if(existChk == 0) { //신규 등록
			int updateResult = allLogInqDao.insertThreshold(search);
			modelAndView.addObject("result",updateResult);
			return modelAndView;
		}
		
		//기존 정보 업데이트
		int updateResult = allLogInqDao.updateThreshold(search);
		System.out.println("update result : " + updateResult);
		
		//DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("result",updateResult);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView findAllLogInqList_kdic(SearchSearch search) {
		/*
		 * 통계에서 년별,월별,일별 날짜가 들어오면
		 * 해당 날짜에 맞춰 년월일이 전부 나오도록 한다.
		 * ex) 2016 -> 20160101 , 20161231
		 */
		String time_check = search.getCheck_times();
		String date_sub = search.getSearch_from();
		
		String emp_user_name = search.getEmp_user_name();
		if (emp_user_name != null) {
			emp_user_name = emp_user_name.replaceAll("& #40;", "(").replaceAll("& #41;", ")");
			search.setEmp_user_name(emp_user_name);
		}
		
		String dept_name = search.getDept_name();
		if (dept_name != null) {
			dept_name = dept_name.replaceAll("& #40;", "(").replaceAll("& #41;", ")");
			search.setDept_name(dept_name);
		}
		
		if(search.getReq_url() != null) {
			search.setReq_url(RequestWrapper.decodeXSS(search.getReq_url()));
		}
		
		Calendar calendar = Calendar.getInstance();

		if("MONTH".equals(time_check))
		{ 
			
			int year =  Integer.parseInt(date_sub.substring(0, 4));
			int month = Integer.parseInt(date_sub.substring(5, date_sub.length()))-1;
			int date = 1;
			String realmonth = date_sub.substring(4, date_sub.length());
			String realdate = "01";			
			calendar.set(year, month, date);

			int lastDay = calendar.getActualMaximum(Calendar.DATE);
			
			String start_date = Integer.toString(year) + realmonth + realdate;
			String end_date = Integer.toString(year) + realmonth + Integer.toString(lastDay);
			search.setSearch_from(start_date);
			search.setSearch_to(end_date);	
		}
		else if("YEAR".equals(time_check))
		{
		
			date_sub = search.getSearch_from();
				
			int year =  Integer.parseInt(date_sub);
			String month = "01";
			int date = 1;
			String realdate = "01";			
			calendar.set(year, 11, date);
			int lastDay = calendar.getActualMaximum(Calendar.DATE);
			
			String start_date = Integer.toString(year) + month + realdate;
			String end_date = Integer.toString(year) + "12" + Integer.toString(lastDay);
			search.setSearch_from(start_date);
			search.setSearch_to(end_date);				
		}
		
		if(search.getStart_h() != null) {
			String start_time = search.getStart_h().substring(0);
			if(start_time.length() < 2) {
				start_time = "0" + start_time;
			}
			search.setStart_time(start_time);
		}
		if(search.getEnd_h() != null) {
			String end_time = search.getEnd_h().substring(0);
			if(end_time.length() < 2) {
				end_time = "0" + end_time;
			}
			search.setEnd_time(end_time);
		}

		SimpleCode simpleCode = new SimpleCode();
		String index = "/allLogInq/list_kdic.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		//2016_12_27 by hjpark
		
		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		long count = allLogInqDao.findAllLogInqOne_count(search);
		search.setCount(count);
		

		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList_byAdmin(search);
		List<AllLogInq> allLogInq = allLogInqDao.findAllLogInqList_kdic(search);
		
		for(int i=0; i<allLogInq.size(); i++) {
			AllLogInq ali = allLogInq.get(i);
			String result_type = ali.getResult_type();
			ali.setResultTypeMap(result_type);
			if(result_type != null) {
				String[] arrResult_type = result_type.split(",");
				Arrays.sort(arrResult_type);
				String res = "";
				for(int j=0; j<arrResult_type.length; j++) {
					if(j ==0)
						res += arrResult_type[j];
					else
						res += "," + arrResult_type[j];
				}
				ali.setResult_type(res);
			}
		}
		
		String checkEmpNameMasking = commonDao.checkEmpNameMasking();
		if(checkEmpNameMasking.equals("Y")) {
			for (int i = 0; i < allLogInq.size(); i++) {
				if(!"".equals(allLogInq.get(i).getEmp_user_name())) {
					String empUserName = allLogInq.get(i).getEmp_user_name();
					StringBuilder builder = new StringBuilder(empUserName);
					builder.setCharAt(empUserName.length() - 2, '*');
					allLogInq.get(i).setEmp_user_name(builder.toString());
				}
			}
		}
		
		AllLogInqList allLogInqList = new AllLogInqList(allLogInq);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("allLogInqList", allLogInqList);
		modelAndView.addObject("systemMasterList", systemMasterList);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView findAllLogInqDetail_kdic(SearchSearch search, HttpServletRequest request) {

		int pageSize = 5;
		search.getAllLogInqDetail().setSize(pageSize);
		
		int cnt = allLogInqDao.findAllLogInqDetailResultCount(search);
		search.getAllLogInqDetail().setTotal_count(cnt);
		
		String ui_type = commonDao.getUiType();
		search.setUi_type(ui_type+"_TYPE");
		
		List<ExtrtCondbyInqDetailChart> data2 = new ArrayList<>();
		AllLogInq allLogInq = new AllLogInq();
		List<AllLogInq> allLogInqDetailList = new ArrayList<AllLogInq>();
		List<AllLogInq> allLogInqDetailList2 = new ArrayList<AllLogInq>();
		
		HttpSession session = request.getSession();
		String biz_log_file = (String) session.getAttribute("sbiz_log_file");
		if (biz_log_file != null && biz_log_file.equals("Y")) {
			search.setCheck_file("true");
		}
		String insert_biz_log_result = (String) session.getAttribute("insert_biz_log_result");
		if(search.getResult_owner_flag() != null && search.getResult_owner_flag().equals("Y")) {
			search.setResult_owner("true");
		}
		try{
			allLogInq = allLogInqDao.findAllLogInqDetail(search);
			String result_type = allLogInq.getResult_type();
			if(result_type != null) {
				String[] arrResult_type = result_type.split(",");
				Arrays.sort(arrResult_type);
				String res = "";
				for(int j=0; j<arrResult_type.length; j++) {
					if(j ==0)
						res += arrResult_type[j];
					else
						res += "," + arrResult_type[j];
				}
				allLogInq.setResult_type(res);
			}
			String checkEmpNameMasking = commonDao.checkEmpNameMasking();
			if(checkEmpNameMasking.equals("Y")) {
				if(!"".equals(allLogInq.getEmp_user_name())) {
					String empUserName = allLogInq.getEmp_user_name();
					StringBuilder builder = new StringBuilder(empUserName);
					builder.setCharAt(empUserName.length() - 2, '*');
					allLogInq.setEmp_user_name(builder.toString());
				}
			}
			if(allLogInq.getEmp_user_id() != null) {
				search.setEmp_user_idD2(allLogInq.getEmp_user_id());
				data2 = extrtCondbyInqDao.getExtrtCondbyInqDetail2_1(search);
			}
			search.setSystem_seq_temp(allLogInq.getSystem_seq());
			if ("M".equals(ui_type)) {
				allLogInqDetailList = allLogInqDao.findAllLogInqDetailResultOwner(search);
			} else {
				allLogInqDetailList = allLogInqDao.findAllLogInqDetailResult_kdic(search);
				for(AllLogInq v : allLogInqDetailList){
					if(v.getResult_content() != null && v.getResult_content() != ""){
						// 보훈병원
						if (ui_type.equals("B") && sub_className.length() > 0) {
							if (v.getResult_type() != null && v.getPrivacy_seq() == 11) {
								Object[] param = {v.getResult_content()};
								Map findColumn = new HashMap<>();
								findColumn.put("pt_name", "string");
								List res = JDBCConnectionUtil.ConnectionResult(sub_className, sub_url, sub_id, sub_pw, sub_sql, param, findColumn);
								if (res.size() > 0) {
									Map resMap = (Map) res.get(0);
									if (resMap.get("pt_name") != null)
										v.setEmp_user_name((String)resMap.get("pt_name"));
								}
							} else if (v.getResult_type() != null && v.getPrivacy_seq() == 12) {
									Object[] param = {v.getResult_content()};
									Map findColumn = new HashMap<>();
									findColumn.put("patId", "string");
									List res = JDBCConnectionUtil.ConnectionResult(pacs_className, pacs_url, pacs_id, pacs_pw, pacs_sql, param, findColumn);
									if (res.size() > 0) {
										Map resMap = (Map) res.get(0);
										if (resMap.get("patId") != null) {
											Object[] param1 = {resMap.get("patId")};
											Map findColumn2 = new HashMap<>();
											findColumn2.put("pt_name", "string");
											List res1 = JDBCConnectionUtil.ConnectionResult(sub_className, sub_url, sub_id, sub_pw, sub_sql, param1, findColumn2);
											if (res1.size() > 0) {
												Map resMap1 = (Map) res1.get(0);
												if (resMap1.get("pt_name") != null)
													v.setEmp_user_name((String)resMap1.get("pt_name"));
											}
										}
									}
								}
						}
						
						String pResult = v.getResult_content();
						if(v.getResult_type() != null && v.getPrivacy_masking() != null && v.getPrivacy_masking().equals("Y") && insert_biz_log_result.equals("Y")) {
							pResult = getResultContextByMasking(pResult, v.getResult_type());
						}
						v.setResult_content_masking(pResult);
					}
				}
			}
			allLogInqDetailList2 = allLogInqDao.findAllLogInqDetailResult2(search);
		}catch(Exception e) {
			logger.error("[AllLogInqSvcImpl.findAllLogInqDetail] MESSAGE : " + e.getMessage());
			throw new ESException("SYS005V");
		}
		
		SimpleCode simpleCode = new SimpleCode();
		String index = "/allLogInq/detail.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("allLogInq", allLogInq);
		modelAndView.addObject("allLogInqDetailList", allLogInqDetailList);
		modelAndView.addObject("allLogInqDetailList2", allLogInqDetailList2);
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("ui_type", ui_type);
		modelAndView.addObject("biz_log_file", biz_log_file);
		modelAndView.addObject("insert_biz_log_result", insert_biz_log_result);
		if ( data2.size() > 0 ) {
			modelAndView.addObject("data2", data2);
		}
		if (search.getResult_owner_flag() != null && search.getResult_owner_flag().equals("Y")) { 
			  search.setResult_owner("true");
			  modelAndView.addObject("result_owner_falg", search.getResult_owner_flag());
		}
		String biz_log_sql = (String) session.getAttribute("sbiz_log_sql");
		if(biz_log_sql != null && biz_log_sql.equals("Y")) {
			// 여주대학교 커스터마이징
			if (ui_type.equals("Y") && allLogInq.getSystem_seq().equals("04")) {
				AllLogInq detail_query = allLogInqDao.findAllLogInqDetailQuery_YJ(search);
				modelAndView.addObject("detail_query", detail_query);
			} else {
				AllLogInq detail_query = allLogInqDao.findAllLogInqDetailQuery(search);
				if(detail_query != null) {
					String tmpList[] = detail_query.getQuery_id().split("\r\n");
					List<String> querylist = new ArrayList<String>();
					List<String> querylist2 = new ArrayList<String>();
					for ( int i=0; i< tmpList.length; ++i ) {
						if ( !tmpList[i].equals("0")) {
							querylist.add(tmpList[i]);
						}
					}
					for(int i=0; i<querylist.size(); i++) {
						if(querylist.get(i).length() >0) {
							querylist2.add(querylist.get(i));
						}
					}
					modelAndView.addObject("querylist2", querylist2);
					modelAndView.addObject("querylistSize", querylist2.size());
					modelAndView.addObject("detail_query", detail_query);
				}
			}
		}
		
		String biz_log_body_req = (String) session.getAttribute("sbiz_log_body_req");
		String biz_log_body_res = (String) session.getAttribute("sbiz_log_body_res");
		if((biz_log_body_req != null && biz_log_body_req.equals("Y")) || (biz_log_body_res != null && biz_log_body_res.equals("Y"))) {
			AllLogInq detail_body = allLogInqDao.findAllLogInqDetailBody(search);
			modelAndView.addObject("detail_body", detail_body);
		}
		
		if(biz_log_file != null && biz_log_file.equals("Y")) {
			List<AllLogInq> allLogInqFileList = allLogInqDao.findLogInqFileList(search);
			modelAndView.addObject("allLogInqFileList", allLogInqFileList);
			List<AllLogInq> allLogInqApprovalList = allLogInqDao.findLogInqApprovalList(search);
			modelAndView.addObject("allLogInqApprovalList", allLogInqApprovalList);
		}
		modelAndView.addObject("biz_log_sql", biz_log_sql);
		modelAndView.addObject("biz_log_body_req", biz_log_body_req);
		modelAndView.addObject("biz_log_body_res", biz_log_body_res);
		modelAndView.addObject("mode_info_viewer", commonDao.getInfoViewerUse());
		modelAndView.addObject("resultTypeList", allLogInqDao.getResultTypeListByLogseq(search));
		String sbiz_log_filedownload = (String) session.getAttribute("sbiz_log_filedownload");
		modelAndView.addObject("sbiz_log_filedownload", sbiz_log_filedownload);
		
		
		String[] logSqlSystem = log_sql_system.split(",");
		String system_seq = allLogInq.getSystem_seq();
		String log_sql_yn = "N";
		for(String seq : logSqlSystem) {
			if(system_seq.equals(seq.trim())) {
				log_sql_yn = "Y";
			}
		}
		modelAndView.addObject("log_sql_yn", log_sql_yn);
		if(log_sql_yn.equals("Y")) {
			// sql
			modelAndView.addObject("tab_flag", search.getTab_flag());
			AllLogSql allLogSql = new AllLogSql();
			allLogSql.setLog_seq(Long.parseLong(search.getDetailLogSeq()));
			allLogSql.setProc_datetime(allLogInq.getProc_date());
			// sql count
			int logSqlResultCt = allLogInqDao.allLogSqlResultCt(allLogSql);
			modelAndView.addObject("logSqlResultCt", logSqlResultCt);
			List<String> sqlPrivacyInfo = allLogInqDao.allLogSqlCtAndHasPrivacyInfo(allLogSql);
			modelAndView.addObject("sqlPrivacyInfo", sqlPrivacyInfo);
			if(sqlPrivacyInfo.size()!=0) {
				// SQL 정보 가져옴
				allLogSql.setLogSqlPageNum(Integer.parseInt(search.getLogSqlPageNum()));
				AllLogSql logSqlInfo = allLogInqDao.allLogSqlInfo(allLogSql);
				String[] params = logSqlInfo.getParams().split("\\|");
				logSqlInfo.setParamsList(Arrays.asList(params));
				String[] columns = logSqlInfo.getColumns().split("\\|");
				logSqlInfo.setColumnsList(Arrays.asList(columns));
				modelAndView.addObject("logSqlInfo", logSqlInfo);
				// SQL 결과 가져옴
				allLogSql.setLog_sql_seq(logSqlInfo.getLog_sql_seq());
				PageInfo logSqlResult = allLogSql.getLogSqlResult();
				logSqlResult.setPage_num(Integer.parseInt(search.getLogSqlResultPageNum()));
				logSqlResult.setSize(10);
				logSqlResult.setTotal_count(allLogInqDao.allLogSqlResultListCt(allLogSql));
				List<AllLogSql> logSqlInfoResult = allLogInqDao.allLogSqlResultList(allLogSql);
				for(AllLogSql als : logSqlInfoResult) {
					String[] result = als.getResult().split("\\|");
					als.setResultList(Arrays.asList(result));
				}
				modelAndView.addObject("logSqlInfoResult", logSqlInfoResult);
			}
			modelAndView.addObject("allLogSql", allLogSql);
		}
		
        try {
            search.getSqlPrivacyDetail().setSize(5);
            List<AllLogInq> sqlPrivacyResult = allLogInqDao.findAllLogInqDetail_sqlPrivacyInfo(search);
            Integer sqlPrivacyResultCount = allLogInqDao.findAllLogInqDetail_sqlPrivacyInfo_count(search);
            search.getSqlPrivacyDetail().setTotal_count(sqlPrivacyResultCount);
            
            modelAndView.addObject("sqlPrivacyResult", sqlPrivacyResult);
        } catch (Exception e) {
            e.printStackTrace();
        }
		
		return modelAndView;
	}
	
}
