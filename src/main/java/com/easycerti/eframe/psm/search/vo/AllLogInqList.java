package com.easycerti.eframe.psm.search.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.AbstractValueObject;
/**
 * 
 * 설명 : 전체로그조회 리스트 Bean
 * @author tjlee
 * @since 2015. 4. 22.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 22.           tjlee            최초 생성
 *
 * </pre>
 */
public class AllLogInqList extends AbstractValueObject{

	// 전체로그조회 리스트
	private List<AllLogInq> allLogInq = null;

	
	public AllLogInqList(){
		
	}
	
	public AllLogInqList(List<AllLogInq> allLogInq){
		this.allLogInq = allLogInq;
	}
	
	public AllLogInqList(List<AllLogInq> allLogInq, String page_total_count){
		this.allLogInq = allLogInq;
		setPage_total_count(page_total_count);
	}

	public List<AllLogInq> getAllLogInq() {
		return allLogInq;
	}

	public void setAllLogInq(List<AllLogInq> allLogInq) {
		this.allLogInq = allLogInq;
	}
}
