package com.easycerti.eframe.psm.search.vo;

import java.util.List;

public class ExtrtCondbyInqDetailChart {

	private String result_type;
	private String code_name;
	private int count;
	private String system_name;
	private String proc_time;
	
	private List<Integer> privacyCount;
	private List<String> privacyName;
	private List<String> systemName;
	private List<String> chartInfo1;
	private List<String> chartInfo2;
	private List<String> chartInfo0;
	
	private int nPrivacyCount1=0;
	private int nPrivacyCount2=0;
	private int nPrivacyCount3=0;
	private int nPrivacyCount4=0;
	private int nPrivacyCount5=0;
	private int nPrivacyCount6=0;
	private int nPrivacyCount7=0;
	private int nPrivacyCount8=0;
	private int nPrivacyCount9=0;
	private int nPrivacyCount10=0;

	
	private int dng_sum_val;
	private String dng_grade;
	private String emp_user_id;
	private String emp_user_name;
	private String dept_id;
	private String dept_name;
	private String user_ip;
	
	
	public int getDng_sum_val() {
		return dng_sum_val;
	}
	public void setDng_sum_val(int dng_sum_val) {
		this.dng_sum_val = dng_sum_val;
	}
	public String getDng_grade() {
		return dng_grade;
	}
	public void setDng_grade(String dng_grade) {
		this.dng_grade = dng_grade;
	}
	public String getEmp_user_id() {
		return emp_user_id;
	}
	public void setEmp_user_id(String emp_user_id) {
		this.emp_user_id = emp_user_id;
	}
	public String getEmp_user_name() {
		return emp_user_name;
	}
	public void setEmp_user_name(String emp_user_name) {
		this.emp_user_name = emp_user_name;
	}
	public String getDept_id() {
		return dept_id;
	}
	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}
	public String getDept_name() {
		return dept_name;
	}
	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}
	public String getUser_ip() {
		return user_ip;
	}
	public void setUser_ip(String user_ip) {
		this.user_ip = user_ip;
	}
	public String getSystem_name_list() {
		return system_name;
	}
	public void setSystem_name_list(String system_name) {
		this.system_name = system_name;
	}
	public List<String> getChartInfo0() {
		return chartInfo0;
	}
	public void setChartInfo0(List<String> chartInfo0) {
		this.chartInfo0 = chartInfo0;
	}
	public String getProc_time() {
		return proc_time;
	}
	public void setProc_time(String proc_time) {
		this.proc_time = proc_time;
	}
	public List<String> getChartInfo2() {
		return chartInfo2;
	}
	public void setChartInfo2(List<String> chartInfo2) {
		this.chartInfo2 = chartInfo2;
	}
	public List<String> getChartInfo1() {
		return chartInfo1;
	}
	public void setChartInfo1(List<String> chartInfo1) {
		this.chartInfo1 = chartInfo1;
	}
	public String getSystem_name() {
		return system_name;
	}
	public void setSystem_name(String system_name) {
		this.system_name = system_name;
	}
	public List<String> getSystemName() {
		return systemName;
	}
	public void setSystemName(List<String> systemName) {
		this.systemName = systemName;
	}
	public String getResult_type() {
		return result_type;
	}
	public void setResult_type(String result_type) {
		this.result_type = result_type;
	}
	public String getCode_name() {
		return code_name;
	}
	public void setCode_name(String code_name) {
		this.code_name = code_name;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public List<Integer> getPrivacyCount() {
		return privacyCount;
	}
	public void setPrivacyCount(List<Integer> privacyCount) {
		this.privacyCount = privacyCount;
	}
	public List<String> getPrivacyName() {
		return privacyName;
	}
	public void setPrivacyName(List<String> privacyName) {
		this.privacyName = privacyName;
	}
	public int getnPrivacyCount1() {
		return nPrivacyCount1;
	}
	public void setnPrivacyCount1(int nPrivacyCount1) {
		this.nPrivacyCount1 = nPrivacyCount1;
	}
	public int getnPrivacyCount2() {
		return nPrivacyCount2;
	}
	public void setnPrivacyCount2(int nPrivacyCount2) {
		this.nPrivacyCount2 = nPrivacyCount2;
	}
	public int getnPrivacyCount3() {
		return nPrivacyCount3;
	}
	public void setnPrivacyCount3(int nPrivacyCount3) {
		this.nPrivacyCount3 = nPrivacyCount3;
	}
	public int getnPrivacyCount4() {
		return nPrivacyCount4;
	}
	public void setnPrivacyCount4(int nPrivacyCount4) {
		this.nPrivacyCount4 = nPrivacyCount4;
	}
	public int getnPrivacyCount5() {
		return nPrivacyCount5;
	}
	public void setnPrivacyCount5(int nPrivacyCount5) {
		this.nPrivacyCount5 = nPrivacyCount5;
	}
	public int getnPrivacyCount6() {
		return nPrivacyCount6;
	}
	public void setnPrivacyCount6(int nPrivacyCount6) {
		this.nPrivacyCount6 = nPrivacyCount6;
	}
	public int getnPrivacyCount7() {
		return nPrivacyCount7;
	}
	public void setnPrivacyCount7(int nPrivacyCount7) {
		this.nPrivacyCount7 = nPrivacyCount7;
	}
	public int getnPrivacyCount8() {
		return nPrivacyCount8;
	}
	public void setnPrivacyCount8(int nPrivacyCount8) {
		this.nPrivacyCount8 = nPrivacyCount8;
	}
	public int getnPrivacyCount9() {
		return nPrivacyCount9;
	}
	public void setnPrivacyCount9(int nPrivacyCount9) {
		this.nPrivacyCount9 = nPrivacyCount9;
	}
	public int getnPrivacyCount10() {
		return nPrivacyCount10;
	}
	public void setnPrivacyCount10(int nPrivacyCount10) {
		this.nPrivacyCount10 = nPrivacyCount10;
	}
}
