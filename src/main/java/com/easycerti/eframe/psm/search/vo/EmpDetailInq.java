package com.easycerti.eframe.psm.search.vo;

import com.easycerti.eframe.common.util.PageInfo;
import com.easycerti.eframe.common.vo.AbstractValueObject;
/**
 * 
 * 설명 : 위험도별조회 VO
 * @author tjlee
 * @since 2015. 4. 22.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 22.           tjlee            최초 생성
 *
 * </pre>
 */
public class EmpDetailInq extends AbstractValueObject{
	private String occr_dt;
	private String dept_id;
	private String emp_user_id;
	private String emp_user_name;
	private String rule_cd;
	private int rule_cnt;
	private int dng_val;
	private String dept_name;
	private String rule_nm;
	private String log_delimiter;
	private int sum_dng_val;
	private String string_agg_rule_nm;
	private String user_ip;
	private long log_seq;
	private String proc_time;
	private String system_seq;
	private String req_url;
	private String result_type;
	private String startday;
	private String endday;
	private int sum_dng_val_code;
	private String sum_dng_val_string;
	private long emp_detail_seq;
	
	private PageInfo pageInfo;

	private String string_agg_rule_nm_enter = "";
	private int string_agg_rule_nm_cnt=0;
	
	private int scen_seq; // 비정상시나리오 코드값
	private String scen_name; // 비정상시나리오명
	private int day_cnt;
	private int month_cnt;
	private String panalType;
	
	public String getPanalType() {
		return panalType;
	}
	public void setPanalType(String panalType) {
		this.panalType = panalType;
	}
	public int getDay_cnt() {
		return day_cnt;
	}
	public void setDay_cnt(int day_cnt) {
		this.day_cnt = day_cnt;
	}
	public int getMonth_cnt() {
		return month_cnt;
	}
	public void setMonth_cnt(int month_cnt) {
		this.month_cnt = month_cnt;
	}
	public String getScen_name() {
		return scen_name;
	}
	public void setScen_name(String scen_name) {
		this.scen_name = scen_name;
	}
	
	public int getScen_seq() {
		return scen_seq;
	}
	public void setScen_seq(int scen_seq) {
		this.scen_seq = scen_seq;
	}
	
	// jsp에서 타이틀 출력을 위해 만듬
	public String getString_agg_rule_nm_enter() {
		if(this.string_agg_rule_nm != null){
			String[] splitRuleAgg = this.string_agg_rule_nm.split(",");
			for(int i=0; i<splitRuleAgg.length; i++){
				string_agg_rule_nm_enter += splitRuleAgg[i] + "\n";;
			}
		}
		return string_agg_rule_nm_enter;
	}
	// jsp에서 추출조건 숫자 확인을 위해 만듬
	public int getString_agg_rule_nm_cnt(){
		if(this.string_agg_rule_nm != null){
			String[] splitRuleAgg = this.string_agg_rule_nm.split(",");
			string_agg_rule_nm_cnt = splitRuleAgg.length;
		}
		return string_agg_rule_nm_cnt;
	}
	
	
	public String getSum_dng_val_string() {
		return sum_dng_val_string;
	}
	public void setSum_dng_val_string(String sum_dng_val_string) {
		this.sum_dng_val_string = sum_dng_val_string;
	}
	public String getReq_url() {
		return req_url;
	}
	public void setReq_url(String req_url) {
		this.req_url = req_url;
	}
	public String getResult_type() {
		return result_type;
	}
	public void setResult_type(String result_type) {
		this.result_type = result_type;
	}
	public long getLog_seq() {
		return log_seq;
	}
	public void setLog_seq(long log_seq) {
		this.log_seq = log_seq;
	}
	public String getProc_time() {
		return proc_time;
	}
	public void setProc_time(String proc_time) {
		this.proc_time = proc_time;
	}
	public String getSystem_seq() {
		return system_seq;
	}
	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}
	public String getUser_ip() {
		return user_ip;
	}
	public void setUser_ip(String user_ip) {
		this.user_ip = user_ip;
	}
	public void setString_agg_rule_nm_enter(String string_agg_rule_nm_enter) {
		this.string_agg_rule_nm_enter = string_agg_rule_nm_enter;
	}
	public void setString_agg_rule_nm_cnt(int string_agg_rule_nm_cnt) {
		this.string_agg_rule_nm_cnt = string_agg_rule_nm_cnt;
	}
	
	public String getOccr_dt() {
		return occr_dt;
	}

	public void setOccr_dt(String occr_dt) {
		this.occr_dt = occr_dt;
	}
	public String getRule_cd() {
		return rule_cd;
	}
	public void setRule_cd(String rule_cd) {
		this.rule_cd = rule_cd;
	}
	public int getRule_cnt() {
		return rule_cnt;
	}

	public void setRule_cnt(int rule_cnt) {
		this.rule_cnt = rule_cnt;
	}

	public int getDng_val() {
		return dng_val;
	}

	public void setDng_val(int dng_val) {
		this.dng_val = dng_val;
	}

	public String getRule_nm() {
		return rule_nm;
	}

	public void setRule_nm(String rule_nm) {
		this.rule_nm = rule_nm;
	}

	public String getLog_delimiter() {
		return log_delimiter;
	}

	public void setLog_delimiter(String log_delimiter) {
		this.log_delimiter = log_delimiter;
	}

	public PageInfo getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(PageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}

	public String getDept_id() {
		return dept_id;
	}

	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}

	public String getEmp_user_id() {
		return emp_user_id;
	}

	public void setEmp_user_id(String emp_user_id) {
		this.emp_user_id = emp_user_id;
	}

	public String getEmp_user_name() {
		return emp_user_name;
	}

	public void setEmp_user_name(String emp_user_name) {
		this.emp_user_name = emp_user_name;
	}

	public String getDept_name() {
		return dept_name;
	}

	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}

	public int getSum_dng_val() {
		return sum_dng_val;
	}

	public void setSum_dng_val(int sum_dng_val) {
		this.sum_dng_val = sum_dng_val;
	}

	public String getString_agg_rule_nm() {
		return string_agg_rule_nm;
	}

	public void setString_agg_rule_nm(String string_agg_rule_nm) {
		this.string_agg_rule_nm = string_agg_rule_nm;
	}
	public String getStartday() {
		return startday;
	}
	public void setStartday(String startday) {
		this.startday = startday;
	}
	public String getEndday() {
		return endday;
	}
	public void setEndday(String endday) {
		this.endday = endday;
	}
	public int getSum_dng_val_code() {
		return sum_dng_val_code;
	}
	public void setSum_dng_val_code(int sum_dng_val_code) {
		this.sum_dng_val_code = sum_dng_val_code;
	}
	public long getEmp_detail_seq() {
		return emp_detail_seq;
	}
	public void setEmp_detail_seq(long emp_detail_seq) {
		this.emp_detail_seq = emp_detail_seq;
	}
	// toString
	@Override
	public String toString() {
		return "EmpDetailInq [occr_dt=" + occr_dt + ", dept_id=" + dept_id + ", emp_user_id=" + emp_user_id + ", emp_user_name=" + emp_user_name + ", rule_cd=" + rule_cd + ", rule_cnt=" + rule_cnt
				+ ", dng_val=" + dng_val + ", dept_name=" + dept_name + ", rule_nm=" + rule_nm + ", log_delimiter=" + log_delimiter + ", sum_dng_val=" + sum_dng_val + ", string_agg_rule_nm="
				+ string_agg_rule_nm + ", user_ip=" + user_ip + ", log_seq=" + log_seq + ", proc_time=" + proc_time + ", system_seq=" + system_seq + ", req_url=" + req_url + ", result_type="
				+ result_type + ", startday=" + startday + ", endday=" + endday + ", sum_dng_val_code=" + sum_dng_val_code + ", sum_dng_val_string=" + sum_dng_val_string + ", emp_detail_seq="
				+ emp_detail_seq + ", pageInfo=" + pageInfo + ", string_agg_rule_nm_enter=" + string_agg_rule_nm_enter + ", string_agg_rule_nm_cnt=" + string_agg_rule_nm_cnt + "]";
	}
}
