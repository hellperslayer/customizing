package com.easycerti.eframe.psm.search.service.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.util.ExcelFileReader;
import com.easycerti.eframe.common.util.XExcelFileReader;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.dashboard.dao.PivotchartDao;
import com.easycerti.eframe.psm.dashboard.service.PivotchartSvc;
import com.easycerti.eframe.psm.dashboard.vo.Dashboard;
import com.easycerti.eframe.psm.search.dao.AllLogInqDao;
import com.easycerti.eframe.psm.search.dao.ExtrtCondbyInqDao;
import com.easycerti.eframe.psm.search.service.ExtrtCondbyInqSvc;
import com.easycerti.eframe.psm.search.vo.AllLogInq;
import com.easycerti.eframe.psm.search.vo.EmpDetailInq;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInq;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInqDetailChart;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInqList;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInqTimeline;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInqTimelineCond;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.setup.dao.ExtrtBaseSetupDao;
import com.easycerti.eframe.psm.setup.dao.IndvinfoTypeSetupDao;
import com.easycerti.eframe.psm.setup.vo.ExtrtBaseSetup;
import com.easycerti.eframe.psm.statistics.vo.Statistics;
import com.easycerti.eframe.psm.system_management.dao.AdminUserMngtDao;
import com.easycerti.eframe.psm.system_management.dao.AgentMngtDao;
import com.easycerti.eframe.psm.system_management.dao.CodeMngtDao;
import com.easycerti.eframe.psm.system_management.dao.DepartmentMngtDao;
import com.easycerti.eframe.psm.system_management.dao.EmpUserMngtDao;
import com.easycerti.eframe.psm.system_management.dao.MenuMngtDao;
import com.easycerti.eframe.psm.system_management.dao.SystemMngtDao;
import com.easycerti.eframe.psm.system_management.service.AlarmMngtSvc;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;
import com.easycerti.eframe.psm.system_management.vo.AdminUserSearch;
import com.easycerti.eframe.psm.system_management.vo.Delegation;
import com.easycerti.eframe.psm.system_management.vo.DelegationList;
import com.easycerti.eframe.psm.system_management.vo.Department;
import com.easycerti.eframe.psm.system_management.vo.EmpUser;
import com.easycerti.eframe.psm.system_management.vo.EmpUserSearch;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

/**
 * 
 * 설명 : 추출조건별조회 Service Implements
 * 
 * @author tjlee
 * @since 2015. 4. 22.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 22.           tjlee            최초 생성
 *   2017. 5. 24.			sylee			detailCart2_5관련내용 추가
 *
 *      </pre>
 */
@Service
public class ExtrtCondbyInqSvcImpl implements ExtrtCondbyInqSvc {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ExtrtCondbyInqDao extrtCondbyInqDao;

	@Autowired
	private PivotchartSvc pivotchartSvc;

	@Autowired
	private PivotchartDao divotchartDao;

	@Autowired
	private IndvinfoTypeSetupDao indvinfoTypeSetupDao;

	@Autowired
	private DataSourceTransactionManager transactionManager;

	@Autowired
	private MenuMngtDao menuDao;

	@Autowired
	private CommonDao commonDao;

	@Autowired
	private ExtrtBaseSetupDao extrtBaseSetupDao;

	@Autowired
	private AllLogInqDao allLogInqDao;

	@Autowired
	private AgentMngtDao agentMngtDao;
	
	@Autowired
	AlarmMngtSvc alarmMngtSvc;
	
	@Autowired
	private SystemMngtDao systemMngtDao;

	@Autowired
	private EmpUserMngtDao empUserMngtDao;
	
	@Autowired
	private DepartmentMngtDao deptMngtDao;
	
	@Autowired
	private AdminUserMngtDao adminUserMngtDao;
	
	@Autowired
	private CodeMngtDao codeMngtDao;
	
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;
	
	@Value("#{configProperties.summonFilePath}")
	private String summonFilePath;
	
	/**
	 * 추출조건별조회 리스트
	 */
	@Override
	public DataModelAndView findExtrtCondbyInqList(SearchSearch search) {

		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		try {
		search.setTotal_count(extrtCondbyInqDao.extrtCondbyInqListCt(search));
		}catch (Exception e) {
			e.printStackTrace();
		}
		List<ExtrtBaseSetup> ruleTblList = extrtCondbyInqDao.findRuleTblListByScenSeq(search);
		List<ExtrtBaseSetup> scenarioList = extrtBaseSetupDao.findScenarioList(null);

		List<ExtrtCondbyInq> ExtrtCondbyInq = extrtCondbyInqDao.extrtCondbyInqList(search);
		ExtrtCondbyInqList extrtCondbyInqList = new ExtrtCondbyInqList(ExtrtCondbyInq);
		//List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList();
		
		String checkEmpNameMasking = commonDao.checkEmpNameMasking();
		if(checkEmpNameMasking.equals("Y")) {
			for (int i = 0; i < ExtrtCondbyInq.size(); i++) {
				if(!"".equals(ExtrtCondbyInq.get(i).getEmp_user_name())) {
					String empUserName = ExtrtCondbyInq.get(i).getEmp_user_name();
					StringBuilder builder = new StringBuilder(empUserName);
					builder.setCharAt(empUserName.length() - 2, '*');
					ExtrtCondbyInq.get(i).setEmp_user_name(builder.toString());
				}
			}
		}
		
		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/list.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);

		
		String ui_type = commonDao.getUiType();
		DataModelAndView modelAndView = new DataModelAndView();

		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList_byAdmin(search);
		modelAndView.addObject("systemMasterList", systemMasterList);
		
		modelAndView.addObject("ruleTblList", ruleTblList);
		modelAndView.addObject("scenarioList", scenarioList);
		modelAndView.addObject("extrtCondbyInqList", extrtCondbyInqList);
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("ui_type", ui_type);

		return modelAndView;
	}
	
	

	@Override
	public DataModelAndView extrtCondbyInqDaily(SearchSearch search) {
		String search_from = search.getSearch_from();
		if (search_from == null || search_from == "") {
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			calendar.add(Calendar.DAY_OF_MONTH, -1);
			search.setSearch_to(sdf.format(calendar.getTime()));
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			search.setSearch_from(sdf.format(calendar.getTime()));
		}
		DataModelAndView modelAndView = new DataModelAndView();
		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/extrtCondbyInqDaliyList.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList_byAdmin(search);
		modelAndView.addObject("systemMasterList", systemMasterList);
		
		List<ExtrtBaseSetup> scenarioList = extrtBaseSetupDao.findScenarioList(null);
		modelAndView.addObject("scenarioList", scenarioList);
		
		List<ExtrtCondbyInq> extrtCondbyInqList = extrtCondbyInqDao.findExtrtCondbyInqDaily(search);
		modelAndView.addObject("extrtCondbyInqList", extrtCondbyInqList);
		ExtrtCondbyInq max = extrtCondbyInqDao.findExtrtCondbyInqDailyMax(search);
		modelAndView.addObject("max", max);
		ExtrtCondbyInq sum = extrtCondbyInqDao.findExtrtCondbyInqDailySum(search);
		modelAndView.addObject("sum", sum);
		modelAndView.addObject("search", search);
		return modelAndView;
	}
	@Override
	public DataModelAndView extrtCondbyInqDept(SearchSearch search) {
		String search_from = search.getSearch_from();
		if (search_from == null || search_from == "") {
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			calendar.add(Calendar.DAY_OF_MONTH, -1);
			search.setSearch_to(sdf.format(calendar.getTime()));
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			search.setSearch_from(sdf.format(calendar.getTime()));
		}
		DataModelAndView modelAndView = new DataModelAndView();
		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/extrtCondbyInqDeptList.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList_byAdmin(search);
		modelAndView.addObject("systemMasterList", systemMasterList);
		
		List<ExtrtBaseSetup> scenarioList = extrtBaseSetupDao.findScenarioList(null);
		modelAndView.addObject("scenarioList", scenarioList);
		
		List<ExtrtCondbyInq> extrtCondbyInqList = extrtCondbyInqDao.findExtrtCondbyInqDept(search);
		modelAndView.addObject("extrtCondbyInqList", extrtCondbyInqList);
		ExtrtCondbyInq max = extrtCondbyInqDao.findExtrtCondbyInqDeptMax(search);
		modelAndView.addObject("max", max);
		ExtrtCondbyInq sum = extrtCondbyInqDao.findExtrtCondbyInqDeptSum(search);
		modelAndView.addObject("sum", sum);
		modelAndView.addObject("search", search);
		return modelAndView;
	}
	@Override
	public DataModelAndView extrtCondbyInqUser(SearchSearch search) {
		String search_from = search.getSearch_from();
		if (search_from == null || search_from == "") {
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			calendar.add(Calendar.DAY_OF_MONTH, -1);
			search.setSearch_to(sdf.format(calendar.getTime()));
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			search.setSearch_from(sdf.format(calendar.getTime()));
		}
		DataModelAndView modelAndView = new DataModelAndView();
		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/extrtCondbyInqUserList.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList_byAdmin(search);
		modelAndView.addObject("systemMasterList", systemMasterList);
		
		List<ExtrtBaseSetup> scenarioList = extrtBaseSetupDao.findScenarioList(null);
		modelAndView.addObject("scenarioList", scenarioList);
		
		List<ExtrtCondbyInq> extrtCondbyInqList = extrtCondbyInqDao.findExtrtCondbyInqUser(search);
		modelAndView.addObject("extrtCondbyInqList", extrtCondbyInqList);
		ExtrtCondbyInq max = extrtCondbyInqDao.findExtrtCondbyInqUserMax(search);
		modelAndView.addObject("max", max);
		ExtrtCondbyInq sum = extrtCondbyInqDao.findExtrtCondbyInqUserSum(search);
		modelAndView.addObject("sum", sum);
		modelAndView.addObject("search", search);
		return modelAndView;
	}
	@Override
	public DataModelAndView extrtCondbyInqSystem(SearchSearch search) {
		String search_from = search.getSearch_from();
		if (search_from == null || search_from == "") {
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			calendar.add(Calendar.DAY_OF_MONTH, -1);
			search.setSearch_to(sdf.format(calendar.getTime()));
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			search.setSearch_from(sdf.format(calendar.getTime()));
		}
		DataModelAndView modelAndView = new DataModelAndView();
		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/extrtCondbyInqSystemList.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList_byAdmin(search);
		modelAndView.addObject("systemMasterList", systemMasterList);
		
		List<ExtrtBaseSetup> scenarioList = extrtBaseSetupDao.findScenarioList(null);
		modelAndView.addObject("scenarioList", scenarioList);
		
		List<ExtrtCondbyInq> extrtCondbyInqList = extrtCondbyInqDao.findExtrtCondbyInqSystem(search);
		
		if(extrtCondbyInqList.size() != 0) {
			ExtrtCondbyInq temp = extrtCondbyInqList.get(0); // 공통을 가장 아래로 보내기 위함
			extrtCondbyInqList.remove(0);
			extrtCondbyInqList.add(temp);
		}
		modelAndView.addObject("extrtCondbyInqList", extrtCondbyInqList);
		ExtrtCondbyInq max = extrtCondbyInqDao.findExtrtCondbyInqSystemMax(search);
		modelAndView.addObject("max", max);
		ExtrtCondbyInq sum = extrtCondbyInqDao.findExtrtCondbyInqSystemSum(search);
		modelAndView.addObject("sum", sum);
		modelAndView.addObject("search", search);
		return modelAndView;
	}
	
	@Override
	public void extrtCondbyInqDaliyDownload(DataModelAndView modelAndView, SearchSearch search) {
		search.setUseExcel("true");
		String fileName = "PSM_기간별위험분석_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "기간별위험분석";
		List<ExtrtCondbyInq> extrtCondbyInq = extrtCondbyInqDao.findExtrtCondbyInqDaily(search);
		ExtrtCondbyInq sum = extrtCondbyInqDao.findExtrtCondbyInqDailySum(search);
		sum.setProc_date("합계");
		extrtCondbyInq.add(sum);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyymmdd");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
		for(ExtrtCondbyInq e : extrtCondbyInq) {
			try {
				Date proc_date = simpleDateFormat.parse(e.getProc_date());
				e.setProc_date(sdf.format(proc_date));
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
		}
		String[] heads = new String[] {"일시", "개인정보과다처리", "비정상접근", "특정인처리", "특정시간대처리", "다운로드의심행위", "합계"};
		String[] columns = new String[] {"proc_date", "scenario1", "scenario2", "scenario3", "scenario4", "scenario5", "totalPrivCount"};
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", extrtCondbyInq);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}
	@Override
	public void extrtCondbyInqDeptDownload(DataModelAndView modelAndView, SearchSearch search) {
		search.setUseExcel("true");
		String fileName = "PSM_부서별위험분석_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "부서별위험분석";
		List<ExtrtCondbyInq> extrtCondbyInq = extrtCondbyInqDao.findExtrtCondbyInqDept(search);
		ExtrtCondbyInq sum = extrtCondbyInqDao.findExtrtCondbyInqDeptSum(search);
		sum.setDept_name("합계");
		extrtCondbyInq.add(sum);
		String[] heads = new String[] {"부서명", "부서ID", "개인정보과다처리", "비정상접근", "특정인처리", "특정시간대처리", "다운로드의심행위", "합계"};
		String[] columns = new String[] {"dept_name", "dept_id", "scenario1", "scenario2", "scenario3", "scenario4", "scenario5", "totalPrivCount"};
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", extrtCondbyInq);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}
	@Override
	public void extrtCondbyInqUserDownload(DataModelAndView modelAndView, SearchSearch search) {
		search.setUseExcel("true");
		String fileName = "PSM_개인별위험분석_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "개인별위험분석";
		List<ExtrtCondbyInq> extrtCondbyInq = extrtCondbyInqDao.findExtrtCondbyInqUser(search);
		ExtrtCondbyInq sum = extrtCondbyInqDao.findExtrtCondbyInqUserSum(search);
		sum.setEmp_user_id("합계");
		extrtCondbyInq.add(sum);
		String[] heads = new String[] {"취급자명", "취급자ID", "개인정보과다처리", "비정상접근", "특정인처리", "특정시간대처리", "다운로드의심행위", "합계"};
		String[] columns = new String[] {"emp_user_name", "emp_user_id", "scenario1", "scenario2", "scenario3", "scenario4", "scenario5", "totalPrivCount"};
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", extrtCondbyInq);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}
	@Override
	public void extrtCondbyInqSystemDownload(DataModelAndView modelAndView, SearchSearch search) {
		search.setUseExcel("true");
		String fileName = "PSM_시스템별위험분석_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "시스템별위험분석";
		List<ExtrtCondbyInq> extrtCondbyInq = extrtCondbyInqDao.findExtrtCondbyInqSystem(search);
		ExtrtCondbyInq temp = extrtCondbyInq.get(0); // 공통을 가장 아래로 보내기 위함
		extrtCondbyInq.remove(0);
		extrtCondbyInq.add(temp);
		ExtrtCondbyInq sum = extrtCondbyInqDao.findExtrtCondbyInqSystemSum(search);
		sum.setSystem_name("합계");
		extrtCondbyInq.add(sum);
		String[] heads = new String[] {"시스템명", "개인정보과다처리", "비정상접근", "특정인처리", "특정시간대처리", "다운로드의심행위", "합계"};
		String[] columns = new String[] {"system_name", "scenario1", "scenario2", "scenario3", "scenario4", "scenario5", "totalPrivCount"};
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", extrtCondbyInq);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}



	@Override
	public Map CountOfTopBar(Map<String, String> parameters, SearchSearch search) {
		parameters = CommonHelper.checkSearchDate(parameters);
		search.setOccr_dt(parameters.get("search_to"));
		int deptCnt = extrtCondbyInqDao.countOfdept(search);
		int empCnt = extrtCondbyInqDao.countOfemp(search);
		int deptCntByWeek = extrtCondbyInqDao.countOfdeptByWeek(search);
		int empCntByWeek = extrtCondbyInqDao.countOfempByWeek(search);
		ExtrtCondbyInq data = extrtCondbyInqDao.empDetailCountByWeek(search);

		long empCntTotal = 0;
		String empName = "-";
		String empSeq = "";
		if (data != null) {
			empCntTotal = data.getCnt();
			empName = data.getScen_name();
			empSeq = data.getScen_seq();
		}

		Map result = new HashMap<String, Integer>();
		result.put("deptCnt", deptCnt);
		result.put("empCnt", empCnt);
		result.put("deptCntByWeek", deptCntByWeek);
		result.put("empCntByWeek", empCntByWeek);
		result.put("empCntTotal", empCntTotal);
		result.put("empName", empName);
		result.put("empSeq", empSeq);
		return result;
	}
	


	/**
	 * 추출조건별 조회 상세
	 */
	/*@Override
	public DataModelAndView findExtrtCondbyInqDetail(SearchSearch search) {
		int pageSize = Integer.valueOf(defaultPageSize);
		search.getExtrtCondbyInqDetail().setSize(pageSize);
		
		List<EmpUser> empUser = extrtCondbyInqDao.findEmpUserCond(search);
		ExtrtCondbyInq extrtCondbyInq = extrtCondbyInqDao.findExtrtCondbyInqListByEmpCd(search);
		
		// 추출조건정보
		ExtrtCondbyInq extrtCondbyInq = extrtCondbyInqDao.extractDetailScenarioInfo(search);
		// 직원정보
		List<EmpUser> empUser = new ArrayList<>();
		// 로그 정보
		List<AllLogInq> extrtCondbyInqLogList = null;
		if ("BA".equals(extrtCondbyInq.getLog_delimiter())) {
			search.getExtrtCondbyInqDetail().setTotal_count(extrtCondbyInqDao.findExtrtCondbyInqListByLogListCount(search));
			extrtCondbyInqLogList = extrtCondbyInqDao.findExtrtCondbyInqListByLogList(search);
			empUser = extrtCondbyInqDao.extractEmpUserInfo(search);
			search.getExtrtCondbyInqDetail().setTotal_count(extrtCondbyInqDao.extractLogCount(search));
			extrtCondbyInqLogList = extrtCondbyInqDao.extractLogList(search);
		} else if ("DN".equals(extrtCondbyInq.getLog_delimiter())) {
			empUser = extrtCondbyInqDao.extractDnEmpUserInfo(search);
			search.getExtrtCondbyInqDetail().setTotal_count(extrtCondbyInqDao.findExtrtCondbyInqListByDownloadLogListCount(search));
			extrtCondbyInqLogList = extrtCondbyInqDao.findExtrtCondbyInqListByDownloadLogList(search);
		} else if ("DB".equals(extrtCondbyInq.getLog_delimiter())) {
			search.getExtrtCondbyInqDetail().setTotal_count(extrtCondbyInqDao.findEmpUserCondCount_dbacLog(search));
			extrtCondbyInqLogList = extrtCondbyInqDao.findExtrtCondbyInqDbacLogListByLogList_summon(search);
		}
		String scen_seq = extrtCondbyInq.getScen_seq();
		if(!"1000".equals(scen_seq)) {
			for (int i = 0; i < extrtCondbyInqLogList.size(); i++) {
				AllLogInq ali = extrtCondbyInqLogList.get(i);
				String result_type = ali.getResult_type();
				String[] arrResult_type = result_type.split(",");
				Arrays.sort(arrResult_type);
				String res = "";
				for (int j = 0; j < arrResult_type.length; j++) {
					if (j == 0)
						res += arrResult_type[j];
					else
						res += "," + arrResult_type[j];
				}
				ali.setResult_type(res);
			}
		} else {
			// 추출조건정보 추가
			String desc_result = "";
			String[] privacy_seq_arr = extrtCondbyInqDao.privacySeqInfo(extrtCondbyInq.getRule_cd()).split(",");
			List<IndvinfoTypeSetup> privacyList = commonDao.getIndvinfoTypeList();
			for(int i=0; i<privacy_seq_arr.length;i++) {
				int seq = Integer.parseInt(privacy_seq_arr[i]);
				for(IndvinfoTypeSetup p : privacyList) {
					if(seq == p.getPrivacy_seq()) {
						desc_result += p.getPrivacy_desc()+", ";
						break;
					}
				}
			}
			desc_result = desc_result.substring(0, desc_result.length()-2);
			extrtCondbyInq.setDesc_result(desc_result);
		}

		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/detail.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);

		DataModelAndView modelAndView = new DataModelAndView();

		String ui_type = commonDao.getUiType();

		if ("D".equals(ui_type) || "Guri".equals(ui_type)) {
			search.setSize(pageSize);
			int count = extrtCondbyInqDao.findExtrtCondbyInqListByListCount_info(search);
			search.setTotal_count(count);

			List<ExtrtCondbyInq> extrtCondbyInqLogList_info = extrtCondbyInqDao
					.findExtrtCondbyInqListByList_info(search);
			modelAndView.addObject("extrtCondbyInqLogList_info", extrtCondbyInqLogList_info);
		}
		
		String checkEmpNameMasking = commonDao.checkEmpNameMasking();
		if(checkEmpNameMasking.equals("Y")) {
			for (int i = 0; i < extrtCondbyInqLogList.size(); i++) {
				if(!"".equals(extrtCondbyInqLogList.get(i).getEmp_user_name())) {
					String empUserName = extrtCondbyInqLogList.get(i).getEmp_user_name();
					StringBuilder builder = new StringBuilder(empUserName);
					builder.setCharAt(empUserName.length() - 2, '*');
					extrtCondbyInqLogList.get(i).setEmp_user_name(builder.toString());
				}
			}
			for (int i = 0; i < empUser.size(); i++) {
				if(!"".equals(empUser.get(i).getEmp_user_name())) {
					String empUserName = empUser.get(i).getEmp_user_name();
					StringBuilder builder = new StringBuilder(empUserName);
					builder.setCharAt(empUserName.length() - 2, '*');
					empUser.get(i).setEmp_user_name(builder.toString());
				}
			}
		}

		modelAndView.addObject("empUser", empUser);
		modelAndView.addObject("extrtCondbyInq", extrtCondbyInq);
		modelAndView.addObject("extrtCondbyInqLogList", extrtCondbyInqLogList);
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("ui_type", ui_type);

		return modelAndView;
	}*/

	@Override
	public DataModelAndView findExtrtCondbyInqDetail(SearchSearch search) {
		DataModelAndView modelAndView = new DataModelAndView();
		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/detail.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		// 추출조건정보
		ExtrtCondbyInq extrtCondbyInq = extrtCondbyInqDao.extractDetailScenarioInfo(search);
		modelAndView.addObject("extrtCondbyInq", extrtCondbyInq);
		
		// 로그 정보
		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		search.setTotal_count(extrtCondbyInqDao.extrtCondbyInqDetailListCt(search));
		
		
		List<ExtrtCondbyInq> detailList = null;
		
		//상세시나리오 - 파일 다운로드인 경우 파일명 정보 조회
		if (extrtCondbyInq.getLog_delimiter().equals("DN")){
			detailList = extrtCondbyInqDao.extrtCondbyInqFileNameList(search);
			
			modelAndView.addObject("detailList", detailList);
		}
		else{
			detailList = extrtCondbyInqDao.extrtCondbyInqDetailList(search);
			
			for(ExtrtCondbyInq e : detailList) {
				String result_type = e.getResult_type();
				String result_content = e.getResult_content();
				if(result_content == null || result_content == "") continue;
				result_content = getResultContextByMaskingByPrivacyseq(result_content, result_type);
				e.setResult_content(result_content);
			}
			/*
			 * for(ExtrtCondbyInq e : detailList) { String result_type = e.getResult_type();
			 * if(result_type != null) { String[] arrResult_type = result_type.split(",");
			 * Arrays.sort(arrResult_type); String res = ""; for(int
			 * j=0;j<arrResult_type.length;j++) { if(j == 0) res += arrResult_type[j]; else
			 * res += "," + arrResult_type[j]; } e.setResult_type(res); }
			 */
				
			/*
			 * String result_content = e.getResult_content(); if(result_content == null ||
			 * result_content == "") continue; result_content =
			 * getResultContextByMaskingByPrivacyseq(result_content, result_type);
			 * e.setResult_content(result_content);
			 */
			modelAndView.addObject("detailList", detailList);
			
		}	
		/*
		 * List<ExtrtCondbyInq> detailList =
		 * extrtCondbyInqDao.extrtCondbyInqDetailList(search); for(ExtrtCondbyInq e :
		 * detailList) { String result_type = e.getResult_type(); String result_content
		 * = e.getResult_content(); if(result_content == null || result_content == "")
		 * continue; result_content =
		 * getResultContextByMaskingByPrivacyseq(result_content, result_type);
		 * e.setResult_content(result_content); } modelAndView.addObject("detailList",
		 * detailList);
		 */
		
		return modelAndView;
	}

	/**
	 * 타임라인 불러오기
	 */
	@Override
	public List<ExtrtCondbyInqTimeline> findExtrtCondbyInqTimelineAll(ExtrtCondbyInqTimelineCond condition) {
		SearchSearch search = new SearchSearch();
		search.setSearch_from(condition.getSearch_from());
		search.setSearch_to(condition.getSearch_to());

		return extrtCondbyInqDao.findExtrtCondbyInqTimelineAll(condition);
	}

	@Override
	public List<ExtrtCondbyInqTimeline> findExtrtCondbyInqTimeline_rule(ExtrtCondbyInqTimelineCond condition) {

		SearchSearch search = new SearchSearch();
		search.setSearch_from(condition.getSearch_from());
		search.setSearch_to(condition.getSearch_to());

		return extrtCondbyInqDao.findExtrtCondbyInqTimeline_rule(condition);
	}

	@Override
	public void findExtrtCondbyInqList_download(DataModelAndView modelAndView, SearchSearch search,
			HttpServletRequest request) {

		search.setUseExcel("true");

		String fileName = "PSM_비정상위험분석_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "비정상위험분석";
		
		SimpleDateFormat par = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd");
		List<ExtrtCondbyInq> extrtCondbyInq = extrtCondbyInqDao.extrtCondbyInqList(search);
		for(ExtrtCondbyInq e : extrtCondbyInq) {
			String rule_view_type = e.getRule_view_type();
			if(rule_view_type.equals("R")) {
				e.setRule_view_type("이용량");
			} else {
				e.setRule_view_type("처리량");
			}
			try {
				Date occr_dt = par.parse(e.getOccr_dt());
				e.setOccr_dt(form.format(occr_dt));
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
		}

		String[] heads = new String[] {};
		String[] columns = new String[] {};
		heads = new String[] { "일시", "비정상위험 시나리오", "비정상위험 상세시나리오", "시스템", "소속", "취급자ID", "취급자명", "위험도", "처리기준", "임계치", "횟수"};
		columns = new String[] { "occr_dt", "scen_name", "rule_nm", "system_name", "dept_name", "emp_user_id", "emp_user_name", "dng_val", "rule_view_type", "limit_cnt", "rule_cnt" };
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", extrtCondbyInq);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}

	/** 20190612 소명요청 리스트 엑셀다운로드 **/
	@Override
	public void summonListExcel(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request) {

		search.setUseExcel("true");

		String fileName = "PSM_소명요청리스트_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "소명요청리스트";

		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
		String date = dayTime.format(new Date(time));

		String year = date.substring(0, 4);
		String month = date.substring(6, 7);

		String yearMonthL = year + "0" + month;
		String yearMonth = "";

		if (search.getYear() == null && search.getMonth() == null) {
			search.setYearMonth(yearMonthL);
		} else {
			if (search.getMonth().length() == 1) {
				search.setMonth("0" + search.getMonth());
			}
			yearMonth = search.getYear() + search.getMonth();
			search.setYearMonth(yearMonth);
		}
		
		// 소명요청 리스트
		List<ExtrtCondbyInq> ExtrtCondbyInq = extrtCondbyInqDao.findSummonList(search);
		ExtrtCondbyInqList summonList = new ExtrtCondbyInqList(ExtrtCondbyInq);
		// ExtrtCondbyInqList extrtCondbyInqList = new
		// ExtrtCondbyInqList(ExtrtCondbyInq);

		// 다수 사용자일경우 소속, 사용자ID, 사용자명 null값이기 때문에 다수사용자로 내용 변경
		for (ExtrtCondbyInq a : ExtrtCondbyInq) {
			if (a.getDept_name().equals("xempty")) {
				a.setDept_name("다수사용자");
				a.setEmp_user_id("다수사용자");
				a.setEmp_user_name("다수사용자");
			}
		}
		
		String ui_type = search.getUi_type();
		String[] columns = null;
		String[] heads = null;
		
		if(ui_type.equals("DGB")) {
			columns = new String[] {"occr_dt","system_name", "scen_name", "rule_nm" , "dept_name", "emp_user_id", "emp_user_name","rule_cnt2","avg_cnt","rule_cnt"};
			heads = new String[] { "일시","시스템", "비정상위험 시나리오", "비정상위험 상세시나리오", "소속", "취급자ID", "취급자명","과다처리건","평균","개인정보처리건"};
		}else {
			 columns = new String[] {"occr_dt","system_name", "scen_name", "rule_nm" , "dept_name", "emp_user_id", "emp_user_name","rule_cnt"};
			 heads = new String[] { "일시","시스템", "비정상위험 시나리오", "비정상위험 상세시나리오", "소속", "취급자ID", "취급자명","비정상처리횟수"};
		}
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", ExtrtCondbyInq);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}
	
	@Override
	public void summonDetailExcel(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request) {

		search.setUseExcel("true");

		String fileName = "PSM_소명상세_로그정보_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "PSM_소명상세_로그정보";

		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
		String date = dayTime.format(new Date(time));

		String year = date.substring(0, 4);
		String month = date.substring(6, 7);

		String yearMonthL = year + "0" + month;
		String yearMonth = "";

		if (search.getYear() == null && search.getMonth() == null) {
			search.setYearMonth(yearMonthL);
		} else {
			if (search.getMonth().length() == 1) {
				search.setMonth("0" + search.getMonth());
			}
			yearMonth = search.getYear() + search.getMonth();
			search.setYearMonth(yearMonth);
		}
					
			
		   //소명요청 리스트 
		  List<ExtrtCondbyInq> ExtrtCondbyInq = extrtCondbyInqDao.extrtCondbyInqDetailList(search); 
		  ExtrtCondbyInqList summonList = new ExtrtCondbyInqList(ExtrtCondbyInq);  
		  ExtrtCondbyInqList extrtCondbyInqList = new ExtrtCondbyInqList(ExtrtCondbyInq);
		  
		  
		  // 다수 사용자일경우 소속, 사용자ID, 사용자명 null값이기 때문에 다수사용자로 내용 변경 
		  for (ExtrtCondbyInq a :ExtrtCondbyInq) {
			    a.setEmp_user_id(search.getEmp_user_id());
			    a.setEmp_user_name(search.getEmp_user_name());
				/*
				 * if (a.getDept_name().equals("xempty")) { a.setDept_name("다수사용자");
				 * a.setEmp_user_id("다수사용자"); a.setEmp_user_name("다수사용자"); }
				 */
			 if(a.getResult_type()!=null) {
				 String str = a.getResult_type();
				 String[] arrays = str.split(",");	
				 int cnt = arrays.length;
				 a.setResult_type(String.valueOf(cnt));
			 }
			 //접근행위
			 String req_type = a.getReq_type();
			 switch(req_type) {
			 	case "CO":
				a.setReq_type("수집");
				break;
			 	case "CR":
			 	a.setReq_type("등록");
			 	break;
			 	case "DL":
			 	a.setReq_type("삭제");
			 	break;
			 	case "DN":
			 	a.setReq_type("다운로드");
			 	break;
			 	case "PR":
			 	a.setReq_type("출력");
			 	break;
			 	case "RD":
			 	a.setReq_type("조회");
			 	break;
			 	case "UD":
			 	a.setReq_type("수정");
			 	break;
			 }
			  
		 }
		 String[] columns = new String[] 
		{ "occr_dt","proc_time","emp_user_id","emp_user_name","ip","system_name","result_type","req_type","req_url"}; 
		 String[] heads = new String[] 
		{"일시","시간","취급자ID","취급자명","IP","시스템","열람한 개인정보 유형(횟수)","접근행위","접근경로"};
		 
		  modelAndView.addObject("fileName", fileName);
		  modelAndView.addObject("sheetName", sheetName);
		  modelAndView.addObject("excelData", ExtrtCondbyInq);
		  modelAndView.addObject("columns", columns); 
		  modelAndView.addObject("heads",heads);
	}

	/** 20190612 소명관리 리스트 엑셀다운로드 **/
	@Override
	public void summonManageListExcel(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request) {

		search.setUseExcel("true");

		String fileName = "PSM_소명관리 리스트_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "소명관리현황";

		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
		String date = dayTime.format(new Date(time));

		String year = date.substring(0, 4);
		String month = date.substring(6, 7);

		String yearMonthL = year + "0" + month;
		String yearMonth = "";

		if (search.getYear() == null && search.getMonth() == null) {
			search.setYearMonth(yearMonthL);
		} else {
			if (search.getMonth().length() == 1) {
				search.setMonth("0" + search.getMonth());
			}
			yearMonth = search.getYear() + search.getMonth();
			search.setYearMonth(yearMonth);
		}
		// 최종권한 여부(summon_cfm_yn) Y일때, 최종권한 승인내용(msg_decision2)를 엑셀에 추가해주기위해 추가한값 
		search.setSummon_cfm_yn((String)request.getSession().getAttribute("summon_cfm_yn"));
		String chk_summon_cfm_yn = (String)request.getSession().getAttribute("summon_cfm_yn");
		//
		
		// 소명관리 리스트
		List<ExtrtCondbyInq> ExtrtCondbyInq = extrtCondbyInqDao.findSummonManageList(search);
		// 판정구분 코드값별로 각각 저장 예) 10 적정, 20 부적정, 30 재소명 그외 미판정
		for (ExtrtCondbyInq a : ExtrtCondbyInq) {
			
			String summon_temp = a.getSummon_status();
			switch (summon_temp) {
			case "10":
				a.setStatusValue("소명요청");
				break;
			case "20":
				a.setStatusValue("재소명");
				break;
			case "30":
				a.setStatusValue("소명응답");
				break;
			case "40":
				a.setStatusValue("소명응답");
				break;
			case "60":
				a.setStatusValue("최종승인완료");
			}
			
			//DGB 전용
			if("Y".equals(chk_summon_cfm_yn) && a.getStatusValue().equals("소명응답") ) {
				a.setStatusValue("부서승인완료");
			}
			//
			
			String decision_temp = a.getDecision_status();
			switch (decision_temp) {
			case "10":
				a.setDesc_result("미판정");
				break;
			case "20":
				a.setDesc_result("재소명");
				break;
			case "30":
				a.setDesc_result("적정");
				break;
			case "40":
				a.setDesc_result("부적정");
				break;
			}
			
			// DGB전용 summon_info테이블의  decision_status_second 컬럼 (기본값10) 이아닐 때,엑셀에 승인구분값을 변경함
			String decision_status_second = a.getDecision_status_second();
			switch(decision_status_second) {
			case "30":
				a.setDesc_result("적정(최종)");
				break;
			case "40":
				a.setDesc_result("부적정(최종)");
			}
			
			//a.setDetect_div(a.getDetect_div().equals("9")?"센터소명":"자체소명");
		}

		ExtrtCondbyInqList summonList = new ExtrtCondbyInqList(ExtrtCondbyInq);
		String[] columns = null;
		String[] heads   = null;
		
		//소명최종판정_사용여부 summon_cfm_yn = Y일때//
		if("Y".equals(chk_summon_cfm_yn)) {
			columns = new String[] { "occr_dt", "dept_name", 
					  "emp_user_id", "emp_user_name","system_name","scen_name","rule_nm","rule_cnt", "msg_reply","msg_decision", "desc_result","msg_decision2"};
			
			String ui_type = commonDao.getUiType();
			
			if("DGB".equals(ui_type)) {
				heads = new String[] { "일시", "소속", 
						"취급자ID", "취급자명","시스템","비정상위험 시나리오","비정상위험 상세시나리오","과다처리건", "소명응답내용", "소명승인내용","승인구분","승인결과"};
			}else {
				heads = new String[] { "일시", "소속", 
						"취급자ID", "취급자명","시스템","비정상위험 시나리오","비정상위험 상세시나리오","비정상걸린횟수", "소명응답내용", "소명승인내용","승인구분","승인결과"};
			}
			
		//소명최종판정_사용여부 summon_cfm_yn = N일때(일반 소명관리 목록화면)
		}else {
			columns = new String[] { "occr_dt", "req_dt", "res_dt", "decision_dt", "system_name", "dept_name", 
					  "emp_user_id", "emp_user_name","scen_name","rule_nm","rule_cnt","statusValue","desc_result", "msg_reply", "msg_decision"};
			heads = new String[] { "수집일", "요청일", "답변일", "판정일", "시스템", "소속", 
					"취급자ID", "취급자명","비정상위험 시나리오","비정상위험 상세시나리오","비정상걸린횟수","진행상태", "판정구분", "소명응답","소명승인내용"};
		}
		

		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", ExtrtCondbyInq);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}

	/** 20190613 부적정판정자 관리 리스트 엑셀다운로드 **/
	@Override
	public void abuseInfoDownloadExcel(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request) {

		search.setUseExcel("true");
		String fileName = "PSM_부적정판정자 관리 리스트_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "부적정판정자 관리 현황";

		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
		String date = dayTime.format(new Date(time));

		String year = date.substring(0, 4);
		String month = date.substring(6, 7);

		String yearMonthL = year + "0" + month;
		String yearMonth = "";

		if (search.getYear() == null && search.getMonth() == null) {
			search.setYearMonth(yearMonthL);
		} else {
			if (search.getMonth().length() == 1) {
				search.setMonth("0" + search.getMonth());
			}
			yearMonth = search.getYear() + search.getMonth();
			search.setYearMonth(yearMonth);
		}
		// 기존 리스트 쿼리
		List<ExtrtCondbyInq> abuseList = extrtCondbyInqDao.findAbuseInfoList(search);
		// 엑셀 전용 쿼리
		List<ExtrtCondbyInq> ExtrtCondbyInq = extrtCondbyInqDao.findAbuseInfoListExcel(search);

		// 예시 소명요청 리스트
		// List<ExtrtCondbyInq> ExtrtCondbyInq =
		// extrtCondbyInqDao.findSummonList(search);

		// 다수 사용자일경우 소속, 사용자ID, 사용자명 null값이기 때문에 다수사용자로 내용 변경
		/*
		 * for(ExtrtCondbyInq a : ExtrtCondbyInq) {
		 * if(a.getDept_name().equals("xempty")) { a.setDept_name("다수사용자");
		 * a.setEmp_user_id("다수사용자"); a.setEmp_user_name("다수사용자"); } }
		 */

		String[] columns = new String[] { "occr_dt", "rule_nm", "request_dt", "dept_name", "request_user_name",
				"request_target_user_name", "result_user_name", "abuse_purpose", "abuse_type", "fault_degree",
				"abuse_content", "administrative_dispo_day", "criminal_dispo_day", "etc_dispo_day" };
		String[] heads = new String[] { "일시", "상세시나리오", "소명요청일", "소속", "소명요청자", "소명답변자", "소명판정자", "오남용목적", "오남용유형",
				"과실정도", "위반내역", "행정처분 처리일자", "형사처분 처리일자", "기타처분 처리일자" };

		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", ExtrtCondbyInq);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}

	/** 20180314 sysong 월별 소명 현황 엑셀 다운로드 **/
	@Override
	public void summonPerby_download(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request) {

		search.setUseExcel("true");

		String monthL = search.getMonth();

		String fileName = "PSM_월별소명현황_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = monthL + "월 소명현황";

		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
		String date = dayTime.format(new Date(time));

		String year = date.substring(0, 4);
		String month = date.substring(6, 7);

		String yearMonthL = year + "0" + month;
		String yearMonth = "";

		if (search.getYear() == null && search.getMonth() == null) {
			search.setYearMonth(yearMonthL+"01");
		} else {
			if (search.getMonth().length() == 1) {
				search.setMonth("0" + search.getMonth());
			}
			yearMonth = search.getYear() + search.getMonth();
			search.setYearMonth(yearMonth+"01");
		}

		List<ExtrtCondbyInq> extrtCondbyInq = extrtCondbyInqDao.findsummonPerby(search);

		String[] columns = new String[] { "scen_name", "rule_nm", "pre_cnt", "this_cnt", "gap" };
		String[] heads = new String[] { "추출조건", "추출조건상세", "이전 월", "당월", "증감" };

		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", extrtCondbyInq);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}

	/** 180314 sysong 소속별 소명 현황 엑셀 다운로드 **/
	@Override
	public void summonDeptby_download(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request) {

		search.setUseExcel("true");

		String monthL = search.getMonth();

		String fileName = "PSM_소속별_소명현황_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = monthL + "월 소속별 소명현황";

		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
		String date = dayTime.format(new Date(time));

		String year = date.substring(0, 4);
		String month = date.substring(6, 7);

		String yearMonthL = year + "0" + month;
		String yearMonth = "";

		if (search.getYear() == null && search.getMonth() == null) {
			search.setYearMonth(yearMonthL+"01");
		} else {
			if (search.getMonth().length() == 1) {
				search.setMonth("0" + search.getMonth());
			}
			yearMonth = search.getYear() + search.getMonth();
			search.setYearMonth(yearMonth+"01");
		}

		List<ExtrtCondbyInq> extrtCondbyInq = extrtCondbyInqDao.findsummonDeptby(search);

		String[] columns = new String[] { "dept_name", "scen_name", "rule_nm", "pre_cnt", "this_cnt", "gap" };
		String[] heads = new String[] { "소속", "추출조건", "추출조건상세", "이전 월", "당월", "증감" };

		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", extrtCondbyInq);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}

	/** 180314 sysong 개인별 소명 현황 **/
	@Override
	public void summonIndvby_download(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request) {

		search.setUseExcel("true");

		String monthL = search.getMonth();

		String fileName = "PSM_개인별_소명현황_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = monthL + "월 개인별 소명현황";

		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
		String date = dayTime.format(new Date(time));

		String year = date.substring(0, 4);
		String month = date.substring(6, 7);

		String yearMonthL = year + "0" + month;
		String yearMonth = "";

		if (search.getYear() == null && search.getMonth() == null) {
			search.setYearMonth(yearMonthL+"01");
		} else {
			if (search.getMonth().length() == 1) {
				search.setMonth("0" + search.getMonth());
			}
			yearMonth = search.getYear() + search.getMonth();
			search.setYearMonth(yearMonth+"01");
		}

		List<ExtrtCondbyInq> extrtCondbyInq = extrtCondbyInqDao.findsummonIndvby(search);

		String[] columns = new String[] { "emp_user_id", "emp_user_name", "dept_name", "scen_name", "rule_nm",
				"log_cnt", "pre_cnt", "this_cnt", "gap" };
		String[] heads = new String[] { "사용자ID", "사용자명", "소속", "추출조건", "추출조건상세", "과다처리 건", "이전 월", "당월", "증감" };

		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", extrtCondbyInq);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}

	/** 20180314 sysong 소명요청 및 판정 현황 엑셀 다운로드 **/
	@Override
	public void summonProcessby_download(DataModelAndView modelAndView, SearchSearch search,
			HttpServletRequest request) {

		search.setUseExcel("true");

		String monthL = search.getMonth();

		String fileName = "PSM_소명요청_및_판정현황_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = monthL + "월 소명요청 및 판정현황";

		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
		String date = dayTime.format(new Date(time));

		String year = date.substring(0, 4);
		String month = date.substring(6, 7);

		String yearMonthL = year + "0" + month;
		String yearMonth = "";

		if (search.getYear() == null && search.getMonth() == null) {
			search.setYearMonth(yearMonthL+"01");
		} else {
			if (search.getMonth().length() == 1) {
				search.setMonth("0" + search.getMonth());
			}
			yearMonth = search.getYear() + search.getMonth();
			search.setYearMonth(yearMonth+"01");
		}

		List<ExtrtCondbyInq> extrtCondbyInq = extrtCondbyInqDao.findsummonProcessby(search);

		String[] columns = new String[] { "dept_name", "scen_name", "rule_nm", "s_nm2", "snm_4", "snm_5", "snm_8" };
		String[] heads = new String[] { "소속", "추출조건", "추출조건상세", "소명요청 건", "판정대기 건", "재요청 건", "판정완료 건" };

		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", extrtCondbyInq);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}

	@Override
	public DataModelAndView sendRequest(Map<String, String> parameters) {
		SearchSearch paramBean = CommonHelper.convertMapToBean(parameters, SearchSearch.class);
		// BizLog에 추출로그와 관련된 log_seq를 가져온다.
		List<ExtrtCondbyInq> logSeq = extrtCondbyInqDao.getBizLogSeq(paramBean);
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			extrtCondbyInqDao.sendRequest(paramBean);
			for (ExtrtCondbyInq extrtCondbyInq : logSeq) {
				extrtCondbyInqDao.setBizLogStatus(extrtCondbyInq);
			}
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			// logger.error("[extrtCondbyInqSvcImpl.sendRequest] MESSAGE : " +
			// e.getMessage());
			throw new ESException("SYS033J");
		}

		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);

		return modelAndView;
	}

	@Override
	public DataModelAndView findLineChart(Map<String, String> parameters) {
		DataModelAndView modelAndView = new DataModelAndView();
		SearchSearch paramBean = CommonHelper.convertMapToBean(parameters, SearchSearch.class);
		if (paramBean.getSearch_from() == null || paramBean.getSearch_from() == "" && paramBean.getSearch_to() == null
				|| paramBean.getSearch_to() == "") {
			paramBean.setSearch_from(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
		}
		if (paramBean.getSearch_from() == null || paramBean.getSearch_from() == "") {
			paramBean.setSearch_from(paramBean.getSearch_to());
		}
		if (paramBean.getSearch_to() == null || paramBean.getSearch_to() == "") {
			paramBean.setSearch_to(paramBean.getSearch_from());
		}

		List<Dashboard> lineChart = null;

		if (paramBean.getDay_month_year().equals("Day")) {
			lineChart = extrtCondbyInqDao.findLineChartDay(paramBean);
		} else if (paramBean.getDay_month_year().equals("Month")) {
			lineChart = extrtCondbyInqDao.findLineChartMonth(paramBean);
		} else if (paramBean.getDay_month_year().equals("Year")) {
			lineChart = extrtCondbyInqDao.findLineChartYear(paramBean);
		}

		modelAndView.addObject("lineChart", lineChart);
		modelAndView.addObject("paramBean", paramBean);

		return modelAndView;
	}

	@Override
	public DataModelAndView detailPieChart(Map<String, String> parameters) {
		SearchSearch paramBean = CommonHelper.convertMapToBean(parameters, SearchSearch.class);
		if (paramBean.getEmp_detail_seq() == null || paramBean.getEmp_detail_seq().equals("")) {
			paramBean.setEmp_detail_seq(paramBean.getEmp_detail_seq());
		}

		List<AllLogInq> pieChart = null;

		pieChart = extrtCondbyInqDao.detailPieChart(paramBean);

		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("pieChart", pieChart);
		modelAndView.addObject("paramBean", paramBean);

		return modelAndView;
	}

	@Override
	public DataModelAndView detailBarChart(Map<String, String> parameters) {
		SearchSearch paramBean = CommonHelper.convertMapToBean(parameters, SearchSearch.class);
		if (paramBean.getEmp_detail_seq() == null || paramBean.getEmp_detail_seq().equals("")) {
			paramBean.setEmp_detail_seq(paramBean.getEmp_detail_seq());
		}

		List<AllLogInq> barChart = null;

		barChart = extrtCondbyInqDao.detailBarChart(paramBean);

		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("barChart", barChart);
		modelAndView.addObject("paramBean", paramBean);

		return modelAndView;
	}

	@Override
	public DataModelAndView getChart0(Map<String, String> parameters) {
		SearchSearch paramBean = CommonHelper.convertMapToBean(parameters, SearchSearch.class);
		if (paramBean.getEmp_user_id() == null || paramBean.getEmp_user_id().equals("")) {
			paramBean.setEmp_user_id(paramBean.getEmp_user_id());
		}

		List<ExtrtCondbyInqDetailChart> chart = null;
		chart = extrtCondbyInqDao.getChart0(paramBean);

		DataModelAndView modelAndView = new DataModelAndView();

		ExtrtCondbyInqDetailChart charTmp = new ExtrtCondbyInqDetailChart();

		List<String> privacyName = new ArrayList<String>();
		List<String> chartInfo0 = new ArrayList<String>();

		for (int i = 0; i < chart.size(); ++i) {
			privacyName.add(chart.get(i).getCode_name());

			if (chart.get(i).getResult_type().equals("1"))
				charTmp.setnPrivacyCount1(chart.get(i).getCount());
			else if (chart.get(i).getResult_type().equals("2"))
				charTmp.setnPrivacyCount2(chart.get(i).getCount());
			else if (chart.get(i).getResult_type().equals("3"))
				charTmp.setnPrivacyCount3(chart.get(i).getCount());
			else if (chart.get(i).getResult_type().equals("4"))
				charTmp.setnPrivacyCount4(chart.get(i).getCount());
			else if (chart.get(i).getResult_type().equals("5"))
				charTmp.setnPrivacyCount5(chart.get(i).getCount());
			else if (chart.get(i).getResult_type().equals("6"))
				charTmp.setnPrivacyCount6(chart.get(i).getCount());
			else if (chart.get(i).getResult_type().equals("7"))
				charTmp.setnPrivacyCount7(chart.get(i).getCount());
			else if (chart.get(i).getResult_type().equals("8"))
				charTmp.setnPrivacyCount8(chart.get(i).getCount());
			else if (chart.get(i).getResult_type().equals("9"))
				charTmp.setnPrivacyCount9(chart.get(i).getCount());
			else if (chart.get(i).getResult_type().equals("10"))
				charTmp.setnPrivacyCount10(chart.get(i).getCount());

			chartInfo0.add(chart.get(i).getCode_name() + "," + chart.get(i).getResult_type());
		}

		charTmp.setPrivacyName(privacyName);
		charTmp.setChartInfo0(chartInfo0);

		modelAndView.addObject("chart", charTmp);
		modelAndView.addObject("paramBean", paramBean);

		return modelAndView;
	}

	@Override
	public DataModelAndView getChart1(Map<String, String> parameters) {
		SearchSearch paramBean = CommonHelper.convertMapToBean(parameters, SearchSearch.class);
		if (paramBean.getEmp_user_id() == null || paramBean.getEmp_user_id().equals("")) {
			paramBean.setEmp_user_id(paramBean.getEmp_user_id());
		}

		List<ExtrtCondbyInqDetailChart> chart = null;
		chart = extrtCondbyInqDao.getChart1(paramBean);

		DataModelAndView modelAndView = new DataModelAndView();

		ExtrtCondbyInqDetailChart charTmp = new ExtrtCondbyInqDetailChart();

		List<String> systemName = new ArrayList<String>();
		List<Integer> privacyCount = new ArrayList<Integer>();
		List<String> chartInfo1 = new ArrayList<String>();

		for (int i = 0; i < chart.size(); ++i) {
			systemName.add(chart.get(i).getSystem_name());
			privacyCount.add(chart.get(i).getCount());
			chartInfo1.add(chart.get(i).getSystem_name() + "," + chart.get(i).getCount());
		}

		charTmp.setSystemName(systemName);
		charTmp.setPrivacyCount(privacyCount);
		charTmp.setChartInfo1(chartInfo1);

		modelAndView.addObject("chart", charTmp);
		modelAndView.addObject("paramBean", paramBean);

		return modelAndView;
	}

	@Override
	public DataModelAndView getChart2(Map<String, String> parameters) {
		SearchSearch paramBean = CommonHelper.convertMapToBean(parameters, SearchSearch.class);
		if (paramBean.getEmp_user_id() == null || paramBean.getEmp_user_id().equals("")) {
			paramBean.setEmp_user_id(paramBean.getEmp_user_id());
		}

		List<ExtrtCondbyInqDetailChart> chart = null;
		chart = extrtCondbyInqDao.getChart2(paramBean);

		DataModelAndView modelAndView = new DataModelAndView();

		ExtrtCondbyInqDetailChart charTmp = new ExtrtCondbyInqDetailChart();

		List<String> systemName = new ArrayList<String>();
		List<Integer> privacyCount = new ArrayList<Integer>();
		List<String> chartInfo2 = new ArrayList<String>();

		for (int i = 0; i < chart.size(); ++i) {

			if (chartInfo2.size() != Integer.parseInt(chart.get(i).getProc_time())) {
				chartInfo2.add(Integer.toString(chartInfo2.size()) + ",0");
				--i;
			} else {
				chartInfo2.add(chart.get(i).getProc_time() + "," + chart.get(i).getCount());
			}

			if (i == chart.size() - 1 && !chart.get(i).getProc_time().equals("23")) {
				int nTmp = Integer.parseInt(chart.get(i).getProc_time());

				for (int j = nTmp + 1; j < 24; ++j) {
					chartInfo2.add(Integer.toString(j) + ",0");
				}
			}
		}

		charTmp.setSystemName(systemName);
		charTmp.setPrivacyCount(privacyCount);
		charTmp.setChartInfo2(chartInfo2);

		modelAndView.addObject("chart", charTmp);
		modelAndView.addObject("paramBean", paramBean);

		return modelAndView;
	}

	@Override
	public String getDeptName(SearchSearch search) {
		return extrtCondbyInqDao.getDeptName(search);
	}

	@Override
	public DataModelAndView findAbnormalList(SearchSearch search) {
		DataModelAndView modelAndView = new DataModelAndView();
		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/abnormalList.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		Dashboard dashboard = new Dashboard();
		dashboard.setAuth_idsList(search.getAuth_idsList());
		List<SystemMaster> systemList = commonDao.getSystemList(dashboard);
		modelAndView.addObject("systemList", systemList);
		
		Code code = new Code();
		//솔루션 최초 설치일 찾기
				code = new Code();
				code.setGroup_code_id("SOLUTION_MASTER");
				code.setCode_id("INSTALL_DATE");
				code.setUse_flag("Y");
				Code install_date = codeMngtDao.findCodeMngtOne(code);
		
		
		String searchType = search.getSearchType();
		
		// top panal
		List<EmpDetailInq> topPanal = extrtCondbyInqDao.scenarioList();
		List<ExtrtCondbyInq> dayCt = extrtCondbyInqDao.countByScenarioByday(search);
		String[] panalTypeArr = {"blue", "red", "purple", "yellow", "green"};
		for(int i=0; i<panalTypeArr.length; i++) {
			EmpDetailInq p = topPanal.get(i);
			p.setPanalType(panalTypeArr[i]);
			ExtrtCondbyInq d = dayCt.get(0);
			if(p.getScen_seq()==1000) {
				p.setDay_cnt(d.getScenario1());
			} else if(p.getScen_seq()==2000) {
				p.setDay_cnt(d.getScenario2());
			} else if(p.getScen_seq()==3000) {
				p.setDay_cnt(d.getScenario3());
			} else if(p.getScen_seq()==4000) {
				p.setDay_cnt(d.getScenario4());
			} else if(p.getScen_seq()==5000) {
				p.setDay_cnt(d.getScenario5());
			}
		}
		if(searchType.equals("today")) {
			List<ExtrtCondbyInq> monthCt = extrtCondbyInqDao.countByScenario(search);
			for(int i=0; i<topPanal.size(); i++) {
				EmpDetailInq p = topPanal.get(i);
				ExtrtCondbyInq d = monthCt.get(0);
				if(p.getScen_seq()==1000) {
					p.setMonth_cnt(d.getScenario1());
				} else if(p.getScen_seq()==2000) {
					p.setMonth_cnt(d.getScenario2());
				} else if(p.getScen_seq()==3000) {
					p.setMonth_cnt(d.getScenario3());
				} else if(p.getScen_seq()==4000) {
					p.setMonth_cnt(d.getScenario4());
				} else if(p.getScen_seq()==5000) {
					p.setMonth_cnt(d.getScenario5());
				}
			}
		}
		modelAndView.addObject("install_date", install_date);
		modelAndView.addObject("topPanal", topPanal);
		modelAndView.addObject("search", search);
		return modelAndView;
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalChart0(SearchSearch search) {
		return extrtCondbyInqDao.findAbnormalChart0(search);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalChart1(SearchSearch search) {
		return extrtCondbyInqDao.findAbnormalChart1(search);
	}

	@Override
	public DataModelAndView findExtrtCondbyInqDetailChart(SearchSearch search) {

		ExtrtCondbyInqDetailChart data1 = extrtCondbyInqDao.getExtrtCondbyInqDetail1(search);
		if(data1==null) {
			data1 = new ExtrtCondbyInqDetailChart();
			data1.setEmp_user_id(search.getEmp_user_id());
			data1.setDng_sum_val(0);
			data1.setDng_grade("정상");
		}
		ExtrtCondbyInqDetailChart userInfo = extrtCondbyInqDao.findEmpUserInfo(search);
		List<ExtrtCondbyInqDetailChart> data2 = extrtCondbyInqDao.getExtrtCondbyInqDetail2(search);
		List<SystemMaster> SystemMasterList = indvinfoTypeSetupDao.getSystemMasterListByAuth(search);

		if (SystemMasterList.size() > 0) {
			if (search.getSystem_seq() == null || search.getSystem_seq().length() == 0) {
				search.setSystem_seq(SystemMasterList.get(0).getSystem_seq());
			}
		}
		
		String checkEmpNameMasking = commonDao.checkEmpNameMasking();
		if(checkEmpNameMasking.equals("Y")) {
			String empUserName = data1.getEmp_user_name();
			StringBuilder builder = new StringBuilder(empUserName);
			builder.setCharAt(empUserName.length() - 2, '*');
			data1.setEmp_user_name(builder.toString());
		}
		
		DataModelAndView modelAndView = new DataModelAndView();

		modelAndView.addObject("userInfo", userInfo);
		modelAndView.addObject("data1", data1);
		modelAndView.addObject("data2", data2);
		List<ExtrtCondbyInqTimeline> rule_list = extrtCondbyInqDao.rule_list();
		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/detailChart.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("SystemMasterList", SystemMasterList);
		modelAndView.addObject("rule_list", rule_list);
		return modelAndView;
	}
	

	@Override
	public int findAbnormalDetailDngChart(ExtrtCondbyInq param) {
		return extrtCondbyInqDao.findAbnormalDetailDngChart(param);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart1(ExtrtCondbyInq param) {
		return extrtCondbyInqDao.findAbnormalDetailChart1(param);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart2(ExtrtCondbyInq param) {
		return extrtCondbyInqDao.findAbnormalDetailChart2(param);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart3(ExtrtCondbyInq param) {
		return extrtCondbyInqDao.findAbnormalDetailChart3(param);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart4(ExtrtCondbyInq param) {
		return extrtCondbyInqDao.findAbnormalDetailChart4(param);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart5(ExtrtCondbyInq param) {
		return extrtCondbyInqDao.findAbnormalDetailChart5(param);
	}
	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart20(ExtrtCondbyInq param) {
		return extrtCondbyInqDao.findAbnormalDetailChart20(param);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart1_1(String emp_user_name) {
		return extrtCondbyInqDao.findAbnormalDetailChart1_1(emp_user_name);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart2_1(String emp_user_name) {
		return extrtCondbyInqDao.findAbnormalDetailChart2_1(emp_user_name);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart3_1(String emp_user_name) {
		return extrtCondbyInqDao.findAbnormalDetailChart3_1(emp_user_name);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart4_1(String emp_user_name) {
		return extrtCondbyInqDao.findAbnormalDetailChart4_1(emp_user_name);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart5_1(String emp_user_name) {
		return extrtCondbyInqDao.findAbnormalDetailChart5_1(emp_user_name);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart1_2(ExtrtCondbyInq param) {
		return extrtCondbyInqDao.findAbnormalDetailChart1_2(param);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart2_2(ExtrtCondbyInq param) {
		return extrtCondbyInqDao.findAbnormalDetailChart2_2(param);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart3_2(ExtrtCondbyInq param) {
		return extrtCondbyInqDao.findAbnormalDetailChart3_2(param);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart4_2(ExtrtCondbyInq param) {
		return extrtCondbyInqDao.findAbnormalDetailChart4_2(param);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart5_2(ExtrtCondbyInq param) {
		return extrtCondbyInqDao.findAbnormalDetailChart5_2(param);
	}
	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart20_2(ExtrtCondbyInq param) {
		return extrtCondbyInqDao.findAbnormalDetailChart20_2(param);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart1_3(String dept_id) {
		return extrtCondbyInqDao.findAbnormalDetailChart1_3(dept_id);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart2_3(String dept_id) {
		return extrtCondbyInqDao.findAbnormalDetailChart2_3(dept_id);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart3_3(String dept_id) {
		return extrtCondbyInqDao.findAbnormalDetailChart3_3(dept_id);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart4_3(String dept_id) {
		return extrtCondbyInqDao.findAbnormalDetailChart4_3(dept_id);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart5_3(String dept_id) {
		return extrtCondbyInqDao.findAbnormalDetailChart5_3(dept_id);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart6(SearchSearch search) {
		return extrtCondbyInqDao.findAbnormalDetailChart6(search);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart7(SearchSearch search) {
		return extrtCondbyInqDao.findAbnormalDetailChart7(search);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart7_1(SearchSearch search) {
		return extrtCondbyInqDao.findAbnormalDetailChart7_1(search);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart8(SearchSearch search) {
		return extrtCondbyInqDao.findAbnormalDetailChart8(search);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart9(SearchSearch search) {
		return extrtCondbyInqDao.findAbnormalDetailChart9(search);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart9_1(SearchSearch search) {
		return extrtCondbyInqDao.findAbnormalDetailChart9_1(search);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart10(SearchSearch search) {
		return extrtCondbyInqDao.findAbnormalDetailChart10(search);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart10_1(SearchSearch search) {
		return extrtCondbyInqDao.findAbnormalDetailChart10_1(search);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart11(ExtrtCondbyInq eci) {
		return extrtCondbyInqDao.findAbnormalDetailChart11(eci);
	}

	@Override
	public List<Statistics> findAbnormalDetailChart12(ExtrtCondbyInq eci) {
		return extrtCondbyInqDao.findAbnormalDetailChart12(eci);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart12_1(ExtrtCondbyInq eci) {
		return extrtCondbyInqDao.findAbnormalDetailChart12_1(eci);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart13(ExtrtCondbyInq eci) {
		return extrtCondbyInqDao.findAbnormalDetailChart13(eci);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDetailChart14(ExtrtCondbyInq eci) {
		return extrtCondbyInqDao.findAbnormalDetailChart14(eci);
	}

	@Override
	public Object findAbnormalDetailChart6_TOT(SearchSearch search) {
		return extrtCondbyInqDao.findAbnormalDetailChart6_TOT(search);
	}

	@Override
	public Object findAbnormalDetailChart7_TOT(SearchSearch search) {
		return extrtCondbyInqDao.findAbnormalDetailChart7_TOT(search);
	}

	@Override
	public Object findAbnormalDetailChart8_TOT(SearchSearch search) {
		return extrtCondbyInqDao.findAbnormalDetailChart8_TOT(search);
	}

	@Override
	public Object findAbnormalDetailChart9_TOT(SearchSearch search) {
		return extrtCondbyInqDao.findAbnormalDetailChart9_TOT(search);
	}

	@Override
	public Object findAbnormalDetailChart10_TOT(SearchSearch search) {
		return extrtCondbyInqDao.findAbnormalDetailChart10_TOT(search);
	}

	@Override
	public List<ExtrtBaseSetup> getExtrtCondByAvgCnt(String emp_user_id) {
		return extrtCondbyInqDao.getExtrtCondByAvgCnt(emp_user_id);
	}

	/**
	 * 부서별 비정상위험분석 상세차트
	 */
	@Override
	public DataModelAndView findExtrtCondbyInqDeptDetailChart(SearchSearch search) {
		
		List<ExtrtCondbyInqDetailChart> data1 = extrtCondbyInqDao.getExtrtCondbyInqDeptDetail1(search);
		List<ExtrtCondbyInqDetailChart> data2 = extrtCondbyInqDao.getExtrtCondbyInqDeptDetail2(search);
		List<SystemMaster> SystemMasterList = indvinfoTypeSetupDao.getSystemMasterListByAuth(search);
		List<ExtrtCondbyInq> data3 = extrtCondbyInqDao.getUserByDeptId(search);
		/*List<ExtrtCondbyInq> data4 = extrtCondbyInqDao.getDeptLank(search);*/
		List<ExtrtCondbyInq> data5 = extrtCondbyInqDao.getDeptLank_new(search);

		if (SystemMasterList.size() > 0) {
			if (search.getSystem_seq() == null || search.getSystem_seq().length() == 0) {
				search.setSystem_seq(SystemMasterList.get(0).getSystem_seq());
			}
		}
		DataModelAndView modelAndView = new DataModelAndView();

		if (data1.size() > 0) {
			modelAndView.addObject("data1", data1.get(0));
			modelAndView.addObject("data2", data2);
			modelAndView.addObject("data3", data3);
			modelAndView.addObject("over", data3.size() - 5);
			/*modelAndView.addObject("data4", data4);*/
			modelAndView.addObject("data5", data5);
		}

		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/deptDetailChart.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("SystemMasterList", SystemMasterList);
		
		// 개인정보 취급량 주간 평균, 주말 평균
		
		
		return modelAndView;
	}

	/**
	 * 소명 요청 리스트
	 */
	@Override
	public DataModelAndView findSummonList(SearchSearch search) {

		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		// search.setTotal_count(extrtCondbyInqDao.findExtrtCondbyInqOne_count(search));
		search.setTotal_count(extrtCondbyInqDao.findSummon_count(search));
		List<ExtrtBaseSetup> ruleTblList = extrtCondbyInqDao.findRuleTblListByScenSeq(search);
		List<ExtrtBaseSetup> scenarioList = extrtBaseSetupDao.findScenarioList(null);

		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList_byAdmin(search);
		List<ExtrtCondbyInq> ExtrtCondbyInq = extrtCondbyInqDao.findSummonList(search);
		ExtrtCondbyInqList summonList = new ExtrtCondbyInqList(ExtrtCondbyInq);

		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/summonList.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);

		DataModelAndView modelAndView = new DataModelAndView();

		modelAndView.addObject("ruleTblList", ruleTblList);
		modelAndView.addObject("scenarioList", scenarioList);
		modelAndView.addObject("summonList", summonList);
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("systemMasterList", systemMasterList);

		return modelAndView;
		/*
		 * 
		 * int pageSize = Integer.valueOf(defaultPageSize); search.setSize(pageSize);
		 * search.setTotal_count(extrtCondbyInqDao.findSummon_count(search));
		 * List<ExtrtBaseSetup> scenarioList = extrtBaseSetupDao.findScenarioList(null);
		 * 
		 * List<ExtrtBaseSetup> ruleTblList = extrtCondbyInqDao.findRuleTblList();
		 * 
		 * List<ExtrtCondbyInq> ExtrtCondbyInq =
		 * extrtCondbyInqDao.findSummonList(search); ExtrtCondbyInqList summonList = new
		 * ExtrtCondbyInqList(ExtrtCondbyInq);
		 * 
		 * SimpleCode simpleCode = new SimpleCode(); String index =
		 * "/extrtCondbyInq/summonList.html"; simpleCode.setMenu_url(index); String
		 * index_id = commonDao.checkIndex(simpleCode);
		 * 
		 * DataModelAndView modelAndView = new DataModelAndView();
		 * 
		 * modelAndView.addObject("ruleTblList", ruleTblList);
		 * modelAndView.addObject("scenarioList", scenarioList);
		 * modelAndView.addObject("summonList", summonList);
		 * modelAndView.addObject("index_id", index_id);
		 * 
		 * return modelAndView;
		 */
	}

	/**
	 * 소명 관리 리스트
	 */
	@Override
	public DataModelAndView findSummonManageList(SearchSearch search) {
		DataModelAndView modelAndView = new DataModelAndView();
		try {
		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		search.setTotal_count(extrtCondbyInqDao.findSummonManage_count(search));

		List<ExtrtBaseSetup> ruleTblList = extrtCondbyInqDao.findRuleTblList();
		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList();

		List<ExtrtCondbyInq> ExtrtCondbyInq = extrtCondbyInqDao.findSummonManageList(search);
		ExtrtCondbyInqList summonList = new ExtrtCondbyInqList(ExtrtCondbyInq);
		
		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/summonManageList.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);

		

		modelAndView.addObject("ruleTblList", ruleTblList);
		modelAndView.addObject("systemMasterList", systemMasterList);
		modelAndView.addObject("summonList", summonList);
		modelAndView.addObject("index_id", index_id);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return modelAndView;
	}

	/**
	 * 소명 요청 상세
	 */
	@Override
	public DataModelAndView findSummonDetail(SearchSearch search) {
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		String ui_type = commonDao.getUiType();
		modelAndView.addObject("ui_type", ui_type);
		
		// 추출조건정보
		ExtrtCondbyInq extrtCondbyInq = extrtCondbyInqDao.extractDetailScenarioInfo(search);
		modelAndView.addObject("extrtCondbyInq", extrtCondbyInq);
		
		// 로그 정보
		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		search.setTotal_count(extrtCondbyInqDao.extrtCondbyInqDetailListCt(search));
		
		List<ExtrtCondbyInq> detailList = null;
		
		//상세시나리오 - 파일 다운로드인 경우 파일명 정보 조회
		if (extrtCondbyInq.getLog_delimiter().equals("DN")){
			detailList = extrtCondbyInqDao.extrtCondbyInqFileNameList(search);
			
			modelAndView.addObject("detailList", detailList);
		}
		else{
			detailList = extrtCondbyInqDao.extrtCondbyInqDetailList(search);
			for(ExtrtCondbyInq e : detailList) {
				String result_type = e.getResult_type();
				if(result_type != null) {
					String[] arrResult_type = result_type.split(",");
					Arrays.sort(arrResult_type);
					String res = "";
					for(int j=0;j<arrResult_type.length;j++) {
						if(j == 0)
								res += arrResult_type[j];
						else
								res += "," + arrResult_type[j];
					}
					e.setResult_type(res);
				}
				
				String result_content = e.getResult_content();
				if(result_content == null || result_content == "") continue;
				result_content = getResultContextByMaskingByPrivacyseq(result_content, result_type);
				e.setResult_content(result_content);
			}
			modelAndView.addObject("detailList", detailList);
		}
		
		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/summonDetail.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);

		String autoSummonValue = commonDao.checkValue();

		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("autoSummonValue", autoSummonValue);
		modelAndView.addObject("search", search);

		return modelAndView;
	}
	
	/**
	 * 소명 요청 상세 > 소명 삭제
	 * */
	@Override
	public DataModelAndView removeSummonOne(Map<String, String> parameters, HttpServletRequest request) {
		

		AdminUser paramBean = CommonHelper.convertMapToBean(parameters, AdminUser.class);
		HttpSession session = request.getSession();
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		DataModelAndView modelAndView = new DataModelAndView();
		try {
			
			extrtCondbyInqDao.removeSummonOne_emp_detail(paramBean);
			extrtCondbyInqDao.removeSummonOne_rule_biz(paramBean);
			
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			logger.error("[ExtrtCondbyInqSvcImpl.removeSummonOne] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS1005J");
		}

		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	/**
	 * 소명 관리 상세
	 */
	@Override
	public DataModelAndView findSummonManageDetail(SearchSearch search, HttpServletRequest request) {

		DataModelAndView modelAndView = new DataModelAndView();
		
		// 추출조건정보
		ExtrtCondbyInq extrtCondbyInq = extrtCondbyInqDao.extractDetailScenarioInfo(search);
		
		
		// 로그 정보
		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		search.setTotal_count(extrtCondbyInqDao.extrtCondbyInqDetailListCt(search));
		
		List<ExtrtCondbyInq> detailList = null;
		
		//상세시나리오 - 파일 다운로드인 경우 파일명 정보 조회
		if (extrtCondbyInq.getLog_delimiter().equals("DN")){
			detailList = extrtCondbyInqDao.extrtCondbyInqFileNameList(search);
			
			modelAndView.addObject("detailList", detailList);
		}
		else{
			detailList = extrtCondbyInqDao.extrtCondbyInqDetailList(search);
		for(ExtrtCondbyInq e : detailList) {
			String result_type = e.getResult_type();
			if(result_type != null) {
				String[] arrResult_type = result_type.split(",");
				Arrays.sort(arrResult_type);
				String res = "";
				for(int j=0;j<arrResult_type.length;j++) {
					if(j == 0)
							res += arrResult_type[j];
					else
							res += "," + arrResult_type[j];
				}
				e.setResult_type(res);
			}
			String result_content = e.getResult_content();
			if(result_content == null || result_content == "") continue;
			result_content = getResultContextByMaskingByPrivacyseq(result_content, result_type);
			e.setResult_content(result_content);
		}
		modelAndView.addObject("detailList", detailList);
		}
		//최종 권한 사용여부(DGB전용)
		search.setSummon_cfm_yn((String)request.getSession().getAttribute("summon_cfm_yn"));
		//소명권고이력
		List<ExtrtCondbyInq> summon_history = extrtCondbyInqDao.findSummonHistory(search);
		
		//소명응답 내용
		ExtrtCondbyInq summon_response = extrtCondbyInqDao.findSummon_response(search);
		
		//소명판정 내용
		ExtrtCondbyInq summon_decision = extrtCondbyInqDao.findSummon_decision(search);
		
		modelAndView.addObject("extrtCondbyInq", extrtCondbyInq);
		modelAndView.addObject("summon_history", summon_history);
		modelAndView.addObject("summon_response", summon_response);
		modelAndView.addObject("summon_decision", summon_decision);
		
		ExtrtCondbyInq raw = extrtCondbyInqDao.findSummonRaw(search.getDesc_seq());
		ExtrtCondbyInq result = extrtCondbyInqDao.findSummonResult(search.getDesc_seq());
		//ExtrtCondbyInq status = extrtCondbyInqDao.findSummonStatus(search.getDesc_seq());
		ExtrtCondbyInq result2 = extrtCondbyInqDao.findSummonResult2(search.getDesc_seq());
		List<ExtrtCondbyInq> abusePurpose = extrtCondbyInqDao.findCodeInfo("ABUSE_PURPOSE");
		List<ExtrtCondbyInq> abuseType = extrtCondbyInqDao.findCodeInfo("ABUSE_TYPE");
		List<ExtrtCondbyInq> faultDegree = extrtCondbyInqDao.findCodeInfo("FAULT_DEGREE");

		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/summonManageDetail.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		HttpSession session = request.getSession();
		
		// modelAndView.addObject("empUser", empUser);
		//modelAndView.addObject("summon", summon);
		
		
		modelAndView.addObject("raw", raw);
		modelAndView.addObject("result", result);
		modelAndView.addObject("result2", result2);
		//modelAndView.addObject("status", status);
		//modelAndView.addObject("biz_log", biz_log);
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("abusePurpose", abusePurpose);
		modelAndView.addObject("abuseType", abuseType);
		modelAndView.addObject("faultDegree", faultDegree);
		//modelAndView.addObject("logType", logType);
		modelAndView.addObject("useFullscan", (String) session.getAttribute("use_fullscan"));

		return modelAndView;
	}

	/**
	 * 
	 * 소명요청상세 -> 소명요청
	 */
	@SuppressWarnings("unused")
	@Override
	public DataModelAndView addSummonOne(Map<String, String> parameters, HttpServletRequest request) {
		
		AdminUser paramBean = CommonHelper.convertMapToBean(parameters, AdminUser.class);
		HttpSession session = request.getSession();
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		DataModelAndView modelAndView = new DataModelAndView();
		
		String ui_type = commonDao.getUiType();
		modelAndView.addObject("ui_type", ui_type);
		try {
			
			//1.대리인 사용유무 -> 2.소명응답자 설정 -> 3.소명이력 생성 -> 4.알람전송

			//메일관련 추출조건 정보
			AdminUser summonInfo = extrtCondbyInqDao.findSummonInfoOne(paramBean);
			
			//1.대리인 사용유무
			String self_summon_yn = extrtCondbyInqDao.checkSelfSummon(paramBean);

			//2.소명응답자 설정
			if("N".equals(self_summon_yn)){ //대리인 사용
				AdminUser summon_agent = new AdminUser();
				
				//옵션 세팅 테이블에서 summon_user[1]를 사용할지, summon_dept[3]를 사용할지 판별
				if( "1".equals((String) session.getAttribute("summon_agent_type"))) {	//summon_user 사용
					summon_agent = extrtCondbyInqDao.findSummon_user(paramBean);
				}else if("2".equals((String) session.getAttribute("summon_agent_type"))) {	//summon_dept 사용
					paramBean.setDept_id(summonInfo.getDept_id());
					summon_agent = extrtCondbyInqDao.findSummon_dept(paramBean);
				}
				
				if(summon_agent == null) {	//설정 가능한 소명위임자가 없습니다.
					throw new ESException("SYS1001J");
				}
				paramBean.setRes_user_id(summon_agent.getSummon_id());
				paramBean.setRes_system_seq(summon_agent.getSummon_system_seq());
				paramBean.setTarget_id(summon_agent.getSummon_id());
				summonInfo.setEmail_address(summon_agent.getEmail_address());
			}else {	//본인이 소명
				paramBean.setRes_user_id(summonInfo.getEmp_user_id());
				paramBean.setRes_system_seq(summonInfo.getSystem_seq());
				paramBean.setTarget_id(summonInfo.getEmp_user_id());
				//내부,외부 구분 and e-mail정보 추출
				EmpUser empInfo = extrtCondbyInqDao.findEmail_addr(paramBean);
				if (empInfo != null) {
					summonInfo.setEmail_address(empInfo.getEmail_address());
					summonInfo.setExternal_staff(empInfo.getExternal_staff());
				}else if(!"3".equals((String) session.getAttribute("summon_alarm_type"))) {
					throw new ESException("SYS1006J");	//일치하는 인사정보가 없습니다.
				}
				
			}
			
			//3.소명이력 생성
			paramBean.setStatus("10");
			extrtCondbyInqDao.addSummonInfo(paramBean);
			extrtCondbyInqDao.addSummonHistory(paramBean);
			
			String email_check_val = paramBean.getEmail_check_val();
			
			//4.알람전송
			//알람연동 [ 1 : 이메일연동, 2: SMS연동, 3 : 미사용 ]
			if( "1".equals((String) session.getAttribute("summon_alarm_type"))) { //[1]이메일연동
				//소명알림 수신여부(미수신 : 'Y')
				if(!email_check_val.equals("Y")){
					summonInfo.setSummon_reponse_type((String) session.getAttribute("summon_response_type"));
					if(summonInfo.getEmail_address() == null || summonInfo.getEmail_address().equals("")) {	//소명응답자 이메일정보 오류
						transactionManager.rollback(transactionStatus);
						throw new ESException("SYS1002J");
					}
					
					String result = alarmMngtSvc.sendSummonMail(summonInfo,request);
					
					if(result.equals("autherror")) {	//메일 계정 인증 실패
						transactionManager.rollback(transactionStatus);
						throw new ESException("SYS1003J");
					}else if(result.equals("error")) {	//메일 발송 실패
						transactionManager.rollback(transactionStatus);
						throw new ESException("SYS1004J");
					}
				}//END if email_check_val
				
			}else if( "2".equals((String) session.getAttribute("summon_alarm_type"))) { //[2]SMS연동
				
				//dgb 전용  sms발송여부
				if("on".equals(parameters.get("sms_check"))) {
					String emp_user_id = paramBean.getDetail_emp_user_id();
					String admin_user_id = paramBean.getAdmin_user_id();
					Map<String,String> sms_info = extrtCondbyInqDao.findSummonSmsInfo(emp_user_id);
					sms_info.put("admin_user_id",admin_user_id);
					extrtCondbyInqDao.saveSummonSms(sms_info);
				}
			}
			
			commonDao.updateEmpdetailCheckIs(paramBean.getDetailEmpDetailSeq());
			if("Shcd".equals(ui_type)) {
				shCardSummonFile(paramBean);
			}
			
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			logger.error("[ExtrtCondbyInqSvcImpl.addSummonOne] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS1005J");
		}

		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}

	/**
	 * 소명관리상세 -> 재소명요청
	 */
	@Override
	public DataModelAndView reSummonOne(Map<String, String> parameters, HttpServletRequest request) {
		AdminUser paramBean = CommonHelper.convertMapToBean(parameters, AdminUser.class);
		HttpSession session = request.getSession();

		// paramBean.setRule_cd(parameters.get("rule_cd_temp"));

		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			
			//1.대리인 사용유무 -> 2.소명응답자 설정 -> 3.소명이력 생성 -> 4.알람전송 
			
			//메일관련 추출조건 정보
			AdminUser summonInfo = extrtCondbyInqDao.findSummonInfoOne(paramBean);
			
			//1.대리인 사용유무
			String self_summon_yn = extrtCondbyInqDao.checkSelfSummon(paramBean);

			//2.소명응답자 설정
			if("N".equals(self_summon_yn)){ //대리인 사용
				AdminUser summon_agent = new AdminUser();
				//옵션 세팅 테이블에서 summon_user[1]를 사용할지, summon_dept[2]를 사용할지 판별
				if( "1".equals((String) session.getAttribute("summon_agent_type"))) {
					summon_agent = extrtCondbyInqDao.findSummon_user(paramBean);
				}else if("2".equals((String) session.getAttribute("summon_agent_type"))) {
					paramBean.setDept_id(summonInfo.getDept_id());
					summon_agent = extrtCondbyInqDao.findSummon_dept(paramBean);
				}
				
				if(summon_agent == null) {	//설정 가능한 소명위임자가 없습니다.
					throw new ESException("SYS1001J");
				}
				
				paramBean.setRes_user_id(summon_agent.getSummon_id());
				paramBean.setRes_system_seq(summon_agent.getSummon_system_seq());
				paramBean.setTarget_id(summon_agent.getSummon_id());
				summonInfo.setEmail_address(summon_agent.getEmail_address());
			}else {
				paramBean.setRes_user_id(summonInfo.getEmp_user_id());
				paramBean.setRes_system_seq(summonInfo.getSystem_seq());
				paramBean.setTarget_id(summonInfo.getEmp_user_id());
				//내부,외부 구분 and e-mail정보 추출
				EmpUser empInfo = extrtCondbyInqDao.findEmail_addr(paramBean);
				if (empInfo != null) {
					summonInfo.setEmail_address(empInfo.getEmail_address());
					summonInfo.setExternal_staff(empInfo.getExternal_staff());
				}else if(!"3".equals((String) session.getAttribute("summon_alarm_type"))){
					throw new ESException("SYS1006J");	//일치하는 인사정보가 없습니다.
				}
			}
			
			//3.소명이력 생성
			extrtCondbyInqDao.reSummonInfo(paramBean);
			extrtCondbyInqDao.addSummonHistory(paramBean);
			
			//4.알람전송
			//알람연동 [ 1 : 이메일연동, 2: SMS연동, 3 : 미사용 ]
			if( "1".equals((String) session.getAttribute("summon_alarm_type"))) { //[1]이메일연동
				summonInfo.setSummon_reponse_type((String) session.getAttribute("summon_response_type"));
				if(summonInfo.getEmail_address() == null || summonInfo.getEmail_address().equals("")) {	//소명응답자 이메일정보 오류.
					transactionManager.rollback(transactionStatus);
					throw new ESException("SYS1002J");
				}
				
				String result = alarmMngtSvc.sendSummonMail(summonInfo,request);
				
				if(result.equals("autherror")) {	//메일 계정 인증 실패
					transactionManager.rollback(transactionStatus);
					throw new ESException("SYS1003J");
				}else if(result.equals("error")) {	//메일 발송 실패
					transactionManager.rollback(transactionStatus);
					throw new ESException("SYS1004J");
				}
			}else if( "2".equals((String) session.getAttribute("summon_alarm_type"))) { //[2]SMS연동
				
			}
			

			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			logger.error("[ExtrtCondbyInqSvcImpl.addSummonOne] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}

		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}

	@Override
	public DataModelAndView addSummonResult(Map<String, String> parameters) {
		AdminUser paramBean = CommonHelper.convertMapToBean(parameters, AdminUser.class);

		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			//1.소명이력 생성
			extrtCondbyInqDao.decisionSummonInfo(paramBean);
			extrtCondbyInqDao.addSummonHistory(paramBean);

			// 1차, 2차판정 기능있을때
			/*
			if (paramBean.getCheckReResult() != null && paramBean.getCheckReResult().equals("Y")) {
				paramBean.setDesc_status("60");
			} else {
				if (paramBean.getDesc_result().equals("30")) {
					paramBean.setDesc_status("30");

					paramBean.setH_type("4");
					paramBean.setAdmin_user_id(paramBean.getResult_user_id());
					paramBean.setH_body("재소명요청 : " + paramBean.getResult_user_id() + "//" + paramBean.getDecision());
					paramBean.setResummon_yn("Y");

				} else {
					paramBean.setDesc_status("80");

					paramBean.setH_type("3");
					paramBean.setAdmin_user_id(paramBean.getResult_user_id());
					paramBean.setH_body("소명판정 : " + paramBean.getResult_user_id() + "//" + paramBean.getDecision());
					paramBean.setResummon_yn("N");
				}
				extrtCondbyInqDao.updateDescInfoForDecisionResult2Clear(paramBean);
			}
			 */

			// 알람
			/*
			 * if(paramBean.getCheckReResult() == null) { List<AdminUser> senderInfo =
			 * extrtCondbyInqDao.findAlarmSenderInfo(); String sender = null; String
			 * senderPhone = null;
			 * 
			 * for(AdminUser si : senderInfo) { switch (si.getParam_name()) { case
			 * "alarm.sender": sender = si.getParam_value(); break; case
			 * "alarm.sender.phone": senderPhone = si.getParam_value(); break; } }
			 * 
			 * List<AdminUser> alarmInfo = null; String alarmTitle = null; String alarmBody
			 * = null; String alarmUrl = null;
			 * 
			 * if(paramBean.getDesc_result().equals("30")) { List<AdminUser> summonManager =
			 * extrtCondbyInqDao.findSummonManagerList(paramBean);
			 * 
			 * alarmInfo = extrtCondbyInqDao.findAlarmRequestInfo(); for(AdminUser ai :
			 * alarmInfo) { switch (ai.getParam_name()) { case "alarm.request.title":
			 * alarmTitle = ai.getParam_value(); break; case "alarm.request.body": alarmBody
			 * = ai.getParam_value(); break; case "alarm.request.url": alarmUrl =
			 * ai.getParam_value(); break; } }
			 * 
			 * 
			 * String alarmTarget = paramBean.getRequest_target_user_id(); String
			 * alarmTargetPhone = null;
			 * 
			 * for(AdminUser a : summonManager) { alarmTarget = alarmTarget
			 * +","+a.getEmp_user_id(); alarmTargetPhone = alarmTargetPhone
			 * +","+a.getMobile_number(); }
			 * 
			 * AlarmManager.sendMessage("MESSENGER,SMS", sender, senderPhone, "Z2019043",
			 * "01022639658", alarmTitle, alarmBody, alarmUrl);
			 * //AlarmManager.sendMessage("MESSENGER", sender, senderPhone, alarmTarget,
			 * alarmTargetPhone, alarmTitle, alarmBody, alarmUrl); } else { List<AdminUser>
			 * summonManager = extrtCondbyInqDao.findSummonManagerList(paramBean);
			 * 
			 * alarmInfo = extrtCondbyInqDao.findAlarmJudgementInfo(); for(AdminUser ai :
			 * alarmInfo) { switch (ai.getParam_name()) { case "alarm.judgment.title":
			 * alarmTitle = ai.getParam_value(); break; case "alarm.judgment.body":
			 * alarmBody = ai.getParam_value(); break; case "alarm.judgment.url": alarmUrl =
			 * ai.getParam_value(); break; } }
			 * 
			 * String alarmTarget = paramBean.getRequest_target_user_id(); String
			 * alarmTargetPhone = null;
			 * 
			 * for(AdminUser a : summonManager) { alarmTarget = alarmTarget
			 * +","+a.getEmp_user_id(); alarmTargetPhone = alarmTargetPhone
			 * +","+a.getMobile_number(); }
			 * 
			 * AlarmManager.sendMessage("MESSENGER,SMS", sender, senderPhone, "Z2019043",
			 * "01022639658", alarmTitle, alarmBody, alarmUrl);
			 * //AlarmManager.sendMessage("MESSENGER", sender, senderPhone, alarmTarget,
			 * alarmTargetPhone, alarmTitle, alarmBody, alarmUrl); } }
			 */

			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			logger.error("[ExtrtCondbyInqSvcImpl.addSummonResult] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}

		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	//소명 최종판정
	@Override
	public DataModelAndView SummonResultConfirm(Map<String, String> parameters) {
		AdminUser paramBean = CommonHelper.convertMapToBean(parameters, AdminUser.class);

		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			//1.최종소명이력 생성
			
			  extrtCondbyInqDao.decision2SummonInfo(paramBean);
			  paramBean.setMsg("최종승인완료");
			  extrtCondbyInqDao.addSummon2History(paramBean);
			 
			

			// 1차, 2차판정 기능있을때
			/*
			if (paramBean.getCheckReResult() != null && paramBean.getCheckReResult().equals("Y")) {
				paramBean.setDesc_status("60");
			} else {
				if (paramBean.getDesc_result().equals("30")) {
					paramBean.setDesc_status("30");

					paramBean.setH_type("4");
					paramBean.setAdmin_user_id(paramBean.getResult_user_id());
					paramBean.setH_body("재소명요청 : " + paramBean.getResult_user_id() + "//" + paramBean.getDecision());
					paramBean.setResummon_yn("Y");

				} else {
					paramBean.setDesc_status("80");

					paramBean.setH_type("3");
					paramBean.setAdmin_user_id(paramBean.getResult_user_id());
					paramBean.setH_body("소명판정 : " + paramBean.getResult_user_id() + "//" + paramBean.getDecision());
					paramBean.setResummon_yn("N");
				}
				extrtCondbyInqDao.updateDescInfoForDecisionResult2Clear(paramBean);
			}
			 */

			// 알람
			/*
			 * if(paramBean.getCheckReResult() == null) { List<AdminUser> senderInfo =
			 * extrtCondbyInqDao.findAlarmSenderInfo(); String sender = null; String
			 * senderPhone = null;
			 * 
			 * for(AdminUser si : senderInfo) { switch (si.getParam_name()) { case
			 * "alarm.sender": sender = si.getParam_value(); break; case
			 * "alarm.sender.phone": senderPhone = si.getParam_value(); break; } }
			 * 
			 * List<AdminUser> alarmInfo = null; String alarmTitle = null; String alarmBody
			 * = null; String alarmUrl = null;
			 * 
			 * if(paramBean.getDesc_result().equals("30")) { List<AdminUser> summonManager =
			 * extrtCondbyInqDao.findSummonManagerList(paramBean);
			 * 
			 * alarmInfo = extrtCondbyInqDao.findAlarmRequestInfo(); for(AdminUser ai :
			 * alarmInfo) { switch (ai.getParam_name()) { case "alarm.request.title":
			 * alarmTitle = ai.getParam_value(); break; case "alarm.request.body": alarmBody
			 * = ai.getParam_value(); break; case "alarm.request.url": alarmUrl =
			 * ai.getParam_value(); break; } }
			 * 
			 * 
			 * String alarmTarget = paramBean.getRequest_target_user_id(); String
			 * alarmTargetPhone = null;
			 * 
			 * for(AdminUser a : summonManager) { alarmTarget = alarmTarget
			 * +","+a.getEmp_user_id(); alarmTargetPhone = alarmTargetPhone
			 * +","+a.getMobile_number(); }
			 * 
			 * AlarmManager.sendMessage("MESSENGER,SMS", sender, senderPhone, "Z2019043",
			 * "01022639658", alarmTitle, alarmBody, alarmUrl);
			 * //AlarmManager.sendMessage("MESSENGER", sender, senderPhone, alarmTarget,
			 * alarmTargetPhone, alarmTitle, alarmBody, alarmUrl); } else { List<AdminUser>
			 * summonManager = extrtCondbyInqDao.findSummonManagerList(paramBean);
			 * 
			 * alarmInfo = extrtCondbyInqDao.findAlarmJudgementInfo(); for(AdminUser ai :
			 * alarmInfo) { switch (ai.getParam_name()) { case "alarm.judgment.title":
			 * alarmTitle = ai.getParam_value(); break; case "alarm.judgment.body":
			 * alarmBody = ai.getParam_value(); break; case "alarm.judgment.url": alarmUrl =
			 * ai.getParam_value(); break; } }
			 * 
			 * String alarmTarget = paramBean.getRequest_target_user_id(); String
			 * alarmTargetPhone = null;
			 * 
			 * for(AdminUser a : summonManager) { alarmTarget = alarmTarget
			 * +","+a.getEmp_user_id(); alarmTargetPhone = alarmTargetPhone
			 * +","+a.getMobile_number(); }
			 * 
			 * AlarmManager.sendMessage("MESSENGER,SMS", sender, senderPhone, "Z2019043",
			 * "01022639658", alarmTitle, alarmBody, alarmUrl);
			 * //AlarmManager.sendMessage("MESSENGER", sender, senderPhone, alarmTarget,
			 * alarmTargetPhone, alarmTitle, alarmBody, alarmUrl); } }
			 */

			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			logger.error("[ExtrtCondbyInqSvcImpl.addSummonResult] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}

		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView addReSummonResult(Map<String, String> parameters) {
		AdminUser paramBean = CommonHelper.convertMapToBean(parameters, AdminUser.class);

		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			if (paramBean.getDesc_result2().equals("30")) {
				paramBean.setDesc_status("30");
				paramBean.setH_type("4");
				paramBean.setAdmin_user_id(paramBean.getResult2_user_id());
				paramBean.setH_body("재소명요청 : " + paramBean.getResult2_user_id() + "//" + paramBean.getResult2_body());
				paramBean.setResummon_yn("Y");
			} else {
				paramBean.setDesc_status("80");
				paramBean.setH_type("6");
				paramBean.setAdmin_user_id(paramBean.getResult2_user_id());
				paramBean.setH_body("소명판정 : " + paramBean.getResult2_user_id() + "//" + paramBean.getResult2_body());
				paramBean.setResummon_yn("N");
			}

			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String now = sdf.format(new Date());
			paramBean.setResult2_dt(now);
			extrtCondbyInqDao.updateDescInfoForReDecision(paramBean);

			extrtCondbyInqDao.addDescHistory(paramBean);

			/*
			 * List<AdminUser> senderInfo = extrtCondbyInqDao.findAlarmSenderInfo(); String
			 * sender = null; String senderPhone = null;
			 * 
			 * for(AdminUser si : senderInfo) { switch (si.getParam_name()) { case
			 * "alarm.sender": sender = si.getParam_value(); break; case
			 * "alarm.sender.phone": senderPhone = si.getParam_value(); break; } }
			 * 
			 * String alarmTitle = null; String alarmBody = null; String alarmUrl = null;
			 * 
			 * List<AdminUser> alarmInfo = extrtCondbyInqDao.findAlarmJudgementInfo();
			 * for(AdminUser ai : alarmInfo) { switch (ai.getParam_name()) { case
			 * "alarm.judgment.title": alarmTitle = ai.getParam_value(); break; case
			 * "alarm.judgment.body": alarmBody = ai.getParam_value(); break; case
			 * "alarm.judgment.url": alarmUrl = ai.getParam_value(); break; } }
			 * 
			 * List<AdminUser> summonManager =
			 * extrtCondbyInqDao.findSummonManagerList(paramBean);
			 * 
			 * String alarmTarget = paramBean.getRequest_target_user_id(); String
			 * alarmTargetPhone = null;
			 * 
			 * for(AdminUser a : summonManager) { alarmTarget = alarmTarget
			 * +","+a.getEmp_user_id(); alarmTargetPhone = alarmTargetPhone
			 * +","+a.getMobile_number(); } AlarmManager.sendMessage("MESSENGER,SMS",
			 * sender, senderPhone, "Z2019043", "01022639658", alarmTitle, alarmBody,
			 * alarmUrl); //AlarmManager.sendMessage("MESSENGER", sender, senderPhone,
			 * alarmTarget, alarmTargetPhone, alarmTitle, alarmBody, alarmUrl);
			 */
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			logger.error("[ExtrtCondbyInqSvcImpl.addSummonResult] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}

		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}

	@Override
	public DataModelAndView removeSummonResult(Map<String, String> parameters) {
		AdminUser paramBean = CommonHelper.convertMapToBean(parameters, AdminUser.class);

		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			extrtCondbyInqDao.removeSummonResult(paramBean);
			extrtCondbyInqDao.updateExtractResultSummonDelete(paramBean.getEmp_detail_seq());
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			logger.error("[ExtrtCondbyInqSvcImpl.removeSummonResult] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}

		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}

	@Override
	public DataModelAndView updateSummonResult(Map<String, String> parameters) {
		AdminUser paramBean = CommonHelper.convertMapToBean(parameters, AdminUser.class);

		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		String expect_dt = paramBean.getExpect_dt();
		paramBean.setExpect_dt(expect_dt.substring(0, 4) + expect_dt.substring(5, 7) + expect_dt.substring(8));
		try {
			extrtCondbyInqDao.updateSummonResult(paramBean);
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			logger.error("[ExtrtCondbyInqSvcImpl.updateSummonResult] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}

		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart1(ExtrtCondbyInq param) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart1(param);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart2(ExtrtCondbyInq param) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart2(param);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart3(ExtrtCondbyInq param) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart3(param);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart4(ExtrtCondbyInq param) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart4(param);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart5(ExtrtCondbyInq param) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart5(param);
	}
	@Override
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart20(ExtrtCondbyInq param) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart20(param);
	}
	
	@Override
	public int findAbnormalDeptDetailDngChart(ExtrtCondbyInq param) {
		return extrtCondbyInqDao.findAbnormalDeptDetailDngChart(param);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart1_1(ExtrtCondbyInq param) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart1_1(param);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart2_1(ExtrtCondbyInq param) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart2_1(param);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart3_1(ExtrtCondbyInq param) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart3_1(param);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart4_1(ExtrtCondbyInq param) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart4_1(param);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart5_1(ExtrtCondbyInq param) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart5_1(param);
	}
	@Override
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart20_1(ExtrtCondbyInq param) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart20_1(param);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart6(SearchSearch search) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart6(search);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart7(SearchSearch search) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart7(search);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart7_1(SearchSearch search) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart7_1(search);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart8(SearchSearch search) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart8(search);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart9(SearchSearch search) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart9(search);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart9_1(SearchSearch search) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart9_1(search);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart10(SearchSearch search) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart10(search);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart10_1(SearchSearch search) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart10_1(search);
	}

	@Override
	public Statistics findAbnormalDeptDetailChart11(ExtrtCondbyInq eci) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart11(eci);
	}

	@Override
	public List<Statistics> findAbnormalDeptDetailChart12(ExtrtCondbyInq eci) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart12(eci);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart12_1(ExtrtCondbyInq eci) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart12_1(eci);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart13(ExtrtCondbyInq eci) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart13(eci);
	}

	@Override
	public List<ExtrtCondbyInq> findAbnormalDeptDetailChart14(ExtrtCondbyInq eci) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart14(eci);
	}

	@Override
	public Object findAbnormalDeptDetailChart6_TOT(SearchSearch search) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart6_TOT(search);
	}

	@Override
	public Object findAbnormalDeptDetailChart7_TOT(SearchSearch search) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart7_TOT(search);
	}

	@Override
	public Object findAbnormalDeptDetailChart8_TOT(SearchSearch search) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart8_TOT(search);
	}

	@Override
	public Object findAbnormalDeptDetailChart9_TOT(SearchSearch search) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart9_TOT(search);
	}

	@Override
	public Object findAbnormalDeptDetailChart10_TOT(SearchSearch search) {
		return extrtCondbyInqDao.findAbnormalDeptDetailChart10_TOT(search);
	}

	@Override
	public DataModelAndView getResultType(SearchSearch search) {
		DataModelAndView modelAndView = new DataModelAndView();
		// List<AllLogInq> list = allLogInqDao.findAllLogInqDetailResult(search);
		// modelAndView.addObject("list", list);
		List<AllLogInq> list = new ArrayList<>();
		List<String> privacyTypeList = extrtCondbyInqDao.getPrivacyTypes();
		//search.setResult_type(privacyType);
		List<AllLogInq> result = null;
		if ("BA".equals(search.getLog_delimiter())) {
			result = extrtCondbyInqDao.getResultTypeByOne(search);
		} else if ("DN".equals(search.getLog_delimiter())){
			result = extrtCondbyInqDao.getResultTypeByOne_DownloadLog(search);
		}
		for (AllLogInq v : result) {

			String pResult = v.getResult_content();
			/*
			 * if( v.getResult_type() != null && "주민등록번호".equals(v.getResult_type()) ) {
			 * pResult = pResult.substring(0,7)+"*******"; }
			 */

			if (v.getResult_type() != null && v.getPrivacy_masking() != null
					&& v.getPrivacy_masking().equals("Y")) {
				pResult = getResultContextByMasking(pResult, v.getResult_type());
			}

			v.setResult_content(pResult);
		}
		list.addAll(result);

		modelAndView.addObject("list", list);
		return modelAndView;
	}

	@Override
	public DataModelAndView setCheckExtrtDetail(Map<String, String> parameters) {
		SearchSearch paramBean = CommonHelper.convertMapToBean(parameters, SearchSearch.class);

		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			extrtCondbyInqDao.setCheckExtrtCondbyInqDetail(paramBean);
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			logger.error("[ExtrtCondbyInqSvcImpl.addSummonOne] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}

		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}

	public String getResultContextByMasking(String result, String type) {
		int len = result.length();
		switch (type) {
		case "주민등록번호":
			result = result.substring(0, 6) + "-*******";
			break;
		case "운전면허번호":
			result = result.substring(0, 6);
			for (int i = 0; i < len - 6; i++)
				result += "*";
			break;
		case "여권번호":
			result = result.substring(0, 5);
			for (int i = 0; i < len - 5; i++)
				result += "*";
			break;
		case "신용카드번호":
			result = result.substring(0, 10) + "****-****";
			break;
		case "건강보험번호":
			result = result.substring(0, 6);
			for (int i = 0; i < len - 6; i++)
				result += "*";
			break;
		case "전화번호":
		case "휴대폰번호":
			String[] arr = result.split("-");
			if (arr.length <= 1) {
				result = result.substring(0, len - 4) + "****";
			} else {
				result = arr[0] + "-" + arr[1] + "-****";
			}
			break;
		case "이메일":
			result = result.replaceAll("(?<=.{3}).(?=.*@)", "*");
			break;
		case "계좌번호":
			arr = result.split("-");
			if (arr.length <= 1) {
				result = result.substring(0, len - 6) + "******";
			} else {
				result = arr[0] + "-" + arr[1] + "-";
				if (arr[2] != null) {
					int l = arr[2].length();
					for (int i = 0; i < l; i++)
						result += "*";
				}
			}
			break;
		case "외국인등록번호":
			result = result.substring(0, 5);
			for (int i = 0; i < len - 5; i++)
				result += "*";
			break;
		}

		return result;
	}
	
	public String getResultContextByMaskingByPrivacyseq(String result, String type) {
		int len = result.length();
		switch (type) {
		case "1":
			result = result.substring(0, 6) + "-*******";
			break;
		case "2":
			result = result.substring(0, 6);
			for (int i = 0; i < len - 6; i++)
				result += "*";
			break;
		case "3":
			result = result.substring(0, 5);
			for (int i = 0; i < len - 5; i++)
				result += "*";
			break;
		case "4":
			result = result.substring(0, 10) + "****-****";
			break;
		case "5":
			result = result.substring(0, 6);
			for (int i = 0; i < len - 6; i++)
				result += "*";
			break;
		case "6":
		case "8":
			String[] arr = result.split("-");
			if (arr.length <= 1) {
				result = result.substring(0, len - 4) + "****";
			} else {
				result = arr[0] + "-" + arr[1] + "-****";
			}
			break;
		case "7":
			result = result.replaceAll("(?<=.{3}).(?=.*@)", "*");
			break;
		case "9":
			arr = result.split("-");
			if (arr.length <= 1) {
				result = result.substring(0, len - 6) + "******";
			} else {
				result = arr[0] + "-" + arr[1] + "-";
				if (arr[2] != null) {
					int l = arr[2].length();
					for (int i = 0; i < l; i++)
						result += "*";
				}
			}
			break;
		case "10":
			result = result.substring(0, 5);
			for (int i = 0; i < len - 5; i++)
				result += "*";
			break;
		}

		return result;
	}

	/** 180314 sysong 소명판정 판정자 위임 **/
	@Override
	public DataModelAndView findSummonDelegation(SearchSearch search) {

		DataModelAndView modelAndView = new DataModelAndView();

		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/summmonDelegation.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);

		List<ExtrtCondbyInq> ExtrtCondbyInq = extrtCondbyInqDao.findSummonDelegation(search);
		ExtrtCondbyInqList extrtCondbyInqList = new ExtrtCondbyInqList(ExtrtCondbyInq);

		List<Delegation> delegation = extrtCondbyInqDao.findDelegation(search);
		DelegationList delegationList = new DelegationList(delegation);

		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("extrtCondbyInqList", extrtCondbyInqList);
		modelAndView.addObject("delegationList", delegationList);

		return modelAndView;
	}

	/** 180314 sysong 소명판정 판정자 위임 판정자 삭제 **/
	@Override
	public DataModelAndView summonUserRemove(Map<String, String> parameters) {
		Delegation paramBean = CommonHelper.convertMapToBean(parameters, Delegation.class);

		try {
			extrtCondbyInqDao.summonUserRemove(paramBean);
			extrtCondbyInqDao.summonAdminUserRemove(paramBean);
		} catch (DataAccessException e) {
			throw new ESException("SYS030J");
		}

		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);

		return modelAndView;
	}

	/** 180314 sysong 월별 소명 현황 **/
	@Override
	public DataModelAndView findsummonPerby(SearchSearch search) {

		DataModelAndView modelAndView = new DataModelAndView();

		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
		String date = dayTime.format(new Date(time));

		String year = date.substring(0, 4);
		String month = date.substring(6, 7);

		String yearMonthL = year + "0" + month;
		String yearMonth = "";

		if (search.getYear() == null && search.getMonth() == null) {
			search.setYearMonth(yearMonthL+"01");
		} else {
			if (search.getMonth().length() == 1) {
				search.setMonth("0" + search.getMonth());
			}
			yearMonth = search.getYear() + search.getMonth();
			search.setYearMonth(yearMonth+"01");
		}
		
		//솔루션 최초 설치일 찾기
		Code code = new Code();
		code.setGroup_code_id("SOLUTION_MASTER");
		code.setCode_id("INSTALL_DATE");
		code.setUse_flag("Y");
		Code install_date = codeMngtDao.findCodeMngtOne(code);
		
		List<ExtrtBaseSetup> ruleTblList = extrtCondbyInqDao.findRuleTblList();
		List<ExtrtBaseSetup> scenarioList = extrtBaseSetupDao.findScenarioList(null);
		List<ExtrtBaseSetup> systemList = extrtCondbyInqDao.findSystemList();

		List<ExtrtCondbyInq> ExtrtCondbyInq = extrtCondbyInqDao.findsummonPerby(search);
		ExtrtCondbyInqList extrtCondbyInqList = new ExtrtCondbyInqList(ExtrtCondbyInq);

		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/summonPerby.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);

		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("install_date", install_date);
		modelAndView.addObject("ruleTblList", ruleTblList);
		modelAndView.addObject("scenarioList", scenarioList);
		modelAndView.addObject("systemList", systemList);
		modelAndView.addObject("extrtCondbyInqList", extrtCondbyInqList);

		return modelAndView;
	}

	/** 20180314 sysong 부서별 소명 현황 **/
	@Override
	public DataModelAndView findsummonDeptby(SearchSearch search) {

		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
		String date = dayTime.format(new Date(time));

		String year = date.substring(0, 4);
		String month = date.substring(6, 7);

		String yearMonthL = year + "0" + month;
		String yearMonth = "";

		if (search.getYear() == null && search.getMonth() == null) {
			search.setYearMonth(yearMonthL+"01");
		} else {
			if (search.getMonth().length() == 1) {
				search.setMonth("0" + search.getMonth());
			}
			yearMonth = search.getYear() + search.getMonth();
			search.setYearMonth(yearMonth+"01");
		}

		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		search.setTotal_count(extrtCondbyInqDao.findsummonDeptby_Count(search));

		String search_from = search.getYearMonth();

		SimpleDateFormat transDate = new SimpleDateFormat("yyyyMMdd");
		Date tdate = null;
		try {
			tdate = transDate.parse(search_from);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(tdate);
		int endDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		String endofMonth = String.valueOf(endDay);

		String search_to = yearMonth+endofMonth;
		
		//솔루션 최초 설치일 찾기
		Code code = new Code();
		code.setGroup_code_id("SOLUTION_MASTER");
		code.setCode_id("INSTALL_DATE");
		code.setUse_flag("Y");
		Code install_date = codeMngtDao.findCodeMngtOne(code);
		
		List<ExtrtBaseSetup> ruleTblList = extrtCondbyInqDao.findRuleTblList();
		List<ExtrtBaseSetup> scenarioList = extrtBaseSetupDao.findScenarioList(null);
		List<ExtrtBaseSetup> systemList = extrtCondbyInqDao.findSystemList();

		List<ExtrtCondbyInq> ExtrtCondbyInq = extrtCondbyInqDao.findsummonDeptby(search);
		ExtrtCondbyInqList extrtCondbyInqList = new ExtrtCondbyInqList(ExtrtCondbyInq);

		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/summonDeptby.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);

		DataModelAndView modelAndView = new DataModelAndView();

		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("install_date", install_date);
		modelAndView.addObject("search_from", search_from);
		modelAndView.addObject("search_to", search_to);
		modelAndView.addObject("ruleTblList", ruleTblList);
		modelAndView.addObject("scenarioList", scenarioList);
		modelAndView.addObject("systemList", systemList);
		modelAndView.addObject("extrtCondbyInqList", extrtCondbyInqList);

		return modelAndView;
	}

	/** 180314 sysong 개인별 소속 현황 **/
	@Override
	public DataModelAndView findsummonIndvby(SearchSearch search) {

		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
		String date = dayTime.format(new Date(time));

		String year = date.substring(0, 4);
		String month = date.substring(6, 7);

		String yearMonthL = year + "0" + month;
		String yearMonth = "";

		if (search.getYear() == null && search.getMonth() == null) {
			search.setYearMonth(yearMonthL+"01");
			yearMonth = yearMonthL;
		} else {
			if (search.getMonth().length() == 1) {
				search.setMonth("0" + search.getMonth());
			}
			yearMonth = search.getYear() + search.getMonth();
			search.setYearMonth(yearMonth+"01");
		}

		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		search.setTotal_count(extrtCondbyInqDao.findsummonIndvby_Count(search));

		String search_from = search.getYearMonth();

		SimpleDateFormat transDate = new SimpleDateFormat("yyyyMMdd");
		Date tdate = null;
		try {
			tdate = transDate.parse(search_from);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(tdate);
		int endDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		String endofMonth = String.valueOf(endDay);

		String search_to = yearMonth+endofMonth;

		List<ExtrtBaseSetup> ruleTblList = extrtCondbyInqDao.findRuleTblList();
		List<ExtrtBaseSetup> scenarioList = extrtBaseSetupDao.findScenarioList(null);
		List<ExtrtBaseSetup> systemList = extrtCondbyInqDao.findSystemList();

		List<ExtrtCondbyInq> ExtrtCondbyInq = extrtCondbyInqDao.findsummonIndvby(search);
		ExtrtCondbyInqList extrtCondbyInqList = new ExtrtCondbyInqList(ExtrtCondbyInq);

		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/summonIndvby.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		//솔루션 최초 설치일 찾기
		Code code = new Code();
		code.setGroup_code_id("SOLUTION_MASTER");
		code.setCode_id("INSTALL_DATE");
		code.setUse_flag("Y");
		Code install_date = codeMngtDao.findCodeMngtOne(code);
		
		DataModelAndView modelAndView = new DataModelAndView();

		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("install_date", install_date);
		modelAndView.addObject("search_from", search_from);
		modelAndView.addObject("search_to", search_to);
		modelAndView.addObject("ruleTblList", ruleTblList);
		modelAndView.addObject("scenarioList", scenarioList);
		modelAndView.addObject("systemList", systemList);
		modelAndView.addObject("extrtCondbyInqList", extrtCondbyInqList);

		return modelAndView;
	}

	/** 180314 sysong 소명요청 및 판정 현황 **/
	@Override
	public DataModelAndView findsummonProcessby(SearchSearch search) {

		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
		String date = dayTime.format(new Date(time));

		String year = date.substring(0, 4);
		String month = date.substring(6, 7);

		String yearMonthL = year + "0" + month;
		String yearMonth = "";

		if (search.getYear() == null && search.getMonth() == null) {
			search.setYearMonth(yearMonthL+"01");
		} else {
			if (search.getMonth().length() == 1) {
				search.setMonth("0" + search.getMonth());
			}
			yearMonth = search.getYear() + search.getMonth();
			search.setYearMonth(yearMonth+"01");
		}
		
		//솔루션 최초 설치일 찾기
		Code code = new Code();
		code.setGroup_code_id("SOLUTION_MASTER");
		code.setCode_id("INSTALL_DATE");
		code.setUse_flag("Y");
		Code install_date = codeMngtDao.findCodeMngtOne(code);
		
		DataModelAndView modelAndView = null;
		try {		
		List<ExtrtBaseSetup> ruleTblList = extrtCondbyInqDao.findRuleTblList();
		List<ExtrtBaseSetup> scenarioList = extrtBaseSetupDao.findScenarioList(null);
		List<ExtrtBaseSetup> systemList = extrtCondbyInqDao.findSystemList();

		List<ExtrtCondbyInq> ExtrtCondbyInq = extrtCondbyInqDao.findsummonProcessby(search);
		ExtrtCondbyInqList extrtCondbyInqList = new ExtrtCondbyInqList(ExtrtCondbyInq);

		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/summonProcessby.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);

		 modelAndView = new DataModelAndView();

		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("install_date", install_date);
		modelAndView.addObject("ruleTblList", ruleTblList);
		modelAndView.addObject("scenarioList", scenarioList);
		modelAndView.addObject("systemList", systemList);
		modelAndView.addObject("extrtCondbyInqList", extrtCondbyInqList);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return modelAndView;
	}

	@Override
	public List<ExtrtCondbyInq> findScenarioByScenSeq(SearchSearch search) {

		List<ExtrtCondbyInq> scenario = extrtCondbyInqDao.findScenarioByScenSeq(search);

		return scenario;
	}

	@Override
	public DataModelAndView saveFollowUp(Map<String, String> parameters) {

		ExtrtCondbyInq paramBean = CommonHelper.convertMapToBean(parameters, ExtrtCondbyInq.class);
		String emp_datail_seq = parameters.get("detailEmpDetailSeq");
		if (emp_datail_seq != null) {
			paramBean.setEmp_detail_seq(Integer.parseInt(parameters.get("detailEmpDetailSeq")));
		}

		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);

		try {
			extrtCondbyInqDao.saveFollowUp(paramBean);
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			logger.error("[ExtrtCondbyInqSvcImpl.saveFollowUp] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}

		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);

		return modelAndView;
	}

	@Override
	public DataModelAndView findSummonUpdate(SearchSearch search) {
		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/findSummonUpdate.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);

		DataModelAndView modelAndView = new DataModelAndView();

		modelAndView.addObject("index_id", index_id);

		return modelAndView;
	}

	/**
	 * 추출조건별조회 리스트
	 */
	@Override
	public DataModelAndView findAbuseInfoList(SearchSearch search) {

		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		search.setTotal_count(extrtCondbyInqDao.findAbuseInfoListCount(search));
		List<ExtrtBaseSetup> ruleTblList = extrtCondbyInqDao.findRuleTblListByScenSeq(search);
		List<ExtrtBaseSetup> scenarioList = extrtBaseSetupDao.findScenarioList(null);

		List<ExtrtCondbyInq> abuseList = extrtCondbyInqDao.findAbuseInfoList(search);
		ExtrtCondbyInqList extrtCondbyInqList = new ExtrtCondbyInqList(abuseList);
		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList();

		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/abuseInfoList.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);

		DataModelAndView modelAndView = new DataModelAndView();

		modelAndView.addObject("ruleTblList", ruleTblList);
		modelAndView.addObject("scenarioList", scenarioList);
		modelAndView.addObject("abuseList", extrtCondbyInqList);
		modelAndView.addObject("systemMasterList", systemMasterList);
		modelAndView.addObject("index_id", index_id);

		return modelAndView;
	}

	@Override
	public DataModelAndView findAbuseInfoDetail(SearchSearch search) {
		ExtrtCondbyInq abuseInfo = extrtCondbyInqDao.findAbuseInfoDetail(search);

		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/abuseInfoDetail.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);

		DataModelAndView modelAndView = new DataModelAndView();

		List<ExtrtCondbyInq> administrativeDispoType = extrtCondbyInqDao.findCodeInfo("ADMINISTRATIVE_DISPO_TYPE");
		List<ExtrtCondbyInq> criminalDispoType = extrtCondbyInqDao.findCodeInfo("CRIMINAL_DISPO_TYPE");

		modelAndView.addObject("administrativeDispoType", administrativeDispoType);
		modelAndView.addObject("criminalDispoType", criminalDispoType);
		modelAndView.addObject("abuseInfo", abuseInfo);
		modelAndView.addObject("abuseInfoCount", extrtCondbyInqDao.findAbuseInfoDetailCount(search));
		modelAndView.addObject("index_id", index_id);

		return modelAndView;
	}

	@Override
	public DataModelAndView saveAbuseInfo(Map<String, String> parameters) {
		AdminUser paramBean = CommonHelper.convertMapToBean(parameters, AdminUser.class);

		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);

		try {
			extrtCondbyInqDao.saveAbuseInfo(paramBean);
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			logger.error("[ExtrtCondbyInqSvcImpl.saveAbuseInfo] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}

		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);

		return modelAndView;
	}

	@Override
	public DataModelAndView findRuleList(SearchSearch search) {
		int pageSize = Integer.valueOf(10);
		search.setSize(pageSize);
		int count = extrtCondbyInqDao.findRuleListCount(search);
		search.setTotal_count(count);
		List<ExtrtBaseSetup> ruleList = extrtCondbyInqDao.findRuleList(search);

		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/ruleSimulate.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);

		DataModelAndView modelAndView = new DataModelAndView();

		modelAndView.addObject("ruleList", ruleList);
		modelAndView.addObject("index_id", index_id);

		return modelAndView;
	}

	@Override
	public DataModelAndView findRuleScript(int rule_seq) {
		ExtrtCondbyInq script = extrtCondbyInqDao.findRuleScript(rule_seq);
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("object", script);
		return modelAndView;
	}

	@Override
	public void truncateExtractTemp() {
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);

		try {
			extrtCondbyInqDao.truncateSimulateEmpDetail();
			extrtCondbyInqDao.truncateRuleExtractTemp();
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			logger.error("[ExtrtCondbyInqSvcImpl.truncateExtractTemp] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS601J");
		}

	}

	@Override
	public void executeSql(ExtrtCondbyInq extrtCondbyInq) {
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			extrtCondbyInqDao.executeSql(extrtCondbyInq);
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			logger.error("[ExtrtCondbyInqSvcImpl.executeSql] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS601J");
		}

	}

	@Override
	public void executeSqlSecond(String sql) {
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			List<String> occrDtList = extrtCondbyInqDao.findRuleExtractTempDate();
			for (String occrDt : occrDtList) {
				sql = sql.replaceAll("EXTRACT_DAY", occrDt);

				ExtrtCondbyInq extrtCondbyInq = new ExtrtCondbyInq();
				extrtCondbyInq.setSql(sql);
				extrtCondbyInqDao.executeSql(extrtCondbyInq);
			}

			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			logger.error("[ExtrtCondbyInqSvcImpl.executeSql] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS601J");
		}

	}

	@Override
	public DataModelAndView findBizLogExtractTemp() {
		/*
		 * int pageSize = Integer.valueOf(10); search.setSize(pageSize); int count =
		 * extrtCondbyInqDao.findRuleListCount(search); search.setTotal_count(count);
		 */
		List<ExtrtCondbyInq> logList = extrtCondbyInqDao.findBizLogExtractTemp();
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("logList", logList);

		return modelAndView;
	}

	/**
	 * 소명 리스트
	 */
	@Override
	public DataModelAndView findCenterSummonList(SearchSearch search) {

		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		search.setTotal_count(extrtCondbyInqDao.findCenterSummonListCount(search));

		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList();

		List<ExtrtCondbyInq> ExtrtCondbyInq = extrtCondbyInqDao.findCenterSummonList(search);

		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/centerSummonList.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);

		DataModelAndView modelAndView = new DataModelAndView();

		modelAndView.addObject("systemMasterList", systemMasterList);
		modelAndView.addObject("centerSummonList", ExtrtCondbyInq);
		modelAndView.addObject("index_id", index_id);

		return modelAndView;
	}

	@SuppressWarnings("unused")
	@Override
	public String findCenterApprReturnUrl(long desc_seq, String admin_user_id) {
		String returnUrl = null;
		String apprSeqStr = null;
		String apprEmpStr = null;
		String apprTestId = "R2018104";
		if (desc_seq != 0) {
			long apprSeq = extrtCondbyInqDao.findApprSeq(desc_seq);

			apprSeqStr = "" + apprSeq;
			if (apprTestId == null) {
				apprEmpStr = admin_user_id;
			} else {
				apprEmpStr = apprTestId;
			}

			String apprMode = extrtCondbyInqDao.findDescApprMode();

			if (apprMode != null && apprMode.equals("Y")) {
				returnUrl = "http://gw.redcross.or.kr/app/prototype.nsf/securityprototype?openform?&type=req_info&empno="
						+ apprEmpStr + "&sno=" + apprSeqStr + "&skey=" + apprSeqStr;
			}
		}
		return returnUrl;
	}

	@Override
	public DataModelAndView findTimeLineChart(SearchSearch search) {
		DataModelAndView modelAndView = new DataModelAndView();

		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/findTimeLineChart.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		Date today = new Date();
		search.initDatesIfEmpty(today, today);
		
		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList_byAdmin(search);
		modelAndView.addObject("systemMasterList", systemMasterList);
		List<ExtrtBaseSetup> scenarioList = extrtBaseSetupDao.findScenarioList(null);
		modelAndView.addObject("scenarioList", scenarioList);
		
		if(search.getScen_seq()!=null && search.getScen_seq()!="") {
			List<ExtrtCondbyInq> rule_list = extrtCondbyInqDao.findScenarioByScenSeq(search);
			modelAndView.addObject("rule_list", rule_list);
		}
		
		// 데이터 가져오기
		String data1 = search.getData1();
		/*
		 * if(data1 == null) { search.setData1(""); String emp_user_name =
		 * extrtCondbyInqDao.empUserIDExample(); search.setEmp_user_name(emp_user_name);
		 * }
		 */
		
		if(data1 != null) {
			search.setSize(Integer.parseInt(defaultPageSize));
			Integer total_count = extrtCondbyInqDao.timeLineListCt(search);
			search.setTotal_count(total_count);
			List<AllLogInq> allLogInq = extrtCondbyInqDao.timeLineList(search);
			for(int i=0; i<allLogInq.size(); i++) {
				AllLogInq ali = allLogInq.get(i);
				String result_type = ali.getResult_type();
				if(result_type != null) {
					String[] arrResult_type = result_type.split(",");
					Arrays.sort(arrResult_type);
					String res = "";
					for(int j=0; j<arrResult_type.length; j++) {
						if(j ==0)
							res += arrResult_type[j];
						else
							res += "," + arrResult_type[j];
					}
					ali.setResult_type(res);
				}
			}
			modelAndView.addObject("allLogInq", allLogInq);
			modelAndView.addObject("timeLineLength", allLogInq.size()*210);
			modelAndView.addObject("search", search);
		}
		return modelAndView;
	}

	@Override
	public DataModelAndView timeLineChartAppend(SearchSearch search) {
		DataModelAndView modelAndView = new DataModelAndView();
		List<AllLogInq> allLogInq = extrtCondbyInqDao.timeLineList(search);
		for(int i=0; i<allLogInq.size(); i++) {
			AllLogInq ali = allLogInq.get(i);
			String result_type = ali.getResult_type();
			if(result_type != null) {
				String[] arrResult_type = result_type.split(",");
				Arrays.sort(arrResult_type);
				String res = "";
				for(int j=0; j<arrResult_type.length; j++) {
					if(j ==0)
						res += arrResult_type[j];
					else
						res += "," + arrResult_type[j];
				}
				ali.setResult_type(res);
			}
		}
		modelAndView.addObject("allLogInq", allLogInq);
		return modelAndView;
	}


	@Override
	public DataModelAndView timelineAppendByUser(SearchSearch search) {
		DataModelAndView modelAndView = new DataModelAndView();
		List<AllLogInq> allLogInq = extrtCondbyInqDao.timeLineListByUser(search);
		modelAndView.addObject("timeLineLength", allLogInq.size()*250);
		modelAndView.addObject("allLogInq", allLogInq);
		modelAndView.addObject("search", search);
		return modelAndView;
	}

	@Override
	public Statistics findAbnormalDetailChart11_new(ExtrtCondbyInq eci) {
		return extrtCondbyInqDao.findAbnormalDetailChart11_new(eci);
	}



	@Override
	public Map<String, String> summonListAdd(Map<String, String> parameters, HttpServletRequest request) {
		int successCount = 0;
		int failCount = 0;
		HashMap<String,String> resultMap = new HashMap<String, String>();
		String[] empDetailList = parameters.get("emp_detail_seq").split(",");
		
		for (String empDetailSeq : empDetailList) {
			if(summonAdd(empDetailSeq, parameters, request))successCount++;
			else failCount++;
		}
		resultMap.put("success", String.valueOf(successCount));
		resultMap.put("fail", String.valueOf(failCount));
		
		return resultMap;
	}
	
	public boolean summonAdd(String empDetailSeq, Map<String, String> parameters, HttpServletRequest request) {
		boolean result = true;
		try {
		SearchSearch search = new SearchSearch();
		search.setDetailEmpDetailSeq(empDetailSeq);
		ExtrtCondbyInq summon = extrtCondbyInqDao.findExtrtCondbyInqListByEmpCd(search);
		
		AdminUser paramBean = convertAdminUser(summon,parameters,request);
		
		extrtCondbyInqDao.addDescInfo(paramBean);
		
		List<AllLogInq> summonLogList = null;
		if ("BA".equals(summon.getLog_delimiter())) {
			search.getExtrtCondbyInqDetail().setTotal_count(extrtCondbyInqDao.findEmpUserCondCount(search));
			summonLogList = extrtCondbyInqDao.findExtrtCondbyInqListByLogList_summon(search);
		} else if ("DN".equals(summon.getLog_delimiter())) {
			search.getExtrtCondbyInqDetail().setTotal_count(extrtCondbyInqDao.findEmpUserCondCount_downloadLog(search));
			summonLogList = extrtCondbyInqDao.findExtrtCondbyInqDownloadLogListByLogList_summon(search);
		} else if ("DB".equals(summon.getLog_delimiter())) {
			search.getExtrtCondbyInqDetail().setTotal_count(extrtCondbyInqDao.findEmpUserCondCount_dbacLog(search));
			summonLogList = extrtCondbyInqDao.findExtrtCondbyInqDbacLogListByLogList_summon(search);
		}
		
		long desc_seq = extrtCondbyInqDao.getDescSeq();
		paramBean.setDesc_seq(desc_seq);		
		for (AllLogInq alllog : summonLogList) {
			paramBean.setLog_seq(alllog.getLog_seq());
			extrtCondbyInqDao.addDescLog(paramBean);
		}
		
		extrtCondbyInqDao.updateExtractResultSummon(Long.parseLong(paramBean.getDetailEmpDetailSeq()));
		paramBean.setH_type("1");
		paramBean.setH_body("소명요청 : " + paramBean.getInsert_user_id() + "//" + paramBean.getDescription2());
		extrtCondbyInqDao.addDescHistory(paramBean);
		commonDao.updateEmpdetailCheckIs(paramBean.getDetailEmpDetailSeq());
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}
		return result;
	}
	public String upLoadSystemMngtOne(Map<String, String> parameters, HttpServletRequest request, HttpServletResponse response)throws Exception {
		String result="false";
		 
		SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		//파일 업로드 기능  		
		MultipartHttpServletRequest mpr = (MultipartHttpServletRequest) request;  
		
		MultipartFile dir = mpr.getFile("file");
			
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		//엑셀 Row 카운트는 1000으로 지정한다		
		int rowCount=5000;
		
		File file = new File("/root/excelUpload/" + dir.getOriginalFilename());
		
		if (!file.exists()) { //폴더 없으면 폴더 생성
			file.mkdirs();
		}
		
		dir.transferTo(file);
		
		//업로드된 파일의 정보를 읽어들여 DB에 추가하는 기능 
		if (dir.getSize() > 0 ) {
				
			List<String[]> readRows = new ArrayList<>();
			
			String ext = file.getName().substring(file.getName().lastIndexOf(".")+1);
			ext = ext.toLowerCase();
			if( "xlsx".equals(ext) ){	
				readRows = new XExcelFileReader(file.getAbsolutePath()).readRows(rowCount);
				
			}else if( "xls".equals(ext) ){	
				readRows = new ExcelFileReader(file.getAbsolutePath()).readRows(rowCount);
			}
		
			for(int i=0; i<3; i++){	
				readRows.remove(0);
			}
			
			boolean isCheck = true;
			
			if( readRows.size() != 0 ){
				
				//int columnCount = readRows.get(0).length;
				//각 셀 내용을 DB 컬럼과 Mapping
				try {
					for(String[] readRowArr : readRows){
						AdminUser paramBean = new AdminUser();
						if(readRowArr[0] != null && readRowArr[0].length() > 0){
							//소명코드
							if(readRowArr[0] != null) {
								paramBean.setEmp_detail_seq(Long.parseLong(readRowArr[0]));
								if(extrtCondbyInqDao.findSummonStatus(readRowArr[0]) == null) {
									result = "파일명 : "+dir.getOriginalFilename()+ ",소명코드 : " + readRowArr[0] +" ,소명코드를 찾을 수 없습니다.";
									isCheck = false;
									break;
								}else if(!("10".equals(extrtCondbyInqDao.findSummonStatus(readRowArr[0])) || "20".equals(extrtCondbyInqDao.findSummonStatus(readRowArr[0])))) {
									result = "파일명 : "+dir.getOriginalFilename()+ ",소명코드 : " + readRowArr[0] +" ,응답이 완료 된 소명 건 입니다.";
									isCheck = false;
									break;
								}
							}
							else {
								result = "파일명 : "+dir.getOriginalFilename()+ ",소명코드 : " + readRowArr[0] +" ,소명코드는 필수입력 사항입니다." + readRowArr[0];
								//null이거나, 소명요청, 소명재요청 상태가 아니면 
								isCheck = false;
								break;
							}
							//응답자ID
							if (readRowArr[1] != null && readRowArr[1].length() > 0) {
								paramBean.setRes_user_id(readRowArr[1]);
							} else {
								result = "파일명 : "+dir.getOriginalFilename()+ ",소명코드 : " + readRowArr[0] +" ,응답자ID는 필수입력 사항입니다." + readRowArr[1];
								isCheck = false;
								break;
							}
							//응답자 시스템명
							if(readRowArr[2] != null && readRowArr[2].length() > 0){
								com.easycerti.eframe.psm.system_management.vo.System system = systemMngtDao.getSystemSeqBySystemName(readRowArr[2]);
								if(system != null)
									paramBean.setRes_system_seq(system.getSystem_seq()); 
								else {
									result = "시스템명은 필수입력 사항입니다." + readRowArr[2];
									isCheck = false;
									break;
								}
							} else {
								result = "파일명 : "+dir.getOriginalFilename()+ ",소명코드 : " + readRowArr[0] +" ,시스템명은 필수입력 사항입니다." + readRowArr[2];
								isCheck = false;
								break;
							}
							//판정자
							if(readRowArr[3] != null && readRowArr[3].length() > 0){
								paramBean.setTarget_id(readRowArr[3]);
							}
							//사유
							if (readRowArr[4] != null && readRowArr[4].length() > 0) {
								paramBean.setMsg(readRowArr[4]);
							} else {
								result = "파일명 : "+dir.getOriginalFilename()+ ",소명코드 : " + readRowArr[0] +" ,사유는 필수입력 사항입니다." + readRowArr[4];
								isCheck = false;
								break;
							}
							//응답 시간
							if (readRowArr[5] != null && readRowArr[5].length() > 0) {
								paramBean.setUpdate_datetime(fm.parse(readRowArr[5]));
							} else {
								result = "파일명 : "+dir.getOriginalFilename()+ ",소명코드 : " + readRowArr[0] +" ,응답시간은 필수입력 사항입니다." + readRowArr[5];
								isCheck = false;
								break;
							}
							extrtCondbyInqDao.updateSummonResponse(paramBean);
						}
					}
					if (isCheck) {
						transactionManager.commit(transactionStatus);
					} else {
						transactionManager.rollback(transactionStatus);
					}
					readRows.clear();
				} catch(DataAccessException e) {
					transactionManager.rollback(transactionStatus);
					e.printStackTrace();
					readRows.clear();
					return result;
				}
			}
			if(isCheck)
				result="true";
		}
		return result;	
	}
	
	
	/* 다중파일 업로드 진행중
	public String upLoadSystemMngtOne(Map<String, String> parameters, HttpServletRequest request, HttpServletResponse response)throws Exception {
		String result="false";
		 
		SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		//파일 업로드 기능  		
		MultipartHttpServletRequest mpr = (MultipartHttpServletRequest) request;  
		
		List<MultipartFile> fileList = mpr.getFiles("file");
		for(MultipartFile dir : fileList) {
			
			TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
			//엑셀 Row 카운트는 1000으로 지정한다		
			int rowCount=5000;
			
			File file = new File("/root/excelUpload/" + dir.getOriginalFilename());
			
			if (!file.exists()) { //폴더 없으면 폴더 생성
				file.mkdirs();
			}
			
			dir.transferTo(file);
			
			//업로드된 파일의 정보를 읽어들여 DB에 추가하는 기능 
			if (dir.getSize() > 0 ) { 
					
				List<String[]> readRows = new ArrayList<>();
				
				String ext = file.getName().substring(file.getName().lastIndexOf(".")+1);
				ext = ext.toLowerCase();
				if( "xlsx".equals(ext) ){	
					readRows = new XExcelFileReader(file.getAbsolutePath()).readRows(rowCount);
					
				}else if( "xls".equals(ext) ){	
					readRows = new ExcelFileReader(file.getAbsolutePath()).readRows(rowCount);
				}
			
				for(int i=0; i<3; i++){	
					readRows.remove(0);
				}
				
				boolean isCheck = true;
				
				if( readRows.size() != 0 ){
					
					//int columnCount = readRows.get(0).length;
					//각 셀 내용을 DB 컬럼과 Mapping
					try {
						for(String[] readRowArr : readRows){
							AdminUser paramBean = new AdminUser();
							if(readRowArr[0] != null && readRowArr[0].length() > 0){
								//소명코드
								if(readRowArr[0] != null)
									paramBean.setEmp_detail_seq(Long.parseLong(readRowArr[0])); 
								else {
									result = "소명코드는 필수입력 사항입니다." + readRowArr[0];
									isCheck = false;
									break;
								}
								//응답자ID
								if (readRowArr[1] != null && readRowArr[1].length() > 0) {
									paramBean.setRes_user_id(readRowArr[1]);
								} else {
									result = "응답자ID는 필수입력 사항입니다." + readRowArr[1];
									isCheck = false;
									break;
								}
								//응답자 시스템명
								if(readRowArr[2] != null && readRowArr[2].length() > 0){
									com.easycerti.eframe.psm.system_management.vo.System system = systemMngtDao.getSystemSeqBySystemName(readRowArr[2]);
									if(system != null)
										paramBean.setRes_system_seq(system.getSystem_seq()); 
									else {
										result = "시스템명은 필수입력 사항입니다." + readRowArr[2];
										isCheck = false;
										break;
									}
								} else {
									result = "파일명 : "+dir.getOriginalFilename()+ ",소명코드 : " + readRowArr[0] +" ,시스템명은 필수입력 사항입니다." + readRowArr[2];
									isCheck = false;
									break;
								}
								//판정자
								if(readRowArr[3] != null && readRowArr[3].length() > 0){
									paramBean.setTarget_id(readRowArr[3]);
								}
								//사유
								if (readRowArr[4] != null && readRowArr[4].length() > 0) {
									paramBean.setMsg(readRowArr[4]);
								} else {
									result = "사유는 필수입력 사항입니다." + readRowArr[4];
									isCheck = false;
									break;
								}
								//응답 시간
								if (readRowArr[5] != null && readRowArr[5].length() > 0) {
									paramBean.setUpdate_datetime(fm.parse(readRowArr[5]));
								} else {
									result = "응답시간은 필수입력 사항입니다." + readRowArr[5];
									isCheck = false;
									break;
								}
								extrtCondbyInqDao.updateSummonResponse(paramBean);
							}
						}
						if (isCheck) {
							transactionManager.commit(transactionStatus);
						} else {
							transactionManager.rollback(transactionStatus);
						}
						readRows.clear();
					} catch(DataAccessException e) {
						transactionManager.rollback(transactionStatus);
						e.printStackTrace();
						readRows.clear();
						return result;
					}
				}
				if(isCheck)
					result="true";
			}
		}
		return result;	
	}
	*/
	
	@SuppressWarnings("unused")
	@Override
	public DataModelAndView addResponseSummonOne(Map<String, String> parameters, HttpServletRequest request) {
		
		AdminUser paramBean = CommonHelper.convertMapToBean(parameters, AdminUser.class);
		HttpSession session = request.getSession();
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		DataModelAndView modelAndView = new DataModelAndView();
		try {
			
			//1.소명답변 
			extrtCondbyInqDao.updateSummonResponse(paramBean);
			
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			logger.error("[ExtrtCondbyInqSvcImpl.addResponseSummonOne] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS1005J");
		}

		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	public AdminUser convertAdminUser(ExtrtCondbyInq summon, Map<String, String> parameters, HttpServletRequest request) {
		AdminUser paramBean = new AdminUser();
		paramBean.setRule_cd(Integer.parseInt(summon.getRule_cd()));
		paramBean.setLog_type(summon.getLog_delimiter());
		paramBean.setDescription2(parameters.get("description"));
		paramBean.setAdmin_user_id(SystemHelper.findSessionUser(request).getAdmin_user_id());
		paramBean.setInsert_user_id(summon.getEmp_user_id());
		paramBean.setDetailEmpDetailSeq(String.valueOf(summon.getEmp_detail_seq()));
		paramBean.setSystem_seq(summon.getSystem_seq());
		paramBean.setProc_date(summon.getOccr_dt());
		paramBean.setOccr_dt(summon.getOccr_dt());
		paramBean.setDesc_status(parameters.get("desc_status"));
		
		return paramBean;
	}



	public DataModelAndView summonDelegatePersonal(Map<String, String> parameters) {
		DataModelAndView modelAndView = new DataModelAndView();
		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/summonDelegatePersonal.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		if(parameters.get("delegation_from") == null || parameters.get("delegation_from").equals("")) {
			Map<String, String> map = defaultDate(7);
			parameters.put("delegation_from",map.get("before"));
			parameters.put("delegation_to",map.get("after"));
		}
		
		if(parameters.get("delegate_search_fr") == null || parameters.get("delegate_search_fr").equals("")) {
			Map<String, String> map = defaultDate(7);
			parameters.put("delegate_search_fr",map.get("before"));
			parameters.put("delegate_search_to",map.get("after"));
		}
		
		if(parameters.get("delegate_hist_search_fr") == null || parameters.get("delegate_hist_search_fr").equals("")) {
			Map<String, String> map = defaultDate(7);
			parameters.put("delegate_hist_search_fr",map.get("before"));
			parameters.put("delegate_hist_search_to",map.get("after"));
		}
		
		try {
			List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList_byAdmin(new SearchSearch());
			List<ExtrtCondbyInq> delegateList = extrtCondbyInqDao.findDelegateList(parameters);
			List<ExtrtCondbyInq> delegateHistList = extrtCondbyInqDao.findDelegateHistList(parameters);
			modelAndView.addObject("systemMasterList", systemMasterList);
			modelAndView.addObject("delegateList", delegateList);
			modelAndView.addObject("delegateHistList", delegateHistList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		modelAndView.addObject("parameters",parameters);
		
		return modelAndView;
	}

	@Override
	public List findEmpUserList(Map<String, String> parameters) {
		EmpUserSearch empUser = new EmpUserSearch();
		empUser.setSystem_seq_1(parameters.get("system_seq"));
		empUser.setUseExcel("true");
		return empUserMngtDao.findEmpUserMngtList(empUser);
	}



	@Override
	public String summonDelegatePersonalAdd(Map<String, String> parameters, HttpServletRequest request) {
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		String result = "success";
		try {
			ExtrtCondbyInq search = CommonHelper.convertMapToBean(parameters, ExtrtCondbyInq.class);
			if(!nullCheck(search))return "null";	// 입력값이 비어서 왔을경우 예외처리
			
			int count = extrtCondbyInqDao.addSummonDelegatePersonal(search);
			if(count<1)return "conflict";	// 이미 위임 날짜가 겹치는 소명 위임이 있음
			
			parameters.put("summon_seq", search.getSummon_seq());
			ExtrtCondbyInq summonUser = extrtCondbyInqDao.findDelegateOne(parameters);
			summonUser.setDelegation_act("grant");
			extrtCondbyInqDao.addSummonDelegateHist(summonUser);
			transactionManager.commit(transactionStatus);
		} catch (Exception e) {
			transactionManager.rollback(transactionStatus);
			e.printStackTrace();
			result = "fail";
			throw new ESException("SYS033J");
		}
		
		return result;
	}

	@Override
	public String summonDelegatePersonalDelete(Map<String, String> parameters, HttpServletRequest request) {
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		String result = "success";
		try {
			ExtrtCondbyInq summonUser = extrtCondbyInqDao.findDelegateOne(parameters);
			summonUser.setDelegation_act("invoke");
			extrtCondbyInqDao.deleteSummonDelegatePersonal(summonUser);
			extrtCondbyInqDao.addSummonDelegateHist(summonUser);
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			e.printStackTrace();
			result = "fail";
			throw new ESException("SYS033J");
		}
		
		return result;
	}
	
	public Map<String,String> defaultDate(int beforeDate){
		Map<String,String> resultMap = new HashMap<String, String>();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -beforeDate);
		
		resultMap.put("before",sdf.format(cal.getTime()));
		resultMap.put("after",sdf.format(new Date()));
		
		return resultMap;
	}
	
	public boolean nullCheck(ExtrtCondbyInq user) {
		int count = 0;
		boolean result = true;
		if(!user.getEmp_user_id().equals("nodata"))count++;
		if(!user.getEmp_system_seq().equals("nodata"))count++;
		if(!user.getSummon_id().equals("nodata"))count++;
		if(!user.getSummon_system_seq().equals("nodata"))count++;
		if(!user.getDelegation_from().equals("nodata"))count++;
		if(!user.getDelegation_to().equals("nodata"))count++;
		if(count<6)result = false;
		return result;
	}
	
	public boolean nullCheck2(ExtrtCondbyInq user) {
		int count = 0;
		boolean result = true;
		if(!user.getDept_id().equals("nodata"))count++;
		if(!user.getSummon_id().equals("nodata"))count++;
		if(!user.getSummon_system_seq().equals("nodata"))count++;
		if(!user.getDelegation_from().equals("nodata"))count++;
		if(!user.getDelegation_to().equals("nodata"))count++;
		if(count<5)result = false;
		return result;
	}
	
	public boolean nullCheck3(ExtrtCondbyInq user) {
		int count = 0;
		boolean result = true;
		if(!user.getApproval_id().equals("nodata"))count++;
		if(!user.getSummon_id().equals("nodata"))count++;
		if(!user.getSummon_system_seq().equals("nodata"))count++;
		if(!user.getDelegation_from().equals("nodata"))count++;
		if(!user.getDelegation_to().equals("nodata"))count++;
		if(count<5)result = false;
		return result;
	}
	
	public boolean nullCheck4(ExtrtCondbyInq user) {
		int count = 0;
		boolean result = true;
		if(!user.getApproval_id().equals("nodata"))count++;
		if(!user.getDept_id().equals("nodata"))count++;
		if(!user.getDelegation_from().equals("nodata"))count++;
		if(!user.getDelegation_to().equals("nodata"))count++;
		if(count<4)result = false;
		return result;
	}



	@Override
	public DataModelAndView summonDelegateDept(Map<String, String> parameters) {
		DataModelAndView modelAndView = new DataModelAndView();
		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/summonDelegateDept.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		if(parameters.get("delegation_from") == null || parameters.get("delegation_from").equals("")) {
			Map<String, String> map = defaultDate(7);
			parameters.put("delegation_from",map.get("before"));
			parameters.put("delegation_to",map.get("after"));
		}
		
		if(parameters.get("delegate_search_fr") == null || parameters.get("delegate_search_fr").equals("")) {
			Map<String, String> map = defaultDate(7);
			parameters.put("delegate_search_fr",map.get("before"));
			parameters.put("delegate_search_to",map.get("after"));
		}
		
		if(parameters.get("delegate_hist_search_fr") == null || parameters.get("delegate_hist_search_fr").equals("")) {
			Map<String, String> map = defaultDate(7);
			parameters.put("delegate_hist_search_fr",map.get("before"));
			parameters.put("delegate_hist_search_to",map.get("after"));
		}
		
		try {
			List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList_byAdmin(new SearchSearch());
			List<Department> departmentList = deptMngtDao.findDepartmentMngtList(new Department());
			List<ExtrtCondbyInq> delegateList = extrtCondbyInqDao.findDelegateList_dept(parameters);
			List<ExtrtCondbyInq> delegateHistList = extrtCondbyInqDao.findDelegateHistList_dept(parameters);
			modelAndView.addObject("systemMasterList", systemMasterList);
			modelAndView.addObject("departmentList", departmentList);
			modelAndView.addObject("delegateList", delegateList);
			modelAndView.addObject("delegateHistList", delegateHistList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		modelAndView.addObject("parameters",parameters);
		
		return modelAndView;
	}



	@Override
	public String summonDelegateDeptAdd(Map<String, String> parameters, HttpServletRequest request) {
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		String result = "success";
		try {
			ExtrtCondbyInq search = CommonHelper.convertMapToBean(parameters, ExtrtCondbyInq.class);
			if(!nullCheck2(search))return "null";	// 입력값이 비어서 왔을경우 예외처리
			
			int count = extrtCondbyInqDao.addSummonDelegateDept(search);
			if(count<1)return "conflict";	// 이미 위임 날짜가 겹치는 소명 위임이 있음
			
			parameters.put("summon_seq", search.getSummon_seq());
			ExtrtCondbyInq summonUser = extrtCondbyInqDao.findDelegateOne_dept(parameters);
			summonUser.setDelegation_act("grant");
			extrtCondbyInqDao.addSummonDelegateHist_dept(summonUser);
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			e.printStackTrace();
			result = "fail";
			throw new ESException("SYS033J");
		}
		
		return result;
	}
	
	@Override
	public String summonDelegateDeptDelete(Map<String, String> parameters, HttpServletRequest request) {
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		String result = "success";
		try {
			ExtrtCondbyInq summonUser = extrtCondbyInqDao.findDelegateOne_dept(parameters);
			summonUser.setDelegation_act("invoke");
			extrtCondbyInqDao.deleteSummonDelegateDept(summonUser);
			extrtCondbyInqDao.addSummonDelegateHist_dept(summonUser);
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			e.printStackTrace();
			result = "fail";
			throw new ESException("SYS033J");
		}
		
		return result;
	}

	@Override
	public DataModelAndView approvalDelegatePersonal(Map<String, String> parameters) {
		DataModelAndView modelAndView = new DataModelAndView();
		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/approvalDelegatePersonal.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		if(parameters.get("delegation_from") == null || parameters.get("delegation_from").equals("")) {
			Map<String, String> map = defaultDate(7);
			parameters.put("delegation_from",map.get("before"));
			parameters.put("delegation_to",map.get("after"));
		}
		
		if(parameters.get("delegate_search_fr") == null || parameters.get("delegate_search_fr").equals("")) {
			Map<String, String> map = defaultDate(7);
			parameters.put("delegate_search_fr",map.get("before"));
			parameters.put("delegate_search_to",map.get("after"));
		}
		
		if(parameters.get("delegate_hist_search_fr") == null || parameters.get("delegate_hist_search_fr").equals("")) {
			Map<String, String> map = defaultDate(7);
			parameters.put("delegate_hist_search_fr",map.get("before"));
			parameters.put("delegate_hist_search_to",map.get("after"));
		}
		
		try {
			AdminUserSearch adminUser = new AdminUserSearch();
			adminUser.setAuth_id("AUTH00004");
			adminUser.setOption("AUTH00004");
			adminUser.setUseExcel("true");
			List<AdminUser> adminUserList = adminUserMngtDao.findAdminUserMngtList(adminUser );
			
			List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList_byAdmin(new SearchSearch());
			List<ExtrtCondbyInq> delegateList = extrtCondbyInqDao.findAppDelegateList(parameters);
			List<ExtrtCondbyInq> delegateHistList = extrtCondbyInqDao.findAppDelegateHistList(parameters);
			modelAndView.addObject("adminUserList", adminUserList);
			modelAndView.addObject("systemMasterList", systemMasterList);
			modelAndView.addObject("delegateList", delegateList);
			modelAndView.addObject("delegateHistList", delegateHistList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		modelAndView.addObject("parameters",parameters);
		
		return modelAndView;
	}



	@Override
	public String approvalDelegatePersonalAdd(Map<String, String> parameters, HttpServletRequest request) {
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		String result = "success";
		try {
			ExtrtCondbyInq search = CommonHelper.convertMapToBean(parameters, ExtrtCondbyInq.class);
			if(!nullCheck3(search))return "null";	// 입력값이 비어서 왔을경우 예외처리
			
			int count = extrtCondbyInqDao.addApprovalDelegatePersonal(search);
			if(count<1)return "conflict";	// 이미 위임 날짜가 겹치는 소명 위임이 있음
			
			parameters.put("summon_seq", search.getSummon_seq());
			ExtrtCondbyInq summonUser = extrtCondbyInqDao.findAppDelegateOne(parameters);
			summonUser.setDelegation_act("grant");
			extrtCondbyInqDao.addApprovalDelegateHist(summonUser);
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			e.printStackTrace();
			result = "fail";
			throw new ESException("SYS033J");
		}
		
		return result;
	}



	@Override
	public String approvalDelegatePersonalDelete(Map<String, String> parameters, HttpServletRequest request) {
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		String result = "success";
		try {
			ExtrtCondbyInq summonUser = extrtCondbyInqDao.findAppDelegateOne(parameters);
			summonUser.setDelegation_act("invoke");
			extrtCondbyInqDao.deleteApprovalDelegatePersonal(summonUser);
			extrtCondbyInqDao.addApprovalDelegateHist(summonUser);
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			e.printStackTrace();
			result = "fail";
			throw new ESException("SYS033J");
		}
		
		return result;
	}



	@Override
	public DataModelAndView approvalDelegateDept(Map<String, String> parameters) {
		DataModelAndView modelAndView = new DataModelAndView();
		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtCondbyInq/approvalDelegateDept.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		if(parameters.get("delegation_from") == null || parameters.get("delegation_from").equals("")) {
			Map<String, String> map = defaultDate(7);
			parameters.put("delegation_from",map.get("before"));
			parameters.put("delegation_to",map.get("after"));
		}
		
		if(parameters.get("delegate_search_fr") == null || parameters.get("delegate_search_fr").equals("")) {
			Map<String, String> map = defaultDate(7);
			parameters.put("delegate_search_fr",map.get("before"));
			parameters.put("delegate_search_to",map.get("after"));
		}
		
		if(parameters.get("delegate_hist_search_fr") == null || parameters.get("delegate_hist_search_fr").equals("")) {
			Map<String, String> map = defaultDate(7);
			parameters.put("delegate_hist_search_fr",map.get("before"));
			parameters.put("delegate_hist_search_to",map.get("after"));
		}
		
		try {
			AdminUserSearch adminUser = new AdminUserSearch();
			adminUser.setAuth_id("AUTH00004");
			adminUser.setOption("AUTH00004");
			adminUser.setUseExcel("true");
			List<AdminUser> adminUserList = adminUserMngtDao.findAdminUserMngtList(adminUser );
			
			List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList_byAdmin(new SearchSearch());
			List<ExtrtCondbyInq> delegateList = extrtCondbyInqDao.findAppDelegateList_dept(parameters);
			List<ExtrtCondbyInq> delegateHistList = extrtCondbyInqDao.findAppDelegateHistList_dept(parameters);
			List<Department> departmentList = deptMngtDao.findDepartmentMngtList(new Department());
			modelAndView.addObject("adminUserList", adminUserList);
			modelAndView.addObject("systemMasterList", systemMasterList);
			modelAndView.addObject("delegateList", delegateList);
			modelAndView.addObject("delegateHistList", delegateHistList);
			modelAndView.addObject("departmentList", departmentList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		modelAndView.addObject("parameters",parameters);
		
		return modelAndView;
	}



	@Override
	public String approvalDelegateDeptAdd(Map<String, String> parameters, HttpServletRequest request) {
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		String result = "success";
		try {
			ExtrtCondbyInq search = CommonHelper.convertMapToBean(parameters, ExtrtCondbyInq.class);
			
			  if(!nullCheck4(search))return "null"; // 입력값이 비어서 왔을경우 예외처리
			 			
			int count = extrtCondbyInqDao.addApprovalDelegateDept(search);
			if(count<1)return "conflict";	// 이미 위임 날짜가 겹치는 소명 위임이 있음
			
			parameters.put("summon_seq", search.getSummon_seq());
			ExtrtCondbyInq summonUser = extrtCondbyInqDao.findAppDelegateOne_dept(parameters);
			summonUser.setDelegation_act("grant");
			extrtCondbyInqDao.addApprovalDelegateHist_dept(summonUser);
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			e.printStackTrace();
			result = "fail";
			throw new ESException("SYS033J");
		}
		
		return result;
	}



	@Override
	public String approvalDelegateDeptDelete(Map<String, String> parameters, HttpServletRequest request) {
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		String result = "success";
		try {
			ExtrtCondbyInq summonUser = extrtCondbyInqDao.findAppDelegateOne_dept(parameters);
			summonUser.setDelegation_act("invoke");
			extrtCondbyInqDao.deleteApprovalDelegateDept(summonUser);
			extrtCondbyInqDao.addApprovalDelegateHist_dept(summonUser);
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			e.printStackTrace();
			result = "fail";
			throw new ESException("SYS033J");
		}
		
		return result;
	}
	
	//신한카드 소명요청 커스텀 (소명파일 생성)
	public void shCardSummonFile(AdminUser paramBean) {
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		
		try {
			ExtrtCondbyInq shCardSummonInfo = extrtCondbyInqDao.findShcard_summonInfo(paramBean);
			extrtCondbyInqDao.addShcard_summmonInfo(paramBean);
			
			String message ="";
			
			SimpleDateFormat format1 = new SimpleDateFormat ( "yyyyMMdd");
			SimpleDateFormat format2 = new SimpleDateFormat ( "HHmmss");
			SimpleDateFormat format3 = new SimpleDateFormat ( "yyyyMMddHH");
			SimpleDateFormat format4 = new SimpleDateFormat ( "HH");
					
			Date time = new Date();
					
			String time1 = format1.format(time);
			String time2 = format2.format(time);
			String time3 = format3.format(time);
			
			Calendar cal = Calendar.getInstance(); cal.setTime(time);
			cal.setTime(time);
			cal.add(Calendar.DATE, +1);
			
			SimpleDateFormat sdformat = new SimpleDateFormat("yyyyMMdd");
			String tomorrow = sdformat.format(cal.getTime());
	
			String fileName = "";
			
			//00 ~ 12 : 12H, nowDay
			//12 ~ 15 : 15H, nowDay
			//15 ~ 00 : 12H, now +1Day 
			if( 0 <= Integer.parseInt(format4.format(time)) && Integer.parseInt(format4.format(time)) < 12){
				fileName = summonFilePath+"SC_PM_EVENT_REQ_12H."+time1;
				message = "HD" + time1 + "ISM00130\n";
			}else if( 12 <= Integer.parseInt(format4.format(time)) && Integer.parseInt(format4.format(time)) < 15) {
				fileName = summonFilePath+"SC_PM_EVENT_REQ_15H."+time1;
				message = "HD" + time1 + "ISM00131\n";
			}else if( 15 <= Integer.parseInt(format4.format(time)) && Integer.parseInt(format4.format(time)) < 24) {
				fileName = summonFilePath+"SC_PM_EVENT_REQ_12H."+tomorrow;
				message = "HD" + time1 + "ISM00130\n";
			}
			
			File file = new File(fileName);
	        
	        String str = shCardSummonInfo.getParameter();
	        int line_cnt = 0;
			FileWriter fw = null;
			
			try {
				
				if (file.isFile()) { //파일이 있을 경우
					FileReader fr = new FileReader(file);
					BufferedReader br = new BufferedReader(fr);

					while (br.readLine() != null) {
						line_cnt++;
					}
					List<String> lines = java.nio.file.Files.readAllLines(file.toPath());
					lines.add(line_cnt-1, str);
					java.nio.file.Files.write(file.toPath(), lines);
					
			    }
			    else {	//파일이 없을 경우
			    	String msg;
			    	
			    	message += str + "\n";
			    	message += "TR000000000000000000\n";
			    	fw = new FileWriter(file);
			    	BufferedWriter bw = new BufferedWriter(fw);
			    	
			    	//기존 파일의 내용에 이어서 쓰려면 true를, 기존 내용을 없애고 새로 쓰려면 false를 지정한다.
			    	fw = new FileWriter(file, false);
					fw.write(message);
					fw.flush();
			    }

			} catch (IOException e) {
				e.printStackTrace();
				transactionManager.rollback(transactionStatus);
				throw new ESException("SHCARD001J");
			} finally {
				try {
					if (fw != null)
						fw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			transactionManager.commit(transactionStatus);
			
		} catch (DataAccessException e) {
			e.printStackTrace();
			transactionManager.rollback(transactionStatus);
			throw new ESException("SHCARD002J");
		}
	}
	
	//20201222 hbjang 명지전문대 비정상위험분석 보고서 커스터마이징
	@Override
	public DataModelAndView findExtrtReport(int emp_detail_seq, String occr_dt) {
		
		DataModelAndView modelAndView = new DataModelAndView();
		try {
			SearchSearch search = new SearchSearch();
			search.setEmp_detail_seq(String.valueOf(emp_detail_seq));
			search.setOccr_dt(occr_dt);
			ExtrtCondbyInq reportInfo = extrtCondbyInqDao.findExtrtReport(search);
			
			modelAndView.addObject("reportInfo", reportInfo);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		
		return modelAndView;
	}
	
	@Override
	public void extrtExcelDownload(DataModelAndView modelAndView, SearchSearch search,
			HttpServletRequest request) {

		search.setUseExcel("true");

		String fileName = "PSM_비정상위험데이터_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "비정상위험데이터";
		
		SimpleDateFormat par = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd");
		List<ExtrtCondbyInq> extrtCondbyInq = extrtCondbyInqDao.findExtrtExcelDownload(search);
//		for(ExtrtCondbyInq e : extrtCondbyInq) {
//			
//		}

		String[] heads = new String[] {};
		String[] columns = new String[] {};
		heads = new String[] { "TITLE", "MONTH", "계정ID", "접속프로그램", "접속일시", "접속자정보", "처리한정보주체 정보", "수행업무", "이름", "직급", "부서/학과명"
								, "데이터 건수","사용목적","관련업무","보유기간","개인정보포함여부","기타","파기예정일","재직상태"};
		columns = new String[] { "rule_nm", "proc_month", "emp_user_id", "req_url", "datetime", "user_ip", "rule_nm", "req_type", "emp_user_name", "h_body", "dept_name"
								, "log_cnt", "h_body", "h_body", "h_body", "msg", "h_body", "h_body", "status"};
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", extrtCondbyInq);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}
}
