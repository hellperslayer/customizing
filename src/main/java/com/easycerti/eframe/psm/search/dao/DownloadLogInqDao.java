package com.easycerti.eframe.psm.search.dao;

import java.util.List;

import com.easycerti.eframe.psm.search.vo.AllLogInq;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.system_management.vo.Misdetect;

public interface DownloadLogInqDao {

	public List<AllLogInq> findDownloadLogInqList(SearchSearch search);
	public List<AllLogInq> findDownloadLogInqAuth20List(SearchSearch search);	//다운로드사유입력 권한자
	public int findDownloadLogInqList_count(SearchSearch search);
	public int findDownloadLogInqListAuth20_count(SearchSearch search);			//다운로드사유입력 권한자
	
	public AllLogInq findDownloadLogInqDetail(SearchSearch search);
	public List<AllLogInq> findDownloadLogInqFileList(SearchSearch search);
	
	public List<AllLogInq> findDownloadLogInqDetailResult(SearchSearch search);
	public int findDownloadLogInqDetailResultCount(SearchSearch search);
	

	public void saveDownloadLogReason(SearchSearch allLogInq);
	public List<AllLogInq> findDownloadLogGroupBySystem(String emp_user_id);
	
	public String checkPattern(Misdetect misdetect);
	public void addPrivacy(Misdetect misdetect);
	public void removePrivacy(Misdetect misdetect);
}
