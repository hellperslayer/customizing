package com.easycerti.eframe.psm.search.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.AbstractValueObject;
/**
 * 
 * 설명 : 추출조건별조회 리스트 Bean
 * @author tjlee
 * @since 2015. 4. 22.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 22.           tjlee            최초 생성
 *
 * </pre>
 */
public class ExtrtCondbyInqList extends AbstractValueObject{

	// 추출조건별조회 리스트
	private List<ExtrtCondbyInq> extrtCondbyInq = null;

	
	public ExtrtCondbyInqList(){
		
	}
	
	public ExtrtCondbyInqList(List<ExtrtCondbyInq> extrtCondbyInq){
		this.extrtCondbyInq = extrtCondbyInq;
	}
	
	public ExtrtCondbyInqList(List<ExtrtCondbyInq> extrtCondbyInq, String page_total_count){
		this.extrtCondbyInq = extrtCondbyInq;
		setPage_total_count(page_total_count);
	}

	public List<ExtrtCondbyInq> getExtrtCondbyInq() {
		return extrtCondbyInq;
	}

	public void setExtrtCondbyInq(List<ExtrtCondbyInq> extrtCondbyInq) {
		this.extrtCondbyInq = extrtCondbyInq;
	}
}
