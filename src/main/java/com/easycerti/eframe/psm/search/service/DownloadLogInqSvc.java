package com.easycerti.eframe.psm.search.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.search.vo.SearchSearch;

public interface DownloadLogInqSvc {

	public DataModelAndView findDownloadLogInqList(SearchSearch search);
	
	public DataModelAndView findDownloadLogInqDetail(SearchSearch search);
	
	public void findDownloadLogInqList_download(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request) throws Exception;
	public void findDownloadLogInqList_downloadCSV(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);
	
	public DataModelAndView saveDownloadLogReason(Map<String, String> parameters);

	public void findDownloadLogInqDetail_download(DataModelAndView modelAndView, SearchSearch search,
			HttpServletRequest request);

	public void findDownloadLoginqDetail_downloadCSV(DataModelAndView modelAndView, SearchSearch search,
			HttpServletRequest request);
	
	public DataModelAndView addPrivacy(Map<String, String> parameters);
	public DataModelAndView removePrivacy(Map<String, String> parameters);

}
