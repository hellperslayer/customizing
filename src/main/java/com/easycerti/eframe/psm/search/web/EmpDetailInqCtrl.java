package com.easycerti.eframe.psm.search.web;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.search.service.EmpDetailInqSvc;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
/**
 * 
 * 설명 : 위험도별조회 Controller
 * @author tjlee
 * @since 2015. 4. 22.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 22.           tjlee            최초 생성
 *
 * </pre>
 */
@Controller
@RequestMapping("/empDetailInq/*")
public class EmpDetailInqCtrl {

	@Autowired
	private EmpDetailInqSvc empDetailInqSvc;
	
	/**
	 * 위험도별조회 List
	 */
	@RequestMapping(value={"list.html"}, method={RequestMethod.POST})
	public DataModelAndView findEmpDetailInqList(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {

		Date today = new Date();
		search.initDatesIfEmpty(today, today);
		
		DataModelAndView modelAndView = empDetailInqSvc.findEmpDetailInqList(search);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("empDetailInqList");

		return modelAndView;
	}

	/**
	 * 위험도별조회상세List
	 */
	@RequestMapping(value={"detail.html"}, method={RequestMethod.POST})
	public DataModelAndView findEmpDetailInqDetail(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {

		//위험도별조회 상세 리스트 페이징 page_num 세팅
		if(parameters.get("empDetailInqDetail_page_num")!=null){
			search.getEmpDetailInqDetail().setPage_num(Integer.valueOf(parameters.get("empDetailInqDetail_page_num")));
		}
		
		DataModelAndView modelAndView = empDetailInqSvc.findEmpDetailInqDetail(search);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName("empDetailInqDetail");

		return modelAndView;
	}
	

	/**
	 * 위험도별조회 엑셀 다운로드
	 */
	@RequestMapping(value="download.html", method={RequestMethod.POST})
	public DataModelAndView findEmpDetailInqList_download(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		empDetailInqSvc.findEmpDetailInqList_download(modelAndView, search, request);
		
		return modelAndView;
	}
}
