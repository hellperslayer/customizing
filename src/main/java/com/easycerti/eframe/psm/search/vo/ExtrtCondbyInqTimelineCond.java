package com.easycerti.eframe.psm.search.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.AbstractValueObject;
/**
 * 
 * 설명 : 추출조건별조회 VO
 * @author tjlee
 * @since 2015. 4. 22.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 22.           tjlee            최초 생성
 *
 * </pre>
 */
public class ExtrtCondbyInqTimelineCond extends AbstractValueObject{
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 3548522096551273127L;
	
	private String search_from;	
	private String search_to;
	private String system_seq;
	private int rule_cd;
	private String user_id;
	private String user_name;
	private String dept_name;
	private String user_ip;
	
	List<String> auth_idsList;
	
	public String getUser_ip() {
		return user_ip;
	}
	public void setUser_ip(String user_ip) {
		this.user_ip = user_ip;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getDept_name() {
		return dept_name;
	}
	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}
	public List<String> getAuth_idsList() {
		return auth_idsList;
	}
	public void setAuth_idsList(List<String> auth_idsList) {
		this.auth_idsList = auth_idsList;
	}
	public String getSearch_from() {
		return search_from;
	}
	public void setSearch_from(String search_from) {
		this.search_from = search_from;
	}
	public String getSearch_to() {
		return search_to;
	}
	public void setSearch_to(String search_to) {
		this.search_to = search_to;
	}
	public String getSystem_seq() {
		return system_seq;
	}
	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}
	public int getRule_cd() {
		return rule_cd;
	}
	public void setRule_cd(int rule_cd) {
		this.rule_cd = rule_cd;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
