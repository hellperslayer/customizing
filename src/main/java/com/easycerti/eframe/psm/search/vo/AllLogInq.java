package com.easycerti.eframe.psm.search.vo;

import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.easycerti.eframe.common.util.PageInfo;
import com.easycerti.eframe.common.vo.AbstractValueObject;
/**
 * 
 * 설명 : 전체로그조회 VO
 * @author tjlee
 * @since 2015. 4. 22.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 22.           tjlee            최초 생성
 *
 * </pre>
 */
public class AllLogInq extends AbstractValueObject{
	/*
	biz_log
	*/
	private String agency_cd;
	private String desc_div;
	private String proc_datetime;
	private String org_cd;
	private String org_nm;
	private String hq_cd;
	private String hq_nm;
	private String acc_org_cd;
	private String acc_hq_cd;
	private String acc_hq_nm;
	private String bran_cd;
	private String bran_nm;
	private String nh_dept_cd;
	private String nh_dept_nm;
	private String cd_org_cd;
	private String cd_org_nm;
	private String car_cd;
	private String site_cd;
	private String member_div;
	private String join_ssn;
	private String sear_cont;
	private String sear_val;
	private String inq_seq;
	private String cert_num;
	private String sear_log24;
	private String per_inf_cd;
	private String esta_sym;
	private String firm_nm;
	private String assu_ssn;
	private String assu_nm;
	private String contact_condition;
	private String psnl_inf_qry_hed_colnm;
	private String psnl_inf_cnts;
	private String inq_reason;
	private String ykiho_cd;
	private String ykiho_nm;
	private String recv_no;
	private String recv_yyyy;
	private String output_pgm_id;
	private String busi_cd;
	private String busi_nm;
	private String busi_dtl_contn;
	private String inq_db;
	private String patient_cd;
	private String patient_nm;
	private String bloodno;
	private String result;
	private String prg_id;
	private String prg_nm;
	private String vcls;
	private String ssn_org_cd;
	private String ssn_org_nm;
	private String ssn_hq_cd;
	private String ssn_hq_nm;
	private String juri_out;
	private String firm_cd;
	private String firm_cd_nm;
	private String firm_hq_cd;
	private String firm_hq_nm;
	private String assu_org_cd;
	private String assu_org_nm;
	private String assu_hq_cd;
	private String assu_hq_nm;
	private String audit_no;
	private String req_date;
	private String ans_date;
	private String resummon_yn;
	private String rule_nm;
	private String summon_reason;
	
	private long log_seq;
	private String proc_date;
	private String proc_time;
	private String emp_user_id;
	private String emp_user_name;
	private String dept_id;
	private String dept_name;
	private String user_ip;
	private String user_id;
	private String scrn_id;
	private String scrn_name;
	private String req_type;
	private String system_seq;
	private String system_name;
	private String system_count;
	private String server_seq;
	private String req_context;
	private String req_url;
	private String req_end_time;
	private String description;
	private String result_type;
	private String privacy_desc;
	private String result_content;
	private String result_content_masking;
	private PageInfo pageInfo;
	private String privacyType;
	private String privacy;
	private String inserter_id;
	private Timestamp inserter_date;
	private int privacy_seq;
	private long biz_log_result_seq;
	private String check_exc;
	private int time_count;
	private String TIME;
	private String result_type_sort; 
	
	private String email_address;
	private String log_delimiter;
	

	//private String proc_date     		;
	//private String 	proc_time     	;
	private String 	emp_cd        	;
	private String 	emp_nm        	;
	//private String 	user_id       	;
	//private String 	user_ip       	;
	private String 	dept_cd       	;
	private String 	dept_nm       	;
	private String 	button_cd     	;
	//private String 	scrn_id       	;
	private String 	scrn_nm       	;
	//private String 	req_url       	;
	private String 	grade         	;
	private String 	status        	;
	
	private String 	log_seq_str       	;
	private String 	sys_cd        	;
	private String 	log_agency_cd 	;
	private String 	threadid      	;
	private String 	sessionid     	;
	private String 	reqcontext    	;
	private String 	requrl        	;
	private String 	reqendtime    	;
	private String 	serverip      	;
	private String 	application   	;
	private String 	summon_dt     	;
	private String 	expect_dt     	;
	private String 	summon_req_emp	;
	private String 	desc_dt       	;
	private String 	desc_seq      	;
	private String 	summon_yn     	;
	private String 	desc_status   	;
	private String 	desc_result   	;
	private String 	hc_rule_nm    	;
	private String 	summon_seq    	;
	private String 	hc_send_dt    	;
	private String 	decision_dt   	;
	private String 	ssn_name      	;	
	private String 	posit_gu     	; 	
	private String 	acc_org_nm  	;  	
	//private String 	result_type	;   	 
	private String summon_flag;
	private String sellertool;
	private String privacy_masking;
	private int cnt;
	private int cnt2;
	private String req_sql;
	private String res_sql;
	
	// 2017-11-14 엑셀다운시 개인정보유형별로 표기하기 위해
	private int data1 = 0;
	private int data2 = 0;
	private int data3 = 0;
	private int data4 = 0;
	private int data5 = 0;
	private int data6 = 0;
	private int data7 = 0;
	private int data8 = 0;
	private int data9 = 0;
	private int data10 = 0;
	private int data11 = 0;
	private int data12 = 0;
	private int data13 = 0;
	private int data14 = 0;
	private int data15 = 0;
	private int data16 = 0;
	private int data17 = 0;
	private int data18 = 0;
	private int data19 = 0;
	private int data20 = 0;
	private int data99 = 0;
	
	private String query_id;
	private String rd_id;
	private String grid_id;
	private String rd_name;
	private String grid_name;
	private String request_data;
	private String file_name;
	private String file_ext;
	private String file_direction;
	private String file_path;
	private String detection_src;
	private String req_body;
	private String resp_body;
	
	private String driver_name;
	private String url;
	private String id;
	private String password;
	private String sql;
	private String privacy_column;
	
	private String approval_date;
	private String approval_type;
	private String approval_level;
	private String approval_result;
	private String approval_user_id;
	private String approval_user_name;
	private String approval_dept_id;
	private String approval_dept_name;
	private int rank;
	private int cnt1;
	private String reason;
	private String result_owner;
	private String download_log_result_seq;
	
	//파일 대장 등록 여부
	private String register_status;
	private String menu_name;
	
	private String threshold;
	private String threshold_use;
	private String threshold_total_inbox;
	
	private int type1=0;
	private int type2=0;
	private int type3=0;
	private int type4=0;
	private int type5=0;
	private int type6=0;
	private int type7=0;
	private int type8=0;
	private int type9=0;
	private int type10=0;
	private int type11=0;
	private int type12=0;
	private int type13=0;
	private int type14=0;
	private int type15=0;
	private int type16=0;
	private int type17=0;
	private int type18=0;
	private int type19=0;
	private int type20=0;
	private int type99=0;
	private Map<String,String> resultTypeMap;
	
    private String privacy_order1;
    private String privacy_order2;
    private String result_count;
    
    private String sql_group_idx;
    
    private String crud_type;
    
	public String getPrivacy_order1() {
        return privacy_order1;
    }
    public void setPrivacy_order1(String privacy_order1) {
        this.privacy_order1 = privacy_order1;
    }
    public String getPrivacy_order2() {
        return privacy_order2;
    }
    public void setPrivacy_order2(String privacy_order2) {
        this.privacy_order2 = privacy_order2;
    }
    public String getSql_group_idx() {
        return sql_group_idx;
    }
    public void setSql_group_idx(String sql_group_idx) {
        this.sql_group_idx = sql_group_idx;
    }
    public String getResult_count() {
        return result_count;
    }
    public void setResult_count(String result_count) {
        this.result_count = result_count;
    }
	
	public void setType1(int type1) {
		this.type1 = type1;
	}
	public void setType2(int type2) {
		this.type2 = type2;
	}
	public void setType3(int type3) {
		this.type3 = type3;
	}
	public void setType4(int type4) {
		this.type4 = type4;
	}
	public void setType5(int type5) {
		this.type5 = type5;
	}
	public void setType6(int type6) {
		this.type6 = type6;
	}
	public void setType7(int type7) {
		this.type7 = type7;
	}
	public void setType8(int type8) {
		this.type8 = type8;
	}
	public void setType9(int type9) {
		this.type9 = type9;
	}
	public void setType10(int type10) {
		this.type10 = type10;
	}
	public void setType11(int type11) {
		this.type11 = type11;
	}
	public void setType12(int type12) {
		this.type12 = type12;
	}
	public void setType13(int type13) {
		this.type13 = type13;
	}
	public void setType14(int type14) {
		this.type14 = type14;
	}
	public void setType15(int type15) {
		this.type15 = type15;
	}
	public void setType16(int type16) {
		this.type16 = type16;
	}
	public void setType17(int type17) {
		this.type17 = type17;
	}
	public void setType18(int type18) {
		this.type18 = type18;
	}
	public void setType19(int type19) {
		this.type19 = type19;
	}
	public void setType20(int type20) {
		this.type20 = type20;
	}
	public void setType99(int type99) {
		this.type99 = type99;
	}
	public int getType1() {
		return type1;
	}
	public void countType1() {
		this.type1++;
	}
	public int getType2() {
		return type2;
	}
	public void countType2() {
		this.type2++;
	}
	public int getType3() {
		return type3;
	}
	public void countType3() {
		this.type3++;
	}
	public int getType4() {
		return type4;
	}
	public void countType4() {
		this.type4++;
	}
	public int getType5() {
		return type5;
	}
	public void countType5() {
		this.type5++;
	}
	public int getType6() {
		return type6;
	}
	public void countType6() {
		this.type6++;
	}
	public int getType7() {
		return type7;
	}
	public void countType7() {
		this.type7++;
	}
	public int getType8() {
		return type8;
	}
	public void countType8() {
		this.type8++;
	}
	public int getType9() {
		return type9;
	}
	public void countType9() {
		this.type9++;
	}
	public int getType10() {
		return type10;
	}
	public void countType10() {
		this.type10++;
	}
	public int getType11() {
		return type11;
	}
	public void countType11() {
		this.type11++;
	}
	public int getType12() {
		return type12;
	}
	public void countType12() {
		this.type12++;
	}
	public int getType13() {
		return type13;
	}
	public void countType13() {
		this.type13++;
	}
	public int getType14() {
		return type14;
	}
	public void countType14() {
		this.type14++;
	}
	public int getType15() {
		return type15;
	}
	public void countType15() {
		this.type15++;
	}
	
	
	public int getType16() {
		return type16;
	}
	public void countType16() {
		this.type16++;
	}
	public int getType17() {
		return type17;
	}
	public void countType17() {
		this.type17++;
	}
	public int getType18() {
		return type18;
	}
	public void countType18() {
		this.type18++;
	}
	public int getType19() {
		return type19;
	}
	public void countType19() {
		this.type19++;
	}
	public void countType20() {
		this.type20++;
	}
	public int getType20() {
		return type20;
	}
	public void countType99() {
		this.type99++;
	}
	public int getType99() {
		return type99;
	}
	public String getMenu_name() {
		return menu_name;
	}
	public void setMenu_name(String menu_name) {
		this.menu_name = menu_name;
	}
	public String getDownload_log_result_seq() {
		return download_log_result_seq;
	}
	public void setDownload_log_result_seq(String download_log_result_seq) {
		this.download_log_result_seq = download_log_result_seq;
	}
	public String getResult_owner() {
		return result_owner;
	}
	public void setResult_owner(String result_owner) {
		this.result_owner = result_owner;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public int getCnt1() {
		return cnt1;
	}
	public void setCnt1(int cnt1) {
		this.cnt1 = cnt1;
	}
	public String getDesc_div() {
		return desc_div;
	}
	public void setDesc_div(String desc_div) {
		this.desc_div = desc_div;
	}
	public String getProc_datetime() {
		return proc_datetime;
	}
	public void setProc_datetime(String proc_datetime) {
		this.proc_datetime = proc_datetime;
	}
	public String getJoin_ssn() {
		return join_ssn;
	}
	public void setJoin_ssn(String join_ssn) {
		this.join_ssn = join_ssn;
	}
	public String getBusi_nm() {
		return busi_nm;
	}
	public void setBusi_nm(String busi_nm) {
		this.busi_nm = busi_nm;
	}
	public String getReq_date() {
		return req_date;
	}
	public void setReq_date(String req_date) {
		this.req_date = req_date;
	}
	public String getAns_date() {
		return ans_date;
	}
	public void setAns_date(String ans_date) {
		this.ans_date = ans_date;
	}
	public String getResummon_yn() {
		return resummon_yn;
	}
	public void setResummon_yn(String resummon_yn) {
		this.resummon_yn = resummon_yn;
	}
	public String getRule_nm() {
		return rule_nm;
	}
	public void setRule_nm(String rule_nm) {
		this.rule_nm = rule_nm;
	}
	public String getSummon_reason() {
		return summon_reason;
	}
	public void setSummon_reason(String summon_reason) {
		this.summon_reason = summon_reason;
	}
	public String getAgency_cd() {
		return agency_cd;
	}
	public void setAgency_cd(String agency_cd) {
		this.agency_cd = agency_cd;
	}
	public String getOrg_cd() {
		return org_cd;
	}
	public void setOrg_cd(String org_cd) {
		this.org_cd = org_cd;
	}
	public String getOrg_nm() {
		return org_nm;
	}
	public void setOrg_nm(String org_nm) {
		this.org_nm = org_nm;
	}
	public String getHq_cd() {
		return hq_cd;
	}
	public void setHq_cd(String hq_cd) {
		this.hq_cd = hq_cd;
	}
	public String getHq_nm() {
		return hq_nm;
	}
	public void setHq_nm(String hq_nm) {
		this.hq_nm = hq_nm;
	}
	public String getAcc_org_cd() {
		return acc_org_cd;
	}
	public void setAcc_org_cd(String acc_org_cd) {
		this.acc_org_cd = acc_org_cd;
	}
	public String getAcc_hq_cd() {
		return acc_hq_cd;
	}
	public void setAcc_hq_cd(String acc_hq_cd) {
		this.acc_hq_cd = acc_hq_cd;
	}
	public String getAcc_hq_nm() {
		return acc_hq_nm;
	}
	public void setAcc_hq_nm(String acc_hq_nm) {
		this.acc_hq_nm = acc_hq_nm;
	}
	public String getBran_cd() {
		return bran_cd;
	}
	public void setBran_cd(String bran_cd) {
		this.bran_cd = bran_cd;
	}
	public String getBran_nm() {
		return bran_nm;
	}
	public void setBran_nm(String bran_nm) {
		this.bran_nm = bran_nm;
	}
	public String getNh_dept_cd() {
		return nh_dept_cd;
	}
	public void setNh_dept_cd(String nh_dept_cd) {
		this.nh_dept_cd = nh_dept_cd;
	}
	public String getNh_dept_nm() {
		return nh_dept_nm;
	}
	public void setNh_dept_nm(String nh_dept_nm) {
		this.nh_dept_nm = nh_dept_nm;
	}
	public String getCd_org_cd() {
		return cd_org_cd;
	}
	public void setCd_org_cd(String cd_org_cd) {
		this.cd_org_cd = cd_org_cd;
	}
	public String getCd_org_nm() {
		return cd_org_nm;
	}
	public void setCd_org_nm(String cd_org_nm) {
		this.cd_org_nm = cd_org_nm;
	}
	public String getCar_cd() {
		return car_cd;
	}
	public void setCar_cd(String car_cd) {
		this.car_cd = car_cd;
	}
	public String getSite_cd() {
		return site_cd;
	}
	public void setSite_cd(String site_cd) {
		this.site_cd = site_cd;
	}
	public String getMember_div() {
		return member_div;
	}
	public void setMember_div(String member_div) {
		this.member_div = member_div;
	}
	public String getSear_cont() {
		return sear_cont;
	}
	public void setSear_cont(String sear_cont) {
		this.sear_cont = sear_cont;
	}
	public String getSear_val() {
		return sear_val;
	}
	public void setSear_val(String sear_val) {
		this.sear_val = sear_val;
	}
	public String getInq_seq() {
		return inq_seq;
	}
	public void setInq_seq(String inq_seq) {
		this.inq_seq = inq_seq;
	}
	public String getCert_num() {
		return cert_num;
	}
	public void setCert_num(String cert_num) {
		this.cert_num = cert_num;
	}
	public String getSear_log24() {
		return sear_log24;
	}
	public void setSear_log24(String sear_log24) {
		this.sear_log24 = sear_log24;
	}
	public String getPer_inf_cd() {
		return per_inf_cd;
	}
	public void setPer_inf_cd(String per_inf_cd) {
		this.per_inf_cd = per_inf_cd;
	}
	public String getEsta_sym() {
		return esta_sym;
	}
	public void setEsta_sym(String esta_sym) {
		this.esta_sym = esta_sym;
	}
	public String getFirm_nm() {
		return firm_nm;
	}
	public void setFirm_nm(String firm_nm) {
		this.firm_nm = firm_nm;
	}
	public String getAssu_ssn() {
		return assu_ssn;
	}
	public void setAssu_ssn(String assu_ssn) {
		this.assu_ssn = assu_ssn;
	}
	public String getAssu_nm() {
		return assu_nm;
	}
	public void setAssu_nm(String assu_nm) {
		this.assu_nm = assu_nm;
	}
	public String getContact_condition() {
		return contact_condition;
	}
	public void setContact_condition(String contact_condition) {
		this.contact_condition = contact_condition;
	}
	public String getPsnl_inf_qry_hed_colnm() {
		return psnl_inf_qry_hed_colnm;
	}
	public void setPsnl_inf_qry_hed_colnm(String psnl_inf_qry_hed_colnm) {
		this.psnl_inf_qry_hed_colnm = psnl_inf_qry_hed_colnm;
	}
	public String getPsnl_inf_cnts() {
		return psnl_inf_cnts;
	}
	public void setPsnl_inf_cnts(String psnl_inf_cnts) {
		this.psnl_inf_cnts = psnl_inf_cnts;
	}
	public String getInq_reason() {
		return inq_reason;
	}
	public void setInq_reason(String inq_reason) {
		this.inq_reason = inq_reason;
	}
	public String getYkiho_cd() {
		return ykiho_cd;
	}
	public void setYkiho_cd(String ykiho_cd) {
		this.ykiho_cd = ykiho_cd;
	}
	public String getYkiho_nm() {
		return ykiho_nm;
	}
	public void setYkiho_nm(String ykiho_nm) {
		this.ykiho_nm = ykiho_nm;
	}
	public String getRecv_no() {
		return recv_no;
	}
	public void setRecv_no(String recv_no) {
		this.recv_no = recv_no;
	}
	public String getRecv_yyyy() {
		return recv_yyyy;
	}
	public void setRecv_yyyy(String recv_yyyy) {
		this.recv_yyyy = recv_yyyy;
	}
	public String getOutput_pgm_id() {
		return output_pgm_id;
	}
	public void setOutput_pgm_id(String output_pgm_id) {
		this.output_pgm_id = output_pgm_id;
	}
	public String getBusi_cd() {
		return busi_cd;
	}
	public void setBusi_cd(String busi_cd) {
		this.busi_cd = busi_cd;
	}
	public String getBusi_dtl_contn() {
		return busi_dtl_contn;
	}
	public void setBusi_dtl_contn(String busi_dtl_contn) {
		this.busi_dtl_contn = busi_dtl_contn;
	}
	public String getInq_db() {
		return inq_db;
	}
	public void setInq_db(String inq_db) {
		this.inq_db = inq_db;
	}
	public String getPatient_cd() {
		return patient_cd;
	}
	public void setPatient_cd(String patient_cd) {
		this.patient_cd = patient_cd;
	}
	public String getPatient_nm() {
		return patient_nm;
	}
	public void setPatient_nm(String patient_nm) {
		this.patient_nm = patient_nm;
	}
	public String getBloodno() {
		return bloodno;
	}
	public void setBloodno(String bloodno) {
		this.bloodno = bloodno;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getPrg_id() {
		return prg_id;
	}
	public void setPrg_id(String prg_id) {
		this.prg_id = prg_id;
	}
	public String getPrg_nm() {
		return prg_nm;
	}
	public void setPrg_nm(String prg_nm) {
		this.prg_nm = prg_nm;
	}
	public String getVcls() {
		return vcls;
	}
	public void setVcls(String vcls) {
		this.vcls = vcls;
	}
	public String getSsn_org_cd() {
		return ssn_org_cd;
	}
	public void setSsn_org_cd(String ssn_org_cd) {
		this.ssn_org_cd = ssn_org_cd;
	}
	public String getSsn_org_nm() {
		return ssn_org_nm;
	}
	public void setSsn_org_nm(String ssn_org_nm) {
		this.ssn_org_nm = ssn_org_nm;
	}
	public String getSsn_hq_cd() {
		return ssn_hq_cd;
	}
	public void setSsn_hq_cd(String ssn_hq_cd) {
		this.ssn_hq_cd = ssn_hq_cd;
	}
	public String getSsn_hq_nm() {
		return ssn_hq_nm;
	}
	public void setSsn_hq_nm(String ssn_hq_nm) {
		this.ssn_hq_nm = ssn_hq_nm;
	}
	public String getJuri_out() {
		return juri_out;
	}
	public void setJuri_out(String juri_out) {
		this.juri_out = juri_out;
	}
	public String getFirm_cd() {
		return firm_cd;
	}
	public void setFirm_cd(String firm_cd) {
		this.firm_cd = firm_cd;
	}
	public String getFirm_cd_nm() {
		return firm_cd_nm;
	}
	public void setFirm_cd_nm(String firm_cd_nm) {
		this.firm_cd_nm = firm_cd_nm;
	}
	public String getFirm_hq_cd() {
		return firm_hq_cd;
	}
	public void setFirm_hq_cd(String firm_hq_cd) {
		this.firm_hq_cd = firm_hq_cd;
	}
	public String getFirm_hq_nm() {
		return firm_hq_nm;
	}
	public void setFirm_hq_nm(String firm_hq_nm) {
		this.firm_hq_nm = firm_hq_nm;
	}
	public String getAssu_org_cd() {
		return assu_org_cd;
	}
	public void setAssu_org_cd(String assu_org_cd) {
		this.assu_org_cd = assu_org_cd;
	}
	public String getAssu_org_nm() {
		return assu_org_nm;
	}
	public void setAssu_org_nm(String assu_org_nm) {
		this.assu_org_nm = assu_org_nm;
	}
	public String getAssu_hq_cd() {
		return assu_hq_cd;
	}
	public void setAssu_hq_cd(String assu_hq_cd) {
		this.assu_hq_cd = assu_hq_cd;
	}
	public String getAssu_hq_nm() {
		return assu_hq_nm;
	}
	public void setAssu_hq_nm(String assu_hq_nm) {
		this.assu_hq_nm = assu_hq_nm;
	}
	public String getAudit_no() {
		return audit_no;
	}
	public void setAudit_no(String audit_no) {
		this.audit_no = audit_no;
	}
	public String getApproval_date() {
		return approval_date;
	}
	public void setApproval_date(String approval_date) {
		this.approval_date = approval_date;
	}
	public String getApproval_type() {
		return approval_type;
	}
	public void setApproval_type(String approval_type) {
		this.approval_type = approval_type;
	}
	public String getApproval_level() {
		return approval_level;
	}
	public void setApproval_level(String approval_level) {
		this.approval_level = approval_level;
	}
	public String getApproval_result() {
		return approval_result;
	}
	public void setApproval_result(String approval_result) {
		this.approval_result = approval_result;
	}
	
	public String getApproval_user_id() {
		return approval_user_id;
	}
	public void setApproval_user_id(String approval_user_id) {
		this.approval_user_id = approval_user_id;
	}
	public String getApproval_user_name() {
		return approval_user_name;
	}
	public void setApproval_user_name(String approval_user_name) {
		this.approval_user_name = approval_user_name;
	}
	public String getApproval_dept_id() {
		return approval_dept_id;
	}
	public void setApproval_dept_id(String approval_dept_id) {
		this.approval_dept_id = approval_dept_id;
	}
	public String getApproval_dept_name() {
		return approval_dept_name;
	}
	public void setApproval_dept_name(String approval_dept_name) {
		this.approval_dept_name = approval_dept_name;
	}
	public String getPrivacy_column() {
		return privacy_column;
	}
	public void setPrivacy_column(String privacy_column) {
		this.privacy_column = privacy_column;
	}
	public String getDriver_name() {
		return driver_name;
	}
	public void setDriver_name(String driver_name) {
		this.driver_name = driver_name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSql() {
		return sql;
	}
	public void setSql(String sql) {
		this.sql = sql;
	}
	public String getRequest_data() {
		return request_data;
	}
	public void setRequest_data(String request_data) {
		this.request_data = request_data;
	}
	public String getQuery_id() {
		return query_id;
	}
	public void setQuery_id(String query_id) {
		this.query_id = query_id;
	}
	public String getRd_id() {
		return rd_id;
	}
	public void setRd_id(String rd_id) {
		this.rd_id = rd_id;
	}
	public String getGrid_id() {
		return grid_id;
	}
	public void setGrid_id(String grid_id) {
		this.grid_id = grid_id;
	}
	public String getRd_name() {
		return rd_name;
	}
	public void setRd_name(String rd_name) {
		this.rd_name = rd_name;
	}
	public String getGrid_name() {
		return grid_name;
	}
	public void setGrid_name(String grid_name) {
		this.grid_name = grid_name;
	}
	public String getSummon_flag() {
		return summon_flag;
	}
	public void setSummon_flag(String summon_flag) {
		this.summon_flag = summon_flag;
	}
	public String getEmail_address() {
		return email_address;
	}
	public String getLog_seq_str() {
		return log_seq_str;
	}
	public void setLog_seq_str(String log_seq_str) {
		this.log_seq_str = log_seq_str;
	}
	public String getEmp_cd() {
		return emp_cd;
	}
	public void setEmp_cd(String emp_cd) {
		this.emp_cd = emp_cd;
	}
	public String getEmp_nm() {
		return emp_nm;
	}
	public void setEmp_nm(String emp_nm) {
		this.emp_nm = emp_nm;
	}
	public String getDept_cd() {
		return dept_cd;
	}
	public void setDept_cd(String dept_cd) {
		this.dept_cd = dept_cd;
	}
	public String getDept_nm() {
		return dept_nm;
	}
	public void setDept_nm(String dept_nm) {
		this.dept_nm = dept_nm;
	}
	public String getButton_cd() {
		return button_cd;
	}
	public void setButton_cd(String button_cd) {
		this.button_cd = button_cd;
	}
	public String getScrn_nm() {
		return scrn_nm;
	}
	public void setScrn_nm(String scrn_nm) {
		this.scrn_nm = scrn_nm;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSys_cd() {
		return sys_cd;
	}
	public void setSys_cd(String sys_cd) {
		this.sys_cd = sys_cd;
	}
	public String getLog_agency_cd() {
		return log_agency_cd;
	}
	public void setLog_agency_cd(String log_agency_cd) {
		this.log_agency_cd = log_agency_cd;
	}
	public String getThreadid() {
		return threadid;
	}
	public void setThreadid(String threadid) {
		this.threadid = threadid;
	}
	public String getSessionid() {
		return sessionid;
	}
	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}
	public String getReqcontext() {
		return reqcontext;
	}
	public void setReqcontext(String reqcontext) {
		this.reqcontext = reqcontext;
	}
	public String getRequrl() {
		return requrl;
	}
	public void setRequrl(String requrl) {
		this.requrl = requrl;
	}
	public String getReqendtime() {
		return reqendtime;
	}
	public void setReqendtime(String reqendtime) {
		this.reqendtime = reqendtime;
	}
	public String getServerip() {
		return serverip;
	}
	public void setServerip(String serverip) {
		this.serverip = serverip;
	}
	public String getApplication() {
		return application;
	}
	public void setApplication(String application) {
		this.application = application;
	}
	public String getSummon_dt() {
		return summon_dt;
	}
	public void setSummon_dt(String summon_dt) {
		this.summon_dt = summon_dt;
	}
	public String getExpect_dt() {
		return expect_dt;
	}
	public void setExpect_dt(String expect_dt) {
		this.expect_dt = expect_dt;
	}
	public String getSummon_req_emp() {
		return summon_req_emp;
	}
	public void setSummon_req_emp(String summon_req_emp) {
		this.summon_req_emp = summon_req_emp;
	}
	public String getDesc_dt() {
		return desc_dt;
	}
	public void setDesc_dt(String desc_dt) {
		this.desc_dt = desc_dt;
	}
	public String getDesc_seq() {
		return desc_seq;
	}
	public void setDesc_seq(String desc_seq) {
		this.desc_seq = desc_seq;
	}
	public String getSummon_yn() {
		return summon_yn;
	}
	public void setSummon_yn(String summon_yn) {
		this.summon_yn = summon_yn;
	}
	public String getDesc_status() {
		return desc_status;
	}
	public void setDesc_status(String desc_status) {
		this.desc_status = desc_status;
	}
	public String getDesc_result() {
		return desc_result;
	}
	public void setDesc_result(String desc_result) {
		this.desc_result = desc_result;
	}
	public String getHc_rule_nm() {
		return hc_rule_nm;
	}
	public void setHc_rule_nm(String hc_rule_nm) {
		this.hc_rule_nm = hc_rule_nm;
	}
	public String getSummon_seq() {
		return summon_seq;
	}
	public void setSummon_seq(String summon_seq) {
		this.summon_seq = summon_seq;
	}
	public String getHc_send_dt() {
		return hc_send_dt;
	}
	public void setHc_send_dt(String hc_send_dt) {
		this.hc_send_dt = hc_send_dt;
	}
	public String getDecision_dt() {
		return decision_dt;
	}
	public void setDecision_dt(String decision_dt) {
		this.decision_dt = decision_dt;
	}
	public String getSsn_name() {
		return ssn_name;
	}
	public void setSsn_name(String ssn_name) {
		this.ssn_name = ssn_name;
	}
	public String getPosit_gu() {
		return posit_gu;
	}
	public void setPosit_gu(String posit_gu) {
		this.posit_gu = posit_gu;
	}
	public String getAcc_org_nm() {
		return acc_org_nm;
	}
	public void setAcc_org_nm(String acc_org_nm) {
		this.acc_org_nm = acc_org_nm;
	}
	public void setEmail_address(String email_address) {
		this.email_address = email_address;
	}
	public String getCheck_exc() {
		return check_exc;
	}
	public void setCheck_exc(String check_exc) {
		this.check_exc = check_exc;
	}
	public long getBiz_log_result_seq() {
		return biz_log_result_seq;
	}
	public void setBiz_log_result_seq(long biz_log_result_seq) {
		this.biz_log_result_seq = biz_log_result_seq;
	}
	public int getPrivacy_seq() {
		return privacy_seq;
	}
	public void setPrivacy_seq(int privacy_seq) {
		this.privacy_seq = privacy_seq;
	}
	/**
	 * 토폴로지용
	 */
	private String starthm;
	private String endhm;
	
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getResult_content() {
		return result_content;
	}
	public void setResult_content(String result_content) {
		this.result_content = result_content;
	}
	public String getResult_content_masking() {
		return result_content_masking;
	}
	public void setResult_content_masking(String result_content_masking) {
		this.result_content_masking = result_content_masking;
	}
	public String getSystem_name() {
		return system_name;
	}
	public void setSystem_name(String system_name) {
		this.system_name = system_name;
	}
	public String getPrivacy_desc() {
		return privacy_desc;
	}
	public void setPrivacy_desc(String privacy_desc) {
		this.privacy_desc = privacy_desc;
	}
	public PageInfo getPageInfo() {
		return pageInfo;
	}
	public void setPageInfo(PageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}
	public String getProc_date() {
		return proc_date;
	}
	public void setProc_date(String proc_date) {
		this.proc_date = proc_date;
	}
	public String getProc_time() {
		return proc_time;
	}
	public void setProc_time(String proc_time) {
		this.proc_time = proc_time;
	}
	public String getEmp_user_id() {
		return emp_user_id;
	}
	public void setEmp_user_id(String emp_user_id) {
		this.emp_user_id = emp_user_id;
	}
	public String getEmp_user_name() {
		return emp_user_name;
	}
	public void setEmp_user_name(String emp_user_name) {
		this.emp_user_name = emp_user_name;
	}
	public String getDept_id() {
		return dept_id;
	}
	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}
	public String getDept_name() {
		return dept_name;
	}
	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}
	public String getUser_ip() {
		return user_ip;
	}
	public void setUser_ip(String user_ip) {
		this.user_ip = user_ip;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getScrn_id() {
		return scrn_id;
	}
	public void setScrn_id(String scrn_id) {
		this.scrn_id = scrn_id;
	}
	public String getScrn_name() {
		return scrn_name;
	}
	public void setScrn_name(String scrn_name) {
		this.scrn_name = scrn_name;
	}
	public String getReq_type() {
		return req_type;
	}
	public void setReq_type(String req_type) {
		this.req_type = req_type;
	}
	public String getSystem_seq() {
		return system_seq;
	}
	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}
	public String getServer_seq() {
		return server_seq;
	}
	public void setServer_seq(String server_seq) {
		this.server_seq = server_seq;
	}
	public String getReq_context() {
		return req_context;
	}
	public void setReq_context(String req_context) {
		this.req_context = req_context;
	}
	public String getReq_url() {
		return req_url;
	}
	public void setReq_url(String req_url) {
		this.req_url = req_url;
	}
	public String getReq_end_time() {
		return req_end_time;
	}
	public void setReq_end_time(String req_end_time) {
		this.req_end_time = req_end_time;
	}
	public String getResult_type() {
		return result_type;
	}
	public void setResult_type(String result_type) {
		this.result_type = result_type;
	}
	public long getLog_seq() {
		return log_seq;
	}
	public void setLog_seq(long log_seq) {
		this.log_seq = log_seq;
	}
	public String getPrivacyType() {
		return privacyType;
	}
	public void setPrivacyType(String privacyType) {
		this.privacyType = privacyType;
	}
	public String getPrivacy() {
		return privacy;
	}
	public void setPrivacy(String privacy) {
		this.privacy = privacy;
	}
	public String getInserter_id() {
		return inserter_id;
	}
	public void setInserter_id(String inserter_id) {
		this.inserter_id = inserter_id;
	}
	public Timestamp getInserter_date() {
		return inserter_date;
	}
	public void setInserter_date(Timestamp inserter_date) {
		this.inserter_date = inserter_date;
	}
	public String getStarthm() {
		return starthm;
	}
	public void setStarthm(String starthm) {
		this.starthm = starthm;
	}
	public String getEndhm() {
		return endhm;
	}
	public void setEndhm(String endhm) {
		this.endhm = endhm;
	}
	public String getSystem_count() {
		return system_count;
	}
	public void setSystem_count(String system_count) {
		this.system_count = system_count;
	}
	public int getTime_count() {
		return time_count;
	}
	public void setTime_count(int time_count) {
		this.time_count = time_count;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	public String getResult_type_sort() {
		return result_type_sort;
	}
	public void setResult_type_sort(String result_type_sort) {
		this.result_type_sort = result_type_sort;
	}
	public String getSellertool() {
		return sellertool;
	}
	public void setSellertool(String sellertool) {
		this.sellertool = sellertool;
	}
	public String getPrivacy_masking() {
		return privacy_masking;
	}
	public void setPrivacy_masking(String privacy_masking) {
		this.privacy_masking = privacy_masking;
	}
	public int getCnt() {
		return cnt;
	}
	public void setCnt(int cnt) {
		this.cnt = cnt;
	}
	public int getCnt2() {
		return cnt2;
	}
	public void setCnt2(int cnt2) {
		this.cnt2 = cnt2;
	}
	public String getReq_sql() {
		return req_sql;
	}
	public void setReq_sql(String req_sql) {
		this.req_sql = req_sql;
	}
	public String getRes_sql() {
		return res_sql;
	}
	public void setRes_sql(String res_sql) {
		this.res_sql = res_sql;
	}
	
	public int getData1() {
		return data1;
	}
	public void setData1(int data1) {
		this.data1 = data1;
	}
	public int getData2() {
		return data2;
	}
	public void setData2(int data2) {
		this.data2 = data2;
	}
	public int getData3() {
		return data3;
	}
	public void setData3(int data3) {
		this.data3 = data3;
	}
	public int getData4() {
		return data4;
	}
	public void setData4(int data4) {
		this.data4 = data4;
	}
	public int getData5() {
		return data5;
	}
	public void setData5(int data5) {
		this.data5 = data5;
	}
	public int getData6() {
		return data6;
	}
	public void setData6(int data6) {
		this.data6 = data6;
	}
	public int getData7() {
		return data7;
	}
	public void setData7(int data7) {
		this.data7 = data7;
	}
	public int getData8() {
		return data8;
	}
	public void setData8(int data8) {
		this.data8 = data8;
	}
	public int getData9() {
		return data9;
	}
	public void setData9(int data9) {
		this.data9 = data9;
	}
	public int getData10() {
		return data10;
	}
	public void setData10(int data10) {
		this.data10 = data10;
	}
	public int getData11() {
		return data11;
	}
	public void setData11(int data11) {
		this.data11 = data11;
	}
	public int getData12() {
		return data12;
	}
	public void setData12(int data12) {
		this.data12 = data12;
	}
	public int getData13() {
		return data13;
	}
	public void setData13(int data13) {
		this.data13 = data13;
	}
	public int getData14() {
		return data14;
	}
	public void setData14(int data14) {
		this.data14 = data14;
	}
	public int getData15() {
		return data15;
	}
	public void setData15(int data15) {
		this.data15 = data15;
	}
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public String getFile_ext() {
		return file_ext;
	}
	public void setFile_ext(String file_ext) {
		this.file_ext = file_ext;
	}
	public String getFile_direction() {
		return file_direction;
	}
	public void setFile_direction(String file_direction) {
		this.file_direction = file_direction;
	}
	public String getFile_path() {
		return file_path;
	}
	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}
	public String getDetection_src() {
		return detection_src;
	}
	public void setDetection_src(String detection_src) {
		this.detection_src = detection_src;
	}
	public String getReq_body() {
		return req_body;
	}
	public void setReq_body(String req_body) {
		this.req_body = req_body;
	}
	public String getResp_body() {
		return resp_body;
	}
	public void setResp_body(String resp_body) {
		this.resp_body = resp_body;
	}
	public String getLog_delimiter() {
		return log_delimiter;
	}
	public void setLog_delimiter(String log_delimiter) {
		this.log_delimiter = log_delimiter;
	}
	public String getRegister_status() {
		return register_status;
	}
	public void setRegister_status(String register_status) {
		this.register_status = register_status;
	}
	public String getThreshold() {
		return threshold;
	}
	public void setThreshold(String threshold) {
		this.threshold = threshold;
	}
	public String getThreshold_use() {
		return threshold_use;
	}
	public void setThreshold_use(String threshold_use) {
		this.threshold_use = threshold_use;
	}
	public String getThreshold_total_inbox() {
		return threshold_total_inbox;
	}
	public void setThreshold_total_inbox(String threshold_total_inbox) {
		this.threshold_total_inbox = threshold_total_inbox;
	}
	
	public void setResultTypeMap(String resultType) {
		if(resultType == null)return;
		this.resultTypeMap = new HashMap<String, String>();
		int newResultTypeCheck = resultType.indexOf(":");
		String resultArr[] = resultType.split(",");
		if(newResultTypeCheck<1) {
			for (String string : resultArr) {
				try {
					this.getClass().getMethod("countType"+Integer.parseInt(string)).invoke(this);
				} catch (Exception e) {
					e.printStackTrace();
				} 
			}
		}else {
			for (String string : resultArr) {
				String typeValue[] = string.split(":");
				if(typeValue.length<2) continue;
				try {
					this.getClass().getMethod("setType"+Integer.parseInt(typeValue[0]),int.class).invoke(this,Integer.parseInt(typeValue[1]));
				} catch (Exception e) {
					e.printStackTrace();
				} 
			}
		}
		for (int i = 1; i <= 20; i++) {
			try {
				Integer resultCount = (Integer) this.getClass().getMethod("getType"+i).invoke(this);
				if(resultCount>0) {
					resultTypeMap.put(String.valueOf(i), String.valueOf(resultCount));
				}
			} catch (Exception e) {
				e.printStackTrace();
			} 
		}

		//지자체 지방세 99번 별도로 추가
			try {
				Integer resultCount = (Integer) this.getClass().getMethod("getType"+99).invoke(this);
				if(resultCount>0) {
					resultTypeMap.put(String.valueOf(99), String.valueOf(resultCount));
				}
			} catch (Exception e) {
				e.printStackTrace();
			} 
	
	}
	
	public Map<String,String> getResultTypeMap(){
		return this.resultTypeMap;
	}
	public String getCrud_type() {
		return crud_type;
	}
	public void setCrud_type(String crud_type) {
		this.crud_type = crud_type;
	}
	public int getData16() {
		return data16;
	}
	public void setData16(int data16) {
		this.data16 = data16;
	}
	public int getData17() {
		return data17;
	}
	public void setData17(int data17) {
		this.data17 = data17;
	}
	public int getData18() {
		return data18;
	}
	public void setData18(int data18) {
		this.data18 = data18;
	}
	public int getData19() {
		return data19;
	}
	public void setData19(int data19) {
		this.data19 = data19;
	}
	public int getData20() {
		return data20;
	}
	public void setData20(int data20) {
		this.data20 = data20;
	}
	public int getData99() {
		return data99;
	}
	public void setData99(int data99) {
		this.data99 = data99;
	}
	public void setResultTypeMap(Map<String, String> resultTypeMap) {
		this.resultTypeMap = resultTypeMap;
	}
}
