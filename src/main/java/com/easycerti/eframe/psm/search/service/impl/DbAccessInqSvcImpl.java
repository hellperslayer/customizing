package com.easycerti.eframe.psm.search.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.search.dao.DbAccessInqDao;
import com.easycerti.eframe.psm.search.service.DbAccessInqSvc;
import com.easycerti.eframe.psm.search.vo.DbAccessInq;
import com.easycerti.eframe.psm.search.vo.DbAccessInqList;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.system_management.dao.AgentMngtDao;
import com.easycerti.eframe.psm.system_management.dao.MenuMngtDao;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

/**
 * 
 * 설명 :  DB접근로그조회 Service Implements
 * @author tjlee
 * @since 2015. 4. 22.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 22.           tjlee            최초 생성
 *
 * </pre>
 */
@Service
public class DbAccessInqSvcImpl implements DbAccessInqSvc {

	@Autowired
	private DbAccessInqDao dbAccessInqDao;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;

	@Autowired
	private MenuMngtDao menuDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private AgentMngtDao agentMngtDao;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;

	
	@Override
	public DataModelAndView findDbAccessInqList(SearchSearch search) {
		
		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		search.setTotal_count(dbAccessInqDao.findDbAccessInqOne_count(search));
		
		List<DbAccessInq> dbAccessInq = dbAccessInqDao.findDbAccessInqList(search);
		DbAccessInqList dbAccessInqList = new DbAccessInqList(dbAccessInq);
		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList_Dbac();
		
		SimpleCode simpleCode = new SimpleCode();
		String index = "/dbAccessInq/list.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("dbAccessInqList", dbAccessInqList);
		modelAndView.addObject("systemMasterList", systemMasterList);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	
	@Override
	public DataModelAndView findDbAccessInqDetail(SearchSearch search) {
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		
		DbAccessInq dbAccessInq = dbAccessInqDao.findDbAccessInqOne(search);
		List<DbAccessInq> dbAccessInqSqlList = dbAccessInqDao.findDbAccessInqDetail(search);
		//List<DbAccessInq> dbAccessInqResultList = dbAccessInqDao.findDbAccessInqResult(search);
		
		/*
		 * String[] arrResult = dbAccessInq.getResult_type().split(",");
		 * if(arrResult.length > 0) { List<DbAccessInq> columnInfoList = new
		 * ArrayList<>(); for (String tmp : arrResult) { if (!"*".equals(tmp)) {
		 * DbAccessInq col = dbAccessInqDao.findColumnInfo(tmp);
		 * columnInfoList.add(col); } } modelAndView.addObject("columnInfoList",
		 * columnInfoList); }
		 */
		
		for(DbAccessInq dbac : dbAccessInqSqlList) {
			String reqsql = dbac.getReqsql().replaceAll("\"", "\'");
			dbac.setReqsql(reqsql);
		}
		SimpleCode simpleCode = new SimpleCode();
		String index = "/dbAccessInq/detail.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		modelAndView.addObject("dbAccessInq", dbAccessInq);
		modelAndView.addObject("dbAccessInqSqlList", dbAccessInqSqlList);
		//modelAndView.addObject("dbAccessInqResultList", dbAccessInqResultList);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}


	@Override
	public void findDbAccessInqList_download(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request) {
		
		search.setUseExcel("true");
		
		String fileName = "PSM_DB접근로그조회_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "DB접근로그조회";
		
		List<DbAccessInq> dbAccessInq = dbAccessInqDao.findDbAccessInqList(search);
		
		SimpleDateFormat parseSdf = new SimpleDateFormat("yyyyMMddHHmmss");
		SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
		
		for(int i=0; i<dbAccessInq.size(); i++){
			try {
				dbAccessInq.get(i).setProc_date(sdf.format(parseSdf.parse(dbAccessInq.get(i).getProc_date() + dbAccessInq.get(i).getProc_time())));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		String[] columns = new String[] {"proc_date", "emp_user_id", "emp_user_name", "dept_name" , "user_ip", "system_name", "result_type", "reqsql"};
		String[] heads = new String[] {"처리일시", "사용자ID", "사용자명", "소속", "사용자IP", "시스템", "개인정보유형", "REQSQL"};
		
		/*String[] columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "req_context"};
		String[] heads = new String[] {"처리일시", "소속", "사용자ID", "사용자명", "사용자IP", "시스템", "COMMAND"};*/
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", dbAccessInq);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}
}
