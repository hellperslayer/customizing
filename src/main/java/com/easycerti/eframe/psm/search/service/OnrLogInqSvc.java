package com.easycerti.eframe.psm.search.service;

import javax.servlet.http.HttpServletRequest;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.search.vo.SearchSearch;

public interface OnrLogInqSvc {

	public DataModelAndView findOnrLogInqList(SearchSearch search);
	
	public DataModelAndView findOnrLogInqDetail(SearchSearch search);
	
	public void findOnrLogInqList_download(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);
	public void findOnrLogInqList_downloadCSV(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);
}
