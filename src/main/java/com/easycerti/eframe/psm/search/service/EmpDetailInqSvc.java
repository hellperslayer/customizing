package com.easycerti.eframe.psm.search.service;

import javax.servlet.http.HttpServletRequest;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
/**
 * 
 * 설명 : 위험도별조회 Service Interface
 * @author tjlee
 * @since 2015. 4. 22.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 22.           tjlee            최초 생성
 *
 * </pre>
 */
public interface EmpDetailInqSvc {

	/**
	 * 설명 : 위험도별조회 리스트 
	 * @author tjlee
	 * @since 2015. 6. 8.
	 * @return DataModelAndView
	 */
	public DataModelAndView findEmpDetailInqList(SearchSearch search);
	
	/**
	 * 설명 : 위험도별조회 상세 
	 * @author tjlee
	 * @since 2015. 6. 8.
	 * @return DataModelAndView
	 */
	public DataModelAndView findEmpDetailInqDetail(SearchSearch search);
	
	/**
	 * 설명 : 위험도별조회 엑셀 다운로드 
	 * @author tjlee
	 * @since 2015. 6. 8.
	 * @return void
	 */
	public void findEmpDetailInqList_download(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);
	
}
