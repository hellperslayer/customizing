package com.easycerti.eframe.psm.statistics.dao;

import java.util.List;
import java.util.Map;

import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.search.vo.AllLogInq;
import com.easycerti.eframe.psm.search.vo.PatchHist;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.statistics.vo.Statistics;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

/**
 * 통계 Dao
 * @author yrchoo
 * @since 2015. 4. 21.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 21.           yrchoo            최초 생성
 *
 * </pre>
 */
public interface StatisticsDao {
	
	/**
	 * 기간별 유형분석 카운트
	 * @author yrchoo
	 * @since 2015. 4. 21.
	 * @return Integer
	 */
	public Integer findPerdbyTypeAnalsOne_count(Statistics statistics);
	
	/**
	 * 기간별 유형분석 리스트
	 * @author yrchoo
	 * @since 2015. 4. 21.
	 * @return List<Statistics>
	 */
	public List<Statistics> findPerdbyTypeAnalsList(Statistics statistics);
	public List<Statistics> findPerdbyTypeAnalsList_byStudentId(Statistics statistics);
	public List<Statistics> findPerdbyDownTypeAnalsList(Statistics statistics);
	
	/**
	 * 시스템별 유형분석 카운트
	 * @param statistics
	 * @return
	 */
	public Integer findSysbyTypeAnalsOne_count(Statistics statistics);
	
	/**
	 * 시스템별 유형분석 리스트
	 * @author yrchoo
	 * @since 2015. 4. 21.
	 * @return List<Statistics>
	 */
	public List<Statistics> findSysbyTypeAnalsList(Statistics statistics);
	public List<Statistics> findSysbyTypeAnalsList_new(Statistics statistics);
	public List<Statistics> findSysbyDownloadTypeAnalsList(Statistics statistics);
	public List<Statistics> findSysbyTypeAnalsList_byStudentId(Statistics statistics);
	/**
	 * 부서별 유형분석 카운트
	 * @author yrchoo
	 * @since 2015. 4. 21.
	 * @return Integer
	 */
	public Integer findDeptbyTypeAnalsOne_count(Statistics statistics);
	
	/**
	 * 부서별 유형분석 리스트
	 * @author yrchoo
	 * @since 2015. 4. 21.
	 * @return List<Statistics>
	 */
	public List<Statistics> findDeptbyTypeAnalsList(Statistics statistics);
	public List<Statistics> findDeptbyTypeAnalsList_new(Statistics statistics);
	public List<Statistics> findDeptbyDownloadTypeAnalsList(Statistics statistics);
	public List<Statistics> findDeptbyTypeAnalsList_byStudentId(Statistics statistics);
	
	/**
	 * 개인별 유형분석 카운트
	 * @author yrchoo
	 * @since 2015. 5. 7.
	 * @return Integer
	 */
	public Integer findIndvbyTypeAnalsOne_count(Statistics statistics);
	public Integer findIndvbyDownloadTypeAnalsOne_count(Statistics statistics);
	
	/**
	 * 개인별 유형분석 리스트
	 * @author yrchoo
	 * @since 2015. 5. 7.
	 * @return List<Statistics>
	 */
	public List<Statistics> findIndvbyTypeAnalsList(Statistics statistics);
	public List<Statistics> findIndvbyTypeAnalsList_new(Statistics statistics);
	public List<Statistics> findIndvbyDownloadTypeAnalsList(Statistics statistics);
	public List<Statistics> findIndvbyTypeAnalsList_byStudentId(Statistics statistics);

	/**
	 * 개인정보별 유형별 합계
	 * @author yrchoo
	 * @since 2015. 4. 21.
	 * @return Statistics
	 */
	public Statistics findStatisticsSumByPrivacyType(Statistics statistics);
	public Statistics findStatisticsSumByDownloadType(Statistics statistics);
	public Statistics findStatisticsSumByPrivacyType_byStudentId(Statistics statistics);
	
	//개인별 위험도 리스트
	public List<Statistics> findIndvbyDngValAnalsList(Statistics statistics);
	public Integer findIndvbyDngValAnalsList_count(Statistics statistics);
	
	public List<Statistics> findRegularInspection(Statistics statistics);
	public List<Map<String,String>> findInspectionTable(Statistics statistics);
	
	public List<Statistics> findInspectionHistList(Statistics statistics);
	public int findInspectionHistList_count(Statistics statistics);
	public Statistics findInspectionHistDetail(Statistics statistics);
	public String getInspectionHistPrevContents(int seq);
	public String getInspectionHistRecentContents();
	public void addInspectionHist(Statistics statistics);
	public void saveInspectionHist(Statistics statistics);
	public int checkInspectionHist(Statistics statistics);
	public String getInspectionHistMaxNextDate();
	
	
	public List<AllLogInq> connectLogResultList(SearchSearch search);
	public Integer connectLogResultListCt(SearchSearch search);
	public List<AllLogInq> downloadLogResultList(SearchSearch search);
	public Integer downloadLogResultListCt(SearchSearch search);
	public String privacyTypeName(String privacyType);
	
	public List<SystemMaster> findSystemListByAdmin(Statistics statistics);
	
	public List<Map<String, String>> findPerdbyStatisticsList(Statistics statistics);
	public Map<String, String> perdbyStatisticsListSum(Statistics statistics);
	public List<Map<String, String>> perdbyStatisticsProcDateSum(Statistics statistics);
	
	public Map<String, String> perdbyStatisticsSum(Statistics statistics);

	public List<PatchHist> findPatchHistList(Statistics paramBean);

	public void addPatchHistOne(String version);
	public List<String> findReportReqTypeList(Statistics paramBean);

	public List<Map<String, String>> findReportReqTypeCountList(Statistics paramBean);
}
