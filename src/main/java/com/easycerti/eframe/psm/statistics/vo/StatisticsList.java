package com.easycerti.eframe.psm.statistics.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 통계관리 리스트 Bean
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 * 
 *
 * </pre>
 */
public class StatisticsList extends AbstractValueObject {

	private List<Statistics> statistics = null;
	
	private int type1_max;
	private int type2_max;
	private int type3_max;
	private int type4_max;
	private int type5_max;
	private int type6_max;
	private int type7_max;
	private int type8_max;
	private int type9_max;
	private int type10_max;
	private int type11_max;
	private int type12_max;
	private int type13_max;
	private int type14_max;
	private int type15_max;
	private int type16_max;
	private int type17_max;
	private int type18_max;
	private int type_sum_max;
	public StatisticsList(){
		
	}
	
	public StatisticsList(List<Statistics> statistics){
		this.statistics = statistics;
	}
	
	public StatisticsList(List<Statistics> statistics, String page_total_count){
		this.statistics = statistics;
		setPage_total_count(page_total_count);
	}
	
	public List<Statistics> getStatistics() {
		return statistics;
	}

	public void setStatistics(List<Statistics> statistics) {
		this.statistics = statistics;
	}

	public int getType1_max() {
		return type1_max;
	}

	public void setType1_max(int type1_max) {
		this.type1_max = type1_max;
	}

	public int getType2_max() {
		return type2_max;
	}

	public void setType2_max(int type2_max) {
		this.type2_max = type2_max;
	}

	public int getType3_max() {
		return type3_max;
	}

	public void setType3_max(int type3_max) {
		this.type3_max = type3_max;
	}

	public int getType4_max() {
		return type4_max;
	}

	public void setType4_max(int type4_max) {
		this.type4_max = type4_max;
	}

	public int getType5_max() {
		return type5_max;
	}

	public void setType5_max(int type5_max) {
		this.type5_max = type5_max;
	}

	public int getType6_max() {
		return type6_max;
	}

	public void setType6_max(int type6_max) {
		this.type6_max = type6_max;
	}

	public int getType7_max() {
		return type7_max;
	}

	public void setType7_max(int type7_max) {
		this.type7_max = type7_max;
	}

	public int getType8_max() {
		return type8_max;
	}

	public void setType8_max(int type8_max) {
		this.type8_max = type8_max;
	}

	public int getType9_max() {
		return type9_max;
	}

	public void setType9_max(int type9_max) {
		this.type9_max = type9_max;
	}

	public int getType10_max() {
		return type10_max;
	}

	public void setType10_max(int type10_max) {
		this.type10_max = type10_max;
	}

	public int getType11_max() {
		return type11_max;
	}

	public void setType11_max(int type11_max) {
		this.type11_max = type11_max;
	}

	public int getType12_max() {
		return type12_max;
	}

	public void setType12_max(int type12_max) {
		this.type12_max = type12_max;
	}

	public int getType13_max() {
		return type13_max;
	}

	public void setType13_max(int type13_max) {
		this.type13_max = type13_max;
	}

	public int getType14_max() {
		return type14_max;
	}

	public void setType14_max(int type14_max) {
		this.type14_max = type14_max;
	}
	
	public int getType15_max() {
		return type15_max;
	}

	public void setType15_max(int type15_max) {
		this.type15_max = type15_max;
	}
	
	public int getType16_max() {
		return type16_max;
	}

	public void setType16_max(int type16_max) {
		this.type16_max = type16_max;
	}

	public int getType17_max() {
		return type17_max;
	}

	public void setType17_max(int type17_max) {
		this.type17_max = type17_max;
	}

	public int getType18_max() {
		return type18_max;
	}

	public void setType18_max(int type18_max) {
		this.type18_max = type18_max;
	}

	public int getType_sum_max() {
		return type_sum_max;
	}

	public void setType_sum_max(int type_sum_max) {
		this.type_sum_max = type_sum_max;
	}
	
	
}
