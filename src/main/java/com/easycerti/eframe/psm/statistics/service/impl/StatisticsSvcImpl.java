package com.easycerti.eframe.psm.statistics.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionStatus;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.PageInfo;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.report.dao.ReportDao;
import com.easycerti.eframe.psm.report.vo.InspectionReport;
import com.easycerti.eframe.psm.search.dao.AllLogInqDao;
import com.easycerti.eframe.psm.search.vo.AllLogInq;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.statistics.dao.StatisticsDao;
import com.easycerti.eframe.psm.statistics.service.StatisticsSvc;
import com.easycerti.eframe.psm.statistics.vo.Statistics;
import com.easycerti.eframe.psm.statistics.vo.StatisticsList;
import com.easycerti.eframe.psm.system_management.dao.AgentMngtDao;
import com.easycerti.eframe.psm.system_management.dao.MenuMngtDao;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

/**
 * 통계 Service Implements
 * 
 * @author yrchoo
 * @since 2015. 4. 21.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 21.           yrchoo            최초 생성
 *
 *      </pre>
 */
@Repository
public class StatisticsSvcImpl implements StatisticsSvc {

	@Autowired
	private StatisticsDao statisticsDao;

	@Autowired
	private MenuMngtDao menuDao;

	@Autowired
	private AgentMngtDao agentMngtDao;

	@Autowired
	private DataSourceTransactionManager transactionManager;

	@Autowired
	private CommonDao commonDao;

	@Autowired
	private AllLogInqDao allLogInqDao;

	@Autowired
	private ReportDao reportDao;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	
	
	@Override
	public DataModelAndView findPerdbyStatisticsList(Map<String, String> parameters, HttpServletRequest request)
			throws ParseException {
		DataModelAndView modelAndView = new DataModelAndView();
		SimpleCode simpleCode = new SimpleCode("/statistics/perdbyStatisticsList.html");
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		
		HttpSession session = request.getSession();
		// 다운로드로그 풀스캔연동여부
		String use_fullscan = (String) session.getAttribute("use_fullscan");
		String searchType = (String) request.getParameter("searchType");
		
		Statistics paramBean = CommonHelper.convertMapToBean(getSearchDateByChkDateType(parameters), Statistics.class);
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	list.add("");	// 시스템코드가 없는 경우 (데이터가 존재하지 않는 날짜에 대한 log)
	    } else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    }
	    for(int i=0; i<list.size();i++) {
	    	String str = list.get(i);
	    	if(str==null || str=="") {
	    		list.remove(i);
	    	}
	    }
	    paramBean.setAuth_idsList(list);
	    if(searchType == null||searchType.equals("01")) {
	    	paramBean.setLog_delimiter("BA");
	    	searchType = "01";
	    } else if(searchType.equals("02")) {
	    	paramBean.setLog_delimiter("BS");
	    } else {
	    	paramBean.setLog_delimiter("DN");
	    }
		List<Map<String, String>> statistics = statisticsDao.findPerdbyStatisticsList(paramBean);
		Integer mapCt = 0;
		if(statistics!=null && statistics.size()>0) {
			mapCt = statistics.get(0).size()-2;
			modelAndView.addObject("mapCt", mapCt);
		}
		
		List<Map<String, String>> procSum = statisticsDao.perdbyStatisticsProcDateSum(paramBean);
		for(int i=0; i<statistics.size();i++) {
			Map<String, String> map = statistics.get(i);
			String proc_date = map.get("proc_date");
			for(int j=0; j<procSum.size();j++) {
				Map<String, String> procMap = procSum.get(j);
				String proc_date_temp = procMap.get("proc_date");
				if(proc_date.equals(proc_date_temp)) {
					procMap.remove("proc_date");
					map.putAll(procMap);
					break;
				}
			}
		}
		mapCt = mapCt+1;
		modelAndView.addObject("statistics", statistics);
		Map<String, Integer> maxMap = perdbyStatisticsListMaxValue(statistics, mapCt);
		modelAndView.addObject("maxMap", maxMap);
		
		List<SystemMaster> systemList = statisticsDao.findSystemListByAdmin(paramBean);
		modelAndView.addObject("systemList", systemList);
		modelAndView.addObject("systemSeq", list);
		
		Map<String, String> statisticsSum = statisticsDao.perdbyStatisticsListSum(paramBean);
		modelAndView.addObject("statisticsSum", statisticsSum);
		
		modelAndView.addObject("paramBean", paramBean);
		modelAndView.addObject("use_fullscan", use_fullscan);
		modelAndView.addObject("searchType", searchType);

		return modelAndView;
	}
	
	public Map<String, Integer> perdbyStatisticsListMaxValue(List<Map<String, String>> statistics, int mapCt) {
		Map<String, Integer> maxMap = new HashMap<>();
		if(statistics.size()>0) {
			for(int i=0; i<mapCt;i++) {
				List<Integer> list = new ArrayList<>();
				String key = "a"+i;
				
					
				
				for(Map<String, String> map : statistics) {
					String temp = String.valueOf(map.get(key));
					list.add(Integer.parseInt(temp));
				}
				maxMap.put(key, Collections.max(list));
			}
		}
		List<Integer> list = new ArrayList<>();
		String key = "logcnt";
		if(statistics.size()>0) {
			for(Map<String, String> map : statistics) {
				String temp = String.valueOf(map.get(key));
				list.add(Integer.parseInt(temp));
			}
			maxMap.put(key, Collections.max(list));
		}
		return maxMap;
	}

	@Override
	public void perdSystemdownload(DataModelAndView modelAndView, Map<String, String> parameters,
			HttpServletRequest request) {
		HttpSession session = request.getSession();
		String searchType = (String) request.getParameter("searchType");
		
		Statistics paramBean = CommonHelper.convertMapToBean(getSearchDateByChkDateType(parameters), Statistics.class);
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	list.add("");	// 시스템코드가 없는 경우 (데이터가 존재하지 않는 날짜에 대한 log)
	    } else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    }
	    for(int i=0; i<list.size();i++) {
	    	String str = list.get(i);
	    	if(str==null || str=="") {
	    		list.remove(i);
	    	}
	    }
	    paramBean.setAuth_idsList(list);
	    String fileName = "";
	    String sheetName = "";
	    if(searchType == null||searchType.equals("01")) {
	    	paramBean.setLog_delimiter("BA");
	    	searchType = "01";
	    	fileName = "PSM_기간별시스템별처리량(접속기록)_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
			sheetName = "기간별시스템별처리량(접속기록)";
	    } else if(searchType.equals("02")) {
	    	paramBean.setLog_delimiter("BS");
	    	fileName = "PSM_기간별시스템별처리량(시스템로그인)_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
			sheetName = "기간별시스템별처리량(시스템로그인)";
	    } else {
	    	paramBean.setLog_delimiter("DN");
	    	fileName = "PSM_기간별시스템별처리량(다운로드)_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
			sheetName = "기간별시스템별처리량(다운로드)";
	    }
		List<Map<String, String>> statistics = statisticsDao.findPerdbyStatisticsList(paramBean);
		
		List<Map<String, String>> procSum = statisticsDao.perdbyStatisticsProcDateSum(paramBean);
		
		for(int i=0; i<statistics.size();i++) {
			Map<String, String> map = statistics.get(i);
			String proc_date = map.get("proc_date");
			for(int j=0; j<procSum.size();j++) {
				Map<String, String> procMap = procSum.get(j);
				String proc_date_temp = procMap.get("proc_date");
				if(proc_date.equals(proc_date_temp)) {
					procMap.remove("proc_date");
					map.putAll(procMap);
					break;
				}
			}
		}
		Integer mapCt = 0;
		if(statistics!=null && statistics.size()>0) {
			mapCt = statistics.get(0).size();
		}
		Map<String, String> totalMap = statisticsDao.perdbyStatisticsSum(paramBean);
		Map<String, String> statisticsSum = statisticsDao.perdbyStatisticsListSum(paramBean);
		statisticsSum.put("proc_date", "합계");
		statisticsSum.putAll(totalMap);
		statistics.add(statisticsSum);
		
		List<SystemMaster> systemList = statisticsDao.findSystemListByAdmin(paramBean);
		
		String[] columns = new String[mapCt];
		columns[0] = "proc_date";
		for(int i=1; i<mapCt-1 ;i++) {
			int index = i-1;
			String column = "a"+index;
			columns[i] = column;
		}
		columns[mapCt-1] = "logcnt";
		
		String[] heads = new String[mapCt];
		heads[0] = "날짜";
		for(int i=1; i<mapCt-1 ;i++) {
			heads[i] = systemList.get(i-1).getSystem_name();
		}
		heads[mapCt-1] = "합계";
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", statistics);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
		
	}

	@Override
	public DataModelAndView findPerdbyTypeAnalsList(Map<String, String> parameters, HttpServletRequest request) throws ParseException {
		 PageInfo pageInfo = new PageInfo(parameters, defaultPageSize);

		HttpSession session = request.getSession();
		// 다운로드로그 풀스캔연동여부
		String use_fullscan = (String) session.getAttribute("use_fullscan");
		String searchType = (String) request.getParameter("searchType");
		
		Statistics paramBean = CommonHelper.convertMapToBean(getSearchDateByChkDateType(parameters), Statistics.class);
		// paramBean.setPageInfo(pageInfo);

		// Integer totalCount = statisticsDao.findPerdbyTypeAnalsOne_count(paramBean);
		// pageInfo.setTotal_count(totalCount);
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	list.add("");	// 시스템코드가 없는 경우 (데이터가 존재하지 않는 날짜에 대한 log)
	    	paramBean.setAuth_idsList(list);
	    } else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	paramBean.setAuth_idsList(list);
	    }

		List<Statistics> statistics = new ArrayList<>();
		Statistics statisticsSum = new Statistics();
		String use_studentId = parameters.get("use_studentId");
//		List<PrivacyType> privTypeLst = new ArrayList<>();
		List<Code> resultTypes = commonDao.getPrivacyDescList();
		
		if(searchType == null) {
			searchType="01";
			paramBean.setSearchType("01");
		}
		if (searchType != null && searchType.equals("03")) { // 다운로드로그 조회
			statistics = statisticsDao.findPerdbyDownTypeAnalsList(paramBean);
			statisticsSum = statisticsDao.findStatisticsSumByDownloadType(paramBean);
		} else { // 접속기록 조회
			/*if (use_studentId.equals("yes")) {
				statistics = statisticsDao.findPerdbyTypeAnalsList_byStudentId(paramBean);
				statisticsSum = statisticsDao.findStatisticsSumByPrivacyType_byStudentId(paramBean);
			} else {*/
				statistics = statisticsDao.findPerdbyTypeAnalsList(paramBean);
				statisticsSum = statisticsDao.findStatisticsSumByPrivacyType(paramBean);
			/*}*/
		}
		
		StatisticsList statisticsList = new StatisticsList(statistics);
		
		// 개인정보 유형별 최대값
		if (statistics.size() != 0) {
			if (use_studentId.equals("yes")) {
				getMaxByPrivacyType_byStudentId(statistics, statisticsList);
			} else {
				getMaxByPrivacyType(statistics, statisticsList);
			}
		}

		SimpleCode simpleCode = new SimpleCode("/statistics/list_perdbyTypeAnals.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		List<SystemMaster> systemList = statisticsDao.findSystemListByAdmin(paramBean);

		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("statisticsList", statisticsList);
		modelAndView.addObject("statisticsSum", statisticsSum);
		modelAndView.addObject("resultTypes", resultTypes);
		modelAndView.addObject("systemList", systemList);
//		modelAndView.addObject("resultTypes", privTypeLst);
		modelAndView.addObject("paramBean", paramBean);
		modelAndView.addObject("use_fullscan", use_fullscan);
		modelAndView.addObject("searchType", searchType);
		modelAndView.addObject("pageInfo", pageInfo);
		modelAndView.addObject("index_id", index_id);

		return modelAndView;
	}

	@Override
	public DataModelAndView findSysbyTypeAnalsList(Map<String, String> parameters, HttpServletRequest request)
			throws ParseException {
		// PageInfo pageInfo = new PageInfo(parameters, defaultPageSize);

		HttpSession session = request.getSession();
		// 다운로드로그 풀스캔연동여부
		String use_fullscan = (String) session.getAttribute("use_fullscan");
		String searchType = request.getParameter("searchType");

		Statistics paramBean = CommonHelper.convertMapToBean(getSearchDateByChkDateType(parameters), Statistics.class);

		List<Statistics> statistics = new ArrayList<>();
		Statistics statisticsSum = new Statistics();
		String use_studentId = parameters.get("use_studentId");
		List<Code> resultTypes = commonDao.getPrivacyDescList();
		
		SearchSearch search = new SearchSearch();
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	paramBean.setAuth_idsList(list);
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	paramBean.setAuth_idsList(list);
	    	search.setAuth_idsList(list);
	    }
		if(searchType == null) {
			searchType="01";
			paramBean.setSearchType("01");
		}
		if (searchType != null && searchType.equals("03")) { // 다운로드로그 조회
			statistics = statisticsDao.findSysbyDownloadTypeAnalsList(paramBean);
			statisticsSum = statisticsDao.findStatisticsSumByDownloadType(paramBean);
		} else {// 접속기록 조회
			/*if (use_studentId.equals("yes")) {
				statistics = statisticsDao.findSysbyTypeAnalsList_byStudentId(paramBean);
				statisticsSum = statisticsDao.findStatisticsSumByPrivacyType_byStudentId(paramBean);
				resultTypes = commonDao.getResultTypeList_byStudentId();
			} else {*/
				statistics = statisticsDao.findSysbyTypeAnalsList_new(paramBean);
				statisticsSum = statisticsDao.findStatisticsSumByPrivacyType(paramBean);
			/*}*/
		}
		StatisticsList statisticsList = new StatisticsList(statistics);

		// 개인정보 유형별 최대값
		if (statistics.size() != 0) {
			if (use_studentId.equals("yes"))
				getMaxByPrivacyType_byStudentId(statistics, statisticsList);
			else
				getMaxByPrivacyType(statistics, statisticsList);
		}

		SimpleCode simpleCode = new SimpleCode("/statistics/list_sysbyTypeAnals.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		List<SystemMaster> systemList = agentMngtDao.findSystemMasterList_byAdmin(search);

		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("statisticsList", statisticsList);
		modelAndView.addObject("statisticsSum", statisticsSum);
		modelAndView.addObject("resultTypes", resultTypes);
		modelAndView.addObject("paramBean", paramBean);
		// modelAndView.addObject("pageInfo", pageInfo);
		modelAndView.addObject("systemList", systemList);
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("use_fullscan", use_fullscan);
		modelAndView.addObject("searchType", searchType);

		return modelAndView;
	}

	@Override
	public DataModelAndView findDeptbyTypeAnalsList(Map<String, String> parameters, HttpServletRequest request) throws ParseException {
		// PageInfo pageInfo = new PageInfo(parameters, defaultPageSize);

		HttpSession session = request.getSession();
		// 다운로드로그 풀스캔연동여부
		String use_fullscan = (String) session.getAttribute("use_fullscan");
		String searchType = request.getParameter("searchType");

		Statistics paramBean = CommonHelper.convertMapToBean(getSearchDateByChkDateType(parameters), Statistics.class);

		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	paramBean.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	paramBean.setAuth_idsList(list);
	    }
		
		List<Statistics> statistics = new ArrayList<>();
		Statistics statisticsSum = new Statistics();
		String use_studentId = parameters.get("use_studentId");
		List<Code> resultTypes = commonDao.getPrivacyDescList();
		if(searchType==null) {
			searchType = "01";
			paramBean.setSearchType("01");
		}
		if (searchType != null && searchType.equals("03")) { // 다운로드로그 조회
			statistics = statisticsDao.findDeptbyDownloadTypeAnalsList(paramBean);
			statisticsSum = statisticsDao.findStatisticsSumByDownloadType(paramBean);

		} else { // 접속기록 조회
			/*if (use_studentId.equals("yes")) {
				statistics = statisticsDao.findDeptbyTypeAnalsList_byStudentId(paramBean);
				statisticsSum = statisticsDao.findStatisticsSumByPrivacyType_byStudentId(paramBean);
				resultTypes = commonDao.getResultTypeList_byStudentId();
			} else {*/
				statistics = statisticsDao.findDeptbyTypeAnalsList_new(paramBean);
				statisticsSum = statisticsDao.findStatisticsSumByPrivacyType(paramBean);
			/*}*/
		}
		
		StatisticsList statisticsList = new StatisticsList(statistics);

		// 개인정보 유형별 최대값
		if (statistics.size() != 0) {
			if (use_studentId.equals("yes"))
				getMaxByPrivacyType_byStudentId(statistics, statisticsList);
			else
				getMaxByPrivacyType(statistics, statisticsList);
		}

		SimpleCode simpleCode = new SimpleCode("/statistics/list_deptbyTypeAnals.html");
		String index_id = commonDao.checkIndex(simpleCode);
		List<SystemMaster> systemList = statisticsDao.findSystemListByAdmin(paramBean);
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("systemList", systemList);
		modelAndView.addObject("statisticsList", statisticsList);
		modelAndView.addObject("statisticsSum", statisticsSum);
		modelAndView.addObject("resultTypes", resultTypes);
		modelAndView.addObject("paramBean", paramBean);
		// modelAndView.addObject("pageInfo", pageInfo);
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("use_fullscan", use_fullscan);
		modelAndView.addObject("searchType", searchType);

		return modelAndView;
	}

	@Override
	public DataModelAndView findIndvbyTypeAnalsList(Map<String, String> parameters, HttpServletRequest request)
			throws ParseException {
		PageInfo pageInfo = new PageInfo(parameters, defaultPageSize);

		HttpSession session = request.getSession();
		// 다운로드로그 풀스캔연동여부
		String use_fullscan = (String) session.getAttribute("use_fullscan");
		String searchType = request.getParameter("searchType");

		Statistics paramBean = CommonHelper.convertMapToBean(getSearchDateByChkDateType(parameters), Statistics.class);
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	paramBean.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	paramBean.setAuth_idsList(list);
	    }
	    
		Integer total_count = 0;
		if (searchType != null && searchType.equals("03")) { // 다운로드로그 조회
			total_count = statisticsDao.findIndvbyDownloadTypeAnalsOne_count(paramBean);			
		} else {
			total_count = statisticsDao.findIndvbyTypeAnalsOne_count(paramBean);
		}
		
		if (paramBean.getPage_num() != null && paramBean.getPage_num().length() > 0) {
			pageInfo.setPage_num(Integer.parseInt(paramBean.getPage_num()));
		} else {
			pageInfo.setPage_num(1);
		}
		pageInfo.setTotal_count(total_count);
		paramBean.setPageInfo(pageInfo);

		List<Statistics> statistics = new ArrayList<>();
		Statistics statisticsSum = new Statistics();
		String use_studentId = parameters.get("use_studentId");
		List<Code> resultTypes = commonDao.getPrivacyDescList();
		if(searchType==null) {
			searchType = "01";
			paramBean.setSearchType("01");
		}
		if (searchType != null && searchType.equals("03")) { // 다운로드로그 조회
			statistics = statisticsDao.findIndvbyDownloadTypeAnalsList(paramBean);
			statisticsSum = statisticsDao.findStatisticsSumByDownloadType(paramBean);
		} else { // 접속기록 조회
			/*if (use_studentId.equals("yes")) {
				statistics = statisticsDao.findIndvbyTypeAnalsList_byStudentId(paramBean);
				statisticsSum = statisticsDao.findStatisticsSumByPrivacyType_byStudentId(paramBean);
				resultTypes = commonDao.getResultTypeList_byStudentId();
			} else {*/
				statistics = statisticsDao.findIndvbyTypeAnalsList_new(paramBean);
				statisticsSum = statisticsDao.findStatisticsSumByPrivacyType(paramBean);
			/*}*/
		}
		
		String checkEmpNameMasking = commonDao.checkEmpNameMasking();
		if(checkEmpNameMasking.equals("Y")) {
			for (int i = 0; i < statistics.size(); i++) {
				if(!"".equals(statistics.get(i).getEmp_user_name())) {
					String empUserName = statistics.get(i).getEmp_user_name();
					StringBuilder builder = new StringBuilder(empUserName);
					builder.setCharAt(empUserName.length() - 2, '*');
					statistics.get(i).setEmp_user_name(builder.toString());
				}
			}
		}

		StatisticsList statisticsList = new StatisticsList(statistics);

		// 개인정보 유형별 최대값
		if (statistics.size() != 0) {
			if (use_studentId.equals("yes"))
				getMaxByPrivacyType_byStudentId(statistics, statisticsList);
			else
				getMaxByPrivacyType(statistics, statisticsList);
		}

		SimpleCode simpleCode = new SimpleCode("/statistics/list_indvbyTypeAnals.html");
		String index_id = commonDao.checkIndex(simpleCode);
		List<SystemMaster> systemList = statisticsDao.findSystemListByAdmin(paramBean);
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("systemList", systemList);
		modelAndView.addObject("statisticsList", statisticsList);
		modelAndView.addObject("statisticsSum", statisticsSum);
		modelAndView.addObject("resultTypes", resultTypes);
		modelAndView.addObject("paramBean", paramBean);
		modelAndView.addObject("pageInfo", pageInfo);
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("use_fullscan", use_fullscan);
		modelAndView.addObject("searchType", searchType);

		return modelAndView;
	}

	@Override
	public DataModelAndView findAllLogInqAnalsList(Map<String, String> parameters) throws ParseException {
		PageInfo pageInfo = new PageInfo(parameters, defaultPageSize);

		Statistics paramBean = CommonHelper.convertMapToBean(getSearchDateByChkDateType(parameters), Statistics.class);
		List<Statistics> statistics = statisticsDao.findIndvbyTypeAnalsList(paramBean);
		Statistics statisticsSum = statisticsDao.findStatisticsSumByPrivacyType(paramBean);

		Integer total_count = statisticsDao.findIndvbyTypeAnalsOne_count(paramBean);
		pageInfo.setTotal_count(total_count);
		StatisticsList statisticsList = new StatisticsList(statistics);

		// 개인정보 유형별 최대값
		if (statistics.size() != 0) {
			getMaxByPrivacyType(statistics, statisticsList);
		}

		SimpleCode simpleCode = new SimpleCode("/statistics/list_allLogInqAnals.html");
		String index_id = commonDao.checkIndex(simpleCode);

		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("statisticsList", statisticsList);
		modelAndView.addObject("statisticsSum", statisticsSum);
		modelAndView.addObject("paramBean", paramBean);
		modelAndView.addObject("pageInfo", pageInfo);
		modelAndView.addObject("index_id", index_id);

		return modelAndView;
	}

	/**
	 * 개인정보 유형별 최대값
	 * 
	 * @param statistics
	 * @param statisticsList
	 */
	public void getMaxByPrivacyType(List<Statistics> statistics, StatisticsList statisticsList) {
		// 1. type별 MAX 값 구하기
		List<Integer> type1_list = new ArrayList<Integer>();
		List<Integer> type2_list = new ArrayList<Integer>();
		List<Integer> type3_list = new ArrayList<Integer>();
		List<Integer> type4_list = new ArrayList<Integer>();
		List<Integer> type5_list = new ArrayList<Integer>();
		List<Integer> type6_list = new ArrayList<Integer>();
		List<Integer> type7_list = new ArrayList<Integer>();
		List<Integer> type8_list = new ArrayList<Integer>();
		List<Integer> type9_list = new ArrayList<Integer>();
		List<Integer> type10_list = new ArrayList<Integer>();
		List<Integer> type11_list = new ArrayList<Integer>();
		List<Integer> type12_list = new ArrayList<Integer>();
		List<Integer> type13_list = new ArrayList<Integer>();
		List<Integer> type14_list = new ArrayList<Integer>();
		List<Integer> type15_list = new ArrayList<Integer>();
		List<Integer> type16_list = new ArrayList<Integer>();
		List<Integer> type17_list = new ArrayList<Integer>();
		List<Integer> type18_list = new ArrayList<Integer>();
		List<Integer> type_sum_list = new ArrayList<Integer>();

		for (Statistics statis : statistics) {
			type1_list.add(statis.getType1());
			type2_list.add(statis.getType2());
			type3_list.add(statis.getType3());
			type4_list.add(statis.getType4());
			type5_list.add(statis.getType5());
			type6_list.add(statis.getType6());
			type7_list.add(statis.getType7());
			type8_list.add(statis.getType8());
			type9_list.add(statis.getType9());
			type10_list.add(statis.getType10());
			type11_list.add(statis.getType11());
			type12_list.add(statis.getType12());
			type13_list.add(statis.getType13());
			type14_list.add(statis.getType14());
			type15_list.add(statis.getType15());
			type16_list.add(statis.getType16());
			type17_list.add(statis.getType17());
			type18_list.add(statis.getType18());
			type_sum_list.add(statis.getType_sum());
		}

		// 2. type별 MAX 값 담기
		statisticsList.setType1_max(Collections.max(type1_list));
		statisticsList.setType2_max(Collections.max(type2_list));
		statisticsList.setType3_max(Collections.max(type3_list));
		statisticsList.setType4_max(Collections.max(type4_list));
		statisticsList.setType5_max(Collections.max(type5_list));
		statisticsList.setType6_max(Collections.max(type6_list));
		statisticsList.setType7_max(Collections.max(type7_list));
		statisticsList.setType8_max(Collections.max(type8_list));
		statisticsList.setType9_max(Collections.max(type9_list));
		statisticsList.setType10_max(Collections.max(type10_list));
		statisticsList.setType11_max(Collections.max(type11_list));
		statisticsList.setType12_max(Collections.max(type12_list));
		statisticsList.setType13_max(Collections.max(type13_list));
		statisticsList.setType14_max(Collections.max(type14_list));
		statisticsList.setType15_max(Collections.max(type15_list));
		statisticsList.setType16_max(Collections.max(type16_list));
		statisticsList.setType17_max(Collections.max(type17_list));
		statisticsList.setType18_max(Collections.max(type18_list));
		statisticsList.setType_sum_max(Collections.max(type_sum_list));
	}

	public void getMaxByPrivacyType_byStudentId(List<Statistics> statistics, StatisticsList statisticsList) {
		// 1. type별 MAX 값 구하기
		List<Integer> type1_list = new ArrayList<Integer>();
		List<Integer> type2_list = new ArrayList<Integer>();
		List<Integer> type3_list = new ArrayList<Integer>();
		List<Integer> type4_list = new ArrayList<Integer>();
		List<Integer> type5_list = new ArrayList<Integer>();
		List<Integer> type6_list = new ArrayList<Integer>();
		List<Integer> type7_list = new ArrayList<Integer>();
		List<Integer> type8_list = new ArrayList<Integer>();
		List<Integer> type9_list = new ArrayList<Integer>();
		List<Integer> type10_list = new ArrayList<Integer>();
		List<Integer> type11_list = new ArrayList<Integer>();
		List<Integer> type12_list = new ArrayList<Integer>();
		List<Integer> type13_list = new ArrayList<Integer>();
		List<Integer> type14_list = new ArrayList<Integer>();
		List<Integer> type15_list = new ArrayList<Integer>();
		List<Integer> type16_list = new ArrayList<Integer>();
		List<Integer> type17_list = new ArrayList<Integer>();
		List<Integer> type18_list = new ArrayList<Integer>();
		List<Integer> type_sum_list = new ArrayList<Integer>();

		for (Statistics statis : statistics) {
			type1_list.add(statis.getType1());
			type2_list.add(statis.getType2());
			type3_list.add(statis.getType3());
			type4_list.add(statis.getType4());
			type5_list.add(statis.getType5());
			type6_list.add(statis.getType6());
			type7_list.add(statis.getType7());
			type8_list.add(statis.getType8());
			type9_list.add(statis.getType9());
			type10_list.add(statis.getType10());
			type11_list.add(statis.getType11());
			type12_list.add(statis.getType12());
			type13_list.add(statis.getType13());
			type14_list.add(statis.getType14());
			type15_list.add(statis.getType15());
			type16_list.add(statis.getType16());
			type17_list.add(statis.getType17());
			type18_list.add(statis.getType18());
			type_sum_list.add(statis.getType_sum());
		}

		// 2. type별 MAX 값 담기
		statisticsList.setType1_max(Collections.max(type1_list));
		statisticsList.setType2_max(Collections.max(type2_list));
		statisticsList.setType3_max(Collections.max(type3_list));
		statisticsList.setType4_max(Collections.max(type4_list));
		statisticsList.setType5_max(Collections.max(type5_list));
		statisticsList.setType6_max(Collections.max(type6_list));
		statisticsList.setType7_max(Collections.max(type7_list));
		statisticsList.setType8_max(Collections.max(type8_list));
		statisticsList.setType9_max(Collections.max(type9_list));
		statisticsList.setType10_max(Collections.max(type10_list));
		statisticsList.setType11_max(Collections.max(type11_list));
		statisticsList.setType12_max(Collections.max(type12_list));
		statisticsList.setType13_max(Collections.max(type13_list));
		statisticsList.setType14_max(Collections.max(type14_list));
		statisticsList.setType15_max(Collections.max(type15_list));
		statisticsList.setType16_max(Collections.max(type16_list));
		statisticsList.setType17_max(Collections.max(type17_list));
		statisticsList.setType18_max(Collections.max(type18_list));
//		statisticsList.setType31_max(Collections.max(type31_list));
//		statisticsList.setType32_max(Collections.max(type32_list));
//		statisticsList.setType33_max(Collections.max(type33_list));
//		statisticsList.setType99_max(Collections.max(type99_list));
		statisticsList.setType_sum_max(Collections.max(type_sum_list));
	}

	/**
	 * 선택별 (일, 월, 연도) 검색 날짜 설정
	 * 
	 * @param parameters
	 * @return
	 */
	public Map<String, String> getSearchDateByChkDateType(Map<String, String> parameters) {
		String chk_date_type = parameters.get("chk_date_type");
		String start_date = parameters.get("search_from");
		String end_date = parameters.get("search_to");

		if (chk_date_type == null) {
			chk_date_type = "";
			return parameters;
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.MILLISECOND, 0);
		/*
		 * int year = cal.get (Calendar.YEAR); int month = cal.get (Calendar.MONTH) + 1
		 * ; int last_day = cal.getActualMaximum(Calendar.DATE);
		 */

		String[] arrStartDate = start_date.split("-");
		int year = Integer.parseInt(arrStartDate[0]);
		int month = Integer.parseInt(arrStartDate[1]);

		String[] arrEndDate = end_date.split("-");
		int to_year = Integer.parseInt(arrEndDate[0]);
		int to_month = Integer.parseInt(arrEndDate[1]);
		int to_day = Integer.parseInt(arrEndDate[2]);
		
		cal.set(to_year, to_month-1, to_day);
		
		//int to_year = cal.get(Calendar.YEAR);
		//int to_month = cal.get(Calendar.MONTH) + 1;
		int last_day = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

		String search_from = "";
		String search_to = "";

		switch (chk_date_type) {
		case "DAY":
			search_from = year + "" + (month < 10 ? "0" + month : month) + "01";
			search_to = to_year + "" + (to_month < 10 ? "0" + to_month : to_month) + last_day;

			parameters.put("search_from", search_from);
			parameters.put("search_to", search_to);
			break;
		case "MONTH":
			// search_from = year + "0101";
			search_from = year + "" + (month < 10 ? "0" + month : month) + "01";
			search_to = to_year + "" + (to_month < 10 ? "0" + to_month : to_month) + last_day;

			parameters.put("search_from", search_from);
			parameters.put("search_to", search_to);
			break;
		case "YEAR":
			// search_from = "20140101"; // 언제부터로 할지 지정하기.
			search_from = year + "0101";
			search_to = to_year + "1231";

			parameters.put("search_from", search_from);
			parameters.put("search_to", search_to);
			break;
		default:
			break;
		}

		return parameters;
	}

	@Override
	public void findPerdbyTypeAnalsList_download(DataModelAndView modelAndView, Map<String, String> parameters,
			HttpServletRequest request) {

		Statistics paramBean = CommonHelper.convertMapToBean(parameters, Statistics.class);
		
		String searchType = (String) request.getParameter("searchType");
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	paramBean.setAuth_idsList(list);
	    } else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	paramBean.setAuth_idsList(list);
	    }

	    String fileName = "";
	    String sheetName = "";
	    
		List<Statistics> statistics = new ArrayList<>();
		Statistics statisticsSum = new Statistics();

		if (searchType != null && searchType.equals("03")) { // 다운로드로그 조회
			statistics = statisticsDao.findPerdbyDownTypeAnalsList(paramBean);
			statisticsSum = statisticsDao.findStatisticsSumByDownloadType(paramBean);
			fileName = "PSM_기간별유형통계(다운로드)_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
			sheetName = "기간별유형통계(다운로드)";

		} else { // 접속기록 조회
			statistics = statisticsDao.findPerdbyTypeAnalsList(paramBean);
			statisticsSum = statisticsDao.findStatisticsSumByPrivacyType(paramBean);
			fileName = "PSM_기간별유형통계" + new SimpleDateFormat("yyyyMMdd").format(new Date());
			sheetName = "기간별유형통계";
		}

		statisticsSum.setProc_date_statistics("합계");
		statistics.add(statisticsSum);
		List<Code> resultTypes = commonDao.getPrivacyDescList();
		String[] columns = new String[resultTypes.size() + 2];
		columns[0] = "proc_date_statistics";
		for (int i = 1; i <= resultTypes.size(); i++) {
			Code code = resultTypes.get(i-1);
			columns[i] = "type" + code.getResult_type_order();
		}
		columns[resultTypes.size() + 1] = "type_sum";
		// String[] heads = new String[] {"날짜", "주민등록번호", "외국인등록번호", "운전면허번호", "여권번호",
		// "신용카드번호", "건강보험번호","전화번호","이메일","휴대폰번호","계좌번호","합계"};
		String[] heads = new String[resultTypes.size() + 2];
		heads[0] = "기간";
		for (int i = 1; i <= resultTypes.size(); i++) {
			Code result = resultTypes.get(i - 1);
			heads[i] = result.getCode_name();
		}
		heads[resultTypes.size() + 1] = "합계";

		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", statistics);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);

	}

	@Override
	public void findSysbyTypeAnalsList_download(DataModelAndView modelAndView, Map<String, String> parameters,
			HttpServletRequest request) {

		Statistics paramBean = CommonHelper.convertMapToBean(parameters, Statistics.class);
		
		String searchType = (String) request.getParameter("searchType");
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	paramBean.setAuth_idsList(list);
	    } else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	paramBean.setAuth_idsList(list);
	    }

		//String fileName = "PSM_시스템별유형통계_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		//String sheetName = "시스템별유형통계";
	    
	    String fileName = "";
	    String sheetName = "";

		List<Statistics> statistics = new ArrayList<>();
		Statistics statisticsSum = new Statistics();
		
		if (searchType != null && searchType.equals("03")) { // 다운로드로그 조회
			statistics = statisticsDao.findSysbyDownloadTypeAnalsList(paramBean);
			statisticsSum = statisticsDao.findStatisticsSumByDownloadType(paramBean);
			fileName = "PSM_시스템별유형통계(다운로드)_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
			sheetName = "시스템별유형통계(다운로드)";

		} else {// 접속기록 조회
			statistics = statisticsDao.findSysbyTypeAnalsList_new(paramBean);
			statisticsSum = statisticsDao.findStatisticsSumByPrivacyType(paramBean);
			fileName = "PSM_시스템별유형통계_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
			sheetName = "시스템별유형통계";
		}
		
		statisticsSum.setSystem_name("합계");
		statistics.add(statisticsSum);

		List<Code> resultTypes = commonDao.getPrivacyDescList();
		String[] columns = new String[resultTypes.size() + 2];
		columns[0] = "system_name";
		for (int i = 1; i <= resultTypes.size(); i++) {
			Code code = resultTypes.get(i-1);
			columns[i] = "type" + code.getResult_type_order();
		}
		columns[resultTypes.size() + 1] = "type_sum";

		String[] heads = new String[resultTypes.size() + 2];
		heads[0] = "시스템명";
		for (int i = 1; i <= resultTypes.size(); i++) {
			Code result = resultTypes.get(i - 1);
			heads[i] = result.getCode_name();
		}
		heads[resultTypes.size() + 1] = "합계";

		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", statistics);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}

	@Override
	public void findDeptbyTypeAnalsList_download(DataModelAndView modelAndView, Map<String, String> parameters,
			HttpServletRequest request) {

		Statistics paramBean = CommonHelper.convertMapToBean(parameters, Statistics.class);
		
		String searchType = (String) request.getParameter("searchType");
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	paramBean.setAuth_idsList(list);
	    } else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	paramBean.setAuth_idsList(list);
	    }

	    String fileName = "";
	    String sheetName = "";
		
		List<Statistics> statistics = new ArrayList<>();
		Statistics statisticsSum = new Statistics();

		if (searchType != null && searchType.equals("03")) { // 다운로드로그 조회
			statistics = statisticsDao.findDeptbyDownloadTypeAnalsList(paramBean);
			statisticsSum = statisticsDao.findStatisticsSumByDownloadType(paramBean);
			fileName = "PSM_부서별유형통계(다운로드)_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
			sheetName = "부서별유형통계(다운로드)";

		} else { // 접속기록 조회
			statistics = statisticsDao.findDeptbyTypeAnalsList_new(paramBean);
			statisticsSum = statisticsDao.findStatisticsSumByPrivacyType(paramBean);
			fileName = "PSM_부서별유형통계_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
			sheetName = "부서별유형통계";
		}

		statisticsSum.setDept_name("합계");
		statistics.add(statisticsSum);

		List<Code> resultTypes = commonDao.getPrivacyDescList();
		String[] columns = new String[resultTypes.size() + 2];
		columns[0] = "dept_name";
		for (int i = 1; i <= resultTypes.size(); i++) {
			Code code = resultTypes.get(i-1);
			columns[i] = "type" + code.getResult_type_order();
		}
		columns[resultTypes.size() + 1] = "type_sum";

		String[] heads = new String[resultTypes.size() + 2];
		heads[0] = "소속";
		for (int i = 1; i <= resultTypes.size(); i++) {
			Code result = resultTypes.get(i - 1);
			heads[i] = result.getCode_name();
		}
		heads[resultTypes.size() + 1] = "합계";

		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", statistics);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}

	@Override
	public void findIndvbyTypeAnalsList_download(DataModelAndView modelAndView, Map<String, String> parameters,
			HttpServletRequest request) {

		Statistics paramBean = CommonHelper.convertMapToBean(parameters, Statistics.class);
		
		String searchType = (String) request.getParameter("searchType");
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	paramBean.setAuth_idsList(list);
	    } else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	paramBean.setAuth_idsList(list);
	    }

	    
	    String fileName = "";
	    String sheetName = "";
		
		List<Statistics> statistics = new ArrayList<>();
		Statistics statisticsSum = new Statistics();
		
		PageInfo pageInfo = new PageInfo(1, 1);
		pageInfo.setUseExcel("true");
		paramBean.setPageInfo(pageInfo);

		if (searchType != null && searchType.equals("03")) { // 다운로드로그 조회
			statistics = statisticsDao.findIndvbyDownloadTypeAnalsList(paramBean);
			statisticsSum = statisticsDao.findStatisticsSumByDownloadType(paramBean);
			fileName = "PSM_개인별유형통계(다운로드)_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
			sheetName = "개인별유형통계(다운로드)";

		} else { // 접속기록 조회
			statistics = statisticsDao.findIndvbyTypeAnalsList_new(paramBean);
			statisticsSum = statisticsDao.findStatisticsSumByPrivacyType(paramBean);
			fileName = "PSM_개인별유형통계_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
			sheetName = "개인별유형통계";
		}

		for (Statistics statistic : statistics) {
			statistic.setEmp_user_name(statistic.getEmp_user_name() + "(" + statistic.getEmp_user_id() + ")");
		}
		statisticsSum.setEmp_user_name("합계");
		statistics.add(statisticsSum);
		
		List<Code> resultTypes = commonDao.getPrivacyDescList();
		String[] columns = new String[resultTypes.size() + 3];
		columns[0] = "emp_user_name";
		columns[1] = "dept_name";
		for (int i = 1; i <= resultTypes.size(); i++) {
			Code code = resultTypes.get(i-1);
			columns[i + 1] = "type" + code.getResult_type_order();
		}
		columns[resultTypes.size() + 2] = "type_sum";

		String[] heads = new String[resultTypes.size() + 3];
		heads[0] = "사용자명";
		heads[1] = "소속";
		for (int i = 1; i <= resultTypes.size(); i++) {
			Code result = resultTypes.get(i - 1);
			heads[i + 1] = result.getCode_name();
		}
		heads[resultTypes.size() + 2] = "합계";

		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", statistics);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}

	@Override
	public DataModelAndView findRegularInspection(Map<String, String> parameters) throws ParseException {

		DataModelAndView modelAndView = new DataModelAndView();

		Statistics paramBean = CommonHelper.convertMapToBean(parameters, Statistics.class);
		paramBean.setPage_size(defaultPageSize);
		if(paramBean.getPage_num() == null)
			paramBean.setPage_num("1");
		List<Statistics> inspection = statisticsDao.findRegularInspection(paramBean);


		if (paramBean.getSelect_month() == null) {
			Calendar c = Calendar.getInstance();
			String year = String.valueOf(c.get(Calendar.YEAR));
			String month = String.valueOf(c.get(Calendar.MONTH) + 1);
			if (month.length() == 1)
				month = "0" + month;
			// String lastDay = String.valueOf(c.getActualMaximum(Calendar.DAY_OF_MONTH));
			paramBean.setStart_date(year + month + "01");
			paramBean.setEnd_date(year + month + "31");
			modelAndView.addObject("sel_year", year);
			modelAndView.addObject("sel_month", month);
		} else {
			String year = paramBean.getSelect_year();
			String month = paramBean.getSelect_month();
			if (month.length() == 1)
				month = "0" + month;
			paramBean.setStart_date(year + month + "01");
			paramBean.setEnd_date(year + month + "31");
			modelAndView.addObject("sel_year", year);
			modelAndView.addObject("sel_month", month);
		}
		int inspectReportCount = reportDao.findReportInspectionList_count(paramBean);
		List<InspectionReport> reportlist = reportDao.findReportInspectionList(paramBean);
		paramBean.setPage_total_count(String.valueOf(reportlist.size()));
		
		List<Map<String, String>> operation = statisticsDao.findInspectionTable(paramBean);

		modelAndView.addObject("inspection", inspection);
		modelAndView.addObject("operation", operation);

		SimpleCode simpleCode = new SimpleCode("/statistics/regular_inspection.html");
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("paramBean", paramBean);

		modelAndView.addObject("patchHist_sw", statisticsDao.findPatchHistList(paramBean.setPatch_sort("sw")));
		modelAndView.addObject("patchHist_data", statisticsDao.findPatchHistList(paramBean.setPatch_sort("data")));
		modelAndView.addObject("patchHist_mapping", statisticsDao.findPatchHistList(paramBean.setPatch_sort("mapping")));
		
		
		if (parameters.get("tab_flag") == null)
			modelAndView.addObject("tab_flag", 1);
		else
			modelAndView.addObject("tab_flag", parameters.get("tab_flag"));
		modelAndView.addObject("select_tab", parameters.get("select_tab"));
		modelAndView.addObject("reportlist", reportlist);
		modelAndView.addObject("reportCount", inspectReportCount);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView findInspection_hist(Map<String, String> parameters) throws ParseException {

		DataModelAndView modelAndView = new DataModelAndView();
		Statistics paramBean = CommonHelper.convertMapToBean(parameters, Statistics.class);

		PageInfo pageInfo = new PageInfo(parameters, defaultPageSize);
		Integer totalCount = statisticsDao.findInspectionHistList_count(paramBean);
		pageInfo.setTotal_count(totalCount);
		paramBean.setPageInfo(pageInfo);

		SimpleCode simpleCode = new SimpleCode("/statistics/inspection_hist.html");
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("paramBean", paramBean);
		modelAndView.addObject("pageInfo", pageInfo);

		List<Statistics> list = statisticsDao.findInspectionHistList(paramBean);
		modelAndView.addObject("list", list);

		return modelAndView;
	}

	@Override
	public DataModelAndView findInspection_hist_detail(Map<String, String> parameters) throws ParseException {

		DataModelAndView modelAndView = new DataModelAndView();
		Statistics paramBean = CommonHelper.convertMapToBean(parameters, Statistics.class);

		SimpleCode simpleCode = new SimpleCode("/statistics/inspection_hist_detail.html");
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("paramBean", paramBean);

		Statistics inspection = statisticsDao.findInspectionHistDetail(paramBean);
		modelAndView.addObject("inspection", inspection);

		int seq = paramBean.getSeq();
		String prev_content = statisticsDao.getInspectionHistPrevContents(seq - 1);
		if (prev_content != null) {
			modelAndView.addObject("prev_content", prev_content);
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		modelAndView.addObject("now", sdf.format(calendar.getTime()));
		String recent_content = statisticsDao.getInspectionHistRecentContents();
		if (recent_content != null) {
			modelAndView.addObject("recent_content", recent_content);
		}

		return modelAndView;
	}

	@Override
	public DataModelAndView addInspection_hist(Map<String, String> parameters) {

		Statistics paramBean = CommonHelper.convertMapToBean(parameters, Statistics.class);

		String month = parameters.get("month");
		if (month.length() == 1) {
			month = "0" + month;
		}
		String proc_month = parameters.get("year") + month;
		paramBean.setProc_month(proc_month);

		// Validation Check (privacy_seq 중복)
		int check = statisticsDao.checkInspectionHist(paramBean);
		if (check > 0) {
			throw new ESException("SYS301J");
		}

		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);

		try {
			statisticsDao.addInspectionHist(paramBean);
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			logger.error("[StatisticsSvcImpl.addInspection_hist] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}

		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}

	@Override
	public DataModelAndView saveInspection_hist(Map<String, String> parameters) {

		Statistics paramBean = CommonHelper.convertMapToBean(parameters, Statistics.class);
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);

		try {
			statisticsDao.saveInspectionHist(paramBean);
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			logger.error("[StatisticsSvcImpl.saveInspection_hist] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}

		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	

	@Override
	public DataModelAndView findIndvbyDngValAnalsList(Map<String, String> parameters, HttpServletRequest request)
			throws ParseException {
		PageInfo pageInfo = new PageInfo(parameters, defaultPageSize);

		HttpSession session = request.getSession();
		String searchType = request.getParameter("searchType");

		Statistics paramBean = CommonHelper.convertMapToBean(getSearchDateByChkDateType(parameters), Statistics.class);
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	paramBean.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	paramBean.setAuth_idsList(list);
	    }
	    
		Integer total_count = statisticsDao.findIndvbyDngValAnalsList_count(paramBean);			
		
		if (paramBean.getPage_num() != null && paramBean.getPage_num().length() > 0) {
			pageInfo.setPage_num(Integer.parseInt(paramBean.getPage_num()));
		} else {
			pageInfo.setPage_num(1);
		}
		pageInfo.setTotal_count(total_count);
		paramBean.setPageInfo(pageInfo);

		List<Statistics> statistics = statisticsDao.findIndvbyDngValAnalsList(paramBean);

		StatisticsList statisticsList = new StatisticsList(statistics);

		SimpleCode simpleCode = new SimpleCode("/statistics/list_indvbyDngValAnals.html");
		String index_id = commonDao.checkIndex(simpleCode);

		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("statisticsList", statisticsList);
		modelAndView.addObject("paramBean", paramBean);
		modelAndView.addObject("pageInfo", pageInfo);
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("searchType", searchType);

		return modelAndView;
	}

	@Override
	public void findIndvbyDngValAnalsList_download(DataModelAndView modelAndView, Map<String, String> parameters,
			HttpServletRequest request) {
		Statistics paramBean = CommonHelper.convertMapToBean(parameters, Statistics.class);
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	paramBean.setAuth_idsList(list);
	    } else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	paramBean.setAuth_idsList(list);
	    }
	    
	    String fileName = "";
	    String sheetName = "";
		
		PageInfo pageInfo = new PageInfo(1, 1);
		pageInfo.setUseExcel("true");
		paramBean.setPageInfo(pageInfo);

		List<Statistics> statistics = statisticsDao.findIndvbyDngValAnalsList(paramBean);
		fileName = "PSM_개인별위험도통계_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		sheetName = "개인별위험도통계";

		String[] columns = {"emp_user_name", "emp_user_id", "dept_name", "dept_id", "dng_val"};
		String[] heads = {"사용자명", "사용자ID", "부서명", "부서ID", "위험도"};
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", statistics);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}

	@Override
	public DataModelAndView connectLogResultList(SearchSearch search, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView();
		
		SimpleCode simpleCode = new SimpleCode();
		String index = "/statistics/connectLogResultList.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		HttpSession session = request.getSession();
		// 개인정보유형 뷰 : 마스킹 처리 유무 Y 또는 N
		String result_type_view = (String) session.getAttribute("result_type");
		// 메뉴명 유무 : 메뉴명 컬럼 노출 여부 1은 노출, 2는 숨김, 3 미사용
		String scrn_name_view = (String) session.getAttribute("scrn_name");
		modelAndView.addObject("scrn_name_view", scrn_name_view);
		List<String> list = (List<String>) session.getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) session.getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
	    String ui_type = (String)session.getAttribute("ui_type");
		if (ui_type != null && ui_type.length() > 0) {
			modelAndView.addObject("ui_type", ui_type);
		}
		if(search.getStart_h() != null) {
			String start_time = search.getStart_h().substring(0);
			if(start_time.length() < 2) {
				start_time = "0" + start_time;
			}
			search.setStart_time(start_time);
		}
		if(search.getEnd_h() != null) {
			String end_time = search.getEnd_h().substring(0);
			if(end_time.length() < 2) {
				end_time = "0" + end_time;
			}
			search.setEnd_time(end_time);
		}
		
		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		String privacyTypeName = statisticsDao.privacyTypeName(search.getPrivacyType());
		modelAndView.addObject("privacyTypeName", privacyTypeName);
		
		// 시스템 리스트 정보
		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList_byAdmin(search);
		modelAndView.addObject("systemMasterList", systemMasterList);
		
		// 결과정보
		Integer total_count = statisticsDao.connectLogResultListCt(search);
		search.setTotal_count(total_count);
		List<AllLogInq> allLogInq = statisticsDao.connectLogResultList(search);
		if(result_type_view.equals("Y")) {
			for(AllLogInq v : allLogInq) {
				String temp = v.getResult_content();
				if(temp != null && temp !="") {
					temp = getResultContextByMasking(temp, v.getResult_type());
					v.setResult_content(temp);
				}
			}
		}
		modelAndView.addObject("allLogInq", allLogInq);
		modelAndView.addObject("search", search);
		return modelAndView;
	}

	@Override
	public DataModelAndView downloadLogResultList(SearchSearch search, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView();
		
		SimpleCode simpleCode = new SimpleCode();
		String index = "/statistics/downloadLogResultList.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		HttpSession session = request.getSession();
		// 개인정보유형 뷰 : 마스킹 처리 유무 Y 또는 N
		String result_type_view = (String) session.getAttribute("result_type");
		// 메뉴명 유무 : 메뉴명 컬럼 노출 여부 1은 노출, 2는 숨김, 3 미사용
		String scrn_name_view = (String) session.getAttribute("scrn_name");
		modelAndView.addObject("scrn_name_view", scrn_name_view);
		// 다운로드로그 풀스캔연동여부
		String use_fullscan = (String) session.getAttribute("use_fullscan");
		modelAndView.addObject("use_fullscan", use_fullscan);
		List<String> list = (List<String>) session.getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) session.getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
	    if(search.getStart_h() != null) {
			String start_time = search.getStart_h().substring(0);
			if(start_time.length() < 2) {
				start_time = "0" + start_time;
			}
			search.setStart_time(start_time);
		}
		if(search.getEnd_h() != null) {
			String end_time = search.getEnd_h().substring(0);
			if(end_time.length() < 2) {
				end_time = "0" + end_time;
			}
			search.setEnd_time(end_time);
		}
	    int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		String privacyTypeName = statisticsDao.privacyTypeName(search.getPrivacyType());
		modelAndView.addObject("privacyTypeName", privacyTypeName);
		
		// 시스템 리스트 정보
		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList_byAdmin(search);
		modelAndView.addObject("systemMasterList", systemMasterList);
		
		// 결과정보
		Integer total_count = statisticsDao.downloadLogResultListCt(search);
		search.setTotal_count(total_count);
		List<AllLogInq> allLogInq = statisticsDao.downloadLogResultList(search);
		if(result_type_view.equals("Y")) {
			for(AllLogInq v : allLogInq) {
				String temp = v.getResult_content();
				if(temp != null && temp !="") {
					temp = getResultContextByMasking(temp, v.getResult_type());
					v.setResult_content(temp);
				}
			}
		}
		modelAndView.addObject("allLogInq", allLogInq);
		
		modelAndView.addObject("search", search);
		return modelAndView;
	}
	
	/*
	 * public static String getResultContextByMasking(String result, String type) {
	 * int typeIntVal = Integer.parseInt(type); int len = result.length();
	 * switch(typeIntVal) { case 1: case 10: result =
	 * result.substring(0,6)+"-*******"; break; case 2: result =
	 * result.substring(0,6); for(int i=0; i<len-6; i++) result += "*"; break; case
	 * 3: result = result.substring(0,5); for(int i=0; i<len-5; i++) result += "*";
	 * break; case 4: result = result.substring(0,10)+"****-****"; break; case 5:
	 * result = result.substring(0,6); for(int i=0; i<len-6; i++) result += "*";
	 * break; case 6: break; case 8: String[] arr = result.split("-"); if(arr.length
	 * <= 1) { result = result.substring(0, len - 4) + "****"; }else { result =
	 * arr[0] + "-" + arr[1] + "-****"; } break; case 7: result =
	 * result.replaceAll("(?<=.{3}).(?=.*@)", "*"); break; case 9: String[] arr2 =
	 * result.split("-"); if(arr2.length <= 2) { result = result.substring(0, len -
	 * 6) + "******"; }else { result = arr2[0] + "-" + arr2[1] + "-"; if(arr2[2] !=
	 * null) { int l = arr2[2].length(); for(int i=0; i<l; i++) result += "*"; } }
	 * break; }
	 * 
	 * return result; }
	 */

	public static String getResultContextByMasking(String result, String type) {
		Pattern[] patternArr = new Pattern[10];/*주민번호, 운전번호, 여권번호, 신용카드번호, 건강보험번호, 전화번호, 이메일, 휴대폰번호, 계좌번호, 외국인등록번호*/
		patternArr[0]=Pattern.compile("\\b(?:[0-9]{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[1,2][0-9]|3[0,1]))-[1-4][0-9]{6}\\b|\\b(?:[0-9]{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[1,2][0-9]|3[0,1]))[1-4][0-9]{6}\\b");
		patternArr[1]=Pattern.compile("(?<!\\d)((1[1-9])|(2[0-68]))([ \\-]{1,3})\\d{6}([ \\-]{1,3})\\d{2}(?!\\d+)");
		patternArr[2]=Pattern.compile("([a-zA-Z]{1}|[a-zA-Z]{2})\\d{8}");
		patternArr[3]=Pattern.compile("(?<!\\d)(30[0-5]\\d{1}|3095|35(2[89]|[3-8]\\d{1}|92)|4\\d{3}|60(11|48|60)|5(021|409)|51(00|55)|53(10|77|88)|55(21|51|96|8[59])|51(2[0346]|4[089]|6[034]|7[6-9])|62(0[0-28]|1[0246]|2[1-9]|[56]\\d{1}|8[1-8])|63(6[01]|96)|64[4-9]\\d{1}|65\\d{2}|9(035|289|350|490|5(30|58)|720|806|999)|94(0[0379]|1[0-25]|2[0156]|3[012569]|4[013-6]|5[025-8]|6[0-578]|[78][05]))( ?\\- ?| |­)\\d{4}\\14\\d{4}\\14\\d{4}(?!\\d+)|(?<!\\d)(30([0-5]\\d{1}|95)|3[689]\\d{2})( ?\\- ?| |­)\\d{6}\\15\\d{4}(?!\\d+)|(?<!\\d)(34|37)\\d{2}( ?\\- ?| |­)\\d{6}\\16\\d{5}(?!\\d+)|(?<!\\d)\\d{4}( ?- ?| )\\d{6}( ?- ?| )\\d{5}(?!\\d+)");
		patternArr[4]=Pattern.compile("(?<!\\d)[1-8]( ?- ?| )\\d{10}(?!\\d+)");
		patternArr[5]=Pattern.compile("(?<!\\d)0(2|31|32|33|41|42|43|44|63|51|52|53|55|61|62|54|64)([ )\\-]{1,3})(\\d{3,4})([ \\-]{0,3})(\\d{4})(?!\\d+)");
		patternArr[6]=Pattern.compile("\\b[\\w\\~\\-\\.]{3,}@[\\w\\~\\-]{2,}\\.(?:kr|co.kr|com|net|asia|co|org|pro|cn|jp|biz|me|in|eu|info|tv|nameso|or.kr|pw|ru|sx|mobi|ph|tw|go.kr|ac.kr|ac)\\b");
		patternArr[7]=Pattern.compile("(?<!\\d)((010[ \\-]{0,3}\\d{4}[ \\-]{0,3}\\d{4})|(01[16789][ \\-]{0,3}\\d{3,4}[ \\-]{0,3}\\d{4}))(?!\\d+)");
		patternArr[8]=Pattern.compile("(?<!\\d)(\\d{3} ?- ?\\d{2} ?- ?\\d{5} ?- ?\\d{1}|\\d{3} ?- ?\\d{7} ?- ?\\d{1} ?- ?\\d{3}|\\d{8} ?- ?\\d{2}|\\d{3} ?- ?\\d{8}|\\d{3} ?- ?\\d{2} ?- ?\\d{6} ?- ?\\d{1}|\\d{3} ?- ?\\d{6} ?- ?\\d{2} ?- ?\\d{2} ?- ?\\d{1}|\\d{3} ?- ?\\d{2} ?- ?\\d{5} ?- ?\\d{3}|\\d{6} ?- ?\\d{2} ?- ?\\d{4}|\\d{6} ?- ?\\d{2} ?- ?\\d{6}|\\d{3} ?- ?\\d{6} ?- ?\\d{3}|\\d{6} ?- ?\\d{2} ?- ?\\d{3} ?- ?\\d{1}|\\d{4} ?- ?\\d{3} ?- ?\\d{6}|\\d{3} ?- ?\\d{2} ?- ?\\d{6}|\\d{3} ?- ?\\d{8} ?- ?\\d{1}|\\d{3} ?- ?\\d{3} ?- ?\\d{8}|\\d{3} ?- ?\\d{3} ?- ?\\d{6}|\\d{3} ?- ?\\d{2,3} ?- ?\\d{7} ?- ?\\d{1}|\\d{3} ?- ?\\d{2} ?- ?\\d{7,8}|\\d{3} ?- ?\\d{5} ?- ?\\d{2} ?- ?\\d{1} ?- ?\\d{2}|\\d{3} ?- ?\\d{5} ?- ?\\d{2} ?- ?\\d{1}|\\d{1} ?- ?\\d{6} ?- ?\\d{2} ?- ?\\d{1}|\\d{1} ?- ?\\d{6} ?- ?\\d{2} ?- ?\\d{1}|\\d{2} ?- ?\\d{2} ?- ?\\d{5} ?- ?\\d{1}|\\d{3} ?- ?\\d{2} ?- ?\\d{6} ?- ?\\d{3}|\\d{3} ?- ?\\d{3} ?- ?\\d{5} ?- ?\\d{1}|\\d{2} ?- ?\\d{2} ?- ?\\d{6}|\\d{3} ?- ?\\d{9} ?- ?\\d{2}|\\d{3} ?- ?\\d{2} ?- ?\\d{2} ?- ?\\d{6} ?- ?\\d{1}|\\d{4} ?- ?\\d{2,3} ?- ?\\d{6} ?- ?\\d{1}|\\d{5} ?- ?\\d{2} ?- ?\\d{5} ?- ?\\d{1}|\\d{3} ?- ?\\d{5} ?- ?\\d{1} ?- ?\\d{3}|\\d{3} ?- ?\\d{2} ?- ?\\d{4} ?- ?\\d{3}|\\d{6} ?- ?\\d{2} ?- ?\\d{5} ?- ?\\d{1}|\\d{3} ?- ?\\d{2} ?- ?\\d{4} ?- ?\\d{3}|\\d{6} ?- ?\\d{2} ?- ?\\d{5} ?- ?\\d{1}|\\d{3} ?- ?\\d{5} ?- ?\\d{2} ?- ?\\d{2} ?- ?\\d{1})(?!\\d)");
		patternArr[9]=Pattern.compile("\\b(?:[0-9]{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[1,2][0-9]|3[0,1]))-[5-8][0-9]{6}\\b|\\b(?:[0-9]{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[1,2][0-9]|3[0,1]))-[5-8][0-9]{6}\\b");
		
		 String resultType[] = {"주민번호","운전번호", "여권번호", "신용번호", "건강보험번호", "전화번호", "이메일", "휴대폰번호", "계좌번호", "외국인등록번호"};
		
		for (int i = 0; i < patternArr.length; i++) {
			if(patternArr[i].matcher(result).find()) {
				type = resultType[i];
				break;
			}
		}
		
		int len = result.length();
		switch(type) {
			case "주민번호":
			case "외국인등록번호":
				result = result.substring(0,6)+"-*******";
				break;
			case "운전번호":
				result = result.substring(0,6);
				for(int i=0; i<len-6; i++)
					result += "*";
				break;
			case "여권번호":
				result = result.substring(0,5);
				for(int i=0; i<len-5; i++)
					result += "*";
				break;
			case "신용번호":
				result = result.substring(0,10)+"****-****";
				break;
			case "건강보험번호":
				result = result.substring(0,6);
				for(int i=0; i<len-6; i++)
					result += "*";
				break;
			case "전화번호":
			case "휴대폰번호":
				String[] arr = result.split("-");
				if(arr.length <= 1) {
					result = result.substring(0, len - 4) + "****";
				}else {
					result = arr[0] + "-" + arr[1] + "-****";
				}
				break;
			case "이메일":
				result = result.replaceAll("(?<=.{3}).(?=.*@)", "*");
				break;
			case "계좌번호":
				String[] arr2 = result.split("-");
				if(arr2.length <= 2) {
					result = result.substring(0, len - 6) + "******";
				}else {
					result = arr2[0] + "-" + arr2[1] + "-";
					if(arr2[2] != null) {
						int l = arr2[2].length();
						for(int i=0; i<l; i++)
							result += "*";
					}
				}
				break;
			default:
				result = result.substring(0, result.length()-6)+"******";
				break;
		}
		return result;
	}
	
	@Override
	public void connectLogResultListExcel(DataModelAndView modelAndView, SearchSearch search,
			HttpServletRequest request) {
		search.setUseExcel("true");
		String fileName = "유형통계_접속기록조회_상세" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "개인정보 접속기록조회";
		
		HttpSession session = request.getSession();
		// 개인정보유형 뷰 : 마스킹 처리 유무 Y 또는 N
		String result_type_view = (String) session.getAttribute("result_type");
		// 메뉴명 유무 : 메뉴명 컬럼 노출 여부 1은 노출, 2는 숨김, 3 미사용
		String scrn_name_view = (String) session.getAttribute("scrn_name");
		List<String> list = (List<String>) session.getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) session.getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
	    String ui_type = (String)session.getAttribute("ui_type");
		if(search.getStart_h() != null) {
			String start_time = search.getStart_h().substring(0);
			if(start_time.length() < 2) {
				start_time = "0" + start_time;
			}
			search.setStart_time(start_time);
		}
		if(search.getEnd_h() != null) {
			String end_time = search.getEnd_h().substring(0);
			if(end_time.length() < 2) {
				end_time = "0" + end_time;
			}
			search.setEnd_time(end_time);
		}
		
		String privacyTypeName = statisticsDao.privacyTypeName(search.getPrivacyType());
		SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
		SimpleDateFormat parseSdf = new SimpleDateFormat("yyyyMMddHHmmss");
		// 결과정보
		List<AllLogInq> allLogInq = statisticsDao.connectLogResultList(search);
		try {
			if(result_type_view.equals("Y")) {
				for(AllLogInq v : allLogInq) {
					String temp = v.getResult_content();
					if(temp != null && temp !="") {
						temp = getResultContextByMasking(temp, v.getResult_type());
						v.setResult_content(temp);
					}
					String dateVal = v.getProc_date()+v.getProc_time();
					Date date = parseSdf.parse(dateVal);
					v.setProc_date(sdf.format(date));
				}
			}
		} catch (Exception e) {
		}
		
		String[] columns;
		String[] heads;
		if(scrn_name_view.equals("1")) {
			columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "result_content", "req_context", "scrn_name", "req_url"};
			heads = new String[] {"기간", "소속", "사용자ID", "사용자명", "사용자IP", "시스템명", privacyTypeName, "수행업무", "메뉴명", "접근 경로"};
		}else if(scrn_name_view.equals("2")){
			String tmpName = "접근경로";
			String tmpName2 = "req_url";
			if(ui_type.equals("K")) {
				tmpName = "메뉴명";
				tmpName2 = "scrn_name"; 
			}
			columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "result_content", "req_context", tmpName2};
			heads = new String[] {"기간", "소속", "사용자ID", "사용자명", "사용자IP", "시스템명", privacyTypeName, "수행업무", tmpName};
		}else {
			columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "result_content", "req_context"};
			heads = new String[] {"기간", "소속", "사용자ID", "사용자명", "사용자IP", "시스템명", privacyTypeName, "수행업무"};
		}
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", allLogInq);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
		
	}

	@Override
	public void connectLogResultListCsv(DataModelAndView modelAndView, SearchSearch search,
			HttpServletRequest request) {
		search.setUseExcel("true");
		String fileName = "유형통계_접속기록조회_상세" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		
		HttpSession session = request.getSession();
		// 개인정보유형 뷰 : 마스킹 처리 유무 Y 또는 N
		String result_type_view = (String) session.getAttribute("result_type");
		// 메뉴명 유무 : 메뉴명 컬럼 노출 여부 1은 노출, 2는 숨김, 3 미사용
		String scrn_name_view = (String) session.getAttribute("scrn_name");
		List<String> list = (List<String>) session.getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) session.getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
	    String ui_type = (String)session.getAttribute("ui_type");
		if(search.getStart_h() != null) {
			String start_time = search.getStart_h().substring(0);
			if(start_time.length() < 2) {
				start_time = "0" + start_time;
			}
			search.setStart_time(start_time);
		}
		if(search.getEnd_h() != null) {
			String end_time = search.getEnd_h().substring(0);
			if(end_time.length() < 2) {
				end_time = "0" + end_time;
			}
			search.setEnd_time(end_time);
		}
		
		String privacyTypeName = statisticsDao.privacyTypeName(search.getPrivacyType());
		SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
		SimpleDateFormat parseSdf = new SimpleDateFormat("yyyyMMddHHmmss");
		// 결과정보
		List<AllLogInq> allLogInq = statisticsDao.connectLogResultList(search);
		try {
			if(result_type_view.equals("Y")) {
				for(AllLogInq v : allLogInq) {
					String temp = v.getResult_content();
					if(temp != null && temp !="") {
						temp = getResultContextByMasking(temp, v.getResult_type());
						v.setResult_content(temp);
					}
					String dateVal = v.getProc_date()+v.getProc_time();
					Date date = parseSdf.parse(dateVal);
					v.setProc_date(sdf.format(date));
				}
			}
		} catch (Exception e) {
		}
		
		List<Map<String, Object>> mapList = new ArrayList<>();

		for(AllLogInq ali : allLogInq) {
			Map<String, Object> map = new HashMap<>();
			map.put("proc_date", ali.getProc_date());
			map.put("dept_name", ali.getDept_name());
			map.put("emp_user_id", ali.getEmp_user_id());
			map.put("emp_user_name", ali.getEmp_user_name());
			map.put("user_ip", ali.getUser_ip());
			map.put("system_name", ali.getSystem_name());
			map.put("result_content", ali.getResult_content());
			map.put("req_context", ali.getReq_context());
			map.put("scrn_name", ali.getScrn_name());
			map.put("req_url", ali.getReq_url());
			mapList.add(map);
		}
		
		
		String[] columns;
		String[] heads;
		if(scrn_name_view.equals("1")) {
			columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "result_content", "req_context", "scrn_name", "req_url"};
			heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "사용자IP", "시스템명", privacyTypeName, "수행업무", "메뉴명", "접근 경로"};
		}else if(scrn_name_view.equals("2")){
			String tmpName = "접근경로";
			String tmpName2 = "req_url";
			if(ui_type.equals("K")) {
				tmpName = "메뉴명";
				tmpName2 = "scrn_name"; 
			}
			columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "result_content", "req_context", tmpName2};
			heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "사용자IP", "시스템명", privacyTypeName, "수행업무", tmpName};
		}else {
			columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "result_content", "req_context"};
			heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "사용자IP", "시스템명", privacyTypeName, "수행업무"};
		}
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("excelData", mapList);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
		
	}

	@Override
	public void downloadLogResultListExcel(DataModelAndView modelAndView, SearchSearch search,
			HttpServletRequest request) {
		search.setUseExcel("true");
		String fileName = "유형통계_다운로드기록조회_상세" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "개인정보 다운로드기록조회";
		
		HttpSession session = request.getSession();
		// 개인정보유형 뷰 : 마스킹 처리 유무 Y 또는 N
		String result_type_view = (String) session.getAttribute("result_type");
		// 메뉴명 유무 : 메뉴명 컬럼 노출 여부 1은 노출, 2는 숨김, 3 미사용
		String scrn_name_view = (String) session.getAttribute("scrn_name");
		// 다운로드로그 풀스캔연동여부
		String use_fullscan = (String) session.getAttribute("use_fullscan");
		List<String> list = (List<String>) session.getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) session.getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
	    if(search.getStart_h() != null) {
			String start_time = search.getStart_h().substring(0);
			if(start_time.length() < 2) {
				start_time = "0" + start_time;
			}
			search.setStart_time(start_time);
		}
		if(search.getEnd_h() != null) {
			String end_time = search.getEnd_h().substring(0);
			if(end_time.length() < 2) {
				end_time = "0" + end_time;
			}
			search.setEnd_time(end_time);
		}
		String privacyTypeName = statisticsDao.privacyTypeName(search.getPrivacyType());
		SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
		SimpleDateFormat parseSdf = new SimpleDateFormat("yyyyMMddHHmmss");
		
		// 결과정보
		List<AllLogInq> allLogInq = statisticsDao.downloadLogResultList(search);
		try {
			if(result_type_view.equals("Y")) {
				for(AllLogInq v : allLogInq) {
					String temp = v.getResult_content();
					if(temp != null && temp !="") {
						temp = getResultContextByMasking(temp, v.getResult_type());
						v.setResult_content(temp);
					}
					String dateVal = v.getProc_date()+v.getProc_time();
					Date date = parseSdf.parse(dateVal);
					v.setProc_date(sdf.format(date));
				}
			}
		} catch (Exception e) {
		}
		
		String[] columns;
		String[] heads;
		if(scrn_name_view.equals("1")) {
			if(use_fullscan.equals("Y")) {
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "file_name", "result_content", "scrn_name", "reason"};
				heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "사용자IP", "시스템명", "파일명", privacyTypeName, "메뉴명", "사유"};
			} else {
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "file_name", "scrn_name", "reason"};
				heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "사용자IP", "시스템명", "파일명", "메뉴명", "사유"};
			}
		}else {
			if(use_fullscan.equals("Y")) {
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "file_name", "result_content", "reason"};
				heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "사용자IP", "시스템명", "파일명", privacyTypeName, "사유"};
			} else {
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "file_name", "reason"};
				heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "사용자IP", "시스템명", "파일명", "사유"};
			}
		}
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", allLogInq);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
		
	}

	@Override
	public void downloadLogResultListCsv(DataModelAndView modelAndView, SearchSearch search,
			HttpServletRequest request) {
		search.setUseExcel("true");
		String fileName = "유형통계_다운로드기록조회_상세" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		
		HttpSession session = request.getSession();
		// 개인정보유형 뷰 : 마스킹 처리 유무 Y 또는 N
		String result_type_view = (String) session.getAttribute("result_type");
		// 메뉴명 유무 : 메뉴명 컬럼 노출 여부 1은 노출, 2는 숨김, 3 미사용
		String scrn_name_view = (String) session.getAttribute("scrn_name");
		// 다운로드로그 풀스캔연동여부
		String use_fullscan = (String) session.getAttribute("use_fullscan");
		List<String> list = (List<String>) session.getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) session.getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
	    if(search.getStart_h() != null) {
			String start_time = search.getStart_h().substring(0);
			if(start_time.length() < 2) {
				start_time = "0" + start_time;
			}
			search.setStart_time(start_time);
		}
		if(search.getEnd_h() != null) {
			String end_time = search.getEnd_h().substring(0);
			if(end_time.length() < 2) {
				end_time = "0" + end_time;
			}
			search.setEnd_time(end_time);
		}
		String privacyTypeName = statisticsDao.privacyTypeName(search.getPrivacyType());
		SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
		SimpleDateFormat parseSdf = new SimpleDateFormat("yyyyMMddHHmmss");
		
		// 결과정보
		List<AllLogInq> allLogInq = statisticsDao.downloadLogResultList(search);
		try {
			if(result_type_view.equals("Y")) {
				for(AllLogInq v : allLogInq) {
					String temp = v.getResult_content();
					if(temp != null && temp !="") {
						temp = getResultContextByMasking(temp, v.getResult_type());
						v.setResult_content(temp);
					}
					String dateVal = v.getProc_date()+v.getProc_time();
					Date date = parseSdf.parse(dateVal);
					v.setProc_date(sdf.format(date));
				}
			}
		} catch (Exception e) {
		}
		
		List<Map<String, Object>> mapList = new ArrayList<>();

		for(AllLogInq ali : allLogInq) {
			Map<String, Object> map = new HashMap<>();
			map.put("proc_date", ali.getProc_date());
			map.put("dept_name", ali.getDept_name());
			map.put("emp_user_id", ali.getEmp_user_id());
			map.put("emp_user_name", ali.getEmp_user_name());
			map.put("user_ip", ali.getUser_ip());
			map.put("system_name", ali.getSystem_name());
			map.put("file_name", ali.getFile_name());
			map.put("result_content", ali.getResult_content());
			map.put("req_context", ali.getReq_context());
			map.put("scrn_name", ali.getScrn_name());
			map.put("reason", ali.getReason());
			mapList.add(map);
		}
		
		String[] columns;
		String[] heads;
		if(scrn_name_view.equals("1")) {
			if(use_fullscan.equals("Y")) {
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "file_name", "result_content", "scrn_name", "reason"};
				heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "사용자IP", "시스템명", "파일명", privacyTypeName, "메뉴명", "사유"};
			} else {
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "file_name", "scrn_name", "reason"};
				heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "사용자IP", "시스템명", "파일명", "메뉴명", "사유"};
			}
		}else {
			if(use_fullscan.equals("Y")) {
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "file_name", "result_content", "reason"};
				heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "사용자IP", "시스템명", "파일명", privacyTypeName, "사유"};
			} else {
				columns = new String[] {"proc_date", "dept_name", "emp_user_id", "emp_user_name", "user_ip", "system_name", "file_name", "reason"};
				heads = new String[] {"일시", "소속", "사용자ID", "사용자명", "사용자IP", "시스템명", "파일명", "사유"};
			}
		}
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("excelData", mapList);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}

	
	
	

}
