package com.easycerti.eframe.psm.statistics.service;

import java.text.ParseException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.search.vo.SearchSearch;

/**
 * 통계관리 Service Interface
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 *   
 *
 * </pre>
 */
public interface StatisticsSvc {
	
	public DataModelAndView findPerdbyStatisticsList(Map<String, String> parameters, HttpServletRequest request) throws ParseException;
	/**
	 * 기간별 유형분석 리스트
	 * @author yrchoo
	 * @since 2015. 4. 21.
	 * @return DataModelAndView
	 */
	public DataModelAndView findPerdbyTypeAnalsList(Map<String, String> parameters, HttpServletRequest request) throws ParseException;
	
	/**
	 * 시스템별 유형분석 리스트
	 * @author yrchoo
	 * @since 2015. 4. 21.
	 * @return DataModelAndView
	 */
	public DataModelAndView findSysbyTypeAnalsList(Map<String, String> parameters, HttpServletRequest request) throws ParseException;
	
	/**
	 * 부서별 유형분석 리스트
	 * @author yrchoo
	 * @since 2015. 4. 21.
	 * @return DataModelAndView
	 */
	public DataModelAndView findDeptbyTypeAnalsList(Map<String, String> parameters, HttpServletRequest request) throws ParseException;
	
	/**
	 * 개인별 유형분석 리스트
	 * @author yrchoo
	 * @since 2015. 5. 7.
	 * @return DataModelAndView
	 */
	public DataModelAndView findIndvbyTypeAnalsList(Map<String, String> parameters, HttpServletRequest request) throws ParseException;
	
	//개인별 위험도 리스트
	public DataModelAndView findIndvbyDngValAnalsList(Map<String, String> parameters, HttpServletRequest request) throws ParseException;
	
	public DataModelAndView findAllLogInqAnalsList(Map<String, String> parameters) throws ParseException;
	
	public DataModelAndView findRegularInspection(Map<String, String> parameters) throws ParseException;
	
	public DataModelAndView findInspection_hist(Map<String, String> parameters) throws ParseException;
	public DataModelAndView findInspection_hist_detail(Map<String, String> parameters) throws ParseException;
	public DataModelAndView addInspection_hist(Map<String, String> parameters);
	public DataModelAndView saveInspection_hist(Map<String, String> parameters);
	
	public void perdSystemdownload(DataModelAndView modelAndView,  Map<String, String> parameters, HttpServletRequest request);
	public void findPerdbyTypeAnalsList_download(DataModelAndView modelAndView,  Map<String, String> parameters, HttpServletRequest request);
	
	public void findSysbyTypeAnalsList_download(DataModelAndView modelAndView,  Map<String, String> parameters, HttpServletRequest request);
	
	public void findDeptbyTypeAnalsList_download(DataModelAndView modelAndView,  Map<String, String> parameters, HttpServletRequest request);

	public void findIndvbyTypeAnalsList_download(DataModelAndView modelAndView,  Map<String, String> parameters, HttpServletRequest request);
	
	public void findIndvbyDngValAnalsList_download(DataModelAndView modelAndView,  Map<String, String> parameters, HttpServletRequest request);

	
	public DataModelAndView connectLogResultList(SearchSearch search, HttpServletRequest request);
	public DataModelAndView downloadLogResultList(SearchSearch search, HttpServletRequest request);
	
	public void connectLogResultListExcel(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);
	public void connectLogResultListCsv(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);
	public void downloadLogResultListExcel(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);
	public void downloadLogResultListCsv(DataModelAndView modelAndView, SearchSearch search, HttpServletRequest request);
}
