package com.easycerti.eframe.psm.statistics.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.service.CommonService;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.statistics.service.StatisticsSvc;

/**
 * 통계 Controller
 * @author yrchoo
 * @since 2015. 4. 21.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 21.           yrchoo            최초 생성
 *
 * </pre>
 */
@Controller
@RequestMapping("/statistics/*")
public class StatisticsCtrl {
	
	@Autowired
	private StatisticsSvc statisticsService;
	
	@Autowired
	private CommonService commonService;
	
	@Value("#{configProperties.use_studentId}")
	private String use_studentId;
	
	@Value("#{configProperties.excel_type}")
	private String excel_type;
	
	@Value("#{versionProperties.version}")
	private String version;
	
	@MngtActHist(log_action = "SELECT", log_message = "[통계보고] 기간별시스템별처리량")
	@RequestMapping(value="perdbyStatisticsList.html",method={RequestMethod.POST})
	public DataModelAndView findPerdbyStatisticsList(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws ParseException{
		
		parameters = CommonHelper.getDateMonth(parameters);
		DataModelAndView modelAndView = statisticsService.findPerdbyStatisticsList(parameters, request);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName("perdbyStatisticsList");
		
		return modelAndView;
	}
	@RequestMapping(value="perdSystemdownload.html", method={RequestMethod.POST})
	public DataModelAndView addperdSystemdownload(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		statisticsService.perdSystemdownload(modelAndView, parameters, request);
		return modelAndView;
	}
	/**
	 * 기간별 유형분석
	 * @author yrchoo
	 * @since 2015. 4. 21.
	 * @return DataModelAndView
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[통계보고] 기간별유형통계")
	@RequestMapping(value="list_perdbyTypeAnals.html",method={RequestMethod.POST})
	public DataModelAndView findPerdbyTypeAnalsList(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws ParseException{
		
		parameters = CommonHelper.getDateMonth(parameters);
		parameters.put("use_studentId", use_studentId);
		DataModelAndView modelAndView = statisticsService.findPerdbyTypeAnalsList(parameters, request);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("use_studentId", use_studentId);
		modelAndView.setViewName("perdbyTypeAnalsList");
		
		return modelAndView;
	}
	
	@RequestMapping(value="download.html", method={RequestMethod.POST})
	public DataModelAndView addPerdbyTypeAnalsList_download(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		statisticsService.findPerdbyTypeAnalsList_download(modelAndView, parameters, request);
		
		return modelAndView;
	}

	/**
	 * 시스템별 유형분석
	 * @author yrchoo
	 * @since 2015. 4. 21.
	 * @return DataModelAndView
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[통계보고] 시스템별유형통계")
	@RequestMapping(value = { "list_sysbyTypeAnals.html" }, method = { RequestMethod.POST })
	public DataModelAndView findSysbyTypeAnalsList(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws ParseException {

		parameters = CommonHelper.getDateMonth(parameters);

		String isSearch = parameters.get("isSearch");
		
		parameters.put("use_studentId", use_studentId);
		DataModelAndView modelAndView = statisticsService.findSysbyTypeAnalsList(parameters, request);
		modelAndView.addObject("isSearch", isSearch);
		modelAndView.addObject("use_studentId", use_studentId);
		modelAndView.setViewName("sysbyTypeAnalsList");

		return modelAndView;
	}
	
	@RequestMapping(value="sysdownload.html", method={RequestMethod.POST})
	public DataModelAndView addSysbyTypeAnalsList_download(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		statisticsService.findSysbyTypeAnalsList_download(modelAndView, parameters, request);
		
		return modelAndView;
	}
	
	/**
	 * 부서별 유형분석
	 * @author yrchoo
	 * @since 2015. 4. 21.
	 * @return DataModelAndView
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[통계보고] 소속별유형통계")
	@RequestMapping(value = { "list_deptbyTypeAnals.html" }, method = { RequestMethod.POST })
	public DataModelAndView findDeptbyTypeAnalsList(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws ParseException {

		parameters = CommonHelper.getDateMonth(parameters);
		
		// 선택할 부서 리스트 
		//List<SimpleCode> departs = commonService.getAvailableDepartments();
		String isSearch = parameters.get("isSearch");
		
		parameters.put("use_studentId", use_studentId);
		DataModelAndView modelAndView = statisticsService.findDeptbyTypeAnalsList(parameters, request);
		
		//modelAndView.addObject("departs", departs);
		modelAndView.addObject("isSearch", isSearch);
		modelAndView.addObject("use_studentId", use_studentId);
		modelAndView.setViewName("deptbyTypeAnalsList");

		return modelAndView;
	}
	
	@RequestMapping(value="deptdownload.html", method={RequestMethod.POST})
	public DataModelAndView addDeptbyTypeAnalsList_download(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		statisticsService.findDeptbyTypeAnalsList_download(modelAndView, parameters, request);
		
		return modelAndView;
	}
	
	@RequestMapping(value="indvdownload.html", method={RequestMethod.POST})
	public DataModelAndView addIndvbyTypeAnalsList_download(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		statisticsService.findIndvbyTypeAnalsList_download(modelAndView, parameters, request);
		
		return modelAndView;
	}
	
	
	/**
	 * 개인별 유형분석
	 * @author yrchoo
	 * @since 2015. 5. 7.
	 * @return DataModelAndView
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[통계보고] 개인별유형통계")
	@RequestMapping(value = { "list_indvbyTypeAnals.html" }, method = { RequestMethod.POST })
	public DataModelAndView findIndvbyTypeAnalsList(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws ParseException {

		parameters = CommonHelper.getDateMonth(parameters);
		
		// 선택할 부서 리스트 
		//List<SimpleCode> departs = commonService.getAvailableDepartments();
		String isSearch = parameters.get("isSearch");
		
		parameters.put("use_studentId", use_studentId);
		DataModelAndView modelAndView = statisticsService.findIndvbyTypeAnalsList(parameters,request);
		
		//modelAndView.addObject("departs", departs);
		modelAndView.addObject("isSearch", isSearch);
		modelAndView.addObject("use_studentId", use_studentId);
		modelAndView.setViewName("indvbyTypeAnalsList");

		return modelAndView;
	}
	
	
	@RequestMapping(value = { "list_allLogInqAnals.html" }, method = { RequestMethod.POST })
	public DataModelAndView findAllLogInqAnals(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws ParseException {

		parameters = CommonHelper.checkSearchDate(parameters);
		
		DataModelAndView modelAndView = statisticsService.findAllLogInqAnalsList(parameters);
		
		String isSearch = parameters.get("isSearch");
		modelAndView.addObject("isSearch", isSearch);
		modelAndView.setViewName("allLogInqAnalsList");

		return modelAndView;
	}
	
	@MngtActHist(log_action = "SELECT", log_message = "[환경관리] 정기점검")
	@RequestMapping(value = { "regular_inspection.html" }, method = { RequestMethod.POST })
	public DataModelAndView findRegularInspection(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws ParseException {

		parameters = CommonHelper.checkSearchDate(parameters);
		/*String search_from = parameters.get("search_from");
		String[] array = search_from.split("-");
		search_from = array[0] + "-" + array[1] + "-" + "01";
		parameters.put("search_from", search_from);*/
		
		DataModelAndView modelAndView = statisticsService.findRegularInspection(parameters);
		modelAndView.addObject("version", version);
		modelAndView.setViewName("regular_inspection");

		return modelAndView;
	}
	
	@RequestMapping(value = { "inspection_hist.html" }, method = { RequestMethod.POST })
	public DataModelAndView findRegularInspection_hist(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws ParseException {

		parameters = CommonHelper.checkSearchDate(parameters, 30);
		
		DataModelAndView modelAndView = statisticsService.findInspection_hist(parameters);
		
		modelAndView.setViewName("inspectionHistList");

		return modelAndView;
	}
	
	@RequestMapping(value = { "inspection_hist_detail.html" }, method = { RequestMethod.POST })
	public DataModelAndView findRegularInspection_hist_detail(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws ParseException {

		DataModelAndView modelAndView = statisticsService.findInspection_hist_detail(parameters);
		
		modelAndView.setViewName("inspectionHistDetail");

		return modelAndView;
	}
	
	@RequestMapping(value={"addInspectionHist.html"}, method={RequestMethod.POST})
	public DataModelAndView addInspectionHist(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		parameters = CommonHelper.checkSearchDate(parameters);
		
		DataModelAndView modelAndView = statisticsService.addInspection_hist(parameters); 
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	@RequestMapping(value={"saveInspectionHist.html"}, method={RequestMethod.POST})
	public DataModelAndView saveInspectionHist(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		parameters = CommonHelper.checkSearchDate(parameters);
		
		DataModelAndView modelAndView = statisticsService.saveInspection_hist(parameters); 
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	/**
	 * 개인별 위험도
	 * @author yrchoo
	 * @since 2015. 5. 7.
	 * @return DataModelAndView
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[빅데이터분석] 개인별 위험도 통계")
	@RequestMapping(value = { "list_indvbyDngValAnals.html" }, method = { RequestMethod.POST })
	public DataModelAndView findIndvbyDngValAnalsList(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws ParseException {

		//parameters = CommonHelper.checkSearchDate(parameters);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);
		Calendar calendar = Calendar.getInstance();
		
		if ((parameters.get("search_from") == null) && (parameters.get("search_to") == null)) {
			
			calendar.add(Calendar.DAY_OF_MONTH, -1);
			parameters.put("search_from", sdf.format(calendar.getTime()));
			parameters.put("search_to", sdf.format(calendar.getTime()));
		}
		
		// 선택할 부서 리스트 
		//List<SimpleCode> departs = commonService.getAvailableDepartments();
		String isSearch = parameters.get("isSearch");
		
		DataModelAndView modelAndView = statisticsService.findIndvbyDngValAnalsList(parameters,request);
		
		//modelAndView.addObject("departs", departs);
		modelAndView.addObject("isSearch", isSearch);
		modelAndView.setViewName("indvbyDngValAnalsList");

		return modelAndView;
	}
	@RequestMapping(value="indvDngValdownload.html", method={RequestMethod.POST})
	public DataModelAndView addIndvbyDngValAnalsList_download(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		statisticsService.findIndvbyDngValAnalsList_download(modelAndView, parameters, request);
		
		return modelAndView;
	}
	
	
	
	@MngtActHist(log_action = "SELECT", log_message = "[통계보고] 통계보고 상세")
	@RequestMapping(value="connectLogResultList.html", method={RequestMethod.POST})
	public DataModelAndView connectLogResultList(@ModelAttribute("search") SearchSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request, String shType) {
		DataModelAndView modelAndView = statisticsService.connectLogResultList(search, request);
		modelAndView.addObject("shType", shType);
		modelAndView.setViewName("connectLogResultList");
		return modelAndView;
	}
	@RequestMapping(value="downloadLogResultList.html", method={RequestMethod.POST})
	public DataModelAndView downloadLogResultList(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request, String shType) {
		DataModelAndView modelAndView = statisticsService.downloadLogResultList(search, request);
		modelAndView.addObject("shType", shType);
		modelAndView.setViewName("downloadLogResultList");
		return modelAndView;
	}
	
	@RequestMapping(value="connectLogResultListExcel.html", method={RequestMethod.POST})
	public DataModelAndView addconnectLogResultListExcel(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		statisticsService.connectLogResultListExcel(modelAndView, search, request);
		return modelAndView;
	}
	
	@RequestMapping(value="connectLogResultListCsv.html", method={RequestMethod.POST})
	public DataModelAndView addconnectLogResultListCsv(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.CSV_DOWNLOAD_VIEW);
		statisticsService.connectLogResultListCsv(modelAndView, search, request);
		return modelAndView;
	}
	@RequestMapping(value="downloadLogResultListExcel.html", method={RequestMethod.POST})
	public DataModelAndView downloadLogResultListExcel(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		statisticsService.downloadLogResultListExcel(modelAndView, search, request);
		return modelAndView;
	}
	
	@RequestMapping(value="downloadLogResultListCsv.html", method={RequestMethod.POST})
	public DataModelAndView downloadLogResultListCsv(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.CSV_DOWNLOAD_VIEW);
		statisticsService.downloadLogResultListCsv(modelAndView, search, request);
		return modelAndView;
	}
}
