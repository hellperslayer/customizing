package com.easycerti.eframe.psm.statistics.vo;

import java.util.List;

import com.easycerti.eframe.common.util.PageInfo;
import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 통계관리 Bean
 * 
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 *   
 *
 * </pre>
 */
public class Statistics extends AbstractValueObject {
	
	// 로그수집일자
	private String proc_date;
	
	// 주민등록번호
	private int type1;
	
	// 운전면허번호
	private int type2;
	
	// 여권번호
	private int type3;
	
	// 신용카드번호
	private int type4;
	
	// 건강보험번호
	private int type5;
	
	// 전화번호
	private int type6;
	
	// 이메일
	private int type7;
	
	// 핸드폰번호
	private int type8;
	
	// 은행계좌번호
	private int type9;
	
	// 외국인등록번호
	private int type10;
	private int type11;
	private int type12;
	private int type13;
	private int type14;
	private int type15;
	private int type31;
	private int type32;
	private int type33;
	// 학번
	private int type99;
	
	private int type16;
	private int type17;
	private int type18;
	
	private String report_type;
	private String patch_type;
	
	private String patch_month;
	private String hist_month;
	
	//패치테이블 컬럼
	private String patch_sort;
	private String patch_version;
	private String patch_date;
	private String script;
	private String description;
	
	private String req_type;
	
	private List<String> req_type_list;
	
	
	public List<String> getReq_type_list() {
		return req_type_list;
	}

	public void setReq_type_list(List<String> req_type_list) {
		this.req_type_list = req_type_list;
	}

	public String getReq_type() {
		return req_type;
	}

	public void setReq_type(String req_type) {
		this.req_type = req_type;
	}

	public String getPatch_sort() {
		return patch_sort;
	}

	public Statistics setPatch_sort(String patch_sort) {
		this.patch_sort = patch_sort;
		return this;
	}

	public String getPatch_version() {
		return patch_version;
	}

	public void setPatch_version(String patch_version) {
		this.patch_version = patch_version;
	}

	public String getPatch_date() {
		return patch_date;
	}

	public void setPatch_date(String patch_date) {
		this.patch_date = patch_date;
	}

	public String getScript() {
		return script;
	}

	public void setScript(String script) {
		this.script = script;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPatch_month() {
		return patch_month;
	}

	public void setPatch_month(String patch_month) {
		this.patch_month = patch_month;
	}

	public String getHist_month() {
		return hist_month;
	}

	public void setHist_month(String hist_month) {
		this.hist_month = hist_month;
	}

	public String getPatch_type() {
		return patch_type;
	}

	public void setPatch_type(String patch_type) {
		this.patch_type = patch_type;
	}

	public String getReport_type() {
		return report_type;
	}

	public void setReport_type(String report_type) {
		this.report_type = report_type;
	}

	public int getType16() {
		return type16;
	}

	public void setType16(int type16) {
		this.type16 = type16;
	}

	public int getType17() {
		return type17;
	}

	public void setType17(int type17) {
		this.type17 = type17;
	}

	public int getType18() {
		return type18;
	}

	public void setType18(int type18) {
		this.type18 = type18;
	}

	private int type_sum;
	
	private int logcnt;
	
	// 일, 월, 연도별 검색 시 사용 로그수집일자
	private String proc_date_statistics;

	// 업무시스템 ID
	private String system_seq;
	
	//메뉴 ID 
	private String sub_menu_id;

	// 업무시스템 이름
	private String system_name;
	
	// 부서 ID
	private String dept_id;
	
	// 부서 이름
	private String dept_name;
	
	// 사원 ID
	private String emp_user_id;
	
	// 사원 이름
	private String emp_user_name;

	// 일, 월, 연도 체크 타입
	private String chk_date_type;
	
	private String row_type;
	
	// 페이징 정보
	private PageInfo pageInfo;
	
	//정렬 타입 
	private String check_type;
	
	//오름차순, 내림차순 정보 
	private int sort_flag;
	
	private String select_year;
	private String select_month;
	
	private String start_date;
	private String end_date;
	
	
	private int seq;
	private String proc_month;
	private String inspector;
	private String next_date;
	private String contents;
	private String manager;
	List<String> auth_idsList;
	private int dng_val;
	
	private String searchType;
	private String log_delimiter;
	
	
	public String getLog_delimiter() {
		return log_delimiter;
	}

	public void setLog_delimiter(String log_delimiter) {
		this.log_delimiter = log_delimiter;
	}

	public int getLogcnt() {
		return logcnt;
	}

	public void setLogcnt(int logcnt) {
		this.logcnt = logcnt;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public int getDng_val() {
		return dng_val;
	}

	public void setDng_val(int dng_val) {
		this.dng_val = dng_val;
	}

	public List<String> getAuth_idsList() {
		return auth_idsList;
	}

	public void setAuth_idsList(List<String> auth_idsList) {
		this.auth_idsList = auth_idsList;
	}

	public String getCheck_type() {
		return check_type;
	}

	public void setCheck_type(String check_type) {
		this.check_type = check_type;
	}

	public int getSort_flag() {
		return sort_flag;
	}

	public void setSort_flag(int sort_flag) {
		this.sort_flag = sort_flag;
	}

	public String getProc_date() {
		return proc_date;
	}

	public void setProc_date(String proc_date) {
		this.proc_date = proc_date;
	}

	
	public int getType1() {
		return type1;
	}

	public void setType1(int type1) {
		this.type1 = type1;
	}

	public int getType2() {
		return type2;
	}

	public void setType2(int type2) {
		this.type2 = type2;
	}

	public int getType3() {
		return type3;
	}

	public void setType3(int type3) {
		this.type3 = type3;
	}

	public int getType4() {
		return type4;
	}

	public void setType4(int type4) {
		this.type4 = type4;
	}

	public int getType5() {
		return type5;
	}

	public void setType5(int type5) {
		this.type5 = type5;
	}

	public int getType6() {
		return type6;
	}

	public void setType6(int type6) {
		this.type6 = type6;
	}

	public int getType7() {
		return type7;
	}

	public void setType7(int type7) {
		this.type7 = type7;
	}

	public int getType8() {
		return type8;
	}

	public void setType8(int type8) {
		this.type8 = type8;
	}

	public int getType9() {
		return type9;
	}

	public void setType9(int type9) {
		this.type9 = type9;
	}

	public int getType10() {
		return type10;
	}

	public void setType10(int type10) {
		this.type10 = type10;
	}
	
	public int getType11() {
		return type11;
	}

	public void setType11(int type11) {
		this.type11 = type11;
	}

	public int getType12() {
		return type12;
	}

	public void setType12(int type12) {
		this.type12 = type12;
	}

	public int getType13() {
		return type13;
	}

	public void setType13(int type13) {
		this.type13 = type13;
	}

	public int getType14() {
		return type14;
	}

	public void setType14(int type14) {
		this.type14 = type14;
	}
	
	public int getType15() {
		return type15;
	}

	public void setType15(int type15) {
		this.type15 = type15;
	}

	public int getType31() {
		return type31;
	}

	public void setType31(int type31) {
		this.type31 = type31;
	}

	public int getType32() {
		return type32;
	}

	public void setType32(int type32) {
		this.type32 = type32;
	}

	public int getType33() {
		return type33;
	}

	public void setType33(int type33) {
		this.type33 = type33;
	}


	public int getType99() {
		return type99;
	}

	public void setType99(int type99) {
		this.type99 = type99;
	}
	
	public int getType_sum() {
		return type_sum;
	}

	public void setType_sum(int type_sum) {
		this.type_sum = type_sum;
	}

	public String getProc_date_statistics() {
		return proc_date_statistics;
	}

	public void setProc_date_statistics(String proc_date_statistics) {
		this.proc_date_statistics = proc_date_statistics;
	}

	public String getSystem_seq() {
		return system_seq;
	}

	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}
	
	
	public String getSub_menu_id() {
		return sub_menu_id;
	}

	public void setSub_menu_id(String sub_menu_id) {
		this.sub_menu_id = sub_menu_id;
	}


	public String getSystem_name() {
		return system_name;
	}

	public void setSystem_name(String system_name) {
		this.system_name = system_name;
	}

	public String getDept_id() {
		return dept_id;
	}

	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}

	public String getDept_name() {
		return dept_name;
	}

	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}

	public String getEmp_user_id() {
		return emp_user_id;
	}

	public void setEmp_user_id(String emp_user_id) {
		this.emp_user_id = emp_user_id;
	}

	public String getEmp_user_name() {
		return emp_user_name;
	}

	public void setEmp_user_name(String emp_user_name) {
		this.emp_user_name = emp_user_name;
	}

	public String getChk_date_type() {
		return chk_date_type;
	}

	public void setChk_date_type(String chk_date_type) {
		this.chk_date_type = chk_date_type;
	}
	
	public PageInfo getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(PageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}

	public String getRow_type() {
		return row_type;
	}

	public void setRow_type(String row_type) {
		this.row_type = row_type;
	}
 
	public String getSelect_year() {
		return select_year;
	}

	public void setSelect_year(String select_year) {
		this.select_year = select_year;
	}

	public String getSelect_month() {
		return select_month;
	}

	public void setSelect_month(String select_month) {
		this.select_month = select_month;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public String getProc_month() {
		return proc_month;
	}

	public void setProc_month(String proc_month) {
		this.proc_month = proc_month;
	}

	public String getInspector() {
		return inspector;
	}

	public void setInspector(String inspector) {
		this.inspector = inspector;
	}

	public String getNext_date() {
		return next_date;
	}

	public void setNext_date(String next_date) {
		this.next_date = next_date;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getManager() {
		return manager;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}	
	
}
