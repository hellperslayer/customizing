package com.easycerti.eframe.psm.setup.service;

import java.util.Map;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;

public interface ExtraBizLogMappSetupSvc {
	
	
	public DataModelAndView findExtraBizLogMappSetupList(SetupSearch search);
	
	public DataModelAndView findExtraBizLogMappSetupDetail(SetupSearch search);
	
	public DataModelAndView addExtraBizLogMappSetupOne(Map<String, String> parameters);
	
	public DataModelAndView saveExtraBizLogMappSetupOne(Map<String, String> parameters);
	
	public DataModelAndView removeExtraBizLogMappSetupOne(Map<String, String> parameters);

	//public void findMenuMappSetupList_download(DataModelAndView modelAndView, SetupSearch search, HttpServletRequest request);
	//public String upLoadSystemMngtOne(Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response)throws Exception;
	
}
