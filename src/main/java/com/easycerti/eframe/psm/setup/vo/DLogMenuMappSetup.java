package com.easycerti.eframe.psm.setup.vo;
import com.easycerti.eframe.common.util.PageInfo;
import com.easycerti.eframe.common.vo.AbstractValueObject;
/**
 * 
 * 설명 : 태그관리 VO
 * @author tjlee
 * @since 2015. 4. 27.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 27.           tjlee            최초 생성
 *
 * </pre>
 */
public class DLogMenuMappSetup extends AbstractValueObject{
	private int num; //row_num 번호
	private int dlog_tag_seq;          // 태그관리번호
	private String url;           //  URL주소
	private String parameter;     // 파라미터
	private String menu_name;     // 메뉴명
	private String use_yn;        // 사용여부
	private String system_seq;        // 시스템코드
	private String system_name; // 시스템명
	private PageInfo pageInfo; // 페이징 변수 
	private String file_name;
	private String reason;
	
	private String register_status;
	private String threshold;
	
	private String patch_type;
	private String patch_version;
	private String patch_date;
	private String script;
	private String description;
	private String patch_sort;
	
	
	
	
	
	public String getPatch_type() {
		return patch_type;
	}
	public void setPatch_type(String patch_type) {
		this.patch_type = patch_type;
	}
	public String getPatch_version() {
		return patch_version;
	}
	public void setPatch_version(String patch_version) {
		this.patch_version = patch_version;
	}
	public String getPatch_date() {
		return patch_date;
	}
	public void setPatch_date(String patch_date) {
		this.patch_date = patch_date;
	}
	public String getScript() {
		return script;
	}
	public void setScript(String script) {
		this.script = script;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPatch_sort() {
		return patch_sort;
	}
	public void setPatch_sort(String patch_sort) {
		this.patch_sort = patch_sort;
	}
	public String getThreshold() {
		return threshold;
	}
	public void setThreshold(String threshold) {
		this.threshold = threshold;
	}
	public int getDlog_tag_seq() {
		return dlog_tag_seq;
	}
	public void setDlog_tag_seq(int dlog_tag_seq) {
		this.dlog_tag_seq = dlog_tag_seq;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getParameter() {
		return parameter;
	}
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}
	public String getMenu_name() {
		return menu_name;
	}
	public void setMenu_name(String menu_name) {
		this.menu_name = menu_name;
	}
	public String getUse_yn() {
		return use_yn;
	}
	public void setUse_yn(String use_yn) {
		this.use_yn = use_yn;
	}
	public String getSystem_seq() {
		return system_seq;
	}
	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}
	public String getSystem_name() {
		return system_name;
	}
	public void setSystem_name(String system_name) {
		this.system_name = system_name;
	}
	public PageInfo getPageInfo() {
		return pageInfo;
	}
	public void setPageInfo(PageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	@Override
	public String toString() {
		return "DLogMenuMappSetup [dlog_tag_seq=" + dlog_tag_seq + ", url=" + url + ", parameter=" + parameter
				+ ", menu_name=" + menu_name + ", use_yn=" + use_yn + ", system_seq=" + system_seq + ", system_name="
				+ system_name + ", pageInfo=" + pageInfo + ", file_name=" + file_name + ", reason=" + reason + "]";
	}
	public String getRegister_status() {
		return register_status;
	}
	public void setRegister_status(String register_status) {
		this.register_status = register_status;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
}
