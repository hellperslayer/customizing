package com.easycerti.eframe.psm.setup.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;

public interface SqlMappSetupSvc {

	public DataModelAndView findSqlMappSetupList(SetupSearch search, HttpServletRequest request);
	
	public DataModelAndView findSqlMappSetupDetail(SetupSearch search);
	
	public DataModelAndView removeSqlMappSetupOne(Map<String, String> parameters);
	
	public void findSqlMappSetupList_download(DataModelAndView modelAndView, SetupSearch search, HttpServletRequest request);
	
	public String upLoadSystemMngtOne(Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception;
	
}
