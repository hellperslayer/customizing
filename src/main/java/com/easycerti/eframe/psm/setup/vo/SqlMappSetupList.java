package com.easycerti.eframe.psm.setup.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.AbstractValueObject;

public class SqlMappSetupList extends AbstractValueObject{

	private List<SqlMappSetup> sqlMappSetup = null;
	
	public SqlMappSetupList() {
		
	}
	
	public SqlMappSetupList(List<SqlMappSetup> sqlMappSetup) {
		this.sqlMappSetup = sqlMappSetup;
	}
	
	public SqlMappSetupList(List<SqlMappSetup> sqlMappSetup, String page_total_count){
		this.sqlMappSetup = sqlMappSetup;
		setPage_total_count(page_total_count);
	}

	public List<SqlMappSetup> getSqlMappSetup() {
		return sqlMappSetup;
	}

	public void setSqlMappSetup(List<SqlMappSetup> sqlMappSetup) {
		this.sqlMappSetup = sqlMappSetup;
	}

	
}
