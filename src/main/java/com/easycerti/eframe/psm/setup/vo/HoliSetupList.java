package com.easycerti.eframe.psm.setup.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.AbstractValueObject;
/**
 * 전체로그조회 리스트 Bean
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 * 
 *
 * </pre>
 */
public class HoliSetupList extends AbstractValueObject{

	// 추가url 리스트
	private List<HoliSetup> holiSetup = null;

	
	public HoliSetupList(){
		
	}
	
	public HoliSetupList(List<HoliSetup> holiSetup){
		this.holiSetup = holiSetup;
	}
	
	public HoliSetupList(List<HoliSetup> holiSetup, String page_total_count){
		this.holiSetup = holiSetup;
		setPage_total_count(page_total_count);
	}

	public List<HoliSetup> getHoliSetup() {
		return holiSetup;
	}

	public void setHoliSetup(List<HoliSetup> holiSetup) {
		this.holiSetup = holiSetup;
	}
}
