package com.easycerti.eframe.psm.setup.dao;

import java.util.List;

import com.easycerti.eframe.psm.search.vo.AllLogInq;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.setup.vo.MenuMappSetup;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;
import com.easycerti.eframe.psm.setup.vo.SqlMappSetup;

public interface SqlMappSetupDao {

	public List<SqlMappSetup> findSqlMappSetupList(SetupSearch search);
	
	public int findSqlMappSetupOne_count(SetupSearch search);
	
	public SqlMappSetup findSqlMappSetupDetail(SetupSearch search);
	
	public List<SqlMappSetup> findSqlMappSetupDetailSql(SetupSearch search);
	
	public void removeSqlMappSetupOne(SqlMappSetup paramBean);
	
	public int uploadSqlMappSetupOne(SqlMappSetup paramBean);
	
	public void uploadSqlMappSetupDetailOne(SqlMappSetup paramBean);
	
	public int findSqlMappDuplicateCnt(SqlMappSetup paramBean);
	
	public List<AllLogInq> findSqlSentence(SearchSearch search);
	
	public List<SqlMappSetup> findSqlMappSetupDownload(SetupSearch search);
	
}
