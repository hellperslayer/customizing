package com.easycerti.eframe.psm.setup.vo;

import com.easycerti.eframe.common.vo.AbstractValueObject;

public class SqlMappSetup extends AbstractValueObject {

	private int tag_sql_url_seq;
	private String url;
	private String use_yn;
	private String system_seq;
	private int tag_sql_seq;
	private String sql;
	private String crud_type;
	private String system_name;

	public int getTag_sql_url_seq() {
		return tag_sql_url_seq;
	}

	public void setTag_sql_url_seq(int tag_sql_url_seq) {
		this.tag_sql_url_seq = tag_sql_url_seq;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUse_yn() {
		return use_yn;
	}

	public void setUse_yn(String use_yn) {
		this.use_yn = use_yn;
	}

	public String getSystem_seq() {
		return system_seq;
	}

	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}

	public int getTag_sql_seq() {
		return tag_sql_seq;
	}

	public void setTag_sql_seq(int tag_sql_seq) {
		this.tag_sql_seq = tag_sql_seq;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public String getCrud_type() {
		return crud_type;
	}

	public void setCrud_type(String crud_type) {
		this.crud_type = crud_type;
	}

	public String getSystem_name() {
		return system_name;
	}

	public void setSystem_name(String system_name) {
		this.system_name = system_name;
	}

	@Override
	public String toString() {
		return "SqlMappSetup [tag_sql_url_seq=" + tag_sql_url_seq + ", url=" + url + ", use_yn=" + use_yn
				+ ", system_seq=" + system_seq + ", tag_sql_seq=" + tag_sql_seq + ", sql=" + sql + ", crud_type="
				+ crud_type + ", system_name=" + system_name + "]";
	}


}
