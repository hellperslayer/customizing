package com.easycerti.eframe.psm.setup.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.setup.vo.ExtrtBaseSetup;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;
import com.easycerti.eframe.psm.system_management.vo.EmpUser;
import com.easycerti.eframe.psm.system_management.vo.EmpUserSearch;
/**
 * 
 * 설명 : 추출환경설정 Svc
 * @author tjlee
 * @since 2015. 5. 4.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 4.           tjlee            최초 생성
 *
 * </pre>
 */
public interface ExtrtBaseSetupSvc {
	
	public DataModelAndView findScenarioList(SetupSearch search);
	
	/**
	 * 설명 : 추출기준설정 리스트
	 * @author tjlee
	 * @since 2015. 6. 8.
	 * @return DataModelAndView
	 */
	public DataModelAndView scenarioDetail(Map<String, String> parameters);
	public DataModelAndView findExtrtBaseSetupList(SetupSearch search);
	
	/**
	 * 설명 : 추출기준설정 상세
	 * @author tjlee
	 * @since 2015. 6. 8.
	 * @return DataModelAndView
	 */
	public DataModelAndView findExtrtBaseSetupDetail(SetupSearch search);
	
	/**
	 * 설명 : 추출기준설정 추가
	 * @author tjlee
	 * @since 2015. 6. 8.
	 * @return DataModelAndView
	 */
	public DataModelAndView addExtrtBaseSetupOne(	Map<String, String> parameters);
	
	/**
	 * 설명 : 추출기준설정 수정
	 * @author tjlee
	 * @since 2015. 6. 8.
	 * @return DataModelAndView
	 */
	public DataModelAndView saveExtrtBaseSetupOne(Map<String, String> parameters);
	
	/**
	 * 설명 : 추출기준설정 삭제 
	 * @author tjlee
	 * @since 2015. 6. 8.
	 * @return DataModelAndView
	 */
	public DataModelAndView removeExtrtBaseSetupOne(Map<String, String> parameters);

	/**
	 * 설명 : 추출기준설정 엑셀 다운로드 
	 * @author tjlee
	 * @since 2015. 6. 8.
	 * @return void
	 */
	public void findExtrtBaseSetupList_download(DataModelAndView modelAndView, SetupSearch search, HttpServletRequest request);
	
	
	public DataModelAndView findEmpUserMngtList(EmpUserSearch search);
	public DataModelAndView findExtrtBaseSetupDetail_indv(EmpUser search);
	public DataModelAndView setExtrtBaseSetupDetail_indv(Map<String, String> parameters);
	public DataModelAndView setExtrtBaseSetupAll_indv(Map<String, String> parameters);
	
	public DataModelAndView saveScenario(Map<String, String> parameters);
	public DataModelAndView removeScenario(Map<String, String> parameters);
	public DataModelAndView addScenario(Map<String, String> parameters);
	
	public DataModelAndView findRuleTableByScenario(Map<String, String> parameters);
	public DataModelAndView thresholdSetting_indv(SetupSearch search);
	public String saveThreshold_indv(String jsonData);
	public DataModelAndView thresholdSetting_dept(SetupSearch search);
	public String saveThreshold_dept(String jsonData);
	
	public DataModelAndView thresholdSettingDetail_dept(Map<String, String> parameters, HttpServletRequest request);
	public DataModelAndView thresholdSettingDetail_indv(Map<String, String> parameters, HttpServletRequest request);
	
	public String deleteThreshold_indv(ExtrtBaseSetup baseSetup);
	public String deleteThreshold_dept(ExtrtBaseSetup baseSetup);
}
