package com.easycerti.eframe.psm.setup.web;

import java.io.File;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.psm.setup.service.MenuMappSetupSvc;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;
import com.easycerti.eframe.psm.system_management.web.EmpUserMngtCtrl;
/**
 * 
 * 설명 : 매뉴매핑설정 Controller 
 * @author tjlee
 * @since 2015. 4. 24.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 24.           tjlee            최초 생성
 *
 * </pre>
 */
@Controller
@RequestMapping("/menuMappSetup/*")
public class MenuMappSetupCtrl {
	@Autowired
	private MenuMappSetupSvc menuMappSetupSvc;
	/**
	 * 매뉴매핑설정 List
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[정책관리] 메뉴매핑설정")
	@RequestMapping(value={"list.html"}, method={RequestMethod.POST})
	public DataModelAndView findMenuMappSetupList(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = menuMappSetupSvc.findMenuMappSetupList(search,request); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("menuMappSetupList");

		return modelAndView;
	}
	
	/**
	 * 매뉴매핑설정상세
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[정책관리] 메뉴매핑설정 상세")
	@RequestMapping(value={"detail.html"}, method={RequestMethod.POST})
	public DataModelAndView findMenuMappSetupDetail(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = menuMappSetupSvc.findMenuMappSetupDetail(search); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("menuMappSetupDetail");

		return modelAndView;
	}
	
	/**
	 * 개인정보유형설정 엑셀 다운로드
	 */
	@RequestMapping(value="download.html", method={RequestMethod.POST})
	public DataModelAndView addMenuMappSetupList_download(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		menuMappSetupSvc.findMenuMappSetupList_download(modelAndView, search, request);
		
		return modelAndView;
	}
	
	
	/**
	 * 매뉴매핑설정 등록
	 */
	@RequestMapping(value={"add.html"}, method={RequestMethod.POST})
	public DataModelAndView addMenuMappSetup(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		parameters = CommonHelper.checkSearchDate(parameters);
		
		//String[] args = {"tag_seq", "url"};
		String[] args = {"url"};
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		DataModelAndView modelAndView = menuMappSetupSvc.addMenuMappSetupOne(parameters); 
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	/**
	 * 매뉴매핑설정 수정
	 */
	@RequestMapping(value={"save.html"}, method={RequestMethod.POST})
	public DataModelAndView saveMenuMappSetup(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		//parameters = CommonHelper.checkSearchDate(parameters);
		
		String[] args = {"url"};
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		DataModelAndView modelAndView = menuMappSetupSvc.saveMenuMappSetupOne(parameters); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	/**
	 * 매뉴매핑설정 삭제
	 */
	@RequestMapping(value={"remove.html"}, method={RequestMethod.POST})
	public DataModelAndView removeMenuMappSetup(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		parameters = CommonHelper.checkSearchDate(parameters);
		
		DataModelAndView modelAndView = menuMappSetupSvc.removeMenuMappSetupOne(parameters); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	@RequestMapping("exdownload.html")
	public ModelAndView download(HttpServletRequest request)throws Exception{
		
		String path = request.getSession().getServletContext().getRealPath("/")+"WEB-INF/";
		 //System.out.println(path);
		File down = new File(path + "excelExam/메뉴매핑_업로드양식.xlsx");
		return new ModelAndView("download","downloadFile",down);
	}
	
	@RequestMapping(value="upload.html", method={RequestMethod.POST})
	@ResponseBody
	public String upMenuMappSetupMngtOne(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
 		parameters.put("insert_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		String result = menuMappSetupSvc.upLoadSystemMngtOne(parameters,request, response);
		
		return result;
	}
	
	/**
	 * 매뉴별개인정보탐지설정
	 */
	@RequestMapping(value={"menuPrivFindList.html"}, method={RequestMethod.POST})
	public DataModelAndView findMenuPrivFindSetupList(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = menuMappSetupSvc.findMenuPrivFindSetupList(search); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("menuPrivFindList");

		return modelAndView;
	}
	
	/**
	 * 매뉴별개인정보탐지설정 추가
	 */
	@RequestMapping(value={"addPrivFind.html"}, method={RequestMethod.POST})
	public DataModelAndView addPrivFind(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		parameters = CommonHelper.checkSearchDate(parameters);
		
		DataModelAndView modelAndView = menuMappSetupSvc.addPrivFind(parameters); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	/**
	 * 매뉴별개인정보탐지설정 수정
	 */
	@RequestMapping(value={"savePrivFind.html"}, method={RequestMethod.POST})
	public DataModelAndView savePrivFind(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		parameters = CommonHelper.checkSearchDate(parameters);
		
		DataModelAndView modelAndView = menuMappSetupSvc.savePrivFind(parameters); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	/**
	 * 매뉴별개인정보탐지설정 삭제
	 */
	@RequestMapping(value={"removePrivFind.html"}, method={RequestMethod.POST})
	public DataModelAndView removePrivFind(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		parameters = CommonHelper.checkSearchDate(parameters);
		
		DataModelAndView modelAndView = menuMappSetupSvc.removePrivFind(parameters); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
}
