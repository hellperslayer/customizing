package com.easycerti.eframe.psm.setup.web;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.psm.setup.service.HoliSetupSvc;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;

@Controller
@RequestMapping("/holiSetup/*")
public class HoliSetupCtrl {
	
	@Autowired
	private HoliSetupSvc holiSetupSvc;
	
	/**
	 * 휴일 리스트
	 * @author yrchoo
	 * @since 2015. 4. 29.
	 * @return DataModelAndView
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[정책관리] 휴일설정관리")
	@RequestMapping(value="list.html", method={RequestMethod.POST})
	public DataModelAndView findHoliSetupList(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {

		Date today = new Date();
		Date before;
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, -1);
		before = cal.getTime();
		search.initDatesIfEmpty(before, today);
		DataModelAndView modelAndView = holiSetupSvc.findHoliSetupList(search);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("holiSetupList");
		
		return modelAndView;
	}
	
	/**
	 * 휴일 상세
	 * @author yrchoo
	 * @since 2015. 4. 29.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="detail.html",method={RequestMethod.POST})
	public DataModelAndView findAddUrlSetupOne(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception {
		
		DataModelAndView modelAndView = holiSetupSvc.findHoliSetupOne(search);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("holiSetupDetail");
		
		return modelAndView;
	}
	
	/**
	 * 휴일설정 엑셀 다운로드
	 * @author tjlee
	 * @since 2015. 6. 8. 
	 * @return DataModelAndView
	 */
	@RequestMapping(value="download.html", method={RequestMethod.POST})
	public DataModelAndView findAddUrlSetupList_download(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		holiSetupSvc.findAddUrlSetupList_download(modelAndView, search, request);
		
		return modelAndView;
	}
	
	/**
	 * 휴일 수정
	 * @author yrchoo
	 * @since 2015. 4. 29.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="save.html",method={RequestMethod.POST})
	public DataModelAndView saveAddUrlSetupOne(@RequestParam Map<String, String> parameters,HttpServletRequest request) throws Exception{
		
		DataModelAndView modelAndView = holiSetupSvc.saveHoliSetupOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}

	/**
	 * 휴일 일괄 등록
	 * @author yrchoo
	 * @since 2015. 4. 29.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="add.html",method={RequestMethod.POST})
	public DataModelAndView addAddUrlSetupOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception {
		
		DataModelAndView modelAndView = holiSetupSvc.addHoliSetup(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@RequestMapping(value="upload.html", method={RequestMethod.POST})
	@ResponseBody
	public String upLoadAccessAuth(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
		String result = holiSetupSvc.upLoadHoliSetup(parameters,request, response);
		return result;
	}
	
	@RequestMapping("exdownload.html")
	public ModelAndView download(HttpServletRequest request)throws Exception{
		String path = request.getSession().getServletContext().getRealPath("/")+"WEB-INF/";
		File down = new File(path + "excelExam/휴일설정_업로드양식.xlsx");
		return new ModelAndView("download","downloadFile",down);
	}

}
