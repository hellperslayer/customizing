package com.easycerti.eframe.psm.setup.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.psm.setup.service.IndvinfoTypeSetupSvc;
import com.easycerti.eframe.psm.setup.vo.IndvinfoTypeSetup;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;

/**
 * 
 * 설명 : 개인정보유형설정 Controller
 * @author tjlee
 * @since 2015. 4. 24.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 24.           tjlee            최초 생성
 *
 * </pre>
 */
@Controller
@RequestMapping("/indvinfoTypeSetup/*")
public class IndvinfoTypeSetupCtrl {
	@Autowired
	private IndvinfoTypeSetupSvc indvinfoTypeSetupSvc;
	/**
	 * 개인정보유형설정 List
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[정책관리] 개인정보주체자설정")
	@RequestMapping(value={"list.html"}, method={RequestMethod.POST,RequestMethod.GET})
	public DataModelAndView findIndvinfoTypeSetupList(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
		
		DataModelAndView modelAndView = indvinfoTypeSetupSvc.findIndvinfoTypeSetupList(search); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("indvinfoTypeSetupList");

		return modelAndView;
	}
	
	/**
	 * 개인정보유형설정상세
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[정책관리] 개인정보설정상세")
	@RequestMapping(value={"detail.html"}, method={RequestMethod.POST})
	public DataModelAndView findIndvinfoTypeSetupDetail(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = indvinfoTypeSetupSvc.findIndvinfoTypeSetupDetail(search); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("indvinfoTypeSetupDetail");

		return modelAndView;
	}
	
	/**
	 * 개인정보유형설정 엑셀 다운로드
	 */
	@RequestMapping(value="download.html", method={RequestMethod.POST})
	public DataModelAndView addIndvinfoTypeSetupList_download(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		indvinfoTypeSetupSvc.findIndvinfoTypeSetupList_download(modelAndView, search, request);
		
		return modelAndView;
	}
	
	
	/**
	 * 개인정보유형설정 등록
	 */
	@RequestMapping(value={"add.html"}, method={RequestMethod.POST})
	public DataModelAndView addIndvinfoTypeSetup(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		parameters = CommonHelper.checkSearchDate(parameters);
		
		//String[] args = {"privacy_seq", "privacy_desc", "regular_expression"};
		String[] args = {"privacy_seq", "privacy_desc"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		DataModelAndView modelAndView = indvinfoTypeSetupSvc.addIndvinfoTypeSetupOne(parameters); 
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	/**
	 * 개인정보유형설정 수정
	 */
	@RequestMapping(value={"save.html"}, method={RequestMethod.POST})
	public DataModelAndView saveIndvinfoTypeSetup(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		parameters = CommonHelper.checkSearchDate(parameters);
		
		String[] args = {"privacy_seq", "privacy_desc", "regular_expression"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		DataModelAndView modelAndView = indvinfoTypeSetupSvc.saveIndvinfoTypeSetupOne(parameters); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	/**
	 * 개인정보유형설정 삭제
	 */
	@RequestMapping(value={"remove.html"}, method={RequestMethod.POST})
	public DataModelAndView removeIndvinfoTypeSetup(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		parameters = CommonHelper.checkSearchDate(parameters);
		
		DataModelAndView modelAndView = indvinfoTypeSetupSvc.removeIndvinfoTypeSetupOne(parameters); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	/**
	 * 참조테이블 관리 List(최초 대메뉴 클릭 시)
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[정책관리] 참조테이블관리")
	@RequestMapping(value={"referenceManagerList.html"}, method={RequestMethod.POST,RequestMethod.GET})
	public DataModelAndView findReferenceManagerList(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		//DataModelAndView modelAndView = indvinfoTypeSetupSvc.findIndvinfoTypeSetupList(search); 
		DataModelAndView modelAndView = indvinfoTypeSetupSvc.findReferenceManagerList(search); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("referenceManagerList");
		//modelAndView.setViewName("indvinfoTypeSetupList");

		return modelAndView;
	}
	
	/**
	 * 참조테이블 선택 시 해당 테이블 컬럼 정보 가져오기
	 */
	@RequestMapping(value={"findColumByTableName.html"}, method={RequestMethod.POST})
	@ResponseBody
	public JSONArray findScenarioByScenSeq(@ModelAttribute("search") SetupSearch search, 
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		JSONArray array = new JSONArray();
		
		List<IndvinfoTypeSetup> list = indvinfoTypeSetupSvc.findReferenceColumList(search);
		
		for(IndvinfoTypeSetup data : list) {
			JSONObject object = new JSONObject();
			object.put("column_name", data.getColumn_name());
			array.add(object);
		}
		
		return array;
	} 
	
	/**
	 * 참조테이블 관리 DeailList (검색버튼 클릭 시)
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[정책관리] 참조테이블관리(검색)")
	@RequestMapping(value={"referenceManagerDetailList.html"}, method={RequestMethod.POST,RequestMethod.GET})
	public DataModelAndView findReferenceManagerDetailList(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = indvinfoTypeSetupSvc.findReferenceManagerDetailList(search); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("referenceManagerList");

		return modelAndView;
	}
	
	/**
	 * 참조테이블 관리 DeailList 중 1개 클릭시 상세보기로 이동
	 */
	@RequestMapping(value={"referenceManagerDetail.html"}, method={RequestMethod.POST})
	public DataModelAndView findReferenceManagerDetail(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = indvinfoTypeSetupSvc.findReferenceManagerDetail(search);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("referenceManagerDetail");

		return modelAndView;
	}
	
	/**
	 * 참조테이블 컬럼 데이터 수정
	 */
	@RequestMapping(value={"referenceManagerDetailsave.html"}, method={RequestMethod.POST})
	public DataModelAndView saveReferenceManagerDetail(@RequestParam Map<String, String> parameters, HttpServletRequest request, @ModelAttribute("search") SetupSearch search) {
		parameters = CommonHelper.checkSearchDate(parameters);
		
		DataModelAndView modelAndView = indvinfoTypeSetupSvc.saveReferenceManagerDetail(parameters,search); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}  
	
	/**
	 * 참조테이블 1 row 삭제
	 */
	@RequestMapping(value={"removeReferenceManagerDetail.html"}, method={RequestMethod.POST})
	public DataModelAndView removeReferenceManagerDetailOne(@RequestParam Map<String, String> parameters, HttpServletRequest request, @ModelAttribute("search") SetupSearch search) {
		parameters = CommonHelper.checkSearchDate(parameters);
		
		DataModelAndView modelAndView = indvinfoTypeSetupSvc.removeReferenceManagerDetailOne(parameters,search); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	@RequestMapping(value={"list_kdic.html"}, method={RequestMethod.POST,RequestMethod.GET})
	public DataModelAndView findIndvinfoTypeSetupList_kdic(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = indvinfoTypeSetupSvc.findIndvinfoTypeSetupList_kdic(search); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("indvinfoTypeSetupList_kdic");

		return modelAndView;
	}
	
	@RequestMapping(value={"detail_kdic.html"}, method={RequestMethod.POST})
	public DataModelAndView findIndvinfoTypeSetupDetail_kdic(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = indvinfoTypeSetupSvc.findIndvinfoTypeSetupDetail_kdic(search); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("indvinfoTypeSetupDetail_kdic");

		return modelAndView;
	}
	
	@RequestMapping(value={"save_kdic.html"}, method={RequestMethod.POST})
	public DataModelAndView saveIndvinfoTypeSetupDetail_kdic(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = indvinfoTypeSetupSvc.saveIndvinfoTypeSetupDetail_kdic(parameters); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@RequestMapping(value={"add_kdic.html"}, method={RequestMethod.POST})
	public DataModelAndView addIndvinfoTypeSetupDetail_kdic(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = indvinfoTypeSetupSvc.addIndvinfoTypeSetupDetail_kdic(parameters); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@RequestMapping(value={"delete_kdic.html"}, method={RequestMethod.POST})
	public DataModelAndView deleteIndvinfoTypeSetupDetail_kdic(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = indvinfoTypeSetupSvc.deleteIndvinfoTypeSetupDetail_kdic(parameters); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
}
