package com.easycerti.eframe.psm.setup.dao;

import java.util.List;

import com.easycerti.eframe.psm.setup.vo.HoliSetup;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;

public interface HoliSetupDao {
	
	/**
	 * 휴일 갯수
	 * @author yrchoo
	 * @since 2015. 4. 29.
	 * @return Integer
	 */
	public Integer findHoliSetupOne_count(HoliSetup holiSetup);
	
	/**
	 * 휴일 리스트
	 * @author yrchoo
	 * @since 2015. 4. 29.
	 * @return List<HoliSetup>
	 */
	public List<HoliSetup> findHoliSetupList(SetupSearch search); 
	
	/**
	 * 휴일 상세
	 * @author yrchoo
	 * @since 2015. 4. 29.
	 * @return HoliSetup
	 */
	public HoliSetup findHoliSetupOne(SetupSearch search);
	
	/**
	 * 휴일 수정
	 * @author yrchoo
	 * @since 2015. 4. 29.
	 * @return void
	 */
	public void saveHoliSetupOne(HoliSetup holiSetup);
	
	/**
	 * 휴일 일괄 등록
	 * @author yrchoo
	 * @since 2015. 4. 29.
	 * @return void
	 */
	public void addHoliSetup(HoliSetup holiSetup);
	
	/**
	 * 휴일 삭제
	 * @author yrchoo
	 * @since 2015. 4. 29.
	 * @return void
	 */
	public void removeHoliSetup(HoliSetup holiSetup);
		
	
	/**
	 * 휴일리스트 상세정보 - 리스트용 
	 * @author tjlee
	 * @since 2015. 6. 4.
	 * @return Integer
	 */
	public Integer findHoliSetupOne_count_list(SetupSearch search);
	
	public void holiSetup_upload(HoliSetup holiSetup);
}
