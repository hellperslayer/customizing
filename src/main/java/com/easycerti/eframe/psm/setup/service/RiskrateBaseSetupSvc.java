package com.easycerti.eframe.psm.setup.service;

import java.util.Map;

import com.easycerti.eframe.common.spring.DataModelAndView;

public interface RiskrateBaseSetupSvc {

	/**
	 * 위험도 목록
	 * @author yrchoo
	 * @since 2015. 4. 29.
	 * @return DataModelAndView
	 */
	public DataModelAndView findRiskrateBaseSetupList(Map<String, String> parameters); 
	
	/**
	 * 위험도 수정
	 * @author yrchoo
	 * @since 2015. 4. 29.
	 * @return DataModelAndView
	 */
	public DataModelAndView saveRiskrateBaseSetupList(Map<String, String> parameters); 
	
}
