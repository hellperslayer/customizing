package com.easycerti.eframe.psm.setup.service.impl;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.easycerti.decryptupload.decrypt;
import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.ExcelFileReader;
import com.easycerti.eframe.common.util.XExcelFileReader;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.setup.dao.DLogMenuMappSetupDao;
import com.easycerti.eframe.psm.setup.service.DLogMenuMappSetupSvc;
import com.easycerti.eframe.psm.setup.vo.DLogMenuMappSetup;
import com.easycerti.eframe.psm.setup.vo.DLogMenuMappSetupList;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;
import com.easycerti.eframe.psm.system_management.dao.AgentMngtDao;
import com.easycerti.eframe.psm.system_management.dao.MenuMngtDao;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

/**
 * 
 * 설명 :	매뉴매핑설정 SvcImpl 
 * @author tjlee
 * @since 2015. 5. 4.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 4.           tjlee            최초 생성
 *
 * </pre>
 */
@Service
public class DLogMenuMappSetupSvcImpl implements DLogMenuMappSetupSvc{
private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private DLogMenuMappSetupDao dLogMenuMappSetupDao;
	
	@Autowired
	private AgentMngtDao agentMngtDao;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;

	@Autowired
	private MenuMngtDao menuDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;

	@Override
	public DataModelAndView findDLogMenuMappSetupList(SetupSearch search, HttpServletRequest request) {
		List<String> auth_idsList = (List<String>) request.getSession().getAttribute("auth_ids_list");
		search.setAuth_idsList(auth_idsList);
		
		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		if(search.getUrl_search()!=null) {
			if(search.getUrl_search().contains("&#x2F;")) {
				search.setUrl_search(search.getUrl_search().replaceAll("&#x2F;", "/"));
			}
		}
		search.setTotal_count(dLogMenuMappSetupDao.findDLogMenuMappSetupOne_count(search));
		List<DLogMenuMappSetup> dLogMenuMappSetup = dLogMenuMappSetupDao.findDLogMenuMappSetupList(search);

		SearchSearch search2 = new SearchSearch();
		search2.setAuth_idsList(auth_idsList);
		
		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList_byAdmin(search2 );
		DLogMenuMappSetupList dLogMenuMappSetupList = new DLogMenuMappSetupList(dLogMenuMappSetup);
		
		SimpleCode simpleCode = new SimpleCode("/dLogMenuMappSetup/list.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("dLogMenuMappSetupList", dLogMenuMappSetupList);
		modelAndView.addObject("systemMasterList", systemMasterList);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView findDLogMenuMappSetupDetail(SetupSearch search) {
		DLogMenuMappSetup dLogMenuMappSetupDetail = null;
		if(search.getDlog_tag_seq() != ""){
			dLogMenuMappSetupDetail = dLogMenuMappSetupDao.findDLogMenuMappSetupDetailMenu(search);
		}
		
		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList();
		
		SimpleCode simpleCode = new SimpleCode("/dLogMenuMappSetup/detail.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("dLogMenuMappSetupDetail", dLogMenuMappSetupDetail);
		modelAndView.addObject("systemMasterList", systemMasterList);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}

	@Override
	public void findDLogMenuMappSetupList_download(DataModelAndView modelAndView, SetupSearch search, HttpServletRequest request) {

		search.setUseExcel("true");
		
		String fileName = "PSM_다운로드메뉴매핑설정_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "다운로드메뉴매핑설정";
		
		List<DLogMenuMappSetup> dLogMenuMappSetupList = dLogMenuMappSetupDao.findDLogMenuMappSetupList(search);
		
		for(DLogMenuMappSetup dLogMenuMappSetup : dLogMenuMappSetupList) {
			String use_yn = dLogMenuMappSetup.getUse_yn();
			dLogMenuMappSetup.setUse_yn(use_yn.equals("Y") ? "사용" : "미사용");
			
			String register_status = dLogMenuMappSetup.getRegister_status();
			dLogMenuMappSetup.setRegister_status(register_status);
		}
		
		String[] columns = new String[] {"dlog_tag_seq", "system_name", "url", "menu_name", "file_name", "reason", "use_yn", "register_status"};
		String[] heads = new String[] {"번호", "시스템", "URL주소", "파일대장명", "파일명", "사유", "사용여부", "파일대장 등록여부"};
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", dLogMenuMappSetupList);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}

	
	@Override
	public DataModelAndView addDLogMenuMappSetupOne(Map<String, String> parameters) {
		DLogMenuMappSetup paramBean = CommonHelper.convertMapToBean(parameters, DLogMenuMappSetup.class);
		
		// Validation Check (privacy_seq 중복)
		/*DLogMenuMappSetup DLogMenuMappSetup = dLogMenuMappSetupDao.findDLogMenuMappSetupDetail(paramBean);
		if(DLogMenuMappSetup != null) {
			throw new ESException("SYS013J");
		}*/
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		
		try {
			SetupSearch search = new SetupSearch();
			search.setUrl_search(paramBean.getUrl());
			search.setSystem_seq_search(paramBean.getSystem_seq());
			
			int duplicateCnt = dLogMenuMappSetupDao.findDLogMenuMappSetupOne_count(search);
			if(duplicateCnt > 0) {
				transactionManager.rollback(transactionStatus);
				throw new ESException("SYS701J");
			}
			
			dLogMenuMappSetupDao.addDLogMenuMappSetupOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[DLogMenuMappSetupSvcImpl.addDLogMenuMappSetupOne] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS001J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView saveDLogMenuMappSetupOne(Map<String, String> parameters) {
		DLogMenuMappSetup paramBean = CommonHelper.convertMapToBean(parameters, DLogMenuMappSetup.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			dLogMenuMappSetupDao.saveDLogMenuMappSetupOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[DLogMenuMappSetupSvcImpl.saveDLogMenuMappSetupOne] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS002J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView removeDLogMenuMappSetupOne(	Map<String, String> parameters) {
		DLogMenuMappSetup paramBean = CommonHelper.convertMapToBean(parameters, DLogMenuMappSetup.class);
		
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			dLogMenuMappSetupDao.removeDLogMenuMappSetupOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[DLogMenuMappSetupSvcImpl.removeDLogMenuMappSetupOne] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS003J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	@Override
	public String upLoadSystemMngtOne(Map<String, String> parameters, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String result="false";
		DLogMenuMappSetup paramBean = CommonHelper.convertMapToBean(parameters, DLogMenuMappSetup.class);
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		//엑셀 Row 카운트는 1000으로 지정한다		
		int rowCount=5000;
		String system_seq = parameters.get("system_seq").split(",")[0];
		String system_name = parameters.get("system_seq").split(",")[1];
		
		//파일 업로드 기능  		
		MultipartHttpServletRequest mpr = (MultipartHttpServletRequest) request;  
		
		MultipartFile dir = mpr.getFile("file");
		
//		File file = new File("C:\\" + dir.getOriginalFilename());
		File file = new File("/root/excelUpload/" + dir.getOriginalFilename());
		if(!file.getName().toLowerCase().contains("enc")) {
			return result;
		}
		String decFileName="";
		String extName = file.getName().substring(file.getName().lastIndexOf(".")+1);
		String extName1 = extName.substring(0, extName.indexOf("_"));
		String version = file.getName().substring(file.getName().lastIndexOf("_")+1);
		String script = file.getName().substring(0, file.getName().indexOf("."));
		
		if(extName1.equals("xlsx")) {
			decFileName= "/root/excelUpload/decFileName.xlsx";
		}else if(extName1.equals("xls")) {
			decFileName= "/root/excelUpload/decFileName.xls";
		}
		
//		if(extName1.equals("xlsx")){
//			decFileName= "C:\\Users\\easycerti\\Desktop\\decFileName.xlsx";
//		}else if(extName1.equals("xls")) {
//			decFileName= "C:\\Users\\easycerti\\Desktop\\decFileName.xls";
//		}
		
		if (!file.exists()) { //폴더 없으면 폴더 생성
			file.mkdirs();
		}
		try {
		dir.transferTo(file);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		//암호화 파일 -> 복호화
		decrypt.decryptFile(file.toString(), decFileName );
		
		
		File file1 = new File(decFileName);
		
		
		//업로드된 파일의 정보를 읽어들여 DB에 추가하는 기능 
		if (dir.getSize() > 0 ) { 
			List<String[]> readRows = new ArrayList<>();
			//String ext = file1.getName().substring(file1.getName().lastIndexOf(".")+1);
			extName1 = extName1.toLowerCase();
			
			if( "xlsx".equals(extName1) ){	
				try {
				readRows = new XExcelFileReader(file1.getAbsolutePath()).readRows(rowCount);
				}catch (Exception e) {
					e.printStackTrace();
					// TODO: handle exception
				}
			}else if( "xls".equals(extName1) ){	
				readRows = new ExcelFileReader(file1.getAbsolutePath()).readRows(rowCount);
			}
			for(int i=0; i<3; i++){	
				readRows.remove(0);
			}
			
			boolean isCheck = true;
			if( readRows.size() != 0 ){
				
			try {
				for(String[] readRowArr : readRows){
					if(readRowArr.length != 0 && readRowArr[1].length() != 0){
						paramBean.setSystem_seq(system_seq);
						paramBean.setUrl(readRowArr[0]);
						paramBean.setMenu_name(readRowArr[1]);
						paramBean.setFile_name(readRowArr[2]);
						paramBean.setParameter(readRowArr[3]);
						paramBean.setReason(readRowArr[4]);
						paramBean.setUse_yn(readRowArr[5].trim());
						paramBean.setRegister_status(readRowArr[6].trim());
						dLogMenuMappSetupDao.uploadDLogMenuMappSetupOne(paramBean);
					}
				}
				if(isCheck) {
					readRows.clear();
					transactionManager.commit(transactionStatus);
				}
			} catch(DataAccessException e) {
				transactionManager.rollback(transactionStatus);
				readRows.clear();
				return result;
			}
		}
			if(isCheck)
				result="true";
			
			//MAPPING업그레이드
		}
		
		try {
			paramBean.setPatch_type("다운로드매핑"); //구분(다운로드매핑)
			paramBean.setPatch_version(version); //버전(파일 뒤 날짜)
			paramBean.setScript(script); //스크립트(파일이름)
			paramBean.setDescription(system_name); //시스템명
			paramBean.setPatch_sort("mapping"); //패치 종류(sw, data, mapping 중 mapping)
			dLogMenuMappSetupDao.uploadPatch(paramBean);
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		
		file.delete();
		file1.delete();
		//복화된 파일 삭제 dec_dir
		//file.delete();
		
		return result;	
	}

}
