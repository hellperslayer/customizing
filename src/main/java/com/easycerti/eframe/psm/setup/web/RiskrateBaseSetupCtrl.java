package com.easycerti.eframe.psm.setup.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.psm.setup.service.RiskrateBaseSetupSvc;

@Controller
@RequestMapping("/riskrateBaseSetup/*")
public class RiskrateBaseSetupCtrl {
	
	@Autowired
	private RiskrateBaseSetupSvc riskrateBaseSetupSvc;
	
	/**
	 * 위험도 목록
	 * @author yrchoo
	 * @since 2015. 4. 29.
	 * @return DataModelAndView
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[정책관리] 위험도기준설정")
	@RequestMapping(value="list.html", method={RequestMethod.POST})
	public DataModelAndView findRiskrateBaseSetupList(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		parameters = CommonHelper.checkSearchDate(parameters);
			
		DataModelAndView modelAndView = riskrateBaseSetupSvc.findRiskrateBaseSetupList(parameters);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName("riskrateBaseSetupList");
		
		return modelAndView;
	}
	
	/**
	 * 위험도 수정
	 * @author yrchoo
	 * @since 2015. 4. 29.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="save.html",method={RequestMethod.POST})
	public DataModelAndView saveRiskrateBaseSetupList(@RequestParam Map<String, String> parameters,HttpServletRequest request) throws Exception{
		
		DataModelAndView modelAndView = riskrateBaseSetupSvc.saveRiskrateBaseSetupList(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}

}
