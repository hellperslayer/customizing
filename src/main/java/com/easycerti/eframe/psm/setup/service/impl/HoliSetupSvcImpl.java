package com.easycerti.eframe.psm.setup.service.impl;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.ExcelFileReader;
import com.easycerti.eframe.common.util.LunarCalendar;
import com.easycerti.eframe.common.util.XExcelFileReader;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.setup.dao.HoliSetupDao;
import com.easycerti.eframe.psm.setup.service.HoliSetupSvc;
import com.easycerti.eframe.psm.setup.vo.HoliSetup;
import com.easycerti.eframe.psm.setup.vo.HoliSetupList;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;
import com.easycerti.eframe.psm.system_management.vo.AccessAuth;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

@Repository
public class HoliSetupSvcImpl implements HoliSetupSvc {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private HoliSetupDao holiSetupDao;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;	
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	@Autowired
	private CommonDao commonDao;
	
	@Override
	public DataModelAndView findHoliSetupList(SetupSearch search) {
		
		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		search.setTotal_count(holiSetupDao.findHoliSetupOne_count_list(search));
		
		List<HoliSetup> holiSetup = holiSetupDao.findHoliSetupList(search);
		
		HoliSetupList holiSetupList = new HoliSetupList(holiSetup);
		
		SimpleCode simpleCode = new SimpleCode("/holiSetup/list.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("holiSetupList", holiSetupList);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView findHoliSetupOne(SetupSearch search) {
		
		HoliSetup holiSetup = holiSetupDao.findHoliSetupOne(search);
		
		SimpleCode simpleCode = new SimpleCode("/holiSetup/detail.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("holiSetup", holiSetup);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}


	@Override
	public void findAddUrlSetupList_download(DataModelAndView modelAndView, SetupSearch search, HttpServletRequest request) {
		search.setUseExcel("true");
		
		String fileName = "PSM_휴일_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "휴일설정";
		
		List<HoliSetup> holiSetupList = holiSetupDao.findHoliSetupList(search);
		
		for(HoliSetup holiSetup : holiSetupList) {
			String holi_yn = holiSetup.getHoli_yn();
			holiSetup.setHoli_yn(holi_yn.equals("Y") ? "휴일" : "정상근무");
			holiSetup.setWork_start_tm(holiSetup.getWork_start_tm() + " ~ " + holiSetup.getWork_end_tm());
		}
		
		String[] columns = new String[] {"holi_dt", "holi_nm", "holi_yn", "work_start_tm"};
		String[] heads = new String[] {"날짜", "휴일명", "휴일여부", "근무시간"};
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", holiSetupList);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}
	
	@Override
	public DataModelAndView saveHoliSetupOne(Map<String, String> parameters) {
		HoliSetup paramBean = CommonHelper.convertMapToBean(parameters, HoliSetup.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			holiSetupDao.saveHoliSetupOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[HoliSetupSvcImpl.saveHoliSetupOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS018J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView addHoliSetup(Map<String, String> parameters) {
		HoliSetup paramBean = CommonHelper.convertMapToBean(parameters, HoliSetup.class);
		
		int year = Integer.valueOf(parameters.get("add_year"));

		int temp = 0;	// 설 첫째날 계산용 임시 변수
		ArrayList<Integer> alterholi = new ArrayList<Integer>();
		String work_start_tm = parameters.get("work_start_tm");
		String work_end_tm = parameters.get("work_end_tm");
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		year = cal.get(Calendar.YEAR);
		
		int days = cal.getActualMaximum(Calendar.DAY_OF_YEAR);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			
			int count = holiSetupDao.findHoliSetupOne_count(paramBean);
			if (count == 365 || count == 366) {
				holiSetupDao.removeHoliSetup(paramBean);
			}
			
			for (int i=0; i < days; i++) {
				cal.set(year, 0, 1 + i);
				int month = (cal.get(Calendar.MONTH)+1);
				int date = cal.get(Calendar.DATE);
				int yoil = cal.get(Calendar.DAY_OF_WEEK);
				LunarCalendar lunar = new LunarCalendar();//양력 -> 음력 계산기
				
				String holi_dt = year + "" + (month < 10 ? "0" + month : month) + "" + (date < 10 ? "0" + date : date);
				paramBean.setHoli_dt(holi_dt);
				paramBean.setWork_start_tm("");
				paramBean.setWork_end_tm("");
				lunar.setSolarDate(holi_dt);
				
				// 1. 기타 공휴일 (holi_nm : [공휴일 이름], holi_yn : Y)
				// 2. 토요일 (holi_nm : 토요일, holi_yn : Y)
				// 3. 일요일 (holi_nm : 일요일, holi_yn : Y)
				// 4. 평일 (holi_nm : 평일, holi_yn : N, work_start_tm : 0900, work_end_tm : 1800)
				if (holi_dt.equals(year + "0101")) {
					paramBean.setHoli_nm("신정");
					paramBean.setHoli_yn("Y");
				} else if (holi_dt.equals(year + "0301")) {
					paramBean.setHoli_nm("삼일절");
					paramBean.setHoli_yn("Y");
				} else if (holi_dt.equals(year + "0505")) {
					paramBean.setHoli_nm("어린이날");
					paramBean.setHoli_yn("Y");
					if(yoil == 1)alterholi.add(i+2);
					else if(yoil == 7)alterholi.add(i+3);
				} else if (holi_dt.equals(year + "0606")) {
					paramBean.setHoli_nm("현충일");
					paramBean.setHoli_yn("Y");
				} else if (holi_dt.equals(year + "0815")) {
					paramBean.setHoli_nm("광복절");
					paramBean.setHoli_yn("Y");
				} else if (holi_dt.equals(year + "1003")) {
					paramBean.setHoli_nm("개천절");
					paramBean.setHoli_yn("Y");
				} else if (holi_dt.equals(year + "1009")) {
					paramBean.setHoli_nm("한글날");
					paramBean.setHoli_yn("Y");
				} else if (holi_dt.equals(year + "1225")) {
					paramBean.setHoli_nm("성탄절");
					paramBean.setHoli_yn("Y");
				} else if (lunar.getLunarDate().equals(year + "0408")) {
					paramBean.setHoli_nm("석가탄신일");
					paramBean.setHoli_yn("Y");
				} else if (lunar.getLunarDate().equals(year + "0101")) {
					temp = i;	// 음력 섣날그믐은 29일 이거나 30일로 일정하지 않아 설날 당일 -1을 설날로 update 처리함
					paramBean.setHoli_nm("설날");
					paramBean.setHoli_yn("Y");
					if(yoil == 1)alterholi.add(i+3);
					else if(yoil == 2)alterholi.add(i+3);
				} else if (lunar.getLunarDate().equals(year + "0102")) {
					paramBean.setHoli_nm("설날");
					paramBean.setHoli_yn("Y");
					if(yoil == 1)alterholi.add(i+2);
				} else if (lunar.getLunarDate().equals(year + "0814")) {
					paramBean.setHoli_nm("추석");
					paramBean.setHoli_yn("Y");
					if(yoil == 1)alterholi.add(i+4);
				} else if (lunar.getLunarDate().equals(year + "0815")) {
					paramBean.setHoli_nm("추석");
					paramBean.setHoli_yn("Y");
					if(yoil == 1)alterholi.add(i+3);
				} else if (lunar.getLunarDate().equals(year + "0816")) {
					paramBean.setHoli_nm("추석");
					paramBean.setHoli_yn("Y");
					if(yoil == 1)alterholi.add(i+2);
				} else if (yoil == 7) {
					paramBean.setHoli_nm("토요일");
					paramBean.setHoli_yn("Y");
				} else if (yoil == 1) {
					paramBean.setHoli_nm("일요일");
					paramBean.setHoli_yn("Y");
				} else {
					paramBean.setHoli_nm("평일");
					paramBean.setHoli_yn("N");
					paramBean.setWork_start_tm(work_start_tm);
					paramBean.setWork_end_tm(work_end_tm);
				}
				
				holiSetupDao.holiSetup_upload(paramBean);
			}
			cal.set(year, 0, temp);
			int month = (cal.get(Calendar.MONTH)+1);
			int date = cal.get(Calendar.DATE);
			int yoil = cal.get(Calendar.DAY_OF_WEEK);
			
			String holi_dt = year + "" + (month < 10 ? "0" + month : month) + "" + (date < 10 ? "0" + date : date);
			paramBean.setHoli_dt(holi_dt);
			paramBean.setWork_start_tm("");
			paramBean.setWork_end_tm("");
			paramBean.setHoli_nm("설날");
			paramBean.setHoli_yn("Y");
			
			holiSetupDao.saveHoliSetupOne(paramBean);
			
			for (Integer integer : alterholi) {
				cal.set(year, 0, integer);
				month = (cal.get(Calendar.MONTH)+1);
				date = cal.get(Calendar.DATE);
				yoil = cal.get(Calendar.DAY_OF_WEEK);
				holi_dt = year + "" + (month < 10 ? "0" + month : month) + "" + (date < 10 ? "0" + date : date);
				paramBean.setHoli_dt(holi_dt);
				paramBean.setWork_start_tm("");
				paramBean.setWork_end_tm("");
				paramBean.setHoli_nm("대체공휴일");
				paramBean.setHoli_yn("Y");
				
				holiSetupDao.saveHoliSetupOne(paramBean);
			}
			
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			logger.error("[HoliSetupSvcImpl.addHoliSetup] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}

	@Override
	public String upLoadHoliSetup(Map<String, String> parameters, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String result="false";
		HoliSetup paramBean = new HoliSetup();
		paramBean.setConflict(parameters.get("conflict"));
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		//엑셀 Row 카운트는 1000으로 지정한다		
		int rowCount=1000;
		
		//파일 업로드 기능  		
		MultipartHttpServletRequest mpr = (MultipartHttpServletRequest) request;  
		MultipartFile dir = mpr.getFile("file");
		File file = new File("/root/excelUpload/" + dir.getOriginalFilename());
		if (!file.exists()) { //폴더 없으면 폴더 생성
			file.mkdirs();
		}
		dir.transferTo(file);
		
		//업로드된 파일의 정보를 읽어들여 DB에 추가하는 기능 
		if (dir.getSize() > 0 ) { 
			List<String[]> readRows = new ArrayList<>();
			String ext = file.getName().substring(file.getName().lastIndexOf(".")+1);
			ext = ext.toLowerCase();
			if( "xlsx".equals(ext) ){	
				readRows = new XExcelFileReader(file.getAbsolutePath()).readRows(rowCount);
			}else if( "xls".equals(ext) ){	
				readRows = new ExcelFileReader(file.getAbsolutePath()).readRows(rowCount);
			}
			for(int i=0; i<2; i++){	
				readRows.remove(0);
			}
			
			boolean isCheck = true;
			if( readRows.size() != 0 ){
			try {
				for(String[] readRowArr : readRows){
					if(readRowArr.length != 0 && readRowArr[0].length() !=0){
						if(readRowArr[0].length() !=0 ){ // 날짜
							paramBean.setHoli_dt(readRowArr[0]);
						}
						paramBean.setHoli_nm(readRowArr[1]); // 휴일명
						if(readRowArr[2].length() != 0) { // 휴일여부
							if(readRowArr[2].equals("휴일")) {
								paramBean.setHoli_yn("Y");
							} else {
								paramBean.setHoli_yn("N");
							}
						}
						if(readRowArr[3].length() != 0) { //근무시간
							String[] worktm = readRowArr[3].split("~");
							String work_start_tm = worktm[0].trim();
							work_start_tm = work_start_tm.replace(":", "");
							paramBean.setWork_start_tm(work_start_tm);
							String work_end_tm = worktm[1].trim();
							work_end_tm = work_end_tm.replace(":", "");
							paramBean.setWork_end_tm(work_end_tm);
						}
						holiSetupDao.holiSetup_upload(paramBean);
					}
				}
				if(isCheck) {
					readRows.clear();
					transactionManager.commit(transactionStatus);
				}
			} catch(DataAccessException e) {
				transactionManager.rollback(transactionStatus);
				readRows.clear();
				e.printStackTrace();
				if(e.getMessage().contains("holiday_unique")) {
					result = "collision";
				}
				return result;
			}
		}
			if(isCheck)
				result="true";
		}
		return result;
	}
	
	

}
