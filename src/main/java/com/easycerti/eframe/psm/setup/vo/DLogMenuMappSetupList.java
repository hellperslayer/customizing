package com.easycerti.eframe.psm.setup.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.AbstractValueObject;
/**
 * 
 * 설명 : 매뉴매핑설정 List VO
 * @author tjlee
 * @since 2015. 4. 24.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 24.           tjlee            최초 생성
 *
 * </pre>
 */
public class DLogMenuMappSetupList extends AbstractValueObject{

	// 매뉴매핑설정 리스트
	private List<DLogMenuMappSetup> dLogMenuMappSetup = null;

	
	public DLogMenuMappSetupList(){
		
	}
	
	public DLogMenuMappSetupList(List<DLogMenuMappSetup> dLogMenuMappSetup){
		this.dLogMenuMappSetup = dLogMenuMappSetup;
	}
	
	public DLogMenuMappSetupList(List<DLogMenuMappSetup> dLogMenuMappSetup, String page_total_count){
		this.dLogMenuMappSetup = dLogMenuMappSetup;
		setPage_total_count(page_total_count);
	}

	public List<DLogMenuMappSetup> getDLogMenuMappSetup() {
		return dLogMenuMappSetup;
	}

	public void setDLogMenuMappSetup(List<DLogMenuMappSetup> dLogMenuMappSetup) {
		this.dLogMenuMappSetup = dLogMenuMappSetup;
	}
}
