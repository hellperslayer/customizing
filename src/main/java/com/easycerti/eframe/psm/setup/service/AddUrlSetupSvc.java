package com.easycerti.eframe.psm.setup.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;

public interface AddUrlSetupSvc {

	/**
	 * URL 목록
	 * @author yrchoo
	 * @since 2015. 4. 27.
	 * @return DataModelAndView
	 */
	public DataModelAndView findAddUrlSetupList(SetupSearch search); 
	
	/**
	 * URL 상세
	 * @author yrchoo
	 * @since 2015. 4. 27.
	 * @return DataModelAndView
	 */
	public DataModelAndView findAddUrlSetupOne(SetupSearch search); 
	
	/**
	 * URL 등록
	 * @author yrchoo
	 * @since 2015. 4. 27.
	 * @return DataModelAndView
	 */
	public DataModelAndView addAddUrlSetupOne(Map<String, String> parameters);
	
	/**
	 * URL 수정
	 * @author yrchoo
	 * @since 2015. 4. 27.
	 * @return DataModelAndView
	 */
	public DataModelAndView saveAddUrlSetupOne(Map<String, String> parameters); 
	
	/**
	 * URL 삭제
	 * @author yrchoo
	 * @since 2015. 4. 27.
	 * @return DataModelAndView
	 */
	public DataModelAndView removeAddUrlSetupOne(Map<String, String> parameters);

	/**
	 * 설명 : URL 엑셀 다운로드
	 * @author tjlee
	 * @since 2015. 6. 8.
	 * @return void
	 */
	public void findAddUrlSetupList_download(DataModelAndView modelAndView, SetupSearch search, HttpServletRequest request); 
	
	public String upLoadAddUrlSetup(Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response)throws Exception;
	
}
