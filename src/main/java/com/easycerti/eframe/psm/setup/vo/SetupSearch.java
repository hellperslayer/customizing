package com.easycerti.eframe.psm.setup.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.SearchBase;
/**
 * 
 * 설명 : 참조데이터 설정 Search VO
 * @author tjlee
 * @since 2015. 5. 28.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 28.           tjlee            최초 생성
 *
 * </pre>
 */

public class SetupSearch extends SearchBase{
	// 개인정보유형
	private String privacy_desc_search;
	// 개인정보 유형 사용여부
	private String use_flag_search;
	// 개인정보유형 상세 키
	private String privacy_seq;
	
	// 추출환경설정 사용여부
	private String use_yn_search;
	// 추출환경설정 실시간추출여부
	private String is_realtime_extract_search;
	// 추출환경설정 상세 키 
	private String rule_seq;
	private String indv_yn;
	// 휴일설정 휴일여부
	private String holi_yn_search;
	// 휴일설정 추가년도
	private String add_year;
	// 휴일설정 상세 키 
	private String holi_dt;
	
	// URL설정 URL속성 
	private String url_code_search;
	// URL설정 사용여부 => use_yn 이미있어서 생략
	// URL설정 URL명
	private String except_url_search;
	// URL설정 URL설명
	private String url_desc_search;
	// URL설정 상세키
	private String seq;
	
	// 메뉴매핑설정 대상서버
	private String system_seq_search;
	// 메뉴매핑설정 URL
	private String url_search;
	// 메뉴매핑설정 상세키
	private String tag_seq;
	// 메뉴매핑설정 request유형
	private String req_type;
	
	private String system_seq;
	private String system_name;
	
	private int scen_seq;
	private String scen_name;
	
	private String alarm_yn;
	private String alarm_yn_search;
	
	//참조테이블 관리 사용될 컬럼 2개
	private String table_name;
	private String kor_tablename;
	private String column_name;
	private String column_data;
	List<String> columnList;
	List<String> tablePriKeyList;
	List<String> valueList;
	private String item_values;
	
	private String detail_search_datas;
	// 다운로드메뉴매핑설정 상세키
	private String dlog_tag_seq;
	// 다운로드메뉴매핑설정 파일명
	private String file_name;
	// 다운로드메뉴매핑설정 사유
	private String reason;
	
	List<String> auth_idsList;
	
	private String log_type_search;
	
	//다운로드매핑설정 > 관리대장등록여부
	private String register_status_search;
	
	private String isSearch;
	
	private String rule_nm;
	
	private String db_name;
	
	private String privacy_table_info_seq;
	
	private int tag_sql_url_seq;
	
	private int total_count;
	
	public int getTotal_count() {
		return total_count;
	}
	public void setTotal_count(int total_count) {
		this.total_count = total_count;
	}
	public int getTag_sql_url_seq() {
		return tag_sql_url_seq;
	}
	public void setTag_sql_url_seq(int tag_sql_url_seq) {
		this.tag_sql_url_seq = tag_sql_url_seq;
	}
	public String getPrivacy_table_info_seq() {
		return privacy_table_info_seq;
	}
	public void setPrivacy_table_info_seq(String privacy_table_info_seq) {
		this.privacy_table_info_seq = privacy_table_info_seq;
	}
	public String getDb_name() {
		return db_name;
	}
	public void setDb_name(String db_name) {
		this.db_name = db_name;
	}
	public String getRule_nm() {
		return rule_nm;
	}
	public void setRule_nm(String rule_nm) {
		this.rule_nm = rule_nm;
	}
	public String getIsSearch() {
		return isSearch;
	}
	public void setIsSearch(String isSearch) {
		this.isSearch = isSearch;
	}
	public String getIndv_yn() {
		return indv_yn;
	}
	public void setIndv_yn(String indv_yn) {
		this.indv_yn = indv_yn;
	}
	public String getSystem_name() {
		return system_name;
	}
	public void setSystem_name(String system_name) {
		this.system_name = system_name;
	}
	public String getLog_type_search() {
		return log_type_search;
	}
	public void setLog_type_search(String log_type_search) {
		this.log_type_search = log_type_search;
	}
	public List<String> getAuth_idsList() {
		return auth_idsList;
	}
	public void setAuth_idsList(List<String> auth_idsList) {
		this.auth_idsList = auth_idsList;
	}
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getDlog_tag_seq() {
		return dlog_tag_seq;
	}
	public void setDlog_tag_seq(String dlog_tag_seq) {
		this.dlog_tag_seq = dlog_tag_seq;
	}
	public String getAlarm_yn_search() {
		return alarm_yn_search;
	}
	public void setAlarm_yn_search(String alarm_yn_search) {
		this.alarm_yn_search = alarm_yn_search;
	}
	public int getScen_seq() {
		return scen_seq;
	}
	public void setScen_seq(int scen_seq) {
		this.scen_seq = scen_seq;
	}
	public String getScen_name() {
		return scen_name;
	}
	public void setScen_name(String scen_name) {
		this.scen_name = scen_name;
	}
	public String getSystem_seq() {
		return system_seq;
	}
	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}
	public String getPrivacy_desc_search() {
		return privacy_desc_search;
	}
	public void setPrivacy_desc_search(String privacy_desc_search) {
		this.privacy_desc_search = privacy_desc_search;
	}
	public String getUse_flag_search() {
		return use_flag_search;
	}
	public void setUse_flag_search(String use_flag_search) {
		this.use_flag_search = use_flag_search;
	}
	public String getPrivacy_seq() {
		return privacy_seq;
	}
	public void setPrivacy_seq(String privacy_seq) {
		this.privacy_seq = privacy_seq;
	}
	public String getUse_yn_search() {
		return use_yn_search;
	}
	public void setUse_yn_search(String use_yn_search) {
		this.use_yn_search = use_yn_search;
	}
	public String getIs_realtime_extract_search() {
		return is_realtime_extract_search;
	}
	public void setIs_realtime_extract_search(String is_realtime_extract_search) {
		this.is_realtime_extract_search = is_realtime_extract_search;
	}
	public String getRule_seq() {
		return rule_seq;
	}
	public void setRule_seq(String rule_seq) {
		this.rule_seq = rule_seq;
	}
	public String getHoli_yn_search() {
		return holi_yn_search;
	}
	public void setHoli_yn_search(String holi_yn_search) {
		this.holi_yn_search = holi_yn_search;
	}
	public String getAdd_year() {
		return add_year;
	}
	public void setAdd_year(String add_year) {
		this.add_year = add_year;
	}
	public String getHoli_dt() {
		return holi_dt;
	}
	public void setHoli_dt(String holi_dt) {
		this.holi_dt = holi_dt;
	}
	public String getUrl_code_search() {
		return url_code_search;
	}
	public void setUrl_code_search(String url_code_search) {
		this.url_code_search = url_code_search;
	}
	public String getExcept_url_search() {
		return except_url_search;
	}
	public void setExcept_url_search(String except_url_search) {
		this.except_url_search = except_url_search;
	}
	public String getUrl_desc_search() {
		return url_desc_search;
	}
	public void setUrl_desc_search(String url_desc_search) {
		this.url_desc_search = url_desc_search;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getSystem_seq_search() {
		return system_seq_search;
	}
	public void setSystem_seq_search(String system_seq_search) {
		this.system_seq_search = system_seq_search;
	}
	public String getUrl_search() {
		return url_search;
	}
	public void setUrl_search(String url_search) {
		this.url_search = url_search;
	}
	public String getTag_seq() {
		return tag_seq;
	}
	public void setTag_seq(String tag_seq) {
		this.tag_seq = tag_seq;
	}
	public String getReq_type() {
		return req_type;
	}
	public void setReq_type(String req_type) {
		this.req_type = req_type;
	}
	public String getAlarm_yn() {
		return alarm_yn;
	}
	public void setAlarm_yn(String alarm_yn) {
		this.alarm_yn = alarm_yn;
	}
	public String getTable_name() {
		return table_name;
	}
	public void setTable_name(String table_name) {
		this.table_name = table_name;
	}
	public String getKor_tablename() {
		return kor_tablename;
	}
	public void setKor_tablename(String kor_tablename) {
		this.kor_tablename = kor_tablename;
	}
	public String getColumn_name() {
		return column_name;
	}
	public void setColumn_name(String column_name) {
		this.column_name = column_name;
	}
	public List<String> getColumnList() {
		return columnList;
	}
	public void setColumnList(List<String> columnList) {
		this.columnList = columnList;
	}
	public String getItem_values() {
		return item_values;
	}
	public void setItem_values(String item_values) {
		this.item_values = item_values;
	}
	public List<String> getTablePriKeyList() {
		return tablePriKeyList;
	}
	public void setTablePriKeyList(List<String> tablePriKeyList) {
		this.tablePriKeyList = tablePriKeyList;
	}
	public String getDetail_search_datas() {
		return detail_search_datas;
	}
	public void setDetail_search_datas(String detail_search_datas) {
		this.detail_search_datas = detail_search_datas;
	}
	public List<String> getValueList() {
		return valueList;
	}
	public void setValueList(List<String> valueList) {
		this.valueList = valueList;
	}
	public String getColumn_data() {
		return column_data;
	}
	public void setColumn_data(String column_data) {
		this.column_data = column_data;
	}
	public String getRegister_status_search() {
		return register_status_search;
	}
	public void setRegister_status_search(String register_status_search) {
		this.register_status_search = register_status_search;
	}
}
