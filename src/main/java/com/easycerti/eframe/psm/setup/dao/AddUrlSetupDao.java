package com.easycerti.eframe.psm.setup.dao;

import java.util.List;

import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.setup.vo.AddUrlSetup;
import com.easycerti.eframe.psm.setup.vo.HoliSetup;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

public interface AddUrlSetupDao {
	
	/**
	 * URL 목록
	 * @author yrchoo
	 * @since 2015. 4. 27.
	 * @return List<AddUrlSetup>
	 */
	public List<AddUrlSetup> findAddUrlSetupList(SetupSearch search); 
	
	/**
	 * URL 갯수
	 * @author yrchoo
	 * @since 2015. 4. 27.
	 * @return Integer
	 */
	public Integer findAddUrlSetupOne_count(SetupSearch search);
	
	/**
	 * URL 상세
	 * @author yrchoo
	 * @since 2015. 4. 27.
	 * @return AddUrlSetup
	 */
	public AddUrlSetup findAddUrlSetupOne(AddUrlSetup addUrlSetup);
	
	/**
	 * URL 등록
	 * @author yrchoo
	 * @since 2015. 4. 27.
	 * @return void
	 */
	public void addAddUrlSetupOne(AddUrlSetup addUrlSetup);
	
	/**
	 * URL 수정
	 * @author yrchoo
	 * @since 2015. 4. 27.
	 * @return void
	 */
	public void saveAddUrlSetupOne(AddUrlSetup addUrlSetup);
	
	/** 
	 * URL 삭제
	 * @author yrchoo
	 * @since 2015. 4. 27.
	 * @return void
	 */
	public void removeAddUrlSetupOne(AddUrlSetup addUrlSetup);
	
	/** 
	 * URL 페이징 위해 상세추가 
	 * @author tjlee
	 * @since 2015. 6. 4.
	 * @return AddUrlSetup
	 */
	public AddUrlSetup findAddUrlSetupOneUrl(SetupSearch search);
	
	public List<SystemMaster> urlSetup_systemMasterList(SetupSearch search);
	public void addUrlSetup_upload(AddUrlSetup addUrlSetup);
	public String addUrlSetup_findSystemSeq(String system_name);
}
