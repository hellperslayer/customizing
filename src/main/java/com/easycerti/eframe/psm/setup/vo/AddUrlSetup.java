package com.easycerti.eframe.psm.setup.vo;

import com.easycerti.eframe.common.util.PageInfo;
import com.easycerti.eframe.common.vo.AbstractValueObject;

public class AddUrlSetup extends AbstractValueObject {
	
	// url 등록번호
	private int seq;
	
	// url
	private String except_url;
	
	// url 설명
	private String url_desc;
	
	// url 사용여부
	private String use_yn;
	
	// url 속성
	private String url_code;
	
	
	// url 속성 이름
	private String url_code_nm;
	
	// 페이징 정보
	private PageInfo pageInfo;
	
	private String system_seq;
	private String system_name;
	
	public String getSystem_name() {
		return system_name;
	}

	public void setSystem_name(String system_name) {
		this.system_name = system_name;
	}

	public String getSystem_seq() {
		return system_seq;
	}

	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public String getExcept_url() {
		return except_url;
	}

	public void setExcept_url(String except_url) {
		this.except_url = except_url;
	}

	public String getUrl_desc() {
		return url_desc;
	}

	public void setUrl_desc(String url_desc) {
		this.url_desc = url_desc;
	}

	public String getUse_yn() {
		return use_yn;
	}

	public void setUse_yn(String use_yn) {
		this.use_yn = use_yn;
	}

	public String getUrl_code() {
		return url_code;
	}

	public void setUrl_code(String url_code) {
		this.url_code = url_code;
	}

	public String getUrl_code_nm() {
		return url_code_nm;
	}

	public void setUrl_code_nm(String url_code_nm) {
		this.url_code_nm = url_code_nm;
	}

	public PageInfo getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(PageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}
	
}
