package com.easycerti.eframe.psm.setup.dao;

import java.util.List;

import com.easycerti.eframe.psm.setup.vo.ExtraBizLogMappSetup;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;

public interface ExtraBizLogMappSetupDao {
	
	public List<ExtraBizLogMappSetup> findExtraBizLogMappSetupList(SetupSearch search);

	public int findExtraBizLogMappSetupOne_count(SetupSearch search);

	public void addExtraBizLogMappSetupOne(ExtraBizLogMappSetup paramBean);

	public void saveExtraBizLogMappSetupOne(ExtraBizLogMappSetup paramBean);

	public void removeExtraBizLogMappSetupOne(ExtraBizLogMappSetup paramBean);

	public ExtraBizLogMappSetup findExtraBizLogMappSetupDetail(SetupSearch search);
	
//	public MenuMappSetup findMenuMappSetupDetailMenu(SetupSearch search);
	
//	public void uploadMenuMappSetupOne(MenuMappSetup paramBean);

}
