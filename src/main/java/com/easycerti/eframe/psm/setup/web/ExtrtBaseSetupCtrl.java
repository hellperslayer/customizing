package com.easycerti.eframe.psm.setup.web;

import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.core.web.RequestWrapper;
import com.easycerti.eframe.psm.search.vo.AllLogSql;
import com.easycerti.eframe.psm.setup.service.ExtrtBaseSetupSvc;
import com.easycerti.eframe.psm.setup.vo.ExtrtBaseSetup;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;
import com.easycerti.eframe.psm.system_management.vo.EmpUser;
import com.easycerti.eframe.psm.system_management.vo.EmpUserSearch;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.org.apache.bcel.internal.generic.MONITORENTER;


/**
 * 
 * 설명 : 추출환경설정 Controller 
 * @author tjlee
 * @since 2015. 4. 24.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 24.           tjlee            최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping("/extrtBaseSetup/*")
public class ExtrtBaseSetupCtrl {
	
	@Autowired
	private ExtrtBaseSetupSvc extrtBaseSetupSvc;
	
	@Autowired
	private CommonDao commonDao;

	// 시나리오 리스트
	@MngtActHist(log_action = "SELECT", log_message = "[빅데이터분석] 비정상위험 시나리오")
	@RequestMapping(value={"scenarioList.html"}, method={RequestMethod.POST})
	public DataModelAndView findScenarioList(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = extrtBaseSetupSvc.findScenarioList(search); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("scenarioList");

		return modelAndView;
	}

	// 상세 시나리오 리스트
	@MngtActHist(log_action = "SELECT", log_message = "[비정상위험 시나리오] 시나리오")
	@RequestMapping(value={"list.html"}, method={RequestMethod.POST})
	public DataModelAndView findExtrtBaseSetupList(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = extrtBaseSetupSvc.findExtrtBaseSetupList(search); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName("extrtBaseSetupList");

		return modelAndView;
	}
	
	/**
	 * 개인정보유형설정 엑셀 다운로드
	 */
	
	@RequestMapping(value="download.html", method={RequestMethod.POST})
	public DataModelAndView addExtrtBaseSetupList_download(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		extrtBaseSetupSvc.findExtrtBaseSetupList_download(modelAndView, search, request);
		
		return modelAndView;
	}
	
	// 상세시나리오 상세화면
	@MngtActHist(log_action = "SELECT", log_message = "[비정상위험 시나리오] 시나리오상세")
	@RequestMapping(value={"detail.html"}, method={RequestMethod.POST})
	public DataModelAndView findExtrtBaseSetupDetail(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = extrtBaseSetupSvc.findExtrtBaseSetupDetail(search); 
		
		String ui_type = commonDao.getUiType();
		modelAndView.addObject("ui_type", ui_type);
		
		modelAndView.setViewName("extrtBaseSetupDetail");
		return modelAndView;
	}
	// 시나리오 상세화면
	@RequestMapping(value={"scenarioDetail.html"}, method={RequestMethod.POST})
	public DataModelAndView scenarioDetail(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = extrtBaseSetupSvc.scenarioDetail(parameters); 
		modelAndView.setViewName("scenarioDetail");
		return modelAndView;
	}
	
	
	// 상세시나리오 등록
	@RequestMapping(value={"add.html"}, method={RequestMethod.POST})
	public DataModelAndView addExtrtBaseSetup(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		parameters = CommonHelper.checkSearchDate(parameters);
		DataModelAndView modelAndView = extrtBaseSetupSvc.addExtrtBaseSetupOne(parameters); 
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	// 상세시나리오 수정
	@RequestMapping(value={"save.html"}, method={RequestMethod.POST})
	public DataModelAndView saveExtrtBaseSetup(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		parameters = CommonHelper.checkSearchDate(parameters);
		DataModelAndView modelAndView = extrtBaseSetupSvc.saveExtrtBaseSetupOne(parameters); 
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	// 상세시나리오 삭제
	@RequestMapping(value={"remove.html"}, method={RequestMethod.POST})
	public DataModelAndView removeExtrtBaseSetup(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		parameters = CommonHelper.checkSearchDate(parameters);
		DataModelAndView modelAndView = extrtBaseSetupSvc.removeExtrtBaseSetupOne(parameters); 
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	/**
	 * 사원 리스트
	 */
	@RequestMapping(value="indvlist.html", method={RequestMethod.POST})
	public DataModelAndView findExtrtBaseSetupList_indv(@ModelAttribute("search") EmpUserSearch search, @RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		if(search.getEmp_user_id_1() != null) {
			String id = search.getEmp_user_id_1();
			String upperId = id.toUpperCase();
			search.setEmp_user_id_1(upperId);
		}
		
		DataModelAndView modelAndView = extrtBaseSetupSvc.findEmpUserMngtList(search);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("extrtBaseSetupList_indv");
			
		return modelAndView;
	}
	
	@RequestMapping(value="indvdetail.html", method={RequestMethod.POST})
	public DataModelAndView findExtrtBaseSetupDetail_indv(@ModelAttribute("search") EmpUser search, @RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = extrtBaseSetupSvc.findExtrtBaseSetupDetail_indv(search);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("extrtBaseSetupDetail_indv");
			
		return modelAndView;
	}
	
	@RequestMapping(value="setindv.html", method={RequestMethod.POST})
	public DataModelAndView setExtrtBaseSetupDetail_indv(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = extrtBaseSetupSvc.setExtrtBaseSetupDetail_indv(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
			
		return modelAndView;
	}
	
	@RequestMapping(value="setAllindv.html", method={RequestMethod.POST})
	public DataModelAndView setExtrtBaseSetupAll_indv(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = extrtBaseSetupSvc.setExtrtBaseSetupAll_indv(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	@RequestMapping(value={"saveScenario.html"}, method={RequestMethod.POST})
	public DataModelAndView saveScenario(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		parameters = CommonHelper.checkSearchDate(parameters);
		DataModelAndView modelAndView = extrtBaseSetupSvc.saveScenario(parameters); 
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	@RequestMapping(value={"removeScenario.html"}, method={RequestMethod.POST})
	public DataModelAndView removeScenario(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		//parameters = CommonHelper.checkSearchDate(parameters);
		
		DataModelAndView modelAndView = extrtBaseSetupSvc.removeScenario(parameters); 
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	@RequestMapping(value={"addScenario.html"}, method={RequestMethod.POST})
	public DataModelAndView addScenario(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		//parameters = CommonHelper.checkSearchDate(parameters);
		
		DataModelAndView modelAndView = extrtBaseSetupSvc.addScenario(parameters); 
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	@RequestMapping(value={"findRuleTableByScenario.html"}, method={RequestMethod.POST})
	public DataModelAndView findRuleTableByScenario(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		//parameters = CommonHelper.checkSearchDate(parameters);
		
		DataModelAndView modelAndView = extrtBaseSetupSvc.findRuleTableByScenario(parameters); 
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}

	@MngtActHist(log_action = "SELECT", log_message = "[빅데이터분석] 개인별 임계치 설정")
	@RequestMapping(value = { "thresholdSetting_indv.html" }, method = { RequestMethod.POST })
	public DataModelAndView thresholdSetting_indv(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) throws ParseException {
		DataModelAndView modelAndView = extrtBaseSetupSvc.thresholdSetting_indv(search);
		modelAndView.setViewName("thresholdSetting_indv");
		return modelAndView;
	}
	@MngtActHist(log_action = "SELECT", log_message = "[빅데이터분석] 개인별 임계치 설정 상세")
	@RequestMapping(value = { "thresholdSettingDetail_indv.html" }, method = { RequestMethod.POST })
	public DataModelAndView thresholdSettingDetail_indv(@RequestParam Map<String, String> parameters, HttpServletRequest request,
			@ModelAttribute("search") EmpUserSearch search) throws ParseException {
		DataModelAndView modelAndView = extrtBaseSetupSvc.thresholdSettingDetail_indv(parameters, request);
		modelAndView.setViewName("thresholdSettingDetail_indv");
		return modelAndView;
	}
	
	
	@MngtActHist(log_action = "SELECT", log_message = "[빅데이터분석] 부서별 임계치 설정")
	@RequestMapping(value = { "thresholdSetting_dept.html" }, method = { RequestMethod.POST })
	public DataModelAndView thresholdSetting_dept(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) throws ParseException {
		DataModelAndView modelAndView = extrtBaseSetupSvc.thresholdSetting_dept(search);
		modelAndView.setViewName("thresholdSetting_dept");
		return modelAndView;
	}
	@MngtActHist(log_action = "SELECT", log_message = "[빅데이터분석] 부서별 임계치 설정 상세")
	@RequestMapping(value = { "thresholdSettingDetail_dept.html" }, method = { RequestMethod.POST })
	public DataModelAndView thresholdSettingDetail_dept(@RequestParam Map<String, String> parameters, HttpServletRequest request
			) throws ParseException {
		DataModelAndView modelAndView = extrtBaseSetupSvc.thresholdSettingDetail_dept(parameters, request);
		modelAndView.setViewName("thresholdSettingDetail_dept");
		return modelAndView;
	}
	
	@ResponseBody
	@RequestMapping(value={"saveThreshold_dept.html"}, method={RequestMethod.POST})
	public String saveThreshold_dept(@RequestParam String jsonData) {
		jsonData = jsonData.replaceAll("&#quot;","\"");
		String result = extrtBaseSetupSvc.saveThreshold_dept(jsonData);
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value={"saveThreshold_indv.html"}, method={RequestMethod.POST})
	public String saveThreshold_indv(@RequestParam String jsonData) {
		jsonData = jsonData.replaceAll("&#quot;","\"");
		String result = extrtBaseSetupSvc.saveThreshold_indv(jsonData); 
		return result;
	}
	@ResponseBody
	@RequestMapping(value={"deleteThreshold_indv.html"}, method={RequestMethod.POST})
	public String deleteThreshold_indv(String emp_user_id, String system_seq, String rule_seq) {
		ExtrtBaseSetup baseSetup = new ExtrtBaseSetup();
		baseSetup.setEmp_user_id(emp_user_id);
		if(system_seq.equals("")) {
			baseSetup.setSystem_seq("00");
		} else {
			baseSetup.setSystem_seq(system_seq);
		}
		baseSetup.setRule_seq(Integer.parseInt(rule_seq));
		String result = extrtBaseSetupSvc.deleteThreshold_indv(baseSetup); 
		return result;
	}
	@ResponseBody
	@RequestMapping(value={"deleteThreshold_dept.html"}, method={RequestMethod.POST})
	public String deleteThreshold_dept(String dept_id, String system_seq, String rule_seq) {
		ExtrtBaseSetup baseSetup = new ExtrtBaseSetup();
		baseSetup.setDept_id(dept_id);
		baseSetup.setRule_seq(Integer.parseInt(rule_seq));
		String result = extrtBaseSetupSvc.deleteThreshold_dept(baseSetup); 
		return result;
	}
}
