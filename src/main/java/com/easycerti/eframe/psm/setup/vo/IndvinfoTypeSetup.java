package com.easycerti.eframe.psm.setup.vo;

import java.util.List;

import com.easycerti.eframe.common.util.PageInfo;
import com.easycerti.eframe.common.vo.AbstractValueObject;
/**
 * 
 * 설명 : 개인정보유형설정 VO
 * @author tjlee
 * @since 2015. 5. 4.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 4.           tjlee            최초 생성
 *
 * </pre>
 */
public class IndvinfoTypeSetup extends AbstractValueObject{
	private int privacy_seq;	// 개인정보유형 테이블 시퀀스
	private String privacy_type; // 개인정보 유형(번호)
	private String privacy_desc; // 개인정보 설명(한글)
	private String regular_expression; // 정규식
	private String use_flag; // 사용여부
	private String system_seq;
	private String privacy_masking;	// 개인정보 마스킹
	private String check_all;
	private String check_all_masking;
	private String check_all_biz_log_result;
	//참조테이블 관리 사용될 컬럼 2개
	private String table_name;
	private String kor_tablename;
	private String column_name;
	List<String> columnList;
	private String primaryKey;
	private String use_biz_log_result;
	private int result_type_order;
	
	
	public int getResult_type_order() {
		return result_type_order;
	}
	public void setResult_type_order(int result_type_order) {
		this.result_type_order = result_type_order;
	}
	public String getSystem_seq() {
		return system_seq;
	}
	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}
	private PageInfo pageInfo; // 페이징 정보
	
	
	public PageInfo getPageInfo() {
		return pageInfo;
	}
	public void setPageInfo(PageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}
	public int getPrivacy_seq() {
		return privacy_seq;
	}
	public void setPrivacy_seq(int privacy_seq) {
		this.privacy_seq = privacy_seq;
	}
	public String getPrivacy_type() {
		return privacy_type;
	}
	public void setPrivacy_type(String privacy_type) {
		this.privacy_type = privacy_type;
	}
	public String getPrivacy_desc() {
		return privacy_desc;
	}
	public void setPrivacy_desc(String privacy_desc) {
		this.privacy_desc = privacy_desc;
	}
	public String getRegular_expression() {
		return regular_expression;
	}
	public void setRegular_expression(String regular_expression) {
		this.regular_expression = regular_expression;
	}
	public String getUse_flag() {
		return use_flag;
	}
	public void setUse_flag(String use_flag) {
		this.use_flag = use_flag;
	}
	public String getPrivacy_masking() {
		return privacy_masking;
	}
	public void setPrivacy_masking(String privacy_masking) {
		this.privacy_masking = privacy_masking;
	}
	public String getCheck_all() {
		return check_all;
	}
	public void setCheck_all(String check_all) {
		this.check_all = check_all;
	}
	public String getCheck_all_masking() {
		return check_all_masking;
	}
	public void setCheck_all_masking(String check_all_masking) {
		this.check_all_masking = check_all_masking;
	}
	public String getCheck_all_biz_log_result() {
		return check_all_biz_log_result;
	}
	public void setCheck_all_biz_log_result(String check_all_biz_log_result) {
		this.check_all_biz_log_result = check_all_biz_log_result;
	}
	public String getTable_name() {
		return table_name;
	}
	public void setTable_name(String table_name) {
		this.table_name = table_name;
	}
	public String getKor_tablename() {
		return kor_tablename;
	}
	public void setKor_tablename(String kor_tablename) {
		this.kor_tablename = kor_tablename;
	}
	public String getColumn_name() {
		return column_name;
	}
	public void setColumn_name(String column_name) {
		this.column_name = column_name;
	}
	public List<String> getColumnList() {
		return columnList;
	}
	public void setColumnList(List<String> columnList) {
		this.columnList = columnList;
	}
	public String getPrimaryKey() {
		return primaryKey;
	}
	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}
	public String getUse_biz_log_result() {
		return use_biz_log_result;
	}
	public void setUse_biz_log_result(String use_biz_log_result) {
		this.use_biz_log_result = use_biz_log_result;
	}

}
