package com.easycerti.eframe.psm.setup.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.AbstractValueObject;
/**
 * 개인정보유형별 리스트 VO
 * 설명 : 
 * @author tjlee
 * @since 2015. 4. 27.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 27.           tjlee            최초 생성
 *
 * </pre>
 */
public class IndvinfoTypeSetupList extends AbstractValueObject{

	private List<IndvinfoTypeSetup> indvinfoTypeSetup = null;

	
	public IndvinfoTypeSetupList(){
		
	}
	
	public IndvinfoTypeSetupList(List<IndvinfoTypeSetup> indvinfoTypeSetup){
		this.indvinfoTypeSetup = indvinfoTypeSetup;
	}
	
	public IndvinfoTypeSetupList(List<IndvinfoTypeSetup> indvinfoTypeSetup, String page_total_count){
		this.indvinfoTypeSetup = indvinfoTypeSetup;
		setPage_total_count(page_total_count);
	}

	public List<IndvinfoTypeSetup> getIndvinfoTypeSetup() {
		return indvinfoTypeSetup;
	}

	public void setIndvinfoTypeSetup(List<IndvinfoTypeSetup> indvinfoTypeSetup) {
		this.indvinfoTypeSetup = indvinfoTypeSetup;
	}

}
