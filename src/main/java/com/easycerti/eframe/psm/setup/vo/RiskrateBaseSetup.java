package com.easycerti.eframe.psm.setup.vo;

import com.easycerti.eframe.common.vo.AbstractValueObject;

public class RiskrateBaseSetup extends AbstractValueObject {
	
	// 위험도 구분번호
	private String dng_cd;
	
	// 위험도명
	private String dng_nm;
	
	// 위험지수
	private int dng_val;

	public String getDng_cd() {
		return dng_cd;
	}

	public void setDng_cd(String dng_cd) {
		this.dng_cd = dng_cd;
	}

	public String getDng_nm() {
		return dng_nm;
	}

	public void setDng_nm(String dng_nm) {
		this.dng_nm = dng_nm;
	}

	public int getDng_val() {
		return dng_val;
	}

	public void setDng_val(int dng_val) {
		this.dng_val = dng_val;
	}

}
