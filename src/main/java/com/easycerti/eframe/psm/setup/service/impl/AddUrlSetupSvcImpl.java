package com.easycerti.eframe.psm.setup.service.impl;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.ExcelFileReader;
import com.easycerti.eframe.common.util.XExcelFileReader;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.setup.dao.AddUrlSetupDao;
import com.easycerti.eframe.psm.setup.service.AddUrlSetupSvc;
import com.easycerti.eframe.psm.setup.vo.AddUrlSetup;
import com.easycerti.eframe.psm.setup.vo.AddUrlSetupList;
import com.easycerti.eframe.psm.setup.vo.HoliSetup;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;
import com.easycerti.eframe.psm.system_management.dao.AgentMngtDao;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

@Repository
public class AddUrlSetupSvcImpl implements AddUrlSetupSvc {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private AddUrlSetupDao addUrlSetupDao;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;	
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	@Autowired
	private CommonDao commonDao;

	@Override
	public DataModelAndView findAddUrlSetupList(SetupSearch search) {
		
		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		search.setTotal_count(addUrlSetupDao.findAddUrlSetupOne_count(search));
		
		List<AddUrlSetup> addUrlSetup = addUrlSetupDao.findAddUrlSetupList(search);
		AddUrlSetupList addUrlSetupList = new AddUrlSetupList(addUrlSetup);
		
		List<SystemMaster> systemMasterList = addUrlSetupDao.urlSetup_systemMasterList(search);
		
		SimpleCode simpleCode = new SimpleCode("/addUrlSetup/list.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("addUrlSetupList", addUrlSetupList);
		modelAndView.addObject("systemMasterList", systemMasterList);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView findAddUrlSetupOne(SetupSearch search) {
		
		AddUrlSetup addUrlSetup = addUrlSetupDao.findAddUrlSetupOneUrl(search);
		
		List<SystemMaster> systemMasterList = addUrlSetupDao.urlSetup_systemMasterList(search);
		
		SimpleCode simpleCode = new SimpleCode("/addUrlSetup/detail.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("addUrlSetup", addUrlSetup);
		modelAndView.addObject("systemMasterList", systemMasterList);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	@Override
	public void findAddUrlSetupList_download(DataModelAndView modelAndView, SetupSearch search, HttpServletRequest request) {

		search.setUseExcel("true");
		
		String fileName = "PSM_URL설정_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "URL설정";
		
		List<AddUrlSetup> addUrlSetupList = addUrlSetupDao.findAddUrlSetupList(search);
		for(AddUrlSetup addUrlSetup : addUrlSetupList) {
			String use_yn = addUrlSetup.getUse_yn();
			addUrlSetup.setUse_yn(use_yn.equals("Y") ? "사용" : "미사용");
		}
		
		String[] columns = new String[] {"seq", "system_name", "url_code_nm", "except_url", "url_desc", "use_yn"};
		String[] heads = new String[] {"등록번호", "시스템", "URL속성", "URL", "URL설명", "사용여부"};
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", addUrlSetupList);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}
	
	@Override
	public DataModelAndView addAddUrlSetupOne(Map<String, String> parameters) {
		AddUrlSetup paramBean = CommonHelper.convertMapToBean(parameters, AddUrlSetup.class);
		AddUrlSetup addUrlSetup = addUrlSetupDao.findAddUrlSetupOne(paramBean);
		if (addUrlSetup != null) {
			throw new ESException("SYS013J");
		}
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			addUrlSetupDao.addAddUrlSetupOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			logger.error("[AddUrlSetupSvcImpl.addAdminUserMngtOne] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}

	@Override
	public DataModelAndView saveAddUrlSetupOne(Map<String, String> parameters) {
		AddUrlSetup paramBean = CommonHelper.convertMapToBean(parameters, AddUrlSetup.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			addUrlSetupDao.saveAddUrlSetupOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[AddUrlSetupSvcImpl.saveAddUrlSetupOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS018J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView removeAddUrlSetupOne(Map<String, String> parameters) {
		AddUrlSetup paramBean = CommonHelper.convertMapToBean(parameters, AddUrlSetup.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			addUrlSetupDao.removeAddUrlSetupOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[AddUrlSetupSvcImpl.removeAddUrlSetupOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS015J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}

	@Override
	public String upLoadAddUrlSetup(Map<String, String> parameters, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String result="false";
		AddUrlSetup paramBean = new AddUrlSetup();
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		//엑셀 Row 카운트는 1000으로 지정한다		
		int rowCount=1000;
		
		//파일 업로드 기능  		
		MultipartHttpServletRequest mpr = (MultipartHttpServletRequest) request;  
		MultipartFile dir = mpr.getFile("file");
		File file = new File("/root/excelUpload/" + dir.getOriginalFilename());
		if (!file.exists()) { //폴더 없으면 폴더 생성
			file.mkdirs();
		}
		dir.transferTo(file);
		
		//업로드된 파일의 정보를 읽어들여 DB에 추가하는 기능 
		if (dir.getSize() > 0 ) { 
			List<String[]> readRows = new ArrayList<>();
			String ext = file.getName().substring(file.getName().lastIndexOf(".")+1);
			ext = ext.toLowerCase();
			if( "xlsx".equals(ext) ){	
				readRows = new XExcelFileReader(file.getAbsolutePath()).readRows(rowCount);
			}else if( "xls".equals(ext) ){	
				readRows = new ExcelFileReader(file.getAbsolutePath()).readRows(rowCount);
			}
			for(int i=0; i<3; i++){	
				readRows.remove(0);
			}
			
			boolean isCheck = true;
			if( readRows.size() != 0 ){
			try {
				for(String[] readRowArr : readRows){
					if(readRowArr.length != 0 && readRowArr[0].length() !=0){
						if(readRowArr[0].length() !=0 ){ // 시스템
							String system_seq = addUrlSetupDao.addUrlSetup_findSystemSeq(readRowArr[0]);
							if(system_seq!=null) {
								paramBean.setSystem_seq(system_seq);
							} else {
								isCheck = false;
								break;
							}
						}
						if(readRowArr[1].length() !=0 ) {
							String urlTemp = readRowArr[1].substring(0, 2);
							if(urlTemp.equals("대용")) {
								paramBean.setUrl_code("BE");
							} else if(urlTemp.equals("제외")) {
								paramBean.setUrl_code("EC");
							} else if(urlTemp.equals("엑셀")) {
								paramBean.setUrl_code("SE");
							} else {
								isCheck = false;
								break;
							}
						}
						paramBean.setExcept_url(readRowArr[2]);
						paramBean.setUrl_desc(readRowArr[3]);
						if(readRowArr[4].length()!=0) {
							if(readRowArr[4].equals("사용")) {
								paramBean.setUse_yn("Y");
							} else {
								paramBean.setUse_yn("N");
							}
						}
						addUrlSetupDao.addUrlSetup_upload(paramBean);
					}
				}
				if(isCheck) {
					readRows.clear();
					transactionManager.commit(transactionStatus);
				}
			} catch(DataAccessException e) {
				transactionManager.rollback(transactionStatus);
				readRows.clear();
				e.printStackTrace();
				return result;
			}
		}
			if(isCheck)
				result="true";
		}
		return result;
	}
	
	

}
