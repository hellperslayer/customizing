package com.easycerti.eframe.psm.setup.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionStatus;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.setup.dao.RiskrateBaseSetupDao;
import com.easycerti.eframe.psm.setup.service.RiskrateBaseSetupSvc;
import com.easycerti.eframe.psm.setup.vo.RiskrateBaseSetup;

@Repository
public class RiskrateBaseSetupSvcImpl implements RiskrateBaseSetupSvc {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private RiskrateBaseSetupDao riskrateBaseSetupDao;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	@Autowired
	private CommonDao commonDao;

	@Override
	public DataModelAndView findRiskrateBaseSetupList(Map<String, String> parameters) {
		RiskrateBaseSetup paramBean = CommonHelper.convertMapToBean(parameters, RiskrateBaseSetup.class);
		
		List<RiskrateBaseSetup> riskrateBaseSetup = riskrateBaseSetupDao.findRiskrateBaseSetupList(paramBean);
		
		SimpleCode simpleCode = new SimpleCode("/riskrateBaseSetup/list.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("riskrateBaseSetup", riskrateBaseSetup);
		modelAndView.addObject("paramBean", paramBean);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView saveRiskrateBaseSetupList(Map<String, String> parameters) {
		RiskrateBaseSetup paramBean = CommonHelper.convertMapToBean(parameters, RiskrateBaseSetup.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			for (int i = 1; i < 6; i++) {
				String dng_val = parameters.get("dng_val_" + i);
				paramBean.setDng_cd(String.valueOf(i));
				paramBean.setDng_val(Integer.valueOf(dng_val));
				riskrateBaseSetupDao.saveRiskrateBaseSetupList(paramBean);
			}
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[AddUrlSetupSvcImpl.saveAddUrlSetupOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS018J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
}
