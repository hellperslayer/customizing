package com.easycerti.eframe.psm.setup.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;

public interface HoliSetupSvc {
	
	/**
	 * 휴일 리스트
	 * @author yrchoo
	 * @since 2015. 4. 29.
	 * @return DataModelAndView
	 */
	public DataModelAndView findHoliSetupList(SetupSearch search); 
	
	/**
	 * 휴일 상세
	 * @author yrchoo
	 * @since 2015. 4. 29.
	 * @return DataModelAndView
	 */
	public DataModelAndView findHoliSetupOne(SetupSearch search);
	
	/**
	 * 휴일 수정
	 * @author yrchoo
	 * @since 2015. 4. 29.
	 * @return DataModelAndView
	 */
	public DataModelAndView saveHoliSetupOne(Map<String, String> parameters);
	
	/**
	 * 휴일 일괄 등록
	 * @author yrchoo
	 * @since 2015. 4. 29.
	 * @return DataModelAndView
	 */
	public DataModelAndView addHoliSetup(Map<String, String> parameters);

	/**
	 * 설명 : 휴일설정 엑셀 다운로드
	 * @author tjlee
	 * @since 2015. 6. 8.
	 * @return void
	 */
	public void findAddUrlSetupList_download(DataModelAndView modelAndView, SetupSearch search, HttpServletRequest request);
	
	public String upLoadHoliSetup(Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response)throws Exception;
	
}
