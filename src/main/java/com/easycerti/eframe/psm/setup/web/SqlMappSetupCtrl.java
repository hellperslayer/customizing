package com.easycerti.eframe.psm.setup.web;

import java.io.File;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.psm.setup.service.SqlMappSetupSvc;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;

@Controller
@RequestMapping("/sqlMappSetup/*")
public class SqlMappSetupCtrl {
	@Autowired
	private SqlMappSetupSvc sqlMappSetupSvc;
	/**
	 * sql매핑설정 List
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[정책관리] SQL매핑설정")
	@RequestMapping(value={"list.html"}, method={RequestMethod.POST})
	public DataModelAndView findSqlMappSetupList(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters,  HttpServletRequest request) {
		
		DataModelAndView modelAndView = sqlMappSetupSvc.findSqlMappSetupList(search,request); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("sqlMappSetupList");
		
		return modelAndView;
		
	}
	
	/**
	 * sql매핑설정상세
	 */
	
	@MngtActHist(log_action = "SELECT", log_message = "[정책관리] SQL매핑설정 상세")
	@RequestMapping(value={"detail.html"}, method={RequestMethod.POST})
	public DataModelAndView findSqlMappSetupDetail(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = sqlMappSetupSvc.findSqlMappSetupDetail(search); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("sqlMappSetupDetail");

		return modelAndView;
	}
	
	/**
	 * sql매핑설정 삭제
	 */
	
	@MngtActHist(log_action = "REMOVE", log_message = "[정책관리] SQL매핑설정 삭제")
	@RequestMapping(value={"remove.html"}, method={RequestMethod.POST})
	public DataModelAndView removeMenuMappSetup(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		parameters = CommonHelper.checkSearchDate(parameters);
		
		DataModelAndView modelAndView = sqlMappSetupSvc.removeSqlMappSetupOne(parameters); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	/**
	 * sql매핑설정 엑셀 다운로드
	 */
	@RequestMapping(value={"download.html"}, method={RequestMethod.POST})
	public DataModelAndView addMenuMappSetupList_download(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		sqlMappSetupSvc.findSqlMappSetupList_download(modelAndView, search, request);
		
		return modelAndView;
	}
	
	@RequestMapping("exdownload.html")
	public ModelAndView download(HttpServletRequest request)throws Exception{
		
		String path = request.getSession().getServletContext().getRealPath("/")+"WEB-INF/";
		File down = new File(path + "excelExam/SQL매핑_업로드양식.xlsx");
		return new ModelAndView("download","downloadFile",down);
	}
	
	@RequestMapping(value="upload.html", method={RequestMethod.POST})
	@ResponseBody
	public String upSqlMappSetupMngtOne(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
 		parameters.put("insert_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		String result = sqlMappSetupSvc.upLoadSystemMngtOne(parameters,request, response);
		
		return result;
	}
	
}
