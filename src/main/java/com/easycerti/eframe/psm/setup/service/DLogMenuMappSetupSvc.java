package com.easycerti.eframe.psm.setup.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.setup.vo.DLogMenuMappSetup;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;
/**
 * 
 * 설명 : 매뉴매핑설정 Svc 
 * @author tjlee
 * @since 2015. 5. 4.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 4.           tjlee            최초 생성
 *
 * </pre>
 */
public interface DLogMenuMappSetupSvc {
	
	/**
	 * 설명 : 메뉴매핑설정 리스트 
	 * @author tjlee
	 * @param request 
	 * @since 2015. 6. 8.
	 * @return DataModelAndView
	 */
	public DataModelAndView findDLogMenuMappSetupList(SetupSearch search, HttpServletRequest request);
	
	/**
	 * 설명 : 메뉴매핑설정 상세 
	 * @author tjlee
	 * @since 2015. 6. 8.
	 * @return DataModelAndView
	 */
	public DataModelAndView findDLogMenuMappSetupDetail(SetupSearch search);
	
	/**
	 * 설명 : 메뉴매핑설정 등록 
	 * @author tjlee
	 * @since 2015. 6. 8.
	 * @return DataModelAndView
	 */
	public DataModelAndView addDLogMenuMappSetupOne(	Map<String, String> parameters);
	
	/**
	 * 설명 : 메뉴매핑설정 수정 
	 * @author tjlee
	 * @since 2015. 6. 8.
	 * @return DataModelAndView
	 */
	public DataModelAndView saveDLogMenuMappSetupOne(Map<String, String> parameters);
	
	/**
	 * 설명 : 메뉴매핑설정 삭제 
	 * @author tjlee
	 * @since 2015. 6. 8.
	 * @return DataModelAndView
	 */
	public DataModelAndView removeDLogMenuMappSetupOne(Map<String, String> parameters);

	/**
	 * 설명 :	메뉴매핑설정 엑셀다운로드 
	 * @author tjlee
	 * @since 2015. 6. 8.
	 * @return void
	 */
	public void findDLogMenuMappSetupList_download(DataModelAndView modelAndView, SetupSearch search, HttpServletRequest request);
	public String upLoadSystemMngtOne(Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response)throws Exception;
	
	
}
