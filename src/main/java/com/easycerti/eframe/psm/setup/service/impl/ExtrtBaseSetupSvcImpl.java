package com.easycerti.eframe.psm.setup.service.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.dashboard.vo.Dashboard;
import com.easycerti.eframe.psm.setup.dao.ExtrtBaseSetupDao;
import com.easycerti.eframe.psm.setup.service.ExtrtBaseSetupSvc;
import com.easycerti.eframe.psm.setup.vo.ExtrtBaseSetup;
import com.easycerti.eframe.psm.setup.vo.ExtrtBaseSetupList;
import com.easycerti.eframe.psm.setup.vo.IndvinfoTypeSetup;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;
import com.easycerti.eframe.psm.setup.web.ExtrtBaseSetupCtrl;
import com.easycerti.eframe.psm.system_management.dao.AgentMngtDao;
import com.easycerti.eframe.psm.system_management.dao.DepartmentMngtDao;
import com.easycerti.eframe.psm.system_management.dao.EmpUserMngtDao;
import com.easycerti.eframe.psm.system_management.dao.MenuMngtDao;
import com.easycerti.eframe.psm.system_management.vo.Department;
import com.easycerti.eframe.psm.system_management.vo.EmpUser;
import com.easycerti.eframe.psm.system_management.vo.EmpUserSearch;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;
import com.google.gson.Gson;
/**
 * 
 * 설명 : 추출환경설정 SvcImpl
 * @author tjlee
 * @since 2015. 5. 4.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 4.           tjlee            최초 생성
 *
 * </pre>
 */
@Service
public class ExtrtBaseSetupSvcImpl implements ExtrtBaseSetupSvc{
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private ExtrtBaseSetupDao extrtBaseSetupDao;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;

	@Autowired
	private MenuMngtDao menuDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private EmpUserMngtDao empUserMngtDao;
	
	@Autowired
	private DepartmentMngtDao departmentMngtDao;
	
	@Autowired
	private AgentMngtDao agentMngtDao;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;

	@Override
	public DataModelAndView findScenarioList(SetupSearch search) {
		DataModelAndView modelAndView = new DataModelAndView();
		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtBaseSetup/scenarioList.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		List<ExtrtBaseSetup> scenarioList = extrtBaseSetupDao.findScenarioList(search);
		modelAndView.addObject("scenarioList", scenarioList);
		
		
		
		return modelAndView;
	}
	
	
	
	@Override
	public DataModelAndView scenarioDetail(Map<String, String> parameters) {
		DataModelAndView modelAndView = new DataModelAndView();
		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtBaseSetup/scenarioDetail.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		List<SystemMaster> systemList = commonDao.getSystemMasterList();
		modelAndView.addObject("systemList", systemList);

		ExtrtBaseSetup scenarioInfo = new ExtrtBaseSetup();
		String scen_seq_str = parameters.get("scen_seq");
		if(scen_seq_str != null && scen_seq_str != "") {
			int scen_seq = Integer.parseInt(scen_seq_str);
			scenarioInfo.setScen_seq(scen_seq);
			scenarioInfo = extrtBaseSetupDao.findScenarioDetail(scenarioInfo);
			// 분석대상 시스템별 선택한 경우
			if(scenarioInfo.getIndv_yn().equals("N")) {
				String[] systemSeqArr = scenarioInfo.getSystem_seq().split(",");
				modelAndView.addObject("systemSeqLength", systemSeqArr.length);
				String system_name = "";
				for(String a : systemSeqArr) {
					for(SystemMaster s : systemList) {
						if(a.equals(s.getSystem_seq())) {
							system_name += s.getSystem_name()+", ";
							break;
						}
					}
				}
				system_name = system_name.substring(0, system_name.length()-2);
				scenarioInfo.setSystem_name(system_name);
			} else {
				scenarioInfo.setSystem_seq("");
			}
			modelAndView.addObject("scenarioInfo", scenarioInfo);
		}
		// 위험도 총계(해당되는 것 제외한 합계)
		int dngValSum = extrtBaseSetupDao.scenarioDngValSum(scenarioInfo);
		modelAndView.addObject("dngValSum", dngValSum);
		
		return modelAndView;
	}



	@Override
	public DataModelAndView findExtrtBaseSetupList(SetupSearch search) {
		DataModelAndView modelAndView = new DataModelAndView();
		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtBaseSetup/list.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		String isSearch = search.getIsSearch();
		if(isSearch==null||isSearch.equals("")) {
			search.setUse_yn_search("Y");
		}
		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		search.setTotal_count(extrtBaseSetupDao.findExtrtBaseSetupOne_count(search));
		List<ExtrtBaseSetup> extrtBaseSetupList = extrtBaseSetupDao.findExtrtBaseSetupList(search);
		modelAndView.addObject("extrtBaseSetupList", extrtBaseSetupList);
		
		List<ExtrtBaseSetup> scenarioList = extrtBaseSetupDao.findScenarioList(search);
		modelAndView.addObject("scenarioList", scenarioList);
		
		modelAndView.addObject("search", search);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView findExtrtBaseSetupDetail(SetupSearch search) {
		DataModelAndView modelAndView = new DataModelAndView();
		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtBaseSetup/detail.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		// 분석대상 시스템 선택 시 나오는 시스템 리스트
		List<SystemMaster> systemList = commonDao.getSystemMasterList();
		modelAndView.addObject("systemList", systemList);
		// 개인정보유형 리스트
		List<IndvinfoTypeSetup> indvinfoTypeList = commonDao.getIndvinfoTypeList();
		modelAndView.addObject("indvinfoTypeList", indvinfoTypeList);
		// 상세정보
		ExtrtBaseSetup extrtBaseSetupDetail = new ExtrtBaseSetup();
		if(search.getRule_seq()!=null && search.getRule_seq() != ""){
			extrtBaseSetupDetail = extrtBaseSetupDao.findExtrtBaseSetupDetailOne(search);
			// 개인정보유형
			if(extrtBaseSetupDetail.getScen_seq()==1000 && extrtBaseSetupDetail.getPrivacy_seq() != null && !extrtBaseSetupDetail.getPrivacy_seq().equals("")) {
				String[] arr = extrtBaseSetupDetail.getPrivacy_seq().split(",");
				modelAndView.addObject("indvinfoLength", arr.length);
				String privacy_desc = "";
				for(String a : arr) {
					for(IndvinfoTypeSetup i : indvinfoTypeList) {
						int privacy_seq = i.getPrivacy_seq();
						if(a.equals(String.valueOf(privacy_seq))) {
							privacy_desc += i.getPrivacy_desc()+", ";
							break;
						}
					}
				}
				privacy_desc = privacy_desc.substring(0, privacy_desc.length()-2);
				extrtBaseSetupDetail.setPrivacy_desc(privacy_desc);
			}
			// 참조값
			if(extrtBaseSetupDetail.getRef_val() != null && extrtBaseSetupDetail.getRef_val() != "") {
				String[] refArr = extrtBaseSetupDetail.getRef_val().split(",");
				modelAndView.addObject("ref_val", refArr);
			}
		} else {
			List<ExtrtBaseSetup> scenarioList = extrtBaseSetupDao.findScenarioList(search);
			modelAndView.addObject("scenarioList", scenarioList);
			extrtBaseSetupDetail.setScen_seq(search.getScen_seq());
			extrtBaseSetupDetail = extrtBaseSetupDao.findScenarioDetail(extrtBaseSetupDetail);
			search.setIsSearch("Y");
		}
		// 분석대상 - 시스템별
		if(extrtBaseSetupDetail.getIndv_yn().equals("N")) {
			String[] systemSeqArr = extrtBaseSetupDetail.getSystem_seq().split(",");
			modelAndView.addObject("systemSeqLength", systemSeqArr.length);
			String system_name = "";
			for(String a : systemSeqArr) {
				for(SystemMaster s : systemList) {
					if(a.equals(s.getSystem_seq())) {
						system_name += s.getSystem_name()+", ";
						break;
					}
				}
			}
			system_name = system_name.substring(0, system_name.length()-2);
			extrtBaseSetupDetail.setSystem_name(system_name);
		} else {
			extrtBaseSetupDetail.setSystem_seq("");
		}
		modelAndView.addObject("extrtBaseSetupDetail", extrtBaseSetupDetail);
		
		// 위험도 총계(해당되는 것 제외한 합계)
		int dngValSum = extrtBaseSetupDao.ruletblDngValSum(extrtBaseSetupDetail);
		modelAndView.addObject("dngValSum", dngValSum);
		
		modelAndView.addObject("search", search);
		return modelAndView;
	}

	@Override
	public void findExtrtBaseSetupList_download(DataModelAndView modelAndView, SetupSearch search, HttpServletRequest request) {
		search.setUseExcel("true");
		
		String fileName = "PSM_시나리오관리_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "시나리오관리";
		
		List<ExtrtBaseSetup> extrtBaseSetupList = extrtBaseSetupDao.findExtrtBaseSetupList(search);

		for(ExtrtBaseSetup extrtBaseSetup : extrtBaseSetupList) {
			String use_yn = extrtBaseSetup.getUse_yn();
			String indv_yn = extrtBaseSetup.getIndv_yn();
			String alarm_yn = extrtBaseSetup.getAlarm_yn();
			extrtBaseSetup.setUse_yn(use_yn.equals("Y") ? "사용" : "미사용");
			extrtBaseSetup.setIndv_yn(indv_yn.equals("Y") ? "통합" : "시스템별");
			extrtBaseSetup.setAlarm_yn(alarm_yn.equals("Y")? "사용" : "미사용");
			
			int limit_type = extrtBaseSetup.getLimit_type();
			if(limit_type==2) {
				extrtBaseSetup.setLimit_type_desc("개인별");
			} else if(limit_type==3) {
				extrtBaseSetup.setLimit_type_desc("부서별");
			} else if(limit_type==4) {
				extrtBaseSetup.setLimit_type_desc("순위");
			} else if(limit_type==5) {
				extrtBaseSetup.setLimit_type_desc("비율");
			} else {
				extrtBaseSetup.setLimit_type_desc("공통");
			}
		}
		
		String[] columns = new String[] {"rule_seq", "rule_nm", "indv_yn", "limit_type_desc", "limit_cnt", "dng_val", "use_yn", "alarm_yn"};
		String[] heads = new String[] {"상세시나리오코드", "상세시나리오명", "분석대상", "분석위험기준", "임계치", "위험지수", "사용여부", "알람사용여부"};
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", extrtBaseSetupList);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}
	
	@Override
	public DataModelAndView addExtrtBaseSetupOne(	Map<String, String> parameters) {
		ExtrtBaseSetup paramBean = CommonHelper.convertMapToBean(parameters, ExtrtBaseSetup.class);
		if(paramBean.getIndv_yn().equals("Y")) {
			String system_arr = extrtBaseSetupDao.systemSeqArr();
			paramBean.setSystem_seq(system_arr);
		}
		
		String result = "SUCCESS";
		int checkRuleSeq = 0;
		checkRuleSeq = extrtBaseSetupDao.checkRuleSeq(paramBean.getRule_seq());
		if(checkRuleSeq!= 0) {
			result = "EXIST";
		} else {
			TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
			try {
				extrtBaseSetupDao.addExtrtBaseSetupOne(paramBean);
				transactionManager.commit(transactionStatus);
			} catch(DataAccessException e) {
				result = "ERROR";
				logger.error("[ExtrtBaseSetupSvcImpl.addExtrtBaseSetupOne] MESSAGE : " + e.getMessage());
				transactionManager.rollback(transactionStatus);
				throw new ESException("SYS012J");
			}
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("result", result);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView saveExtrtBaseSetupOne(	Map<String, String> parameters) {
		String result = "ERROR";
		String limit_type_deleteYn = parameters.get("use_yn_search");
		
		ExtrtBaseSetup paramBean = CommonHelper.convertMapToBean(parameters, ExtrtBaseSetup.class);
		if(paramBean.getIndv_yn().equals("Y")) {
			String system_arr = extrtBaseSetupDao.systemSeqArr();
			paramBean.setSystem_seq(system_arr);
		}
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			extrtBaseSetupDao.saveExtrtBaseSetupOne(paramBean);
			if(limit_type_deleteYn.equals("2")) { // 개인별로 설정 후 다른 것으로 바꾼 경우
				extrtBaseSetupDao.removeThresholdIndvByRule(paramBean);
			} else if(limit_type_deleteYn.equals("3")) { // 부서별로 설정 후 다른 것으로 바꾼 경우
				extrtBaseSetupDao.removeThresholdDeptByRule(paramBean);
			}
			transactionManager.commit(transactionStatus);
			result = "SUCCESS";
		} catch(DataAccessException e) {
			e.printStackTrace();
			logger.error("[ExtrtBaseSetupSvcImpl.saveExtrtBaseSetupOne] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("result", result);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView removeExtrtBaseSetupOne(	Map<String, String> parameters) {
		ExtrtBaseSetup paramBean = CommonHelper.convertMapToBean(parameters, ExtrtBaseSetup.class);
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			// 개인별, 부서별 임계치 설정 삭제
			extrtBaseSetupDao.removeThresholdIndvByRule(paramBean);
			extrtBaseSetupDao.removeThresholdDeptByRule(paramBean);
			// 상세시나리오 삭제
			extrtBaseSetupDao.removeExtrtBaseSetupOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[ExtrtBaseSetupSvcImpl.removeExtrtBaseSetupOne] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView findEmpUserMngtList(EmpUserSearch search) {
		int pageSize = Integer.valueOf(defaultPageSize);
		
		search.setSize(pageSize);
		search.setTotal_count(empUserMngtDao.findEmpUserMngtOne_count(search));
		
		List<EmpUser> empUserList = empUserMngtDao.findEmpUserMngtList(search);
		
		Department departmentBean = new Department();
		departmentBean.setUse_flag('Y');
		List<Department> deptList = departmentMngtDao.findDepartmentMngtList_choose(departmentBean);
		
		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList();
		List<ExtrtBaseSetup> ruleinfoList = extrtBaseSetupDao.findRuleInfo();
		
		SimpleCode simpleCode = new SimpleCode("/extrtBaseSetup/indvlist.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("empUserList", empUserList);
		modelAndView.addObject("deptList", deptList);
		modelAndView.addObject("systemMasterList", systemMasterList);
		modelAndView.addObject("ruleinfoList", ruleinfoList);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView findExtrtBaseSetupDetail_indv(EmpUser search) {
		
		List<ExtrtBaseSetup> list = extrtBaseSetupDao.findExtrtBaseSetupDetail_indv(search);
		
		for(int i=0; i<list.size(); i++) {
			ExtrtBaseSetup ebs = list.get(i);
			float prct_val = ebs.getPrct_val();
			int avg_val = ebs.getAvg_val();
			BigDecimal prct = new BigDecimal(prct_val);
			BigDecimal avg = new BigDecimal(avg_val);
			
			// 결과값
			BigDecimal res = prct.multiply(avg);
			ebs.setDng_val(res.intValue());
			
			// 퍼센트로 보여준다
			BigDecimal tmp = new BigDecimal(100);
			prct = prct.multiply(tmp);
			ebs.setPrct_int(prct.intValue());
		}
		SimpleCode simpleCode = new SimpleCode();
		String index = "/extrtBaseSetup/indvdetail.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("extrtBaseSetupDetail", list);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView setExtrtBaseSetupDetail_indv(Map<String, String> parameters) {
		
		ExtrtBaseSetup paramBean = CommonHelper.convertMapToBean(parameters, ExtrtBaseSetup.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		
		int cnt = Integer.parseInt(parameters.get("totalcnt"));
		try {
			for (int i = 0; i < cnt; i++) {
				String rule_seq = parameters.get("rule_seq_" + i);
				paramBean.setRule_seq(Integer.parseInt(rule_seq));
				String use_yn = parameters.get("use_yn_" + i);
				paramBean.setUse_yn(use_yn);
				String prct_val = parameters.get("prct_val_" + i);
				BigDecimal bd = new BigDecimal(prct_val);
				BigDecimal bd2 = new BigDecimal(100);
				BigDecimal res = bd.divide(bd2);
				paramBean.setPrct_val(res.floatValue());
				extrtBaseSetupDao.setExtrtBaseSetupDetail_indv(paramBean);
			}
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[ExtrtBaseSetupSvcImpl.setExtrtBaseSetupDetail_indv] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView setExtrtBaseSetupAll_indv(Map<String, String> parameters) {

		ExtrtBaseSetup paramBean = CommonHelper.convertMapToBean(parameters, ExtrtBaseSetup.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		
		try {
			String prct_val = parameters.get("prct_val");
			BigDecimal prct = new BigDecimal(prct_val);
			BigDecimal tmp = new BigDecimal(100);
			prct = prct.divide(tmp);
			paramBean.setPrct_val(prct.floatValue());
			extrtBaseSetupDao.setExtrtBaseSetupAll_indv(paramBean);
			extrtBaseSetupDao.setExtrtBaseSetupDetail(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[ExtrtBaseSetupSvcImpl.setExtrtBaseSetupDetail_indv] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	



	@Override
	public DataModelAndView saveScenario(Map<String, String> parameters) {
		ExtrtBaseSetup paramBean = CommonHelper.convertMapToBean(parameters, ExtrtBaseSetup.class);
		if(paramBean.getIndv_yn().equals("Y")) {
			String system_arr = extrtBaseSetupDao.systemSeqArr();
			paramBean.setSystem_seq(system_arr);
		}
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			extrtBaseSetupDao.updateScenario(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[ExtrtBaseSetupSvcImpl.saveScenario] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS058J");
		}
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView removeScenario(Map<String, String> parameters) {
		
		ExtrtBaseSetup paramBean = CommonHelper.convertMapToBean(parameters, ExtrtBaseSetup.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		DataModelAndView modelAndView = new DataModelAndView();
		try {
			int cnt = extrtBaseSetupDao.findRuleTableByScenario(paramBean);
			if(cnt > 0)
				modelAndView.addObject("result", 1);
			else {
				extrtBaseSetupDao.removeScenario(paramBean);
				transactionManager.commit(transactionStatus);
			}
		} catch(DataAccessException e) {
			logger.error("[ExtrtBaseSetupSvcImpl.removeScenario] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS059J");
		}
		
		//modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView addScenario(Map<String, String> parameters) {
		DataModelAndView modelAndView = new DataModelAndView();
		String result = "";
		ExtrtBaseSetup paramBean = CommonHelper.convertMapToBean(parameters, ExtrtBaseSetup.class);
		
		int scenSeqCheck = extrtBaseSetupDao.scenSeqCheck(paramBean.getScen_seq());
		if(scenSeqCheck>0) {
			result = "scen_seq duplication";
			modelAndView.addObject("result", result);
			return modelAndView;
		}
		if(paramBean.getIndv_yn().equals("Y")) {
			String system_arr = extrtBaseSetupDao.systemSeqArr();
			paramBean.setSystem_seq(system_arr);
		}
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			extrtBaseSetupDao.addScenario(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[ExtrtBaseSetupSvcImpl.addScenario] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS060J");
		}
		
		modelAndView.addObject("result", result);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView findRuleTableByScenario(Map<String, String> parameters) {
		
		ExtrtBaseSetup paramBean = CommonHelper.convertMapToBean(parameters, ExtrtBaseSetup.class);
		
		DataModelAndView modelAndView = new DataModelAndView();
		try {
			int cnt = extrtBaseSetupDao.findRuleTableByScenario(paramBean);
			if(cnt > 0) {
				modelAndView.addObject("result", 1);
			} else {
				modelAndView.addObject("result", 0);
			}
			
		} catch(DataAccessException e) {
			logger.error("[ExtrtBaseSetupSvcImpl.findRuleTableByScenario] MESSAGE : " + e.getMessage());
			throw new ESException("SYS059J");
		}
		
		//modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView thresholdSetting_indv(SetupSearch search) {
		DataModelAndView modelAndView = new DataModelAndView();
		SimpleCode simpleCode = new SimpleCode("/extrtBaseSetup/thresholdSetting_indv.html");
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		Integer totalCount = extrtBaseSetupDao.thresholdDeptListCt(search);
		if(totalCount == null) {
			search.setTotal_count(0);
		} else {
			search.setTotal_count(totalCount);
		}
		List<ExtrtBaseSetup> ruleList = extrtBaseSetupDao.thresholdIndvList(search);
		modelAndView.addObject("ruleList", ruleList);
		modelAndView.addObject("search", search);
		return modelAndView;
	}
	
	@Override
	public String saveThreshold_indv(String jsonData) {
		Gson gson = new Gson();
		ExtrtBaseSetup[] arr = gson.fromJson(jsonData, ExtrtBaseSetup[].class);
		List<ExtrtBaseSetup> list = Arrays.asList(arr);
		
		String result = "success";
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			for(int i=0; i<list.size();i++) {
				extrtBaseSetupDao.insertThresholdIndv(list.get(i));
			}
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			result = "error";
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
		return result;
	}
	
	@Override
	public DataModelAndView thresholdSetting_dept(SetupSearch search) {
		DataModelAndView modelAndView = new DataModelAndView();
		SimpleCode simpleCode = new SimpleCode("/extrtBaseSetup/thresholdSetting_dept.html");
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		Integer totalCount = extrtBaseSetupDao.thresholdDeptListCt(search);
		if(totalCount == null) {
			search.setTotal_count(0);
		} else {
			search.setTotal_count(totalCount);
		}
		List<ExtrtBaseSetup> ruleList = extrtBaseSetupDao.thresholdDeptList(search);
		modelAndView.addObject("ruleList", ruleList);
		modelAndView.addObject("search", search);
		return modelAndView;
	}
	
	@Override
	public String saveThreshold_dept(String jsonData) {
		Gson gson = new Gson();
		ExtrtBaseSetup[] arr = gson.fromJson(jsonData, ExtrtBaseSetup[].class);
		List<ExtrtBaseSetup> list = Arrays.asList(arr);
		
		String result = "success";
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			for(int i=0; i<list.size();i++) {
				extrtBaseSetupDao.insertThresholdDept(list.get(i));
			}
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			e.printStackTrace();
			result = "error";
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
		return result;
	}

	@Override
	public DataModelAndView thresholdSettingDetail_dept(Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView();
		SimpleCode simpleCode = new SimpleCode("/extrtBaseSetup/thresholdSettingDetail_dept.html");
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		String rule_seq = parameters.get("rule_seq");
		ExtrtBaseSetup extrtBaseSetup = new ExtrtBaseSetup();
		if(rule_seq != null && rule_seq != "") {
			extrtBaseSetup.setRule_seq(Integer.parseInt(rule_seq));
		}
		List<ExtrtBaseSetup> ruleList = extrtBaseSetupDao.findDeptRuleList(extrtBaseSetup);
		modelAndView.addObject("ruleList", ruleList);
		List<ExtrtBaseSetup> thresholdList = extrtBaseSetupDao.thresholdSettingDeptList(extrtBaseSetup);
		modelAndView.addObject("thresholdList", thresholdList);
		
		modelAndView.addObject("parameters", parameters);
		return modelAndView;
	}

	@Override
	public DataModelAndView thresholdSettingDetail_indv(Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView();
		SimpleCode simpleCode = new SimpleCode("/extrtBaseSetup/thresholdSettingDetail_indv.html");
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		String rule_seq = parameters.get("rule_seq");
		ExtrtBaseSetup extrtBaseSetup = new ExtrtBaseSetup();
		if(rule_seq != null && rule_seq != "") {
			extrtBaseSetup.setRule_seq(Integer.parseInt(rule_seq));
		}
		List<ExtrtBaseSetup> ruleList = extrtBaseSetupDao.findIndvRuleList(extrtBaseSetup);
		modelAndView.addObject("ruleList", ruleList);
		List<ExtrtBaseSetup> thresholdList = extrtBaseSetupDao.thresholdSettingIndvList(extrtBaseSetup);
		modelAndView.addObject("thresholdList", thresholdList);
		
		modelAndView.addObject("parameters", parameters);
		return modelAndView;
	}


	@Override
	public String deleteThreshold_indv(ExtrtBaseSetup baseSetup) {
		String result = "success";
		try {
			extrtBaseSetupDao.removeThreshold_indv(baseSetup);
		} catch (Exception e) {
			result = "error";
		}
		return result;
	}

	@Override
	public String deleteThreshold_dept(ExtrtBaseSetup baseSetup) {
		String result = "success";
		try {
			extrtBaseSetupDao.removeThreshold_dept(baseSetup);
		} catch (Exception e) {
			result = "error";
		}
		return result;
	}
	
	
}
