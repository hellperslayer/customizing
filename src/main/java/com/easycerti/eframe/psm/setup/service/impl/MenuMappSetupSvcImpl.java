package com.easycerti.eframe.psm.setup.service.impl;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.ExcelFileReader;
import com.easycerti.eframe.common.util.XExcelFileReader;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.setup.dao.MenuMappSetupDao;
import com.easycerti.eframe.psm.setup.service.MenuMappSetupSvc;
import com.easycerti.eframe.psm.setup.vo.MenuMappSetup;
import com.easycerti.eframe.psm.setup.vo.MenuMappSetupList;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;
import com.easycerti.eframe.psm.system_management.dao.AgentMngtDao;
import com.easycerti.eframe.psm.system_management.dao.MenuMngtDao;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

/**
 * 
 * 설명 :	매뉴매핑설정 SvcImpl 
 * @author tjlee
 * @since 2015. 5. 4.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 4.           tjlee            최초 생성
 *
 * </pre>
 */
@Service
public class MenuMappSetupSvcImpl implements MenuMappSetupSvc{
private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private MenuMappSetupDao menuMappSetupDao;
	
	@Autowired
	private AgentMngtDao agentMngtDao;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;

	@Autowired
	private MenuMngtDao menuDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;

	@Override
	public DataModelAndView findMenuMappSetupList(SetupSearch search, HttpServletRequest request) {
		List<String> auth_idsList = (List<String>) request.getSession().getAttribute("auth_ids_list");
		search.setAuth_idsList(auth_idsList);
		
		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		search.setTotal_count(menuMappSetupDao.findMenuMappSetupOne_count(search));
		List<MenuMappSetup> menuMappSetup = menuMappSetupDao.findMenuMappSetupList(search);
		
		SearchSearch search2 = new SearchSearch();
		search2.setAuth_idsList(auth_idsList);
		
		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList_byAdmin(search2 );
		MenuMappSetupList menuMappSetupList = new MenuMappSetupList(menuMappSetup);
		
		SimpleCode simpleCode = new SimpleCode("/menuMappSetup/list.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("menuMappSetupList", menuMappSetupList);
		modelAndView.addObject("systemMasterList", systemMasterList);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView findMenuMappSetupDetail(SetupSearch search) {
		MenuMappSetup menuMappSetupDetail = null;
		if(search.getTag_seq() != ""){
			menuMappSetupDetail = menuMappSetupDao.findMenuMappSetupDetailMenu(search);
		}
		
		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList();
		
		SimpleCode simpleCode = new SimpleCode("/menuMappSetup/detail.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("menuMappSetupDetail", menuMappSetupDetail);
		modelAndView.addObject("systemMasterList", systemMasterList);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}

	@Override
	public void findMenuMappSetupList_download(DataModelAndView modelAndView, SetupSearch search, HttpServletRequest request) {

		search.setUseExcel("true");
		
		String fileName = "PSM_메뉴매핑설정_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "메뉴매핑설정";
		
		List<MenuMappSetup> menuMappSetupList = menuMappSetupDao.findMenuMappSetupList(search);
		
		for(MenuMappSetup menuMappSetup : menuMappSetupList) {
			String use_yn = menuMappSetup.getUse_yn();
			menuMappSetup.setUse_yn(use_yn.equals("Y") ? "사용" : "미사용");
		}
		
		String[] columns = new String[] {"tag_seq", "system_name", "url", "menu_name", "crud_type", "use_yn"};
		String[] heads = new String[] {"번호", "시스템", "URL주소", "메뉴명", "수행업무", "사용여부"};
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", menuMappSetupList);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}

	
	@Override
	public DataModelAndView addMenuMappSetupOne(Map<String, String> parameters) {
		MenuMappSetup paramBean = CommonHelper.convertMapToBean(parameters, MenuMappSetup.class);
		
		// Validation Check (privacy_seq 중복)
		/*MenuMappSetup MenuMappSetup = menuMappSetupDao.findMenuMappSetupDetail(paramBean);
		if(MenuMappSetup != null) {
			throw new ESException("SYS013J");
		}*/
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		
		try {
			menuMappSetupDao.addMenuMappSetupOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[MenuMappSetupSvcImpl.addMenuMappSetupOne] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS001J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView saveMenuMappSetupOne(Map<String, String> parameters) {
		MenuMappSetup paramBean = CommonHelper.convertMapToBean(parameters, MenuMappSetup.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			menuMappSetupDao.saveMenuMappSetupOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[MenuMappSetupSvcImpl.saveMenuMappSetupOne] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS002J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView removeMenuMappSetupOne(	Map<String, String> parameters) {
		MenuMappSetup paramBean = CommonHelper.convertMapToBean(parameters, MenuMappSetup.class);
		
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			menuMappSetupDao.removeMenuMappSetupOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[MenuMappSetupSvcImpl.removeMenuMappSetupOne] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS003J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	@Override
	public String upLoadSystemMngtOne(Map<String, String> parameters, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String result="false";
		MenuMappSetup paramBean = CommonHelper.convertMapToBean(parameters, MenuMappSetup.class);
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		//엑셀 Row 카운트는 1000으로 지정한다		
		int rowCount=5000;
		
		//파일 업로드 기능  		
		MultipartHttpServletRequest mpr = (MultipartHttpServletRequest) request;  
		
		MultipartFile dir = mpr.getFile("file");
				
		File file = new File("/root/excelUpload/" + dir.getOriginalFilename());
		
		if (!file.exists()) { //폴더 없으면 폴더 생성
			file.mkdirs();
		}
		
		dir.transferTo(file);
		
		//업로드된 파일의 정보를 읽어들여 DB에 추가하는 기능 
		if (dir.getSize() > 0 ) { 
				
			List<String[]> readRows = new ArrayList<>();
			
			String ext = file.getName().substring(file.getName().lastIndexOf(".")+1);
			ext = ext.toLowerCase();
			if( "xlsx".equals(ext) ){	
				readRows = new XExcelFileReader(file.getAbsolutePath()).readRows(rowCount);
				
			}else if( "xls".equals(ext) ){	
				readRows = new ExcelFileReader(file.getAbsolutePath()).readRows(rowCount);
			}
		
			for(int i=0; i<3; i++){	
				readRows.remove(0);
			}
			
			boolean isCheck = true;
			if( readRows.size() != 0 ){
				
			/*int columnCount = readRows.get(0).length;*/
			//각 셀 내용을 DB 컬럼과 Mapping
			try {
				for(String[] readRowArr : readRows){
					if(readRowArr.length != 0 && readRowArr[1].length() != 0){
						paramBean.setUrl(readRowArr[1]);
						paramBean.setMenu_name(readRowArr[2]);
						paramBean.setCrud_type(readRowArr[3]);
						paramBean.setUse_yn(readRowArr[4]);
						paramBean.setSystem_seq(readRowArr[0]);
						menuMappSetupDao.uploadMenuMappSetupOne(paramBean);
					}
				}
				if(isCheck) {
					readRows.clear();
					transactionManager.commit(transactionStatus);
				}
			} catch(DataAccessException e) {
				transactionManager.rollback(transactionStatus);
				readRows.clear();
				e.printStackTrace();
			/*	logger.error("[EmpUserMngtSvcImpl.upLoadEmpUserMngtOne] MESSAGE : " + e.getMessage());
				throw new ESException("SYS039J");*/
				return result;
			}
		}
			if(isCheck)
				result="true";
		}
		
		return result;	
	}
	
	@Override
	public DataModelAndView findMenuPrivFindSetupList(SetupSearch search) {
		
		int pageSize = Integer.valueOf("5");
		search.setSize(pageSize);
		search.setTotal_count(menuMappSetupDao.findMenuPrivFindSetupList_count(search));
		List<MenuMappSetup> menuPrivFindList = menuMappSetupDao.findMenuPrivFindSetupList(search);
		MenuMappSetup menuMappDetail = menuMappSetupDao.findMenuMappSetupDetailMenu(search);
		
		SimpleCode simpleCode = new SimpleCode("/menuMappSetup/menuPrivFindList.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("menuPrivFindList", menuPrivFindList);
		modelAndView.addObject("menuMappDetail", menuMappDetail);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView addPrivFind(Map<String, String> parameters) {
		MenuMappSetup paramBean = CommonHelper.convertMapToBean(parameters, MenuMappSetup.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		
		try {
			menuMappSetupDao.addPrivFind(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[MenuMappSetupSvcImpl.addPrivFind] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS066J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView savePrivFind(Map<String, String> parameters) {
		MenuMappSetup paramBean = CommonHelper.convertMapToBean(parameters, MenuMappSetup.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		
		try {
			menuMappSetupDao.savePrivFind(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[MenuMappSetupSvcImpl.savePrivFind] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS068J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView removePrivFind(	Map<String, String> parameters) {
		MenuMappSetup paramBean = CommonHelper.convertMapToBean(parameters, MenuMappSetup.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			menuMappSetupDao.removePrivFind(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[MenuMappSetupSvcImpl.removePrivFind] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS067J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}

}
