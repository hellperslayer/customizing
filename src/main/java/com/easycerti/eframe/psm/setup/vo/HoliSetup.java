package com.easycerti.eframe.psm.setup.vo;

import com.easycerti.eframe.common.util.PageInfo;
import com.easycerti.eframe.common.vo.AbstractValueObject;

public class HoliSetup extends AbstractValueObject {
	// 휴일날짜
	private String holi_dt;
	
	// 휴일 이름
	private String holi_nm;
	
	// 휴일 여부
	private String holi_yn;
	
	// 근무 시작시간
	private String work_start_tm;
	
	// 근무 종료시간
	private String work_end_tm;
	
	
	// 휴일 등록 시 생성할 년도
	private int add_year;
	
	// 페이징 정보
	private PageInfo pageInfo;

	// 날짜 충돌처리 옵션
	private String conflict;
	
	public HoliSetup() {
		this.holi_dt = "";
		this.holi_nm = "";
		this.holi_yn = "";
		this.work_start_tm = "";
		this.work_end_tm = "";
	}
	
	public String getConflict() {
		return conflict;
	}

	public void setConflict(String conflict) {
		this.conflict = conflict;
	}

	public String getHoli_dt() {
		return holi_dt;
	}

	public void setHoli_dt(String holi_dt) {
		this.holi_dt = holi_dt;
	}

	public String getHoli_nm() {
		return holi_nm;
	}

	public void setHoli_nm(String holi_nm) {
		this.holi_nm = holi_nm;
	}

	public String getHoli_yn() {
		return holi_yn;
	}

	public void setHoli_yn(String holi_yn) {
		this.holi_yn = holi_yn;
	}

	public String getWork_start_tm() {
		return work_start_tm;
	}

	public void setWork_start_tm(String work_start_tm) {
		this.work_start_tm = work_start_tm;
	}

	public String getWork_end_tm() {
		return work_end_tm;
	}

	public void setWork_end_tm(String work_end_tm) {
		this.work_end_tm = work_end_tm;
	}

	public PageInfo getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(PageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}

	public int getAdd_year() {
		return add_year;
	}

	public void setAdd_year(int add_year) {
		this.add_year = add_year;
	}
	
}
