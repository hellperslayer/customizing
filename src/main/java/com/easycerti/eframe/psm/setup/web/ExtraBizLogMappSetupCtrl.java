package com.easycerti.eframe.psm.setup.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.psm.setup.service.ExtraBizLogMappSetupSvc;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;

@Controller
@RequestMapping("/extraBizLogMappSetup/*")
public class ExtraBizLogMappSetupCtrl {
	@Autowired
	private ExtraBizLogMappSetupSvc extraBizLogMappSetupSvc;
	
	@RequestMapping(value={"list.html"}, method={RequestMethod.POST})
	public DataModelAndView findExtraBizLogMappSetupList(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = extraBizLogMappSetupSvc.findExtraBizLogMappSetupList(search); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("extraBizLogMappSetupList");

		return modelAndView;
	}
	
	@RequestMapping(value={"detail.html"}, method={RequestMethod.POST})
	public DataModelAndView findMenuMappSetupDetail(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = extraBizLogMappSetupSvc.findExtraBizLogMappSetupDetail(search); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("extraBizLogMappSetupDetail");

		return modelAndView;
	}
	
	/*@RequestMapping(value="download.html", method={RequestMethod.POST})
	public DataModelAndView addMenuMappSetupList_download(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		menuMappSetupSvc.findMenuMappSetupList_download(modelAndView, search, request);
		
		return modelAndView;
	}*/
	
	
	@RequestMapping(value={"add.html"}, method={RequestMethod.POST})
	public DataModelAndView addMenuMappSetup(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		parameters = CommonHelper.checkSearchDate(parameters);
		
		//String[] args = {"tag_seq", "url"};
		String[] args = {"url"};
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		DataModelAndView modelAndView = extraBizLogMappSetupSvc.addExtraBizLogMappSetupOne(parameters); 
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	@RequestMapping(value={"save.html"}, method={RequestMethod.POST})
	public DataModelAndView saveMenuMappSetup(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		//parameters = CommonHelper.checkSearchDate(parameters);
		
		String[] args = {"url"};
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		DataModelAndView modelAndView = extraBizLogMappSetupSvc.saveExtraBizLogMappSetupOne(parameters);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	@RequestMapping(value={"remove.html"}, method={RequestMethod.POST})
	public DataModelAndView removeMenuMappSetup(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		parameters = CommonHelper.checkSearchDate(parameters);
		
		DataModelAndView modelAndView = extraBizLogMappSetupSvc.removeExtraBizLogMappSetupOne(parameters);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	/*@RequestMapping("exdownload.html")
	public ModelAndView download()throws Exception{
		
		String path = EmpUserMngtCtrl.class.getResource("").getPath();
		 //System.out.println(path);
		File down = new File(path + "excelExam/메뉴매핑_업로드양식.xlsx");
		return new ModelAndView("download","downloadFile",down);
	}
	
	@RequestMapping(value="upload.html", method={RequestMethod.POST})
	@ResponseBody
	public String upMenuMappSetupMngtOne(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
 		parameters.put("insert_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		String result = menuMappSetupSvc.upLoadSystemMngtOne(parameters,request, response);
		
		return result;
	}
	
	@RequestMapping(value={"setlist.html"}, method={RequestMethod.POST})
	public DataModelAndView findMenuMappSetList(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = menuMappSetupSvc.findMenuMappSetupList(search); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("menuMappSetList");

		return modelAndView;
	}*/
}
