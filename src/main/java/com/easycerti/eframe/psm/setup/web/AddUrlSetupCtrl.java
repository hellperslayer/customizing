package com.easycerti.eframe.psm.setup.web;

import java.io.File;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.psm.setup.service.AddUrlSetupSvc;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;

@Controller
@RequestMapping("/addUrlSetup/*")
public class AddUrlSetupCtrl {
	
	@Autowired
	private AddUrlSetupSvc addUrlSetupSvc;
	
	/**
	 * URL 목록
	 * @author yrchoo
	 * @since 2015. 4. 27.
	 * @return DataModelAndView
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[정책관리] 제외URL매핑설정")
	@RequestMapping(value="list.html", method={RequestMethod.POST})
	public DataModelAndView findAddUrlSetupList(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
		DataModelAndView modelAndView = addUrlSetupSvc.findAddUrlSetupList(search);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("addUrlSetupList");
		
		return modelAndView;
	}
	
	/**
	 * URL 상세
	 * @author yrchoo
	 * @since 2015. 4. 27.
	 * @return DataModelAndView
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[정책관리] URL 매핑설정 상세")
	@RequestMapping(value="detail.html",method={RequestMethod.POST})
	public DataModelAndView findAddUrlSetupOne(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception {
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
		DataModelAndView modelAndView = addUrlSetupSvc.findAddUrlSetupOne(search);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("addUrlSetupDetail");
		
		return modelAndView;
	}
	
	/**
	 * 개인정보유형설정 엑셀 다운로드
	 */
	@RequestMapping(value="download.html", method={RequestMethod.POST})
	public DataModelAndView addAddUrlSetupList_download(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
		addUrlSetupSvc.findAddUrlSetupList_download(modelAndView, search, request);
		
		return modelAndView;
	}
	
	/**
	 * URL 등록
	 * @author yrchoo
	 * @since 2015. 4. 27.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="add.html",method={RequestMethod.POST})
	public DataModelAndView addAddUrlSetupOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception {
		
		DataModelAndView modelAndView = addUrlSetupSvc.addAddUrlSetupOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	/**
	 * URL 수정
	 * @author yrchoo
	 * @since 2015. 4. 27.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="save.html",method={RequestMethod.POST})
	public DataModelAndView saveAddUrlSetupOne(@RequestParam Map<String, String> parameters,HttpServletRequest request) throws Exception{
		String[] args = {"seq"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		DataModelAndView modelAndView = addUrlSetupSvc.saveAddUrlSetupOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	/**
	 * URL 삭제
	 * @author yrchoo
	 * @since 2015. 4. 27.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="remove.html",method={RequestMethod.POST})
	public DataModelAndView removeAddUrlSetupOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		
		DataModelAndView modelAndView = addUrlSetupSvc.removeAddUrlSetupOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	
	
	@RequestMapping(value="upload.html", method={RequestMethod.POST})
	@ResponseBody
	public String upLoadAddurlSetup(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
		String result = addUrlSetupSvc.upLoadAddUrlSetup(parameters,request, response);
		return result;
	}
	
	@RequestMapping("exdownload.html")
	public ModelAndView download(HttpServletRequest request)throws Exception{
		String path = request.getSession().getServletContext().getRealPath("/")+"WEB-INF/";
		File down = new File(path + "excelExam/URL설정_업로드양식.xlsx");
		return new ModelAndView("download","downloadFile",down);
	}

}
