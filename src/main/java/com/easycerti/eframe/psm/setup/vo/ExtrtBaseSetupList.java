package com.easycerti.eframe.psm.setup.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.AbstractValueObject;
/**
 * 
 * 설명 : 추출조건설정 리스트 VO
 * @author tjlee
 * @since 2015. 4. 24.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 24.           tjlee            최초 생성
 *
 * </pre>
 */
public class ExtrtBaseSetupList extends AbstractValueObject{

	// 추출조건설정 리스트
	private List<ExtrtBaseSetup> extrtBaseSetup = null;

	
	public ExtrtBaseSetupList(){
		
	}
	
	public ExtrtBaseSetupList(List<ExtrtBaseSetup> extrtBaseSetup){
		this.extrtBaseSetup = extrtBaseSetup;
	}
	
	public ExtrtBaseSetupList(List<ExtrtBaseSetup> extrtBaseSetup, String page_total_count){
		this.extrtBaseSetup = extrtBaseSetup;
		setPage_total_count(page_total_count);
	}

	public List<ExtrtBaseSetup> getExtrtBaseSetup() {
		return extrtBaseSetup;
	}

	public void setExtrtBaseSetup(List<ExtrtBaseSetup> extrtBaseSetup) {
		this.extrtBaseSetup = extrtBaseSetup;
	}
	
}
