package com.easycerti.eframe.psm.setup.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.AbstractValueObject;
/**
 * 전체로그조회 리스트 Bean
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 * 
 *
 * </pre>
 */
public class AddUrlSetupList extends AbstractValueObject{

	// 추가url 리스트
	private List<AddUrlSetup> addUrlSetup = null;

	
	public AddUrlSetupList(){
		
	}
	
	public AddUrlSetupList(List<AddUrlSetup> addUrlSetup){
		this.addUrlSetup = addUrlSetup;
	}
	
	public AddUrlSetupList(List<AddUrlSetup> addUrlSetup, String page_total_count){
		this.addUrlSetup = addUrlSetup;
		setPage_total_count(page_total_count);
	}

	public List<AddUrlSetup> getAddUrlSetup() {
		return addUrlSetup;
	}

	public void setAddUrlSetup(List<AddUrlSetup> addUrlSetup) {
		this.addUrlSetup = addUrlSetup;
	}
}
