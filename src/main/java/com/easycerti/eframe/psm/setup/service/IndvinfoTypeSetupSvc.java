package com.easycerti.eframe.psm.setup.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.setup.vo.IndvinfoTypeSetup;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;
/**
 * 
 * 설명 : 개인정보유형설정 Svc
 * @author tjlee
 * @since 2015. 5. 4.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 4.           tjlee            최초 생성
 *
 * </pre>
 */
public interface IndvinfoTypeSetupSvc {
	/**
	 * 설명 : 개인정보유형설정 리스트
	 * @author tjlee
	 * @since 2015. 6. 4.
	 * @return DataModelAndView
	 */
	public DataModelAndView findIndvinfoTypeSetupList(SetupSearch search); 
	
	/**
	 * 설명 : 개인정보유형설정 상세
	 * @author tjlee
	 * @since 2015. 6. 4.
	 * @return DataModelAndView
	 */
	public DataModelAndView findIndvinfoTypeSetupDetail(SetupSearch search);
	
	/**
	 * 설명 : 개인정보유형설정 등록 
	 * @author tjlee
	 * @since 2015. 6. 4.
	 * @return DataModelAndView
	 */
	public DataModelAndView addIndvinfoTypeSetupOne(	Map<String, String> parameters);
	
	/**
	 * 설명 : 개인정보유형설정 수정 
	 * @author tjlee
	 * @since 2015. 6. 4.
	 * @return DataModelAndView
	 */
	public DataModelAndView saveIndvinfoTypeSetupOne(Map<String, String> parameters);
	
	/**
	 * 설명 : 개인정보유형설정 삭제
	 * @author tjlee
	 * @since 2015. 6. 4.
	 * @return DataModelAndView
	 */
	public DataModelAndView removeIndvinfoTypeSetupOne(Map<String, String> parameters);
	
	/**
	 * 설명 : 개인정보유형설정 엑셀다운로드
	 * @author tjlee
	 * @since 2015. 6. 4.
	 * @return void
	 */
	public void findIndvinfoTypeSetupList_download(DataModelAndView modelAndView, SetupSearch search, HttpServletRequest request);
	
	public DataModelAndView findReferenceManagerList(SetupSearch search); 
	public DataModelAndView findReferenceManagerDetailList(SetupSearch search); 
	List<IndvinfoTypeSetup> findReferenceColumList(SetupSearch search); 
	public DataModelAndView findReferenceManagerDetail(SetupSearch search);
	
	//참조테이블 관리 상세내용 수정
	public DataModelAndView saveReferenceManagerDetail(Map<String, String> parameters, SetupSearch search);
	//참조테이블 관리 컬럼 데이터 1row 삭제
	public DataModelAndView removeReferenceManagerDetailOne(Map<String, String> parameters, SetupSearch search);

	public DataModelAndView findIndvinfoTypeSetupList_kdic(SetupSearch search);

	public DataModelAndView findIndvinfoTypeSetupDetail_kdic(SetupSearch search);

	public DataModelAndView saveIndvinfoTypeSetupDetail_kdic(Map<String, String> parameters);

	public DataModelAndView addIndvinfoTypeSetupDetail_kdic(Map<String, String> parameters);

	public DataModelAndView deleteIndvinfoTypeSetupDetail_kdic(Map<String, String> parameters);
}
