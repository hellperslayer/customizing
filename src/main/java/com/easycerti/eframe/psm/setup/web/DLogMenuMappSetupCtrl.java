package com.easycerti.eframe.psm.setup.web;

import java.io.File;
import java.io.InputStream;
import java.util.Map;

import javax.servlet.GenericServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.psm.setup.service.DLogMenuMappSetupSvc;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;
import com.easycerti.eframe.psm.system_management.web.EmpUserMngtCtrl;
/**
 * 
 * 설명 : 매뉴매핑설정 Controller 
 * @author tjlee
 * @since 2015. 4. 24.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 24.           tjlee            최초 생성
 *
 * </pre>
 */
@Controller
@RequestMapping("/dLogMenuMappSetup/*")
public class DLogMenuMappSetupCtrl {
	@Autowired
	private DLogMenuMappSetupSvc dLogMenuMappSetupSvc;
	/**
	 * 매뉴매핑설정 List
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[정책관리] 다운로드매핑설정")
	@RequestMapping(value={"list.html"}, method={RequestMethod.POST})
	public DataModelAndView findDLogMenuMappSetupList(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = dLogMenuMappSetupSvc.findDLogMenuMappSetupList(search,request); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("dLogMenuMappSetupList");

		return modelAndView;
	}
	
	/**
	 * 매뉴매핑설정상세
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[정책관리] 다운로드매핑설정 상세")
	@RequestMapping(value={"detail.html"}, method={RequestMethod.POST})
	public DataModelAndView findDLogMenuMappSetupDetail(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = dLogMenuMappSetupSvc.findDLogMenuMappSetupDetail(search); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("dLogMenuMappSetupDetail");

		return modelAndView;
	}
	
	/**
	 * 개인정보유형설정 엑셀 다운로드
	 */
	@RequestMapping(value="download.html", method={RequestMethod.POST})
	public DataModelAndView addDLogMenuMappSetupList_download(@ModelAttribute("search") SetupSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		dLogMenuMappSetupSvc.findDLogMenuMappSetupList_download(modelAndView, search, request);
		
		return modelAndView;
	}
	
	
	/**
	 * 매뉴매핑설정 등록
	 */
	@RequestMapping(value={"add.html"}, method={RequestMethod.POST})
	public DataModelAndView addDLogMenuMappSetup(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		parameters = CommonHelper.checkSearchDate(parameters);
		
		//String[] args = {"tag_seq", "url"};
		String[] args = {"url"};
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		DataModelAndView modelAndView = dLogMenuMappSetupSvc.addDLogMenuMappSetupOne(parameters); 
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	/**
	 * 매뉴매핑설정 수정
	 */
	@RequestMapping(value={"save.html"}, method={RequestMethod.POST})
	public DataModelAndView saveDLogMenuMappSetup(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		//parameters = CommonHelper.checkSearchDate(parameters);
		
		String[] args = {"url"};
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		DataModelAndView modelAndView = dLogMenuMappSetupSvc.saveDLogMenuMappSetupOne(parameters); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	/**
	 * 매뉴매핑설정 삭제
	 */
	@RequestMapping(value={"remove.html"}, method={RequestMethod.POST})
	public DataModelAndView removeDLogMenuMappSetup(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		parameters = CommonHelper.checkSearchDate(parameters);
		
		DataModelAndView modelAndView = dLogMenuMappSetupSvc.removeDLogMenuMappSetupOne(parameters); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	@RequestMapping("exdownload.html")
	public ModelAndView download(HttpServletRequest request)throws Exception{
		String path = request.getSession().getServletContext().getRealPath("/")+"WEB-INF/";
		System.out.println(path);
		File down = new File(path + "excelExam/다운로드_메뉴매핑_업로드양식.xlsx");
		return new ModelAndView("download","downloadFile",down);
	}
	
	@RequestMapping(value="upload.html", method={RequestMethod.POST})
	@ResponseBody
	public String upDLogMenuMappSetupMngtOne(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
 		parameters.put("insert_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		String result = dLogMenuMappSetupSvc.upLoadSystemMngtOne(parameters,request, response);
		
		return result;
	}
	
	
}
