package com.easycerti.eframe.psm.setup.vo;
import com.easycerti.eframe.common.util.PageInfo;
import com.easycerti.eframe.common.vo.AbstractValueObject;
/**
 * 
 * 설명 : 태그관리 VO
 * @author tjlee
 * @since 2015. 4. 27.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 27.           tjlee            최초 생성
 *
 * </pre>
 */
public class MenuMappSetup extends AbstractValueObject{
	private int tag_seq;          // 태그관리번호
	private String url;           //  URL주소
	private String url_type;     // URL 매핑타입
	private String parameter;     // 파라미터
	private String menu_name;     // 메뉴명
	private String crud_type;     // CRUD타입
	private String use_yn;        // 사용여부
	private String system_seq;        // 시스템코드
	private String system_name; // 시스템명
	private PageInfo pageInfo; // 페이징 변수 
	private String forcelog_yn;
	private String forcelog_privacy_type;
	private String forcelog_desc;
	private int tag_regexp_seq;
	private String privacy_type;
	private String privacy_type_str;
	private String regular_expression;
	private int get_group_num;
	private String use_flag;
	
	
	public String getPrivacy_type_str() {
		return privacy_type_str;
	}
	public void setPrivacy_type_str(String privacy_type_str) {
		this.privacy_type_str = privacy_type_str;
	}
	public String getUse_flag() {
		return use_flag;
	}
	public void setUse_flag(String use_flag) {
		this.use_flag = use_flag;
	}
	public int getTag_regexp_seq() {
		return tag_regexp_seq;
	}
	public void setTag_regexp_seq(int tag_regexp_seq) {
		this.tag_regexp_seq = tag_regexp_seq;
	}
	public String getPrivacy_type() {
		return privacy_type;
	}
	public void setPrivacy_type(String privacy_type) {
		this.privacy_type = privacy_type;
	}
	public String getRegular_expression() {
		return regular_expression;
	}
	public void setRegular_expression(String regular_expression) {
		this.regular_expression = regular_expression;
	}
	public int getGet_group_num() {
		return get_group_num;
	}
	public void setGet_group_num(int get_group_num) {
		this.get_group_num = get_group_num;
	}
	public int getTag_seq() {
		return tag_seq;
	}
	public void setTag_seq(int tag_seq) {
		this.tag_seq = tag_seq;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUrl_type() {
		return url_type;
	}
	public void setUrl_type(String url_type) {
		this.url_type = url_type;
	}
	public String getParameter() {
		return parameter;
	}
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}
	public String getMenu_name() {
		return menu_name;
	}
	public void setMenu_name(String menu_name) {
		this.menu_name = menu_name;
	}
	public String getCrud_type() {
		return crud_type;
	}
	public void setCrud_type(String crud_type) {
		this.crud_type = crud_type;
	}
	public String getUse_yn() {
		return use_yn;
	}
	public void setUse_yn(String use_yn) {
		this.use_yn = use_yn;
	}
	public String getSystem_seq() {
		return system_seq;
	}
	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}
	public String getSystem_name() {
		return system_name;
	}
	public void setSystem_name(String system_name) {
		this.system_name = system_name;
	}
	public PageInfo getPageInfo() {
		return pageInfo;
	}
	public void setPageInfo(PageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}
	public String getForcelog_yn() {
		return forcelog_yn;
	}
	public void setForcelog_yn(String forcelog_yn) {
		this.forcelog_yn = forcelog_yn;
	}
	public String getForcelog_privacy_type() {
		return forcelog_privacy_type;
	}
	public void setForcelog_privacy_type(String forcelog_privacy_type) {
		this.forcelog_privacy_type = forcelog_privacy_type;
	}
	public String getForcelog_desc() {
		return forcelog_desc;
	}
	public void setForcelog_desc(String forcelog_desc) {
		this.forcelog_desc = forcelog_desc;
	}
	@Override
	public String toString() {
		return "MenuMappSetup [tag_seq=" + tag_seq + ", " + (url != null ? "url=" + url + ", " : "") + (url_type != null ? "url_type=" + url_type + ", " : "")
				+ (parameter != null ? "parameter=" + parameter + ", " : "") + (menu_name != null ? "menu_name=" + menu_name + ", " : "") + (crud_type != null ? "crud_type=" + crud_type + ", " : "")
				+ (use_yn != null ? "use_yn=" + use_yn + ", " : "") + (system_seq != null ? "system_seq=" + system_seq + ", " : "") + (system_name != null ? "system_name=" + system_name + ", " : "")
				+ (pageInfo != null ? "pageInfo=" + pageInfo : "") + "]";
	}
	
}
