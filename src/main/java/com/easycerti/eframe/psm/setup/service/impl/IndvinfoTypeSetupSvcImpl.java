package com.easycerti.eframe.psm.setup.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInq;
import com.easycerti.eframe.psm.setup.dao.IndvinfoTypeSetupDao;
import com.easycerti.eframe.psm.setup.service.IndvinfoTypeSetupSvc;
import com.easycerti.eframe.psm.setup.vo.IndvinfoTypeSetup;
import com.easycerti.eframe.psm.setup.vo.IndvinfoTypeSetupList;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;
import com.easycerti.eframe.psm.system_management.dao.CodeMngtDao;
import com.easycerti.eframe.psm.system_management.dao.MenuMngtDao;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;
/**
 * 
 * 설명 : 개인정보유형설정 SvcImpl 
 * @author tjlee
 * @since 2015. 5. 4.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 4.           tjlee            최초 생성
 *
 * </pre>
 */
@Service
public class IndvinfoTypeSetupSvcImpl implements IndvinfoTypeSetupSvc{
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private IndvinfoTypeSetupDao indvinfoTypeSetupDao;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;

	@Autowired
	private MenuMngtDao menuDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private CodeMngtDao codeMngtDao;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;

	@Override
	public DataModelAndView findIndvinfoTypeSetupList(SetupSearch search) {
		DataModelAndView modelAndView = new DataModelAndView();
		SimpleCode simpleCode = new SimpleCode("/indvinfoTypeSetup/list.html");
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		
		List<SystemMaster> SystemMasterList = indvinfoTypeSetupDao.getSystemMasterListByAuth2(search);
		
		if ( SystemMasterList.size() > 0 ) {
			if ( search.getSystem_seq() == null || search.getSystem_seq().length() == 0 ) {
				search.setSystem_seq(SystemMasterList.get(0).getSystem_seq());
			}
		}
		search.setTotal_count(indvinfoTypeSetupDao.findIndvinfoTypeSetupOne_count(search));
		
		List<IndvinfoTypeSetup> indvinfoTypeSetup = indvinfoTypeSetupDao.findIndvinfoTypeSetupList(search);
		
		IndvinfoTypeSetupList indvinfoTypeSetupList = new IndvinfoTypeSetupList(indvinfoTypeSetup);
		
		int indvinfoTypeCnt = indvinfoTypeSetupDao.indvinfoTypeCnt();
		modelAndView.addObject("indvinfoTypeCnt", indvinfoTypeCnt);
		
		modelAndView.addObject("SystemMasterList", SystemMasterList);
		modelAndView.addObject("indvinfoTypeSetupList", indvinfoTypeSetupList);
		
		return modelAndView;
	} 

	@Override
	public DataModelAndView findIndvinfoTypeSetupDetail(SetupSearch search) {
		
		IndvinfoTypeSetup indvinfoTypeSetup = null;
		if(search.getPrivacy_seq() != ""){
			indvinfoTypeSetup = indvinfoTypeSetupDao.findIndvinfoTypeSetupDetail(search);
		}
		
		SimpleCode simpleCode = new SimpleCode("/indvinfoTypeSetup/detail.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("indvinfoTypeSetup", indvinfoTypeSetup);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}

	@Override
	public void findIndvinfoTypeSetupList_download(DataModelAndView modelAndView, SetupSearch search, HttpServletRequest request) {
		search.setUseExcel("true");
		String fileName = "PSM_개인정보유형설정_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "개인정보유형";
		
		List<IndvinfoTypeSetup> indvinfoTypeSetupList = indvinfoTypeSetupDao.findIndvinfoTypeSetupList(search);
		
		for(IndvinfoTypeSetup indvinfoTypeSetup : indvinfoTypeSetupList) {
			String use_flag = indvinfoTypeSetup.getUse_flag();
			indvinfoTypeSetup.setUse_flag(use_flag.equals("Y") ? "사용" : "미사용");
			indvinfoTypeSetup.setPrivacy_masking(indvinfoTypeSetup.getPrivacy_masking().equals("Y")? "사용":"미사용");
			indvinfoTypeSetup.setUse_biz_log_result(indvinfoTypeSetup.getUse_biz_log_result().equals("Y")? "사용":"미사용");
		}
		
		String[] columns = new String[] {"privacy_type", "privacy_desc", "use_flag", "privacy_masking", "use_biz_log_result"};
		String[] heads = new String[] {"타입", "개인정보유형 설명", "사용여부", "개인정보 마스킹", "개인정보 내용보기"};
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", indvinfoTypeSetupList);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}
	
	@Override
	public DataModelAndView addIndvinfoTypeSetupOne(	Map<String, String> parameters) {
		DataModelAndView modelAndView = new DataModelAndView();
		IndvinfoTypeSetup paramBean = CommonHelper.convertMapToBean(parameters, IndvinfoTypeSetup.class);
		// Validation Check (privacy_seq 중복)
		IndvinfoTypeSetup indvinfoTypeSetup = indvinfoTypeSetupDao.findIndvinfoTypeSetupOne(paramBean);
		if(indvinfoTypeSetup != null) {
			throw new ESException("SYS013J");
		}
		
		int result_type_order = paramBean.getResult_type_order();
		int checkVal = indvinfoTypeSetupDao.indvinfoResultTypeOrderCheck(paramBean);
		if(checkVal>0) {
			throw new ESException("SYS012J");
		}
		
		Code code = new Code();
		code.setGroup_code_id("RESULT_TYPE");
		code.setCode_id(String.valueOf(paramBean.getPrivacy_seq()));
		code.setSort_order(paramBean.getPrivacy_seq());
		code.setUse_flag(paramBean.getUse_flag());
		code.setCode_type("SYSTEM");
		code.setCode_name(paramBean.getPrivacy_desc());
		code.setDescription(paramBean.getPrivacy_desc());
		
		Code resCode = codeMngtDao.findCodeMngtOne(code);
		if (resCode != null) {
			throw new ESException("SYS013J");
		}
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		
		try {
			indvinfoTypeSetupDao.addIndvinfoTypeSetupOne(paramBean);
			indvinfoTypeSetupDao.addIndvinfoTypeSetupOneUse(paramBean);
			codeMngtDao.addCodeMngtOne(code);
			
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[IndvinfoTypeSetupSvcImpl.addIndvinfoTypeSetupOne] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
		
		
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView saveIndvinfoTypeSetupOne(Map<String, String> parameters) {
		IndvinfoTypeSetup paramBean = CommonHelper.convertMapToBean(parameters, IndvinfoTypeSetup.class);
		
		int result_type_order = paramBean.getResult_type_order();
		int checkVal = indvinfoTypeSetupDao.indvinfoResultTypeOrderCheck(paramBean);
		if(checkVal>0) {
			throw new ESException("SYS012J");
		}
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			indvinfoTypeSetupDao.saveIndvinfoTypeSetupOne(paramBean);
			
			paramBean.setPrivacy_type(Integer.toString(paramBean.getPrivacy_seq()));
			indvinfoTypeSetupDao.addIndvinfoTypeSetupOneUse_conflict(paramBean);
			indvinfoTypeSetupDao.saveIndvinfoTypeSetupOneUse(paramBean);
			indvinfoTypeSetupDao.saveIndvinfoTypeSetupOneUseMasking(paramBean);
			indvinfoTypeSetupDao.saveIndvinfoTypeSetupOneUseBizLogResult(paramBean);
			transactionManager.commit(transactionStatus);
			
		} catch(DataAccessException e) {
			logger.error("[IndvinfoTypeSetupSvcImpl.saveIndvinfoTypeSetupOne] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView removeIndvinfoTypeSetupOne(	Map<String, String> parameters) {
		IndvinfoTypeSetup paramBean = CommonHelper.convertMapToBean(parameters, IndvinfoTypeSetup.class);
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			indvinfoTypeSetupDao.removeIndvinfoTypeSetupOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[IndvinfoTypeSetupSvcImpl.removeIndvinfoTypeSetupOne] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	// 참조테이블관리 리스트(제일 처음에 대메뉴 클릭 시)
	@Override
	public DataModelAndView findReferenceManagerList(SetupSearch search) {
		
		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		
		//List<SystemMaster> SystemMasterList = indvinfoTypeSetupDao.getSystemMasterList();
		// 테이블 선택하여 검색버튼 클릭 할 경우 리스트 개수 체크
		//search.setTotal_count(indvinfoTypeSetupDao.findIndvinfoTypeSetupOne_count(search));
		//List<IndvinfoTypeSetup> indvinfoTypeSetup = indvinfoTypeSetupDao.findIndvinfoTypeSetupList(search);
		
		//기본 전체 참조테이블 리스트 
		List<IndvinfoTypeSetup> referenceManagerList = indvinfoTypeSetupDao.findReferenceManagerList(search);
		//선택한 테이블의 컬럼 정보
		List<IndvinfoTypeSetup> column_nameList = new ArrayList<>(); 
		
		SimpleCode simpleCode = new SimpleCode("/indvinfoTypeSetup/referenceManagerList.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("referenceManagerList", referenceManagerList);
		modelAndView.addObject("column_nameList", column_nameList);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	} 
	
	// 참조테이블 선택한 테이블 컬럼  리스트
	@Override
	public List<IndvinfoTypeSetup> findReferenceColumList(SetupSearch search) {
		
		List<IndvinfoTypeSetup> referenceColumList = indvinfoTypeSetupDao.findReferenceColumList(search);
		
		return referenceColumList;
	} 
	
	// 참조테이블관리 리스트
	@Override
	public DataModelAndView findReferenceManagerDetailList(SetupSearch search) {
		
		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		List<Map<String, String>> referenceManagerDetailList = null;
		
		//primarykey 리스트
		List<String> primayKeyList = indvinfoTypeSetupDao.findTablePriKeyList(search);
		//기본 전체 참조테이블 리스트 
		List<IndvinfoTypeSetup> referenceManagerList = indvinfoTypeSetupDao.findReferenceManagerList(search);
		//검색 조건에 맞는 결과 컬럼 리스트 결과
		List<String> column_nameList = indvinfoTypeSetupDao.findColum_nameList(search);
		
		//컬럼 리스트 셋팅
		if(column_nameList.size() != 0) {
			search.setColumnList(column_nameList);
		}
		//테이블명 존재여부 체크 (선택안함 일 경우 체크)
		String table_name_chk = search.getTable_name();
		// 테이블 선택하여 검색버튼 클릭 할 경우 
		if(table_name_chk != null && !table_name_chk.equals("") ) {
			search.setTotal_count(indvinfoTypeSetupDao.findReferenceManagerDetailList_Count(search));
			//리스트 가져오는 부분
			referenceManagerDetailList = indvinfoTypeSetupDao.findReferenceManagerDetailList(search);
			//pk와 데이터 셋팅
			for( Map<String, String> dataMap : referenceManagerDetailList ) {
				String pKeyDatas="";
				for( int i = 0 ; i < primayKeyList.size() ; i++ ) {
					if( i != 0 ) { pKeyDatas += ",";}
					pKeyDatas+= String.format("%s", dataMap.get(primayKeyList.get(i)) );
				}
				dataMap.put("pKeyDatas", pKeyDatas);
			}
		}

		SimpleCode simpleCode = new SimpleCode("/indvinfoTypeSetup/referenceManagerList.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("search", search);
		modelAndView.addObject("referenceManagerList", referenceManagerList);
		modelAndView.addObject("referenceManagerDetailList", referenceManagerDetailList);
		modelAndView.addObject("column_nameList", column_nameList);
		modelAndView.addObject("primaryKeyList", primayKeyList);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	} 
	
	//참조테이블 리스트 클릭하여 상세보기 
	@Override
	public DataModelAndView findReferenceManagerDetail(SetupSearch search) {
		
		String search_datas = search.getDetail_search_datas();
/*		String item_values = search.getItem_values();
		item_values = item_values.replace("{", "");
		item_values = item_values.replace("}", "");
		item_values = item_values.replace(" ", "");
		String[] listMap = item_values.split(",");
*/
		//검색 조건에 맞는 결과 컬럼 리스트 결과
		List<String> column_nameList = indvinfoTypeSetupDao.findColum_nameList(search);
		if(column_nameList.size() != 0) {
			search.setColumnList(column_nameList);
		}
		
		//프라이머리 키 가져와서 search빈 tablePriKeyList 리스트에 담아준다.
		List<String> tablePriKeyList = indvinfoTypeSetupDao.findTablePriKeyList(search);
		List<String> tmp_where = new ArrayList<>();
		
		// 프라이머리키 리스트에 있는 값과 검색결과에 나온 item_values의 값을 비교하여 같은 값이 있으면 그값을 사용해 where 조건을 만들어 준다
		String[] split = search_datas.split(",");
		int i = 0;
		for(String data : split) {
			String t = String.format("%s='%s'", tablePriKeyList.get(i), data);
			tmp_where.add(t);
			i++;
		}
		
		// 프라이머리키 리스트에 있는 값과 검색결과에 나온 item_values의 값을 비교하여 같은 값이 있으면 그값을 사용해 where 조건을 만들어 준다
//		for (int j = 0; j < listMap.length; j++) {
//			for (int i = 0; i < tablePriKeyList.size(); i++) {
//				String prikey = tablePriKeyList.get(i);
//				String item = listMap[j];
//				
//				if(item.contains(prikey)) {
//					// emp_user_id=P2016364 AND proc_date=20190611 이런식으로 들어가서 = 다음에 ' ' 로 감싸줘야한다.
//					int tmpIndex =item.lastIndexOf("=");
//					int tmplastIndex = item.length();
//					
//					String tmp1 = item.substring(0, tmpIndex);
//					String tmp2 = item.substring(tmpIndex + 1, tmplastIndex);
//					String tmp3 = tmp1 + "='" + tmp2 + "'";
//					
//					//프라이머리 키가 list안에 있을 경우
//					tmp_where.add(tmp3);
//				}
//			}
//		}
		search.setTablePriKeyList(tmp_where);
		
		Map<String, String> referenceManagerDetail = indvinfoTypeSetupDao.findReferenceManagerDetail(search);
		
		String pKeyDatas="";
		for( int j = 0 ; j < tablePriKeyList.size() ; j++ ) {
			if( j != 0 ) { pKeyDatas += ",";}
			pKeyDatas+= String.format("%s", referenceManagerDetail.get(tablePriKeyList.get(j)) );
		}
		referenceManagerDetail.put("pKeyDatas", pKeyDatas);
		
		SimpleCode simpleCode = new SimpleCode("/indvinfoTypeSetup/referenceManagerListDetail.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("referenceManagerDetail", referenceManagerDetail);
		modelAndView.addObject("column_nameList", column_nameList);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	// 참조테이블 컬럼 데이터 수정
	@Override
	public DataModelAndView saveReferenceManagerDetail(Map<String, String> parameters, SetupSearch search) {
		IndvinfoTypeSetup paramBean = CommonHelper.convertMapToBean(parameters, IndvinfoTypeSetup.class);
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		// 참조테이블 데이터 컬럼 업데이트
		try {
			List<String> valueList = new ArrayList<>();
			String value = null;
			int i = 1;
			while((value = parameters.get("input_data_"+i)) != null) {
				valueList.add(value);
				i++;
			}
			search.setValueList(valueList);
			
			//검색 조건에 맞는 결과 컬럼 리스트 결과
			List<String> column_nameList = indvinfoTypeSetupDao.findColum_nameList(search);
			if(column_nameList.size() != 0) {
				search.setColumnList(column_nameList);
			}
			//프라이머리 키 가져와서 search빈 tablePriKeyList 리스트에 담아준다.
			List<String> tablePriKeyList = indvinfoTypeSetupDao.findTablePriKeyList(search);
			
			String search_datas = parameters.get("search_datas");
			List<String> tmp_where = new ArrayList<>();
			String[] split = search_datas.split(",");
			i = 0;
			for(String data : split) {
				String t = String.format("%s='%s'", tablePriKeyList.get(i), data);
				tmp_where.add(t);
				i++;
			}
			
			List<String> tmp_set = new ArrayList<>();
			i = 0;
			for(String data : valueList) {
				String t = String.format("%s='%s'", column_nameList.get(i), data);
				tmp_set.add(t);
				i++;
			}
			
			search.setValueList(tmp_set);
			search.setTablePriKeyList(tmp_where);
			
			int eCount = indvinfoTypeSetupDao.findReferenceManagerDetailUpdate(search);
			//eCount 가 1이면 정상처리, 나머지는 에러 아래 catch문에서 에러 처리 및 로그 저장
			transactionManager.commit(transactionStatus);
			
		} catch(DataAccessException e) {
			logger.error("[IndvinfoTypeSetupSvcImpl.saveReferenceManagerDetail] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	} 
	
	// 참조테이블 컬럼 1 row 삭제
	@Override
	public DataModelAndView removeReferenceManagerDetailOne (Map<String, String> parameters, SetupSearch search) {
		IndvinfoTypeSetup paramBean = CommonHelper.convertMapToBean(parameters, IndvinfoTypeSetup.class);
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			int i = 1;
			//컬럼 리스트
			List<String> column_nameList = indvinfoTypeSetupDao.findColum_nameList(search);
			if(column_nameList.size() != 0) {
				search.setColumnList(column_nameList);
			}
			
			//프라이머리 키 가져와서 search빈 tablePriKeyList 리스트에 담아준다.
			List<String> tablePriKeyList = indvinfoTypeSetupDao.findTablePriKeyList(search);
			
			//primary key로 where 조건 가져오기
			String search_datas = parameters.get("search_datas");
			List<String> tmp_where = new ArrayList<>();
			String[] split = search_datas.split(",");
			i = 0;
			for(String data : split) {
				String t = String.format("%s='%s'", tablePriKeyList.get(i), data);
				tmp_where.add(t);
				i++;
			}
			//where 조건 셋팅
			search.setTablePriKeyList(tmp_where);
			indvinfoTypeSetupDao.removeReferenceManagerDetailOne(search);
			
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[IndvinfoTypeSetupSvcImpl.removeReferenceManagerDetailOne] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}

	@Override
	public DataModelAndView findIndvinfoTypeSetupList_kdic(SetupSearch search) {
		DataModelAndView modelAndView = new DataModelAndView();
		SimpleCode simpleCode = new SimpleCode("/indvinfoTypeSetup/list_kdic.html");
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		int privacyTableListCount = indvinfoTypeSetupDao.findPrivacyTableInfoList_count(search);
		search.setSize(Integer.parseInt(defaultPageSize));
		search.setTotal_count(privacyTableListCount);
		
		List<Map<String,String>> privacyTableList = indvinfoTypeSetupDao.findPrivacyTableInfoList(search);
		modelAndView.addObject("privacyTableList", privacyTableList);
		modelAndView.addObject("search", search);
		return modelAndView;
	}

	@Override
	public DataModelAndView findIndvinfoTypeSetupDetail_kdic(SetupSearch search) {
		DataModelAndView modelAndView = new DataModelAndView();
		SimpleCode simpleCode = new SimpleCode("/indvinfoTypeSetup/detail_kdic.html");
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		List<Map<String,String>> privacyTypeList=indvinfoTypeSetupDao.findRegularExpressionMngList();
		Map <String,String> privacyType = new HashMap<String, String>();
		Map <String,String> privacyTypeAll = new HashMap<String, String>();
		for (Map<String, String> map : privacyTypeList) {
			if(map.get("use_flag").equals("Y")) {
				privacyType.put(map.get("privacy_type"),map.get("privacy_desc"));
				privacyTypeAll.put(map.get("privacy_type"),map.get("privacy_desc"));
			}else {
				privacyTypeAll.put(map.get("privacy_type"),map.get("privacy_desc"));
			}
		}
		
		if(search.getPrivacy_table_info_seq()!=null && !search.getPrivacy_table_info_seq().equals("")) {
			List<Map<String,String>> privacyTableDetailList = indvinfoTypeSetupDao.findPrivacyTableInfoDetail(search);
			Map<String,String> privacyTableDetail = privacyTableDetailList.get(0);
			modelAndView.addObject("privacyTableDetailList", privacyTableDetailList);
			modelAndView.addObject("privacyTableDetail", privacyTableDetail);
		}
		
		modelAndView.addObject("privacyType", privacyType);
		modelAndView.addObject("privacyTypeAll", privacyTypeAll);
		modelAndView.addObject("search", search);
		return modelAndView;
	}

	@Override
	public DataModelAndView saveIndvinfoTypeSetupDetail_kdic(Map<String, String> parameters) {
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		DataModelAndView modalAndView = new DataModelAndView();
		JSONParser parser = new JSONParser();
		try {
			JSONArray jArray  = (JSONArray) parser.parse(parameters.get("jsonData"));
			for (Object object : jArray) {
				JSONObject jobj = (JSONObject) object;
				if(((JSONObject)object).size()>4) {
					if(jobj.get("remove").equals("Y")) {
						indvinfoTypeSetupDao.deleteIndvinfoTypeSetupOneColumn_kdic(jobj);
					}else {
						indvinfoTypeSetupDao.saveIndvinfoTypeSetupOneColumn_kdic(jobj);
					}
				}else if(((JSONObject)object).size()==4) {
					indvinfoTypeSetupDao.addIndvinfoTypeSetupOneColumn_kdic(jobj);
				}else {
					indvinfoTypeSetupDao.saveIndvinfoTypeSetupOneTable_kdic(jobj);
				}
			}
			modalAndView.addObject("result", "success");
			transactionManager.commit(transactionStatus);
		} catch (ParseException e) {
			e.printStackTrace();
			modalAndView.addObject("result", "fail");
			transactionManager.rollback(transactionStatus);
		}
		
		return modalAndView;
	}

	@Override
	public DataModelAndView addIndvinfoTypeSetupDetail_kdic(Map<String, String> parameters) {
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		DataModelAndView modalAndView = new DataModelAndView();
		JSONParser parser = new JSONParser();
		String key = "";
		try {
			JSONArray jArray  = (JSONArray) parser.parse(parameters.get("jsonData"));
			for (Object object : jArray) {
				JSONObject jobj = (JSONObject) object;
				if(((JSONObject)object).size()==2) {
					indvinfoTypeSetupDao.addIndvinfoTypeSetupOneTable_kdic(jobj);
					key = indvinfoTypeSetupDao.findIndvinfoTypeSetupOneTableKey_kdic();
				}else {
					if(jobj.get("remove").equals("N")) {
						jobj.put("privacy_table_info_seq", key);
						indvinfoTypeSetupDao.addIndvinfoTypeSetupOneColumn_kdic(jobj);
					}
				}
			}
			modalAndView.addObject("result", "success");
			transactionManager.commit(transactionStatus);
		} catch (ParseException e) {
			e.printStackTrace();
			modalAndView.addObject("result", "fail");
			transactionManager.rollback(transactionStatus);
		}
		
		return modalAndView;
	}

	@Override
	public DataModelAndView deleteIndvinfoTypeSetupDetail_kdic(Map<String, String> parameters) {
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		DataModelAndView modalAndView = new DataModelAndView();
		try {
			indvinfoTypeSetupDao.deleteIndvinfoTypeSetupOneTable_kdic(parameters);
			modalAndView.addObject("result", "success");
			transactionManager.commit(transactionStatus);
		} catch (Exception e) {
			e.printStackTrace();
			modalAndView.addObject("result", "fail");
			transactionManager.rollback(transactionStatus);
		}
		
		return modalAndView;
	}
}

