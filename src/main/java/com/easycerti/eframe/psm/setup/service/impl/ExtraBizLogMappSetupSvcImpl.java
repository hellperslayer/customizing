package com.easycerti.eframe.psm.setup.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.setup.dao.ExtraBizLogMappSetupDao;
import com.easycerti.eframe.psm.setup.service.ExtraBizLogMappSetupSvc;
import com.easycerti.eframe.psm.setup.vo.ExtraBizLogMappSetup;
import com.easycerti.eframe.psm.setup.vo.ExtraBizLogMappSetupList;
import com.easycerti.eframe.psm.setup.vo.MenuMappSetup;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;
import com.easycerti.eframe.psm.system_management.dao.AgentMngtDao;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

@Service
public class ExtraBizLogMappSetupSvcImpl implements ExtraBizLogMappSetupSvc {
	
	@Autowired
	ExtraBizLogMappSetupDao extraBizLogMappSetupDao;
	
	@Autowired
	AgentMngtDao agentMngtDao;
	
	@Autowired
	CommonDao commonDao;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;
	
	@Override
	public DataModelAndView findExtraBizLogMappSetupList(SetupSearch search) {

		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		search.setTotal_count(extraBizLogMappSetupDao.findExtraBizLogMappSetupOne_count(search));
		List<ExtraBizLogMappSetup> extraBizLogMappSetup = extraBizLogMappSetupDao.findExtraBizLogMappSetupList(search);
		
		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList();
		List<Code> extraBizLogTypeList = agentMngtDao.findExtraBizLogType();
		
		ExtraBizLogMappSetupList extraBizLogMappSetupList = new ExtraBizLogMappSetupList(extraBizLogMappSetup);
		
		SimpleCode simpleCode = new SimpleCode("/extraBizLogMappSetup/list.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("extraBizLogMappSetupList", extraBizLogMappSetupList);
		modelAndView.addObject("systemMasterList", systemMasterList);
		modelAndView.addObject("extraBizLogTypeList", extraBizLogTypeList);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView findExtraBizLogMappSetupDetail(SetupSearch search) {

		ExtraBizLogMappSetup extraBizLogMappSetup = null;
		if(search.getSeq() != ""){
			extraBizLogMappSetup = extraBizLogMappSetupDao.findExtraBizLogMappSetupDetail(search);
		}
		
		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList();
		List<Code> extraBizLogTypeList = agentMngtDao.findExtraBizLogType();
		SimpleCode simpleCode = new SimpleCode("/extraBizLogMappSetup/detail.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("extraBizLogMappSetup", extraBizLogMappSetup);
		modelAndView.addObject("systemMasterList", systemMasterList);
		modelAndView.addObject("extraBizLogTypeList",extraBizLogTypeList);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView addExtraBizLogMappSetupOne(Map<String, String> parameters) {
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		
		ExtraBizLogMappSetup paramBean = CommonHelper.convertMapToBean(parameters, ExtraBizLogMappSetup.class);
		
		try {
			extraBizLogMappSetupDao.addExtraBizLogMappSetupOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[ExtraBizLogMappSetupSvcImpl.addExtraBizLogMappSetupOne] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS070J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView saveExtraBizLogMappSetupOne(Map<String, String> parameters) {

		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		
		ExtraBizLogMappSetup paramBean = CommonHelper.convertMapToBean(parameters, ExtraBizLogMappSetup.class);
		
		try {
			extraBizLogMappSetupDao.saveExtraBizLogMappSetupOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[ExtraBizLogMappSetupSvcImpl.saveExtraBizLogMappSetupOne] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS071J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView removeExtraBizLogMappSetupOne(Map<String, String> parameters) {
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		
		ExtraBizLogMappSetup paramBean = CommonHelper.convertMapToBean(parameters, ExtraBizLogMappSetup.class);
		
		try {
			extraBizLogMappSetupDao.removeExtraBizLogMappSetupOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[ExtraBizLogMappSetupSvcImpl.removeExtraBizLogMappSetupOne] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS072J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
}
