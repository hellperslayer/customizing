package com.easycerti.eframe.psm.setup.service.impl;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.ExcelFileReader;
import com.easycerti.eframe.common.util.XExcelFileReader;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.setup.dao.SqlMappSetupDao;
import com.easycerti.eframe.psm.setup.service.SqlMappSetupSvc;
import com.easycerti.eframe.psm.setup.vo.MenuMappSetup;
import com.easycerti.eframe.psm.setup.vo.MenuMappSetupList;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;
import com.easycerti.eframe.psm.setup.vo.SqlMappSetup;
import com.easycerti.eframe.psm.setup.vo.SqlMappSetupList;
import com.easycerti.eframe.psm.system_management.dao.AgentMngtDao;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

@Service
public class SqlMappSetupSvcImpl implements SqlMappSetupSvc {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private SqlMappSetupDao sqlMappSetupDao;
	
	@Autowired
	private AgentMngtDao agentMngtDao;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	@Autowired
	private CommonDao commonDao;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;
	
	@Override
	public DataModelAndView findSqlMappSetupList(SetupSearch search, HttpServletRequest request) {

		List<String> auth_idsList = (List<String>) request.getSession().getAttribute("auth_ids_list");
		search.setAuth_idsList(auth_idsList);
		
		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		search.setTotal_count(sqlMappSetupDao.findSqlMappSetupOne_count(search));
		List<SqlMappSetup> sqlMappSetup = sqlMappSetupDao.findSqlMappSetupList(search);
		
		SearchSearch search2 = new SearchSearch();
		search2.setAuth_idsList(auth_idsList);
		
		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList_byAdmin(search2);
		SqlMappSetupList sqlMappSetupList = new SqlMappSetupList(sqlMappSetup);
		
		SimpleCode simpleCode = new SimpleCode("/sqlMappSetup/list.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("sqlMappSetupList", sqlMappSetupList);
		modelAndView.addObject("systemMasterList", systemMasterList);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView findSqlMappSetupDetail(SetupSearch search) {
		
		SqlMappSetup sqlMappSetupDetail = null;
		List<SqlMappSetup> sqlMappSetupDetailSql = null;
		if(search.getTag_sql_url_seq() != 0){
			sqlMappSetupDetail = sqlMappSetupDao.findSqlMappSetupDetail(search);
			sqlMappSetupDetailSql = sqlMappSetupDao.findSqlMappSetupDetailSql(search);
		}
		
		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList();
		
		SimpleCode simpleCode = new SimpleCode("/sqlMappSetup/detail.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("sqlMappSetupDetail", sqlMappSetupDetail);
		modelAndView.addObject("sqlMappSetupDetailSql", sqlMappSetupDetailSql);
		modelAndView.addObject("systemMasterList", systemMasterList);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView removeSqlMappSetupOne(Map<String, String> parameters) {
		
		SqlMappSetup paramBean = CommonHelper.convertMapToBean(parameters, SqlMappSetup.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			sqlMappSetupDao.removeSqlMappSetupOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[SqlMappSetupSvcImpl.removesqlMappSetupOne] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS003J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}

	@Override
	public void findSqlMappSetupList_download(DataModelAndView modelAndView, SetupSearch search,
			HttpServletRequest request) {
		
		List<String> auth_idsList = (List<String>) request.getSession().getAttribute("auth_ids_list");
		search.setAuth_idsList(auth_idsList);
		
		search.setUseExcel("true");
		
		String fileName = "PSM_SQL매핑설정_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "SQL매핑설정";
		
//		List<SqlMappSetup> sqlMappSetupList = sqlMappSetupDao.findSqlMappSetupList(search);
		List<SqlMappSetup> sqlMappSetupList = sqlMappSetupDao.findSqlMappSetupDownload(search);
		
//		for(SqlMappSetup sqlMappSetup : sqlMappSetupList) {
//			String use_yn = sqlMappSetup.getUse_yn();
//			sqlMappSetup.setUse_yn(use_yn.equals("Y") ? "사용" : "미사용");
//		}
		
		String[] columns = new String[] {"system_name", "url", "crud_type", "sql"};
		String[] heads = new String[] {"시스템", "SQL 보유  URL", "행위 구분", "SQL리스트"};
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", sqlMappSetupList);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
		
	}

	@Override
	public String upLoadSystemMngtOne(Map<String, String> parameters, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		String result="false";
		SqlMappSetup paramBean = CommonHelper.convertMapToBean(parameters, SqlMappSetup.class);
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		//엑셀 Row 카운트는 1000으로 지정한다		
		int rowCount=5000;
		try {
			//파일 업로드 기능  		
			MultipartHttpServletRequest mpr = (MultipartHttpServletRequest) request;  
			
			MultipartFile dir = mpr.getFile("file");
					
			File file = new File("/root/excelUpload/" + dir.getOriginalFilename());
			
			if (!file.exists()) { //폴더 없으면 폴더 생성
				file.mkdirs();
			}
			
			dir.transferTo(file);
			
			//업로드된 파일의 정보를 읽어들여 DB에 추가하는 기능 
			if (dir.getSize() > 0 ) { 
					
				List<String[]> readRows = new ArrayList<>();
				
				String ext = file.getName().substring(file.getName().lastIndexOf(".")+1);
				ext = ext.toLowerCase();
				if( "xlsx".equals(ext) ){	
					readRows = new XExcelFileReader(file.getAbsolutePath()).readRows(rowCount);
					
				}else if( "xls".equals(ext) ){	
					readRows = new ExcelFileReader(file.getAbsolutePath()).readRows(rowCount);
				}
			
				for(int i=0; i<3; i++){	
					readRows.remove(0);
				}
				
				boolean isCheck = true;
				if( readRows.size() != 0 ){
					
				/*int columnCount = readRows.get(0).length;*/
				//각 셀 내용을 DB 컬럼과 Mapping
				try {
					for(String[] readRowArr : readRows){
						if(readRowArr.length != 0 && readRowArr[1].length() != 0){
							paramBean.setUrl(readRowArr[1]);
							paramBean.setCrud_type(readRowArr[2]);
							paramBean.setSql(readRowArr[3]);
							paramBean.setSystem_seq(readRowArr[0]);
							
							int lastSeq = sqlMappSetupDao.findSqlMappDuplicateCnt(paramBean);
							if(lastSeq == 0) {
								paramBean.setTag_sql_url_seq(sqlMappSetupDao.uploadSqlMappSetupOne(paramBean));
							} else {
								paramBean.setTag_sql_url_seq(lastSeq);
							}
							
							sqlMappSetupDao.uploadSqlMappSetupDetailOne(paramBean);
						}
					}
					if(isCheck) {
						readRows.clear();
						transactionManager.commit(transactionStatus);
					}
				} catch(DataAccessException e) {
					transactionManager.rollback(transactionStatus);
					readRows.clear();
					e.printStackTrace();
				/*	logger.error("[EmpUserMngtSvcImpl.upLoadEmpUserMngtOne] MESSAGE : " + e.getMessage());
					throw new ESException("SYS039J");*/
					return result;
				}
			}
				if(isCheck)
					result="true";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return result;	
	}

}
