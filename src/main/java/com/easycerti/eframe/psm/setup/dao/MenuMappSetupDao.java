package com.easycerti.eframe.psm.setup.dao;

import java.util.List;

import com.easycerti.eframe.psm.setup.vo.MenuMappSetup;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;
/**
 * 
 * 설명 : 매뉴매핑설정 Dao 
 * @author tjlee
 * @since 2015. 5. 4.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 4.           tjlee            최초 생성
 *
 * </pre>
 */
public interface MenuMappSetupDao {
	
	public List<MenuMappSetup> findMenuMappSetupList(SetupSearch search);

	public int findMenuMappSetupOne_count(SetupSearch search);

	public void addMenuMappSetupOne(MenuMappSetup paramBean);

	public void saveMenuMappSetupOne(MenuMappSetup paramBean);

	public void removeMenuMappSetupOne(MenuMappSetup paramBean);

	public MenuMappSetup findMenuMappSetupDetail(MenuMappSetup paramBean);
	
	public MenuMappSetup findMenuMappSetupDetailMenu(SetupSearch search);
	
	public void uploadMenuMappSetupOne(MenuMappSetup paramBean);

	public int findMenuPrivFindSetupList_count(SetupSearch search);
	public List<MenuMappSetup> findMenuPrivFindSetupList(SetupSearch search);

	public void addPrivFind(MenuMappSetup paramBean);
	public void savePrivFind(MenuMappSetup paramBean);

	public void removePrivFind(MenuMappSetup paramBean);
}
