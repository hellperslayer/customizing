package com.easycerti.eframe.psm.setup.dao;

import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;

import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.setup.vo.IndvinfoTypeSetup;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;
/**
 * 
 * 설명 : 개인정보유형설정 Dao
 * @author tjlee
 * @since 2015. 5. 4.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 4.           tjlee            최초 생성
 *
 * </pre>
 */
public interface IndvinfoTypeSetupDao {
	
	public int findIndvinfoTypeSetupOne_count(SetupSearch search);
	
	public List<IndvinfoTypeSetup> findIndvinfoTypeSetupList(SetupSearch search);
	
	public IndvinfoTypeSetup findIndvinfoTypeSetupDetail(SetupSearch search);
	
	public Map<String, String> findReferenceManagerDetail(SetupSearch search);
	
	public IndvinfoTypeSetup findIndvinfoTypeSetupOne(	IndvinfoTypeSetup paramBean);
	
	public void addIndvinfoTypeSetupOne(IndvinfoTypeSetup paramBean);
	public void addIndvinfoTypeSetupOneUse(IndvinfoTypeSetup paramBean);
	
	public void saveIndvinfoTypeSetupOne(IndvinfoTypeSetup paramBean);
	
	public void removeIndvinfoTypeSetupOne(IndvinfoTypeSetup paramBean);

	public List<SystemMaster> getSystemMasterList();
	
	public List<SystemMaster> getSystemMasterListByAuth(SearchSearch search);
	public List<SystemMaster> getSystemMasterListByAuth2(SetupSearch search);

	public void saveIndvinfoTypeSetupOneUse(IndvinfoTypeSetup paramBean);
	public void saveIndvinfoTypeSetupOneUseMasking(IndvinfoTypeSetup paramBean);
	public void saveIndvinfoTypeSetupOneUseBizLogResult(IndvinfoTypeSetup paramBean);
	
	public List<IndvinfoTypeSetup> findReferenceManagerList(SetupSearch search);
	public List<IndvinfoTypeSetup> findReferenceColumList(SetupSearch search);
	public List<String> findColum_nameList(SetupSearch search);
	public List<Map<String, String>> findReferenceManagerDetailList(SetupSearch search);
	public List<String> findTablePriKeyList(SetupSearch search);
	public int findReferenceManagerDetailList_Count(SetupSearch search);

	public int findReferenceManagerDetailUpdate(SetupSearch search);
	public void removeReferenceManagerDetailOne(SetupSearch search);
	
	public int indvinfoTypeCnt();
	public int indvinfoResultTypeOrderCheck(IndvinfoTypeSetup paramBean);

	public void addIndvinfoTypeSetupOneUse_conflict(IndvinfoTypeSetup paramBean);

	public List<Map<String, String>> findPrivacyTableInfoList(SetupSearch search);

	public int findPrivacyTableInfoList_count(SetupSearch search);

	public List<Map<String, String>> findPrivacyTableInfoDetail(SetupSearch search);

	public void saveIndvinfoTypeSetupOneColumn_kdic(JSONObject jobj);

	public void saveIndvinfoTypeSetupOneTable_kdic(JSONObject jobj);

	public void deleteIndvinfoTypeSetupOneColumn_kdic(JSONObject jobj);

	public void addIndvinfoTypeSetupOneColumn_kdic(JSONObject jobj);

	public void addIndvinfoTypeSetupOneTable_kdic(JSONObject jobj);

	public String findIndvinfoTypeSetupOneTableKey_kdic();

	public List<Map<String, String>> findRegularExpressionMngList();

	public void deleteIndvinfoTypeSetupOneTable_kdic(Map<String,String> map);
}
