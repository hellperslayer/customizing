package com.easycerti.eframe.psm.setup.vo;

import com.easycerti.eframe.common.util.PageInfo;
import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 
 * 설명 : 추출환경설정 VO
 * @author tjlee
 * @since 2015. 4. 27.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 27.           tjlee            최초 생성
 *
 * </pre>
 */
public class ExtrtBaseSetup extends AbstractValueObject{

	private int rule_seq	; // 룰번호
	private String rule_nm; // 룰이름
	private String rule_desc; // 추출조건설명
	private String log_delimiter; // 로그구분
	private int dng_val; // 위험지수
	private String script; //추출조건쿼리
	private int limit_cnt; // 임계치
	private String is_realtime_extract; // 실시간 추출 여부
	private String realtime_extract_time; // 실시간 추출시간
	private String use_yn; // 사용여부
	private String user_limit_cnt_yn; // 개인별 임계치 사용여부 
	private String emp_user_id;
	private String emp_user_name;
	
	private PageInfo pageInfo;
	
	private int avg_val;
	private float prct_val;
	private int prct_int;
	private int limit_val;
	private String indv_yn;
	private float user_dng_prct;
	private String system_name;
	private String system_seq;
	// 시나리오 관련
	private int scen_seq;
	private String scen_name;
	private String alarm_yn;
	
	private int limit_type;
	private String limit_type_desc;
	private int limit_type_cnt;
	
	private String dept_id;
	private String dept_name;
	private int threshold;
	
	private int threshold_dept;
	
	private String privacy_seq;
	private String privacy_desc;
	private String ref_val;
	
	private String script_desc;
	// 테이블 접근형태
	private String rule_result_type;
	// 원시로그접근기준
	private String rule_view_type;
	// 개인정보유형 저장 유무
	private String result_type_yn;
	// 시간 정보 저장 유무
	private String time_view_yn;
	// IP 정보 저장 유무
	private String ip_yn;
	
	public String getResult_type_yn() {
		return result_type_yn;
	}
	public void setResult_type_yn(String result_type_yn) {
		this.result_type_yn = result_type_yn;
	}
	public String getTime_view_yn() {
		return time_view_yn;
	}
	public void setTime_view_yn(String time_view_yn) {
		this.time_view_yn = time_view_yn;
	}
	public String getIp_yn() {
		return ip_yn;
	}
	public void setIp_yn(String ip_yn) {
		this.ip_yn = ip_yn;
	}
	public String getEmp_user_name() {
		return emp_user_name;
	}
	public void setEmp_user_name(String emp_user_name) {
		this.emp_user_name = emp_user_name;
	}
	public String getDept_name() {
		return dept_name;
	}
	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}
	public String getRule_result_type() {
		return rule_result_type;
	}
	public void setRule_result_type(String rule_result_type) {
		this.rule_result_type = rule_result_type;
	}
	public String getRule_view_type() {
		return rule_view_type;
	}
	public void setRule_view_type(String rule_view_type) {
		this.rule_view_type = rule_view_type;
	}
	public String getScript_desc() {
		return script_desc;
	}
	public void setScript_desc(String script_desc) {
		this.script_desc = script_desc;
	}
	public String getLimit_type_desc() {
		return limit_type_desc;
	}
	public void setLimit_type_desc(String limit_type_desc) {
		this.limit_type_desc = limit_type_desc;
	}
	public String getPrivacy_desc() {
		return privacy_desc;
	}
	public void setPrivacy_desc(String privacy_desc) {
		this.privacy_desc = privacy_desc;
	}
	public String getPrivacy_seq() {
		return privacy_seq;
	}
	public void setPrivacy_seq(String privacy_seq) {
		this.privacy_seq = privacy_seq;
	}
	public String getRef_val() {
		return ref_val;
	}
	public void setRef_val(String ref_val) {
		this.ref_val = ref_val;
	}
	public int getThreshold_dept() {
		return threshold_dept;
	}
	public void setThreshold_dept(int threshold_dept) {
		this.threshold_dept = threshold_dept;
	}
	public String getSystem_seq() {
		return system_seq;
	}
	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}
	public int getThreshold() {
		return threshold;
	}
	public void setThreshold(int threshold) {
		this.threshold = threshold;
	}
	public String getDept_id() {
		return dept_id;
	}
	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}
	public int getLimit_type() {
		return limit_type;
	}
	public void setLimit_type(int limit_type) {
		this.limit_type = limit_type;
	}
	public int getLimit_type_cnt() {
		return limit_type_cnt;
	}
	public void setLimit_type_cnt(int limit_type_cnt) {
		this.limit_type_cnt = limit_type_cnt;
	}
	public String getSystem_name() {
		return system_name;
	}
	public void setSystem_name(String system_name) {
		this.system_name = system_name;
	}
	public int getScen_seq() {
		return scen_seq;
	}
	public void setScen_seq(int scen_seq) {
		this.scen_seq = scen_seq;
	}
	public String getScen_name() {
		return scen_name;
	}
	public void setScen_name(String scen_name) {
		this.scen_name = scen_name;
	}
	public PageInfo getPageInfo() {
		return pageInfo;
	}
	public void setPageInfo(PageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}
	public int getRule_seq() {
		return rule_seq;
	}
	public void setRule_seq(int rule_seq) {
		this.rule_seq = rule_seq;
	}
	public String getRule_nm() {
		return rule_nm;
	}
	public void setRule_nm(String rule_nm) {
		this.rule_nm = rule_nm;
	}
	public String getRule_desc() {
		return rule_desc;
	}
	public void setRule_desc(String rule_desc) {
		this.rule_desc = rule_desc;
	}
	public String getLog_delimiter() {
		return log_delimiter;
	}
	public void setLog_delimiter(String log_delimiter) {
		this.log_delimiter = log_delimiter;
	}
	public int getDng_val() {
		return dng_val;
	}
	public void setDng_val(int dng_val) {
		this.dng_val = dng_val;
	}
	public String getScript() {
		return script;
	}
	public void setScript(String script) {
		this.script = script;
	}
	public int getLimit_cnt() {
		return limit_cnt;
	}
	public void setLimit_cnt(int limit_cnt) {
		this.limit_cnt = limit_cnt;
	}
	public String getIs_realtime_extract() {
		return is_realtime_extract;
	}
	public void setIs_realtime_extract(String is_realtime_extract) {
		this.is_realtime_extract = is_realtime_extract;
	}
	public String getRealtime_extract_time() {
		return realtime_extract_time;
	}
	public void setRealtime_extract_time(String realtime_extract_time) {
		this.realtime_extract_time = realtime_extract_time;
	}
	public String getUse_yn() {
		return use_yn;
	}
	public void setUse_yn(String use_yn) {
		this.use_yn = use_yn;
	}
	public String getUser_limit_cnt_yn() {
		return user_limit_cnt_yn;
	}
	public void setUser_limit_cnt_yn(String user_limit_cnt_yn) {
		this.user_limit_cnt_yn = user_limit_cnt_yn;
	}
	public int getAvg_val() {
		return avg_val;
	}
	public void setAvg_val(int avg_val) {
		this.avg_val = avg_val;
	}
	public float getPrct_val() {
		return prct_val;
	}
	public void setPrct_val(float prct_val) {
		this.prct_val = prct_val;
	}
	
	public float getUser_dng_prct() {
		return user_dng_prct;
	}
	public void setUser_dng_prct(float user_dng_prct) {
		this.user_dng_prct = user_dng_prct;
	}
	public int getPrct_int() {
		return prct_int;
	}
	public void setPrct_int(int prct_int) {
		this.prct_int = prct_int;
	}
	public String getEmp_user_id() {
		return emp_user_id;
	}
	public void setEmp_user_id(String emp_user_id) {
		this.emp_user_id = emp_user_id;
	}
	
	public String getIndv_yn() {
		return indv_yn;
	}
	public void setIndv_yn(String indv_yn) {
		this.indv_yn = indv_yn;
	}
	public int getLimit_val() {
		return limit_val;
	}
	public void setLimit_val(int limit_val) {
		this.limit_val = limit_val;
	}
	public String getAlarm_yn() {
		return alarm_yn;
	}
	public void setAlarm_yn(String alarm_yn) {
		this.alarm_yn = alarm_yn;
	}
	@Override
	public String toString() {
		return "ExtrtBaseSetup [rule_seq=" + rule_seq + ", rule_nm=" + rule_nm + ", rule_desc=" + rule_desc + ", log_delimiter=" + log_delimiter + ", dng_val=" + dng_val + ", script=" + script
				+ ", limit_cnt=" + limit_cnt + ", is_realtime_extract=" + is_realtime_extract + ", realtime_extract_time=" + realtime_extract_time + ", use_yn=" + use_yn + ", user_limit_cnt_yn="
				+ user_limit_cnt_yn + ", scen_seq=" + scen_seq +"]";
	}
	
	
}
