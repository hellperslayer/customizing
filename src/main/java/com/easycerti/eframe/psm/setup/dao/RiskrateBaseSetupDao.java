package com.easycerti.eframe.psm.setup.dao;

import java.util.List;

import com.easycerti.eframe.psm.setup.vo.RiskrateBaseSetup;

public interface RiskrateBaseSetupDao {
	
	/**
	 * 위험도 목록
	 * @author yrchoo
	 * @since 2015. 4. 29.
	 * @return List<RiskrateBaseSetup>
	 */
	public List<RiskrateBaseSetup> findRiskrateBaseSetupList(RiskrateBaseSetup riskrateBaseSetup); 
	
	/**
	 * 위험도 수정
	 * @author yrchoo
	 * @since 2015. 4. 29.
	 * @return void
	 */
	public void saveRiskrateBaseSetupList(RiskrateBaseSetup riskrateBaseSetup); 

}
