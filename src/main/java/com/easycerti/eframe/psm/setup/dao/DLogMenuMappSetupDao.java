package com.easycerti.eframe.psm.setup.dao;

import java.util.List;

import com.easycerti.eframe.psm.setup.vo.DLogMenuMappSetup;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;
import com.easycerti.eframe.psm.system_management.vo.System;
/**
 * 
 * 설명 : 매뉴매핑설정 Dao 
 * @author tjlee
 * @since 2015. 5. 4.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 4.           tjlee            최초 생성
 *
 * </pre>
 */
public interface DLogMenuMappSetupDao {
	
	public List<DLogMenuMappSetup> findDLogMenuMappSetupList(SetupSearch search);

	public int findDLogMenuMappSetupOne_count(SetupSearch search);

	public void addDLogMenuMappSetupOne(DLogMenuMappSetup paramBean);

	public void saveDLogMenuMappSetupOne(DLogMenuMappSetup paramBean);

	public void removeDLogMenuMappSetupOne(DLogMenuMappSetup paramBean);

	public DLogMenuMappSetup findDLogMenuMappSetupDetail(DLogMenuMappSetup paramBean);
	
	public DLogMenuMappSetup findDLogMenuMappSetupDetailMenu(SetupSearch search);
	
	public void uploadDLogMenuMappSetupOne(DLogMenuMappSetup paramBean);

	public void saveThresholdtoSystem(System paramBean);

	public void uploadPatch(DLogMenuMappSetup paramBean);

}
