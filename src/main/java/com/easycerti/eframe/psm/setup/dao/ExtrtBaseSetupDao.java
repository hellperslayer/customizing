package com.easycerti.eframe.psm.setup.dao;

import java.util.List;

import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInq;
import com.easycerti.eframe.psm.setup.vo.ExtrtBaseSetup;
import com.easycerti.eframe.psm.setup.vo.SetupSearch;
import com.easycerti.eframe.psm.system_management.vo.EmpUser;
import com.easycerti.eframe.psm.system_management.vo.EmpUserSearch;
/**
 * 
 * 설명 : 추출환경설정 Dao
 * @author tjlee
 * @since 2015. 5. 4.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 4.           tjlee            최초 생성
 *
 * </pre>
 */
public interface ExtrtBaseSetupDao {
	
	public List<ExtrtBaseSetup> findScenarioList(SetupSearch search);
	public ExtrtBaseSetup findScenarioDetail(ExtrtBaseSetup extrtBaseSetup);
	public int scenarioDngValSum(ExtrtBaseSetup extrtBaseSetup);
	public int ruletblDngValSum(ExtrtBaseSetup extrtBaseSetup);
	
	public List<ExtrtBaseSetup> findExtrtBaseSetupList(SetupSearch search);

	public int findExtrtBaseSetupOne_count(SetupSearch search);

	public void addExtrtBaseSetupOne(ExtrtBaseSetup paramBean);

	public void saveExtrtBaseSetupOne(ExtrtBaseSetup paramBean);
	public void removeThresholdDeptByRule(ExtrtBaseSetup paramBean);
	public void removeThresholdIndvByRule(ExtrtBaseSetup paramBean);

	public void removeExtrtBaseSetupOne(ExtrtBaseSetup paramBean);

	public ExtrtBaseSetup findExtrtBaseSetupDetail(ExtrtBaseSetup paramBean);
	public ExtrtBaseSetup findExtrtBaseSetupDetailOne(SetupSearch search);
	
	public int selectMaxRuleSeq();
	public int checkRuleSeq(int rule_seq);
	//public int selectMaxRuleSeq(SetupSearch search);
	
	public List<ExtrtBaseSetup> findExtrtBaseSetupDetail_indv(EmpUser search);
	public void setExtrtBaseSetupDetail_indv(ExtrtBaseSetup paramBean);
	public void setExtrtBaseSetupAll_indv(ExtrtBaseSetup paramBean);
	public void setExtrtBaseSetupDetail(ExtrtBaseSetup paramBean);
	public List<ExtrtBaseSetup> findRuleInfo();
	
	public ExtrtBaseSetup findEmpRuleInfo(ExtrtCondbyInq paramBean);
	
	public String systemSeqArr();
	public int scenSeqCheck(int scen_seq);
	public void addScenario(ExtrtBaseSetup paramBean);
	public void updateScenario(ExtrtBaseSetup paramBean);
	
	public int findRuleTableByScenario(ExtrtBaseSetup paramBean);
	public void removeScenario(ExtrtBaseSetup paramBean);
	
	public void insertThreshold_indv(EmpUser paramBean);
	public void updateThreshold_indv(EmpUser paramBean);
	public int checkThreshold_indv(EmpUser paramBean);
	
	public void insertThresholdDept(ExtrtBaseSetup extrtBaseSetup);
	public void insertThresholdIndv(ExtrtBaseSetup extrtBaseSetup);
	public void removeThreshold_indv(ExtrtBaseSetup baseSetup);
	public void removeThreshold_dept(ExtrtBaseSetup baseSetup);
	
	public List<String> empUserList(String dept_id);
	
	public int checkThreshold_dept(EmpUser paramBean);
	
	public Integer thresholdDeptListCt(SetupSearch search);
	public List<ExtrtBaseSetup> thresholdDeptList(SetupSearch search);
	
	public Integer thresholdIndvListCt(SetupSearch search);
	public List<ExtrtBaseSetup> thresholdIndvList(SetupSearch search);
	
	public List<EmpUser> findDeptList();
	public List<EmpUser> findUserList();
	
	public List<ExtrtBaseSetup> thresholdSettingDeptList(ExtrtBaseSetup extrtBaseSetup);
	public List<ExtrtBaseSetup> thresholdSettingIndvList(ExtrtBaseSetup extrtBaseSetup);
	public List<ExtrtBaseSetup> thresholdSettingIndvRuleList();
	
	public List<ExtrtBaseSetup> findDeptRuleList(ExtrtBaseSetup extrtBaseSetup);
	public List<ExtrtBaseSetup> findIndvRuleList(ExtrtBaseSetup extrtBaseSetup);
}
