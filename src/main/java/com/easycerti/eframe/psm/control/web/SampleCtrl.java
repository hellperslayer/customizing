package com.easycerti.eframe.psm.control.web;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.service.CommonService;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.control.service.CacheService;
import com.easycerti.eframe.psm.history_management.service.ManagerActHistSvc;
import com.easycerti.eframe.psm.system_management.dao.MenuMngtDao;

/**
 * 권한 관련 Controller
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 * 
 *
 * </pre>
 */
@Controller
@RequestMapping(value = "")
public class SampleCtrl {

	// private static final Logger logger = LoggerFactory
	// .getLogger(SampleCtrl.class);

	@Autowired
	private ManagerActHistSvc managerActHistSvc;

	@Autowired
	private CommonService commonService;

	@Autowired
	private CacheService cacheSvc;
	
	@Autowired
	private MenuMngtDao menuMngtDao;

	// @Resource(name="remoteControlUri")
	// private String controlUri;

	// @Resource(name="remoteControlPort")
	// private Integer controlPort;

	/********** 공통 코드 사용 방법 **************/
	@Resource(name = "cacheDictionary")
	private Map<String, Map<String, String>> cacheDictionary;
	private Map<String, String> catMap;

	private void testCache() {
		if (catMap == null)
			catMap = cacheDictionary.get("CAT");

		Set<String> keySet = catMap.keySet();
		for (String key : keySet) {
			System.out.println(key + "=" + catMap.get(key));
		}
	}

	/*********************************************/

	/**
	 * 권한 리스트 화면
	 */
	@RequestMapping(value = "/control/samples.html", method = { RequestMethod.POST })
	public DataModelAndView samples(
			@RequestParam Map<String, String> parameters,
			HttpServletRequest request) {
		parameters = CommonHelper.checkSearchDate(parameters);
		// DataModelAndView modelAndView =
		// logAuthAdminUserService.findListLogAuthAdminUser(parameters);
		DataModelAndView modelAndView = new DataModelAndView();

		// testCache();

		modelAndView.addObject("paramBean", parameters);
		//modelAndView.setViewName("samples");
		modelAndView.setViewName("samples");
		
		return modelAndView;
	}

//	@RequestMapping(value = "/download/pdf.html")
//	public ModelAndView samplesReportPdf(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
//		Menu paramBean = CommonHelper.convertMapToBean(parameters, Menu.class);
//		
//		List<Menu> menuList = menuMngtDao.findMenuMngtList(paramBean);
//		
//		//menuList.add(menu);
//		JRDataSource ds = new JRBeanCollectionDataSource(menuList);
//		
//		Map<String, Object> parameterMap = new HashMap<String, Object>();
//		parameterMap.put("datasource", ds);
//
//		ModelAndView modelAndView = new ModelAndView("pdfReport", parameterMap);
//		
//		return modelAndView;
//	}

//	@RequestMapping(value = "/download/html.html")
//	public ModelAndView sampleReportHtml(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
//
//		Menu paramBean = CommonHelper.convertMapToBean(parameters, Menu.class);
///*		List<Menu> menuList = new ArrayList<Menu>();
//		Menu menu = new Menu();
//		menu.setMenu_id("menu_id");
//		menu.setMenu_name("menu_메뉴");
//		menu.setParent_menu_id("parent_menu_id");
//		menu.setMenu_url("menu_id");
//		menu.setMenu_depth(1);
//		menu.setDescription("menu_id");
//		menu.setUse_flag(new Character((char) 0));
//		menu.setSort_order(1);
//		menu.setInsert_user_id("menu_id");
//		menu.setInsert_datetime(new Date());
//		menu.setUpdate_user_id("menu_id");
//		menu.setUpdate_datetime(new Date());*/
//		
//		List<Menu> menuList = menuMngtDao.findMenuMngtList(paramBean);
//		
//		//menuList.add(menu);
//		JRDataSource ds = new JRBeanCollectionDataSource(menuList);
//		
//		Map<String, Object> parameterMap = new HashMap<String, Object>();
//		parameterMap.put("datasource", ds);
//
//		ModelAndView modelAndView = new ModelAndView("htmlReport", parameterMap);
//		
//		return modelAndView;
//	}
	
	@ModelAttribute("departs")
	public List<SimpleCode> getDepartments() {
		return commonService.getAvailableDepartments();
	}
}
