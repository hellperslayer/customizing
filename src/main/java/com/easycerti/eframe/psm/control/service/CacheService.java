package com.easycerti.eframe.psm.control.service;

import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.control.vo.GroupCode;

public interface CacheService {
	
	DataModelAndView getEntireGroups(Map<String, String> parameters);
	List<GroupCode> getAvailableGroups();
	Map<String, Map<String, String>> getCacheDictionary();
	void refreshAll();
	void refreshGroup();
	void refreshGroupCodes();
	void flushToServletContext(ServletContext servletContext);
}
