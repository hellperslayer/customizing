package com.easycerti.eframe.psm.control.vo;

import java.util.Date;

import com.easycerti.eframe.core.type.YesNo;

public class ScheduleLog {

	// fields
	private int id;
	private String beanId;
	private String descr;
	private Date startDate;
	private Date endDate;
	private YesNo success;
	
	// getter and setter
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBeanId() {
		return beanId;
	}
	public void setBeanId(String beanId) {
		this.beanId = beanId;
	}
	public String getDescr() {
		return descr;
	}
	public void setDescr(String descr) {
		this.descr = descr;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public YesNo getSuccess() {
		return success;
	}
	public void setSuccess(YesNo success) {
		this.success = success;
	}
	
	//toString
	@Override
	public String toString() {
		return "Schedule_log [id=" + id + ", beanId=" + beanId + ", descr="
				+ descr + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", success=" + success + "]";
	}
}
