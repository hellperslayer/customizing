package com.easycerti.eframe.psm.control.vo;

import java.util.Date;

import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 코드 VO
 * 
 * @author yjyoo
 * @since 2015. 4. 17.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 17.           yjyoo            최초 생성
 *
 * </pre>
 */
public class Code extends AbstractValueObject{
	
	// 그룹코드 ID
	private String group_code_id = null;
	
	// 코드 ID
	private String code_id = null;
	
	// 코드 명
	private String code_name = null;
	
	// 코드 타입
	private String code_type = null;
	
	// 코드 설명
	private String description = null;
	
	// 정렬순서
	private Integer sort_order = null;
	
	// 사용여부
	private String use_flag = null;
	
	// 등록자
	private String insert_user_id = null;
	
	// 등록일자
	private Date insert_datetime = null;
	
	// 수정자
	private String update_user_id = null;
	
	private String system_seq = null;
	

	// 수정일자
	private Date update_datetime = null;
	
	/**
	 * etc
	 */
	// 그룹코드 이름
	private String group_code_name = null;
	
	private int agent_param_seq;
	private String agent_name;
	private String param_name;
	private String param_value;
	private String param_desc;
	private String show_yn;
	
	//접속자 권한
	private String auth_id = null;
	
	private int result_type_order;
	
	public int getResult_type_order() {
		return result_type_order;
	}

	public void setResult_type_order(int result_type_order) {
		this.result_type_order = result_type_order;
	}

	public String getAuth_id() {
		return auth_id;
	}

	public void setAuth_id(String auth_id) {
		this.auth_id = auth_id;
	}

	public int getAgent_param_seq() {
		return agent_param_seq;
	}

	public void setAgent_param_seq(int agent_param_seq) {
		this.agent_param_seq = agent_param_seq;
	}

	public String getAgent_name() {
		return agent_name;
	}

	public void setAgent_name(String agent_name) {
		this.agent_name = agent_name;
	}

	public String getParam_name() {
		return param_name;
	}

	public void setParam_name(String param_name) {
		this.param_name = param_name;
	}

	public String getParam_value() {
		return param_value;
	}

	public void setParam_value(String param_value) {
		this.param_value = param_value;
	}

	public String getParam_desc() {
		return param_desc;
	}

	public void setParam_desc(String param_desc) {
		this.param_desc = param_desc;
	}

	public String getShow_yn() {
		return show_yn;
	}

	public void setShow_yn(String show_yn) {
		this.show_yn = show_yn;
	}
	
	public String getGroup_code_id() {
		return group_code_id;
	}

	public void setGroup_code_id(String group_code_id) {
		this.group_code_id = group_code_id;
	}

	public String getCode_id() {
		return code_id;
	}

	public void setCode_id(String code_id) {
		this.code_id = code_id;
	}

	public String getCode_name() {
		return code_name;
	}

	public void setCode_name(String code_name) {
		this.code_name = code_name;
	}

	public String getCode_type() {
		return code_type;
	}

	public void setCode_type(String code_type) {
		this.code_type = code_type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getSort_order() {
		return sort_order;
	}

	public void setSort_order(Integer sort_order) {
		this.sort_order = sort_order;
	}

	public String getUse_flag() {
		return use_flag;
	}

	public void setUse_flag(String use_flag) {
		this.use_flag = use_flag;
	}

	public String getInsert_user_id() {
		return insert_user_id;
	}

	public void setInsert_user_id(String insert_user_id) {
		this.insert_user_id = insert_user_id;
	}

	public Date getInsert_datetime() {
		return insert_datetime;
	}

	public void setInsert_datetime(Date insert_datetime) {
		this.insert_datetime = insert_datetime;
	}

	public String getUpdate_user_id() {
		return update_user_id;
	}

	public void setUpdate_user_id(String update_user_id) {
		this.update_user_id = update_user_id;
	}

	public Date getUpdate_datetime() {
		return update_datetime;
	}

	public void setUpdate_datetime(Date update_datetime) {
		this.update_datetime = update_datetime;
	}

	public String getGroup_code_name() {
		return group_code_name;
	}

	public void setGroup_code_name(String group_code_name) {
		this.group_code_name = group_code_name;
	}
	public String getSystem_seq() {
		return system_seq;
	}

	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}

	@Override
	public String toString() {
		return "CodeBean [group_code_id=" + group_code_id + ", code_id="
				+ code_id + ", code_name=" + code_name + ", code_type="
				+ code_type + ", description=" + description + ", sort_order="
				+ sort_order + ", use_flag=" + use_flag + ", insert_user_id="
				+ insert_user_id + ", insert_datetime=" + insert_datetime
				+ ", update_user_id=" + update_user_id + ", update_datetime="
				+ update_datetime + ", group_code_name=" + group_code_name
				+ "]";
	}
}
