package com.easycerti.eframe.psm.control.service.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.core.key.Constant;
import com.easycerti.eframe.psm.control.dao.CacheDao;
import com.easycerti.eframe.psm.control.service.CacheService;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.control.vo.GroupCode;

@Service
public class CacheServiceImpl implements CacheService {

	@Resource(name="cacheDictionary")
	private Map<String, Map<String, String>> cacheDictionary;
	
	@Autowired
	private CacheDao cacheDao;

	@Override
	public DataModelAndView getEntireGroups(Map<String, String> parameters) {
		// TODO Auto-generated method stub
		
		GroupCode paramBean = CommonHelper.convertMapToBean(parameters, GroupCode.class);
		
		DataModelAndView modelAndView = new DataModelAndView();
		List<GroupCode> groupCodes = cacheDao.getAvailableGroups();
		modelAndView.addObject("groupCodes", groupCodes);
		modelAndView.addObject("paramBean",paramBean);
		
		return modelAndView;
	}
	
	@Override
	public List<GroupCode> getAvailableGroups() {
		// TODO Auto-generated method stub
		return cacheDao.getAvailableGroups();
	}

	@Override
	public Map<String, Map<String, String>> getCacheDictionary() {
		// TODO Auto-generated method stub
		return cacheDictionary;
	}

	@Override
	public void refreshAll() {
		// TODO Auto-generated method stub
		refreshGroup();
		refreshGroupCodes();
	}

	@Override
	public void refreshGroup() {
		// TODO Auto-generated method stub
		if (cacheDictionary == null)
			throw new RuntimeException("CacheDictionary is not initialized");
		List<GroupCode> groups = getAvailableGroups();
		Map<String, String> newMap = new HashMap<String, String>(1, 1);
		for (GroupCode group : groups) {
			newMap.put(group.getGroup_code_id(), group.getGroup_code_name());
		}
		updateDictionary(Constant.CACHE_GROUP, newMap);
	}

	@Override
	public void refreshGroupCodes() {
		// TODO Auto-generated method stub
		if (cacheDictionary == null)
			throw new RuntimeException("CacheDictionary is not initialized");
		List<Code> groupCodes = cacheDao.getAvailableCodes();
		String groupCode = null;
		Map<String, String> newGroupMap = null;
		for (Code item : groupCodes) {
			if (groupCode == null) {
				newGroupMap = new LinkedHashMap<String, String>(1, 1);
			} else if (!item.getGroup_code_id().equals(groupCode)) {
				updateDictionary(groupCode, newGroupMap);
				newGroupMap = new LinkedHashMap<String, String>(1, 1);
			}
			groupCode = item.getGroup_code_id();
			newGroupMap.put(item.getCode_id(), item.getCode_name());
		}
		if (groupCode != null)
			cacheDictionary.put(groupCode, newGroupMap);
	}

	private void updateDictionary(String code, Map<String, String> newMap) {
		Map<String, String> oldMap = cacheDictionary.get(code);
		if (oldMap == null) {
			cacheDictionary.put(code, new LinkedHashMap<String, String>(newMap));
		} else {
			oldMap.clear();
			oldMap.putAll(newMap);
		}
	}
	
	@Override
	public void flushToServletContext(ServletContext servletContext) {
		// TODO Auto-generated method stub
		Set<String> keySet = cacheDictionary.keySet();
		for (String key : keySet) {
			servletContext.setAttribute(Constant.CACHE_ATTR_PREFIX + key, cacheDictionary.get(key));
		}
	}
}
