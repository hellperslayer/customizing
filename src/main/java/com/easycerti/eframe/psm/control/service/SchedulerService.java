package com.easycerti.eframe.psm.control.service;

import java.util.List;
import java.util.Map;

import com.easycerti.eframe.psm.control.vo.ControlSearch;
import com.easycerti.eframe.psm.control.vo.Schedule;
import com.easycerti.eframe.psm.control.vo.ScheduleLog;
import com.easycerti.eframe.psm.control.vo.Server;

public interface SchedulerService {

	void addSchedule(Schedule schedule);

	List<Schedule> getEntireSchedules();

	Schedule findScheduleOne(String schId);

	List<Schedule> findScheduleList(ControlSearch search);
	
	void saveScheduleOne(Schedule schedule);

	void removeScheduleOne(String schId);

	List<Server> getServerList();

	Map<String, String> getSchedulerJobMap();

	void reSchedule() throws Exception;
	
	// 이하 스케줄러 로그
	void addLog(ScheduleLog scheduleLog);

	void updateLog(ScheduleLog scheduleLog);

	ScheduleLog getLog(int id);
}
