package com.easycerti.eframe.psm.control.dao;

import java.util.List;

import com.easycerti.eframe.core.type.Switch;
import com.easycerti.eframe.psm.control.vo.ControlSearch;
import com.easycerti.eframe.psm.control.vo.Schedule;
import com.easycerti.eframe.psm.control.vo.ScheduleLog;
import com.easycerti.eframe.psm.control.vo.Server;

public interface SchedulerDao {

	int addSchedule(Schedule schedule);
	List<Schedule> getEntireSchedules();
	Schedule findScheduleOne(int schId);
	
	Switch getSchedulingByServerName(String name);
	List<Server> getServerList();
	int saveScheduleOne(Schedule schedule);
	int removeSchedule(int schId);
	
	// 이하 스케줄러 로그
	void addLog(ScheduleLog scheduleLog);
	void updateLog(ScheduleLog scheduleLog);
	ScheduleLog getLog(int id);
	
	List<Schedule> findScheduleList(ControlSearch search);
	int findScheduleList_count(ControlSearch search);
}
