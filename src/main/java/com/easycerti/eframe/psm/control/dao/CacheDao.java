package com.easycerti.eframe.psm.control.dao;

import java.util.List;

import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.control.vo.GroupCode;

public interface CacheDao {
	List<GroupCode> getAvailableGroups();
	List<Code> getAvailableCodes();
}
