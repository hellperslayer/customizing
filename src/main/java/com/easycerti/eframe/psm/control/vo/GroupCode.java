package com.easycerti.eframe.psm.control.vo;

import java.util.Date;

import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 그룹코드 VO
 * 
 * @author yjyoo
 * @since 2015. 4. 17.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 17.           yjyoo            최초 생성
 *
 * </pre>
 */
public class GroupCode extends AbstractValueObject {
	
	// 코드그룹 ID
	private String group_code_id = null;
	
	// 코드그룹 명
	private String group_code_name = null;
	
	// 코드그룹 설명
	private String description = null;
	
	// 사용여부
	private String use_flag = null;
	
	// 등록자
	private String insert_user_id = null;
	
	// 등록일자
	private Date insert_datetime = null;
	
	// 수정자
	private String update_user_id = null;
	
	// system_master
	private String system_seq = null;
	
	// 수정일자
	private Date update_datetime = null;
	
	public String getGroup_code_id() {
		return group_code_id;
	}
	public void setGroup_code_id(String group_code_id) {
		this.group_code_id = group_code_id;
	}
	public String getGroup_code_name() {
		return group_code_name;
	}
	public void setGroup_code_name(String group_code_name) {
		this.group_code_name = group_code_name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUse_flag() {
		return use_flag;
	}
	public void setUse_flag(String use_flag) {
		this.use_flag = use_flag;
	}
	public String getInsert_user_id() {
		return insert_user_id;
	}
	public void setInsert_user_id(String insert_user_id) {
		this.insert_user_id = insert_user_id;
	}
	public Date getInsert_datetime() {
		return insert_datetime;
	}
	public void setInsert_datetime(Date insert_datetime) {
		this.insert_datetime = insert_datetime;
	}
	public String getUpdate_user_id() {
		return update_user_id;
	}
	public void setUpdate_user_id(String update_user_id) {
		this.update_user_id = update_user_id;
	}
	public Date getUpdate_datetime() {
		return update_datetime;
	}
	public void setUpdate_datetime(Date update_datetime) {
		this.update_datetime = update_datetime;
	}
	public String getSystem_seq() {
		return system_seq;
	}
	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}

	
	@Override
	public String toString() {
		return "GroupCodeBean [group_code_id=" + group_code_id
				+ ", group_code_name=" + group_code_name + ", description="
				+ description + ", use_flag=" + use_flag + ", insert_user_id="
				+ insert_user_id + ", insert_datetime=" + insert_datetime
				+ ", update_user_id=" + update_user_id + ", update_datetime="
				+ update_datetime + ",system_seq="+ system_seq +"]";
	}
}
