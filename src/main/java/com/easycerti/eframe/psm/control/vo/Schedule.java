package com.easycerti.eframe.psm.control.vo;

import java.sql.Date;

import com.easycerti.eframe.core.type.YesNo;

public class Schedule {

	// fields
	private int schId;
	private String month;
	private String day;
	private String hour;
	private String minute;
	private String beanId;

	private YesNo enabled;
	private String descr;

	private String inserter_id;
	private String updater_id;
	private Date inserted_date;
	private Date updated_date;

	public int getSchId() {
		return schId;
	}

	// getter and setter
	public void setSchId(int schId) {
		this.schId = schId;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getHour() {
		return hour;
	}

	public void setHour(String hour) {
		this.hour = hour;
	}

	public String getMinute() {
		return minute;
	}

	public void setMinute(String minute) {
		this.minute = minute;
	}

	public String getBeanId() {
		return beanId;
	}

	public void setBeanId(String beanId) {
		this.beanId = beanId;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public YesNo getEnabled() {
		return enabled;
	}

	public void setEnabled(YesNo enabled) {
		this.enabled = enabled;
	}

	public String getInserter_id() {
		return inserter_id;
	}

	public void setInserter_id(String inserter_id) {
		this.inserter_id = inserter_id;
	}

	public String getUpdater_id() {
		return updater_id;
	}

	public void setUpdater_id(String updater_id) {
		this.updater_id = updater_id;
	}

	public Date getInserted_date() {
		return inserted_date;
	}

	public void setInserted_date(Date inserted_date) {
		this.inserted_date = inserted_date;
	}

	public Date getUpdated_date() {
		return updated_date;
	}

	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}

	// toString
	@Override
	public String toString() {
		return "Schedule [schId=" + schId + ", month=" + month + ", day=" + day
				+ ", hour=" + hour + ", minute=" + minute + ", beanId="
				+ beanId + ", enabled=" + enabled + ", descr=" + descr
				+ ", inserter_id=" + inserter_id + ", updater_id=" + updater_id
				+ ", inserted_date=" + inserted_date + ", updated_date="
				+ updated_date + "]";
	}
}
