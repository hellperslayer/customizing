package com.easycerti.eframe.psm.control.vo;

import com.easycerti.eframe.common.vo.SearchBase;

public class ControlSearch extends SearchBase {
	private String beanId;

	public String getBeanId_1() {
		return beanId;
	}

	public void setBeanId_1(String beanId) {
		this.beanId = beanId;
	}

	@Override
	public String toString() {
		return "ControlSearch [beanId=" + beanId + "]";
	}
}
