package com.easycerti.eframe.psm.test.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.test.service.TestExamSvc;
import com.easycerti.eframe.psm.test.vo.TestExamVo;

@Controller
@RequestMapping("/testExam/*")
public class TestExamCtrl {

	@Autowired
	public TestExamSvc testExamSvc;
	
	@RequestMapping(value={"list.html"}, method={RequestMethod.POST})
	public DataModelAndView testExamList(@ModelAttribute("search") TestExamVo search, @RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = testExamSvc.testExamList(search); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("testExamList");

		return modelAndView;
	}
	
	@RequestMapping(value={"setChart.html"}, method={RequestMethod.POST})
	public DataModelAndView setChart(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = testExamSvc.setChart(); 
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
}
