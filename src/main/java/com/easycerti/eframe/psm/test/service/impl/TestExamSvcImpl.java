package com.easycerti.eframe.psm.test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.test.dao.TestExamDao;
import com.easycerti.eframe.psm.test.service.TestExamSvc;
import com.easycerti.eframe.psm.test.vo.TestExamVo;

@Service
public class TestExamSvcImpl implements TestExamSvc {

	@Autowired
	TestExamDao testExamDao;
	
	@Override
	public DataModelAndView testExamList(TestExamVo vo) {
		
		DataModelAndView dataModelAndView = new DataModelAndView();
		
		List<TestExamVo> testList = testExamDao.selectTestExamList(vo);
		dataModelAndView.addObject("testList", testList);
		
		return dataModelAndView;
	}
	
	@Override
	public DataModelAndView setChart() {
		
		DataModelAndView dataModelAndView = new DataModelAndView();
		
		List<TestExamVo> result = testExamDao.setChart();
		dataModelAndView.addObject("result", result);
		
		return dataModelAndView;
	}
}
