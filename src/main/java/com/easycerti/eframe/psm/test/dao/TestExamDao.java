package com.easycerti.eframe.psm.test.dao;

import java.util.List;

import com.easycerti.eframe.psm.test.vo.TestExamVo;

public interface TestExamDao {
	public List<TestExamVo> selectTestExamList(TestExamVo search);
	
	public List<TestExamVo> setChart();
}
