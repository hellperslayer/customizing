package com.easycerti.eframe.psm.test.service;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.test.vo.TestExamVo;

public interface TestExamSvc {

	public DataModelAndView testExamList(TestExamVo vo);
	
	public DataModelAndView setChart();
}
