package com.easycerti.eframe.psm.system_management.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.system_management.service.ChangeThemeMngtSvc;

@Controller
@RequestMapping(value="/changeThemeMngt/*")
public class ChangeThemeMngtCtrl {
	
	@Autowired
	private ChangeThemeMngtSvc changeThemeMngtSvc;
	
	@RequestMapping(value="list.html",method={RequestMethod.POST})
	public DataModelAndView ChangeThemeList(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		DataModelAndView modelAndView = new DataModelAndView();
		
		modelAndView = changeThemeMngtSvc.changeThemeList(parameters);
		modelAndView.setViewName("changeThemeList");
		
		return modelAndView;
	
	}	
	
	@RequestMapping(value="update.html",method={RequestMethod.POST})
	public DataModelAndView ChangeThemeUpdate(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		DataModelAndView modelAndView = new DataModelAndView();
		
		modelAndView = changeThemeMngtSvc.changeThemeUpdate(parameters);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	
	}	
	
}
