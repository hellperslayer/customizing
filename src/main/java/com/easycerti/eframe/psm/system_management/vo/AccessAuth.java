package com.easycerti.eframe.psm.system_management.vo;


import com.easycerti.eframe.common.vo.AbstractValueObject;

public class AccessAuth extends AbstractValueObject {
	
	public AccessAuth() {}
	
	// 사원 ID
	private String emp_user_id;
	
	// 시스템 번호
	private String system_seq;
	private String temp_system_seq;
	private String system_name;
	
	// 직원 이름
	private String emp_user_name;
	
	// 부서 ID
	private String dept_id;
	private String dept_name;
	
	// 전화번호
	private String mobile_number;

	// 상태
	private String status;
	
	private String access_date;
	private String expire_date;
	private String update_date;
	private String change_date;
	private String approver;
	private String reason;
	private String update_reason;
	private int seq;
	private String manual_flag;
	private String process_content;
	
	// 관리자 정보
	private String approver_id;
	private String approver_ip;
	
	
	
	

	public String getApprover_id() {
		return approver_id;
	}

	public void setApprover_id(String approver_id) {
		this.approver_id = approver_id;
	}

	public String getApprover_ip() {
		return approver_ip;
	}

	public void setApprover_ip(String approver_ip) {
		this.approver_ip = approver_ip;
	}

	public String getProcess_content() {
		return process_content;
	}

	public void setProcess_content(String process_content) {
		this.process_content = process_content;
	}

	public String getUpdate_reason() {
		return update_reason;
	}

	public void setUpdate_reason(String update_reason) {
		this.update_reason = update_reason;
	}

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public String getManual_flag() {
		return manual_flag;
	}

	public void setManual_flag(String manual_flag) {
		this.manual_flag = manual_flag;
	}

	public String getEmp_user_id() {
		return emp_user_id;
	}

	public void setEmp_user_id(String emp_user_id) {
		this.emp_user_id = emp_user_id;
	}
	
	public String getSystem_seq() {
		return system_seq;
	}

	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}

	public String getTemp_system_seq() {
		return temp_system_seq;
	}

	public void setTemp_system_seq(String temp_system_seq) {
		this.temp_system_seq = temp_system_seq;
	}

	public String getSystem_name() {
		return system_name;
	}

	public void setSystem_name(String system_name) {
		this.system_name = system_name;
	}

	public String getEmp_user_name() {
		return emp_user_name;
	}

	public void setEmp_user_name(String emp_user_name) {
		this.emp_user_name = emp_user_name;
	}

	public String getDept_id() {
		return dept_id;
	}

	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}

	public String getDept_name() {
		return dept_name;
	}

	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}

	public String getMobile_number() {
		return mobile_number;
	}

	public void setMobile_number(String mobile_number) {
		this.mobile_number = mobile_number;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAccess_date() {
		return access_date;
	}

	public void setAccess_date(String access_date) {
		this.access_date = access_date;
	}

	public String getExpire_date() {
		return expire_date;
	}

	public void setExpire_date(String expire_date) {
		this.expire_date = expire_date;
	}

	public String getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}

	public String getChange_date() {
		return change_date;
	}

	public void setChange_date(String change_date) {
		this.change_date = change_date;
	}

	public String getApprover() {
		return approver;
	}

	public void setApprover(String approver) {
		this.approver = approver;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}	
	
}
