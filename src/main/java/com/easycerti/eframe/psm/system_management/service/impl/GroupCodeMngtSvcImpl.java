package com.easycerti.eframe.psm.system_management.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.StringUtils;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.control.service.CacheService;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.control.vo.GroupCode;
import com.easycerti.eframe.psm.system_management.dao.CodeMngtDao;
import com.easycerti.eframe.psm.system_management.dao.GroupCodeMngtDao;
import com.easycerti.eframe.psm.system_management.service.GroupCodeMngtSvc;
import com.easycerti.eframe.psm.system_management.vo.GroupCodeList;

/**
 * 그룹코드 관련 Service Implements
 * 
 * @author yjyoo
 * @since 2015. 4. 17.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 17.           yjyoo            최초 생성
 *
 * </pre>
 */
@Service
public class GroupCodeMngtSvcImpl implements GroupCodeMngtSvc {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private GroupCodeMngtDao groupCodeMngtDao;
	
	@Autowired
	private CodeMngtDao codeMngtDao;
	
	@Autowired
	private CacheService cacheSvc;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	@Autowired
	private CommonDao commonDao;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;
	
	@Override
	public DataModelAndView findGroupCodeMngtList(Map<String, String> parameters) {
		parameters = CommonHelper.setPageParam(parameters, defaultPageSize);
		GroupCode paramBean = CommonHelper.convertMapToBean(parameters, GroupCode.class);
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		List<GroupCode> groupCodes = groupCodeMngtDao.findGroupCodeMngtList(paramBean);
		Integer totalCount = groupCodeMngtDao.findGroupCodeMngtOne_count(paramBean);
		GroupCodeList groupCodeListBean = new GroupCodeList(groupCodes, totalCount.toString());
		
		SimpleCode simpleCode = new SimpleCode("/groupCodeMngt/list.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		modelAndView.addObject("groupCodeList", groupCodeListBean);
		modelAndView.addObject("paramBean", paramBean);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView findGroupCodeMngtOne(Map<String, String> parameters) {
		GroupCode paramBean = CommonHelper.convertMapToBean(parameters, GroupCode.class);

		SimpleCode simpleCode = new SimpleCode("/groupCodeMngt/detail.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		if(StringUtils.notNullCheck(paramBean.getGroup_code_id())){ // 상세
			GroupCode groupCodeDetail = groupCodeMngtDao.findGroupCodeMngtOne(paramBean);
			modelAndView.addObject("groupCodeDetail", groupCodeDetail);
		}
		
		modelAndView.addObject("paramBean", paramBean);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView addGroupCodeMngtOne(Map<String, String> parameters) {
		GroupCode paramBean = CommonHelper.convertMapToBean(parameters, GroupCode.class);

		// Validation Check (group_code 중복)
		GroupCode groupCode = groupCodeMngtDao.findGroupCodeMngtOne(paramBean);
		if(groupCode != null) {
			throw new ESException("SYS017J");
		}
					
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			groupCodeMngtDao.addGroupCodeMngtOne(paramBean);
			transactionManager.commit(transactionStatus);
			cacheSvc.refreshGroup();
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[GroupCodeMngtSvcImpl.addGroupCodeMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS016J");
		}

		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView saveGroupCodeMngtOne(Map<String, String> parameters) {
		GroupCode paramBean = CommonHelper.convertMapToBean(parameters, GroupCode.class);
		
		// 그룹코드 '미사용' 으로 수정 시 하위 코드 확인 작업
		if(paramBean.getUse_flag().equals(CommonResource.NO.charAt(0))) {
			Code codeParamBean = new Code();
			codeParamBean.setGroup_code_id(paramBean.getGroup_code_id());
			codeParamBean.setCode_type("SYSTEM");
			List<Code> codeList = codeMngtDao.findCodeMngtList(codeParamBean);
			
			// 하위에 시스템코드가 있으면 미사용으로 수정할 수 없음
			if(codeList.size() > 0) {
				throw new ESException("SYS054J");
			}
		}
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			groupCodeMngtDao.saveGroupCodeMngtOne(paramBean);
			transactionManager.commit(transactionStatus);
			cacheSvc.refreshGroup();
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[GroupCodeMngtSvcImpl.saveGroupCodeMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS018J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView removeGroupCodeMngtOne(Map<String, String> parameters) {
		GroupCode paramBean = CommonHelper.convertMapToBean(parameters, GroupCode.class);
	
		Code codeParamBean = new Code();
		codeParamBean.setGroup_code_id(paramBean.getGroup_code_id());
		codeParamBean.setCode_type("SYSTEM");
		List<Code> codeList = codeMngtDao.findCodeMngtList(codeParamBean);
		
		// 하위에 시스템코드가 있으면 삭제할 수 없음
		if(codeList.size() > 0) {
			throw new ESException("SYS052J");
		}
		else {
			codeParamBean.setCode_type(CommonResource.EMPTY_STRING);
		}
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			// 그룹코드 삭제시 관계된 코드도 일괄삭제 (시스템 코드가 없는 경우에만)
			codeMngtDao.removeCodeMngtOne(codeParamBean);
			groupCodeMngtDao.removeGroupCodeMngtOne(paramBean);
			transactionManager.commit(transactionStatus);
			cacheSvc.refreshGroup();
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[GroupCodeSvcImpl.removeGroupCodeMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS019J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}

}
