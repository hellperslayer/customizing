package com.easycerti.eframe.psm.system_management.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.system_management.vo.EmpUser;
import com.easycerti.eframe.psm.system_management.vo.EmpUserSearch;

/**
 * 사원 관련 Service Interface
 * 
 * @author yjyoo
 * @since 2015. 4. 21.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 21.           yjyoo            최초 생성
 *   2015. 5. 26.           yjyoo            엑셀 다운로드 추가
 *
 * </pre>
 */
public interface EmpUserMngtSvc {
	
	/**
	 * 사원 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 21.
	 * @return DataModelAndView
	 */
	public DataModelAndView findEmpUserMngtList(EmpUserSearch search);
	
	/**
	 * 사원 리스트 엑셀 다운로드
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 26.
	 * @return void
	 */
	public void findEmpUserMngtList_download(DataModelAndView modelAndView, EmpUserSearch search, HttpServletRequest request);
	
	/**
	 * 사원 상세정보
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 21.
	 * @return DataModelAndView
	 */
	public DataModelAndView findEmpUserMngtOne(EmpUser empUser);
	
	/**
	 * 사원 추가 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 21.
	 * @return DataModelAndView
	 */
	public DataModelAndView addEmpUserMngtOne(Map<String, String> parameters, HttpServletRequest request) throws Exception;
	
	/**
	 * 사원 수정 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 21.
	 * @return DataModelAndView
	 */
	public DataModelAndView saveEmpUserMngtOne(Map<String, String> parameters, HttpServletRequest request) throws Exception;
	
	/**
	 * 사원 삭제 (단건) 
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 21.
	 * @return DataModelAndView
	 */
	public DataModelAndView removeEmpUserMngtOne(Map<String, String> parameters, HttpServletRequest request);
	
	/**
	 * 사용자 로그인
	 *  
	 * @author tjlee
	 * @since 2015. 6. 11.
	 * @return DataModelAndView
	 */
	public DataModelAndView login(Map<String, String> parameters, HttpServletRequest request);
	
	/**
	 * 엑셀 업로드
	 *  
	 * @author ehchoi
	 * @since 2016. 7. 07.
	 * @return DataModelAndView
	 */
	public String upLoadSystemMngtOne(Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response)throws Exception;
	
}
