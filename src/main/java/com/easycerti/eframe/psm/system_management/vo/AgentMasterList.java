package com.easycerti.eframe.psm.system_management.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 대상에이전트리스트 VO
 * 
 * @author yjyoo
 * @since 2015. 4. 24.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 24.           yjyoo            최초 생성
 *
 * </pre>
 */
public class AgentMasterList extends AbstractValueObject {
	
	// 에이전트 리스트
	private List<AgentMaster> agents = null;
	
	public AgentMasterList() {}
	
	public AgentMasterList(List<AgentMaster> agents){
		this.agents = agents;
	}
	
	public AgentMasterList(List<AgentMaster> agents, String count){
		this.agents = agents;
		setPage_total_count(count);
	}

	public List<AgentMaster> getAgents() {
		return agents;
	}

	public void setAgents(List<AgentMaster> agents) {
		this.agents = agents;
	}
	
	
}
