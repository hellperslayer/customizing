package com.easycerti.eframe.psm.system_management.service;

import java.util.Map;

import com.easycerti.eframe.common.spring.DataModelAndView;

/**
 * 그룹코드 관련 Service Interface
 * 
 * @author yjyoo
 * @since 2015. 4. 17.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 17.           yjyoo            최초 생성
 *
 * </pre>
 */
public interface GroupCodeMngtSvc {

	/**
	 * 그룹코드 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 21.
	 * @return DataModelAndView
	 */
	public DataModelAndView findGroupCodeMngtList(Map<String, String> parameters);
	
	/**
	 * 그룹코드 상세정보
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	public DataModelAndView findGroupCodeMngtOne(Map<String, String> parameters);
	
	/**
	 * 그룹코드 추가 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	public DataModelAndView addGroupCodeMngtOne(Map<String, String> parameters);
	
	/**
	 * 그룹코드 수정 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	public DataModelAndView saveGroupCodeMngtOne(Map<String, String> parameters);
	
	/**
	 * 그룹코드 삭제 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	public DataModelAndView removeGroupCodeMngtOne(Map<String, String> parameters);
	
}
