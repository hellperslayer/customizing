package com.easycerti.eframe.psm.system_management.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.StringUtils;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.psm.system_management.dao.AdminUserMngtDao;
import com.easycerti.eframe.psm.system_management.dao.AuthMngtDao;
import com.easycerti.eframe.psm.system_management.service.AdminUserMngtSvc;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;
import com.easycerti.eframe.psm.system_management.vo.AdminUserSearch;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

/**
 * 관리자 관련 Controller
 * 
 * @author yjyoo
 * @since 2015. 4. 20.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 20.           yjyoo           최초 생성
 *   2015. 5. 22.			yjyoo			엑셀 다운로드 추가
 *
 * </pre>
 */
@Controller
@RequestMapping(value="/adminUserMngt/*")
public class AdminUserMngtCtrl {

	@Autowired
	private AdminUserMngtSvc adminUserMngtSvc;
	
	@Autowired
	private AdminUserMngtDao adminUserMngtDao;
	
	/**
	 * 관리자 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[환경관리] 관리자관리")
	@RequestMapping(value="list.html",method={RequestMethod.POST})
	public DataModelAndView findAdminUserMngtList(@ModelAttribute("search") AdminUserSearch search, @RequestParam Map<String, String> parameters, HttpServletRequest request){
		DataModelAndView modelAndView = adminUserMngtSvc.findAdminUserMngtList(search, request);

		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("adminUserMngtList");
		
		return modelAndView;
	}
	
	/**
	 * 관리자 리스트 엑셀 다운로드
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 22.
	 * @return DataModelAndView
	 */
	
	@RequestMapping(value="download.html",method={RequestMethod.POST})
	public DataModelAndView addfindAdminUserMngtList_download(@ModelAttribute("search") AdminUserSearch search, @RequestParam Map<String, String> parameters, HttpServletRequest request){
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);

		adminUserMngtSvc.findAdminUserMngtList_download(modelAndView, search, request);
		
		return modelAndView;
	}
	
	/**
	 * 관리자 상세정보
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[환경관리] 관리자관리 상세")
	@RequestMapping(value="detail.html",method={RequestMethod.POST})
	public DataModelAndView findAdminUserMngtOne(@RequestParam String admin_user_id, 
			AdminUserSearch search, @RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		DataModelAndView modelAndView = adminUserMngtSvc.findAdminUserMngtOne(admin_user_id, request);
		
		List<SystemMaster> systemMaster = adminUserMngtSvc.getSystemMaster();
		
		modelAndView.addObject("systemMaster", systemMaster);
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("adminUserMngtDetail");
		
		return modelAndView;
	}
	
	/**
	 * 관리자 추가 (json)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="add.html",method={RequestMethod.POST})
	public DataModelAndView addAdminUserMngtOne(@RequestParam Map<String, String> parameters,HttpServletRequest request) throws Exception{
		//String[] args = {"admin_user_id", "password", "admin_user_name"};
		String[] args = {"admin_user_id", "admin_user_name"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		// 권한 체크 (요청 권한이 현재 사용자 권한보다 높을 경우 리턴) 
		AdminUser adminUser = SystemHelper.findSessionUser(request);
		int compare = parameters.get("auth_id").compareTo(adminUser.getAuth_id());
		if (compare < 0) {
			throw new ESException("SYS070J");
		}
		
		parameters.put("insert_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		DataModelAndView modelAndView = adminUserMngtSvc.addAdminUserMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	/**
	 * 관리자 수정 (json)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="save.html",method={RequestMethod.POST})
	public DataModelAndView saveAdminUserMngtOne(@RequestParam Map<String, String> parameters,HttpServletRequest request) throws Exception{
		String[] args = {"admin_user_id", "admin_user_name"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){ //,"password"
			throw new ESException("SYS004J");
		}
		
		// 권한 체크 (요청 권한이 현재 사용자 권한보다 높을 경우 리턴) 
		AdminUser adminUser = SystemHelper.findSessionUser(request);
		int compare = parameters.get("auth_id").compareTo(adminUser.getAuth_id());
		if (compare < 0) {
			throw new ESException("SYS070J");
		}
		
		parameters.put("update_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		DataModelAndView modelAndView = adminUserMngtSvc.saveAdminUserMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	/**
	 * 관리자 삭제 (json)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="remove.html",method={RequestMethod.POST})
	public DataModelAndView removeAdminUserMngtOne(@RequestParam Map<String, String> parameters,HttpServletRequest request) throws Exception{
		String[] args = {"admin_user_id"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		parameters.put("update_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		DataModelAndView modelAndView = adminUserMngtSvc.removeAdminUserMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@RequestMapping(value="initPassword.html", method={RequestMethod.POST})
	@ResponseBody
	public int initPassword(@RequestParam Map<String, String> parameters,HttpServletRequest request) {
		
		int result = 0;
		
		String admin_user_id = parameters.get("admin_user_id");
		
		try {
			adminUserMngtDao.initPassword(admin_user_id);
			result = 1;
		} catch (Exception e) {
			result = 0;
		}
		
		return result;
	}
	
	@RequestMapping(value = "adminUserSysList", method = {RequestMethod.POST})
	public DataModelAndView adminUserSysList(@RequestParam Map<String, String> parameters) {
		DataModelAndView modelAndView = adminUserMngtSvc.adminUserSysList(parameters);
		modelAndView.setViewName("adminUserSystemSelect");
		return modelAndView;
	}
}
