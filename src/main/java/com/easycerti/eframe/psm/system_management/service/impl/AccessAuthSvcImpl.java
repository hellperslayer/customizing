package com.easycerti.eframe.psm.system_management.service.impl;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.ExcelFileReader;
import com.easycerti.eframe.common.util.XExcelFileReader;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.system_management.dao.AccessAuthDao;
import com.easycerti.eframe.psm.system_management.dao.AgentMngtDao;
import com.easycerti.eframe.psm.system_management.dao.DepartmentMngtDao;
import com.easycerti.eframe.psm.system_management.service.AccessAuthSvc;
import com.easycerti.eframe.psm.system_management.vo.AccessAuth;
import com.easycerti.eframe.psm.system_management.vo.AccessAuthSearch;
import com.easycerti.eframe.psm.system_management.vo.Department;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

@Service
public class AccessAuthSvcImpl implements AccessAuthSvc {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private AccessAuthDao accessAuthDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private AgentMngtDao agentMngtDao;
	
	@Autowired
	private DepartmentMngtDao departmentMngtDao;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;
	
	@Override
	public DataModelAndView findAccessAuthList(AccessAuthSearch search) {
		int pageSize = Integer.valueOf(defaultPageSize);
		
		search.setSize(pageSize);
		search.setTotal_count(accessAuthDao.findAccessAuthList_count(search));
		
		List<AccessAuth> empUserList = accessAuthDao.findAccessAuthList(search);
		List<SystemMaster> systemMasterList = accessAuthDao.findSystemMasterList_byAdmin(search);
		
		SimpleCode simpleCode = new SimpleCode("/accessAuth/list.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("empUserList", empUserList);
		modelAndView.addObject("systemMasterList", systemMasterList);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	@Override
	public void findAccessAuthList_download(DataModelAndView modelAndView, AccessAuthSearch search,
			HttpServletRequest request) {
		String fileName = "PSM_접근권한_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "접근권한관리";
		
		search.setUseExcel("true");
		List<AccessAuth> empUsers = accessAuthDao.findAccessAuthList(search);
		
		for(AccessAuth access : empUsers) {
			if(access.getStatus().equals("R"))
				access.setStatus("퇴직");
			else if(access.getStatus().equals("L"))
				access.setStatus("휴직");
			else
				access.setStatus("재직");
			
			if(access.getManual_flag().equals("Y")) {
				access.setManual_flag("수동");
			} else {
				access.setManual_flag("자동");
			}
		}
		String[] columns = new String[] {"system_name", "approver", "approver_id", "approver_ip", "emp_user_name", "emp_user_id", "dept_name", "status", "access_date", "change_date", "expire_date", "reason", "manual_flag"};
		String[] heads = new String[] {"시스템", "관리자명", "관리자ID", "관리자IP", "사용자명", "사용자ID", "소속", "접근권한", "권한부여 일자", "권한변경 일자", "권한해제 일자", "사유", "수동관리여부"};
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", empUsers);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}
	
	@Override
	public DataModelAndView findAccessAuthDetail(AccessAuthSearch search) {

		AccessAuth accessAuth = accessAuthDao.findAccessAuthDetail(search);
		List<SystemMaster> systemList = agentMngtDao.findSystemMasterListAll();
		SimpleCode simpleCode = new SimpleCode("/accessAuth/detail.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		// 부서 리스트
		Department departmentParamBean = new Department();
		departmentParamBean.setUse_flag('Y');
		List<Department> departments = departmentMngtDao.findDepartmentMngtList_choose(departmentParamBean);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("accessAuth", accessAuth);
		modelAndView.addObject("systemList", systemList);
		modelAndView.addObject("departments",departments);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView addAccessAuth(Map<String, String> parameters) {
		AccessAuth paramBean = CommonHelper.convertMapToBean(parameters, AccessAuth.class);

		int empUserCount = accessAuthDao.findAccessAuth_validation(paramBean);
		if(empUserCount > 0) {
			throw new ESException("SYS037J");
		}
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			SimpleDateFormat transFormat2 = new SimpleDateFormat("yyyyMMddHHmmss");
			String update = transFormat2.format(new Date());
			SimpleDateFormat transFormat = new SimpleDateFormat("yyyyMMdd");
			String date = transFormat.format(new Date());
			paramBean.setUpdate_date(update);
			paramBean.setAccess_date(date);
			accessAuthDao.addAccessAuth(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[AccessAuthSvcImpl.addAccessAuth] MESSAGE : " + e.getMessage());
			throw new ESException("SYS036J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView saveAccessAuth(Map<String, String> parameters) {
		AccessAuth paramBean = CommonHelper.convertMapToBean(parameters, AccessAuth.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			SimpleDateFormat transFormat = new SimpleDateFormat("yyyyMMdd");
			SimpleDateFormat transFormat2 = new SimpleDateFormat("yyyyMMddHHmmss");
			String date = transFormat.format(new Date());
			String update = transFormat2.format(new Date());
			if(!parameters.get("temp_status").equals(paramBean.getStatus())) { // 상태가 변경되었을때
				if(paramBean.getStatus().equals("R")) { // 퇴직
					paramBean.setExpire_date(date);
				}else {	
					paramBean.setExpire_date("");
					paramBean.setChange_date(date);
				}
			}
			paramBean.setTemp_system_seq(paramBean.getSystem_seq());
			paramBean.setUpdate_date(update);
			accessAuthDao.saveAccessAuth(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[AccessAuthSvcImpl.saveAccessAuth] MESSAGE : " + e.getMessage());
			throw new ESException("SYS014J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView removeAccessAuth(Map<String, String> parameters) {
		AccessAuth paramBean = CommonHelper.convertMapToBean(parameters, AccessAuth.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			accessAuthDao.removeAccessAuth(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[AccessAuthSvcImpl.removeAccessAuth] MESSAGE : " + e.getMessage());
			throw new ESException("SYS015J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	@Override
	public String upLoadAccessAuth(Map<String, String> parameters, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		String result="false";
		String[] status_type = {"X","W","R","L","V","E","D"}; 
		AccessAuth paramBean = CommonHelper.convertMapToBean(parameters, AccessAuth.class);
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		//엑셀 Row 카운트는 1000으로 지정한다		
		int rowCount=1000;
		
		//파일 업로드 기능  		
		MultipartHttpServletRequest mpr = (MultipartHttpServletRequest) request;  
		
		MultipartFile dir = mpr.getFile("file");
				
		File file = new File("/root/excelUpload/" + dir.getOriginalFilename());
		
		if (!file.exists()) { //폴더 없으면 폴더 생성
			file.mkdirs();
		}
		
		dir.transferTo(file);
		
		//업로드된 파일의 정보를 읽어들여 DB에 추가하는 기능 
		if (dir.getSize() > 0 ) { 
				
			List<String[]> readRows = new ArrayList<>();
			
			String ext = file.getName().substring(file.getName().lastIndexOf(".")+1);
			ext = ext.toLowerCase();
			if( "xlsx".equals(ext) ){	
				readRows = new XExcelFileReader(file.getAbsolutePath()).readRows(rowCount);
				
			}else if( "xls".equals(ext) ){	
				readRows = new ExcelFileReader(file.getAbsolutePath()).readRows(rowCount);
			}
		
			for(int i=0; i<3; i++){	
				readRows.remove(0);
			}
			
			boolean isCheck = true;
			if( readRows.size() != 0 ){
				
			/*int columnCount = readRows.get(0).length;*/
			//각 셀 내용을 DB 컬럼과 Mapping
			try {
				for(String[] readRowArr : readRows){
					
					if(readRowArr.length != 0 && readRowArr[0].length() !=0){
						if(readRowArr[0].length() !=0 ){
							SystemMaster sm = accessAuthDao.findSystemSeq(readRowArr[0]);
							if(sm != null)
								paramBean.setSystem_seq(sm.getSystem_seq()); 
							else {
								isCheck = false;
								break;
							}
						}else {
							isCheck = false;
							break;
						}
						paramBean.setEmp_user_name(readRowArr[1]);
						paramBean.setEmp_user_id(readRowArr[2]);
						if(readRowArr[3].length() != 0) {
							if(readRowArr[3].equals("있음")) {
								paramBean.setStatus("W");
							} else {
								paramBean.setStatus("R");
							}
						}
						if(readRowArr[4].length() != 0)
							paramBean.setAccess_date(readRowArr[4]);
						if(readRowArr[5].length() != 0)
							paramBean.setExpire_date(readRowArr[5]);
						if(readRowArr[6].length() !=0)
							paramBean.setReason(readRowArr[6]);
						paramBean.setApprover(readRowArr[7]);
						if(readRowArr[8].length() != 0) {
							if(readRowArr[8].equals("수동관리")) {
								paramBean.setManual_flag("Y");
							} else {
								paramBean.setManual_flag("N");
							}
						}
						
						accessAuthDao.addAccessAuth(paramBean);
					}
				}
				if(isCheck) {
					readRows.clear();
					transactionManager.commit(transactionStatus);
				}
			} catch(DataAccessException e) {
				transactionManager.rollback(transactionStatus);
				readRows.clear();
				e.printStackTrace();
			/*	logger.error("[EmpUserMngtSvcImpl.upLoadEmpUserMngtOne] MESSAGE : " + e.getMessage());
				throw new ESException("SYS039J");*/
				return result;
			}
		}
			if(isCheck)
				result="true";
		}
		
		return result;
	}
}
