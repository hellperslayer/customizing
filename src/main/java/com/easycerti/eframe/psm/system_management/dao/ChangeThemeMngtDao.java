package com.easycerti.eframe.psm.system_management.dao;

import com.easycerti.eframe.psm.system_management.vo.ChangeTheme;

public interface ChangeThemeMngtDao {
	//테마 수정
	void changeThemeUpdateTheme(ChangeTheme paramBean);
	//대시보드 수정
	void changeThemeUpdateDashboard(ChangeTheme paramBean);

}
