package com.easycerti.eframe.psm.system_management.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.psm.system_management.service.IpMngtSvc;


/**
 * IP 대역 관리 Controller
 * 
 * @author ehchoi
 * @since 2016. 4. 15.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2016. 4. 15.           ehchoi           최초 생성
 *
 * </pre>
 */
@Controller
@RequestMapping(value="/ipMngt/*")
public class IpMngtCtrl {

	@Autowired
	private IpMngtSvc ipMngtSvc;
	
	/**
	 * ip 리스트</br>
	 * - 부서 데이터는 tree 형태
	 * 
	 * @author ehchoi
	 * @since 2016. 4. 18.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="list.html",method={RequestMethod.POST})
	public DataModelAndView findIpMngtList(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		DataModelAndView modelAndView = ipMngtSvc.findIpMngtList(parameters);
		modelAndView.setViewName("ipMngtList");
		
		return modelAndView;
	}
	
	/**
	 * 부서 상세정보
	 * 
	 * @author ehchoi
	 * @since 2016. 4. 18.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="detail.html",method={RequestMethod.POST})
	public DataModelAndView findIpMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		DataModelAndView modelAndView = ipMngtSvc.findIpMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	
	@RequestMapping(value="add.html",method={RequestMethod.POST})
	public DataModelAndView addDepartmentMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		String[] args = {"parent_dept_id", "dept_name", "use_flag"};
		
	/*	if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}*/
		
		parameters.put("insert_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		DataModelAndView modelAndView = ipMngtSvc.addIpMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	/**
	 * 부서 수정 (json)
	 * 
	 * @author ehchoi
	 * @since 2016. 4. 18.
	 * @return String
	 */
	/*	@RequestMapping(value="save.html",method={RequestMethod.POST})
	public String saveIpMngtOne (@ModelAttribute("ip") Ip ip) throws Exception{
		int i =0;
		try{	
			i = ipMngtSvc.saveIpMngtOne(ip);
		}catch (Exception e) {
			return "false";
		}	
		return "true";
	}*/
	
	@RequestMapping(value="save.html",method={RequestMethod.POST})
	public DataModelAndView saveIpMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		String[] args = {"parent_dept_id", "dept_name", "use_flag"};
		
		parameters.put("insert_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		DataModelAndView modelAndView = ipMngtSvc.saveIpMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
		
/*	@RequestMapping(value="save.html",method={RequestMethod.POST})
	public String saveIpMngtOne (@ModelAttribute("ip") Ip ip) throws Exception{
		int i =0;
		try{	
			i = ipMngtSvc.saveIpMngtOne(ip);
		}catch (Exception e) {
			return "false";
		}	
		return "true";
	}
	*/
	/**
	 * 부서 삭제 (json)
	 * 
	 * @author ehchoi
	 * @since 2016. 4. 18.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="remove.html",method={RequestMethod.POST})
	public DataModelAndView removeIpMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		String[] args = {"dept_id"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		DataModelAndView modelAndView = ipMngtSvc.removeIpMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
}
