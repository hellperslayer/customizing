package com.easycerti.eframe.psm.system_management.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.system_management.dao.IpInfoMngDao;
import com.easycerti.eframe.psm.system_management.service.IpInfoMngSvc;
import com.easycerti.eframe.psm.system_management.vo.IpInfoBean;
import com.easycerti.eframe.psm.system_management.vo.IpInfoList;

@Service
public class IpInfoMngSvcImpl implements IpInfoMngSvc {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private IpInfoMngDao ipInfoMngDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;
	
	@Override
	public DataModelAndView ipInfoMngList(Map<String, String> parameters) {
		parameters = CommonHelper.setPageParam(parameters, defaultPageSize);				
		SimpleCode simpleCode = new SimpleCode();
		IpInfoBean paramBean = CommonHelper.convertMapToBean(parameters, IpInfoBean.class);
		
		String index = "ipInfo/list.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		List<IpInfoBean> ipInfoBean = ipInfoMngDao.ipInfoMngList(paramBean);
		Integer totalCount = ipInfoMngDao.ipInfoMngList_count(paramBean);
		IpInfoList ipInfoList = new IpInfoList(ipInfoBean, totalCount.toString());
		DataModelAndView modelAndView = new DataModelAndView();
		
		
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("ipInfoList", ipInfoList);
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}

	@Override
	public void addUserInfo(Map<String, String> parameters) {
		IpInfoBean paramBean = CommonHelper.convertMapToBean(parameters, IpInfoBean.class);
		
		ipInfoMngDao.addUserInfo(paramBean);
		
	}

	@Override
	public DataModelAndView deleteUserInfo(Map<String, String> parameters) {
		IpInfoBean paramBean = CommonHelper.convertMapToBean(parameters, IpInfoBean.class);
		
		try {
			ipInfoMngDao.deleteUserInfo(paramBean);
		} catch(DataAccessException e) {
			throw new ESException("SYS030J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
}
