package com.easycerti.eframe.psm.system_management.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.setup.dao.DLogMenuMappSetupDao;
import com.easycerti.eframe.psm.setup.vo.IndvinfoTypeSetup;
import com.easycerti.eframe.psm.system_management.dao.AdminUserMngtDao;
import com.easycerti.eframe.psm.system_management.dao.EmpUserMngtDao;
import com.easycerti.eframe.psm.system_management.dao.SystemMngtDao;
import com.easycerti.eframe.psm.system_management.service.SystemMngtSvc;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;
import com.easycerti.eframe.psm.system_management.vo.System;

/**
 * 부서 관련 Service Implements
 * 
 * @author yjyoo
 * @since 2015. 4. 17.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2016. 4. 18.           ehchoi            최초 생성
 *
 * </pre>
 */
@Service
public class SystemMngtSvcImpl implements SystemMngtSvc {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private SystemMngtDao systemMngtDao;
	
	@Autowired
	private AdminUserMngtDao adminUserMngtDao;
	
	@Autowired
	private EmpUserMngtDao empUserMngtDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private DLogMenuMappSetupDao dLogMenuMappSetupDao;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	@Override
	public DataModelAndView findSystemMngtList(Map<String, String> parameters) {
		System paramBean = CommonHelper.convertMapToBean(parameters, System.class);
		SimpleCode simpleCode = new SimpleCode();
		String index = "/systemMngt/list.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		List<System> systems = systemMngtDao.findSystemMngtList(paramBean);
		List<Map<String, String>> agentType = systemMngtDao.findAgentTypeList();
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("hierarchySystems", systems);
		modelAndView.addObject("agentType", agentType);
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView findSystemMngtOne(Map<String, String> parameters) {
		System paramBean = CommonHelper.convertMapToBean(parameters, System.class);
		SimpleCode simpleCode = new SimpleCode();
		String index = "systemMngt/detail.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		System detailBean = systemMngtDao.findSystemMngtOne(paramBean);
		List<System> identiInfoList = systemMngtDao.findSystemIdentificationList(paramBean);
		modelAndView.addObject("systemDetail", detailBean);
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("identiInfoList", identiInfoList);
		modelAndView.addObject("paramBean",paramBean);
		return modelAndView;
	}

	@Override
	public DataModelAndView addSystemMngtOne(Map<String, String> parameters) {
		System paramBean = CommonHelper.convertMapToBean(parameters, System.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			systemMngtDao.addSystemMngtOne(paramBean);
			systemMngtDao.addSystemApprovalOne(paramBean);
			//systemMngtDao.addSystemMngtOneUse(paramBean);
			
			for (String key : parameters.keySet()) {
				if (key.indexOf("privacy_seq_") > -1) {
					String privacy_seq = parameters.get(key);
					String flag = parameters.get("radio_" + privacy_seq);
					String flag2 = parameters.get("radio2_" + privacy_seq);
					
					System system = new System();
					system.setSystem_seq(paramBean.getSystem_seq());
					system.setPrivacy_type(privacy_seq);
					system.setUse_flag(flag);
					system.setPrivacy_masking(flag2);
					systemMngtDao.addRegularExpressionMngUse(system);
				}
			}
			
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[SystemMngtSvcImpl.addSystemMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS063J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView saveSystemMngtOne(Map<String, String> parameters) {
		System paramBean = CommonHelper.convertMapToBean(parameters, System.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			systemMngtDao.saveSystemMngtOne(paramBean);
			systemMngtDao.saveSystemApprovalOne(paramBean);
			if(parameters.get("sub_batch")!=null&&parameters.get("sub_batch").equals("on")) {
				dLogMenuMappSetupDao.saveThresholdtoSystem(paramBean);
			} 
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			e.printStackTrace();
			logger.error("[SystemMngtSvcImpl.saveSystemMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS064J") ;
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
/*	public int saveIpMngtOne(Ip ip) throws Exception {
		return ipMngtDao.saveIpMngtOne(ip);
	}*/

	@Override
	public DataModelAndView removeSystemMngtOne(Map<String, String> parameters) {
		System paramBean = CommonHelper.convertMapToBean(parameters, System.class);
					
		AdminUser adminUserParamBean = new AdminUser();
			
		if(adminUserMngtDao.findAdminUserBySystemSeq(paramBean) > 0)
			throw new ESException("SYS061J");
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			// 관리자 부서 초기화
			adminUserMngtDao.saveAdminUserMngtOne_toRemoveDept(adminUserParamBean);
			// 부서 삭제
			systemMngtDao.removeSystemMngtOne(paramBean);
			systemMngtDao.removeSystemApprovalOne(paramBean);
			systemMngtDao.removeSystemMngtOneUse(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[SystemMngtSvcImpl.removeSystemMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS065J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView addView(Map<String, String> parameters) {

		SimpleCode simpleCode = new SimpleCode();
		simpleCode.setMenu_url("/systemMngt/addView.html");
		String index_id = commonDao.checkIndex(simpleCode);
		List<IndvinfoTypeSetup> list = systemMngtDao.findReqTypeList();
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("list", list);
		return modelAndView;
	}

	@Override
	public String saveIdentiInfo(Map<String, String> parameters) {
		String result = "";
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			System system = new System();
			system.setSystem_seq(parameters.get("system_seq"));
			system.setPrivacy_type("1"); //주민등록번호
			system.setSort_order(1);
			system.setExistence_yn(parameters.get("existence_yn_1"));
			system.setInformation_ct(Integer.parseInt(parameters.get("information_ct_1")));
			systemMngtDao.saveSystemIdentificationInfo(system);
			system.setPrivacy_type("2"); //운전면허번호
			system.setSort_order(2);
			system.setExistence_yn(parameters.get("existence_yn_2"));
			system.setInformation_ct(Integer.parseInt(parameters.get("information_ct_2")));
			systemMngtDao.saveSystemIdentificationInfo(system);
			system.setPrivacy_type("3"); //여권번호
			system.setSort_order(3);
			system.setExistence_yn(parameters.get("existence_yn_3"));
			system.setInformation_ct(Integer.parseInt(parameters.get("information_ct_3")));
			systemMngtDao.saveSystemIdentificationInfo(system);
			system.setPrivacy_type("10"); //외국인등록번호
			system.setSort_order(4);
			system.setExistence_yn(parameters.get("existence_yn_4"));
			system.setInformation_ct(Integer.parseInt(parameters.get("information_ct_4")));
			systemMngtDao.saveSystemIdentificationInfo(system);
			transactionManager.commit(transactionStatus);
			result="success";
		} catch (Exception e) {
			transactionManager.rollback(transactionStatus);
			result="error";
		}
		return result;
	}
	
	
}
