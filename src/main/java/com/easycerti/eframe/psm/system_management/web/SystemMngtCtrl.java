package com.easycerti.eframe.psm.system_management.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.psm.system_management.service.SystemMngtSvc;

/**
 * IP 대역 관리 Controller
 * 
 * @author ehchoi
 * @since 2016. 4. 15.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2016. 4. 15.           ehchoi           최초 생성
 *
 * </pre>
 */
@Controller
@RequestMapping(value="/systemMngt/*")
public class SystemMngtCtrl {

	@Autowired
	private SystemMngtSvc systemMngtSvc;
	
	/**
	 * ip 리스트</br>
	 * - 부서 데이터는 tree 형태
	 * 
	 * @author ehchoi
	 * @since 2016. 4. 18.
	 * @return DataModelAndView
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[정책관리] 시스템관리")
	@RequestMapping(value="list.html",method={RequestMethod.POST})
	public DataModelAndView findSystemMngtList(@RequestParam Map<String, String> parameters, HttpServletRequest request){

		DataModelAndView modelAndView = systemMngtSvc.findSystemMngtList(parameters);
		modelAndView.setViewName("system_menu");
		modelAndView.addObject("sel_system_seq", parameters.get("sel_system_seq"));
		
		return modelAndView;
	}
	
	/**
	 * 부서 상세정보
	 * 
	 * @author ehchoi
	 * @since 2016. 4. 18.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="detail.html",method={RequestMethod.POST})
	public DataModelAndView findSystemMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{

		DataModelAndView modelAndView = systemMngtSvc.findSystemMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	
	@RequestMapping(value="add.html",method={RequestMethod.POST})
	public DataModelAndView addSystemMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		
		DataModelAndView modelAndView = systemMngtSvc.addSystemMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	@RequestMapping(value="addView.html", method={RequestMethod.POST})
	public DataModelAndView findAddView(@RequestParam Map<String,String> parameters, HttpServletRequest request) throws Exception{
		
		DataModelAndView modelAndView = systemMngtSvc.addView(parameters);
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName("system_menu_add");
		
		return modelAndView;
	}

	
	@RequestMapping(value="save.html",method={RequestMethod.POST})
	public DataModelAndView saveSystemMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
			
		parameters.put("insert_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		DataModelAndView modelAndView = systemMngtSvc.saveSystemMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		modelAndView.addObject("sel_system_seq", parameters.get("sel_system_seq"));

		return modelAndView;
	}
	

	/**
	 * 부서 삭제 (json)
	 * 
	 * @author ehchoi
	 * @since 2016. 4. 18.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="remove.html",method={RequestMethod.POST})
	public DataModelAndView removeSystemMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
	
		
		DataModelAndView modelAndView = systemMngtSvc.removeSystemMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	@MngtActHist(log_action = "UPDATE", log_message = "[정책관리] 시스템관리", l_category = "정책관리>시스템관리")
	@RequestMapping(value={"saveIdentiInfo.html"}, method={RequestMethod.POST})
	@ResponseBody
	public String saveIdentiInfo(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		String result = systemMngtSvc.saveIdentiInfo(parameters);
		return result;
	}
}

