package com.easycerti.eframe.psm.system_management.web;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.system_management.service.SummaryMngtSvc;

@Controller
@RequestMapping(value="/summaryMngt/*")
public class SummaryMngtCtrl {
	
	@Autowired
	private SummaryMngtSvc summaryMngtSvc;
	
	@MngtActHist(log_action = "SELECT", log_message = "[환경관리] 통계마감관리")
	@RequestMapping(value="list.html")
	public DataModelAndView summaryMngtList() {
		
		DataModelAndView modelAndView = summaryMngtSvc.summaryMngtList(); 
		modelAndView.setViewName("summaryMngtList");
		
		return modelAndView;
	}
	
	@MngtActHist(log_action = "SELECT&UPDATE&DELETE", log_message = "[통계마감관리] 생성")
	@RequestMapping(value="createSummary.html",method={RequestMethod.POST})
	public DataModelAndView createSummary(@RequestParam Map<String, String> parameters, 
			@RequestParam("type") String type, @RequestParam("procdate") String procdate) throws Exception{
	
		int cnt = summaryMngtSvc.createSummary(procdate, type);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("procdate", procdate);
		modelAndView.addObject("cnt", cnt);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	} 
	
	@MngtActHist(log_action = "SELECT&UPDATE&DELETE", log_message = "[통계마감관리] 생성")
	@RequestMapping(value="createSummaryAll.html",method={RequestMethod.POST})
	public DataModelAndView createSummaryAll(@RequestParam Map<String, String> parameters, 
			@RequestParam("procdate") String procdate) throws Exception{
	
		int cnt = summaryMngtSvc.createSummaryAll(procdate);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("procdate", procdate);
		modelAndView.addObject("cnt", cnt);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	} 
	
	@MngtActHist(log_action = "SELECT&UPDATE&DELETE", log_message = "[통계마감관리] 생성")
	@RequestMapping(value="createSummaryDaily.html",method={RequestMethod.POST})
	public DataModelAndView createSummaryDaily(@RequestParam Map<String, String> parameters, 
			@RequestParam("procdate") String procdate) throws Exception{
	
		int cnt = summaryMngtSvc.createSummaryDaily(procdate);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("procdate", procdate);
		modelAndView.addObject("cnt", cnt);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	} 
	
	@MngtActHist(log_action = "SELECT&UPDATE&DELETE", log_message = "[통계마감관리] 생성")
	@RequestMapping(value="createSummaryStatistics.html",method={RequestMethod.POST})
	public DataModelAndView createSummaryStatistics(@RequestParam Map<String, String> parameters, 
			@RequestParam("procdate") String procdate) throws Exception{
		
		int cnt = summaryMngtSvc.createSummaryStatistics(procdate);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("procdate", procdate);
		modelAndView.addObject("cnt", cnt);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	} 
	
	@MngtActHist(log_action = "SELECT&UPDATE&DELETE", log_message = "[통계마감관리] 생성")
	@RequestMapping(value="createSummaryReqtype.html",method={RequestMethod.POST})
	public DataModelAndView createSummaryReqtype(@RequestParam Map<String, String> parameters, 
			@RequestParam("procdate") String procdate) throws Exception{
		
		int cnt = summaryMngtSvc.createSummaryReqtype(procdate);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("procdate", procdate);
		modelAndView.addObject("cnt", cnt);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	} 
	
	@MngtActHist(log_action = "SELECT&UPDATE&DELETE", log_message = "[통계마감관리] 생성")
	@RequestMapping(value="createSummaryDownload.html",method={RequestMethod.POST})
	public DataModelAndView createSummaryDownload(@RequestParam Map<String, String> parameters, 
			@RequestParam("procdate") String procdate) throws Exception{
		
		int cnt = summaryMngtSvc.createSummaryDownload(procdate);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("procdate", procdate);
		modelAndView.addObject("cnt", cnt);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	} 
	
	@MngtActHist(log_action = "SELECT&UPDATE&DELETE", log_message = "[통계마감관리] 생성")
	@RequestMapping(value="createSummaryTime.html",method={RequestMethod.POST})
	public DataModelAndView createSummaryTime(@RequestParam Map<String, String> parameters, 
			@RequestParam("procdate") String procdate) throws Exception{
		
		int cnt = summaryMngtSvc.createSummaryTime(procdate);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("procdate", procdate);
		modelAndView.addObject("cnt", cnt);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	} 
	
	@MngtActHist(log_action = "SELECT&UPDATE&DELETE", log_message = "[통계마감관리] 생성")
	@RequestMapping(value="createSummaryMonth.html",method={RequestMethod.POST})
	public DataModelAndView createSummaryMonth(@RequestParam Map<String, String> parameters) throws Exception{
	
		int cnt = summaryMngtSvc.createSummaryMonth(parameters);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("cnt", cnt);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	} 
	
	/** 해당하는 기간내에 일마감 데이터가 있는지 확인하여 해당 일마감 데이터 row수를 리턴함
	 * 확인하는 기간 범위가 매개변수로 필요하며 startDate endDate 이름으로 
	 * ex) 20200401 20200430 식의 문자열로 전송필요 달의 마지막날은 구지 마지막날을 계산하지 않아도 오류없음
	 */
	@RequestMapping(value="checkSummaryDaily.html",method={RequestMethod.POST})
	@ResponseBody
	public String checkSummaryDaily(@RequestParam Map<String, String> parameters) throws Exception{
		return summaryMngtSvc.checkSummaryDaily(parameters);
	} 
	
	@RequestMapping(value="checkSummaryMonth.html",method={RequestMethod.POST})
	@ResponseBody
	public String checkSummaryMonth(@RequestParam Map<String, String> parameters) throws Exception{
		return summaryMngtSvc.checkSummaryMonth(parameters);
	}
	
	@RequestMapping(value="addInstallDate.html",method={RequestMethod.POST})
	@ResponseBody
	public String addInstallDate(@RequestParam Map<String, String> parameters) {
		return summaryMngtSvc.addInstallDate(parameters);
	}
	
	@ResponseBody
	@RequestMapping(value="createSummaryRule.html",method={RequestMethod.POST})
	public String createSummaryRule(@RequestParam Map<String, String> parameters, 
			@RequestParam("search_from") String search_from, @RequestParam("search_to") String search_to) throws Exception{
		String result="";
		search_from = search_from.replace("-", "");
		search_to = search_to.replace("-", "");
		String temp="/usr/local/PSM/agent/extractor/startup_web.sh "+search_from+" "+search_to;
		Process process;
		try {
			process = Runtime.getRuntime().exec(temp);
			process.waitFor();

			String retunStr = "";
			//실행 후 리턴 exitValue 값이 0이면 성공 , 1은 에러
			if (process.exitValue() == 0) {
				BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
				retunStr = stdInput.readLine();
				System.out.println(retunStr);
				/*boolean checkVal = summaryMngtSvc.extractorLogCheck(procdate);
				if(checkVal) {
					result = "success";
				} else {
					result = "fail";
				}*/
				result = "success";
				stdInput.close();
			} else if (process.exitValue() == 1) {
				BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));
				String s = null;
				while ((s = stdError.readLine()) != null) {
					retunStr = retunStr + s + "\r\n";
				}
				result = "fail";
				stdError.close();
			}
			process.destroy();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("procdate", procdate);
		modelAndView.addObject("cnt", result);
		modelAndView.setViewName(CommonResource.JSON_VIEW);*/
		
		System.out.println(result);
		return result;
	}
	@MngtActHist(log_action = "SELECT&UPDATE&DELETE", log_message = "[통계마감관리] 생성")
	@RequestMapping(value="createSummaryAbnormal.html",method={RequestMethod.POST})
	public DataModelAndView createSummaryAbnormal(@RequestParam Map<String, String> parameters, 
			@RequestParam("procdate") String procdate) throws Exception{
		
		int cnt = summaryMngtSvc.createSummaryAbnormal(procdate);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("procdate", procdate);
		modelAndView.addObject("cnt", cnt);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
}
