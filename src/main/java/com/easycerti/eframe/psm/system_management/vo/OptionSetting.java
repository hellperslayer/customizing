package com.easycerti.eframe.psm.system_management.vo;

import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 옵션설정 VO
 */

public class OptionSetting extends AbstractValueObject {

	private String option_id;
	private String option_name;
	private String description;
	private String value;
	private String flag_name;
	private String auth;
	
	private String option_type;
	private Integer option_type_ct = 0;
	
	
	public Integer getOption_type_ct() {
		return option_type_ct;
	}
	public void setOption_type_ct(Integer option_type_ct) {
		this.option_type_ct = option_type_ct;
	}
	public String getOption_type() {
		return option_type;
	}
	public void setOption_type(String option_type) {
		this.option_type = option_type;
	}
	public String getOption_id() {
		return option_id;
	}
	public void setOption_id(String option_id) {
		this.option_id = option_id;
	}
	public String getOption_name() {
		return option_name;
	}
	public void setOption_name(String option_name) {
		this.option_name = option_name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getFlag_name() {
		return flag_name;
	}
	public void setFlag_name(String flag_name) {
		this.flag_name = flag_name;
	}
	public String getAuth() {
		return auth;
	}
	public void setAuth(String auth) {
		this.auth = auth;
	}
	
}
