package com.easycerti.eframe.psm.system_management.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 권한 리스트 VO
 * 
 * @author yjyoo
 * @since 2015. 4. 20.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 20.           yjyoo            최초 생성
 *
 * </pre>
 */
public class AuthList extends AbstractValueObject {
	
	// 권한 리스트
	private List<Auth> auths = null;
	
	public AuthList() {}
	
	public AuthList(List<Auth> auths){
		this.auths = auths;
	}
	
	public AuthList(List<Auth> auths, String count){
		this.auths = auths;
		setPage_total_count(count);
	}

	public List<Auth> getAuths() {
		return auths;
	}

	public void setAuths(List<Auth> auths) {
		this.auths = auths;
	}
	
	
}
