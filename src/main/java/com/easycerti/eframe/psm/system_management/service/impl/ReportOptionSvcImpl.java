package com.easycerti.eframe.psm.system_management.service.impl;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.web.multipart.MultipartFile;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.StringUtils;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.system_management.dao.AuthMngtDao;
import com.easycerti.eframe.psm.system_management.dao.CodeMngtDao;
import com.easycerti.eframe.psm.system_management.dao.ReportOptionDao;
import com.easycerti.eframe.psm.system_management.service.ReportOptionSvc;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;
import com.easycerti.eframe.psm.system_management.vo.Auth;
import com.easycerti.eframe.psm.system_management.vo.LogoImage;
import com.easycerti.eframe.psm.system_management.vo.ReportApproval;
import com.easycerti.eframe.psm.system_management.vo.ReportOption;

@Service
public class ReportOptionSvcImpl implements ReportOptionSvc {

	@Autowired
	private CommonDao commonDao;

	@Autowired
	private ReportOptionDao reportOptionDao;

	@Autowired
	private CodeMngtDao codeMngtDao;

	@Autowired
	private AuthMngtDao authMngtDao;

	@Autowired
	private DataSourceTransactionManager transactionManager;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public DataModelAndView reportOptionList(Map<String, String> parameters, HttpServletRequest request) {

		DataModelAndView modelAndView = new DataModelAndView();

		SimpleCode simpleCode = new SimpleCode("/reportOption/list.html");
		AdminUser adminUser = SystemHelper.getAdminUserInfo(request);
		String auth_id = adminUser.getAuth_id();
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);

		if (parameters.get("tab_flag") == null)
			modelAndView.addObject("tab_flag", 1);
		else
			modelAndView.addObject("tab_flag", parameters.get("tab_flag"));

		// 일반사항
		List<ReportOption> general = reportOptionDao.reprotOption_GeneralList();

		for (int i = 0; i < general.size(); i++) {
			String option_id = general.get(i).getOption_id();
			String value_status = general.get(i).getValue();
			if (option_id.equals("use_systemApproval")) {
				if (value_status.equals("Y")) {
					modelAndView.addObject("value_status", "사용");
				}
			}
		}

		List<Auth> authList = reportOptionDao.findAuthMngtList(new Auth());
		Map<String, Auth> authMap = new HashMap<String, Auth>();
		for (Auth auth2 : authList) {
			authMap.put(auth2.getAuth_id(), auth2);
		}

		// 로고관리
		List<LogoImage> image_logo_list = commonDao.getLogoImageList();
		List<LogoImage> image_logo_list2 = commonDao.getLogoImageList2();

		// 결재라인관리
		Map<String, String> authorizeLine = reportOptionDao.getAuthorizeLine();
		Auth auth = new Auth();

		List<ReportApproval> systemList = new ArrayList<ReportApproval>();
		// 보고서옵션 시스템리스트
		int appCt = reportOptionDao.findApprovalListCt();
		if (appCt == 0) {
			systemList = reportOptionDao.findSystemList();
		}
		// 보고서옵션 시스템리스트 없을경우 insert
		for (ReportApproval sys : systemList) {
			ReportApproval app = new ReportApproval();
			app.setSystem_seq(sys.getSystem_seq());
			app.setSystem_name(sys.getSystem_name());
			reportOptionDao.setApprovalList(app);
		}
		
		//보고서 결재자 태그 insert
		updReportApproval();
		
		// 권한체크
		Auth authBean = new Auth();
		if (StringUtils.notNullCheck(auth_id)) {
			auth.setAuth_id(auth_id);
			authBean = authMngtDao.findAuthMngtOne(auth);
		}

		// 결재라인관리 - 점검보고서 결재라인
		List<ReportApproval> reportApproval = new ArrayList<ReportApproval>();
		if (authBean.getSys_auth_ids() != null) {
			String[] sys_auth_ids = authBean.getSys_auth_ids().split(",");
			for (String sys : sys_auth_ids) {
				ReportApproval app = new ReportApproval();
				app.setSystem_seq(sys);
				app = reportOptionDao.findApprovalList2(app);
				reportApproval.add(app);
			}
		}

		// 총평관리
		Code code = new Code();
		code.setGroup_code_id("REPORT_TYPE");
		code.setUse_flag("Y");
		List<Code> reportcode = codeMngtDao.findCodeMngtList(code);

		modelAndView.addObject("authMap", authMap);
		modelAndView.addObject("general", general);
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("reportCode", reportcode);
		modelAndView.addObject("image_logo_list", image_logo_list);
		modelAndView.addObject("image_logo_list2", image_logo_list2);
		modelAndView.addObject("authorizeLine", authorizeLine);
		modelAndView.addObject("reportApproval", reportApproval);

		return modelAndView;

	}

	@Override
	public DataModelAndView updGeneral(Map<String, String> parameters) {

		List<ReportOption> authList = new ArrayList<ReportOption>();

		for (String option_id : parameters.keySet()) {
			ReportOption dummy = new ReportOption();
			if (option_id.indexOf("admin_auth_") > -1) {
				dummy.setOption_id(option_id);
				dummy.setValue(parameters.get(option_id));
				authList.add(dummy);
			}
		}
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			for (String option_id : parameters.keySet()) {

				String val = parameters.get(option_id);

				ReportOption reportOption = new ReportOption();
				reportOption.setOption_id(option_id);
				reportOption.setValue(val);

				for (ReportOption a : authList) {
					if (a.getOption_id().substring(11).equals(option_id)) {
						reportOption.setAuth(a.getValue());
					}
					reportOptionDao.updGeneral(reportOption);
				}
			}
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[ReportOptionSvcImpl.setReportOption] MESSAGE : " + e.getMessage());
			throw new ESException("SYS035J");
		}

		return null;

	}

	@Override
	public DataModelAndView addFile(MultipartFile file, HttpServletRequest request) {

		DataModelAndView modelAndView = new DataModelAndView();
		String rootPath = request.getSession().getServletContext().getRealPath("/");
		String attach_path = "resources/upload/";
		String filename = UUID.randomUUID().toString().replaceAll("-", "") + file.getOriginalFilename();

		String savePath = rootPath + attach_path;
//		System.out.println(savePath);

		File fi = new File(savePath);
		if (!fi.exists())
			fi.mkdirs();

		fi = new File(savePath + filename);
		try {
			file.transferTo(fi);
			commonDao.initImageLogo_use();

			HashMap<Object, Object> map = new HashMap<>();

			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			map.put("reg_date", sdf.format(new Date()));
			map.put("stored_name", filename);
			map.put("origin_name", file.getOriginalFilename());
			commonDao.insertImage_logo(map);
		} catch (IOException e) {

			e.printStackTrace();
		}
		return modelAndView;
	}

	@Override
	public DataModelAndView addFile2(MultipartFile file, HttpServletRequest request) {

		DataModelAndView modelAndView = new DataModelAndView();
		String rootPath = request.getSession().getServletContext().getRealPath("/");
		String attach_path = "resources/upload/";
		String filename = UUID.randomUUID().toString().replaceAll("-", "") + file.getOriginalFilename();

		String savePath = rootPath + attach_path;
		// System.out.println(savePath);

		File fi = new File(savePath);
		if (!fi.exists())
			fi.mkdirs();

		fi = new File(savePath + filename);

		try {
			file.transferTo(fi);
			commonDao.initImageLogo_use2();

			HashMap<Object, Object> map = new HashMap<>();

			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			map.put("reg_date", sdf.format(new Date()));
			map.put("stored_name", filename);
			map.put("origin_name", file.getOriginalFilename());
			commonDao.insertImage_logo2(map);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return modelAndView;
	}

	@Override
	public void useLogoImage(int idx, int type) {

		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			if (type == 1) {
				commonDao.initImageLogo_use();
			} else if (type == 2) {
				commonDao.initImageLogo_use();
				commonDao.useLogImage(idx);
			}
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			logger.error("[ReportOptionSvcImpl.useLogoImage] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
	}

	@Override
	public void useLogoImage2(int idx, int type) {

		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			if (type == 1) {
				commonDao.initImageLogo_use2();
			} else if (type == 2) {
				commonDao.initImageLogo_use2();
				commonDao.useLogImage2(idx);
			}
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			logger.error("[CustomerInfoSvcImpl.useLogoImage] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
	}

	@Override
	public void deleteLogoImage(int idx) {

		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			commonDao.deleteLogoImage(idx);
			transactionManager.commit(transactionStatus);
		} catch (DataAccessException e) {
			logger.error("[ReportOptionSvcImpl.deleteLogoImage] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
	}

	@Override
	public String addAuthorizeLine(Map<String, String> parameters) {
		String result = "success";
		try {
			reportOptionDao.addAuthorizeLine(parameters);
			updReportApproval();
		} catch (Exception e) {
			e.printStackTrace();
			result = "fail";
		}
		return result;
	}

	@Override
	public String addDefaultDesc(Map<String, String> parameters) {
		String result = "success";
		try {
			reportOptionDao.addDefaultDesc(parameters);
		} catch (Exception e) {
			e.printStackTrace();
			result = "fail";
		}
		return result;
	}

	@Override
	public String findDefaultDesc(Map<String, String> parameters) {
		String result = reportOptionDao.findDefaultDesc(parameters);
		if (result == null) {
			result = "";
		}
		return result;
	}

	@Override
	public String updReportApproval(Map<String, String> parameters) {

		for (String param : parameters.keySet()) {
			if (param.equals("system_seq")) {
				if (parameters.get(param).length() == 1) {
					parameters.replace(param, "0" + parameters.get(param));
				}
			}
		}

		String result = "success";
		try {

			reportOptionDao.updReportApproval(parameters);
		} catch (Exception e) {
			e.printStackTrace();
			result = "fail";
		}
		return result;
	}

	@Override
	public DataModelAndView wholeUpdReportApproval(Map<String, String> parameters) {

		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);

		try {
			Iterator it = parameters.entrySet().iterator();
			ReportApproval app = new ReportApproval();

			int itCK = 1;
			while (it.hasNext()) {

				Map.Entry entry = (Map.Entry) it.next();

				if (itCK == 1) {
					app.setSystem_seq(entry.getValue().toString());
					itCK++;
				} else if (itCK == 2) {
					app.setFirst_auth(entry.getValue().toString());
					itCK++;
				} else if (itCK == 3) {
					app.setSecond_auth(entry.getValue().toString());
					itCK++;
				} else if (itCK == 4) {
					app.setThird_auth(entry.getValue().toString());

					reportOptionDao.wholeUpdReportApproval(app);

					itCK = 1;
				}
			}

			transactionManager.commit(transactionStatus);

		} catch (DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[ReportOptionSvcImpl.wholeUpdReportApproval] MESSAGE : " + e.getMessage());
			throw new ESException("SYS035J");
		}
		return null;
	}

	@Override
	public void updReportApproval() {
		ReportApproval reportApproval = reportOptionDao.findAuthorize();
		String count = reportApproval.getCount();
		String name1 = reportApproval.getName1();
		String name2 = reportApproval.getName2();
		String name3 = reportApproval.getName3();

		String html_source = "<table border=\"1\" id=\"preview\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse:collapse; border: 1px solid black\"><tbody><tr id=\"authorNameTr\"><td rowspan=\"2\" id=\"authorNameFirst\" valign=\"middle\" style=\"width: 27px; height: 114px; padding: 1.4pt 5.1pt 1.4pt 6pt;\"><p><span>결</span></p><p style=\"margin: 0px\"><span>재</span></p></td>";
		List<ReportApproval> appList = reportOptionDao.findApprovalList();

		if (count.equals("0")) {
			ReportApproval reportApp = new ReportApproval();
			html_source = "<table border=\"1\" id=\"preview\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse: collapse; border: 1px solid black; display: none;\"><tbody><tr id=\"authorNameTr\"><td rowspan=\"2\" id=\"authorNameFirst\" valign=\"middle\" style=\"width: 27px; height: 114px; padding: 1.4pt 5.1pt 1.4pt 6pt;\"><p><span>결</span></p><p style=\"margin: 0px\"><span>재</span></p></td></tr><tr id=\"authorMarkTr\"></tr></tbody></table>";
			for (ReportApproval list : appList) {
				reportApp.setHtml_source(html_source);
				reportApp.setSystem_seq(list.getSystem_seq());
				reportOptionDao.updReportApproval2(reportApp);
			}
		} else if (count.equals("1")) {
			ReportApproval reportApp = new ReportApproval();
			html_source += "<td valign=\"middle\" class=\"authorNameTd\"><p style=\"margin: 0px; text-align: center;\"><span>"
					+ name1 + "</span></p></td></tr>";
			String html_source_sam = html_source;
			for (ReportApproval list : appList) {
				html_source = html_source_sam;
				String seq = list.getSystem_seq();
				reportApp.setSystem_seq(seq);
				String first = list.getFirst_auth();
				if (first == null) {
					first = "&nbsp;";
				}
				html_source += "<tr id=\"authorMarkTr\"><td valign=\"middle\" class=\"authorMarkTd\" style=\"width: 91px; height: 79px; padding: 1.4pt 5.1pt 1.4pt 5.1pt;\"><p style=\"margin: 0px; text-align: center;\"><span>"
						+ first + "</span></p></td></tr></tbody></table>";
				reportApp.setHtml_source(html_source);
				reportOptionDao.updReportApproval2(reportApp);
			}
		} else if (count.equals("2")) {
			ReportApproval reportApp = new ReportApproval();
			html_source += "<td valign=\"middle\" class=\"authorNameTd\"><p style=\"margin: 0px; text-align: center;\"><span>"
					+ name1
					+ "</span></p></td><td valign=\"middle\" class=\"authorNameTd\"><p style=\"margin: 0px; text-align: center;\"><span>"
					+ name2 + "</span></p></td></tr>";
			String html_source_sam = html_source;
			for (ReportApproval list : appList) {
				html_source = html_source_sam;
				String seq = list.getSystem_seq();
				reportApp.setSystem_seq(seq);
				String first = list.getFirst_auth();
				String second = list.getSecond_auth();
				if (first == null || second == null) {
					first = "&nbsp;";
					second = "&nbsp;";
				}
				html_source += "<tr id=\"authorMarkTr\"><td valign=\"middle\" class=\"authorMarkTd\" style=\"width: 91px; height: 79px; padding: 1.4pt 5.1pt 1.4pt 5.1pt;\"><p style=\"margin: 0px; text-align: center;\"><span>"
						+ first
						+ "</span></p></td><td valign=\"middle\" class=\"authorMarkTd\" style=\"width: 91px; height: 79px; padding: 1.4pt 5.1pt 1.4pt 5.1pt;\"><p style=\"margin: 0px; text-align: center;\"><span>"
						+ second + "</span></p></td></tr></tbody></table>";
				reportApp.setHtml_source(html_source);
				reportOptionDao.updReportApproval2(reportApp);
			}
		} else if (count.equals("3")) {
			ReportApproval reportApp = new ReportApproval();
			html_source += "<td valign=\"middle\" class=\"authorNameTd\"><p style=\"margin: 0px; text-align: center;\"><span>"
					+ name1
					+ "</span></p></td><td valign=\"middle\" class=\"authorNameTd\"><p style=\"margin: 0px; text-align: center;\"><span>"
					+ name2
					+ "</span></p></td><td valign=\"middle\" class=\"authorNameTd\"><p style=\"margin: 0px; text-align: center;\"><span>"
					+ name3 + "</span></p></td></tr>";
			String html_source_sam = html_source;
			for (ReportApproval list : appList) {
				html_source = html_source_sam;
				String seq = list.getSystem_seq();
				reportApp.setSystem_seq(seq);
				String first = list.getFirst_auth();
				String second = list.getSecond_auth();
				String third = list.getThird_auth();
				if (first == null || second == null || third == null) {
					first = "&nbsp;";
					second = "&nbsp;";
					third = "&nbsp;";
				}
				html_source += "<tr id=\"authorMarkTr\"><td valign=\"middle\" class=\"authorMarkTd\" style=\"width: 91px; height: 79px; padding: 1.4pt 5.1pt 1.4pt 5.1pt;\"><p style=\"margin: 0px; text-align: center;\"><span>"
						+ first
						+ "</span></p></td><td valign=\"middle\" class=\"authorMarkTd\" style=\"width: 91px; height: 79px; padding: 1.4pt 5.1pt 1.4pt 5.1pt;\"><p style=\"margin: 0px; text-align: center;\"><span>"
						+ second
						+ "</span></p></td><td valign=\"middle\" class=\"authorMarkTd\" style=\"width: 91px; height: 79px; padding: 1.4pt 5.1pt 1.4pt 5.1pt;\"><p style=\"margin: 0px; text-align: center;\"><span>"
						+ third + "</span></p></td></tr></tbody></table>";
				reportApp.setHtml_source(html_source);
				reportOptionDao.updReportApproval2(reportApp);
			}
		}
	}

}
