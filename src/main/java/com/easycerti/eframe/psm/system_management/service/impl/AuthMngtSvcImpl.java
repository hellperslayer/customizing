package com.easycerti.eframe.psm.system_management.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.StringUtils;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.system_management.dao.AdminUserMngtDao;
import com.easycerti.eframe.psm.system_management.dao.AuthMngtDao;
import com.easycerti.eframe.psm.system_management.dao.CodeMngtDao;
import com.easycerti.eframe.psm.system_management.dao.MenuAuthMngtDao;
import com.easycerti.eframe.psm.system_management.dao.SystemMngtDao;
import com.easycerti.eframe.psm.system_management.service.AuthMngtSvc;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;
import com.easycerti.eframe.psm.system_management.vo.Auth;
import com.easycerti.eframe.psm.system_management.vo.AuthList;
import com.easycerti.eframe.psm.system_management.vo.MenuAuth;
import com.easycerti.eframe.psm.system_management.vo.System;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

/**
 * 권한 관련 Service Implements
 * 
 * @author yjyoo
 * @since 2015. 4. 20.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 20.           yjyoo            최초 생성
 *
 * </pre>
 */
@Service
public class AuthMngtSvcImpl implements AuthMngtSvc {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private AuthMngtDao authMngtDao;
	
	@Autowired
	private MenuAuthMngtDao menuAuthMngtDao;
	
	@Autowired
	private AdminUserMngtDao adminUserMngtDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private SystemMngtDao systemMngtDao;
	
	@Autowired
	private CodeMngtDao codeMngtDao;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;
	
	@Override
	public DataModelAndView findAuthMngtList(Map<String, String> parameters) {
		parameters = CommonHelper.setPageParam(parameters, defaultPageSize);
		Auth paramBean = CommonHelper.convertMapToBean(parameters, Auth.class);
		
		List<Auth> auths = authMngtDao.findAuthMngtList(paramBean);
		Integer totalCount = authMngtDao.findAuthMngtOne_count(paramBean);
		AuthList authListBean = new AuthList(auths, totalCount.toString());
		
		SimpleCode simpleCode = new SimpleCode("/authMngt/list.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("authList", authListBean);
		modelAndView.addObject("paramBean", paramBean);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	@Override
	public void findAuthMngtList_download(DataModelAndView modelAndView, Map<String, String> parameters, HttpServletRequest request) {
		Auth paramBean = CommonHelper.convertMapToBean(parameters, Auth.class);
		
		String fileName = "PSM_권한관리_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "권한관리";
		
		List<Auth> auths = authMngtDao.findAuthMngtList(paramBean);
		
		for(Auth auth : auths) {
			String use_flag = auth.getUse_flag();
			auth.setUse_flag(use_flag.equals("Y") ? "사용" : "미사용");
		}
		
		String[] columns = new String[] {"auth_id", "auth_name", "use_flag"};
		String[] heads = new String[] {"권한ID", "권한명", "사용여부"};
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", auths);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}

	@Override
	public DataModelAndView findAuthMngtOne(Map<String, String> parameters) {
		Auth paramBean = CommonHelper.convertMapToBean(parameters, Auth.class);
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		if(StringUtils.notNullCheck(paramBean.getAuth_id())) {
			Auth authBean = authMngtDao.findAuthMngtOne(paramBean);
			modelAndView.addObject("authDetail", authBean);
			//접근 가능한 시스템, 보고서 출력, 생성가능보고서 목록 내용이 ,를 구분자로 둔 한줄 데이터로 받음
			//,로 스플릿 하여 맵으로 담아줌	
			//체크박스에 확인 체크를 위한 자원
			
			//접근가능 시스템
			if(authBean.getSys_auth_ids()!=null) {
				String[] sys_auth_ids = authBean.getSys_auth_ids().split(",");
				Map<String, String> sysAuthMap = new HashMap<String, String>();
				for (String sys : sys_auth_ids) {
					sysAuthMap.put(sys,"true");
				}
				modelAndView.addObject("sysAuthMap", sysAuthMap);
			}
			//출력가능 보고서
			if(authBean.getSys_report_ids()!=null) {
				String[] sys_report_ids = authBean.getSys_report_ids().split(",");
				Map<String, String> sysReportMap = new HashMap<String, String>();
				for (String sys : sys_report_ids) {
					sysReportMap.put(sys,"true");
				}
				modelAndView.addObject("sysReportMap", sysReportMap);
			}
			//생성가능 보고서
			if(authBean.getMake_report_ids()!=null) {
				String[] make_report_ids = authBean.getMake_report_ids().split(",");
				Map<String, String> makeReportMap = new HashMap<String, String>();
				for (String sys : make_report_ids) {
					makeReportMap.put(sys,"true");
				}
				modelAndView.addObject("makeReportMap", makeReportMap);
			}
		}
		
		//시스템 전체 목록
		System system = new System();
		List<SystemMaster> systems = adminUserMngtDao.getSystemMaster();
		//List<System> systems = systemMngtDao.findSystemMngtList(system);
		modelAndView.addObject("systems", systems);
		
		//보고서 전체 목록
		Code code = new Code();
		code.setGroup_code_id("REPORT_TYPE");
		code.setUse_flag("Y");
		List<Code> reportcode = codeMngtDao.findCodeMngtList(code);
		modelAndView.addObject("reportcode", reportcode);
		
		//해당권한을 가진 유저 목록
		AdminUser adminUser = new AdminUser();
		adminUser.setAuth_id(paramBean.getAuth_id());
		List<AdminUser> users = adminUserMngtDao.findAdminUserMngtList_fromAuthId(adminUser);
		modelAndView.addObject("users",users);
		
		SimpleCode simpleCode = new SimpleCode("/authMngt/detail.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("systems", systems);
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}

	@Override
	public DataModelAndView addAuthMngtOne(Map<String, String> parameters) {
		Auth paramBean = CommonHelper.convertMapToBean(parameters, Auth.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			authMngtDao.addAuthMngtOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[AuthMngtSvcImpl.addAuthMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS030J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView saveAuthMngtOne(Map<String, String> parameters) {
		Auth paramBean = CommonHelper.convertMapToBean(parameters, Auth.class);
		String[] reportAuthUser = parameters.get("report_auth_user").split(",");
		String[] notReportAuthUser = parameters.get("not_report_auth_user").split(",");
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			//보고서 생성권한 부여
			authMngtDao.saveAuthMngtOne(paramBean);
			/*
			if(reportAuthUser.length>0) {
				AdminUser adminUser = new AdminUser();
				adminUser.setAdmin_user_id_arr(reportAuthUser);
				adminUser.setMake_report_auth("Y");
				adminUser.setAuth_ids(paramBean.getSys_auth_ids());
				adminUserMngtDao.saveAdminUserMngtList(adminUser);
			}
			//보고서 생성권한 회수
			if(notReportAuthUser.length>0) {
				AdminUser adminUser = new AdminUser();
				adminUser.setAdmin_user_id_arr(notReportAuthUser);
				adminUser.setMake_report_auth("N");
				adminUser.setAuth_ids(paramBean.getSys_auth_ids());
				adminUserMngtDao.saveAdminUserMngtList(adminUser);
			}*/
			
			AdminUser adminUser = new AdminUser();
			adminUser.setAuth_id(paramBean.getAuth_id());
			adminUser.setAuth_ids(paramBean.getSys_auth_ids());
			String sub_batch = parameters.get("sub_batch");
			if(sub_batch!= null && sub_batch.equals("on")) {
				adminUserMngtDao.saveAdminUserMngtList(adminUser);
			}else {
				
				adminUserMngtDao.saveAdminUserMngtList_except(adminUser);
			}
			
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[AuthMngtSvcImpl.saveAuthMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS031J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
	
		return modelAndView;
	}

	@Override
	public DataModelAndView removeAuthMngtOne(Map<String, String> parameters) {
		Auth paramBean = CommonHelper.convertMapToBean(parameters, Auth.class);
		
		MenuAuth menuAuthParamBean = new MenuAuth();
		menuAuthParamBean.setAuth_id(paramBean.getAuth_id());
		
		AdminUser adminUserParamBean = new AdminUser();
		adminUserParamBean.setAuth_id(paramBean.getAuth_id());
//		adminUserParamBean.setDel_flag('N');
//		List<AdminUser> adminUserList = adminUserMngtDao.findAdminUserMngtList_fromAuthId(adminUserParamBean);
//		
//		// 해당 권한을 가지고 있는 관리자가 있을 경우 삭제할 수 없음.
//		if(adminUserList.size() > 0) {
//			throw new ESException("SYS055J");
//		}
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			// 권한 삭제시 해당 메뉴권한 삭제, 관리자권한 NULL 처리
			menuAuthMngtDao.removeMenuAuthMngtOne(menuAuthParamBean);
			adminUserMngtDao.saveAdminUserMngtOne_toRemoveAuth(adminUserParamBean);
			authMngtDao.removeAuthMngtOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[AuthMngtSvcImpl.removeAuthMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS032J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}

}
