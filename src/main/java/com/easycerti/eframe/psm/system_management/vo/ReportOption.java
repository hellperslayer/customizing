package com.easycerti.eframe.psm.system_management.vo;

import com.easycerti.eframe.common.vo.AbstractValueObject;

public class ReportOption extends AbstractValueObject{
	
	//use_reportLine
	private String option_id;
	
	private String option_name;
	private String description;
	
	//기본셋팅 상태
	private String value;
	
	//사용여부
	private String flag_name;
	
	//시스템별 인증
	private String auth;
	
	private String auth_name;

	
	
	
	
	public String getOption_id() {
		return option_id;
	}
	
	public void setOption_id(String option_id) {
		this.option_id = option_id;
	}
	
	public String getOption_name() {
		return option_name;
	}
	
	public void setOption_name(String option_name) {
		this.option_name = option_name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	public String getFlag_name() {
		return flag_name;
	}
	
	public void setFlag_name(String flag_name) {
		this.flag_name = flag_name;
	}
	
	public String getAuth() {
		return auth;
	}
	
	public void setAuth(String auth) {
		this.auth = auth;
	}

	public String getAuth_name() {
		return auth_name;
	}

	public void setAuth_name(String auth_name) {
		this.auth_name = auth_name;
	}
	
	

	
	
}
