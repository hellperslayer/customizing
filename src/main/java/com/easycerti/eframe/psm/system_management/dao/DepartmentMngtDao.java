package com.easycerti.eframe.psm.system_management.dao;

import java.util.List;

import com.easycerti.eframe.psm.system_management.vo.Department;
import com.easycerti.eframe.psm.system_management.vo.EmpUserSearch;

/**
 * 부서 관리 Dao Interface
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 *   
 *
 * </pre>
 */
public interface DepartmentMngtDao {
	
	/**
	 * 부서 리스트 가져오기
	 */
	public List<Department> findDepartmentMngtList(Department department);
	public List<Department> findDepartmentList(EmpUserSearch search);
	
	/**
	 * 부서 리스트 가져오기 (선택된 부서만)
	 */
	public List<Department> findDepartmentMngtList_choose(Department department);
	
	/**
	 * 부서 리스트 가져오기2 (부서등록을 위한)
	 */
	public List<Department> department_option();
	
	/**
	 * 부서 리스트 갯수 가져오기
	 */
	public Integer findDepartmentMngtOne_count(Department department);
	public Integer findDepartmentList_count(EmpUserSearch search);
	
	/**
	 * 부서 정보 가져오기
	 */
	public Department findDepartmentMngtOne(Department department);
	
	/**
	 * 최상위 부서 ID 가져오기
	 */
	public String findDepartmentMngtOne_root();
	
	/**
	 * 부서 추가하기 (단건)
	 */
	public void addDepartmentMngtOne(Department department);
	
	/**
	 * 부서 수정하기 (단건)
	 */
	public void saveDepartmentMngtOne(Department department);
	
	/**
	 * 부서 삭제하기 (단건)
	 */
	public void removeDepartmentMngtOne(Department department);
	
	public Department findDepartmentByname(String dept_name);
	public List<Department> findDepartmentMngtDepth();
}
