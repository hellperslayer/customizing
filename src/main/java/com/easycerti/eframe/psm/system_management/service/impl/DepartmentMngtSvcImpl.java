package com.easycerti.eframe.psm.system_management.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.StringUtils;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.system_management.dao.AdminUserMngtDao;
import com.easycerti.eframe.psm.system_management.dao.DepartmentMngtDao;
import com.easycerti.eframe.psm.system_management.dao.EmpUserMngtDao;
import com.easycerti.eframe.psm.system_management.service.DepartmentMngtSvc;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;
import com.easycerti.eframe.psm.system_management.vo.Department;
import com.easycerti.eframe.psm.system_management.vo.EmpUser;

/**
 * 부서 관련 Service Implements
 * 
 * @author yjyoo
 * @since 2015. 4. 17.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 17.           yjyoo            최초 생성
 *
 * </pre>
 */
@Service
public class DepartmentMngtSvcImpl implements DepartmentMngtSvc {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private DepartmentMngtDao departmentMngtDao;
	
	@Autowired
	private AdminUserMngtDao adminUserMngtDao;
	
	@Autowired
	private EmpUserMngtDao empUserMngtDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	@Override
	public DataModelAndView findDepartmentMngtList(Map<String, String> parameters) {
		Department paramBean = CommonHelper.convertMapToBean(parameters, Department.class);
		
		List<Department> departments = departmentMngtDao.findDepartmentMngtList(paramBean);
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("hierarchyDepartments", departments);
		modelAndView.addObject("paramBean", paramBean);
		
		SimpleCode simpleCode = new SimpleCode("/departmentMngt/list.html");
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView findDepartmentMngtOne(Map<String, String> parameters) {
		Department paramBean = CommonHelper.convertMapToBean(parameters, Department.class);
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		if(StringUtils.notNullCheck(paramBean.getDept_id())){
			String rootDept_id = null;
			if("ROOT".equals(paramBean.getDept_id())) {
				rootDept_id = departmentMngtDao.findDepartmentMngtOne_root();
				paramBean.setDept_id(rootDept_id);
			}
			
			Department detailBean = departmentMngtDao.findDepartmentMngtOne(paramBean);
			modelAndView.addObject("departmentDetail", detailBean);
		}
		
		modelAndView.addObject("paramBean",paramBean);
		return modelAndView;
	}

	@Override
	public DataModelAndView addDepartmentMngtOne(Map<String, String> parameters) {
		Department paramBean = CommonHelper.convertMapToBean(parameters, Department.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			departmentMngtDao.addDepartmentMngtOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[DepartmentMngtSvcImpl.addDepartmentMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS033J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView saveDepartmentMngtOne(Map<String, String> parameters) {
		Department paramBean = CommonHelper.convertMapToBean(parameters, Department.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			departmentMngtDao.saveDepartmentMngtOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[DepartmentMngtSvcImpl.saveDepartmentMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS034J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView removeDepartmentMngtOne(Map<String, String> parameters) {
		Department paramBean = CommonHelper.convertMapToBean(parameters, Department.class);
		
		Department departmentParamBean = new Department();
		departmentParamBean.setParent_dept_id(paramBean.getDept_id());
		// 하위 부서 존재 시 삭제할 수 없음
		int childDeptCount = departmentMngtDao.findDepartmentMngtOne_count(departmentParamBean);
		if(childDeptCount > 0) {
			throw new ESException("SYS053J");
		}
		
		EmpUser empUserParamBean = new EmpUser();
		empUserParamBean.setDept_id(paramBean.getDept_id());
		// 하위 사용자 존재 시 삭제할 수 없음
		int userCount = empUserMngtDao.findEmpUserMngtOne_countToUseDept(empUserParamBean);
		if(userCount > 0) {
			throw new ESException("SYS051J");
		}
		
		AdminUser adminUserParamBean = new AdminUser();
		adminUserParamBean.setDept_id(paramBean.getDept_id());
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			// 관리자 부서 초기화
			adminUserMngtDao.saveAdminUserMngtOne_toRemoveDept(adminUserParamBean);
			// 부서 삭제
			departmentMngtDao.removeDepartmentMngtOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[DepartmentMngtSvcImpl.removeDepartmentMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS035J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	

	public List<Department> findDepartmentMngtDepth() {
		return departmentMngtDao.findDepartmentMngtDepth();
	}
}
