package com.easycerti.eframe.psm.system_management.web;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.psm.system_management.service.AccessAuthSvc;
import com.easycerti.eframe.psm.system_management.vo.AccessAuthSearch;

@Controller
@RequestMapping(value="/accessAuth/*")
public class AccessAuthCtrl {
	
	@Autowired
	private AccessAuthSvc accessAuthSvc;
	
	@MngtActHist(log_action = "SELECT", log_message = "[접근관리] 관리자접근권한관리")
	@RequestMapping(value="list.html",method={RequestMethod.POST})
	public DataModelAndView findAccessAuthList(@ModelAttribute("search") AccessAuthSearch search, @RequestParam Map<String, String> parameters, HttpServletRequest request){
		Date today = new Date();
		search.initDatesIfEmpty_2week(today, today);
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list != null) {
	    	search.setAuth_idsList(list);
	    }else {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	search.setAuth_idsList(list);
	    }
		DataModelAndView modelAndView = accessAuthSvc.findAccessAuthList(search);

		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("accessAuthList");
		
		return modelAndView;
	}
	
	@MngtActHist(log_action = "SELECT", log_message = "[접근관리] 접근권한관리 상세")
	@RequestMapping(value="detail.html",method={RequestMethod.POST})
	public DataModelAndView findAccessAuthDetail(@ModelAttribute("search") AccessAuthSearch search, @RequestParam Map<String, String> parameters, HttpServletRequest request){
		DataModelAndView modelAndView = accessAuthSvc.findAccessAuthDetail(search);

		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("accessAuthDetail");
		
		return modelAndView;
	}
	
	@RequestMapping(value="add.html", method={RequestMethod.POST})
	public DataModelAndView addAccessAuth(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception {
					
		//parameters.put("insert_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		DataModelAndView modelAndView = accessAuthSvc.addAccessAuth(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@RequestMapping(value="save.html",method={RequestMethod.POST})
	public DataModelAndView saveAccessAuth(@RequestParam Map<String, String> parameters,HttpServletRequest request) throws Exception{
		String[] args = {"emp_user_id", "emp_user_name"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){ //,"password"
			throw new ESException("SYS004J");
		}
		
		DataModelAndView modelAndView = accessAuthSvc.saveAccessAuth(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@RequestMapping(value="remove.html",method={RequestMethod.POST})
	public DataModelAndView removeAccessAuth(@RequestParam Map<String, String> parameters,HttpServletRequest request) throws Exception{
		String[] args = {"seq"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		parameters.put("update_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		DataModelAndView modelAndView = accessAuthSvc.removeAccessAuth(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@RequestMapping(value="download.html", method={RequestMethod.POST})
	public DataModelAndView addAccessAuthList_download(@ModelAttribute("search") AccessAuthSearch search, @RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		accessAuthSvc.findAccessAuthList_download(modelAndView, search, request);
		
		return modelAndView;
	}
	
	@RequestMapping(value="upload.html", method={RequestMethod.POST})
	@ResponseBody
	public String upLoadAccessAuth(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		parameters.put("insert_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		String result = accessAuthSvc.upLoadAccessAuth(parameters,request, response);
		
		return result;
	}
	
	@RequestMapping("exdownload.html")
	public ModelAndView download(HttpServletRequest request)throws Exception{
		
		String path = request.getSession().getServletContext().getRealPath("/")+"WEB-INF/";
		 //System.out.println(path);
		File down = new File(path + "excelExam/접근권한_업로드양식.xlsx");
		return new ModelAndView("download","downloadFile",down);
	}
}
