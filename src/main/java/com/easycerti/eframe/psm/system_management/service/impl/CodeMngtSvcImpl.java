package com.easycerti.eframe.psm.system_management.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.StringUtils;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.control.service.CacheService;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.system_management.dao.CodeMngtDao;
import com.easycerti.eframe.psm.system_management.service.CodeMngtSvc;
import com.easycerti.eframe.psm.system_management.vo.CodeList;

/**
 * 코드 관련 Service Implements 
 * 
 * @author yjyoo
 * @since 2015. 4. 17.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 17.           yjyoo            최초 생성
 *
 * </pre>
 */
@Service
public class CodeMngtSvcImpl implements CodeMngtSvc {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private CodeMngtDao codeMngtDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private CacheService cacheSvc;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	@Override
	public DataModelAndView findCodeMngtList(Map<String, String> parameters) {
		Code paramBean = CommonHelper.convertMapToBean(parameters, Code.class);
		
		List<Code> codes = codeMngtDao.findCodeMngtList(paramBean);
		CodeList codeList = new CodeList(codes);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("codeList", codeList);
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView findCodeMngtOne(Map<String, String> parameters) {
		Code paramBean = CommonHelper.convertMapToBean(parameters, Code.class);
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		if(StringUtils.notNullCheck(paramBean.getCode_id())){ // 상세
			Code codeDetail = codeMngtDao.findCodeMngtOne(paramBean);
			modelAndView.addObject("codeDetail", codeDetail);
		}
		SimpleCode simpleCode = new SimpleCode("/codeMngt/detail.html");
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView addCodeMngtOne(Map<String, String> parameters) {
		Code paramBean = CommonHelper.convertMapToBean(parameters, Code.class);
		
		// Validation Check (group_code & code 중복)
		Code code = codeMngtDao.findCodeMngtOne(paramBean);
		if(code != null) {
			throw new ESException("SYS021J");
		}
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			codeMngtDao.addCodeMngtOne(paramBean);
			transactionManager.commit(transactionStatus);
			cacheSvc.refreshGroupCodes();
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[CodeMngtSvcImpl.addCodeMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS020J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView saveCodeMngtOne(Map<String, String> parameters) throws Exception {
		Code paramBean = CommonHelper.convertMapToBean(parameters, Code.class);
		
		/*Code codeBean = codeMngtDao.findCodeMngtOne(paramBean);
		if(!SystemHelper.enableEditCode(codeBean)){
			throw new ESException("SYS023J");
		}*/
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			codeMngtDao.saveCodeMngtOne(paramBean);
			transactionManager.commit(transactionStatus);
			cacheSvc.refreshGroupCodes();
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[CodeMngtSvcImpl.saveCodeMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS022J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView removeCodeMngtOne(Map<String, String> parameters) throws Exception {
		Code paramBean = CommonHelper.convertMapToBean(parameters, Code.class);
		
		Code codeBean = codeMngtDao.findCodeMngtOne(paramBean);
		if(!SystemHelper.enableEditCode(codeBean)) {
			throw new ESException("SYS025J");
		}
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			codeMngtDao.removeCodeMngtOne(paramBean);
			transactionManager.commit(transactionStatus);
			cacheSvc.refreshGroupCodes();
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[CodeMngtSvcImpl.removeCodeMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS024J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}

}
