package com.easycerti.eframe.psm.system_management.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.AbstractValueObject;

public class IpInfoList extends AbstractValueObject {
	private List<IpInfoBean> ipInfo = null;
	
	public IpInfoList() {}
	
	public IpInfoList(List<IpInfoBean> ipInfo){
		this.ipInfo = ipInfo;
	}
	
	public IpInfoList(List<IpInfoBean> ipInfo, String count){
		this.ipInfo = ipInfo;
		setPage_total_count(count);
	}

	public List<IpInfoBean> getIpInfo() {
		return ipInfo;
	}

	public void setIpInfo(List<IpInfoBean> ipInfo) {
		this.ipInfo = ipInfo;
	}
}
