package com.easycerti.eframe.psm.system_management.dao;

import java.util.List;

import com.easycerti.eframe.psm.system_management.vo.Talarm;
import com.easycerti.eframe.psm.system_management.vo.TalarmSearch;

/**
 * 사원 관련 Dao Interface
 * 
 * @author yjyoo
 * @since 2015. 4. 21.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 21.           yjyoo            최초 생성
 *
 * </pre>
 */
public interface TalarmMngtDao {
	
	/**
	 * 사원 리스트
	 */
	public List<Talarm> findTalarmMngtList(TalarmSearch talarm);

	/**
	 * 사원 리스트 갯수
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 21.
	 * @return Integer
	 */
	public Integer findTalarmMngtOne_count(TalarmSearch talarm);
	
	/**
	 * 사원 상세정보
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 21.
	 * @return Talarm
	 */
	public Talarm findTalarmMngtOne(Talarm talarm);
	
	/**
	 * 사원 추가 전 중복 체크
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 21.
	 * @return Integer
	 */
	public Integer findTalarmMngtOne_validation(Talarm talarm);
	
	/**
	 * 사원 추가 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 21.
	 * @return void
	 */
	public void addTalarmMngtOne(Talarm talarm);
	
	/**
	 * 사원 수정 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 21.
	 * @return void
	 */
	public void saveTalarmMngtOne(Talarm talarm);
	
	public void saveTalarmMngtTwo(Talarm talarm);
	
	/**
	 * 사원 삭제
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 21.
	 * @return void
	 */
	public void removeTalarmMngtOne(Talarm talarm);
	
	/**
	 * 부서 삭제 전 사용중인 사용자가 있는지 확인
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 21.
	 * @return Integer
	 */
	public Integer findTalarmMngtOne_countToUseDept(Talarm talarm);
	
}
