package com.easycerti.eframe.psm.system_management.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.psm.system_management.service.AuthMngtSvc;

/**
 * 권한 관련 Controller
 * 
 * @author yjyoo
 * @since 2015. 4. 20.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 20.           yjyoo           최초 생성
 *   2015. 5. 26.			yjyoo			엑셀 다운로드 추가
 *
 * </pre>
 */
@Controller
@RequestMapping(value="/authMngt/*")
public class AuthMngtCtrl {
	
	@Autowired
	private AuthMngtSvc authMngtSvc;
	
	/**
	 * 권한 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[환경관리] 권한관리")
	@RequestMapping(value="list.html",method={RequestMethod.POST})
	public DataModelAndView findAuthMngtList(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		DataModelAndView modelAndView = authMngtSvc.findAuthMngtList(parameters);
		
		modelAndView.addObject("isSearch", parameters.get("isSearch"));
		modelAndView.setViewName("authMngtList");
		return modelAndView;
	}
	
	/**
	 * 권한 리스트 엑셀 다운로드
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 26.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="download.html",method={RequestMethod.POST})
	public DataModelAndView findAuthMngtList_download(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		authMngtSvc.findAuthMngtList_download(modelAndView, parameters, request);
		
		return modelAndView;
	}
	
	/**
	 * 권한 상세정보
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[환경관리] 권한관리 상세")
	@RequestMapping(value="detail.html",method={RequestMethod.POST})
	public DataModelAndView findAuthMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		DataModelAndView modelAndView = authMngtSvc.findAuthMngtOne(parameters);
		
		modelAndView.setViewName("authMngtDetail");
		return modelAndView;
	}
	
	/**
	 * 권한 추가 (json)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="add.html",method={RequestMethod.POST})
	public DataModelAndView addAuthMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		String[] args = {"auth_name", "use_flag"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		// 세션에서 관리자 ID추출
		parameters.put("insert_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		DataModelAndView modelAndView = authMngtSvc.addAuthMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	/**
	 * 권한 수정 (json)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="save.html",method={RequestMethod.POST})
	public DataModelAndView saveAuthMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		String[] args = {"auth_id", "auth_name", "use_flag"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		// 세션에서 관리자 ID추출
		parameters.put("update_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		DataModelAndView modelAndView = authMngtSvc.saveAuthMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	/**
	 * 권한 삭제 (json)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="remove.html",method={RequestMethod.POST})
	public DataModelAndView removeAuthMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		String[] args = {"auth_id"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		DataModelAndView modelAndView = authMngtSvc.removeAuthMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}

}
