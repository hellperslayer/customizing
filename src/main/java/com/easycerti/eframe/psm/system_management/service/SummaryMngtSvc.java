package com.easycerti.eframe.psm.system_management.service;

import java.util.Map;

import com.easycerti.eframe.common.spring.DataModelAndView;

public interface SummaryMngtSvc {
	
	public DataModelAndView summaryMngtList();
	public int createSummary(String procdate, String type);
	public int createSummaryAll(String procdate);
	
	public int createSummaryDaily(String procdate);
	public int createSummaryMonth(Map<String,String> parameter);
	
	/** 해당하는 기간내에 일마감 데이터가 있는지 확인하여 리턴함 */
	public String checkSummaryDaily(Map<String, String> parameters);
	
	public String addInstallDate(Map<String, String> parameters);
	public String checkSummaryMonth(Map<String, String> parameters);
	
	public boolean extractorLogCheck(String procdate);
	
	public int createSummaryStatistics(String procdate);
	public int createSummaryReqtype(String procdate);
	public int createSummaryDownload(String procdate);
	public int createSummaryTime(String procdate);
	
	public int createSummaryAbnormal(String procdate);
}
