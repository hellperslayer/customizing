package com.easycerti.eframe.psm.system_management.vo;

import java.util.ArrayList;
import java.util.List;

import com.easycerti.eframe.psm.report.vo.ReportData;

public class PrivacyReport {
	
	private String logType;			//로그타입(BA, DN)
	
	private int prevLogCnt;			//전년
	private int nowLogCnt;			//현재
	private int compareLogCnt;		//비교 
	private int prevPrivacyCnt;		//전년
	private int nowPrivacyCnt;		//현재
	private int comparePrivacyCnt;	//비교
	
	private int compareCnt;		//전년비교
	
	private String privacy_desc;
	private String cnt;
	
	private int cnt0;
	private int logcnt0;
	private int logcnt1;
	private int logcnt2;
	private int logcnt3;
	
	private int cnt1;
	private int cnt2;
	private int cnt3;
	private int logcnt;				//처리량(로그건수)
	private int privacyCnt;			//이용량(개인정보유형처리(이용)건수)
	private int totalCnt;
	
	private String tot_cnt;
	
	private String start_date;
	private String end_date;
	private String compare_start_date;
	private String compare_end_date;
	private String prevYear;
	private String nowYear;
	private String month;
	private int week;
	
	//1월 ~ 12 월
	private int m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12;
	private int m1_1,m1_2,m1_3,m1_4,m1_5,m1_6,m1_7,m1_8,m1_9,m1_10,m1_11,m1_12;
	private int half1, half2;

	private String system_seq ;
	private String system_name="";
	private String dept_id;
	private String dept_name;
	private String emp_user_id;
	private String emp_user_name;
	private String req_type;
	private String result_type;
	
	private String proc_date;
	private String proc_time;
	private String reponse_dt;
	
	private String bCheckPlus;
	private String bCheckPlus1;
	private String bCheckPlus2;
	private String bCheckPlus3;
	
	private String rule_nm;
	private String occr_dt;
	
	private String rule_cd;
	private String rule;
	
	private String mapping_id;
	
	
	private int type1=0;
	private int type2=0;
	private int type3=0;
	private int type4=0;
	private int type5=0;
	private int type6=0;
	private int type7=0;
	private int type8=0;
	private int type9=0;
	private int type10=0;
	private int type11=0;
	private int type12=0;
	private int type13=0;
	private int type14=0;
	private int type15=0;
	private int type16=0;
	private int type17=0;
	private int type18=0;

	private int type99=0;
	
	private int scenario1 = 0;
	private int scenario2 = 0;
	private int scenario3 = 0;
	private int scenario4 = 0;
	private int scenario5 = 0;
	
	//list sort
	private int stype1 = 0;
	private int stype2 = 0;
	private int stype3 = 0;
	private int stype4 = 0;
	
	public int getType99() {
		return type99;
	}

	public void setType99(int type99) {
		this.type99 = type99;
	}
	private int type1_1, type1_2, type1_3, type1_4;
	
	
	
	List<String> sysList = new ArrayList<String>();
	
	
	private int rank;
	private String requrl;
	private String req_url;
	
	private String scrn_nm;
	private String total_count;
	
	private String search_from;
	private String search_to;
	private String log_type;
	
	private String privacy_type;
	private String reason;
	private String reason_code="";
	private String code_id;
	private String code_name;
	
	List<String> auth_idsList;
	
	private String user_ip;
	private String target_id;
	private String target_auth;
	private String target_auth_name;
	
	private int system_seqCnt;
	private int deptCnt;
	
	private String emp_detail_seq;
	private String extract_result_seq;
	private String desc_status;
	private String desc_result;
	
	private int r2;
	private int r4;
	private int r5;
	private int r8;
	private int r9;
	private int r_sum;
	
	private int rs0;
	private int rs1;
	private int rs2;
	private int rs3;
	private int rs4;
	private int rs_sum;
	
	private String month1;
	private String month2;
	private String month3;
	private String month4;
	private String month5;
	private String month6;

	
	private String type;
	
	private String period_type;		//월간 : 1, 분기 : 2, 반기 : 3, 연간 : 4
	private String quarter_type;	//분기보고서 quarter_type(1~4)
	private String half_type;		//분기별보고서 half_type(1~4)
	
	private String file_ext;
	private String file_ext_cnt;
	
	private int prevLogCntSum;
	private int prevPrivacyCntSum;
	private int nowLogCntSum;
	private int nowPrivacyCntSum;
	
	private int now_ext_cnt;
	private int priv_ext_cnt;
	private String dataUnit; //한글 데이터 단위
	private int dataUnit_num;
	private String searchType;
	private String logCtType;
	
	
	private String st_proc_date;
	private String inst_name;
	private String cnt_client_ip;
	private String cnt_sql;
	private String cnt_application;
	private String sum_resp_time;
	private String sum_server_time;
	private String sum_byte_in;
	private String sum_byte_out;
	private String sum_rtrn_rows;
	private String client_ip;
	private String cnt_inst_name;
	private String app_name;
	
	private String req_dt;
	private String summon_status;
	private String decision_status;
	private String script_desc;

	public String getScript_desc() {
		return script_desc;
	}

	public void setScript_desc(String script_desc) {
		this.script_desc = script_desc;
	}

	public String getReq_dt() {
		return req_dt;
	}

	public void setReq_dt(String req_dt) {
		this.req_dt = req_dt;
	}

	public String getSummon_status() {
		return summon_status;
	}

	public void setSummon_status(String summon_status) {
		this.summon_status = summon_status;
	}

	public String getDecision_status() {
		return decision_status;
	}

	public void setDecision_status(String decision_status) {
		this.decision_status = decision_status;
	}

	public int getR9() {
		return r9;
	}

	public void setR9(int r9) {
		this.r9 = r9;
	}

	public int getRs3() {
		return rs3;
	}

	public void setRs3(int rs3) {
		this.rs3 = rs3;
	}

	public int getRs4() {
		return rs4;
	}

	public void setRs4(int rs4) {
		this.rs4 = rs4;
	}

	public int getTotalCnt() {
		return totalCnt;
	}

	public void setTotalCnt(int totalCnt) {
		this.totalCnt = totalCnt;
	}

	public int getScenario1() {
		return scenario1;
	}

	public void setScenario1(int scenario1) {
		this.scenario1 = scenario1;
	}

	public int getScenario2() {
		return scenario2;
	}

	public void setScenario2(int scenario2) {
		this.scenario2 = scenario2;
	}

	public int getScenario3() {
		return scenario3;
	}

	public void setScenario3(int scenario3) {
		this.scenario3 = scenario3;
	}

	public int getScenario4() {
		return scenario4;
	}

	public void setScenario4(int scenario4) {
		this.scenario4 = scenario4;
	}

	public int getScenario5() {
		return scenario5;
	}

	public void setScenario5(int scenario5) {
		this.scenario5 = scenario5;
	}

	public String getLogCtType() {
		return logCtType;
	}

	public void setLogCtType(String logCtType) {
		this.logCtType = logCtType;
	}
	public String getSearchType() {
		return searchType;
	}
	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}
	public String getQuarter_type() {
		return quarter_type;
	}
	public void setQuarter_type(String quarter_type) {
		this.quarter_type = quarter_type;
	}
	public String getPeriod_type() {
		return period_type;
	}
	public void setPeriod_type(String period_type) {
		this.period_type = period_type;
	}
	public String getLogType() {
		return logType;
	}
	public void setLogType(String logType) {
		this.logType = logType;
	}
	public int getM1() {
		return m1;
	}
	public void setM1(int m1) {
		this.m1 = m1;
	}
	public int getM2() {
		return m2;
	}
	public void setM2(int m2) {
		this.m2 = m2;
	}
	public int getM3() {
		return m3;
	}
	public void setM3(int m3) {
		this.m3 = m3;
	}
	public int getM4() {
		return m4;
	}
	public void setM4(int m4) {
		this.m4 = m4;
	}
	public int getM5() {
		return m5;
	}
	public void setM5(int m5) {
		this.m5 = m5;
	}
	public int getM6() {
		return m6;
	}
	public void setM6(int m6) {
		this.m6 = m6;
	}
	public int getM7() {
		return m7;
	}
	public void setM7(int m7) {
		this.m7 = m7;
	}
	public int getM8() {
		return m8;
	}
	public void setM8(int m8) {
		this.m8 = m8;
	}
	public int getM9() {
		return m9;
	}
	public void setM9(int m9) {
		this.m9 = m9;
	}
	public int getM10() {
		return m10;
	}
	public void setM10(int m10) {
		this.m10 = m10;
	}
	public int getM11() {
		return m11;
	}
	public void setM11(int m11) {
		this.m11 = m11;
	}
	public int getM12() {
		return m12;
	}
	public void setM12(int m12) {
		this.m12 = m12;
	}	
	public int getHalf1() {
		return half1;
	}
	public void setHalf1(int half1) {
		this.half1 = half1;
	}
	public int getHalf2() {
		return half2;
	}
	public void setHalf2(int half2) {
		this.half2 = half2;
	}
	
	
	
	public String getPrevYear() {
		return prevYear;
	}
	public void setPrevYear(String prevYear) {
		this.prevYear = prevYear;
	}
	public String getNowYear() {
		return nowYear;
	}
	public void setNowYear(String nowYear) {
		this.nowYear = nowYear;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getNowLogCnt() {
		return nowLogCnt;
	}
	public void setNowLogCnt(int nowLogCnt) {
		this.nowLogCnt = nowLogCnt;
	}
	public int getNowPrivacyCnt() {
		return nowPrivacyCnt;
	}
	public void setNowPrivacyCnt(int nowPrivacyCnt) {
		this.nowPrivacyCnt = nowPrivacyCnt;
	}
	public int getPrivacyCnt() {
		return privacyCnt;
	}
	public void setPrivacyCnt(int privacyCnt) {
		this.privacyCnt = privacyCnt;
	}
	
	
	public int getLogcnt() {
		return logcnt;
	}
	public void setLogcnt(int logcnt) {
		this.logcnt = logcnt;
	}
	public String getMonth1() {
		return month1;
	}
	public void setMonth1(String month1) {
		this.month1 = month1;
	}
	public String getMonth2() {
		return month2;
	}
	public void setMonth2(String month2) {
		this.month2 = month2;
	}
	public String getMonth3() {
		return month3;
	}
	public void setMonth3(String month3) {
		this.month3 = month3;
	}
	public String getMonth4() {
		return month4;
	}
	public void setMonth4(String month4) {
		this.month4 = month4;
	}
	public String getMonth5() {
		return month5;
	}
	public void setMonth5(String month5) {
		this.month5 = month5;
	}
	public String getMonth6() {
		return month6;
	}
	public void setMonth6(String month6) {
		this.month6 = month6;
	}
	public String getReponse_dt() {
		return reponse_dt;
	}
	public void setReponse_dt(String reponse_dt) {
		this.reponse_dt = reponse_dt;
	}
	public String getExtract_result_seq() {
		return extract_result_seq;
	}
	public void setExtract_result_seq(String extract_result_seq) {
		this.extract_result_seq = extract_result_seq;
	}
	public int getSystem_seqCnt() {
		return system_seqCnt;
	}
	public void setSystem_seqCnt(int system_seqCnt) {
		this.system_seqCnt = system_seqCnt;
	}
	public String getEmp_detail_seq() {
		return emp_detail_seq;
	}
	public void setEmp_detail_seq(String emp_detail_seq) {
		this.emp_detail_seq = emp_detail_seq;
	}
	public String getDesc_status() {
		return desc_status;
	}
	public void setDesc_status(String desc_status) {
		this.desc_status = desc_status;
	}
	public String getDesc_result() {
		return desc_result;
	}
	public void setDesc_result(String desc_result) {
		this.desc_result = desc_result;
	}
	public int getR2() {
		return r2;
	}
	public void setR2(int r2) {
		this.r2 = r2;
	}
	public int getR4() {
		return r4;
	}
	public void setR4(int r4) {
		this.r4 = r4;
	}
	public int getR5() {
		return r5;
	}
	public void setR5(int r5) {
		this.r5 = r5;
	}
	public int getR8() {
		return r8;
	}
	public void setR8(int r8) {
		this.r8 = r8;
	}
	public int getRs0() {
		return rs0;
	}
	public void setRs0(int rs0) {
		this.rs0 = rs0;
	}
	public int getRs1() {
		return rs1;
	}
	public void setRs1(int rs1) {
		this.rs1 = rs1;
	}
	public int getRs2() {
		return rs2;
	}
	public void setRs2(int rs2) {
		this.rs2 = rs2;
	}
	public int getRs_sum() {
		return rs_sum;
	}
	public void setRs_sum(int rs_sum) {
		this.rs_sum = rs_sum;
	}
	public int getR_sum() {
		return r_sum;
	}
	public void setR_sum(int r_sum) {
		this.r_sum = r_sum;
	}
	public int getDeptCnt() {
		return deptCnt;
	}
	public void setDeptCnt(int deptCnt) {
		this.deptCnt = deptCnt;
	}
	public String getUser_ip() {
		return user_ip;
	}
	public void setUser_ip(String user_ip) {
		this.user_ip = user_ip;
	}
	public String getLog_type() {
		return log_type;
	}
	public void setLog_type(String log_type) {
		this.log_type = log_type;
	}
	public String getTarget_id() {
		return target_id;
	}
	public void setTarget_id(String target_id) {
		this.target_id = target_id;
	}
	public String getTarget_auth() {
		return target_auth;
	}
	public void setTarget_auth(String target_auth) {
		this.target_auth = target_auth;
	}
	public String getTarget_auth_name() {
		return target_auth_name;
	}
	public void setTarget_auth_name(String target_auth_name) {
		this.target_auth_name = target_auth_name;
	}
	public List<String> getAuth_idsList() {
		return auth_idsList;
	}
	public void setAuth_idsList(List<String> auth_idsList) {
		this.auth_idsList = auth_idsList;
	}
	public String getSearch_from() {
		return search_from;
	}
	public void setSearch_from(String search_from) {
		this.search_from = search_from;
	}
	public String getSearch_to() {
		return search_to;
	}
	public void setSearch_to(String search_to) {
		this.search_to = search_to;
	}
	private List<ReportData> reportData = new ArrayList<ReportData>();

	
	public String getProc_time() {
		return proc_time;
	}
	public void setProc_time(String proc_time) {
		this.proc_time = proc_time;
	}
	public String getDept_id() {
		return dept_id;
	}
	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}
	public int getType1() {
		return type1;
	}
	public void setType1(int type1) {
		this.type1 = type1;
	}
	public int getType2() {
		return type2;
	}
	public void setType2(int type2) {
		this.type2 = type2;
	}
	public int getType3() {
		return type3;
	}
	public void setType3(int type3) {
		this.type3 = type3;
	}
	public int getType4() {
		return type4;
	}
	public void setType4(int type4) {
		this.type4 = type4;
	}
	public int getType5() {
		return type5;
	}
	public void setType5(int type5) {
		this.type5 = type5;
	}
	public int getType6() {
		return type6;
	}
	public void setType6(int type6) {
		this.type6 = type6;
	}
	public int getType7() {
		return type7;
	}
	public void setType7(int type7) {
		this.type7 = type7;
	}
	public int getType8() {
		return type8;
	}
	public void setType8(int type8) {
		this.type8 = type8;
	}
	public int getType9() {
		return type9;
	}
	public void setType9(int type9) {
		this.type9 = type9;
	}
	public int getType10() {
		return type10;
	}
	public void setType10(int type10) {
		this.type10 = type10;
	}
	public int getType11() {
		return type11;
	}
	public void setType11(int type11) {
		this.type11 = type11;
	}
	public int getType12() {
		return type12;
	}
	public void setType12(int type12) {
		this.type12 = type12;
	}
	
	public int getType13() {
		return type13;
	}
	public void setType13(int type13) {
		this.type13 = type13;
	}
	public int getType14() {
		return type14;
	}
	public void setType14(int type14) {
		this.type14 = type14;
	}
	public int getType15() {
		return type15;
	}
	public void setType15(int type15) {
		this.type15 = type15;
	}
	
	public int getType16() {
		return type16;
	}

	public void setType16(int type16) {
		this.type16 = type16;
	}

	public int getType17() {
		return type17;
	}

	public void setType17(int type17) {
		this.type17 = type17;
	}

	public int getType18() {
		return type18;
	}

	public void setType18(int type18) {
		this.type18 = type18;
	}

	public String getRule_cd() {
		return rule_cd;
	}
	public void setRule_cd(String rule_cd) {
		this.rule_cd = rule_cd;
	}
	public String getRule_nm() {
		return rule_nm;
	}
	public void setRule_nm(String rule_nm) {
		this.rule_nm = rule_nm;
	}
	public String getOccr_dt() {
		return occr_dt;
	}
	public void setOccr_dt(String occr_dt) {
		this.occr_dt = occr_dt;
	}
	public String getbCheckPlus() {
		return bCheckPlus;
	}
	public void setbCheckPlus(String bCheckPlus) {
		this.bCheckPlus = bCheckPlus;
	}
	public String getbCheckPlus1() {
		return bCheckPlus1;
	}
	public void setbCheckPlus1(String bCheckPlus1) {
		this.bCheckPlus1 = bCheckPlus1;
	}
	
	
	public String getbCheckPlus2() {
		return bCheckPlus2;
	}
	public void setbCheckPlus2(String bCheckPlus2) {
		this.bCheckPlus2 = bCheckPlus2;
	}
	public String getbCheckPlus3() {
		return bCheckPlus3;
	}
	public void setbCheckPlus3(String bCheckPlus3) {
		this.bCheckPlus3 = bCheckPlus3;
	}
	public String getProc_date() {
		return proc_date;
	}
	public void setProc_date(String proc_date) {
		this.proc_date = proc_date;
	}
	
	public String getEmp_user_name() {
		return emp_user_name;
	}
	public void setEmp_user_name(String emp_user_name) {
		this.emp_user_name = emp_user_name;
	}
	public String getDept_name() {
		return dept_name;
	}
	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}
	List<String> list = new ArrayList<String>();
	
	
	public List<String> getList() {
		return list;
	}
	public void setList(List<String> list) {
		this.list = list;
	}
	public String getEmp_user_id() {
		return emp_user_id;
	}
	public void setEmp_user_id(String emp_user_id) {
		this.emp_user_id = emp_user_id;
	}
	public String getTot_cnt() {
		return tot_cnt;
	}
	public void setTot_cnt(String tot_cnt) {
		this.tot_cnt = tot_cnt;
	}
	
	public String getSystem_seq() {
		return system_seq;
	}
	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}
	public String getStart_date() {
		return start_date;
	}
	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}
	public String getEnd_date() {
		return end_date;
	}
	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
	
	public String getCompare_start_date() {
		return compare_start_date;
	}
	public void setCompare_start_date(String compare_start_date) {
		this.compare_start_date = compare_start_date;
	}
	public String getCompare_end_date() {
		return compare_end_date;
	}
	public void setCompare_end_date(String compare_end_date) {
		this.compare_end_date = compare_end_date;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public int getWeek() {
		return week;
	}
	public void setWeek(int week) {
		this.week = week;
	}
	public String getResult_type() {
		return result_type;
	}
	public void setResult_type(String result_type) {
		this.result_type = result_type;
	}
	public String getPrivacy_desc() {
		return privacy_desc;
	}
	public void setPrivacy_desc(String privacy_desc) {
		this.privacy_desc = privacy_desc;
	}
	public String getCnt() {
		return cnt;
	}
	public void setCnt(String cnt) {
		this.cnt = cnt;
	}
	public String getReq_type() {
		return req_type;
	}
	public void setReq_type(String req_type) {
		this.req_type = req_type;
	}
	public String getSystem_name() {
		return system_name;
	}
	public void setSystem_name(String system_name) {
		this.system_name = system_name;
	}
	public String getRule() {
		return rule;
	}
	public void setRule(String rule) {
		this.rule = rule;
	}
	public List<String> getSysList() {
		return sysList;
	}
	public void setSysList(List<String> sysList) {
		this.sysList = sysList;
	}
	public String getHalf_type() {
		return half_type;
	}
	public void setHalf_type(String half_type) {
		this.half_type = half_type;
	}
	public List<ReportData> getReportData() {
		return reportData;
	}
	public void setReportData(List<ReportData> reportData) {
		this.reportData = reportData;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public String getRequrl() {
		return requrl;
	}
	public void setRequrl(String requrl) {
		this.requrl = requrl;
	}
	public String getScrn_nm() {
		return scrn_nm;
	}
	public void setScrn_nm(String scrn_nm) {
		this.scrn_nm = scrn_nm;
	}
	public String getTotal_count() {
		return total_count;
	}
	public void setTotal_count(String total_count) {
		this.total_count = total_count;
	}

	public int getCnt1() {
		return cnt1;
	}
	public void setCnt1(int cnt1) {
		this.cnt1 = cnt1;
	}
	public int getCnt2() {
		return cnt2;
	}
	public void setCnt2(int cnt2) {
		this.cnt2 = cnt2;
	}
	public int getCnt3() {
		return cnt3;
	}
	public void setCnt3(int cnt3) {
		this.cnt3 = cnt3;
	}
	public String getReq_url() {
		return req_url;
	}
	public void setReq_url(String req_url) {
		this.req_url = req_url;
	}
	public String getPrivacy_type() {
		return privacy_type;
	}
	public void setPrivacy_type(String privacy_type) {
		this.privacy_type = privacy_type;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getReason_code() {
		return reason_code;
	}
	public void setReason_code(String reason_code) {
		this.reason_code = reason_code;
	}
	public String getCode_id() {
		return code_id;
	}
	public void setCode_id(String code_id) {
		this.code_id = code_id;
	}
	public String getCode_name() {
		return code_name;
	}
	public void setCode_name(String code_name) {
		this.code_name = code_name;
	}
	public int getCompareCnt() {
		return compareCnt;
	}
	public void setCompareCnt(int compareCnt) {
		this.compareCnt = compareCnt;
	}
	public int getCompareLogCnt() {
		return compareLogCnt;
	}
	public void setCompareLogCnt(int compareLogCnt) {
		this.compareLogCnt = compareLogCnt;
	}
	public int getPrevLogCnt() {
		return prevLogCnt;
	}
	public void setPrevLogCnt(int prevLogCnt) {
		this.prevLogCnt = prevLogCnt;
	}
	public int getComparePrivacyCnt() {
		return comparePrivacyCnt;
	}
	public void setComparePrivacyCnt(int comparePrivacyCnt) {
		this.comparePrivacyCnt = comparePrivacyCnt;
	}
	public int getPrevPrivacyCnt() {
		return prevPrivacyCnt;
	}
	public void setPrevPrivacyCnt(int prevPrivacyCnt) {
		this.prevPrivacyCnt = prevPrivacyCnt;
	}
	public String getFile_ext() {
		return file_ext;
	}
	public void setFile_ext(String file_ext) {
		this.file_ext = file_ext;
	}
	public String getFile_ext_cnt() {
		return file_ext_cnt;
	}
	public void setFile_ext_cnt(String file_ext_cnt) {
		this.file_ext_cnt = file_ext_cnt;
	}
	public int getPrevLogCntSum() {
		return prevLogCntSum;
	}
	public void setPrevLogCntSum(int prevLogCntSum) {
		this.prevLogCntSum = prevLogCntSum;
	}
	public int getPrevPrivacyCntSum() {
		return prevPrivacyCntSum;
	}
	public void setPrevPrivacyCntSum(int prevPrivacyCntSum) {
		this.prevPrivacyCntSum = prevPrivacyCntSum;
	}
	public int getNowLogCntSum() {
		return nowLogCntSum;
	}
	public void setNowLogCntSum(int nowLogCntSum) {
		this.nowLogCntSum = nowLogCntSum;
	}
	public int getNowPrivacyCntSum() {
		return nowPrivacyCntSum;
	}
	public void setNowPrivacyCntSum(int nowPrivacyCntSum) {
		this.nowPrivacyCntSum = nowPrivacyCntSum;
	}
	public int getNow_ext_cnt() {
		return now_ext_cnt;
	}
	public void setNow_ext_cnt(int now_ext_cnt) {
		this.now_ext_cnt = now_ext_cnt;
	}
	public int getPriv_ext_cnt() {
		return priv_ext_cnt;
	}
	public void setPriv_ext_cnt(int priv_ext_cnt) {
		this.priv_ext_cnt = priv_ext_cnt;
	}
	public int getM1_1() {
		return m1_1;
	}
	public void setM1_1(int m1_1) {
		this.m1_1 = m1_1;
	}
	public int getM1_2() {
		return m1_2;
	}
	public void setM1_2(int m1_2) {
		this.m1_2 = m1_2;
	}
	public int getM1_3() {
		return m1_3;
	}
	public void setM1_3(int m1_3) {
		this.m1_3 = m1_3;
	}
	public int getM1_4() {
		return m1_4;
	}
	public void setM1_4(int m1_4) {
		this.m1_4 = m1_4;
	}
	public int getM1_5() {
		return m1_5;
	}
	public void setM1_5(int m1_5) {
		this.m1_5 = m1_5;
	}
	public int getM1_6() {
		return m1_6;
	}
	public void setM1_6(int m1_6) {
		this.m1_6 = m1_6;
	}
	public int getM1_7() {
		return m1_7;
	}
	public void setM1_7(int m1_7) {
		this.m1_7 = m1_7;
	}
	public int getM1_8() {
		return m1_8;
	}
	public void setM1_8(int m1_8) {
		this.m1_8 = m1_8;
	}
	public int getM1_9() {
		return m1_9;
	}
	public void setM1_9(int m1_9) {
		this.m1_9 = m1_9;
	}
	public int getM1_10() {
		return m1_10;
	}
	public void setM1_10(int m1_10) {
		this.m1_10 = m1_10;
	}
	public int getM1_11() {
		return m1_11;
	}
	public void setM1_11(int m1_11) {
		this.m1_11 = m1_11;
	}
	public int getM1_12() {
		return m1_12;
	}
	public void setM1_12(int m1_12) {
		this.m1_12 = m1_12;
	}
	public int getType1_1() {
		return type1_1;
	}
	public void setType1_1(int type1_1) {
		this.type1_1 = type1_1;
	}
	public int getType1_2() {
		return type1_2;
	}
	public void setType1_2(int type1_2) {
		this.type1_2 = type1_2;
	}
	public int getType1_3() {
		return type1_3;
	}
	public void setType1_3(int type1_3) {
		this.type1_3 = type1_3;
	}
	public int getType1_4() {
		return type1_4;
	}
	public void setType1_4(int type1_4) {
		this.type1_4 = type1_4;
	}
	public String getDataUnit() {
		return dataUnit;
	}
	public void setDataUnit(String dataUnit) {
		this.dataUnit = dataUnit;
	}
	public int getDataUnit_num() {
		return dataUnit_num;
	}
	public void setDataUnit_num(int dataUnit_num) {
		this.dataUnit_num = dataUnit_num;
	}

	public String getMapping_id() {
		return mapping_id;
	}

	public void setMapping_id(String mapping_id) {
		this.mapping_id = mapping_id;
	}

	public int getStype1() {
		return stype1;
	}

	public void setStype1(int stype1) {
		this.stype1 = stype1;
	}

	public int getStype2() {
		return stype2;
	}

	public void setStype2(int stype2) {
		this.stype2 = stype2;
	}

	public int getStype3() {
		return stype3;
	}

	public void setStype3(int stype3) {
		this.stype3 = stype3;
	}

	public int getStype4() {
		return stype4;
	}

	public void setStype4(int stype4) {
		this.stype4 = stype4;
	}

	public int getCnt0() {
		return cnt0;
	}

	public void setCnt0(int cnt0) {
		this.cnt0 = cnt0;
	}

	public int getLogcnt0() {
		return logcnt0;
	}

	public void setLogcnt0(int logcnt0) {
		this.logcnt0 = logcnt0;
	}

	public int getLogcnt1() {
		return logcnt1;
	}

	public void setLogcnt1(int logcnt1) {
		this.logcnt1 = logcnt1;
	}

	public int getLogcnt2() {
		return logcnt2;
	}

	public void setLogcnt2(int logcnt2) {
		this.logcnt2 = logcnt2;
	}

	public int getLogcnt3() {
		return logcnt3;
	}

	public void setLogcnt3(int logcnt3) {
		this.logcnt3 = logcnt3;
	}

	public String getSt_proc_date() {
		return st_proc_date;
	}

	public void setSt_proc_date(String st_proc_date) {
		this.st_proc_date = st_proc_date;
	}

	public String getInst_name() {
		return inst_name;
	}

	public void setInst_name(String inst_name) {
		this.inst_name = inst_name;
	}

	public String getCnt_client_ip() {
		return cnt_client_ip;
	}

	public void setCnt_client_ip(String cnt_client_ip) {
		this.cnt_client_ip = cnt_client_ip;
	}

	public String getCnt_sql() {
		return cnt_sql;
	}

	public void setCnt_sql(String cnt_sql) {
		this.cnt_sql = cnt_sql;
	}

	public String getCnt_application() {
		return cnt_application;
	}

	public void setCnt_application(String cnt_application) {
		this.cnt_application = cnt_application;
	}

	public String getSum_resp_time() {
		return sum_resp_time;
	}

	public void setSum_resp_time(String sum_resp_time) {
		this.sum_resp_time = sum_resp_time;
	}

	public String getSum_server_time() {
		return sum_server_time;
	}

	public void setSum_server_time(String sum_server_time) {
		this.sum_server_time = sum_server_time;
	}

	public String getSum_byte_in() {
		return sum_byte_in;
	}

	public void setSum_byte_in(String sum_byte_in) {
		this.sum_byte_in = sum_byte_in;
	}

	public String getSum_byte_out() {
		return sum_byte_out;
	}

	public void setSum_byte_out(String sum_byte_out) {
		this.sum_byte_out = sum_byte_out;
	}

	public String getSum_rtrn_rows() {
		return sum_rtrn_rows;
	}

	public void setSum_rtrn_rows(String sum_rtrn_rows) {
		this.sum_rtrn_rows = sum_rtrn_rows;
	}

	public String getClient_ip() {
		return client_ip;
	}

	public void setClient_ip(String client_ip) {
		this.client_ip = client_ip;
	}

	public String getCnt_inst_name() {
		return cnt_inst_name;
	}

	public void setCnt_inst_name(String cnt_inst_name) {
		this.cnt_inst_name = cnt_inst_name;
	}

	public String getApp_name() {
		return app_name;
	}

	public void setApp_name(String app_name) {
		this.app_name = app_name;
	}
	
}
