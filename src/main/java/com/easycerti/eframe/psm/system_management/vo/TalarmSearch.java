package com.easycerti.eframe.psm.system_management.vo;


import com.easycerti.eframe.common.vo.SearchBase;

/**
 * 사원관리 검색 VO
 * 
 * @author yjyoo
 * @since 2015. 5. 27.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 27.           yjyoo            최초 생성
 *
 * </pre>
 */
public class TalarmSearch extends SearchBase {
	
	// 사원 ID
	private String emp_user_id;
	
	private String emp_user_id_s;
	
	// 직원 이름
	private String emp_user_name;
	
	// 부서 ID
	private String dept_id;
	
	// 상태
	private String status;
	
	private String email_address;
	
	private String mobile_number;
	
	private String useyn_email;
	
	private String useyn_htel;
	
	// 하위 부서 검색
	private String deptLowSearchFlag;
	
	public String getEmp_user_id() {
		return emp_user_id;
	}

	public void setEmp_user_id(String emp_user_id) {
		this.emp_user_id = emp_user_id;
	}
	
	public String getEmp_user_id_s() {
		return emp_user_id_s;
	}

	public void setEmp_user_id_s(String emp_user_id_s) {
		this.emp_user_id_s = emp_user_id_s;
	}
	

	public String getEmp_user_name() {
		return emp_user_name;
	}

	public void setEmp_user_name(String emp_user_name) {
		this.emp_user_name = emp_user_name;
	}

	public String getDept_id() {
		return dept_id;
	}

	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDeptLowSearchFlag() {
		return deptLowSearchFlag;
	}

	public void setDeptLowSearchFlag(String deptLowSearchFlag) {
		this.deptLowSearchFlag = deptLowSearchFlag;
	}
	
	public String getEmail_address() {
		return email_address;
	}

	public void setEmail_address(String email_address) {
		this.email_address = email_address;
	}
	
	public String getMobile_number() {
		return mobile_number;
	}

	public void setMobile_number(String mobile_number) {
		this.mobile_number = mobile_number;
	}
	
	public String getUseyn_email() {
		return useyn_email;
	}

	public void setUseyn_email(String useyn_email) {
		this.useyn_email = useyn_email;
	}
	
	public String getUseyn_htel() {
		return useyn_htel;
	}

	public void setUseyn_htel(String useyn_htel) {
		this.useyn_htel = useyn_htel;
	}

	@Override
	public String toString() {
		return "TalarmSearch [emp_user_id=" + emp_user_id_s + ", emp_user_name=" + emp_user_name + ", email_address=" + email_address
				+ ", mobile_number=" + mobile_number + ", useyn_email=" + useyn_email + ", useyn_htel=" + useyn_htel + "]";
	}	
}
