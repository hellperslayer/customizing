package com.easycerti.eframe.psm.system_management.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.AuthenticationFailedException;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import com.easycerti.eframe.common.dao.BootInitialDao;
import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.scheduler.web.LogCountCheck_Scheduler.MyAuthentication;
import com.easycerti.eframe.psm.search.dao.AllLogInqDao;
import com.easycerti.eframe.psm.search.dao.ExtrtCondbyInqDao;
import com.easycerti.eframe.psm.search.vo.AllLogInq;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInq;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.system_management.dao.AlarmMngtDao;
import com.easycerti.eframe.psm.system_management.service.AlarmMngtSvc;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;
import com.easycerti.eframe.psm.system_management.vo.AlarmMngt;
import com.easycerti.eframe.psm.system_management.vo.SendMailInfo;
import com.easycerti.eframe.psm.system_management.vo.System;

/**
 * 옵션 설정 Service Implements
 */

@Service
public class AlarmMngtSvcImpl implements AlarmMngtSvc {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private AlarmMngtDao alarmMngtDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private BootInitialDao bootInitialDao;
	
	@Autowired
	private ExtrtCondbyInqDao extrtCondbyInqDao;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;

	@Autowired
	private AllLogInqDao allLogInqDao;

	@Value("#{configProperties.smtp_host}")
	private String smtp_host;
	@Value("#{configProperties.smtp_port}")
	private String smtp_port;
	@Value("#{configProperties.smtp_id}")
	private String smtp_id;
	@Value("#{configProperties.smtp_pw}")
	private String smtp_pw;
	@Value("#{configProperties.context_url}")
	private String context_url;
	@Value("#{configProperties.smtp_fromName}")
	private String smtp_fromName;
	@Value("#{configProperties.smtp_fromPhone}")
	private String smtp_fromPhone;
	
	@Value("#{versionProperties.version}")
	private String version;
	
	@Override
	public DataModelAndView findAlarmMngtList(Map<String, String> parameters) {
		
		System paramBean = CommonHelper.convertMapToBean(parameters, System.class);
		
		SimpleCode simpleCode = new SimpleCode();
		String index = "/alarmMngt/list.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		List<AlarmMngt> list = alarmMngtDao.findAlarmMngtList();
		List<AlarmMngt> list2 = alarmMngtDao.findAlarmMngtSystemMail();
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("list", list);
		modelAndView.addObject("list2", list2);
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView setAlarmMngt(Map<String, String> parameters) {
		AlarmMngt paramBean = CommonHelper.convertMapToBean(parameters, AlarmMngt.class);
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			String[] seqList = paramBean.getTot_agent_seq().split("/@!#");
			String[] optionsList = paramBean.getTot_agent_options().split("/@!#");
			
			for(int i = 0; i < seqList.length; i++) {
				AlarmMngt alarmMngt = new AlarmMngt();
				alarmMngt.setAgent_seq(Integer.parseInt(seqList[i]));
				alarmMngt.setAgent_options(optionsList[i]);
				alarmMngtDao.setAlarmMngt(alarmMngt);
			}
			
			transactionManager.commit(transactionStatus);
			
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[AlarmMngtSvcImpl.setAlarmMngt] MESSAGE : " + e.getMessage());
			throw new ESException("SYS035J");
		}
		
		return null;
	}
	
	@Override
	public DataModelAndView setAlarmMngt_email(Map<String, String> parameters) {
		AlarmMngt paramBean = CommonHelper.convertMapToBean(parameters, AlarmMngt.class);
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			
			AlarmMngt alarmMngt = new AlarmMngt();
			alarmMngtDao.setAlarmMngt_email(paramBean);
			
			transactionManager.commit(transactionStatus);
			
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[AlarmMngtSvcImpl.setAlarmMngt] MESSAGE : " + e.getMessage());
			throw new ESException("SYS035J");
		}
		
		return null;
	}

	@Override
	public String sendMail(Map<String, String> parameters) {
		
		String inputData[] = {"id","pw","to_id","site_name"};
		for (String string : inputData) {
			if(parameters.get(string).length()<1) {
				return "missingdata";
			}
		}
		
		SendMailInfo sendMailInfo = new SendMailInfo(); // 메일전송 정보 담아두는 vo
		boolean sendBool = false;
		String result = "success";		
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Calendar cal = GregorianCalendar.getInstance(Locale.KOREA);
		Date yesterday = new Date();
		SimpleDateFormat mailDate = new SimpleDateFormat("yyyy년 MM월 dd일");

		cal.setTime(yesterday);
		cal.add(Calendar.DATE, -1);
		String procdate = sdf.format(cal.getTime());

		SearchSearch search = new SearchSearch();
		search.setProc_date(procdate);
		List<AllLogInq> systemCount = new ArrayList<AllLogInq>();
		try {
			systemCount = allLogInqDao.findLogCountBySystem(search);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		/*
		 * for (AllLogInq allLogInq : systemCount) { if (allLogInq.getCnt() < 1) {
		 * sendBool = true; break; } }
		 */
		//sendBool&&
			sendMailInfo.setSubject("["+parameters.get("site_name")+" 테스트 메일] 시스템별 로그 수집 내용입니다.");
			
			String body = "<h1 style='display:inline'>"+parameters.get("site_name")+"</h1> 에서 발송된 "+mailDate.format(cal.getTime())+"자 로그 수집 결과 입니다. (adminVer. "+version+")</br></br>"
					+ "<table border=\"1\" cellspacing=\"0\" cellpadding=\"0\" style=\"word-break: normal; width: 1182px; height: 60px; font-size: 10pt; border-collapse: collapse; border: 1px none rgb(0, 0, 0);\">"
					+ "<tr>"
					+ "<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px; background-color : #e2d9d9; text-align: center; \"><p>시스템 번호</p></td>"
					+ "<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px; background-color : #e2d9d9; text-align: center;\"><p>시스템 이름</p></td>"
					+ "<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px; background-color : #e2d9d9; text-align: center;\"><p>ID로그</p></td>"
					+ "<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px; background-color : #e2d9d9; text-align: center;\"><p>시스템로그</p></td>"
					+ "</tr>";
			for (AllLogInq allLogInq : systemCount) {
				String bgColor = "none";
				if(allLogInq.getCnt()+allLogInq.getCnt2()==0) {bgColor = "#ff00003b";}
				body = body + "<tr>" 
							+ "<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px; background-color : "+bgColor+"\"><p>"+ allLogInq.getSystem_seq() +"</p></td>"
							+ "<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px; background-color : "+bgColor+"\"><p>"+ allLogInq.getSystem_name() +"</p></td>"
							+ "<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px; background-color : "+bgColor+"\"><p>"+ allLogInq.getCnt2() +"</p></td>" 
							+ "<td style=\"border: 1px solid rgb(0, 0, 0); width: 118.2px; height: 20px; background-color : "+bgColor+"\"><p>"+ allLogInq.getCnt() +"</p></td>" 
							+ "</tr>";
			}
			body = body + "</table>";
			sendMailInfo.setBody(body);
			sendMailInfo.setFrom_mail_address(parameters.get("id"));
			sendMailInfo.setFrom_mail_pass(parameters.get("pw"));
			sendMailInfo.setSend_mail_address(parameters.get("to_id"));
			
			try {
				sendEmail(sendMailInfo);
				//System.out.println("로그 체크 메일발송 성공");
				result = "success";
			} catch (AuthenticationFailedException e) {
				//System.out.println("메일 계정 인증 실패");
				result = "autherror";
			} catch (Exception e) {
				e.printStackTrace();
				//System.out.println("메일발송 실패");
				result = "error";
			}
		return result;
	}
	@Override
	public String sendSummonMail(AdminUser summonInfo,HttpServletRequest request) {
		String result = "success";
		
		String summonUrl = context_url;
		String fromName = smtp_fromName;
		String fromPhone = smtp_fromPhone;
		
		String log_delimiter = summonInfo.getLog_delimiter().equals("BA") ? "접속기록로그" : (summonInfo.getLog_delimiter().equals("DN") ? "다운로드로그" : "DB접근로그");
		String rule_view_type = summonInfo.getRule_view_type().equals("M") ? "처리량" : "이용량";
		
		SendMailInfo sendMailInfo = new SendMailInfo(); // 메일전송 정보 담아두는 vo
		sendMailInfo.setSubject("[" + summonInfo.getSystem_name() + "] 개인정보처리시스템 오남용 사용이력에 대한 사유 요청");
		
		String str1 = "<br><b>□ 추출조건정보</b><br><table border=\"1\" style=\"width: 800px; height: 200px;\">"
				+ "<tr bgcolor=\"#e7ecf1\">"
				+ "<th border=\"1\" width=\"15%\" style=\"text-align: center; vertical-align: middle;\">소명코드</th>"
				+ "<td border=\"1\" width=\"30%\" style=\"text-align: center; vertical-align: middle;\">"+summonInfo.getEmp_detail_seq()+"</td>"
				+ "<th border=\"1\" width=\"15%\" style=\"text-align: center; vertical-align: middle;\">발생년월일</th>"
				+ "<td border=\"1\" width=\"30%\" style=\"text-align: center; vertical-align: middle;\">"+summonInfo.getOccr_dt()+"</td>"
				+ "</tr>"
				+ "<tr>"
				+ "<th border=\"1\" width=\"15%\" style=\"text-align: center; vertical-align: middle;\">시나리오명</th>"
				+ "<td border=\"1\" width=\"30%\" style=\"text-align: center; vertical-align: middle;\">"+summonInfo.getScen_name()+"</td>"
				+ "<th border=\"1\" width=\"15%\" style=\"text-align: center; vertical-align: middle;\">상세시나리오명</th>"
				+ "<td border=\"1\" width=\"30%\" style=\"text-align: center; vertical-align: middle;\">"+summonInfo.getRule_nm()+"</td>"
				+ "</tr>"
				+ "<tr bgcolor=\"#e7ecf1\">"
				+ "<th border=\"1\" style=\"text-align: center; vertical-align: middle;\">취급자</th>"
				+ "<td border=\"1\" style=\"text-align: center; vertical-align: middle;\">"+summonInfo.getEmp_user_name()+"("+summonInfo.getEmp_user_id()+")</td>"
				+ "<th border=\"1\" style=\"text-align: center; vertical-align: middle;\">소속</th>"
				+ "<td border=\"1\" style=\"text-align: center; vertical-align: middle;\">"+summonInfo.getDept_name()+"</td>"
				+ "</tr>"
				+ "<tr>"
				+ "<th border=\"1\" style=\"text-align: center; vertical-align: middle;\">발생시스템</th>"
				+ "<td border=\"1\" style=\"text-align: center; vertical-align: middle;\">"+summonInfo.getSystem_name()+"</td>"
				+ "<th border=\"1\" style=\"text-align: center; vertical-align: middle;\">로그구분</th>"
				+ "<td border=\"1\" style=\"text-align: center; vertical-align: middle;\">"+log_delimiter+"</td>"
				+ "</tr>"
				+ "<tr bgcolor=\"#e7ecf1\">"
				+ "<th border=\"1\" style=\"text-align: center; vertical-align: middle;\">처리기준</th>"
				+ "<td border=\"1\" style=\"text-align: center; vertical-align: middle;\">"+rule_view_type+"</td>"
				+ "<th border=\"1\" style=\"text-align: center; vertical-align: middle;\">비정상걸린횟수</th>"
				+ "<td border=\"1\" style=\"text-align: center; vertical-align: middle;\">"+summonInfo.getRule_cnt()+"</td>"
				+ "</tr></table>"
				+ "<br><b>□ 법령 근거</b><br>" 
				+ "- 개인정보보호법 제29조 (안전조치의무), 동법 시행령 제30조(개인정보의 안전성 확보 조치)<br>"
				+ "- 개인정보의 안정성 확보조치 기준 제8조 (접속기록의 보관 및 점검)<br>"
				+ "- 개인정보 보호규칙 제28조(안전조치의무)<br>" 
				+ "- 신용정보법 제19조(신용정보전산시스템의 안전보호)<br>"
				+ "- 정보통신망법 제15조 (개인정보의 보호조치)<br>"
				+ "- 기술적·관리적 보호조치 기준 제5조 (접속기록의 위·변조방지)<br><br>"
				+ "<br>수신 대상자님은 개인정보 열람에 대한 비정상 의심 행위가 탐지되어<br>"
				+ "정당한 이유를 확인하고자 하오니 답변 부탁드립니다.<br><br>";
		
		String summon_option1 = "오남용 행위 사유 답변 페이지 바로가기(URL클릭) → <a href='" + summonUrl+ "'>답변 페이지 열기</a><br><br>";
		String summon_option2 = "오남용 행위 사유 답변 페이지 바로가기(URL클릭) → <a href='"+summonUrl+"/summonDetail2.do?summon_param="+extrtCondbyInqDao.encodeSummon_param(summonInfo)+"'>"
				+ "답변 페이지 열기</a><br><br>";
		String summon_option3 = "첨부파일을 다운받아 사유 및 응답시간 입력 후 회신주시기 바랍니다.<br>";
		
		String str2 = "	※ 메일 수신일 기준 14일 이내 사유를 등록해 주세요.</span><br>"
				+ "※ 문의사항이 있으시면 담당자에게 연락 주시기 바랍니다.<br>"
				+ "※ 문의: "+fromName+" "+fromPhone+"<br>";
		String body = "";
		
		//외부직원-본인 소명
		if(summonInfo.getExternal_staff() == 1) {	//외주 직원 첨부파일 답변
			try {
	            // 엑셀파일
				String path = request.getSession().getServletContext().getRealPath("/")+"WEB-INF/";
	            File file = new File(path+"excelExam/소명응답_업로드양식.xlsx");
	 
	            // 엑셀 파일 오픈
	            XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(file));
	            Cell cell = null;
	            
	            // 첫번재 sheet 내용 읽기
	            for (Row row : wb.getSheetAt(0)) { 
	                // 셋째줄부터..
	                if (row.getRowNum() == 3) {
	                	// 콘솔 출력
	                    cell = row.createCell(0); cell.setCellValue(summonInfo.getEmp_detail_seq());	//소명코드
	                    cell = row.createCell(1); cell.setCellValue(summonInfo.getEmp_user_id());	//응답자ID
	                    cell = row.createCell(2); cell.setCellValue(summonInfo.getSystem_name());	//응답자 시스템명
	                    cell = row.createCell(3); cell.setCellValue(""); //판정자
	                    cell = row.createCell(4); cell.setCellValue(""); //사유
	                }
	            }
	            // 엑셀 파일 저장
	            FileOutputStream fileOut = new FileOutputStream("소명응답_업로드양식.xlsx");
	            wb.write(fileOut);
	        } catch (FileNotFoundException fe) {
	        	logger.error("[FileNotFoundException]  : " + fe.getMessage());
	        } catch (IOException ie) {
	        	logger.error("[IOException]  : " + ie.getMessage());
	        }
			
			body = str1 + summon_option3 + str2;
			//sendMailInfo.setFileName("소명응답_업로드양식.xlsx");
		}else if("Y".equals(summonInfo.getSummon_reponse_type())){ //소명응답 페이지 접근 방식 1 (id,pw 입력 로그인)
			body = str1 + summon_option1 + str2;
		}else if("N".equals(summonInfo.getSummon_reponse_type())){ //소명응답 페이지 접근 방식 2 (emp_detail_seq 암복호화 후 답변 페이지 접근)
			body = str1 + summon_option2 + str2;
		}
		
		sendMailInfo.setBody(body);
		sendMailInfo.setFrom_mail_address(smtp_id);	//보내는 계정 ID
		sendMailInfo.setFrom_mail_pass(smtp_pw);	//보내는 계정 PWD
		sendMailInfo.setSend_mail_address(summonInfo.getEmail_address());	//받는사람
		
		
		try {
			sendEmail(sendMailInfo);
			//System.out.println("로그 체크 메일발송 성공");
			result = "success";
		} catch (AuthenticationFailedException e) {
			//System.out.println("메일 계정 인증 실패");
			result = "autherror";
		} catch (Exception e) {
			e.printStackTrace();
			//System.out.println("메일발송 실패");
			result = "error";
		}finally {
			File deleteFile = new File("/root/excelUpload/소명응답_업로드양식.xlsx");
			deleteFile.delete();
		}
		
		return result;
	}

	public void sendEmail(SendMailInfo sendMailInfo) throws Exception{
		String subject = sendMailInfo.getSubject(); 	// 메일 제목
		String body = sendMailInfo.getBody();		// 메일 내용
		String from_mail_address = sendMailInfo.getFrom_mail_address();	//보내는사람
		String send_mail_address = sendMailInfo.getSend_mail_address();	//받는사람
		String from_mail_pass = sendMailInfo.getFrom_mail_pass();

		Properties props = new Properties();
		
		props.put("mail.transprot.protocol", "smtp");
		props.put("mail.smtp.host", smtp_host);
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.port", smtp_port);
		props.put("mail.smtp.ssl.enable", "true");
		Authenticator auth = new MyAuthentication(from_mail_address, from_mail_pass);
		Session mailSession = null;
		try {
			mailSession = Session.getInstance(props, auth );
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (send_mail_address != null && !send_mail_address.equals("")) {
			Message msg = new MimeMessage(mailSession);
			// msg.setFrom(new InternetAddress(MimeUtility.encodeText(fromName, "UTF-8",
			// "B")));
			msg.setFrom(new InternetAddress(from_mail_address));
			InternetAddress to = new InternetAddress(send_mail_address);
			msg.setRecipient(Message.RecipientType.TO, to);
			msg.setSubject(subject);
			msg.setSentDate(new java.util.Date());
			
			Multipart multipart = new MimeMultipart();
			
			//메일 본분
			MimeBodyPart bodypart = new MimeBodyPart();
	        bodypart.setContent(body, "text/html;charset=euc-kr");
	        
	        //첨부파일이 있을 경우
	        if(sendMailInfo.getFileName() != null ) {
	        	MimeBodyPart attachFilePart = new MimeBodyPart();
		        String file_name = sendMailInfo.getFileName();
		        attachFilePart.setFileName(MimeUtility.encodeText(file_name, "EUC_KR", "B"));
		        FileDataSource filedataSource = new FileDataSource(file_name);
		        DataHandler dataHandler = new DataHandler(filedataSource);
		        attachFilePart.setDataHandler(dataHandler);
		        multipart.addBodyPart(attachFilePart);
	        }
	        
	        multipart.addBodyPart(bodypart);
			msg.setContent(multipart);
			//msg.setContent(body, "text/html;charset=euc-kr");
			//msg.setHeader("content-Type", "text/html");

			Transport.send(msg);
		}
	}
	
	public class MyAuthentication extends Authenticator {

		PasswordAuthentication pa;

		public MyAuthentication() { 
			
			// ID와 비밀번호를 입력한다.
			pa = new PasswordAuthentication("easycerti087@gmail.com", "dlwltjxl1!");
		}
		
		public MyAuthentication(String id, String pass) {
			pa = new PasswordAuthentication(id, pass);
		}

		// 시스템에서 사용하는 인증정보
		public PasswordAuthentication getPasswordAuthentication() {
			return pa;
		}
	}
}
