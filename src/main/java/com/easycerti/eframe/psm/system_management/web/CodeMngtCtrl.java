package com.easycerti.eframe.psm.system_management.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.psm.system_management.service.CodeMngtSvc;

/**
 * 코드 관련 Controller
 * 
 * @author yjyoo
 * @since 2015. 4. 17.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 17.           yjyoo            최초 생성
 *
 * </pre>
 */
@Controller
@RequestMapping(value="/codeMngt/*")
public class CodeMngtCtrl {
	
	@Autowired
	private CodeMngtSvc codeMngtSvc;
	
	/**
	 * 코드 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="list.html",method={RequestMethod.POST})
	public DataModelAndView findCodeMngtList(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception {
		String[] args = {"group_code_id"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)) {
			throw new Exception();
		}
		
		DataModelAndView modelAndView = codeMngtSvc.findCodeMngtList(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	/**
	 * 코드 상세정보
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="detail.html",method={RequestMethod.POST})
	public DataModelAndView findCodeMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception {
		String[] args = {"group_code_id"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)) {
			throw new Exception();
		}
		
		DataModelAndView modelAndView = codeMngtSvc.findCodeMngtOne(parameters);
		modelAndView.setViewName("codeMngtDetail");
		
		return modelAndView;
	}
	
	/**
	 * 코드 추가 (json)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="add.html",method={RequestMethod.POST})
	public DataModelAndView addCodeMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception {
		String[] args = {"group_code_id", "code_id", "code_name", "use_flag", "code_type"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)) {
			throw new ESException("SYS004J");
		}
		
		parameters.put("insert_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		DataModelAndView modelAndView = codeMngtSvc.addCodeMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	/**
	 * 코드 수정 (json)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="save.html",method={RequestMethod.POST})
	public DataModelAndView saveCodeMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception {
		String[] args = {"group_code_id", "code_id", "code_name", "use_flag", "code_type"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)) {
			throw new ESException("SYS004J");
		}
		
		parameters.put("update_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		DataModelAndView modelAndView = codeMngtSvc.saveCodeMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	/**
	 * 코드 삭제 (json)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="remove.html",method={RequestMethod.POST})
	public DataModelAndView removeCodeMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception {
		String[] args = {"code_id",  "group_code_id"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)) {
			throw new ESException("SYS004J");
		}
		
		DataModelAndView modelAndView = codeMngtSvc.removeCodeMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
}
