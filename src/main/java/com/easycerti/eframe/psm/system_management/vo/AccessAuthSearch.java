package com.easycerti.eframe.psm.system_management.vo;


import java.util.ArrayList;
import java.util.List;

import com.easycerti.eframe.common.vo.SearchBase;

public class AccessAuthSearch extends SearchBase {
	
	// 사원 ID
	private String emp_user_id;
	
	// 시스템 번호
	private String system_seq;
	
	// 직원 이름
	private String emp_user_name;
	
	// 상태
	private String status;
	
	private String accessDateType;
	
	private String manual_flag;
	private int seq;
	
	// 관리자 정보
	private String approver;
	private String approver_id;
	private String approver_ip;

	List<String> auth_idsList;
	
	
	
	

	public List<String> getAuth_idsList() {
		return auth_idsList;
	}

	public void setAuth_idsList(List<String> auth_idsList) {
		this.auth_idsList = auth_idsList;
	}

	public String getApprover() {
		return approver;
	}

	public void setApprover(String approver) {
		this.approver = approver;
	}

	public String getApprover_id() {
		return approver_id;
	}

	public void setApprover_id(String approver_id) {
		this.approver_id = approver_id;
	}

	public String getApprover_ip() {
		return approver_ip;
	}

	public void setApprover_ip(String approver_ip) {
		this.approver_ip = approver_ip;
	}

	List<String> sysList = new ArrayList<String>();
	
	public List<String> getSysList() {
		return sysList;
	}

	public void setSysList(List<String> sysList) {
		this.sysList = sysList;
	}

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public String getManual_flag() {
		return manual_flag;
	}

	public void setManual_flag(String manual_flag) {
		this.manual_flag = manual_flag;
	}

	public String getAccessDateType() {
		return accessDateType;
	}

	public void setAccessDateType(String accessDateType) {
		this.accessDateType = accessDateType;
	}

	public String getEmp_user_name() {
		return emp_user_name;
	}

	public void setEmp_user_name(String emp_user_name) {
		this.emp_user_name = emp_user_name;
	}


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	

	public String getEmp_user_id() {
		return emp_user_id;
	}

	public void setEmp_user_id(String emp_user_id) {
		this.emp_user_id = emp_user_id;
	}

	public String getSystem_seq() {
		return system_seq;
	}

	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}

	@Override
	public String toString() {
		return "EmpUserSearch [emp_user_id=" + emp_user_id
				+ ", emp_user_name=" + emp_user_name
				+ ", status=" + status + "]";
	}	
}
