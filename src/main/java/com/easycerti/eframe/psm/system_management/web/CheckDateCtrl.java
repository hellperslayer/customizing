package com.easycerti.eframe.psm.system_management.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.system_management.service.CheckDateSvc;

@Controller
@RequestMapping(value="/checkDate/*")
public class CheckDateCtrl {
	
	@Autowired
	private CheckDateSvc checkDateSvc;
	
	@RequestMapping(value="list.html",method={RequestMethod.POST})
	public DataModelAndView checkDateList(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		DataModelAndView modelAndView = checkDateSvc.checkDateList(parameters);
		
		modelAndView.setViewName("checkDateList");
		return modelAndView;
	
	}	
}
