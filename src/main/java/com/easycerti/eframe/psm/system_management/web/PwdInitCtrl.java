package com.easycerti.eframe.psm.system_management.web;

import java.util.Properties;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.psm.scheduler.web.LogCountCheck_Scheduler.MyAuthentication;
import com.easycerti.eframe.psm.search.dao.AllLogInqDao;
import com.easycerti.eframe.psm.search.dao.ExtrtCondbyInqDao;
import com.easycerti.eframe.psm.system_management.dao.AgentMngtDao;
import com.easycerti.eframe.psm.system_management.dao.AlarmMngtDao;
import com.easycerti.eframe.psm.system_management.vo.SendMailInfo;

@Controller
public class PwdInitCtrl {
	
	@Autowired
	private AllLogInqDao allLogInqDao;

	@Value("#{configProperties.smtp_host}")
	private String smtp_host;
	
	@Value("#{configProperties.smtp_port}")
	private String smtp_port;
	
	@Value("#{versionProperties.version}")
	private String version;
	
	@RequestMapping(value = "chkEmail.html", method={RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public String emailChk(@RequestParam("id") String id) throws Exception{
		
		String tmpPwd = ""; //임시비밀번호
		
		int pwdLength = 8;
		char[] passwordTable =  { 
				'1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
				'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 
                'w', 'x', 'y', 'z','A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 
                'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                'Y', 'Z','!', '@', '#', '$', '%', '^', '&', '*', '(', ')'
                };
		char[] num = {
				'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'
		};
		
		char[] eng = {
				'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 
                'w', 'x', 'y', 'z'
		};
		
		char[] beng = {
				'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 
                'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                'Y', 'Z'
		};
		char[] emo = {
				'!', '@', '#', '$', '%', '^', '&', '*', '(', ')'
		};
		
		
		Random random = new Random(System.currentTimeMillis());
//        int tablelength = passwordTable.length;
        StringBuffer buf = new StringBuffer();
     
        for(int i = 0; i < 4; i++) {
            buf.append(passwordTable[random.nextInt(passwordTable.length)]);
        }
        
        for(int i = 0; i < 1; i++) {
            buf.append(num[random.nextInt(num.length)]);
        }
        
        for(int i = 0; i < 1; i++) {
            buf.append(eng[random.nextInt(eng.length)]);
        }
        
        for(int i = 0; i < 1; i++) {
            buf.append(beng[random.nextInt(beng.length)]);
        }
        
        for(int i = 0; i < 1; i++) {
            buf.append(emo[random.nextInt(emo.length)]);
        }
        
        tmpPwd = buf.toString();
        
		String email = allLogInqDao.emailChk(id);
		
		if(email != null || email != "") {
			int result = allLogInqDao.updateTmpPwd(id,tmpPwd);
		}
		
		sendEmail(email,tmpPwd);

		
		if(email != null) {
			return email;
		}else {
			return "1";
		}
		
	}
	
	public void sendEmail(String email, String tmpPwd) throws Exception{
		
		SendMailInfo sendMailInfo = new SendMailInfo();
		
		String subject = "PSM 임시 비밀번호 안내"; 	// 메일 제목
		String body = "임시 비밀번호는 [ "+tmpPwd+" ]입니다.";		// 메일 내용
		String from_mail_address = "qa_sender@easycerti.com";	//보내는사람
		String send_mail_address = email;	//받는사람
		String from_mail_pass = "dlwl5501!"; //보내는 사람 패스워드

		Properties props = new Properties();
		
		props.put("mail.transprot.protocol", "smtp");
		props.put("mail.smtp.host", smtp_host);
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.port", smtp_port);
		props.put("mail.smtp.ssl.enable", "true");
		Authenticator auth = new MyAuthentication(from_mail_address, from_mail_pass);
		Session mailSession = null;
		try {
			mailSession = Session.getInstance(props, auth);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (send_mail_address != null && !send_mail_address.equals("")) {
			Message msg = new MimeMessage(mailSession);
			// msg.setFrom(new InternetAddress(MimeUtility.encodeText(fromName, "UTF-8",
			// "B")));
			msg.setFrom(new InternetAddress(from_mail_address));
			InternetAddress to = new InternetAddress(send_mail_address);
			msg.setRecipient(Message.RecipientType.TO, to);
			msg.setSubject(subject);
			msg.setSentDate(new java.util.Date());
			msg.setContent(body, "text/html;charset=euc-kr");
			msg.setHeader("content-Type", "text/html");

			try {
				Transport.send(msg);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public class MyAuthentication extends Authenticator {

		PasswordAuthentication pa;

		public MyAuthentication() { 
			
			// ID와 비밀번호를 입력한다.
			pa = new PasswordAuthentication("qa_sender@easycerti.com", "dlwl5501!");
		}
		
		public MyAuthentication(String id, String pass) {
			pa = new PasswordAuthentication(id, pass);
		}

		// 시스템에서 사용하는 인증정보
		public PasswordAuthentication getPasswordAuthentication() {
			return pa;
		}
	}

}
