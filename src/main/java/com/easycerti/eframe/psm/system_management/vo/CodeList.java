package com.easycerti.eframe.psm.system_management.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.AbstractValueObject;
import com.easycerti.eframe.psm.control.vo.Code;

/**
 * 코드 리스트 VO
 * 
 * @author yjyoo
 * @since 2015. 4. 17.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 17.           yjyoo            최초 생성
 *
 * </pre>
 */
public class CodeList extends AbstractValueObject {
	
	// 코드 리스트
	private List<Code> codes = null;

	public CodeList() {}
	
	public CodeList(List<Code> codes){
		this.codes = codes;
	}
	
	public CodeList(List<Code> codes, String count){
		this.codes = codes;
		this.setPage_total_count(count);
	}
	
	public List<Code> getCodes() {
		return codes;
	}

	public void setCodes(List<Code> codes) {
		this.codes = codes;
	}
}
