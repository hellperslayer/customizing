package com.easycerti.eframe.psm.system_management.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.easycerti.eframe.common.spring.DataModelAndView;

public interface IpPermSvc {
	
	public DataModelAndView ipPermList(Map<String, String> parameters);
	public DataModelAndView ipPermDetail(Map<String, String> parameters, HttpServletRequest request);
	public DataModelAndView addIpPerm(Map<String, String> parameters);
	public DataModelAndView saveIpPerm(Map<String, String> parameters);
	public DataModelAndView removeIpPerm(Map<String, String> parameters);

}
