package com.easycerti.eframe.psm.system_management.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.system_management.vo.AccessAuthSearch;

public interface AccessAuthSvc {
	public DataModelAndView findAccessAuthList(AccessAuthSearch search);
	
	public DataModelAndView findAccessAuthDetail(AccessAuthSearch search);
	
	public String upLoadAccessAuth(Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response)throws Exception;
	
	public void findAccessAuthList_download(DataModelAndView modelAndView, AccessAuthSearch search, HttpServletRequest request);
	
	public DataModelAndView addAccessAuth(Map<String, String> parameters);
	public DataModelAndView saveAccessAuth(Map<String, String> parameters);
	public DataModelAndView removeAccessAuth(Map<String, String> parameters);
}
