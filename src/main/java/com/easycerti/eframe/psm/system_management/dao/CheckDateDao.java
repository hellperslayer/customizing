package com.easycerti.eframe.psm.system_management.dao;

import com.easycerti.eframe.psm.system_management.vo.CheckDate;

public interface CheckDateDao {
	public String checkDateList(CheckDate checkDate);
}
