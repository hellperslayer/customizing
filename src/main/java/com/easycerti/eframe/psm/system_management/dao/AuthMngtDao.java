package com.easycerti.eframe.psm.system_management.dao;

import java.util.List;

import com.easycerti.eframe.psm.system_management.vo.AdminUser;
import com.easycerti.eframe.psm.system_management.vo.Auth;

/**
 * 권한 관련 Dao Interface
 * 
 * @author yjyoo
 * @since 2015. 4. 20.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 20.           yjyoo            최초 생성
 *
 * </pre>
 */
public interface AuthMngtDao {

	/**
	 * 권한 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return List<Auth>
	 */
	public List<Auth> findAuthMngtList(Auth auth);
	
	/**
	 * 권한 리스트 갯수
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return Integer
	 */
	public Integer findAuthMngtOne_count(Auth auth);
	
	/**
	 * 권한 상세정보
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return Auth
	 */
	public Auth findAuthMngtOne(Auth auth);
	
	/**
	 * 해당 관리자의 권한정보
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return Auth
	 */
	public Auth findAuthMngtOne_fromUser(AdminUser adminUser);

	/**
	 * 권한 추가 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return void
	 */
	public void addAuthMngtOne(Auth auth);
	
	/**
	 * 권한 수정 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return void
	 */
	public void saveAuthMngtOne(Auth auth);
	
	/**
	 * 권한 삭제 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return void
	 */
	public void removeAuthMngtOne(Auth auth);
}
