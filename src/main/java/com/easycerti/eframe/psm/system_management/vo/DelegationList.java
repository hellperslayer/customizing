package com.easycerti.eframe.psm.system_management.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.AbstractValueObject;

public class DelegationList extends AbstractValueObject{
	
	/** 20180314 소명판정 결제자 위임 **/
	private List<Delegation> delegation = null;
	
	public DelegationList() {
		
	}
	
	public DelegationList(List<Delegation> delegation) {
		this.delegation = delegation;
	}
	
	public DelegationList(List<Delegation> delegation, String page_total_count) {
		this.delegation = delegation;
		setPage_total_count(page_total_count);
	}
	public List<Delegation> getDelegation() {
		return delegation;
	}

	public void setDelegation(List<Delegation> delegation) {
		this.delegation = delegation;
	}
	

}
