package com.easycerti.eframe.psm.system_management.web;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.psm.system_management.service.ReportOptionSvc;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;

@Controller
@RequestMapping("/reportOption/*")
public class ReportOptionCtrl {

	@Autowired
	private ReportOptionSvc reportOptionSvc;


	@Value("#{configProperties.use_studentId}")
	private String use_studentId;

	@Value("#{configProperties.excel_type}")
	private String excel_type;

	@Value("#{versionProperties.version}")
	private String version;

	@RequestMapping(value = "list.html", method = { RequestMethod.POST, RequestMethod.GET })
	public DataModelAndView reportOption(@RequestParam Map<String, String> parameters, HttpServletRequest request) {

		AdminUser adminUser = SystemHelper.getAdminUserInfo(request);
		String auth = adminUser.getAuth_id();

		parameters = CommonHelper.checkSearchDate(parameters);
		DataModelAndView modelAndView = reportOptionSvc.reportOptionList(parameters, request);
		
		modelAndView.addObject("auth", auth);
		modelAndView.addObject("version", version);
		modelAndView.setViewName("reportOption");
		
		return modelAndView;
	}
	
	//일반사항 업데이트
	@RequestMapping(value = "updGeneral.html", method = { RequestMethod.POST })
	public DataModelAndView updGeneral(@RequestParam Map<String, String> parameters, HttpServletRequest request) {

		DataModelAndView modelAndView = reportOptionSvc.updGeneral(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	
	@MngtActHist(log_action = "INSERT", log_message = "[고객관리] 로고 이미지 등록 ")
	@RequestMapping(value = "upload.html", method = { RequestMethod.POST })
	public DataModelAndView uploadFile(@RequestParam Map<String, String> parameters,
			@RequestParam("filename") MultipartFile file, HttpServletRequest request) {

		DataModelAndView modelAndView = reportOptionSvc.addFile(file, request);
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	
	@MngtActHist(log_action = "INSERT", log_message = "[고객관리] 점검보고서 로고 이미지 등록 ")
	@RequestMapping(value = "upload2.html", method = { RequestMethod.POST })
	public DataModelAndView uploadFile2(@RequestParam Map<String, String> parameters,
			@RequestParam("filename2") MultipartFile file, HttpServletRequest request) {

		DataModelAndView modelAndView = reportOptionSvc.addFile2(file, request);
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}

	@MngtActHist(log_action = "UPDATE", log_message = "[고객관리] 로고이미지 사용여부 설정")
	@RequestMapping(value = "useLogoImage.html", method = { RequestMethod.POST })
	public DataModelAndView useLogoImage(@RequestParam Map<String, String> parameters, @RequestParam("idx") int idx,
			@RequestParam("type") int type) {

		reportOptionSvc.useLogoImage(idx, type);

		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}

	@MngtActHist(log_action = "UPDATE", log_message = "[고객관리] 로고이미지 사용여부 설정")
	@RequestMapping(value = "useLogoImage2.html", method = { RequestMethod.POST })
	public DataModelAndView useLogoImage2(@RequestParam Map<String, String> parameters, @RequestParam("idx") int idx,
			@RequestParam("type") int type) {

		reportOptionSvc.useLogoImage2(idx, type);

		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}

	@MngtActHist(log_action = "DELETE", log_message = "[고객관리] 로고이미지 삭제")
	@RequestMapping(value = "deleteLogoImage.html", method = { RequestMethod.POST })
	public DataModelAndView deleteLogoImage(@RequestParam Map<String, String> parameters,
			@RequestParam("idx") int idx) {

		reportOptionSvc.deleteLogoImage(idx);

		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}

	@MngtActHist(log_action = "UPDATE", log_message = "[고객관리] 결제라인 수정")
	@ResponseBody
	@RequestMapping(value = "addAuthorizeLine.html", method = { RequestMethod.POST })
	public String addAuthorizeLine(@RequestParam Map<String, String> parameters) {
		return reportOptionSvc.addAuthorizeLine(parameters);
	}

	@MngtActHist(log_action = "UPDATE", log_message = "[고객관리] 점검보고서 총평 기본문구 수정")
	@ResponseBody
	@RequestMapping(value = "addDefaultDesc.html", method = { RequestMethod.POST })
	public String addDefaultDesc(@RequestParam Map<String, String> parameters) {
		return reportOptionSvc.addDefaultDesc(parameters);
	}

	@ResponseBody
	@RequestMapping(value = "findDefaultDesc.html", method = { RequestMethod.POST })
	public String findDefaultDesc(@RequestParam Map<String, String> parameters) {
		return reportOptionSvc.findDefaultDesc(parameters);
	}

	public String getCurrentDayTime() {
		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyyMMdd-HH-mm-ss", Locale.KOREA);
		return dayTime.format(new Date(time));
	}
	
	//점검보고서 결재라인 업데이트
	@ResponseBody
	@RequestMapping(value = "updReportApproval.html", method = { RequestMethod.POST })
	public String updReportApproval(@RequestParam Map<String, String> parameters) {
		return reportOptionSvc.updReportApproval(parameters);
	}
	
	
	//점검보고서 결재라인 일괄업데이트
	@RequestMapping(value = "wholeUpdReportApproval.html", method = { RequestMethod.POST })
	public DataModelAndView wholeUpdReportApproval(@RequestParam Map<String, String> parameters, HttpServletRequest request) {


		DataModelAndView modelAndView = reportOptionSvc.wholeUpdReportApproval(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	

}

