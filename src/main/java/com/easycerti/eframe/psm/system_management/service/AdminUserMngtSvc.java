package com.easycerti.eframe.psm.system_management.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.system_management.vo.AdminUserSearch;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

/**
 * 관리자 관련 Service Interface
 * 
 * @author yjyoo
 * @since 2015. 4. 20.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 20.           yjyoo           최초 생성
 *   2015. 5. 22.			yjyoo			엑셀 다운로드 추가
 *
 * </pre>
 */
public interface AdminUserMngtSvc {
	
	/**
	 * 관리자 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	public DataModelAndView findAdminUserMngtList(AdminUserSearch search, HttpServletRequest request);

	/**
	 * 관리자 리스트 엑셀 다운로드
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 22.
	 */
	public void findAdminUserMngtList_download(DataModelAndView modelAndView, AdminUserSearch search, HttpServletRequest request);
	
	public DataModelAndView checkTime();
	/**
	 * 관리자 상세정보
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	public DataModelAndView findAdminUserMngtOne(String admin_user_id, HttpServletRequest request);
	
	/**
	 * 관리자 추가 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	public DataModelAndView addAdminUserMngtOne(Map<String, String> parameters);
	
	/**
	 * 관리자 수정 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	public DataModelAndView saveAdminUserMngtOne(Map<String, String> parameters);
	
	/**
	 * 관리자 삭제 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	public DataModelAndView removeAdminUserMngtOne(Map<String, String> parameters);
	
	/**
	 * 관리자 로그인
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	public DataModelAndView login(Map<String, String> parameters, HttpServletRequest request);
	
	/**
	 * 관리자 로그아웃
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	public DataModelAndView logout(Map<String, String> parameters, HttpServletRequest request) throws Exception;

	/**
	 * 관리자 비밀번호 변경
	 * @author tjlee
	 * @since 2015. 7. 03.
	 * @return DataModelAndView
	 */
	public DataModelAndView rePassword(Map<String, String> parameters, HttpServletRequest request) throws Exception;
	public DataModelAndView changePassword(Map<String, String> parameters, HttpServletRequest request) throws Exception;

	public List<SystemMaster> getSystemMaster();

	public DataModelAndView adminUserSysList(Map<String, String> parameters);

	public List<HashMap<String, Object>> emailChk(String id);
	
	
	

	
}
