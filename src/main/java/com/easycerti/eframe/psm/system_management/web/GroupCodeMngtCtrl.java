package com.easycerti.eframe.psm.system_management.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.psm.system_management.service.GroupCodeMngtSvc;

/**
 * 그룹코드 관련 Controller
 * 
 * @author yjyoo
 * @since 2015. 4. 17.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 17.           yjyoo            최초 생성
 *
 * </pre>
 */

@Controller
@RequestMapping(value="/groupCodeMngt/*")
public class GroupCodeMngtCtrl {
	
	@Autowired
	private GroupCodeMngtSvc groupCodeMngtSvc;
	
	/**
	 * 그룹코드 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[환경관리] 코드관리")
	@RequestMapping(value="list.html",method={RequestMethod.POST})
	public DataModelAndView findGroupCodeMngtList(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		DataModelAndView modelAndView = groupCodeMngtSvc.findGroupCodeMngtList(parameters);
		modelAndView.setViewName("groupCodeMngtList");
		
		return modelAndView;
	}
	
	/**
	 * 그룹코드 상세
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="detail.html",method={RequestMethod.POST})
	public DataModelAndView findGroupCodeMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		DataModelAndView modelAndView = groupCodeMngtSvc.findGroupCodeMngtOne(parameters);
		modelAndView.setViewName("groupCodeMngtDetail");
		
		return modelAndView;
	}
	
	/**
	 * 그룹코드 추가 (json)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="add.html",method={RequestMethod.POST})
	public DataModelAndView addGroupCodeMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		String[] args = {"group_code_id", "group_code_name", "use_flag"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		parameters.put("insert_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		DataModelAndView modelAndView = groupCodeMngtSvc.addGroupCodeMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	/**
	 * 그룹코드 수정 (json)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="save.html",method={RequestMethod.POST})
	public DataModelAndView saveGroupCodeMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		String[] args = {"group_code_id", "group_code_name", "use_flag"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		parameters.put("update_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		DataModelAndView modelAndView = groupCodeMngtSvc.saveGroupCodeMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	/**
	 * 그룹코드 삭제 (json)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="remove.html",method={RequestMethod.POST})
	public DataModelAndView removeGroupCodeMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		String[] args = {"group_code_id"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		DataModelAndView modelAndView = groupCodeMngtSvc.removeGroupCodeMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	} 

}
