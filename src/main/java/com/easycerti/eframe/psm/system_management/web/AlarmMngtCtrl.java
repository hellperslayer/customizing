package com.easycerti.eframe.psm.system_management.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.system_management.service.AlarmMngtSvc;

/**
 * 옵션 설정 Controller
 */

@Controller
@RequestMapping(value="/alarmMngt/*")
public class AlarmMngtCtrl {
	
	@Autowired
	AlarmMngtSvc alarmMngtSvc;
	
	@MngtActHist(log_action = "SELECT", log_message = "[환경관리] 알람연동설정")
	@RequestMapping(value="list.html",method={RequestMethod.POST})
	public DataModelAndView findAlarmMngtList(@RequestParam Map<String, String> parameters, HttpServletRequest request){

		DataModelAndView modelAndView = alarmMngtSvc.findAlarmMngtList(parameters);
		modelAndView.setViewName("alarmMngtList");
		
		return modelAndView;
	}
	
	@MngtActHist(log_action = "UPDATE", log_message = "[환경관리] 알람연동설정 수정")
	@RequestMapping(value="setAlarmMngt.html",method={RequestMethod.POST})
	public DataModelAndView setAlarmMngt(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		
		DataModelAndView modelAndView = alarmMngtSvc.setAlarmMngt(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@MngtActHist(log_action = "UPDATE", log_message = "[에이전트모니터링] 메일서버 수정")
	@RequestMapping(value="setAlarmMngt_email.html",method={RequestMethod.POST})
	public DataModelAndView setAlarmMngt_email(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		
		DataModelAndView modelAndView = alarmMngtSvc.setAlarmMngt_email(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}

	@RequestMapping(value="testmail.html",method={RequestMethod.POST})
	@ResponseBody
	public String testmail(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		String result = alarmMngtSvc.sendMail(parameters);
		return result;
	}
}
