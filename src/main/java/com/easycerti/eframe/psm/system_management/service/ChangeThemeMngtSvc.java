package com.easycerti.eframe.psm.system_management.service;

import java.util.Map;

import com.easycerti.eframe.common.spring.DataModelAndView;

public interface ChangeThemeMngtSvc {

	DataModelAndView changeThemeList(Map<String, String> parameters);

	DataModelAndView changeThemeUpdate(Map<String, String> parameters);

}
