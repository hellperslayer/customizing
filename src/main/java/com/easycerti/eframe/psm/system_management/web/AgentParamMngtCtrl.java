package com.easycerti.eframe.psm.system_management.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.system_management.service.AgentParamMngtSvc;

/**
 * 관리자 관련 Controller
 * 
 * @author yjyoo
 * @since 2015. 4. 20.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 20.           yjyoo           최초 생성
 *   2015. 5. 22.			yjyoo			엑셀 다운로드 추가
 *
 * </pre>
 */
@Controller
@RequestMapping(value="/agentParamMngt/*")
public class AgentParamMngtCtrl {

	@Autowired
	private AgentParamMngtSvc adminUserMngtSvc;
	
	/**
	 * 관리자 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="list.html",method={RequestMethod.POST})
	public DataModelAndView findAgentParamMngtList(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		DataModelAndView modelAndView = adminUserMngtSvc.findAgentParamMngtList(parameters);

		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName("agentParamMngtList");
		
		return modelAndView;
	}
	
	/**
	 * 관리자 상세정보
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="detail.html",method={RequestMethod.POST})
	public DataModelAndView findAgentParamMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		DataModelAndView modelAndView = adminUserMngtSvc.findAgentParamMngtOne(parameters);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.setViewName("agentParamMngtDetail");
		
		return modelAndView;
	}
	
	/**
	 * 관리자 추가 (json)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	/*@RequestMapping(value="add.html",method={RequestMethod.POST})
	public DataModelAndView addAgentParamMngtOne(@RequestParam Map<String, String> parameters,HttpServletRequest request) throws Exception{
		//String[] args = {"admin_user_id", "password", "admin_user_name"};
		String[] args = {"admin_user_id", "admin_user_name"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		parameters.put("insert_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		DataModelAndView modelAndView = adminUserMngtSvc.addAgentParamMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	*//**
	 * 관리자 수정 (json)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="save.html",method={RequestMethod.POST})
	public DataModelAndView saveAgentParamMngtOne(@RequestParam Map<String, String> parameters,HttpServletRequest request) throws Exception{
		DataModelAndView modelAndView = adminUserMngtSvc.saveAgentParamMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	/**
	 * 관리자 삭제 (json)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 *//*
	@RequestMapping(value="remove.html",method={RequestMethod.POST})
	public DataModelAndView removeAgentParamMngtOne(@RequestParam Map<String, String> parameters,HttpServletRequest request) throws Exception{
		String[] args = {"admin_user_id"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		parameters.put("update_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		DataModelAndView modelAndView = adminUserMngtSvc.removeAgentParamMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}*/
}
