package com.easycerti.eframe.psm.system_management.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;


/**
 * 옵션 설정 Service Interface
 */

public interface AlarmMngtSvc {

	public DataModelAndView findAlarmMngtList(Map<String, String> parameters);
	
	public DataModelAndView setAlarmMngt(Map<String, String> parameters);
	
	public DataModelAndView setAlarmMngt_email(Map<String, String> parameters);

	public String sendMail(Map<String, String> parameters);
	
	public String sendSummonMail(AdminUser summonInfo,HttpServletRequest request);
	
}
