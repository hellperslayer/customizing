package com.easycerti.eframe.psm.system_management.vo;

import java.util.Date;

import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 대상에이전트 VO
 * 
 * @author yjyoo
 * @since 2015. 4. 24.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 24.           yjyoo            최초 생성
 *
 * </pre>
 */
public class SendMailInfo extends AbstractValueObject {
	
	private String type_name;			//(agent_status:에이전트 상태, summon_request:소명요청)
	private String subject;				//메일 제목
	private String body;				//메일 내용
	private String from_mail_address;	//보내는 사람
	private String from_mail_pass;		//보내는 사람 패스워드
	private String send_mail_address;	//받는 사람
	private String send_type;			//발송유형 (mail,sms,message)
	private String send_yn;				//발송유무
	private String use_yn;				//사용유무
	private int flag;					//발송flag(0:발송X, 1:1번 발송, 2:2번발송)
	private String send_dt;				//발송 날짜
	private Date send_timestamp;		//발송 날짜,시간
	private String fileName;			//첨부파일명
	
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFrom_mail_pass() {
		return from_mail_pass;
	}
	public void setFrom_mail_pass(String from_mail_pass) {
		this.from_mail_pass = from_mail_pass;
	}
	public String getSend_dt() {
		return send_dt;
	}
	public void setSend_dt(String send_dt) {
		this.send_dt = send_dt;
	}
	public Date getSend_timestamp() {
		return send_timestamp;
	}
	public void setSend_timestamp(Date send_timestamp) {
		this.send_timestamp = send_timestamp;
	}
	public String getUse_yn() {
		return use_yn;
	}
	public void setUse_yn(String use_yn) {
		this.use_yn = use_yn;
	}
	public String getType_name() {
		return type_name;
	}
	public void setType_name(String type_name) {
		this.type_name = type_name;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getFrom_mail_address() {
		return from_mail_address;
	}
	public void setFrom_mail_address(String from_mail_address) {
		this.from_mail_address = from_mail_address;
	}
	public String getSend_mail_address() {
		return send_mail_address;
	}
	public void setSend_mail_address(String send_mail_address) {
		this.send_mail_address = send_mail_address;
	}
	public String getSend_type() {
		return send_type;
	}
	public void setSend_type(String send_type) {
		this.send_type = send_type;
	}
	public String getSend_yn() {
		return send_yn;
	}
	public void setSend_yn(String send_yn) {
		this.send_yn = send_yn;
	}
	public int getFlag() {
		return flag;
	}
	public void setFlag(int flag) {
		this.flag = flag;
	}
	
	
}
