package com.easycerti.eframe.psm.system_management.dao;

import java.util.List;

import com.easycerti.eframe.psm.system_management.vo.AdminUser;
import com.easycerti.eframe.psm.system_management.vo.AgentParam;
import com.easycerti.eframe.psm.system_management.vo.System;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

/**
 * 관리자 관련 Dao Interface
 * 
 * @author yjyoo
 * @since 2015. 4. 20.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 20.           yjyoo          최초 생성
 *
 * </pre>
 */
public interface AgentParamMngtDao {
	
	/**
	 * 관리자 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return List<AdminUser>
	 */
	public List<AgentParam> findAgentParamMngtList();

	/**
	 * 관리자 상세정보
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return AdminUser
	 */
	public List<AgentParam> findAgentParamMngtOne(AgentParam agentParam);
	
	/**
	 * 관리자 추가 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return void
	 */
	public void addAgentParamMngtOne(AdminUser adminUser);

	/**
	 * 관리자 수정 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return void
	 */
	public void saveAgentParamMngtOne(AgentParam agentParam);
	
	/**
	 * 관리자 삭제 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return void
	 */
	public void removeAgentParamMngtOne(AdminUser adminUser);
	
	/**
	 * 관리자 로그인
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return AdminUser
	 */
	public AdminUser findAgentParamMngtOne_forLogin(AdminUser adminUser);
	
	/**
	 * 관리자 로그인 처리 시 관리자 비밀번호 확인 값, 로그인 실패 시간 수정
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 7.
	 * @return void
	 */
	public void saveAgentParamMngtOne_login(AdminUser adminUser);

	/**
	 * 권한 삭제시 관리자의 권한 Null 처리 (수정)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return void
	 */
	public void saveAgentParamMngtOne_toRemoveAuth(AdminUser adminUser);
	
	/**
	 * 부서 삭제시 관리자의 부서 Null 처리 (수정)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return void
	 */
	public void saveAgentParamMngtOne_toRemoveDept(AdminUser adminUser);
	
	/**
	 * 특정 권한을 가지고 있는 관리자 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 21.
	 * @return List<AdminUser>
	 */
	public List<AdminUser> findAgentParamMngtList_fromAuthId(AdminUser adminUser);

	/**
	 * 관리자 로그인 비밀번호 변경
	 * 
	 * @author tjlee
	 * @since 2015. 7. 03.
	 */
	public void saveAgentParamMngtRePassword(AdminUser adminUser);
	
	public String checktime();

	public List<SystemMaster> getSystemMaster();

	public String getUserAuthIds(String admin_user_id);
	
	/**
	 * 20180314 sysong
	 * 위임자 체크
	 */
	 
	public String delegationCheckterm(AdminUser adminUser);
	public String delegationCheckUser(AdminUser adminUser);
	
	public int findAdminUserBySystemSeq(System system);
	
	/**
	 * 로그인 할 때 마지막 로그인시간이 6개월 이전이면 계정잠금
	 * 
	 * @author hbjang
	 * @since 2018. 11. 2.
	 * @return void
	 */
	public void saveAgentParamMngtOne_toUserLock(AdminUser adminUser);
}
