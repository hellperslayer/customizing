package com.easycerti.eframe.psm.system_management.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.easycerti.eframe.common.spring.DataModelAndView;

public interface MisdetectMngSvc {
	public DataModelAndView misdetectMngList(Map<String, String> parameters);
	public DataModelAndView removePrivacy(Map<String, String> parameters);
	public DataModelAndView addPrivacy(Map<String, String> parameters);
	public DataModelAndView addAllPrivacy(Map<String, String> parameters);
	public void findfindIpMngtList_download(DataModelAndView modelAndView, Map<String, String> parameters,  HttpServletRequest request);
	public String upLoadSystemMngtOne(Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response)throws Exception;
	
	
	public DataModelAndView misdetectMngList_summary(Map<String, String> parameters, HttpServletRequest request);
	public DataModelAndView misdetectMngDetail_summary(Map<String, String> parameters, HttpServletRequest request);
	public String addMisdetectSummry(Map<String, String> parameters, HttpServletRequest request);
	
}
