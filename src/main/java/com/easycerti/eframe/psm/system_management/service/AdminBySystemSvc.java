package com.easycerti.eframe.psm.system_management.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.system_management.vo.AdminUserSearch;

public interface AdminBySystemSvc {
	
	public DataModelAndView findAdminBySystemList(AdminUserSearch search, HttpServletRequest request);

	public void findAdminBySystemList_download(DataModelAndView modelAndView, AdminUserSearch search, HttpServletRequest request);
	
	public DataModelAndView findAdminBySystemDetail(String admin_user_id, HttpServletRequest request);
	
	public DataModelAndView addAdminBySystem(Map<String, String> parameters);
	
	public DataModelAndView saveAdminBySystem(Map<String, String> parameters);
	
	public DataModelAndView removeAdminBySystem(Map<String, String> parameters);
	
}
