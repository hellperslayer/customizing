package com.easycerti.eframe.psm.system_management.vo;

import com.easycerti.eframe.common.vo.AbstractValueObject;

public class IpInfoBean extends AbstractValueObject  {
	private String user_Id;
	private String login_Ip;
	
	public String getUser_Id() {
		return user_Id;
	}
	public void setUser_Id(String user_Id) {
		this.user_Id = user_Id;
	}
	public String getLogin_Ip() {
		return login_Ip;
	}
	public void setLogin_Ip(String login_Ip) {
		this.login_Ip = login_Ip;
	}
}
