package com.easycerti.eframe.psm.system_management.vo;

import java.util.Date;

import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 권한 VO
 * 
 * @author yjyoo
 * @since 2015. 4. 20.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 20.           yjyoo            최초 생성
 *
 * </pre>
 */
public class Auth extends AbstractValueObject {
	
	// 권한 ID
	private String auth_id = null;
	
	// 권한 명
	private String auth_name = null;
	
	// 사용여부
	private String use_flag  = null;
	
	// 등록자
	private String insert_user_id = null;
	
	// 등록일자
	private Date insert_datetime = null;
	
	// 수정자
	private String update_user_id = null;
	
	private String system_seq = null;

	// 수정일자
	private Date update_datetime  = null;
	
	// 설명
	private String comment  = null;
	
	// 접근시스템 목록
	private String sys_auth_ids = null;

	// 접근 보고서 시스템코드 목록
	private String sys_report_ids = null;
	
	// 생성가능 보고서 코드 목록
	private String make_report_ids = null;
	
	private String option = null;
	
	public String getOption() {
		return option;
	}

	public void setOption(String option) {
		this.option = option;
	}

	public String getMake_report_ids() {
		return make_report_ids;
	}

	public void setMake_report_ids(String make_report_ids) {
		this.make_report_ids = make_report_ids;
	}

	public String getSys_report_ids() {
		return sys_report_ids;
	}

	public void setSys_report_ids(String sys_report_ids) {
		this.sys_report_ids = sys_report_ids;
	}

	public String getSys_auth_ids() {
		return sys_auth_ids;
	}

	public void setSys_auth_ids(String sys_auth_ids) {
		this.sys_auth_ids = sys_auth_ids;
	}

	public String getAuth_id() {
		return auth_id;
	}

	public void setAuth_id(String auth_id) {
		this.auth_id = auth_id;
	}

	public String getAuth_name() {
		return auth_name;
	}

	public void setAuth_name(String auth_name) {
		this.auth_name = auth_name;
	}

	public String getUse_flag() {
		return use_flag;
	}

	public void setUse_flag(String use_flag) {
		this.use_flag = use_flag;
	}

	public String getInsert_user_id() {
		return insert_user_id;
	}

	public void setInsert_user_id(String insert_user_id) {
		this.insert_user_id = insert_user_id;
	}

	public Date getInsert_datetime() {
		return insert_datetime;
	}

	public void setInsert_datetime(Date insert_datetime) {
		this.insert_datetime = insert_datetime;
	}

	public String getUpdate_user_id() {
		return update_user_id;
	}

	public void setUpdate_user_id(String update_user_id) {
		this.update_user_id = update_user_id;
	}

	public Date getUpdate_datetime() {
		return update_datetime;
	}

	public void setUpdate_datetime(Date update_datetime) {
		this.update_datetime = update_datetime;
	}
	public String getSystem_seq() {
		return system_seq;
	}

	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
}

