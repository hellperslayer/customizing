package com.easycerti.eframe.psm.system_management.dao;

import java.util.List;

import com.easycerti.eframe.psm.system_management.vo.Ip;

/**
 * 부서 관리 Dao Interface
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일                           수정자                           수정내용
 * ------------        -------------     ----------------------
 * 2016. 4. 18.           ehchoi                 최초 생성
 *
 * </pre>
 */
public interface IpMngtDao {
	
	/**
	 * 부서 리스트 가져오기
	 */
	public List<Ip> findIpMngtList(Ip ip);
	
	/**
	 * 부서 리스트 가져오기 (선택된 부서만)
	 */
	public List<Ip> findIpMngtList_choose(Ip ip);
	
	/**
	 * 부서 리스트 갯수 가져오기
	 */
	public Integer findIpMngtOne_count(Ip ip);
	
	/**
	 * 부서 정보 가져오기
	 */
	public Ip findIpMngtOne(Ip ip);
	
	/**
	 * 최상위 부서 ID 가져오기
	 */
	public String findIpMngtOne_root();
	
	/**
	 * 부서 추가하기 (단건)
	 */
	public void addIpMngtOne(Ip ip);
	
	/**
	 * 부서 수정하기 (단건)
	 */
	public void saveIpMngtOne(Ip ip);
	
	/**
	 * 부서 삭제하기 (단건)
	 */
	public void removeIpMngtOne(Ip ip);
	
}
