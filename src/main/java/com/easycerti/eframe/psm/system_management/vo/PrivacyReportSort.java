package com.easycerti.eframe.psm.system_management.vo;

public class PrivacyReportSort {
	private int stype1;
	private int stype2;
	private int stype3;
	private int stype4;
	
	private int type1;
	private int privacyCnt;
	
	private String code_name;

	public int getStype1() {
		return stype1;
	}

	public void setStype1(int stype1) {
		this.stype1 = stype1;
	}

	public int getStype2() {
		return stype2;
	}

	public void setStype2(int stype2) {
		this.stype2 = stype2;
	}

	public int getStype3() {
		return stype3;
	}

	public void setStype3(int stype3) {
		this.stype3 = stype3;
	}

	public int getStype4() {
		return stype4;
	}

	public void setStype4(int stype4) {
		this.stype4 = stype4;
	}

	public String getCode_name() {
		return code_name;
	}

	public void setCode_name(String code_name) {
		this.code_name = code_name;
	}

	public int getType1() {
		return type1;
	}

	public void setType1(int type1) {
		this.type1 = type1;
	}

	public int getPrivacyCnt() {
		return privacyCnt;
	}

	public void setPrivacyCnt(int privacyCnt) {
		this.privacyCnt = privacyCnt;
	}
}
