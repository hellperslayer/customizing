package com.easycerti.eframe.psm.system_management.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 대상서버리스트 VO
 * 
 * @author yjyoo
 * @since 2015. 4. 24.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 24.           yjyoo            최초 생성
 *
 * </pre>
 */
public class ServerMasterList extends AbstractValueObject {
	
	// 서버 리스트
	private List<ServerMaster> serverMasters = null;
	
	public ServerMasterList() {}
	
	public ServerMasterList(List<ServerMaster> serverMasters){
		this.serverMasters = serverMasters;
	}
	
	public ServerMasterList(List<ServerMaster> serverMasters, String count){
		this.serverMasters = serverMasters;
		setPage_total_count(count);
	}

	public List<ServerMaster> getServerMasters() {
		return serverMasters;
	}

	public void setServerMasters(List<ServerMaster> serverMasters) {
		this.serverMasters = serverMasters;
	}
	
	
}
