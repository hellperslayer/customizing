package com.easycerti.eframe.psm.system_management.service;

import java.util.Map;

import com.easycerti.eframe.common.spring.DataModelAndView;

/**
 * 코드 관련 Service Interface
 * 
 * @author yjyoo
 * @since 2015. 4. 17.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 17.           yjyoo            최초 생성
 *
 * </pre>
 */
public interface CodeMngtSvc {

	/**
	 * 코드 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	public DataModelAndView findCodeMngtList(Map<String, String> parameters);
	
	/**
	 * 코드 상세정보
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	public DataModelAndView findCodeMngtOne(Map<String, String> parameters);
	
	/**
	 * 코드 추가 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	public DataModelAndView addCodeMngtOne(Map<String, String> parameters);
	
	/**
	 * 코드 수정 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	public DataModelAndView saveCodeMngtOne(Map<String, String> parameters) throws Exception;
	
	/**
	 * 코드 삭제 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	public DataModelAndView removeCodeMngtOne(Map<String, String> parameters) throws Exception;
}
