package com.easycerti.eframe.psm.system_management.web;

import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.psm.system_management.service.IpPermSvc;

@Controller
@RequestMapping(value="/ipPerm/*")
public class IpPermCtrl {

	@Autowired
	private IpPermSvc ipPermSvc;
	
	/**
	 * ip 리스트</br>
	 * - 부서 데이터는 tree 형태
	 * 
	 * @author ehchoi
	 * @since 2016. 4. 18.
	 * @return DataModelAndView
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[환경관리] 접속허용 IP")
	@RequestMapping(value="list.html",method={RequestMethod.POST})
	public DataModelAndView findIpMngtList(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		DataModelAndView modelAndView = ipPermSvc.ipPermList(parameters);
		modelAndView.setViewName("ipPermList");
		
		return modelAndView;
	}
	@RequestMapping(value="checkIpForm.html",method={RequestMethod.POST})
	@ResponseBody
	public boolean checkIpForm(@RequestParam(value = "ip") String ip){
		String validIp = "^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";
		if (Pattern.matches(validIp, ip)) {
			return true;
		}
		return false;
	}
	
	@MngtActHist(log_action = "SELECT", log_message = "[환경관리] 접속허용 IP 상세")
	@RequestMapping(value="detail.html",method={RequestMethod.POST})
	public DataModelAndView findIpPermD(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		DataModelAndView modelAndView = ipPermSvc.ipPermDetail(parameters, request);	
		modelAndView.setViewName("ipPermDetail");
		return modelAndView;
	}
	
	
	@RequestMapping(value="add.html",method={RequestMethod.POST})
	public DataModelAndView addIpPerm(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		String[] args = {"admin_id", "ip_content", "use_flag"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
				
		DataModelAndView modelAndView = ipPermSvc.addIpPerm(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	

	
	@RequestMapping(value="save.html",method={RequestMethod.POST})
	public DataModelAndView saveIpPerm(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		String[] args = {"ip_content", "use_flag"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		
		DataModelAndView modelAndView = ipPermSvc.saveIpPerm(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	

	
	@RequestMapping(value="remove.html",method={RequestMethod.POST})
	public DataModelAndView removeIpPerm(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		String[] args = {"ip_seq"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		DataModelAndView modelAndView = ipPermSvc.removeIpPerm(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	} 
	
}
