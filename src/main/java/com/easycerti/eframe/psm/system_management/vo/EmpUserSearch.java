package com.easycerti.eframe.psm.system_management.vo;


import com.easycerti.eframe.common.vo.SearchBase;

/**
 * 사원관리 검색 VO
 * 
 * @author yjyoo
 * @since 2015. 5. 27.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 27.           yjyoo            최초 생성
 *
 * </pre>
 */
public class EmpUserSearch extends SearchBase {
	
	// 사원 ID
	private String emp_user_id_1;
	private String emp_user_id;
	
	// 시스템 번호
	private String system_seq_1;
	private String system_seq;
	// 직원 이름
	private String emp_user_name;
	
	// 부서 ID
	private String dept_id;
	
	// 상태
	private String status;
	
	// 하위 부서 검색
	private String deptLowSearchFlag;
	
	private String dept_name;
	// 사용 여부
	private String use_flag;
	
	
	public String getUse_flag() {
		return use_flag;
	}

	public void setUse_flag(String use_flag) {
		this.use_flag = use_flag;
	}

	public String getSystem_seq() {
		return system_seq;
	}

	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}

	public String getEmp_user_id() {
		return emp_user_id;
	}

	public void setEmp_user_id(String emp_user_id) {
		this.emp_user_id = emp_user_id;
	}

	public String getDept_name() {
		return dept_name;
	}

	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}

	public String getEmp_user_id_1() {
		return emp_user_id_1;
	}

	public void setEmp_user_id_1(String emp_user_id_1) {
		this.emp_user_id_1 = emp_user_id_1;
	}
	
	public String getSystem_seq_1() {
		return system_seq_1;
	}

	public void setSystem_seq_1(String system_seq_1) {
		this.system_seq_1 = system_seq_1;
	}

	public String getEmp_user_name() {
		return emp_user_name;
	}

	public void setEmp_user_name(String emp_user_name) {
		this.emp_user_name = emp_user_name;
	}

	public String getDept_id() {
		return dept_id;
	}

	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDeptLowSearchFlag() {
		return deptLowSearchFlag;
	}

	public void setDeptLowSearchFlag(String deptLowSearchFlag) {
		this.deptLowSearchFlag = deptLowSearchFlag;
	}

	@Override
	public String toString() {
		return "EmpUserSearch [emp_user_id_1=" + emp_user_id_1
				+ ", emp_user_name=" + emp_user_name + ", dept_id=" + dept_id
				+ ", status=" + status + ", deptLowSearchFlag="
				+ deptLowSearchFlag + "]";
	}	
}
