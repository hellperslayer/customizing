package com.easycerti.eframe.psm.system_management.vo;

import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 옵션설정 VO
 */

public class AlarmMngt extends AbstractValueObject {

	private int agent_seq;
	private String agent_options;
	private String option_name;
	private String dbtype;
	private String jdbc_url;
	private String id;
	private String pw;
	private String tot_agent_options;
	private String tot_agent_seq;
	private String to_id;
	private String from_id;
	private String site_name;
	
	
	
	public String getSite_name() {
		return site_name;
	}

	public void setSite_name(String site_name) {
		this.site_name = site_name;
	}

	public String getTo_id() {
		return to_id;
	}

	public void setTo_id(String to_id) {
		this.to_id = to_id;
	}

	public String getFrom_id() {
		return from_id;
	}

	public void setFrom_id(String from_id) {
		this.from_id = from_id;
	}

	public String getTot_agent_options() {
		return tot_agent_options;
	}

	public void setTot_agent_options(String tot_agent_options) {
		this.tot_agent_options = tot_agent_options;
	}

	public String getTot_agent_seq() {
		return tot_agent_seq;
	}

	public void setTot_agent_seq(String tot_agent_seq) {
		this.tot_agent_seq = tot_agent_seq;
	}

	public String getDbtype() {
		return dbtype;
	}

	public void setDbtype(String dbtype) {
		this.dbtype = dbtype;
	}

	public String getJdbc_url() {
		return jdbc_url;
	}

	public void setJdbc_url(String jdbc_url) {
		this.jdbc_url = jdbc_url;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPw() {
		return pw;
	}

	public void setPw(String pw) {
		this.pw = pw;
	}

	public int getAgent_seq() {
		return agent_seq;
	}
	
	public void setAgent_seq(int agent_seq) {
		this.agent_seq = agent_seq;
	}
	
	public String getAgent_options() {
		return agent_options;
	}
	
	public void setAgent_options(String agent_options) {
		this.agent_options = agent_options;
	}
	
	public String getOption_name() {
		return option_name;
	}
	
	public void setOption_name(String option_name) {
		this.option_name = option_name;
	}
	
}
