package com.easycerti.eframe.psm.system_management.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import com.easycerti.eframe.common.dao.BootInitialDao;
import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.setup.dao.ExtrtBaseSetupDao;
import com.easycerti.eframe.psm.system_management.dao.AuthMngtDao;
import com.easycerti.eframe.psm.system_management.dao.OptionSettingDao;
import com.easycerti.eframe.psm.system_management.service.OptionSettingSvc;
import com.easycerti.eframe.psm.system_management.vo.Auth;
import com.easycerti.eframe.psm.system_management.vo.Menu;
import com.easycerti.eframe.psm.system_management.vo.OptionSetting;
import com.easycerti.eframe.psm.system_management.vo.PrivacyReport;

/**
 * 옵션 설정 Service Implements
 */

@Service
public class OptionSettingSvcImpl implements OptionSettingSvc {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private OptionSettingDao optionSettingDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private BootInitialDao bootInitialDao;
	
	@Autowired
	private ExtrtBaseSetupDao extrtBaseSetupDao;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;

	@Autowired
	private AuthMngtDao authMngtDao;
	
	@Override
	public DataModelAndView findOptionSettingList(Map<String, String> parameters, String auth) {
		
		System paramBean = CommonHelper.convertMapToBean(parameters, System.class);
		
		SimpleCode simpleCode = new SimpleCode();
		String index = "/optionSetting/list.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		/*String master = commonDao.getMaster();
		String sessionTime = commonDao.getSessionTime();
		String result_type_view = commonDao.getResultTypeView();
		String side_bar_view = commonDao.getSidebarView();
		String scrn_name_view = commonDao.getScrnNameView();
		String mapping_id = commonDao.getMappingId();*/
		
		Map<String,Integer> optionTypeCt = new HashMap<String,Integer>();
		
		
		List<OptionSetting> list = optionSettingDao.findOptionSettingListByAuth(auth);
		for(int i=0; i<list.size();i++) {
			OptionSetting option = list.get(i);
			if(option.getOption_id().equals("change_pwd") || option.getOption_id().equals("session_time")) {
				option.setOption_type("시스템보안");
				Integer value = optionTypeCt.get("시스템보안")==null?0:optionTypeCt.get("시스템보안");
				optionTypeCt.put("시스템보안", value+1);
			} else if(option.getOption_id().equals("empUserName_masking") || option.getOption_id().equals("file_delete_date")) {
				option.setOption_type("정보보호");
				Integer value = optionTypeCt.get("정보보호")==null?0:optionTypeCt.get("정보보호");
				optionTypeCt.put("정보보호", value+1);
			} else if(option.getOption_id().equals("side_bar") || option.getOption_id().equals("time_interval") || option.getOption_id().equals("ui_type")
					|| option.getOption_id().equals("init_login_page") || option.getOption_id().equals("master") || option.getOption_id().equals("mode_access_auth")
					|| option.getOption_id().equals("search_type")|| option.getOption_id().equals("same_auth_edit")) {
				option.setOption_type("사용자옵션");
				Integer value = optionTypeCt.get("사용자옵션")==null?0:optionTypeCt.get("사용자옵션");
				optionTypeCt.put("사용자옵션", value+1);
			} else if(option.getOption_id().equals("result_owner") || option.getOption_id().equals("result_type") || option.getOption_id().equals("insert_biz_log_result")
					|| option.getOption_id().equals("mapping_id") || option.getOption_id().equals("sbiz_log_sql") || option.getOption_id().equals("scrn_name")) {
				option.setOption_type("접속기록옵션");
				Integer value = optionTypeCt.get("접속기록옵션")==null?0:optionTypeCt.get("접속기록옵션");
				optionTypeCt.put("접속기록옵션", value+1);
			} else if(option.getOption_id().equals("use_alarm") || option.getOption_id().equals("use_bmt")) {
				option.setOption_type("편리성");
				Integer value = optionTypeCt.get("편리성")==null?0:optionTypeCt.get("편리성");
				optionTypeCt.put("편리성", value+1);
			} else if(option.getOption_id().equals("use_reportLine") || option.getOption_id().equals("use_reportLogo") || option.getOption_id().equals("use_systemSeq")
					 || option.getOption_id().equals("report_auth_type")) {
				option.setOption_type("보고서관리");
				Integer value = optionTypeCt.get("보고서관리")==null?0:optionTypeCt.get("보고서관리");
				optionTypeCt.put("보고서관리", value+1);
			} else if(option.getOption_id().equals("use_fullscan") || option.getOption_id().equals("sbiz_log_file") || option.getOption_id().equals("sbiz_log_filedownload")) {
				option.setOption_type("다운로드관리");
				Integer value = optionTypeCt.get("다운로드관리")==null?0:optionTypeCt.get("다운로드관리");
				optionTypeCt.put("다운로드관리", value+1);
			} else if(option.getOption_id().equals("sbiz_log_body_req") || option.getOption_id().equals("sbiz_log_body_res") || option.getOption_id().equals("use_dashboard_scenario")
					|| option.getOption_id().equals("auto_summon") || option.getOption_id().equals("mode_desc") || option.getOption_id().equals("summon_agent_type") 
					|| option.getOption_id().equals("summon_approval_type") || option.getOption_id().equals("summon_response_type") || option.getOption_id().equals("summon_alarm_type")
					|| option.getOption_id().equals("summon_cfm_yn") || option.getOption_id().equals("summon_alarm_yn")) {
				option.setOption_type("소명관리");
				Integer value = optionTypeCt.get("소명관리")==null?0:optionTypeCt.get("소명관리");
				optionTypeCt.put("소명관리", value+1);
			} else {
				option.setOption_type("기타");
				Integer value = optionTypeCt.get("기타")==null?0:optionTypeCt.get("기타");
				optionTypeCt.put("기타", value+1);
			}
		}
		
		Collections.sort(list, new Comparator<OptionSetting>() {
			@Override
			public int compare(OptionSetting o1, OptionSetting o2) {
				return o1.getOption_type().compareTo(o2.getOption_type());
			}
			
		});
		
		List<Code> ui_type = commonDao.getUitypeByCode(); 
		String bmt_name = commonDao.getBmtName();
		
		List<Menu> init_login_page = commonDao.getInitLoginPage();

		List<Auth> authList = authMngtDao.findAuthMngtList(new Auth());
		Map<String,Auth> authMap = new HashMap<String, Auth>();
		for (Auth auth2 : authList) {
			authMap.put(auth2.getAuth_id(), auth2);
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("list", list);
		modelAndView.addObject("ui_type", ui_type);
		modelAndView.addObject("init_login_page", init_login_page);
		modelAndView.addObject("option_type_ct", optionTypeCt);
		if (bmt_name != null && bmt_name.length() > 0) {
			modelAndView.addObject("bmt_name", bmt_name);
		}
		
		/*Code dashboard_scenario = commonDao.getDashboardScenario();
		if (dashboard_scenario != null) {
			List<ExtrtBaseSetup> scenarioList = extrtBaseSetupDao.findScenarioList(null);
			modelAndView.addObject("scenarioList", scenarioList);
			modelAndView.addObject("dashboard_scenario", dashboard_scenario);
		}*/
		/*modelAndView.addObject("masterflag", master);
		modelAndView.addObject("result_type_view", result_type_view);
		modelAndView.addObject("side_bar_view", side_bar_view);
		modelAndView.addObject("scrn_name_view", scrn_name_view);
		modelAndView.addObject("sessionTime", sessionTime);
		modelAndView.addObject("mapping_id", mapping_id);*/
		modelAndView.addObject("paramBean", paramBean);
		modelAndView.addObject("authMap", authMap);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView setOptionSetting(Map<String, String> parameters) {
		
		//List<OptionSetting> list = optionSettingDao.findOptionSettingList();
		
		List<OptionSetting> authList = new ArrayList<OptionSetting>();
		
		for(String option_id : parameters.keySet()){
			OptionSetting dummy = new OptionSetting();
			if(option_id.indexOf("admin_auth_") > -1) {
				dummy.setOption_id(option_id);
				dummy.setValue(parameters.get(option_id));
				authList.add(dummy);
			}
		}
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			for(String option_id : parameters.keySet() ){
				String val = parameters.get(option_id);
				if(option_id.equals("master")) {
					optionSettingDao.setMaster(val);
					optionSettingDao.setMaster2(val);
				}
				
				if(option_id.equals("mode_access_auth")) {
					if(val.equals("Y")) {
						if(bootInitialDao.findTable("access_auth") <= 0) {
							optionSettingDao.createAccessAuth();
						}
					}
				/*} else if (option_id.equals("biz_log_file")) {
					if (val.equals("Y")) {
						if(bootInitialDao.findTable("biz_log_file") <= 0) {
							optionSettingDao.createAccessAuth();
						}
						Map<String, String> findColumn_detection_src = new HashMap<>();
						findColumn_detection_src.put("table_name", "biz_log_result");
						findColumn_detection_src.put("column_name", "detection_src");
						if (bootInitialDao.findColumn(findColumn_detection_src) <= 0) {
							bootInitialDao.addColumnDetection_src();
						}
						Map<String, String> findColumn_file_id = new HashMap<>();
						findColumn_file_id.put("table_name", "biz_log_result");
						findColumn_file_id.put("column_name", "file_id");
						if (bootInitialDao.findColumn(findColumn_file_id) <= 0) {
							bootInitialDao.addColumnFile_id();
						}
					}*/
				} else if (option_id.equals("insert_biz_log_result")) {
					if (val.equals("Y")) {
						bootInitialDao.dropTrigger_insert_biz_log_result();
					} else {
						if (bootInitialDao.findTrigger_insert_biz_log_result() <= 0) {
							bootInitialDao.createTrigger_insert_biz_log_result();
						}
					}
				}
				
				OptionSetting optionSetting = new OptionSetting();
				optionSetting.setOption_id(option_id);				
				optionSetting.setValue(val);
				for(OptionSetting a : authList) {
					if(	a.getOption_id().substring(11).equals(option_id) ) {
						optionSetting.setAuth(a.getValue());
					}
				}
				
				/*
				 * for (int i = 0; i < list.size(); ++i) {
				 * if(list.get(i).getOption_id().equals(option_id.substring(0,
				 * option_id.length()-5))) { optionSetting.setOption_id(option_id);
				 * optionSetting.setAuth(val); } }
				 */
				
				optionSettingDao.setOptionSetting(optionSetting);
			}
			
			String bmt_name = parameters.get("bmt_name");
			if (bmt_name != null && bmt_name.length() > 0) {
				commonDao.setBmtName(bmt_name);
			}
			
			String select_scenario = parameters.get("select_scenario");
			if (select_scenario != null && select_scenario.length() > 0) {
				commonDao.setDashboardScenario(select_scenario);
			}
			
			String ui_type = parameters.get("ui_type");
			if("Kurly".equals(ui_type)) {
				if(bootInitialDao.findTable("auth_info_list") <= 0) {
					commonDao.createAuth_info_list();
				}
				if(bootInitialDao.findTable("extra_biz_log_setup") <= 0) {
					commonDao.createExtra_biz_log_setup();
				}
			}
			
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[OptionSettingSvcImpl.setOptionSetting] MESSAGE : " + e.getMessage());
			throw new ESException("SYS035J");
		}
		
		return null;
	}
	
	@Override
	public DataModelAndView setMaster(Map<String, String> parameters) {
					
		String use_flag = parameters.get("use_flag");
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			optionSettingDao.setMaster(use_flag);
			optionSettingDao.setMaster2(use_flag);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[OptionSettingSvcImpl.removeSystemMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS035J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView setSessionTime(String time) {
					
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			optionSettingDao.setSessionTime(time);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[OptionSettingSvcImpl.removeSystemMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS035J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView setResultTypeView(Map<String, String> parameters) {
					
		String result_type_view = parameters.get("result_type_view");
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			optionSettingDao.setResultTypeView(result_type_view);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[OptionSettingSvcImpl.setResultTypeView] MESSAGE : " + e.getMessage());
			throw new ESException("SYS035J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView setSidebarView(Map<String, String> parameters) {
					
		String side_bar_view = parameters.get("side_bar_view");
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			optionSettingDao.setSidebarView(side_bar_view);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[OptionSettingSvcImpl.setSidebarView] MESSAGE : " + e.getMessage());
			throw new ESException("SYS035J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView setScrnNameView(Map<String, String> parameters) {
		
		String scrn_name_view = parameters.get("scrn_name_view");
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			optionSettingDao.setScrnNameView(scrn_name_view);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[OptionSettingSvcImpl.setScrnNameView] MESSAGE : " + e.getMessage());
			throw new ESException("SYS035J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView setMappingId(Map<String, String> parameters) {
		
		String mapping_id = parameters.get("mapping_id");
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			optionSettingDao.setMappingId(mapping_id);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[OptionSettingSvcImpl.setMappingId] MESSAGE : " + e.getMessage());
			throw new ESException("SYS035J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView setUse_systemSeq(Map<String, String> parameters) {
		
		String use_systemSeq = parameters.get("use_systemSeq");
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			optionSettingDao.setUse_systemSeq(use_systemSeq);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[OptionSettingSvcImpl.setMappingId] MESSAGE : " + e.getMessage());
			throw new ESException("SYS035J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView setUi_type(Map<String, String> parameters) {
		
		String use_systemSeq = parameters.get("use_systemSeq");
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			optionSettingDao.setUi_type(use_systemSeq);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[OptionSettingSvcImpl.setMappingId] MESSAGE : " + e.getMessage());
			throw new ESException("SYS035J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		return modelAndView;
	}

	
}
