package com.easycerti.eframe.psm.system_management.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.system_management.service.AgentMngtSvc;
import com.easycerti.eframe.psm.system_management.vo.AgentMaster;
import com.easycerti.eframe.psm.system_management.vo.AgentMasterSearch;

/**
 * 에이전트 관련 Controller
 * 
 * @author yjyoo
 * @since 2015. 4. 24.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 24.           yjyoo            최초 생성
 *
 * </pre>
 */
@Controller
@RequestMapping(value="/agentMngt/*")
public class AgentMngtCtrl {
	
	@Autowired
	private AgentMngtSvc agentMngtSvc;
	
	/**
	 * 에이전트 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 24.
	 * @return DataModelAndView
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[환경관리] 에이전트모니터링")
	@RequestMapping(value="list.html",method={RequestMethod.POST})
	public DataModelAndView findAgentMngtList(@ModelAttribute("search") AgentMasterSearch search, @RequestParam Map<String, String> parameters, HttpServletRequest request){
		DataModelAndView modelAndView = agentMngtSvc.findAgentMngtList(search);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("agentMngtList");
		return modelAndView;
	}
	
	@MngtActHist(log_action = "SELECT", log_message = "[환경관리] 에이전트동작설정 상세")
	@RequestMapping(value="detail.html",method={RequestMethod.POST})
	public DataModelAndView findAgentMngtDetail(@ModelAttribute("search") AgentMaster search, @RequestParam Map<String, String> parameters, HttpServletRequest request){
		DataModelAndView modelAndView = agentMngtSvc.findAgentMngtDetail(search);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("agentMngtDetail");
		return modelAndView;
	}
	
	@MngtActHist(log_action = "INSERT", l_category = "모니터링",m_category = "에이전트모니터링",s_category = "신규등록" ,log_message = "[환경관리] 에이전트 등록")
	@RequestMapping(value="add.html",method={RequestMethod.POST})
	public DataModelAndView addAgent(@ModelAttribute("search") AgentMaster search, @RequestParam Map<String, String> parameters, HttpServletRequest request){
		DataModelAndView modelAndView = agentMngtSvc.addAgent(search);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@MngtActHist(log_action = "DELETE", l_category = "모니터링",m_category = "에이전트모니터링",s_category = "삭제" , log_message = "[환경관리] 에이전트 삭제")
	@RequestMapping(value="remove.html",method={RequestMethod.POST})
	public DataModelAndView removeAgent(@ModelAttribute("search") AgentMaster search, @RequestParam Map<String, String> parameters, HttpServletRequest request){
		DataModelAndView modelAndView = agentMngtSvc.removeAgent(search);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@MngtActHist(log_action = "UPDATE", log_message = "[환경관리] 에이전트 수정")
	@RequestMapping(value="save.html",method={RequestMethod.POST})
	public DataModelAndView saveAgent(@ModelAttribute("search") AgentMaster search, @RequestParam Map<String, String> parameters, HttpServletRequest request){
		DataModelAndView modelAndView = agentMngtSvc.saveAgent(search);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}

	@MngtActHist(log_action = "UPDATE", log_message = "[환경관리] 에이전트 동작 변경")
	@RequestMapping(value="actionAgent.html",method={RequestMethod.POST})
	public DataModelAndView actionAgent(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = agentMngtSvc.actionAgent(parameters);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@MngtActHist(log_action = "UPDATE", log_message = "[에이전트모니터링] 라이센스")
	@RequestMapping(value="license_Hist.html",method={RequestMethod.POST})
	public DataModelAndView setLicenCe_Hist(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		
		DataModelAndView modelAndView = agentMngtSvc.setLicenCe_Hist(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
}
