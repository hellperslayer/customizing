package com.easycerti.eframe.psm.system_management.dao;

import java.util.List;
import java.util.Map;

import com.easycerti.eframe.psm.system_management.vo.Misdetect;

public interface MisdetectMngDao {	
	public List<Misdetect> misdetectMngList(Misdetect misdetect);
	public int misdetectMngList_count(Misdetect misdetect);
	public void deletePrivacy(Misdetect misdetect); 
	public void addPrivacy(Misdetect misdetect); 	
	public List<Misdetect> privacy_kind(Misdetect misdetect);
	public List<Misdetect> topPrivList(Misdetect paramBean);
	public Integer topPrivList_count();
	
	public void addMisdetectSummry(Map<String, String> parameters);
	
	public List<Misdetect> findMisdetectSummryList(Map<String, String> parameters);
	public Misdetect findMisdetectSummryDetail(Map<String, String> parameters);
	public int findMisdetectSummryListCount(Map<String, String> parameters);
}
