package com.easycerti.eframe.psm.system_management.dao;

import java.util.List;

import com.easycerti.eframe.psm.system_management.vo.Menu;

/**
 * 메뉴 관련 Dao Interface
 * 
 * @author yjyoo
 * @since 2015. 4. 20.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 20.           yjyoo            최초 생성
 *
 * </pre>
 */
public interface MenuMngtDao {

	/**
	 * 메뉴 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return List<Menu>
	 */
	public List<Menu> findMenuMngtList(Menu menu);
	
	/**
	 * 메뉴 리스트 갯수
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return Integer
	 */
	public Integer findMenuMngtOne_count(Menu menu);
	
	/**
	 * 메뉴 상세정보
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return Menu
	 */
	public Menu findMenuMngtOne(Menu menu);
	
	/**
	 * 메뉴 추가 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return void
	 */
	public void addMenuMngtOne(Menu menu);
	
	/**
	 * 메뉴 수정 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return void
	 */
	public void saveMenuMngtOne(Menu menu);
	
	/**
	 * 메뉴 삭제 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return void
	 */
	public void removeMenuMngtOne(Menu menu);
	
	/**
	 * 지정 메뉴로부터 메뉴 리스트 Hierachy 구조
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return List<Menu>
	 */
	public List<Menu> findMenuMngtList_forHierarchy(Menu menu);
	
	/**
	 * 지정 depth의 메뉴 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return List<Menu>
	 */
	public List<Menu> findMenuMngtList_toDepth(Menu menu);
	
	/**
	 * 해당 메뉴에서 지정된 DEPTH의 자신 상위 메뉴 찾기
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return Menu
	 */
	public Menu findMenuMngtOne_upDepth(Menu menu);
	
	/**
	 * 해당 메뉴의 상위 전체 경로 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return List<Menu>
	 */
	public List<Menu> findMenuMngtList_fullPathInfo(Menu menu);
	
	/**
	 * 해당 메뉴의 하위 메뉴 갯수 가져오기
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return Integer
	 */
	public Integer findMenuMngtOne_childCount(Menu menu);

	public List<Menu> findChildMenuList(String menu_id);
	
}
