package com.easycerti.eframe.psm.system_management.service;

import java.util.Map;

import com.easycerti.eframe.common.spring.DataModelAndView;

/**
 * 메뉴 관련 Service Interface
 * 
 * @author yjyoo
 * @since 2015. 4. 20.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 20.           yjyoo            최초 생성
 *
 * </pre>
 */
public interface MenuMngtSvc {

	/**
	 * 메뉴 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	public DataModelAndView findMenuMngtList(Map<String, String> parameters);
	
	/**
	 * 메뉴 상세정보
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	public DataModelAndView findMenuMngtOne(Map<String, String> parameters);
	
	/**
	 * 메뉴 추가 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	public DataModelAndView addMenuMngtOne(Map<String, String> parameters);
	
	/**
	 * 메뉴 수정 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	public DataModelAndView saveMenuMngtOne(Map<String, String> parameters);
	
	/**
	 * 메뉴 삭제 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	public DataModelAndView removeMenuMngtOne(Map<String, String> parameters) throws Exception;
	
	/**
	 * 메뉴 리스트 hierarchy 구조
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	public DataModelAndView findMenuMngtList_forHierarchy(Map<String, String> parameters);
	
}
