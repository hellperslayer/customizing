package com.easycerti.eframe.psm.system_management.vo;

import com.easycerti.eframe.common.vo.AbstractValueObject;

public class ChangeTheme extends AbstractValueObject{
	private Integer system_seq;
	
	private String chng_layout;
	
	private String dashboard;
	
	public String getChng_layout() {
		return chng_layout;
	}

	public void setChng_layout(String chng_layout) {
		this.chng_layout = chng_layout;
	}

	public String getDashboard() {
		return dashboard;
	}

	public void setDashboard(String dashboard) {
		this.dashboard = dashboard;
	}

	public Integer getSystem_seq() {
		return system_seq;
	}

	public void setSystem_seq(Integer system_seq) {
		this.system_seq = system_seq;
	}
	
}
