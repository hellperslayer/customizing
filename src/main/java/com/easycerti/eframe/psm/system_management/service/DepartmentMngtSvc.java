package com.easycerti.eframe.psm.system_management.service;

import java.util.List;
import java.util.Map;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.system_management.vo.Department;


/**
 * 부서 관련 Service Interface
 * 
 * @author yjyoo
 * @since 2015. 4. 17.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 17.           yjyoo            최초 생성
 *
 * </pre>
 */
public interface DepartmentMngtSvc {

	/**
	 * 부서 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	public DataModelAndView findDepartmentMngtList(Map<String, String> parameters);
	
	/**
	 * 부서 상세정보
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	public DataModelAndView findDepartmentMngtOne(Map<String, String> parameters);
	
	/**
	 * 부서 추가 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	public DataModelAndView addDepartmentMngtOne(Map<String, String> parameters);
	
	/**
	 * 부서 수정 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	public DataModelAndView saveDepartmentMngtOne(Map<String, String> parameters);
	
	/**
	 * 부서 삭제 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	public DataModelAndView removeDepartmentMngtOne(Map<String, String> parameters);

	public List<Department> findDepartmentMngtDepth();
}
