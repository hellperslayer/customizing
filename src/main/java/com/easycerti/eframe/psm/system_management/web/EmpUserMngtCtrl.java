package com.easycerti.eframe.psm.system_management.web;

import java.io.File;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.psm.system_management.dao.EmpUserMngtDao;
import com.easycerti.eframe.psm.system_management.service.DepartmentMngtSvc;
import com.easycerti.eframe.psm.system_management.service.EmpUserMngtSvc;
import com.easycerti.eframe.psm.system_management.vo.Department;
import com.easycerti.eframe.psm.system_management.vo.EmpUser;
import com.easycerti.eframe.psm.system_management.vo.EmpUserSearch;

/**
 * 사원 관련 Controller
 * 
 * @author yjyoo
 * @since 2015. 4. 21.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 21.           yjyoo            최초 생성
 *   2015. 5. 26.           yjyoo            엑셀 다운로드 추가
 *
 * </pre>
 */
@Controller
@RequestMapping("/empUserMngt/*")
public class EmpUserMngtCtrl {
	
	@Autowired
	private EmpUserMngtSvc empUserMngtSvc;
	
	@Autowired
	private DepartmentMngtSvc departmentMngtSvc;
	
	@Autowired
	private EmpUserMngtDao empUserMngtDao;
	
	/**
	 * 사원 리스트
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[환경관리] 사원관리")
	@RequestMapping(value="list.html", method={RequestMethod.POST})
	public DataModelAndView findEmpUserMngtList(@ModelAttribute("search") EmpUserSearch search, @RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		/*if(search.getEmp_user_id_1() != null) {
			String id = search.getEmp_user_id_1();
			String upperId = id.toUpperCase();
			search.setEmp_user_id_1(upperId);
		}*/
		
		DataModelAndView modelAndView = empUserMngtSvc.findEmpUserMngtList(search);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("empUserMngtList");
			
		return modelAndView;
	}
	
	/**
	 * 사원 리스트 엑셀 다운로드
	 */
	@RequestMapping(value="download.html", method={RequestMethod.POST})
	public DataModelAndView addEmpUserMngtList_download(@ModelAttribute("search") EmpUserSearch search, @RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		empUserMngtSvc.findEmpUserMngtList_download(modelAndView, search, request);
		
		return modelAndView;
	}
	
	/**
	 * 사원 상세정보
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[환경관리] 사원관리 상세")
	@RequestMapping(value="detail.html", method={RequestMethod.POST})
	public DataModelAndView findEmpUserMngtOne(@RequestParam String emp_user_id, @RequestParam String system_seq,
			EmpUserSearch search, @RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		EmpUser empUser = new EmpUser();
		empUser.setEmp_user_id(emp_user_id);
		empUser.setSystem_seq(system_seq);
		
		DataModelAndView modelAndView = empUserMngtSvc.findEmpUserMngtOne(empUser);
		
		List<Department> descDepthList = departmentMngtSvc.findDepartmentMngtDepth();
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.addObject("descDepthList", descDepthList);
		modelAndView.setViewName("empUserMngtDetail");
		
		return modelAndView;
	}
	
	/**
	 * 사원 추가 (json)
	 */
	@RequestMapping(value="add.html", method={RequestMethod.POST})
	public DataModelAndView addEmpUserMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception {
					
		parameters.put("insert_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		DataModelAndView modelAndView = empUserMngtSvc.addEmpUserMngtOne(parameters, request);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	/**
	 * 사원 수정 (json)
	 */
	@RequestMapping(value="save.html", method={RequestMethod.POST})
	public DataModelAndView saveEmpUserMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception {
		
		parameters.put("update_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		DataModelAndView modelAndView = empUserMngtSvc.saveEmpUserMngtOne(parameters, request);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	/**
	 * 사원 삭제 (json)
	 */
	@RequestMapping(value="remove.html", method={RequestMethod.POST})
	public DataModelAndView removeEmpUserMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception {
		String[] args = {"emp_user_id"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)) {
			throw new ESException("SYS004J");
		}
		
		parameters.put("update_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		DataModelAndView modelAndView = empUserMngtSvc.removeEmpUserMngtOne(parameters, request);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		//modelAndView.setViewName("login");

		return modelAndView;
	}
	
	//엑셀 업로드
	
	@RequestMapping(value="upload.html", method={RequestMethod.POST})
	@ResponseBody
	public String upEmpUserMngtOne(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		parameters.put("insert_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		String result = empUserMngtSvc.upLoadSystemMngtOne(parameters,request, response);
		
		/*ModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("result", result);
		modelAndView.setViewName(CommonResource.JSON_VIEW);*/
		
		return result;
	}
	
	@RequestMapping("exdownload.html")
	public ModelAndView download(HttpServletRequest request)throws Exception{
		
		String path = request.getSession().getServletContext().getRealPath("/")+"WEB-INF/";
		 //System.out.println(path);
		File down = new File(path + "excelExam/사원_업로드양식.xlsx");
		return new ModelAndView("download","downloadFile",down);
	}
	
	//사원 비밀번호 초기화
	@RequestMapping(value="initPassword.html", method={RequestMethod.POST})
	@ResponseBody
	public int initPassword(@RequestParam Map<String, String> parameters,HttpServletRequest request) {
		
		int result = 0;
		
		EmpUser empUser = new EmpUser();
		empUser.setEmp_user_id(parameters.get("emp_user_id"));
		empUser.setSystem_seq(parameters.get("system_seq"));
		
		
		try {
			empUserMngtDao.initPassword(empUser);
			result = 1;
		} catch (Exception e) {
			result = 0;
		}
		
		return result;
	}
	
}
