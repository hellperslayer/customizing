package com.easycerti.eframe.psm.system_management.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 부서 리스트 VO
 * 
 * @author yjyoo
 * @since 2015. 4. 17.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일                              수정자                    수정내용
 *  --------------    -------------    ----------------------
 *   2016. 4. 18.           ehchoi            최초 생성
 *
 * </pre>
 */
public class IpList extends AbstractValueObject {

	// 부서 리스트
	private List<Ip> ips = null;

	public List<Ip> getips() {
		return ips;
	}

	public void setIps(List<Ip> ips) {
		this.ips = ips;
	}
}
