package com.easycerti.eframe.psm.system_management.web;

import java.io.File;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.psm.system_management.service.TalarmMngtSvc;
import com.easycerti.eframe.psm.system_management.vo.Talarm;
import com.easycerti.eframe.psm.system_management.vo.TalarmSearch;

/**
 * 사원 관련 Controller
 * 
 * @author yjyoo
 * @since 2015. 4. 21.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 21.           yjyoo            최초 생성
 *   2015. 5. 26.           yjyoo            엑셀 다운로드 추가
 *
 * </pre>
 */
@Controller
@RequestMapping("/talarmMngt/*")
public class TalarmMngtCtrl {
	
	@Autowired
	private TalarmMngtSvc talarmMngtSvc;
	
	/**
	 * 사원 리스트
	 */
	@RequestMapping(value="list.html", method={RequestMethod.POST})
	public DataModelAndView findTalarmMngtList(@ModelAttribute("search") TalarmSearch search, @RequestParam Map<String, String> parameters, HttpServletRequest request) {
		if(search.getEmp_user_id() != null) {
			String id = search.getEmp_user_id();
			String upperId = id.toUpperCase();
			search.setEmp_user_id(upperId);
		}
		
		DataModelAndView modelAndView = talarmMngtSvc.findTalarmMngtList(search);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("talarmMngtList");
			
		return modelAndView;
	}
	
	/**
	 * 사원 리스트 엑셀 다운로드
	 */
	@RequestMapping(value="download.html", method={RequestMethod.POST})
	public DataModelAndView findTalarmMngtList_download(@ModelAttribute("search") TalarmSearch search, @RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		talarmMngtSvc.findTalarmMngtList_download(modelAndView, search, request);
		
		return modelAndView;
	}
	
	/**
	 * 사원 상세정보
	 */
	@RequestMapping(value="detail.html", method={RequestMethod.POST})
	public DataModelAndView findTalarmMngtOne(@RequestParam String emp_user_id, TalarmSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		Talarm talarm = new Talarm();
		talarm.setEmp_user_id(emp_user_id);
		
		DataModelAndView modelAndView = talarmMngtSvc.findTalarmMngtOne(talarm);
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("talarmMngtDetail");
		
		return modelAndView;
	}
	
	
	/**
	 * 사원 수정 (json)
	 */
	@RequestMapping(value="save.html", method={RequestMethod.POST})
	public DataModelAndView saveTalarmMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception {
		
		parameters.put("update_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		DataModelAndView modelAndView = talarmMngtSvc.saveTalarmMngtOne(parameters);
		System.out.println(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@RequestMapping(value="save1.html", method={RequestMethod.POST})
	public DataModelAndView saveTalarmMngtTwo(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception {
		
		parameters.put("update_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		DataModelAndView modelAndView = talarmMngtSvc.saveTalarmMngtTwo(parameters);
		System.out.println(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	/**
	 * 사원 삭제 (json)
	 */
	@RequestMapping(value="remove.html", method={RequestMethod.POST})
	public @ResponseBody DataModelAndView removeTalarmMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception {
		String[] args = {"emp_user_id"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)) {
			throw new ESException("SYS004J");
		}
		
		parameters.put("update_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		DataModelAndView modelAndView = talarmMngtSvc.removeTalarmMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	//엑셀 업로드
	
	@RequestMapping(value="upload.html", method={RequestMethod.POST})
	@ResponseBody
	public String upTalarmMngtOne(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		parameters.put("insert_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		String result = talarmMngtSvc.upLoadSystemMngtOne(parameters,request, response);
		
		/*ModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("result", result);
		modelAndView.setViewName(CommonResource.JSON_VIEW);*/
		
		return result;
	}
	
	@RequestMapping("exdownload.html")
	public ModelAndView download(HttpServletRequest request)throws Exception{
		
		String path = request.getSession().getServletContext().getRealPath("/")+"WEB-INF/";
		 System.out.println(path);
		File down = new File(path + "excelExam/사원_업로드양식.xlsx");
		return new ModelAndView("download","downloadFile",down);
	}
	
}
