package com.easycerti.eframe.psm.system_management.dao;

import java.util.List;

import com.easycerti.eframe.psm.system_management.vo.EmpUser;
import com.easycerti.eframe.psm.system_management.vo.EmpUserSearch;

/**
 * 사원 관련 Dao Interface
 * 
 * @author yjyoo
 * @since 2015. 4. 21.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 21.           yjyoo            최초 생성
 *
 * </pre>
 */
public interface EmpUserMngtDao {
	
	/**
	 * 사원 리스트
	 */
	public List<EmpUser> findEmpUserMngtList(EmpUserSearch empUser);
	
	public List<EmpUser> findEmpUserMngtListWithThreshold(EmpUserSearch empUser);
	/**
	 * 사원 리스트 갯수
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 21.
	 * @return Integer
	 */
	public Integer findEmpUserMngtOne_count(EmpUserSearch empUser);
	
	/**
	 * 사원 상세정보
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 21.
	 * @return EmpUser
	 */
	public EmpUser findEmpUserMngtOne(EmpUser empUser);
	
	/**
	 * 사원 추가 전 중복 체크
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 21.
	 * @return Integer
	 */
	public Integer findEmpUserMngtOne_validation(EmpUser empUser);
	
	/**
	 * 사원 추가 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 21.
	 * @return void
	 */
	public void addEmpUserMngtOne(EmpUser empUser);
	
	/**
	 * 사원 수정 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 21.
	 * @return void
	 */
	public void saveEmpUserMngtOne(EmpUser empUser);
	
	/**
	 * 사원 삭제
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 21.
	 * @return void
	 */
	public void removeEmpUserMngtOne(EmpUser empUser);
	
	/**
	 * 부서 삭제 전 사용중인 사용자가 있는지 확인
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 21.
	 * @return Integer
	 */
	public Integer findEmpUserMngtOne_countToUseDept(EmpUser empUser);
	
	public void initPassword(EmpUser empUser);	//사원 비밀번호 초기화
	
}
