package com.easycerti.eframe.psm.system_management.dao;

import java.util.List;

import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;
import com.easycerti.eframe.psm.system_management.vo.AdminUserSearch;
import com.easycerti.eframe.psm.system_management.vo.IpPerm;

public interface IpPermDao {
	public List<IpPerm> ipPermList(IpPerm ipPerm);
	public IpPerm ipPermDetail(IpPerm ipPerm);
	public void addIpPerm(IpPerm ipPerm);
	public void saveIpPerm(IpPerm ipPerm);
	public void removeIpPerm(IpPerm ipPerm);
	public List<AdminUser> findAdminUserMngtList();
	public List<AdminUser> findAdminUserMngtSuperList();
	public List<AdminUser> findAdminUserSMngtList();
	
}
