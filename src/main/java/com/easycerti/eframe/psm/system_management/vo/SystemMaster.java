package com.easycerti.eframe.psm.system_management.vo;

import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 대상시스템 VO
 * 
 * @author yjyoo
 * @since 2015. 4. 24.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 24.           yjyoo            최초 생성
 *
 * </pre>
 */
public class SystemMaster extends AbstractValueObject {
	
	// 시스템번호
	private String system_seq = null;
	
	// 시스템명
	private String system_name = null;
	
	// 시스템종류
	private String system_type = null;
	
	private String main_url = null;
	private String result_type;
	
	private int tot_count;
	
	private int resultType1;
	private int resultType2;
	private int resultType3;
	private int resultType4;
	private int resultType5;
	private int resultType6;
	private int resultType7;
	private int resultType8;
	private int resultType9;
	private int resultType10;
	private int resultType99;
	
	private String top1="";
	private String top2="";
	private String top3="";
	private String top4="";
	private String top5="";
	private String top6="";
	private String top7="";
	private String top8="";
	private String top9="";
	private String top10="";
	
	public String getTop6() {
		return top6;
	}

	public void setTop6(String top6) {
		this.top6 = top6;
	}

	public String getTop7() {
		return top7;
	}

	public void setTop7(String top7) {
		this.top7 = top7;
	}

	public String getTop8() {
		return top8;
	}

	public void setTop8(String top8) {
		this.top8 = top8;
	}

	public String getTop9() {
		return top9;
	}

	public void setTop9(String top9) {
		this.top9 = top9;
	}

	public String getTop10() {
		return top10;
	}

	public void setTop10(String top10) {
		this.top10 = top10;
	}

	public String getTop1() {
		return top1;
	}

	public void setTop1(String top1) {
		this.top1 = top1;
	}

	public String getTop2() {
		return top2;
	}

	public void setTop2(String top2) {
		this.top2 = top2;
	}

	public String getTop3() {
		return top3;
	}

	public void setTop3(String top3) {
		this.top3 = top3;
	}

	public String getTop4() {
		return top4;
	}

	public void setTop4(String top4) {
		this.top4 = top4;
	}

	public String getTop5() {
		return top5;
	}

	public void setTop5(String top5) {
		this.top5 = top5;
	}

	public int getResultType1() {
		return resultType1;
	}

	public void setResultType1(int resultType1) {
		this.resultType1 = resultType1;
	}

	public int getResultType2() {
		return resultType2;
	}

	public void setResultType2(int resultType2) {
		this.resultType2 = resultType2;
	}

	public int getResultType3() {
		return resultType3;
	}

	public void setResultType3(int resultType3) {
		this.resultType3 = resultType3;
	}

	public int getResultType4() {
		return resultType4;
	}

	public void setResultType4(int resultType4) {
		this.resultType4 = resultType4;
	}

	public int getResultType5() {
		return resultType5;
	}

	public void setResultType5(int resultType5) {
		this.resultType5 = resultType5;
	}

	public int getResultType6() {
		return resultType6;
	}

	public void setResultType6(int resultType6) {
		this.resultType6 = resultType6;
	}

	public int getResultType7() {
		return resultType7;
	}

	public void setResultType7(int resultType7) {
		this.resultType7 = resultType7;
	}

	public int getResultType8() {
		return resultType8;
	}

	public void setResultType8(int resultType8) {
		this.resultType8 = resultType8;
	}

	public int getResultType9() {
		return resultType9;
	}

	public void setResultType9(int resultType9) {
		this.resultType9 = resultType9;
	}

	public int getResultType10() {
		return resultType10;
	}

	public void setResultType10(int resultType10) {
		this.resultType10 = resultType10;
	}

	public int getTot_count() {
		return tot_count;
	}

	public void setTot_count(int tot_count) {
		this.tot_count = tot_count;
	}

	public String getMain_url() {
		return main_url;
	}

	public void setMain_url(String main_url) {
		this.main_url = main_url;
	}

	public String getSystem_seq() {
		return system_seq;
	}

	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}

	public String getSystem_name() {
		return system_name;
	}

	public void setSystem_name(String system_name) {
		this.system_name = system_name;
	}

	public String getSystem_type() {
		return system_type;
	}

	public void setSystem_type(String system_type) {
		this.system_type = system_type;
	}

	public int getResultType99() {
		return resultType99;
	}

	public void setResultType99(int resultType99) {
		this.resultType99 = resultType99;
	}

	public String getResult_type() {
		return result_type;
	}

	public void setResult_type(String result_type) {
		this.result_type = result_type;
	}
	
}
