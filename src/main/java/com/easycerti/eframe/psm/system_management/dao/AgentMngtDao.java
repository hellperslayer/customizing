package com.easycerti.eframe.psm.system_management.dao;

import java.util.List;
import java.util.Map;

import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.system_management.vo.AgentMaster;
import com.easycerti.eframe.psm.system_management.vo.AgentMasterSearch;
import com.easycerti.eframe.psm.system_management.vo.AlarmMngt;
import com.easycerti.eframe.psm.system_management.vo.PrivacyReport;
import com.easycerti.eframe.psm.system_management.vo.SendMailInfo;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

/**
 * 대상에이전트 관련 Dao Interface
 * 
 * @author yjyoo
 * @since 2015. 4. 24.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 24.           yjyoo            최초 생성
 *
 * </pre>
 */
public interface AgentMngtDao {

	/**
	 * 에이전트 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 24.
	 * @return List<AgentMaster>
	 */
	public List<AgentMaster> findAgentMngtList(AgentMasterSearch agentMaster);
	
	public AgentMaster findAgentMngtDetail(AgentMaster agentMaster);
	public void addAgent(AgentMaster agentMaster);
	public void removeAgent(AgentMaster agentMaster);
	//public void removeAgentHist(AgentMaster agentMaster);
	public void updateAgent(AgentMaster agentMaster);
	
	/**
	 * 에이전트 리스트 갯수
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 24.
	 * @return Integer
	 */
	public Integer findAgentMngtOne_count(AgentMasterSearch agentMaster);
	
	
	/**
	 * 시스템 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 14.
	 * @return List<SystemMaster>
	 */
	public List<SystemMaster> findSystemMasterList();
	public List<SystemMaster> findSystemMasterList_byAdmin(SearchSearch search);
	public List<SystemMaster> findSystemMasterList_Onnara();
	public List<SystemMaster> findSystemMasterList_Dbac();
	public List<SystemMaster> findSystemMasterListAll();
	
	public List<SystemMaster> findSystemMasterDetailList(PrivacyReport pr);
	
	public List<SystemMaster> findSystemMasterDetailDownList(PrivacyReport pr);


	public List<PrivacyReport> findPrivacyReportCntList(PrivacyReport tmpBean);


	public List<PrivacyReport> findPrivacyReportCntbyEmp(PrivacyReport tmpBean);


	public String findDeptNameByEmpUserId(String emp_user_id);


	public String findNameByEmpUserId(String emp_user_id);

	public List<PrivacyReport> findSystemMasterTop10(PrivacyReport tmpBean);

	public List<SystemMaster> findSystemMasterList_new(SearchSearch search);
	
	public List<PrivacyReport> findPrivacyReportBySystem(PrivacyReport pr);
	public PrivacyReport findPrivacyReportBySystemTop1(PrivacyReport pr);
	public List<PrivacyReport> findPrivacyReportByDept(PrivacyReport pr);
	public List<PrivacyReport> findPrivacyReportByDeptUseStudentId(PrivacyReport pr);
	public PrivacyReport findPrivacyReportByDeptTop1(PrivacyReport pr);
	public List<PrivacyReport> findPrivacyReportByReqtype(PrivacyReport pr);
	public List<PrivacyReport> findPrivacyReportByReqtypeCnt(PrivacyReport pr);
	public List<PrivacyReport> findPrivacyReportByResultType(PrivacyReport pr);
	public Integer findEmpDetailCount(PrivacyReport pr);
	public List<PrivacyReport> findPrivTypeCount(PrivacyReport pr);
	public List<PrivacyReport> findEmpDetailList(PrivacyReport pr);
	public List<PrivacyReport> findPrivTypeResultCountTop10(PrivacyReport pr);
	public List<PrivacyReport> findPrivTypeResultCount(PrivacyReport report);
	public List<PrivacyReport> findPrivTypeResultCountBySysTop10(PrivacyReport pr);
	public List<PrivacyReport> findPrivTypeResultCountBySys(PrivacyReport report);
	public List<PrivacyReport> findDeptTypeResultCountBySysTop10(PrivacyReport pr);
	public List<PrivacyReport> findDeptTypeResultCountBySys(PrivacyReport report);
	public List<PrivacyReport> findDeptTypeResultCountTop10(PrivacyReport pr);
	public List<PrivacyReport> findDeptTypeResultCount(PrivacyReport report);
	public List<PrivacyReport> findSystemTypeResultCountTop10(PrivacyReport pr);
	public List<PrivacyReport> findSystemTypeResultCount(PrivacyReport report);
	public List<PrivacyReport> findSystemMaster(PrivacyReport pr);
	public List<PrivacyReport> findSystemCurdCount(PrivacyReport tmp);
	public List<PrivacyReport> findPrivacyReportByDept_1(PrivacyReport pr);
	public List<PrivacyReport> findPrivacyReportByDept_2(PrivacyReport pr);
	public List<PrivacyReport> findReportDetail2_chart1(PrivacyReport pr);
	public List<PrivacyReport> findReportDetail2_chart2(PrivacyReport pr);
	public List<PrivacyReport> findReportDetailbyIndividual(PrivacyReport pr);
	

	public List<PrivacyReport> findReportDetail_chart5(PrivacyReport pr);
	public List<PrivacyReport> findReportDetail_chart5_useStudentId(PrivacyReport pr);
	public PrivacyReport findReportDetail_chart5_detail(PrivacyReport report);

	public List<PrivacyReport> findPrivacyReportDetailChart3_1(PrivacyReport pr);
	public List<PrivacyReport> findPrivacyReportDetailChart3_3(PrivacyReport pr);
	public List<PrivacyReport> findPrivacyReportDetailChart3_4(PrivacyReport pr);

	public List<PrivacyReport> findEmpDetailListCount(PrivacyReport pr);

	public List<PrivacyReport> findPrivTypeCountWeek(PrivacyReport pr);

	public List<PrivacyReport> findTypeReportCount(PrivacyReport pr);
	public List<PrivacyReport> findSystemReportCount(PrivacyReport pr);
	public List<PrivacyReport> findDeptReportCount(PrivacyReport pr);
	public List<PrivacyReport> findIndvReportCount(PrivacyReport pr);
	
	public String getApprover(String system_seq);

	public List<PrivacyReport> findSystemTypeDownloadResultCountTop10(PrivacyReport pr);
	public List<PrivacyReport> findSystemTypeDownloadResultCount(PrivacyReport pr);
	
	public List<Code> findExtraBizLogType();
	public List<Code> findExtraBizLogTypeNotAuth();
	
	public Code findExtraBizLogTypeOne(String log_type);

	public List<PrivacyReport> findPrivacyReportByReqtype_download(PrivacyReport pr);
	public List<Map<String, String>> findServerSeqList();
	
	public List<AgentMaster> findLicense_history();
	
	public void setLicense_Hist(AgentMaster agentMaster);	//에이전트동작설정 - 히스토리
	public SendMailInfo findSendMailInfo(String type_name);	//이메일발송 테이블 조회
	public void updateMailInfo(SendMailInfo sendMailInfo);	//이메일발송 테이블 업데이트
	public int updateMailInfo_sendYn(String type_name);
	
	
}
