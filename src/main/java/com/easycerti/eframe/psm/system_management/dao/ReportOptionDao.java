package com.easycerti.eframe.psm.system_management.dao;


import java.util.List;
import java.util.Map;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.system_management.vo.Auth;
import com.easycerti.eframe.psm.system_management.vo.ReportApproval;
import com.easycerti.eframe.psm.system_management.vo.ReportOption;

public interface ReportOptionDao {

	public List<ReportOption> reprotOption_GeneralList();
	public List<Auth> findAuthMngtList(Auth auth);
	public void updGeneral(ReportOption reportOption);
	public List<Code> findCodeMngtList(Code code);
	public String findDefaultDesc(Map<String, String> parameters);
	public void addDefaultDesc(Map<String, String> parameters);
	public void addAuthorizeLine(Map<String, String> parameters);
	public Map<String, String> getAuthorizeLine();
	public void updReportApproval(Map<String, String> parameters);
	public void wholeUpdReportApproval(ReportApproval reportApproval);
	public void updReportApproval2(ReportApproval reportApproval);
	public List<ReportApproval> findApprovalList();
	public ReportApproval findApprovalList2(ReportApproval reportApproval);
	public int findApprovalListCt();
	public List<ReportApproval> findSystemList();
	public void setApprovalList(ReportApproval reportApproval);
	public ReportApproval findAuthorize();

}
