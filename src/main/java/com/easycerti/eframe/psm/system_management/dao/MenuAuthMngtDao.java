package com.easycerti.eframe.psm.system_management.dao;

import java.util.List;

import com.easycerti.eframe.psm.system_management.vo.MenuAuth;

/**
 * 메뉴권한 관리 Dao Interface
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 *  
 *
 * </pre>
 */
public interface MenuAuthMngtDao {

	/**
	 * 메뉴 권한 리스트 가져오기
	 */
	public List<MenuAuth> findMenuAuthMngtList(MenuAuth menuAuth);
	
	/**
	 * 메뉴 권한 리스트 갯수 가져오기
	 */
	public Integer findMenuAuthMngtOne_count(MenuAuth menuAuth);
	
	/**
	 * 메뉴 권한 정보 가져오기
	 */
	public MenuAuth findMenuAuthMngtOne(MenuAuth menuAuth);
	
	/**
	 * 메뉴 권한 추가하기 (단건)
	 */
	public void addMenuAuthMngtOne(MenuAuth menuAuth);
	
	/**
	 * 메뉴 수정하기 (단건)
	 */
	public void saveMenuAuthMngtOne(MenuAuth menuAuth);
	
	/**
	 * 메뉴 삭제하기 (단건)
	 */
	public void removeMenuAuthMngtOne(MenuAuth menuAuth);
	
	/**
	 * 해당 관리자의 권한 체크
	 */
	public Integer findMenuAuthMngtOne_toUserAuth(MenuAuth menuAuth);

	/**
	 * 해당 관리자의 메뉴 권한 체크
	 */
	public Integer findMenuAuthMngtOne_toUserMenuAuth(MenuAuth menuAuth);
	
	/**
	 * 메뉴 별 권한 리스트
	 */
	public List<MenuAuth> findMenuAuthMngtList_toMenu(MenuAuth menuAuth);
	
	/**
	 * 해당 메뉴 id 가져오기
	 */
	public List<String> getMenuIdByUrl(String menu_url);
}
