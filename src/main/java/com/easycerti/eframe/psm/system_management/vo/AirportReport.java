package com.easycerti.eframe.psm.system_management.vo;

public class AirportReport {
	private String st_proc_date;
	private String inst_name;
	private String cnt_client_ip;
	private String cnt_sql;
	private String cnt_application;
	private String sum_resp_time;
	private String sum_server_time;
	private String sum_byte_in;
	private String sum_byte_out;
	private String sum_rtrn_rows;
	private String client_ip;
	private String cnt_inst_name;
	
	private String start_date;
	private String end_date;
	public String getSt_proc_date() {
		return st_proc_date;
	}
	public void setSt_proc_date(String st_proc_date) {
		this.st_proc_date = st_proc_date;
	}
	public String getInst_name() {
		return inst_name;
	}
	public void setInst_name(String inst_name) {
		this.inst_name = inst_name;
	}
	public String getCnt_client_ip() {
		return cnt_client_ip;
	}
	public void setCnt_client_ip(String cnt_client_ip) {
		this.cnt_client_ip = cnt_client_ip;
	}
	public String getCnt_sql() {
		return cnt_sql;
	}
	public void setCnt_sql(String cnt_sql) {
		this.cnt_sql = cnt_sql;
	}
	public String getCnt_application() {
		return cnt_application;
	}
	public void setCnt_application(String cnt_application) {
		this.cnt_application = cnt_application;
	}
	public String getSum_resp_time() {
		return sum_resp_time;
	}
	public void setSum_resp_time(String sum_resp_time) {
		this.sum_resp_time = sum_resp_time;
	}
	public String getSum_server_time() {
		return sum_server_time;
	}
	public void setSum_server_time(String sum_server_time) {
		this.sum_server_time = sum_server_time;
	}
	public String getSum_byte_in() {
		return sum_byte_in;
	}
	public void setSum_byte_in(String sum_byte_in) {
		this.sum_byte_in = sum_byte_in;
	}
	public String getSum_byte_out() {
		return sum_byte_out;
	}
	public void setSum_byte_out(String sum_byte_out) {
		this.sum_byte_out = sum_byte_out;
	}
	public String getSum_rtrn_rows() {
		return sum_rtrn_rows;
	}
	public void setSum_rtrn_rows(String sum_rtrn_rows) {
		this.sum_rtrn_rows = sum_rtrn_rows;
	}
	public String getClient_ip() {
		return client_ip;
	}
	public void setClient_ip(String client_ip) {
		this.client_ip = client_ip;
	}
	public String getCnt_inst_name() {
		return cnt_inst_name;
	}
	public void setCnt_inst_name(String cnt_inst_name) {
		this.cnt_inst_name = cnt_inst_name;
	}
	public String getStart_date() {
		return start_date;
	}
	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}
	public String getEnd_date() {
		return end_date;
	}
	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
}
