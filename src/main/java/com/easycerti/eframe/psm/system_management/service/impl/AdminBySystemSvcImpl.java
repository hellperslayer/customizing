package com.easycerti.eframe.psm.system_management.service.impl;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.StringUtils;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.system_management.dao.AdminBySystemDao;
import com.easycerti.eframe.psm.system_management.dao.AdminUserMngtDao;
import com.easycerti.eframe.psm.system_management.dao.AuthMngtDao;
import com.easycerti.eframe.psm.system_management.service.AdminBySystemSvc;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;
import com.easycerti.eframe.psm.system_management.vo.AdminUserSearch;
import com.easycerti.eframe.psm.system_management.vo.Auth;

@Service
public class AdminBySystemSvcImpl implements AdminBySystemSvc {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private AdminBySystemDao adminBySystemDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private AuthMngtDao authMngtDao;
	
	@Autowired
	private AdminUserMngtDao adminUserMngtDao;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	@Override
	public DataModelAndView findAdminBySystemList(AdminUserSearch search, HttpServletRequest request) {
		int pageSize = Integer.valueOf(defaultPageSize);
		
		AdminUser adminInfo = SystemHelper.getAdminUserInfo(request);
		search.setAuth_id(adminInfo.getAuth_id());

		search.setSize(pageSize);
		search.setTotal_count(adminBySystemDao.findAdminBySystemList_count(search));
		
		List<AdminUser> adminUserList = adminBySystemDao.findAdminBySystemList(search);
		
		SimpleCode simpleCode = new SimpleCode("/adminBySystem/list.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("adminUserList", adminUserList);
		modelAndView.addObject("index_id", index_id);

		return modelAndView;
	}
	
	@Override
	public DataModelAndView findAdminBySystemDetail(String admin_user_id, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView();

		if(StringUtils.notNullCheck(admin_user_id)){
			AdminUser detailBean = adminBySystemDao.findAdminBySystemDetail(admin_user_id); 
			modelAndView.addObject("adminUserDetail", detailBean);
		}
		
		// 권한 리스트
		AdminUser adminInfo = SystemHelper.getAdminUserInfo(request);
		Auth authParamBean = new Auth();
		authParamBean.setUse_flag("Y");
		authParamBean.setAuth_id(adminInfo.getAuth_id());
		List<Auth> auths = authMngtDao.findAuthMngtList(authParamBean);
		
		SimpleCode simpleCode = new SimpleCode("/adminBySystem/detail.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		modelAndView.addObject("auths", auths);
		modelAndView.addObject("index_id",index_id);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView addAdminBySystem(Map<String, String> parameters) {
		AdminUser paramBean = CommonHelper.convertMapToBean(parameters, AdminUser.class);
		
		// Validation Check (admin_user_id 중복)
		AdminUser adminUser = adminUserMngtDao.findAdminUserMngtOne(paramBean.getAdmin_user_id());
		if(adminUser != null) {
			throw new ESException("SYS013J");
		}
		
		//salt값 세팅
		SecureRandom saltValue = null;
		int n = 0;
		try {
			saltValue = SecureRandom.getInstance("SHA1PRNG");
		} catch (NoSuchAlgorithmException e) {
			throw new ESException("SYS012J");
		}
		n = saltValue.nextInt(10);
		paramBean.setSalt_value(String.valueOf(n));
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			adminBySystemDao.addAdminBySystem(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[AdminBySystemSvcImpl.addAdminBySystem] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView saveAdminBySystem(Map<String, String> parameters) {
		AdminUser paramBean = CommonHelper.convertMapToBean(parameters, AdminUser.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			
			adminBySystemDao.saveAdminBySystem(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[AdminBySystemSvcImpl.saveAdminBySystem] MESSAGE : " + e.getMessage());
			throw new ESException("SYS014J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView removeAdminBySystem(Map<String, String> parameters) {
		AdminUser paramBean = CommonHelper.convertMapToBean(parameters, AdminUser.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			adminUserMngtDao.removeAdminUserMngtOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[AdminBySystemSvcImpl.removeAdminBySystem] MESSAGE : " + e.getMessage());
			throw new ESException("SYS015J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	@Override
	public void findAdminBySystemList_download(DataModelAndView modelAndView, AdminUserSearch search,
			HttpServletRequest request) {
		
		AdminUser adminInfo = SystemHelper.getAdminUserInfo(request);
		search.setAuth_id(adminInfo.getAuth_id());
		
		String fileName = "PSM_시스템별_관리자관리_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "시스템별 관리자관리";

		search.setUseExcel("true");
		List<AdminUser> adminUsers = adminBySystemDao.findAdminBySystemList(search);
		
		String[] columns = new String[] {"system_name", "admin_user_id", "admin_user_name", "mobile_number", "email_address", "auth_name"};
		String[] heads = new String[] {"시스템", "관리자ID", "관리자명", "연락처", "E-MAIL", "권한명"};
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", adminUsers);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}
}
