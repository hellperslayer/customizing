package com.easycerti.eframe.psm.system_management.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.psm.system_management.service.MenuMngtSvc;

/**
 * 메뉴 관련 Controller
 * 
 * @author yjyoo
 * @since 2015. 4. 20.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 20.           yjyoo            최초 생성
 *
 * </pre>
 */
@Controller
@RequestMapping(value="/menuMngt/*")
public class MenuMngtCtrl {
	
	@Autowired
	private MenuMngtSvc menuMngtSvc;
	
	/**
	 * 메뉴 리스트</br>
	 * - 메뉴 데이터는 tree 형태
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[환경관리] 메뉴관리")
	@RequestMapping(value="list.html",method={RequestMethod.POST})
	public DataModelAndView findMenuMngtList(@RequestParam Map<String, String> parameters, HttpServletRequest request){

		DataModelAndView modelAndView = menuMngtSvc.findMenuMngtList(parameters);
		modelAndView.setViewName("menuMngtList");
		modelAndView.addObject("sel_menu_id", parameters.get("sel_menu_id"));
		return modelAndView;
	}
	
	/**
	 * 메뉴 상세정보 (json)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="detail.html",method={RequestMethod.POST})
	public DataModelAndView findMenuMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		String[] args = {"menu_id"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		DataModelAndView modelAndView = menuMngtSvc.findMenuMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	
	/**
	 * 메뉴 추가 (json)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="add.html",method={RequestMethod.POST})
	public DataModelAndView addMenuMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		String[] args = {"menu_name", "use_flag"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		parameters.put("insert_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		DataModelAndView modelAndView = menuMngtSvc.addMenuMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	/**
	 * 메뉴 수정 (json)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="save.html",method={RequestMethod.POST})
	public DataModelAndView saveMenuMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		String[] args = {"menu_id", "menu_name", "use_flag"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){ // auth_ids
			throw new ESException("SYS004J");
		}
		
		parameters.put("update_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		DataModelAndView modelAndView = menuMngtSvc.saveMenuMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		modelAndView.addObject("sel_menu_id", parameters.get("menu_id"));
		
		return modelAndView;
	}
	
	/**
	 * 메뉴 삭제 (json)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="remove.html",method={RequestMethod.POST})
	public DataModelAndView removeMenuMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		String[] args = {"menu_id"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		DataModelAndView modelAndView = menuMngtSvc.removeMenuMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
}
