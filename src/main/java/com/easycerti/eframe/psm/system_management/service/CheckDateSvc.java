package com.easycerti.eframe.psm.system_management.service;

import java.util.Map;

import com.easycerti.eframe.common.spring.DataModelAndView;

public interface CheckDateSvc {
	
	public DataModelAndView checkDateList(Map<String, String> parameters);

}
