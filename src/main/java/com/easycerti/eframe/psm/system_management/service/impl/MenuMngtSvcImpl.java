package com.easycerti.eframe.psm.system_management.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.StringUtils;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.system_management.dao.AuthMngtDao;
import com.easycerti.eframe.psm.system_management.dao.MenuAuthMngtDao;
import com.easycerti.eframe.psm.system_management.dao.MenuMngtDao;
import com.easycerti.eframe.psm.system_management.service.MenuMngtSvc;
import com.easycerti.eframe.psm.system_management.vo.Auth;
import com.easycerti.eframe.psm.system_management.vo.Menu;
import com.easycerti.eframe.psm.system_management.vo.MenuAuth;

/**
 * 메뉴 관련 Service Implements
 * 
 * @author yjyoo
 * @since 2015. 4. 20.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 20.           yjyoo            최초 생성
 *
 * </pre>
 */
@Service
public class MenuMngtSvcImpl implements MenuMngtSvc {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private MenuMngtDao menuMngtDao;
	
	@Autowired
	private AuthMngtDao authMngtDao;
	
	@Autowired
	private MenuAuthMngtDao menuAuthMngtDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	@Override
	public DataModelAndView findMenuMngtList(Map<String, String> parameters) {
		Menu paramBean = CommonHelper.convertMapToBean(parameters, Menu.class);
		
		DataModelAndView modelAndView = new DataModelAndView();
		// 메뉴 리스트
		List<Menu> menus = menuMngtDao.findMenuMngtList(paramBean);
		modelAndView.addObject("hierarchyMenus", menus);
		modelAndView.addObject("paramBean", paramBean);
		
		// 권한 리스트
		Auth authParamBean = new Auth();
		authParamBean.setUse_flag("Y");
		List<Auth> auths = authMngtDao.findAuthMngtList(authParamBean);
		modelAndView.addObject("auths", auths);
		
		SimpleCode simpleCode = new SimpleCode("/menuMngt/list.html");
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView findMenuMngtOne(Map<String, String> parameters) {
		Menu paramBean = CommonHelper.convertMapToBean(parameters, Menu.class);
		
		Menu detailMenu = menuMngtDao.findMenuMngtOne(paramBean);
		
		// 해당 메뉴의 권한 리스트
		MenuAuth menuAuthParamBean = new MenuAuth();
		menuAuthParamBean.setMenu_id(paramBean.getMenu_id());
		List<MenuAuth> menuAuths = menuAuthMngtDao.findMenuAuthMngtList_toMenu(menuAuthParamBean);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean",paramBean);
		modelAndView.addObject("menuDetail", detailMenu);
		modelAndView.addObject("menuAuths", menuAuths);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView addMenuMngtOne(Map<String, String> parameters) {
		Menu paramBean = CommonHelper.convertMapToBean(parameters, Menu.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			menuMngtDao.addMenuMngtOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[MenuMngtSvcImpl.addMenuMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS026J");
		}
		
//		if(paramBean.getAuths_ids() != null && !SystemResource.EMPTY_STRING.equals(paramBean.getAuth_ids())){
//			// 요청된 메뉴권한 매핑정보 추가
//			String[] menuAuths = paramBean.getAuth_ids().split(",");
//			for(String mappingMenuAuth : menuAuths){
//				MenuAuthBean menuAuthBean = new MenuAuth();
//				menuAuthBean.setMenu_id(paramBean.getMenu_id());
//				menuAuthBean.setAuth_id(mappingMenuAuth);
//				menuAuthBean.setInsert_user_id(paramBean.getUpdate_user_id());
//				menuAuthMngtDao.addMenuAuthMngtOne(menuAuthBean);
//			}
//		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView saveMenuMngtOne(Map<String, String> parameters) {
		Menu paramBean = CommonHelper.convertMapToBean(parameters, Menu.class);
		boolean batchBool = false;
		List<Menu> subMenuList = new ArrayList<Menu>();
		MenuAuth menuAuthParamBean = new MenuAuth();
		menuAuthParamBean.setMenu_id(paramBean.getMenu_id());
		menuMngtDao.saveMenuMngtOne(paramBean);
		if(paramBean.getSub_batch()!=null&&paramBean.getSub_batch().equals("on")) {
			batchBool = true;
		}
		// 메뉴관리 화면
 		if (paramBean.getMenu_id().equals("MENU00012")) {
 			if (paramBean.getUse_flag().equals('N')) {	// 미사용 방지
				throw new ESException("SYS201J");
			}
			
			if (paramBean.getAuth_ids().length() <= 0) { // 하나 이상의 권한 필요
				throw new ESException("SYS202J");
			}
		}
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			// 기존 메뉴권한 매핑정보 삭제
			menuAuthMngtDao.removeMenuAuthMngtOne(menuAuthParamBean);
			
			//하위 메뉴까지 일괄 적용할 경우 하위메뉴 같이 삭제
			if (batchBool) {
				subMenuList = menuMngtDao.findChildMenuList(paramBean.getMenu_id()); //하위메뉴 리스트 불러옴
				for (Menu menu : subMenuList) {
					MenuAuth menuAuth = new MenuAuth();
					menuAuth.setAuth_id(menuAuthParamBean.getAuth_id());
					menuAuth.setMenu_id(menu.getMenu_id());
					menuAuthMngtDao.removeMenuAuthMngtOne(menuAuth);
				}
			}
			
			if(StringUtils.notNullCheck(paramBean.getAuth_ids())){
				// 요청된 메뉴권한 매핑정보 추가
				String[] menuAuths = paramBean.getAuth_ids().split(",");
				for(String mappingMenuAuth : menuAuths){
					MenuAuth menuAuthBean = new MenuAuth();
					if(batchBool) {	// 0
						for (Menu menu : subMenuList) { // 하위 메뉴까지 동시에 같은 권한을 추가하는 반복분
							menuAuthBean.setMenu_id(menu.getMenu_id());
							menuAuthBean.setAuth_id(mappingMenuAuth);
							menuAuthBean.setInsert_user_id(paramBean.getUpdate_user_id());
							menuAuthMngtDao.addMenuAuthMngtOne(menuAuthBean);
						}
					}else {	// 선택만 메뉴만 권한을 추가하는 부분
						menuAuthBean.setMenu_id(paramBean.getMenu_id());
						menuAuthBean.setAuth_id(mappingMenuAuth);
						menuAuthBean.setInsert_user_id(paramBean.getUpdate_user_id());
						menuAuthMngtDao.addMenuAuthMngtOne(menuAuthBean);
					}
				}
			}
			menuMngtDao.saveMenuMngtOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[MenuMngtSvcImpl.saveMenuMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS027J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}

	@Override
	public DataModelAndView removeMenuMngtOne(Map<String, String> parameters) throws Exception {
		Menu paramBean = CommonHelper.convertMapToBean(parameters, Menu.class);
		
		Integer childCount = menuMngtDao.findMenuMngtOne_childCount(paramBean);
		if(childCount == null || childCount.intValue() > 0) {
			throw new ESException("SYS029J");
		}
		
		// 메뉴관리 화면 삭제 불가
		if (paramBean.getMenu_id().equals("MENU00012")) {
			throw new ESException("SYS203J");
		}
		
		MenuAuth menuAuthParamBean = new MenuAuth();
		menuAuthParamBean.setMenu_id(paramBean.getMenu_id());
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			// 메뉴권한 삭제
			menuAuthMngtDao.removeMenuAuthMngtOne(menuAuthParamBean);
			menuMngtDao.removeMenuMngtOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[MenuMngtSvcImpl.removeMenuMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS028J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}

	@Override
	public DataModelAndView findMenuMngtList_forHierarchy(Map<String, String> parameters) {
		return null;
	}
	
}
