package com.easycerti.eframe.psm.system_management.dao;

import java.util.List;

import com.easycerti.eframe.psm.system_management.vo.AccessAuth;
import com.easycerti.eframe.psm.system_management.vo.AccessAuthSearch;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

public interface AccessAuthDao {
	
	public List<AccessAuth> findAccessAuthList(AccessAuthSearch search);		
	public int findAccessAuthList_count(AccessAuthSearch search);
	public void addAccessAuth(AccessAuth accessAuth);
	public SystemMaster findSystemSeq(String system_name);
	public AccessAuth findAccessAuthDetail(AccessAuthSearch search);
	public void saveAccessAuth(AccessAuth accessAuth);
	public void removeAccessAuth(AccessAuth accessAuth);
	public int findAccessAuth_validation(AccessAuth accessAuth);
	public AccessAuth findAccessAuthInfo(AccessAuth accessAuth);
	public List<SystemMaster> findSystemMasterList_byAdmin(AccessAuthSearch search);
}
