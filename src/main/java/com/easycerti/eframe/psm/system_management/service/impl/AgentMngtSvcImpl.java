package com.easycerti.eframe.psm.system_management.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.history_management.dao.AgentMasterHistDao;
import com.easycerti.eframe.psm.history_management.vo.AgentMasterHist;
import com.easycerti.eframe.psm.system_management.dao.AgentMngtDao;
import com.easycerti.eframe.psm.system_management.dao.AlarmMngtDao;
import com.easycerti.eframe.psm.system_management.service.AgentMngtSvc;
import com.easycerti.eframe.psm.system_management.vo.AgentMaster;
import com.easycerti.eframe.psm.system_management.vo.AgentMasterSearch;
import com.easycerti.eframe.psm.system_management.vo.AlarmMngt;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;
import com.easycerti.psm.log.generator.util.CommandClient;

/**
 * 에이전트 관련 Service Implements
 * 
 * @author yjyoo
 * @since 2015. 4. 24.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 24.           yjyoo            최초 생성
 *
 * </pre>
 */
@Service
public class AgentMngtSvcImpl implements AgentMngtSvc {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private AlarmMngtDao alarmMngtDao;
	
	@Autowired
	private AgentMngtDao agentMngtDao;

	@Autowired
	private AgentMasterHistDao agentMasterHistDao;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	@Autowired
	private CommonDao commonDao;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;
	
	@Override
	public DataModelAndView findAgentMngtList(AgentMasterSearch search) {
		int pageSize = Integer.valueOf(defaultPageSize);
		
		search.setSize(pageSize);
		search.setTotal_count(agentMngtDao.findAgentMngtOne_count(search));
		
		//search.setAgent_type("F");	// 일단 Filter Agent만 가져오기
		
		List<AgentMaster> agentMasterList = agentMngtDao.findAgentMngtList(search);
		
		List<AlarmMngt> alarm_info = alarmMngtDao.findAlarmMngtList_email();
		List<AgentMaster> license_hist = agentMngtDao.findLicense_history();
		
		// 상태 정보 가져오기 (socket)
		String retStatus = "";
		for (AgentMaster am : agentMasterList){
			try{
				String ip = am.getIp();
				int port = Integer.parseInt(am.getPort());
				
				retStatus = CommandClient.sendCommand(ip, port, "CheckAgentEnable", "tmp");
				
				if( retStatus != null && retStatus.indexOf("use") > -1 ){
					am.setStatus("use");
				}
				else if(retStatus != null && retStatus.indexOf("false") > -1 ){
					am.setStatus("false");
				}else {
					am.setStatus("unknown");
				}
			}catch (Exception e){
				am.setStatus("unknown");
			}
		}
		
		SimpleCode simpleCode = new SimpleCode("/agentMngt/list.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("license_hist", license_hist);
		modelAndView.addObject("alarm_info", alarm_info);
		modelAndView.addObject("agentList", agentMasterList);
		modelAndView.addObject("index_id", index_id);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView findAgentMngtDetail(AgentMaster search) {

		DataModelAndView modelAndView = new DataModelAndView();
		
		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterList();
		modelAndView.addObject("systemMasterList", systemMasterList);
		
		AgentMaster agent = agentMngtDao.findAgentMngtDetail(search);
		modelAndView.addObject("agent", agent);
		
		SimpleCode simpleCode = new SimpleCode("/agentMngt/detail.html");
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView addAgent(AgentMaster search) {
		
		AgentMaster agent = agentMngtDao.findAgentMngtDetail(search);
		if(agent != null) {
			throw new ESException("SYS401J");
		}
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		
		try {
			agentMngtDao.addAgent(search);
			
			AgentMasterHist amhParamBean = new AgentMasterHist();
			amhParamBean.setAction_log_id(CommonHelper.getGUID());
			amhParamBean.setAgent_seq(search.getAgent_seq());
			amhParamBean.setServer_seq("01");
			amhParamBean.setAction("use");
			amhParamBean.setMessage("SUCCESS");
			agentMasterHistDao.addAgentMasterHistOne(amhParamBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[AgentMngtSvcImpl.addAgent] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS402J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("search", search);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView removeAgent(AgentMaster search) {
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		
		try {
			agentMngtDao.removeAgent(search);
			//agentMngtDao.removeAgentHist(search);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[AgentMngtSvcImpl.removeAgent] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS403J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("search", search);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView saveAgent(AgentMaster search) {
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		
		try {
			agentMngtDao.updateAgent(search);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[AgentMngtSvcImpl.saveAgent] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS404J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("search", search);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView actionAgent(Map<String, String> parameters) {
		AgentMaster paramBean = CommonHelper.convertMapToBean(parameters, AgentMaster.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			String ip = paramBean.getIp();
			int port = Integer.parseInt(paramBean.getPort());
			String value = paramBean.getStatus();
			
			String actionResult = CommandClient.sendCommand(ip, port, "ez.mon.agent", value);
			
			AgentMasterHist amhParamBean = new AgentMasterHist();
			amhParamBean.setAction_log_id(CommonHelper.getGUID());
			amhParamBean.setAgent_seq(paramBean.getAgent_seq());
			amhParamBean.setServer_seq(paramBean.getServer_seq());
			amhParamBean.setAction(value);
			
			if(actionResult == null) {
				amhParamBean.setMessage("ERROR");
				paramBean.setResult("0");
			}
			else {
				amhParamBean.setMessage("SUCCESS");
				paramBean.setResult("1");
			}
			
			// 에이전트동작정지이력 추가
			agentMasterHistDao.addAgentMasterHistOne(amhParamBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[AgentMngtSvcImpl.actionAgent] MESSAGE : " + e.getMessage());
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView setLicenCe_Hist(Map<String, String> parameters) {
		AgentMaster paramBean = CommonHelper.convertMapToBean(parameters, AgentMaster.class);
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			
			AgentMaster agentMaster = new AgentMaster();
			agentMngtDao.setLicense_Hist(paramBean);
			
			transactionManager.commit(transactionStatus);
			
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[AlarmMngtSvcImpl.setAlarmMngt] MESSAGE : " + e.getMessage());
			throw new ESException("SYS035J");
		}
		
		return null;
	}
}
