package com.easycerti.eframe.psm.system_management.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpRequest;
import org.springframework.web.multipart.MultipartFile;

import com.easycerti.eframe.common.spring.DataModelAndView;

public interface ReportOptionSvc {
	public DataModelAndView reportOptionList(Map<String, String> parameters, HttpServletRequest request); 
	public DataModelAndView updGeneral(Map<String, String> parameters);
	public void useLogoImage(int idx, int type);
	public DataModelAndView addFile(MultipartFile file, HttpServletRequest request);
	public DataModelAndView addFile2(MultipartFile file, HttpServletRequest request);
	public void useLogoImage2(int idx, int type);
	public void deleteLogoImage(int idx);
	public String addAuthorizeLine(Map<String, String> parameters);
	public String addDefaultDesc(Map<String, String> parameters); 
	public String findDefaultDesc(Map<String, String> parameters);
	public String updReportApproval(Map<String, String> parameters);
	public DataModelAndView wholeUpdReportApproval(Map<String, String> parameters);
	public void updReportApproval();
	

}
