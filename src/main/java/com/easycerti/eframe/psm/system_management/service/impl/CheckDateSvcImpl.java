package com.easycerti.eframe.psm.system_management.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.system_management.dao.CheckDateDao;
import com.easycerti.eframe.psm.system_management.service.CheckDateSvc;
import com.easycerti.eframe.psm.system_management.vo.CheckDate;

@Service
public class CheckDateSvcImpl implements CheckDateSvc {
	
	@Autowired
	CheckDateDao checkDateDao;
	
	@Autowired
	CommonDao commonDao;
		
	@Override
	public DataModelAndView checkDateList(Map<String, String> parameters) {
		CheckDate paramBean = CommonHelper.convertMapToBean(parameters, CheckDate.class);
		SimpleCode simpleCode = new SimpleCode();
		String index = "checkDate/list.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		String checkDate = checkDateDao.checkDateList(paramBean);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("checkDate", checkDate);
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}

}
