package com.easycerti.eframe.psm.system_management.service.impl;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.StringUtils;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.core.util.SessionManager;
import com.easycerti.eframe.psm.statistics.dao.StatisticsDao;
import com.easycerti.eframe.psm.system_management.dao.AdminUserMngtDao;
import com.easycerti.eframe.psm.system_management.dao.AuthMngtDao;
import com.easycerti.eframe.psm.system_management.dao.DepartmentMngtDao;
import com.easycerti.eframe.psm.system_management.dao.OptionSettingDao;
import com.easycerti.eframe.psm.system_management.service.AdminUserMngtSvc;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;
import com.easycerti.eframe.psm.system_management.vo.AdminUserSearch;
import com.easycerti.eframe.psm.system_management.vo.Auth;
import com.easycerti.eframe.psm.system_management.vo.Department;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

/**
 * 관리자 관련 Service Implements
 * 
 * @author yjyoo
 * @since 2015. 4. 20.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 20.           yjyoo            최초 생성
 *
 * </pre>
 */
@Service
public class AdminUserMngtSvcImpl implements AdminUserMngtSvc {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private AdminUserMngtDao adminUserMngtDao;
	
	@Autowired
	private AuthMngtDao authMngtDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private DepartmentMngtDao departmentMngtDao;
	
	@Autowired
	private OptionSettingDao optionSettingDao;
	
	@Autowired
	private StatisticsDao statisticsDao;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;
	
	// 비밀번호 확인값 (properties 확인)
	@Value("#{configProperties.banMinute}")
	private String banMinute;
	
	// 비밀번호 변경기간
	@Value("#{configProperties.rePasswordDay}")
	private String rePasswordDay;
	
	@Value("#{configProperties.init_password}")
	private String init_password;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	@Override
	public DataModelAndView login(Map<String, String> parameters, HttpServletRequest request) {
		AdminUser paramBean = CommonHelper.convertMapToBean(parameters, AdminUser.class);
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		AdminUser checkBean = new AdminUser();

		String isRePassword = "N";
		//String isRePassword_sha = "N";
		String isInitLogin = "N";
		String isInspection = "N";
		String inspection_date = "";
		String ui_type = commonDao.getUiType();
		
		// ID 등록 여부 확인
		checkBean.setAdmin_user_id(paramBean.getAdmin_user_id());
		AdminUser adminUserBean_id = adminUserMngtDao.findAdminUserMngtOne_forLogin(checkBean);
		
		// 위임자 체크
		/*String delegationCheck_date = adminUserMngtDao.delegationCheckterm(checkBean);
		String delegationCheck_user = adminUserMngtDao.delegationCheckUser(checkBean);
		if(delegationCheck_user != null) {
			if(delegationCheck_date == null) {
				throw new ESException("SYS056J");
			}
		}*/
		
		// 등록된 ID 없음
		if(adminUserBean_id == null){
			throw new ESException("SYS056J");
			//throw new ESException("SYS058J");
		}
		// 등록된 ID 있음
		else {
			//사용기한
			Date today = new Date();
			SimpleDateFormat currentDate = new SimpleDateFormat("yyyyMMdd");
			
			String use_by_date = adminUserMngtDao.checktime();
			
			if(use_by_date!=null&&use_by_date!=""){
				adminUserBean_id.setUse_by_date(use_by_date);
			}
			
			int useByDate = Integer.parseInt(adminUserBean_id.getUse_by_date());  
			int userCurrentDate = Integer.parseInt(currentDate.format(today));  
			//사용기한 비교
			if(userCurrentDate >= useByDate){
				//사용기간 만료
				throw new ESException("SYS007J");
				
			}else{
				String et = "M";
				et+="D";
				et+="5";
				int pwd_check = adminUserBean_id.getPwd_check();								// 비밀번호 확인값
				Calendar calendar = Calendar.getInstance();
				Date now_datetime = calendar.getTime();											// 현재 시간
				Calendar calendar2 = Calendar.getInstance();
				calendar2.add(Calendar.MONTH, -6);
				Date monthBefore_datetime = calendar2.getTime();													// 장기 미사용 계정 확인용 6개월전 시간
				Date last_login_success_datetime = adminUserBean_id.getLast_login_success_datetime();	// 마지막 로그인 실패 시간
				Date last_login_fail_datetime = adminUserBean_id.getLast_login_fail_datetime();	// 마지막 로그인 실패 시간
				Date permit_datetime = null;													// 로그인 허가 시간
				if(last_login_fail_datetime != null) {
					calendar.setTime(last_login_fail_datetime);
					calendar.add(Calendar.MINUTE, Integer.parseInt(banMinute));
					permit_datetime = calendar.getTime();
				} 
				
				// 1. 비밀번호 확인
				if(ui_type.equals("G") || ui_type.equals("518a")) {
					checkBean.setPassword(paramBean.getPassword()+adminUserBean_id.getSalt_value());
				} else {
					checkBean.setPassword(paramBean.getPassword());
				}
				
				int pw_length = adminUserBean_id.getPassword().length();
				if (pw_length <= 32) {
					checkBean.setEncrypt_type(et);
				}
				checkBean.setSso_yn(paramBean.getSso_yn());
				AdminUser adminUserBean_pw = adminUserMngtDao.findAdminUserMngtOne_forLogin(checkBean);
				
				// 1-1. 비밀번호 틀림
				if(adminUserBean_pw == null) {
					checkBean.setPwd_check(pwd_check - 1);					// 비밀번호 확인값 차감 (-1)
					checkBean.setLast_login_fail_datetime(now_datetime);	// 현재 시간
					adminUserMngtDao.saveAdminUserMngtOne_login(checkBean);
					
					// 1-1-1. 남은 비밀번호 확인값이 2 이상일 경우엔 남은횟수 메시지 보여줌
					if(pwd_check > 1) {
						throw new ESException("SYS99" + (pwd_check - 1) + "J");
						//throw new ESException("SYS058J");
					}
					// 1-1-2. 남은 비밀번호 확인값이 1일 경우엔 가능횟수 초과 메시지 보여줌
					else if(pwd_check <= 1) {
						throw new ESException("SYS057J");
					}
				}
				// 1-2. 비밀번호 맞음
				else {
					// 최초 로그인
					if (adminUserBean_id.getLast_login_success_datetime() == null &&
							adminUserBean_id.getUpdate_datetime() == null) {
						isRePassword = "Y";
						isInitLogin = "Y";
					} else {
						// 중복 로그인 방지
						SessionManager userSession = SessionManager.getInstance();
						String requestId = adminUserBean_id.getAdmin_user_id() + "/" + request.getRemoteAddr();
						List<String> userSessionId = userSession.printLoginUserIds();
						String sessionMapIp = userSession.printLoginUserIp(adminUserBean_id.getAdmin_user_id());
//						System.out.println("================================================");
						for(int i=0; i<userSessionId.size(); i++){
							System.out.println("[" + now_datetime + "] userId : " + userSessionId.get(i));
						}
//						System.out.println("================================================");
						if(sessionMapIp != null){
							if(!sessionMapIp.equals(request.getRemoteAddr())){
								userSession.removeSession(adminUserBean_id.getAdmin_user_id()+"/"+sessionMapIp);
//								System.out.println(userSession.getUserCount());
							}
						}
						
						// 2. 장기 미사용 계정 확인
						// 2-1. 최근 로그인 시간이 6개월 이전이거나 USER_LOCK(계정잠금여부)가 Y이면 로그인 불가, 담당자가 풀어주어야함
						if(last_login_success_datetime != null && (monthBefore_datetime.after(last_login_success_datetime) || adminUserBean_id.getUser_lock().equals("Y"))) {
							adminUserMngtDao.saveAdminUserMngtOne_toUserLock(adminUserBean_id);			//계정 잠금 상태를 Y로 변환
							throw new ESException("SYS062J");
						}
						
						// 3. 마지막 로그인 실패 시간 확인
						// 3-1. 마지막 로그인 실패 시간이 없거나 마지막 로그인 실패 시간에서 10분이 지난 경우 (정상 로그인 처리 진행)
						if(permit_datetime == null || now_datetime.after(permit_datetime)) {
							checkBean.setPwd_check(5);	// 비밀번호 확인값 기본 설정값으로 초기화
							checkBean.setLast_login_success_datetime(now_datetime);	// 마지막 로그인 성공 시간 업데이트
							adminUserMngtDao.saveAdminUserMngtOne_login(checkBean);
							CommonHelper.saveSessionUser(request, adminUserBean_pw);
							userSession.setSession(request.getSession(), requestId, request);
//							System.out.println(userSession.getUserCount());
							
							// 비밀번호 변경일자 확인 
							Calendar cal = Calendar.getInstance();
							String changePwdDay = optionSettingDao.getChangePasswordDay();
							cal.set(Calendar.DATE, -(changePwdDay.length() <= 0 ? 0 : Integer.parseInt(changePwdDay)));
							
							if (pw_length <= 32) {
								//isRePassword = "Y";
								//isRePassword_sha = "Y";
								checkBean.setNewPassword(checkBean.getPassword());
								adminUserMngtDao.saveAdminUserMngtRePassword(checkBean);
							} else {
								if(adminUserBean_id.getLast_pwd_change_datetime().getTime() <=  cal.getTime().getTime()){
									isRePassword = "Y";
								} 
							}
							
							// 점검일 체크
//							String next_date = statisticsDao.getInspectionHistMaxNextDate();
//							if (next_date != null) {
//								cal = Calendar.getInstance();
//								cal.add(Calendar.DAY_OF_MONTH, 7);
//								SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
//								String check_date = sdf.format(cal.getTime());
//								int check = check_date.compareTo(next_date);
//								if (check >= 0) {
//									isInspection = "Y";
//									inspection_date = next_date;
//								} 
//							}
						}
						// 3-2. 마지막 로그인 실패 시간에서 10분이 지나지 않은 경우
						else if(permit_datetime != null || now_datetime.before(permit_datetime)) {
							// 4. 비밀번호 확인값 확인
							// 4-1. 남은 비밀번호 확인값이 1 이상일 경우 (정상 로그인 처리 진행) 
							if(adminUserBean_pw.getPwd_check() >= 1) {
								checkBean.setPwd_check(5);	// 비밀번호 확인값 기본 설정값으로 초기화
								checkBean.setLast_login_success_datetime(now_datetime);	// 마지막 로그인 성공 시간 업데이트
								adminUserMngtDao.saveAdminUserMngtOne_login(checkBean);
								CommonHelper.saveSessionUser(request, adminUserBean_pw);
								userSession.setSession(request.getSession(), requestId, request);
//								System.out.println(userSession.getUserCount());
								
								// 비밀번호 변경일자 확인 
								Calendar cal = Calendar.getInstance();
								String changePwdDay = optionSettingDao.getChangePasswordDay();
								cal.set(Calendar.DATE, -(changePwdDay.length() <= 0 ? 0 : Integer.parseInt(changePwdDay)));
								
								if (pw_length <= 32) {
									//isRePassword = "Y";
									//isRePassword_sha = "Y";
									checkBean.setNewPassword(checkBean.getPassword());
									adminUserMngtDao.saveAdminUserMngtRePassword(checkBean);
								} else {
									if(adminUserBean_id.getLast_pwd_change_datetime().getTime() <=  cal.getTime().getTime()){
										isRePassword = "Y";
									}
								}
								
								// 점검일 체크
//								String next_date = statisticsDao.getInspectionHistMaxNextDate();
//								if (next_date != null) {
//									cal = Calendar.getInstance();
//									cal.add(Calendar.DAY_OF_MONTH, 7);
//									SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
//									String check_date = sdf.format(cal.getTime());
//									int check = check_date.compareTo(next_date);
//									if (check >= 0) {
//										isInspection = "Y";
//										inspection_date = next_date;
//									} 
//								}
							}
							// 4-2. 남은 비밀번호 확인값이 1 미만일 경우
							else if(adminUserBean_pw.getPwd_check() < 1) {
								checkBean.setPwd_check(adminUserBean_pw.getPwd_check());	// 현재 비밀번호 확인값
								checkBean.setLast_login_fail_datetime(now_datetime);		// 현재 시간
								adminUserMngtDao.saveAdminUserMngtOne_login(checkBean);
								throw new ESException("SYS057J");
							}
						}
					}
				}
			}
			
		
		}
		
		modelAndView.addObject("paramBean", paramBean);
		modelAndView.addObject("isRePassword", isRePassword);
		//modelAndView.addObject("isRePassword_sha", isRePassword_sha);
		modelAndView.addObject("isInitLogin", isInitLogin);
		modelAndView.addObject("isInspection", isInspection);
		if (isInspection.equals("Y")) {
			modelAndView.addObject("inspection_date", inspection_date);
		}
		return modelAndView;
	}

	@Override
	public DataModelAndView logout(Map<String, String> parameters, HttpServletRequest request) throws Exception {
		
		SessionManager sessionManager = SessionManager.getInstance();
		sessionManager.deleteLoginTime(request);
		
		CommonHelper.removeSessionUser(request);
		CommonHelper.removeSessionMenuInfo(request);
		
		return new DataModelAndView();
	}

	@Override
	public DataModelAndView findAdminUserMngtList(AdminUserSearch search, HttpServletRequest request) {
		int pageSize = Integer.valueOf(defaultPageSize);
		
		AdminUser adminInfo = SystemHelper.getAdminUserInfo(request);
		search.setAuth_id(adminInfo.getAuth_id());
		
		String sameAuthOption = (String) request.getSession().getAttribute("same_auth_edit");
		search.setOption(sameAuthOption);
		
		search.setSize(pageSize);
		search.setTotal_count(adminUserMngtDao.findAdminUserMngtOne_count(search));
		
		List<AdminUser> adminUserList = adminUserMngtDao.findAdminUserMngtList(search);
		
		Department departmentBean = new Department();
		departmentBean.setUse_flag('Y');
		
		List<Department> deptList = departmentMngtDao.findDepartmentMngtList_choose(departmentBean);
		
		SimpleCode simpleCode = new SimpleCode("/adminUserMngt/list.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("adminUserList", adminUserList);
		modelAndView.addObject("deptList", deptList);
		modelAndView.addObject("index_id", index_id);

		return modelAndView;
	}
	
	@Override
	public void findAdminUserMngtList_download(DataModelAndView modelAndView, AdminUserSearch search, HttpServletRequest request) {
		AdminUser adminInfo = SystemHelper.getAdminUserInfo(request);
		search.setAuth_id(adminInfo.getAuth_id());
		
		String fileName = "PSM_관리자관리_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "관리자관리";

		search.setUseExcel("true");
		List<AdminUser> adminUsers = adminUserMngtDao.findAdminUserMngtList(search);
		
		for(AdminUser adminUser : adminUsers) {
			String alarm_flag = adminUser.getAlarm_flag();
			adminUser.setAlarm_flag(alarm_flag.equals("Y") ? "사용" : "미사용");
		}
		
//		String[] columns = new String[] {"admin_user_id", "admin_user_name", "dept_name", "email_address", "auth_name", "alarm_flag"};
//		String[] heads = new String[] {"관리자ID", "관리자명", "소속", "E-MAIL", "권한명", "알람여부"};

		String[] columns = new String[] {"admin_user_id", "admin_user_name", "dept_name", "email_address", "auth_name"};
		String[] heads = new String[] {"관리자ID", "관리자명", "소속", "E-MAIL", "권한명"};
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", adminUsers);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}

	@Override
	public DataModelAndView findAdminUserMngtOne(String admin_user_id, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView();

		if(StringUtils.notNullCheck(admin_user_id)){
			AdminUser detailBean = adminUserMngtDao.findAdminUserMngtOne(admin_user_id); 
			modelAndView.addObject("adminUserDetail", detailBean);
		}
		
		// 권한 리스트
		AdminUser adminInfo = SystemHelper.getAdminUserInfo(request);
		Auth authParamBean = new Auth();
		authParamBean.setUse_flag("Y");
		authParamBean.setAuth_id(adminInfo.getAuth_id());
		
		String sameAuthOption = (String) request.getSession().getAttribute("same_auth_edit");
		authParamBean.setOption(sameAuthOption);
		
		List<Auth> auths = authMngtDao.findAuthMngtList(authParamBean);
		
		// 부서 리스트
		Department departmentParamBean = new Department();
		departmentParamBean.setUse_flag('Y');
		List<Department> departments = departmentMngtDao.findDepartmentMngtList_choose(departmentParamBean);
		
		
		SimpleCode simpleCode = new SimpleCode("/adminUserMngt/detail.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		modelAndView.addObject("auths", auths);
		modelAndView.addObject("departments",departments);
		modelAndView.addObject("index_id",index_id);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView addAdminUserMngtOne(Map<String, String> parameters) {
		AdminUser paramBean = CommonHelper.convertMapToBean(parameters, AdminUser.class);
		String ui_type = commonDao.getUiType();
		
		// Validation Check (admin_user_id 중복)
		AdminUser adminUser = adminUserMngtDao.findAdminUserMngtOne(paramBean.getAdmin_user_id());
		if(adminUser != null) {
			throw new ESException("SYS013J");
		}
		
		if(ui_type.equals("G") || ui_type.equals("518a")) {
			SecureRandom saltValue = null;
			String saltStr = "";
			try {
				saltValue = SecureRandom.getInstance("SHA1PRNG");
				for(int i = 0; i < 10; i++) {
					saltStr += saltValue.nextInt(10);
				}
			} catch (NoSuchAlgorithmException e) {
				throw new ESException("SYS012J");
			}
			paramBean.setPassword(paramBean.getAdmin_user_id()+saltStr);
			paramBean.setSalt_value(saltStr);
		} else {
			paramBean.setPassword(paramBean.getAdmin_user_id());
			paramBean.setSalt_value("a");
		}
		
		// 초기비밀번호 세팅
		//paramBean.setPassword(init_password);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			adminUserMngtDao.addAdminUserMngtOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[AdminUserMngtSvcImpl.addAdminUserMngtOne] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}

	@Override
	public DataModelAndView saveAdminUserMngtOne(Map<String, String> parameters) {
		AdminUser paramBean = CommonHelper.convertMapToBean(parameters, AdminUser.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			
			if (paramBean.getUser_lock().equals("N")) {
				paramBean.setLast_login_success_datetime(new Date());
			}
			adminUserMngtDao.saveAdminUserMngtOne(paramBean);
			
			//System.out.println("#######################" + paramBean);
			
			
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[AdminUserMngtSvcImpl.saveAdminUserMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS014J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}

	@Override
	public DataModelAndView removeAdminUserMngtOne(Map<String, String> parameters) {
		AdminUser paramBean = CommonHelper.convertMapToBean(parameters, AdminUser.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			adminUserMngtDao.removeAdminUserMngtOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[AdminUserMngtSvcImpl.removeAdminUserMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS015J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}

	@Override
	public DataModelAndView rePassword(Map<String, String> parameters, HttpServletRequest request) throws Exception {
		AdminUser paramBean = CommonHelper.convertMapToBean(parameters, AdminUser.class);
		paramBean.setPassword(paramBean.getCurrentPasswd());
		AdminUser adminUser = adminUserMngtDao.findAdminUserMngtOne_forLogin(paramBean);
		String ui_type = commonDao.getUiType();
		String res = "";
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		if(adminUser != null){
			if(ui_type.equals("G") || ui_type.equals("518a")) {
				adminUser.setNewPassword(parameters.get("newPasswd")+adminUser.getSalt_value());
			} else {
				adminUser.setNewPassword(parameters.get("newPasswd"));
			}
			
			try {
				String userId = parameters.get("admin_user_id");
				String newPasswd = parameters.get("newPasswd");
				String p = "^(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!%*#?&])[A-Za-z[0-9]$@$!%*#?&]{9,20}$";
				Matcher m = Pattern.compile(p).matcher(newPasswd); 
				
				if(Pattern.compile(p).matcher(newPasswd).find()) {
					adminUserMngtDao.saveAdminUserMngtRePassword(adminUser);
					adminUserMngtDao.savePasswordHist(userId,newPasswd);
					transactionManager.commit(transactionStatus);
					res="SUCCESS";
					
				}
				if(!Pattern.compile(p).matcher(newPasswd).find()) {
					res = "ERROR01";
					transactionManager.rollback(transactionStatus);
				}
				
				
			} catch(DataAccessException e) {
				transactionManager.rollback(transactionStatus);
				logger.error("[AdminUserMngtSvcImpl.saveRePassword] MESSAGE : " + e.getMessage());
				throw new ESException("SYS014J");
			}
		} else {
			throw new ESException("SYS071J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		modelAndView.addObject("res", res);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView changePassword(Map<String, String> parameters, HttpServletRequest request)
			throws Exception {
		AdminUser paramBean = CommonHelper.convertMapToBean(parameters, AdminUser.class);
		paramBean.setPassword(paramBean.getCurrentPasswd());
		AdminUser adminInfo = SystemHelper.getAdminUserInfo(request);
		paramBean.setAdmin_user_id(adminInfo.getAdmin_user_id());
		String ui_type = commonDao.getUiType();
		String res = "";
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		AdminUser adminUser = adminUserMngtDao.findAdminUserMngtOne_forLogin(paramBean);
		if(adminUser != null){
			if(ui_type.equals("G") || ui_type.equals("518a")) {
				adminUser.setNewPassword(parameters.get("newPasswd")+adminUser.getSalt_value());
			} else {
				adminUser.setNewPassword(parameters.get("newPasswd"));
			}
			
			try {
				String newPasswd = parameters.get("newPasswd");
				String p = "^(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!%*#?&])[A-Za-z[0-9]$@$!%*#?&]{9,20}$";
				
				Matcher m = Pattern.compile(p).matcher(newPasswd); 
				
				String userid = adminUser.getAdmin_user_id();
				String userPassword = adminUser.getNewPassword();
				
				//비밀번호 변경 내역 조회
				String passwordHist = adminUserMngtDao.findPasswordHist(userid,userPassword);
				
				//이전에 사용한 적 없는 비밀번호
				if(Pattern.compile(p).matcher(newPasswd).find()&&passwordHist==null) {
					//비밀번호 변경
					adminUserMngtDao.saveAdminUserMngtRePassword(adminUser);
					//비밀번호 변경 기록 저장
					adminUserMngtDao.savePasswordHist(userid,userPassword);
					transactionManager.commit(transactionStatus);
					res = "SUCCESS";
				}
				//양식에 맞지 않는 비밀번호
				if(!Pattern.compile(p).matcher(newPasswd).find()) {
					res = "ERROR01";
					transactionManager.rollback(transactionStatus);
				}
				//이전에 사용한 적 있는 비밀번호
				if(Pattern.compile(p).matcher(newPasswd).find()&&passwordHist!=null) {
					res = "ALREADY_USED";
					transactionManager.rollback(transactionStatus);
				}
				
			} catch(DataAccessException e) {
				transactionManager.rollback(transactionStatus);
				logger.error("[AdminUserMngtSvcImpl.saveRePassword] MESSAGE : " + e.getMessage());
				res = "ERROR";
				throw new ESException("SYS014J");
			}
		} else {
			res = "WRONG_PWD";
			transactionManager.rollback(transactionStatus);
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		modelAndView.addObject("res", res);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView checkTime(){
				
		  Calendar calendar = Calendar.getInstance();
		  String limitTime = adminUserMngtDao.checktime();
		  SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		  String nowTime = dateFormat.format(calendar.getTime());
		  DataModelAndView modelAndView = new DataModelAndView();
		  modelAndView.addObject("nowTime", nowTime);
		  modelAndView.addObject("limitTime", limitTime);
		
		return modelAndView;
	}

	@Override
	public List<SystemMaster> getSystemMaster() {
		return adminUserMngtDao.getSystemMaster();
	}


	@Override
	public DataModelAndView adminUserSysList(Map<String, String> parameters) {
		DataModelAndView modelAndView = new DataModelAndView();
		String admin_user_id = parameters.get("admin_user_id");
		String auth_id = parameters.get("auth_id");
		
		List<SystemMaster> systemMaster = adminUserMngtDao.getAdminSysMaster(auth_id);
		
		modelAndView.addObject("systemMaster", systemMaster);
		
		AdminUser detailBean = new AdminUser();
		if(StringUtils.notNullCheck(admin_user_id)){
			detailBean = adminUserMngtDao.findAdminUserMngtOne(admin_user_id); 
		}else {
			Auth auth = new Auth();
			auth.setAuth_id(auth_id);
			auth = authMngtDao.findAuthMngtOne(auth);
			detailBean.setAuth_ids(auth.getSys_auth_ids());
		}
		
		modelAndView.addObject("adminUserDetail", detailBean);
		return modelAndView;
	}

	@Override
	public List<HashMap<String, Object>> emailChk(String id) {
		// TODO Auto-generated method stub
		return null;
	}


}
