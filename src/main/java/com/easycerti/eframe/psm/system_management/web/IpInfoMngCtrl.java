package com.easycerti.eframe.psm.system_management.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.system_management.dao.IpInfoMngDao;
import com.easycerti.eframe.psm.system_management.service.IpInfoMngSvc;
import com.easycerti.eframe.psm.system_management.vo.IpInfoBean;


@Controller
@RequestMapping(value="/ipInfo/*")
public class IpInfoMngCtrl {
	
	@Autowired
	private IpInfoMngSvc ipInfoSvc;
	
	@Autowired
	private IpInfoMngDao ipInfoMngDao;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;
	
	
	@RequestMapping(value="list.html",method={RequestMethod.POST})
	public DataModelAndView findIpMngtList(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		
		DataModelAndView modelAndView =  ipInfoSvc.ipInfoMngList(parameters);
		
		modelAndView.addObject("paramBean", parameters);
		
		modelAndView.setViewName("ipInfoMngList");
		
		return modelAndView;
	}
	
	@RequestMapping(value="userInfoAdd.html" ,method={RequestMethod.POST})
	@ResponseBody
	public int userInfoAdd(@RequestParam Map<String, String> parameters, 
			HttpServletRequest request) throws Exception {
		
		int result = 0; 
		
		String userId = parameters.get("user_Id");
		String loginIp = parameters.get("login_Ip");
		
		IpInfoBean infoBean = new IpInfoBean();
		
		infoBean.setUser_Id(userId.trim());
		infoBean.setLogin_Ip(loginIp.trim());
		
		int res = ipInfoMngDao.checkUserInfo(infoBean);
		
		if(res <= 0) {
			ipInfoSvc.addUserInfo(parameters);
			result = 1;
		}
		
		return result;
	}
	
	@RequestMapping(value="delete.html" ,method={RequestMethod.POST})
	public DataModelAndView deleteUserInfo(@RequestParam Map<String, String> parameters, 
			HttpServletRequest request) throws Exception {
		
		DataModelAndView modelAndView = ipInfoSvc.deleteUserInfo(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView; 
	}
	

}
