package com.easycerti.eframe.psm.system_management.service.impl;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.web.multipart.MultipartFile;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.report.dao.ReportDao;
import com.easycerti.eframe.psm.system_management.dao.CodeMngtDao;
import com.easycerti.eframe.psm.system_management.service.CustomerInfoSvc;
import com.easycerti.eframe.psm.system_management.vo.LogoImage;

@Service
public class CustomerInfoSvcImpl implements CustomerInfoSvc{
	
	@Autowired
	CommonDao commonDao;
	
	@Autowired
	ReportDao reportDao;

	@Autowired
	private CodeMngtDao codeMngtDao;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	
	@Override
	public DataModelAndView customerInfoList(Map<String, String> parameters) {
		DataModelAndView modelAndView = new DataModelAndView();
		//ChangeTheme paramBean = CommonHelper.convertMapToBean(parameters, ChangeTheme.class);
		
		SimpleCode simpleCode = new SimpleCode();
		String index = "/customerInfo/list.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		Code code = new Code();
		code.setGroup_code_id("REPORT_TYPE");
		code.setUse_flag("Y");
		List<Code> reportcode = codeMngtDao.findCodeMngtList(code);
		
		List<LogoImage> image_logo_list = commonDao.getLogoImageList();
		List<LogoImage> image_logo_list2 = commonDao.getLogoImageList2();
		Map<String,String> authorizeLine = reportDao.getAuthorizeLine();
		
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("reportCode", reportcode);
		modelAndView.addObject("image_logo_list", image_logo_list);
		modelAndView.addObject("image_logo_list2", image_logo_list2);
		modelAndView.addObject("authorizeLine", authorizeLine);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView addFile(MultipartFile file, HttpServletRequest request) {
		
		DataModelAndView modelAndView = new DataModelAndView();
		String rootPath = request.getSession().getServletContext().getRealPath("/") ;
		String attach_path = "resources/upload/";
	    String filename = UUID.randomUUID().toString().replaceAll("-", "") + file.getOriginalFilename();

		String savePath = rootPath + attach_path;
		//System.out.println(savePath);
		
		File fi = new File(savePath);
		if(!fi.exists())
			fi.mkdirs();
		
		fi = new File(savePath + filename);
	    try {
			file.transferTo(fi);
			commonDao.initImageLogo_use();
			
			HashMap map = new HashMap<>();
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			map.put("reg_date", sdf.format(new Date()));
			map.put("stored_name", filename);
			map.put("origin_name", file.getOriginalFilename());
			commonDao.insertImage_logo(map);
		} catch (IOException e) {
			// TODO Auto-generated catch block	
			e.printStackTrace();
		}
	    
		return modelAndView;
	}
	
	@Override
	public DataModelAndView addFile2(MultipartFile file, HttpServletRequest request) {
		
		DataModelAndView modelAndView = new DataModelAndView();
		String rootPath = request.getSession().getServletContext().getRealPath("/") ;
		String attach_path = "resources/upload/";
	    String filename = UUID.randomUUID().toString().replaceAll("-", "") + file.getOriginalFilename();

		String savePath = rootPath + attach_path;
		//System.out.println(savePath);
		

		File fi = new File(savePath);
		if(!fi.exists())
			fi.mkdirs();
		
		fi = new File(savePath + filename);
		
	    try {
			file.transferTo(fi);
			commonDao.initImageLogo_use2();
			 
			HashMap map = new HashMap<>();
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			map.put("reg_date", sdf.format(new Date()));
			map.put("stored_name", filename);
			map.put("origin_name", file.getOriginalFilename());
			commonDao.insertImage_logo2(map);
		} catch (IOException e) {
			// TODO Auto-generated catch block	
			e.printStackTrace();
		}
	    
		return modelAndView;
	}
	
	@Override
	public void useLogoImage(int idx, int type) {
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			if(type == 1) {
				commonDao.initImageLogo_use();
			}else if(type == 2) {
				commonDao.initImageLogo_use();
				commonDao.useLogImage(idx);
			}
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[CustomerInfoSvcImpl.useLogoImage] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
	}
	
	@Override
	public void useLogoImage2(int idx, int type) {
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			if(type == 1) {
				commonDao.initImageLogo_use2();
			}else if(type == 2) {
				commonDao.initImageLogo_use2();
				commonDao.useLogImage2(idx);
			}
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[CustomerInfoSvcImpl.useLogoImage] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
	}
	
	@Override
	public void deleteLogoImage(int idx) {

		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			commonDao.deleteLogoImage(idx);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[CustomerInfoSvcImpl.deleteLogoImage] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
	}

	@Override
	public String addAuthorizeLine(Map<String, String> parameters) {
		String result = "success";
		try {
			reportDao.addAuthorizeLine(parameters);
		} catch (Exception e) {
			e.printStackTrace();
			result = "fail";
		}
		return result;
	}
	
	@Override
	public String addDefaultDesc(Map<String, String> parameters) {
		String result = "success";
		try {
			reportDao.addDefaultDesc(parameters);
		} catch (Exception e) {
			e.printStackTrace();
			result = "fail";
		}
		return result;
	}

	@Override
	public String findDefaultDesc(Map<String, String> parameters) {
		String result = reportDao.findDefaultDesc(parameters);
		if(result == null) {
			result = "";
		}
		return result;
	}
}
