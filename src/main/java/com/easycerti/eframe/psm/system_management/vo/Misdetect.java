package com.easycerti.eframe.psm.system_management.vo;

import com.easycerti.eframe.common.vo.AbstractValueObject;

public class Misdetect extends AbstractValueObject  {
	
	private long cnt;
	private int result_type;
	
	private String result_title;
	
	private int misdetect_id;
	private String misdetect_pattern;
	private String privacy_type;
	private String use_flag;
	private String system_seq;
	private String privacy_desc;
	// 상세로그번호
	private String detailLogSeq;	
	// 상세날짜
	private String detailProcDate;
	private long biz_log_result_seq;
	
	private String privacy_seq;
	private String proc_date;
	private String result_content;
	private String use_yn;
	
	private String log_seq;
	
	private String download_log_result_seq;
	//제외유형
	private String misdetect_type;
	
	private String range_to;
	private String range_from;
	
	
	public String getRange_to() {
		return range_to;
	}
	public void setRange_to(String range_to) {
		this.range_to = range_to;
	}
	public String getRange_from() {
		return range_from;
	}
	public void setRange_from(String range_from) {
		this.range_from = range_from;
	}
	public String getMisdetect_type() {
		return misdetect_type;
	}
	public void setMisdetect_type(String misdetect_type) {
		this.misdetect_type = misdetect_type;
	}
	public String getSystem_seq() {
		return system_seq;
	}
	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}
	public String getLog_seq() {
		return log_seq;
	}
	public void setLog_seq(String log_seq) {
		this.log_seq = log_seq;
	}
	public String getResult_title() {
		return result_title;
	}
	public void setResult_title(String result_title) {
		this.result_title = result_title;
	}
	public String getPrivacy_seq() {
		return privacy_seq;
	}
	public void setPrivacy_seq(String privacy_seq) {
		this.privacy_seq = privacy_seq;
	}
	public String getProc_date() {
		return proc_date;
	}
	public void setProc_date(String proc_date) {
		this.proc_date = proc_date;
	}
	public String getUse_yn() {
		return use_yn;
	}
	public void setUse_yn(String use_yn) {
		this.use_yn = use_yn;
	}
	public String getDownload_log_result_seq() {
		return download_log_result_seq;
	}
	public void setDownload_log_result_seq(String download_log_result_seq) {
		this.download_log_result_seq = download_log_result_seq;
	}
	public int getResult_type() {
		return result_type;
	}
	public void setResult_type(int result_type) {
		this.result_type = result_type;
	}
	public long getCnt() {
		return cnt;
	}
	public void setCnt(long cnt) {
		this.cnt = cnt;
	}
	public long getBiz_log_result_seq() {
		return biz_log_result_seq;
	}
	public void setBiz_log_result_seq(long biz_log_result_seq) {
		this.biz_log_result_seq = biz_log_result_seq;
	}
	
	public String getDetailLogSeq() {
		return detailLogSeq;
	}
	public void setDetailLogSeq(String detailLogSeq) {
		this.detailLogSeq = detailLogSeq;
	}
	public String getDetailProcDate() {
		return detailProcDate;
	}
	public void setDetailProcDate(String detailProcDate) {
		this.detailProcDate = detailProcDate;
	}
	public String getResult_content() {
		return result_content;
	}
	public void setResult_content(String result_content) {
		this.result_content = result_content;
	}
	public String getPrivacy_desc() {
		return privacy_desc;
	}
	public void setPrivacy_desc(String privacy_desc) {
		this.privacy_desc = privacy_desc;
	}
	public int getMisdetect_id() {
		return misdetect_id;
	}
	public void setMisdetect_id(int misdetect_id) {
		this.misdetect_id = misdetect_id;
	}
	public String getMisdetect_pattern() {
		return misdetect_pattern;
	}
	public void setMisdetect_pattern(String misdetect_pattern) {
		this.misdetect_pattern = misdetect_pattern;
	}
	public String getPrivacy_type() {
		return privacy_type;
	}
	public void setPrivacy_type(String privacy_type) {
		this.privacy_type = privacy_type;
	}
	public String getUse_flag() {
		return use_flag;
	}
	public void setUse_flag(String use_flag) {
		this.use_flag = use_flag;
	}
	

}
