package com.easycerti.eframe.psm.system_management.service.impl;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.ExcelFileReader;
import com.easycerti.eframe.common.util.StringUtils;
import com.easycerti.eframe.common.util.XExcelFileReader;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.system_management.dao.AccessAuthDao;
import com.easycerti.eframe.psm.system_management.dao.AgentMngtDao;
import com.easycerti.eframe.psm.system_management.dao.CodeMngtDao;
import com.easycerti.eframe.psm.system_management.dao.DepartmentMngtDao;
import com.easycerti.eframe.psm.system_management.dao.EmpUserMngtDao;
import com.easycerti.eframe.psm.system_management.dao.SystemMngtDao;
import com.easycerti.eframe.psm.system_management.service.EmpUserMngtSvc;
import com.easycerti.eframe.psm.system_management.vo.AccessAuth;
import com.easycerti.eframe.psm.system_management.vo.Department;
import com.easycerti.eframe.psm.system_management.vo.EmpUser;
import com.easycerti.eframe.psm.system_management.vo.EmpUserSearch;
import com.easycerti.eframe.psm.system_management.vo.System;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;


/**
 * 사원 관리 Service Implements
 * 
 * @author yjyoo
 * @since 2015. 4. 21.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 21.           yjyoo            최초 생성
 *
 * </pre>
 */
@Service
public class EmpUserMngtSvcImpl implements EmpUserMngtSvc {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private EmpUserMngtDao empUserMngtDao;
	
	@Autowired
	private DepartmentMngtDao departmentMngtDao;
	
	@Autowired
	private CodeMngtDao codeDao;
	
	@Autowired
	private AgentMngtDao agentMngtDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private AccessAuthDao accessAuthDao;
	
	@Autowired
	private SystemMngtDao systemMngtDao;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;
	

	@Override
	public DataModelAndView findEmpUserMngtList(EmpUserSearch search) {
		int pageSize = Integer.valueOf(defaultPageSize);
		
		search.setSize(pageSize);
		search.setTotal_count(empUserMngtDao.findEmpUserMngtOne_count(search));
		
		List<EmpUser> empUserList = empUserMngtDao.findEmpUserMngtList(search);
		
		Department departmentBean = new Department();
		departmentBean.setUse_flag('Y');
		List<Department> deptList = departmentMngtDao.findDepartmentMngtList_choose(departmentBean);
		
		List<SystemMaster> systemMasterList = agentMngtDao.findSystemMasterListAll();
		
		SimpleCode simpleCode = new SimpleCode("/empUserMngt/list.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("empUserList", empUserList);
		modelAndView.addObject("deptList", deptList);
		modelAndView.addObject("systemMasterList", systemMasterList);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	@Override
	public void findEmpUserMngtList_download(DataModelAndView modelAndView, EmpUserSearch search, HttpServletRequest request) {
		String fileName = "PSM_사원관리_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "사원관리";
		
		search.setUseExcel("true");
		List<EmpUser> empUsers = empUserMngtDao.findEmpUserMngtList(search);
		
		String[] columns = new String[] {"dept_name", "emp_user_name", "emp_user_id", "system_name", "ip", "status_name"};
		String[] heads = new String[] {"소속", "사용자명", "사용자ID", "시스템명", "IP", "상태"};
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", empUsers);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}

	@Override
	public DataModelAndView findEmpUserMngtOne(EmpUser empUser) {
		DataModelAndView modelAndView = new DataModelAndView();
		
		if(StringUtils.notNullCheck(empUser.getEmp_user_id()) && StringUtils.notNullCheck(empUser.getSystem_seq())) {
			EmpUser detailBean = empUserMngtDao.findEmpUserMngtOne(empUser);
			modelAndView.addObject("empUserDetail", detailBean);
		}
		
		// 부서 리스트
		Department departmentBean = new Department();
		departmentBean.setUse_flag('Y');
		List<Department> deptList = departmentMngtDao.findDepartmentMngtList_choose(departmentBean);
		modelAndView.addObject("deptList", deptList);
		
		List<SystemMaster> systemList = agentMngtDao.findSystemMasterListAll();
		modelAndView.addObject("systemList", systemList);
		
		SimpleCode simpleCode = new SimpleCode("/empUserMngt/detail.html");
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView addEmpUserMngtOne(Map<String, String> parameters, HttpServletRequest request) throws Exception {
		EmpUser paramBean = CommonHelper.convertMapToBean(parameters, EmpUser.class);
		
		int empUserCount = empUserMngtDao.findEmpUserMngtOne_validation(paramBean);
		if(empUserCount > 0) {
			throw new ESException("SYS037J");
		}
		
		HttpSession session = request.getSession();
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			empUserMngtDao.addEmpUserMngtOne(paramBean);
			
			if(session.getAttribute("mode_access_auth").equals("Y")){
				AccessAuth accessAuth = CommonHelper.convertMapToBean(parameters, AccessAuth.class);
				
				/*if(accessAuthDao.findAccessAuth_validation(accessAuth) > 0)
					throw new ESException("SYS040J");*/
				
				SimpleDateFormat transFormat = new SimpleDateFormat("yyyyMMdd");
				String date = transFormat.format(new Date());
				accessAuth.setApprover(agentMngtDao.getApprover(paramBean.getSystem_seq()));
				accessAuth.setAccess_date(date);
				accessAuthDao.addAccessAuth(accessAuth);
			}
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[EmpUserMngtSvcImpl.addEmpUserMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS036J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView saveEmpUserMngtOne(Map<String, String> parameters, HttpServletRequest request) throws Exception {
		EmpUser paramBean = CommonHelper.convertMapToBean(parameters, EmpUser.class);
		
		int empUserCount = empUserMngtDao.findEmpUserMngtOne_validation(paramBean);
		if(empUserCount > 1) {
			throw new Exception("SYS037J");
		}
		
		HttpSession session = request.getSession();
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			empUserMngtDao.saveEmpUserMngtOne(paramBean);
			
			if(session.getAttribute("mode_access_auth").equals("Y")){
				AccessAuth accessAuth = CommonHelper.convertMapToBean(parameters, AccessAuth.class);
				AccessAuth accessAuthInfo = accessAuthDao.findAccessAuthInfo(accessAuth);
				
				if(accessAuthInfo!=null && accessAuthInfo.getManual_flag().equals("N")) { //수동관리여부가 N으로(자동관리) 되어있을때에만 데이터 추가 입력
					accessAuth.setAccess_date(accessAuthInfo.getAccess_date());
					accessAuth.setSystem_seq(parameters.get("temp_system_seq"));
					
					SimpleDateFormat transFormat = new SimpleDateFormat("yyyyMMdd");
					String date = transFormat.format(new Date());
					if(!parameters.get("temp_status").equals(paramBean.getStatus())) { // 상태가 변경되었을때
						if(paramBean.getStatus().equals("R")) { // 퇴직
							accessAuth.setExpire_date(date);
						}else {	
							accessAuth.setChange_date(date);
						}
						accessAuth.setProcess_content("상태 변경");
					}
					if(!parameters.get("temp_dept_id").equals(paramBean.getDept_id())) {
						accessAuth.setChange_date(date);
						accessAuth.setProcess_content("부서 변경");
					}
					if(!parameters.get("temp_system_seq").equals(paramBean.getSystem_seq())) {
						accessAuth.setChange_date(date);
						accessAuth.setProcess_content("시스템 변경");
					}
					accessAuth.setApprover(agentMngtDao.getApprover(paramBean.getSystem_seq()));
					accessAuth.setSystem_seq(paramBean.getSystem_seq());
					accessAuthDao.addAccessAuth(accessAuth);
				}
			}
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[EmpUserMngtSvcImpl.saveEmpUserMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS038J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView removeEmpUserMngtOne(Map<String, String> paramaters, HttpServletRequest request) {
		EmpUser paramBean = CommonHelper.convertMapToBean(paramaters, EmpUser.class);
	
		HttpSession session = request.getSession();
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			empUserMngtDao.removeEmpUserMngtOne(paramBean);
			
			if(session.getAttribute("mode_access_auth").equals("Y")){
				AccessAuth accessAuth = CommonHelper.convertMapToBean(paramaters, AccessAuth.class);
				//if(accessAuthDao.findAccessAuth_validation(accessAuth) > 0) {
				SimpleDateFormat transFormat = new SimpleDateFormat("yyyyMMdd");
				//SimpleDateFormat transFormat2 = new SimpleDateFormat("yyyyMMddHHmmss");
				String exdate = transFormat.format(new Date());
				//String update = transFormat2.format(new Date());
				accessAuth.setApprover(agentMngtDao.getApprover(paramBean.getSystem_seq()));
				accessAuth.setStatus("R");
				accessAuth.setExpire_date(exdate);
				//accessAuth.setUpdate_date(update);
				accessAuthDao.addAccessAuth(accessAuth);
				//}
			}
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[EmpUserMngtSvcImpl.removeEmpUserMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS039J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView login(Map<String, String> parameters, HttpServletRequest request) {
		EmpUser paramBean = CommonHelper.convertMapToBean(parameters, EmpUser.class);
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		EmpUser checkBean = new EmpUser();
		
		// ID 등록 여부 확인
		checkBean.setEmp_user_id(paramBean.getEmp_user_id());
		checkBean.setSystem_seq(paramBean.getSystem_seq());
		EmpUser empUserBean_id = empUserMngtDao.findEmpUserMngtOne(checkBean);
		
		// 등록된 ID 없음
		if(empUserBean_id == null){
			throw new ESException("SYS056J");
		}
		// 등록된 ID 있음
		else {
			CommonHelper.saveSessionUser(request, empUserBean_id);
			request.getSession().setAttribute("userType", "USER");
		}
		modelAndView.addObject("paramBean", paramBean);
		modelAndView.addObject("empUser", empUserBean_id);
		return modelAndView;
	}
	
	public String upLoadSystemMngtOne(Map<String, String> parameters, HttpServletRequest request, HttpServletResponse response)throws Exception {
		String result="false";
		 
		//EmpUser paramBean = CommonHelper.convertMapToBean(parameters, EmpUser.class);
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		//엑셀 Row 카운트는 1000으로 지정한다		
		int rowCount=5000;
		
		//파일 업로드 기능  		
		MultipartHttpServletRequest mpr = (MultipartHttpServletRequest) request;  
		
		MultipartFile dir = mpr.getFile("file");
				
		File file = new File("/root/excelUpload/" + dir.getOriginalFilename());
		
		if (!file.exists()) { //폴더 없으면 폴더 생성
			file.mkdirs();
		}
		
		dir.transferTo(file);
		
		//업로드된 파일의 정보를 읽어들여 DB에 추가하는 기능 
		if (dir.getSize() > 0 ) { 
				
			List<String[]> readRows = new ArrayList<>();
			
			String ext = file.getName().substring(file.getName().lastIndexOf(".")+1);
			ext = ext.toLowerCase();
			if( "xlsx".equals(ext) ){	
				readRows = new XExcelFileReader(file.getAbsolutePath()).readRows(rowCount);
				
			}else if( "xls".equals(ext) ){	
				readRows = new ExcelFileReader(file.getAbsolutePath()).readRows(rowCount);
			}
		
			for(int i=0; i<3; i++){	
				readRows.remove(0);
			}
			
			boolean isCheck = true;
			SimpleDateFormat transFormat = new SimpleDateFormat("yyyyMMdd");
			String date = transFormat.format(new Date());
			String[] status_type = {"W","R","L","V","E","D"};
			
			if( readRows.size() != 0 ){
				
				/*int columnCount = readRows.get(0).length;*/
				//각 셀 내용을 DB 컬럼과 Mapping
				try {
					for(String[] readRowArr : readRows){
						EmpUser paramBean = new EmpUser();
						if(readRowArr[0] != null && readRowArr[0].length() > 0){
							
							// 소속
							Department dm = departmentMngtDao.findDepartmentByname(readRowArr[0]);
							if(dm != null)
								paramBean.setDept_name(readRowArr[0]); 
							else {
								result = "NO_DEPT," + readRowArr[0];
								isCheck = false;
								break;
							}
							
							// 사용자명
							if (readRowArr[1] != null && readRowArr[1].length() > 0) {
								paramBean.setEmp_user_name(readRowArr[1]);
							} else {
								isCheck = false;
								break;
							}
							
							// 사용자ID
							if (readRowArr[2] != null && readRowArr[2].length() > 0) {
								paramBean.setEmp_user_id(readRowArr[2]);
							} else {
								isCheck = false;
								break;
							}
							
							
							// 시스템
							if(readRowArr[3] != null && readRowArr[3].length() > 0){
								System system = systemMngtDao.getSystemSeqBySystemName(readRowArr[3]);
								if(system != null)
									paramBean.setSystem_seq(system.getSystem_seq()); 
								else {
									result = "NO_SYSTEM," + readRowArr[3];
									isCheck = false;
									break;
								}
							} else {
								isCheck = false;
								break;
							}
							
							if (empUserMngtDao.findEmpUserMngtOne(paramBean) != null) {
								continue;
							}
							
							// 사용자IP
							if(readRowArr[4] != null && readRowArr[4].length() > 0){
								paramBean.setIp(readRowArr[4]);
							}
							
							// 상태
							if(readRowArr[5] != null && readRowArr[5].length() > 0){
								if ("재직".equals(readRowArr[5])) {
									paramBean.setStatus(status_type[0]);
								} else if ("퇴직".equals(readRowArr[5])) {
									paramBean.setStatus(status_type[1]);
								} else if ("휴직".equals(readRowArr[5])) {
									paramBean.setStatus(status_type[2]);
								} else if ("휴가".equals(readRowArr[5])) {
									paramBean.setStatus(status_type[2]);
								} else {
									paramBean.setStatus(status_type[0]);
								}
							} else {
								paramBean.setStatus(status_type[0]);
							}
							
							// EMAIL
							if(readRowArr.length >= 7 && readRowArr[6] != null && readRowArr[6].length() > 0){
								paramBean.setEmail_address(readRowArr[6]);
							}
							
							// 핸드폰번호
							if(readRowArr.length >= 8 &&readRowArr[7] != null && readRowArr[7].length() > 0){
								paramBean.setMobile_number(readRowArr[7]);
							}
							
							empUserMngtDao.addEmpUserMngtOne(paramBean);
							
							// 접근권한관리 insert
							if (request.getSession().getAttribute("mode_access_auth").equals("Y")) {
								AccessAuth accessAuth = new AccessAuth();
								accessAuth.setEmp_user_id(paramBean.getEmp_user_id());
								accessAuth.setEmp_user_name(paramBean.getEmp_user_name());
								accessAuth.setSystem_seq(paramBean.getSystem_seq());
								accessAuth.setStatus(paramBean.getStatus());
								accessAuth.setApprover(agentMngtDao.getApprover(paramBean.getSystem_seq()));
								accessAuth.setAccess_date(date);
								accessAuthDao.addAccessAuth(accessAuth);
							}
						}
					}
					if (isCheck) {
						transactionManager.commit(transactionStatus);
					} else {
						transactionManager.rollback(transactionStatus);
					}
					readRows.clear();
				} catch(DataAccessException e) {
					transactionManager.rollback(transactionStatus);
					e.printStackTrace();
					readRows.clear();
				/*	logger.error("[EmpUserMngtSvcImpl.upLoadEmpUserMngtOne] MESSAGE : " + e.getMessage());
					throw new ESException("SYS039J");*/
					return result;
				}
			}
			if(isCheck)
				result="true";
		}
		
		return result;	
	}
	
	public String upLoadSystemMngtOne_bak(Map<String, String> parameters, HttpServletRequest request, HttpServletResponse response)throws Exception {
		String result="false";
		 
		EmpUser paramBean = CommonHelper.convertMapToBean(parameters, EmpUser.class);
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		//엑셀 Row 카운트는 1000으로 지정한다		
		int rowCount=5000;
		
		//파일 업로드 기능  		
		MultipartHttpServletRequest mpr = (MultipartHttpServletRequest) request;  
		
		MultipartFile dir = mpr.getFile("file");
				
		File file = new File("/root/excelUpload/" + dir.getOriginalFilename());
		
		if (!file.exists()) { //폴더 없으면 폴더 생성
			file.mkdirs();
		}
		
		dir.transferTo(file);
		
		//업로드된 파일의 정보를 읽어들여 DB에 추가하는 기능 
		if (dir.getSize() > 0 ) { 
				
			List<String[]> readRows = new ArrayList<>();
			
			String ext = file.getName().substring(file.getName().lastIndexOf(".")+1);
			ext = ext.toLowerCase();
			if( "xlsx".equals(ext) ){	
				readRows = new XExcelFileReader(file.getAbsolutePath()).readRows(rowCount);
				
			}else if( "xls".equals(ext) ){	
				readRows = new ExcelFileReader(file.getAbsolutePath()).readRows(rowCount);
			}
		
			for(int i=0; i<3; i++){	
				readRows.remove(0);
			}
			
			boolean isCheck = true;
			SimpleDateFormat transFormat = new SimpleDateFormat("yyyyMMdd");
			String date = transFormat.format(new Date());
			String[] status_type = {"W","R","L","V","E","D"};
			
			if( readRows.size() != 0 ){
				
			/*int columnCount = readRows.get(0).length;*/
			//각 셀 내용을 DB 컬럼과 Mapping
			try {
				for(String[] readRowArr : readRows){
					if(readRowArr.length > 0){
						
						// 소속
						if(readRowArr[0] != null && readRowArr[0].length() > 0){
							Department dm = departmentMngtDao.findDepartmentByname(readRowArr[0]);
							if(dm != null)
								paramBean.setDept_name(readRowArr[0]); 
							else {
								isCheck = false;
								break;
							}
						}else {
							isCheck = false;
							break;
						}
							
						// 사용자명
						if (readRowArr[1] != null && readRowArr[1].length() > 0) {
							paramBean.setEmp_user_name(readRowArr[1]);
						} else {
							isCheck = false;
							break;
						}
						
						// 사용자ID
						if (readRowArr[2] != null && readRowArr[2].length() > 0) {
							paramBean.setEmp_user_id(readRowArr[2]);
						} else {
							isCheck = false;
							break;
						}
						
						// 시스템
						if(readRowArr[3] != null && readRowArr[3].length() > 0){
							System system = systemMngtDao.getSystemSeqBySystemName(readRowArr[3]);
							if(system != null)
								paramBean.setSystem_seq(system.getSystem_seq()); 
							else {
								isCheck = false;
								break;
							}
						}else {
							isCheck = false;
							break;
						}
						
						// 사용자IP
						if(readRowArr[4] != null && readRowArr[4].length() > 0){
							paramBean.setIp(readRowArr[4]);
						}
						
						// 상태
						if(readRowArr[5] != null && readRowArr[5].length() > 0){
							if ("재직".equals(readRowArr[5])) {
								paramBean.setStatus(status_type[0]);
							} else if ("퇴직".equals(readRowArr[5])) {
								paramBean.setStatus(status_type[1]);
							} else if ("휴직".equals(readRowArr[5])) {
								paramBean.setStatus(status_type[2]);
							} else if ("휴가".equals(readRowArr[5])) {
								paramBean.setStatus(status_type[2]);
							} else {
								paramBean.setStatus(status_type[0]);
							}
						} else {
							paramBean.setStatus(status_type[0]);
						}
						/*if("".equals(readRowArr[5])){
							paramBean.setStatus(status_type[0]);
						}else if("재직".equals(readRowArr[5])){
							paramBean.setStatus(status_type[1]);
						}else if("퇴직".equals(readRowArr[5])){
							paramBean.setStatus(status_type[2]);
						}else if("휴직".equals(readRowArr[5])){
							paramBean.setStatus(status_type[3]);
						}else if("휴가".equals(readRowArr[5])){
							paramBean.setStatus(status_type[4]);
						}else if("교육".equals(readRowArr[5])){
							paramBean.setStatus(status_type[5]);
						}else if("파견".equals(readRowArr[5])){
							paramBean.setStatus(status_type[6]);
						}
						}else {
							paramBean.setStatus(status_type[0]);
						}*/
						
						// EMAIL
						if(readRowArr[6] != null && readRowArr[6].length() > 0){
							paramBean.setEmail_address(readRowArr[6]);
						}
						
						// 핸드폰번호
						if(readRowArr[7] != null && readRowArr[7].length() > 0){
							paramBean.setMobile_number(readRowArr[7]);
						}
						empUserMngtDao.addEmpUserMngtOne(paramBean);
						
						if(request.getSession().getAttribute("mode_access_auth").equals("Y")){
							AccessAuth accessAuth = new AccessAuth();
							accessAuth.setEmp_user_id(paramBean.getEmp_user_id());
							accessAuth.setEmp_user_name(paramBean.getEmp_user_name());
							accessAuth.setSystem_seq(paramBean.getSystem_seq());
							accessAuth.setStatus(paramBean.getStatus());
							accessAuth.setApprover(agentMngtDao.getApprover(paramBean.getSystem_seq()));
							accessAuth.setAccess_date(date);
							accessAuthDao.addAccessAuth(accessAuth);
						}
					}
				}
				if(isCheck) {
					readRows.clear();
					transactionManager.commit(transactionStatus);
				}
			} catch(DataAccessException e) {
				transactionManager.rollback(transactionStatus);
				readRows.clear();
			/*	logger.error("[EmpUserMngtSvcImpl.upLoadEmpUserMngtOne] MESSAGE : " + e.getMessage());
				throw new ESException("SYS039J");*/
				return result;
			}
		}
			if(isCheck)
				result="true";
		}
		
		return result;	
	}
	
}
