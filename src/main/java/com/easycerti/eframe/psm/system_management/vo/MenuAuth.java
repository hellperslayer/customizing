package com.easycerti.eframe.psm.system_management.vo;

import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 메뉴권한 VO
 * 
 * @author yjyoo
 * @since 2015. 4. 20.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 20.           yjyoo            최초 생성
 *
 * </pre>
 */
public class MenuAuth extends AbstractValueObject {
	
	// 메뉴 ID
	private String menu_id = null;
	
	// 권한 ID
	private String auth_id = null;
	
	/**
	 * Etc
	 */
	
	// 관리자 ID
	private String admin_user_id = null;

	public String getMenu_id() {
		return menu_id;
	}

	public void setMenu_id(String menu_id) {
		this.menu_id = menu_id;
	}

	public String getAuth_id() {
		return auth_id;
	}

	public void setAuth_id(String auth_id) {
		this.auth_id = auth_id;
	}

	public String getAdmin_user_id() {
		return admin_user_id;
	}

	public void setAdmin_user_id(String admin_user_id) {
		this.admin_user_id = admin_user_id;
	}
}
