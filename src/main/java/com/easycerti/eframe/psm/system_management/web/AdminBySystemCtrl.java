package com.easycerti.eframe.psm.system_management.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.psm.system_management.service.AdminBySystemSvc;
import com.easycerti.eframe.psm.system_management.service.AdminUserMngtSvc;
import com.easycerti.eframe.psm.system_management.vo.AdminUserSearch;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

/**
 * 관리자 관련 Controller
 * 
 * @author yjyoo
 * @since 2015. 4. 20.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 20.           yjyoo           최초 생성
 *   2015. 5. 22.			yjyoo			엑셀 다운로드 추가
 *
 * </pre>
 */
@Controller
@RequestMapping(value="/adminBySystem/*")
public class AdminBySystemCtrl {

	@Autowired
	private AdminBySystemSvc adminBySystemSvc;
	
	@Autowired
	private AdminUserMngtSvc adminUserMngtSvc;
	
	@RequestMapping(value="list.html",method={RequestMethod.POST})
	public DataModelAndView findAdminBySystemList(@ModelAttribute("search") AdminUserSearch search, @RequestParam Map<String, String> parameters, HttpServletRequest request){
		DataModelAndView modelAndView = adminBySystemSvc.findAdminBySystemList(search, request);
		
		List<SystemMaster> sysList = adminUserMngtSvc.getSystemMaster();
		
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.addObject("sysList", sysList);
		modelAndView.setViewName("adminBySystemList");
		
		return modelAndView;
	}
	
	@RequestMapping(value="download.html",method={RequestMethod.POST})
	public DataModelAndView findAdminUserMngtList_download(@ModelAttribute("search") AdminUserSearch search, @RequestParam Map<String, String> parameters, HttpServletRequest request){
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);

		adminBySystemSvc.findAdminBySystemList_download(modelAndView, search, request);
		
		return modelAndView;
	}
	
	@RequestMapping(value="detail.html",method={RequestMethod.POST})
	public DataModelAndView findAdminBySystemDetail(@RequestParam String admin_user_id, 
			AdminUserSearch search, @RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		DataModelAndView modelAndView = adminBySystemSvc.findAdminBySystemDetail(admin_user_id, request);
		
		List<SystemMaster> systemMaster = adminUserMngtSvc.getSystemMaster();
		
		modelAndView.addObject("systemMaster", systemMaster);
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("search", search);
		modelAndView.setViewName("adminBySystemDetail");
		
		return modelAndView;
	}
	
	@RequestMapping(value="add.html",method={RequestMethod.POST})
	public DataModelAndView addAdminUserMngtOne(@RequestParam Map<String, String> parameters,HttpServletRequest request) throws Exception{
		String[] args = {"admin_user_id", "password", "admin_user_name"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		parameters.put("insert_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		DataModelAndView modelAndView = adminBySystemSvc.addAdminBySystem(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@RequestMapping(value="save.html",method={RequestMethod.POST})
	public DataModelAndView saveAdminUserMngtOne(@RequestParam Map<String, String> parameters,HttpServletRequest request) throws Exception{
		String[] args = {"admin_user_id", "admin_user_name"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){ //,"password"
			throw new ESException("SYS004J");
		}
		
		parameters.put("update_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		DataModelAndView modelAndView = adminBySystemSvc.saveAdminBySystem(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@RequestMapping(value="remove.html",method={RequestMethod.POST})
	public DataModelAndView removeAdminUserMngtOne(@RequestParam Map<String, String> parameters,HttpServletRequest request) throws Exception{
		String[] args = {"admin_user_id"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		parameters.put("update_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		DataModelAndView modelAndView = adminBySystemSvc.removeAdminBySystem(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
}
