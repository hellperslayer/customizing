package com.easycerti.eframe.psm.system_management.vo;

import com.easycerti.eframe.common.vo.SearchBase;

/**
 * 대상에이전트 검색조건 VO
 * 
 * @author yjyoo
 * @since 2015. 4. 24.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 24.           yjyoo            최초 생성
 *
 * </pre>
 */
public class AgentMasterSearch extends SearchBase {
	
	private String agent_type;

	public String getAgent_type() {
		return agent_type;
	}

	public void setAgent_type(String agent_type) {
		this.agent_type = agent_type;
	}
	
}
