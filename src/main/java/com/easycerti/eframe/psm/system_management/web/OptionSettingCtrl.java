package com.easycerti.eframe.psm.system_management.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.ModelAndView;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.core.util.SessionManager;
import com.easycerti.eframe.psm.system_management.service.OptionSettingSvc;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;
import com.lowagie.text.List;

/**
 * 옵션 설정 Controller
 */

@Controller
@RequestMapping(value="/optionSetting/*")
public class OptionSettingCtrl {
	
	@Autowired
	OptionSettingSvc optionSettingSvc;
	
	@MngtActHist(log_action = "SELECT", log_message = "[환경관리] 옵션 설정")
	@RequestMapping(value="list.html",method={RequestMethod.POST})
	public DataModelAndView findOptionSettingList(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		
		AdminUser adminUser = SystemHelper.getAdminUserInfo(request);
		String auth = adminUser.getAuth_id();		
		DataModelAndView modelAndView = optionSettingSvc.findOptionSettingList(parameters,auth);
		
		modelAndView.addObject("auth",auth );
		modelAndView.setViewName("optionSettingList");
		
		return modelAndView;
	}
	
	@MngtActHist(log_action = "UPDATE", log_message = "[환경관리] 옵션 수정")
	@RequestMapping(value="setOptionSetting.html",method={RequestMethod.POST})
	public DataModelAndView setOptionSetting(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		
		DataModelAndView modelAndView = optionSettingSvc.setOptionSetting(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	@RequestMapping(value="setMaster.html",method={RequestMethod.POST})
	public DataModelAndView setMaster(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
	
		DataModelAndView modelAndView = optionSettingSvc.setMaster(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	@RequestMapping(value="setSessionTime.html",method={RequestMethod.POST})
	public DataModelAndView setSessionTime(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
	
		String time = parameters.get("sessionTime");
		DataModelAndView modelAndView = optionSettingSvc.setSessionTime(time);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	@RequestMapping(value="setResultTypeView.html",method={RequestMethod.POST})
	public DataModelAndView setResultTypeView(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
	
		
		DataModelAndView modelAndView = optionSettingSvc.setResultTypeView(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	@RequestMapping(value="setSidebarView.html",method={RequestMethod.POST})
	public DataModelAndView setSidebarView(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
	
		
		DataModelAndView modelAndView = optionSettingSvc.setSidebarView(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	@RequestMapping(value="setScrnNameView.html",method={RequestMethod.POST})
	public DataModelAndView setScrnNameView(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
	
		
		DataModelAndView modelAndView = optionSettingSvc.setScrnNameView(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	@RequestMapping(value="setMappingId.html",method={RequestMethod.POST})
	public DataModelAndView setMappingId(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
	
		
		DataModelAndView modelAndView = optionSettingSvc.setMappingId(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	@RequestMapping(value="setUse_systemSeq.html",method={RequestMethod.POST})
	public DataModelAndView setUse_systemSeq(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
	
		
		DataModelAndView modelAndView = optionSettingSvc.setUse_systemSeq(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	@RequestMapping(value="setUi_type.html",method={RequestMethod.POST})
	public DataModelAndView setUi_type(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
	
		
		DataModelAndView modelAndView = optionSettingSvc.setUi_type(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
}
