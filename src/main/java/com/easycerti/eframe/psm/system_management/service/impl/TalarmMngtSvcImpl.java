package com.easycerti.eframe.psm.system_management.service.impl;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.ExcelFileReader;
import com.easycerti.eframe.common.util.XExcelFileReader;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.system_management.dao.TalarmMngtDao;
import com.easycerti.eframe.psm.system_management.service.TalarmMngtSvc;
import com.easycerti.eframe.psm.system_management.vo.Talarm;
import com.easycerti.eframe.psm.system_management.vo.TalarmSearch;


/**
 * 사원 관리 Service Implements
 * 
 * @author yjyoo
 * @since 2015. 4. 21.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 21.           yjyoo            최초 생성
 *
 * </pre>
 */
@Service
public class TalarmMngtSvcImpl implements TalarmMngtSvc {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private TalarmMngtDao talarmMngtDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;
	

	@Override
	public DataModelAndView findTalarmMngtList(TalarmSearch search) {
		int pageSize = Integer.valueOf(defaultPageSize);
		
		search.setSize(pageSize);
		search.setTotal_count(talarmMngtDao.findTalarmMngtOne_count(search));
		
		List<Talarm> talarmList = talarmMngtDao.findTalarmMngtList(search);
		
		SimpleCode simpleCode = new SimpleCode("/talarmMngt/list.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("talarmList", talarmList);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	@Override
	public void findTalarmMngtList_download(DataModelAndView modelAndView, TalarmSearch search, HttpServletRequest request) {
		String fileName = "PSM_알람관리_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "알람관리";
		
		search.setUseExcel("true");
		List<Talarm> talarms = talarmMngtDao.findTalarmMngtList(search);
		
		String[] columns = new String[] {"emp_user_name", "email_address", "mobile_number", "useyn_email", "useyn_htel"};
		String[] heads = new String[] {"사용자명", "이메일주소", "연락처", "이메일수신여부", "전화수신여부"};
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", talarms);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
	}

	@Override
	public DataModelAndView findTalarmMngtOne(Talarm talarm) {
		DataModelAndView modelAndView = new DataModelAndView();
		
		Talarm detailBean = talarmMngtDao.findTalarmMngtOne(talarm);
		modelAndView.addObject("talarmDetail", detailBean);

		SimpleCode simpleCode = new SimpleCode("/talarmMngt/detail.html");
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView addTalarmMngtOne(Map<String, String> parameters) throws Exception {
		Talarm paramBean = CommonHelper.convertMapToBean(parameters, Talarm.class);

		int talarmCount = talarmMngtDao.findTalarmMngtOne_validation(paramBean);
		if(talarmCount > 0) {
			throw new ESException("SYS037J");
		}
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			talarmMngtDao.addTalarmMngtOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[TalarmMngtSvcImpl.addTalarmMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS036J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView saveTalarmMngtOne(Map<String, String> parameters) throws Exception {
		Talarm paramBean = CommonHelper.convertMapToBean(parameters, Talarm.class);
		
		int talarmCount = talarmMngtDao.findTalarmMngtOne_validation(paramBean);
		if(talarmCount > 1) {
			throw new Exception("SYS037J");
		}
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			talarmMngtDao.saveTalarmMngtOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[TalarmMngtSvcImpl.saveTalarmMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS038J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView saveTalarmMngtTwo(Map<String, String> parameters) throws Exception {
		Talarm paramBean = CommonHelper.convertMapToBean(parameters, Talarm.class);
		
		int talarmCount = talarmMngtDao.findTalarmMngtOne_validation(paramBean);
		if(talarmCount > 1) {
			throw new Exception("SYS037J");
		}
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			talarmMngtDao.saveTalarmMngtTwo(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[TalarmMngtSvcImpl.saveTalarmMngtTwo] MESSAGE : " + e.getMessage());
			throw new ESException("SYS038J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView removeTalarmMngtOne(Map<String, String> paramaters) {
		Talarm paramBean = CommonHelper.convertMapToBean(paramaters, Talarm.class);
	
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			talarmMngtDao.removeTalarmMngtOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[TalarmMngtSvcImpl.removeTalarmMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS039J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView login(Map<String, String> parameters, HttpServletRequest request) {
		Talarm paramBean = CommonHelper.convertMapToBean(parameters, Talarm.class);
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		Talarm checkBean = new Talarm();
		
		// ID 등록 여부 확인
		checkBean.setEmp_user_id(paramBean.getEmp_user_id());
		Talarm talarmBean_id = talarmMngtDao.findTalarmMngtOne(checkBean);
		
		// 등록된 ID 없음
		if(talarmBean_id == null){
			throw new ESException("SYS056J");
		}
		// 등록된 ID 있음
		else {
			CommonHelper.saveSessionUser(request, talarmBean_id);
			request.getSession().setAttribute("userType", "USER");
		}
		modelAndView.addObject("paramBean", paramBean);
		modelAndView.addObject("talarm", talarmBean_id);
		return modelAndView;
	}
	
	
	public String upLoadSystemMngtOne(Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response)throws Exception {
		String result="false";
		Talarm paramBean = CommonHelper.convertMapToBean(parameters, Talarm.class);
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		//엑셀 Row 카운트는 1000으로 지정한다		
		int rowCount=1000;
		
		//파일 업로드 기능  		
		MultipartHttpServletRequest mpr = (MultipartHttpServletRequest) request;  
		
		MultipartFile dir = mpr.getFile("file");
				
		File file = new File("/root/excelUpload/" + dir.getOriginalFilename());
		
		if (!file.exists()) { //폴더 없으면 폴더 생성
			file.mkdirs();
		}
		
		dir.transferTo(file);
		
		//업로드된 파일의 정보를 읽어들여 DB에 추가하는 기능 
		if (dir.getSize() > 0 ) { 
				
			List<String[]> readRows = new ArrayList<>();
			
			String ext = file.getName().substring(file.getName().lastIndexOf(".")+1);
			ext = ext.toLowerCase();
			if( "xlsx".equals(ext) ){	
				readRows = new XExcelFileReader(file.getAbsolutePath()).readRows(rowCount);
				
			}else if( "xls".equals(ext) ){	
				readRows = new ExcelFileReader(file.getAbsolutePath()).readRows(rowCount);
			}
		
			for(int i=0; i<3; i++){	
				readRows.remove(0);
			}
			
			boolean isCheck = true;
			if( readRows.size() != 0 ){
				
			/*int columnCount = readRows.get(0).length;*/
			//각 셀 내용을 DB 컬럼과 Mapping
			try {
				for(String[] readRowArr : readRows){
					
					if(readRowArr.length != 0 && readRowArr[1].length() !=0){
						
						paramBean.setEmp_user_id(readRowArr[1]);
						paramBean.setEmp_user_name(readRowArr[2]);
						paramBean.setEmail_address(readRowArr[3]);
						paramBean.setMobile_number(readRowArr[4]);
						paramBean.setUseyn_email(readRowArr[5]);
						paramBean.setUseyn_htel(readRowArr[6]);
						talarmMngtDao.addTalarmMngtOne(paramBean);

					}
			}
				if(isCheck) {
					readRows.clear();
					transactionManager.commit(transactionStatus);
				}
			} catch(DataAccessException e) {
				transactionManager.rollback(transactionStatus);
				readRows.clear();
			/*	logger.error("[TalarmMngtSvcImpl.upLoadTalarmMngtOne] MESSAGE : " + e.getMessage());
				throw new ESException("SYS039J");*/
				return result;
			}
		}
			if(isCheck)
				result="true";
		}
		
		return result;	
	}
	
}
