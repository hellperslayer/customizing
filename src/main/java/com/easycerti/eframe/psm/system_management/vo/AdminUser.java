package com.easycerti.eframe.psm.system_management.vo;

import java.util.Date;

import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 관리자 VO
 * 
 * @author yjyoo
 * @since 2015. 4. 20.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 20.           yjyoo            최초 생성
 *
 * </pre>
 */
public class AdminUser extends AbstractValueObject{
	
	// 관리자 ID
	private String admin_user_id = null;
	
	// 관리자 명
	private String admin_user_name = null;

	// 권한 ID
	private String auth_id = null;
	
	// 부서 ID
	private String dept_id = null;
	
	// 비밀번호
	private String password = null;
	
	// 비밀번호 확인값
	private Integer pwd_check = null;
	
	// 마지막 비밀번호 변경 일자
	private Date last_pwd_change_datetime = null;
	
	// 마지막 로그인 성공 일자
	private Date last_login_success_datetime = null;
	
	// 마지막 로그인 실패 일자
	private Date last_login_fail_datetime = null;
	
	// 등급
	private Integer grade = null;
	
	// 이메일
	private String email_address = null;
	
	// 핸드폰 번호
	private String mobile_number = null;
	
	// 삭제여부
	private String del_flag = null;
	
	// 알람여부
	private String alarm_flag = null;

	// 관리자 설명
	private String description = null;
	
	// 등록자
	private String insert_user_id = null;
	
	// 등록일자
	private Date insert_datetime = null;
	
	// 수정자
	private String update_user_id = null;
	
	// 수정일자
	private Date update_datetime = null;
	
	// 사이트권한
	private String auth_ids = null;
	/**
	 *  etc
	 */
	
	// 부서명
	private String dept_name = null;
	
	// 권한명
	private String auth_name = null;
	
	// 로그인 가능 횟수
	private Long cnt = null;
	
	// 하위 부서 검색
	private String deptLowSearchFlag;
	
	// 비밀번호 변경 
	private String newPassword;
	private String currentPasswd;
	
	//사용기한
	private String use_by_date = "99999999";
	
	
	
	//biz_log_summon join table()
	
	//요청대상자 id
	private String emp_cd ;
	
	//biz_log 번호
	private long log_seq ;
	private String logSeqs;
	
	// ks ??
	private String log_agency_cd ;
	
	//요청일자
	private String summon_dt ;
	
	//마감일자? + 10일
	private String expect_dt ;
	
	//요청자
	private String summon_req_emp ;
	private String desc_dt ;
	
	//요청시분초 (to_char(now(), 'hh24miss'))
	private long desc_seq ;
	
	//소명상태?? 초기 y = 소명요청 
	private String summon_yn ;
	
	//소명상태?? 초기 '2' = 소명요청
	private String desc_status ;

	//소명상태?? 초기 '0' = 소명요청
	private String desc_result ;
	private String hc_rule_nm ;

	//소명상태?? 초기 '1' = 소명요청
	private String summon_seq ;
	private String hc_send_dt ;
	private String decision_dt ;
	private String ssn_name ;	
	private String posit_gu ; 	
	private String acc_org_nm ;  	
	private String result_type ; 
	private String detailProcDate;
	private String detailEmpCd;
	//로그일자
	private String proc_date ;
	
	//요청대상자 id
	private String emp_user_id ;
	private String emp_user_name;
	private String detailLogSeq ;
	
	
	//desc_violation_tmon_psm table
	
	//시퀀스
	private int desc_violation_seq ;
	private String occr_dt ;
	private String agency_cd ;
	private String detect_div ;
	private String seq ;
//	private String desc_seq ;
	private String log_gubun ;
	private int rule_cd ;
//	private int summon_seq ;
	private String request ;
//	private String summon_dt ;
//	private String emp_cd ;
	
	private String deci_nm;
	private String desc_decision_seq;
	private String decision;
	
	//소명요청사유
	private String description1 ;
	
	private String description2 ;
	
	private String detailEmpDetailSeq;
	
	private String system_name;
	private String system_seq;
	private String is_check;
	private long emp_detail_seq;
	private String summon_flag;
	private String user_lock;
	
	private String encrypt_type;
	
	private String isInitLogin;
	private String salt_value;
	
	
	
	private String log_type;
	private String h_body;
	private String h_type;
	private String result_user_id;
	private String request_user_id;
	private String request_target_user_id;
	private String result_dt;
	private String org_cd;
	private String org_nm;
	private String desc_access_cd;
	private long extract_result_seq;
	
	private String result2_dt;
	private String result2_body;
	private String result2_user_id;
	private String desc_result2;
	private String checkReResult;
	
	private int appr_seq;
	private String appr_body;
	private String appr_status;
	private String appr_type;
	private String check_yn;
	private String sso_yn;
	private String param_value;
	private String param_name;

	private String abuse_purpose;
	private String abuse_type;
	private String fault_degree;
	
	private String abuse_content;
	private String administrative_dispo_day;
	private String administrative_dispo_type;
	private String criminal_dispo_day;
	private String criminal_dispo_type;
	private String etc_dispo_day;
	private String etc_dispo_type;
	private String rule_nm;
	private String request_user_name;
	private String response_user_name;
	private String result_user_name;
	private String extract_dt ;
	private String resummon_yn ;
	private String collect_dt;
	
	private String appr_seq_list;
	private String appr_emp_list;
	
	//보고서 생성 권한
	private String make_report_auth;

	//다수유저 동시 수정을 위한 배열
	private String[] admin_user_id_arr;
	
	private String detail_emp_user_id;
	private String detail_system_seq;
	
	private String res_user_id;			//소명응답자 ID
	private String res_system_seq;		//소명응답자 시스템코드
	private String summon_id;			//소명대리인 ID
	private String summon_system_seq;	//소명대리인 시스템코드
	private String msg;					//summon_history 메시지
	private String status;				//summon_history 진행상태 값
	private String summon_status;		//소명진행상태
	private String decision_status;
	private String decision_status_second;
	private String target_id;			//
	private String scen_name;
	private int limit_cnt;
	private int rule_cnt;
	private int dng_val;
	private String log_delimiter;
	private String rule_view_type;
	private int external_staff;		//내부,외부직 구분
	private String summon_reponse_type;
	private String email_check_val; //소명알람 수신여부('Y' : 미수신)
	
	
	public String getEmail_check_val() {
		return email_check_val;
	}

	public void setEmail_check_val(String email_check_val) {
		this.email_check_val = email_check_val;
	}

	public String getSummon_reponse_type() {
		return summon_reponse_type;
	}

	public void setSummon_reponse_type(String summon_reponse_type) {
		this.summon_reponse_type = summon_reponse_type;
	}

	public int getExternal_staff() {
		return external_staff;
	}

	public void setExternal_staff(int external_staff) {
		this.external_staff = external_staff;
	}

	public int getRule_cnt() {
		return rule_cnt;
	}

	public void setRule_cnt(int rule_cnt) {
		this.rule_cnt = rule_cnt;
	}

	public int getDng_val() {
		return dng_val;
	}

	public void setDng_val(int dng_val) {
		this.dng_val = dng_val;
	}

	public String getRule_view_type() {
		return rule_view_type;
	}

	public void setRule_view_type(String rule_view_type) {
		this.rule_view_type = rule_view_type;
	}

	public String getLog_delimiter() {
		return log_delimiter;
	}

	public void setLog_delimiter(String log_delimiter) {
		this.log_delimiter = log_delimiter;
	}

	public int getLimit_cnt() {
		return limit_cnt;
	}

	public void setLimit_cnt(int limit_cnt) {
		this.limit_cnt = limit_cnt;
	}

	public String getEmp_user_name() {
		return emp_user_name;
	}

	public void setEmp_user_name(String emp_user_name) {
		this.emp_user_name = emp_user_name;
	}

	public String getScen_name() {
		return scen_name;
	}

	public void setScen_name(String scen_name) {
		this.scen_name = scen_name;
	}

	public String getDecision_status() {
		return decision_status;
	}

	public void setDecision_status(String decision_status) {
		this.decision_status = decision_status;
	}

	public String getDetail_emp_user_id() {
		return detail_emp_user_id;
	}

	public void setDetail_emp_user_id(String detail_emp_user_id) {
		this.detail_emp_user_id = detail_emp_user_id;
	}

	public String getDetail_system_seq() {
		return detail_system_seq;
	}

	public void setDetail_system_seq(String detail_system_seq) {
		this.detail_system_seq = detail_system_seq;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTarget_id() {
		return target_id;
	}

	public void setTarget_id(String target_id) {
		this.target_id = target_id;
	}

	public String getSummon_status() {
		return summon_status;
	}

	public void setSummon_status(String summon_status) {
		this.summon_status = summon_status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getSummon_id() {
		return summon_id;
	}

	public void setSummon_id(String summon_id) {
		this.summon_id = summon_id;
	}

	public String getSummon_system_seq() {
		return summon_system_seq;
	}

	public void setSummon_system_seq(String summon_system_seq) {
		this.summon_system_seq = summon_system_seq;
	}

	public String getRes_user_id() {
		return res_user_id;
	}

	public void setRes_user_id(String res_user_id) {
		this.res_user_id = res_user_id;
	}

	public String getRes_system_seq() {
		return res_system_seq;
	}

	public void setRes_system_seq(String res_system_seq) {
		this.res_system_seq = res_system_seq;
	}

	public String[] getAdmin_user_id_arr() {
		return admin_user_id_arr;
	}

	public void setAdmin_user_id_arr(String[] admin_user_id_arr) {
		this.admin_user_id_arr = admin_user_id_arr;
	}

	public String getMake_report_auth() {
		return make_report_auth;
	}

	public void setMake_report_auth(String make_report_auth) {
		this.make_report_auth = make_report_auth;
	}

	public String getAppr_seq_list() {
		return appr_seq_list;
	}

	public void setAppr_seq_list(String appr_seq_list) {
		this.appr_seq_list = appr_seq_list;
	}

	public String getAppr_emp_list() {
		return appr_emp_list;
	}

	public void setAppr_emp_list(String appr_emp_list) {
		this.appr_emp_list = appr_emp_list;
	}

	public String getCollect_dt() {
		return collect_dt;
	}

	public void setCollect_dt(String collect_dt) {
		this.collect_dt = collect_dt;
	}

	public String getResummon_yn() {
		return resummon_yn;
	}

	public void setResummon_yn(String resummon_yn) {
		this.resummon_yn = resummon_yn;
	}

	public String getExtract_dt() {
		return extract_dt;
	}

	public void setExtract_dt(String extract_dt) {
		this.extract_dt = extract_dt;
	}

	public String getRule_nm() {
		return rule_nm;
	}

	public void setRule_nm(String rule_nm) {
		this.rule_nm = rule_nm;
	}

	public String getRequest_user_name() {
		return request_user_name;
	}

	public void setRequest_user_name(String request_user_name) {
		this.request_user_name = request_user_name;
	}

	public String getResponse_user_name() {
		return response_user_name;
	}

	public void setResponse_user_name(String response_user_name) {
		this.response_user_name = response_user_name;
	}

	public String getResult_user_name() {
		return result_user_name;
	}

	public void setResult_user_name(String result_user_name) {
		this.result_user_name = result_user_name;
	}

	public String getAbuse_content() {
		return abuse_content;
	}

	public void setAbuse_content(String abuse_content) {
		this.abuse_content = abuse_content;
	}

	public String getAdministrative_dispo_day() {
		return administrative_dispo_day;
	}

	public void setAdministrative_dispo_day(String administrative_dispo_day) {
		this.administrative_dispo_day = administrative_dispo_day;
	}

	public String getAdministrative_dispo_type() {
		return administrative_dispo_type;
	}

	public void setAdministrative_dispo_type(String administrative_dispo_type) {
		this.administrative_dispo_type = administrative_dispo_type;
	}

	public String getCriminal_dispo_day() {
		return criminal_dispo_day;
	}

	public void setCriminal_dispo_day(String criminal_dispo_day) {
		this.criminal_dispo_day = criminal_dispo_day;
	}

	public String getCriminal_dispo_type() {
		return criminal_dispo_type;
	}

	public void setCriminal_dispo_type(String criminal_dispo_type) {
		this.criminal_dispo_type = criminal_dispo_type;
	}

	public String getEtc_dispo_day() {
		return etc_dispo_day;
	}

	public void setEtc_dispo_day(String etc_dispo_day) {
		this.etc_dispo_day = etc_dispo_day;
	}

	public String getEtc_dispo_type() {
		return etc_dispo_type;
	}

	public void setEtc_dispo_type(String etc_dispo_type) {
		this.etc_dispo_type = etc_dispo_type;
	}

	public String getAbuse_purpose() {
		return abuse_purpose;
	}

	public void setAbuse_purpose(String abuse_purpose) {
		this.abuse_purpose = abuse_purpose;
	}

	public String getAbuse_type() {
		return abuse_type;
	}

	public void setAbuse_type(String abuse_type) {
		this.abuse_type = abuse_type;
	}

	public String getFault_degree() {
		return fault_degree;
	}

	public void setFault_degree(String fault_degree) {
		this.fault_degree = fault_degree;
	}

	public String getParam_value() {
		return param_value;
	}

	public void setParam_value(String param_value) {
		this.param_value = param_value;
	}

	public String getParam_name() {
		return param_name;
	}

	public void setParam_name(String param_name) {
		this.param_name = param_name;
	}

	public String getSso_yn() {
		return sso_yn;
	}

	public void setSso_yn(String sso_yn) {
		this.sso_yn = sso_yn;
	}

	public String getRequest_user_id() {
		return request_user_id;
	}

	public void setRequest_user_id(String request_user_id) {
		this.request_user_id = request_user_id;
	}

	public String getRequest_target_user_id() {
		return request_target_user_id;
	}

	public void setRequest_target_user_id(String request_target_user_id) {
		this.request_target_user_id = request_target_user_id;
	}

	public int getAppr_seq() {
		return appr_seq;
	}

	public void setAppr_seq(int appr_seq) {
		this.appr_seq = appr_seq;
	}

	public String getAppr_body() {
		return appr_body;
	}

	public void setAppr_body(String appr_body) {
		this.appr_body = appr_body;
	}

	public String getAppr_status() {
		return appr_status;
	}

	public void setAppr_status(String appr_status) {
		this.appr_status = appr_status;
	}

	public String getAppr_type() {
		return appr_type;
	}

	public void setAppr_type(String appr_type) {
		this.appr_type = appr_type;
	}

	public String getCheck_yn() {
		return check_yn;
	}

	public void setCheck_yn(String check_yn) {
		this.check_yn = check_yn;
	}

	public String getCheckReResult() {
		return checkReResult;
	}

	public void setCheckReResult(String checkReResult) {
		this.checkReResult = checkReResult;
	}

	public String getResult2_dt() {
		return result2_dt;
	}

	public void setResult2_dt(String result2_dt) {
		this.result2_dt = result2_dt;
	}

	public String getResult2_body() {
		return result2_body;
	}

	public void setResult2_body(String result2_body) {
		this.result2_body = result2_body;
	}

	public String getResult2_user_id() {
		return result2_user_id;
	}

	public void setResult2_user_id(String result2_user_id) {
		this.result2_user_id = result2_user_id;
	}
	
	public String getDesc_result2() {
		return desc_result2;
	}

	public void setDesc_result2(String desc_result2) {
		this.desc_result2 = desc_result2;
	}

	public long getExtract_result_seq() {
		return extract_result_seq;
	}

	public void setExtract_result_seq(long extract_result_seq) {
		this.extract_result_seq = extract_result_seq;
	}

	public String getDetailEmpCd() {
		return detailEmpCd;
	}

	public void setDetailEmpCd(String detailEmpCd) {
		this.detailEmpCd = detailEmpCd;
	}

	public String getOrg_cd() {
		return org_cd;
	}

	public void setOrg_cd(String org_cd) {
		this.org_cd = org_cd;
	}

	public String getOrg_nm() {
		return org_nm;
	}

	public void setOrg_nm(String org_nm) {
		this.org_nm = org_nm;
	}

	public String getDesc_access_cd() {
		return desc_access_cd;
	}

	public void setDesc_access_cd(String desc_access_cd) {
		this.desc_access_cd = desc_access_cd;
	}

	public String getDetailProcDate() {
		return detailProcDate;
	}

	public void setDetailProcDate(String detailProcDate) {
		this.detailProcDate = detailProcDate;
	}

	public String getResult_dt() {
		return result_dt;
	}

	public void setResult_dt(String result_dt) {
		this.result_dt = result_dt;
	}

	public String getResult_user_id() {
		return result_user_id;
	}

	public void setResult_user_id(String result_user_id) {
		this.result_user_id = result_user_id;
	}

	public String getH_body() {
		return h_body;
	}

	public void setH_body(String h_body) {
		this.h_body = h_body;
	}

	public String getH_type() {
		return h_type;
	}

	public void setH_type(String h_type) {
		this.h_type = h_type;
	}

	public String getLog_type() {
		return log_type;
	}

	public void setLog_type(String log_type) {
		this.log_type = log_type;
	}

	public String getSalt_value() {
		return salt_value;
	}

	public void setSalt_value(String salt_value) {
		this.salt_value = salt_value;
	}

	public String getIsInitLogin() {
		return isInitLogin;
	}

	public void setIsInitLogin(String isInitLogin) {
		this.isInitLogin = isInitLogin;
	}

	public String getUser_lock() {
		return user_lock;
	}

	public void setUser_lock(String user_lock) {
		this.user_lock = user_lock;
	}

	public String getSummon_flag() {
		return summon_flag;
	}

	public void setSummon_flag(String summon_flag) {
		this.summon_flag = summon_flag;
	}
	public String getIs_check() {
		return is_check;
	}

	public void setIs_check(String is_check) {
		this.is_check = is_check;
	}

	public long getEmp_detail_seq() {
		return emp_detail_seq;
	}

	public void setEmp_detail_seq(long emp_detail_seq) {
		this.emp_detail_seq = emp_detail_seq;
	}

	public String getDescription1() {
		return description1;
	}

	public void setDescription1(String description1) {
		this.description1 = description1;
	}
	
	public String getDescription2() {
		return description2;
	}

	public void setDescription2(String description2) {
		this.description2 = description2;
	}

	public String getDetailLogSeq() {
		return detailLogSeq;
	}

	public int getDesc_violation_seq() {
		return desc_violation_seq;
	}

	public void setDesc_violation_seq(int desc_violation_seq) {
		this.desc_violation_seq = desc_violation_seq;
	}

	public String getOccr_dt() {
		return occr_dt;
	}

	public void setOccr_dt(String occr_dt) {
		this.occr_dt = occr_dt;
	}

	public String getAgency_cd() {
		return agency_cd;
	}

	public void setAgency_cd(String agency_cd) {
		this.agency_cd = agency_cd;
	}

	public String getDetect_div() {
		return detect_div;
	}

	public void setDetect_div(String detect_div) {
		this.detect_div = detect_div;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getLog_gubun() {
		return log_gubun;
	}

	public void setLog_gubun(String log_gubun) {
		this.log_gubun = log_gubun;
	}

	public int getRule_cd() {
		return rule_cd;
	}

	public void setRule_cd(int rule_cd) {
		this.rule_cd = rule_cd;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public void setDetailLogSeq(String detailLogSeq) {
		this.detailLogSeq = detailLogSeq;
	}

	public String getEmp_user_id() {
		return emp_user_id;
	}

	public void setEmp_user_id(String emp_user_id) {
		this.emp_user_id = emp_user_id;
	}

	public String getEmp_cd() {
		return emp_cd;
	}

	public void setEmp_cd(String emp_cd) {
		this.emp_cd = emp_cd;
	}

	public long getLog_seq() {
		return log_seq;
	}

	public void setLog_seq(long log_seq) {
		this.log_seq = log_seq;
	}
	
	public String getLogSeqs() {
		return logSeqs;
	}

	public void setLogSeqs(String logSeqs) {
		this.logSeqs = logSeqs;
	}

	public String getLog_agency_cd() {
		return log_agency_cd;
	}

	public void setLog_agency_cd(String log_agency_cd) {
		this.log_agency_cd = log_agency_cd;
	}

	public String getSummon_dt() {
		return summon_dt;
	}

	public void setSummon_dt(String summon_dt) {
		this.summon_dt = summon_dt;
	}

	public String getExpect_dt() {
		return expect_dt;
	}

	public void setExpect_dt(String expect_dt) {
		this.expect_dt = expect_dt;
	}

	public String getSummon_req_emp() {
		return summon_req_emp;
	}

	public void setSummon_req_emp(String summon_req_emp) {
		this.summon_req_emp = summon_req_emp;
	}

	public String getDesc_dt() {
		return desc_dt;
	}

	public void setDesc_dt(String desc_dt) {
		this.desc_dt = desc_dt;
	}

	public long getDesc_seq() {
		return desc_seq;
	}

	public void setDesc_seq(long desc_seq) {
		this.desc_seq = desc_seq;
	}

	public String getSummon_yn() {
		return summon_yn;
	}

	public void setSummon_yn(String summon_yn) {
		this.summon_yn = summon_yn;
	}

	public String getDesc_status() {
		return desc_status;
	}

	public void setDesc_status(String desc_status) {
		this.desc_status = desc_status;
	}

	public String getDesc_result() {
		return desc_result;
	}

	public void setDesc_result(String desc_result) {
		this.desc_result = desc_result;
	}

	public String getHc_rule_nm() {
		return hc_rule_nm;
	}

	public void setHc_rule_nm(String hc_rule_nm) {
		this.hc_rule_nm = hc_rule_nm;
	}

	public String getSummon_seq() {
		return summon_seq;
	}

	public void setSummon_seq(String summon_seq) {
		this.summon_seq = summon_seq;
	}

	public String getHc_send_dt() {
		return hc_send_dt;
	}

	public void setHc_send_dt(String hc_send_dt) {
		this.hc_send_dt = hc_send_dt;
	}

	public String getDecision_dt() {
		return decision_dt;
	}

	public void setDecision_dt(String decision_dt) {
		this.decision_dt = decision_dt;
	}

	public String getSsn_name() {
		return ssn_name;
	}

	public void setSsn_name(String ssn_name) {
		this.ssn_name = ssn_name;
	}

	public String getPosit_gu() {
		return posit_gu;
	}

	public void setPosit_gu(String posit_gu) {
		this.posit_gu = posit_gu;
	}

	public String getAcc_org_nm() {
		return acc_org_nm;
	}

	public void setAcc_org_nm(String acc_org_nm) {
		this.acc_org_nm = acc_org_nm;
	}

	public String getResult_type() {
		return result_type;
	}

	public void setResult_type(String result_type) {
		this.result_type = result_type;
	}

	public String getProc_date() {
		return proc_date;
	}

	public void setProc_date(String proc_date) {
		this.proc_date = proc_date;
	}

	public String getUse_by_date() {
		return use_by_date;
	}

	public void setUse_by_date(String use_by_date) {
		this.use_by_date = use_by_date;
	}

	public String getAdmin_user_id() {
		return admin_user_id;
	}

	public void setAdmin_user_id(String admin_user_id) {
		this.admin_user_id = admin_user_id;
	}

	public String getAdmin_user_name() {
		return admin_user_name;
	}

	public void setAdmin_user_name(String admin_user_name) {
		this.admin_user_name = admin_user_name;
	}

	public String getAuth_id() {
		return auth_id;
	}

	public void setAuth_id(String auth_id) {
		this.auth_id = auth_id;
	}

	public String getDept_id() {
		return dept_id;
	}

	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getPwd_check() {
		return pwd_check;
	}

	public void setPwd_check(Integer pwd_check) {
		this.pwd_check = pwd_check;
	}
	
	public Date getLast_pwd_change_datetime() {
		return last_pwd_change_datetime;
	}

	public void setLast_pwd_change_datetime(Date last_pwd_change_datetime) {
		this.last_pwd_change_datetime = last_pwd_change_datetime;
	}

	public Date getLast_login_success_datetime() {
		return last_login_success_datetime;
	}

	public void setLast_login_success_datetime(Date last_login_success_datetime) {
		this.last_login_success_datetime = last_login_success_datetime;
	}

	public Date getLast_login_fail_datetime() {
		return last_login_fail_datetime;
	}

	public void setLast_login_fail_datetime(Date last_login_fail_datetime) {
		this.last_login_fail_datetime = last_login_fail_datetime;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public String getEmail_address() {
		return email_address;
	}

	public void setEmail_address(String email_address) {
		this.email_address = email_address;
	}

	public String getMobile_number() {
		return mobile_number;
	}

	public void setMobile_number(String mobile_number) {
		this.mobile_number = mobile_number;
	}

	public String getDel_flag() {
		return del_flag;
	}

	public void setDel_flag(String del_flag) {
		this.del_flag = del_flag;
	}
	
	public String getAlarm_flag() {
		return alarm_flag;
	}

	public void setAlarm_flag(String alarm_flag) {
		this.alarm_flag = alarm_flag;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getInsert_user_id() {
		return insert_user_id;
	}

	public void setInsert_user_id(String insert_user_id) {
		this.insert_user_id = insert_user_id;
	}

	public Date getInsert_datetime() {
		return insert_datetime;
	}

	public void setInsert_datetime(Date insert_datetime) {
		this.insert_datetime = insert_datetime;
	}

	public String getUpdate_user_id() {
		return update_user_id;
	}

	public void setUpdate_user_id(String update_user_id) {
		this.update_user_id = update_user_id;
	}

	public Date getUpdate_datetime() {
		return update_datetime;
	}

	public void setUpdate_datetime(Date update_datetime) {
		this.update_datetime = update_datetime;
	}

	public String getDept_name() {
		return dept_name;
	}

	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}

	public String getAuth_name() {
		return auth_name;
	}

	public void setAuth_name(String auth_name) {
		this.auth_name = auth_name;
	}

	public Long getCnt() {
		return cnt;
	}

	public void setCnt(Long cnt) {
		this.cnt = cnt;
	}
	
	public String getDeptLowSearchFlag() {
		return deptLowSearchFlag;
	}

	public void setDeptLowSearchFlag(String deptLowSearchFlag) {
		this.deptLowSearchFlag = deptLowSearchFlag;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getCurrentPasswd() {
		return currentPasswd;
	}

	public void setCurrentPasswd(String currentPasswd) {
		this.currentPasswd = currentPasswd;
	}

	public String getAuth_ids() {
		return auth_ids;
	}

	public void setAuth_ids(String auth_ids) {
		this.auth_ids = auth_ids;
	}

	public String getDetailEmpDetailSeq() {
		return detailEmpDetailSeq;
	}

	public void setDetailEmpDetailSeq(String detailEmpDetailSeq) {
		this.detailEmpDetailSeq = detailEmpDetailSeq;
	}

	public String getDeci_nm() {
		return deci_nm;
	}

	public void setDeci_nm(String deci_nm) {
		this.deci_nm = deci_nm;
	}

	public String getDesc_decision_seq() {
		return desc_decision_seq;
	}

	public void setDesc_decision_seq(String desc_decision_seq) {
		this.desc_decision_seq = desc_decision_seq;
	}

	public String getDecision() {
		return decision;
	}

	public void setDecision(String decision) {
		this.decision = decision;
	}

	public String getSystem_name() {
		return system_name;
	}

	public void setSystem_name(String system_name) {
		this.system_name = system_name;
	}

	public String getSystem_seq() {
		return system_seq;
	}

	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}

	public String getEncrypt_type() {
		return encrypt_type;
	}

	public void setEncrypt_type(String encrypt_type) {
		this.encrypt_type = encrypt_type;
	}
	
	private String _userId;

	public String get_userId() {
		return _userId;
	}

	public void set_userId(String _userId) {
		this._userId = _userId;
	}

	public String getDecision_status_second() {
		return decision_status_second;
	}

	public void setDecision_status_second(String decision_status_second) {
		this.decision_status_second = decision_status_second;
	}
	
}
