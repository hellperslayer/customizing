package com.easycerti.eframe.psm.system_management.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 메뉴권한 리스트 VO
 * 
 * @author yjyoo
 * @since 2015. 4. 20.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 20.           yjyoo            최초 생성
 *
 * </pre>
 */
public class MenuAuthList extends AbstractValueObject {

	// 메뉴권한 리스트
	private List<MenuAuth> menuAuths = null;

	public List<MenuAuth> getMenuAuths() {
		return menuAuths;
	}

	public void setMenuAuths(List<MenuAuth> menuAuths) {
		this.menuAuths = menuAuths;
	}
	
}
