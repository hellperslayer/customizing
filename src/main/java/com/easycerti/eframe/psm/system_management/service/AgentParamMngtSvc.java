package com.easycerti.eframe.psm.system_management.service;

import java.util.Map;

import com.easycerti.eframe.common.spring.DataModelAndView;

/**
 * 관리자 관련 Service Interface
 * 
 * @author yjyoo
 * @since 2015. 4. 20.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 20.           yjyoo           최초 생성
 *   2015. 5. 22.			yjyoo			엑셀 다운로드 추가
 *
 * </pre>
 */
public interface AgentParamMngtSvc {
	
	/**
	 * 관리자 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	public DataModelAndView findAgentParamMngtList(Map<String, String> parameters);

	/**
	 * 관리자 상세정보
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	public DataModelAndView findAgentParamMngtOne(Map<String, String> parameters);
	/*
	*//**
	 * 관리자 추가 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 *//*
	public DataModelAndView addAgentParamMngtOne(Map<String, String> parameters);
	
	*//**
	 * 관리자 수정 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	public DataModelAndView saveAgentParamMngtOne(Map<String, String> parameters);
	
	/**
	 * 관리자 삭제 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 *//*
	public DataModelAndView removeAgentParamMngtOne(Map<String, String> parameters);
	
	*//**
	 * 관리자 로그인
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 *//*
	public DataModelAndView login(Map<String, String> parameters, HttpServletRequest request);
	
	*//**
	 * 관리자 로그아웃
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 *//*
	public DataModelAndView logout(Map<String, String> parameters, HttpServletRequest request) throws Exception;

	*//**
	 * 관리자 비밀번호 변경
	 * @author tjlee
	 * @since 2015. 7. 03.
	 * @return DataModelAndView
	 *//*
	public DataModelAndView rePassword(Map<String, String> parameters, HttpServletRequest request) throws Exception;
	public DataModelAndView changePassword(Map<String, String> parameters, HttpServletRequest request) throws Exception;

	public List<SystemMaster> getSystemMaster();
	
	*/
	

	
}
