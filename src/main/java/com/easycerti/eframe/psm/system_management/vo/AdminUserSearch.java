package com.easycerti.eframe.psm.system_management.vo;


import com.easycerti.eframe.common.vo.SearchBase;

/**
 * 관리자관리 검색 VO
 * 
 * @author yjyoo
 * @since 2015. 6. 11.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 6. 11.           yjyoo            최초 생성
 *
 * </pre>
 */
public class AdminUserSearch extends SearchBase {
	
	// 관리자 ID
	private String admin_user_id_1;
	
	// 관리자 이름
	private String admin_user_name;
	
	// 부서 ID
	private String dept_id;
	
	// 하위 부서 검색
	private String deptLowSearchFlag;
	
	// 관리자 권한 ID
	private String auth_id;
	
	private String system_seq;

	private String option;
	
	public String getOption() {
		return option;
	}

	public void setOption(String option) {
		this.option = option;
	}

	public String getAdmin_user_id_1() {
		return admin_user_id_1;
	}

	public void setAdmin_user_id_1(String admin_user_id_1) {
		this.admin_user_id_1 = admin_user_id_1;
	}

	public String getAdmin_user_name() {
		return admin_user_name;
	}

	public void setAdmin_user_name(String admin_user_name) {
		this.admin_user_name = admin_user_name;
	}

	public String getDept_id() {
		return dept_id;
	}

	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}

	public String getDeptLowSearchFlag() {
		return deptLowSearchFlag;
	}

	public void setDeptLowSearchFlag(String deptLowSearchFlag) {
		this.deptLowSearchFlag = deptLowSearchFlag;
	}

	public String getAuth_id() {
		return auth_id;
	}

	public void setAuth_id(String auth_id) {
		this.auth_id = auth_id;
	}

	public String getSystem_seq() {
		return system_seq;
	}

	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}

	@Override
	public String toString() {
		return "AdminUserSearch [admin_user_id_1=" + admin_user_id_1
				+ ", admin_user_name=" + admin_user_name + ", dept_id="
				+ dept_id + ", deptLowSearchFlag=" + deptLowSearchFlag
				+ ", auth_id=" + auth_id + "]";
	}
	
}
