package com.easycerti.eframe.psm.system_management.vo;

import com.easycerti.eframe.common.vo.AbstractValueObject;

public class IpPerm extends AbstractValueObject {
	private int ip_seq;
	private String ip_content;
	private String use_flag;
	private int system_seq;
	private String admin_id;
	
	public int getSystem_seq() {
		return system_seq;
	}
	public void setSystem_seq(int system_seq) {
		this.system_seq = system_seq;
	}
	public int getIp_seq() {
		return ip_seq;
	}
	public void setIp_seq(int ip_seq) {
		this.ip_seq = ip_seq;
	}
	public String getIp_content() {
		return ip_content;
	}
	public void setIp_content(String ip_content) {
		this.ip_content = ip_content;
	}
	public String getUse_flag() {
		return use_flag;
	}
	public void setUse_flag(String use_flag) {
		this.use_flag = use_flag;
	}
	public String getAdmin_id() {
		return admin_id;
	}
	public void setAdmin_id(String admin_id) {
		this.admin_id = admin_id;
	}

}
