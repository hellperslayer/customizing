package com.easycerti.eframe.psm.system_management.service;

import java.util.Map;

import com.easycerti.eframe.common.spring.DataModelAndView;

public interface IpInfoMngSvc {
	public DataModelAndView ipInfoMngList(Map<String, String> parameters);
	public void addUserInfo(Map<String, String> parameters);
	public DataModelAndView deleteUserInfo(Map<String, String> parameters);
}
