package com.easycerti.eframe.psm.system_management.service.impl;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.system_management.dao.CodeMngtDao;
import com.easycerti.eframe.psm.system_management.dao.SummaryMngtDao;
import com.easycerti.eframe.psm.system_management.service.SummaryMngtSvc;

@Service
public class SummaryMngtSvcImpl implements SummaryMngtSvc{

	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	@Autowired
	private SummaryMngtDao summaryMngtDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private CodeMngtDao codeMngtDao;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public DataModelAndView summaryMngtList() {
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);
		Calendar calendar = Calendar.getInstance();
		
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		String search_to = sdf.format(calendar.getTime());
		calendar.add(Calendar.MONTH, -1);
		String search_from = sdf.format(calendar.getTime());
		
		Map<String, String> paramBean = new HashMap<>();
		paramBean.put("search_from", search_from);
		paramBean.put("search_to", search_to);
		modelAndView.addObject("paramBean", paramBean);
		
		//활성화된 보고서 목록 출력
		Code code = new Code();
		code.setGroup_code_id("REPORT_TYPE");
		code.setUse_flag("Y");
		List<Code> reportcode = codeMngtDao.findCodeMngtList(code);
		modelAndView.addObject("reportcode", reportcode);
		
		SimpleCode simpleCode = new SimpleCode("/summaryMngt/list.html");
		String index_id = commonDao.checkIndex(simpleCode);
		modelAndView.addObject("index_id", index_id);
		
		code = new Code();
		code.setGroup_code_id("SOLUTION_MASTER");
		code.setCode_id("INSTALL_DATE");
		code.setUse_flag("Y");
		Code install_date = codeMngtDao.findCodeMngtOne(code);
		modelAndView.addObject("install_date", install_date);
		
		return modelAndView;
	}
	
	@Override
	public int createSummary(String procdate, String type) {
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			int res = 0;
			Map<String,String> param = new HashMap<>();
			param.put("proc_date", procdate);
			param.put("type", type);
			summaryMngtDao.deleteSummary(param);
			
			switch (type) {
			case "03":
				res = summaryMngtDao.createSummaryA03(procdate);
				break;
			case "05":
				res = summaryMngtDao.createSummaryA05(procdate);
				break;
			case "06":
				res = summaryMngtDao.createSummaryA06(procdate);
				break;
			case "07":
				res = summaryMngtDao.createSummaryA07(procdate);
				break;
			case "99":
				res = summaryMngtDao.createSummaryA99(procdate);
				break;
			case "biz_log":
				res = summaryMngtDao.createSummaryBizLog(procdate);
				break;
			}
			/*summaryMngtDao.deleteSummary(procdate);
			int res = summaryMngtDao.createSummary(procdate);*/
			transactionManager.commit(transactionStatus);
			return res;
		} catch(DataAccessException e) {
			logger.error("[SummaryMngtSvcImpl.createSummary] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
	}
	
	@Override
	public int createSummaryAll(String procdate) {

		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			int res = 0;
			summaryMngtDao.deleteSummaryAll(procdate);
			
			res += summaryMngtDao.createSummaryA03(procdate);
			res += summaryMngtDao.createSummaryA05(procdate);
			res += summaryMngtDao.createSummaryA06(procdate);
			res += summaryMngtDao.createSummaryA07(procdate);
			res += summaryMngtDao.createSummaryA99(procdate);
			res += summaryMngtDao.createSummaryBizLog(procdate);
			
			res += summaryMngtDao.createSummaryB03(procdate);
			res += summaryMngtDao.createSummaryB05(procdate);
			res += summaryMngtDao.createSummaryB06(procdate);
			res += summaryMngtDao.createSummaryB07(procdate);
			res += summaryMngtDao.createSummaryDownloadLog(procdate);
			
			transactionManager.commit(transactionStatus);
			return res;
		} catch(DataAccessException e) {
			logger.error("[SummaryMngtSvcImpl.createSummaryAll] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
	}
	
	@Override
	public int createSummaryDaily(String procdate) {
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			int res = 0;
			summaryMngtDao.deleteSummaryStatistics(procdate);
			summaryMngtDao.createSummaryStatisticsDefault(procdate);
			
			summaryMngtDao.deleteSummaryTime(procdate);
			summaryMngtDao.createSummaryTimeDefault(procdate);
			
			summaryMngtDao.deleteSummaryDownload(procdate);
			summaryMngtDao.createSummaryDownloadDefault(procdate);
			
			summaryMngtDao.deleteSummaryReqtype(procdate);
			summaryMngtDao.createSummaryReqtypeDefault(procdate);
			
			res += summaryMngtDao.createSummaryStatistics(procdate);
			res += summaryMngtDao.createSummaryStatisticsDown(procdate);
			res += summaryMngtDao.createSummaryStatisticsSystem(procdate);
			
			res += summaryMngtDao.createSummaryTime(procdate);
			res += summaryMngtDao.createSummaryTimeDown(procdate);
			res += summaryMngtDao.createSummaryTimeSystem(procdate);
			
			res += summaryMngtDao.createSummaryDownload(procdate);
			
			res += summaryMngtDao.createSummaryReqtype(procdate);
			res += summaryMngtDao.createSummaryReqtypeDown(procdate);
			res += summaryMngtDao.createSummaryReqtypeSystem(procdate);
			
			
			transactionManager.commit(transactionStatus);
			return res;
		} catch(DataAccessException e) {
			logger.error("[SummaryMngtSvcImpl.createSummaryDaily] MESSAGE : " + e.getMessage());
			e.printStackTrace();
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
	}
	
	@Override
	public int createSummaryMonth(Map<String, String> parameter) {
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			int res = 0;
			String tmpDate = parameter.get("startDate").substring(0, 6);
			
			summaryMngtDao.deleteSummaryStatisticsMonth(tmpDate);
			summaryMngtDao.createSummaryStatisticsMonthDefault(tmpDate);
			summaryMngtDao.deleteSummaryTimeMonth(tmpDate);
//			summaryMngtDao.createSummaryTimeMonthDefault(tmpDate);
			summaryMngtDao.deleteSummaryDownloadMonth(tmpDate);
//			summaryMngtDao.createSummaryDownloadMonthDefault(tmpDate);
			summaryMngtDao.deleteSummaryReqtypeMonth(tmpDate);
			
			res += summaryMngtDao.createSummaryStatisticsMonth(parameter);
			res += summaryMngtDao.createSummaryTimeMonth(parameter);
			res += summaryMngtDao.createSummaryDownloadMonth(parameter);
			res += summaryMngtDao.createSummaryReqtypeMonth(parameter);
			
			// 비정상위험 월마감
			summaryMngtDao.deleteSummaryAbnormalMonth(tmpDate);
			summaryMngtDao.createSummaryAbnormalMonthDefault(tmpDate);
			res += summaryMngtDao.createSummaryAbnormalMonth(parameter);
			
			transactionManager.commit(transactionStatus);
			return res;
		} catch(DataAccessException e) {
			logger.error("[SummaryMngtSvcImpl.createSummaryMonth] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
	}

	@Override
	public String checkSummaryDaily(Map<String, String> parameters) {
		Integer count = 0;
		String reportType = parameters.get("reportType");
		int summaryCount = Integer.parseInt(parameters.get("summaryCount"));
		
		String result = "";
		
		//보고서별로 일마감 테이블 확인하여 데이터 있는지 체크
		//우선 하드코딩으로 개발하였으나 차후 별도의 관리테이블 생성하여 다시 구현을 권장
		if(reportType.equals("2")||reportType.equals("13")) {
			parameters.put("log_delimiter","BA");
		}else if(reportType.equals("12")){
			parameters.put("log_delimiter","DN");
		}
		
		if (summaryCount<1) {
			String startDate = parameters.get("startDate");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			Calendar calendar = new GregorianCalendar();
			Date date;
			try {
				date = sdf.parse(startDate);
				calendar.setTime(date);
				calendar.add(Calendar.MONTH, -1);
				date = calendar.getTime();
				parameters.put("startDate",sdf.format(date));
				date = sdf.parse(startDate);
				calendar.setTime(date);
				calendar.add(Calendar.DATE, -1);
				date = calendar.getTime();
				parameters.put("endDate",sdf.format(date));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		try {
			if((count = summaryMngtDao.checkSummaryDaily(parameters))==null) {
				if(summaryCount<1) {
					result = "SUCCESS";	//이전 보고서도 없고 이전 데이터도 없을 때
				}else {
					result = "NONEDATA";	//이전 보고서가 있고 이번달 데이터가 없을 때
				}
			}else {
				if(summaryCount<1) {
					result = "BEFOREDATA";	//이전 보고서가 없고 이전 데이터가 있을때
				}else {
					result = "SUCCESS";//이전 보고서도 있고 데이터도 있을때
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}


	@Override
	public String checkSummaryMonth(Map<String, String> parameters) {
		int count = summaryMngtDao.checkSummaryMonth(parameters);
		return null;
	}
	
	@Override
	public String addInstallDate(Map<String, String> parameters) {
		String result = "SUCCESS";
		parameters.put("procdate",parameters.get("procdate").replace("-", ""));
		try {
			parameters.put("targetTable","summary_download");
			summaryMngtDao.createSummaryInstallDate(parameters);
			parameters.put("targetTable","summary_reqtype");
			summaryMngtDao.createSummaryInstallDate(parameters);
			parameters.put("targetTable","summary_statistics");
			summaryMngtDao.createSummaryInstallDate(parameters);
			parameters.put("targetTable","summary_time");
			summaryMngtDao.createSummaryInstallDate(parameters);
		} catch (Exception e) {
			e.printStackTrace();
			result = "FAIL";
		}
		return result;
	}

	@Override
	public boolean extractorLogCheck(String procdate) {
		boolean result = false;
		String temp = "/usr/local/PSM/agent/extractor/logs/PSMExtractor.log";
		try {
			Thread.sleep(1000);
			Process process = new ProcessBuilder("tail", "-f", temp).start();
			BufferedInputStream bis = new BufferedInputStream(process.getInputStream());
			BufferedReader r = new BufferedReader(new InputStreamReader(bis, StandardCharsets.UTF_8));
			while(r.readLine()!=null) {
				String log = r.readLine();
				if(log.equals("*************************완료*************************")) {
					logger.debug("EXTRACTOR SUCCESS : "+procdate);
					result = true;
					break;
				} else if(log.equals("*************************실패*************************")) {
					logger.debug("EXTRACTOR FAIL : "+procdate);
					break;
				}
			}
			r.close();
			bis.close();
			process.destroy();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	public int createSummaryAbnormal(String procdate) {
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			int res = 0;
			summaryMngtDao.deleteSummaryAbnormal(procdate);
			summaryMngtDao.createSummaryAbnormalDefault(procdate);
			
			res += summaryMngtDao.createSummaryAbnormal(procdate);
			
			transactionManager.commit(transactionStatus);
			return res;
		} catch(DataAccessException e) {
			logger.error("[SummaryMngtSvcImpl.createSummaryAbnormal] MESSAGE : " + e.getMessage());
			e.printStackTrace();
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
	}

	/*
	 * createSummaryDaily에서 한번에 일마감을 돌리나, 시스템이 느린곳에서는 한번에 돌지 않아 마감 기능별로 분리함
	 * statistics, reqtype, download, time
	 */
	@Override
	public int createSummaryStatistics(String procdate) {
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			int res = 0;
			summaryMngtDao.deleteSummaryStatistics(procdate);
			summaryMngtDao.createSummaryStatisticsDefault(procdate);
			
			res += summaryMngtDao.createSummaryStatistics(procdate);
			res += summaryMngtDao.createSummaryStatisticsDown(procdate);
			res += summaryMngtDao.createSummaryStatisticsSystem(procdate);			
			
			transactionManager.commit(transactionStatus);
			return res;
		} catch(DataAccessException e) {
			logger.error("[SummaryMngtSvcImpl.createSummaryStatistics] MESSAGE : " + e.getMessage());
			e.printStackTrace();
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
	}

	@Override
	public int createSummaryReqtype(String procdate) {
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			int res = 0;			
			summaryMngtDao.deleteSummaryReqtype(procdate);
			summaryMngtDao.createSummaryReqtypeDefault(procdate);
			
			res += summaryMngtDao.createSummaryReqtype(procdate);
			res += summaryMngtDao.createSummaryReqtypeDown(procdate);
			res += summaryMngtDao.createSummaryReqtypeSystem(procdate);
			
			transactionManager.commit(transactionStatus);
			return res;
		} catch(DataAccessException e) {
			logger.error("[SummaryMngtSvcImpl.createSummaryDaily] MESSAGE : " + e.getMessage());
			e.printStackTrace();
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
	}

	@Override
	public int createSummaryDownload(String procdate) {
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			int res = 0;
			summaryMngtDao.deleteSummaryDownload(procdate);
			summaryMngtDao.createSummaryDownloadDefault(procdate);
			
			res += summaryMngtDao.createSummaryDownload(procdate);			
			
			transactionManager.commit(transactionStatus);
			return res;
		} catch(DataAccessException e) {
			logger.error("[SummaryMngtSvcImpl.createSummaryDaily] MESSAGE : " + e.getMessage());
			e.printStackTrace();
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
	}

	@Override
	public int createSummaryTime(String procdate) {
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			int res = 0;
			summaryMngtDao.deleteSummaryTime(procdate);
			summaryMngtDao.createSummaryTimeDefault(procdate);
			
			res += summaryMngtDao.createSummaryTime(procdate);
			res += summaryMngtDao.createSummaryTimeDown(procdate);
			res += summaryMngtDao.createSummaryTimeSystem(procdate);
			
			transactionManager.commit(transactionStatus);
			return res;
		} catch(DataAccessException e) {
			logger.error("[SummaryMngtSvcImpl.createSummaryDaily] MESSAGE : " + e.getMessage());
			e.printStackTrace();
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
	}
}
