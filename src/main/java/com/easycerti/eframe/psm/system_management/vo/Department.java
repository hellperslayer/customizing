package com.easycerti.eframe.psm.system_management.vo;

import java.util.Date;

import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 부서 VO
 * 
 * @author yjyoo
 * @since 2015. 4. 17.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 17.           yjyoo            최초 생성
 *
 * </pre>
 */
public class Department extends AbstractValueObject {

	// 부서 ID
	private String dept_id = null;
	
	// 상위부서 ID
	private String parent_dept_id = null;
	
	// 부서 명
	private String dept_name = null;
	
	// 부서 설명
	private String description = null;
	
	// 부서 깊이
	private Integer dept_depth = null;
	
	// 정렬순서
	private Integer sort_order = null;
	
	// 사용여부
	private Character use_flag = null;
	
	// 등록자
	private String insert_user_id = null;
	
	// 등록일시
	private Date insert_datetime = null;
	
	// 수정자
	private String update_user_id = null;
	
	private String system_seq = null;
	
	// 수정일시
	private Date update_datetime = null;
	
	/**
	 * Etc
	 */
	// 부서 전체 경로
	private String full_dept_name = null;
	
	// 부서 depth만큼 '-' 추가
	private String simple_dept_name = null;
	
	// 부모부서명
	private String parent_dept_name = null;
	
	// 등록 일시 (String type)
	private String insert_datetime_string = null;
	
	// 수정 일시 (String type)
	private String update_datetime_string = null;
	private String threshold;
	
	private String sel_dept_id = null;
	
	public String getSel_dept_id() {
		return sel_dept_id;
	}

	public void setSel_dept_id(String sel_dept_id) {
		this.sel_dept_id = sel_dept_id;
	}

	public String getDept_id() {
		return dept_id;
	}

	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}

	public String getParent_dept_id() {
		return parent_dept_id;
	}

	public void setParent_dept_id(String parent_dept_id) {
		this.parent_dept_id = parent_dept_id;
	}

	public String getDept_name() {
		return dept_name;
	}

	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getDept_depth() {
		return dept_depth;
	}

	public void setDept_depth(Integer dept_depth) {
		this.dept_depth = dept_depth;
	}

	public Integer getSort_order() {
		return sort_order;
	}

	public void setSort_order(Integer sort_order) {
		this.sort_order = sort_order;
	}

	public Character getUse_flag() {
		return use_flag;
	}

	public void setUse_flag(Character use_flag) {
		this.use_flag = use_flag;
	}

	public String getInsert_user_id() {
		return insert_user_id;
	}

	public void setInsert_user_id(String insert_user_id) {
		this.insert_user_id = insert_user_id;
	}

	public Date getInsert_datetime() {
		return insert_datetime;
	}

	public void setInsert_datetime(Date insert_datetime) {
		this.insert_datetime = insert_datetime;
	}

	public String getUpdate_user_id() {
		return update_user_id;
	}

	public void setUpdate_user_id(String update_user_id) {
		this.update_user_id = update_user_id;
	}

	public Date getUpdate_datetime() {
		return update_datetime;
	}

	public void setUpdate_datetime(Date update_datetime) {
		this.update_datetime = update_datetime;
	}

	public String getFull_dept_name() {
		return full_dept_name;
	}

	public void setFull_dept_name(String full_dept_name) {
		this.full_dept_name = full_dept_name;
	}

	public String getSimple_dept_name() {
		return simple_dept_name;
	}

	public void setSimple_dept_name(String simple_dept_name) {
		this.simple_dept_name = simple_dept_name;
	}
	
	public String getParent_dept_name() {
		return parent_dept_name;
	}

	public void setParent_dept_name(String parent_dept_name) {
		this.parent_dept_name = parent_dept_name;
	}

	public String getInsert_datetime_string() {
		return insert_datetime_string;
	}

	public void setInsert_datetime_string(String insert_datetime_string) {
		this.insert_datetime_string = insert_datetime_string;
	}

	public String getUpdate_datetime_string() {
		return update_datetime_string;
	}

	public void setUpdate_datetime_string(String update_datetime_string) {
		this.update_datetime_string = update_datetime_string;
	}
	public String getSystem_seq() {
		return system_seq;
	}

	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}

	public String getThreshold() {
		return threshold;
	}

	public void setThreshold(String threshold) {
		this.threshold = threshold;
	}

	
}
