package com.easycerti.eframe.psm.system_management.dao;

import java.util.List;

import com.easycerti.eframe.psm.control.vo.Code;

/**
 * 코드 관련 Dao Interface
 * 
 * @author yjyoo
 * @since 2015. 4. 17.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 17.           yjyoo            최초 생성
 *
 * </pre>
 */
public interface CodeMngtDao {
	
	/**
	 * 코드 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return List<Code>
	 */
	public List<Code> findCodeMngtList(Code code);
	
	/**
	 * 코드 상세정보
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return Code
	 */
	public Code findCodeMngtOne(Code code);
	
	/**
	 * 코드 추가
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return void
	 */
	public void addCodeMngtOne(Code code);
	
	/**
	 * 코드 수정
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return void
	 */
	public void saveCodeMngtOne(Code code);
	
	/**
	 * 코드 삭제 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return void
	 */
	public void removeCodeMngtOne(Code code);
	
}
