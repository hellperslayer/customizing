package com.easycerti.eframe.psm.system_management.vo;


import java.sql.Timestamp;

import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 사원 VO
 * 
 * @author yjyoo
 * @since 2015. 4. 21.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 21.           yjyoo            최초 생성
 *
 * </pre>
 */
public class EmpUser extends AbstractValueObject {
	
	public EmpUser() {}
	
	public EmpUser(String emp_user_name,String emp_user_id,String dept_name,String ip) {
		this.emp_user_name = emp_user_name;
		this.emp_user_id = emp_user_id;
		this.dept_name = dept_name;
		this.ip = ip;
	}
	// 사원 ID
	private String emp_user_id;
	
	// 시스템 번호
	private String system_seq;
	private String sys_cd;
	
	// 직원 이름
	private String emp_user_name;
	
	// 부서 ID
	private String dept_id;
	
	// 이메일
	private String email_address;
	
	// 전화번호
	private String mobile_number;

	// IP
	private String ip;
	
	// BIZ_LOG IP
	private String user_ip;
	
	// 로그인ID
	private String user_id;
	
	// 로그인PW
	private String emp_user_login_pw;
	
	// 상태
	private String status;
	
	// 사원 추가한 관리자 ID
	private String insert_user_id;
	
	// 사원 추가한 시간
	private Timestamp insert_datetime;
	
	// 사원 수정한 관리자 ID
	private String update_user_id;
	
	// 사용여부
	private String use_flag;
	
/*	public CommonsMultipartFile getFile() {
		return file;
	}

	public void setFile(CommonsMultipartFile file) {
		this.file = file;
	}
	//파일 업로드
	private CommonsMultipartFile file;*/
	// 사원 수정한 시간
	private Timestamp update_datetime;
	

	
	
	/**
	 * etc
	 */
	// 시스템 이름
	private String system_name;
	
	// 부서 이름
	private String dept_name;
	
	// 상태 이름
	private String status_name;
	
	// 하위 부서 검색
	private String deptLowSearchFlag;
	
	private String threshold;
	private String threshold_dept;
	private String org_cd;
	private String org_nm;
	private String desc_access_cd;
	private String desc_access_org_cd;
	
	private int external_staff;
	private String self_summon;
	
	
	public String getSelf_summon() {
		return self_summon;
	}

	public void setSelf_summon(String self_summon) {
		this.self_summon = self_summon;
	}

	public int getExternal_staff() {
		return external_staff;
	}

	public void setExternal_staff(int external_staff) {
		this.external_staff = external_staff;
	}

	public String getDesc_access_org_cd() {
		return desc_access_org_cd;
	}

	public void setDesc_access_org_cd(String desc_access_org_cd) {
		this.desc_access_org_cd = desc_access_org_cd;
	}

	public String getOrg_cd() {
		return org_cd;
	}

	public void setOrg_cd(String org_cd) {
		this.org_cd = org_cd;
	}

	public String getOrg_nm() {
		return org_nm;
	}

	public void setOrg_nm(String org_nm) {
		this.org_nm = org_nm;
	}

	public String getDesc_access_cd() {
		return desc_access_cd;
	}

	public void setDesc_access_cd(String desc_access_cd) {
		this.desc_access_cd = desc_access_cd;
	}

	public String getUser_ip() {
		return user_ip;
	}

	public void setUser_ip(String user_ip) {
		this.user_ip = user_ip;
	}
	public String getEmp_user_id() {
		return emp_user_id;
	}

	public void setEmp_user_id(String emp_user_id) {
		this.emp_user_id = emp_user_id;
	}
	
	public String getSystem_seq() {
		return system_seq;
	}

	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}

	public String getSys_cd() {
		return sys_cd;
	}

	public void setSys_cd(String sys_cd) {
		this.sys_cd = sys_cd;
	}

	public String getEmp_user_name() {
		return emp_user_name;
	}

	public void setEmp_user_name(String emp_user_name) {
		this.emp_user_name = emp_user_name;
	}

	public String getDept_id() {
		return dept_id;
	}

	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}

	public String getEmail_address() {
		return email_address;
	}

	public void setEmail_address(String email_address) {
		this.email_address = email_address;
	}

	public String getMobile_number() {
		return mobile_number;
	}

	public void setMobile_number(String mobile_number) {
		this.mobile_number = mobile_number;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getEmp_user_login_pw() {
		return emp_user_login_pw;
	}

	public void setEmp_user_login_pw(String emp_user_login_pw) {
		this.emp_user_login_pw = emp_user_login_pw;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getInsert_user_id() {
		return insert_user_id;
	}

	public void setInsert_user_id(String insert_user_id) {
		this.insert_user_id = insert_user_id;
	}

	public Timestamp getInsert_datetime() {
		return insert_datetime;
	}

	public void setInsert_datetime(Timestamp insert_datetime) {
		this.insert_datetime = insert_datetime;
	}

	public String getUpdate_user_id() {
		return update_user_id;
	}

	public void setUpdate_user_id(String update_user_id) {
		this.update_user_id = update_user_id;
	}

	public Timestamp getUpdate_datetime() {
		return update_datetime;
	}

	public void setUpdate_datetime(Timestamp update_datetime) {
		this.update_datetime = update_datetime;
	}
	
	public String getSystem_name() {
		return system_name;
	}

	public void setSystem_name(String system_name) {
		this.system_name = system_name;
	}

	public String getDept_name() {
		return dept_name;
	}

	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}
	
	public String getStatus_name() {
		return status_name;
	}

	public void setStatus_name(String status_name) {
		this.status_name = status_name;
	}

	public String getDeptLowSearchFlag() {
		return deptLowSearchFlag;
	}

	public void setDeptLowSearchFlag(String deptLowSearchFlag) {
		this.deptLowSearchFlag = deptLowSearchFlag;
	}

	public String getThreshold() {
		return threshold;
	}

	public void setThreshold(String threshold) {
		this.threshold = threshold;
	}

	public String getThreshold_dept() {
		return threshold_dept;
	}

	public void setThreshold_dept(String threshold_dept) {
		this.threshold_dept = threshold_dept;
	}

	public String getUse_flag() {
		return use_flag;
	}

	public void setUse_flag(String use_flag) {
		this.use_flag = use_flag;
	}
	
}
