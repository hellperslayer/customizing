package com.easycerti.eframe.psm.system_management.dao;

import java.util.List;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.system_management.vo.OptionSetting;

/**
 * 옵션 설정 Dao Interface
 */
public interface OptionSettingDao {
	
	public List<OptionSetting> findOptionSettingList();
	public List<OptionSetting> findOptionSettingListByAuth(String auth);
	public void setOptionSetting(OptionSetting optionSetting);
	public void setMaster(String flag);
	public void setMaster2(String flag);
	public void setSessionTime(String flag);
	public void setResultTypeView(String type);
	public void setSidebarView(String type);
	public void setScrnNameView(String type);
	public void setMappingId(String type);
	public void setUse_systemSeq(String type);
	public void setUi_type(String type);
	public void createAccessAuth();
	public void createBizLogFile();
	public String getChangePasswordDay();
	public String getResultOwnerFlag();
	public void alterColumnResultOwner();
	public int chechColumnOwnerFlag();
	public String getOptionSettingValue(String option_id);

}
