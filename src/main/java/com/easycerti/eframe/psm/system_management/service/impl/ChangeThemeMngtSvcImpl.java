package com.easycerti.eframe.psm.system_management.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.system_management.dao.ChangeThemeMngtDao;
import com.easycerti.eframe.psm.system_management.service.ChangeThemeMngtSvc;
import com.easycerti.eframe.psm.system_management.vo.ChangeTheme;

@Service
public class ChangeThemeMngtSvcImpl implements ChangeThemeMngtSvc{
	
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private ChangeThemeMngtDao changeThemeMngtDao;

	@Override
	public DataModelAndView changeThemeList(Map<String, String> parameters) {
		DataModelAndView modelAndView = new DataModelAndView();
		ChangeTheme paramBean = CommonHelper.convertMapToBean(parameters, ChangeTheme.class);
		
		SimpleCode simpleCode = new SimpleCode();
		String index = "/changeThemeMngt/list.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView changeThemeUpdate(Map<String, String> parameters) {
		DataModelAndView modelAndView = new DataModelAndView();
		ChangeTheme paramBean = CommonHelper.convertMapToBean(parameters, ChangeTheme.class);
		
		changeThemeMngtDao.changeThemeUpdateTheme(paramBean);
		changeThemeMngtDao.changeThemeUpdateDashboard(paramBean);
		
		return modelAndView;
	}
	
	
}
