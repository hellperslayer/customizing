package com.easycerti.eframe.psm.system_management.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.AbstractValueObject;
import com.easycerti.eframe.psm.control.vo.GroupCode;

/**
 * 그룹코드 리스트 VO
 * 
 * @author yjyoo
 * @since 2015. 4. 17.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 17.           yjyoo            최초 생성
 *
 * </pre>
 */
public class GroupCodeList extends AbstractValueObject {

	// 그룹코드 리스트
	private List<GroupCode> groupCodes = null;
	
	public GroupCodeList() {}
	
	public GroupCodeList(List<GroupCode> groupCodes){
		this.groupCodes = groupCodes;
	}
	
	public GroupCodeList(List<GroupCode> groupCodes, String count){
		this.groupCodes = groupCodes;
		setPage_total_count(count);
	}

	public List<GroupCode> getGroupCodes() {
		return groupCodes;
	}

	public void setGroupCodes(List<GroupCode> groupCodes) {
		this.groupCodes = groupCodes;
	}
	
}
