package com.easycerti.eframe.psm.system_management.vo;

import java.util.Date;

import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 대상에이전트 VO
 * 
 * @author yjyoo
 * @since 2015. 4. 24.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 24.           yjyoo            최초 생성
 *
 * </pre>
 */
public class AgentMaster extends AbstractValueObject {
	
	// 에이전트번호
	private String agent_seq = null;

	// 서버번호
	private String server_seq = null;
	
	// 에이전트명
	private String agent_name = null;
	
	// 에이전트종류
	private String agent_type = null;

	// IP
	private String ip = null;
	
	// PORT
	private String port = null;

	// 설명
	private String description = null;
	
	/**
	 * Etc 
	 */
	// 동작 시간
	private Date command_datetime = null;
	
	// 상태
	private String status = null;
	
	// 동작 변경 결과
	private String result = null;
	
	private String ba_logcnt;	//전일 접속기록로그
	private String dn_logcnt;	//전일 다운로드로그
	private String bs_logcnt;	//전일 시스템로그
	
	private String manager_name;	//수집서버 명
	private String manager_ip;		//수집서버 ip
	
	private String system_seq;	//대상 시스템
	private String system_name;	//대상 시스템명
	
	private String history;		//라이센스 히스토리
	
	private String license_seq;

	public String getLicense_seq() {
		return license_seq;
	}

	public void setLicense_seq(String license_seq) {
		this.license_seq = license_seq;
	}

	public String getHistory() {
		return history;
	}

	public void setHistory(String history) {
		this.history = history;
	}

	public String getSystem_name() {
		return system_name;
	}

	public void setSystem_name(String system_name) {
		this.system_name = system_name;
	}

	public String getSystem_seq() {
		return system_seq;
	}

	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}

	public String getManager_name() {
		return manager_name;
	}

	public void setManager_name(String manager_name) {
		this.manager_name = manager_name;
	}

	public String getManager_ip() {
		return manager_ip;
	}

	public void setManager_ip(String manager_ip) {
		this.manager_ip = manager_ip;
	}

	public String getBa_logcnt() {
		return ba_logcnt;
	}

	public void setBa_logcnt(String ba_logcnt) {
		this.ba_logcnt = ba_logcnt;
	}

	public String getDn_logcnt() {
		return dn_logcnt;
	}

	public void setDn_logcnt(String dn_logcnt) {
		this.dn_logcnt = dn_logcnt;
	}

	public String getBs_logcnt() {
		return bs_logcnt;
	}

	public void setBs_logcnt(String bs_logcnt) {
		this.bs_logcnt = bs_logcnt;
	}

	public String getAgent_seq() {
		return agent_seq;
	}

	public void setAgent_seq(String agent_seq) {
		this.agent_seq = agent_seq;
	}

	public String getServer_seq() {
		return server_seq;
	}

	public void setServer_seq(String server_seq) {
		this.server_seq = server_seq;
	}

	public String getAgent_name() {
		return agent_name;
	}

	public void setAgent_name(String agent_name) {
		this.agent_name = agent_name;
	}

	public String getAgent_type() {
		return agent_type;
	}

	public void setAgent_type(String agent_type) {
		this.agent_type = agent_type;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCommand_datetime() {
		return command_datetime;
	}

	public void setCommand_datetime(Date command_datetime) {
		this.command_datetime = command_datetime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
	
}
