package com.easycerti.eframe.psm.system_management.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.easycerti.eframe.common.spring.DataModelAndView;

/**
 * 권한 관련 Service Interface
 * 
 * @author yjyoo
 * @since 2015. 4. 20.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 20.           yjyoo           최초 생성
 *   2015. 5. 26.			yjyoo			엑셀 다운로드 추가
 *
 * </pre>
 */
public interface AuthMngtSvc {

	/**
	 * 권한 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	public DataModelAndView findAuthMngtList(Map<String, String> parameters);
	
	/**
	 * 권한 리스트 엑셀 다운로드
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 26.
	 * @return void
	 */
	public void findAuthMngtList_download(DataModelAndView modelAndView, Map<String, String> parameters, HttpServletRequest request);
	
	/**
	 * 권한 리스트 갯수
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	public DataModelAndView findAuthMngtOne(Map<String, String> parameters);
	
	/**
	 * 권한 추가 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	public DataModelAndView addAuthMngtOne(Map<String, String> parameters);
	
	/**
	 * 권한 수정 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	public DataModelAndView saveAuthMngtOne(Map<String, String> parameters);
	
	/**
	 * 권한 삭제 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 20.
	 * @return DataModelAndView
	 */
	public DataModelAndView removeAuthMngtOne(Map<String, String> parameters);
	
}
