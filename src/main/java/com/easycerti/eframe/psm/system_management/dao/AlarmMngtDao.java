package com.easycerti.eframe.psm.system_management.dao;

import java.util.List;

import com.easycerti.eframe.psm.system_management.vo.AlarmMngt;

/**
 * 옵션 설정 Dao Interface
 */
public interface AlarmMngtDao {
	
	public List<AlarmMngt> findAlarmMngtList();
	public void setAlarmMngt(AlarmMngt alarmMngt);
	
	public List<AlarmMngt> findAlarmMngtList_email();
	public void setAlarmMngt_email(AlarmMngt alarmMngt);

	public List<AlarmMngt> findAlarmMngtSystemMail();
}
