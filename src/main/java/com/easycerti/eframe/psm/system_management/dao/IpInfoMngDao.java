package com.easycerti.eframe.psm.system_management.dao;

import java.util.List;

import com.easycerti.eframe.psm.system_management.vo.IpInfoBean;

public interface IpInfoMngDao {	
	public List<IpInfoBean> ipInfoMngList(IpInfoBean ipInfoBean);
	public int ipInfoMngList_count(IpInfoBean ipInfoBean);
	public void addUserInfo(IpInfoBean ipInfoBean);
	public void deleteUserInfo(IpInfoBean ipInfoBean);
	public int checkUserInfo(IpInfoBean ipInfoBean);
}
