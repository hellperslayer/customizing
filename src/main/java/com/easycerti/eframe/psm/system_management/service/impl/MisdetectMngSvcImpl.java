package com.easycerti.eframe.psm.system_management.service.impl;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.ExcelFileReader;
import com.easycerti.eframe.common.util.XExcelFileReader;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.system_management.dao.CodeMngtDao;
import com.easycerti.eframe.psm.system_management.dao.MisdetectMngDao;
import com.easycerti.eframe.psm.system_management.dao.SystemMngtDao;
import com.easycerti.eframe.psm.system_management.service.MisdetectMngSvc;
import com.easycerti.eframe.psm.system_management.vo.Misdetect;
import com.easycerti.eframe.psm.system_management.vo.MisdetectList;
import com.easycerti.eframe.psm.system_management.vo.System;

@Service
public class MisdetectMngSvcImpl implements MisdetectMngSvc {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private MisdetectMngDao misdetectDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private SystemMngtDao systemMngtDao;

	@Autowired
	private CodeMngtDao codeMngtDao;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;
	
	@Override
	public DataModelAndView misdetectMngList(Map<String, String> parameters){
		parameters = CommonHelper.setPageParam(parameters, defaultPageSize);				
		SimpleCode simpleCode = new SimpleCode();
		Misdetect paramBean = CommonHelper.convertMapToBean(parameters, Misdetect.class);
		
		String index = "misdetect/list.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		List<Misdetect> misdetect = misdetectDao.misdetectMngList(paramBean);
		List<Misdetect> privList = misdetectDao.privacy_kind(paramBean);
		Integer totalCount = misdetectDao.misdetectMngList_count(paramBean);
		MisdetectList misdetectList = new MisdetectList(misdetect, totalCount.toString());
		DataModelAndView modelAndView = new DataModelAndView();
		
		
		//오탐일괄등록
	/*	List<Misdetect> topPrivListTmp = misdetectDao.topPrivList(paramBean);
		Integer topPrivListCount = misdetectDao.topPrivList_count();
		
		MisdetectList topPrivList = new MisdetectList(topPrivListTmp, topPrivListCount.toString());
		*/
		
		modelAndView.addObject("privList", privList);
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("misdetectList", misdetectList);
	//	modelAndView.addObject("topPrivList", topPrivList);
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView misdetectMngList_summary(Map<String, String> parameters, HttpServletRequest request){
		parameters = CommonHelper.setPageParam(parameters, defaultPageSize);				
		SimpleCode simpleCode = new SimpleCode();
		Misdetect paramBean = CommonHelper.convertMapToBean(parameters, Misdetect.class);
		
		System system = new System();
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list");
		system.setAuth_idsList(list);
		List<System> systemList = systemMngtDao.findSystemMngtListAuth(system);
		
		if(parameters.get("page_cur_num")==null) {
			parameters.put("page_cur_num","1");
			parameters.put("page_num","1");
		}else {
			parameters.put("page_num",parameters.get("page_cur_num"));
		}
		
		/*
		HashMap<String, String> systemMap = new HashMap<String, String>();
		systemMap.put("all", "전체시스템");
		for (System system2 : systemList) {
			systemMap.put(system2.getSystem_seq(), system2.getSystem_name());
		}
		modelAndView.addObject("systemMap", systemMap);
		*/
		HashMap<String, String> mistypeMap = new HashMap<String, String>();
		mistypeMap.put("1", "범위");
		mistypeMap.put("2", "문자포함");
		mistypeMap.put("3", "정규식");
		
		String index = "misdetect/list_summary.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		parameters.put("page_size", defaultPageSize);
		parameters.put("size", defaultPageSize);
		List<Misdetect> misdetectList = misdetectDao.findMisdetectSummryList(parameters);
		int misdetectListCount = misdetectDao.findMisdetectSummryListCount(parameters);
		
		parameters.put("total_count",String.valueOf(misdetectListCount));
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("misdetectList", misdetectList);
		modelAndView.addObject("systemList", systemList);
		modelAndView.addObject("mistypeMap", mistypeMap);
		modelAndView.addObject("parameters", parameters);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView misdetectMngDetail_summary(Map<String, String> parameters, HttpServletRequest request){
		parameters = CommonHelper.setPageParam(parameters, defaultPageSize);				
		SimpleCode simpleCode = new SimpleCode();
		Misdetect paramBean = CommonHelper.convertMapToBean(parameters, Misdetect.class);
		
		String index = "misdetect/detail_summary.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		
		System system = new System();
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list");
		system.setAuth_idsList(list);
		List<System> systemList = systemMngtDao.findSystemMngtListAuth(system);
		
		Misdetect misdetectDetail = null;
		if(parameters.get("log_seq")!=null && !parameters.get("log_seq").equals("") ) {
			try {
				misdetectDetail = misdetectDao.findMisdetectSummryDetail(parameters);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("misdetectDetail", misdetectDetail);
		modelAndView.addObject("paramBean", paramBean);
		modelAndView.addObject("systemList", systemList);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView removePrivacy(Map<String, String> parameters){
		Misdetect paramBean = CommonHelper.convertMapToBean(parameters, Misdetect.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			misdetectDao.deletePrivacy(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[misdetectSvcimpl.removeMisdetect] MESSAGE : " + e.getMessage());
			throw new ESException("SYS030J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView addPrivacy(Map<String, String> parameters){
		Misdetect paramBean = CommonHelper.convertMapToBean(parameters, Misdetect.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			misdetectDao.addPrivacy(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[misdetectSvcimpl.addMisdetect] MESSAGE : " + e.getMessage());
			throw new ESException("SYS030J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView addAllPrivacy(Map<String, String> parameters) {
		Misdetect paramBean = CommonHelper.convertMapToBean(parameters, Misdetect.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			misdetectDao.addPrivacy(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[misdetectSvcimpl.addMisdetect] MESSAGE : " + e.getMessage());
			throw new ESException("SYS030J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}

	@Override
	public void findfindIpMngtList_download(DataModelAndView modelAndView, Map<String, String> parameters,
			HttpServletRequest request) {
		
		
		SearchSearch search = new SearchSearch();
		Misdetect paramBean = CommonHelper.convertMapToBean(parameters, Misdetect.class);
		
		search.setUseExcel("true");
		
		String fileName = "PSM_예외처리DB_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
		String sheetName = "예외처리DB";
		
		List<Misdetect> Misdetect =  misdetectDao.misdetectMngList(paramBean);
		
		String[] columns = new String[] {"misdetect_id", "privacy_desc", "misdetect_pattern"  };
		String[] heads = new String[] {"No", "개인정보 유형", "예외 개인정보"};
		
		modelAndView.addObject("fileName", fileName);
		modelAndView.addObject("sheetName", sheetName);
		modelAndView.addObject("excelData", Misdetect);
		modelAndView.addObject("columns", columns);
		modelAndView.addObject("heads", heads);
		
	}

	@Override
	public String upLoadSystemMngtOne(Map<String, String> parameters, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String result="false";
		Misdetect paramBean = CommonHelper.convertMapToBean(parameters, Misdetect.class);
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		//엑셀 Row 카운트는 1000으로 지정한다		
		int rowCount=1000;
		
		//파일 업로드 기능  		
		MultipartHttpServletRequest mpr = (MultipartHttpServletRequest) request;  
		
		MultipartFile dir = mpr.getFile("file");
				
		File file = new File("/root/excelUpload/" + dir.getOriginalFilename());
		
		if (!file.exists()) { //폴더 없으면 폴더 생성
			file.mkdirs();
		}
		
		dir.transferTo(file);
		
		//업로드된 파일의 정보를 읽어들여 DB에 추가하는 기능 
		if (dir.getSize() > 0 ) { 
				
			List<String[]> readRows = new ArrayList<>();
			
			String ext = file.getName().substring(file.getName().lastIndexOf(".")+1);
			ext = ext.toLowerCase();
			if( "xlsx".equals(ext) ){	
				readRows = new XExcelFileReader(file.getAbsolutePath()).readRows(rowCount);
				
			}else if( "xls".equals(ext) ){	
				readRows = new ExcelFileReader(file.getAbsolutePath()).readRows(rowCount);
			}
		
			for(int i=0; i<3; i++){	
				readRows.remove(0);
			}
			
			boolean isCheck = true;
			if( readRows.size() != 0 ){
				
			/*int columnCount = readRows.get(0).length;*/
			//각 셀 내용을 DB 컬럼과 Mapping
			try {
				for(String[] readRowArr : readRows){
					if(readRowArr.length != 0 && readRowArr[1].length() != 0){
						paramBean.setPrivacy_type(readRowArr[0]);
						paramBean.setMisdetect_pattern(readRowArr[1]);
						misdetectDao.addPrivacy(paramBean);
					}
				}
				if(isCheck) {
					readRows.clear();
					transactionManager.commit(transactionStatus);
				}
			} catch(DataAccessException e) {
				transactionManager.rollback(transactionStatus);
				readRows.clear();
			/*	logger.error("[EmpUserMngtSvcImpl.upLoadEmpUserMngtOne] MESSAGE : " + e.getMessage());
				throw new ESException("SYS039J");*/
				return result;
			}
		}
			if(isCheck)
				result="true";
		}
		
		return result;	
	}

	@Override
	public String addMisdetectSummry(Map<String, String> parameters, HttpServletRequest request) {
		String result = "success";
		try {
			misdetectDao.addMisdetectSummry(parameters);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			result = "fail";
		}
		
		return result;
	}
}
