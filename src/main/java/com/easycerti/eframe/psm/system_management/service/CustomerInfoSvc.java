package com.easycerti.eframe.psm.system_management.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;

import com.easycerti.eframe.common.spring.DataModelAndView;

public interface CustomerInfoSvc {
	
	public DataModelAndView customerInfoList(Map<String, String> parameters);
	public DataModelAndView addFile(MultipartFile file, HttpServletRequest request);
	public DataModelAndView addFile2(MultipartFile file, HttpServletRequest request);
	public void useLogoImage(int idx, int type);
	public void useLogoImage2(int idx, int type);
	public void deleteLogoImage(int idx);
	
	public String addAuthorizeLine(Map<String, String> parameters);
	public String addDefaultDesc(Map<String, String> parameters);
	public String findDefaultDesc(Map<String, String> parameters);
}
