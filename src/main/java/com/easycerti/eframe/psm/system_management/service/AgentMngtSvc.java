package com.easycerti.eframe.psm.system_management.service;

import java.util.Map;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.system_management.vo.AgentMaster;
import com.easycerti.eframe.psm.system_management.vo.AgentMasterSearch;

/**
 * 에이전트 관련 Service Interface
 * 
 * @author yjyoo
 * @since 2015. 4. 24.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 24.           yjyoo            최초 생성
 *
 * </pre>
 */
public interface AgentMngtSvc {

	/**
	 * 에이전트 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 24.
	 * @return DataModelAndView
	 */
	public DataModelAndView findAgentMngtList(AgentMasterSearch search);
	public DataModelAndView findAgentMngtDetail(AgentMaster search);
	public DataModelAndView addAgent(AgentMaster search);
	public DataModelAndView removeAgent(AgentMaster search);
	public DataModelAndView saveAgent(AgentMaster search);
	
	/**
	 * 에이전트 동작/정지 컨트롤
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 29.
	 * @return DataModelAndView
	 */
	public DataModelAndView actionAgent(Map<String, String> parameters);
	
	public DataModelAndView setLicenCe_Hist(Map<String, String> parameters);
	
	
}
