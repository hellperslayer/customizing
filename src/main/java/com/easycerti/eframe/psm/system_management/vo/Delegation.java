package com.easycerti.eframe.psm.system_management.vo;

import com.easycerti.eframe.common.vo.AbstractValueObject;

public class Delegation extends AbstractValueObject {
	
	/** 20180314 소명판정 결제자 위힘 **/
	private String approval_id;
	private String approval_name;
	private String delegation_id;
	private String delegation_name;
	private String delegation_date;
	private String delegation_date_to;
	private String delegation_date_from;
	private int delegation_seq;
	private String admin_user_id;
	private String cancel_yn;
	private String dept_id;
	
	
	public String getDept_id() {
		return dept_id;
	}
	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}
	public String getCancel_yn() {
		return cancel_yn;
	}
	public void setCancel_yn(String cancel_yn) {
		this.cancel_yn = cancel_yn;
	}
	public int getDelegation_seq() {
		return delegation_seq;
	}
	public void setDelegation_seq(int delegation_seq) {
		this.delegation_seq = delegation_seq;
	}
	public String getDelegation_date_to() {
		return delegation_date_to;
	}
	public void setDelegation_date_to(String delegation_date_to) {
		this.delegation_date_to = delegation_date_to;
	}
	public String getDelegation_date_from() {
		return delegation_date_from;
	}
	public void setDelegation_date_from(String delegation_date_from) {
		this.delegation_date_from = delegation_date_from;
	}
	public String getApproval_id() {
		return approval_id;
	}
	public void setApproval_id(String approval_id) {
		this.approval_id = approval_id;
	}
	public String getApproval_name() {
		return approval_name;
	}
	public void setApproval_name(String approval_name) {
		this.approval_name = approval_name;
	}
	public String getDelegation_id() {
		return delegation_id;
	}
	public void setDelegation_id(String delegation_id) {
		this.delegation_id = delegation_id;
	}
	public String getDelegation_name() {
		return delegation_name;
	}
	public void setDelegation_name(String delegation_name) {
		this.delegation_name = delegation_name;
	}
	public String getDelegation_date() {
		return delegation_date;
	}
	public void setDelegation_date(String delegation_date) {
		this.delegation_date = delegation_date;
	}
	public String getAdmin_user_id() {
		return admin_user_id;
	}
	public void setAdmin_user_id(String admin_user_id) {
		this.admin_user_id = admin_user_id;
	}
	

}
