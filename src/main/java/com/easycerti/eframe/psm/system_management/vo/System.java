package com.easycerti.eframe.psm.system_management.vo;

import java.io.Serializable;
import java.util.List;

public class System implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//시스템 번호
	private String system_seq;
	//시스템 이름
	private String system_name;
	//시스템 타입
	private String system_type;
	//시스템 설명
	private String description;
	//메인메뉴
	private String main_menu_id;
	//서브메뉴
	private String sub_menu_id;
	
	private String main_url;
	
	List<String> auth_idsList;
	
	private String administrator;
	
	private String use_flag;
	private String privacy_masking;
	private String privacy_type;
	private String privacy_desc;
	private String agent_type;
	private String collection_server_ip;
	private String analysis_server_ip;
	
	private String existence_yn;
	private Integer information_ct;
	private int sort_order;
	
	private String network_type;
	
	private String threshold;
	
	public String getThreshold() {
		return threshold;
	}
	public void setThreshold(String threshold) {
		this.threshold = threshold;
	}
	public String getNetwork_type() {
		return network_type;
	}
	public void setNetwork_type(String network_type) {
		this.network_type = network_type;
	}
	public int getSort_order() {
		return sort_order;
	}
	public void setSort_order(int sort_order) {
		this.sort_order = sort_order;
	}
	public String getPrivacy_desc() {
		return privacy_desc;
	}
	public void setPrivacy_desc(String privacy_desc) {
		this.privacy_desc = privacy_desc;
	}
	public String getExistence_yn() {
		return existence_yn;
	}
	public void setExistence_yn(String existence_yn) {
		this.existence_yn = existence_yn;
	}
	public Integer getInformation_ct() {
		return information_ct;
	}
	public void setInformation_ct(Integer information_ct) {
		this.information_ct = information_ct;
	}
	public String getAgent_type() {
		return agent_type;
	}
	public void setAgent_type(String agent_type) {
		this.agent_type = agent_type;
	}
	public String getCollection_server_ip() {
		return collection_server_ip;
	}
	public void setCollection_server_ip(String collection_server_ip) {
		this.collection_server_ip = collection_server_ip;
	}
	public String getAnalysis_server_ip() {
		return analysis_server_ip;
	}
	public void setAnalysis_server_ip(String analysis_server_ip) {
		this.analysis_server_ip = analysis_server_ip;
	}
	public String getUse_flag() {
		return use_flag;
	}
	public void setUse_flag(String use_flag) {
		this.use_flag = use_flag;
	}
	public String getPrivacy_masking() {
		return privacy_masking;
	}
	public void setPrivacy_masking(String privacy_masking) {
		this.privacy_masking = privacy_masking;
	}
	public String getPrivacy_type() {
		return privacy_type;
	}
	public void setPrivacy_type(String privacy_type) {
		this.privacy_type = privacy_type;
	}
	public List<String> getAuth_idsList() {
		return auth_idsList;
	}
	public void setAuth_idsList(List<String> auth_idsList) {
		this.auth_idsList = auth_idsList;
	}
	public String getMain_url() {
		return main_url;
	}
	public void setMain_url(String main_url) {
		this.main_url = main_url;
	}
	public String getSub_menu_id() {
		return sub_menu_id;
	}
	public void setSub_menu_id(String sub_menu_id) {
		this.sub_menu_id = sub_menu_id;
	}
	public String getMain_menu_id() {
		return main_menu_id;
	}
	public void setMain_menu_id(String main_menu_id) {
		this.main_menu_id = main_menu_id;
	}
	public String getSystem_seq() {
		return system_seq;
	}
	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}
	public String getSystem_name() {
		return system_name;
	}
	public void setSystem_name(String system_name) {
		this.system_name = system_name;
	}
	public String getSystem_type() {
		return system_type;
	}
	public void setSystem_type(String system_type) {
		this.system_type = system_type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAdministrator() {
		return administrator;
	}
	public void setAdministrator(String administrator) {
		this.administrator = administrator;
	}
}
