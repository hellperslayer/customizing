package com.easycerti.eframe.psm.system_management.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.StringUtils;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.system_management.dao.AdminUserMngtDao;
import com.easycerti.eframe.psm.system_management.dao.EmpUserMngtDao;
import com.easycerti.eframe.psm.system_management.dao.IpMngtDao;
import com.easycerti.eframe.psm.system_management.service.IpMngtSvc;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;
import com.easycerti.eframe.psm.system_management.vo.EmpUser;
import com.easycerti.eframe.psm.system_management.vo.Ip;

/**
 * 부서 관련 Service Implements
 * 
 * @author yjyoo
 * @since 2015. 4. 17.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2016. 4. 18.           ehchoi            최초 생성
 *
 * </pre>
 */
@Service
public class IpMngSvcImpl implements IpMngtSvc {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private IpMngtDao ipMngtDao;
	
	@Autowired
	private AdminUserMngtDao adminUserMngtDao;
	
	@Autowired
	private EmpUserMngtDao empUserMngtDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	@Override
	public DataModelAndView findIpMngtList(Map<String, String> parameters) {
		Ip paramBean = CommonHelper.convertMapToBean(parameters, Ip.class);
		
		List<Ip> ips = ipMngtDao.findIpMngtList(paramBean);
		
		SimpleCode simpleCode = new SimpleCode("/ipMngt/list.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("hierarchyDepartments", ips);
		modelAndView.addObject("paramBean", paramBean);
		modelAndView.addObject("index_id", index_id);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView findIpMngtOne(Map<String, String> parameters) {
		Ip paramBean = CommonHelper.convertMapToBean(parameters, Ip.class);
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		if(StringUtils.notNullCheck(paramBean.getDept_id())){
			String rootDept_id = null;
			if("ROOT".equals(paramBean.getDept_id())) {
				rootDept_id = ipMngtDao.findIpMngtOne_root();
				paramBean.setDept_id(rootDept_id);
			}
			
			Ip detailBean = ipMngtDao.findIpMngtOne(paramBean);
			modelAndView.addObject("ipDetail", detailBean);
		}
		
		modelAndView.addObject("paramBean",paramBean);
		return modelAndView;
	}

	@Override
	public DataModelAndView addIpMngtOne(Map<String, String> parameters) {
		Ip paramBean = CommonHelper.convertMapToBean(parameters, Ip.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			ipMngtDao.addIpMngtOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[IpMngtSvcImpl.addIpMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS033J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView saveIpMngtOne(Map<String, String> parameters) {
		Ip paramBean = CommonHelper.convertMapToBean(parameters, Ip.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			ipMngtDao.saveIpMngtOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[IpMngtSvcImpl.saveIpMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS033J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
/*	public int saveIpMngtOne(Ip ip) throws Exception {
		return ipMngtDao.saveIpMngtOne(ip);
	}*/

	@Override
	public DataModelAndView removeIpMngtOne(Map<String, String> parameters) {
		Ip paramBean = CommonHelper.convertMapToBean(parameters, Ip.class);
		
		Ip ipParamBean = new Ip();
		ipParamBean.setParent_dept_id(paramBean.getDept_id());
		// 하위 부서 존재 시 삭제할 수 없음
		int childDeptCount = ipMngtDao.findIpMngtOne_count(ipParamBean);
		if(childDeptCount > 0) {
			throw new ESException("SYS053J");
		}
		
		EmpUser empUserParamBean = new EmpUser();
		empUserParamBean.setDept_id(paramBean.getDept_id());
		// 하위 사용자 존재 시 삭제할 수 없음
		int userCount = empUserMngtDao.findEmpUserMngtOne_countToUseDept(empUserParamBean);
		if(userCount > 0) {
			throw new ESException("SYS051J");
		}
		
		AdminUser adminUserParamBean = new AdminUser();
		adminUserParamBean.setDept_id(paramBean.getDept_id());
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			// 관리자 부서 초기화
			adminUserMngtDao.saveAdminUserMngtOne_toRemoveDept(adminUserParamBean);
			// 부서 삭제
			ipMngtDao.removeIpMngtOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[IpMngtSvcImpl.removeIpMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS035J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
}
