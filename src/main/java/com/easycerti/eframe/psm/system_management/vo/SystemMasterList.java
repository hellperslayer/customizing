package com.easycerti.eframe.psm.system_management.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 대상시스템리스트 VO
 * 
 * @author yjyoo
 * @since 2015. 4. 24.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 24.           yjyoo            최초 생성
 *
 * </pre>
 */
public class SystemMasterList extends AbstractValueObject {
	
	// 시스템 리스트
	private List<SystemMaster> systemMasters = null;
	
	public SystemMasterList() {}
	
	public SystemMasterList(List<SystemMaster> systemMasters){
		this.systemMasters = systemMasters;
	}
	
	public SystemMasterList(List<SystemMaster> systemMasters, String count){
		this.systemMasters = systemMasters;
		setPage_total_count(count);
	}

	public List<SystemMaster> getSystemMasters() {
		return systemMasters;
	}

	public void setSystemMasters(List<SystemMaster> systemMasters) {
		this.systemMasters = systemMasters;
	}
	
	
}
