package com.easycerti.eframe.psm.system_management.service;

import java.util.Map;

import org.springframework.web.servlet.ModelAndView;

/**
 * 메뉴권한 관리 Service Interface
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 *   
 *
 * </pre>
 */
public interface MenuAuthMngtSvc {

	public ModelAndView fintMenuAuthMngtList(Map<String, String> parameters);
	
	public ModelAndView fintMenuAuthMngtOne(Map<String, String> parameters);
	
	public ModelAndView addMenuAuthMngtOne(Map<String, String> parameters);
	
	public ModelAndView saveMenuAuthMngtOne(Map<String, String> parameters);
	
	public ModelAndView removeMenuAuthMngtOne(Map<String, String> parameters);
}
