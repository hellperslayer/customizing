package com.easycerti.eframe.psm.system_management.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 메뉴 VO
 * 
 * @author yjyoo
 * @since 2015. 4. 20.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 20.           yjyoo            최초 생성
 *
 * </pre>
 */
public class Menu extends AbstractValueObject {
	
	// 메뉴 ID
	private String menu_id = null;
	
	// 상위메뉴 ID
	private String parent_menu_id = null;
	
	// 메뉴 명
	private String menu_name = null;
	
	// 메뉴 URL
	private String menu_url = null;
	
	// 메뉴 설명
	private String description = null;
	
	// 메뉴 깊이
	private Integer menu_depth = null;
	
	// 정렬순서
	private Integer sort_order = null;
	
	// 사용여부
	private Character use_flag = null;
	
	/**
	 * Etc
	 */
	
	// 권한 ID
	private String auth_id = null;
	
	//system 넘버 
	private String system_seq = null;
	
	// 메뉴 수정시 권한목록 (',' 를 구분자로 한 String)
	private String auth_ids = null;
	
	// 전체 메뉴명 (select box 사용)
	private String full_menu_name = null;
	
	// 메뉴명 간략화 (select box 사용)
	private String simple_menu_name = null;
	
	// 등록 일시 (String type)
	private String insert_datetime_string = null;
	
	// 수정 일시 (String type)
	private String update_datetime_string = null;
	
	// 각 메뉴의 해당 권한 리스트
	private List<MenuAuth> menuAuths = null;
	
	// 하위메뉴 일괄 변경을 위한 키값
	private String sub_batch = null;
	
	public Menu(){};
	
	public String getSub_batch() {
		return sub_batch;
	}

	public void setSub_batch(String sub_batch) {
		this.sub_batch = sub_batch;
	}

	public Menu(String menu_id){
		this.menu_id = menu_id;
	}

	public String getMenu_id() {
		return menu_id;
	}

	public void setMenu_id(String menu_id) {
		this.menu_id = menu_id;
	}

	public String getParent_menu_id() {
		return parent_menu_id;
	}

	public void setParent_menu_id(String parent_menu_id) {
		this.parent_menu_id = parent_menu_id;
	}

	public String getMenu_name() {
		return menu_name;
	}

	public void setMenu_name(String menu_name) {
		this.menu_name = menu_name;
	}

	public String getMenu_url() {
		return menu_url;
	}

	public void setMenu_url(String menu_url) {
		this.menu_url = menu_url;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getMenu_depth() {
		return menu_depth;
	}

	public void setMenu_depth(Integer menu_depth) {
		this.menu_depth = menu_depth;
	}

	public Integer getSort_order() {
		return sort_order;
	}

	public void setSort_order(Integer sort_order) {
		this.sort_order = sort_order;
	}

	public Character getUse_flag() {
		return use_flag;
	}

	public void setUse_flag(Character use_flag) {
		this.use_flag = use_flag;
	}

	public String getAuth_id() {
		return auth_id;
	}

	public void setAuth_id(String auth_id) {
		this.auth_id = auth_id;
	}

	public String getFull_menu_name() {
		return full_menu_name;
	}

	public void setFull_menu_name(String full_menu_name) {
		this.full_menu_name = full_menu_name;
	}

	public String getSimple_menu_name() {
		return simple_menu_name;
	}

	public void setSimple_menu_name(String simple_menu_name) {
		this.simple_menu_name = simple_menu_name;
	}

	public String getInsert_datetime_string() {
		return insert_datetime_string;
	}

	public void setInsert_datetime_string(String insert_datetime_string) {
		this.insert_datetime_string = insert_datetime_string;
	}

	public String getUpdate_datetime_string() {
		return update_datetime_string;
	}

	public void setUpdate_datetime_string(String update_datetime_string) {
		this.update_datetime_string = update_datetime_string;
	}

	public List<MenuAuth> getMenuAuths() {
		return menuAuths;
	}

	public void setMenuAuths(List<MenuAuth> menuAuths) {
		this.menuAuths = menuAuths;
	}

	public String getAuth_ids() {
		return auth_ids;
	}

	public void setAuth_ids(String auth_ids) {
		this.auth_ids = auth_ids;
	}
	public String getSystem_seq() {
		return system_seq;
	}

	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}


	// toString
	@Override
	public String toString() {
		return "Menu [menu_id=" + menu_id + ", parent_menu_id="
				+ parent_menu_id + ", menu_name=" + menu_name + ", menu_url="
				+ menu_url + ", description=" + description + ", menu_depth="
				+ menu_depth + ", sort_order=" + sort_order + ", use_flag="
				+ use_flag + ", auth_id=" + auth_id + ", auth_ids=" + auth_ids
				+ ", full_menu_name=" + full_menu_name + ", simple_menu_name="
				+ simple_menu_name + ", insert_datetime_string="
				+ insert_datetime_string + ", update_datetime_string="
				+ update_datetime_string + ", menuAuths=" + menuAuths + ", system_seq =" + system_seq + "]";
	}
}
