package com.easycerti.eframe.psm.system_management.dao;

import java.util.List;

import com.easycerti.eframe.psm.control.vo.GroupCode;

/**
 * 그룹코드 관련 Dao Interface
 * 
 * @author yjyoo
 * @since 2015. 4. 17.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 17.           yjyoo            최초 생성
 *
 * </pre>
 */
public interface GroupCodeMngtDao {

	/**
	 * 그룹코드 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return List<GroupCode>
	 */
	public List<GroupCode> findGroupCodeMngtList(GroupCode groupCode);
	
	/**
	 * 그룹코드 리스트 갯수
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return Integer
	 */
	public Integer findGroupCodeMngtOne_count(GroupCode groupCode);
	
	/**
	 * 그룹코드 상세정보
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return GroupCode
	 */
	public GroupCode findGroupCodeMngtOne(GroupCode groupCode);
	
	/**
	 * 그룹코드 추가 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return void
	 */
	public void addGroupCodeMngtOne(GroupCode groupCode);
	
	/**
	 * 그룹코드 수정 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return void
	 */
	public void saveGroupCodeMngtOne(GroupCode groupCode);
	
	/**
	 * 그룹코드 삭제 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return void
	 */
	public void removeGroupCodeMngtOne(GroupCode groupCode);
	
}
