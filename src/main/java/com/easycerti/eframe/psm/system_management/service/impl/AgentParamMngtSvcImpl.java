package com.easycerti.eframe.psm.system_management.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.system_management.dao.AgentParamMngtDao;
import com.easycerti.eframe.psm.system_management.service.AgentParamMngtSvc;
import com.easycerti.eframe.psm.system_management.vo.AgentParam;

/**
 * 관리자 관련 Service Implements
 * 
 * @author yjyoo
 * @since 2015. 4. 20.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 20.           yjyoo            최초 생성
 *
 * </pre>
 */
@Service
public class AgentParamMngtSvcImpl implements AgentParamMngtSvc {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private AgentParamMngtDao agentParamMngtDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;

	@Override
	public DataModelAndView findAgentParamMngtList(Map<String, String> parameters) {
		AgentParam paramBean = CommonHelper.convertMapToBean(parameters, AgentParam.class);
		List<AgentParam> agentParamList = agentParamMngtDao.findAgentParamMngtList();
		
		SimpleCode simpleCode = new SimpleCode("/agentParamMngt/list.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("agentParamList", agentParamList);
		modelAndView.addObject("index_id", index_id);

		return modelAndView;
	}
	
	@Override
	public DataModelAndView findAgentParamMngtOne(Map<String, String> parameters) {
		AgentParam paramBean = CommonHelper.convertMapToBean(parameters, AgentParam.class);
		DataModelAndView modelAndView = new DataModelAndView();

		List<AgentParam> agentParamDetail = agentParamMngtDao.findAgentParamMngtOne(paramBean);
		
		int cnt = 0;
		String seqArray = null;
		for(AgentParam a : agentParamDetail) {
			cnt++;
			if(cnt == 1) {
				seqArray = String.valueOf(a.getAgent_param_seq());
			} else if(cnt == agentParamDetail.size()) {
				seqArray = seqArray +","+ a.getAgent_param_seq();
			} else {
				seqArray = seqArray +","+ a.getAgent_param_seq();
			}
		}
		modelAndView.addObject("seqArray", seqArray);
		modelAndView.addObject("seqLength", cnt);
		modelAndView.addObject("agentParamDetail", agentParamDetail);
			
		SimpleCode simpleCode = new SimpleCode("/agentParamMngt/detail.html");
		String index_id = commonDao.checkIndex(simpleCode);
		
		modelAndView.addObject("index_id",index_id);
		
		return modelAndView;
	}
	/*
	@Override
	public DataModelAndView addAgentParamMngtOne(Map<String, String> parameters) {
		AdminUser paramBean = CommonHelper.convertMapToBean(parameters, AdminUser.class);
		String ui_type = commonDao.getUiType();
		
		// Validation Check (admin_user_id 중복)
		AdminUser adminUser = agentParamMngtDao.findAgentParamMngtOne(paramBean.getAdmin_user_id());
		if(adminUser != null) {
			throw new ESException("SYS013J");
		}
		
		if(ui_type.equals("G") || ui_type.equals("518a")) {
			SecureRandom saltValue = null;
			String saltStr = "";
			try {
				saltValue = SecureRandom.getInstance("SHA1PRNG");
				for(int i = 0; i < 10; i++) {
					saltStr += saltValue.nextInt(10);
				}
			} catch (NoSuchAlgorithmException e) {
				throw new ESException("SYS012J");
			}
			paramBean.setPassword(paramBean.getAdmin_user_id()+saltStr);
			paramBean.setSalt_value(saltStr);
		} else {
			paramBean.setPassword(paramBean.getAdmin_user_id());
			paramBean.setSalt_value("a");
		}
		
		// 초기비밀번호 세팅
		//paramBean.setPassword(init_password);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			agentParamMngtDao.addAgentParamMngtOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			logger.error("[AgentParamMngtSvcImpl.addAgentParamMngtOne] MESSAGE : " + e.getMessage());
			transactionManager.rollback(transactionStatus);
			throw new ESException("SYS012J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	*/
	@Override
	public DataModelAndView saveAgentParamMngtOne(Map<String, String> parameters) {
		AgentParam paramBean = CommonHelper.convertMapToBean(parameters, AgentParam.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			if(paramBean.getSeqLength()==1) {
				String seqArray = paramBean.getSeqArray();
				String valueArray = paramBean.getValueArray();
				
				AgentParam ap = new AgentParam();
				ap.setAgent_param_seq(Integer.parseInt(seqArray));
				ap.setParam_value(valueArray);
				
				agentParamMngtDao.saveAgentParamMngtOne(ap);
			} else {
				String[] seqArray = paramBean.getSeqArray().split(",");
				String[] valueArray = paramBean.getValueArray().split("///");
				
				for(int a = 0; a < seqArray.length; a++) {
					AgentParam ap = new AgentParam();
					ap.setAgent_param_seq(Integer.parseInt(seqArray[a]));
					ap.setParam_value(valueArray[a]);
					
					agentParamMngtDao.saveAgentParamMngtOne(ap);
				}
			}
			
			
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[AgentParamMngtSvcImpl.saveAgentParamMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS014J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	/*
	@Override
	public DataModelAndView removeAgentParamMngtOne(Map<String, String> parameters) {
		AdminUser paramBean = CommonHelper.convertMapToBean(parameters, AdminUser.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			agentParamMngtDao.removeAgentParamMngtOne(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[AgentParamMngtSvcImpl.removeAgentParamMngtOne] MESSAGE : " + e.getMessage());
			throw new ESException("SYS015J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}

	@Override
	public DataModelAndView rePassword(Map<String, String> parameters, HttpServletRequest request) throws Exception {
		AdminUser paramBean = CommonHelper.convertMapToBean(parameters, AdminUser.class);
		AdminUser adminUser = agentParamMngtDao.findAgentParamMngtOne_forLogin(paramBean);
		String ui_type = commonDao.getUiType();
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		if(adminUser != null){
			if(ui_type.equals("G") || ui_type.equals("518a")) {
				adminUser.setNewPassword(parameters.get("newPasswd")+adminUser.getSalt_value());
			} else {
				adminUser.setNewPassword(parameters.get("newPasswd"));
			}
			
			try {
				agentParamMngtDao.saveAgentParamMngtRePassword(adminUser);
				//System.out.println("#######################" + paramBean);
				
				transactionManager.commit(transactionStatus);
				
			} catch(DataAccessException e) {
				transactionManager.rollback(transactionStatus);
				logger.error("[AgentParamMngtSvcImpl.saveRePassword] MESSAGE : " + e.getMessage());
				throw new ESException("SYS014J");
			}
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView changePassword(Map<String, String> parameters, HttpServletRequest request)
			throws Exception {
		
		AdminUser paramBean = CommonHelper.convertMapToBean(parameters, AdminUser.class);
		AdminUser adminInfo = SystemHelper.getAdminUserInfo(request);
		paramBean.setAdmin_user_id(adminInfo.getAdmin_user_id());
		String ui_type = commonDao.getUiType();
		String res = "";
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		AdminUser adminUser = agentParamMngtDao.findAgentParamMngtOne_forLogin(paramBean);
		if(adminUser != null){
			if(ui_type.equals("G") || ui_type.equals("518a")) {
				adminUser.setNewPassword(parameters.get("newPasswd")+adminUser.getSalt_value());
			} else {
				adminUser.setNewPassword(parameters.get("newPasswd"));
			}
			
			try {
				agentParamMngtDao.saveAgentParamMngtRePassword(adminUser);
				transactionManager.commit(transactionStatus);
				res = "SUCCESS";
				
			} catch(DataAccessException e) {
				transactionManager.rollback(transactionStatus);
				logger.error("[AgentParamMngtSvcImpl.saveRePassword] MESSAGE : " + e.getMessage());
				res = "ERROR";
				throw new ESException("SYS014J");
			}
		} else {
			res = "WRONG_PWD";
			transactionManager.rollback(transactionStatus);
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		modelAndView.addObject("res", res);
		return modelAndView;
	}
	
	@Override
	public DataModelAndView checkTime(){
				
		  Calendar calendar = Calendar.getInstance();
		  String limitTime = agentParamMngtDao.checktime();
		  SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		  String nowTime = dateFormat.format(calendar.getTime());
		  DataModelAndView modelAndView = new DataModelAndView();
		  modelAndView.addObject("nowTime", nowTime);
		  modelAndView.addObject("limitTime", limitTime);
		
		return modelAndView;
	}

	@Override
	public List<SystemMaster> getSystemMaster() {
		return agentParamMngtDao.getSystemMaster();
	}
	
*/

}
