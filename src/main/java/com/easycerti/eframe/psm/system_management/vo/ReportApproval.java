package com.easycerti.eframe.psm.system_management.vo;

public class ReportApproval {
	
	private String system_seq;
	private String system_name;
	private String html_source;
	private String first_auth;
	private String second_auth;
	private String third_auth;
	private String count;
	private String name1;
	private String name2;
	private String name3;
	
	public String getSystem_seq() {
		return system_seq;
	}
	
	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}
	
	public String getSystem_name() {
		return system_name;
	}
	
	public void setSystem_name(String system_name) {
		this.system_name = system_name;
	}
	
	public String getFirst_auth() {
		return first_auth;
	}
	
	public void setFirst_auth(String first_auth) {
		this.first_auth = first_auth;
	}
	
	public String getSecond_auth() {
		return second_auth;
	}
	
	public void setSecond_auth(String second_auth) {
		this.second_auth = second_auth;
	}
	
	public String getThird_auth() {
		return third_auth;
	}
	
	public void setThird_auth(String third_auth) {
		this.third_auth = third_auth;
	}

	public String getHtml_source() {
		return html_source;
	}

	public void setHtml_source(String html_source) {
		this.html_source = html_source;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public String getName1() {
		return name1;
	}

	public void setName1(String name1) {
		this.name1 = name1;
	}

	public String getName2() {
		return name2;
	}

	public void setName2(String name2) {
		this.name2 = name2;
	}

	public String getName3() {
		return name3;
	}

	public void setName3(String name3) {
		this.name3 = name3;
	}
	
	
	
	
}
