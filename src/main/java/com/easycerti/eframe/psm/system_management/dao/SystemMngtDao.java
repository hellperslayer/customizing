package com.easycerti.eframe.psm.system_management.dao;

import java.util.List;
import java.util.Map;

import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.setup.vo.IndvinfoTypeSetup;
import com.easycerti.eframe.psm.system_management.vo.System;

/**
 * 부서 관리 Dao Interface
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일                           수정자                           수정내용
 * ------------        -------------     ----------------------
 * 2016. 4. 18.           ehchoi                 최초 생성
 *
 * </pre>
 */
public interface SystemMngtDao {
	
	public List<System> findSystemMngtList(System system);
	
	public List<System> findSystemMngtList_choose(System system);
	
	public Integer findSystemMngtOne_count(System system);
	
	public System findSystemMngtOne(System system);
	
	public String findSystemMngtOne_root();
	
	public void addSystemMngtOne(System system);
	public void addSystemApprovalOne(System system);
	
	public void saveSystemMngtOne(System system);
	public void saveSystemApprovalOne(System system);
	
	public void removeSystemMngtOne(System system);
	public void removeSystemApprovalOne(System system);
	
	public List<System> findSystemMngtListAuth(System systemBean);
	public List<System> findSystemMngtListAuth_Onnara(System systemBean);
	public List<System> findSystemMngtListAuth_Dbac(System systemBean);

	public void removeSystemMngtOneUse(System paramBean);

	public void addSystemMngtOneUse(System paramBean);
	
	public List<String> findSystemCodesByOnnara(List<String> list);
	public List<String> findSystemCodesByNormal(List<String> list);
	
	public List<IndvinfoTypeSetup> findReqTypeList();
	public void addRegularExpressionMngUse(System system);
	
	public System getSystemSeqBySystemName(String system_name);
	public List<Map<String, String>> findAgentTypeList();
	
	public List<System> findSystemIdentificationList(System system);
	public void saveSystemIdentificationInfo(System system);
	
}
