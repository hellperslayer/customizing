package com.easycerti.eframe.psm.system_management.web;

import java.io.File;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.system_management.service.MisdetectMngSvc;


@Controller
@RequestMapping(value="/misdetect/*")
public class MisdetectMngCtrl {
	
	@Autowired
	private MisdetectMngSvc misdetectSvc;
	
	@MngtActHist(log_action = "SELECT", log_message = "[정책관리] 예외처리DB(실시간)")
	@RequestMapping(value="list.html",method={RequestMethod.POST})
	public DataModelAndView findIpMngtList(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		DataModelAndView modelAndView = misdetectSvc.misdetectMngList(parameters);
		
		modelAndView.addObject("paramBean", parameters);
		
		modelAndView.setViewName("misdetectionMngList");
		
		return modelAndView;
	}
	
	@RequestMapping(value="list_summary.html",method={RequestMethod.POST})
	public DataModelAndView list_summary(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		DataModelAndView modelAndView = misdetectSvc.misdetectMngList_summary(parameters,request);
		
		modelAndView.addObject("paramBean", parameters);
		
		modelAndView.setViewName("misdetectionMngList_summary");
		
		return modelAndView;
	}
	
	@RequestMapping(value="detail_summary.html",method={RequestMethod.POST})
	public DataModelAndView detail_summary(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		DataModelAndView modelAndView = misdetectSvc.misdetectMngDetail_summary(parameters,request);
		
		modelAndView.addObject("paramBean", parameters);
		
		modelAndView.setViewName("misdetectionMngDetail_summary");
		
		return modelAndView;
	}
	
	/*개인정보 제외 삭제*/
	@RequestMapping(value="remove.html",method={RequestMethod.POST})
	public DataModelAndView removePriv(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{

		DataModelAndView modelAndView = misdetectSvc.removePrivacy(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	/*개인정보 제외 */
	@RequestMapping(value="add.html",method={RequestMethod.POST})
	public DataModelAndView addPriv(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{

		DataModelAndView modelAndView = misdetectSvc.addPrivacy(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	/*개인정보 전체 제외 추가*/
	@RequestMapping(value="add_all.html",method={RequestMethod.POST})
	public DataModelAndView addAllPriv(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{

		DataModelAndView modelAndView = misdetectSvc.addAllPrivacy(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@RequestMapping(value="download.html", method={RequestMethod.POST})
	public DataModelAndView addfindIpMngtList_download(@ModelAttribute("search") SearchSearch search,
			@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = new DataModelAndView(CommonResource.EXCEL_DOWNLOAD_VIEW);
		
		misdetectSvc.findfindIpMngtList_download(modelAndView, parameters, request);
		
		return modelAndView;
	}
	
	@RequestMapping("exdownload.html")
	public ModelAndView download(HttpServletRequest request)throws Exception{
		
		String path = request.getSession().getServletContext().getRealPath("/")+"WEB-INF/";
		 //System.out.println(path);
		File down = new File(path + "excelExam/예외처리DB_업로드양식.xlsx");
		return new ModelAndView("download","downloadFile",down);
	}
	
	@RequestMapping(value="upload.html", method={RequestMethod.POST})
	@ResponseBody
	public String upEmpUserMngtOne(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		parameters.put("insert_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		String result = misdetectSvc.upLoadSystemMngtOne(parameters,request, response);
		
		/*ModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("result", result);
		modelAndView.setViewName(CommonResource.JSON_VIEW);*/
		
		return result;
	}
	
	@RequestMapping(value="addMisdetectSummry.html", method={RequestMethod.POST})
	@ResponseBody
	public String addMisdetectSummry(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
		String result = misdetectSvc.addMisdetectSummry(parameters,request);
		return result;
	}

}
