package com.easycerti.eframe.psm.system_management.vo;


import java.sql.Timestamp;

import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 사원 VO
 * 
 * @author yjyoo
 * @since 2015. 4. 21.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 21.           yjyoo            최초 생성
 *
 * </pre>
 */
public class Talarm extends AbstractValueObject {
	

	// 사원 ID
	private String emp_user_id;
	
	// 직원 이름
	private String emp_user_name;
	
	// 이메일
	private String email_address;
	
	// 이메일
	private String email_address_up;
	
	// 전화번호
	private String mobile_number;
	
	// 전화번호
	private String mobile_number_up;
	
	// 사원 추가한 시간
	private Timestamp insert_datetime;
	
	// 사원 수정한 관리자 ID
	private String update_user_id;
	
	
	
/*	public CommonsMultipartFile getFile() {
		return file;
	}

	public void setFile(CommonsMultipartFile file) {
		this.file = file;
	}
	//파일 업로드
	private CommonsMultipartFile file;*/
	// 사원 수정한 시간
	private Timestamp update_datetime;
	
	// 하위 부서 검색
	private String deptLowSearchFlag;
	
	private String useyn_email;
	
	private String useyn_htel;
	
	private String email_subject;
	private String email_content;
	private String htel_content;

	public String getEmp_user_id() {
		return emp_user_id;
	}

	public void setEmp_user_id(String emp_user_id) {
		this.emp_user_id = emp_user_id;
	}

	public String getEmp_user_name() {
		return emp_user_name;
	}

	public void setEmp_user_name(String emp_user_name) {
		this.emp_user_name = emp_user_name;
	}

	public String getEmail_address() {
		return email_address;
	}

	public void setEmail_address(String email_address) {
		this.email_address = email_address;
	}

	public String getMobile_number() {
		return mobile_number;
	}

	public void setMobile_number(String mobile_number) {
		this.mobile_number = mobile_number;
	}
	
	public String getEmail_address_up() {
		return email_address_up;
	}

	public void setEmail_address_up(String email_address_up) {
		this.email_address_up = email_address_up;
	}

	public String getMobile_number_up() {
		return mobile_number_up;
	}

	public void setMobile_number_up(String mobile_number_up) {
		this.mobile_number_up = mobile_number_up;
	}


	public Timestamp getInsert_datetime() {
		return insert_datetime;
	}

	public void setInsert_datetime(Timestamp insert_datetime) {
		this.insert_datetime = insert_datetime;
	}

	public String getUpdate_user_id() {
		return update_user_id;
	}

	public void setUpdate_user_id(String update_user_id) {
		this.update_user_id = update_user_id;
	}

	public Timestamp getUpdate_datetime() {
		return update_datetime;
	}

	public void setUpdate_datetime(Timestamp update_datetime) {
		this.update_datetime = update_datetime;
	}
	
	public String getDeptLowSearchFlag() {
		return deptLowSearchFlag;
	}

	public void setDeptLowSearchFlag(String deptLowSearchFlag) {
		this.deptLowSearchFlag = deptLowSearchFlag;
	}
	
	public String getUseyn_email() {
		return useyn_email;
	}

	public void setUseyn_email(String useyn_email) {
		this.useyn_email = useyn_email;
	}
	public String getEmail_subject() {
		return email_subject;
	}

	public void setEmail_subject(String email_subject) {
		this.email_subject = email_subject;
	}
	
	public String getEmail_content() {
		return email_content;
	}

	public void setEmail_content(String email_content) {
		this.email_content = email_content;
	}
	
	public String getHtel_content() {
		return htel_content;
	}

	public void setHtel_content(String htel_content) {
		this.htel_content = htel_content;
	}
	
	public String getUseyn_htel() {
		return useyn_htel;
	}

	public void setUseyn_htel(String useyn_htel) {
		this.useyn_htel = useyn_htel;
	}
}
