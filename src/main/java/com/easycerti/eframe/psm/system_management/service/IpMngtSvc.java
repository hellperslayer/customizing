package com.easycerti.eframe.psm.system_management.service;

import java.util.Map;

import com.easycerti.eframe.common.spring.DataModelAndView;


/**
 * 부서 관련 Service Interface
 * 
 * @author yjyoo
 * @since 2015. 4. 17.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2016. 4. 18.           ehchoi            최초 생성
 *
 * </pre>
 */
public interface IpMngtSvc {

	/**
	 * 부서 리스트
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	public DataModelAndView findIpMngtList(Map<String, String> parameters);
	
	/**
	 * 부서 상세정보
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	public DataModelAndView findIpMngtOne(Map<String, String> parameters);
	
	/**
	 * 부서 추가 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	public DataModelAndView addIpMngtOne(Map<String, String> parameters);
	
	/**
	 * 부서 수정 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 * @throws Exception 
	 */
/*	public int saveIpMngtOne(Ip ip) throws Exception;*/
	public DataModelAndView saveIpMngtOne(Map<String, String> parameters);
	/**
	 * 부서 삭제 (단건)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	public DataModelAndView removeIpMngtOne(Map<String, String> parameters);
	
}
