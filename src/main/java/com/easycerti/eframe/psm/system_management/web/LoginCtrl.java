package com.easycerti.eframe.psm.system_management.web;

import java.io.File;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.util.Map;

import javax.crypto.Cipher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.psm.search.dao.AllLogInqDao;
import com.easycerti.eframe.psm.system_management.service.AdminUserMngtSvc;

/**
 * 로그인, 아웃 및 메인 관련 Controller
 * 
 * @author yjyoo
 * @since 2015. 5. 7.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 15.           yjyoo            최초 생성
 *
 * </pre>
 */
@Controller
public class LoginCtrl {
	
	@Autowired
	private AdminUserMngtSvc adminUserMngtSvc;
	
	@Autowired
	private AllLogInqDao allLogInqdao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Value("#{versionProperties.version}")
	private String version;
	 
	private static String RSA_WEB_KEY = "_RSA_WEB_Key_";	// 개인키 session key
	private static String RSA_INSTANCE = "RSA";				// rsa transformation
	/**
	 * 로그인 화면
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 15.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="loginView.html", method={RequestMethod.GET, RequestMethod.POST})
	public DataModelAndView loginView(HttpServletRequest request){
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView = adminUserMngtSvc.checkTime();
		
		// RSA 키 생성
		initRsa(request);
		
		HttpSession session = request.getSession();
		
		Integer chng_layout = (Integer) session.getAttribute("chng_layout");
		
		if(chng_layout == 0){
			modelAndView.setViewName("login_gun");
		}else{
			modelAndView.setViewName("login");
		}
		
		String rootPath = request.getSession().getServletContext().getRealPath("/");
		String attach_path = "resources/upload/";
		
		String filename = commonDao.selectCurrentImage_logo();
		String savePath = rootPath + attach_path + filename;
		
		File file = new File(savePath);
		
		if(file.exists())
			modelAndView.addObject("filename", filename);
		
		System.out.println("[WEB UI Version] : " + version);
		modelAndView.addObject("ui_version", version);
		
		return modelAndView;
	}
	
	
	/**
	 * 로그인 처리
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 15.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="login.html", method={RequestMethod.POST,RequestMethod.GET})
	public DataModelAndView login(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception {
		if(request.getSession().getAttribute("SSO_ID") != null) {
			parameters.put("sso_yn", "Y");
			parameters.put("admin_user_id", (String)request.getSession().getAttribute("SSO_ID"));
			parameters.put("password", (String)request.getSession().getAttribute("SSO_ID"));
		} else {
			String[] args = {"admin_user_id", "password"};
			
			if( !CommonHelper.checkExistenceToParameter(parameters, args)){
				throw new ESException("SYS004J");
			}
			
			HttpSession session = request.getSession();
	        PrivateKey privateKey = (PrivateKey) session.getAttribute(LoginCtrl.RSA_WEB_KEY);

			String admin_user_id = decryptRsa(privateKey, (String)parameters.get("admin_user_id"));
			String password = decryptRsa(privateKey, (String)parameters.get("password"));
			
			parameters.put("admin_user_id", admin_user_id);
			parameters.put("password", password);
		}
		
		
		DataModelAndView modelAndView = adminUserMngtSvc.login(parameters, request);
		
		if(request.getSession().getAttribute("SSO_ID") != null) {
			modelAndView.setViewName("sso/login_move");
		} else {
			modelAndView.setViewName(CommonResource.JSON_VIEW);
		}
		

		return modelAndView;
	}
	
	/**
	 * 로그아웃 처리
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 15.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="logout.html", method={RequestMethod.GET, RequestMethod.POST})
	public DataModelAndView logout(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		DataModelAndView modelAndView = adminUserMngtSvc.logout(parameters, request);
		RedirectView redirectView = new RedirectView("loginView.html");
		redirectView.setExposeModelAttributes(false);
		modelAndView.setView(redirectView);
		
		return modelAndView;
	}
	
	/**
	 * 메인화면 요청
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 15.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="main.html", method={RequestMethod.GET, RequestMethod.POST})
	public DataModelAndView main(){
		DataModelAndView modelAndView = new DataModelAndView("main");
		modelAndView.addObject("mainFlag", CommonResource.YES);
		
		return modelAndView;
	}
	
	/**
	 * sessionTimeout
	 * 
	 * @author tjlee
	 * @since 2015. 5. 08.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="sessionTimeout.html", method={RequestMethod.GET, RequestMethod.POST})
	public DataModelAndView sessionTimeout(){
		DataModelAndView modelAndView = new DataModelAndView("sessionTimeout");
		return modelAndView;
	}
	
	/**
	 * notAllowedIP
	 * 
	 * @author ehchoi
	 * @since 2016. 6. 16.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="notAllowedIP.html", method={RequestMethod.GET, RequestMethod.POST})
	public DataModelAndView notAllowedIP(){
		DataModelAndView modelAndView = new DataModelAndView("notAllowedIP");
		return modelAndView;
	}
	
	/**
	 * 설명 : duplicationLoginPage
	 * @author tjlee
	 * @since 2015. 5. 19.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="duplicationLoginPage.html", method={RequestMethod.GET, RequestMethod.POST})
	public DataModelAndView duplicationLoginPage(){
		DataModelAndView modelAndView = new DataModelAndView("duplicationLoginPage");
		return modelAndView;
	}
	
	@RequestMapping(value="notAuthPage.html", method={RequestMethod.GET, RequestMethod.POST})
	public DataModelAndView notAuthPage(){
		DataModelAndView modelAndView = new DataModelAndView("notAuthPage");
		return modelAndView;
	}
	
	/**
	 * 설명 : 비밀번호 변경
	 * @author tjlee
	 * @since 2015. 5. 19.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="rePassword.html", method={RequestMethod.POST})
	public DataModelAndView saveRePassword(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception {
		String[] args = {"admin_user_id", "currentPasswd", "newPasswd"};
		
		if( !CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		DataModelAndView modelAndView = adminUserMngtSvc.rePassword(parameters, request);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	@RequestMapping(value="changePassword.html", method={RequestMethod.POST})
	public DataModelAndView changeRePassword(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception {
		String[] args = {"currentPasswd", "newPasswd"};
		if( !CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SY0S04J");
		}
		
		DataModelAndView modelAndView = adminUserMngtSvc.changePassword(parameters, request);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	/**
     * 복호화
     * 
     * @param privateKey
     * @param securedValue
     * @return
     * @throws Exception
     */
    private String decryptRsa(PrivateKey privateKey, String securedValue) throws Exception {
        Cipher cipher = Cipher.getInstance(LoginCtrl.RSA_INSTANCE);
        byte[] encryptedBytes = hexToByteArray(securedValue);
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] decryptedBytes = cipher.doFinal(encryptedBytes);
        String decryptedValue = new String(decryptedBytes, "utf-8"); // 문자 인코딩 주의.
        return decryptedValue;
    }
 
    /**
     * 16진 문자열을 byte 배열로 변환한다.
     * 
     * @param hex
     * @return
     */
    public static byte[] hexToByteArray(String hex) {
        if (hex == null || hex.length() % 2 != 0) { return new byte[] {}; }
 
        byte[] bytes = new byte[hex.length() / 2];
        for (int i = 0; i < hex.length(); i += 2) {
            byte value = (byte) Integer.parseInt(hex.substring(i, i + 2), 16);
            bytes[(int) Math.floor(i / 2)] = value;
        }
        return bytes;
    }
 
    /**
     * rsa 공개키, 개인키 생성
     * 
     * @param request
     */
	public void initRsa(HttpServletRequest request) {
        HttpSession session = request.getSession();
 
        KeyPairGenerator generator;
        try {
            generator = KeyPairGenerator.getInstance(LoginCtrl.RSA_INSTANCE);
            generator.initialize(1024);
 
            KeyPair keyPair = generator.genKeyPair();
            KeyFactory keyFactory = KeyFactory.getInstance(LoginCtrl.RSA_INSTANCE);
            PublicKey publicKey = keyPair.getPublic();
            PrivateKey privateKey = keyPair.getPrivate();
 
            session.setAttribute(LoginCtrl.RSA_WEB_KEY, privateKey); // session에 RSA 개인키를 세션에 저장
 
            RSAPublicKeySpec publicSpec = (RSAPublicKeySpec) keyFactory.getKeySpec(publicKey, RSAPublicKeySpec.class);
            String publicKeyModulus = publicSpec.getModulus().toString(16);
            String publicKeyExponent = publicSpec.getPublicExponent().toString(16);
 
            request.setAttribute("RSAModulus", publicKeyModulus); // rsa modulus 를 request 에 추가
            request.setAttribute("RSAExponent", publicKeyExponent); // rsa exponent 를 request 에 추가
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
	
	
	
	@RequestMapping(value="ssologin.html", method={RequestMethod.GET, RequestMethod.POST})
	public DataModelAndView ssologin(HttpServletRequest request){
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView = adminUserMngtSvc.checkTime();
		/*
		// RSA 키 생성
		initRsa(request);
		
		HttpSession session = request.getSession();
		
		Integer chng_layout = (Integer) session.getAttribute("chng_layout");
		
		if(chng_layout == 0){
			modelAndView.setViewName("login_gun");
		}else{
			modelAndView.setViewName("login");
		}
		
		String rootPath = request.getSession().getServletContext().getRealPath("/");
		String attach_path = "resources/upload/";
		
		String filename = commonDao.selectCurrentImage_logo();
		String savePath = rootPath + attach_path + filename;
		
		File file = new File(savePath);
		
		if(file.exists())
			modelAndView.addObject("filename", filename);
		
		System.out.println("[WEB UI Version] : " + UI_VERSION);
		modelAndView.addObject("ui_version", UI_VERSION);
		*/
		modelAndView.setViewName("sso/login_exec");
		
		return modelAndView;
	}
	
	@RequestMapping(value="reset.html", method={RequestMethod.GET, RequestMethod.POST})
	public ModelAndView reset(){
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.setViewName("pass_reset");
		return modelAndView;
	}
	
	/*
	 * @RequestMapping(value = "chkEmail.html", method={RequestMethod.GET,
	 * RequestMethod.POST})
	 * 
	 * @ResponseBody public String emailChk(@RequestParam("id") String id){
	 * 
	 * String email = allLogInqdao.emailChk(id); System.out.println(email); return
	 * email; }
	 */
}

