package com.easycerti.eframe.psm.system_management.dao;

import java.util.Map;

public interface SummaryMngtDao {

	public int createSummaryA03(String procdate);
	public int createSummaryA05(String procdate);
	public int createSummaryA06(String procdate);
	public int createSummaryA07(String procdate);
	public int createSummaryA99(String procdate);
	public int createSummaryBizLog(String procdate);

	public int createSummaryB03(String procdate);
	public int createSummaryB05(String procdate);
	public int createSummaryB06(String procdate);
	public int createSummaryB07(String procdate);
	public int createSummaryDownloadLog(String procdate);
	
	public void deleteSummary(Map<String,String> param);
	public void deleteSummaryAll(String procdate);
	
	
	public void deleteSummaryDaily(String proc_date);
	public void createSummaryDailyDefault(String proc_date);
	public int createSummaryDaily(String proc_date);
	public int createSummaryDailyDown(String proc_date);
	
	public void deleteSummaryStatistics(String proc_date);
	public void createSummaryStatisticsDefault(String proc_date);
	public int createSummaryStatistics(String proc_date);
	public int createSummaryStatisticsSystem(String proc_date);
	public int createSummaryStatisticsDown(String proc_date);
	
	public void deleteSummaryTime(String proc_date);
	public void createSummaryTimeDefault(String proc_date);
	public int createSummaryTime(String proc_date);
	public int createSummaryTimeSystem(String proc_date);
	public int createSummaryTimeDown(String proc_date);
	
	public void deleteSummaryDownload(String proc_date);
	public void createSummaryDownloadDefault(String proc_date);
	public int createSummaryDownload(String proc_date);
	
	public void deleteSummaryReqtype(String proc_date);
	public void createSummaryReqtypeDefault(String proc_date);
	public int createSummaryReqtype(String proc_date);
	public int createSummaryReqtypeSystem(String proc_date);
	
	
	public void deleteSummaryMonth(String proc_date);
	public int createSummaryMonth(Map<String,String> param);
	
	public void deleteSummaryStatisticsMonth(String proc_date);
	public void createSummaryStatisticsMonthDefault(String proc_date);
	public int createSummaryStatisticsMonth(Map<String,String> param);
	
	public void deleteSummaryTimeMonth(String proc_date);
	public void createSummaryTimeMonthDefault(String proc_date);
	public int createSummaryTimeMonth(Map<String,String> param);
	
	public void deleteSummaryDownloadMonth(String proc_date);
	public void createSummaryDownloadMonthDefault(String proc_date);
	public int createSummaryDownloadMonth(Map<String,String> param);
	
	public void deleteSummaryReqtypeMonth(String proc_date);
	public int createSummaryReqtypeMonth(Map<String,String> param);
	
	public Integer checkSummaryDaily(Map<String, String> parameters);
	public int checkSummaryMonth(Map<String, String> parameters);
	public String findInstallDate(Map<String, String> parameters);
	
	public int createSummaryReqtypeDown(String proc_date);
	public void createSummaryInstallDate(Map<String, String> parameters);
	public String findMinSummaryMonthlyDate();
	
	// 비정상위험 일마감
	public void deleteSummaryAbnormal(String proc_date);
	public void createSummaryAbnormalDefault(String proc_date);
	public int createSummaryAbnormal(String proc_date);
	// 비정상위험 월마감
	public void deleteSummaryAbnormalMonth(String proc_date);
	public void createSummaryAbnormalMonthDefault(String proc_date);
	public int createSummaryAbnormalMonth(Map<String,String> param);
	
}
