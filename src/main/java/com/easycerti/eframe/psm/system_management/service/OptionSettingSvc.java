package com.easycerti.eframe.psm.system_management.service;

import java.util.Map;

import com.easycerti.eframe.common.spring.DataModelAndView;


/**
 * 옵션 설정 Service Interface
 */

public interface OptionSettingSvc {

	public DataModelAndView findOptionSettingList(Map<String, String> parameters,String auth);
	public DataModelAndView setOptionSetting(Map<String, String> parameters);
	public DataModelAndView setMaster(Map<String, String> parameters);
	public DataModelAndView setSessionTime(String time);
	public DataModelAndView setResultTypeView(Map<String, String> parameters);
	public DataModelAndView setSidebarView(Map<String, String> parameters);
	public DataModelAndView setScrnNameView(Map<String, String> parameters);
	public DataModelAndView setMappingId(Map<String, String> parameters);
	public DataModelAndView setUse_systemSeq(Map<String, String> parameters);
	public DataModelAndView setUi_type(Map<String, String> parameters);
}
