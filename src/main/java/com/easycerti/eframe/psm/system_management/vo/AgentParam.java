package com.easycerti.eframe.psm.system_management.vo;

import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 관리자 VO
 * 
 * @author yjyoo
 * @since 2015. 4. 20.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 20.           yjyoo            최초 생성
 *
 * </pre>
 */
public class AgentParam extends AbstractValueObject{
	private int agent_param_seq;
	private String agent_name;
	private String param_name;
	private String param_value;
	private String param_desc;
	private String show_yn;
	private String seqArray;
	private String valueArray;
	private int seqLength;
	
	public int getSeqLength() {
		return seqLength;
	}
	public void setSeqLength(int seqLength) {
		this.seqLength = seqLength;
	}
	public String getSeqArray() {
		return seqArray;
	}
	public void setSeqArray(String seqArray) {
		this.seqArray = seqArray;
	}
	public String getValueArray() {
		return valueArray;
	}
	public void setValueArray(String valueArray) {
		this.valueArray = valueArray;
	}
	public int getAgent_param_seq() {
		return agent_param_seq;
	}
	public void setAgent_param_seq(int agent_param_seq) {
		this.agent_param_seq = agent_param_seq;
	}
	public String getAgent_name() {
		return agent_name;
	}
	public void setAgent_name(String agent_name) {
		this.agent_name = agent_name;
	}
	public String getParam_name() {
		return param_name;
	}
	public void setParam_name(String param_name) {
		this.param_name = param_name;
	}
	public String getParam_value() {
		return param_value;
	}
	public void setParam_value(String param_value) {
		this.param_value = param_value;
	}
	public String getParam_desc() {
		return param_desc;
	}
	public void setParam_desc(String param_desc) {
		this.param_desc = param_desc;
	}
	public String getShow_yn() {
		return show_yn;
	}
	public void setShow_yn(String show_yn) {
		this.show_yn = show_yn;
	}
	
}
