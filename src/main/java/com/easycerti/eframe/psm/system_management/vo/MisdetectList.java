package com.easycerti.eframe.psm.system_management.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.AbstractValueObject;

public class MisdetectList extends AbstractValueObject {
	private List<Misdetect> mis = null;
	
	public MisdetectList() {}
	
	public MisdetectList(List<Misdetect> mis){
		this.mis = mis;
	}
	
	public MisdetectList(List<Misdetect> mis, String count){
		this.mis = mis;
		setPage_total_count(count);
	}

	public List<Misdetect> getMis() {
		return mis;
	}

	public void setMis(List<Misdetect> mis) {
		this.mis = mis;
	}

}
