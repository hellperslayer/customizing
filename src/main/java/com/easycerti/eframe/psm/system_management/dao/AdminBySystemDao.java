package com.easycerti.eframe.psm.system_management.dao;

import java.util.List;

import com.easycerti.eframe.psm.system_management.vo.AdminUser;
import com.easycerti.eframe.psm.system_management.vo.AdminUserSearch;

public interface AdminBySystemDao {
	
	public List<AdminUser> findAdminBySystemList(AdminUserSearch adminUser);

	public Integer findAdminBySystemList_count(AdminUserSearch adminUser);

	public AdminUser findAdminBySystemDetail(String admin_user_id);
	
	public void addAdminBySystem(AdminUser adminUser);

	public void saveAdminBySystem(AdminUser adminUser);
}
