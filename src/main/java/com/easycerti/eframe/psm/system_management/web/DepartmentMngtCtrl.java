package com.easycerti.eframe.psm.system_management.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.psm.system_management.service.DepartmentMngtSvc;

/**
 * 부서 관련 Controller
 * 
 * @author yjyoo
 * @since 2015. 4. 17.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 4. 17.           yjyoo            최초 생성
 *
 * </pre>
 */
@Controller
@RequestMapping(value="/departmentMngt/*")
public class DepartmentMngtCtrl {

	@Autowired
	private DepartmentMngtSvc departmentMngtSvc;
	
	/**
	 * 부서 리스트</br>
	 * - 부서 데이터는 tree 형태
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[환경관리] 조직관리")
	@RequestMapping(value="list.html",method={RequestMethod.POST})
	public DataModelAndView findDepartmentMngtList(@RequestParam Map<String, String> parameters, HttpServletRequest request){

		DataModelAndView modelAndView = departmentMngtSvc.findDepartmentMngtList(parameters);
		modelAndView.setViewName("departmentMngtList");
		modelAndView.addObject("sel_dept_id", parameters.get("sel_dept_id"));
	
		return modelAndView;
	}
	
	/**
	 * 부서 상세정보
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="detail.html",method={RequestMethod.POST})
	public DataModelAndView findDepartmentMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{

		DataModelAndView modelAndView = departmentMngtSvc.findDepartmentMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);

		return modelAndView;
	}
	
	/**
	 * 부서 추가 (json)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="add.html",method={RequestMethod.POST})
	public DataModelAndView addDepartmentMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		String[] args = {"parent_dept_id", "dept_name"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		parameters.put("insert_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		DataModelAndView modelAndView = departmentMngtSvc.addDepartmentMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	/**
	 * 부서 수정 (json)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="save.html",method={RequestMethod.POST})
	public DataModelAndView saveDepartmentMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		String[] args = {"dept_id", "parent_dept_id", "dept_name"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		parameters.put("update_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		
		DataModelAndView modelAndView = departmentMngtSvc.saveDepartmentMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	/**
	 * 부서 삭제 (json)
	 * 
	 * @author yjyoo
	 * @since 2015. 4. 17.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="remove.html",method={RequestMethod.POST})
	public DataModelAndView removeDepartmentMngtOne(@RequestParam Map<String, String> parameters, HttpServletRequest request) throws Exception{
		String[] args = {"dept_id"};
		
		if(!CommonHelper.checkExistenceToParameter(parameters, args)){
			throw new ESException("SYS004J");
		}
		
		DataModelAndView modelAndView = departmentMngtSvc.removeDepartmentMngtOne(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		return modelAndView;
	}
	
	@RequestMapping(value="defaultTree.html",method={RequestMethod.POST})
	public DataModelAndView findDepartmentMngtDefaultTree(@RequestParam Map<String, String> parameters, HttpServletRequest request){

		DataModelAndView modelAndView = departmentMngtSvc.findDepartmentMngtList(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
	
		return modelAndView;
	}
}
