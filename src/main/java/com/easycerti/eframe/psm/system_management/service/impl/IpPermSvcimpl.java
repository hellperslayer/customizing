package com.easycerti.eframe.psm.system_management.service.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.system_management.dao.AdminUserMngtDao;
import com.easycerti.eframe.psm.system_management.dao.IpPermDao;
import com.easycerti.eframe.psm.system_management.service.IpPermSvc;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;
import com.easycerti.eframe.psm.system_management.vo.AdminUserSearch;
import com.easycerti.eframe.psm.system_management.vo.IpPerm;

@Service
public class IpPermSvcimpl implements IpPermSvc {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private IpPermDao ipPermDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private AdminUserMngtDao adminUserMngtDao;
	
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	@Override
	public DataModelAndView ipPermList(Map<String, String> parameters) {
		SimpleCode simpleCode = new SimpleCode();
		IpPerm paramBean = CommonHelper.convertMapToBean(parameters, IpPerm.class);
		String index = "ipPerm/list.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		List<IpPerm> ips = ipPermDao.ipPermList(paramBean);
		DataModelAndView modelAndView = new DataModelAndView();
		
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("ips", ips);
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView ipPermDetail(Map<String, String> parameters, HttpServletRequest request) {
		SimpleCode simpleCode = new SimpleCode();
		IpPerm paramBean = CommonHelper.convertMapToBean(parameters, IpPerm.class);
		String index = "ipPerm/detail.html";
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex(simpleCode);
		DataModelAndView modelAndView = new DataModelAndView();
		if(paramBean.getIp_seq() != 0){ // 상세
			IpPerm ipPermDetail = ipPermDao.ipPermDetail(paramBean);
			modelAndView.addObject("ipPermDetail", ipPermDetail);
		}
		
		AdminUser adminInfo = SystemHelper.getAdminUserInfo(request);
		AdminUserSearch search = new AdminUserSearch();
		search.setAuth_id(adminInfo.getAuth_id());
		List<AdminUser> adminUsers = null;
		
		if("AUTH00000".equals(adminInfo.getAuth_id())) {
			adminUsers = ipPermDao.findAdminUserMngtSuperList();
		} else if("AUTH00001".equals(adminInfo.getAuth_id())) {
			adminUsers = ipPermDao.findAdminUserMngtList();
		} else {
			adminUsers = ipPermDao.findAdminUserSMngtList();
			
		}
		//List<AdminUser> adminUsers = adminUserMngtDao.findAdminUserMngtList(search);
		
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("paramBean", paramBean);
		modelAndView.addObject("adminUsers", adminUsers);
		
		return modelAndView;
	}
	
	
	@Override
	public DataModelAndView addIpPerm(Map<String, String> parameters) {
		IpPerm paramBean = CommonHelper.convertMapToBean(parameters, IpPerm.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			ipPermDao.addIpPerm(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[IpPermSvcimpl.addIpPerm] MESSAGE : " + e.getMessage());
			throw new ESException("SYS030J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView saveIpPerm(Map<String, String> parameters) {
		IpPerm paramBean = CommonHelper.convertMapToBean(parameters, IpPerm.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			ipPermDao.saveIpPerm(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[IpPermSvcimpl.saveIpPerm] MESSAGE : " + e.getMessage());
			throw new ESException("SYS031J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
	
		return modelAndView;
	}

	@Override
	public DataModelAndView removeIpPerm(Map<String, String> parameters) {
		IpPerm paramBean = CommonHelper.convertMapToBean(parameters, IpPerm.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			// 권한 삭제시 해당 메뉴권한 삭제, 관리자권한 NULL 처리
			ipPermDao.removeIpPerm(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[IpPermSvcimpl.removeIpPerm] MESSAGE : " + e.getMessage());
			throw new ESException("SYS032J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	
}
