package com.easycerti.eframe.psm.system_report_management.web;

import java.util.Calendar;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.PdfUtil;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.report.dao.ReportDao;
import com.easycerti.eframe.psm.report.service.ReportSvc;
import com.easycerti.eframe.psm.search.dao.AllLogInqDao;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.system_management.dao.AccessAuthDao;
import com.easycerti.eframe.psm.system_management.dao.AdminUserMngtDao;
import com.easycerti.eframe.psm.system_management.vo.PrivacyReport;
import com.easycerti.eframe.psm.system_report_management.service.SystemReportManagementSvc;

@Controller
@RequestMapping("/systemReport/*")
public class SystemReportManagementCtrl {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private SystemReportManagementSvc systemReportManagementSvc;
	
	@Autowired
	private CommonDao commonDao;
	
	
	@Autowired
	private ReportSvc reportSvc;
	
	@Autowired
	private ReportDao reportDao;

	@Autowired
	private AccessAuthDao accessAuthDao;
	
	@Autowired
	private AllLogInqDao allLogInqDao;
	
	@Autowired
	private PdfUtil pdfUtil;
	
	@Autowired
	private AdminUserMngtDao adminUserMngtDao;
	
	@Value("#{configProperties.use_studentId}")
	private String use_studentId;
	
	@Value("#{configProperties.logo_report_url}")
	private String logo_report_url;
	
	@Value("#{configProperties.use_approval_line}")
	private String use_approval_line;
	
	@Value("#{configProperties.report_path}")
	private String report_path;
	
	
	
	
	/*
	 * 현대건설 시스템별보고서 관리
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[통계보고] 시스템별보고서 관리")
	@RequestMapping(value = "systemReportManagement.html", method = {RequestMethod.POST }) 
	public DataModelAndView findSystemReportManagement(@RequestParam Map<String, String> parameters, 
			@ModelAttribute("search") SearchSearch search, HttpServletRequest request) {
	  
	parameters = CommonHelper.checkSearchDateByWeek(parameters);
	
	SimpleCode simpleCode = new SimpleCode(); 
	String index = "/systemReport/systemReportManagement.html"; 
	simpleCode.setMenu_url(index); 
	String index_id = commonDao.checkIndex(simpleCode); 
	Map<String, String> reportMap = systemReportManagementSvc.findReportCode();
	 
	DataModelAndView modelAndView = systemReportManagementSvc.findReportList(search, request);
	  
	modelAndView.addObject("index_id", index_id);
	modelAndView.addObject("paramBean", parameters);
	modelAndView.addObject("reportMap", reportMap);
	modelAndView.addObject("search",search);
	  
	modelAndView.setViewName("systemReportManagement");
	  
	return modelAndView; 
	
	}
	
	@RequestMapping(value = "getCurrentReport.html", method = {RequestMethod.POST })
	@ResponseBody
	public DataModelAndView getCurrentReport(@RequestParam Map<String, String> parameters) {
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("curr_report", parameters.get("curr_report"));
		modelAndView.setViewName("systemReportManagement");
		
		return modelAndView;
	}
	
	
	//[수준진단보고서] 시스템 별 일괄 생성
	/*@RequestMapping(value="system_empReport.html")
	public ModelAndView findReportEmpLevel_new(HttpServletRequest request,
			@ModelAttribute("search") SearchSearch search, 
			@RequestParam(value = "type", required = false) int type,
			@RequestParam(value = "period_type", required = false) int period_type,
			@RequestParam(value = "system_seq",required=false) String system_seq,
			@RequestParam(value = "start_date", required = false) String start_date,
			@RequestParam(value = "end_date", required = false) String end_date,
			@RequestParam(value = "menu_id", required = false) String menu_id,
			@RequestParam(value = "half_type", required = false) String half_type,
			@RequestParam(value = "quarter_type", required = false) String quarter_type,
			@RequestParam(value = "download_type", required = false) String download_type,
			@RequestParam(value = "system_report", required = false) String system_report) {
		
		ModelAndView modelAndView = new ModelAndView();
		System.out.println(">> 수준진단보고서 ");
		System.out.println("system report : " + system_report);
		
		
		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
	    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
		String date = dayTime.format(new Date(time));
		String ui_type = commonDao.getUiType();
		modelAndView.addObject("ui_type", ui_type);
		
		// 마지막날짜가 현재보다 클 경우 어제날짜까지로 지정해준다.
		Calendar cal = Calendar.getInstance();
		String compareDate = "";
				
		HttpSession session = request.getSession();
		String master = (String)session.getAttribute("master");
	    String use_reportLogo = (String)session.getAttribute("use_reportLogo");
	    String use_reportLine = (String)session.getAttribute("use_reportLine");
	    if (use_reportLogo != null && use_reportLogo.equals("Y")) {
		    String rootPath = request.getSession().getServletContext().getRealPath("/");
			String attach_path = "resources/upload/";
			String filename = commonDao.selectCurrentImage_logo2();
			String savePath = rootPath + attach_path + filename;
			File file = new File(savePath);
			if(file.exists()) {
				modelAndView.addObject("filename", filename);
			}
	    }
	    
	    
	    //수준진단보고서 > report_option
	    Map<String, String> optionMap = new HashMap<String, String>();
		optionMap.put("code_id", "13");
		String proc_month = start_date.replace("-", "").substring(0, 6);
		optionMap.put("proc_month", proc_month);
		optionMap.put("period_type", String.valueOf(period_type));
		List<Map<String, String>> reportOption = reportDao.findReportOption(optionMap);
		
		//총평 데이터 반환
		String report_desc_str = reportDao.getReportDescStr(optionMap);
		
		modelAndView.addObject("report_desc_str", report_desc_str);
		modelAndView.addObject("reportOption", reportOption);
	    modelAndView.addObject("use_reportLogo", use_reportLogo);
	    modelAndView.addObject("use_reportLine", use_reportLine);
		modelAndView.addObject("masterflag", master);
		modelAndView.addObject("report_type", type);
		modelAndView.addObject("period_type", period_type);
		modelAndView.addObject("start_date", start_date);
		modelAndView.addObject("end_date", end_date);
		modelAndView.addObject("date", date);
		modelAndView.addObject("system_seq", system_seq);
		modelAndView.addObject("logo_report_url", logo_report_url);
		modelAndView.addObject("menu_id", menu_id);
		modelAndView.addObject("half_type", half_type);
		modelAndView.addObject("quarter_type", quarter_type);

		//선택 시스템 목록
		List<String> syslist = new ArrayList<String>();
		String tmpList[] = system_seq.split(",");
		for ( int i=0; i< tmpList.length; ++i ) {
			syslist.add(tmpList[i]);
		}
		syslist.add("");
		
		String sDate[] = start_date.split("-");
		String eDate[] = end_date.split("-");

		int strYear = Integer.parseInt(sDate[0]);
		int strMonth = Integer.parseInt(sDate[1]);
		int endYear = Integer.parseInt(eDate[0]);
		int endMonth = Integer.parseInt(eDate[1]);
		int diffMon = (endYear - strYear)* 12 + (endMonth - strMonth);
		String stDate = start_date.replaceAll("-", "").substring(0,6);
		String edDate = end_date.replaceAll("-", "").substring(0,6);
		
		PrivacyReport pr = new PrivacyReport();			//선택 기간
		PrivacyReport prCompare = new PrivacyReport();	//비교 기간 (동년전월, 동년 이전분기)
		PrivacyReport pr1 = new PrivacyReport();		//
	    
		
		// 접속유형 default 세팅
		String mapping_id = (String) session.getAttribute("mapping_id");
		if(mapping_id.equals("Y")) {
			if ( search.getMapping_id() == null ) 
				search.setMapping_id("code1");
			if ( search.getMapping_id().equals("code2") ) 
				search.setMapping_id(null);
		}else {
			if(search.getMapping_id() == null || search.getMapping_id().equals("code2"))
				search.setMapping_id(null);
			else
				search.setMapping_id("code1");
		}
		
		
		String compare_type = "1";
		modelAndView.addObject("compare_type", compare_type);
		if (period_type == 1) {
			pr.setStart_date(stDate);
			pr.setEnd_date(edDate);
			pr.setSysList(syslist);
			
			if(compare_type == "1") {			//동년 전월 (compare_type =1)
				cal.set(strYear, strMonth-1, 1);
			    cal.add(Calendar.MONTH, -1);    	
			    compareDate = dateFormatter.format(cal.getTime());
				pr.setCompare_start_date(compareDate.substring(0,6));		
				pr.setCompare_end_date(compareDate.substring(0,6));			
				
				prCompare.setStart_date(compareDate.substring(0,6));
				prCompare.setEnd_date(compareDate.substring(0,6));
				prCompare.setSysList(syslist);
			}else if(compare_type == "2") {		//전년동월 (compare_type =2)
				pr.setCompare_start_date(strYear-1+stDate.substring(4, 6));
				pr.setCompare_end_date(strYear-1+edDate.substring(4, 6));
				
				//비교 기간1(전년동월)
				prCompare.setStart_date(strYear-1+stDate.substring(4, 6));
				prCompare.setEnd_date(strYear-1+edDate.substring(4, 6));
				prCompare.setSysList(syslist);
			}
			
			
			pr1.setStart_date(strYear+"01");		
			pr1.setEnd_date(edDate);				
		    pr1.setSysList(syslist);				
			pr1.setMonth(strMonth+"");
			pr1.setPeriod_type("1");
			
		}else if(period_type == 2) {	//분기
			pr1.setStart_date(stDate);
			pr1.setEnd_date(edDate);
			pr1.setSysList(syslist);
			pr1.setPeriod_type("2");
			pr1.setQuarter_type(quarter_type);
			
			pr.setStart_date(stDate);
			pr.setEnd_date(edDate);
			pr.setSysList(syslist);
			if(compare_type == "1") {			//동년 전분기 (compare_type =1)
				cal.set(strYear, strMonth-1, 1);		
			    cal.add(Calendar.MONTH, -3);    		
			    compareDate = dateFormatter.format(cal.getTime());
				pr.setCompare_start_date(compareDate.substring(0,6));
				prCompare.setStart_date(compareDate.substring(0,6));
				
				cal.set(strYear, strMonth-1, 1);		
			    cal.add(Calendar.MONTH, -1);    		
			    compareDate = dateFormatter.format(cal.getTime());
				pr.setCompare_end_date(compareDate.substring(0,6));
				prCompare.setEnd_date(compareDate.substring(0,6));
				prCompare.setSysList(syslist);
			}else if(compare_type == "2") {		//전년 동분기 (compare_type =2)
				pr.setCompare_start_date(strYear-1+stDate.substring(4, 6));
				pr.setCompare_end_date(strYear-1+edDate.substring(4, 6));
				//전년 동일분기
				prCompare.setStart_date(strYear-1+stDate.substring(4, 6));
				prCompare.setEnd_date(strYear-1+edDate.substring(4, 6));
				prCompare.setSysList(syslist);
			}
			
		}
		else if(period_type == 4) {	//년
			pr.setStart_date(stDate);
			pr.setEnd_date(edDate);
			pr.setSysList(syslist);
			pr.setCompare_start_date(strYear-1+stDate.substring(4, 6));
			pr.setCompare_end_date(strYear-1+edDate.substring(4, 6));
			
			pr1.setStart_date(stDate);
			pr1.setEnd_date(edDate);
			pr1.setSysList(syslist);
			pr1.setPeriod_type("4");
			
			//전년
			prCompare.setStart_date(strYear-1+stDate.substring(4, 6));
			prCompare.setEnd_date(strYear-1+edDate.substring(4, 6));
			prCompare.setSysList(syslist);
			
		}
	    
		List<SystemMaster> systems = reportSvc.findSystemMasterDetailList(pr);
		modelAndView.addObject("systems", systems);
		
		int diffMonth = getDiffMonth(pr);
		modelAndView.addObject("diffMonth", diffMonth);
		
		int logCnt = reportSvc.findPrivacyTotalLogCnt(pr);				//개인정보 접속기록 총 처리량(로그건수)
		modelAndView.addObject("logCnt", logCnt);
		int prevLogCnt = reportSvc.findPrivacyTotalLogCnt(prCompare);	//개인정보 접속기록 비교기간 총 처리량(로그건수)
		modelAndView.addObject("prevLogCnt", prevLogCnt);
		
		int compareLogcnt = logCnt - prevLogCnt;
		modelAndView.addObject("compareLogcnt", compareLogcnt);
		
		int privacyCnt = reportSvc.findPrivacyTotalCnt(pr);				//개인정보 접속기록 이용량(개인정보유형처리(이용)건수)
		modelAndView.addObject("privacyCnt", privacyCnt);
		
		int prevPrivacyCnt = reportSvc.findPrivacyTotalCnt(prCompare);	//개인정보 접속기록 비교기간 이용량(개인정보유형처리(이용)건수)
		modelAndView.addObject("prevPrivacyCnt", prevPrivacyCnt);
		
		int comparePrivacyCnt = privacyCnt - prevPrivacyCnt;
		modelAndView.addObject("comparePrivacyCnt", comparePrivacyCnt);
		
		List<PrivacyReport> privacyProcessCntBySystem = reportSvc.privacyProcessCntBySystem(pr1);	//1.시스템별 개인정보 접속기록 처리현황
		modelAndView.addObject("privacyProcessCntBySystem", privacyProcessCntBySystem);
		
		List<PrivacyReport> privacyUseCntBySystem = reportSvc.privacyUseCntBySystem(pr1);			//2.시스템별 개인정보 접속기록 이용현황
		modelAndView.addObject("privacyUseCntBySystem", privacyUseCntBySystem);
		
		List<PrivacyReport> comparePrevCntBySystem = reportSvc.comparePrevCntBySystem(pr); 			//3.시스템별 개인정보 접속기록 현황(비교)

		modelAndView.addObject("compUnit01", "");
		modelAndView.addObject("compUnit01_num", "");
		modelAndView.addObject("comparePrevCntBySystem", comparePrevCntBySystem);
		
		List<PrivacyReport> privacyProcessCntByDept = reportSvc.privacyProcessCntByDept(pr1);		//4.소속별 개인정보 접속기록 처리현황
		if(privacyProcessCntByDept.size()==1){
			privacyProcessCntByDept = new ArrayList<PrivacyReport>();
		}
		modelAndView.addObject("privacyProcessCntByDept", privacyProcessCntByDept);
		
		List<PrivacyReport> privacyUseCntByDept = reportSvc.privacyUseCntByDept(pr1);				//5.부서별 개인정보 접속기록 이용현황
		if(privacyUseCntByDept.size()==1){
			privacyUseCntByDept = new ArrayList<PrivacyReport>();
		}
		modelAndView.addObject("privacyUseCntByDept", privacyUseCntByDept);
		
		List<PrivacyReport> comparePrevCntByDept = reportSvc.comparePrevCntByDept(pr); 				//6.소속별 개인정보 접속기록 현황(비교)
		if(comparePrevCntByDept.size()==1){
			comparePrevCntByDept = new ArrayList<PrivacyReport>();
		}
		try {
			
			modelAndView.addObject("compUnit02", "");
			modelAndView.addObject("compUnit02_num", "");
			modelAndView.addObject("comparePrevCntByDept", comparePrevCntByDept);
		} catch (Exception e) {
		}
		
		
		List<PrivacyReport> privacyProcessCntByReqType = reportSvc.privacyProcessCntByReqType(pr1);	//9.수행업무별 접속기록 처리현황
		
		modelAndView.addObject("compUnit05", "");
		modelAndView.addObject("privacyProcessCntByReqType", privacyProcessCntByReqType);
		
		List<PrivacyReport> comparePrevCntByReqType = reportSvc.comparePrevCntByReqType(pr); 		//10.수행업무별 접속기록 처리현황(비교)
		modelAndView.addObject("compUnit06", "");
		modelAndView.addObject("compUnit06_num", "");
		modelAndView.addObject("comparePrevCntByReqType", comparePrevCntByReqType);
		
		List<PrivacyReport> comparePrevCntByUser = reportSvc.comparePrevCntByUser(pr);
		modelAndView.addObject("comparePrevCntByUser", comparePrevCntByUser);
		
		
		
		List<PrivacyReport> privacyUseCntByResultType = new ArrayList<>();
		List<PrivacyReport> comparePrevCntByResultType = new ArrayList<>();
		
		List<Code> resultTypeList = reportSvc.findResultTypeList();									
		ArrayList<PrivacyReport> listSort = new ArrayList<>(resultTypeList.size());
		ArrayList<PrivacyReport> listSort_tmp = new ArrayList<>(resultTypeList.size());
		for(int i=0; i<resultTypeList.size(); i++) {
			Code code = resultTypeList.get(i);
			pr1.setType("TYPE"+code.getCode_id());
			pr.setType("TYPE"+code.getCode_id());
			PrivacyReport returnResult = reportSvc.privacyUseCntByResultType(pr1);					//7.개인정보 유형별 접속기록 이용현황
			PrivacyReport returnResult2 = reportSvc.comparePrevCntByResultType(pr);					//7.개인정보 유형별 접속기록 이용현황(비교)
			returnResult.setCode_name(code.getCode_name());
			returnResult2.setCode_name(code.getCode_name());
			privacyUseCntByResultType.add(i, returnResult);
			comparePrevCntByResultType.add(i, returnResult2);
			
			listSort.add(i, returnResult);
			listSort_tmp.add(i, returnResult);
		}
		ListComparator comp = new ListComparator();
		Collections.sort(privacyUseCntByResultType, comp); //list sorting
		Collections.sort(comparePrevCntByResultType, comp);//list sorting
		
		
		modelAndView.addObject("compUnit03", "");
		modelAndView.addObject("privacyUseCntByResultType", privacyUseCntByResultType);

		modelAndView.addObject("compUnit04", "");
		modelAndView.addObject("compUnit04_num", "");
		modelAndView.addObject("comparePrevCntByResultType", comparePrevCntByResultType);
		
		
		List<PrivacyReport> privacyProcessCntByReqType = reportSvc.privacyProcessCntByReqType(pr1);	//9.수행업무별 접속기록 처리현황
		
		modelAndView.addObject("compUnit05", "");
		modelAndView.addObject("privacyProcessCntByReqType", privacyProcessCntByReqType);
		
		List<PrivacyReport> comparePrevCntByReqType = reportSvc.comparePrevCntByReqType(pr); 		//10.수행업무별 접속기록 처리현황(비교)
		modelAndView.addObject("compUnit06", "");
		modelAndView.addObject("compUnit06_num", "");
		modelAndView.addObject("comparePrevCntByReqType", comparePrevCntByReqType);
		
		//병합 쿼리
		List<PrivacyReport> empWordGrid01 = reportSvc.getEmpWordGrid01(pr1);
		
		modelAndView.addObject("wordUnit01", "");
		modelAndView.addObject("empWordGrid01", empWordGrid01);
		
		List<PrivacyReport> empWordGrid02 = reportSvc.getEmpWordGrid02(pr1);
		modelAndView.addObject("wordUnit02", "");
		modelAndView.addObject("empWordGrid02", empWordGrid02);
		
		
	    modelAndView.addObject("reportType", type);
	    modelAndView.addObject("authorize", reportSvc.reportAuthorizeInfo());
		modelAndView.addObject("download_type", download_type);
		String pdfFullPath = request.getRequestURL() +"?"+ request.getQueryString();
		modelAndView.addObject("pdfFullPath", pdfFullPath);
		modelAndView.addObject("ui_type", ui_type);
		
		
		
		
		modelAndView.setViewName("system_empReport");
		return modelAndView;
	}*/
	
	
	
	//==============================================================================================================
	public int getDiffMonth(PrivacyReport pr) {
		int sYear = Integer.parseInt(pr.getStart_date().substring(0, 4));
		int sMonth = Integer.parseInt(pr.getStart_date().substring(4, 6));
		
		int eYear = Integer.parseInt(pr.getEnd_date().substring(0, 4));
		int eMonth = Integer.parseInt(pr.getEnd_date().substring(4, 6));
		
		return (eYear - sYear) * 12 + (eMonth - sMonth) +1;
	}
	
	public String getMonday(String yyyy,String mm, String wk){

 		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy.MM.dd");
 		Calendar c = Calendar.getInstance();
 		
 		int y=Integer.parseInt(yyyy);
 		int m=Integer.parseInt(mm)-1;
 		int w=Integer.parseInt(wk);

 		c.set(Calendar.YEAR,y);
 		c.set(Calendar.MONTH,m);
 		c.set(Calendar.WEEK_OF_MONTH,w);
 		c.set(Calendar.DAY_OF_WEEK,Calendar.MONDAY);
 		return formatter.format(c.getTime());
 	}
}
