package com.easycerti.eframe.psm.system_report_management.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.vo.SearchBase;

public interface SystemReportManagementSvc {

	Map<String, String> findReportCode();
	
	DataModelAndView findReportList(SearchBase search, HttpServletRequest request);
	
}
