package com.easycerti.eframe.psm.system_report_management.dao;

import java.util.List;

import com.easycerti.eframe.common.vo.SearchBase;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.report.vo.Report;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;

public interface SystemReportManagementDao {

	public List<Code> findReportCodeList();
	
	public int findReportList_count(SearchBase search);

	public List<Report> getSystemReportList(String auth_ids);

	public String getSystemName(String system_seq);
	
}
