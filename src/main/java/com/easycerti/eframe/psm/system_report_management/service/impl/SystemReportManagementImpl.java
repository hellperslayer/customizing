package com.easycerti.eframe.psm.system_report_management.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.vo.SearchBase;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.report.dao.ReportDao;
import com.easycerti.eframe.psm.report.vo.Report;
import com.easycerti.eframe.psm.system_management.dao.AdminUserMngtDao;
import com.easycerti.eframe.psm.system_management.dao.CodeMngtDao;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;
import com.easycerti.eframe.psm.system_management.vo.System;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;
import com.easycerti.eframe.psm.system_report_management.dao.SystemReportManagementDao;
import com.easycerti.eframe.psm.system_report_management.service.SystemReportManagementSvc;

@Repository
public class SystemReportManagementImpl implements SystemReportManagementSvc {

	@Autowired
	private SystemReportManagementDao systemReportManagementDao;
	
	@Autowired
	private CodeMngtDao codeMngtDao;
	
	@Autowired
	private AdminUserMngtDao adminUserMngtDao;
	
	@Value("#{configProperties.defaultPageSize}")
	private String defaultPageSize;
	
	@Override
	public Map<String, String> findReportCode() {
		List<Code> list = systemReportManagementDao.findReportCodeList();
		Map<String, String> result = new HashMap<String, String>();
		for (Code code : list) {
			result.put(code.getCode_id(), code.getCode_name());
		}
		return result;
	}

	@Override
	public DataModelAndView findReportList(SearchBase search, HttpServletRequest request) {
		int pageSize = Integer.valueOf(defaultPageSize);
		search.setSize(pageSize);
		int count = systemReportManagementDao.findReportList_count(search);
		search.setTotal_count(count);

		DataModelAndView modelAndView = new DataModelAndView();
		
		AdminUser adminUser = (AdminUser) request.getSession().getAttribute("userSession");
		
		Code code = new Code();
		code.setGroup_code_id("REPORT_TYPE");
		code.setUse_flag("Y");
		code.setAuth_id(adminUser.getAuth_id());
		List<Code> reportcode = codeMngtDao.findCodeMngtList(code);
		
		//솔루션 최초 설치일 찾기
		code = new Code();
		code.setGroup_code_id("SOLUTION_MASTER");
		code.setCode_id("INSTALL_DATE");
		code.setUse_flag("Y");
		Code install_date = codeMngtDao.findCodeMngtOne(code);
		Map<String, String> reportMap = new HashMap<String, String>();
		for (Code code2 : reportcode) {
			reportMap.put(code2.getCode_id(),code2.getCode_name());
		}
		
		//접속한 계정이 접근할수 있는 시스템 코드를 가져옴
		AdminUser adminUserBean;
		adminUserBean = SystemHelper.findSessionUser(request);
		AdminUser authBean = adminUserMngtDao.findAdminUserMngtOne(adminUserBean.getAdmin_user_id());
		List<Report> authSysList = new ArrayList<Report>();
		
		if(authBean.getAuth_ids()!=null) {
			String[] sys_auth_ids = authBean.getAuth_ids().split(",");
			modelAndView.addObject("sysAuthIds", sys_auth_ids);
			
			for(int i=0;i<sys_auth_ids.length;i++) {
				Report tmp = new Report();
				tmp.setAuth_id(sys_auth_ids[i]);
				tmp.setSystem_seq(sys_auth_ids[i]);
				tmp.setSystem_name(systemReportManagementDao.getSystemName(sys_auth_ids[i]));
				
				authSysList.add(tmp);
			}
		}

		//전체 시스템 목록 MAP으로 담아 넘겨줌
		System system = new System();
		List<SystemMaster> systems = adminUserMngtDao.getSystemMaster();
		Map<String, String> systemMap = new HashMap<String, String>(); 
		for (SystemMaster systemMaster : systems) {
			systemMap.put(systemMaster.getSystem_seq(), systemMaster.getSystem_name());
		}
		
		List<Report> reportList = systemReportManagementDao.getSystemReportList(authBean.getAuth_ids());
		
		
		modelAndView.addObject("install_date", install_date);
		modelAndView.addObject("userAuth", authBean);
		modelAndView.addObject("systemMap", systemMap);
		modelAndView.addObject("reportMap", reportMap);
		modelAndView.addObject("reportcode", reportcode);
		modelAndView.addObject("reportList", reportList);
		modelAndView.addObject("authSysList", authSysList);
		return modelAndView;
	}

}
