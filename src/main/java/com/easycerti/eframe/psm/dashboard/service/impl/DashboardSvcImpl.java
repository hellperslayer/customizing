package com.easycerti.eframe.psm.dashboard.service.impl;

import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionStatus;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.spring.TransactionUtil;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.GetStatisticsTypeData;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.dashboard.dao.DashboardDao;
import com.easycerti.eframe.psm.dashboard.service.DashboardSvc;
import com.easycerti.eframe.psm.dashboard.vo.Dashboard;
import com.easycerti.eframe.psm.report.service.ReportSvc;
import com.easycerti.eframe.psm.search.dao.AllLogInqDao;
import com.easycerti.eframe.psm.search.vo.AllLogInq;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInq;
import com.easycerti.eframe.psm.system_management.dao.SummaryMngtDao;
import com.easycerti.eframe.psm.system_management.dao.SystemMngtDao;
import com.easycerti.eframe.psm.system_management.vo.System;

/**
 * 대시보드 관련 Service Implements
 * 
 * @author yjyoo
 * @since 2015. 5. 12.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 12.           yjyoo            최초 생성
 *
 * </pre>
 */
@Repository
public class DashboardSvcImpl implements DashboardSvc {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private DashboardDao dashboardDao;
	
	@Autowired
	private AllLogInqDao allLogInqDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private SummaryMngtDao summaryMngtDao;
	
	@Autowired
	private SystemMngtDao systemMngtDao;
	
	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	@Autowired
	private ReportSvc reportSvc;
	
	@Override
	public DataModelAndView findDashboardList_chart1(Map<String, String> parameters) {
		Dashboard paramBean = CommonHelper.convertMapToBean(parameters, Dashboard.class);
		
		List<Dashboard> dashboard_chart1 = dashboardDao.findDashboardList_chart1(paramBean);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("dashboard_chart1", dashboard_chart1);
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView findDashboardList_recent(Map<String, String> parameters) {
		Dashboard paramBean = CommonHelper.convertMapToBean(parameters, Dashboard.class);

		List<AllLogInq> dashboard_recent = dashboardDao.findDashboardList_recent(paramBean);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("dashboard_recent", dashboard_recent);
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView findDashboardList_chart2(Map<String, String> parameters, HttpServletRequest request) {
		Dashboard paramBean = CommonHelper.convertMapToBean(parameters, Dashboard.class);
		/*
		if(paramBean.getSearch_from() == null || paramBean.getSearch_from() == "" && paramBean.getSearch_to() == null || paramBean.getSearch_to() == ""){
			paramBean.setSearch_from(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
		}
		if(paramBean.getSearch_from() == null || paramBean.getSearch_from() == ""){
			paramBean.setSearch_from(paramBean.getSearch_to());
		}
		if(paramBean.getSearch_to() == null || paramBean.getSearch_to() == ""){
			paramBean.setSearch_to(paramBean.getSearch_from());
		}
		*/
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
		if (list == null) {
			list = (List<String>) request.getSession().getAttribute("auth_ids_list");
		}
		paramBean.setAuth_idsList(list);
		DataModelAndView modelAndView = new DataModelAndView();
		if(parameters.get("selVal") != null && !parameters.get("selVal").isEmpty()) {
			String selVal = String.valueOf(parameters.get("selVal"));
			if(selVal.equals("2")) {
				List<AllLogInq> dashboard_chart2 = dashboardDao.findUserAccessDate(paramBean);
				String checkEmpNameMasking = commonDao.checkEmpNameMasking();
				if(checkEmpNameMasking.equals("Y")) {
					for (int i = 0; i < dashboard_chart2.size(); i++) {
						String emp_user_name = dashboard_chart2.get(i).getEmp_user_name();
						StringBuilder builder = new StringBuilder(emp_user_name);
						builder.setCharAt(emp_user_name.length() - 2, '*');
						dashboard_chart2.get(i).setEmp_user_name(builder.toString());
					}
				}
				modelAndView.addObject("dashboard_chart2", dashboard_chart2);
				modelAndView.addObject("cnt", dashboard_chart2.size());
				modelAndView.addObject("paramBean", paramBean);
			} else if(selVal.equals("3")) {
				List<AllLogInq> dashboard_chart2 = dashboardDao.findUserAccessMonth(paramBean);
				String checkEmpNameMasking = commonDao.checkEmpNameMasking();
				if(checkEmpNameMasking.equals("Y")) {
					for (int i = 0; i < dashboard_chart2.size(); i++) {
						String emp_user_name = dashboard_chart2.get(i).getEmp_user_name();
						StringBuilder builder = new StringBuilder(emp_user_name);
						builder.setCharAt(emp_user_name.length() - 2, '*');
						dashboard_chart2.get(i).setEmp_user_name(builder.toString());
					}
				}
				modelAndView.addObject("dashboard_chart2", dashboard_chart2);
				modelAndView.addObject("cnt", dashboard_chart2.size());
				modelAndView.addObject("paramBean", paramBean);
			}
		} else {
			List<AllLogInq> dashboard_chart2 = dashboardDao.findDashboardList_recent(paramBean);
			String checkEmpNameMasking = commonDao.checkEmpNameMasking();
			if(checkEmpNameMasking.equals("Y")) {
				for (int i = 0; i < dashboard_chart2.size(); i++) {
					String emp_user_name = dashboard_chart2.get(i).getEmp_user_name();
					StringBuilder builder = new StringBuilder(emp_user_name);
					builder.setCharAt(emp_user_name.length() - 2, '*');
					dashboard_chart2.get(i).setEmp_user_name(builder.toString());
				}
			}
			modelAndView.addObject("dashboard_chart2", dashboard_chart2);
			modelAndView.addObject("cnt", dashboard_chart2.size());
			modelAndView.addObject("paramBean", paramBean);
		}
		return modelAndView;
	}
	
	@Override
	public DataModelAndView findDashboardList_chart3(Map<String, String> parameters, HttpServletRequest request) {
		Dashboard paramBean = CommonHelper.convertMapToBean(parameters, Dashboard.class);
		/*
		if(paramBean.getSearch_from() == null || paramBean.getSearch_from() == "" && paramBean.getSearch_to() == null || paramBean.getSearch_to() == ""){
			paramBean.setSearch_from(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
		}
		if(paramBean.getSearch_from() == null || paramBean.getSearch_from() == ""){
			paramBean.setSearch_from(paramBean.getSearch_to());
		}
		if(paramBean.getSearch_to() == null || paramBean.getSearch_to() == ""){
			paramBean.setSearch_to(paramBean.getSearch_from());
		}
		
		if(paramBean.getPop_from() != null && paramBean.getPop_to() != null){
			paramBean.setSearch_from(paramBean.getPop_from());
			paramBean.setSearch_to(paramBean.getPop_to());
		}
		*/
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
		if (list == null) {
			list = (List<String>) request.getSession().getAttribute("auth_ids_list");
		}
		
		paramBean.setAuth_idsList(list);
		DataModelAndView modelAndView = new DataModelAndView();
		
		if(parameters.get("selVal") != null && !parameters.get("selVal").isEmpty()) {
			String selVal = String.valueOf(parameters.get("selVal"));
			if(selVal.equals("2")) {
				List<Dashboard> dashboard_chart3 = dashboardDao.findSysAccessDate(paramBean);
				modelAndView.addObject("dashboard_chart3", dashboard_chart3);
				modelAndView.addObject("paramBean", paramBean);
			} else if(selVal.equals("3")) {
				List<Dashboard> dashboard_chart3 = dashboardDao.findSysAccessMonth(paramBean);
				modelAndView.addObject("dashboard_chart3", dashboard_chart3);
				modelAndView.addObject("paramBean", paramBean);
			}
		} else {
			List<Dashboard> dashboard_chart3 = dashboardDao.findDashboardList_chart3(paramBean);
			modelAndView.addObject("dashboard_chart3", dashboard_chart3);
			modelAndView.addObject("paramBean", paramBean);
		}
		
		return modelAndView;
	}

	@Override
	public DataModelAndView findDashboardList_chart4(Map<String, String> parameters, HttpServletRequest request) {
		Dashboard paramBean = CommonHelper.convertMapToBean(parameters, Dashboard.class);
		/*
		if(paramBean.getSearch_from() == null || paramBean.getSearch_from() == "" && paramBean.getSearch_to() == null || paramBean.getSearch_to() == ""){
			paramBean.setSearch_from(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
		}
		if(paramBean.getSearch_from() == null || paramBean.getSearch_from() == ""){
			paramBean.setSearch_from(paramBean.getSearch_to());
		}
		if(paramBean.getSearch_to() == null || paramBean.getSearch_to() == ""){
			paramBean.setSearch_to(paramBean.getSearch_from());
		}
		
		if(paramBean.getPop_from() != null && paramBean.getPop_to() != null){
			paramBean.setSearch_from(paramBean.getPop_from());
			paramBean.setSearch_to(paramBean.getPop_to());
		}
		*/
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
		if (list == null) {
			list = (List<String>) request.getSession().getAttribute("auth_ids_list");
		}
		paramBean.setAuth_idsList(list);
		DataModelAndView modelAndView = new DataModelAndView();
		
		/*if(parameters.get("selVal") != null && !parameters.get("selVal").isEmpty()) {
			String selVal = String.valueOf(parameters.get("selVal"));
			if(selVal.equals("2")) {
				List<Dashboard> dashboard_chart4 = dashboardDao.findTypeAccessDate(paramBean);
				int codeId = 0;
				String codeName = "";
				List<Code> resultTypeList = reportSvc.findResultTypeList();
				List<Dashboard> dashVoLst = new ArrayList<Dashboard>();
				for(int i=0; i<dashboard_chart4.size(); i++) {
					Dashboard dashVo = dashboard_chart4.get(i);
					for(Code codeVo : resultTypeList) {
						Dashboard dashTempVo = new Dashboard();
						codeId = Integer.parseInt(codeVo.getCode_id());
						codeName = codeVo.getCode_name();
						if(codeId == 1) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType1());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 2) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType2());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 3) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType3());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 4) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType4());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 5) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType5());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 6) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType6());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 7) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType7());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 8) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType8());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 9) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType9());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 10) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType10());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 11) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType11());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 12) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType12());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 13) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType13());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 14) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType14());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 15) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType15());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 31) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType31());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 32) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType32());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 33) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType33());
							dashVoLst.add(dashTempVo);
						}
					}
				}
				modelAndView.addObject("dashboard_chart4", dashVoLst);
				modelAndView.addObject("paramBean", paramBean);
			} else if(selVal.equals("3")) {
				List<Dashboard> dashboard_chart4 = dashboardDao.findTypeAccessMonth(paramBean);
				int codeId = 0;
				String codeName = "";
				List<Code> resultTypeList = reportSvc.findResultTypeList();
				List<Dashboard> dashVoLst = new ArrayList<Dashboard>();
				for(int i=0; i<dashboard_chart4.size(); i++) {
					Dashboard dashVo = dashboard_chart4.get(i);
					for(Code codeVo : resultTypeList) {
						Dashboard dashTempVo = new Dashboard();
						codeId = Integer.parseInt(codeVo.getCode_id());
						codeName = codeVo.getCode_name();
						if(codeId == 1) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType1());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 2) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType2());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 3) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType3());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 4) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType4());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 5) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType5());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 6) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType6());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 7) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType7());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 8) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType8());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 9) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType9());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 10) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType10());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 11) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType11());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 12) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType12());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 13) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType13());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 14) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType14());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 15) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType15());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 31) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType31());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 32) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType32());
							dashVoLst.add(dashTempVo);
						} else if(codeId == 33) {
							dashTempVo.setResult_type(String.valueOf(codeId));
							dashTempVo.setData1(codeName);
							dashTempVo.setCnt1(dashVo.getType33());
							dashVoLst.add(dashTempVo);
						}
					}
				}
				modelAndView.addObject("dashboard_chart4", dashVoLst);
				modelAndView.addObject("paramBean", paramBean);
			}
		} else {
			List<Dashboard> dashboard_chart4 = dashboardDao.findDashboardList_chart4(paramBean);
			modelAndView.addObject("dashboard_chart4", dashboard_chart4);
			modelAndView.addObject("paramBean", paramBean);
		}*/
		
		return modelAndView;
	}

	@Override
	public DataModelAndView findDashboardList_chart5(Map<String, String> parameters, HttpServletRequest request) {
		Dashboard paramBean = CommonHelper.convertMapToBean(parameters, Dashboard.class);
		if(paramBean.getSearch_from() == null || paramBean.getSearch_from() == "" && paramBean.getSearch_to() == null || paramBean.getSearch_to() == ""){
			Date dt = new Date();
			paramBean.setSearch_to(new SimpleDateFormat("yyyy-MM-dd").format(dt));
			
			Calendar date = Calendar.getInstance();
			date.add(Calendar.DATE , -7);
			dt=date.getTime();
			
			paramBean.setSearch_from(new SimpleDateFormat("yyyy-MM-dd").format(dt));
		}
		if(paramBean.getSearch_from() == null || paramBean.getSearch_from() == ""){
			paramBean.setSearch_from(paramBean.getSearch_to());
		}
		if(paramBean.getSearch_to() == null || paramBean.getSearch_to() == ""){
			paramBean.setSearch_to(paramBean.getSearch_from());
		}
		
		if(paramBean.getPop_from() != null && paramBean.getPop_to() != null){
			paramBean.setSearch_from(paramBean.getPop_from());
			paramBean.setSearch_to(paramBean.getPop_to());
		}
		
		String sf = paramBean.getSearch_from();
		String st = paramBean.getSearch_to();
		if(sf.equals(st)) {
			String today = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			if(today.equals(sf)) {
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DATE , -7);
				Date date = cal.getTime();
				paramBean.setSearch_from(new SimpleDateFormat("yyyy-MM-dd").format(date));
			}
		}
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
		if (list == null) {
			list = (List<String>) request.getSession().getAttribute("auth_ids_list");
		}
		
		paramBean.setAuth_idsList(list);
		
		List<Dashboard> dashboard_chart5 = dashboardDao.findDashboardList_chart5(paramBean);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("dashboard_chart5", dashboard_chart5);
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView findDashboardList_chart6(Map<String, String> parameters, HttpServletRequest request) {
		Dashboard paramBean = CommonHelper.convertMapToBean(parameters, Dashboard.class);
		if(paramBean.getSearch_from() == null || paramBean.getSearch_from() == "" && paramBean.getSearch_to() == null || paramBean.getSearch_to() == ""){
			paramBean.setSearch_from(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
		}
		if(paramBean.getSearch_from() == null || paramBean.getSearch_from() == ""){
			paramBean.setSearch_from(paramBean.getSearch_to());
		}
		if(paramBean.getSearch_to() == null || paramBean.getSearch_to() == ""){
			paramBean.setSearch_to(paramBean.getSearch_from());
		}
		
		if(paramBean.getPop_from() != null && paramBean.getPop_to() != null){
			paramBean.setSearch_from(paramBean.getPop_from());
			paramBean.setSearch_to(paramBean.getPop_to());
		}
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
		if (list == null) {
			list = (List<String>) request.getSession().getAttribute("auth_ids_list");
		}
		
		paramBean.setAuth_idsList(list);
		
		List<Dashboard> dashboard_chart6 = new ArrayList<>();
		String useDashboardScenario = dashboardDao.getUseDashboardScenario();
		if (useDashboardScenario.equals("Y")) {
			/*Code dashboard_scenario = commonDao.getDashboardScenario();
			paramBean.setScen_seq(dashboard_scenario.getCode_id());*/
			dashboard_chart6 = dashboardDao.findDashboardList_chart6_detail(paramBean);
		} else {
			dashboard_chart6 = dashboardDao.findDashboardList_chart6(paramBean);
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("dashboard_chart6", dashboard_chart6);
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView findDashboardList_chart7(Map<String, String> parameters, HttpServletRequest request) {
		Dashboard paramBean = CommonHelper.convertMapToBean(parameters, Dashboard.class);
		if(paramBean.getSearch_from() == null || paramBean.getSearch_from() == "" && paramBean.getSearch_to() == null || paramBean.getSearch_to() == ""){
			paramBean.setSearch_from(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
		}
		if(paramBean.getSearch_from() == null || paramBean.getSearch_from() == ""){
			paramBean.setSearch_from(paramBean.getSearch_to());
		}
		if(paramBean.getSearch_to() == null || paramBean.getSearch_to() == ""){
			paramBean.setSearch_to(paramBean.getSearch_from());
		}
		
		if(paramBean.getPop_from() != null && paramBean.getPop_to() != null){
			paramBean.setSearch_from(paramBean.getPop_from());
			paramBean.setSearch_to(paramBean.getPop_to());
		}
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
		if (list == null) {
			list = (List<String>) request.getSession().getAttribute("auth_ids_list");
		}
		paramBean.setAuth_idsList(list);
		
		DataModelAndView modelAndView = new DataModelAndView();
		if(parameters.get("selVal") != null && !parameters.get("selVal").isEmpty()) {
			String selVal = String.valueOf(parameters.get("selVal"));
			if(selVal.equals("2")) {
				List<Dashboard> chartList = dashboardDao.findDeptLogDate(paramBean);
				modelAndView.addObject("dashboard_chart7", chartList);
				modelAndView.addObject("paramBean", paramBean);
			} else if(selVal.equals("3")) {
				List<Dashboard> chartList = dashboardDao.findDeptLogMonth(paramBean);
				modelAndView.addObject("dashboard_chart7", chartList);
				modelAndView.addObject("paramBean", paramBean);
			}
		} else {
			List<Dashboard> dashboard_chart7 = dashboardDao.findDashboardList_chart7(paramBean);
			modelAndView.addObject("dashboard_chart7", dashboard_chart7);
			modelAndView.addObject("paramBean", paramBean);
		}
		
		return modelAndView;
	}

	@Override
	public DataModelAndView findDashboardList_chart8(Map<String, String> parameters, HttpServletRequest request) {
		Dashboard paramBean = CommonHelper.convertMapToBean(parameters, Dashboard.class);
		List<Dashboard> dashboard_chart8 = null;
		DataModelAndView modelAndView = new DataModelAndView();
		
		HttpSession session = request.getSession();
		String ui_type = (String)session.getAttribute("ui_type");
		if (ui_type != null & ui_type.length() > 0) {
//			if(ui_type.equals("G")) {
//				dashboard_chart8 = dashboardDao.findDashboardList_chart8_stack(paramBean);
//			} else {
//				dashboard_chart8 = dashboardDao.findDashboardList_chart8(paramBean);
//			}
			dashboard_chart8 = dashboardDao.findDashboardList_chart8_stack(paramBean);
			modelAndView.addObject("ui_type", ui_type);
		} else {
			dashboard_chart8 = dashboardDao.findDashboardList_chart8(paramBean);	
		}
		
		modelAndView.addObject("dashboard_chart8", dashboard_chart8);
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView findDashboardList_chart_emp(Map<String, String> parameters) {
		Dashboard paramBean = CommonHelper.convertMapToBean(parameters, Dashboard.class);
		
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		String proc_date = format.format(new Date());
		
		paramBean.setProc_date(proc_date);
		
		ExtrtCondbyInq dashboard_chart_emp = dashboardDao.findDashboardList_chart_emp(paramBean);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("dashboard_chart_emp", dashboard_chart_emp);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView findDashboardListDelete(Map<String, String> parameters) {
		
		Dashboard paramBean = CommonHelper.convertMapToBean(parameters, Dashboard.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			dashboardDao.findDashboardListDelete(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[IpPermSvcimpl.addIpPerm] MESSAGE : " + e.getMessage());
			throw new ESException("SYS030J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView findDashboardListUpdate(Map<String, String> parameters) {
		
		Dashboard paramBean = CommonHelper.convertMapToBean(parameters, Dashboard.class);
		
		TransactionStatus transactionStatus = TransactionUtil.getMybatisTransactionStatus(transactionManager);
		try {
			dashboardDao.findDashboardListUpdate(paramBean);
			transactionManager.commit(transactionStatus);
		} catch(DataAccessException e) {
			transactionManager.rollback(transactionStatus);
			logger.error("[IpPermSvcimpl.addIpPerm] MESSAGE : " + e.getMessage());
			throw new ESException("SYS030J");
		}
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView findDashPopupList(Map<String, String> parameters) {
		
		List<Dashboard> items = dashboardDao.findDashboardList();
		
		DataModelAndView modelAndView = new DataModelAndView();
		
		modelAndView.addObject("items", items);
		
		return modelAndView;
	}

	@Override
	public DataModelAndView extract_chart(Map<String, String> parameters) {
		Dashboard paramBean = CommonHelper.convertMapToBean(parameters, Dashboard.class);
		if(paramBean.getSearch_from() == null || paramBean.getSearch_from() == "" && paramBean.getSearch_to() == null || paramBean.getSearch_to() == ""){
			paramBean.setSearch_from(new SimpleDateFormat("yyyyMMdd").format(new Date()));
		}
		if(paramBean.getSearch_from() == null || paramBean.getSearch_from() == ""){
			paramBean.setSearch_from(paramBean.getSearch_to());
		}
		if(paramBean.getSearch_to() == null || paramBean.getSearch_to() == ""){
			paramBean.setSearch_to(paramBean.getSearch_from());
		}
		paramBean.setSearch_from(paramBean.getSearch_from().replaceAll("-", ""));
		paramBean.setSearch_to(paramBean.getSearch_to().replaceAll("-", ""));
		List<Dashboard> dashboard_extract = dashboardDao.extract_chart(paramBean);
		
//		String startDateS = paramBean.getSearch_from().replaceAll("-", "");
//		String endDateS = paramBean.getSearch_to().replaceAll("-", "");
		String startDateS = paramBean.getSearch_from().replaceAll("-", "");
		String endDateS = paramBean.getSearch_to().replaceAll("-", "");
		
		Calendar cal = Calendar.getInstance();
		List<String> dateList = new ArrayList<>();
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
			Date startDate = dateFormat.parse(startDateS);
			cal.setTime(startDate);
			while( true ){
				String curDateS = dateFormat.format( cal.getTime() );
				dateList.add(curDateS);
				if( endDateS.equals( curDateS ) ){
					break;
				}
				cal.add(Calendar.DATE, 1);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		Set<String> ruleCdMap = new HashSet<>();
		
		for( Dashboard dboard : dashboard_extract ){
			ruleCdMap.add(dboard.getRule_cd());
		}
		
		List<String> ruleCdList = new ArrayList<>();
		
		Iterator<String> rcmIter = ruleCdMap.iterator();
		while( rcmIter.hasNext() ){
			String t = rcmIter.next();
			ruleCdList.add(t);
		}
		
		List<Dashboard> dashboard_data = new ArrayList<>();
		
		for( String ruleCd : ruleCdList ){
			for( String date : dateList ){
				Dashboard dashboard = new Dashboard();
				
				boolean breakFlag = false;
				for( Dashboard tDashboard : dashboard_extract ){
					if( ruleCd.equals(tDashboard.getRule_cd()) && date.equals(tDashboard.getOccr_dt()) ){
						dashboard = tDashboard;
						breakFlag = true;
						break;
					}
				}
				
				if( breakFlag == false ){
					dashboard.setOccr_dt(date);
					dashboard.setRule_cd(ruleCd);
					dashboard.setCnt(0);
				}
				
				dashboard_data.add(dashboard);
			}
		}
		
		
		List<Dashboard> extract_name = dashboardDao.extract_name(paramBean);
		DataModelAndView modelAndView = new DataModelAndView();
		//modelAndView.addObject("dashboard_extract", dashboard_extract);
		modelAndView.addObject("dashboard_extract", dashboard_data);
		modelAndView.addObject("extract_name", extract_name);
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}

	@Override
	public List<Dashboard> getNewDashboardChart1() {
		return dashboardDao.getNewDashboardChart1();
	}
	
	@Override
	public List<Dashboard> getNewDashboardChart1(Dashboard dashboard) {
		return dashboardDao.getNewDashboardChart1(dashboard);
	}
	
	@Override
	public List<Dashboard> getNewDashboardChart2() {
		return dashboardDao.getNewDashboardChart2();
	}
	
	@Override
	public List<Dashboard> getNewDashboardChart3() {
		return dashboardDao.getNewDashboardChart3();
	}
	
	@Override
	public List<Dashboard> getNewDashboardChart4() {
		return dashboardDao.getNewDashboardChart4();
	}
	
	@Override
	public List<Dashboard> getNewDashboardChart5() {
		return dashboardDao.getNewDashboardChart5();
	}
	
	@Override
	public List<Dashboard> getNewDashboardChart6() {
		return dashboardDao.getNewDashboardChart6();
	}
	
	@Override
	public List<Dashboard> getNewDashboardChart7() {
		return dashboardDao.getNewDashboardChart7();
	}
	
	@Override
	public List<Dashboard> getNewDashboardChart8() {
		return dashboardDao.getNewDashboardChart8();
	}
	
	@Override
	public List<Dashboard> getNewDashboardChart9() {
		return dashboardDao.getNewDashboardChart9();
	}
	
	@Override
	public List<Dashboard> getNewDashboardChart10() {
		return dashboardDao.getNewDashboardChart10();
	}
	@Override
	public List<Dashboard> getNewDashboardChart11() {
		return dashboardDao.getNewDashboardChart11();
	}
	
	@Override
	public List<Dashboard> getNewDashboardChart12() {
		return dashboardDao.getNewDashboardChart12();
	}
	
	@Override
	public List<Dashboard> getNewDashboardChart13() {
		return dashboardDao.getNewDashboardChart13();
	}

	@Override
	public DataModelAndView setDashboardView(Map<String, String> parameters,HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView();
		Calendar cal = GregorianCalendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, -1);
		Date date = cal.getTime();
		
		SimpleDateFormat sdf = null;
		String periodType = "";
		String selectSys = "";
		
		HttpSession session = request.getSession();
		String ui_type = (String)session.getAttribute("ui_type");
		
		//대시보드 유형이 없을 경우 기본 일간으로 넘어가게 설정
		if((periodType=parameters.get("periodType"))==null) {
			parameters.put("periodType","date");
			periodType = "date";
		}
		
		//월간일 경우 날짜 부분 삭제
		if(periodType.equals("month")) {
			sdf = new SimpleDateFormat("yyyy-MM");
		}else {
			sdf = new SimpleDateFormat("yyyy-MM-dd");
		}
		
		//날짜 데이터가 없을 경우 오늘날짜
		if(parameters.get("search_from")==null || parameters.get("search_from").equals("")) {
			parameters.put("search_from",sdf.format(date));
		}
		
		parameters.put("year",parameters.get("search_from").split("-")[0]);
		parameters.put("month",parameters.get("search_from").split("-")[1]);
		if(parameters.get("search_to")==null || parameters.get("search_to").equals("")) {
			parameters.put("search_to",sdf.format(date));
		}
		Dashboard dashboard = new Dashboard();
		dashboard.setSearch_from(parameters.get("search_from").replace("-", ""));
		dashboard.setSearch_to(parameters.get("search_to").replace("-", ""));
		dashboard.setUi_type(ui_type);

		
		List<String> auth_idsList = (List<String>) request.getSession().getAttribute("auth_ids_list");
		System system = new System();
		if((selectSys = parameters.get("system_seq"))!=null){
			dashboard.setSystem_seq(selectSys);
			parameters.put("system_seq", selectSys);
		}
		system.setAuth_idsList(auth_idsList);
		List<System> systemList = systemMngtDao.findSystemMngtListAuth(system);
		dashboard.setAuth_idsList(auth_idsList);
		
		String methodTailName = "";
		if(periodType.equals("now")) {
			methodTailName = "Rt_partition";
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd");
			Date date2 = new Date();
			
			cal = Calendar.getInstance();
			cal.setTime(date2);
			cal.add(Calendar.YEAR, -1);
			dashboard.setProc_date(sdf2.format(cal.getTime()));
			dashboard.setSearch_from(sdf2.format(new Date()));
		}else if(periodType.equals("date")) {
			methodTailName = "Daily";
		}else if(periodType.equals("month")) {
			methodTailName = "Month";
			dashboard.setSearch_from(dashboard.getSearch_from().substring(0, 6));
			dashboard.setSearch_to(dashboard.getSearch_to().substring(0, 6));
		}
		
		try {
			//시스템별 접속기록 Top5					DAO의 클래스에서			대상 메소드 + 기간별 구분 이름을 통해 메소드 이름과 매개변수 정보로 실행 메소드 정보 획득		실행될 메소드를 가지고 있는 객체와 매개변수를 넘기고 메소드 실행
			List<AllLogInq> systemConnectTop5 = (List<AllLogInq>) dashboardDao.getClass().getMethod("getDataBoardChart1"+methodTailName, Dashboard.class).invoke(dashboardDao,dashboard);
			//개인정보 접속기록 Top5
	 		List<AllLogInq> personalConnectTop5 = (List<AllLogInq>) dashboardDao.getClass().getMethod("getDataBoardChart2"+methodTailName, Dashboard.class).invoke(dashboardDao,dashboard);
			//소속별 개인정보 로그 수집 현황
			List<AllLogInq> deptLogCollect = (List<AllLogInq>) dashboardDao.getClass().getMethod("getDataBoardChart3"+methodTailName, Dashboard.class).invoke(dashboardDao,dashboard);
			//개인정보 유형별 접속기록 현황
			List<AllLogInq> privacyTypeConnect = (List<AllLogInq>) dashboardDao.getClass().getMethod("getDataBoardChart4"+methodTailName, Dashboard.class).invoke(dashboardDao,dashboard);
			
			List<Code> resultTypes = commonDao.getPrivacyDescList();
			
			String[][] arr = new String[resultTypes.size()][2];
			for(int i=0; i<resultTypes.size();i++){
				Code code = resultTypes.get(i);
				int cnt = GetStatisticsTypeData.getType(privacyTypeConnect.get(0), "type"+code.getResult_type_order());
				arr[i][0] = code.getCode_name();
				arr[i][1] = String.valueOf(cnt);
			}
			modelAndView.addObject("ui_type", ui_type);
			modelAndView.addObject("arr", arr);
			
			modelAndView.addObject("resultTypes", resultTypes);
			modelAndView.addObject("privacyTypeConnect", privacyTypeConnect);
			
			modelAndView.addObject("systemConnectTop5", systemConnectTop5);
			modelAndView.addObject("personalConnectTop5", personalConnectTop5);
			modelAndView.addObject("deptLogCollect", deptLogCollect);
			modelAndView.addObject("privacyTypeConnect", privacyTypeConnect);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//시스템 정보
		List<Dashboard> systemInfo = dashboardDao.getDataBoardChart5_serverPart();	//가장 마지막에 수집된 서버별 서버상태 정보 하나씩 입력
		modelAndView.addObject("systemInfo", systemInfo);
		modelAndView.addObject("systemList", systemList);
		
		modelAndView.addObject("parameters", parameters);
		modelAndView.setViewName("databoard");
		return modelAndView;
	}

}
