package com.easycerti.eframe.psm.dashboard.vo;
/**
 * 대시보드 부서정보(지도 버전) VO
 * 
 */
public class DeptInfo_map {
	
	private String name;
	
	private int pos_x;
	
	private int pos_y;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPos_x() {
		return pos_x;
	}

	public void setPos_x(int pos_x) {
		this.pos_x = pos_x;
	}

	public int getPos_y() {
		return pos_y;
	}

	public void setPos_y(int pos_y) {
		this.pos_y = pos_y;
	}
		
}
