package com.easycerti.eframe.psm.dashboard.dao;

import java.util.List;

import com.easycerti.eframe.psm.dashboard.vo.DeptInfo_map;
import com.easycerti.eframe.psm.dashboard.vo.MapInfo;


public interface DashboardMapDao {
	
	public MapInfo findDashboardMap();
	
	public List<DeptInfo_map> findDeptInfo(String name);
}
