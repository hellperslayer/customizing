package com.easycerti.eframe.psm.dashboard.vo;

import java.util.List;

public class TempData {
	
	private String name;
	private String id;
	private String[] citys;
	private int waringStep;
	private String posTop;
	private String posLeft;
	private String image;
	
	private List data2;
	private List data3;
	private List data4;
	private List data5;
	private List data6;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getWaringStep() {
		return waringStep;
	}
	public void setWaringStep(int waringStep) {
		this.waringStep = waringStep;
	}
	public List getData4() {
		return data4;
	}
	public void setData4(List data4) {
		this.data4 = data4;
	}
	public List getData5() {
		return data5;
	}
	public void setData5(List data5) {
		this.data5 = data5;
	}
	public List getData3() {
		return data3;
	}
	public void setData3(List data3) {
		this.data3 = data3;
	}
	public List getData6() {
		return data6;
	}
	public void setData6(List data6) {
		this.data6 = data6;
	}
	public String[] getCitys() {
		return citys;
	}
	public void setCitys(String[] citys) {
		this.citys = citys;
	}
	public List getData2() {
		return data2;
	}
	public void setData2(List data2) {
		this.data2 = data2;
	}
	public String getPosTop() {
		return posTop;
	}
	public void setPosTop(String posTop) {
		this.posTop = posTop;
	}
	public String getPosLeft() {
		return posLeft;
	}
	public void setPosLeft(String posLeft) {
		this.posLeft = posLeft;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	
}
