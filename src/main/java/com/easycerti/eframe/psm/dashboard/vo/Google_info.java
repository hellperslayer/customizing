package com.easycerti.eframe.psm.dashboard.vo;

import com.easycerti.eframe.common.vo.AbstractValueObject;

public class Google_info extends AbstractValueObject{
	
	private int coord_type;
	
	private String coord_name;
	
	private String coordinate;
	
	private String coord_memo;
	
	private String coord_date;
	
	private int system_seq;

	public int getCoord_type() {
		return coord_type;
	}

	public void setCoord_type(int coord_type) {
		this.coord_type = coord_type;
	}

	public String getCoord_name() {
		return coord_name;
	}

	public void setCoord_name(String coord_name) {
		this.coord_name = coord_name;
	}

	public String getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(String coordinate) {
		this.coordinate = coordinate;
	}

	public String getCoord_memo() {
		return coord_memo;
	}

	public void setCoord_memo(String coord_memo) {
		this.coord_memo = coord_memo;
	}

	public String getCoord_date() {
		return coord_date;
	}

	public void setCoord_date(String coord_date) {
		this.coord_date = coord_date;
	}

	public int getSystem_seq() {
		return system_seq;
	}

	public void setSystem_seq(int system_seq) {
		this.system_seq = system_seq;
	}
}
