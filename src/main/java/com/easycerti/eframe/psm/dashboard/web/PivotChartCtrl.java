package com.easycerti.eframe.psm.dashboard.web;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.dashboard.service.PivotchartSvc;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.system_management.dao.AgentMngtDao;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

@Controller
@RequestMapping(value="/pivotchart/*")
public class PivotChartCtrl {
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private AgentMngtDao agentMngtDao;
	
	@Autowired
	private PivotchartSvc pivotchartSvc;
	
	@MngtActHist(log_action = "SELECT")
	@RequestMapping(value="pivotchart.html")
	public DataModelAndView setPivotchartView(@RequestParam Map<String, String> parameters, HttpServletRequest request, String type) {
		DataModelAndView modelAndView = new DataModelAndView();
		//parameters = CommonHelper.checkSearchDateByWeek(parameters);

		HttpSession session = request.getSession();
		// 다운로드로그 풀스캔연동여부
		String use_fullscan = (String) session.getAttribute("use_fullscan");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);
		Calendar calendar = Calendar.getInstance();
		
		if ((parameters.get("search_from") == null) && (parameters.get("search_to") == null)) {
			calendar.add(Calendar.DAY_OF_MONTH, -1);
			String search_to = sdf.format(calendar.getTime());
			parameters.put("search_to", search_to);
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			parameters.put("search_from", sdf.format(calendar.getTime()));
		}
		String index = "";
		if(type == null) {
			modelAndView.addObject("type", "1");
			index = "/pivotchart/pivotchart.html?type=1";
		}else {
			modelAndView.addObject("type", type);
			index = "/pivotchart/pivotchart.html?type="+type; 
		}
		if(type.equals("1")) {
			modelAndView.addObject("log_message", "[인텔리젼스] 처리현황(일자별/시스템별)");
			modelAndView.addObject("l_category", "인텔리젼스 > 처리현황(일자별/시스템별)");
		}else if(type.equals("2")) {
			modelAndView.addObject("log_message", "[인텔리젼스] 처리현황(취급자별)");
			modelAndView.addObject("l_category", "인텔리젼스 > 처리현황(취급자별)");
		}else if(type.equals("3")) {
			modelAndView.addObject("log_message", "[인텔리젼스] 처리현황(시스템별/유형별)");
			modelAndView.addObject("l_category", "인텔리젼스 > 처리현황(시스템별/유형별)");
		}else if(type.equals("4")) {
			modelAndView.addObject("log_message", "[인텔리젼스] 개인정보유형별분포현황");
			modelAndView.addObject("l_category", "인텔리젼스 > 개인정보유형");
		}else if(type.equals("5")) {
			modelAndView.addObject("log_message", "[인텔리젼스] 수행업무별분포현황");
			modelAndView.addObject("l_category", "인텔리젼스 > 수행업무별분포현황");
		}else if(type.equals("6")) {
			modelAndView.addObject("log_message", "[인텔리젼스] 처리현황(소속별)");
			modelAndView.addObject("l_category", "인텔리젼스 > 처리현황(소속별)");
		}else if(type.equals("7")) {
			modelAndView.addObject("log_message", "test");
		}else if(type.equals("8")) {
			modelAndView.addObject("log_message", "test");
		}else if(type.equals("9")) {
			modelAndView.addObject("log_message", "[인텔리젼스] 클라우드 CPU,MEM,HDD");
			modelAndView.addObject("l_category", "인텔리젼스 > 클라우드 CPU,MEM,HDD");
		}else if(type.equals("10")) {
			modelAndView.addObject("log_message", "test");
		}else if(type.equals("11")) {
			modelAndView.addObject("log_message", "[빅데이터분석] 비정상 위험부서 TOP 10");
			modelAndView.addObject("l_category", "빅데이터분석 > 비정상 위험부서 TOP 10");
		}else if(type.equals("12")) {
			modelAndView.addObject("log_message", "[빅데이터분석] 비정상 위험 유형별 현황");
			modelAndView.addObject("l_category", "빅데이터분석 > 비정상 위험 유형별 현황");
		}else if(type.equals("13")) {
			modelAndView.addObject("log_message", "test");
		}else if(type.equals("14")) {
			modelAndView.addObject("log_message", "test");
		}else if(type.equals("15")) {
			modelAndView.addObject("log_message", "[빅데이터분석] 비정상 위험 개인 TOP 10");
			modelAndView.addObject("l_category", "빅데이터분석 > 비정상 위험 개인 TOP 10");
		}
		SimpleCode simpleCode = new SimpleCode();
		simpleCode.setMenu_url(index);
		String index_id = commonDao.checkIndex2(simpleCode);
		
		SearchSearch search = new SearchSearch();
		List<String> auth_idsList = (List<String>) request.getSession().getAttribute("auth_ids_list");
		search.setAuth_idsList(auth_idsList);
		
		// 취급자별 차트만 해당
		if(type.equals("2")) {
			String pageInfo = parameters.get("page_num");
			if(pageInfo==null || pageInfo=="") {
				pageInfo = "1";
				search.setLogCtType("logCt");
			} else {
				search.setSystem_seq(parameters.get("system_seq"));
				search.setLogCtType(parameters.get("logCtType"));
				search.setSearchType(parameters.get("searchType"));
			}
			search.setSearch_from(parameters.get("search_from").replaceAll("-", ""));
			search.setSearch_to(parameters.get("search_to").replaceAll("-", ""));
			search.setPage_num(Integer.parseInt(pageInfo));
			search.setSize(15);
			search.setAuth_idsList(auth_idsList);
			Integer total_count;
			if(search.getSearchType()!= null && search.getSearchType().equals("03")) {
				total_count = pivotchartSvc.getDownloadLogChart2Ct(search);
			} else {
				total_count = pivotchartSvc.getNewDashboardChart2Ct(search);
			}
			search.setTotal_count(total_count);
			modelAndView.addObject("search", search);
		}
		
		//소속별 처리현황 챠트
		if(type.equals("6")) {
			String pageInfo = parameters.get("page_num");
			if(pageInfo==null || pageInfo=="") {
				pageInfo = "1";
				search.setLogCtType("logCt");
			} else {
				search.setSystem_seq(parameters.get("system_seq"));
				search.setLogCtType(parameters.get("logCtType"));
				search.setSearchType(parameters.get("searchType"));
			}
			search.setSearch_from(parameters.get("search_from").replaceAll("-", ""));
			search.setSearch_to(parameters.get("search_to").replaceAll("-", ""));
			search.setPage_num(Integer.parseInt(pageInfo));
			search.setSize(15);
			search.setAuth_idsList(auth_idsList);
			Integer total_count;
			if(search.getSearchType()!= null && search.getSearchType().equals("03")) {
				total_count = pivotchartSvc.getDownloadLogChart6Ct(search);
			} else {
				total_count = pivotchartSvc.getNewDashboardChart6Ct(search);
			}
			search.setTotal_count(total_count);
			modelAndView.addObject("search", search);
		}

		
		// ------------------
		
		List<SystemMaster> systemList = agentMngtDao.findSystemMasterList_byAdmin(search);
		
		if(type.equals("9")) {
			List<Map<String, String>> serverList = agentMngtDao.findServerSeqList();
			modelAndView.addObject("serverList", serverList);
		}
		modelAndView.addObject("today", sdf.format(new Date()));
		modelAndView.addObject("systemList", systemList);
		modelAndView.addObject("index_id", index_id);
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("use_fullscan", use_fullscan);
		
		modelAndView.setViewName("pivotchart");
				
		return modelAndView;
	}
	
	
	
}
