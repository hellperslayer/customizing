package com.easycerti.eframe.psm.dashboard.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.easycerti.eframe.psm.dashboard.dao.PivotchartDao;
import com.easycerti.eframe.psm.dashboard.service.PivotchartSvc;
import com.easycerti.eframe.psm.dashboard.vo.Dashboard;
import com.easycerti.eframe.psm.search.vo.SearchSearch;

/**
 * 대시보드 관련 Service Implements
 * 
 * @author yjyoo
 * @since 2015. 5. 12.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 12.           yjyoo            최초 생성
 *
 * </pre>
 */
@Repository
public class PivotchartSvcImpl implements PivotchartSvc {
	
	@Autowired
	private PivotchartDao pivotchartdao;
	
	@Override
	public List<Dashboard> getNewDashboardChart2(SearchSearch search) {
		
		return pivotchartdao.getNewDashboardChart2(search);
	}
	
	@Override
	public List<Dashboard> getNewDashboardChart3(Dashboard dashboard) {
		
		return pivotchartdao.getNewDashboardChart3(dashboard);
	}
	
	@Override
	public List<Dashboard> getNewDashboardChart4(Dashboard dashboard) {
		
		return pivotchartdao.getNewDashboardChart4(dashboard);
	}
	
	@Override
	public List<Dashboard> getNewDashboardChart5(Dashboard dashboard) {
		
		return pivotchartdao.getNewDashboardChart5(dashboard);
	}
	
	@Override
	public List<Dashboard> getNewDashboardChart6(Dashboard dashboard) {
		
		return pivotchartdao.getNewDashboardChart6(dashboard);
	}
	
	@Override
	public List<Dashboard> getNewDashboardChart7(Dashboard dashboard) {
		
		return pivotchartdao.getNewDashboardChart7(dashboard);
	}
	
	@Override
	public List<Dashboard> getNewDashboardChart8(Dashboard dashboard) {
		
		return pivotchartdao.getNewDashboardChart8(dashboard);
	}
	
	@Override
	public List<Dashboard> getNewDashboardChart9(Dashboard dashboard) {
		
		return pivotchartdao.getNewDashboardChart9(dashboard);
	}
	
	@Override
	public List<Dashboard> getNewDashboardChart10(Dashboard dashboard) {
		
		return pivotchartdao.getNewDashboardChart10(dashboard);
	}
	
	@Override
	public List<Dashboard> getNewDashboardChart11(Dashboard dashboard) {
		
		return pivotchartdao.getNewDashboardChart11(dashboard);
	}
	
	@Override
	public List<Dashboard> getNewDashboardChart12(Dashboard dashboard) {
		
		return pivotchartdao.getNewDashboardChart12(dashboard);
	}
	
	@Override
	public List<Dashboard> getNewDashboardChart13(Dashboard dashboard) {
		
		return pivotchartdao.getNewDashboardChart13(dashboard);
	}
	
	@Override
	public List<Dashboard> getNewDashboardChart14(Dashboard dashboard) {
		
		return pivotchartdao.getNewDashboardChart14(dashboard);
	}
	
	@Override
	public List<Dashboard> getDownloadLogChart1(Dashboard dashboard) {
		
		return pivotchartdao.getDownloadLogChart1(dashboard);
	}
	
	@Override
	public List<Dashboard> getDownloadLogChart2(SearchSearch search) {
		
		return pivotchartdao.getDownloadLogChart2(search);
	}
	
	@Override
	public List<Dashboard> getDownloadLogChart3(Dashboard dashboard) {
		
		return pivotchartdao.getDownloadLogChart3(dashboard);
	}
	
	@Override
	public List<Dashboard> getDownloadLogChart4(Dashboard dashboard) {
		
		return pivotchartdao.getDownloadLogChart4(dashboard);
	}
	
	@Override
	public List<Dashboard> getDownloadLogChart5(Dashboard dashboard) {
		
		return pivotchartdao.getDownloadLogChart5(dashboard);
	}
	
	@Override
	public List<Dashboard> getDownloadLogChart6(Dashboard dashboard) {
		
		return pivotchartdao.getDownloadLogChart6(dashboard);
	}

	@Override
	public Integer getNewDashboardChart2Ct(SearchSearch search) {
		return pivotchartdao.getNewDashboardChart2Ct(search);
	}

	@Override
	public Integer getDownloadLogChart2Ct(SearchSearch search) {
		return pivotchartdao.getDownloadLogChart2Ct(search);
	}

	@Override
	public Integer getDownloadLogChart6Ct(SearchSearch search) {
		return pivotchartdao.getDownloadLogChart6Ct(search);
	}

	@Override
	public Integer getNewDashboardChart6Ct(SearchSearch search) {
		return pivotchartdao.getNewDashboardChart6Ct(search);
	}

	@Override
	public List<Dashboard> getDownloadLogChart6(SearchSearch search) {
		return pivotchartdao.getDownloadLogChart6(search);
	}

	@Override
	public List<Dashboard> getNewDashboardChart6(SearchSearch search) {
		return pivotchartdao.getNewDashboardChart6(search);
	}
	
	
	
}
