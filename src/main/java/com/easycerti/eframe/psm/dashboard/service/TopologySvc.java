package com.easycerti.eframe.psm.dashboard.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.easycerti.eframe.common.spring.DataModelAndView;

/**
 * 토폴로지 관련 Service Interface
 * 
 * @author yjyoo
 * @since 2015. 5. 4.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 4.           yjyoo            최초 생성
 *
 * </pre>
 */
public interface TopologySvc {
	
	/**
	 * 토폴로지 검색조건 Init (첫 진입 시)
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 6.
	 * @return DataModelAndView
	 */
	public DataModelAndView setInitTopologySearch(Map<String, String> parameters);
	
	/**
	 * 토폴로지 1 Depth
	 * 
	 * @author yjyoo
	 * @param request 
	 * @since 2015. 5. 4.
	 * @return DataModelAndView
	 */
	public DataModelAndView findTopologyList_1depth(Map<String, String> parameters, HttpServletRequest request);
	
	/**
	 * 토폴로지 2 Depth
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 6.
	 * @return DataModelAndView
	 */
	public DataModelAndView findTopologyList_2depth(Map<String, String> parameters);
	
	/**
	 * 토폴로지 3 Depth
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 7.
	 * @return DataModelAndView
	 */
	public DataModelAndView findTopologyList_3depth(Map<String, String> parameters);
	
}
