package com.easycerti.eframe.psm.dashboard.service.impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.easycerti.eframe.psm.dashboard.dao.DashboardMapDao;
import com.easycerti.eframe.psm.dashboard.service.DashboardMapSvc;
import com.easycerti.eframe.psm.dashboard.vo.DeptInfo_map;
import com.easycerti.eframe.psm.dashboard.vo.MapInfo;

@Repository
public class DashboardMapSvcImpl implements DashboardMapSvc {
	
	@Autowired
	DashboardMapDao dashboardMapDao;
	
	@Override
	public HashMap initMap() {
		
		HashMap res = new HashMap();
		MapInfo mapInfo = dashboardMapDao.findDashboardMap();
		
		List<DeptInfo_map> deptInfo = dashboardMapDao.findDeptInfo(mapInfo.getMap_name());
		
		res.put("mapInfo", mapInfo);
		res.put("deptInfo", deptInfo);
		
		return res;
	}
}

