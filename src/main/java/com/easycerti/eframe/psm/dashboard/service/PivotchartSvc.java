package com.easycerti.eframe.psm.dashboard.service;

import java.util.List;

import com.easycerti.eframe.psm.dashboard.vo.Dashboard;
import com.easycerti.eframe.psm.search.vo.SearchSearch;

/**
 * 대시보드 관련 Service Interface
 * 
 * @author yjyoo
 * @since 2015. 5. 12.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 12.           yjyoo           최초 생성
 *   2015. 5. 15.			yjyoo			수집 서버 현황 대시보드 추가
 *   2015. 5. 26.			yjyoo			부서별 로그 수집 현황 대시보드 추가
 *
 * </pre>
 */
public interface PivotchartSvc {
	public List<Dashboard> getNewDashboardChart2(SearchSearch search);
	public Integer getNewDashboardChart2Ct(SearchSearch search);
	
	public List<Dashboard> getNewDashboardChart3(Dashboard dashboard);
	public List<Dashboard> getNewDashboardChart4(Dashboard dashboard);
	public List<Dashboard> getNewDashboardChart5(Dashboard dashboard);
	public List<Dashboard> getNewDashboardChart6(Dashboard dashboard);
	public List<Dashboard> getNewDashboardChart7(Dashboard dashboard);
	public List<Dashboard> getNewDashboardChart8(Dashboard dashboard);
	public List<Dashboard> getNewDashboardChart9(Dashboard dashboard);
	public List<Dashboard> getNewDashboardChart10(Dashboard dashboard);
	public List<Dashboard> getNewDashboardChart11(Dashboard dashboard);
	public List<Dashboard> getNewDashboardChart12(Dashboard dashboard);
	public List<Dashboard> getNewDashboardChart13(Dashboard dashboard);
	public List<Dashboard> getNewDashboardChart14(Dashboard dashboard);
	
	public List<Dashboard> getDownloadLogChart1(Dashboard dashboard);
	public List<Dashboard> getDownloadLogChart2(SearchSearch search);
	public Integer getDownloadLogChart2Ct(SearchSearch search);
	public List<Dashboard> getDownloadLogChart3(Dashboard dashboard);
	public List<Dashboard> getDownloadLogChart4(Dashboard dashboard);
	public List<Dashboard> getDownloadLogChart5(Dashboard dashboard);
	public List<Dashboard> getDownloadLogChart6(Dashboard dashboard);
	public Integer getDownloadLogChart6Ct(SearchSearch search);
	public Integer getNewDashboardChart6Ct(SearchSearch search);
	public List<Dashboard> getDownloadLogChart6(SearchSearch search);
	public List<Dashboard> getNewDashboardChart6(SearchSearch search);
}

