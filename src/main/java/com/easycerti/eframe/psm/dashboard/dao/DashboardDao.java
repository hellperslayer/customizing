package com.easycerti.eframe.psm.dashboard.dao;

import java.util.List;

import com.easycerti.eframe.psm.dashboard.vo.Dashboard;
import com.easycerti.eframe.psm.search.vo.AllLogInq;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInq;

/**
 * 대시보드 관련 Dao Interface
 * 
 * @author yjyoo
 * @since 2015. 5. 12.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 12.           yjyoo           최초 생성
 *   2015. 5. 15.			yjyoo			수집 서버 현황 대시보드 추가
 *   2015. 5. 26.			yjyoo			부서별 로그 수집 현황 대시보드 추가
 *
 * </pre>
 */
public interface DashboardDao {
	
	/**
	 * 대시보드 1 - 최근 전체 로그 건수 > 전체 수집 로그 건수, 개인정보 처리 로그 건수
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 12.
	 * @return List<Dashboard>
	 */
	public List<Dashboard> findDashboardList_chart1(Dashboard dashboard);

	/**
	 * 대시보드 2 - 최근 수집 로그 TOP10
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 12.
	 * @return List<Dashboard>
	 */
	public List<AllLogInq> findDashboardList_chart2(Dashboard dashboard);
	
	/**
	 * 대시보드_chart2 - 개인정보 접속기록 로그 TOP5(일간)
	 * 
	 * @author syjung
	 * @since 2020. 2. 6.
	 * @return List<Dashboard>
	 */
	public List<AllLogInq> findUserAccessDate(Dashboard dashboard);
	
	/**
	 * 대시보드_chart2 - 개인정보 접속기록 로그 TOP5(월간)
	 * 
	 * @author syjung
	 * @since 2020. 2. 10.
	 * @return List<Dashboard>
	 */
	public List<AllLogInq> findUserAccessMonth(Dashboard dashboard);
	
	public List<AllLogInq> findDashboardList_recent(Dashboard dashboard);
	
	/**
	 * 대시보드 3 - 시스템별 로그 수집 현황
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 12.
	 * @return List<Dashboard>
	 */
	public List<Dashboard> findDashboardList_chart3(Dashboard dashboard);
	
	/**
	 * 대시보드_chart3 - 시스템별 접속기록 TOP5(일간)
	 * 
	 * @author syjung
	 * @since 2020. 2. 6.
	 * @return List<Dashboard>
	 */
	public List<Dashboard> findSysAccessDate(Dashboard dashboard);
	
	/**
	 * 대시보드_chart3 - 시스템별 접속기록 TOP5(월간)
	 * 
	 * @author syjung
	 * @since 2020. 2. 6.
	 * @return List<Dashboard>
	 */
	public List<Dashboard> findSysAccessMonth(Dashboard dashboard);
	
	/**
	 * 대시보드 4 - 개인정보 유형별 로그 수집 현황
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 12.
	 * @return List<Dashboard>
	 */
	public List<Dashboard> findDashboardList_chart4(Dashboard dashboard);
	
	/**
	 * 대시보드 4 - 대시보드 4 - 개인정보 유형별 접속기록 현황(일간)
	 * 
	 * @author syjung
	 * @since 2020. 2. 6.
	 * @return List<Dashboard>
	 */
	public List<Dashboard> findTypeAccessDate(Dashboard dashboard);
	
	/**
	 * 대시보드 4 - 대시보드 4 - 개인정보 유형별 접속기록 현황(월간)
	 * 
	 * @author syjung
	 * @since 2020. 2. 6.
	 * @return List<Dashboard>
	 */
	public List<Dashboard> findTypeAccessMonth(Dashboard dashboard);
	
	/**
	 * 대시보드 5 - 추출 로그 추이 분석 현황
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 12.
	 * @return List<Dashboard>
	 */
	public List<Dashboard> findDashboardList_chart5(Dashboard dashboard);
	
	/**
	 * 대시보드 6 - 추출조건별 추출로그 현황
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 12.
	 * @return List<Dashboard>
	 */
	public List<Dashboard> findDashboardList_chart6(Dashboard dashboard);
	public List<Dashboard> findDashboardList_chart6_detail(Dashboard dashboard);
	
	/**
	 * 대시보드 7 - 부서별 로그 수집 현황
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 26.
	 * @return List<Dashboard>
	 */
	public List<Dashboard> findDashboardList_chart7(Dashboard dashboard);
	
	/**
	 * 대시보드 7 - 부서별 로그 수집 현황(일간)
	 * 
	 * @author syjung
	 * @since 2020. 2. 7.
	 * @return List<Dashboard>
	 */
	public List<Dashboard> findDeptLogDate(Dashboard dashboard);
	
	/**
	 * 대시보드 7 - 부서별 로그 수집 현황(월간)
	 * 
	 * @author syjung
	 * @since 2020. 2. 7.
	 * @return List<Dashboard>
	 */
	public List<Dashboard> findDeptLogMonth(Dashboard dashboard);
	
	/**
	 * 대시보드 8 - 수집 서버 현황
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 15.
	 * @return List<Dashboard>
	 */
	public List<Dashboard> findDashboardList_chart8(Dashboard dashboard);
	public List<Dashboard> findDashboardList_chart8_stack(Dashboard dashboard);
	public ExtrtCondbyInq findDashboardList_chart_emp(Dashboard dashboard);
	
	public void findDashboardGoogleList_add(Dashboard dashboard);

	public List<Dashboard> findDashboardList();

	public void findDashboardListDelete(Dashboard paramBean);

	public void findDashboardListUpdate(Dashboard paramBean);

	public List<Dashboard> extract_name(Dashboard dashboard);
	
	public List<Dashboard> extract_chart(Dashboard dashboard);

	public List<Dashboard> getNewDashboardChart1();
	public List<Dashboard> getNewDashboardChart1(Dashboard dashboard);
	public List<Dashboard> getNewDashboardChart2();
	public List<Dashboard> getNewDashboardChart3();
	public List<Dashboard> getNewDashboardChart4();
	public List<Dashboard> getNewDashboardChart5();
	public List<Dashboard> getNewDashboardChart6();
	public List<Dashboard> getNewDashboardChart7();
	public List<Dashboard> getNewDashboardChart8();
	public List<Dashboard> getNewDashboardChart9();
	public List<Dashboard> getNewDashboardChart10();
	public List<Dashboard> getNewDashboardChart11();
	public List<Dashboard> getNewDashboardChart12();
	public List<Dashboard> getNewDashboardChart13();
	
	public String getUseDashboardScenario();
	
	// data board UI 개편
	public List<AllLogInq> getDataBoardChart1Rt(Dashboard dashboard);
	public List<AllLogInq> getDataBoardChart1Daily(Dashboard dashboard);
	public List<AllLogInq> getDataBoardChart1Month(Dashboard dashboard);
	
	public List<AllLogInq> getDataBoardChart2Rt(Dashboard dashboard);
	public List<AllLogInq> getDataBoardChart2Daily(Dashboard dashboard);
	public List<AllLogInq> getDataBoardChart2Month(Dashboard dashboard);
	
	public List<AllLogInq> getDataBoardChart3Rt(Dashboard dashboard);
	public List<AllLogInq> getDataBoardChart3Daily(Dashboard dashboard);
	public List<AllLogInq> getDataBoardChart3Month(Dashboard dashboard);
	
	public List<AllLogInq> getDataBoardChart4Rt(Dashboard dashboard);
	public List<AllLogInq> getDataBoardChart4Daily(Dashboard dashboard); // 추가 가공 필요
	public List<AllLogInq> getDataBoardChart4Month(Dashboard dashboard); // 추가 가공 필요
	
	public List<Dashboard> getDataBoardChart5();

	public List<AllLogInq> getDataBoardChart1Rt_partition(Dashboard dashboard);
	public List<AllLogInq> getDataBoardChart2Rt_partition(Dashboard dashboard);
	public List<AllLogInq> getDataBoardChart3Rt_partition(Dashboard dashboard);
	public List<AllLogInq> getDataBoardChart4Rt_partition(Dashboard dashboard);

	public List<Dashboard> getDataBoardChart5_serverPart();
}
