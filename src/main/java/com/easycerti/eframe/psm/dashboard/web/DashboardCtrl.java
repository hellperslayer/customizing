package com.easycerti.eframe.psm.dashboard.web;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.GetStatisticsTypeData;
import com.easycerti.eframe.common.vo.PrivacyType;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.dashboard.dao.DashboardDao;
import com.easycerti.eframe.psm.dashboard.dao.PivotchartDao;
import com.easycerti.eframe.psm.dashboard.service.DashboardMapSvc;
import com.easycerti.eframe.psm.dashboard.service.DashboardSvc;
import com.easycerti.eframe.psm.dashboard.service.PivotchartSvc;
import com.easycerti.eframe.psm.dashboard.vo.Dashboard;
import com.easycerti.eframe.psm.report.service.ReportSvc;
import com.easycerti.eframe.psm.search.dao.AllLogInqDao;
import com.easycerti.eframe.psm.search.dao.ExtrtCondbyInqDao;
import com.easycerti.eframe.psm.search.vo.SearchSearch;
import com.easycerti.eframe.psm.setup.dao.IndvinfoTypeSetupDao;
import com.easycerti.eframe.psm.setup.vo.ExtrtBaseSetup;
import com.easycerti.eframe.psm.system_management.dao.AgentMngtDao;
import com.easycerti.eframe.psm.system_management.vo.EmpUser;
import com.easycerti.eframe.psm.system_management.vo.PrivacyReport;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

/**
 * 대시보드 관련 Controller
 * 
 * @author yjyoo
 * @since 2015. 5. 4.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 4.           yjyoo            최초 생성
 *   2015. 5. 12.		   yjyoo			대시보드 4개 항목 추가
 *   2015. 5. 14.		   yjyoo			대시보드 2개 항목 추가
 *   2015. 5. 15.		   yjyoo			대시보드 2개 항목 추가
 *   2015. 5. 26.		   yjyoo			대시보드 1개 항목 추가
 *
 * </pre>
 */
@Controller
@RequestMapping(value="/dashboard/*")
public class DashboardCtrl {
	
	@Autowired
	private DashboardSvc dashboardSvc;
	
	@Autowired
	private AgentMngtDao agentMngtDao;
	
	@Autowired
	private DashboardDao dashboardDao;
	
	@Autowired
	private IndvinfoTypeSetupDao indvinfoTypeSetupDao;
	
	@Autowired
	private DashboardMapSvc dashboardMapSvc;
	
	@Autowired
	private PivotchartSvc pivotchartSvc;
	
	@Autowired
	private PivotchartDao divotchartDao;
	
	@Autowired
	private ExtrtCondbyInqDao extrtCondbyInqDao;
	
	@Autowired
	private ReportSvc reportSvc;
	
	@Autowired
	private AllLogInqDao allLogInqDao;
	
	@Autowired
	private CommonDao commonDao;
	
	/**
	 * 상황판 화면 가져오기 (기본 대시보드 탭 셋팅)
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 4.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="init_dashboardView_old.html", method={RequestMethod.POST})
	public DataModelAndView setDashboardView_old(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		Dashboard paramBean = CommonHelper.convertMapToBean(parameters, Dashboard.class);
		DataModelAndView modelAndView = new DataModelAndView();	
		
		HttpSession session = request.getSession();
		String master = (String)session.getAttribute("master");
		if(master.equals("Y"))	
			modelAndView.setViewName("dashboard2");
		else
			modelAndView.setViewName("dashboard2_nomaster");
		
		//modelAndView.setViewName("dashboardCharts");
		modelAndView.addObject("mainFlag", CommonResource.YES);
		//modelAndView.addObject("tabFlag", "tab1");
		modelAndView.addObject("mainMenuId", "MENU00030");
		//modelAndView.addObject("rescentList", dashboardDao.findDashboardList_recent(paramBean));
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
		if (list == null) {
			list = (List<String>) request.getSession().getAttribute("auth_ids_list");
		}
		SearchSearch search = new SearchSearch();
		search.setAuth_idsList(list);
		modelAndView.addObject("systemList", agentMngtDao.findSystemMasterList_byAdmin(search));
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	@MngtActHist(log_action = "SELECT", log_message = "[모니터링] 데이터보드(구)")
	@RequestMapping(value="init_dashboardView_new.html", method={RequestMethod.POST})
	public DataModelAndView setDashboardView_test(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		Dashboard paramBean = CommonHelper.convertMapToBean(parameters, Dashboard.class);
		DataModelAndView modelAndView = new DataModelAndView();
		
		HttpSession session = request.getSession();
		String master = (String)session.getAttribute("master");
		if(master.equals("Y")) {
			modelAndView.setViewName("dashboard2");
		} else {
			modelAndView.setViewName("dashboard2_nomaster");
		}
		//modelAndView.setViewName("dashboardCharts");
//		modelAndView.addObject("mainFlag", CommonResource.YES);
//		modelAndView.addObject("mainMenuId", "MENU00030");
		//modelAndView.addObject("rescentList", dashboardDao.findDashboardList_recent(paramBean));
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
		if (list == null) {
			list = (List<String>) request.getSession().getAttribute("auth_ids_list");
		}
		SimpleCode simpleCode = new SimpleCode("/dashboard/init_dashboardView_new.html");
		String index_id = commonDao.checkIndex(simpleCode);
		SearchSearch search = new SearchSearch();
		modelAndView.addObject("index_id", index_id);
		search.setAuth_idsList(list);
		modelAndView.addObject("systemList", agentMngtDao.findSystemMasterList_byAdmin(search));
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	/**
	 * 20200508 데이터보드(구 대시보드) 페이지 신규 페이지 디자인
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[모니터링] 데이터보드(신)")
	@RequestMapping(value="init_dashboardView.html", method={RequestMethod.POST})
	public DataModelAndView setDashboardView(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = dashboardSvc.setDashboardView(parameters,request);	
		
		return modelAndView;
	}
	
	@RequestMapping(value="init_dashboardView_chart.html", method={RequestMethod.POST})
	public DataModelAndView setDashboardView_new(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		Dashboard paramBean = CommonHelper.convertMapToBean(parameters, Dashboard.class);
		DataModelAndView modelAndView = new DataModelAndView();	
		//modelAndView.setViewName("dashboard");
		
		
		modelAndView.setViewName("dashboardCharts");
		modelAndView.addObject("mainFlag", CommonResource.YES);
		modelAndView.addObject("tabFlag", "tab1");
		modelAndView.addObject("mainMenuId", "MENU00030");
		modelAndView.addObject("rescentList", dashboardDao.findDashboardList_recent(paramBean));
		modelAndView.addObject("systemMasterList", agentMngtDao.findSystemMasterList());
		
		return modelAndView;
	}
	@RequestMapping(value="init_dashboardView_map.html", method={RequestMethod.POST})
	public DataModelAndView setDashboardView_map(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		Dashboard paramBean = CommonHelper.convertMapToBean(parameters, Dashboard.class);
		DataModelAndView modelAndView = new DataModelAndView();	
		//modelAndView.setViewName("dashboard");
		modelAndView.setViewName("dashboardMap");
		//modelAndView.addObject("mainFlag", CommonResource.YES);
		//modelAndView.addObject("tabFlag", "tab1");
		//modelAndView.addObject("mainMenuId", "MENU00030");
		//modelAndView.addObject("rescentList", dashboardDao.findDashboardList_recent(paramBean));
		//modelAndView.addObject("systemMasterList", agentMngtDao.findSystemMasterList());
		
		return modelAndView;
	}
	
	@RequestMapping(value="init_dashboardView_pivot.html", method={RequestMethod.POST})
	public DataModelAndView setDashboardPivotView(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		Dashboard paramBean = CommonHelper.convertMapToBean(parameters, Dashboard.class);
		DataModelAndView modelAndView = new DataModelAndView();	
		modelAndView.setViewName("dashboardPivot");
		modelAndView.addObject("mainFlag", CommonResource.YES);
		modelAndView.addObject("tabFlag", "tab1");
		modelAndView.addObject("paramBean", parameters);
		modelAndView.addObject("mainMenuId", "MENU00445");	
//		modelAndView.addObject("mainMenuId", "MENU00050");	
		return modelAndView;
	}
	
	
	
	
	@RequestMapping(value="init_dashboardViewGoogle.html", method={RequestMethod.POST})
	public DataModelAndView setDashboardViewTopology(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView();
		
		modelAndView.setViewName("dashboardGoogle");
		modelAndView.addObject("mainFlag", CommonResource.YES);
		modelAndView.addObject("tabFlag", "tab1");
		modelAndView.addObject("mainMenuId", "MENU00443");
		modelAndView.addObject("systemMasterList", agentMngtDao.findSystemMasterList());
		
		return modelAndView;
	}
	
	@RequestMapping(value="dashboardPopView.html", method={RequestMethod.POST})
	public DataModelAndView setDashboardPopView(@RequestParam Map<String, String> parameters,String dstype,String select_system_seq ,HttpServletRequest request) {
		DataModelAndView modelAndView = new DataModelAndView();
		Dashboard paramBean = CommonHelper.convertMapToBean(parameters, Dashboard.class);

		modelAndView.setViewName("dashboardPop");
		modelAndView.addObject("mainFlag", CommonResource.YES);
		modelAndView.addObject("dstype", dstype);
		modelAndView.addObject("paramBean", paramBean);
		modelAndView.addObject("rescentList", dashboardDao.findDashboardList_recent(paramBean));
		modelAndView.addObject("select_system_seq", select_system_seq);
		return modelAndView;
	}
	
	/**
	 * 대시보드 1 - 최근 전체 로그 건수 > 전체 수집 로그 건수, 개인정보 처리 로그 건수
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 14.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="dashboard_chart1.html", method={RequestMethod.POST})
	public DataModelAndView findDashboardList_chart1(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = dashboardSvc.findDashboardList_chart1(parameters);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@RequestMapping(value="dashboard_recent.html", method={RequestMethod.POST})
	public DataModelAndView findDashboardList_recent(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = dashboardSvc.findDashboardList_recent(parameters);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	/**
	 * 대시보드 2 - 실시간 차트
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 14.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="dashboard_chart2.html", method={RequestMethod.POST})
	public DataModelAndView findDashboardList_chart1_real(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = dashboardSvc.findDashboardList_chart2(parameters, request);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}

	/**
	 * 대시보드 3 - 시스템별 로그 수집 현황
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 12.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="dashboard_chart3.html", method={RequestMethod.POST})
	public DataModelAndView findDashboardList_chart3(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		
		DataModelAndView modelAndView = dashboardSvc.findDashboardList_chart3(parameters, request);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	/**
	 * 대시보드 4 - 개인정보 유형별 로그 수집 현황
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 12.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="dashboard_chart4.html", method={RequestMethod.POST})
	public DataModelAndView findDashboardList_chart4(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = dashboardSvc.findDashboardList_chart4(parameters, request);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	/**
	 * 대시보드 5 - 추출 로그 추이 분석 현황
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 12.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="dashboard_chart5.html", method={RequestMethod.POST})
	public DataModelAndView findDashboardList_chart5(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = dashboardSvc.findDashboardList_chart5(parameters, request);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	/**
	 * 대시보드 6 - 추출조건별 추출로그 현황
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 12.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="dashboard_chart6.html", method={RequestMethod.POST})
	public DataModelAndView findDashboardList_chart6(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = dashboardSvc.findDashboardList_chart6(parameters, request);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	/**
	 * 대시보드 7 - 부서별 로그 수집 현황
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 26.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="dashboard_chart7.html", method={RequestMethod.POST})
	public DataModelAndView findDashboardList_chart7(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = dashboardSvc.findDashboardList_chart7(parameters, request);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	/**
	 * 대시보드 8 - 수집 서버 현황
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 15.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="dashboard_chart8.html", method={RequestMethod.POST})
	public DataModelAndView findDashboardList_chart8(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = dashboardSvc.findDashboardList_chart8(parameters, request);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	/**
	 * 대시보드 - 비정상행위 실시간 현황
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 15.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="dashboard_chart_emp.html", method={RequestMethod.POST})
	public DataModelAndView findDashboardList_chart_emp(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = dashboardSvc.findDashboardList_chart_emp(parameters);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	////////////구글맵
	@RequestMapping(value="item.html",method={RequestMethod.POST})
	public @ResponseBody List<Dashboard> findAuthMngtList(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		List<Dashboard> items = dashboardDao.findDashboardList();
	
		return items;
	}
	
	////////////구글맵 리스트 삭제
	@RequestMapping(value="delete.html",method={RequestMethod.POST})
	public DataModelAndView findAuthMngtListDelete(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		System.out.println("delete");
		
		DataModelAndView modelAndView = dashboardSvc.findDashboardListDelete(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	//구글맵 수정
	@RequestMapping(value="update.html",method={RequestMethod.POST})
	public DataModelAndView findAuthMngtListUpdate(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		System.out.println("update");
		
		DataModelAndView modelAndView = dashboardSvc.findDashboardListUpdate(parameters);
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	@RequestMapping(value="extract_chart.html", method={RequestMethod.POST})
	public DataModelAndView extract_chart(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
		DataModelAndView modelAndView = dashboardSvc.extract_chart(parameters);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	////////////구글맵 리스트 호출
	
	public JSONObject getInitPrivType(JSONObject jsonObj) {
		
		jsonObj.put("priv1", 0);
		jsonObj.put("priv2", 0);
		jsonObj.put("priv3", 0);
		jsonObj.put("priv4", 0);
		jsonObj.put("priv5", 0);
		jsonObj.put("priv6", 0);
		jsonObj.put("priv7", 0);
		jsonObj.put("priv8", 0);
		jsonObj.put("priv9", 0);
		jsonObj.put("priv10", 0);
		
		jsonObj.put("privDesc1", "주민등록번호");
		jsonObj.put("privDesc2", "운전면허번호");
		jsonObj.put("privDesc3", "여권번호");
		jsonObj.put("privDesc4", "신용카드번호");
		jsonObj.put("privDesc5", "건강보험번호");
		jsonObj.put("privDesc6", "전화번호");
		jsonObj.put("privDesc7", "이메일");
		jsonObj.put("privDesc8", "휴대전화번호");
		jsonObj.put("privDesc9", "계좌번호");
		jsonObj.put("privDesc10", "외국인등록번호");
		
		return jsonObj;
	}
	
	@RequestMapping(value="new_dashboard_chart1.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray newDashboardChart1(
			@RequestParam Map<String, String> parameters, HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "startdate", required = false) String startdate,
			@RequestParam(value = "enddate", required = false) String enddate,
			@RequestParam(value = "searchType", required = false) String searchType,
			@RequestParam(value = "logCtType", required = false) String logCtType) throws Exception {
		
		List<String> auth_idsList = (List<String>) request.getSession().getAttribute("auth_ids_list");
		auth_idsList.add("");
		SearchSearch search = new SearchSearch();
		search.setAuth_idsList(auth_idsList);
		List<SystemMaster> SystemMasterList = indvinfoTypeSetupDao.getSystemMasterListByAuth(search);
		
		JSONObject dataProvider=new JSONObject();
		JSONArray graphsArray = new JSONArray();
		
		for ( int i=0; i<SystemMasterList.size(); ++i ) {
			String strKey="system_name"+SystemMasterList.get(i).getSystem_seq();
			dataProvider.put(strKey,0);
			JSONObject graphs=new JSONObject();
			graphs.put("balloonText","[[title]]: [[value]]");
			graphs.put("bullet","round");
			graphs.put("title",SystemMasterList.get(i).getSystem_name());
			graphs.put("valueField",strKey);
			graphs.put("fillAlphas",0);
			graphsArray.add(graphs);
		}
		
		List<Dashboard> beanList;
		if(startdate == null) {
			beanList = dashboardSvc.getNewDashboardChart1();
		} else {
			Dashboard dashboard = new Dashboard();
			dashboard.setAuth_idsList(auth_idsList);
			dashboard.setSearch_from(startdate.replaceAll("-", ""));
			dashboard.setSearch_to(enddate.replaceAll("-", ""));
			dashboard.setSearchType(searchType);
			dashboard.setLogCtType(logCtType);
			if (searchType != null && searchType.equals("03")) { 
				beanList = pivotchartSvc.getDownloadLogChart1(dashboard);
			} else {
				beanList = dashboardSvc.getNewDashboardChart1(dashboard);
			}
		}
		
		JSONArray jArray = new JSONArray();
		String privProcDate="";
		JSONObject jsonObj=new JSONObject();
		if(0 < beanList.size()) {
			for ( int i=0; i<beanList.size(); ++i ) {
				Dashboard temp = beanList.get(i);
				
				String proc_date=temp.getProc_date();
				String system_seq=temp.getSystem_seq();
				String system_name=temp.getSystem_name();
				int privCount=temp.getnPrivCount();
				String strKey="system_name"+temp.getSystem_seq();
				
				if ( privProcDate.length() == 0 ) {
					privProcDate=proc_date;
					jsonObj.put("proc_date", proc_date);				
					dataProvider.put(strKey,privCount);
					dataProvider.put("proc_date", proc_date);	
					
				} else if (proc_date.equals(privProcDate)) {
					//같을떄
					privProcDate=proc_date;
					dataProvider.put(strKey,privCount);
					dataProvider.put("proc_date", proc_date);
				} else {
					//다를때
					jsonObj.put("dataProvider", dataProvider);
					jsonObj.put("graphs", graphsArray);
					
					jArray.add(jsonObj);
					
					jsonObj=new JSONObject();
					
					dataProvider=new JSONObject();
					
					for ( int j=0; j<SystemMasterList.size(); ++j ) {
						String strKeyT="system_name"+SystemMasterList.get(j).getSystem_seq();
						dataProvider.put(strKeyT,0);
					}
					privProcDate=proc_date;
					jsonObj.put("proc_date", proc_date);				
					dataProvider.put(strKey,privCount);
					dataProvider.put("proc_date", proc_date);
					
				}	
			}
			jsonObj.put("dataProvider", dataProvider);
			jsonObj.put("graphs", graphsArray);
			jArray.add(jsonObj);
		}
		return jArray;
	}
	
	@RequestMapping(value="new_dashboard_chart2.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray newDashboardChart2(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "search_from", required = false) String search_from,
			@RequestParam(value = "search_to", required = false) String search_to,
			@RequestParam(value = "system_seq", required = false) String system_seq,
			@RequestParam(value = "searchType", required = false) String searchType,
			@RequestParam(value = "logCtType", required = false) String logCtType,
			@RequestParam(value = "page_num", required = false) String page_num) throws Exception {
		List<Dashboard> beanList;
		
		List<String> auth_idsList = (List<String>) request.getSession().getAttribute("auth_ids_list");
		
		SearchSearch search = new SearchSearch();
		search.setAuth_idsList(auth_idsList);
		search.setSearch_from(search_from.replaceAll("-", ""));
		search.setSearch_to(search_to.replaceAll("-", ""));
		search.setSystem_seq(system_seq);
		search.setLogCtType(logCtType);
		search.setSearchType(searchType);
		search.setSize(15);
		if(page_num != null && page_num !="") {
			search.setPage_num(Integer.parseInt(page_num));
		}
		if (searchType != null && searchType.equals("03")) { 
			beanList = pivotchartSvc.getDownloadLogChart2(search);
		} else {
			beanList = pivotchartSvc.getNewDashboardChart2(search);
		}
		
		String checkEmpNameMasking = commonDao.checkEmpNameMasking();
		if(checkEmpNameMasking.equals("Y")) {
			for (int i = 0; i < beanList.size(); i++) {
				if(!"".equals(beanList.get(i).getEmp_user_name())) {
					String empUserName = beanList.get(i).getEmp_user_name();
					StringBuilder builder = new StringBuilder(empUserName);
					builder.setCharAt(empUserName.length() - 2, '*');
					beanList.get(i).setEmp_user_name(builder.toString());
				}
			}
		}
			
		JSONArray jArray = new JSONArray();
	
		for ( int i=0; i<beanList.size(); ++i ) {
			Dashboard temp = beanList.get(i);
				
			String emp_user_id=temp.getEmp_user_id();
			String emp_user_name=temp.getEmp_user_name();
			int nPrivCount=temp.getnPrivCount();
			
			JSONObject jsonObj=new JSONObject();
			jsonObj.put("emp_user_id", emp_user_id);
			if(searchType.equals("02")) {
				jsonObj.put("emp_user_name", emp_user_id);
			} else {
				jsonObj.put("emp_user_name", emp_user_name+"("+emp_user_id+")");
			}
			jsonObj.put("nPrivCount", nPrivCount);
			jArray.add(jsonObj);	
		}
		return jArray;
	}
	
	@RequestMapping(value = "new_dashboard_chart3.html")
	@ResponseBody
	public JSONObject newDashboardChart3(
			@RequestParam(value = "startdate", required = false) String startdate, HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "enddate", required = false) String enddate,
			@RequestParam(value = "searchType", required = false) String searchType) {

		JSONObject jObj = new JSONObject();
		JSONArray dataProvider = new JSONArray();
		JSONArray graphs = new JSONArray();
		
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
		
		PrivacyReport pr = new PrivacyReport();
		pr.setStart_date(startdate.replaceAll("-", ""));
		pr.setEnd_date(enddate.replaceAll("-", ""));
		pr.setSearchType(searchType);
		if(list != null) {
			pr.setAuth_idsList(list);
		}else {
			list = (List<String>) request.getSession().getAttribute("auth_ids_list");
			pr.setAuth_idsList(list);
		}
		
		List<Code> resultTypes = commonDao.getPrivacyDescList();
		
		for(int i=0; i<resultTypes.size(); i++) {
			Code code = resultTypes.get(i);
			JSONObject obj = new JSONObject();
			obj.put("balloonText", "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>");
			obj.put("fillAlphas", 0.8);
			obj.put("labelText", "[[value]]");
			obj.put("title", code.getCode_name());
			obj.put("type", "column");
			obj.put("color", "#000000");
			obj.put("valueField", code.getResult_type_order());
			graphs.add(obj);
		}
		
		List<PrivacyReport> systemTypeResultCountTop10 = null;

		if (searchType != null && searchType.equals("03")) { 
			systemTypeResultCountTop10 = reportSvc.findSystemTypeDownloadResultCountTop10(pr);
		} else {
			systemTypeResultCountTop10 = reportSvc.findSystemTypeResultCountTop10(pr);
		}
		
		int resStr = 0;
		PrivacyReport report = null;
		for (int i = 0; i < systemTypeResultCountTop10.size(); i++) {
			JSONObject obj = new JSONObject();
			report = systemTypeResultCountTop10.get(i);
			report.setStart_date(startdate.replaceAll("-", ""));
			report.setEnd_date(enddate.replaceAll("-", ""));
			obj.put("category", report.getSystem_name());
			
			for(Code commonVo : resultTypes) {
				resStr = commonVo.getResult_type_order();
				int cntVal = GetStatisticsTypeData.getType(report, "type"+resStr);
				obj.put(resStr, cntVal);
			}
			dataProvider.add(obj);
		}
		
		jObj.put("dataProvider", dataProvider);
		jObj.put("graphs", graphs);
		
		return jObj;
	}
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="new_dashboard_chart4.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray newDashboardChart4(
			@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "startdate", required = false) String startdate,
			@RequestParam(value = "enddate", required = false) String enddate,
			@RequestParam(value = "system_seq", required = false) String system_seq,
			@RequestParam(value = "searchType", required = false) String searchType) throws Exception {
	
		List<Dashboard> beanList;
		if(startdate == null) {
			beanList = dashboardSvc.getNewDashboardChart4();
		} else {
			Dashboard dashboard = new Dashboard();
			List<String> auth_idsList = (List<String>) request.getSession().getAttribute("auth_ids_list");
			dashboard.setAuth_idsList(auth_idsList);
			dashboard.setSearch_from(startdate.replaceAll("-", ""));
			dashboard.setSearch_to(enddate.replaceAll("-", ""));
			dashboard.setSystem_seq(system_seq);
			dashboard.setSearchType(searchType);
			if (searchType != null && searchType.equals("03")) { 
				beanList = pivotchartSvc.getDownloadLogChart4(dashboard);
			} else {
				beanList = pivotchartSvc.getNewDashboardChart4(dashboard);
			}
		}
		
		JSONArray jArray = new JSONArray();
		JSONObject job = new JSONObject();
		int codeId = 0;
		String codeName = "";
		int cntVal = 0;
		List<Code> resultTypes = commonDao.getPrivacyDescList();
		for(int i=0; i<beanList.size(); i++) {
			Dashboard dashVo = beanList.get(i);
			for(Code codeVo : resultTypes) {
				codeId = codeVo.getResult_type_order();
				codeName = codeVo.getCode_name();
				cntVal = GetStatisticsTypeData.getType(dashVo, "type"+codeId);
				job = new JSONObject();
				job.put("privacy_seq", codeId);
				job.put("privacy_desc", codeName);
				job.put("nPrivCount", cntVal);
				jArray.add(job);
			}
		}
		return jArray;
	}
	
	@SuppressWarnings({ "unchecked", "unused" })
	@RequestMapping(value="new_dashboard_chart5.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONObject newDashboardChart5(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "startdate", required = false) String startdate,
			@RequestParam(value = "enddate", required = false) String enddate,
			@RequestParam(value = "system_seq", required = false) String system_seq,
			@RequestParam(value = "searchType", required = false) String searchType) throws Exception {
		/*
		List<Dashboard> beanList = null;
		JSONArray jArray = new JSONArray();
		
		if(startdate == null) {
			beanList = dashboardSvc.getNewDashboardChart5();
			String privProcDate="";
			JSONObject jsonObj=new JSONObject();
			
			for ( int i=0; i<beanList.size(); ++i ) {
				Dashboard temp = beanList.get(i);
				
				String proc_date=temp.getProc_date();
				String system_name=temp.getSystem_name();
				int result_type=temp.getPrivacy_seq();
				String privacy_desc=temp.getPrivacy_desc();
				
				String strKey = "priv"+result_type;
				
				if ( privProcDate.length() == 0 ) {
					jsonObj=getInitPrivType(jsonObj);
					privProcDate=proc_date;
					jsonObj.put("system_name", system_name);
					jsonObj.put("proc_date", proc_date);
					jsonObj.put("privacy_desc", privacy_desc);
					jsonObj.put(strKey, temp.getnPrivCount());
				} else if (proc_date.equals(privProcDate)) {
					//같을떄
					privProcDate=proc_date;
					jsonObj.put(strKey, temp.getnPrivCount());
				} else {
					//다를때
					privProcDate=proc_date;
					jArray.add(jsonObj);
					
					jsonObj=new JSONObject();
					jsonObj=getInitPrivType(jsonObj);
					
					jsonObj.put("system_name", system_name);
					jsonObj.put("privacy_desc", privacy_desc);
					jsonObj.put("proc_date", proc_date);
					jsonObj.put(strKey, temp.getnPrivCount());
					
				}	
			}
			jArray.add(jsonObj);
		}else {
			Dashboard dashboard = new Dashboard();
			List<String> auth_idsList = (List<String>) request.getSession().getAttribute("auth_ids_list");
			dashboard.setAuth_idsList(auth_idsList);
			dashboard.setSearch_from(startdate.replaceAll("-", ""));
			dashboard.setSearch_to(enddate.replaceAll("-", ""));
			dashboard.setSystem_seq(system_seq);
			if (searchType != null && searchType.equals("2")) { 
				beanList = pivotchartSvc.getDownloadLogChart5(dashboard);
			} else {
				beanList = pivotchartSvc.getNewDashboardChart5(dashboard);
			}
			
			for(int i=0; i<beanList.size(); i++) {
				Dashboard db = beanList.get(i);
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("req_type", db.getReq_type());
				jsonObj.put("cnt", db.getCnt());
				jArray.add(jsonObj);
			}
			
		}
		return jArray;
		*/
		
		List<String> auth_idsList = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
		if (auth_idsList == null) {
			auth_idsList = (List<String>) request.getSession().getAttribute("auth_ids_list");
		}
		
		Dashboard dashboard = new Dashboard();
		List<Dashboard> beanList = null;
		if(startdate == null) {
			beanList = dashboardSvc.getNewDashboardChart5();
		} else {
			dashboard.setAuth_idsList(auth_idsList);
			dashboard.setSearch_from(startdate.replaceAll("-", ""));
			dashboard.setSearch_to(enddate.replaceAll("-", ""));
			dashboard.setSystem_seq(system_seq);
			dashboard.setSearchType(searchType);
			if (searchType != null && searchType.equals("03")) { 
				beanList = pivotchartSvc.getDownloadLogChart5(dashboard);
			} else {
				beanList = pivotchartSvc.getNewDashboardChart5(dashboard);
			}
		}
		
		JSONObject jObj = new JSONObject();
		JSONArray dataProvider = new JSONArray();
		JSONArray graphs = new JSONArray();
		List<Code> reqTypeList = commonDao.findReqTypeList();
		for(int i=0; i<reqTypeList.size(); i++) {
			Code code = reqTypeList.get(i);
			JSONObject obj = new JSONObject();
			obj.put("balloonText", "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b>건</span>");
			obj.put("fillAlphas", 0.8);
			obj.put("labelText", "[[value]]");
			obj.put("title", code.getCode_name());
			obj.put("type", "column");
			obj.put("color", "#000000");
			obj.put("valueField", code.getCode_id());
			graphs.add(obj);
		}
		
		Dashboard dashVo = null;
		List<SystemMaster> systemList = commonDao.getSystemList(dashboard);
		for(int j=0; j<systemList.size();j++) {
			String systemName = systemList.get(j).getSystem_name();
			JSONObject obj = new JSONObject();
			obj.put("category", systemName);
			for(int i=0; i < beanList.size(); i++) {
				dashVo = beanList.get(i);
				if(systemName.equals(dashVo.getSystem_name())) {
					for(Code codeVo : reqTypeList) {
						String codeId = codeVo.getCode_id();
						if(codeId.equals(dashVo.getReq_type())) {
							obj.put(codeId, dashVo.getCnt());
						}
					}
				}
			}
			dataProvider.add(obj);
		}
		
		jObj.put("dataProvider", dataProvider);
		jObj.put("graphs", graphs);
		return jObj;
	}
	
	
	@RequestMapping(value="new_dashboard_chart6.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray newDashboardChart6(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "search_from", required = false) String search_from,
			@RequestParam(value = "search_to", required = false) String search_to,
			@RequestParam(value = "system_seq", required = false) String system_seq,
			@RequestParam(value = "searchType", required = false) String searchType,
			@RequestParam(value = "logCtType", required = false) String logCtType,
			@RequestParam(value = "page_num", required = false) String page_num) throws Exception {
	
		List<Dashboard> beanList;
		List<String> auth_idsList = (List<String>) request.getSession().getAttribute("auth_ids_list");
		
		SearchSearch search = new SearchSearch();
		search.setAuth_idsList(auth_idsList);
		search.setSearch_from(search_from.replaceAll("-", ""));
		search.setSearch_to(search_to.replaceAll("-", ""));
		search.setSystem_seq(system_seq);
		search.setLogCtType(logCtType);
		search.setSearchType(searchType);
		search.setSize(15);
		if(page_num != null && page_num !="") {
			search.setPage_num(Integer.parseInt(page_num));
		}
		if (searchType != null && searchType.equals("03")) { 
			beanList = pivotchartSvc.getDownloadLogChart6(search);
		} else {
			beanList = pivotchartSvc.getNewDashboardChart6(search);
		}
		
		String checkEmpNameMasking = commonDao.checkEmpNameMasking();
		if(checkEmpNameMasking.equals("Y")) {
			for (int i = 0; i < beanList.size(); i++) {
				if(!"".equals(beanList.get(i).getEmp_user_name())) {
					String empUserName = beanList.get(i).getEmp_user_name();
					StringBuilder builder = new StringBuilder(empUserName);
					builder.setCharAt(empUserName.length() - 2, '*');
					beanList.get(i).setEmp_user_name(builder.toString());
				}
			}
		}
//		if(startdate == null) {
//			beanList = dashboardSvc.getNewDashboardChart6();
//		} else {
//			Dashboard dashboard = new Dashboard();
//			List<String> auth_idsList = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
//			if (auth_idsList == null) {
//				auth_idsList = (List<String>) request.getSession().getAttribute("auth_ids_list");
//			}
//			dashboard.setAuth_idsList(auth_idsList);
//			dashboard.setSearch_from(startdate.replaceAll("-", ""));
//			dashboard.setSearch_to(enddate.replaceAll("-", ""));
//			dashboard.setSystem_seq(system_seq);
//			dashboard.setSearchType(searchType);
//			dashboard.setLogCtType(logCtType);
//			if (searchType != null && searchType.equals("03")) {
//				beanList = pivotchartSvc.getDownloadLogChart6(dashboard);
//			} else {
//				beanList = pivotchartSvc.getNewDashboardChart6(dashboard);
//			}
//		}
		
		JSONArray jArray = new JSONArray();
		
		for ( int i=0; i<beanList.size(); ++i ) {
			Dashboard temp = beanList.get(i);
			String dept_name=temp.getDept_name();
			String dept_id=temp.getDept_id();
			int nPrivCount=temp.getnPrivCount();
			JSONObject jsonObj=new JSONObject();
			if(searchType.equals("02")) {
				jsonObj.put("dept_name", "시스템로그");
			} else {
				jsonObj.put("dept_name", dept_name+"("+dept_id+")");
			}
			jsonObj.put("nPrivCount", nPrivCount);
			jArray.add(jsonObj);
			
		}
		
		return jArray;
	}
	
	@RequestMapping(value="new_dashboard_chart7.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray newDashboardChart7(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "startdate", required = false) String startdate,
			@RequestParam(value = "enddate", required = false) String enddate) throws Exception {
		
		List<SystemMaster> SystemMasterList = indvinfoTypeSetupDao.getSystemMasterList();
		
		//JSONObject dataProvider=new JSONObject();
		
		JSONArray graphsArray = new JSONArray();
		JSONArray jArray = new JSONArray();
		
		String color[] = {"#FF0F00", "#FF6600", "#FF9E01", "#FCD202", "#F8FF01"};
		
		for ( int i=0; i<SystemMasterList.size(); ++i ) {
			
			String strKey1="cnt"+SystemMasterList.get(i).getSystem_seq();
			
			JSONObject graphs=new JSONObject();
			
			String tmp = "<div style='margin:5px;'><b>[[x]]</b><br>시스템명:<b>"+SystemMasterList.get(i).getSystem_name()+"</b><br>검출건수:<b>[[value]]</b></div>";
			
			graphs.put("balloonText",tmp);
			graphs.put("bullet","round");
			graphs.put("id","AmGraph-"+i);
			graphs.put("lineAlpha",0);
			graphs.put("lineColor",color[i%5]);
			graphs.put("fillAlphas",0);
			graphs.put("valueField",strKey1);
			graphs.put("xField","proc_date");
			graphs.put("yField",strKey1);
			
			graphsArray.add(graphs);
		}
		
		
		List<Dashboard> beanList;
		if(startdate == null)
			beanList = dashboardSvc.getNewDashboardChart7();
		else {
			Dashboard dashboard = new Dashboard();
			dashboard.setSearch_from(startdate.replaceAll("-", ""));
			dashboard.setSearch_to(enddate.replaceAll("-", ""));
			beanList = pivotchartSvc.getNewDashboardChart7(dashboard);
		}
		
		String privProcDate="";
		JSONObject jsonObj=new JSONObject();
		
		for ( int i=0; i<beanList.size(); ++i ) {
			Dashboard temp = beanList.get(i);
			
			String proc_date=temp.getProc_date();
			String system_seq=temp.getSystem_seq();
			String system_name=temp.getSystem_name();
			int cnt=temp.getCnt1();
			String strKey="cnt"+temp.getSystem_seq();
			
			String str = proc_date;
			//proc_date=str.substring(0,4)+"-"+str.substring(4,6)+"-"+str.substring(6,8);
			
			if ( privProcDate.length() == 0 ) {
				privProcDate=proc_date;
				
				jsonObj.put("proc_date", proc_date);			
				jsonObj.put(strKey, cnt);
				
			} else if (proc_date.equals(privProcDate)) {
				//같을떄
				privProcDate=proc_date;
				jsonObj.put(strKey, cnt);
				
			} else {
				//다를때
				jArray.add(jsonObj);
				jsonObj=new JSONObject();
				
				privProcDate=proc_date;
				jsonObj.put("proc_date", proc_date);	
				jsonObj.put(strKey, cnt);
			}	
			
		}
		
		jArray.add(jsonObj);

		jArray.add(graphsArray);
		return jArray;
	}
	
	@RequestMapping(value="new_dashboard_chart8.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray newDashboardChart8(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "startdate", required = false) String startdate,
			@RequestParam(value = "enddate", required = false) String enddate) throws Exception {
		List<SystemMaster> SystemMasterList = indvinfoTypeSetupDao.getSystemMasterList();
		
		JSONObject dataProvider=new JSONObject();
		
		JSONArray graphsArray = new JSONArray();
		
		
		for ( int i=0; i<SystemMasterList.size(); ++i ) {
			String strKey="system_name"+SystemMasterList.get(i).getSystem_seq();
			dataProvider.put(strKey,0);
			JSONObject graphs=new JSONObject();
			
			graphs.put("balloonText","[[title]]: [[value]]");
			graphs.put("bullet","round");
			graphs.put("title",SystemMasterList.get(i).getSystem_name());
			graphs.put("valueField",strKey);
			graphs.put("fillAlphas",0);
			
			graphsArray.add(graphs);
		}
		
		List<Dashboard> beanList;
		if(startdate == null)
			beanList = dashboardSvc.getNewDashboardChart8();
		else {
			Dashboard dashboard = new Dashboard();
			dashboard.setSearch_from(startdate.replaceAll("-", ""));
			dashboard.setSearch_to(enddate.replaceAll("-", ""));
			beanList = pivotchartSvc.getNewDashboardChart8(dashboard);
		}
		
		JSONArray jArray = new JSONArray();
		String privProcDate="";
		JSONObject jsonObj=new JSONObject();
		
		for ( int i=0; i<beanList.size(); ++i ) {
			Dashboard temp = beanList.get(i);
			
			String proc_date=temp.getProc_date();
			String system_seq=temp.getSystem_seq();
			String system_name=temp.getSystem_name();
			int privCount=temp.getnPrivCount();
			String strKey="system_name"+temp.getSystem_seq();
			
			if ( privProcDate.length() == 0 ) {
				privProcDate=proc_date;
				jsonObj.put("proc_date", proc_date);				
				dataProvider.put(strKey,privCount);
				dataProvider.put("proc_date", proc_date);	
				
			} else if (proc_date.equals(privProcDate)) {
				//같을떄
				privProcDate=proc_date;
				dataProvider.put(strKey,privCount);
				dataProvider.put("proc_date", proc_date);
			} else {
				//다를때
				jsonObj.put("dataProvider", dataProvider);
				jsonObj.put("graphs", graphsArray);
				
				jArray.add(jsonObj);
				
				jsonObj=new JSONObject();
				
				dataProvider=new JSONObject();
				
				for ( int j=0; j<SystemMasterList.size(); ++j ) {
					String strKeyT="system_name"+SystemMasterList.get(j).getSystem_seq();
					dataProvider.put(strKeyT,0);
				}
				privProcDate=proc_date;
				jsonObj.put("proc_date", proc_date);				
				dataProvider.put(strKey,privCount);
				dataProvider.put("proc_date", proc_date);
				
			}	
		}
		
		jsonObj.put("dataProvider", dataProvider);
		jsonObj.put("graphs", graphsArray);
		
		jArray.add(jsonObj);
		return jArray;
	}
	
	@RequestMapping(value="new_dashboard_chart9.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray newDashboardChart9(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "enddate", required = false) String enddate) throws Exception {
		
		List<Dashboard> beanList;

		Dashboard dashboard = new Dashboard();
		dashboard.setSearch_to(enddate.replaceAll("-", ""));
		dashboard.setServer_seqSt(parameters.get("server_seq"));
		beanList = pivotchartSvc.getNewDashboardChart9(dashboard);
		
		JSONArray jArray = new JSONArray();
		JSONArray graphsArray = new JSONArray();
		
		for ( int i=0; i<3; ++i ) {
			JSONObject graphs=new JSONObject();
			
			String strTmp = "";
			String strTitle = "";
			if ( i == 0 ){ 
				strTmp="cnt1";
				strTitle="CPU";
			} else if ( i == 1 ){ 
				strTmp="cnt2";
				strTitle="MEMORY";
			} else if ( i == 2 ){ 
				strTmp="cnt3";
				strTitle="HDD";
			}
			
			graphs.put("balloonText","<span style='font-size:14px; color:#000000;'><b>[[value]]</b></span>");
			graphs.put("fillAlphas",0.5);
			graphs.put("lineAlpha",0.5);
			graphs.put("title",strTitle);
			graphs.put("valueField",strTmp);
			
			graphsArray.add(graphs);
		}
		
		JSONObject jsonObj=new JSONObject();
		
		HashMap<String, Integer[]> tempMap = new HashMap<String, Integer[]>();
		
		for ( int i=0; i<24; ++i ) {
			String proc_time=String.format("%02d", (i));
			Integer [] temp = {0,0,0};
			tempMap.put(proc_time,temp );
		}
		
		for ( int i=0; i<beanList.size(); ++i ) {
			Dashboard temp = beanList.get(i);
			
			String proc_time=temp.getProc_time();
			int cnt1=temp.getCnt1();
			int cnt2=temp.getCnt2();
			int cnt3=temp.getCnt3();
			
			Integer [] temp1 = {cnt1,cnt2,cnt3};
			tempMap.put(proc_time,temp1);
			
		}
		
		for ( int i=0; i<24; ++i ) {
			String proc_time=String.format("%02d", (i));

			jsonObj=new JSONObject();
			
			jsonObj.put("year", proc_time);
			jsonObj.put("cnt1", tempMap.get(proc_time)[0]);
			jsonObj.put("cnt2", tempMap.get(proc_time)[1]);
			jsonObj.put("cnt3", tempMap.get(proc_time)[2]);
			jArray.add(jsonObj);
		}
		
		jArray.add(graphsArray);
		
		return jArray;
	}
	
	@RequestMapping(value="new_dashboard_chart10.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray newDashboardChart10(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "startdate", required = false) String startdate,
			@RequestParam(value = "enddate", required = false) String enddate) throws Exception {
		
		//JSONObject dataProvider=new JSONObject();
		JSONArray graphsArray = new JSONArray();
		JSONArray jArray = new JSONArray();
		
		String color[] = {"#FF0F00", "#FF6600", "#FF9E01", "#FCD202", "#F8FF01"};
		
		List<Dashboard> beanList;
		if(startdate == null)
			beanList = dashboardSvc.getNewDashboardChart10();
		else {
			Dashboard dashboard = new Dashboard();
			dashboard.setSearch_from(startdate.replaceAll("-", ""));
			dashboard.setSearch_to(enddate.replaceAll("-", ""));
			beanList = pivotchartSvc.getNewDashboardChart10(dashboard);
		}
		
		String privProcDate="";
		JSONObject jsonObj=new JSONObject();
		
		int nTime1 = 0;
		int nTime2 = 0;
		
		if ( beanList.size() == 0 ) {
			JSONObject graphs=new JSONObject();
			String tmp = "<div style='margin:5px;'><b>[[x]]</b><br>사용자명:<b></b><br>추출건수:<b>[[value]]</b></div>";
			graphs.put("balloonText",tmp);
			graphs.put("bullet","round");
			graphs.put("id","AmGraph-"+0);
			graphs.put("lineAlpha",0);
			graphs.put("lineColor","FF0F00");
			graphs.put("fillAlphas",0);
			graphs.put("valueField","strKey");
			graphs.put("xField","proc_time");
			graphs.put("yField","strKey");
			
			graphsArray.add(graphs);
		}
		
		for ( int i=0; i<beanList.size(); ++i ) {
			Dashboard temp = beanList.get(i);
			
			String proc_time=temp.getProc_time();
			String emp_user_id=temp.getEmp_user_id();
			String emp_user_name=temp.getEmp_user_name();
			int nPrivCount=temp.getnPrivCount();
			String strKey="cnt"+emp_user_id;
			
			JSONObject graphs=new JSONObject();
			
			String tmp = "<div style='margin:5px;'><b>[[x]]</b><br>사용자명:<b>"+emp_user_name+"</b><br>추출건수:<b>[[value]]</b></div>";
			
			graphs.put("balloonText",tmp);
			graphs.put("bullet","round");
			graphs.put("id","AmGraph-"+i);
			graphs.put("lineAlpha",0);
			graphs.put("lineColor",color[i%5]);
			graphs.put("fillAlphas",0);
			graphs.put("valueField",strKey);
			graphs.put("xField","proc_time");
			graphs.put("yField",strKey);
			
			graphsArray.add(graphs);
			
			if ( privProcDate.length() == 0 ) {
				
				nTime1=0;
				nTime2=Integer.parseInt(proc_time);
				
				jArray = insertTmpData(jArray, nTime1, nTime2);
				
				
				jsonObj.put("proc_time", proc_time);			
				jsonObj.put(strKey, nPrivCount);
				
				privProcDate=proc_time;
				
			} else if (proc_time.equals(privProcDate)) {
				//같을떄
				privProcDate=proc_time;
				jsonObj.put(strKey, nPrivCount);
				
			} else {
				//다를때
				jArray.add(jsonObj);
				jsonObj=new JSONObject();
				
				nTime1=Integer.parseInt(privProcDate);
				nTime2=Integer.parseInt(proc_time);
				
				jArray = insertTmpData(jArray, nTime1, nTime2);
				
				privProcDate=proc_time;
				jsonObj.put("proc_time", proc_time);	
				jsonObj.put(strKey, nPrivCount);
			}	
			
		}
		
		
		
		
		jArray.add(jsonObj);

		jArray = insertTmpData(jArray, -1, nTime2);
		jArray.add(graphsArray);
		return jArray;
	}
	
	public JSONArray insertTmpData(JSONArray jArray, int nTime, int proc_time) {
		
		if ( nTime == -1 ) {
			
			for ( int i=proc_time+1; i<24; ++i ) {
				JSONObject jsonObj=new JSONObject();
				String proc_timeS=String.format("%02d", (i));
				jsonObj.put("proc_time", proc_timeS);
				jArray.add(jsonObj);
			}
			
		} else {
			if ( jArray.size() == 0 ) {
				for ( int i=0; i<proc_time; ++i ) {
					JSONObject jsonObj=new JSONObject();
					String proc_timeS=String.format("%02d", (i));
					jsonObj.put("proc_time", proc_timeS);
					jArray.add(jsonObj);
				}
			} else if ( nTime+1 == proc_time) {
				return jArray;
			} else {
				for ( int i=nTime; i<proc_time; ++i ) {
					JSONObject jsonObj=new JSONObject();
					String proc_timeS=String.format("%02d", (i));
					jsonObj.put("proc_time", proc_timeS);
					jArray.add(jsonObj);
				}
			}
		}
		
		
		return jArray;
	}	
	
	@RequestMapping(value="new_dashboard_chart11.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray newDashboardChart11(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "startdate", required = false) String startdate,
			@RequestParam(value = "enddate", required = false) String enddate,
			@RequestParam(value = "system_seq", required = false) String system_seq) throws Exception {
		List<SystemMaster> SystemMasterList = indvinfoTypeSetupDao.getSystemMasterList();
		
		JSONObject dataProvider=new JSONObject();
		
		JSONArray graphsArray = new JSONArray();
		
		HashMap <String, String> tempMap = new HashMap<String, String>();

		List<Dashboard> beanList;
		List<Dashboard> top10 = new ArrayList();
		if(startdate == null)
			beanList = dashboardSvc.getNewDashboardChart11();
		else {
			Dashboard dashboard = new Dashboard();
			dashboard.setSearch_from(startdate.replaceAll("-", ""));
			dashboard.setSearch_to(enddate.replaceAll("-", ""));
			dashboard.setSystem_seq(system_seq);
			beanList = pivotchartSvc.getNewDashboardChart11(dashboard);
			top10 = divotchartDao.findEmpDetailByDeptTop10(dashboard);
		}
		
		JSONArray jArray = new JSONArray();
		String privData1="";
		JSONObject jsonObj=new JSONObject();
		int nTotal=0;
		
		
		if ( beanList.size() == 0 ) {
			JSONObject graphs=new JSONObject();
			
			graphs.put("title","key");
			graphs.put("labelText","[[value]]");
			graphs.put("valueField","key");
			graphs.put("type","column");
			graphs.put("fillAlphas",0.5);
			graphs.put("balloonText", "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]건</b></span>");
			
			graphsArray.add(graphs);
			
		}
		
		for ( int i=0; i<beanList.size(); ++i ) {
			Dashboard temp = beanList.get(i);
			
			String data1=temp.getData1();
			//String data2=temp.getData2();
			String data3=temp.getData3();
			
			if(startdate != null){
				boolean flag = false;
				for(int j=0; j<top10.size(); j++) {
					String dept_name = top10.get(j).getDept_name();
					if(dept_name.equals(data1)) {
						flag = true;
						break;
					}
				}
				if(flag != true)
					continue;
			}
			
			int nPrivCount=temp.getnPrivCount();
			
			if ( privData1.length() == 0 ) {
				privData1=data1;
				dataProvider.put("continent", data1);	
				dataProvider.put(data3, nPrivCount);	
				nTotal=nTotal+nPrivCount;
				tempMap.put(data3, data3);
				
			} else if (data1.equals(privData1)) {
				//같을떄
				privData1=data1;
				dataProvider.put("continent", data1);	
				dataProvider.put(data3, nPrivCount);
				nTotal=nTotal+nPrivCount;
				tempMap.put(data3, data3);
			} else {
				//다를때
				
				dataProvider.put("total", nTotal);
				jsonObj.put("dataProvider", dataProvider);
				jsonObj.put("graphs", graphsArray);
				
				jArray.add(jsonObj);
				
				jsonObj=new JSONObject();
				dataProvider=new JSONObject();
				nTotal=0;
				privData1=data1;
				dataProvider.put("continent", data1);	
				dataProvider.put(data3, nPrivCount);
				tempMap.put(data3, data3);
				nTotal=nTotal+nPrivCount;
			}	
		}
		
		dataProvider.put("total", nTotal);
		jsonObj.put("dataProvider", dataProvider);
		jsonObj.put("graphs", graphsArray);
		
		jArray.add(jsonObj);
		
		Iterator<String> iterator = tempMap.keySet().iterator();
		while (iterator.hasNext()) {
			String key = (String) iterator.next();
			
			JSONObject graphs=new JSONObject();
			
			graphs.put("title",key);
			graphs.put("labelText","[[value]]");
			graphs.put("valueField",key);
			graphs.put("type","column");
			graphs.put("fillAlphas",0.5);
			graphs.put("balloonText", "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]건</b></span>");
			
			graphsArray.add(graphs);
			
		 }
		
		jArray.add(graphsArray);

		return jArray;
	}
	
	@RequestMapping(value="pivotchart11.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray pivotChart11(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "startdate", required = false) String startdate,
			@RequestParam(value = "enddate", required = false) String enddate,
			@RequestParam(value = "system_seq", required = false) String system_seq) throws Exception {
		
		SearchSearch search = new SearchSearch();
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list == null) {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    }
	    search.setAuth_idsList(list);
		List<SystemMaster> SystemMasterList = indvinfoTypeSetupDao.getSystemMasterListByAuth(search);
		list.add("00"); // 공통 시스템 코드 추가
		
		Dashboard dashboard = new Dashboard();
		dashboard.setSearch_from(startdate.replaceAll("-", ""));
		dashboard.setSearch_to(enddate.replaceAll("-", ""));
		dashboard.setSystem_seq(system_seq);
		dashboard.setAuth_idsList(list);
		List<Dashboard> top10 = divotchartDao.findEmpDetailByDeptTop10(dashboard);
		
		JSONArray jArray = new JSONArray();
		for ( int i=0; i<top10.size(); ++i ) {
			Dashboard temp = top10.get(i);
			String dept_id = temp.getDept_id();
			String dept_name = temp.getDept_name();
			if(dept_id.equals("DEPT99999")) {
				dept_name = "공통";
			} else {
				dept_name += "("+dept_id+")";
			}
			int cnt = temp.getnPrivCount();
			JSONObject jsonObj=new JSONObject();
			jsonObj.put("dept_name", dept_name);
			jsonObj.put("nPrivCount", cnt);
			jArray.add(jsonObj);
		}
		return jArray;
	}
	@RequestMapping(value="pivotchart15.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray pivotChart15(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "startdate", required = false) String startdate,
			@RequestParam(value = "enddate", required = false) String enddate,
			@RequestParam(value = "system_seq", required = false) String system_seq) throws Exception {
		
		SearchSearch search = new SearchSearch();
		List<String> list = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list == null) {
	    	list = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    }
	    search.setAuth_idsList(list);
		List<SystemMaster> SystemMasterList = indvinfoTypeSetupDao.getSystemMasterListByAuth(search);
		list.add("00"); // 공통 시스템 코드 추가
		
		Dashboard dashboard = new Dashboard();
		dashboard.setSearch_from(startdate.replaceAll("-", ""));
		dashboard.setSearch_to(enddate.replaceAll("-", ""));
		dashboard.setSystem_seq(system_seq);
		dashboard.setAuth_idsList(list);
		List<Dashboard> top10 = divotchartDao.findEmpDetailByUserTop10(dashboard);
		
		JSONArray jArray = new JSONArray();
		for ( int i=0; i<top10.size(); ++i ) {
			Dashboard temp = top10.get(i);
			String emp_user_id = temp.getEmp_user_id();
			String emp_user_name = temp.getEmp_user_name();
			if(emp_user_id.equals("ZZZZ99999")) {
				emp_user_name = "공통";
			} else {
				emp_user_name += "("+emp_user_id+")";
			}
			int cnt = temp.getnPrivCount();
			JSONObject jsonObj=new JSONObject();
			jsonObj.put("emp_user_name", emp_user_name);
			jsonObj.put("nPrivCount", cnt);
			jArray.add(jsonObj);
		}
		return jArray;
	}
	
	/*@RequestMapping(value="new_dashboard_chart11.html", method={RequestMethod.POST})
	@ResponseBody
	public Map newDashboardChart11(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "startdate", required = false) String startdate,
			@RequestParam(value = "enddate", required = false) String enddate,
			@RequestParam(value = "system_seq", required = false) String system_seq) throws Exception {
		List<Report> rule_list = new ArrayList<Report>();
		
		Report tmp1 = new Report();
		tmp1.setRule_nm("고유식별정보 과다사용");
		rule_list.add(tmp1);
		
		Report tmp2 = new Report();
		tmp2.setRule_nm("비정상 접근");
		rule_list.add(tmp2);
		
		Report tmp3 = new Report();
		tmp3.setRule_nm("과다처리");
		rule_list.add(tmp3);
		
		Report tmp4 = new Report();
		tmp4.setRule_nm("특정인 처리");
		rule_list.add(tmp4);
		
		Report tmp5 = new Report();
		tmp5.setRule_nm("특정시간대 처리");
		rule_list.add(tmp5);
		
		
		Map map = new HashMap<>();
		map.put("start_date", start_date.replaceAll("-", ""));
		map.put("end_date", end_date.replaceAll("-", ""));
		map.put("rule_list", rule_list);

		List<Map> emp_list = reportDao.findReportDetail2_chart3(map);

		JSONArray jArray = new JSONArray();
		for (int i = 0; i < emp_list.size(); i++) {
			
			if ( i>4 ) break;
			
			Map data = emp_list.get(i);
			JSONObject obj = new JSONObject();
			
			data.put("start_date", start_date.replaceAll("-", ""));
			data.put("end_date", end_date.replaceAll("-", ""));
			
			List<Map> emp_listDetail = reportDao.findReportDetail2_chart3Detail(data);
			
			int nType1=0;
			int nType2=0;
			int nType3=0;
			int nType4=0;
			int nType5=0;
			
			for ( int j=0; j<emp_listDetail.size(); ++j ) {
				Map dataTemp = emp_listDetail.get(j);
				
				if (dataTemp.get("scen_seq").equals(1000)) {
					nType1=1;
					obj.put("nType1", dataTemp.get("cnt"));
				}
				else if (dataTemp.get("scen_seq").equals(2000)) {
					nType2=1;
					obj.put("nType2", dataTemp.get("cnt"));
				}
				else if (dataTemp.get("scen_seq").equals(3000)) {
					nType3=1;
					obj.put("nType3", dataTemp.get("cnt"));
				}
				else if (dataTemp.get("scen_seq").equals(4000)) {
					nType4=1;
					obj.put("nType4", dataTemp.get("cnt"));
				}
				else if (dataTemp.get("scen_seq").equals(5000)) {
					nType5=1;
					obj.put("nType5", dataTemp.get("cnt"));
				}
			}
			
			obj.put("dept_name", data.get("dept_name"));
			if ( nType1 == 0 ) obj.put("nType1", nType1);
			if ( nType2 == 0 ) obj.put("nType2", nType2);
			if ( nType3 == 0 ) obj.put("nType3", nType3);
			if ( nType4 == 0 ) obj.put("nType4", nType4);
			if ( nType5 == 0 ) obj.put("nType5", nType5);
			
			jArray.add(obj);
		}

		Map res = new HashMap<>();
		res.put("rule_list", rule_list);
		res.put("emp_list", emp_list);

		return res;
	}*/
	
	@RequestMapping(value="new_dashboard_chart12.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray newDashboardChart12(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "startdate", required = false) String startdate,
			@RequestParam(value = "enddate", required = false) String enddate,
			@RequestParam(value = "system_seq", required = false) String system_seq) throws Exception {
		
		Dashboard dashboard = new Dashboard();
		List<String> list2 = (List<String>) request.getSession().getAttribute("auth_ids_list_normal");
	    if(list2 != null) {
	    	dashboard.setAuth_idsList(list2);
	    }else {
	    	list2 = (List<String>) request.getSession().getAttribute("auth_ids_list");
	    	dashboard.setAuth_idsList(list2);
	    }
	    dashboard.setSearch_from(startdate.replaceAll("-", ""));
	    dashboard.setSearch_to(enddate.replaceAll("-", ""));
	    dashboard.setSystem_seq(system_seq);
		List<Dashboard> beanList = pivotchartSvc.getNewDashboardChart12(dashboard);
		
		JSONArray jArray = new JSONArray();
		for ( Dashboard temp : beanList ) {
			JSONObject jsonObj=new JSONObject();
			String scen_name = temp.getScen_name();
			int nPrivCount=temp.getnPrivCount();
			jsonObj.put("scen_name", scen_name);
			jsonObj.put("nPrivCount", nPrivCount);
			jArray.add(jsonObj);
		}
		return jArray;
	}
	
	@RequestMapping(value="new_dashboard_chart13.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray newDashboardChart13(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "startdate", required = false) String startdate,
			@RequestParam(value = "enddate", required = false) String enddate) throws Exception {
		
		List<Dashboard> beanList;
		if(startdate == null)
			beanList = dashboardSvc.getNewDashboardChart13();
		else {
			Dashboard dashboard = new Dashboard();
			dashboard.setSearch_from(startdate.replaceAll("-", ""));
			dashboard.setSearch_to(enddate.replaceAll("-", ""));
			beanList = pivotchartSvc.getNewDashboardChart13(dashboard);
		}
		
		JSONArray jArray = new JSONArray();
		String privProcDate="";
		JSONObject jsonObj=new JSONObject();
		
		HashMap<String, Integer> tempMap = new HashMap<String, Integer>();
		
		for ( int i=0; i<24; ++i ) {
			String proc_time=String.format("%02d", (i));
			int nCount =0;
			tempMap.put(proc_time,nCount );
		}
		
		for ( int i=0; i<beanList.size(); ++i ) {
			Dashboard temp = beanList.get(i);
			
			String proc_time=temp.getProc_time();
			int nPrivCount=temp.getnPrivCount();
			tempMap.put(proc_time,nPrivCount);
			
		}
		
		for ( int i=0; i<24; ++i ) {
			String proc_time=String.format("%02d", (i));

			jsonObj=new JSONObject();
			
			jsonObj.put("proc_date", proc_time);
			jsonObj.put("nPrivCount", tempMap.get(proc_time));
			jArray.add(jsonObj);
		}
		return jArray;
	}
	
	@RequestMapping(value="new_dashboard_chart14.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray newDashboardChart14(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "startdate", required = false) String startdate,
			@RequestParam(value = "enddate", required = false) String enddate,
			@RequestParam(value = "emp_user_id", required = false) String emp_user_id,
			@RequestParam(value = "emp_user_name", required = false) String emp_user_name) throws Exception {
		
		
		List<ExtrtBaseSetup> ruleTblList = extrtCondbyInqDao.findRuleTblList();
		
		JSONObject dataProvider=new JSONObject();
		
		JSONArray graphsArray = new JSONArray();
		
		for ( int i=0; i<ruleTblList.size(); ++i ) {
			String strKey="rule_nm"+ruleTblList.get(i).getRule_nm();
			dataProvider.put(strKey,0);
			JSONObject graphs=new JSONObject();
			
			graphs.put("balloonText","[[title]]: [[value]]");
			graphs.put("bullet","round");
			graphs.put("title",ruleTblList.get(i).getRule_nm());
			graphs.put("valueField",strKey);
			graphs.put("fillAlphas",0);
			
			graphsArray.add(graphs);
		}
		
		Dashboard dashboard = new Dashboard();
		dashboard.setSearch_from(startdate.replaceAll("-", ""));
		dashboard.setSearch_to(enddate.replaceAll("-", ""));
		
		if(emp_user_id != null) {
			String upperId = emp_user_id.toUpperCase();
			dashboard.setEmp_user_id(upperId);
		}
		dashboard.setEmp_user_id(emp_user_id);
		dashboard.setEmp_user_name(emp_user_name);
		List<Dashboard> beanList = pivotchartSvc.getNewDashboardChart14(dashboard);
		
		JSONArray jArray = new JSONArray();
		String privProcDate="";
		JSONObject jsonObj=new JSONObject();
		
		for ( int i=0; i<beanList.size(); ++i ) {
			Dashboard temp = beanList.get(i);
			
			String occr_dt = temp.getOccr_dt();
			String rule_nm = temp.getRule_nm();
			int privCount=temp.getnPrivCount();
			String strKey="rule_nm"+temp.getRule_nm();
			
			if ( privProcDate.length() == 0 ) {
				privProcDate=occr_dt;
				jsonObj.put("occr_dt", occr_dt);				
				dataProvider.put(strKey,privCount);
				dataProvider.put("occr_dt", occr_dt);	
				
			} else if (occr_dt.equals(privProcDate)) {
				//같을떄
				privProcDate=occr_dt;
				dataProvider.put(strKey,privCount);
				dataProvider.put("occr_dt", occr_dt);
			} else {
				//다를때
				jsonObj.put("dataProvider", dataProvider);
				jsonObj.put("graphs", graphsArray);
				
				jArray.add(jsonObj);
				
				jsonObj=new JSONObject();
				
				dataProvider=new JSONObject();
				
				for ( int j=0; j<ruleTblList.size(); ++j ) {
					String strKeyT="rule_nm"+ruleTblList.get(j).getRule_nm();
					dataProvider.put(strKeyT,0);
				}
				privProcDate=occr_dt;
				jsonObj.put("occr_dt", occr_dt);				
				dataProvider.put(strKey,privCount);
				dataProvider.put("occr_dt", occr_dt);
				
			}	
		}
		
		jsonObj.put("dataProvider", dataProvider);
		jsonObj.put("graphs", graphsArray);
		
		jArray.add(jsonObj);
		return jArray;
	}
	
	@RequestMapping(value="dashboard_chartDemo1.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray dashboardChartDemo1(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		//List<Dashboard> beanList = dashboardSvc.getNewDashboardChart6();
		
		String[] system_name = {"새울행정시스템", "주민등록시스템", "세외수입시스템", "표준지방세정보시스템", "재정시스템", "건축행정시스템",
								"부동산종합공부시스템", "부동산거래관리시스템", "지방세가상계좌시스템", "인사행정시스템", "온나라시스템"};
		String[] color = {"#003870", "#004856", "#006673", "#037F76", "#39BB9D", "#E56542", "#CC323A", "#931A2E", "#911751", "#801F63", "#511549"};
		
		JSONArray jArray = new JSONArray();
		for(int i=0; i<system_name.length; i++) {
			String category = system_name[i];
			
			JSONArray segments = new JSONArray();
			Date prevTime = null;
			Date curTime = new Date();
			int cnt = (int)(Math.random()*3+2);
			for(int j=1; j<=cnt; j++) {
				String task = "Task #" + j;
				
				Calendar cal = Calendar.getInstance();
				if(prevTime == null) {
					cal.setTime(curTime);
					cal.set(Calendar.HOUR_OF_DAY, 8);
					cal.set(Calendar.MINUTE, 0);
				}else
					cal.setTime(prevTime);
				
				int hour = (int)(Math.random()*2);
				int minute = (int)(Math.random()*60);
				cal.add(Calendar.HOUR, hour);
				cal.add(Calendar.MINUTE, minute);
				Date start = cal.getTime();
				
				if(curTime.before(start))
					break;
				
				int hour2 = (int)(Math.random()*4 + 1);
				int minute2 = (int)(Math.random()*60);
				cal.add(Calendar.HOUR, hour2);
				cal.add(Calendar.MINUTE, minute2);
				Date end = cal.getTime();
				
				if(curTime.before(end))
					end = curTime;
				
				prevTime = end;
				
				DateFormat df = new SimpleDateFormat("HH:mm");
				
				JSONObject segment = new JSONObject();
				segment.put("task", task);
				segment.put("color", color[i]);
				segment.put("start", df.format(start));
				segment.put("end", df.format(end));
				
				segments.add(segment);
			}
			
			JSONObject jObj = new JSONObject();
			jObj.put("category", category);
			jObj.put("segments", segments);
			
			jArray.add(jObj);
		}
		
		return jArray;
	}
	
	@RequestMapping(value="dashboard_chartDemo2.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray dashboardChartDemo2(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		/*List<Dashboard> beanList = dashboardSvc.getNewDashboardChart7();
		JSONArray jArray = new JSONArray();
		
		String privProcDate="";
		for ( int i=0; i<beanList.size(); ++i ) {
			Dashboard temp = beanList.get(i);
			
			String proc_date=temp.getProc_date();
			int cnt = temp.getCnt1();
			String str = proc_date;
			proc_date=str.substring(0,4)+"-"+str.substring(4,6)+"-"+str.substring(6,8);
			
			if(privProcDate.equals(proc_date)) {
				continue;
			} else {
				privProcDate = proc_date;
				JSONObject jsonObj=new JSONObject();
				jsonObj.put("log_date", proc_date);
				jsonObj.put("log_value", cnt);
				jArray.add(jsonObj);
			}
		}*/
		
		JSONArray jArray = new JSONArray();
		
		for(int i=0; i<=20; i++) {
			Date date = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			cal.add(Calendar.SECOND, i);
			
			JSONObject jsonObj=new JSONObject();
			jsonObj.put("log_date", df.format(cal.getTime()));
			jsonObj.put("log_value", (int)(Math.random() * 50 + 50));
			jArray.add(jsonObj);
		}
		
		return jArray;
	}
	
	@RequestMapping(value="dashboard_chartDemo3.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray dashboardChartDemo3(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		//List<Dashboard> beanList = dashboardSvc.getNewDashboardChart6();
		
		JSONArray jArray = new JSONArray();
		
		String[] category = {
  	        "마케팅","인사","정보","시설","홍보","기획","대외협력","경영관리","개발","연구","컨설팅","기술지원"
		};
		for(int i=1; i<=12; i++) {
			
			JSONObject jsonObj=new JSONObject();
			int ran = (int)(Math.random() * 100 + 1);
			int ran2 = (int)(Math.random() * 10);
			jsonObj.put("category_team", category[i-1]);
			jsonObj.put("info_average", ran);
			jsonObj.put("info_value", Math.random() > 0.5 ? ran + ran2 : ran - ran2);
			jArray.add(jsonObj);
		}
		
		return jArray;
	}
	
	@RequestMapping(value="dashboard_chartDemo4.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray dashboardChartDemo4(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		//List<Dashboard> beanList = dashboardSvc.getNewDashboardChart6();
		
		JSONArray jArray = new JSONArray();
		
		for(int i=0; i<=20; i++) {
			Date date = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			cal.add(Calendar.SECOND, i);
			
			JSONObject jsonObj=new JSONObject();
			jsonObj.put("log_date", df.format(cal.getTime()));
			jsonObj.put("log_value", (int)(Math.random() * 50 + 50));
			jArray.add(jsonObj);
		}
		
		return jArray;
	}
	
	@RequestMapping(value="dashboard_chartDemo5.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray dashboardChartDemo5(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		//List<Dashboard> beanList = dashboardSvc.getNewDashboardChart6();
		
		JSONArray jArray = new JSONArray();
		
		for(int i=0; i<12; i++) {
			Dashboard info = new Dashboard();
			info.setSystem_seq("team_"+i);
			info.setCnt1((int)(Math.random() * 100) + 1);
			info.setCnt2((int)(Math.random() * 100) + 1);
			info.setCnt3((int)(Math.random() * 100) + 1);
			
			JSONObject jsonObj=new JSONObject();
			jsonObj.put("team", info.getSystem_seq());
			jsonObj.put("rrnum", info.getCnt1());
			jsonObj.put("accnum", info.getCnt2());
			jsonObj.put("phonenum", info.getCnt3());
			jArray.add(jsonObj);
		}
		
		return jArray;
	}
	
	@RequestMapping(value="dashboard_chartDemo6.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray dashboardChartDemo6(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		//List<Dashboard> beanList = dashboardSvc.getNewDashboardChart6();
		
		JSONArray jArray = new JSONArray();
		String[] system = {"새올행정시스템", "주민등록시스템", "세외수입시스템", "표준지방세정보시스템", "재정시스템"};
		String[] cate = {"마케팅", "인사", "정보", "홍보", "기획"};
		String[] id = {"easy100", "KHK84613", "tjlee", "ehchoi", "yjyoo"};
		String[] name = {"김이지", "김휘원", "이태진", "은호최", "유연주"};
		String[] ip = {"192.168.0.20", "192.168.0.28", "192.168.0.22", "192.168.0.16", "192.168.0.14"};
		
		for(int i=0; i<5; i++) {
			
			JSONObject jsonObj=new JSONObject();
			jsonObj.put("system", system[i]);
			jsonObj.put("cate", cate[i]);
			jsonObj.put("id", id[i]);
			jsonObj.put("name", name[i]);
			jsonObj.put("ip", ip[i]);
			jArray.add(jsonObj);
		}
		
		return jArray;
	}
	
	@RequestMapping(value="dashboard_chartDemo7.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray dashboardChartDemo7(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		//List<Dashboard> beanList = dashboardSvc.getNewDashboardChart6();
		
		JSONArray jArray = new JSONArray();
		String[] cate = {"주민등록번호", "전화번호", "이메일", "휴대폰번호"};
		for(int i=0; i<4; i++) {
			int ran;
			if(i == 0)
				ran = (int)(Math.random() * 20) + 20;
			else
				ran = (int)(Math.random() * 20) + 1;
			
			JSONObject jsonObj=new JSONObject();
			jsonObj.put("personal_type", cate[i]);
			jsonObj.put("personal_value", ran);
			jArray.add(jsonObj);
		}
		
		return jArray;
	}
	
	@RequestMapping(value="dashboard_chartDemo8.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray dashboardChartDemo8(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		//List<Dashboard> beanList = dashboardSvc.getNewDashboardChart6();
		
		JSONArray jArray = new JSONArray();
		String[] cate = {"조회", "출력", "다운로드"};
		for(int i=0; i<3; i++) {
			int ran;
			if(i == 0)
				ran = (int)(Math.random() * 20) + 50;
			else
				ran = (int)(Math.random() * 20) + 1;
			
			JSONObject jsonObj=new JSONObject();
			jsonObj.put("personal_type", cate[i]);
			jsonObj.put("personal_value", ran);
			jArray.add(jsonObj);
		}
		
		return jArray;
	}
	
	@RequestMapping(value="dashboard_chartDemo9.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray dashboardChartDemo9(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		//List<Dashboard> beanList = dashboardSvc.getNewDashboardChart6();
		
		JSONArray jArray = new JSONArray();
		
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		cal.get(cal.MONTH);
		String days[] = {"Mon", "Tue", "Wed", "Ths", "Fri", "Sat", "Sun"};
		
		for(int i=0; i<7; i++) {
			Dashboard info = new Dashboard();
			info.setProc_date(days[i]);
			info.setCnt1((int)(Math.random() * 100) + 1);
			info.setCnt2((int)(Math.random() * 100) + 1);
			
			JSONObject jsonObj=new JSONObject();
			jsonObj.put("day", info.getProc_date());
			jsonObj.put("last", info.getCnt1());
			jsonObj.put("current", info.getCnt2());
			jArray.add(jsonObj);
		}
		
		return jArray;
	}
	
	@RequestMapping(value="dashboard_chartDemo10.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray dashboardChartDemo10(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		//List<Dashboard> beanList = dashboardSvc.getNewDashboardChart6();
		
		JSONArray jArray = new JSONArray();
		
		for(int i=0; i<=30; i++) {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Date date = df.parse("2017-01-01");
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			cal.add(Calendar.DATE, i);
			Dashboard db = new Dashboard();
			db.setProc_date(df.format(cal.getTime()));
			int a = (int)(Math.random() * 100) + 1;
			int b = (int)(Math.random() * 100) + 1;
			int min = Math.min(a, b);
			int max = Math.max(a, b);
			
			db.setCnt1(min);
			db.setCnt2(max);
			db.setCnt3((min+max)/2);
			
			JSONObject jsonObj=new JSONObject();
			jsonObj.put("rrn_date", db.getProc_date());
			jsonObj.put("rrn_fvalue", db.getCnt1());
			jsonObj.put("rrn_tvalue", db.getCnt2());
			jsonObj.put("rrn_value", db.getCnt3());
			jArray.add(jsonObj);
		}
		
		return jArray;
	}
	
	@RequestMapping(value="dashboard_chartDemo11.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray dashboardChartDemo11(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		//List<Dashboard> beanList = dashboardSvc.getNewDashboardChart6();
		
		JSONArray jArray = new JSONArray();
		String[] name = {"김이지", "김휘원", "이태진", "은호최", "유연주"};
		
		for(int i=0; i<5; i++) {
			
			JSONObject jsonObj=new JSONObject();
			jsonObj.put("name", name[i]);
			jsonObj.put("logcnt", (int)(Math.random()*10+1));
			jsonObj.put("cnt", (int)(Math.random()*10+10));
			jArray.add(jsonObj);
		}
		
		return jArray;
	}
	
	@RequestMapping(value="dashboard_chartDemo12.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray dashboardChartDemo12(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		//List<Dashboard> beanList = dashboardSvc.getNewDashboardChart6();
		
		JSONArray jArray = new JSONArray();
		
		return jArray;
	}
	
	@RequestMapping(value="dashboard_chartDemo13.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray dashboardChartDemo13(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		//List<Dashboard> beanList = dashboardSvc.getNewDashboardChart6();
		
		JSONArray jArray = new JSONArray();
		
		String days[] = {"M-6", "M-5", "M-4", "M-3", "M-2", "M-1", "M"};
		for(int i=0; i<7; i++) {
			JSONObject jsonObj=new JSONObject();
			jsonObj.put("day", days[i]);
			jsonObj.put("last", (int)(Math.random() * 30000) + 10000);
			jsonObj.put("current", (int)(Math.random() * 30000) + 10000);
			jArray.add(jsonObj);
		}
		
		return jArray;
	}
	
	@RequestMapping(value="dashboard_chartDemo14.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray dashboardChartDemo14(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		//List<Dashboard> beanList = dashboardSvc.getNewDashboardChart6();
		
		JSONArray jArray = new JSONArray();
		String[] name = {"마케팅", "인사", "정보", "홍보", "기획"};
		
		for(int i=0; i<5; i++) {
			
			JSONObject jsonObj=new JSONObject();
			jsonObj.put("name", name[i]);
			jsonObj.put("logcnt", (int)(Math.random()*10+1));
			jsonObj.put("cnt", (int)(Math.random()*10+10));
			jArray.add(jsonObj);
		}
		
		return jArray;
	}
	
	@RequestMapping(value="dashboard_chartDemo15.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray dashboardChartDemo15(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		//List<Dashboard> beanList = dashboardSvc.getNewDashboardChart6();
		
		JSONArray jArray = new JSONArray();
		String days[] = {"Mon", "Tue", "Wed", "Ths", "Fri", "Sat", "Sun"};
		for(int i=0; i<7; i++) {
			JSONObject jsonObj=new JSONObject();
			jsonObj.put("psm_type", days[i]);
			jsonObj.put("psm_value", (int)(Math.random() * 10) + 1);
			jArray.add(jsonObj);
		}
		
		return jArray;
	}
	
	@RequestMapping(value="dashboard_chartDemo16.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray dashboardChartDemo16(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		//List<Dashboard> beanList = dashboardSvc.getNewDashboardChart6();
		
		JSONArray jArray = new JSONArray();
		
		for(int i=0; i<60; i++) {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Date date = df.parse("2017-01-01");
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			cal.add(Calendar.DATE, i);
			Dashboard db = new Dashboard();
			db.setProc_date(df.format(cal.getTime()));
			db.setCnt1((int)(Math.random() * 100) + 1);
			db.setCnt2((int)(Math.random() * 100) + 1);
			db.setCnt3((int)(Math.random() * 100) + 1);
			
			JSONObject jsonObj=new JSONObject();
			jsonObj.put("content_date", db.getProc_date());
			jsonObj.put("content1", db.getCnt1());
			jsonObj.put("content2", db.getCnt2());
			jsonObj.put("content3", db.getCnt3());
			jArray.add(jsonObj);
		}
		
		return jArray;
	}
	
	@RequestMapping(value="dashboard_chartDemo17.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray dashboardChartDemo17(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		//List<Dashboard> beanList = dashboardSvc.getNewDashboardChart6();
		
		JSONArray jArray = new JSONArray();
		
		int val = (int)(Math.random()*100 + 1);
		for(int i=1; i<25; i++) {
			String time;
			if(i<10)
				time = "0" + i;
			else
				time = "" + i;
			
			JSONObject jsonObj=new JSONObject();
			jsonObj.put("time", time);
			
			if(i < 14)
				val += (int)(Math.random() * i * 100 + 1);
			else {
				val -= (int)(Math.random() * i * 100 + 1);
				val = Math.max(val, 0);
			}
			jsonObj.put("info", val);
			jArray.add(jsonObj);
		}
		
		return jArray;
	}
	
	@RequestMapping(value="dashboard_chartDemo18.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray dashboardChartDemo18(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		//List<Dashboard> beanList = dashboardSvc.getNewDashboardChart6();
		
		JSONArray jArray = new JSONArray();
		
		int val = (int)(Math.random()*200 + 1);
		for(int i=1; i<25; i++) {
			String time;
			if(i<10)
				time = "0" + i;
			else
				time = "" + i;
			
			JSONObject jsonObj=new JSONObject();
			jsonObj.put("time", time);
			
			if(i < 14)
				val += (int)(Math.random() * i * 100 + 1);
			else {
				val -= (int)(Math.random() * i * 100 + 1);
				val = Math.max(val, 0);
			}
			jsonObj.put("info", val);
			jArray.add(jsonObj);
		}
		
		return jArray;
	}
	
	@RequestMapping(value="dashboard_chartDemo19.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray dashboardChartDemo19(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		//List<Dashboard> beanList = dashboardSvc.getNewDashboardChart6();
		
		JSONArray jArray = new JSONArray();
		
		String days[] = {"Mon", "Tue", "Wed", "Ths", "Fri", "Sat", "Sun"};
		for(int i=0; i<7; i++) {
			Dashboard info = new Dashboard();
			info.setProc_date(days[i]);
			info.setCnt1((int)(Math.random() * 5) + 1);
			info.setCnt2((int)(Math.random() * 5) + 1);
			info.setCnt3((int)(Math.random() * 5) + 1);
			
			JSONObject jsonObj=new JSONObject();
			jsonObj.put("day19", info.getProc_date());
			jsonObj.put("hdd", info.getCnt1());
			jsonObj.put("memory", info.getCnt2());
			jsonObj.put("cpu", info.getCnt3());
			jArray.add(jsonObj);
		}
		
		return jArray;
	}
	
	@RequestMapping(value="dashboard_chartDemo20.html", method={RequestMethod.POST})
	@ResponseBody
	public JSONArray dashboardChartDemo20(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		//List<Dashboard> beanList = dashboardSvc.getNewDashboardChart6();
		
		JSONArray jArray = new JSONArray();
		
		JSONObject jsonObj=new JSONObject();
		jsonObj.put("hdd", (int)(Math.random() * 99) + 1);
		jsonObj.put("memory", (int)(Math.random() * 99) + 1);
		jsonObj.put("cpu", (int)(Math.random() * 99) + 1);
		jArray.add(jsonObj);
		
		return jArray;
	}
	
	//----------------------------------------------------------------------------------------
	// dashboard 지도ver.
	//
	//----------------------------------------------------------------------------------------
	@RequestMapping(value="dashboard_map1.html", method={RequestMethod.POST})
	@ResponseBody
	public HashMap dashboardMap1(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		
		/*int idx = Integer.parseInt(parameters.get("index"));
		TempData data = dashboardMapSvc.getTempData(idx);
		HashMap map = new HashMap();
		map.put("name", data.getName());
		map.put("waringStep", data.getWaringStep());
		
		return map;*/
		return null;
	}
	
	@RequestMapping(value="dashboard_map2.html", method={RequestMethod.POST})
	@ResponseBody
	public HashMap dashboardMap2(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		/*int idx = Integer.parseInt(parameters.get("index"));
		TempData data = dashboardMapSvc.getTempData(idx);
		
		HashMap map = new HashMap();
		
		map.put("list", data.getData4());
		map.put("waringStep", data.getWaringStep());
		map.put("id", data.getId());
		map.put("posTop", data.getPosTop());
		map.put("posLeft", data.getPosLeft());
		
		return map;*/
		return null;
	}
	
	// 조직별 위험현황 순위
	@RequestMapping(value="dashboard_map3.html", method={RequestMethod.POST})
	@ResponseBody
	public List dashboardMap3(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		/*int idx = Integer.parseInt(parameters.get("index"));
		TempData data = dashboardMapSvc.getTempData(idx);
		
		return data.getData3();*/
		
		return null;
	}
	
	@RequestMapping(value="dashboard_map4.html", method={RequestMethod.POST})
	@ResponseBody
	public List dashboardMap4(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		/*int idx = Integer.parseInt(parameters.get("index"));
		TempData data = dashboardMapSvc.getTempData(idx);
		
		return data.getData4();*/
		
		return null;
	}
	
	@RequestMapping(value="dashboard_map5.html", method={RequestMethod.POST})
	@ResponseBody
	public List dashboardMap5(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		/*int idx = Integer.parseInt(parameters.get("index"));
		TempData data = dashboardMapSvc.getTempData(idx);
		
		return data.getData5();*/
		
		return null;
	}
	
	@RequestMapping(value="dashboard_map6.html", method={RequestMethod.POST})
	@ResponseBody
	public List dashboardMap6(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		/*int idx = Integer.parseInt(parameters.get("index"));
		TempData data = dashboardMapSvc.getTempData(idx);
		
		return data.getData6();*/
		
		return null;
	}
	
	@RequestMapping(value="dashboard_map_init.html", method={RequestMethod.POST})
	@ResponseBody
	public HashMap dashboardMap_init(@RequestParam Map<String, String> parameters,HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		/*List<TempData> list = dashboardMapSvc.getTempDataAll();
		List res = new ArrayList();

		for(int i=0; i<list.size(); i++) {
			TempData td = list.get(i);
			HashMap map = new HashMap();
			map.put("image", td.getImage());
			map.put("id", td.getId());
			res.add(map);
		}
		
		return res;*/

		HashMap res = dashboardMapSvc.initMap();
		
		return res;
	}
	
}
