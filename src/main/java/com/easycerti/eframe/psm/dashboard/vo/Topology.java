package com.easycerti.eframe.psm.dashboard.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 토폴로지 VO
 * 
 * @author yjyoo
 * @since 2015. 5. 4.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 4.           yjyoo            최초 생성
 *
 * </pre>
 */
public class Topology extends AbstractValueObject {
	
	// 부모키
	private String parent_key;

	// 부모값
	private String parent_value;
	
	// 자신키
	private String this_key;
	
	// 자신값
	private String this_value;
	
	// 토폴로지 깊이
	private Integer dept;
	
	// 이미지 경로
	private String img_src;

	// 이미지 사이즈
	private Integer img_size;
	
	// 갯수
	private Integer cnt;
	
	// 중심키
	private String key;
	
	// 실시간 여부
	private String realtime;
	
	// 찾고자하는 IP
	private String search_ip;
	
	// 검색일자
	private String start_date;
	
	// 시작 시
	private String starth;

	// 시작 분
	private String startm;
	
	// 시작 시분
	private String starthm;
	
	// 종료 시
	private String endh;

	// 종료 분
	private String endm;
	
	// 종료 시분
	private String endhm;
	
	// 사원 ID
	private String emp_user_id;
	
	// IP
	private String user_ip;
	
	// 대상시스템
	private String system_seq;
	
	// 상위N명
	private String top_n;
	
	// 상위N명 사용여부
	private String isTop_n;
	
	// 검색조건 사용여부
	private String toggleCd;
	
	// 서브메뉴아이디
	private String sub_menu_id;
	
	private String privacyType;

	private List<String> auth_idsList;
	
	public List<String> getAuth_idsList() {
		return auth_idsList;
	}

	public void setAuth_idsList(List<String> auth_idsList) {
		this.auth_idsList = auth_idsList;
	}

	public String getSub_menu_id() {
		return sub_menu_id;
	}

	public void setSub_menu_id(String sub_menu_id) {
		this.sub_menu_id = sub_menu_id;
	}

	public String getParent_key() {
		return parent_key;
	}

	public void setParent_key(String parent_key) {
		this.parent_key = parent_key;
	}

	public String getParent_value() {
		return parent_value;
	}

	public void setParent_value(String parent_value) {
		this.parent_value = parent_value;
	}

	public String getThis_key() {
		return this_key;
	}

	public void setThis_key(String this_key) {
		this.this_key = this_key;
	}

	public String getThis_value() {
		return this_value;
	}

	public void setThis_value(String this_value) {
		this.this_value = this_value;
	}

	public Integer getDept() {
		return dept;
	}

	public void setDept(Integer dept) {
		this.dept = dept;
	}

	public String getImg_src() {
		return img_src;
	}

	public void setImg_src(String img_src) {
		this.img_src = img_src;
	}

	public Integer getImg_size() {
		return img_size;
	}

	public void setImg_size(Integer img_size) {
		this.img_size = img_size;
	}

	public Integer getCnt() {
		return cnt;
	}

	public void setCnt(Integer cnt) {
		this.cnt = cnt;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getRealtime() {
		return realtime;
	}

	public void setRealtime(String realtime) {
		this.realtime = realtime;
	}

	public String getSearch_ip() {
		return search_ip;
	}

	public void setSearch_ip(String search_ip) {
		this.search_ip = search_ip;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getStarth() {
		return starth;
	}

	public void setStarth(String starth) {
		this.starth = starth;
	}

	public String getStartm() {
		return startm;
	}

	public void setStartm(String startm) {
		this.startm = startm;
	}

	public String getStarthm() {
		return starthm;
	}

	public void setStarthm(String starthm) {
		this.starthm = starthm;
	}

	public String getEndh() {
		return endh;
	}

	public void setEndh(String endh) {
		this.endh = endh;
	}

	public String getEndm() {
		return endm;
	}

	public void setEndm(String endm) {
		this.endm = endm;
	}

	public String getEndhm() {
		return endhm;
	}

	public void setEndhm(String endhm) {
		this.endhm = endhm;
	}
	
	public String getEmp_user_id() {
		return emp_user_id;
	}

	public void setEmp_user_id(String emp_user_id) {
		this.emp_user_id = emp_user_id;
	}

	public String getUser_ip() {
		return user_ip;
	}

	public void setUser_ip(String user_ip) {
		this.user_ip = user_ip;
	}

	public String getSystem_seq() {
		return system_seq;
	}

	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}

	public String getTop_n() {
		return top_n;
	}

	public void setTop_n(String top_n) {
		this.top_n = top_n;
	}

	public String getIsTop_n() {
		return isTop_n;
	}

	public void setIsTop_n(String isTop_n) {
		this.isTop_n = isTop_n;
	}

	public String getToggleCd() {
		return toggleCd;
	}

	public void setToggleCd(String toggleCd) {
		this.toggleCd = toggleCd;
	}

	public String getPrivacyType() {
		return privacyType;
	}

	public void setPrivacyType(String privacyType) {
		this.privacyType = privacyType;
	}

}
