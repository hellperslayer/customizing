package com.easycerti.eframe.psm.dashboard.vo;

import java.util.List;

import com.easycerti.eframe.common.vo.AbstractValueObject;

/**
 * 대시보드 VO
 * 
 * @author yjyoo
 * @since 2015. 5. 12.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 12.           yjyoo            최초 생성
 *
 * </pre>
 */
public class Dashboard extends AbstractValueObject {
	// 실시간 여부
	private String realtime;
	// 일자
	private String proc_date;
	
	// 시간
	private String proc_time;

	// 시
	private String proc_hour;
	
	// 분
	private String proc_min;
	
	// 초
	private String proc_sec;
	
	// 대상 시스템
	private String system_seq;
	
	// 첫번째 항목
	private String data1;
	
	// 두번째 항목
	private String data2;
	
	// 세번째 항목
	private String data3;
	
	// 네번째 항목
	private String data4;
	
	// 첫번째 항목의 갯수
	private int cnt1;
	
	// 두번째 항목의 갯수
	private int cnt2;
	
	// 세번째 항목의 갯수
	private int cnt3;
	
	// 네번째 항목의 갯수
	private int cnt4;
	
	private String pop_from;
	
	private String occr_dt;
	
	private String rule_nm;
	
	private String rule_cd;
	
	private Integer cnt;
	
	private String scen_seq;
	
	List<String> auth_idsList;
	
	private String scen_name;
	
	private String dept_id;
	private int server_seq;
	private String rank;
	private int type1;
	private int type2;
	private int type3;
	private int type4;
	private int type5;
	private int type6;
	private int type7;
	private int type8;
	private int type9;
	private int type10;
	private int type11;
	private int type12;
	private int type13;
	private int type14;
	private int type15;
	private int type16;
	private int type17;
	private int type18;
	
	private String server_seqSt;
	private String server_name;
	
	private String ui_type;
	
	public String getServer_name() {
		return server_name;
	}
	public void setServer_name(String server_name) {
		this.server_name = server_name;
	}
	public String getServer_seqSt() {
		return server_seqSt;
	}

	public void setServer_seqSt(String server_seqSt) {
		this.server_seqSt = server_seqSt;
	}

	private String searchType;
	private String logCtType;
	
	
	public String getLogCtType() {
		return logCtType;
	}

	public void setLogCtType(String logCtType) {
		this.logCtType = logCtType;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public int getServer_seq() {
		return server_seq;
	}

	public void setServer_seq(int server_seq) {
		this.server_seq = server_seq;
	}

	public String getOccr_dt() {
		return occr_dt;
	}

	public void setOccr_dt(String occr_dt) {
		this.occr_dt = occr_dt;
	}

	public String getRule_nm() {
		return rule_nm;
	}

	public void setRule_nm(String rule_nm) {
		this.rule_nm = rule_nm;
	}

	public String getRule_cd() {
		return rule_cd;
	}

	public void setRule_cd(String rule_cd) {
		this.rule_cd = rule_cd;
	}

	public Integer getCnt() {
		return cnt;
	}

	public void setCnt(Integer cnt) {
		this.cnt = cnt;
	}

	public String getPop_from() {
		return pop_from;
	}

	public void setPop_from(String pop_from) {
		this.pop_from = pop_from;
	}

	public String getPop_to() {
		return pop_to;
	}

	public void setPop_to(String pop_to) {
		this.pop_to = pop_to;
	}

	private String pop_to;
	
	//시스템별 로그 수집현황 날짜 
	private String system_from;

	private String system_to;
	
	private Integer coord_type;
	
	public Integer getCoord_type() {
		return coord_type;
	}

	public void setCoord_type(Integer coord_type) {
		this.coord_type = coord_type;
	}

	public String getCoord_name() {
		return coord_name;
	}

	public void setCoord_name(String coord_name) {
		this.coord_name = coord_name;
	}

	public String getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(String coordinate) {
		this.coordinate = coordinate;
	}

	public String getCoord_memo() {
		return coord_memo;
	}

	public void setCoord_memo(String coord_memo) {
		this.coord_memo = coord_memo;
	}

	public String getCoord_date() {
		return coord_date;
	}

	public void setCoord_date(String coord_date) {
		this.coord_date = coord_date;
	}

	private int coord_id;
	private String coord_name;
	
	private String coordinate;
	
	private String coord_memo;
	
	private String coord_date;
	
	
	public String getSystem_from() {
		return system_from;
	}

	public void setSystem_from(String system_from) {
		this.system_from = system_from;
	}

	public String getSystem_to() {
		return system_to;
	}

	public void setSystem_to(String system_to) {
		this.system_to = system_to;
	}
	
	
	public String getRealtime() {
		return realtime;
	}

	public void setRealtime(String realtime) {
		this.realtime = realtime;
	}

	public String getProc_date() {
		return proc_date;
	}

	public void setProc_date(String proc_date) {
		this.proc_date = proc_date;
	}
	
	public String getProc_time() {
		return proc_time;
	}

	public void setProc_time(String proc_time) {
		this.proc_time = proc_time;
	}

	public String getProc_hour() {
		return proc_hour;
	}

	public void setProc_hour(String proc_hour) {
		this.proc_hour = proc_hour;
	}

	public String getProc_min() {
		return proc_min;
	}

	public void setProc_min(String proc_min) {
		this.proc_min = proc_min;
	}

	public String getProc_sec() {
		return proc_sec;
	}

	public void setProc_sec(String proc_sec) {
		this.proc_sec = proc_sec;
	}

	public String getSystem_seq() {
		return system_seq;
	}

	public void setSystem_seq(String system_seq) {
		this.system_seq = system_seq;
	}

	public String getData1() {
		return data1;
	}

	public void setData1(String data1) {
		this.data1 = data1;
	}

	public String getData2() {
		return data2;
	}

	public void setData2(String data2) {
		this.data2 = data2;
	}

	public String getData3() {
		return data3;
	}

	public void setData3(String data3) {
		this.data3 = data3;
	}

	public String getData4() {
		return data4;
	}

	public void setData4(String data4) {
		this.data4 = data4;
	}

	public int getCnt1() {
		return cnt1;
	}

	public void setCnt1(int cnt1) {
		this.cnt1 = cnt1;
	}

	public int getCnt2() {
		return cnt2;
	}

	public void setCnt2(int cnt2) {
		this.cnt2 = cnt2;
	}

	public int getCnt3() {
		return cnt3;
	}

	public void setCnt3(int cnt3) {
		this.cnt3 = cnt3;
	}

	public int getCnt4() {
		return cnt4;
	}

	public void setCnt4(int cnt4) {
		this.cnt4 = cnt4;
	}

	public int getCoord_id() {
		return coord_id;
	}

	public void setCoord_id(int coord_id) {
		this.coord_id = coord_id;
	}
	
	
	String system_name;
	String result_type;
	String privacy_type;
	
	int nPrivCount;
	int privacy_seq;
	
	String privacy_desc;
	String dept_name;
	
	String emp_user_id;
	String emp_user_name;
	
	String req_type;
	

	public String getEmp_user_id() {
		return emp_user_id;
	}

	public void setEmp_user_id(String emp_user_id) {
		this.emp_user_id = emp_user_id;
	}

	public String getEmp_user_name() {
		return emp_user_name;
	}

	public void setEmp_user_name(String emp_user_name) {
		this.emp_user_name = emp_user_name;
	}

	public int getPrivacy_seq() {
		return privacy_seq;
	}

	public void setPrivacy_seq(int privacy_seq) {
		this.privacy_seq = privacy_seq;
	}

	public String getDept_name() {
		return dept_name;
	}

	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}

	public String getPrivacy_desc() {
		return privacy_desc;
	}

	public void setPrivacy_desc(String privacy_desc) {
		this.privacy_desc = privacy_desc;
	}

	public String getSystem_name() {
		return system_name;
	}

	public void setSystem_name(String system_name) {
		this.system_name = system_name;
	}

	public String getResult_type() {
		return result_type;
	}

	public void setResult_type(String result_type) {
		this.result_type = result_type;
	}

	public String getPrivacy_type() {
		return privacy_type;
	}

	public void setPrivacy_type(String privacy_type) {
		this.privacy_type = privacy_type;
	}

	public int getnPrivCount() {
		return nPrivCount;
	}

	public void setnPrivCount(int nPrivCount) {
		this.nPrivCount = nPrivCount;
	}

	public String getReq_type() {
		return req_type;
	}

	public void setReq_type(String req_type) {
		this.req_type = req_type;
	}

	public List<String> getAuth_idsList() {
		return auth_idsList;
	}

	public void setAuth_idsList(List<String> auth_idsList) {
		this.auth_idsList = auth_idsList;
	}

	public String getScen_seq() {
		return scen_seq;
	}

	public void setScen_seq(String scen_seq) {
		this.scen_seq = scen_seq;
	}

	public String getScen_name() {
		return scen_name;
	}

	public void setScen_name(String scen_name) {
		this.scen_name = scen_name;
	}

	public String getDept_id() {
		return dept_id;
	}

	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}

	public int getType1() {
		return type1;
	}

	public void setType1(int type1) {
		this.type1 = type1;
	}

	public int getType2() {
		return type2;
	}

	public void setType2(int type2) {
		this.type2 = type2;
	}

	public int getType3() {
		return type3;
	}

	public void setType3(int type3) {
		this.type3 = type3;
	}

	public int getType4() {
		return type4;
	}

	public void setType4(int type4) {
		this.type4 = type4;
	}

	public int getType5() {
		return type5;
	}

	public void setType5(int type5) {
		this.type5 = type5;
	}

	public int getType6() {
		return type6;
	}

	public void setType6(int type6) {
		this.type6 = type6;
	}

	public int getType7() {
		return type7;
	}

	public void setType7(int type7) {
		this.type7 = type7;
	}

	public int getType8() {
		return type8;
	}

	public void setType8(int type8) {
		this.type8 = type8;
	}

	public int getType9() {
		return type9;
	}

	public void setType9(int type9) {
		this.type9 = type9;
	}

	public int getType10() {
		return type10;
	}

	public void setType10(int type10) {
		this.type10 = type10;
	}

	public int getType11() {
		return type11;
	}

	public void setType11(int type11) {
		this.type11 = type11;
	}

	public int getType12() {
		return type12;
	}

	public void setType12(int type12) {
		this.type12 = type12;
	}

	public int getType13() {
		return type13;
	}

	public void setType13(int type13) {
		this.type13 = type13;
	}

	public int getType14() {
		return type14;
	}

	public void setType14(int type14) {
		this.type14 = type14;
	}

	public int getType15() {
		return type15;
	}

	public void setType15(int type15) {
		this.type15 = type15;
	}

	public int getType16() {
		return type16;
	}

	public void setType16(int type16) {
		this.type16 = type16;
	}

	public int getType17() {
		return type17;
	}

	public void setType17(int type17) {
		this.type17 = type17;
	}

	public int getType18() {
		return type18;
	}

	public void setType18(int type18) {
		this.type18 = type18;
	}
	public String getUi_type() {
		return ui_type;
	}
	public void setUi_type(String ui_type) {
		this.ui_type = ui_type;
	}

	
	
	
	
}
