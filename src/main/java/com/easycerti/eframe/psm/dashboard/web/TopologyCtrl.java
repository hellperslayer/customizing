package com.easycerti.eframe.psm.dashboard.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.psm.dashboard.service.TopologySvc;
import com.easycerti.eframe.psm.dashboard.vo.Topology;

/**
 * 토폴로지 관련 Controller
 * 
 * @author yjyoo
 * @since 2015. 5. 4.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 4.           yjyoo            최초 생성
 *
 * </pre>
 */
@Controller
@RequestMapping(value="/topology/*")
public class TopologyCtrl {
	
	@Autowired
	private TopologySvc topologySvc;
	
	/**
	 * 토폴로지 화면 Init
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 6.
	 * @return DataModelAndView
	 */
	@MngtActHist(log_action = "SELECT", log_message = "[모니터링] 토폴로지")
	@RequestMapping(value="init_topologyView.html",method={RequestMethod.POST})
	public DataModelAndView setInitTopology(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		Topology paramBean = CommonHelper.convertMapToBean(parameters, Topology.class);
		DataModelAndView modelAndView = new DataModelAndView();
		//기본적으로 상위 체크되도록
		paramBean.setIsTop_n("on");
		paramBean.setRealtime("on");
		modelAndView.setViewName("dashboard");
		//modelAndView.setViewName("topology");
		modelAndView.addObject("mainFlag", CommonResource.YES);
		modelAndView.addObject("tabFlag", "tab2");
		modelAndView.addObject("mainMenuId", "MENU00030");
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	/**
	 * 토폴로지 검색조건 Init (첫 진입 시)
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 6.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="init_topologySearch.html",method={RequestMethod.POST})
	public DataModelAndView setInitTopologySearch(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		DataModelAndView modelAndView = topologySvc.setInitTopologySearch(parameters);
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}
	
	/**
	 * 토폴로지 1~3 Depth
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 4.
	 * @return DataModelAndView
	 */
	@RequestMapping(value="list_allDepth.html",method={RequestMethod.POST})
	public DataModelAndView findTopologyList(@RequestParam Map<String, String> parameters, HttpServletRequest request){
		DataModelAndView modelAndView = new DataModelAndView();
		
		int dept = Integer.parseInt(parameters.get("dept"));
		
		if(dept == 0) {
			modelAndView = topologySvc.findTopologyList_1depth(parameters, request);
		}
		else if(dept == 1) {
			modelAndView = topologySvc.findTopologyList_2depth(parameters);
		}
		else if(dept == 2) {
			modelAndView = topologySvc.findTopologyList_3depth(parameters);
		}
		
		modelAndView.setViewName(CommonResource.JSON_VIEW);
		
		return modelAndView;
	}

}
