package com.easycerti.eframe.psm.dashboard.dao;

import java.util.List;

import com.easycerti.eframe.psm.dashboard.vo.Topology;

/**
 * 토폴로지 관련 Dao Interface
 * 
 * @author yjyoo
 * @since 2015. 5. 4.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 4.           yjyoo            최초 생성
 *
 * </pre>
 */
public interface TopologyDao {
	
	/**
	 * 토폴로지 1 Depth
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 4.
	 * @return List<Topology>
	 */
	public List<Topology> findTopologyList_1depth(Topology topology);
	
	/**
	 * 토폴로지 2 Depth
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 6.
	 * @return List<Topology>
	 */
	public List<Topology> findTopologyList_2depth(Topology topology);
	
	/**
	 * 토폴로지 3 Depth
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 7.
	 * @return List<Topology>
	 */
	public List<Topology> findTopologyList_3depth(Topology topology);
	
}
