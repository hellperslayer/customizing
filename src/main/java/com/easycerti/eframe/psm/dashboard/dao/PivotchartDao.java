package com.easycerti.eframe.psm.dashboard.dao;

import java.util.List;

import com.easycerti.eframe.psm.dashboard.vo.Dashboard;
import com.easycerti.eframe.psm.search.vo.SearchSearch;


public interface PivotchartDao {
	
	public List<Dashboard> getNewDashboardChart2(SearchSearch search);
	public Integer getNewDashboardChart2Ct(SearchSearch search);
	
	public List<Dashboard> getNewDashboardChart3(Dashboard dashboard);
	public List<Dashboard> getNewDashboardChart4(Dashboard dashboard);
	public List<Dashboard> getNewDashboardChart5(Dashboard dashboard);
	public List<Dashboard> getNewDashboardChart6(Dashboard dashboard);
	public List<Dashboard> getNewDashboardChart7(Dashboard dashboard);
	public List<Dashboard> getNewDashboardChart8(Dashboard dashboard);
	public List<Dashboard> getNewDashboardChart9(Dashboard dashboard);
	public List<Dashboard> getNewDashboardChart10(Dashboard dashboard);
	public List<Dashboard> getNewDashboardChart11(Dashboard dashboard);
	public List<Dashboard> getNewDashboardChart11_data(Dashboard dashboard);
	public List<Dashboard> getNewDashboardChart12(Dashboard dashboard);
	public List<Dashboard> getNewDashboardChart13(Dashboard dashboard);
	public List<Dashboard> getNewDashboardChart14(Dashboard dashboard);
	public List<Dashboard> findEmpDetailByDeptTop10(Dashboard dashboard);
	public List<Dashboard> findEmpDetailByUserTop10(Dashboard dashboard);

	public List<Dashboard> getDownloadLogChart1(Dashboard dashboard);
	public List<Dashboard> getDownloadLogChart2(SearchSearch search);
	public Integer getDownloadLogChart2Ct(SearchSearch search);
	
	public List<Dashboard> getDownloadLogChart3(Dashboard dashboard);
	public List<Dashboard> getDownloadLogChart4(Dashboard dashboard);
	public List<Dashboard> getDownloadLogChart5(Dashboard dashboard);
	public List<Dashboard> getDownloadLogChart6(Dashboard dashboard);
	public Integer getDownloadLogChart6Ct(SearchSearch search);
	public Integer getNewDashboardChart6Ct(SearchSearch search);
	public List<Dashboard> getDownloadLogChart6(SearchSearch search);
	public List<Dashboard> getNewDashboardChart6(SearchSearch search);
}
