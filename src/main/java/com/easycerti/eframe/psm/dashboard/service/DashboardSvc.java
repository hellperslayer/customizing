package com.easycerti.eframe.psm.dashboard.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.psm.dashboard.vo.Dashboard;

/**
 * 대시보드 관련 Service Interface
 * 
 * @author yjyoo
 * @since 2015. 5. 12.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 12.           yjyoo           최초 생성
 *   2015. 5. 15.			yjyoo			수집 서버 현황 대시보드 추가
 *   2015. 5. 26.			yjyoo			부서별 로그 수집 현황 대시보드 추가
 *
 * </pre>
 */
public interface DashboardSvc {
	
	/**
	 * 대시보드 1 - 최근 전체 로그 건수 > 전체 수집 로그 건수, 개인정보 처리 로그 건수
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 12.
	 * @return DataModelAndView
	 */
	public DataModelAndView findDashboardList_chart1(Map<String, String> parameters);
	
	
	public DataModelAndView findDashboardList_recent(Map<String, String> parameters);
	/**
	 * 대시보드 2 - 최근 수집 로그 TOP10
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 12.
	 * @return DataModelAndView
	 */
	public DataModelAndView findDashboardList_chart2(Map<String, String> parameters, HttpServletRequest request);
	
	/**
	 * 대시보드 3 - 시스템별 로그 수집 현황
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 12.
	 * @return DataModelAndView
	 */
	public DataModelAndView findDashboardList_chart3(Map<String, String> parameters, HttpServletRequest request);
	
	/**
	 * 대시보드 4 - 개인정보 유형별 로그 수집 현황
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 12.
	 * @return DataModelAndView
	 */
	public DataModelAndView findDashboardList_chart4(Map<String, String> parameters, HttpServletRequest request);
	
	/**
	 * 대시보드 5 - 추출 로그 추이 분석 현황
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 12.
	 * @return DataModelAndView
	 */
	public DataModelAndView findDashboardList_chart5(Map<String, String> parameters, HttpServletRequest request);
	
	/**
	 * 대시보드 6 - 추출조건별 추출로그 현황
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 12.
	 * @return DataModelAndView
	 */
	public DataModelAndView findDashboardList_chart6(Map<String, String> parameters, HttpServletRequest request);
	
	/**
	 * 대시보드 7 - 부서별 로그 수집 현황
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 26.
	 * @return DataModelAndView
	 */
	public DataModelAndView findDashboardList_chart7(Map<String, String> parameters, HttpServletRequest request);
	
	/**
	 * 대시보드 8 - 수집 서버 현황
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 15.
	 * @return DataModelAndView
	 */
	public DataModelAndView findDashboardList_chart8(Map<String, String> parameters, HttpServletRequest request);
	public DataModelAndView findDashboardList_chart_emp(Map<String, String> parameters);

	public DataModelAndView findDashboardListDelete(Map<String, String> parameters);


	public DataModelAndView findDashboardListUpdate(Map<String, String> parameters);


	public DataModelAndView findDashPopupList(Map<String, String> parameters);

	public DataModelAndView extract_chart(Map<String, String> parameters);
	
	public List<Dashboard> getNewDashboardChart1();
	public List<Dashboard> getNewDashboardChart1(Dashboard dashboard);
	public List<Dashboard> getNewDashboardChart2();
	public List<Dashboard> getNewDashboardChart3();
	public List<Dashboard> getNewDashboardChart4();
	public List<Dashboard> getNewDashboardChart5();
	public List<Dashboard> getNewDashboardChart6();
	public List<Dashboard> getNewDashboardChart7();
	public List<Dashboard> getNewDashboardChart8();
	public List<Dashboard> getNewDashboardChart9();
	public List<Dashboard> getNewDashboardChart10();
	public List<Dashboard> getNewDashboardChart11();
	public List<Dashboard> getNewDashboardChart12();
	public List<Dashboard> getNewDashboardChart13();


	public DataModelAndView setDashboardView(Map<String, String> parameters, HttpServletRequest request);
}
