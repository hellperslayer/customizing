package com.easycerti.eframe.psm.dashboard.service.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.psm.dashboard.dao.TopologyDao;
import com.easycerti.eframe.psm.dashboard.service.TopologySvc;
import com.easycerti.eframe.psm.dashboard.vo.Topology;

/**
 * 토폴로지 관련 Service Implements
 * 
 * @author yjyoo
 * @since 2015. 5. 4.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 4.           yjyoo            최초 생성
 *
 * </pre>
 */
@Repository
public class TopologySvcImpl implements TopologySvc {
	
	@Autowired
	private TopologyDao topologyDao;
	
	@Override
	public DataModelAndView setInitTopologySearch(Map<String, String> parameters) {
	Topology paramBean = CommonHelper.convertMapToBean(parameters, Topology.class);

	if(paramBean.getRealtime() == null) {
	paramBean.setRealtime("on");
	}
	if(paramBean.getIsTop_n() == null) {
	paramBean.setIsTop_n("on");
	}

	SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");

	String nowDate = format.format(new Date());
	String h = nowDate.substring(8,10);
	String m = nowDate.substring(10,12);


	String starth = paramBean.getStarth();
	String startm = paramBean.getStartm();
	String endh = paramBean.getEndh();
	String endm = paramBean.getEndm();
	Calendar cal = Calendar.getInstance();
	cal.add(cal.MINUTE, -30);

	// 첫 로딩시는 현재 시간으로 맵핑
	if(starth == null || starth.equals(CommonResource.EMPTY_STRING)) {
	//paramBean.setEndh(Integer.toString(Integer.parseInt(h)));
	paramBean.setStarth(Integer.toString(cal.get(cal.HOUR_OF_DAY)));
	}
	if(startm == null || startm.equals(CommonResource.EMPTY_STRING)) {
	//paramBean.setEndm(Integer.toString(Integer.parseInt(m)));
	paramBean.setStartm(Integer.toString(cal.get(cal.MINUTE)));
	}
	if(endh == null || endh.equals(CommonResource.EMPTY_STRING)) {
	paramBean.setEndh(Integer.toString(Integer.parseInt(h)));
	}
	if(endm == null || endm.equals(CommonResource.EMPTY_STRING)) {
	paramBean.setEndm(Integer.toString(Integer.parseInt(m)));
	}

	DataModelAndView modelAndView = new DataModelAndView();
	modelAndView.addObject("paramBean", paramBean);

	return modelAndView;
	}
	
	@Override
	public DataModelAndView findTopologyList_1depth(Map<String, String> parameters, HttpServletRequest request) {
		Topology paramBean = CommonHelper.convertMapToBean(parameters, Topology.class);
		List<String> auth_idsList = (List<String>) request.getSession().getAttribute("auth_ids_list");
		paramBean.setAuth_idsList(auth_idsList);
		DataModelAndView modelAndView = new DataModelAndView();
		try {
			List<Topology> topologys_1depth = topologyDao.findTopologyList_1depth(paramBean);
			
			
			modelAndView.addObject("topologys", topologys_1depth);
			modelAndView.addObject("paramBean", paramBean);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView findTopologyList_2depth(Map<String, String> parameters) {
		Topology paramBean = CommonHelper.convertMapToBean(parameters, Topology.class);
		
		String start_date = paramBean.getStart_date().replaceAll("-", "");
		String starth = CommonHelper.formatTime(paramBean.getStarth());
		String startm = CommonHelper.formatTime(paramBean.getStartm());
		String endh = CommonHelper.formatTime(paramBean.getEndh());
		String endm = CommonHelper.formatTime(paramBean.getEndm());
		
		String starthm = starth + "" + startm + "00";
		String endhm = endh + "" + endm + "59";
		
		paramBean.setStart_date(start_date);
		paramBean.setStarthm(starthm);
		paramBean.setEndhm(endhm);
		
		List<Topology> topologys_2depth = topologyDao.findTopologyList_2depth(paramBean);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("topologys", topologys_2depth);
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
	
	@Override
	public DataModelAndView findTopologyList_3depth(Map<String, String> parameters) {
		Topology paramBean = CommonHelper.convertMapToBean(parameters, Topology.class);
		
		String start_date = paramBean.getStart_date().replaceAll("-", "");
		String starth = CommonHelper.formatTime(paramBean.getStarth());
		String startm = CommonHelper.formatTime(paramBean.getStartm());
		String endh = CommonHelper.formatTime(paramBean.getEndh());
		String endm = CommonHelper.formatTime(paramBean.getEndm());
		
		String starthm = starth + "" + startm + "00";
		String endhm = endh + "" + endm + "59";
		
		paramBean.setStart_date(start_date);
		paramBean.setStarthm(starthm);
		paramBean.setEndhm(endhm);
		
		String key = paramBean.getKey();
		if(key.length() > 2) {
			//paramBean.setUser_ip(key.substring(0, key.length() - 2));
			paramBean.setEmp_user_id(key.substring(0, key.length() - 2));
			paramBean.setSystem_seq(key.substring(key.length() - 2));
		}
		
		List<Topology> topologys_3depth = topologyDao.findTopologyList_3depth(paramBean);
		
		DataModelAndView modelAndView = new DataModelAndView();
		modelAndView.addObject("topologys", topologys_3depth);
		modelAndView.addObject("paramBean", paramBean);
		
		return modelAndView;
	}
}
