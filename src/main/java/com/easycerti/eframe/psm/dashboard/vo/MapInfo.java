package com.easycerti.eframe.psm.dashboard.vo;
/**
 * 대시보드 지도 VO
 * 
 */
public class MapInfo {
	
	private String map_name;
	
	private String img_name;
	
	private String use_flag;

	public String getMap_name() {
		return map_name;
	}

	public void setMap_name(String map_name) {
		this.map_name = map_name;
	}

	public String getImg_name() {
		return img_name;
	}

	public void setImg_name(String img_name) {
		this.img_name = img_name;
	}

	public String getUse_flag() {
		return use_flag;
	}

	public void setUse_flag(String use_flag) {
		this.use_flag = use_flag;
	}	
}
