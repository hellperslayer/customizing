package com.easycerti.eframe.core.type.handler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.EnumSet;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import com.easycerti.eframe.core.type.Symbolic;

public class AbstractSymbolicTypeHandler<E extends Enum<E>> extends
		BaseTypeHandler<E> {

	private EnumSet<E> symbolicEnumSet;

	protected AbstractSymbolicTypeHandler(Class<E> symbolicEnumClass) {
		if (!Symbolic.class.isAssignableFrom(symbolicEnumClass))
			throw new RuntimeException("Class should be '"
					+ Symbolic.class.getName() + "'type - "
					+ symbolicEnumClass.getName());
		symbolicEnumSet = EnumSet.allOf(symbolicEnumClass);
	}

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, E parameter,
			JdbcType jdbcType) throws SQLException {

		if (parameter != null)
			ps.setString(i, ((Symbolic) parameter).getSymbol());
		else
			ps.setNull(i, Types.VARCHAR);
	}

	@Override
	public E getNullableResult(ResultSet rs, String columnName)
			throws SQLException {
		String result = rs.getString(columnName);
		if (rs.wasNull())
			return null;

		return valueOf(result);
	}

	@Override
	public E getNullableResult(ResultSet rs, int columnIndex)
			throws SQLException {
		String result = rs.getString(columnIndex);

		if (rs.wasNull())
			return null;

		return valueOf(result);
	}

	@Override
	public E getNullableResult(CallableStatement cs, int columnIndex)
			throws SQLException {

		String result = cs.getString(columnIndex);

		if (cs.wasNull())
			return null;

		return valueOf(result);
	}

	public E valueOf(String value) {
		for (E e : symbolicEnumSet) {

			if (((Symbolic) e).getSymbol().equals(value))
				return e;
		}
		return null;
	}
}
