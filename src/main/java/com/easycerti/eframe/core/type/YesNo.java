package com.easycerti.eframe.core.type;

public enum YesNo implements Symbolic {

	YES("Y", "사용"), NO("N", "미사용");
// 한글 깨짐 테스트
	private String symbol;
	private String name;

	YesNo(String symbol, String name) {
		this.symbol = symbol;
		this.name = name;
	}

	public String getSymbol() {
		return symbol;
	}

	public String getName() {
		return name;
	}
}
