package com.easycerti.eframe.core.type.handler;

import com.easycerti.eframe.core.type.YesNo;

public class YesNoTypeHandler extends AbstractSymbolicTypeHandler<YesNo> {
	public YesNoTypeHandler(Class<YesNo> javaType) {
		super(javaType);
	}
}
