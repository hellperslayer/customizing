package com.easycerti.eframe.core.type;

public interface Symbolic {

	String getSymbol();

}
