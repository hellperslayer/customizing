package com.easycerti.eframe.core.type.handler;

import com.easycerti.eframe.core.type.Switch;

public class SwitchTypeHandler extends AbstractSymbolicTypeHandler<Switch> {

	public SwitchTypeHandler(Class<Switch> javaType) {
		super(javaType);
	}
}
