package com.easycerti.eframe.core.control.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.FrameworkServlet;

import com.easycerti.eframe.core.control.command.ControlCommand;
import com.easycerti.eframe.core.control.command.ControlResult;
import com.easycerti.eframe.core.key.WebParamKey;
import com.easycerti.eframe.core.web.servlet.ParameterInjectionServlet;

public class ControlServlet extends ParameterInjectionServlet {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -5038180297861572683L;

	private String springDispatcherServletName;
	private boolean accessFiltering = false;
	private String[] accessableAddressPatterns;

	private ApplicationEventPublisher eventPublisher;
	private ServletContext servletContext;

	private static final Logger logger = LoggerFactory
			.getLogger(ControlServlet.class);

	@Override
	protected void postInit() throws ServletException {
		servletContext = getServletContext();
		if ((accessableAddressPatterns != null)
				&& (accessableAddressPatterns[0] != null))
			accessableAddressPatterns = accessableAddressPatterns[0].split(";");
	}

	@Override
	public void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		boolean isAccessable = false;

		String clientIP = request.getRemoteAddr();
		logger.debug("ControlServlet - Received request from '" + clientIP + "'");

		if (accessFiltering && (accessableAddressPatterns != null)) {
			for (String ipRegEx : accessableAddressPatterns) {
				if (clientIP.matches(ipRegEx)) {
					System.out.println(ipRegEx);
					isAccessable = true;
					break;
				}
			}
		}

		if (!isAccessable) {
			logger.debug("ControlServlet - Client IP is not matching with allowed IP patterns");
			sendResponse(response, ControlResult.FORBIDDEN);
			return;
		}

		String commandValue = request
				.getParameter(WebParamKey.CTRL_COMMAND_KEY);
		logger.info("ControlServlet - command=" + commandValue);

		ControlCommand command = null;
		try {
			command = ControlCommand.valueOf(commandValue);
		} catch (Exception e) {
			sendResponse(response, ControlResult.INAVLID);
			return;
		}

		publishEvent(command.getEvent(), response);
	}

	private void publishEvent(ApplicationEvent event,
			HttpServletResponse response) {
		if (event == null) {
			sendResponse(response, ControlResult.NOT_PREPARED);
			return;
		}

		if (eventPublisher == null) {
			eventPublisher = WebApplicationContextUtils
					.getWebApplicationContext(servletContext,
							FrameworkServlet.SERVLET_CONTEXT_PREFIX
									+ springDispatcherServletName);
		}
		if (eventPublisher == null)
			throw new RuntimeException(
					"Failed to find the spring application context - "
							+ springDispatcherServletName);

		eventPublisher.publishEvent(event);
		sendResponse(response, ControlResult.SUCCESS);
	}

	private void sendResponse(HttpServletResponse response, ControlResult result) {
		try {
			PrintWriter writer = response.getWriter();
			writer.println(WebParamKey.CTRL_RESULT_KEY + "=" + result);
			writer.flush();
		} catch (IOException ioe) {
			logger.error("ControlServlet failed to send response", ioe);
		}
	}
}
