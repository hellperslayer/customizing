package com.easycerti.eframe.core.control.event.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEvent;

import com.easycerti.eframe.core.event.handler.ApplicationEventHandler;
import com.easycerti.eframe.core.util.BeanFinder;
import com.easycerti.eframe.psm.control.service.SchedulerService;

public class ReschedulingEventHandler implements ApplicationEventHandler {

	protected SchedulerService schedulerService;

	private static final Logger logger = LoggerFactory
			.getLogger(ReschedulingEventHandler.class);

	@Override
	public void handle(ApplicationEvent event) {

		logger.debug("ReschedulingEventHandler received event");

		if (schedulerService == null) {
			schedulerService = (SchedulerService) BeanFinder
					.getBean(SchedulerService.class);
			if (schedulerService == null)
				throw new RuntimeException("Failed to find SchedulerService");
		}

		try {
			schedulerService.reSchedule();
		} catch (Exception e) {
			logger.error("Scheduler (re)starting failed", e);
		}
	}
}
