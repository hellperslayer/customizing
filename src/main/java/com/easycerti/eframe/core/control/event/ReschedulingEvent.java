package com.easycerti.eframe.core.control.event;

import org.springframework.context.ApplicationEvent;

import com.easycerti.eframe.core.control.command.ControlCommand;

public class ReschedulingEvent extends ApplicationEvent {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -6220924535507041870L;

	public ReschedulingEvent(Object source) {
		super(ControlCommand.class);
	}
}
