package com.easycerti.eframe.core.control.command;

import org.springframework.context.ApplicationEvent;

import com.easycerti.eframe.core.control.event.CacheReloadEvent;

public enum ControlCommand {

	CacheReload(new CacheReloadEvent(null));

//	Rescheduling(new ReschedulingEvent(null));

	private ApplicationEvent event;

	ControlCommand(ApplicationEvent event) {
		this.event = event;
	}

	public ApplicationEvent getEvent() {
		return event;
	}
}