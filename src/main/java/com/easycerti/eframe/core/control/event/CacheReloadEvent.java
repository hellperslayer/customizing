package com.easycerti.eframe.core.control.event;

import org.springframework.context.ApplicationEvent;

import com.easycerti.eframe.core.control.command.ControlCommand;

public class CacheReloadEvent extends ApplicationEvent {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -2421318229272454930L;

	public CacheReloadEvent(Object source) {
		super(ControlCommand.class);
	}
}
