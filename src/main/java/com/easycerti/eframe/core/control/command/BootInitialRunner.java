package com.easycerti.eframe.core.control.command;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.easycerti.eframe.common.dao.BootInitialDao;
import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.service.PoolingService;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.system_management.vo.Menu;
import com.easycerti.eframe.psm.system_management.vo.OptionSetting;

@Component
public class BootInitialRunner {
	
	private static final Logger logger = LoggerFactory.getLogger(BootInitialRunner.class);
	
	@Autowired
	CommonDao cDao;
	
	@Autowired
	BootInitialDao bDao;
	
	@Autowired
	PoolingService poolingService;
	
	@Value("#{configProperties.use_studentId}")
	private String use_studentId;
	
	@PostConstruct
    public void init(){
		Map<String, String> tempMap = new HashMap<>();
		
		if(bDao.findTable("ui_version") <= 0) {
			String create_table = "create table ui_version(	ui_version varchar primary key default '0' );"
					+ "insert into ui_version values ('0');";
			Map<String,String> map = new HashMap<>();
			map.put("create_table", create_table);
			bDao.createTable(map);
		}
		
		String version = "3.5.2020.06.30";
		if(bDao.versionCheck(version)) {
			String ui_type;
			ui_type = cDao.getUiType();
			logger.info(ui_type);
			
			if ("K".equals(ui_type)) {
				if (bDao.getCountQuery_id() <= 0){
					bDao.addColumnQuery_id();
				}
				if (bDao.getCountRd_id() <= 0){
					bDao.addColumnRd_id();
				}
				if (bDao.getCountRd_name() <= 0){
					bDao.addColumnRd_name();
				}
				if (bDao.getCountGrid_id() <= 0){
					bDao.addColumnGrid_id();
				}
				if (bDao.getCountGrid_name() <= 0){
					bDao.addColumnGrid_name();
				}
				if (bDao.getCountRequest_data() <= 0){
					bDao.addColumnRequest_data();
				}
			}
			
			int checkGroupCode_uiType = bDao.checkGroupCode("ui_type");
			if(checkGroupCode_uiType <= 0) {
				Code code = new Code();
				code.setGroup_code_id("ui_type");
				code.setGroup_code_name("UI타입");
				code.setDescription("UI타입별로 커스터마이징 표출");
				code.setUse_flag("Y");
				bDao.insertGroupCode(code);
			}
			
			/*int uiTypeCnt = bDao.checkCountUitype();
			if(uiTypeCnt <= 0) {
				bDao.insertCommonType();
				bDao.insertKiramsType();
				bDao.insertDaejinType();
				bDao.insertMoefType();
				bDao.insertYeojuType();
			}*/
			
			// UI 타입
			if (bDao.checkCountUitype("P") <= 0) bDao.insertCommonType(); 
			if (bDao.checkCountUitype("K") <= 0) bDao.insertKiramsType();
			if (bDao.checkCountUitype("D") <= 0) bDao.insertDaejinType();
			if (bDao.checkCountUitype("M") <= 0) bDao.insertMoefType();
			if (bDao.checkCountUitype("Y") <= 0) bDao.insertYeojuType();
			if (bDao.checkCountUitype("B") <= 0) bDao.insertBohunType();
			if (bDao.checkCountUitype("H") <= 0) bDao.insertHapcheonType();
			if (bDao.checkCountUitype("Kurly") <= 0) bDao.insertKurlyType();
			if (bDao.checkCountUitype("Guri") <= 0) bDao.insertGuriType();
			
			int optionCnt = bDao.checkCountOption();
			if(optionCnt <= 0) {
				bDao.insertUitype();
			}
			
			// report 타입
			if(bDao.checkGroupCode("REPORT_TYPE")<=0) {
				Code code = new Code();
				code.setGroup_code_id("REPORT_TYPE");
				code.setGroup_code_name("점검보고서");
				code.setDescription("보고서 타입 번호로 제목을 호출하기 위한 목적");
				code.setUse_flag("Y");
				bDao.insertGroupCode(code);
			}
			
			if(bDao.checkCodeCount("REPORT_TYPE")<13) {
				String[] reportTitle = { "전체보기", "담당자용_수준진단보고", "CPO_오남용보고", "담당자용_시스템별_수준진단보고", "고유식별정보_처리현황보고",
						"반기별_수준진단보고", "접근권한_관리대장", "접근행위별_접속기록로그_TOP10", "접속기록로그_및_접근권한관리_TOP20", "개인정보_다운로드_보고",
						"권한부여_관리_보고서","통합보고서","다운로드보고서","수준진단보고서" };
				for (int i = 0; i < reportTitle.length; i++) {
					Code code = new Code();
					code.setGroup_code_id("REPORT_TYPE");
					code.setCode_id(String.valueOf(i));
					code.setSort_order(i);
					code.setUse_flag("Y");
					code.setCode_type("System");
					code.setCode_name(reportTitle[i]);
					code.setDescription(reportTitle[i]);
					if (bDao.checkReportCode(code) <= 0)
						bDao.insertCode(code);
				}
			}
			
			
			
			//----------------------------------- 옵션 설정-----------------------------------
			// 검색창 타입
			int checkOption_searchType = bDao.checkOptionSetting("search_type");
			if(checkOption_searchType <= 0) {
				OptionSetting option = new OptionSetting();
				option.setOption_id("search_type");
				option.setOption_name("검색창 타입");
				option.setDescription("검색창 기본 펼치기/접기");
				option.setValue("Y");
				option.setFlag_name("펼치기, 접기");
				bDao.insertOptionSetting(option);
			}
			
			if(bDao.findTable("biz_log_sql") <= 0) {
				String create_table = "create table biz_log_sql (log_seq integer not null, sql_seq serial not null";
				create_table += ", sql text, system_seq varchar(4), proc_date varchar(8), proc_time varchar(6), proc_datetime timestamp without time zone)";
				Map<String,String> map = new HashMap<>();
				map.put("create_table", create_table);
				bDao.createTable(map);
			}
			
			if(bDao.findTable("biz_log_body") <= 0) {
				String create_table = "create table biz_log_body (log_seq integer, req_body text";
				create_table += ", resp_body text, primary key (log_seq))";
				Map<String,String> map = new HashMap<>();
				map.put("create_table", create_table);
				bDao.createTable(map);
			}

			
			if (bDao.checkOptionSetting("sbiz_log_body") > 0) {
				String create_query = "delete from option_setting where option_id = 'sbiz_log_body'";
				Map<String,String> map = new HashMap<>();
				map.put("create_query", create_query);
				bDao.createQuery(map);
			} 
			
			if (bDao.checkOptionSetting("sbiz_log_body_req") <= 0) {
				OptionSetting option = new OptionSetting();
				option.setOption_id("sbiz_log_body_req");
				option.setOption_name("요청 본문");
				option.setDescription("요청 본문 보여주기");
				option.setValue("N");
				option.setFlag_name("사용, 미사용");
				bDao.insertOptionSetting(option);
			}
			
			if (bDao.checkOptionSetting("sbiz_log_body_res") <= 0) {
				OptionSetting option = new OptionSetting();
				option.setOption_id("sbiz_log_body_res");
				option.setOption_name("응답 본문");
				option.setDescription("응답 본문 보여주기");
				option.setValue("N");
				option.setFlag_name("사용, 미사용");
				bDao.insertOptionSetting(option);
			}
			
			if(bDao.checkOptionSetting("sbiz_log_sql") <= 0) {
				OptionSetting option = new OptionSetting();
				option.setOption_id("sbiz_log_sql");
				option.setOption_name("쿼리");
				option.setDescription("쿼리 보여주기");
				option.setValue("N");
				option.setFlag_name("사용, 미사용");
				bDao.insertOptionSetting(option);
			}
			
			if(bDao.checkOptionSetting("sbiz_log_sql") <= 0) {
				OptionSetting option = new OptionSetting();
				option.setOption_id("sbiz_log_sql");
				option.setOption_name("쿼리");
				option.setDescription("쿼리 보여주기");
				option.setValue("N");
				option.setFlag_name("사용, 미사용");
				bDao.insertOptionSetting(option);
			}
			
			if(bDao.checkOptionSetting("sbiz_log_file") <= 0) {
				OptionSetting option = new OptionSetting();
				option.setOption_id("sbiz_log_file");
				option.setOption_name("파일정보");
				option.setDescription("파일정보 유무");
				option.setValue("N");
				option.setFlag_name("사용, 미사용");
				bDao.insertOptionSetting(option);
			}
			
			if(bDao.checkOptionSetting("sbiz_log_filedownload") <= 0) {
				OptionSetting option = new OptionSetting();
				option.setOption_id("sbiz_log_filedownload");
				option.setOption_name("파일정보 다운로드");
				option.setDescription("파일정보 다운로드 이용");
				option.setValue("Y");
				option.setFlag_name("사용, 미사용");
				bDao.insertOptionSetting(option);
			}
			
			if(bDao.checkOptionSetting("use_systemSeq") <= 0) {
				OptionSetting option = new OptionSetting();
				option.setOption_id("use_systemSeq");
				option.setOption_name("시스템별 추출");
				option.setDescription("시스템별 추출 유무");
				option.setValue("N");
				option.setFlag_name("사용, 미사용");
				bDao.insertOptionSetting(option);
			}
			
			if(bDao.checkOptionSetting("use_reportLogo") <= 0) {
				OptionSetting option = new OptionSetting();
				option.setOption_id("use_reportLogo");
				option.setOption_name("점검보고서 로고이미지");
				option.setDescription("점검보고서 로고이미지 유무");
				option.setValue("Y");
				option.setFlag_name("사용, 미사용");
				bDao.insertOptionSetting(option);
			}
			
			if(bDao.checkOptionSetting("use_reportLine") <= 0) {
				OptionSetting option = new OptionSetting();
				option.setOption_id("use_reportLine");
				option.setOption_name("점검보고서 결재라인");
				option.setDescription("점검보고서 결재라인 유무");
				option.setValue("Y");
				option.setFlag_name("사용, 미사용");
				bDao.insertOptionSetting(option);
			}
			
			if(bDao.checkOptionSetting("use_bmt") <= 0) {
				OptionSetting option = new OptionSetting();
				option.setOption_id("use_bmt");
				option.setOption_name("BMT버전");
				option.setDescription("BMT 사용 유무");
				option.setValue("N");
				option.setFlag_name("사용, 미사용");
				bDao.insertOptionSetting(option);
			}
			
			int checkOption_dashboard = bDao.checkOptionSetting("use_dashboard_scenario");
			if(checkOption_dashboard <= 0) {
				OptionSetting option = new OptionSetting();
				option.setOption_id("use_dashboard_scenario");
				option.setOption_name("대시보드 상세시나리오");
				option.setDescription("대시보드 상세시나리오 on/off");
				option.setValue("N");
				option.setFlag_name("사용,미사용");
				bDao.insertOptionSetting(option);
			}
			
			int checkOption_autoSummon = bDao.checkOptionSetting("auto_summon");
			if(checkOption_autoSummon <= 0) {
				OptionSetting option = new OptionSetting();
				option.setOption_id("auto_summon");
				option.setOption_name("자동소명");
				option.setDescription("자동소명 유무");
				option.setValue("N");
				option.setFlag_name("자동,수동");
				bDao.insertOptionSetting(option);
			}
			
			int checkOption_changePwd = bDao.checkOptionSetting("change_pwd");
			if(checkOption_changePwd <= 0) {
				OptionSetting option = new OptionSetting();
				option.setOption_id("change_pwd");
				option.setOption_name("비밀번호 변경기간");
				option.setDescription("비밀번호 변경기간");
				option.setValue("90");
				option.setFlag_name("일");
				bDao.insertOptionSetting(option);
			}
			
			int insert_biz_log_result = bDao.checkOptionSetting("insert_biz_log_result");
			if(insert_biz_log_result <= 0) {
				OptionSetting option = new OptionSetting();
				option.setOption_id("insert_biz_log_result");
				option.setOption_name("개인정보내용처리");
				option.setDescription("개인정보내용처리");
				option.setValue("Y");
				option.setFlag_name("처리함,처리안함");
				bDao.insertOptionSetting(option);
			}
			
			
			  int checkOption_result_owner = bDao.checkOptionSetting("result_owner");
			  if(checkOption_result_owner <= 0) { 
				  OptionSetting option = new OptionSetting(); option.setOption_id("result_owner");
				  option.setOption_name("정보주체"); option.setDescription("정보주체 유무");
				  option.setValue("N"); option.setFlag_name("사용, 미사용");
				  bDao.insertOptionSetting(option); 
			 }
			  
			int checkOption_empUserName_masking = bDao.checkOptionSetting("empUserName_masking");
			if(checkOption_empUserName_masking <= 0) {
				OptionSetting option = new OptionSetting();
				option.setOption_id("empUserName_masking");
				option.setOption_name("사용자명 마스킹처리");
				option.setDescription("사용자명 마스킹처리");
				option.setValue("N");
				option.setFlag_name("사용,미사용");
				bDao.insertOptionSetting(option);
			}
			
			
			
			//----------------------------------- 옵션 설정 end -----------------------------------
			
			if(bDao.findTable("summary_biz_log") <= 0) {
				bDao.createTable_summaryBizLog();
			}
			
			// ========================= 접근권한관리 관련 ==============================
			if(bDao.checkOptionSetting("mode_access_auth") <= 0) {
				OptionSetting option = new OptionSetting();
				option.setOption_id("mode_access_auth");
				option.setOption_name("접근권한관리");
				option.setDescription("접근권한관리 사용 유무");
				option.setValue("N");
				option.setFlag_name("사용, 미사용");
				bDao.insertOptionSetting(option);
			}
			
			int checkGroupCode_bmtName = bDao.checkGroupCode("BMT_NAME");
			if(checkGroupCode_bmtName <= 0) {
				Code code = new Code();
				code.setGroup_code_id("BMT_NAME");
				code.setGroup_code_name("설정이름");
				code.setDescription("BMT버전에서의 이름설정");
				code.setUse_flag("Y");
				bDao.insertGroupCode(code);
			}
			
			if(bDao.checkCode("BMT_NAME") <= 0) {
				Code code = new Code();
				code.setGroup_code_id("BMT_NAME");
				code.setCode_id("easycerti");
				code.setSort_order(1);
				code.setUse_flag("Y");
				code.setCode_type("SYSTEM");
				code.setCode_name("BMT 설정이름");
				code.setDescription("BMT 설정이름");
				bDao.insertCode(code);
			}
			
			if(bDao.checkOptionSetting("mode_desc") <= 0) {
				OptionSetting option = new OptionSetting();
				option.setOption_id("mode_desc");
				option.setOption_name("소명사용자연계유형");
				option.setDescription("소명사용자연계유형 Y)ADMIN_USER, N)EMP_USER");
				option.setValue("Y");
				option.setFlag_name("ADMIN_USER, EMP_USER");
				bDao.insertOptionSetting(option);
			}
			
			// ssh 190906 DB_Backup
			if(bDao.checkOptionSetting("file_delete_date") <= 0) {
				OptionSetting option = new OptionSetting();
				option.setOption_id("file_delete_date");
				option.setOption_name("백업파일 삭제 기간");
				option.setDescription("백업파일 삭제 기간");
				option.setValue("365");
				option.setFlag_name("일");
				bDao.insertOptionSetting(option);
			}
			
			if(bDao.checkOptionSetting("use_fullscan") <= 0) {
				OptionSetting option = new OptionSetting();
				option.setOption_id("use_fullscan");
				option.setOption_name("다운로드로그 풀스캔연동여부");
				option.setDescription("다운로드로그 풀스캔연동여부");
				option.setValue("N");
				option.setFlag_name("사용, 미사용");
				bDao.insertOptionSetting(option);
			}
			
			if(bDao.checkOptionSetting("use_alarm") <= 0) {
				OptionSetting option = new OptionSetting();
				option.setOption_id("use_alarm");
				option.setOption_name("알람사용여부");
				option.setDescription("알람사용여부");
				option.setValue("N");
				option.setFlag_name("사용, 미사용");
				bDao.insertOptionSetting(option);
			}
			
			/*int checkGroupCode_dashboard = bDao.checkGroupCode("DASHBOARD");
			if(checkGroupCode_dashboard <= 0) {
				Code code = new Code();
				code.setGroup_code_id("DASHBOARD");
				code.setGroup_code_name("대시보드");
				code.setDescription("비정상행위별 분석현황 분류");
				code.setUse_flag("Y");
				bDao.insertGroupCode(code);
			}
			
			if(bDao.checkCode("DASHBOARD") <= 0) {
				Code code = new Code();
				code.setGroup_code_id("DASHBOARD");
				code.setCode_id("1000");
				code.setSort_order(1);
				code.setUse_flag("N");
				code.setCode_type("SYSTEM");
				code.setCode_name("대시보드");
				code.setDescription("비정상행위별 분석현황 분류");
				bDao.insertCode(code);
			}*/
			
			
			/*if(cDao.getAccessAuthUse().equals("Y")) {
				if(bDao.findTable("access_auth") <= 0) {
					String create_table = "create table biz_log_body (emp_user_id character varying(50) NOT NULL,";
					create_table += "emp_user_name character varying(100),";
					create_table += "mobile_number character varying(50),";
					create_table += "access_date character varying(8),";
					create_table += "expire_date character varying(8),";
					create_table += "reason text,";	
					create_table += "approver character varying(100),";
					create_table += "system_seq character varying(25) NOT NULL,";
					create_table += "status character(1),";
					create_table += "update_date character varying(8),";
					create_table += "CONSTRAINT access_auth_key PRIMARY KEY (emp_user_id, system_seq)";
					Map<String,String> map = new HashMap<>();
					map.put("create_table", create_table);
					bDao.createTable(map);
				}
			}*/
			// ===========================================================================
			
			tempMap.put("table_name", "image_logo");
			tempMap.put("column_name", "image_type");
			if (bDao.findColumn(tempMap) <= 0) {
				bDao.addColumnImage_type();
				bDao.alterColumnImageLogo_use();
			}
			
			tempMap.put("table_name", "admin_user");
			tempMap.put("column_name", "user_lock");
			if (bDao.findColumn(tempMap) <= 0) {
				bDao.addColumnUser_rock();
			}
			
			tempMap.put("table_name", "admin_user");
			tempMap.put("column_name", "salt_value");
			if (bDao.findColumn(tempMap) <= 0) {
				bDao.addColumnSalt_value();
			}
			
			// t_tag 시퀀스 추가
			if (bDao.findSequence("seq_t_tag") <= 0) {
				bDao.createSequence_t_tag();
				int max = bDao.getMaxTag_seq();
				int tag_seq = 0;
				if (max == 0) { 
					tag_seq = 1;
				} else {
					tag_seq = max;
				}
				bDao.initSequence_t_tag(tag_seq);
			}
			
			// admin_user password type --> varchar(100)
			bDao.alterAdminUser_passwordType();
			
			tempMap.put("table_name", "system_master");
			tempMap.put("column_name", "administrator");
			if (bDao.findColumn(tempMap) <= 0) {
				bDao.addColumnSystemMaster_administrator();
			}
			
			String url = "/systemMngt/addView.html";
			if (bDao.checkMenuByUrl(url) <= 0) {
				Menu menu = new Menu();
				menu.setParent_menu_id("MENU00434");
				menu.setMenu_url(url);
				menu.setMenu_name("시스템 등록");
				menu.setSort_order(1);
				bDao.addMenu_addSystemMaster(menu);
			}
			
			// biz_log_result의 result_content값을 무효화하기 위한 function
			if (bDao.findFunction_insert_biz_log_result() <= 0) {
				bDao.createFunction_insert_biz_log_result();
			}
			
			tempMap.put("table_name", "emp_detail");
			tempMap.put("column_name", "followup");
			if (bDao.findColumn(tempMap) <= 0) {
				bDao.addColumnEmpDetail_follwup();
			}
			
			if (bDao.findTable("threshold_indv") <= 0) {
				bDao.createTable_threshold_indv();
			}
			
			if (bDao.findTable("threshold_dept") <= 0) {
				bDao.createTable_threshold_dept();
			}
			
			if (bDao.findTable("inspection_hist") <= 0) {
				bDao.createTable_inspection_hist();
			}
			
			if(bDao.findTable("biz_log_file") <= 0) {
				bDao.createTable_biz_log_file();
			}
			
			if(bDao.findTable("biz_log_approval") <= 0) {
				bDao.createTable_biz_log_approval();
			}
			
			tempMap.put("table_name", "department");
			tempMap.put("column_name", "full_dept_name");
			if (bDao.findColumn(tempMap) <= 0) {
				bDao.addColumnDepartment_fullDeptName();
				bDao.update_department();
			}
			
			// department update
			if (bDao.findFunction_update_department() <= 0) {
				bDao.createFunction_update_department();
				
				if (bDao.findTrigger_update_department() <= 0) {
					bDao.createTrigger_update_department();
				}
			}
			
			tempMap.put("table_name", "t_tag");
			tempMap.put("column_name", "forcelog_yn");
			if (bDao.findColumn(tempMap) <= 0) {
				bDao.addColumnT_Tag_forcelog_yn();
				bDao.addColumnT_Tag_forcelog_privacy_type();
				bDao.addColumnT_Tag_forcelog_desc();
			}
			
			//소명프로세스 변경 관련 테이블 수정
			tempMap.put("table_name", "emp_user");
			tempMap.put("column_name", "desc_access_cd");
			if (bDao.findColumn(tempMap) <= 0) {
				bDao.addColumnEmpUser_Desc_access_cd();
				bDao.addColumnEmpUser_Desc_access_org_cd();
			}
			
			tempMap.put("table_name", "emp_detail");
			tempMap.put("column_name", "is_summon_yn");
			if (bDao.findColumn(tempMap) <= 0) {
				bDao.addColumnEmpDetail_Is_summon_yn();
			}
			
			//
			tempMap.put("table_name", "agent_master");
			tempMap.put("column_name", "manager_name");
			if (bDao.findColumn(tempMap) <= 0) {
				bDao.addColumnAgentMaster_manager_name();
			}
			
			//기존 테이블 명칭변경 및 소명메인테이블 생성
			if (bDao.findTable("desc_info") <= 0) {
				/*
				 * bDao.renameDescViolation(); bDao.renameDescRaw(); bDao.renameDescResult();
				 * bDao.renameDescMemo(); bDao.renameDescDecision();
				 */
				bDao.createTable_desc_info();
			}
			
			//소명로그 연계테이블
			if (bDao.findTable("desc_log") <= 0) {
				bDao.createTable_desc_log();
			}
			
			//소명이력
			if (bDao.findTable("desc_history") <= 0) {
				bDao.createTable_desc_history();
			}
			
			//소명결재관련
			if (bDao.findTable("desc_appr") <= 0) {
				bDao.createTable_desc_appr();
			}
			
			//부적정 처분
			if (bDao.findTable("abuse_info") <= 0) {
				bDao.createTable_abuse_info();
			}
			
			//다운로드 로그 테이블 생성
			if (bDao.findTable("download_log") <= 0) {
				bDao.createTable_download_log();
			}
			if (bDao.findTable("download_log_result") <= 0) {
				bDao.createTable_download_log_result();
			}
			//다운로드 매핑 설정 테이블 생성
			if (bDao.findTable("t_dlog_tag") <= 0) {
				bDao.createTable_t_dlog_tag();
			}
			
			//다운로드 로그 칼럼추가
			tempMap.put("table_name", "download_log");
			tempMap.put("column_name", "reason_code");
			if (bDao.findColumn(tempMap) <= 0) {
				bDao.addColumnDownload_log_reason_code();
			}
			
			//옵션세팅 칼럼추가
			tempMap.put("table_name", "option_setting");
			tempMap.put("column_name", "auth");
			if (bDao.findColumn(tempMap) <= 0) {
				bDao.addColumnOption_setting_auth();
			}
			
			// 20190927 ssy 비정상위험 시나리오 컬럼 추가
			tempMap.put("table_name", "ruletbl");
			tempMap.put("column_name", "alarm_yn");
			if (bDao.findColumn(tempMap) <= 0) {
				bDao.addColumnRuletbl_alarm_yn();
			}
			
			//summary_statistics 테이블 생성
			if (bDao.findTable("summary_statistics") <= 0) {
				bDao.createTable_summary_statistics();
			}else{
				tempMap.put("table_name", "summary_statistics");
				tempMap.put("column_name", "type31");
				if(bDao.findColumn(tempMap)>0) {
					bDao.renameColumn_summary_statistics();
				}
			}
			
			//summary_statistics_monthly 테이블 생성
			if (bDao.findTable("summary_statistics_monthly") <= 0) {
				bDao.createTable_summary_statistics_monthly();
			}else{
				tempMap.put("table_name", "summary_statistics_monthly");
				tempMap.put("column_name", "type31");
				if(bDao.findColumn(tempMap)>0) {
					bDao.renameColumn_summary_statistics_monthly();
				}
			}
			//summary_reqtype 테이블 생성
			if (bDao.findTable("summary_reqtype") <= 0) {
				bDao.createTable_summary_reqtype();
			}
			//summary_reqtype_monthly 테이블 생성
			if (bDao.findTable("summary_reqtype_monthly") <= 0) {
				bDao.createTable_summary_reqtype_monthly();
			}
			//summary_statistics 테이블 생성
			if (bDao.findTable("summary_time") <= 0) {
				bDao.createTable_summary_time();
			}
			//summary_statistics 테이블 생성
			if (bDao.findTable("summary_time_monthly") <= 0) {
				bDao.createTable_summary_time_monthly();
			}
			//license_history 테이블 생성
			if (bDao.findTable("license_history") <= 0) {
				bDao.createTable_license_history();
			}
			//스케줄러 - 메일연동관련 테이블 생성
			if (bDao.findTable("send_mail_info") <= 0) {
				bDao.createTable_send_mail_info();
			}
			
			//download_log테이블 createIndex_unq_idx_download_log_1 인덱스 추가
	        if (bDao.findIndex("unq_idx_download_log_1") <= 0) {
	            bDao.createIndex_unq_idx_download_log_1(); 
			}
			
			//t_dlog_tag테이블 dlog_tag_seq컬럼 시퀀스 추가
			if (bDao.findSequence("t_dlog_tag_dlog_tag_seq_seq") <= 0) {
				bDao.createSequence_t_dlog_tag_dlog_tag_seq_seq();
				int max = bDao.getMax_dlog_tag_seq();
				int dlog_tag_seq = 0;
				if (max == 0) { 
					dlog_tag_seq = 1;
				} else {
					dlog_tag_seq = max;
				}
				bDao.initSequence_t_tag(dlog_tag_seq);
			}
			
			// report_management 테이블 생성
			if(bDao.findTable("report_management") <= 0) {
				bDao.createTable_report_management();
			}
			
			tempMap.put("table_name", "report_management");
			tempMap.put("column_name", "proc_month");
			if(bDao.findColumn(tempMap)<=0) {
				bDao.addColumnProc_month();
			}
			
			tempMap.put("table_name", "report_management");
			tempMap.put("column_name", "report_type");
			if(bDao.findColumn(tempMap)<=0) {
				bDao.addColumnReport_type();
			}
			
			tempMap.put("table_name", "report_management");
			tempMap.put("column_name", "file_path");
			if(bDao.findColumn(tempMap) <= 0) {
				bDao.addColumnFile_path();
			}
			
			tempMap.put("table_name", "report_management");
			tempMap.put("column_name", "period_type");
			if(bDao.findColumn(tempMap) <= 0) {
				bDao.addColumnPeriod_type();
			}
			
			tempMap.put("table_name", "report_management");
			tempMap.put("column_name", "userfile_path");
			if(bDao.findColumn(tempMap) <= 0) {
				bDao.addColumnUserfile_path();
			}
			
			tempMap.put("table_name", "regular_expression_mng_use");
			tempMap.put("column_name", "use_biz_log_result");
			if(bDao.findColumn(tempMap) <= 0) {
				bDao.addColumnUse_biz_log_result();
			}
			
			tempMap.put("table_name", "emp_detail");
			tempMap.put("column_name", "extract_key");
			if(bDao.findColumn(tempMap) <= 0) {
				bDao.addColumnUse_extract_key();
			}
			
			tempMap.put("table_name", "auth");
			tempMap.put("column_name", "sys_auth_ids");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "alter table auth add column sys_auth_ids VARCHAR(255);COMMENT ON COLUMN auth.sys_auth_ids IS '해당 권한이 접속할 수 있는 시스템 목록'";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			tempMap.put("table_name", "auth");
			tempMap.put("column_name", "sys_report_ids");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "alter table auth add column sys_report_ids VARCHAR(255);COMMENT ON COLUMN auth.sys_report_ids IS '해당 권한이 출력할 수 있는 보고서 시스템코드 목록'";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			tempMap.put("table_name", "auth");
			tempMap.put("column_name", "make_report_ids");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "alter table auth add column make_report_ids VARCHAR(255);COMMENT ON COLUMN auth.make_report_ids IS '해당 권한이 출력할 수 있는 보고서코드 목록'";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			tempMap.put("table_name", "admin_user");
			tempMap.put("column_name", "make_report_auth");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "alter table admin_user add column make_report_auth VARCHAR(2) default 'N';COMMENT ON COLUMN admin_user.make_report_auth IS '보고서 생성 권한. Y : 보고서 생성 가능, N : 보고서 생성 불가 조회만 가능'";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			tempMap.put("table_name", "t_dlog_tag");
			tempMap.put("column_name", "register_status");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "alter table t_dlog_tag add column register_status varchar(2) default 'N'";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			if(bDao.findTable("report_option") <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "CREATE TABLE public.report_option (" + 
						"	code_id varchar NOT NULL," + 
						"	description text NULL," + 
						"	update_id varchar NULL," + 
						"	input_chart varchar NULL," + 
						"	input_description varchar NULL," + 
						"	proc_month varchar NOT NULL," + 
						"	period_type varchar NOT NULL," + 
						"	CONSTRAINT report_description_20200423_pk PRIMARY KEY (code_id, proc_month, period_type)" + 
						")";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			if(bDao.findTable("summary_download") <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "CREATE TABLE summary_download  ( " + 
						"log_delimiter varchar(2) NULL," + 
						"proc_date    varchar(8) NULL," + 
						"system_seq   varchar(25) NULL," + 
						"file_ext     varchar(10) NULL," + 
						"file_ext_cnt int8 NULL " + 
						")";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			if(bDao.findTable("summary_download_monthly") <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "CREATE TABLE summary_download_monthly  ( " + 
						"log_delimiter varchar(2) NULL," + 
						"month        text NULL," + 
						"week         text NULL," + 
						"system_seq   varchar(25) NULL," + 
						"file_ext     varchar(10) NULL," + 
						"file_ext_cnt int8 NULL " + 
						")";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			if(bDao.findTable("summary_download_log") <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "CREATE TABLE summary_download_log  ( " + 
						"proc_date  varchar(8) NULL," + 
						"emp_user_id varchar(16) NULL," + 
						"dept_id    varchar(30) NULL," + 
						"system_seq varchar(4) NULL," + 
						"req_type   varchar(5) NULL," + 
						"cnt        int8 NULL " + 
						")";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			if(bDao.findTable("summary_reqtype") <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "CREATE TABLE summary_reqtype (" + 
						"	log_delimiter varchar(2) NULL," + 
						"	proc_date varchar(8) NULL," + 
						"	system_seq varchar(25) NULL," + 
						"	system_name varchar(32) NULL," + 
						"	dept_id varchar(50) NULL," + 
						"	dept_name varchar(100) NULL," + 
						"	emp_user_id varchar(255) NULL," + 
						"	emp_user_name varchar(100) NULL," + 
						"	req_type varchar(5) NULL," + 
						"	cnt int8 NULL" + 
						")";
				query.put("create_query", create_query);
				bDao.createQuery(query);
				create_query = "CREATE INDEX idx_summary_reqtype_01 ON public.summary_reqtype USING btree (log_delimiter, proc_date DESC, system_seq, dept_id, emp_user_id)";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			if(bDao.findTable("summary_reqtype_monthly") <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "CREATE TABLE summary_reqtype_monthly (" + 
						"	log_delimiter varchar(2) NULL," + 
						"	\"month\" text NULL," + 
						"	week text NULL," + 
						"	system_seq varchar(25) NULL," + 
						"	system_name varchar(32) NULL," + 
						"	dept_id varchar(50) NULL," + 
						"	dept_name varchar(100) NULL," + 
						"	emp_user_id varchar(255) NULL," + 
						"	emp_user_name varchar(100) NULL," + 
						"	req_type varchar(5) NULL," + 
						"	cnt int8 NULL," + 
						"	logcnt int8 NULL" + 
						")";
				query.put("create_query", create_query);
				bDao.createQuery(query);
				create_query = "CREATE INDEX idx_summary_reqtype_monthly_01 ON public.summary_reqtype_monthly USING btree (log_delimiter, month DESC, week, system_seq, dept_id, emp_user_id)";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			if(bDao.findTable("summary_time") <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "CREATE TABLE summary_time (" + 
						"	log_delimiter varchar(2) NULL," + 
						"	proc_date varchar(8) NULL," + 
						"	system_seq varchar(25) NULL," + 
						"	system_name varchar(32) NULL," + 
						"	dept_id varchar(50) NULL," + 
						"	dept_name varchar(100) NULL," + 
						"	emp_user_id varchar(255) NULL," + 
						"	emp_user_name varchar(100) NULL," + 
						"	proc_time varchar(2) NULL," + 
						"	cnt int8 NULL" + 
						")";
				query.put("create_query", create_query);
				bDao.createQuery(query);
				create_query = "CREATE INDEX idx_summary_time_01 ON public.summary_time USING btree (log_delimiter, proc_date DESC, proc_time, dept_id, emp_user_id, cnt)";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			if(bDao.findTable("summary_time_monthly") <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "CREATE TABLE public.summary_time_monthly (" + 
						"	log_delimiter varchar(2) NULL," + 
						"	\"month\" text NULL," + 
						"	week text NULL," + 
						"	system_seq varchar(25) NULL," + 
						"	system_name varchar(32) NULL," + 
						"	dept_id varchar(50) NULL," + 
						"	dept_name varchar(100) NULL," + 
						"	emp_user_id varchar(255) NULL," + 
						"	emp_user_name varchar(100) NULL," + 
						"	proc_time varchar(2) NULL," + 
						"	cnt int8 NULL" + 
						")";
				query.put("create_query", create_query);
				bDao.createQuery(query);
				create_query = "CREATE INDEX idx_summary_time_monthly_01 ON public.summary_time_monthly USING btree (log_delimiter, month DESC, proc_time, dept_id, emp_user_id, cnt)";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			if(bDao.findTable("summary_statistics") <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "CREATE TABLE public.summary_statistics (" + 
						"	log_delimiter varchar(2) NULL," + 
						"	proc_date varchar(8) NULL," + 
						"	system_seq varchar(25) NULL," + 
						"	system_name varchar(32) NULL," + 
						"	dept_id varchar(50) NULL," + 
						"	dept_name varchar(100) NULL," + 
						"	emp_user_id varchar(255) NULL," + 
						"	emp_user_name varchar(100) NULL," + 
						"	type1 int8 NULL," + 
						"	type2 int8 NULL," + 
						"	type3 int8 NULL," + 
						"	type4 int8 NULL," + 
						"	type5 int8 NULL," + 
						"	type6 int8 NULL," + 
						"	type7 int8 NULL," + 
						"	type8 int8 NULL," + 
						"	type9 int8 NULL," + 
						"	type10 int8 NULL," + 
						"	type11 int8 NULL," + 
						"	type12 int8 NULL," + 
						"	type13 int8 NULL," + 
						"	type14 int8 NULL," + 
						"	type15 int8 NULL," + 
						"	type31 int8 NULL," + 
						"	type32 int8 NULL," + 
						"	type33 int8 NULL," + 
						"	cnt int8 NULL," + 
						"	logcnt int8 NULL" + 
						")";
				query.put("create_query", create_query);
				bDao.createQuery(query);
				create_query = "CREATE INDEX idx_summary_statistics_01 ON public.summary_statistics USING btree (log_delimiter, proc_date, system_seq, dept_id, emp_user_id, dept_name, emp_user_name)";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			if(bDao.findTable("access_auth") <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "CREATE TABLE access_auth (" + 
						"	seq serial NOT NULL," + 
						"	system_seq varchar(25) NOT NULL," + 
						"	emp_user_id varchar(50) NOT NULL," + 
						"	emp_user_name varchar(100) NULL," + 
						"	dept_id varchar(30) NULL," + 
						"	access_date varchar(8) NULL," + 
						"	change_date varchar(8) NULL," + 
						"	expire_date varchar(8) NULL," + 
						"	update_date varchar(14) NULL," + 
						"	reason text NULL," + 
						"	update_reason text NULL," + 
						"	process_content text NULL," + 
						"	status bpchar(1) NULL," + 
						"	approver varchar(100) NULL," + 
						"	manual_flag varchar(1) NULL," + 
						"	approver_id varchar(10) NULL," + 
						"	approver_ip varchar(20) NULL," + 
						"	CONSTRAINT access_auth_key PRIMARY KEY (seq)" + 
						")";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			tempMap.put("table_name", "access_auth");
			tempMap.put("column_name", "approver_id");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE access_auth ADD approver_id varchar(10) NULL;" + 
						"COMMENT ON COLUMN public.access_auth.approver_id IS '관리자ID'";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			tempMap.put("table_name", "access_auth");
			tempMap.put("column_name", "approver_ip");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE access_auth ADD approver_ip varchar(20) NULL;" + 
						"COMMENT ON COLUMN public.access_auth.approver_ip IS '관리자IP'";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			tempMap.put("table_name", "system_info");
			tempMap.put("column_name", "server_seq");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "alter table system_info alter column server_seq type varchar(10)'";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			/*보고서 코드 정리*/
			bDao.addReportCodeCPO();
			bDao.addReportCodeAllLog();
			bDao.addReportCodeDownload();
			bDao.updateReportCode();
			
			if(bDao.findTable("agent_option_setting") <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "CREATE TABLE public.agent_option_setting (" + 
						"	agent_seq int4 NOT NULL," + 
						"	agent_options json NOT NULL," + 
						"	option_name varchar(50) NOT NULL," + 
						"	CONSTRAINT agent_option_setting_pk PRIMARY KEY (agent_seq, option_name)" + 
						")";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			if(bDao.findTable("api_threshold") <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "CREATE TABLE public.api_threshold (" + 
						"	system_seq varchar(25) NOT NULL," + 
						"	threshold varchar(5) NULL DEFAULT '0'::character varying," + 
						"	threshold_use varchar(25) NULL DEFAULT 'N'::character varying," + 
						"	CONSTRAINT api_threshold_pkey PRIMARY KEY (system_seq)" + 
						")";
				query.put("create_query", create_query);
				bDao.createQuery(query);
				create_query = "ALTER TABLE public.api_threshold ADD CONSTRAINT api_threshold_system_seq_fkey FOREIGN KEY (system_seq) REFERENCES system_master(system_seq)";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			/*
			List<Code> agentParamInfo = cDao.findAgentParamInfo();
			String mDriver = "", mUrl = "", mId = "", mPw = "";
			String sDriver = "", sUrl = "", sId = "", sPw = "";
			for(Code a : agentParamInfo) {
				if (a.getParam_name().equals("messenger.db.driver")) {
					mDriver = a.getParam_value();
				} else if (a.getParam_name().equals("messenger.db.url")) {
					mUrl = a.getParam_value();
				} else if (a.getParam_name().equals("messenger.db.username")) {
					mId = a.getParam_value();
				} else if (a.getParam_name().equals("messenger.db.password")) {
					mPw = a.getParam_value();
				} else if (a.getParam_name().equals("sms.db.driver")) {
					sDriver = a.getParam_value();
				} else if (a.getParam_name().equals("sms.db.url")) {
					sUrl = a.getParam_value();
				} else if (a.getParam_name().equals("sms.db.username")) {
					sId = a.getParam_value();
				} else if (a.getParam_name().equals("sms.db.password")) {
					sPw = a.getParam_value();
				}
			}
			
			AlarmManager.init(
					mDriver
					, mUrl
					, mId
					, mPw
					, sDriver
					, sUrl
					, sId
					, sPw
				);

			Thread poolingThread = new Thread(poolingService);
			poolingThread.start();*/
			
			/*AlarmManager.init(
					"oracle.jdbc.driver.OracleDriver"
					, "jdbc:oracle:thin:@192.1.1.97:1522/hjdb2"
					, "MSGUSR"
					, "msgusr#hjdb2"
					, ""
					, ""
					, ""
					, ""
				);*/
			tempMap.put("table_name", "add_url");
			tempMap.put("column_name", "system_seq");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE public.add_url ADD system_seq varchar(25) NULL";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			tempMap.put("table_name", "system_master");
			tempMap.put("column_name", "agent_type");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE public.system_master ADD agent_type varchar(10) NULL";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			tempMap.put("table_name", "system_master");
			tempMap.put("column_name", "collection_server_ip");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE public.system_master ADD collection_server_ip varchar(20) NULL";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			tempMap.put("table_name", "system_master");
			tempMap.put("column_name", "analysis_server_ip");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE public.system_master ADD analysis_server_ip varchar(20) NULL";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			tempMap.put("table_name", "backup_hist");
			tempMap.put("column_name", "log_type");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE public.backup_hist ADD log_type varchar(10)";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			tempMap.put("table_name", "backup_hist");
			tempMap.put("column_name", "log_type");
			if(bDao.findColumn(tempMap) > 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "update backup_hist set log_type = null";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			if(bDao.findTable("unique_identification_info") <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "CREATE TABLE public.unique_identification_info (" + 
						"	system_seq varchar(10) NOT NULL," + 
						"	privacy_type varchar(10) NOT NULL," + 
						"	existence_yn varchar(2) NULL DEFAULT 'N'::character varying," + 
						"	information_ct int4 NULL DEFAULT 0," + 
						"	sort_order int4 NULL" + 
						")";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			tempMap.put("table_name", "unique_identification_info");
			tempMap.put("column_name", "sort_order");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE public.unique_identification_info ADD sort_order int4 NULL";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			// AGENT_TYPE 타입
			if(bDao.checkGroupCode("AGENT_TYPE")<=0) {
				Code code = new Code();
				code.setGroup_code_id("AGENT_TYPE");
				code.setGroup_code_name("에이전트 타입");
				code.setDescription("에이전트 타입");
				code.setUse_flag("Y");
				bDao.insertGroupCode(code);
			}
			if(bDao.checkCodeCount("AGENT_TYPE")<13) {
				String[] code_id = {"AF","AB","AH","AL","DTD","FTP","AA","DC","DM","CM","etc"};
				String[] code_name = {"AGENT(filter)","AGENT(BCI)","AGENT(Hybrid)","AGENTLESS","DBtoDB","FTP","AGENT(API)","DOWNLOAD(CLOUD)","DOWNLOAD(MIRROR)","CLOUD MONITORING","기타"};
				for (int i = 0; i < code_id.length; i++) {
					Code code = new Code();
					code.setGroup_code_id("AGENT_TYPE");
					code.setCode_id(code_id[i]);
					code.setSort_order(i+1);
					code.setUse_flag("Y");
					code.setCode_type("SYSTEM");
					code.setCode_name(code_name[i]);
					code.setDescription("");
					if (bDao.checkReportCode(code) <= 0)
						bDao.insertCode(code);
				}
			}
			
			if(bDao.findTable("report_authorize") <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "CREATE TABLE public.report_authorize (" + 
						"	seq int4 NOT NULL," + 
						"	count varchar NULL," + 
						"	name1 varchar NULL," + 
						"	name2 varchar NULL," + 
						"	name3 varchar NULL," + 
						"	html_source varchar NULL," + 
						"	CONSTRAINT report_authorize_pkey PRIMARY KEY (seq)" + 
						");";
				query.put("create_query", create_query);
				bDao.createQuery(query);
				create_query = "INSERT INTO public.report_authorize" + 
						"(seq, count, name1, name2, name3, html_source)" + 
						"VALUES(1, '1', '담당', '검토', '승인', '');";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			// 빅데이터 분석 시나리오 고도화
			tempMap.put("table_name", "emp_user");
			tempMap.put("column_name", "auth_yn");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE emp_user ADD auth_yn varchar(1) NULL";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			if(bDao.findTable("download_log_extract_temp") <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "create table download_log_extract_temp (" + 
						"	like download_log" + 
						")";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			if(bDao.findTable("download_log_extract_temp_rt") <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "create table download_log_extract_temp_rt (" + 
						"	like download_log_extract_temp" + 
						")";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			if(bDao.findTable("report_defaultdesc") <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "CREATE TABLE report_defaultdesc (" + 
						"	report_code varchar NOT NULL," + 
						"	description text NULL," + 
						"	period_type varchar NULL," + 
						"	CONSTRAINT report_defaultdesc_pkey PRIMARY KEY (report_code)" + 
						");";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			tempMap.put("table_name", "ruletbl");
			tempMap.put("column_name", "result_type");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE public.ruletbl ADD result_type varchar(2) NULL;"; 
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			tempMap.put("table_name", "ruletbl");
			tempMap.put("column_name", "limit_type_cnt");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "alter table ruletbl add column limit_type_cnt int default 1";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			tempMap.put("table_name", "ruletbl");
			tempMap.put("column_name", "indv_yn");
			if(bDao.findColumn(tempMap) == 1) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE public.ruletbl ALTER COLUMN indv_yn SET DEFAULT 'Y'::character varying";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			tempMap.put("table_name", "ruletbl");
			tempMap.put("column_name", "system_seq");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE public.ruletbl ADD system_seq varchar(100) NULL;";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			tempMap.put("table_name", "ruletbl");
			tempMap.put("column_name", "ref_val");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE public.ruletbl ADD ref_val text NULL;";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			tempMap.put("table_name", "ruletbl");
			tempMap.put("column_name", "script_desc");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE public.ruletbl ADD script_desc varchar(8000) NULL;"; 
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			tempMap.put("table_name", "ruletbl");
			tempMap.put("column_name", "privacy_seq");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE public.ruletbl ADD privacy_seq varchar(100) NULL;"; 
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			tempMap.put("table_name", "threshold_dept");
			tempMap.put("column_name", "system_seq");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE public.threshold_dept ADD system_seq varchar(25) NOT NULL DEFAULT '00'::character varying";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			tempMap.put("table_name", "threshold_dept");
			tempMap.put("column_name", "rule_seq");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE public.threshold_dept ADD rule_seq int NOT NULL DEFAULT 0";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			tempMap.put("table_name", "threshold_indv");
			tempMap.put("column_name", "rule_seq");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE public.threshold_indv ADD rule_seq int NOT NULL DEFAULT 0";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			tempMap.put("table_name", "threshold_indv");
			tempMap.put("column_name", "system_seq");
			if(bDao.findColumn(tempMap) == 1) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE public.threshold_indv ALTER COLUMN system_seq SET DEFAULT '00'::character varying";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			/*if(bDao.findConstraint("threshold_dept_pkey")<=0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE threshold_dept ADD CONSTRAINT threshold_dept_pkey PRIMARY KEY (dept_id, system_seq, rule_seq)";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			if(bDao.findConstraint("threshold_indv_pkey")<=0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE threshold_indv ADD CONSTRAINT threshold_indv_pkey PRIMARY KEY (emp_user_id, system_seq, rule_seq)";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}*/
			
			if(bDao.findIndex("threshold_dept_dept_id_idx")<=0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "CREATE unique INDEX threshold_dept_dept_id_idx ON public.threshold_dept (dept_id,rule_seq,system_seq)";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			if(bDao.findIndex("threshold_indv_dept_id_idx")<=0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "CREATE unique INDEX threshold_indv_dept_id_idx ON public.threshold_indv (emp_user_id,rule_seq,system_seq)";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			// 정보주체조회 관련
			if(bDao.findTable("sql_info") <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "CREATE TABLE public.sql_info (" + 
						"	sql_seq bigserial NOT NULL," +
						"	\"sql\" text NOT NULL," + 
						"	\"columns\" text NOT NULL," + 
						"	sha1 varchar(40) NULL DEFAULT NULL::character varying," + 
						"	collect_yn varchar(2) NULL DEFAULT 'Y'::character varying," + 
						"	CONSTRAINT pk_table_21 PRIMARY KEY (sql_seq)" + 
						")";
				query.put("create_query", create_query);
				bDao.createQuery(query);
				create_query = "CREATE UNIQUE INDEX sql_info_sha1 ON public.sql_info USING btree (sha1)";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			if(bDao.findTable("log_sql") <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "CREATE TABLE public.log_sql (" + 
						"	log_sql_seq bigserial NOT NULL," +
						"	log_seq int8 NOT NULL," + 
						"	sql_seq int8 NOT NULL," + 
						"	params text NOT NULL," + 
						"	proc_datetime timestamp NOT NULL," + 
						"	result_count int4 NULL," + 
						"	has_privacy_info varchar(2) NULL DEFAULT 'N'::character varying," +
						"	CONSTRAINT pk_log_sql PRIMARY KEY (proc_datetime, log_sql_seq)" +
						") PARTITION BY RANGE (proc_datetime)";
				query.put("create_query", create_query);
				bDao.createQuery(query);
				create_query = "CREATE INDEX fkidx_23 ON ONLY public.log_sql USING btree (sql_seq)";
				query.put("create_query", create_query);
				bDao.createQuery(query);
				create_query = "CREATE INDEX fkidx_9 ON ONLY public.log_sql USING btree (log_seq)";
				query.put("create_query", create_query);
				bDao.createQuery(query);
				create_query = "ALTER TABLE public.log_sql ADD CONSTRAINT fk_22 FOREIGN KEY (sql_seq) REFERENCES sql_info(sql_seq)";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			if(bDao.findTable("sql_result") <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "CREATE TABLE public.sql_result (" + 
						"	result_seq bigserial NOT NULL," +
						"	log_sql_seq int8 NOT NULL," + 
						"	log_seq int8 NOT NULL," + 
						"	\"result\" text NOT NULL," + 
						"	proc_datetime timestamp NOT NULL," + 
						"	result_group_idx int4 NULL," + 
						"	CONSTRAINT pk_sql_result PRIMARY KEY (proc_datetime, result_seq)" +
						") PARTITION BY RANGE (proc_datetime)";
				query.put("create_query", create_query);
				bDao.createQuery(query);
				create_query = "CREATE INDEX fkidx_13 ON ONLY public.sql_result USING btree (log_sql_seq)";
				query.put("create_query", create_query);
				bDao.createQuery(query);
				create_query = "CREATE INDEX fkidx_18 ON ONLY public.sql_result USING btree (log_seq)";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			tempMap.put("table_name", "regular_expression_mng");
			tempMap.put("column_name", "result_type_order");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE public.regular_expression_mng ADD result_type_order int NULL";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			tempMap.put("table_name", "system_master");
			tempMap.put("column_name", "network_type");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE public.system_master ADD network_type varchar(2) NULL";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			if(bDao.findTable("misdetect_summary") <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "CREATE TABLE public.misdetect_summary (" + 
						"	privacy_seq int4 NULL," + 
						"	log_seq int4 primary key," + 
						"	system_seq varchar NULL," + 
						"	proc_date varchar(8) NULL," + 
						"	result_title varchar(200) NULL," + 
						"	result_content varchar(256) NULL," + 
						"	use_yn varchar(2) NULL," + 
						"	misdetect_type varchar NULL," + 
						"	range_to varchar NULL," + 
						"	range_from varchar NULL," + 
						"	privacy_type int4 NULL" + 
						");"
						+ "  CREATE SEQUENCE public.misdetect_summary_log_seq\r\n" + 
						"						INCREMENT BY 1 \r\n" + 
						"						MINVALUE 1  \r\n" + 
						"						NO CYCLE;";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			tempMap.put("table_name", "system_master");
			tempMap.put("column_name", "threshold");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE public.system_master ADD threshold int4 NULL;" + 
						"COMMENT ON COLUMN public.system_master.threshold IS '다운로드 임계치';" + 
						"";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			tempMap.put("table_name", "t_dlog_tag");
			tempMap.put("column_name", "threshold");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE public.t_dlog_tag ADD threshold int4 NULL;" + 
						"COMMENT ON COLUMN public.t_dlog_tag.threshold IS '다운로드 임계치';" + 
						"";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			tempMap.put("table_name", "t_tag");
			tempMap.put("column_name", "forcelog_yn");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE t_tag add column forcelog_yn varchar(100) default 'N'";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}		
			
			if(bDao.findConstraint("regular_expression_mng_use_unique")<1) {
				bDao.updateRegularConflict();
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE regular_expression_mng_use ADD CONSTRAINT regular_expression_mng_use_unique UNIQUE (system_seq,privacy_type)";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			if(bDao.findConstraint("holiday_unique")<1) {
				bDao.updateHolidayConflict();
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE holiday ADD CONSTRAINT holiday_unique UNIQUE (holi_dt)";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			if(bDao.findConstraint("t_dlog_tag_unique")<1) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE t_dlog_tag ADD CONSTRAINT t_dlog_tag_unique UNIQUE (url, system_seq)";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			if(bDao.findTable("inspection_report") <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "CREATE TABLE inspection_report (\r\n" + 
						"	report_seq serial NOT NULL,\r\n" + 
						"	report_data json NULL,\r\n" + 
						"	proc_date varchar NULL,\r\n" + 
						"	report_type varchar NULL,\r\n" + 
						"	update_id varchar NULL,\r\n" + 
						"	CONSTRAINT inspection_report_pkey PRIMARY KEY (report_seq)\r\n" + 
						");";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			tempMap.put("table_name", "inspection_report");
			tempMap.put("column_name", "update_id");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE inspection_report ADD update_id varchar NULL;";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}	
			
			if(bDao.findAlarmMail() <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "INSERT INTO agent_option_setting" + 
						"(agent_seq, agent_options, option_name)" + 
						"VALUES((select coalesce(max(agent_seq),0)+1 from agent_option_setting), '{\"Site_Name\": \"이지서티\", \"Sender_Pass\": \"dlwl5501!\", \"Sender_Email\": \"qa_sender@easycerti.com\", \"Receiver_Email\": \"\"}', '장애알림메일');";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			tempMap.put("table_name", "emp_user");
			tempMap.put("column_name", "use_flag");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE public.emp_user ADD use_flag varchar(2) NOT NULL DEFAULT 'Y'::character varying;";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}	
			
			tempMap.put("table_name", "patch_hist");
			tempMap.put("column_name", "patch_sort");
			if(bDao.findTable("patch_hist") <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "CREATE TABLE public.patch_hist (\r\n" + 
						"	seq serial NOT NULL,\r\n" + 
						"	proc_date varchar NULL,\r\n" + 
						"	patch_type varchar NULL,\r\n" + 
						"	patch_version varchar NULL,\r\n" + 
						"	patch_date varchar NULL DEFAULT to_char(now(), 'YYYY-MM-DD HH24:MI:SS'::text),\r\n" + 
						"	script varchar NULL,\r\n" + 
						"	description varchar NULL,\r\n" + 
						"	patch_sort varchar NULL, -- sw, data, mapping\r\n" + 
						"	CONSTRAINT patch_hist_pkey PRIMARY KEY (seq)\r\n" + 
						");";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}else if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE public.patch_hist ALTER COLUMN patch_date SET DEFAULT to_char(now(), 'YYYY-MM-DD HH24:MI:SS'::text);";
				create_query += "ALTER TABLE public.patch_hist ADD patch_sort varchar NULL;" + 
						"COMMENT ON COLUMN public.patch_hist.patch_sort IS 'sw, data, mapping';";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			bDao.versionUpdate(version);
		}
		
		version = "3.5.2020.07.01";
		if(bDao.versionCheck(version)) {
			tempMap.put("table_name", "inspection_report");
			tempMap.put("column_name", "search_fr");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE inspection_report ADD search_fr varchar NULL;"
						+ "ALTER TABLE inspection_report ADD search_to varchar NULL;"
						+ "ALTER TABLE inspection_report ADD update_time timestamp NULL DEFAULT now();";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			tempMap.put("table_name", "scenario");
			tempMap.put("column_name", "log_delimiter");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE scenario ADD log_delimiter varchar(10) NULL;";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			tempMap.put("table_name", "scenario");
			tempMap.put("column_name", "dng_val");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE public.scenario ADD dng_val int4 NULL;";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			tempMap.put("table_name", "scenario");
			tempMap.put("column_name", "indv_yn");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE public.scenario ADD indv_yn varchar(1) NULL DEFAULT 'Y'::character varying;";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			
			tempMap.put("table_name", "inspection_report");
			tempMap.put("column_name", "report_title");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE inspection_report ADD report_title varchar NULL;";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			tempMap.put("table_name", "emp_detail");
			tempMap.put("column_name", "system_seq");
			if(bDao.findColumn(tempMap) <= 0) {
				HashMap<String, String> query = new HashMap<String, String>();
				String create_query = "ALTER TABLE emp_detail ADD system_seq varchar(25) NULL;";
				query.put("create_query", create_query);
				bDao.createQuery(query);
			}
			bDao.versionUpdate(version);
		}
		version = "3.5.2020.07.22";
		if(bDao.versionCheck(version)) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query ="delete from report_authorize;"
					+ "INSERT INTO report_authorize\r\n" + 
				"(seq, count, name1, name2, name3, html_source)\r\n" + 
				"VALUES(1, '3', '담당', '검토', '승인', '<table border=\"1\" id=\"preview\" cellspacing=\"0\" cellpadding=\"0\" "
				+ "style=\"border-collapse:collapse; border: 1px solid black\"><tbody><tr id=\"authorNameTr\">"
				+ "<td rowspan=\"2\" id=\"authorNameFirst\" valign=\"middle\" style=\"width: 27px; height: 114px; padding: 1.4pt 5.1pt 1.4pt 6pt;\">"
				+ "<p><span>결</span></p><p style=\"margin: 0px\"><span>재</span></p></td>"
				+ "<td valign=\"middle\" class=\"authorNameTd\"><p style=\"margin: 0px; text-align: center;\"><span>담당</span></p></td>"
				+ "<td valign=\"middle\" class=\"authorNameTd\"><p style=\"margin: 0px; text-align: center;\"><span>검토</span></p></td>"
				+ "<td valign=\"middle\" class=\"authorNameTd\"><p style=\"margin: 0px; text-align: center;\"><span>승인</span></p></td>"
				+ "</tr>"
				+ "<tr id=\"authorMarkTr\">"
				+ "<td valign=\"middle\" class=\"authorMarkTd\" style=\"width: 91px; height: 79px; padding: 1.4pt 5.1pt 1.4pt 5.1pt;\"><p><span>&nbsp;</span></p></td>"
				+ "<td valign=\"middle\" class=\"authorMarkTd\" style=\"width: 91px; height: 79px; padding: 1.4pt 5.1pt 1.4pt 5.1pt;\"><p><span>&nbsp;</span></p></td>"
				+ "<td valign=\"middle\" class=\"authorMarkTd\" style=\"width: 91px; height: 79px; padding: 1.4pt 5.1pt 1.4pt 5.1pt;\"><p><span>&nbsp;</span></p></td>"
				+ "</tr></tbody></table>');\r\n";
			query.put("create_query", create_query);
			bDao.createQuery(query);
			
			create_query ="insert into report_defaultdesc\r\n" + 
					"values('2','개인정보의 비정상 처리에 대한 점검 및 후속조치 활동을 통해 개인정보의 오남용 예방 및 유출방지에 힘쓰고 있음',null)\r\n" + 
					"on conflict (report_code)\r\n" + 
					"do update set\r\n" + 
					"description = '개인정보의 비정상 처리에 대한 점검 및 후속조치 활동을 통해 개인정보의 오남용 예방 및 유출방지에 힘쓰고 있음';";
			query.put("create_query", create_query);
			bDao.createQuery(query);
			
			create_query ="insert into report_defaultdesc\r\n" + 
					"values('12','개인정보 다운로드 접속기록을 분석하여 대량 다운로드 행위 예방 등 개인정보보호 강화 활동을 지속적으로 수행하고 있음',null)\r\n" + 
					"on conflict (report_code)\r\n" + 
					"do update set\r\n" + 
					"description = '개인정보 다운로드 접속기록을 분석하여 대량 다운로드 행위 예방 등 개인정보보호 강화 활동을 지속적으로 수행하고 있음';";
			query.put("create_query", create_query);
			bDao.createQuery(query);
			
			create_query ="insert into report_defaultdesc\r\n" + 
					"values('13','안전한 개인정보 관리를 위해 접속기록을 분석하여 개인정보보호 강화 활동을 지속적으로 수행하고 있음',null)\r\n" + 
					"on conflict (report_code)\r\n" + 
					"do update set\r\n" + 
					"description = '안전한 개인정보 관리를 위해 접속기록을 분석하여 개인정보보호 강화 활동을 지속적으로 수행하고 있음';";
			query.put("create_query", create_query);
			bDao.createQuery(query);
			bDao.versionUpdate(version);
		}
		
		version = "3.5.2020.12.16";
		if(bDao.versionCheck(version)) {
			//최종권한 사용여부(DGB전용) 작성자: 권창우 작업날짜:2020-12-30
			int checkOption_summon_cfm_yn = bDao.checkOptionSetting("summon_cfm_yn");
			if(checkOption_summon_cfm_yn <= 0) {
				OptionSetting option = new OptionSetting();
				option.setOption_id("summon_cfm_yn");
				option.setOption_name("소명최종판정_사용여부");
				option.setDescription("소명판정 최종판정단계 사용여부");
				option.setValue("N");
				option.setFlag_name("사용,미사용");
				bDao.insertOptionSetting(option);
			}
			bDao.versionUpdate(version);
		}
		
		
		
		// 비정상위험분석 고도화 start
		tempMap.put("table_name", "emp_detail");
		tempMap.put("column_name", "rule_result_type");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.emp_detail ADD rule_result_type varchar(2) NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "emp_detail");
		tempMap.put("column_name", "rule_view_type");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.emp_detail ADD rule_view_type varchar(2) NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "emp_detail");
		tempMap.put("column_name", "limit_cnt");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.emp_detail ADD limit_cnt int NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "emp_detail");
		tempMap.put("column_name", "result_content");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.emp_detail ADD result_content varchar NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "rule_extract_temp_rt");
		tempMap.put("column_name", "system_seq");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.rule_extract_temp_rt ADD system_seq text NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		
		
		tempMap.put("table_name", "rule_extract_temp_rt");
		tempMap.put("column_name", "limit_cnt");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.rule_extract_temp_rt ADD limit_cnt int4 NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "rule_extract_temp_rt");
		tempMap.put("column_name", "ref_log_seqs");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.rule_extract_temp_rt ADD ref_log_seqs text NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "rule_extract_temp_rt");
		tempMap.put("column_name", "result_type");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.rule_extract_temp_rt ADD result_type varchar(3) NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "rule_extract_temp_rt");
		tempMap.put("column_name", "proc_time");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.rule_extract_temp_rt ADD proc_time varchar(10) NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "rule_extract_temp_rt");
		tempMap.put("column_name", "rule_cnt");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.rule_extract_temp_rt ADD rule_cnt int4 NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "rule_extract_temp_rt");
		tempMap.put("column_name", "remark");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.rule_extract_temp_rt ADD remark varchar NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "rule_extract_temp_rt");
		tempMap.put("column_name", "result_content");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.rule_extract_temp_rt ADD result_content varchar NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		
		tempMap.put("table_name", "ruletbl");
		tempMap.put("column_name", "rule_result_type");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.ruletbl ADD rule_result_type varchar(2) NOT NULL DEFAULT '1'::character varying;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "ruletbl");
		tempMap.put("column_name", "rule_view_type");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.ruletbl ADD rule_view_type varchar(2) NOT NULL DEFAULT 'M'::character varying;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "ruletbl");
		tempMap.put("column_name", "result_type_yn");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.ruletbl ADD result_type_yn varchar(2) NOT NULL DEFAULT 'N'::character varying;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "ruletbl");
		tempMap.put("column_name", "time_view_yn");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.ruletbl ADD time_view_yn varchar(2) NOT NULL DEFAULT 'Y'::character varying;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "ruletbl");
		tempMap.put("column_name", "ip_yn");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.ruletbl ADD ip_yn varchar(2) NOT NULL DEFAULT 'Y'::character varying;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		
		if(bDao.findTable("summary_abnormal") <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "CREATE TABLE public.summary_abnormal (\r\n" + 
					"			proc_date varchar(8) NULL,\r\n" + 
					"			system_seq varchar(25) NULL,\r\n" + 
					"			system_name varchar(32) NULL,\r\n" + 
					"			dept_id varchar(50) NULL,\r\n" + 
					"			dept_name varchar(100) NULL,\r\n" + 
					"			emp_user_id varchar(255) NULL,\r\n" + 
					"			emp_user_name varchar(100) NULL,\r\n" + 
					"			scenario1 bigint NULL,\r\n" + 
					"			scenario2 bigint NULL,\r\n" + 
					"			scenario3 bigint NULL,\r\n" + 
					"			scenario4 bigint NULL,\r\n" + 
					"			scenario5 bigint NULL,\r\n" + 
					"			cnt bigint NULL,\r\n" + 
					"			logcnt bigint NULL\r\n" + 
					"			);" ;
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		if(bDao.findTable("summary_abnormal_monthly") <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "CREATE TABLE public.summary_abnormal_monthly (\r\n" + 
					"	\"month\" text NULL,\r\n" + 
					"	week text NULL,\r\n" + 
					"	system_seq varchar(25) NULL,\r\n" + 
					"	system_name varchar(32) NULL,\r\n" + 
					"	dept_id varchar(50) NULL,\r\n" + 
					"	dept_name varchar(100) NULL,\r\n" + 
					"	emp_user_id varchar(255) NULL,\r\n" + 
					"	emp_user_name varchar(100) NULL,\r\n" + 
					"	scenario1 int8 NULL,\r\n" + 
					"	scenario2 int8 NULL,\r\n" + 
					"	scenario3 int8 NULL,\r\n" + 
					"	scenario4 int8 NULL,\r\n" + 
					"	scenario5 int8 NULL,\r\n" + 
					"	cnt int8 NULL,\r\n" + 
					"	logcnt int8 NULL\r\n" + 
					");" ;
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		if(bDao.findTable("simulate_emp_detail") <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "CREATE TABLE public.simulate_emp_detail (\r\n" + 
					"	occr_dt varchar(8) NOT NULL,\r\n" + 
					"	dept_id varchar(30) NOT NULL,\r\n" + 
					"	emp_user_id varchar(16) NOT NULL,\r\n" + 
					"	emp_user_name varchar(50) NOT NULL,\r\n" + 
					"	dept_name varchar(40) NOT NULL,\r\n" + 
					"	rule_cnt int4 NOT NULL,\r\n" + 
					"	log_seqs text NULL,\r\n" + 
					"	system_seq varchar(25) NULL,\r\n" + 
					"	limit_cnt int4 NULL,\r\n" + 
					"	result_content varchar NULL\r\n" + 
					");"; 
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		
		tempMap.put("table_name", "simulate_emp_detail");
		tempMap.put("column_name", "limit_cnt");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.simulate_emp_detail ADD limit_cnt int4 NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		
		tempMap.put("table_name", "simulate_emp_detail");
		tempMap.put("column_name", "result_content");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.simulate_emp_detail ADD result_content varchar NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		
		if(bDao.findTable("rule_extract_temp_test")<=0){
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "CREATE TABLE public.rule_extract_temp_test (\r\n" + 
					"	occr_dt varchar(8) NOT NULL,\r\n" + 
					"	log_seq int4 NOT NULL,\r\n" + 
					"	rule_cd int4 NOT NULL,\r\n" + 
					"	emp_user_id varchar(16) NOT NULL,\r\n" + 
					"	system_seq varchar(32) NULL,\r\n" + 
					"	limit_cnt int4 NULL,\r\n" + 
					"	\"key\" text NULL,\r\n" + 
					"	ref_log_seqs text NULL,\r\n" + 
					"	result_type varchar(3) NULL,\r\n" + 
					"	proc_time varchar(10) NULL,\r\n" + 
					"	rule_cnt int4 NULL,\r\n" + 
					"	remark varchar NULL,\r\n" + 
					"	result_content varchar NULL\r\n" + 
					");"; 
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		
		tempMap.put("table_name", "rule_extract_temp_test");
		tempMap.put("column_name", "remark");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.rule_extract_temp_test ADD remark varchar NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "rule_extract_temp_test");
		tempMap.put("column_name", "result_content");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.rule_extract_temp_test ADD result_content varchar NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "rule_biz");
		tempMap.put("column_name", "log_delimiter");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.rule_biz ADD log_delimiter varchar(10) NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "rule_biz");
		tempMap.put("column_name", "req_context");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.rule_biz ADD req_context varchar(256) NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "rule_biz");
		tempMap.put("column_name", "log_result_seq");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.rule_biz ADD log_result_seq int8 NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "rule_biz");
		tempMap.put("column_name", "result_type");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.rule_biz ADD result_type text NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "rule_biz");
		tempMap.put("column_name", "proc_time");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.rule_biz ADD proc_time varchar(8) NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "rule_biz");
		tempMap.put("column_name", "result_content");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.rule_biz ADD result_content varchar NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "rule_biz");
		tempMap.put("column_name", "time_view_yn");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.rule_biz ADD time_view_yn varchar NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "rule_biz");
		tempMap.put("column_name", "ip");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.rule_biz ADD ip varchar NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "rule_biz");
		tempMap.put("column_name", "dept_id");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.rule_biz ADD dept_id varchar NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "rule_biz");
		tempMap.put("column_name", "dept_name");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.rule_biz ADD dept_name varchar NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "rule_biz");
		tempMap.put("column_name", "emp_user_id");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.rule_biz ADD emp_user_id varchar NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "rule_biz");
		tempMap.put("column_name", "emp_user_name");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.rule_biz ADD emp_user_name varchar NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "rule_biz");
		tempMap.put("column_name", "system_seq");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.rule_biz ADD system_seq varchar NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "rule_biz");
		tempMap.put("column_name", "remark");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.rule_biz ADD remark varchar NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		if (bDao.findSequence("rule_biz_rule_biz_seq") <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "create sequence rule_biz_rule_biz_seq increment by 1 minvalue 1 no cycle; \n"
					+ "ALTER TABLE public.rule_biz ADD rule_biz_seq bigserial NOT NULL; \n"
					+ "ALTER TABLE rule_biz ALTER COLUMN rule_biz_seq SET DEFAULT nextval('rule_biz_rule_biz_seq'); \n"
					+ "alter table rule_biz drop constraint rule_biz_pkey; \n"
					+ "alter table rule_biz add constraint rule_biz_pkey primary key(rule_biz_seq); ";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "scenario");
		tempMap.put("column_name", "limit_type");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.scenario ADD limit_type int4 NULL DEFAULT 6;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "scenario");
		tempMap.put("column_name", "limit_cnt");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.scenario ADD limit_cnt int4 NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "scenario");
		tempMap.put("column_name", "limit_type_cnt");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.scenario ADD limit_type_cnt int4 NULL DEFAULT 0;"; 
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "scenario");
		tempMap.put("column_name", "system_seq");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.scenario ADD system_seq varchar(100) NULL;"; 
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		if(bDao.findTable("admin_user_hist")<=0){
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "CREATE TABLE admin_user_hist (\r\n" + 
					"			admin_user_id varchar(50) NULL,\r\n" + 
					"			\"password\" varchar(100) NULL,\r\n" + 
					"			pwd_change_time timestamp NULL\r\n" + 
					"		);";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}

		// 비정상위험분석 고도화 end
		if(bDao.findTable("approval_dept_histlist")<=0){
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "CREATE TABLE public.approval_dept_histlist (\r\n" + 
					"	approval_hist_seq serial NOT NULL,\r\n" + 
					"	dept_id varchar(50) NULL,\r\n" + 
					"	approval_id varchar(50) NULL,\r\n" + 
					"	delegation_date varchar(8) NULL,\r\n" + 
					"	delegation_from varchar(8) NULL,\r\n" + 
					"	delegation_to varchar(8) NULL,\r\n" + 
					"	delegation_act text NULL,\r\n" + 
					"	delegation_act_date varchar(50) NULL,\r\n" + 
					"	CONSTRAINT approval_dept_histlist_pkey PRIMARY KEY (approval_hist_seq),\r\n" + 
					"	CONSTRAINT approval_dept FOREIGN KEY (approval_id) REFERENCES admin_user(admin_user_id) ON DELETE CASCADE,\r\n" + 
					"	CONSTRAINT approval_dept_dept_id_fk FOREIGN KEY (dept_id) REFERENCES department(dept_id) ON DELETE CASCADE\r\n" + 
					");"; 
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		if(bDao.findTable("approval_user_histlist")<=0){
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "CREATE TABLE public.approval_user_histlist (\r\n" + 
					"	approval_hist_seq serial NOT NULL,\r\n" + 
					"	approval_id varchar(50) NULL,\r\n" + 
					"	summon_id varchar(50) NULL,\r\n" + 
					"	summon_system_seq varchar(25) NULL,\r\n" + 
					"	delegation_date varchar(8) NULL,\r\n" + 
					"	delegation_from varchar(8) NULL,\r\n" + 
					"	delegation_to varchar(8) NULL,\r\n" + 
					"	delegation_act text NULL,\r\n" + 
					"	delegation_act_date varchar(50) NULL,\r\n" + 
					"	CONSTRAINT approval_user_histlist_pkey PRIMARY KEY (approval_hist_seq),\r\n" + 
					"	CONSTRAINT approval_user FOREIGN KEY (approval_id) REFERENCES admin_user(admin_user_id) ON DELETE CASCADE,\r\n" + 
					"	CONSTRAINT approval_user_fk2 FOREIGN KEY (summon_id, summon_system_seq) REFERENCES emp_user(emp_user_id, system_seq) ON DELETE CASCADE\r\n" + 
					");"; 
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		
		tempMap.put("table_name", "report_management");
		tempMap.put("column_name", "auth_id");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.report_management ADD auth_id varchar(255) NULL;"; 
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		
		tempMap.put("table_name", "report_management");
		tempMap.put("column_name", "auth_type");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.report_management ADD auth_type varchar(2) NULL DEFAULT 'N'::character varying;\r\n" + 
					"COMMENT ON COLUMN public.report_management.auth_type IS 'Y:권한별보고서,N:개인별보고서';"; 
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		
		//시스템 보고서 생성을 위한 boot initail
		//	- 현대건설
		tempMap.put("table_name", "report_management");
		tempMap.put("column_name", "system_seq");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.report_management ADD system_seq character varying(25) NULL;\r\n" + 
					"COMMENT ON COLUMN public.report_management.system_seq IS '시스템별 보고서 일괄 생성을 위한 system_seq 저장 column';"; 
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		
		if (bDao.checkOptionSetting("report_auth_type") <= 0) {
			OptionSetting option = new OptionSetting();
			option.setOption_id("report_auth_type");
			option.setOption_name("보고서 권한별/개인별 이용");
			option.setDescription("보고서 권한별/개인별 이용");
			option.setValue("N");
			option.setFlag_name("권한별,개인별");
			bDao.insertOptionSetting(option);
		}
		
		if(bDao.checkOptionSetting("same_auth_edit") <= 0) {
			OptionSetting option = new OptionSetting();
			option.setOption_id("same_auth_edit");
			option.setOption_name("동일권한 권한수정");
			option.setDescription("권한이 같은 사용자간 권한 하향조정 가능 여부");
			option.setValue("Y");
			option.setFlag_name("사용, 미사용");
			bDao.insertOptionSetting(option);
		}
		
		int checkOption_summon_agent_type = bDao.checkOptionSetting("summon_agent_type");
		if(checkOption_summon_agent_type <= 0) {
			OptionSetting option = new OptionSetting();
			option.setOption_id("summon_agent_type");
			option.setOption_name("소명응답자 설정");
			option.setDescription("소명대리인 매핑 테이블");
			option.setValue("3");
			option.setFlag_name("SUMMON_USER,SUMMON_DEPT,미사용");
			bDao.insertOptionSetting(option);
		}
		
		int checkOption_summon_approval_type = bDao.checkOptionSetting("summon_approval_type");
		if(checkOption_summon_approval_type <= 0) {
			OptionSetting option = new OptionSetting();
			option.setOption_id("summon_approval_type");
			option.setOption_name("소명판정자 설정");
			option.setDescription("소명판정자 매핑 테이블");
			option.setValue("3");
			option.setFlag_name("APPROVAL_USER,APPROVAL_DEPT,미사용");
			bDao.insertOptionSetting(option);
		}
		
		int checkOption_summon_response_type = bDao.checkOptionSetting("summon_response_type");
		if(checkOption_summon_response_type <= 0) {
			OptionSetting option = new OptionSetting();
			option.setOption_id("summon_response_type");
			option.setOption_name("소명답변 페이지 접근");
			option.setDescription("Y)로그인방식,N)답변페이지접근");
			option.setValue("Y");
			option.setFlag_name("로그인방식,답변페이지");
			bDao.insertOptionSetting(option);
		}
		
		if(bDao.checkOptionSetting("summon_alarm_type") <= 0) {
			OptionSetting option = new OptionSetting();
			option.setOption_id("summon_alarm_type");
			option.setOption_name("알람연계유형");
			option.setDescription("알람연계유형");
			option.setValue("1");
			option.setFlag_name("이메일, SMS, 미사용");
			bDao.insertOptionSetting(option);
		}
		
		if(bDao.findTable("summon_info") <= 0) {
			String create_table = "CREATE TABLE summon_info ("
					+	"summon_info_seq bigint NOT NULL,"
					+ 	"emp_detail_seq bigint NOT NULL," 
					+	"req_dt varchar(8) NULL,"
					+	"expect_dt varchar(8) NULL," 
					+ 	"res_dt varchar(8) NULL," 
					+	"decision_dt varchar(8) NULL," 
					+	"req_user_id varchar(255) NULL," 
					+   "res_user_id varchar(255) NULL,"
					+   "res_system_seq varchar(25) NULL,"
					+   "decision_user_id varchar(255) NULL,"
					+   "summon_manual_yn varchar(1) NULL,"
					+   "decision_type varchar(5) NULL,"
					+   "decision_id_manual_yn varchar(1) NULL,"
					+   "decision_id_first varchar(255) NULL,"
					+   "decision_id_second varchar(255) NULL,"
					+   "decision_id_third varchar(255) NULL,"
					+   "decision_id_fourth varchar(255) NULL,"
					+   "decision_id_fifth varchar(255) NULL,"
					+   "summon_status varchar(2) NULL,"
					+   "decision_status varchar(2) NULL DEFAULT '10',"
					+	"decision_status_second varchar(2) NULL DEFAULT '10'::character varying,"
					+	"CONSTRAINT summon_info_pkey PRIMARY KEY (summon_info_seq)" 
					+	");"
					+   "create sequence summon_info_seq increment by 1 minvalue 1 no cycle;"; 
			Map<String,String> map = new HashMap<>();
			map.put("create_table", create_table);
			bDao.createTable(map);
		}
		
		//작성자: 권창우 작업날짜:2021-01-04 -> 변경(version체크에서 컬럼확인으로) 수정날짜: 2021-02-09
		tempMap.put("table_name", "summon_info");
		tempMap.put("column_name", "decision_status_second");
		if(bDao.findColumn(tempMap) <= 0) {
			Map<String,String> value = new HashMap<>();
			value.put("table_name", "summon_info");
			value.put("column_name", "decision_status_second");
			int check_summon_info_decision_status_second = bDao.findColumn(value);
			if(check_summon_info_decision_status_second <= 0) {
				bDao.insert_decision_status_second(value);
			}
		}
		
		if(bDao.findTable("summon_history") <= 0) {
			String create_table = "CREATE TABLE summon_history ("
					+	"summon_history_seq bigint NOT NULL,"
					+ 	"emp_detail_seq bigint NOT NULL," 
					+   "datetime timestamp NULL,"
					+   "user_id varchar(255) NULL,"
					+   "target_id varchar(255) NULL,"
					+   "res_system_seq varchar(25) NULL,"
					+   "msg text NULL,"
					+   "status varchar(2) NULL,"
					+	"CONSTRAINT summon_history_pkey PRIMARY KEY (summon_history_seq)" 
					+	");"
					+   "create sequence summon_history_seq increment by 1 minvalue 1 no cycle;"; 
			Map<String,String> map = new HashMap<>();
			map.put("create_table", create_table);
			bDao.createTable(map);
		}
		
		
		if(bDao.findTable("summon_user") <= 0) {
			String create_table = "CREATE TABLE summon_user ("
					+	"emp_user_id varchar(255) NOT NULL,"
					+ 	"system_seq varchar(25) NOT NULL," 
					+	"summon_id varchar(255) NULL,"
					+	"summon_system_seq varchar(10) NULL," 
					+ 	"delegation_date timestamp NULL," 
					+	"delegation_date_from timestamp NULL," 
					+	"delegation_date_to timestamp NULL,"
					+   "summon_seq serial NOT NULL,"
					+	"CONSTRAINT summon_user_pkey PRIMARY KEY (emp_user_id, system_seq)" 
					+	")"; 
			Map<String,String> map = new HashMap<>();
			map.put("create_table", create_table);
			bDao.createTable(map);
		}
		
		if(bDao.findTable("summon_dept") <= 0) {
			String create_table = "CREATE TABLE summon_dept (" 
					+	"dept_id varchar(30) NOT NULL,"
					+	"summon_id varchar(255) NULL," 
					+	"summon_system_seq varchar(10) NULL," 
					+	"delegation_date timestamp NULL," 
					+	"delegation_date_from timestamp NULL," 
					+	"delegation_date_to timestamp NULL," 
					+	"summon_seq serial NOT NULL,"
					+	"CONSTRAINT summon_dept_pkey PRIMARY KEY (dept_id)" 
					+	")";
			Map<String,String> map = new HashMap<>();
			map.put("create_table", create_table);
			bDao.createTable(map);
		}
		
		if(bDao.findTable("approval_user") <= 0) {
			String create_table = "CREATE TABLE approval_user ("
					+ "approval_id varchar(255) NULL,"
					+ "emp_user_id varchar(255) NULL,"
					+ "system_seq varchar(25) NULL,"
					+ "delegation_date timestamp NULL,"
					+ "delegation_date_from timestamp NULL,"
					+ "delegation_date_to timestamp NULL,"
					+ "CONSTRAINT approval_user_pkey PRIMARY KEY (approval_id, emp_user_id, system_seq)"
					+ ")";
			Map<String,String> map = new HashMap<>();
			map.put("create_table", create_table);
			bDao.createTable(map);
		}
		
		if(bDao.findTable("approval_dept") <= 0) {
			String create_table = "CREATE TABLE approval_dept ("
					+ "approval_id varchar(255) NULL,"
					+ "dept_id varchar(30) NULL,"
					+ "delegation_date timestamp NULL,"
					+ "delegation_date_from timestamp NULL,"
					+ "delegation_date_to timestamp NULL,"
					+ "CONSTRAINT approval_dept_pkey PRIMARY KEY (approval_id, dept_id)"
					+ ")";
			Map<String,String> map = new HashMap<>();
			map.put("create_table", create_table);
			bDao.createTable(map);
		}
		
		tempMap.put("table_name", "summon_user");
		tempMap.put("column_name", "summon_seq");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.summon_user ADD summon_seq serial NOT NULL;"; 
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "summon_dept");
		tempMap.put("column_name", "summon_seq");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.summon_dept ADD summon_seq serial NOT NULL;"; 
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "approval_user");
		tempMap.put("column_name", "approval_seq");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.approval_user ADD approval_seq serial NOT NULL;"; 
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		tempMap.put("table_name", "approval_dept");
		tempMap.put("column_name", "approval_seq");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE public.approval_dept ADD approval_seq serial NOT NULL;"; 
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		
		tempMap.put("table_name", "emp_user");
		tempMap.put("column_name", "self_summon");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE emp_user ADD column self_summon varchar(1) NULL DEFAULT 'Y';"; 
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		
		tempMap.put("table_name", "emp_user");
		tempMap.put("column_name", "external_staff");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE emp_user ADD column external_staff int NULL DEFAULT 0;"; 
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		
		
		if(bDao.findTable("summon_user_histlist") <= 0) {
			String create_table = "CREATE TABLE public.summon_user_histlist (\r\n" + 
					"	summon_hist_seq serial NOT NULL,\r\n" + 
					"	emp_user_id varchar(50) NULL,\r\n" + 
					"	emp_system_seq varchar(25) NULL,\r\n" + 
					"	dept_id varchar(30) NULL,\r\n" + 
					"	summon_id varchar(50) NULL,\r\n" + 
					"	summon_system_seq varchar(25) NULL,\r\n" + 
					"	delegation_date varchar(8) NULL,\r\n" + 
					"	delegation_from varchar(8) NULL,\r\n" + 
					"	delegation_to varchar(8) NULL,\r\n" + 
					"	delegation_act text NULL,\r\n" + 
					"	delegation_act_date varchar(50) NULL,\r\n" + 
					"	CONSTRAINT summon_user_histlist_pkey PRIMARY KEY (summon_hist_seq),\r\n" + 
					"	CONSTRAINT summon_user_fk1 FOREIGN KEY (dept_id) REFERENCES department(dept_id) ON DELETE CASCADE,\r\n" + 
					"	CONSTRAINT summon_user_fk2 FOREIGN KEY (emp_user_id, emp_system_seq) REFERENCES emp_user(emp_user_id, system_seq) ON DELETE CASCADE,\r\n" + 
					"	CONSTRAINT summon_user_fk3 FOREIGN KEY (summon_id, summon_system_seq) REFERENCES emp_user(emp_user_id, system_seq) ON DELETE CASCADE\r\n" + 
					");";
			Map<String,String> map = new HashMap<>();
			map.put("create_table", create_table);
			bDao.createTable(map);
		}
		if(bDao.findTable("summon_dept_histlist") <= 0) {
			String create_table = "CREATE TABLE public.summon_dept_histlist (\r\n" + 
					"	summon_hist_seq serial NOT NULL,\r\n" + 
					"	dept_id varchar(30) NULL,\r\n" + 
					"	summon_id varchar(50) NULL,\r\n" + 
					"	summon_system_seq varchar(25) NULL,\r\n" + 
					"	delegation_date varchar(8) NULL,\r\n" + 
					"	delegation_from varchar(8) NULL,\r\n" + 
					"	delegation_to varchar(8) NULL,\r\n" + 
					"	delegation_act text NULL,\r\n" + 
					"	delegation_act_date varchar(50) NULL,\r\n" + 
					"	CONSTRAINT summon_dept_histlist_pkey PRIMARY KEY (summon_hist_seq),\r\n" + 
					"	CONSTRAINT summon_user_fk1 FOREIGN KEY (dept_id) REFERENCES department(dept_id) ON DELETE CASCADE,\r\n" + 
					"	CONSTRAINT summon_user_fk2 FOREIGN KEY (summon_id, summon_system_seq) REFERENCES emp_user(emp_user_id, system_seq) ON DELETE CASCADE\r\n" + 
					");";
			Map<String,String> map = new HashMap<>();
			map.put("create_table", create_table);
			bDao.createTable(map);
		}
		if(bDao.findTable("approval_user_histlist") <= 0) {
			String create_table = "CREATE TABLE public.approval_user_histlist (\r\n" + 
					"	approval_hist_seq serial NOT NULL,\r\n" + 
					"	approval_id varchar(50) NULL,\r\n" + 
					"	summon_id varchar(50) NULL,\r\n" + 
					"	summon_system_seq varchar(25) NULL,\r\n" + 
					"	delegation_date varchar(8) NULL,\r\n" + 
					"	delegation_from varchar(8) NULL,\r\n" + 
					"	delegation_to varchar(8) NULL,\r\n" + 
					"	delegation_act text NULL,\r\n" + 
					"	delegation_act_date varchar(50) NULL,\r\n" + 
					"	CONSTRAINT approval_user_histlist_pkey PRIMARY KEY (approval_hist_seq),\r\n" + 
					"	CONSTRAINT approval_user FOREIGN KEY (approval_id) REFERENCES admin_user(admin_user_id) ON DELETE CASCADE,\r\n" + 
					"	CONSTRAINT approval_user_fk2 FOREIGN KEY (summon_id, summon_system_seq) REFERENCES emp_user(emp_user_id, system_seq) ON DELETE CASCADE\r\n" + 
					");";
			Map<String,String> map = new HashMap<>();
			map.put("create_table", create_table);
			bDao.createTable(map);
		}
		if(bDao.findTable("approval_dept_histlist") <= 0) {
			String create_table = "CREATE TABLE public.approval_dept_histlist (\r\n" + 
					"	approval_hist_seq serial NOT NULL,\r\n" + 
					"	dept_id varchar(50) NULL,\r\n" + 
					"	approval_id varchar(50) NULL,\r\n" + 
					"	delegation_date varchar(8) NULL,\r\n" + 
					"	delegation_from varchar(8) NULL,\r\n" + 
					"	delegation_to varchar(8) NULL,\r\n" + 
					"	delegation_act text NULL,\r\n" + 
					"	delegation_act_date varchar(50) NULL,\r\n" + 
					"	CONSTRAINT approval_dept_histlist_pkey PRIMARY KEY (approval_hist_seq),\r\n" + 
					"	CONSTRAINT approval_dept FOREIGN KEY (approval_id) REFERENCES admin_user(admin_user_id) ON DELETE CASCADE,\r\n" + 
					"	CONSTRAINT approval_dept_dept_id_fk FOREIGN KEY (dept_id) REFERENCES department(dept_id) ON DELETE CASCADE\r\n" + 
					");";
			Map<String,String> map = new HashMap<>();
			map.put("create_table", create_table);
			bDao.createTable(map);
		}

		
		if(bDao.findTable("system_approval") <= 0) {
			String create_table = "CREATE TABLE public.system_approval (\r\n"
					+ "	system_seq varchar(25) primary key,\r\n"
					+ " system_name varchar(32) not null,\r\n"
					+ " first_auth varchar(32),\r\n"
					+ " second_auth varchar(32),\r\n"
					+ " third_auth varchar(32),\r\n"
					+ " html_source varchar NULL" 
					+ " );"
					+ " insert into option_setting (option_id, option_name, description, value, flag_name, auth)\r\n"
					+ " values ('use_systemReport', '시스템별 보고서 사용여부', '시스템별 보고서 사용여부', 'Y', '사용,미사용', 'AUTH00001'),\r\n"
					+ " ('use_systemApproval', '시스템별 결재 사용여부', '시스템별 결재 사용여부', 'Y', '사용,미사용', 'AUTH00001');";
			Map<String,String> map = new HashMap<>();
			map.put("create_table", create_table);
			bDao.createTable(map);
		}

		if(bDao.checkOptionSetting("print_error_info") <= 0) {
			OptionSetting option = new OptionSetting();
			option.setOption_id("print_error_info");
			option.setOption_name("에러출력");
			option.setDescription("에러출력 유무");
			option.setValue("Y");
			option.setFlag_name("사용, 미사용");
			bDao.insertOptionSetting(option);
		}
		
		//summary_daily 접속기록조회 관련 테이블 추가날짜 (20210216)
		if(bDao.findTable("summary_daily") <= 0) {
			String create_table = "CREATE TABLE summary_daily (\r\n" + 
					"	proc_date varchar(8) not NULL,\r\n" + 
					"	emp_user_id varchar(255) not NULL,\r\n" + 
					"	emp_user_name varchar(50) not NULL,\r\n" + 
					"	dept_name varchar(255) not NULL,\r\n" + 
					"	dept_id varchar(50) not NULL,\r\n" + 
					"	system_seq varchar(25) not NULL,\r\n" + 
					"	user_ip varchar(15) not NULL,\r\n" + 
					"	cnt int8 null,\r\n" + 
					"	constraint summary_daily_pk primary key(proc_date, emp_user_id, emp_user_name, system_seq, dept_name, dept_id, user_ip)\r\n" + 
					");";
			Map<String,String> map = new HashMap<>();
			map.put("create_table", create_table);
			bDao.createTable(map);
		}
		
		if(bDao.findView("v_biz_log_summary") <= 0) {
			Date d = new Date();
			SimpleDateFormat simple = new SimpleDateFormat ( "yyyyMMdd");
			String date = simple.format(d);
			String create_view = "create or replace view v_biz_log_summary as  (\r\n" + 
					"	select PROC_DATE, EMP_USER_ID, EMP_USER_NAME, DEPT_NAME, DEPT_ID, SYSTEM_SEQ, USER_IP, 1 as cnt\r\n" + 
					"	from biz_log_summary_"+date+"\r\n" + 
					"	union all\r\n" + 
					"	 select PROC_DATE, EMP_USER_ID, EMP_USER_NAME, DEPT_NAME, DEPT_ID, SYSTEM_SEQ, USER_IP, cnt\r\n" + 
					"	from summary_daily" + 
					");";
			Map<String,String> map = new HashMap<>();
			map.put("create_view", create_view);
			bDao.create_v_biz_log_summary(map);
		}
		
		tempMap.put("table_name", "api_threshold");
		tempMap.put("column_name", "threshold");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE api_threshold ADD threshold varchar(20) NULL;";
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		
		tempMap.put("table_name", "biz_log_result");
		tempMap.put("column_name", "detection_src");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE biz_log_result ADD COLUMN detection_src varchar(3) NULL;"; 
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		
		tempMap.put("table_name", "biz_log_result");
		tempMap.put("column_name", "result_owner");
		if(bDao.findColumn(tempMap) <= 0) {
			HashMap<String, String> query = new HashMap<String, String>();
			String create_query = "ALTER TABLE biz_log_result ADD COLUMN result_owner varchar(255) NULL;"; 
			query.put("create_query", create_query);
			bDao.createQuery(query);
		}
		
		if(bDao.checkOptionSetting("summon_alarm_yn") <= 0) {
			OptionSetting option = new OptionSetting();
			option.setOption_id("summon_alarm_yn");
			option.setOption_name("소명알람미사용 사용여부");
			option.setDescription("Y)알람사용,N)알람미사용");
			option.setValue("N");
			option.setFlag_name("사용, 미사용");
			bDao.insertOptionSetting(option);
		}
		
    }
}
