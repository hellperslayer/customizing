package com.easycerti.eframe.core.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

/**
 * 
 * 설명 : 로그인 중복체크
 * @author tjlee
 * @since 2015. 5. 18.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 18.           tjlee            최초 생성
 *
 * </pre>
 */
public class SessionManager implements HttpSessionBindingListener
{
	private static SessionManager loginManager = null;
	//로그인한 접속자+아이피 를 담기위한 해시테이블 key=Session , value = id/ip
	private static Hashtable loginUsers = new Hashtable();
	//로그인한접속자의 ip와 id+마지막 시간을 담기위한 해시테이블
	private static Hashtable<String, String> loginUserTime = new Hashtable<String, String>();
	
	/*
	* 싱글톤 패턴 사용
	*/
	public static synchronized SessionManager getInstance(){
		if(loginManager == null){
			loginManager = new SessionManager();
		}
		return loginManager;
	}

	/*
	* 이 메소드는 세션이 연결됐을때 호출된다.(session.setAttribute("login", this))
	* Hashtable에 세션과 접속자 아이디를 저장한다.
	*/
	public void valueBound(HttpSessionBindingEvent event) {
	//session값을 put한다.
		loginUsers.put(event.getSession(), event.getName());
		System.out.println("LOGIN EVENT : "+event.getName());
		System.out.println("CURRENT LOGIN USER : " + getUserCount());
	}

	/*
	* 이 메소드는 세션이 끊겼을때 호출된다.(invalidate)
	* Hashtable에 저장된 로그인한 정보를 제거해 준다.
	*/
	public void valueUnbound(HttpSessionBindingEvent event) {
	//session값을 찾아서 없애준다.
		loginUsers.remove(event.getSession());
		System.out.println(" " + event.getName() + "님이 로그아웃 하셨습니다.");
		System.out.println("현재 접속자 수 : " + getUserCount());
	}

	/*
	* 입력받은 아이디를 해시테이블에서 삭제. 
	* @param userID 사용자 아이디
	* @return void
	*/ 
	public void removeSession(String userId){
		Enumeration e = loginUsers.keys();
		HttpSession session = null;
		while(e.hasMoreElements()){
			session = (HttpSession)e.nextElement();
			if(loginUsers.get(session).equals(userId)){
			//세션이 invalidate될때 HttpSessionBindingListener를 
			//구현하는 클레스의 valueUnbound()함수가 호출된다.
			session.invalidate();
			}
		}
	}


	/*
	* 사용자가 입력한 ID, PW가 맞는지 확인하는 메소드
	* @param userID 사용자 아이디
	* @param userPW 사용자 패스워드
	* @return boolean ID/PW가 일치하는 지 여부
	*/
	public boolean isValid(String userId, String userPw){
	/*
	* 이부분에 Database 로직이 들어간다.
	*/
	return true;
	}


	/*
	* 해당 아이디의 동시 사용을 막기위해서 
	* 이미 사용중인 아이디인지를 확인한다.
	* @param userID 사용자 아이디
	* @return boolean 이미 사용 중인 경우 true, 사용중이 아니면 false
	*/
	public boolean isUsing(String userID){
		return loginUsers.containsValue(userID);
	}
	/*
	* 로그인을 완료한 사용자의 아이디를 세션에 저장하는 메소드
	* @param session 세션 객체
	* @param userID 사용자 아이디
	*/
	public void setSession(HttpSession session, String userId, HttpServletRequest request){
	//이순간에 Session Binding이벤트가 일어나는 시점
	//name값으로 userId, value값으로 자기자신(HttpSessionBindingListener를 구현하는 Object)
		session.setAttribute(userId, this);//login에 자기자신을 집어넣는다.
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar now = Calendar.getInstance();
		String nowdate = sdf.format(now.getTime());
		loginUserTime.put(request.getRemoteAddr(), userId.split("/")[0] + "/" + nowdate);
	}
	
	// 아이피가 같은 해시맵이 있으면 해시맵 리턴, 아니면 널 리턴
	public Hashtable<String, String> getLoginUserTime(HttpServletRequest request){
		if(loginUserTime.get(request.getRemoteAddr()) != null ){
			return loginUserTime;
		}else{
			return null;
		}
	}

	/*
	* 입력받은 세션Object로 아이디를 리턴한다.
	* @param session : 접속한 사용자의 session Object
	* @return String : 접속자 아이디
	*/
	public String getUserID(HttpSession session){
		return (String)loginUsers.get(session);
	}


	/*
	* 현재 접속한 총 사용자 수
	* @return int 현재 접속자 수
	*/
	public int getUserCount(){
		return loginUsers.size();
	}


	/*
	* 현재 접속중인 모든 사용자 아이디를 출력
	* @return void
	*/
	public void printloginUsers(){
		Enumeration e = loginUsers.keys();
		HttpSession session = null;
		System.out.println("===========================================");
		int i = 0;
		while(e.hasMoreElements()){
			session = (HttpSession)e.nextElement();
			System.out.println((++i) + ". 접속자 : " + loginUsers.get(session));
		}
		System.out.println("===========================================");
	}
	
	// 현재세션맵에 담겨있는 아이디값 리턴
	public List<String> printLoginUserIds(){
		List<String> printLoginUserIds = new ArrayList<String>();
		Enumeration e = loginUsers.keys();
		Object session = null;
		int i = 0;
		while(e.hasMoreElements()){
			session = e.nextElement();
			if(session != null){
				String value = (String) loginUsers.get(session);
				printLoginUserIds.add(value.split("/")[0]);
			}
		}
		return printLoginUserIds;
	}
	
	// 아이디가 같은값일때 아이피 리턴 아니면 null 리턴
	public String printLoginUserIp(String userId){
		Enumeration e = loginUsers.keys();
		Object session = null;
		String ip = null;
		int i = 0;
		while(e.hasMoreElements()){
			session = e.nextElement();
			if(session != null){
				String value = (String) loginUsers.get(session);
				if(userId.equals(value.split("/")[0])){
					ip = value.split("/")[1];				                                               
				}
			}
		}
		return ip;
	}
	
	/**
	 * 설명 : 
	 * @author tjlee
	 * @since 2015. 5. 19.
	 * @return Collection
	 */
	public void deleteLoginTime(HttpServletRequest request){
		
	}
	/*
	* 현재 접속중인 모든 사용자리스트를 리턴
	* @return list
	*/
	public Collection getUsers(){
		Collection collection = loginUsers.values();
		return collection;
	}
}
