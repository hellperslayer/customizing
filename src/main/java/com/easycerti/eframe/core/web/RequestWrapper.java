package com.easycerti.eframe.core.web;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public final class RequestWrapper extends HttpServletRequestWrapper{

	public RequestWrapper(HttpServletRequest servletRequest) {
        super(servletRequest);
    }
 
    public String[] getParameterValues(String parameter) {
 
      String[] values = super.getParameterValues(parameter);
      if (values==null)  {
                  return null;
          }
      int count = values.length;
      String[] encodedValues = new String[count];
      for (int i = 0; i < count; i++) {
                 encodedValues[i] = cleanXSS(values[i]);
       }
      return encodedValues;
    }
 
    public String getParameter(String parameter) {
          String value = super.getParameter(parameter);
          if (value == null) {
                 return null;
                  }
          return cleanXSS(value);
    }
 
    public String getHeader(String name) {
        String value = super.getHeader(name);
        if (value == null)
            return null;
        return cleanXSS(value);
 
    }
    
    
	/*
	 * //<input type="hidden">일 때 필터를 거치지 않아 추가한 로직
	 * 
	 * @Override public Map<String, String[]> getParameterMap() { Map<String,
	 * String[]> parameterMap = super.getParameterMap();
	 * 
	 * Map<String, String[]> retMap = new HashMap<String, String[]>();
	 * 
	 * Set<String> keySet = parameterMap.keySet();
	 * 
	 * if( keySet == null ) return this.getParameterMap();
	 * 
	 * Iterator<String> iterator = keySet.iterator(); while(iterator.hasNext()) {
	 * String key = iterator.next();
	 * 
	 * String[] values = parameterMap.get(key); String[] clone = values.clone();
	 * for( int i = 0 ; i < values.length ; i++ ) { String value = values[i]; String
	 * xssFilteredValue = cleanXSS(value); clone[i] = xssFilteredValue; }
	 * 
	 * retMap.put(key, clone); } return retMap; }
	 */
  	
    private String cleanXSS(String value) {
                //You'll need to remove the spaces from the html entities below
    	value = value.replaceAll("&", "&#38;");
    	value = value.replaceAll("<", "&lt;");
    	value = value.replaceAll(">", "&gt;");
    	value = value.replaceAll("#", "&#35;");
    	value = value.replaceAll("\"", "&#quot;");
    	value = value.replaceAll("/", "&#x2F;");
    	value = value.replaceAll("\\)", "&#41;");
    	value = value.replaceAll("\\(", "&#40;");
    	value = value.replaceAll("'", "&#x27;");
        value = value.replaceAll("eval\\((.*)\\)", "");
        value = value.replaceAll("[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']", "\"\"");
        value = value.replaceAll("script", "");
        return value;
    }
    
    public static String decodeXSS(String value) {
    	value = value.replaceAll("&lt;","<");
    	value = value.replaceAll("&gt;",">");
    	value = value.replaceAll("&#35;","#");
    	value = value.replaceAll("&#quot;","\"");
    	value = value.replaceAll("&#x2F;","/");
    	value = value.replaceAll("&#41;","\\)");
    	value = value.replaceAll("&#40;","\\(");
    	value = value.replaceAll("&#38;","&");
    	value = value.replaceAll("&#x27;","'");
        value = value.replaceAll("\"\"","[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']");
    	
    	return value;
    }
}
