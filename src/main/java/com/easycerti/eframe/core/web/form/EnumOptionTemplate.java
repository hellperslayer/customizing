package com.easycerti.eframe.core.web.form;

import java.util.EnumSet;

import org.springframework.beans.factory.InitializingBean;

import com.easycerti.eframe.core.type.YesNo;
import com.easycerti.eframe.psm.calling_management.type.Judge;
import com.easycerti.eframe.psm.calling_management.type.Status;

public class EnumOptionTemplate extends AbstractOptionTemplate implements
		InitializingBean {

	private EnumSet<YesNo> yesNo;
	private EnumSet<Status> status;
	private EnumSet<Judge> judge;

	public void afterPropertiesSet() {
		yesNo = EnumSet.allOf(YesNo.class);
		status = EnumSet.allOf(Status.class);
		judge = EnumSet.allOf(Judge.class);
	}

	public EnumSet<YesNo> getYesNo() {
		return yesNo;
	}
	public EnumSet<Status> getStatus() {
		return status;
	}
	public EnumSet<Judge> getJudge() {
		return judge;
	}
}