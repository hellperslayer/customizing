package com.easycerti.eframe.common.util;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class LogMessageUtil {
/*
	public static void putLogMessge(Map<String, String> params, String title, String... keys) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("[SUCCESS - ");
		stringBuilder.append(title);
		stringBuilder.append("]");
		
		for (String key : keys) { 
			if (key != null && (!"".equals(key))) {
				stringBuilder.append("/");
				stringBuilder.append(key);
				if (params.get(key) != null && (!"".equals(key))) {
					stringBuilder.append(":");
					stringBuilder.append(params.get(key));
				}
			}
		}
		
		params.put("log_message", stringBuilder.toString());
	}
	*/
	public static void putLogMessge(Map<String, String> params, String title, String commaSeperatedString) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("[SUCCESS - ");
		stringBuilder.append(title);
		stringBuilder.append("]");
		
		List<String> keys = Arrays.asList(commaSeperatedString.split(","));
		
		for (String key : keys) { 
			if (key != null && (!"".equals(key))) {
				stringBuilder.append("/");
				stringBuilder.append(key);
				if (params.get(key) != null && (!"".equals(key))) {
					stringBuilder.append(":");
					stringBuilder.append(params.get(key));
				}
			}
		}
		
		params.put("log_message", stringBuilder.toString());
	}
}
