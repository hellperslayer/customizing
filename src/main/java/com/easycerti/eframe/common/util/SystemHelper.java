package com.easycerti.eframe.common.util;

import java.util.regex.Matcher;

import javax.servlet.http.HttpServletRequest;

import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;
import com.easycerti.eframe.psm.system_management.vo.EmpUser;

/**
 * 시스템영역에 공통되는 처리를 위함</br>
 * - ex) 페이징, 파라미터 체크 등
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 *  
 *
 * </pre>
 */
public class SystemHelper {
	
	/**
	 * 세션에서 관리자 정보를 가져온다.
	 */
	public static AdminUser getAdminUserInfo(HttpServletRequest request){
		return (AdminUser) CommonHelper.findSessionUser(request);
	}
	
	/**
	 * request에 담긴 관리자 ID 정보를 꺼낸다.</br>
	 */
	public static String getAdminUserIdFromRequest(HttpServletRequest request){
		AdminUser admin = getAdminUserInfo(request);
		if(admin == null){
			return CommonResource.EMPTY_STRING;
		}else{
			return admin.getAdmin_user_id();
		}
	}
	
	/**
	 * 세션에서 인가정보를 가져온다.
	 */
	public static AdminUser findSessionUser(HttpServletRequest request){
		AdminUser user = null;
		
		try {
			user = (AdminUser)CommonHelper.findSessionUser(request);
		} catch (ClassCastException e) {
			return null;
		}
		return user;
	}
	
	/**
	 * 세션에서 인가정보를 가져온다.
	 */
//	public static AdminUserBean findSessionUser(HttpServletRequest request, String rootContextPath){
//		return (AdminUserBean)CommonHelper.findSessionUser(request,rootContextPath);
//	}
	
	/**
	 * 해당코드 수정 가능여부</br>
	 * - true : 수정가능, false : 수정불가(시스템)
	 */
	public static boolean enableEditCode(Code codeBean){
		if(codeBean != null && "SYSTEM".equals(codeBean.getCode_type())){
			return false;
		}else{
			return true;
		}
	}
	
	/**
	 * 클라이언트 요청시 요청타입 json여부 확인.
	 */
	public static boolean checkRequestType(HttpServletRequest request){
		Matcher mather = CommonResource.PATTERN_REQUEST_ACCEPT_JSON_TYPE.matcher(request.getHeader("accept"));
		return mather.matches();
	}
	
	/**
	 * 세션에서 사용자 정보를 가져온다.
	 */
	public static EmpUser findSessionEmpUser(HttpServletRequest request){
		EmpUser user = null;
		try {
			user = (EmpUser)CommonHelper.findSessionUser(request);
		} catch (ClassCastException e) {
			return null;
		}
		return user;
	}
	
}
