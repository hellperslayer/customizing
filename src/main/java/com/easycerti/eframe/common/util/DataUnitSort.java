package com.easycerti.eframe.common.util;

import java.util.List;

import com.easycerti.eframe.psm.system_management.vo.PrivacyReport;

public class DataUnitSort {
	
	static String[] unitWord = {"", "십", "백", "천", "만", "십만", "백만", "천만", "억"};
	static int[] resultArr = new int[unitWord.length];
	static String resultStr = "";
	
	static String unitKor = "";
	static int unitNum = 1;
	static String resultUnit = "";
	
	//word 그리드 메소드 동일 적용
	public static List<PrivacyReport> dataUnitSort(PrivacyReport pr1, List<PrivacyReport> targetList, String flag){
		targetList.get(0).setDataUnit_num(unitNum);
		
		//1분기 보고서 생성
		if(pr1.getPeriod_type().equals("1")) {
			
			//최대값 
			int max_number = max_period_type1(targetList, flag);
			if(String.valueOf(max_number).length() > 3) { //세 자리수 표현
				unitKor = getStringUnit(max_number);
				unitNum = Integer.parseInt(unitKor);
				
				resultUnit = getKoreanUnit(unitNum);
				
				//기준 단위로 나눈 데이터 값으로 변환
				//targetList = convertDataUnit(targetList, unitNum, resultUnit, flag);
			}
			
		}else if(pr1.getPeriod_type().equals("2")) {
			
			int max_number = max_period_type2(targetList, flag);
			if(String.valueOf(max_number).length() > 3) {
				unitKor = getStringUnit(max_number);
				unitNum = Integer.parseInt(unitKor);
				
				resultUnit = getKoreanUnit(unitNum);
				
				//targetList = convertDataUnit_period02(targetList, unitNum, resultUnit, flag);
			}
			
		}else if(pr1.getPeriod_type().equals("4")) {
			int max_number = 0;
			/*if(flag.equals("grid")) {
				max_number = max_period_type4(targetList, flag);
			}else {*/
				max_number = max_period_type2(targetList, flag);
				if(String.valueOf(max_number).length() > 3) {
					unitKor = getStringUnit(max_number);
					unitNum = Integer.parseInt(unitKor);
					
					resultUnit = getKoreanUnit(unitNum);
					
					//targetList = convertDataUnit_period02(targetList, unitNum, resultUnit, flag);
				}
			//}
		}
		return targetList;
	}
	
	//compareData
	public static List<PrivacyReport> dataUnitSort_compare(PrivacyReport pr1, List<PrivacyReport> targetList, String flag){
		
		int max_number = max_number_compare(targetList, flag);
		targetList.get(0).setDataUnit_num(unitNum);
		
		if(	String.valueOf(max_number).length() > 3) { //세 자리수 표현
			unitKor = getStringUnit(max_number);
			unitNum = Integer.parseInt(unitKor);
			
			resultUnit = getKoreanUnit(unitNum);
			
			//기준 단위로 나눈 데이터 값으로 변환
			//targetList = convertDataUnit_compare(targetList, unitNum, resultUnit, flag);
		}
		return targetList;
	}
	
	
	
	
	//========================================================================================================
	//기준 단위 값으로 원본 데이터를 나눈 몫
	private static List<PrivacyReport> convertDataUnit(List<PrivacyReport> targetList, int unitNum2, String resultUnit, String flag) {
		int tmp = 0;
		for(int i=0;i<targetList.size();i++) {
			
			if(flag.equals("wordGrid")) {
				tmp = targetList.get(i).getType1() / unitNum;
				targetList.get(i).setType1(tmp);
				tmp = targetList.get(i).getType1_1() / unitNum;
				targetList.get(i).setType1_1(tmp);
				
				tmp = targetList.get(i).getLogcnt() / unitNum;
				targetList.get(i).setLogcnt(tmp);
				tmp = targetList.get(i).getPrivacyCnt() / unitNum;
				targetList.get(i).setPrivacyCnt(tmp);
			}
			if(flag.equals("ext")) {
				tmp = Integer.parseInt(targetList.get(i).getFile_ext_cnt()) / unitNum;
				targetList.get(i).setFile_ext_cnt(String.valueOf(tmp));
			}
			if(flag.equals("grid")) {
				tmp = targetList.get(i).getType1() / unitNum;
				targetList.get(i).setType1(tmp);
				tmp = targetList.get(i).getPrivacyCnt() / unitNum;
				targetList.get(i).setPrivacyCnt(tmp);
			}
			if(flag.equals("process")) {
				tmp = targetList.get(i).getType1() / unitNum;
				targetList.get(i).setType1(tmp);
				tmp = targetList.get(i).getLogcnt() / unitNum;
				targetList.get(i).setLogcnt(tmp);
			}
	
			targetList.get(i).setDataUnit(resultUnit);
		}
		
		return targetList;
	}
	
	
	//compare unit
	private static List<PrivacyReport> convertDataUnit_compare(List<PrivacyReport> targetList, int unitNum2, String resultUnit2, String flag) {
		
		int tmp = 0;
		for(int i=0;i<targetList.size();i++) {
			if(flag.equals("ext")) {
				tmp = targetList.get(i).getPriv_ext_cnt() / unitNum;
				targetList.get(i).setPriv_ext_cnt(tmp);
				
				tmp = targetList.get(i).getNow_ext_cnt() / unitNum;
				targetList.get(i).setNow_ext_cnt(tmp);
			}
			if(flag.equals("priv")) {
				tmp = targetList.get(i).getPrevPrivacyCnt() / unitNum;
				targetList.get(i).setPrevPrivacyCnt(tmp);
				tmp = targetList.get(i).getNowPrivacyCnt() / unitNum;
				targetList.get(i).setNowPrivacyCnt(tmp);
			}
			if(flag.equals("comp")) {
				tmp = targetList.get(i).getPrevLogCnt() / unitNum;
				targetList.get(i).setPrevLogCnt(tmp);
				tmp = targetList.get(i).getPrevPrivacyCnt() / unitNum;
				targetList.get(i).setPrevPrivacyCnt(tmp);
				tmp = targetList.get(i).getNowLogCnt() / unitNum;
				targetList.get(i).setNowLogCnt(tmp);
				tmp = targetList.get(i).getNowPrivacyCnt() / unitNum;
				targetList.get(i).setNowPrivacyCnt(tmp);
			}
			
			targetList.get(i).setDataUnit(resultUnit);
			targetList.get(i).setDataUnit_num(unitNum);
		}
		return targetList;
	}

	
	private static List<PrivacyReport> convertDataUnit_period02(List<PrivacyReport> targetList, int unitNum2, String resultUnit2, String flag) {
		int tmp = 0;
		for(int i=0;i<targetList.size();i++) {
			
			if(flag.equals("wordGrid")) {
				tmp = targetList.get(i).getType1() / unitNum;
				targetList.get(i).setType1(tmp);
				tmp = targetList.get(i).getType2() / unitNum;
				targetList.get(i).setType2(tmp);
				tmp = targetList.get(i).getType3() / unitNum;
				targetList.get(i).setType3(tmp);
				tmp = targetList.get(i).getType4() / unitNum;
				targetList.get(i).setType4(tmp);
				
				tmp = targetList.get(i).getType1_1() / unitNum;
				targetList.get(i).setType1_1(tmp);
				tmp = targetList.get(i).getType1_2() / unitNum;
				targetList.get(i).setType1_2(tmp);
				tmp = targetList.get(i).getType1_3() / unitNum;
				targetList.get(i).setType1_3(tmp);
				tmp = targetList.get(i).getType1_4() / unitNum;
				targetList.get(i).setType1_4(tmp);
				
				tmp = targetList.get(i).getLogcnt() / unitNum;
				targetList.get(i).setLogcnt(tmp);
				tmp = targetList.get(i).getPrivacyCnt() / unitNum;
				targetList.get(i).setPrivacyCnt(tmp);
			}
			
			if(flag.equals("grid")) {
				tmp = targetList.get(i).getType1() / unitNum;
				targetList.get(i).setType1(tmp);
				tmp = targetList.get(i).getType2() / unitNum;
				targetList.get(i).setType2(tmp);
				tmp = targetList.get(i).getType3() / unitNum;
				targetList.get(i).setType3(tmp);
				tmp = targetList.get(i).getType4() / unitNum;
				targetList.get(i).setType4(tmp);
				
				tmp = targetList.get(i).getPrivacyCnt() / unitNum;
				targetList.get(i).setPrivacyCnt(tmp);
			}
			
			if(flag.equals("process")) {
				tmp = targetList.get(i).getType1() / unitNum;
				targetList.get(i).setType1(tmp);
				tmp = targetList.get(i).getType2() / unitNum;
				targetList.get(i).setType2(tmp);
				tmp = targetList.get(i).getType3() / unitNum;
				targetList.get(i).setType3(tmp);
				tmp = targetList.get(i).getType4() / unitNum;
				targetList.get(i).setType4(tmp);
				
				tmp = targetList.get(i).getLogcnt() / unitNum;
				targetList.get(i).setLogcnt(tmp);
			}
			
			targetList.get(i).setDataUnit(resultUnit);
			targetList.get(i).setDataUnit_num(unitNum);
		}
			
		return targetList;
	}

	//============================================================================================================
	
	public static int max_period_type1(List<PrivacyReport> paramList, String flag) {
		int max_num = paramList.get(0).getType1();
		int[] tmpVal = new int[4]; //wordGrid & ext
		
		if(flag.equals("wordGrid")) {
			for(int i=0;i<paramList.size();i++) {
				tmpVal[0] += paramList.get(i).getType1();
				tmpVal[1] += paramList.get(i).getType1_1();
				tmpVal[2] += paramList.get(i).getLogcnt();
				tmpVal[3] += paramList.get(i).getPrivacyCnt();
			}
		}
		
		if(flag.equals("grid")) {
			for(int i=0;i<paramList.size();i++) {
				tmpVal[0] += paramList.get(i).getType1();
				tmpVal[1] += paramList.get(i).getPrivacyCnt();
			}
		}
		
		if(flag.equals("ext")) {
			for(int i=0;i<paramList.size();i++) {
				tmpVal[0] += Integer.parseInt(paramList.get(i).getFile_ext_cnt());
			}
		}
		
		if(flag.equals("process")) {
			for(int i=0;i<paramList.size();i++) {
				tmpVal[0] += paramList.get(i).getType1();
				tmpVal[1] += paramList.get(i).getLogcnt();
			}
		}
		
		for(int j=0;j<tmpVal.length;j++) {
			if(max_num < tmpVal[j])
				max_num = tmpVal[j];
		}
		
		return max_num;
	}
	
	private static int max_period_type2(List<PrivacyReport> paramList, String flag) {
		int max_num = 0;
		int[] tmpVal = new int[10]; //wordGrid
		
		if(flag.equals("wordGrid")) {
			for(int i=0;i<paramList.size();i++) {
				tmpVal[0] += paramList.get(i).getType1();
				tmpVal[1] += paramList.get(i).getType2();
				tmpVal[2] += paramList.get(i).getType3();
				tmpVal[3] += paramList.get(i).getType4();
				tmpVal[4] += paramList.get(i).getType1_1();
				tmpVal[5] += paramList.get(i).getType1_2();
				tmpVal[6] += paramList.get(i).getType1_3();
				tmpVal[7] += paramList.get(i).getType1_4();
				tmpVal[8] += paramList.get(i).getLogcnt();
				tmpVal[9] += paramList.get(i).getPrivacyCnt();
			}
		}
		
		if(flag.equals("grid")) {
			for(int i=0;i<paramList.size();i++) {
				tmpVal[0] += paramList.get(i).getType1();
				tmpVal[1] += paramList.get(i).getType2();
				tmpVal[2] += paramList.get(i).getType3();
				tmpVal[3] += paramList.get(i).getType4();
				tmpVal[5] += paramList.get(i).getPrivacyCnt();
			}
		}
		
		if(flag.equals("process")) {
			for(int i=0;i<paramList.size();i++) {
				tmpVal[0] += paramList.get(i).getType1();
				tmpVal[1] += paramList.get(i).getType2();
				tmpVal[2] += paramList.get(i).getType3();
				tmpVal[3] += paramList.get(i).getType4();
				tmpVal[5] += paramList.get(i).getLogcnt();
			}
		}
		
		for(int j=0;j<tmpVal.length;j++) {
			if(max_num < tmpVal[j])
				max_num = tmpVal[j];
		}
		return max_num;
	}

	
	private static int max_period_type4(List<PrivacyReport> paramList, String flag) {
		int max_num = 0;
		int[] tmpVal = new int[3];
		for(int i=0;i<paramList.size();i++) {
			tmpVal[0] += paramList.get(i).getType1();
			tmpVal[1] += paramList.get(i).getType2();
			tmpVal[2] += paramList.get(i).getPrivacyCnt();
		}
		for(int j=0;j<tmpVal.length;j++) {
			if(max_num < tmpVal[j])
				max_num = tmpVal[j];
		}
		return max_num;
	}
	
	//compare grid
	private static int max_number_compare(List<PrivacyReport> paramList, String flag) {
		int max_num = 0;
		int[] tmpVal = new int[4]; //comp
		
		if(flag.equals("comp")) {
			for(int i=0;i<paramList.size();i++) {
				tmpVal[0] += paramList.get(i).getPrevLogCnt();
				tmpVal[1] += paramList.get(i).getPrevPrivacyCnt();
				tmpVal[2] += paramList.get(i).getNowLogCnt();
				tmpVal[3] += paramList.get(i).getNowPrivacyCnt();
			}
		}
		
		if(flag.equals("ext")) {
			for(int i=0;i<paramList.size();i++) {
				tmpVal[0] += paramList.get(i).getPriv_ext_cnt();
				tmpVal[1] += paramList.get(i).getNow_ext_cnt();
			}
		}
		
		if(flag.equals("priv")) {
			for(int i=0;i<paramList.size();i++) {
				tmpVal[0] += paramList.get(i).getPrevPrivacyCnt();
				tmpVal[1] += paramList.get(i).getNowPrivacyCnt();
			}
		}
		
		for(int j=0;j<tmpVal.length;j++) {
			if(max_num < tmpVal[j])
				max_num = tmpVal[j];
		}
		return max_num;
	}


	//================================================================================================
	//한글 단위 구하기(천만)
	private static String getKoreanUnit(int unitNum) {
		
		String result = "";
		double tmpNum = 0;
		
		for(int i=0;i<unitWord.length;i++) {
			tmpNum = (unitNum % Math.pow(10, i+1)) / Math.pow(10, i);
			tmpNum = Math.floor(tmpNum);
			if(tmpNum > 0.0)
				resultArr[i] = (int) tmpNum;
			
			if(resultArr[i] != 0)
				result = unitWord[i];
		}
		return result;
	}


	//문자열 단위 ("10000")
	private static String getStringUnit(int max_number) {
		int length = (int)(Math.log10(max_number) + 1) - 3;
		
		String str = "1";
		for(int i=0;i<length;i++) {
			str += "0";
		}
		return str;
	}
}
