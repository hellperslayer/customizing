package com.easycerti.eframe.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.github.jhonnymertz.wkhtmltopdf.wrapper.Pdf;
import com.github.jhonnymertz.wkhtmltopdf.wrapper.configurations.WrapperConfig;
import com.github.jhonnymertz.wkhtmltopdf.wrapper.params.Param;

@Component
public class PdfUtil {
	
	private static HttpServletRequest request;
	
	private Pdf pdf;
	private String filePath;
	private String fileName;
	private String urlPath;
	private String delayTime;
	private String savefilePath;
	
	private String wkPath;
	private String OS;
	
	private List<String> paramList = new ArrayList<String>();
	private File Folder;
	
	@Value("#{configProperties.report_path}")
	private static String report_path;
	
	public PdfUtil() {
		OS = System.getProperty("os.name").toLowerCase();
		if(OS.contains("win")) {
			filePath = report_path; //폴더 경로
		}else if(OS.contains("nux")) {
			filePath = report_path;
		}
	}
	
	/**
	 * 프로젝트 내에 pdf문서를 생성하기 위한 외부 프로그램이 포함되어 있습니다
	 * 해당 프로그램의 경로를 찾기 위해 request정보를 받아 루트패스를 찾아 셋팅합니다
	 * 만약 request정보를 셋팅하지 않고 사용하는 경우 해당 서버에 wkhtmltopdf 프로그램에 대한 설치와 환경변수 셋팅이 되어있어야 합니다
	 * @param request
	 */
	public PdfUtil(HttpServletRequest request) {
		this();
		setRequest(request);
	}
	
	/**
	 * PDF파일로 만들어줄 HTML문서의 URL, 문서제목, 저장 폴더이름 순서대로 입력합니다.
	 * 윈도우 기준  "C:\PSM_REPORT\저장 폴더이름\문서제목.pdf"
	 * 리눅스 기준  "'설정된 경로'/저장 폴더이름/문서제목.pdf"
	 * 형태로 pdf파일이 생성됩니다.
     */
	public void setPdfConfig(String url, String title, String path) {
		urlPath = url;
		fileName = title;
		savefilePath = filePath+path+File.separator;
		
		Folder = new File(savefilePath);
		savefilePath = savefilePath+fileName+".pdf";
	}
	
	public void setDelayTime(String time) {
		delayTime = time;
	}
	
	/**
	 * PDF문서를 기동된 서버의 특정 위치에 생성합니다
	 * 생성할 PDF문서에 대한 정보를 충분히 셋팅한 후 이 메소드를 호출해 주세요 (setPdfConfig 셋팅메소드)
	 * @return
	 * 생성된 pdf문서의 경로를 반환합니다
	 */
	public String makePDF() {
		pdf = new Pdf();
		pdf.addPageFromUrl(urlPath);
		if(delayTime == null) delayTime = "5000";
		pdf.addParam(new Param("--print-media-type"),new Param("--enable-javascript"), new Param("--no-stop-slow-scripts"), new Param("--javascript-delay"), new Param(delayTime));
		
		if (!Folder.exists()) {
			//System.out.println("폴더없음");
			try{
			    Folder.mkdirs(); //폴더 생성합니다.
			   // System.out.println("폴더생성");
		        } 
		        catch(Exception e){
			    e.getStackTrace();
			    System.out.println(" >> makePDF : ");
			    e.printStackTrace();
			}
		}
		try {
			pdf.saveAs(savefilePath);
			System.out.println("pdf make finish");
		} catch (IOException | InterruptedException e) {
			System.out.println(" >> makePDF : ");
			e.printStackTrace();
		}
		urlPath = "";
		return getFilePath();
	}
	
	/**
	 * 프로젝트 내에 pdf문서를 생성하기 위한 외부 프로그램이 포함되어 있습니다
	 * 해당 프로그램의 경로를 찾기 위해 request정보를 받아 루트패스를 찾아 셋팅합니다
	 * 만약 request정보를 셋팅하지 않고 사용하는 경우 해당 서버에 wkhtmltopdf 프로그램에 대한 설치와 환경변수 셋팅이 되어있어야 합니다
	 * @param request
	 */
	public void setRequest(HttpServletRequest request) {
		this.request = request;
		String path = request.getSession().getServletContext().getRealPath("/")+"WEB-INF/htmltopdf";
		File font = new File("/usr/share/fonts/nanumfont/");
		File font2 = new File("c:\\fonts\\nanumfont\\");
		File originfont = new File(path+"/PDFfromLinux/nanumfont/");
		File htmltopdfPath = new File(path);
		
		/*
		 * if (htmltopdfPath.exists()) { if (OS.contains("win")) { wkPath = path +
		 * "/PDFfromWindow/bin/wkhtmltopdf.exe"; } else if (OS.contains("nux")) { try {
		 * Runtime.getRuntime().exec("chmod 777 " + path +
		 * "/PDFfromLinux/bin/wkhtmltopdf"); } catch (IOException e) {
		 * e.printStackTrace(); } wkPath = path + "/PDFfromLinux/bin/wkhtmltopdf"; } try
		 * { pdf = new Pdf(new WrapperConfig(wkPath)); } catch (Exception e) { pdf = new
		 * Pdf(); } } else { pdf = new Pdf(); }
		 */
		
        String osname = System.getProperty("os.name").toLowerCase();

        String cmd = osname.contains("windows") ? "where.exe wkhtmltopdf" : "cd /; which wkhtmltopdf";
        String text = "/usr/local/bin/wkhtmltopdf";
        Process p;
		try {
			p = Runtime.getRuntime().exec(cmd);
	        p.waitFor();
	        text = IOUtils.toString(p.getInputStream(), Charset.defaultCharset()).trim();
	        if (text.isEmpty())
	        	text = "/usr/local/bin/wkhtmltopdf";
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		
		pdf = new Pdf(new WrapperConfig(text));
		if(OS.contains("nux")&&!font.exists()) {
			System.out.println("폰트 없음 나눔폰트 생성");
			copy(originfont,font);
		}
	}
	
	/** 
	 * @return
	 * 생성된 pdf문서의 경로를 반환합니다
	 */
	public String getFilePath() {
		return savefilePath;
	}
	
	public static void copy(File sourceF, File targetF){
		File[] target_file = sourceF.listFiles();
		targetF.mkdirs();
		for (File file : target_file) {
			File temp = new File(targetF.getAbsolutePath() + File.separator + file.getName());
			if (file.isDirectory()) {
				temp.mkdir();
			} else {
				FileInputStream fis = null;
				FileOutputStream fos = null;
				try {
					fis = new FileInputStream(file);
					fos = new FileOutputStream(temp);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				byte[] b = new byte[4096];
				int cnt = 0;
				try {
					while ((cnt = fis.read(b)) != -1) {
						fos.write(b, 0, cnt);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	

	public static String getPrimaryPath() {
		String sOS = System.getProperty("os.name").toLowerCase();
		String sfilePath = "";
		if(sOS.contains("win")) {
			sfilePath = report_path; //폴더 경로
		}else if(sOS.contains("nux")) {
			sfilePath = report_path;
		}
		
		return sfilePath;
	}
	
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
}
