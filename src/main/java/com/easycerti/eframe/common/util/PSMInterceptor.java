package com.easycerti.eframe.common.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.core.util.SessionManager;
import com.easycerti.eframe.psm.system_management.dao.AdminUserMngtDao;
import com.easycerti.eframe.psm.system_management.dao.AuthMngtDao;
import com.easycerti.eframe.psm.system_management.dao.MenuAuthMngtDao;
import com.easycerti.eframe.psm.system_management.dao.MenuMngtDao;
import com.easycerti.eframe.psm.system_management.dao.OptionSettingDao;
import com.easycerti.eframe.psm.system_management.dao.SystemMngtDao;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;
import com.easycerti.eframe.psm.system_management.vo.Auth;
import com.easycerti.eframe.psm.system_management.vo.Menu;
import com.easycerti.eframe.psm.system_management.vo.MenuAuth;
import com.easycerti.eframe.psm.system_management.vo.OptionSetting;
import com.easycerti.eframe.psm.system_management.vo.System;
import com.google.gson.Gson;

/**
 * 인가절차 유효성 체크를 위한 interceptor
 * - ObjectCont에서 인가 프로세스를 앞단에서 처리.
 * 
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 *  
 *
 * </pre>
 */
public class PSMInterceptor extends HandlerInterceptorAdapter {
	
	/**
	 * 인가절차가 필요없는 path</br>
	 * - properties/config.properties 참조
	 */
	@Autowired
	private CommonDao commonDao;
	@Autowired
	private  AdminUserMngtDao adminUserMngtDao;
	@Autowired
	private OptionSettingDao optionSettingDao; 
	@Autowired
	private MenuAuthMngtDao menuAuthMngtDao;
	private String[] noneAuthorTargetUrls;
	private List<String> noneAuthorTargetUrlsList = new ArrayList<>();
	
	@Value("#{configProperties.iframe_url}")
	private String iframe_url;
	
	public void setNoneAuthorTargetArr(String noneAuthorUrls){
		if(noneAuthorUrls != null){
			this.noneAuthorTargetUrls = noneAuthorUrls.split(CommonResource.COMMA_STRING);
			for ( String s : noneAuthorTargetUrls ) {
				noneAuthorTargetUrlsList.add(s);
			}
			// config.properties 파일을 수정하지않기위해
			noneAuthorTargetUrlsList.add("/rePassword.html");
			noneAuthorTargetUrlsList.add("/ssologin.html");
		}
	}
	
	/**
	 * 비인가 요청의 경우 redirect Path 설정을 위함.</br>
	 * - 설정하지 않을 경우 contextpath로 보냄.
	 */
	private String redirectPath = null;
	public void setRedirectPath(String redirectPath){
		this.redirectPath = redirectPath;
	}
	
	private String[] allowedIPs;
 	public void setAllowedIPs(String[] allowedIPs) {
		this.allowedIPs = allowedIPs;
	}
 	 
	private boolean isInitAllowedIPs = false;
	protected void initAllowedIPs() throws ServletException {
		SimpleCode simpleCode = new SimpleCode();
		allowedIPs = commonDao.getIptables(simpleCode);
		if(allowedIPs.length == 0){
			isInitAllowedIPs = true;
			return;
		}
		if ((allowedIPs != null) && (allowedIPs[0] != null))
		{
			//allowedIPs = allowedIPs[0].split(";");
			isInitAllowedIPs = true;
		}else if ((allowedIPs == null) && (allowedIPs[0] == null)){
			isInitAllowedIPs = true;
		}
	}
	private boolean isSideBar = true;
	/**
	 * dispatch servlet 처리 직후 request mapping을 타기 전 처리.
	 */
	
	private boolean checkAuth(String admin_user_id, String path) {
		
		boolean check = false;
		
		MenuAuth menuAuthBean = new MenuAuth();
		menuAuthBean.setAdmin_user_id(admin_user_id);
		
		int auth_result = menuAuthMngtDao.findMenuAuthMngtOne_toUserAuth(menuAuthBean);
		
		if(auth_result > 0) {
			List<String> curMenuId = menuAuthMngtDao.getMenuIdByUrl(path);
			if (curMenuId.size() > 0) {
				for (String menuId : curMenuId) {
					menuAuthBean.setMenu_id(menuId);
					int result = menuAuthMngtDao.findMenuAuthMngtOne_toUserMenuAuth(menuAuthBean);
					if (result > 0) {
						check = true;
						break;
					}
				}
			} else {
				check = true;
			}
		}
		
		return check;
	}
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		
		String ui_type = commonDao.getUiType();
		// https 에서 뒤로가기 때문에 주석처리함
		/*response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Pragma","no-cache");*/
		response.setHeader("X-Content-Type-Options","nosniff");
		//
		if(!"".equals(iframe_url))
			response.setHeader("X-Frame-Options", iframe_url);
		else
			response.setHeader("X-Frame-Options","SAMEORIGIN");
		response.setHeader("X-XSS-Protection","1;mode=block");
		response.setDateHeader("Expires",0);

		isInitAllowedIPs = false;
		
		String path = request.getServletPath();
		
		/**
		 * ALLOWED IP CHECK
		 */
		if( !isInitAllowedIPs ){
			//this.initAllowedIPs();
		}
		
		if(path.equals("/extrtCondbyInq/deptDetailChart.html") || path.equals("/extrtCondbyInq/detailChart.html")) 
			isSideBar = false;
		else 
			isSideBar = true;
		
		if( !"".equals(path) && "/notAllowedIP.html".equals(path) ){
			;
		}
		
		else 
		{
			boolean isAccessable = false;

			String clientIP = request.getRemoteAddr();

			if (allowedIPs != null) {
				/*for (String ipRegEx : allowedIPs) {
					if (clientIP.matches(ipRegEx)) {
						isAccessable = true;
						break;
					}
				}*/
				
				// 2017.6.30 jhkim 
				// 2개의 IP만 허용
				for(int i=0; i<allowedIPs.length; i++) {
					if(i >= 2) 
						break;
					
					if (clientIP.matches(allowedIPs[i])) {
						isAccessable = true;
						break;
					}
				}
			}
			
			if(allowedIPs.length == 0){
				isAccessable = true;
			}

			if (!isAccessable) {
				throw new ESException("NotAllowedIP");
			}
		}
		
		boolean authorTargetFlag = true;
		String fullUrl = request.getRequestURL().toString();
		if(!fullUrl.contains("report")) {
			if(noneAuthorTargetUrlsList != null){
				for(String target : noneAuthorTargetUrlsList){
					if(path != null && !CommonResource.EMPTY_STRING.equals(path) && path.equals(target)){ // 인가 체크 대상이 아님.
						authorTargetFlag = false;
						break;
					}else{ // 인가 유효성 체크 대상.
						authorTargetFlag = true;
					}
				}
			}
		} else {
			authorTargetFlag = false;
		}
		
		if (authorTargetFlag) {
			
			AdminUser adminUserBean;
			adminUserBean = SystemHelper.findSessionUser(request);
			if (adminUserBean == null) { // 비인가 접근
				
				// 중복로그인 페이지 이동 추가
				SessionManager userSession = SessionManager.getInstance();
				Hashtable<String,String> userLoginTime = userSession.getLoginUserTime(request);
				
				// userSession의 loginUserTime 맵에 내 아이피가 들어있을때
				if(userLoginTime != null){
					
					// userSession의 loginUserTime 맵에 내 아이피로 들고온 아이디와 시간확인
					String iddate = userLoginTime.get(request.getRemoteAddr());
					String id = iddate.split("/")[0];
					String date = iddate.split("/")[1];
					
					// 내가접속했던 아이디로 현재 아이디로 접속한 사람이 있나 확인
					List<String> userSessionId = userSession.printLoginUserIds();
					for(int i=0; i<userSessionId.size(); i++){
						if(id.equals(userSessionId.get(i))){
							String nowUserIp = userSession.printLoginUserIp(id);
							String nowUserLoginTime = userLoginTime.get(nowUserIp).split("/")[1];
							SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							Date myDate = parser.parse(date);
							Date nowUserDate = parser.parse(nowUserLoginTime);
							
							// 현재아이디의 접속시간이 내가 접속한 이후 인가 확인
							if(myDate.getTime() <= nowUserDate.getTime()){
								//내 아이피의 해쉬맵 삭제
								userSession.deleteLoginTime(request);
								//중복로그인 방지 페이지로 이동
								throw new ESException("DuplicationLoginPage");
							}
						}
					}
				}
				
				throw new ESException("SYS006V");
					
			} else {
				String admin_id = adminUserBean.getAdmin_user_id();
				String[] ips = commonDao.getIptablesByAdminId(admin_id);
				String clientIP = request.getRemoteAddr();
				boolean isAccess = false;
				
				if(ips == null || ips.length == 0)	//접속허용ip 테이블에 없으면 일단은 통과
					isAccess = true;
				else {
					for(String ip : ips) {
						/*if(clientIP.matches(ip))
							isAccess = true;
							break;*/
						
						String[] spIps = ip.split("\\.");
						String tempIp;
						if(spIps[3].equals("0")) {
							tempIp = spIps[0] + "." + spIps[1] + "." + spIps[2];
							if(clientIP.indexOf(tempIp) > -1)
								isAccess = true;
						} else {
							if (clientIP.matches(ip)) {
								isAccess = true;
								break;
							}
						}
					}
				}
				
				if(!isAccess)
					throw new ESException("NotAllowedIP");
				
				if (!checkAuth(admin_id, path)) 
					throw new ESException("NotAuthPage");
			}
		}
		
		/*시군구 : 0 , 아카데미 : 1 로 수정하세요*/
		//테마선택
		
		try{
			Integer chng_layout = Integer.parseInt(commonDao.getTheme());
			//대시보드선택
			Integer dashboard = Integer.parseInt(commonDao.getDashboard());
			
			request.getSession().setAttribute("chng_layout", chng_layout);
			request.getSession().setAttribute("dashboard", dashboard);
			
			/*String master = commonDao.getMaster();
			//개인정보유형 뷰
			String result_type_view = commonDao.getResultTypeView();
			//메뉴명
			String scrn_name_view = commonDao.getScrnNameView();
			//사이드바 고정유무
			String side_bar_view = commonDao.getSidebarView();
			//접속유형 설정
			String mapping_id = commonDao.getMappingId();
			//세션타임아웃 적용
			Integer sessionTime = Integer.parseInt(commonDao.getSessionTime());
			request.getSession().setMaxInactiveInterval(sessionTime * 60);
			
			request.getSession().setAttribute("masterflag", master);
			request.getSession().setAttribute("result_type_view", result_type_view);
			request.getSession().setAttribute("scrn_name_view", scrn_name_view);
			request.getSession().setAttribute("side_bar_view", side_bar_view);
			request.getSession().setAttribute("mapping_id", mapping_id);*/
			
			// 2017-11-10 옵션설정 수정
			List<OptionSetting> list = optionSettingDao.findOptionSettingList();
			for(int i=0; i<list.size(); i++) {
				OptionSetting optionSetting = list.get(i);
				if(!optionSetting.getOption_id().equals("session_time"))
					request.getSession().setAttribute(optionSetting.getOption_id(), optionSetting.getValue());
				else {
					Integer sessionTime = Integer.parseInt(optionSetting.getValue());
					request.getSession().setMaxInactiveInterval(sessionTime * 60);
				}
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return super.preHandle(request, response, handler);
	}

	@Autowired
	private AuthMngtDao authMngtDao;
	
	@Autowired
	private MenuMngtDao menuMngtDao;
	
	@Autowired
	private SystemMngtDao systemMngtDao;
	
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		
		// 화면일 경우
		if (modelAndView != null
				&& !CommonResource.JSON_VIEW.equals(modelAndView.getViewName())) {
			String mainMenuStr = null;
			String subMenuStr = null;
			String sysMenuStr = null;
			String path = request.getServletPath();
			if (SystemHelper.findSessionUser(request) != null
					&& (request.getSession().getAttribute("mainMenus") == null || request
							.getSession().getAttribute("leftMenus") == null)) {

				AdminUser adminUserBean = null;
				adminUserBean = SystemHelper.findSessionUser(request);
				
				if (adminUserBean != null) {
					Menu menuBean = new Menu();
					System systemBean = new System();
					menuBean.setMenu_depth(1);
					AdminUser adminUserParamBean = new AdminUser();
					adminUserParamBean.setAdmin_user_id(SystemHelper
							.getAdminUserIdFromRequest(request));
					Auth authBean = authMngtDao
							.findAuthMngtOne_fromUser(adminUserBean);

					menuBean.setAuth_id(authBean.getAuth_id());
					List<Menu> mainMenus = menuMngtDao
							.findMenuMngtList_toDepth(menuBean);
					
					String auth_ids = adminUserMngtDao.getUserAuthIds(adminUserBean.getAdmin_user_id());
					String [] auth_tmp = auth_ids.split(",");
					
					List<String> list = new ArrayList<String>();
					
					for ( int i=0; i<auth_tmp.length; ++i ) {
						list.add(auth_tmp[i]);
					}
				
					systemBean.setAuth_idsList(list);
					
					List<System> sysMenus;
					String[] arrPath = path.split("/");
					List<String> tmpList = new ArrayList<>();
					List<String> tmpList2 = new ArrayList<>();
					switch (arrPath[1]) {
						case "dbAccessInq" : 
							sysMenus = systemMngtDao.findSystemMngtListAuth_Dbac(systemBean);
							break;
						case "onrLogInq" :
							sysMenus = systemMngtDao.findSystemMngtListAuth_Onnara(systemBean);
							break;
						default:
							sysMenus = systemMngtDao.findSystemMngtListAuth(systemBean);
							break;
					}
						
					modelAndView.addObject("mainMenus", mainMenus);

					Map<String, List<Menu>> subMenuMap = new HashMap<String, List<Menu>>();
					
					for (Menu mainMenu : mainMenus) {
						mainMenu.setAuth_id(authBean.getAuth_id());

						List<Menu> leftMenus = menuMngtDao
								.findMenuMngtList_forHierarchy(mainMenu);
						subMenuMap.put(mainMenu.getMenu_id(), leftMenus);
					}
					
			
					// 메뉴정보를 json으로 parse
					Gson gson = new Gson();
					mainMenuStr = gson.toJson(mainMenus);
					subMenuStr = gson.toJson(subMenuMap);
					sysMenuStr = gson.toJson(sysMenus);
					request.getSession().setAttribute("mainMenu", mainMenuStr);
					request.getSession().setAttribute("subMenu", subMenuStr);
					request.getSession().setAttribute("subSys", sysMenus);
					
					request.getSession().setAttribute("auth_ids_list", list);
					tmpList = systemMngtDao.findSystemCodesByOnnara(list);
					if(tmpList.size() > 0) {
						request.getSession().setAttribute("auth_ids_list_onnara", tmpList);
					}
					tmpList2 = systemMngtDao.findSystemCodesByNormal(list);
					if(tmpList2.size() > 0) {
						request.getSession().setAttribute("auth_ids_list_normal", tmpList2);
					}
					
					
					//  2016.12.27 by hjpark
					//request.getSession().setAttribute("auth_ids_list", systemBean.getAuth_idsList());
				}
			} else {
				mainMenuStr = (String) request.getSession().getAttribute("mainMenu");
				subMenuStr = (String) request.getSession().getAttribute("subMenu");
			}
			
			Calendar calendar = Calendar.getInstance();
			String limitTime = adminUserMngtDao.checktime();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
			String nowTime = dateFormat.format(calendar.getTime());
			
			/*시군구 : 0 , 아카데미 : 1*/
			Integer chng_layout = (Integer) request.getSession().getAttribute("chng_layout");
			Integer dashboard = (Integer) request.getSession().getAttribute("dashboard");
			String side_bar_view = (String) request.getSession().getAttribute("side_bar");
			
			modelAndView.addObject("nowTime", nowTime);
			modelAndView.addObject("limitTime", limitTime);
			modelAndView.addObject("chng_layout", chng_layout);
			modelAndView.addObject("dashboard", dashboard);
			modelAndView.addObject("side_bar_view", side_bar_view);
			modelAndView.addObject("subMenu", subMenuStr);
			modelAndView.addObject("subSys", sysMenuStr);
			modelAndView.addObject("mainMenu", mainMenuStr);
			modelAndView.addObject("isSideBar", isSideBar);
			modelAndView.addObject("path", path);
		}
		
		super.postHandle(request, response, handler, modelAndView);
	}
}
