package com.easycerti.eframe.common.util;

import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * 엑셀 다운로드 Util
 * 
 * @author yjyoo
 * @since 2015. 5. 22.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 22.           yjyoo            최초 생성
 *
 * </pre>
 */
public class ExcelUtil extends AbstractExcelView {
	
	private String fileExtension = ".xlsx";
	
	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}

	@Override
	protected void buildExcelDocument(Map<String, Object> model, XSSFWorkbook workbook, 
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		String fileName = (String) model.get("fileName");
		fileName = fileName + this.fileExtension;
		
		// 브라우저 종류 확인, fileName 한글 깨짐 방지
		String userAgent = request.getHeader("User-Agent").toUpperCase();
		if(userAgent.indexOf("MSIE") > -1 || userAgent.indexOf("TRIDENT") > -1 || userAgent.indexOf("MOZILLA") > -1) {
			fileName = URLEncoder.encode(fileName, "utf-8");
		}
		else {
			fileName = new String(fileName.getBytes("utf-8"), "iso-8859-1");
		}
		response.setContentType("Application/Msexcel");
		response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");
		response.setHeader("Content-Transfer-Encoding", "binary");
		
		List<Map<String, Object>> modelList = (List)model.get("modelList");
		if(modelList!=null) { // sheet 여러개
			for(int i=0; i<modelList.size();i++) {
				Map<String, Object> temp = modelList.get(i);
				String sheetName = (String) temp.get("sheetName");
				List<Object> excelData = (List<Object>) temp.get("excelData");
				String[] heads = (String[]) temp.get("heads");
				String[] columns = (String[]) temp.get("columns");
				Integer sheetNum = (Integer) temp.get("sheetNum");
				createSheetAndRow(excelData, heads, columns, workbook, sheetName, sheetNum);
			}
		} else { // sheet 하나
			String sheetName = (String) model.get("sheetName");
			List<Object> excelData = (List<Object>) model.get("excelData");
			String[] heads = (String[]) model.get("heads");
			String[] columns = (String[]) model.get("columns");
			createSheetAndRow(excelData, heads, columns, workbook, sheetName, 0);
		}
		
	}
	
	/**
	 * 스타일 지정 - sheet 제목
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 22.
	 * @return XSSFCellStyle
	 */
	public static XSSFCellStyle getSubjectStyle(XSSFWorkbook workbook) {
		XSSFFont font = workbook.createFont();
		font.setFontName("돋움");
		font.setFontHeightInPoints((short) 11);
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		
		XSSFCellStyle style = workbook.createCellStyle();
		style.setFont(font);
		style.setFillPattern((short) 1);
		style.setFillForegroundColor(IndexedColors.LEMON_CHIFFON.getIndex());
		style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		
		return style;
	}
	
	/**
	 * 스타일 지정 - 첫 Row (항목명)
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 22.
	 * @return XSSFCellStyle
	 */
	public static XSSFCellStyle getColumnLabelStyle(XSSFWorkbook workbook) {
		XSSFFont font = workbook.createFont();
		font.setFontName("돋움");
		font.setFontHeightInPoints((short) 10);
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);

		XSSFCellStyle style = workbook.createCellStyle();
		style.setFont(font);
		style.setFillPattern((short) 1);
		style.setFillForegroundColor((short) 22);
		style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		style.setBorderTop(XSSFCellStyle.BORDER_DOUBLE);
		style.setBorderBottom(XSSFCellStyle.BORDER_DOUBLE);
		style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		style.setBorderRight(XSSFCellStyle.BORDER_THIN);
		
		return style;
	}
	
	/**
	 * 스타일 지정 - 데이터 Row
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 22.
	 * @return XSSFCellStyle
	 */
	public static XSSFCellStyle getColumnStyle(XSSFWorkbook workbook) {
		XSSFFont font = workbook.createFont();
		font.setFontName("돋움");
		font.setFontHeightInPoints((short) 10);
		XSSFCellStyle style = workbook.createCellStyle();
		style.setFont(font);
		style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		style.setBorderTop(XSSFCellStyle.BORDER_THIN);
		style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		style.setBorderRight(XSSFCellStyle.BORDER_THIN);
		style.setWrapText(true);
		return style;
	}
	
	public static XSSFCellStyle getIntColumnStyle(XSSFWorkbook workbook) {
		XSSFFont font = workbook.createFont();
		font.setFontName("돋움");
		font.setFontHeightInPoints((short) 10);
		XSSFDataFormat format = workbook.createDataFormat();
		XSSFCellStyle style = workbook.createCellStyle();
		style.setFont(font);
		style.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
		style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		style.setBorderTop(XSSFCellStyle.BORDER_THIN);
		style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		style.setBorderRight(XSSFCellStyle.BORDER_THIN);
		style.setDataFormat(format.getFormat("#,##0"));
		style.setWrapText(true);
		return style;
	}
	
	/**
	 * sheet, 첫 row (항목명) 만들기
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 22.
	 * @return void
	 */
	public static void createSheetAndRow(List<Object> excelData, String[] heads, String[] columns, XSSFWorkbook workbook, String sheetName, Integer sheetNum) {
		XSSFSheet sheet = createSheet(workbook, sheetName, heads, sheetNum);
		
		XSSFCellStyle subjectStyle = getSubjectStyle(workbook);
		XSSFCellStyle columnLabelStyle = getColumnLabelStyle(workbook);
		XSSFCellStyle columnStyle = getColumnStyle(workbook);
		XSSFCellStyle intColumnStyle = getIntColumnStyle(workbook);
		
		createSubject(sheet, sheetName, heads, subjectStyle);
		createColumnLabel(sheet, heads, columnLabelStyle);
		createRow(sheet, excelData, columns, columnStyle, intColumnStyle);
	}
	
	/**
	 * sheet 생성
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 22.
	 * @return XSSFSheet
	 */
	public static XSSFSheet createSheet(XSSFWorkbook workbook, String sheetName, String[] column, Integer sheetNum) {
		XSSFSheet sheet = workbook.createSheet();
		workbook.setSheetName(sheetNum == null ? 0 : sheetNum.intValue(), sheetName);

		for (int i = 0; i < column.length; i++) {
			sheet.setColumnWidth(i, 7680);
		}
		return sheet;
	}
	
	/**
	 * sheet 제목 만들기
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 22.
	 * @return void
	 */
	public static void createSubject(XSSFSheet sheet, String sheetName, String[] heads, XSSFCellStyle style) {
		XSSFRow subjectRow = sheet.createRow(0);
		subjectRow.setHeight((short) 550);

		XSSFCell cell = subjectRow.createCell(0);
		cell.setCellValue(sheetName);

		int subjectCol = heads.length - 1;
		
		sheet.addMergedRegion(new CellRangeAddress(0, (short) 0, 0, (short) subjectCol));
		
		cell.setCellStyle(style);
	}

	/**
	 * 첫 row (항목명) 만들기
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 22.
	 * @return void
	 */
	public static void createColumnLabel(XSSFSheet sheet, String[] heads, XSSFCellStyle style) {
		XSSFRow headRow = sheet.createRow(2);
		headRow.setHeight((short) 450);
		for (int i = 0; i < heads.length; i++) {
			XSSFCell cell = headRow.createCell(i);
			cell.setCellStyle(style);
			cell.setCellValue(heads[i]);
		}
	}

	/**
	 * 데이터 row 만들기
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 22.
	 * @return void
	 */
	public static void createRow(XSSFSheet sheet, List<Object> excelData, String[] columns, XSSFCellStyle style, XSSFCellStyle intColumnStyle) {
	    int rowIdx = 2;

	    for (Iterator localIterator = excelData.iterator(); localIterator.hasNext(); ) { 
	    	Object obj = localIterator.next();
	    	
	    	if( obj instanceof java.util.Map ) {
	    		Map<String, String> data = (Map<String, String>) obj;
	    		
	    		int columnIdx = 0;
	    		rowIdx++; 
	    		XSSFRow row = sheet.createRow(rowIdx);
	    		XSSFCell cell = row.createCell(0);
	    		
	    		for (String column : columns) {
	    			cell = row.createCell(columnIdx++);
	    			String value;
	    			if(data.get(column) instanceof String) {
	    				value = data.get(column);
	    				cell.setCellStyle(style);
	    			} else {
	    				value = String.valueOf(data.get(column));
	    				cell.setCellStyle(intColumnStyle);
	    			}
	    			cell.setCellValue(value);
	    			row.setHeight((short) 350);
	    			if(rowIdx % 2 == 0) {
						style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
					}
					
	    		}
	    	}
	    	else {
	    		int columnIdx = 0;
	    		rowIdx++; 
	    		XSSFRow row = sheet.createRow(rowIdx);
	    		XSSFCell cell = row.createCell(0);
	    		
	    		Class classObj = obj.getClass();
	    		
	    		Field[] dFields = classObj.getDeclaredFields();
	    		for (String column : columns) {
	    			for (Field field : dFields) {
	    				field.setAccessible(true);
	    				try {
	    					if (column.equals(field.getName())) {
	    						cell = row.createCell(columnIdx++);
	    						
	    						Class fieldType = field.getType();
	    						
	    						if (fieldType.equals(Integer.TYPE)) {
	    							cell.setCellValue(((Integer)field.get(obj)).intValue());
	    							cell.setCellStyle(intColumnStyle);
	    						}
	    						if (fieldType.equals(String.class)) {
	    							cell.setCellValue((String)field.get(obj));
	    							cell.setCellStyle(style);
	    						}
	    						if (fieldType.equals(Date.class)) {
	    							cell.setCellValue((Date)field.get(obj));
	    							cell.setCellStyle(style);
	    						}
	    						if (fieldType.equals(Timestamp.class)) {
	    							cell.setCellValue((Timestamp)field.get(obj));
	    							cell.setCellStyle(style);
	    						}
	    						row.setHeight((short) 350);
	    						
	    						if(rowIdx % 2 == 0) {
	    							style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
	    						}
	    						
	    					}
	    				}
	    				catch (Exception e) {
	    					e.printStackTrace();
	    				}
	    			}
	    		}
	    	}
	    	
	    }
	}
}
