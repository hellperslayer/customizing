package com.easycerti.eframe.common.util;

import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

public class EncryptStrGenerator {

	public static void main(String[] args) {
		StandardPBEStringEncryptor pbeEnc1 = new StandardPBEStringEncryptor();
		Security.addProvider(new BouncyCastleProvider());
		pbeEnc1.setProviderName("BC");
		pbeEnc1.setAlgorithm("PBEWITHSHA256AND128BITAES-CBC-BC");
		pbeEnc1.setPassword("dlwltjxl1!");
		
		String str1 = "wjdqhdnjsQ1!";
//		String str2 = "psm123";
		
		System.out.println(pbeEnc1.encrypt(str1));
//		System.out.println(pbeEnc1.decrypt(str2));
	}
	
	public String getDecryptedMessage(String encryptedMessage) {
		StandardPBEStringEncryptor pbeEnc1 = new StandardPBEStringEncryptor();
		Security.addProvider(new BouncyCastleProvider());
		pbeEnc1.setProviderName("BC");
		pbeEnc1.setAlgorithm("PBEWITHSHA256AND128BITAES-CBC-BC");
		pbeEnc1.setPassword("dlwltjxl1!");
		
		String decStr = pbeEnc1.decrypt(encryptedMessage);
		
		
		return decStr;
	}
}
