package com.easycerti.eframe.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class ExcelFileReader implements DataFileReader {
	private File file = null;
	HSSFWorkbook workbook;
	HSSFSheet sheet;
	int rowindex=0;

	public ExcelFileReader(String excelPath) throws Exception {
		file = new File(excelPath);
		if( !file.exists() ) throw new Exception("No Excel File!!");
		
		FileInputStream fis=new FileInputStream(file);
		workbook=new HSSFWorkbook(fis);
		sheet=workbook.getSheetAt(0);
		fis.close();
	}
	
	public List<String[]> readRows() throws Exception {
		List<String[]> dataRows = new ArrayList<String[]>();
		
		int columnindex=0;
		//시트 수 (첫번째에만 존재하므로 0을 준다)
		//만약 각 시트를 읽기위해서는 FOR문을 한번더 돌려준다
		
		//행의 수
		int rows=sheet.getPhysicalNumberOfRows();
		for(;rowindex<rows;rowindex++){
			List<String> rowValues = new ArrayList<String>();
		    //행을 읽는다
		    HSSFRow row=sheet.getRow(rowindex);
		    if(row !=null)
		    {
		        //셀의 수
		        int cells=row.getPhysicalNumberOfCells();
		        for(columnindex=0;columnindex<=cells;columnindex++)
		        {
		            //셀값을 읽는다
		            HSSFCell cell=row.getCell(columnindex);
		            String value="";
		            //셀이 빈값일경우를 위한 널체크
		            if(cell==null)
		            {
		                continue;
		            }
		            else
		            {
		                //타입별로 내용 읽기
		                switch (cell.getCellType())
		                {
			                case HSSFCell.CELL_TYPE_FORMULA:
			                    value=cell.getCellFormula();
			                    break;
			                case HSSFCell.CELL_TYPE_NUMERIC:
			                    value= ((Double)cell.getNumericCellValue()).intValue() +"";
			                    break;
			                case HSSFCell.CELL_TYPE_STRING:
			                    value=cell.getStringCellValue();
			                    break;
			                case HSSFCell.CELL_TYPE_BLANK:
			                    value="";
			                    break;
			                case HSSFCell.CELL_TYPE_ERROR:
			                    value=cell.getErrorCellValue()+"";
			                    break;
		                }
		            }
		            System.out.println(value);
		            rowValues.add(value);
		        }
		        dataRows.add( rowValues.toArray(new String[rowValues.size()]) );
		    }
		}
		return dataRows;
	}
	
	public List<String[]> readRows(int rownum) throws Exception {
		List<String[]> dataRows = new ArrayList<String[]>();
		
		int columnindex=0;
		//행의 수
		int rows=sheet.getPhysicalNumberOfRows();
		int currentRowNum=0;
		for(;rowindex<rows;rowindex++,currentRowNum++){
			if( currentRowNum > rownum ) break;
			List<String> rowValues = new ArrayList<String>();
		    //행을 읽는다
		    HSSFRow row=sheet.getRow(rowindex);
		    if(row !=null)
		    {
		        //셀의 수
		        int cells=row.getPhysicalNumberOfCells();
		        for(columnindex=0;columnindex<=cells;columnindex++)
		        {
		            //셀값을 읽는다
		            HSSFCell cell=row.getCell(columnindex);
		            String value="";
		            //셀이 빈값일경우를 위한 널체크
		            if(cell==null)
		            {
		                continue;
		            }
		            else
		            {
		                //타입별로 내용 읽기
		                switch (cell.getCellType())
		                {
			                case HSSFCell.CELL_TYPE_FORMULA:
			                    value=cell.getCellFormula();
			                    break;
			                case HSSFCell.CELL_TYPE_NUMERIC:
			                    value= ((Double)cell.getNumericCellValue()).intValue() +"";
			                    break;
			                case HSSFCell.CELL_TYPE_STRING:
			                    value=cell.getStringCellValue().replaceAll("\n", "");
			                    break;
			                case HSSFCell.CELL_TYPE_BLANK:
			                    value="";
			                    break;
			                case HSSFCell.CELL_TYPE_ERROR:
			                    value=cell.getErrorCellValue()+"";
			                    break;
		                }
		            }
		            rowValues.add(value);
		        }
		        dataRows.add( rowValues.toArray(new String[rowValues.size()]) );
		    }
		}
		return dataRows;
	}
	
	@Override
	protected void finalize() throws Throwable {
	
		super.finalize();
	}
}
