package com.easycerti.eframe.common.util;

import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.util.Region;
import org.springframework.web.servlet.view.AbstractView;
import org.supercsv.io.CsvMapWriter;
import org.supercsv.io.ICsvMapWriter;
import org.supercsv.prefs.CsvPreference;

/**
 * 엑셀 다운로드 Util
 * 
 * @author yjyoo
 * @since 2015. 5. 22.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 5. 22.           yjyoo            최초 생성
 *
 * </pre>
 */
public class CsvUtil extends AbstractView  {
	
	private String fileExtension = ".csv";
	
	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}

	@Override
	protected void renderMergedOutputModel(Map<String, Object> modelMap, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		String fileName = (String) modelMap.get("fileName");
		fileName = fileName + this.fileExtension;
		
		String userAgent = request.getHeader("User-Agent").toUpperCase();
		if(userAgent.indexOf("MSIE") > -1 || userAgent.indexOf("TRIDENT") > -1 || userAgent.indexOf("MOZILLA") > -1) {
			fileName = URLEncoder.encode(fileName, "utf-8");
		}
		else {
			fileName = new String(fileName.getBytes("utf-8"), "iso-8859-1");
		}
		response.setContentType("text/csv; charset=MS949");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");
 
        excelDataPurification(modelMap, request, response);
    }
	
	protected void excelDataPurification(Map<String, Object> modelMap, HttpServletRequest request, HttpServletResponse response) throws Exception {
        // Map 을 사용한다면 CsvMapWriter 사용
        ICsvMapWriter csvMapWriter = new CsvMapWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);
 
        // VO 를 사용한다면 CsvBeanWriter 사용
        //        ICsvBeanWriter csvBeanWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);
 
        String[] columnIds = (String[]) modelMap.get("columns");
        String[] columnNames = (String[]) modelMap.get("heads");
 
        //String[] colids = columnIds.split(",");
        //String[] colnms = columnNames.split(",");
 
        // 엑셀 데이터
        List<Map<String, Object>> excelDataList = (List<Map<String, Object>>) modelMap.get("excelData");
 
        csvMapWriter.writeHeader(columnNames);
 
        if (excelDataList != null) {
            for (int i = 0; i < excelDataList.size(); i++) {
                Map<String, Object> rowData = (Map<String, Object>) excelDataList.get(i);
                csvMapWriter.write(rowData, columnIds);
            }
        }
        csvMapWriter.close();
    }
	
	/**
	 * 스타일 지정 - sheet 제목
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 22.
	 * @return HSSFCellStyle
	 */
	public static HSSFCellStyle getSubjectStyle(HSSFWorkbook workbook) {
		HSSFFont font = workbook.createFont();
		font.setFontName("돋움");
		font.setFontHeightInPoints((short) 11);
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		
		HSSFCellStyle style = workbook.createCellStyle();
		style.setFont(font);
		style.setFillPattern((short) 1);
		style.setFillForegroundColor(HSSFColor.LEMON_CHIFFON.index);
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		
		return style;
	}
	
	/**
	 * 스타일 지정 - 첫 Row (항목명)
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 22.
	 * @return HSSFCellStyle
	 */
	public static HSSFCellStyle getColumnLabelStyle(HSSFWorkbook workbook) {
		HSSFFont font = workbook.createFont();
		font.setFontName("돋움");
		font.setFontHeightInPoints((short) 10);
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

		HSSFCellStyle style = workbook.createCellStyle();
		style.setFont(font);
		style.setFillPattern((short) 1);
		style.setFillForegroundColor((short) 22);
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		style.setBorderTop(HSSFCellStyle.BORDER_DOUBLE);
		style.setBorderBottom(HSSFCellStyle.BORDER_DOUBLE);
		style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		style.setBorderRight(HSSFCellStyle.BORDER_THIN);
		
		return style;
	}
	
	/**
	 * 스타일 지정 - 데이터 Row
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 22.
	 * @return HSSFCellStyle
	 */
	public static HSSFCellStyle getColumnStyle(HSSFWorkbook workbook) {
		HSSFFont font = workbook.createFont();
		font.setFontName("돋움");
		font.setFontHeightInPoints((short) 10);
		
		HSSFCellStyle style = workbook.createCellStyle();
		style.setFont(font);
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		style.setBorderTop(HSSFCellStyle.BORDER_THIN);
		style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		style.setBorderRight(HSSFCellStyle.BORDER_THIN);
		style.setWrapText(true);
		
		return style;
	}
	
	/**
	 * sheet, 첫 row (항목명) 만들기
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 22.
	 * @return void
	 */
	public static void createSheetAndRow(List<Object> excelData, String[] heads, String[] columns, HSSFWorkbook workbook, String sheetName, Integer sheetNum) {
		HSSFSheet sheet = createSheet(workbook, sheetName, heads, sheetNum);
		
		HSSFCellStyle subjectStyle = getSubjectStyle(workbook);
		HSSFCellStyle columnLabelStyle = getColumnLabelStyle(workbook);
		HSSFCellStyle columnStyle = getColumnStyle(workbook);
		
		createSubject(sheet, sheetName, heads, subjectStyle);
		createColumnLabel(sheet, heads, columnLabelStyle);
		createRow(sheet, excelData, columns, columnStyle);
	}
	
	/**
	 * sheet 생성
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 22.
	 * @return HSSFSheet
	 */
	public static HSSFSheet createSheet(HSSFWorkbook workbook, String sheetName, String[] column, Integer sheetNum) {
		HSSFSheet sheet = workbook.createSheet();
		workbook.setSheetName(sheetNum == null ? 0 : sheetNum.intValue(), sheetName);

		for (int i = 0; i < column.length; i++) {
			sheet.setColumnWidth(i, 7680);
		}
		return sheet;
	}
	
	/**
	 * sheet 제목 만들기
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 22.
	 * @return void
	 */
	public static void createSubject(HSSFSheet sheet, String sheetName, String[] heads, HSSFCellStyle style) {
		HSSFRow subjectRow = sheet.createRow(0);
		subjectRow.setHeight((short) 550);

		HSSFCell cell = subjectRow.createCell(0);
		cell.setCellValue(sheetName);

		int subjectCol = heads.length - 1;
		sheet.addMergedRegion(new Region(0, (short) 0, 0, (short) subjectCol));
		
		cell.setCellStyle(style);
	}

	/**
	 * 첫 row (항목명) 만들기
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 22.
	 * @return void
	 */
	public static void createColumnLabel(HSSFSheet sheet, String[] heads, HSSFCellStyle style) {
		HSSFRow headRow = sheet.createRow(2);
		headRow.setHeight((short) 450);
		for (int i = 0; i < heads.length; i++) {
			HSSFCell cell = headRow.createCell(i);
			cell.setCellStyle(style);
			cell.setCellValue(heads[i]);
		}
	}

	/**
	 * 데이터 row 만들기
	 * 
	 * @author yjyoo
	 * @since 2015. 5. 22.
	 * @return void
	 */
	public static void createRow(HSSFSheet sheet, List<Object> excelData, String[] columns, HSSFCellStyle style) {
	    int rowIdx = 2;

	    for (Iterator localIterator = excelData.iterator(); localIterator.hasNext(); ) { 
	    	Object obj = localIterator.next();
	    	int columnIdx = 0;
	    	rowIdx++; 
	    	HSSFRow row = sheet.createRow(rowIdx);
	    	HSSFCell cell = row.createCell(0);

	    	Class classObj = obj.getClass();

	    	Field[] dFields = classObj.getDeclaredFields();

	    	for (String column : columns) {
	    		for (Field field : dFields) {
	    			field.setAccessible(true);
	    			try {
	    				if (column.equals(field.getName())) {
	    					cell = row.createCell(columnIdx++);

	    					Class fieldType = field.getType();

	    					if (fieldType.equals(Integer.TYPE)) {
	    						cell.setCellValue(((Integer)field.get(obj)).intValue());
	    					}
	    					if (fieldType.equals(String.class)) {
	    						cell.setCellValue((String)field.get(obj));
	    					}
	    					if (fieldType.equals(Date.class)) {
	    						cell.setCellValue((Date)field.get(obj));
	    					}
	    					if (fieldType.equals(Timestamp.class)) {
	    						cell.setCellValue((Timestamp)field.get(obj));
	    					}
	    					row.setHeight((short) 350);
	    					
	    					if(rowIdx % 2 == 0) {
	    			    		style.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
	    			    	}
	    			    	cell.setCellStyle(style);
	    				}
	    			}
	    			catch (Exception e) {
	    				e.printStackTrace();
	    			}
	    		}
	    	}
	    }
	}
}
