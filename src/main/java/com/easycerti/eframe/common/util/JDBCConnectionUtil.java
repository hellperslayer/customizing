package com.easycerti.eframe.common.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JDBCConnectionUtil {

	public static List<Map<String, Object>> ConnectionResult(String className, String url, String id, String pass, String sql, Object[] param, Map findColumn) {
		List resList = new ArrayList<>();
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			Class.forName(className);
			con = DriverManager.getConnection(url, id, pass);
			
			pstmt = con.prepareStatement(sql);
			if (param.length > 0) {
				for (int i = 0; i < param.length; i++) {
					Object obj = param[i];
					if (obj instanceof Integer) {
						pstmt.setInt(i+1, (int) obj);
					} else if (obj instanceof String) {
						pstmt.setString(i+1, obj.toString());
					} else {
						pstmt.setString(i+1, obj.toString());
					}
				}
			}
			rs = pstmt.executeQuery();
			while(rs.next()) {
				Map map = new HashMap<>();
				for (Object key : findColumn.keySet()) {
					int idx = rs.findColumn(key.toString());
					Object obj = findColumn.get(key);
					if (obj.toString().equals("int")) {
						map.put(key.toString(), rs.getInt(idx));
					} else if (obj.toString().equals("string")) {
						map.put(key.toString(), rs.getString(idx));
					} else {
						map.put(key.toString(), rs.getString(idx));
					}
				}
				resList.add(map);
			}
		}
		catch (Exception e) {
			System.out.println("데이터베이스 연결 실패" + e);
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (con != null)
					con.close();
				if (rs != null)
					rs.close();
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
		
		return resList;
	}
}
