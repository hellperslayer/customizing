package com.easycerti.eframe.common.util;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public class BeanUtils {
	/**
	 * Map 을 Bean으로 말아준다.</br> - commons-beanutils-1.8.3 필요 </br> -
	 * commons-beanutils-core-1.8.3 필요
	 *
	 * @param map
	 * @param bean
	 * @throws java.lang.reflect.InvocationTargetException
	 * @throws IllegalAccessException
	 */
	public static void convertMapToBean(Map map, Object bean) throws InvocationTargetException, IllegalAccessException {
		if (map != null && bean != null) {
			org.apache.commons.beanutils.BeanUtils.populate(bean, map);
		}
	}

	/**
	 * Map을 bean instance를 생성하여 말아준다.</br> - commons-beanutils-1.8.3 필요 </br> -
	 * commons-beanutils-core-1.8.3 필요
	 * 
	 * @param map
	 * @param clazz
	 * @param <T>
	 * @return
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws InvocationTargetException
	 */
	public static <T> T convertMapToBean(Map map, Class<T> clazz) throws IllegalAccessException, InstantiationException, InvocationTargetException {
		T bean = null;
		if (map != null) {
			bean = clazz.newInstance();
			org.apache.commons.beanutils.BeanUtils.populate(bean, map);
		}

		return bean;
	}
}