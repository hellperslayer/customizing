package com.easycerti.eframe.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import com.easycerti.eframe.psm.system_management.vo.PrivacyReport;

/**
 * 
 * @author easy
 *
 */
public class DateUtil {
	// 현재 날짜를 yyyy.MM.dd 포맷의 string으로 return
	public static String getDateToString() {
        SimpleDateFormat formatter =
            new SimpleDateFormat("yyyyMMdd");
        return formatter.format(new Date());
    }
	// 현재 날짜를 특정 포맷의 string으로 return
	public static String getDateToString(String format) {
		SimpleDateFormat formatter =
	            new SimpleDateFormat(format);
		return formatter.format(new Date());
	}
	// 특정 날짜를 특정 포맷의 string으로 return
	public static String getDateToString(Date date, String format) {
		SimpleDateFormat formatter =
	            new SimpleDateFormat(format);
		return formatter.format(date);
	}
	// 특정 날짜를 특정 포맷 및 특정 timezone의 string으로 return
	public static String getDateToString(Date date, String format, String timeZoneId) {
		SimpleDateFormat formatter =
	            new SimpleDateFormat(format);
		TimeZone timeZone = TimeZone.getTimeZone(timeZoneId);
		formatter.setTimeZone(timeZone);
		return formatter.format(date);
	}
	/**
	 * yyyyMMdd 포맷의 string형 날짜를 date 타입으로 return
	 * @param date(string) - yyyyMMdd
	 * @return Date
	 * @throws ParseException
	 */
	public static Date getStringToDate(String date) throws ParseException {
		SimpleDateFormat formatter = 
				new SimpleDateFormat("yyyyMMdd");
		
		return formatter.parse(date);
	}
	
	/**
	 * 특정 포맷의 string형 날짜를 date 타입으로 return
	 * @param date(string)
	 * @param format(string)
	 * @return Date
	 * @throws ParseException
	 */
	public static Date getStringToDate(String date, String format) throws ParseException {
		SimpleDateFormat formatter =
				new SimpleDateFormat(format);
		try {
			return formatter.parse(date);
		} catch (ParseException e) {
			throw e;
		}
	}
	
	/**
	 * 특정 포맷 및 특정 타임존의 string형 날짜를 date 타입으로 return
	 * @param date(string)
	 * @param format(string)
	 * @return Date
	 * @throws ParseException
	 */
	public static Date getStringToDate(String date, String format, String timeZoneId) throws ParseException {
		SimpleDateFormat formatter =
				new SimpleDateFormat(format);
		formatter.setTimeZone(TimeZone.getTimeZone(timeZoneId));
		try {
			return formatter.parse(date);
		} catch (ParseException e) {
			throw e;
		}
	}
	
	/**
	 * 
	 * @param crzDate
	 * @return
	 */
	public static String getStringWithHyphen(String yyyyMMdd) {
		String _date = yyyyMMdd.replaceAll("\\-", "");
		
		if (_date.length() > 6) {
			_date = _date.substring(0, 4) + "-" + _date.substring(4, 6) + "-" + _date.substring(6);
		} else if (_date.length() > 4) {
			_date = _date.substring(0, 4) + "-" + _date.substring(4);
		}
		
		return _date;
	}
	
	public static String addDate(String date, int days) throws ParseException {
		Calendar temp = Calendar.getInstance();
		temp.setTime(new SimpleDateFormat("yyyyMMdd").parse(date));
		temp.add(Calendar.DATE, days);
		return getDateToString(temp.getTime(), "yyyyMMdd");
	}
	public static String addMonth(String date, int amount) throws ParseException {
		Calendar temp = Calendar.getInstance();
		temp.setTime(new SimpleDateFormat("yyyyMMdd").parse(date));
		temp.add(Calendar.MONTH, amount);
		return getDateToString(temp.getTime(), "yyyyMMdd");
	}
	
	/**
	 * Date.getDate() 로 두 날짜의 차를 구하는 것은 위험하다.(그 예로 day saving time...)
	 * calendar를 이용해 차를 구하는 것이 바람직 한 것 같다.
	 * 무턱대고 calendar를 이용하여 하루씩 더해본다면 두 날짜의 차가 클경우 성능에 상당한 문제가 발생할 수 있으므로,
	 * 두 날짜 차를 구한 후, 검증하는 방식
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static long daysBetween(final Calendar startDate, final Calendar endDate) {
		// 하루를 밀리세컨드로 변환
		int MILLIS_IN_DAY = 1000 * 60 * 60 * 24;
		long endInstant = endDate.getTimeInMillis();	// 피감 날짜를 밀리세컨드로 변화
		int presumedDays = (int) ((endInstant - startDate.getTimeInMillis()) / MILLIS_IN_DAY);
		Calendar cursor = (Calendar) startDate.clone();
		cursor.add(Calendar.DAY_OF_YEAR, presumedDays);
		long instant = cursor.getTimeInMillis();
		if (instant == endInstant)
			return presumedDays;
		final int step = instant < endInstant ? 1 : -1;
		do {
			cursor.add(Calendar.DAY_OF_MONTH, step);
			presumedDays += step;
		} while (cursor.getTimeInMillis() != endInstant);
		return presumedDays;
	}
	
    public static int getNumberByPattern(String pattern) {
        java.text.SimpleDateFormat formatter =
            new java.text.SimpleDateFormat (pattern, java.util.Locale.KOREA);
        String dateString = formatter.format(new java.util.Date());
        return Integer.parseInt(dateString);
    }
    public static String getStringByPattern(String pattern) {
        java.text.SimpleDateFormat formatter =
            new java.text.SimpleDateFormat (pattern, java.util.Locale.KOREA);
        return formatter.format(new java.util.Date());
    }
    public static String getCurrentTimeToString() {
        java.text.SimpleDateFormat formatter =
            new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss", java.util.Locale.KOREA);
        return formatter.format(new java.util.Date());
    }
    
    public static Date getLastWeek() {
		GregorianCalendar dayBeforeThisWeek = new GregorianCalendar();
		int dayFromMonday = (dayBeforeThisWeek.get(Calendar.DAY_OF_WEEK) + 7 - Calendar.MONDAY) % 7;
		dayBeforeThisWeek.add(Calendar.DATE, -dayFromMonday - 2);
		return dayBeforeThisWeek.getTime();
	}
    /**
	 * @desc : 마지막날짜가 현재보다 클 경우 어제날짜까지로 지정해준다.
	 * @auth : syjung
     */
    public String getReportEndDate(String endDate) {
		try {
			SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
			Date tmpDate = dayTime.parse(endDate);
			int compare = tmpDate.compareTo(new Date());
			if( compare >= 0) {
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DAY_OF_MONTH, -1);
				endDate = dayTime.format(cal.getTime());
			}
			return endDate;
		} catch (ParseException e) {
			e.printStackTrace();
			return endDate;
		}
    }
    
    /**
  	 * @desc : 현재날자 반환
	 * @auth : syjung
     */
	public String getDateNow() {
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd");
		long time = System.currentTimeMillis();
		return dayTime.format(new Date(time));
	}
    /**
     * @return : yyyyMMdd
  	 * @desc : start_date : 입력날짜, end_date : 입력날짜로 세팅
	 * @auth : syjung
	 * @return : 해당일자의 날짜 세팅 후 점검보고서VO를 반환
     */
    public PrivacyReport getInputDatePriReportVO(String startDate, String endDate) {
    	PrivacyReport pr = new PrivacyReport();
		// stDate, edDate : yyyyMM
		String stDate = startDate.replaceAll("-", "");
		String edDate = endDate.replaceAll("-", "");
		pr.setStart_date(stDate);
		pr.setEnd_date(edDate);
    	return pr;
    }
    /**
     * @return : yyyyMM
  	 * @desc : start_date : 입력날짜, end_date : 입력날짜로 세팅
	 * @auth : syjung
	 * @return : 해당일자의 날짜 세팅 후 점검보고서VO를 반환
     */
    public PrivacyReport getYyyyMmDatePriReportVO(String startDate, String endDate) {
    	PrivacyReport pr = new PrivacyReport();
		// stDate, edDate : yyyyMM
    	String sDate[] = startDate.split("-");
		String eDate[] = endDate.split("-");
		String stDate = sDate[0] + sDate[1];
		String edDate = eDate[0] + eDate[1];
		pr.setStart_date(stDate);
		pr.setEnd_date(edDate);
    	return pr;
    }
    
    /**
     * @return : yyyyMMdd
  	 * @desc : start_date : 이전 달 1일, end_date : 이전 달 31일로 세팅
	 * @auth : syjung
	 * @return : 해당일자의 날짜 세팅 후 점검보고서VO를 반환
     */
    public PrivacyReport getBefMonthPriReportVO(String startDate, String endDate) {
    	PrivacyReport pr = new PrivacyReport();
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
    	String compareDate = "";
    	String sDate[] = startDate.split("-");
		String eDate[] = endDate.split("-");
		int strYear = Integer.parseInt(sDate[0]);
		int strMonth = Integer.parseInt(sDate[1]);
		int endYear = Integer.parseInt(eDate[0]);
		int endMonth = Integer.parseInt(eDate[1]);
		int diffMonth = (endYear - strYear)* 12 + (endMonth - strMonth);
		// stDate, edDate : yyyyMM
		String stDate = startDate.replaceAll("-", "").substring(0,6);
		String edDate = endDate.replaceAll("-", "").substring(0,6);
    	Calendar cal = Calendar.getInstance();
	    cal.set(strYear, strMonth-1, 1);		//선택기간 시작월 1일
	    cal.add(Calendar.MONTH, -1);    		// 비교 기간 끝(선택기간 한달전)
	    compareDate = dateFormatter.format(cal.getTime());
	    edDate = compareDate.substring(0,6)+"31";
		cal.add(Calendar.MONTH, -diffMonth);    	// 비교 기간 시작
		compareDate = dateFormatter.format(cal.getTime());
		stDate = compareDate.substring(0,6)+"01";
		pr.setStart_date(stDate);
		pr.setEnd_date(edDate);
    	return pr;
    }
    
    /**
     * @return : yyyyMM
  	 * @desc : start_date : 이전 달 1일, end_date : 이전 달 31일로 세팅
	 * @auth : syjung
	 * @return : 해당일자의 날짜 세팅 후 점검보고서VO를 반환
     */
    public PrivacyReport getYyyyMmMonthPriReportVO(String startDate, String endDate) {
    	PrivacyReport pr = new PrivacyReport();
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
    	String compareDate = "";
    	String sDate[] = startDate.split("-");
		String eDate[] = endDate.split("-");
		int strYear = Integer.parseInt(sDate[0]);
		int strMonth = Integer.parseInt(sDate[1]);
		int endYear = Integer.parseInt(eDate[0]);
		int endMonth = Integer.parseInt(eDate[1]);
		int diffMonth = (endYear - strYear)* 12 + (endMonth - strMonth);
		// stDate, edDate : yyyyMM
		String stDate = startDate.replaceAll("-", "").substring(0,6);
		String edDate = endDate.replaceAll("-", "").substring(0,6);
    	Calendar cal = Calendar.getInstance();
	    cal.set(strYear, strMonth-1, 1);		//선택기간 시작월 1일
	    cal.add(Calendar.MONTH, -1);    		// 비교 기간 끝(선택기간 한달전)
	    compareDate = dateFormatter.format(cal.getTime());
	    edDate = compareDate.substring(0,6);
		cal.add(Calendar.MONTH, -diffMonth);    	// 비교 기간 시작
		compareDate = dateFormatter.format(cal.getTime());
		stDate = compareDate.substring(0,6);
		pr.setStart_date(stDate);
		pr.setEnd_date(edDate);
    	return pr;
    }
    
    /**
     * @return : yyyyMMdd
  	 * @desc : start_date : 이전 년도 1일, end_date : 이전 년도 31일로 세팅
	 * @auth : syjung
	 * @return : 해당일자의 날짜 세팅 후 점검보고서VO를 반환
     */
    public PrivacyReport getBefYearPriReportVO(String startDate, String endDate) {
    	PrivacyReport pr = new PrivacyReport();
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
    	String compareDate = "";
    	String sDate[] = startDate.split("-");
		String eDate[] = endDate.split("-");
		int strYear = Integer.parseInt(sDate[0]);
		int strMonth = Integer.parseInt(sDate[1]);
		int endYear = Integer.parseInt(eDate[0]);
		int endMonth = Integer.parseInt(eDate[1]);
		int diffMonth = (endYear - strYear)* 12 + (endMonth - strMonth);
		// stDate, edDate : yyyyMM
		String stDate = startDate.replaceAll("-", "").substring(0,6);
		String edDate = endDate.replaceAll("-", "").substring(0,6);
    	Calendar cal = Calendar.getInstance();
    	cal.set(strYear, strMonth-1, 1);
		cal.add(Calendar.YEAR, -1);    		// 비교 기간 끝(선택기간 한달전)
	    compareDate = dateFormatter.format(cal.getTime());
		edDate = compareDate.substring(0,6)+"31";
		cal.add(Calendar.YEAR, -diffMonth);    	// 비교 기간 시작
		compareDate = dateFormatter.format(cal.getTime());
		stDate = compareDate.substring(0,6)+"01";
		pr.setStart_date(stDate);
		pr.setEnd_date(edDate);
    	return pr;
    }
    
    /**
     * @return : yyyyMMdd
  	 * @desc : start_date : 이전 년도 1일, end_date : 이전 년도 31일로 세팅
	 * @auth : syjung
	 * @return : 해당일자의 날짜 세팅 후 점검보고서VO를 반환
     */
    public PrivacyReport getYyyyMmYearPriReportVO(String startDate, String endDate) {
    	PrivacyReport pr = new PrivacyReport();
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
    	String compareDate = "";
    	String sDate[] = startDate.split("-");
		String eDate[] = endDate.split("-");
		int strYear = Integer.parseInt(sDate[0]);
		int strMonth = Integer.parseInt(sDate[1]);
		int endYear = Integer.parseInt(eDate[0]);
		int endMonth = Integer.parseInt(eDate[1]);
		int diffMonth = (endYear - strYear)* 12 + (endMonth - strMonth);
		// stDate, edDate : yyyyMM
		String stDate = startDate.replaceAll("-", "").substring(0,6);
		String edDate = endDate.replaceAll("-", "").substring(0,6);
    	Calendar cal = Calendar.getInstance();
    	cal.set(strYear, strMonth-1, 1);
		cal.add(Calendar.YEAR, -1);    		// 비교 기간 끝(선택기간 한달전)
	    compareDate = dateFormatter.format(cal.getTime());
		edDate = compareDate.substring(0,6)+"31";
		cal.add(Calendar.YEAR, -diffMonth);    	// 비교 기간 시작
		compareDate = dateFormatter.format(cal.getTime());
		stDate = compareDate.substring(0,6)+"01";
		pr.setStart_date(stDate);
		pr.setEnd_date(edDate);
    	return pr;
    }
}
