package com.easycerti.eframe.common.util;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class GetSHA256 {
	public static String getHashcode(String filepath) throws NoSuchAlgorithmException{

		BufferedInputStream bistream = null;
		MessageDigest md;
		byte[] mdbytes = null;
		try {
			bistream = new BufferedInputStream(new FileInputStream(filepath));
			md = MessageDigest.getInstance("SHA-256");
			byte[] dataBytes = new byte[1024];
	
			int nread = 0;
			while ((nread = bistream.read(dataBytes)) != -1) {
				md.update(dataBytes, 0, nread);
			}
			mdbytes = md.digest();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(bistream!=null)	try {	bistream.close();} catch (IOException e) { e.printStackTrace();}
		}
		// convert the byte to hex format
		StringBuffer sb = new StringBuffer("");
		for (int i = 0; i < mdbytes.length; i++) {
			sb.append(String.format("%02x", mdbytes[i] & 0xff));
		}
		
		return sb.toString();
	}
	
	public static String getHashcodeSHA128(String filepath) throws NoSuchAlgorithmException{

		BufferedInputStream bistream = null;
		MessageDigest md;
		byte[] mdbytes = null;
		try {
			bistream = new BufferedInputStream(new FileInputStream(filepath));
			md = MessageDigest.getInstance("SHA");
			byte[] dataBytes = new byte[1024];
	
			int nread = 0;
			while ((nread = bistream.read(dataBytes)) != -1) {
				md.update(dataBytes, 0, nread);
			}
			mdbytes = md.digest();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(bistream!=null)	try {	bistream.close();} catch (IOException e) { e.printStackTrace();}
		}
		// convert the byte to hex format
		StringBuffer sb = new StringBuffer("");
		for (int i = 0; i < mdbytes.length; i++) {
			sb.append(String.format("%02x", mdbytes[i] & 0xff));
		}
		
		return sb.toString();
	}
	
	public static String getHashcodeMD5(String filepath) throws NoSuchAlgorithmException{

		BufferedInputStream bistream = null;
		MessageDigest md;
		byte[] mdbytes = null;
		try {
			bistream = new BufferedInputStream(new FileInputStream(filepath));
			String et = "M";
			et+="D";
			et+="5";
			md = MessageDigest.getInstance(et);
			byte[] dataBytes = new byte[1024];
	
			int nread = 0;
			while ((nread = bistream.read(dataBytes)) != -1) {
				md.update(dataBytes, 0, nread);
			}
			mdbytes = md.digest();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(bistream!=null)	try {	bistream.close();} catch (IOException e) { e.printStackTrace();}
		}
		// convert the byte to hex format
		StringBuffer sb = new StringBuffer("");
		for (int i = 0; i < mdbytes.length; i++) {
			sb.append(String.format("%02x", mdbytes[i] & 0xff));
		}
		
		return sb.toString();
	}
}
