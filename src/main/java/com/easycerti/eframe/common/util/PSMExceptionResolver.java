package com.easycerti.eframe.common.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.psm.system_management.dao.OptionSettingDao;

/**
 * PSM의 전역 exception 관리</br>
 * - 해당 Context의 모든 exception을 처리한다.
 * 
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   
 *
 * </pre>
 */
public class PSMExceptionResolver implements HandlerExceptionResolver {
	
	private ResourceBundleMessageSource messageSource = null;
	
	/**
	 * 관리자 감사 로그 Dao
	 */
//	@Autowired
//	private LogAuthAdminUserDAO logAuthAdminUserDao;

	@Autowired
	private OptionSettingDao optionSettingDao;

	
	public void setMessageSource(ResourceBundleMessageSource messageSource){
		this.messageSource = messageSource;
	}

	@Override
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object object, Exception exception) {

		DataModelAndView modelAndView = new DataModelAndView("error");
		String code = null;
		String printErrorInfo = optionSettingDao.getOptionSettingValue("print_error_info");
		
		if(exception != null && exception instanceof ESException){
			modelAndView.addObject("title", "WARNING");
			ESException esException = (ESException) exception;
			code = esException.getStatusCode();
			if(code.equals("SYS006V")) { // session timeout
				
				boolean requestJsonFlag = SystemHelper.checkRequestType(request);
				if(requestJsonFlag){ // json요청
					String message = messageSource.getMessage(code, null, request.getLocale());
					modelAndView.addObject("title", "ERROR");
					modelAndView.addObject("statusCode", code);
					modelAndView.addObject("message", message);
					modelAndView.setViewName(CommonResource.JSON_VIEW);
				}
				else{ // 일반요청
					RedirectView redirectView = new RedirectView(request.getContextPath() + "/sessionTimeout.html");
					redirectView.setExposeModelAttributes(false);
					modelAndView.setView(redirectView);
				}
			}
			else if(code.equals("DuplicationLoginPage")){
				RedirectView redirectView = new RedirectView(request.getContextPath() + "/duplicationLoginPage.html");
				redirectView.setExposeModelAttributes(false);
				modelAndView.setView(redirectView);
			}
			else if(code.equals("SYS006V_USER")){
				RedirectView redirectView = new RedirectView(request.getContextPath() + "/loginUserView.psm");
				redirectView.setExposeModelAttributes(false);
				modelAndView.setView(redirectView);
				return modelAndView;
			}
			else if(code.equals("NotAllowedIP")){
				RedirectView redirectView = new RedirectView(request.getContextPath() + "/notAllowedIP.html");
				redirectView.setExposeModelAttributes(false);
				modelAndView.setView(redirectView);
				return modelAndView;
			}
			else if(code.equals("NotAuthPage")){
				RedirectView redirectView = new RedirectView(request.getContextPath() + "/notAuthPage.html");
				redirectView.setExposeModelAttributes(false);
				modelAndView.setView(redirectView);
				return modelAndView;
			}
			else {
				String viewType = code.substring(code.length()-1, code.length());
				if("J".equals(viewType)){
					modelAndView.setViewName(CommonResource.JSON_VIEW);
				}else if("X".equals(viewType)){
					modelAndView.setViewName(CommonResource.XML_VIEW);
				}
				
				String message = messageSource.getMessage(code, null, request.getLocale());
				
				modelAndView.addObject("statusCode", code);
				modelAndView.addObject("message", message);
				
				if(!code.equals("SYS004J")) {
					if(exception.getStackTrace() != null && printErrorInfo != null && printErrorInfo.equals("Y")){
						modelAndView.addObject("detailMessage", exception.getMessage() + "\n" + getExceptionStackTraceToString(exception.getStackTrace()));
					}
				}
			}
		}else if(exception != null && exception instanceof DataAccessException){
			modelAndView.addObject("title", "WARNING");
			modelAndView.setViewName(CommonResource.JSON_VIEW);
			modelAndView.addObject("statusCode","ERR000");
			modelAndView.addObject("message","실패");
			if(exception.getStackTrace() != null && printErrorInfo != null && printErrorInfo.equals("Y")){

				modelAndView.addObject("detailMessage", exception.getMessage() + "\n" + getExceptionStackTraceToString(exception.getStackTrace()));
			}
			
		}else if(exception != null && exception instanceof HttpRequestMethodNotSupportedException){ // http exception
			modelAndView.addObject("title", "WARNING");
			code = "SYS005V";
			String message = messageSource.getMessage(code, null, request.getLocale());
			modelAndView.addObject("statusCode",code);
			modelAndView.addObject("message",message);
			if(exception.getStackTrace() != null && printErrorInfo != null && printErrorInfo.equals("Y")){
				modelAndView.addObject("detailMessage", exception.getMessage() + "\n" + getExceptionStackTraceToString(exception.getStackTrace()));
			}
		}else{
			modelAndView.addObject("title", "ERROR");
			code = "SYS001V";
			modelAndView.addObject("statusCode", code);
			modelAndView.addObject("message", messageSource.getMessage(code, null, request.getLocale()));
			if(exception.getStackTrace() != null && printErrorInfo != null && printErrorInfo.equals("Y")){
				modelAndView.addObject("detailMessage", exception.getMessage() + "\n" + getExceptionStackTraceToString(exception.getStackTrace()));
			}
		}
		
		/**
		 * 관리자 감사 로그 (request)
		 */
//		try {
//			String auth_id = UUID.randomUUID().toString().replaceAll("-", "");
//			String admin_user_id = SystemHelper.getAdminUserIdFromRequest(request);
//			String ip_address = java.net.InetAddress.getLocalHost().getHostAddress();
//			String menu_id = request.getParameter("sub_menu_id");
//			if(menu_id == null || menu_id.equals(CommonResource.EMPTY_STRING)) {
//				menu_id = request.getParameter("main_menu_id");
//				
//				if(menu_id == null || menu_id.equals(CommonResource.EMPTY_STRING)) {
//					menu_id = "";
//				}
//			}
//			String log_message = "";
//			String log_action = SystemResource.ADMIN_LOG_TYPE_ERROR;
//			
//			LogAuthAdminUserBean logAuthAdminUser = new LogAuthAdminUserBean();
//			logAuthAdminUser.setAuth_id(auth_id);
//			logAuthAdminUser.setAdmin_user_id(admin_user_id);
//			logAuthAdminUser.setIp_address(ip_address);
//			logAuthAdminUser.setMenu_id(menu_id);
//			logAuthAdminUser.setLog_message(log_message);
//			logAuthAdminUser.setLog_action(log_action);
//			
//			logAuthAdminUserDao.addLogAuthAdminUser(logAuthAdminUser);
//		
//		} catch (UnknownHostException e) {
//			e.printStackTrace();
//		}
		
		return modelAndView;
	}
	
	private String getExceptionStackTraceToString(StackTraceElement[] stackTraceElements){
		StringBuilder stringBuilder = new StringBuilder(); 
        for (int i=0;i<stackTraceElements.length;i++) {
            stringBuilder.append(stackTraceElements[i].toString());
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
	}
	

}
