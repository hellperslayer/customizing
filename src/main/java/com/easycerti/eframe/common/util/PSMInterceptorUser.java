package com.easycerti.eframe.common.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.psm.system_management.vo.EmpUser;

/**
 * 인가절차 유효성 체크를 위한 interceptor
 * - ObjectCont에서 인가 프로세스를 앞단에서 처리.
 * 
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 *  
 *
 * </pre>
 */
public class PSMInterceptorUser extends HandlerInterceptorAdapter {
	
	/**
	 * 인가절차가 필요없는 path</br>
	 * - properties/config.properties 참조
	 */
	private String[] noneAuthorTargetUrlsUser;
	
	public void setNoneAuthorTargetArr(String noneAuthorUrlsUser){
		if(noneAuthorUrlsUser != null){
			this.noneAuthorTargetUrlsUser = noneAuthorUrlsUser.split(CommonResource.COMMA_STRING);
		}
	}
	
	/**
	 * 비인가 요청의 경우 redirect Path 설정을 위함.</br>
	 * - 설정하지 않을 경우 contextpath로 보냄.
	 */
	private String redirectPath = null;
	public void setRedirectPath(String redirectPath){
		this.redirectPath = redirectPath;
	}
	
	/**
	 * dispatch servlet 처리 직후 request mapping을 타기 전 처리.
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		
		String userType = (String) request.getSession().getAttribute("userType");
//		EmpUser userSession = null;
//		if(userType!= null && userType.equals("USER")){
//			userSession = (EmpUser)request.getSession().getAttribute("userSession");
//		}
		
		String path = request.getServletPath();
		boolean authorTargetFlag = true;
		if(noneAuthorTargetUrlsUser != null){
			for(String target : noneAuthorTargetUrlsUser){
				if(path != null && !CommonResource.EMPTY_STRING.equals(path) && path.equals(target)){
					authorTargetFlag = false;
					break;
				}else{ // 인가 유효성 체크 대상.
					authorTargetFlag = true;
				}
			}
		}
		
		// 인가체크대상일때
		if(!authorTargetFlag){
			EmpUser empUser = null;
			if(userType.equals("USER")){
				empUser = (EmpUser)CommonHelper.findSessionUser(request);
			}
			if(empUser == null){
				throw new ESException("SYS006V_USER");
			}
		}
		
		return super.preHandle(request, response, handler);
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		
		super.postHandle(request, response, handler, modelAndView);
	}
}
