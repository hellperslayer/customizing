package com.easycerti.eframe.common.util;
import java.lang.reflect.Field;

public class GetStatisticsTypeData {
	
	public static int getType(Object obj, String fieldName) {
		int retVal = 0;
		Class classObj = obj.getClass();

    	Field[] dFields = classObj.getDeclaredFields();

		try {
			Field f = classObj.getDeclaredField(fieldName);
			
			f.setAccessible(true);
			
			Object object = f.get(obj);
			if( object != null && object instanceof Integer ) {
				retVal = (int) f.get(obj);
//				System.out.println("[EzAgent] [Field] : "+f.getName()+", [Value] : "+retVal);
			}
			f.setAccessible(false);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} 
		return retVal;
	}
	
	
	/* 사용법 참고
	public static void main(String[] args) {
		Statistics s = new Statistics();
		s.setType1(123);
		s.setType2(444);
		int type = GetStatisticsTypeData.getType(s, "type"+2);
		System.out.println(type);
	}
	*/
}
