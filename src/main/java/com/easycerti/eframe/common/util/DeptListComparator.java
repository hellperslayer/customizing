package com.easycerti.eframe.common.util;

import java.util.Comparator;

import com.easycerti.eframe.psm.system_management.vo.PrivacyReport;

public class DeptListComparator implements Comparator<PrivacyReport>{
	@Override
	public int compare(PrivacyReport first, PrivacyReport second) {
		int fNowLogCnt = first.getNowLogCnt();
		int sNowLogCnt = second.getNowLogCnt();
		
		
		//if(fNowLogCnt != 0) {
			if(fNowLogCnt > sNowLogCnt) {
				return -1;
			}else if(fNowLogCnt < sNowLogCnt) {
				return 1;
			}else 
				return 0;
		//}
	}
}
