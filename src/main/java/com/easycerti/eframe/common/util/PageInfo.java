package com.easycerti.eframe.common.util;

import java.util.Map;

/**
 * PageUtil
 * @author crzstriker
 * Util Patter이 아님에 주의
 */
public class PageInfo {
	
	// fields
	private int page_num;
	private int cur_num;
	private int size;
	private int total_count;
	private long count;
	
	//private int start_index;
	// 엑셀다운로드용 변수 : 엑셀다운로드일때 SvcImpl에서 값을 true로 변환
	private String useExcel = "false";
	
	// constructors
	public PageInfo() {
		this(1, 10, 0);
	}
	public PageInfo(int page_num, int size) {
		this(page_num, size, 0);
	}
	public PageInfo(int page_num, int size, int total_count) {
		this.page_num = page_num;
		this.size = size;
		this.total_count = total_count;
	}
	public PageInfo(int page_num, int size, long count) {
		this.page_num = page_num;
		this.size = size;
		this.count = count;
	}
	public PageInfo(Map<String, String> parameters, String defaultPageSize) {
		if ((parameters != null) && (parameters.size() > 0)) {
			Integer pageNum = Integer
					.valueOf((parameters.get("page_num") == null)
							|| ("".equals(parameters.get("page_num"))) ? 1
							: Integer.parseInt((String) parameters
									.get("page_num")));
			Integer pageSize = Integer
					.valueOf((parameters.get("page_size") == null)
							|| ("".equals(parameters.get("page_size"))) ? Integer
							.parseInt(defaultPageSize) : Integer
							.parseInt((String) parameters.get("page_size")));

			Integer pageIndex = Integer.valueOf((pageNum.intValue() - 1) * pageSize.intValue());

			this.cur_num = pageNum;
			this.size = pageSize;
			this.page_num = pageNum;
			//this.start_index = pageIndex;
		}
	}
	
	public long getCount() {
		return count;
	}
	public void setCount(long count) {
		this.count = count;
	}
	
	public String getUseExcel() {
		return useExcel;
	}
	public void setUseExcel(String useExcel) {
		this.useExcel = useExcel;
	}
	// getter and setter's
	public int getPage_num() {
		return page_num;
	}
	public void setPage_num(int cur_num) {
		this.page_num = cur_num;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public int getTotal_count() {
		return total_count;
	}
	public void setTotal_count(int total_count) {
		this.total_count = total_count;
	}
	public int getStart_index() {
		return page_num < 1?0:(page_num - 1) * size;
	}
//	public void setStart_index(int start_index) {
//		this.start_index = start_index;
//	}
	public int getCur_num() {
		return cur_num;
	}
	public void setCur_num(int cur_num) {
		this.cur_num = cur_num;
	}
	
	// toString
	@Override
	public String toString() {
		return "PageInfo [page_num=" + page_num + ", total_count=" + total_count
				+ ", start_index=" + getStart_index() + ", size=" + size + "]";
	}
}
