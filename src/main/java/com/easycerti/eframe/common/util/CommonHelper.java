package com.easycerti.eframe.common.util;

import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public class CommonHelper {
	public static Map<String, String> setPageParam(Map<String, String> parameters, String defaultPageSize) {
		if ((parameters != null) && (parameters.size() > 0)) {
			Integer pageNum = Integer
					.valueOf((parameters.get("page_num") == null)
							|| ("".equals(parameters.get("page_num"))) ? 1
							: Integer.parseInt((String) parameters
									.get("page_num")));
			Integer pageSize = Integer
					.valueOf((parameters.get("page_size") == null)
							|| ("".equals(parameters.get("page_size"))) ? Integer
							.parseInt(defaultPageSize) : Integer
							.parseInt((String) parameters.get("page_size")));

			Integer pageIndex = Integer.valueOf((pageNum.intValue() - 1) * pageSize.intValue());

			parameters.remove("page_num");
			parameters.remove("page_size");

			parameters.put("page_cur_num", String.valueOf(pageNum));
			parameters.put("page_size", String.valueOf(pageSize));
			parameters.put("page_start_index", String.valueOf(pageIndex));
		}

		return parameters;
	}

	public static boolean checkExistenceToParameter(Map<String, String> parameters, String[] args) {
		boolean returnValue = false;
		try {
			returnValue = MapUtils.checkExistence(parameters, args);
		} catch (Exception e) {
			returnValue = false;
		}
		return returnValue;
	}

	public static <T> T convertMapToBean(Map<String, String> map, Class<T> clazz) {
		Object resultBean = null;
		try {
			resultBean = BeanUtils.convertMapToBean(map, clazz);
		} catch (InvocationTargetException e) {
			resultBean = null;
		} catch (InstantiationException e1) {
			resultBean = null;
		} catch (IllegalAccessException e1) {
			resultBean = null;
		}

		return (T) resultBean;
	}

	public static Map<String, String> checkSearchDate(Map<String, String> parameters) {
		return checkSearchDate(parameters, Integer.valueOf(1), Locale.KOREA);
	}
	
	public static Map<String, String> checkSearchDateByWeek(Map<String, String> parameters) {
		return checkSearchDateByWeek(parameters, Integer.valueOf(1), Locale.KOREA);
	}

	public static Map<String, String> checkSearchDate(Map<String, String> parameters, Integer durationDate) {
		return checkSearchDate(parameters, durationDate, Locale.KOREA);
	}

	public static Map<String, String> checkSearchDate(Map<String, String> parameters, Integer durationDate, Locale locale) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", locale);
		Calendar calendar = Calendar.getInstance();
		Date toDate = new Date();
		
		if ((parameters.get("search_from") == null)
				&& (parameters.get("search_to") == null)) {
			
			durationDate = Integer.valueOf(durationDate.intValue() * -1);
			calendar.add(5, durationDate.intValue());
			
			parameters.put("search_from", sdf.format(calendar.getTime()));
			parameters.put("search_to", sdf.format(toDate));
		}
				
		return parameters;
	}
	
	public static Map<String, String> checkSearchDateByWeek(Map<String, String> parameters, Integer durationDate, Locale locale) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", locale);
		Calendar calendar = Calendar.getInstance();
		Date toDate = new Date();
		
		if ((parameters.get("search_from") == null)
				&& (parameters.get("search_to") == null)) {
			
			durationDate = Integer.valueOf(durationDate.intValue() * -7);
			calendar.add(5, durationDate.intValue());
			
			parameters.put("search_from", sdf.format(calendar.getTime()));
			parameters.put("search_to", sdf.format(toDate));
		}
				
		return parameters;
	}
	
	public static Map<String, String> getDateMonth(Map<String, String> parameters) {
		
		if ((parameters.get("search_from") == null)
				&& (parameters.get("search_to") == null)) {
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String last = sdf.format(new Date());
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, -1);
			
			String[] arrDate = last.split("-");
			
			String first = "";
			if (arrDate[2].equals("01")) {
				String[] tmp = sdf.format(cal.getTime()).split("-");
				first = tmp[0] + "-" + tmp[1] + "-01";
			} else {
				first = arrDate[0] + "-" + arrDate[1] + "-01";
			}
			
			parameters.put("search_from", first);
			parameters.put("search_to", sdf.format(cal.getTime()));
		}
		
		return parameters;
	}

	public static void saveSessionUser(HttpServletRequest request, Object object) {
		request.getSession().setAttribute("userSession", object);
	}

	public static Object findSessionUser(HttpServletRequest request) {
		return request.getSession().getAttribute("userSession");
	}

	public static void removeSessionUser(HttpServletRequest request) {
		request.getSession().removeAttribute("userSession");
	}
	
	public static void removeSessionUserType(HttpServletRequest request) {
		request.getSession().removeAttribute("userSession");
	}

	public static void removeSessionMenuInfo(HttpServletRequest request) {
		request.getSession().removeAttribute("mainMenu");
		request.getSession().removeAttribute("subMenu");
	}

	public static String getGUID() {
		return StringUtils.getGUID32();
	}
	
	public static String formatTime(String time){
		if(time == null){
			return "00";
		}
		else if(time.length() == 1){
			return "0" + time;
		}
		return time;
	}
}