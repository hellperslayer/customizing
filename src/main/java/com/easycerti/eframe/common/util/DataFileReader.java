package com.easycerti.eframe.common.util;

import java.util.List;

public interface DataFileReader {
	public List<String[]> readRows(int batchSize) throws Exception;
}
