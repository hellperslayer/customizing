package com.easycerti.eframe.common.util;

import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class JDBCDriverManager implements ServletContextListener {

	private Driver driver;
	
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        this.driver = new org.postgresql.Driver();
        boolean skipRegistration = false;
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();
            if (driver instanceof org.postgresql.Driver) {
            	org.postgresql.Driver alreadyRegistered = (org.postgresql.Driver) driver;
                if (alreadyRegistered.getClass() == this.driver.getClass()) {
                    // same class in the VM already registered itself
                    skipRegistration = true;
                    this.driver = alreadyRegistered;
                    break;
                }
            }
        }

        try {
            if (!skipRegistration) {
                DriverManager.registerDriver(driver);
                System.out.println("driver was registered manually");
            } else {
            	System.out.println("driver was registered automatically");
            }
        } catch (SQLException e) {
        	System.out.println("Error registering Postgresql Driver : " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        if (this.driver != null) {
            try {
                DriverManager.deregisterDriver(driver);
            } catch (SQLException e) {
            	
            }
            this.driver = null;
        } else {
        	
        }

    }

}
