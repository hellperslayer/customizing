package com.easycerti.eframe.common.util;

import java.util.Comparator;

import com.easycerti.eframe.psm.system_management.vo.PrivacyReport;

public class ListComparator implements Comparator<PrivacyReport>{
	@Override
	public int compare(PrivacyReport first, PrivacyReport second) {
		int ftype1 = first.getType1();
		int stype1 = second.getType1();
		int ftype2 = first.getType2();
		int stype2 = second.getType2();
		int ftype3 = first.getType3();
		int stype3 = second.getType3();
		int ftype4 = first.getType4();
		int stype4 = second.getType4();
		
		int fprevPrivacyCnt = first.getPrevPrivacyCnt();
		int sprevPrivacyCnt = second.getPrevPrivacyCnt();
		
		int fnowPrivacyCnt = first.getNowPrivacyCnt();
		int snowPrivacyCnt = second.getNowPrivacyCnt();
		
		String period_type = first.getPeriod_type(); //보고서 type
		
		if(fnowPrivacyCnt != 0) {
			if(fnowPrivacyCnt > snowPrivacyCnt) {
				return -1;
			}else if(fnowPrivacyCnt < snowPrivacyCnt) {
				return 1;
			}else 
				return 0;
		}
		if(ftype1 != 0) {
			if(ftype1 > stype1) {
				return -1;
			}else if(ftype1 < stype1) {
				return 1;
			}else 
				return 0;
		}
		else 
			return 0;
	}
}
