package com.easycerti.eframe.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.UUID;
import java.util.regex.Pattern;

import com.easycerti.eframe.common.vo.CommonResourceBean;

/**
 * 문자열 관련 유틸
 */
public class StringUtils {

    /**
     * GUID를 생성한다.(32자)
     * @return
     */
    public static String getGUID32(){
        return UUID.randomUUID().toString().replaceAll(CommonResourceBean.HYPHEN_STRING, CommonResourceBean.EMPTY_STRING);
    }

    /**
     * GUID를 생성한다.(36자)
     * @return
     */
    public static String getGUID36(){
        return UUID.randomUUID().toString();
    }

	/**
	 * inputStream -> string UTF-8
	 * @param is
	 * @return String
	 * @throws java.io.IOException
	 */
	public static String convertStreamToString(InputStream is) throws IOException {
		if (is != null) {
			char[] buffer = new char[1024];
            String resultString;
			try (Reader reader = new BufferedReader(new InputStreamReader(is, CommonResourceBean.CHARSET_UTF8));Writer writer = new StringWriter();){
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
                resultString = writer.toString();
			}
            return resultString;
		} else {
			return CommonResourceBean.EMPTY_STRING;
		}
	}

    /**
     * 문자열 형식을 변환한다.
     * @param input
     * @param inputCharset
     * @param outputCharset
     * @return
     */
    public static String convertToCharsetString(String input, String inputCharset, String outputCharset){
        try {
            input = new String(inputCharset == null ? input.getBytes() : input.getBytes(inputCharset), outputCharset); //"ISO-8859-1"
        } catch (UnsupportedEncodingException e) {
            input = CommonResourceBean.EMPTY_STRING;
        }
        return input;
    }

    /**
     * 문자열 형식을 변환한다.
     * @param input
     * @param outputCharset
     * @return
     */
    public static String convertToCharsetString(String input, String outputCharset){
        return convertToCharsetString(input, null, outputCharset);
    }

    /**
     * 문자열 형식을 변환한다.
     * @param input
     * @return
     */
    public static String convertToCharsetString(String input){
        return convertToCharsetString(input, null, CommonResourceBean.CHARSET_UTF8);
    }

	/**
	 * String NULL 체크
	 * @param data
	 * @return boolean (true : NOT NULL, false : NULL)
	 */
	public static boolean notNullCheck(String data){
		if(data == null || data.equals(CommonResourceBean.EMPTY_STRING))
			return false;
		else
			return true;
	}
	
	/**
	 * String CDATA 추가
	 * @param text
	 * @return String
	 */
	public static String addCDATA(String text){
		if(text == null || CommonResourceBean.EMPTY_STRING.equals(text)) return text;
		
		StringBuilder sb = new StringBuilder();
		sb.append("<![CDATA[ ");
		sb.append(text);
		sb.append(" ]]>");
		
		return sb.toString();
	}
	
	/**
	 * string에서 html tag 제거
	 * @param text
	 * @return String
	 */
	public static String removeHTMLTag(String text){
		if(text == null || text.equals(CommonResourceBean.EMPTY_STRING)) return text;
		text = text.replaceAll(CommonResourceBean.HTML_PATTERN, CommonResourceBean.EMPTY_STRING);
		return text; //text.replaceAll("\r\n", CommonResourceBean.EMPTY_STRING);
	}
	
    public static String removeBackSlash(String text){
        return convertBackSlash(text, CommonResourceBean.EMPTY_STRING);
    }

    public static String convertBackSlashToSlash(String text){
        return convertBackSlash(text, CommonResourceBean.SLASH_STRING);
    }

    private static String convertBackSlash(String text, String replaseText){
        if(text == null) return text;

        int idx = text.indexOf(CommonResourceBean.BACK_SLASH_STRING);
        if(idx > -1){
            return text.replace(CommonResourceBean.BACK_SLASH_STRING, replaseText);
        }
        return text;
    }
	
	/**
	 * 확장자 가져오기
	 * 
	 * @param text
	 * @return String
	 */
	public static String getExtension(String text) {

		String extension = null;

		if (!CommonResourceBean.EMPTY_STRING.equals(text)) {
			extension = text.substring(text.lastIndexOf(CommonResourceBean.PERIOD_STRING) + 1, text.length());
		}
		return extension;
	}

	/**
	 * 확장자 제거
	 * 
	 * @param text
	 * @return String
	 */
	public static String getFileNameRemoveExtension(String text) {

		String resultStr = text.substring(0, text.lastIndexOf(CommonResourceBean.PERIOD_STRING));
		return resultStr;
	}

    /**
     * long 시간 데이터를 string 시간 데이터로 변환 </br>
     *
     * @param longTime
     * @return hh:MM:ss
     */
    public static String convertLongToStringTime(Long longTime){
        return convertLongToStringTime(longTime,null);
    }

    /**
     * long 시간 데이터를 string 시간 데이터로 변환 </br>
     * - 구분기호가 없을 경우 default ':'
     *
     * @param longTime
     * @return hh:MM:ss
     */
    public static String convertLongToStringTime(Long longTime,String divisionStr){
        String division = ":";

        if(divisionStr != null){
            division = divisionStr;
        }

        String second = "" + (longTime / 1000) % 60;
        String minute = "" + (longTime / (1000 * 60)) % 60;
        String hour = "" + (longTime / (1000 * 60 * 60)) % 24;

        String time= convertTwoCipher(hour)+division+convertTwoCipher(minute)+division+convertTwoCipher(second);
        return time;
    }


    /**
     * 숫자 string의 자릿수를 체크하여, 2자릿수로 만든다.</br>
     * - 3 -> 03
     * @param str
     * @return
     */
    public static String convertTwoCipher(String str){
        return convertTwoCipher(str,2);
    }

    /**
     * 숫자 string의 자릿수를 체크하여, 원하는 자릿수로 만든다.</br>
     * - 자릿수 3 : 5 -> 005
     * @param str
     * @return
     */
    public static String convertTwoCipher(String str,int length){
        if(str.length() <= length - 1) {
            str = "0" + str;
            return convertTwoCipher(str,length);
        }else{
            return str;
        }
    }

    /**
     * 문자열의 숫자 여부를 체크한다.</br>
     * - ex) 123 : true, 233r : false
     * @param arg
     * @return
     */
    public static boolean isNumber(String arg){
        return Pattern.matches("[0-9]*",arg);
    }
	
    /**
     * 
     * 
     * @author hwkim
     * @since 2015. 6. 24.
     * @return boolean
     */
    public static boolean isEmpty(String str) {
		return ((str == null) || (str.isEmpty()));
	}
    
    /**
     * 
     * 설명
     * @author hwkim
     * @since 2015. 6. 24.
     * @return boolean
     */
    public static boolean isEmptyOrWhitespace(String str) {
		return (isEmpty(str) || (str.trim()).isEmpty());
	}
}
