package com.easycerti.eframe.common.util;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.easycerti.eframe.common.spring.DataModelAndView;

public class MapUtils {
	public static Map<String, String> getUrlEncodingForMap(Map<String, String> parameters) {
		Map resultMap = new HashMap();

		for (String paramKey : parameters.keySet()) {
			String value = (String) parameters.get(paramKey);
			if (paramKey != null) {
				try {
					value = URLEncoder.encode(value, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
			resultMap.put(paramKey, value);
		}
		return resultMap;
	}

	public static Map<String, String> getUrlDecodingForMap(Map<String, String> parameters) {
		Map resultMap = new HashMap();

		for (String paramKey : parameters.keySet()) {
			String value = (String) parameters.get(paramKey);
			if (paramKey != null) {
				try {
					value = URLDecoder.decode(value, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
			resultMap.put(paramKey, value);
		}
		return resultMap;
	}

	public static boolean nullCheckMap(Map<String, String> map, String[] args) {
		boolean returnValue = true;
		for (String arg : args) {
			if (!StringUtils.notNullCheck((String) map.get(arg))) {
				returnValue = false;
				break;
			}
		}
		return returnValue;
	}

	public static MultiValueMap<String, String> convertMapToMultiValue(Map<String, String> parameters, String[] paramNames) {
		LinkedMultiValueMap multiValueMap = new LinkedMultiValueMap();

		if (paramNames != null) {
			for (String paramName : paramNames)
				if (StringUtils.notNullCheck((String) parameters.get(paramName)))
					multiValueMap.add(paramName, parameters.get(paramName));
		} else {
			for (String key : parameters.keySet()) {
				if (StringUtils.notNullCheck((String) parameters.get(key))) {
					multiValueMap.add(key, parameters.get(key));
				}
			}
		}

		return multiValueMap;
	}

	public static DataModelAndView addFlashMapToMAV(DataModelAndView kmav, Map<String, ?> flashMap) {
		if ((flashMap == null) || (flashMap.size() < 1)) {
			return kmav;
		}

		for (String flashMapKey : flashMap.keySet()) {
			kmav.addObject(flashMapKey, flashMap.get(flashMapKey));
		}

		return kmav;
	}

	public static Map<String, String> getRedirectAttrToParam(Map<String, String> parameters, Map<String, ?> flashMap) {
		if ((flashMap == null) || (flashMap.size() < 1)) {
			return new HashMap();
		}

		for (String attrMapKey : flashMap.keySet()) {
			if ((flashMap.get(attrMapKey) instanceof String)) {
				parameters.put(attrMapKey, (String) flashMap.get(attrMapKey));
			}
		}

		return parameters;
	}

	public static void convertBeanToMap(Object bean, Map map) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
		if ((bean != null) && (map != null)) {
			Map tempMap = null;
			tempMap = PropertyUtils.describe(bean);

			if ((tempMap != null) && (map != null)) {
				tempMap.remove("class");
				map.putAll(tempMap);
			}
		}
	}

	public static boolean checkExistence(Map<String, String> parameters, String[] args) {
		boolean existFlag = true;
		if ((parameters != null) && (parameters.size() > 0) && (args != null) && (args.length > 0)) {
			for (String arg : args) {
				if ((parameters.get(arg) == null) || ("".equals(parameters.get(arg))))
					existFlag = false;
			}
		} else {
			existFlag = false;
		}

		return existFlag;
	}
}