package com.easycerti.eframe.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * @author ijchoi
 * @since 2020.03.27
 * @see
 * <pre>
 * 감사이력 데이터 insert 어노테이션, 컨트롤러 메소드에서만 탐지함
 * 
 * 필드명 (페이지 표기 컬럼)
 * 필수 입력 필드 : log_action (로그종류) (SELECT, UPDATE 등등)
 * 			   ,log_message (로그메세지) 만약 자바 로직상에서 로그메시지 입력이 필요한 경우 modelandview에 log_message 오브젝트로 추가하면 해당 메시지를 우선으로 저장
 * 선택 입력 필드 : l_category, m_category, s_category (접근 메뉴)
 * 				전부 입력시 (l_category) > (m_category) > (s_category) 으로 표기됌
 * 				전부 미입력시 DB에 저장된 해당 페이지 경로를 찾아 저장
 * 
 * aspect 클래스 MngtActHistAspect에서 어노테이션을 탐지하여 값을 manager_act_hist 테이블 입력
 * 
 *</pre>
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface MngtActHist {
	String log_action();
	String l_category() default "";
	String m_category() default "";
	String s_category() default "";
	String log_message() default "";
}
