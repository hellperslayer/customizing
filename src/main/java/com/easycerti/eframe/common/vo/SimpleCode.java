package com.easycerti.eframe.common.vo;

public class SimpleCode {

	// fields
	private String code_id;
	private String code_name;
	private int ip_seq;
	private String ip_content;
	private String menu_url;
	private String menu_id;
	
	public SimpleCode() {}
	public SimpleCode(String url) {
		menu_url = url;
	}
	
	public String getMenu_url() {
		return menu_url;
	}
	public void setMenu_url(String menu_url) {
		this.menu_url = menu_url;
	}
	public String getMenu_id() {
		return menu_id;
	}
	public void setMenu_id(String menu_id) {
		this.menu_id = menu_id;
	}
	public int getIp_seq() {
		return ip_seq;
	}
	public void setIp_seq(int ip_seq) {
		this.ip_seq = ip_seq;
	}
	public String getIp_content() {
		return ip_content;
	}
	public void setIp_content(String ip_content) {
		this.ip_content = ip_content;
	}
	// getter and setter's
	public String getCode_id() {
		return code_id;
	}
	public void setCode_id(String code_id) {
		this.code_id = code_id;
	}
	public String getCode_name() {
		return code_name;
	}
	public void setCode_name(String code_name) {
		this.code_name = code_name;
	}
	
	// toString
	@Override
	public String toString() {
		return "SimpleCodeBean [code_id=" + code_id + ", code_name="
				+ code_name + "]";
	}
}
