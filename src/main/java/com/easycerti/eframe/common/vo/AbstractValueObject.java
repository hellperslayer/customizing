package com.easycerti.eframe.common.vo;

import java.util.Date;

public abstract class AbstractValueObject {

	private String main_menu_id = null;

	private String sub_menu_id = null;

	private String page_cur_num = null;

	private String page_total_count = null;

	private String page_num = null;

	private String page_start_index = null;

	private String page_size = null;

	private String search_from = null;

	private String search_to = null;

	private String cur_menu_id = null;

	private String insert_user_id = null;

	private Date insert_datetime = null;

	private String update_user_id = null;

	private Date update_datetime = null;
	
	

	
	public String getMain_menu_id() {
		return this.main_menu_id;
	}

	public void setMain_menu_id(String main_menu_id) {
		this.main_menu_id = main_menu_id;
	}

	public String getSub_menu_id() {
		return this.sub_menu_id;
	}

	public void setSub_menu_id(String sub_menu_id) {
		this.sub_menu_id = sub_menu_id;
	}

	public String getPage_cur_num() {
		return this.page_cur_num;
	}

	public void setPage_cur_num(String page_cur_num) {
		this.page_cur_num = page_cur_num;
	}

	public String getPage_total_count() {
		return this.page_total_count;
	}

	public void setPage_total_count(String page_total_count) {
		this.page_total_count = page_total_count;
	}

	public String getPage_num() {
		return this.page_num;
	}

	public void setPage_num(String page_num) {
		this.page_num = page_num;
	}

	public String getPage_start_index() {
		return this.page_start_index;
	}

	public void setPage_start_index(String page_start_index) {
		this.page_start_index = page_start_index;
	}

	public String getPage_size() {
		return this.page_size;
	}

	public void setPage_size(String page_size) {
		this.page_size = page_size;
	}

	public String getSearch_from() {
		return this.search_from;
	}

	public void setSearch_from(String search_from) {
		this.search_from = search_from;
	}

	public String getSearch_to() {
		return this.search_to;
	}

	public void setSearch_to(String search_to) {
		this.search_to = search_to;
	}

	public String getCur_menu_id() {
		return this.cur_menu_id;
	}

	public void setCur_menu_id(String cur_menu_id) {
		this.cur_menu_id = cur_menu_id;
	}

	public String getInsert_user_id() {
		return this.insert_user_id;
	}

	public void setInsert_user_id(String insert_user_id) {
		this.insert_user_id = insert_user_id;
	}

	public Date getInsert_datetime() {
		return this.insert_datetime;
	}

	public void setInsert_datetime(Date insert_datetime) {
		this.insert_datetime = insert_datetime;
	}

	public String getUpdate_user_id() {
		return this.update_user_id;
	}

	public void setUpdate_user_id(String update_user_id) {
		this.update_user_id = update_user_id;
	}

	public Date getUpdate_datetime() {
		return this.update_datetime;
	}

	public void setUpdate_datetime(Date update_datetime) {
		this.update_datetime = update_datetime;
	}
	
}