package com.easycerti.eframe.common.vo;

public class ESException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	private String statusCode;
	private String message;
	private String detailMessage;
	private String[] args;

	public ESException(String statusCode) {
		this.statusCode = statusCode;
	}

	public ESException(String statusCode, String message) {
		this.statusCode = statusCode;
		this.message = message;
	}

	public ESException(String statusCode, String[] args) {
		this.statusCode = statusCode;
		this.args = args;
	}

	public String getStatusCode() {
		return this.statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDetailMessage() {
		return this.detailMessage;
	}

	public void setDetailMessage(String detailMessage) {
		this.detailMessage = detailMessage;
	}

	public String[] getArgs() {
		return this.args;
	}

	public void setArgs(String[] args) {
		this.args = args;
	}
}