package com.easycerti.eframe.common.vo;

public class CommonResourceBean {
	/**
	 * character set UTF-8
	 */
	public final static String CHARSET_UTF8 = "UTF-8";

	/**
	 * character set EUC-KR
	 */
	public final static String CHARSET_EUCKR = "EUC-KR";

	/**
	 * request mapping header Accept
	 */
	public final static String REQUEST_HEADER = "Accept=application/json,application/xml";
	/**
	 * contentType XML
	 */
	public final static String CONTENTTYPE_XML = "application/xml; charset=UTF-8";
	/**
	 * contentType JSON
	 */
	public final static String CONTENTTYPE_JSON = "application/json; charset=UTF-8";
	/**
	 * json view
	 */
	public final static String JSON_VIEW = "jsonView";
	/**
	 * xml view
	 */
	public final static String XML_VIEW = "xmlView";
	/**
	 * 빈 문자()
	 */
	public final static String EMPTY_STRING = "";
	/**
	 * 콜론(:)
	 */
	public final static String COLON_STRING = ":";
	/**
	 * 콤마(,)
	 */
	public final static String COMMA_STRING = ",";
	/**
	 * 슬래시(/)
	 */
	public final static String SLASH_STRING = "/";
	/**
	 * 백슬래시(\)
	 */
	public final static String BACK_SLASH_STRING = "\\";
	/**
	 * 마침표(.)
	 */
	public final static String PERIOD_STRING = ".";
	/**
	 * 하이픈(-)
	 */
	public final static String HYPHEN_STRING = "-";
	/**
	 * 쌍따음표(")
	 */
	public final static String QUOTATION_STRING = "\"";
	/**
	 * 예
	 */
	public final static String YES = "Y";
	/**
	 * 아니오
	 */
	public final static String NO = "N";
	/**
	 * xml format
	 */
	public final static String XML_INFO = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
	/**
	 * 날짜포맷(yyyy-MM-dd HH:mm:ss)
	 */
	public final static String PATTERN_DATETIME = "yyyy-MM-dd HH:mm:ss";
	/**
	 * 날짜포맷(yyyy-MM-dd)
	 */
	public final static String PATTERN_DATE = "yyyy-MM-dd";
	/**
	 * HTML 태그 정규식
	 */
	public final static String HTML_PATTERN = "<(/)?([a-zA-Z]*)(\\s[a-zA-Z]*=[^>]*)?(\\s)*(/)?>";
}