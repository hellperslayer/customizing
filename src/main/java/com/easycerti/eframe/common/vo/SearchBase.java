package com.easycerti.eframe.common.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import com.easycerti.eframe.common.util.DateUtil;
import com.easycerti.eframe.common.util.PageInfo;

/**
 * 
 * 설명
 * 대부분의 검색조건에 search_from 과 search_to 필드가
 * 검색조건으로 들어가는 곳에 사용하자
 * 
 * @author hwkim
 * @since 2015. 6. 12.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 6. 12.           easy            최초 생성
 *
 * </pre>
 */
public class SearchBase extends PageInfo {
	
	private final static short DEFAULT_TERM = 7;
	
	// fields
	private String search_from;
	private String search_to;
	
	private String main_menu_id;
	private String sub_menu_id;
	
	// constructors
	public SearchBase() {super();}
	public SearchBase(int page_num, int page_size) {
		super(page_num, page_size);
	}
	
	/**
	 * 오늘부터 7일전으로 날짜 세팅
	 * 
	 * @author hwkim
	 * @since 2015. 6. 12.
	 * @return void
	 */
	public void initDatesWithDefaultIfEmpty() {
		initDatesWithDefaultIfEmpty(DEFAULT_TERM);
	}
	
	/**
	 * 오늘 부터 일정기간 전으로 날짜 세팅
	 * 
	 * @author hwkim
	 * @since 2015. 6. 12.
	 * @return void
	 */
	public void initDatesWithDefaultIfEmpty(int term) {
		if ("".equals(getSearch_from()) || "".equals(getSearch_to())) {
			search_to = DateUtil.getDateToString();
			try {
				search_from = DateUtil.addDate(search_to, -term);
			} catch (ParseException e) {
				throw new ESException("SYS101J");
			}
		}
	}
	
	public void initDatesWithTerm(int term) {
		if ("".equals(getSearch_from()) || "".equals(getSearch_to())) {
			String today = DateUtil.getDateToString();
			try {
				search_from = DateUtil.addDate(today, -term);
				search_to = search_from;
			} catch (ParseException e) {
				throw new ESException("SYS101J");
			}
		}
	}
	
	public void initDatesWithDefaultIfEmptyByMonth(int term) {
		if ("".equals(getSearch_from()) || "".equals(getSearch_to())) {
			search_to = DateUtil.getDateToString();
			try {
				search_from = DateUtil.addMonth(search_to, -term);
			} catch (ParseException e) {
				throw new ESException("SYS101J");
			}
		}
	}
	
	public void initDateByMonth(String year, String month) {
		try {
			Calendar cal = new GregorianCalendar(Locale.KOREA);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			cal.set(Integer.parseInt(year), Integer.parseInt(month)-1, 1);
			search_from = sdf.format(cal.getTime());
			cal.setTime(sdf.parse(search_from));
			int day = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
			cal.set(Integer.parseInt(year), Integer.parseInt(month)-1, day);
			search_to = sdf.format(cal.getTime());
		} catch (ParseException e) {
			throw new ESException("SYS101J");
		}
	}
	
	/**
	 * 
	 * 특정한 시작 날짜와 종료날짜로 설정
	 * 
	 * @author easy
	 * @since 2015. 6. 12.
	 * @return void
	 */
	public void initDatesIfEmpty(Date search_from, Date search_to) {
		if ("".equals(getSearch_from()) || "".equals(getSearch_to())) {
			
			this.search_from = DateUtil.getDateToString(search_from, "yyyyMMdd");
			this.search_to = DateUtil.getDateToString(search_to, "yyyyMMdd");
		}
	}
	
	public void initDatesIfEmpty_2week(Date search_from, Date search_to) {
		if ("".equals(getSearch_from()) || "".equals(getSearch_to())) {
			
			Calendar cal = new GregorianCalendar(Locale.KOREA);
			cal.setTime(search_from);
			cal.add(Calendar.DAY_OF_YEAR, -14); // 하루를 더한다. 
			
			this.search_from = DateUtil.getDateToString(cal.getTime(), "yyyyMMdd");
			this.search_to = DateUtil.getDateToString(search_to, "yyyyMMdd");
		}
	}
	
	public void initDatesIfEmpty_week(Date search_from, Date search_to) {
		if ("".equals(getSearch_from()) || "".equals(getSearch_to())) {
			
			Calendar cal = new GregorianCalendar(Locale.KOREA);
			cal.setTime(search_from);
			cal.add(Calendar.DAY_OF_YEAR, -7); 
			
			this.search_from = DateUtil.getDateToString(cal.getTime(), "yyyyMMdd");
			this.search_to = DateUtil.getDateToString(search_to, "yyyyMMdd");
		}
	}
	
	public void initDatesIfEmpty_week(Date search_from, Date search_to, int amount) {
		if ("".equals(getSearch_from()) || "".equals(getSearch_to())) {
			
			Calendar cal = new GregorianCalendar(Locale.KOREA);
			cal.setTime(search_from);
			cal.add(Calendar.DATE, -7 * amount); 
			
			this.search_from = DateUtil.getDateToString(cal.getTime(), "yyyyMMdd");
			this.search_to = DateUtil.getDateToString(search_to, "yyyyMMdd");
		}
	}
	
	public void initDatesIfEmpty_month(Date search_from, Date search_to, int amount) {
		if ("".equals(getSearch_from()) || "".equals(getSearch_to())) {
			
			Calendar cal = new GregorianCalendar(Locale.KOREA);
			cal.setTime(search_from);
			cal.add(Calendar.MONTH, -amount);
			
			this.search_from = DateUtil.getDateToString(cal.getTime(), "yyyyMMdd");
			this.search_to = DateUtil.getDateToString(search_to, "yyyyMMdd");
		}
	}
	
	// getters and setters
	public String getSearch_from() {
		return (search_from == null?"":search_from);
	}
	public String getMain_menu_id() {
		return main_menu_id;
	}
	public void setMain_menu_id(String main_menu_id) {
		this.main_menu_id = main_menu_id;
	}
	public String getSub_menu_id() {
		return sub_menu_id;
	}
	public void setSub_menu_id(String sub_menu_id) {
		this.sub_menu_id = sub_menu_id;
	}
	public void setSearch_from(String search_from) {
		this.search_from = search_from.replaceAll("-", "");
	}
	public String getSearch_to() {
		return (search_to == null?"":search_to);
	}
	public void setSearch_to(String search_to) {
		this.search_to = search_to.replaceAll("-", "");
	}
	public String getSearch_fromWithHyphen() {
		return (search_from == null?"":DateUtil.getStringWithHyphen(search_from));
	}
	public String getSearch_toWithHyphen() {
		return (search_to == null?"":DateUtil.getStringWithHyphen(search_to));
	}

	// toString
	@Override
	public String toString() {
		return "SearchBase [search_from=" + search_from + ", search_to=" + "]";
	}
}
