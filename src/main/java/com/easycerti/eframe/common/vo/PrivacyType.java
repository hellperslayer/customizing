package com.easycerti.eframe.common.vo;

public class PrivacyType extends AbstractValueObject{
	private int privacy_seq;
	private String privacy_type;
	private String privacy_desc;
	private String regular_expression;
	private String use_flag;
	private String privacy_type_id;
	private String privacy_name;
	
	
	
	public String getPrivacy_name() {
		return privacy_name;
	}
	public void setPrivacy_name(String privacy_name) {
		this.privacy_name = privacy_name;
	}
	public String getPrivacy_type_id() {
		return privacy_type_id;
	}
	public void setPrivacy_type_id(String privacy_type_id) {
		this.privacy_type_id = privacy_type_id;
	}
	public int getPrivacy_seq() {
		return privacy_seq;
	}
	public void setPrivacy_seq(int privacy_seq) {
		this.privacy_seq = privacy_seq;
	}
	public String getPrivacy_type() {
		return privacy_type;
	}
	public void setPrivacy_type(String privacy_type) {
		this.privacy_type = privacy_type;
	}
	public String getPrivacy_desc() {
		return privacy_desc;
	}
	public void setPrivacy_desc(String privacy_desc) {
		this.privacy_desc = privacy_desc;
	}
	public String getRegular_expression() {
		return regular_expression;
	}
	public void setRegular_expression(String regular_expression) {
		this.regular_expression = regular_expression;
	}
	public String getUse_flag() {
		return use_flag;
	}
	public void setUse_flag(String use_flag) {
		this.use_flag = use_flag;
	}
}
