package com.easycerti.eframe.common.aspect;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.dao.DataAccessException;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.easycerti.eframe.common.resource.CommonResource;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.RequestUtil;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.vo.ESException;
import com.easycerti.eframe.psm.history_management.dao.ManagerActHistDao;
import com.easycerti.eframe.psm.history_management.vo.ManagerActHist;
import com.easycerti.eframe.psm.system_management.dao.MenuMngtDao;
import com.easycerti.eframe.psm.system_management.vo.Menu;

@Aspect
public class LoggingAspect {

private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private MessageSourceAccessor messageSourceAccessor;
	
	@Autowired
	private ManagerActHistDao managerActHistDao;
	
	@Autowired
	private MenuMngtDao menuMngtDao;
	
    @AfterReturning("execution(* com.easycerti.eframe.psm..web.*.add*(..)) "
    		+ "or execution(* com.easycerti.eframe.psm..web.*.save*(..)) "
    		+ "or execution(* com.easycerti.eframe.psm..web.*.remove*(..)) ")
	public void logAfterSucces(JoinPoint joinPoint) {
    	logger.info("# LoggingAspect - logAfterSucces");
    	leaveLogAuthAdminUser(getAuthLogParameters("success", null));
	}
    
	@AfterThrowing(pointcut = "execution(* com.easycerti.eframe.psm..web.*.add*(..)) "
    		+ "or execution(* com.easycerti.eframe.psm..web.*.save*(..)) "
    		+ "or execution(* com.easycerti.eframe.psm..web.*.remove*(..)) ", 
    		throwing = "error")
	public void logAfterThrowing(JoinPoint joinPoint, Throwable error) {
		logger.info("# LoggingAspect - logAfterThrowing");
		leaveLogAuthAdminUser(getAuthLogParameters("failed", error));
	}
	
	@SuppressWarnings("unchecked")
	private Map<String, String> getAuthLogParameters(String resultType, Throwable error) {
		HttpServletRequest request = 
				((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		
		StringBuilder stringBuilder = new StringBuilder();
		
		if (resultType.equals(CommonResource.ADMIN_LOG_TYPE_SUCCESS)) {
			String logMessageTitle = request.getParameter("log_message_title");
			String logMessageParams = request.getParameter("log_message_params");
			stringBuilder.append("[SUCCESS - ");
			stringBuilder.append(logMessageTitle);
			stringBuilder.append("]");
			
			if(logMessageParams != null) {
				List<String> keys = Arrays.asList(logMessageParams.split(","));
				
				for (String key : keys) { 
					if (key != null && (!"".equals(key))) {
						stringBuilder.append("/");
						stringBuilder.append(key);
						if (request.getParameter(key) != null && (!"".equals(key))) {
							stringBuilder.append(":");
							stringBuilder.append(request.getParameter(key));
						}
					}
				}
			}
		} else {
			String logMessageTitle;
			if (error != null && error instanceof ESException) {
				logMessageTitle = messageSourceAccessor.getMessage(((ESException)error).getStatusCode());
			} else {
				logMessageTitle = error.getMessage();
			}
			
			stringBuilder.append("[ERROR - ");
			stringBuilder.append(logMessageTitle);
			stringBuilder.append("]");
		}
		
		Map<String, String> parameters = RequestUtil.getParameterMap(request);
		parameters.put("admin_user_id", SystemHelper.getAdminUserIdFromRequest(request));
		parameters.put("ip_address", request.getRemoteAddr());
		parameters.put("log_message", stringBuilder.toString());
		
		return parameters;
	}
	
	private void leaveLogAuthAdminUser(Map<String, String> parameters) {
		ManagerActHist paramBean = CommonHelper.convertMapToBean(parameters, ManagerActHist.class);
		Menu menuParamBean = new Menu();
		menuParamBean.setMenu_id(paramBean.getMenu_id());
		List<Menu> menuList = menuMngtDao.findMenuMngtList_fullPathInfo(menuParamBean);
		
		try {
			paramBean.setLog_auth_id(CommonHelper.getGUID());
			
			// DEPTH : 2 (리스트)
			if(menuList.size() == 2) {
				paramBean.setL_category(menuList.get(0).getMenu_name());
				paramBean.setM_category(menuList.get(1).getMenu_name());
			}
			// DEPTH : 3 (상세), DEPTH : 4 (비정상위험 시나리오 수정)
			else if(menuList.size() == 3 || menuList.size() == 4) {
				paramBean.setL_category(menuList.get(0).getMenu_name());
				paramBean.setM_category(menuList.get(1).getMenu_name());
				paramBean.setS_category(menuList.get(2).getMenu_name());
			}
			
			managerActHistDao.leaveManagerActHistOne(paramBean);
		} catch(DataAccessException e) {
			logger.info("  > failed to leave AuthLog.");
		}
	}
}
