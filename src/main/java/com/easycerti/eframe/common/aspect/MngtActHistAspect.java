package com.easycerti.eframe.common.aspect;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.easycerti.eframe.common.annotation.MngtActHist;
import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.spring.DataModelAndView;
import com.easycerti.eframe.common.util.CommonHelper;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.history_management.dao.ManagerActHistDao;
import com.easycerti.eframe.psm.history_management.vo.ManagerActHist;
import com.easycerti.eframe.psm.system_management.dao.MenuMngtDao;
import com.easycerti.eframe.psm.system_management.vo.Menu;

@Aspect
public class MngtActHistAspect {

private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private ManagerActHistDao managerActHistDao;

	@Autowired
	private MenuMngtDao menuMngtDao;
	
	@Autowired
	private CommonDao commonDao;
	private void MngtActHistAdd(JoinPoint joinPoint, Object obj) {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();
		MngtActHist mngtActHist = method.getAnnotation(MngtActHist.class);
		if(mngtActHist!=null) {
			try {
				RequestMapping requestm = (RequestMapping) joinPoint.getSignature().getDeclaringType().getAnnotation(RequestMapping.class);
				HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
				String url = "";
				
				if(requestm!=null) {url = requestm.value()[0].replace("/*", "");}
				
				RequestMapping requestMapping = method.getAnnotation(RequestMapping.class);
				url += "/"+requestMapping.value()[0];
				
				SimpleCode simpleCode = new SimpleCode(url);
				String index_id = commonDao.checkIndex(simpleCode);
				
				Menu menu = new Menu();
				menu.setMenu_id(index_id);
				List<Menu> menuList = menuMngtDao.findMenuMngtList_fullPathInfo(menu);
				
				ManagerActHist managerActHist = new ManagerActHist();
				managerActHist.setMenu_id(index_id);
				managerActHist.setLog_auth_id(CommonHelper.getGUID());
				try {
					managerActHist.setAdmin_user_id(SystemHelper.findSessionUser(request).getAdmin_user_id());
				} catch (Exception e) {
					System.out.println("로그인정보 없음");
				}
				managerActHist.setIp_address(request.getRemoteAddr());
				managerActHist.setLog_action(mngtActHist.log_action());
				
				
				//shyu
				String l_category = null;
				if(obj instanceof ModelAndView) {
					ModelAndView ModelAndView = (ModelAndView) obj;
					l_category = (String) ModelAndView.getModel().get("l_category");
				}else if(obj instanceof Map) {
					Map map = (Map) obj;
					l_category = (String) map.get("l_category");
				}
				if(l_category == null || l_category.equals("")) {
					l_category = mngtActHist.l_category();
				}
				managerActHist.setL_category(l_category);
				//-shyu
				
				// 3가지의 카테고리중 하나도 값이 안들어있을 경우를 체크하기 위하여, 접근경로 표기를 위함
				boolean categoryCheck = true;	
				if(mngtActHist.l_category() != null && !mngtActHist.l_category().equals("")) {	//어노테이션 l_category에 무언가 값이 있을 경우
					managerActHist.setL_category(mngtActHist.l_category());
					categoryCheck = false;
				}
				if(mngtActHist.m_category() != null && !mngtActHist.m_category().equals("")) {	//어노테이션 m_category에 무언가 값이 있을 경우
					managerActHist.setM_category(mngtActHist.m_category());
					categoryCheck = false;
				}
				if(mngtActHist.s_category() != null && !mngtActHist.s_category().equals("")) {	//어노테이션 s_category에 무언가 값이 있을 경우
					managerActHist.setS_category(mngtActHist.s_category());
					categoryCheck = false;
				}
				
				if(categoryCheck && menuList.size()>0 &&menuList.get(0)!=null) {	//셋다 값이 비어있을 경우  DB에서 가져온 정보 입력
					for (int i = 0; i < menuList.size(); i++) {
						if(i==0) {
							managerActHist.setL_category(menuList.get(i).getMenu_name());
						}else if(i==1) {
							managerActHist.setM_category(menuList.get(i).getMenu_name());
						}else if(i==2) {
							managerActHist.setS_category(menuList.get(i).getMenu_name());
						}
					}
				}
				//요까지
				
				//log_message 로그메시지를 modelandview에 혹은 Map에 log_message 키값으로 입력하였을 경우 우선하여 로그메시지 입력
				//만약 값이 없을경우 어노테이션에 입력된 값을 입력
				String log_message = null;
				if(obj instanceof ModelAndView) {
					ModelAndView ModelAndView = (ModelAndView) obj;
					log_message = (String) ModelAndView.getModel().get("log_message");
				}else if(obj instanceof Map) {
					Map map = (Map) obj;
					log_message = (String) map.get("log_message");
				}
				if(log_message==null || log_message.equals("")){
					log_message = mngtActHist.log_message();
				}
				managerActHist.setLog_message(log_message);
				//요까지
				//l_cateogry 메시지  modelandview에 혹은 Map에 log_message 키값으로 입력하였을 경우 우선하여 로그메시지 입력
				//만약 값이 없을경우 어노테이션에 입력된 값을 입력
				
				
				
				managerActHistDao.leaveManagerActHistOne(managerActHist);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}
}
