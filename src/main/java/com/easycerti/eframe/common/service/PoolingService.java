package com.easycerti.eframe.common.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.easycerti.eframe.psm.search.dao.ExtrtCondbyInqDao;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;
import com.easycerti.ubisafer.psm.alarm.sender.AlarmManager;

@Component
public class PoolingService implements Runnable {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	ExtrtCondbyInqDao extrtCondbyInqDao;
	
	@Override
	public void run() {
		while(true) {
			try {
				List<AdminUser> apprInfo = extrtCondbyInqDao.findDescAppr();
				List<AdminUser> senderInfo = extrtCondbyInqDao.findAlarmSenderInfo();
				String sender = null;
				String senderPhone = null;
				
				for(AdminUser si : senderInfo) {
					switch (si.getParam_name()) {
						case "alarm.sender": sender = si.getParam_value(); break;
						case "alarm.sender.phone": senderPhone = si.getParam_value(); break;
					}
				}
				
				for(AdminUser au : apprInfo) {
					List<AdminUser> summonManager = extrtCondbyInqDao.findSummonManagerList(au);
					String alarmTarget = au.getRequest_target_user_id();
					String alarmTargetPhone = au.getMobile_number();
					for(AdminUser a : summonManager) {
						alarmTarget = alarmTarget +","+a.getEmp_user_id();
						alarmTargetPhone = alarmTargetPhone +","+a.getMobile_number();
					}
					
					
					List<AdminUser> alarmInfo = null;
					String alarmTitle = null;
					String alarmBody = null;
					String alarmUrl = null;
					if(au.getAppr_type().equals("10")) {
						if(au.getAppr_status().equals("03")) {
							alarmInfo = extrtCondbyInqDao.findAlarmRequestInfo();
							for(AdminUser ai : alarmInfo) {
								switch (ai.getParam_name()) {
									case "alarm.request.title": alarmTitle = ai.getParam_value(); break;
									case "alarm.request.body": alarmBody = ai.getParam_value(); break;
									case "alarm.request.url": alarmUrl = ai.getParam_value(); break;
								}
							}
						}
					} else if(au.getAppr_type().equals("20")) {
						//소명답변
						if(au.getAppr_status().equals("03")) {
							//완결 : 판정요청알림(관리자)
							alarmInfo = extrtCondbyInqDao.findAlarmResponseInfo();
							for(AdminUser ai : alarmInfo) {
								switch (ai.getParam_name()) {
									case "alarm.response.title": alarmTitle = ai.getParam_value(); break;
									case "alarm.response.body": alarmBody = ai.getParam_value(); break;
									case "alarm.response.url": alarmUrl = ai.getParam_value(); break;
								}
							}
						} else {
							//회수,반려
							alarmInfo = extrtCondbyInqDao.findAlarmCancelInfo();
							for(AdminUser ai : alarmInfo) {
								switch (ai.getParam_name()) {
									case "alarm.cancel.title": alarmTitle = ai.getParam_value(); break;
									case "alarm.cancel.body": alarmBody = ai.getParam_value(); break;
									case "alarm.cancel.url": alarmUrl = ai.getParam_value(); break;
								}
							}
						}
					}
					
					if(au.getAppr_type().equals("10")) {
						//소명요청
						if(au.getAppr_status().equals("03")) {
							//완결 : 소명응답 알람 및 요청결재중(15)을 소명요청(20)으로 업데이트
							AlarmManager.sendMessage("MESSENGER,SMS", sender, senderPhone, "Z2019043", "01022639658", alarmTitle, alarmBody, alarmUrl);
							//AlarmManager.sendMessage("MESSENGER", sender, senderPhone, alarmTarget, alarmTargetPhone, alarmTitle, alarmBody, alarmUrl);
							
							au.setDesc_status("20");
							extrtCondbyInqDao.updateDescStatus(au);
						} else {
							//회수,반려 : 소명삭제
							extrtCondbyInqDao.removeSummonResult(au);
							
						}
					} else if(au.getAppr_type().equals("20")) {
						//소명답변
						if(au.getAppr_status().equals("03")) {
							//완결 : 판정요청알림(관리자)
							alarmTarget = extrtCondbyInqDao.findSummonTotalUserList();
							
							AlarmManager.sendMessage("MESSENGER,SMS", sender, senderPhone, "Z2019043", "01022639658", alarmTitle, alarmBody, alarmUrl);
							//AlarmManager.sendMessage("MESSENGER", sender, senderPhone, alarmTarget, alarmTargetPhone, alarmTitle, alarmBody, alarmUrl);
							
							au.setDesc_status("40");
							extrtCondbyInqDao.updateDescStatus(au);
						} else {
							//회수,반려
							AlarmManager.sendMessage("MESSENGER,SMS", sender, senderPhone, "Z2019043", "01022639658", alarmTitle, alarmBody, alarmUrl);
							//AlarmManager.sendMessage("MESSENGER", sender, senderPhone, alarmTarget, alarmTargetPhone, alarmTitle, alarmBody, alarmUrl);
							
							au.setDesc_status("20");
							extrtCondbyInqDao.updateDescStatus(au);
							extrtCondbyInqDao.updateDescInfoResponseClear(au);
						}
					}
					
					//체크여부 업데이트
					extrtCondbyInqDao.updateDescApprCheck(au);
				}
				
				Thread.sleep(1000*60);
			} catch (Exception e) {
				logger.error("", e);
			}
		}
	}
	
}
