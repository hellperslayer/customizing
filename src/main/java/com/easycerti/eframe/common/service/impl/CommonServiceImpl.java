package com.easycerti.eframe.common.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.easycerti.eframe.common.dao.CommonDao;
import com.easycerti.eframe.common.service.CommonService;
import com.easycerti.eframe.common.vo.PrivacyType;
import com.easycerti.eframe.common.vo.SimpleCode;

@Service
public class CommonServiceImpl implements CommonService {

	
	@Autowired
	private CommonDao commonDao;

	@Override
	public List<SimpleCode> getAvailableDepartments() {
		return commonDao.getAvailableDepartments();
	}

	@Override
	public List<PrivacyType> getPrivacyTypesExceptRegular() {
		return commonDao.getPrivacyTypesExceptRegular();
	}

	@Override
	public List<SimpleCode> getSystemMaster() {
		return commonDao.getSystemMaster();
	}

	@Override
	public List<Map<String, String>> getAdminUsers() {
		// TODO Auto-generated method stub
		return commonDao.getAdminUsers();
	}
}
