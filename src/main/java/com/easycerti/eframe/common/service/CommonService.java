package com.easycerti.eframe.common.service;

import java.util.List;
import java.util.Map;

import com.easycerti.eframe.common.vo.PrivacyType;
import com.easycerti.eframe.common.vo.SimpleCode;

public interface CommonService {
	public List<SimpleCode> getAvailableDepartments();
	public List<PrivacyType> getPrivacyTypesExceptRegular();
	public List<SimpleCode> getSystemMaster();
	List<Map<String, String>> getAdminUsers();
}
