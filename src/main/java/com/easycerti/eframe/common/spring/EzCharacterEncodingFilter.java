package com.easycerti.eframe.common.spring;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.filter.CharacterEncodingFilter;

public class EzCharacterEncodingFilter extends CharacterEncodingFilter {
	
    @Override
    protected void doFilterInternal(HttpServletRequest request,
            HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

    	if( request.getRequestURI().indexOf("/psm/remote/RemoteSummonReceiver.do") == -1 ) {
    		super.doFilterInternal(request, response, filterChain);
    	}
    	else {
    		super.doFilter(request, response, filterChain);
    	}
    }
}
