package com.easycerti.eframe.common.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.easycerti.eframe.common.vo.PrivacyType;
import com.easycerti.eframe.common.vo.SimpleCode;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.dashboard.vo.Dashboard;
import com.easycerti.eframe.psm.search.vo.AllLogInq;
import com.easycerti.eframe.psm.search.vo.ExtrtCondbyInq;
import com.easycerti.eframe.psm.setup.vo.IndvinfoTypeSetup;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;
import com.easycerti.eframe.psm.system_management.vo.LogoImage;
import com.easycerti.eframe.psm.system_management.vo.Menu;
import com.easycerti.eframe.psm.system_management.vo.SystemMaster;

public interface CommonDao {
	List<SimpleCode> getAvailableDepartments();
	public List<PrivacyType> getPrivacyTypesExceptRegular();
	public List<PrivacyType> getPrivacyTypeExpressRegularUse();
	public List<Code> getResultTypesByCode(); 
	public List<Code> getUitypeByCode(); 
	public List<AllLogInq> getResultTypesByLogSeq(AllLogInq allLoginq); 
	public List<SimpleCode> getSystemMaster();
	List<Map<String, String>> getAdminUsers();
	public String [] getIptables(SimpleCode simpleCode);
	public String [] getIptablesByAdminId(String admin_id);
	public String checkIndex(SimpleCode simpleCode);
	public String checkIndex2(SimpleCode simpleCode);
	//테마선택
	public String getTheme();
	//대시보드 선택
	public String getDashboard();
	
	public void insertImage_logo(HashMap map);
	public void insertImage_logo2(HashMap map);
	public void initImageLogo_use();
	public void initImageLogo_use2();
	public String selectCurrentImage_logo();
	public String selectCurrentImage_logo2();
	public List<LogoImage> getLogoImageList();
	public List<LogoImage> getLogoImageList2();
	public void useLogImage(int idx);
	public void useLogImage2(int idx);
	public void deleteLogoImage(int idx);
	
	public String getMaster();
	public String getSessionTime();
	public String getResultTypeView();
	public String getSidebarView();
	public String getScrnNameView();
	public String getMappingId();
	public String getUiType();
	public String getAccessAuthUse();
	public String getInfoViewerUse();
	
	public String getBmtName();
	public void setBmtName(String bmt_name);
	
	public Code getDashboardScenario();
	public void setDashboardScenario(String scen_seq);
	
	public List<Code> getResultTypeList();
	public List<Code> getPrivacyDescList();
	public List<Code> getResultTypeList_byStudentId();
	
	public String checkValue();
	List<AdminUser> empdetailCheckIs();
	List<Integer> getLogseqs(int empDetailSeq);
	public void updateEmpdetailCheckIs(String empDetailSeq);
	List<Integer> getSummonLogseq(AdminUser adminUser);
	List<AllLogInq> getbizlogSummonLogs(AdminUser adminUser);
	public String checkSummon(ExtrtCondbyInq extrtCondbyinq);
	
	public List<Code> findAgentParamInfo();
	public List<Code> findReqTypeList();
	
	public List<Menu> getInitLoginPage();
	public void createAuth_info_list();
	public void createExtra_biz_log_setup();
	public String checkEmpNameMasking();
	
	public List<SystemMaster> getSystemList(Dashboard dashboard);
	public List<SystemMaster> getSystemMasterList();
	
	public List<IndvinfoTypeSetup> getIndvinfoTypeList();
	
}
