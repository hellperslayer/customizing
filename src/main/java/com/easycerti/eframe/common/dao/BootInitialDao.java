package com.easycerti.eframe.common.dao;

import java.util.Map;

import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.system_management.vo.Menu;
import com.easycerti.eframe.psm.system_management.vo.OptionSetting;

public interface BootInitialDao {
	
	//
	public int checkCountOption();
	public int checkCountUitype(String code_id);
	public int checkMenuByUrl(String url);
	public int checkReportCode(Code code);
	public int checkCodeCount(String string);
	
	public int checkOptionSetting(String option_id);
	public void insertOptionSetting(OptionSetting option);
	public int checkGroupCode(String group_code_id);
	public void insertGroupCode(Code code);
	public int checkCode(String code_id);
	public void insertCode(Code code);
	
	//
	public void insertCommonType();
	public void insertKiramsType();
	public void insertDaejinType();
	public void insertYeojuType();
	public void insertMoefType();
	public void insertBohunType();
	public void insertHapcheonType();
	public void insertKurlyType();
	public void insertGuriType();
	public void insertUitype();

	//
	public int findTable(String table_name);
	public int findColumn(Map<String,String> map);
	public int findSequence(String sequence_name);
	public int findIndex(String index_name);
	
	public int findConstraint(String conname);
	
	public void createTable(Map<String,String> map);
	public void createQuery(Map<String,String> map);
	
	public void addMenu_addSystemMaster(Menu menu);
	
	//
	public int findFunction_insert_biz_log_result();
	public void createFunction_insert_biz_log_result();
	public int findTrigger_insert_biz_log_result();
	public void createTrigger_insert_biz_log_result();
	public void dropTrigger_insert_biz_log_result();
	
	public int findFunction_update_department();
	public void createFunction_update_department();
	public void update_department();
	public int findTrigger_update_department();
	public void createTrigger_update_department();
	
	//테이블 추가
	public void createTable_desc_info();
	public void createTable_desc_log();
	public void createTable_desc_history();
	public void createTable_desc_appr();
	public void createTable_abuse_info();
	public void createTable_threshold_indv();
	public void createTable_threshold_dept();
	public void createTable_inspection_hist();
	public void createTable_biz_log_file();
	public void createTable_biz_log_approval();	
	public void createTable_download_log();
	public void createTable_download_log_result();
	public void createTable_t_dlog_tag();
	public void createTable_summaryBizLog();					//summary_biz_log테이블 생성
	public void createTable_summary_download_log();				//summary_download_log 테이블 생성
	public void createTable_report_management();
	
	public void createTable_summary_statistics();				//통계 데이터 일마감 테이블 (인텔리젼스, xx별 통계 사용)
	public void createTable_summary_statistics_monthly();		//통계 데이터 월마감 테이블 (점검보고서 사용)
	public void renameColumn_summary_statistics();				// type31~33 -> 16~18 변경
	public void renameColumn_summary_statistics_monthly();		// type31~33 -> 16~18 변경
	public void createTable_summary_reqtype();					//수행업무별 일마감 테이블
	public void createTable_summary_reqtype_monthly();			//수행업무별 월마감 테이블
	public void createTable_summary_time();						//proc_time별 일마감 테이블 (24시간 패턴 시 사용)
	public void createTable_summary_time_monthly();				//proc_time별 월마감 테이블 (24시간 패턴 시 사용)
	public void createTable_license_history();					//에이전트동작설정 -> 라이센스 히스토리 테이블
	public void createTable_send_mail_info();					//스케줄러 - 메일연동관련 테이블

	//테이블 명 변경
	public void renameDescViolation();
	public void renameDescRaw();
	public void renameDescResult();
	public void renameDescMemo();
	public void renameDescDecision();
	
	//인덱스 추가
	public void createIndex_unq_idx_download_log_1();			//download_log테이블 unq인덱스 추가

	//시퀀스 추가
	public int getMax_dlog_tag_seq();							//
	public void createSequence_t_dlog_tag_dlog_tag_seq_seq();	//t_dlog_tag테이블 dlog_tag_seq컬럼 시퀀스 추가
	public void initSequence_dlog_tag_seq(int max);				//
	
	public int getMaxTag_seq();
	public void createSequence_t_tag();
	public void initSequence_t_tag(int max);
	
	//컬럼 추가
	public int getCountQuery_id();
	public void addColumnQuery_id();
	public int getCountRd_id();
	public void addColumnRd_id();
	public int getCountRd_name();
	public void addColumnRd_name();
	public int getCountGrid_id();
	public void addColumnGrid_id();
	public int getCountGrid_name();
	public void addColumnGrid_name();
	public int getCountRequest_data();
	public void addColumnRequest_data();
	public void addColumnDetection_src();
	public void addColumnFile_id();
	public void addColumnReq_sql();
	public void addColumnRes_sql();
	public void addColumnImage_type();
	public void addColumnUser_rock();
	public void addColumnSalt_value();
	public void addColumnEmpDetail_follwup();
	public void addColumnDepartment_fullDeptName();
	public void addColumnT_Tag_forcelog_yn();
	public void addColumnT_Tag_forcelog_privacy_type();
	public void addColumnT_Tag_forcelog_desc();
	public void addColumnEmpUser_Desc_access_cd();
	public void addColumnEmpUser_Desc_access_org_cd();
	public void addColumnEmpDetail_Is_summon_yn();
	public void addColumnDownload_log_reason_code();
	public void addColumnOption_setting_auth();
	public void addColumnRuletbl_alarm_yn();
	public void addColumnProc_month();
	public void addColumnReport_type();
	public void addColumnFile_path();
	public void addColumnUse_biz_log_result();
	public void addColumnPeriod_type();
	public void addColumnUserfile_path();
	public void addColumnUse_extract_key();
	public void addColumnSystemMaster_administrator();
	public void addColumnAgentMaster_manager_name();	//manager_name,manager_ip,system_seq 컬럼 추가
	
	//컬럼 사이즈 판별
	public int lengthEmp_user_idBySumary_a03();			//emp_user_id size 판별
	public int lengthEmp_user_idBySumary_b03();
	public int lengthEmp_user_idBySumary_a07();
	public int lengthEmp_user_idBySumary_b07();
	public int lengthEmp_user_idBySumary_biz_log();
		
	//컬럼 속성 변경
	public void alterColumnImageLogo_use();
	public void alterAdminUser_passwordType();
	public void alterEmp_user_idBySumary_a03();			//emp_user_id (16 -> 255)
	public void alterEmp_user_idBySumary_b03();			//emp_user_id (16 -> 255)
	public void alterEmp_user_idBySumary_a07();			//emp_user_id (16 -> 255)
	public void alterEmp_user_idBySumary_b07();			//emp_user_id (16 -> 255)
	public void alterEmp_user_idBySumary_biz_log();		//emp_user_id (16 -> 255)
	
	//보고서 관련 코드 정리
	public void addReportCodeCPO();			//오남용 보고서 추가/수정
	public void addReportCodeAllLog();		//수준진단 보고서 추가/수정
	public void addReportCodeDownload();	//다운로드 보고서 추가/수정
	public void updateReportCode();	// 오남용, 수준진단, 다운로드 보고서만 남도록 update
	
	
	public int findAlarmMail();	//알람메일 관련 옵션 확인
	public void updateHolidayConflict();	//휴일설정 unique 옵션 설정 전 중복데이터 제거
	public void updateRegularConflict();	//개인정보유형 unique 옵션 설정 전 중복데이터 제거
	
	
	//bootinit에 DB수정작업이 많이 늘어남에 따라 버전 확인하여 효율적으로 작업하기 위해 추가
		/**
	 * DB에 저장된 버전과 파라미터로 넘어온 버전 정보를 비교하여 저장된 정보가 파라미터보다 작을경우 true 반환
	 * @param version
	 * @return boolean
	 */
	public boolean versionCheck(String version);	
	/**
	 * DB에 버전 업데이트, DB업데이트 작업 단위 마지막마다 넣어줘야함
	 * @param version
	 */
	public void versionUpdate(String version);
	public void insert_decision_status_second(Map<String, String> value);
	public int findView(String string);
	public void create_v_biz_log_summary(Map<String, String> map);
}
