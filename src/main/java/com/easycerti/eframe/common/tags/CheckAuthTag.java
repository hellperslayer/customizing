package com.easycerti.eframe.common.tags;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import com.easycerti.eframe.common.spring.ApplicationContextManager;
import com.easycerti.eframe.common.util.SystemHelper;
import com.easycerti.eframe.psm.system_management.dao.MenuAuthMngtDao;
import com.easycerti.eframe.psm.system_management.vo.AdminUser;
import com.easycerti.eframe.psm.system_management.vo.MenuAuth;

/**
 * 화면별 권한 체크를 위한 custom tag</br>
 * - 각 화면별로 태그 추가필요 </br>
 * - ex)<xmp><%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>
 *      <ctl:checkMenuAuth menuId="MENU00003"/></xmp>
 *       
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *  
 *
 * </pre>
 */
public class CheckAuthTag extends TagSupport{

	private static final long serialVersionUID = 1L;
	
	// 메뉴 ID
	private String menuId = null;
	
	// html tag
	private String resultTag = null;
	
	private MenuAuthMngtDao menuAuthMngtDao;
	
	public CheckAuthTag(){
		menuAuthMngtDao = ApplicationContextManager.getInstance().getBean(MenuAuthMngtDao.class);
	}
	
	public void setMenuId(String menuId){
		this.menuId = menuId;
	}
	
	@Override
	public int doStartTag() throws JspException {
		HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
		AdminUser adminUser = SystemHelper.findSessionUser(request);
		if(adminUser == null || adminUser.getAdmin_user_id() == null || this.menuId == null){
			failure();
			return super.doStartTag(); 
		}
		
		MenuAuth menuAuthBean = new MenuAuth();
		menuAuthBean.setAdmin_user_id(adminUser.getAdmin_user_id());
		
		// 관리자의 권한이 사용중인지 미사용중인지 확인
		Integer auth_result = menuAuthMngtDao.findMenuAuthMngtOne_toUserAuth(menuAuthBean);
		
		if(auth_result.intValue() > 0) {
			menuAuthBean.setMenu_id(this.menuId);
			
			// 관리자가 해당 메뉴에 대한 권한이 있는지 확인
			Integer result = menuAuthMngtDao.findMenuAuthMngtOne_toUserMenuAuth(menuAuthBean);
			
			if(result.intValue() > 0){
				complete();
			}else{
				failure();
			}
		}
		else{
			failure();
		}
		
		
		return super.doStartTag();
	}
	
	@Override
	public int doEndTag() throws JspException {
		JspWriter writer = pageContext.getOut();
		try {
			writer.print(this.resultTag);
		} catch (IOException e) {
			throw new JspException(e.getMessage());
		}
		return super.doEndTag();
	}

	// 권한있음
	public void complete(){
		this.resultTag = "";
	}
	
	// 권한없음
	public void failure(){
		// 경고 메시지 후 화면처리 script
		this.resultTag = "<script type='text/javascript'>alert('해당 메뉴에 권한이 없습니다.');if(opener){window.close();}else{history.back();}</script>";
	}
}
