package com.easycerti.eframe.common.tags;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.easycerti.eframe.common.spring.ApplicationContextManager;
import com.easycerti.eframe.psm.system_management.dao.MenuMngtDao;
import com.easycerti.eframe.psm.system_management.vo.Menu;

/**
 * 페이지별 상단에 Navigator 생성 </br>
 * - 각 화면별로 태그 추가필요 </br>
 * - ex)<xmp><%@taglib uri="/WEB-INF/tags2/tld2/ctl.tld" prefix="ctl" %>
 *      <ctl:drawNavMenu menuId="MENU00003"/></xmp>
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   
 *
 * </pre>
 */
public class MenuNavigatorTag extends TagSupport {
	
	private static final long serialVersionUID = 1L;

	// 메뉴 ID
	private String menuId = null;
	
	public void setMenuId(String menuId){
		this.menuId = menuId;
	}
	
	private String resultTag = null;
	private MenuMngtDao menuMngtDao;
	
	public MenuNavigatorTag(){
		this.menuMngtDao = ApplicationContextManager.getInstance().getBean(MenuMngtDao.class); 
	}
	
	@Override
	public int doStartTag() throws JspException {
		if(menuMngtDao == null || this.menuId == null){
			failure();
			return super.doStartTag();
		}
		
		
		
		
		Menu menuBean = new Menu(this.menuId);
		List<Menu> navMenus = menuMngtDao.findMenuMngtList_fullPathInfo(menuBean);
		String currentMenuName = "";
		if(navMenus != null && navMenus.size() > 0){
			currentMenuName = draw(navMenus);
		}else{
			failure();
		}
		
		HttpServletRequest request = 
				((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		request.setAttribute("currentMenuName", currentMenuName);
		
		return super.doStartTag();
	}
	
	@Override
	public int doEndTag() throws JspException {
		JspWriter writer = pageContext.getOut();
		try {
			writer.print(this.resultTag);
		} catch (IOException e) {
			throw new JspException(e.getMessage());
		}
		return super.doEndTag();
	}

	// 메뉴 FullPath정보를 가지고 html 구성
	private String draw(List<Menu> navMenus){
		String currentMenuName = "";
		String menuDepth = "";
		StringBuilder navMenuHtmlSb = new StringBuilder();
		navMenuHtmlSb.append("<div id='nav' class='place' style='margin-bottom:10px;'>");
		navMenuHtmlSb.append("<form id='menuNavigatorForm' method='POST'>");
		navMenuHtmlSb.append("<input type='hidden' id='main_menu_id' name='main_menu_id' value='");
		navMenuHtmlSb.append(navMenus.get(0).getMenu_id());
		navMenuHtmlSb.append("' />");
		navMenuHtmlSb.append("<span class='dep1'>");
		for(Menu menu : navMenus){
			if(menuDepth.equals("")) {
				menuDepth += "<span style='margin-left:10px; color:lightgray; font-size:14px;'>"+menu.getMenu_name().replaceAll("<br>", "")+" > ";
			}else {
				menuDepth += " "+menu.getMenu_name()+" >";
			}
			if(this.menuId.equals(menu.getMenu_id())){
/*				navMenuHtmlSb.append("<span class='dep2'><b>");
				navMenuHtmlSb.append(menu.getMenu_name());
				navMenuHtmlSb.append("</b></span>");
*/				currentMenuName ="<span style='font-size:20px;font-weight:300'>"+ menu.getMenu_name()+"</span>";
			}
/*			if(CommonResource.EMPTY_STRING.equals(menu.getMenu_url())){
				navMenuHtmlSb.append(menu.getMenu_name());
				navMenuHtmlSb.append("&nbsp;&gt;");
			}else if(this.menuId.equals(menu.getMenu_id())){
				navMenuHtmlSb.append("<span class='dep2'><b>");
				navMenuHtmlSb.append(menu.getMenu_name());
				navMenuHtmlSb.append("</b></span>");
				currentMenuName = menu.getMenu_name();
			}else{
				navMenuHtmlSb.append("<span>");
				navMenuHtmlSb.append(menu.getMenu_name());
				navMenuHtmlSb.append("</span>");
				navMenuHtmlSb.append("&nbsp;&gt;");
			}
//			}else if(this.menuId.equals(menu.getMenu_id())){
//				navMenuHtmlSb.append("<span>");
//				navMenuHtmlSb.append(menu.getMenu_name());
//				navMenuHtmlSb.append("</span>");
//			}else{
//				navMenuHtmlSb.append("<a onclick='moveMenuNavigator(\"");
//				navMenuHtmlSb.append(pageContext.getServletContext().getContextPath());
//				navMenuHtmlSb.append(menu.getMenu_url());
//				navMenuHtmlSb.append("\");'>");
//				navMenuHtmlSb.append(menu.getMenu_name());
//				navMenuHtmlSb.append("</a>&nbsp;&gt;&nbsp;");
//			}*/
			
		}
		menuDepth = menuDepth.substring(0, menuDepth.length()-1)+"</span>";
		currentMenuName = currentMenuName+menuDepth;
		navMenuHtmlSb.append("</span>");
		
		navMenuHtmlSb.append("</form>");

		navMenuHtmlSb.append("</div>");
		
		complete(navMenuHtmlSb.toString());
		return currentMenuName;
	}
	
	// 완료
	private void complete(String tag){
		this.resultTag = tag;
	}
	
	// 실패
	private void failure(){
		this.resultTag = "";
	}
	
}
