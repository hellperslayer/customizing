package com.easycerti.eframe.common.tags;

import java.io.Writer;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 * 
 * 설명 : null Conversion 클래스생성(커스텀태그)
 * @author tjlee
 * @since 2015. 7. 2.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2015. 7. 2.           tjlee            최초 생성
 *
 * </pre>
 */
public class NullConversion extends SimpleTagSupport{
	
	private String nullCheck;
	
	public String getNullCheck() {
		return nullCheck;
	}

	public void setNullCheck(String nullCheck) {
		this.nullCheck = nullCheck;
	}

	private Writer getWriter() {
        JspWriter out = getJspContext().getOut();
        return out;
    }
	
	@Override
    public void doTag() throws JspException {
		Writer out = getWriter();
		try {
			StringBuilder html = new StringBuilder("");
			if(nullCheck == null || nullCheck.equals("null") || nullCheck.equals("")){
				nullCheck = "-";
			}
			html.append(nullCheck);
			
			out.write(html.toString());
		} catch (java.io.IOException ex) {
			throw new JspException("Error in NullConversion tag", ex);
		}
	}
}
