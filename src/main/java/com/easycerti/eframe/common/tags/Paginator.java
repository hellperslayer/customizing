package com.easycerti.eframe.common.tags;

import java.io.Writer;
import java.text.NumberFormat;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class Paginator extends SimpleTagSupport {
	
	private String uri;
    private int currentPage;
    private int totalRowCount;
    private int rowBlockCount;
    private int pageBlockCount = 10; // 일단 디폴트 10으로 하자
    private int pageSize;
    private String pageName ="";
    
    public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public int getTotalRowCount() {
		return totalRowCount;
	}
	public void setTotalRowCount(int totalRowCount) {
		this.totalRowCount = totalRowCount;
	}
	public int getRowBlockCount() {
		return rowBlockCount;
	}
	public void setRowBlockCount(int rowBlockCount) {
		this.rowBlockCount = rowBlockCount;
	}
	public int getPageBlockCount() {
		return pageBlockCount;
	}
	public void setPageBlockCount(int pageBlockCount) {
		this.pageBlockCount = pageBlockCount;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
    public String getPageName() {
		return pageName;
	}
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	private Writer getWriter() {
        JspWriter out = getJspContext().getOut();
        return out;
    }
 
    @Override
    public void doTag() throws JspException {
        Writer out = getWriter();
 
        int totalPages = (int)Math.ceil(totalRowCount / (double)rowBlockCount);
        boolean lastPage = currentPage == totalPages;
        int pgStart = (currentPage - 1) / pageBlockCount * pageBlockCount + 1;//Math.max(currentPage - pageBlockCount / 2, 1);
        int pgEnd = pgStart + pageBlockCount - 1;
        if (totalPages < pgEnd)
        	pgEnd = totalPages;
        
//        if (pgEnd > totalPages + 1) {
//            int diff = pgEnd - totalPages;
//            pgStart -= diff - 1;
//            if (pgStart < 1)
//                pgStart = 1;
//            pgEnd = totalPages + 1;
//        }
 
        try {
        	StringBuilder html = new StringBuilder("");
        	
        	html.append("<ul class=\"pagination\" align=\"center\">");
        	if (currentPage == 1)
        		html.append("<li><a onclick=\"javascript:alert('현재 첫번째 페이지 입니다.');\">&lt;&lt;</a></li>");
        	else
        		html.append("<li><a onclick=\"javascript:goPage" + pageName + "(1);\">&lt;&lt;</a></li>");
        	
        	if (currentPage == 1)
        		html.append("<li><a onclick=\"javascript:alert('현재 첫번째 페이지 입니다.');\">&lt;</a></li>&nbsp;");
        	else if (currentPage > pageBlockCount)
        		html.append("<li><a onclick=\"javascript:goPage" + pageName + "(" + String.valueOf(pgStart - 10) + ");\">&lt;</a></li>&nbsp;");
        	else
        		html.append("<li><a onclick=\"javascript:goPage" + pageName + "(1);\">&lt;</a></li>&nbsp;");

        	for (int i = pgStart; i <= pgEnd; i++) {
				if (i == currentPage)
					html.append("<li class=\"active\"><a>" + String.valueOf(currentPage) + "</a></li>&nbsp;");
				else
					html.append("<li><a onclick=\"goPage" + pageName + "(" + String.valueOf(i) + ");\">" + String.valueOf(i) + "</a></li>&nbsp;");
			}
        	
        	if (lastPage)
        		html.append("<li><a onclick=\"javascript:alert('현재 마지막 페이지 입니다.');\">&gt;</a></li>&nbsp;");
        	else if (totalPages > pgEnd)
        		html.append("<li><a onclick=\"javascript:goPage" + pageName + "(" + String.valueOf(pgEnd + 1) + ");\">&gt;</a></li>&nbsp;");
        	else
        		html.append("<li><a onclick=\"javascript:goPage" + pageName + "(" + String.valueOf(totalPages) + ");\">&gt;</a></li>&nbsp;");
        	
        	if (lastPage)
        		html.append("<li><a onclick=\"javascript:alert('현재 마지막 페이지 입니다.');\">&gt;&gt;</a></li>&nbsp;");
        	else
        		html.append("<li><a onclick=\"javascript:goPage" + pageName + "(" + String.valueOf(totalPages) + ");\">&gt;&gt;</a></li>&nbsp;");
        	
        	NumberFormat nf = NumberFormat.getInstance(); 
        	String total = nf.format(totalRowCount);
        	html.append("<span style=\"padding: 10px; float: left;\">총 " + total
        			+ " 건 (" + String.valueOf(currentPage) + " / " + String.valueOf(totalPages) 
        			+ " page)</span>&nbsp;");
        	
        	html.append("</ul>");
        	
        	out.write(html.toString());
 
        } catch (java.io.IOException ex) {
            throw new JspException("Error in Paginator tag", ex);
        }
    }
}
