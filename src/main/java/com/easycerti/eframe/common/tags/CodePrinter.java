package com.easycerti.eframe.common.tags;

import java.io.Writer;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import com.easycerti.eframe.common.spring.ApplicationContextManager;
import com.easycerti.eframe.psm.control.vo.Code;
import com.easycerti.eframe.psm.system_management.dao.CodeMngtDao;

/**
 * 
 * 설명 : 코드 정보 자동 변환 태그(커스텀태그)
 * @author ijchoi
 * @since 2020. 3. 17.
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 *  
 *   수정일            수정자           수정내용
 *  --------------    -------------    ----------------------
 *   2020. 3. 17.          ijchoi            최초 생성
 *	
 *	value = code 테이블의 code_id값 입력	(필수값)
 *	groupId = code 테이블의 group_code_id 입력 
 *	codeMap = code_id값을 키값으로 갖는 맵 입력
 *	(맵이나 그룹 아이디 둘중 하나 필수)
 * </pre>
 */
public class CodePrinter extends SimpleTagSupport{
	
	private String value;
	private String groupId;
	private Map<String, String> codeMap;
	
	private CodeMngtDao codeMngtDao;

	public Map<String, String> getCodeMap() {
		return codeMap;
	}

	public void setCodeMap(Map<String, String> codeMap) {
		this.codeMap = codeMap;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	private Writer getWriter() {
        JspWriter out = getJspContext().getOut();
        return out;
    }
	
	@Override
    public void doTag() throws JspException {
		String code_name = "-";
		
		Code code = new Code();
		code.setUse_flag("Y");
		code.setCode_id(value);
		
		if(codeMap == null) {
			codeMngtDao = ApplicationContextManager.getInstance().getBean(CodeMngtDao.class);
			code.setGroup_code_id(groupId);
			code = codeMngtDao.findCodeMngtOne(code);
			if(code!=null) {
				code_name = code.getCode_name();
			}
		}else {
			code_name = codeMap.get(value);
		}
		
		Writer out = getWriter();
		try {
			StringBuilder html = new StringBuilder("");
			html.append(code_name);
			out.write(html.toString());
		} catch (java.io.IOException ex) {
			throw new JspException("Error in NullConversion tag", ex);
		}
	}
}
