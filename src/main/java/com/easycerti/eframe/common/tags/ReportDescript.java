package com.easycerti.eframe.common.tags;

import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import com.easycerti.eframe.common.spring.ApplicationContextManager;
import com.easycerti.eframe.psm.report.dao.ReportDao;

public class ReportDescript  extends SimpleTagSupport {
	private String codeId;
	
	
	
	public String getCodeId() {
		return codeId;
	}

	public void setCodeId(String codeId) {
		this.codeId = codeId;
	}

	private Writer getWriter() {
        JspWriter out = getJspContext().getOut();
        return out;
    }
	
	@Override
    public void doTag() throws JspException {
		ReportDao reportDao = ApplicationContextManager.getInstance().getBean(ReportDao.class);
		String description = "";
		String input_description = "";
		
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("code_id", codeId);
		List<Map<String,String>> list = reportDao.findReportOption(parameters);
		if(list.size()>0) {
			description = "<P CLASS=HStyle5 STYLE='margin-left:39.5pt;text-indent:-21.2pt;line-height:180%;'><SPAN STYLE='font-size:14.0pt;font-family:\"한양신명조,한컴돋움\";line-height:180%'>❍"; 
			description = description+list.get(0).get("description");
			description = description +"</SPAN></P>";
			input_description = list.get(0).get("input_description");
		}
		
		Writer out = getWriter();
		try {
			StringBuilder html = new StringBuilder("");
			if(input_description.equals("true")) {
				html.append(description);
			}
			out.write(html.toString());
		} catch (java.io.IOException ex) {
			throw new JspException("Error in NullConversion tag", ex);
		}
	}
}
