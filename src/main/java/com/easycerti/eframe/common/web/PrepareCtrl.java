package com.easycerti.eframe.common.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PrepareCtrl {
	
	@RequestMapping("/none.html") 
	public ModelAndView preparePage() {
		ModelAndView mav = new ModelAndView("/errors/prepare");
		
		return mav;
	}
}
