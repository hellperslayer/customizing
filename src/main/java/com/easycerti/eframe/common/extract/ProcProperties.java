package com.easycerti.eframe.common.extract;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;


public class ProcProperties {
	static ConcurrentHashMap<String, Properties> propertiesMap = new ConcurrentHashMap<String, Properties>();
	
	static String fileName = (ProcProperties.class.getProtectionDomain().getCodeSource().getLocation().getPath().substring(
							0
							, ProcProperties.class.getProtectionDomain().getCodeSource().getLocation().getPath().lastIndexOf("/"))
							+File.separator
							+ "conf/console.properties").replaceAll("%20", " ").replaceAll("/bin", "");
	
	/**
	 * properties 파일로부터 각종 정보를 읽는다.<br>
	 * 
	 * @param fileName		property 파일 이름
	 * @param key			읽어올 key
	 * @return				주어진 key 해당하는 value
	 */
	public static String readProperty(String key) {
		
		Properties properties = propertiesMap.get(fileName);
		
		if(properties == null){
			synchronized (propertiesMap) {
				properties = propertiesMap.get(fileName);
				if(properties == null){

					// Read properties file.
					try {
						properties = readProperties();
						propertiesMap.put(fileName,properties); 
					} catch (IOException e) {
						throw new IllegalStateException("Could not read properties file " + fileName + 
								" : " + e.getMessage(), e);
					}
				}
			}
		}
		
		String property = properties.getProperty(key);
		return property == null ? null : property.trim();
	}
	
	public static String readSQL(String key){
		StringBuilder sb = new StringBuilder();
		
		String line = null;
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(fileName)));
			while(( line = br.readLine()) != null){
				if(line.equals(key)){
					while(( line = br.readLine()) != null){
						if(line.equals("[SQL END]")){
							break;
						}
						else{
							sb.append(line+"\n");
						}
					}
				}
			}
			
		}catch (Exception e) {
			throw new IllegalStateException("Could not read properties file " + fileName + 
					" : " + e.getMessage(), e);
		}
		
		
		return sb.toString();
	}
	
	public static Properties readProperties() throws IOException {
		
		Properties result = null;
		synchronized (propertiesMap) {
			result = propertiesMap.get(fileName);
			
			if(result != null)
				return result; 

				FileInputStream fis = null;	
				try {
					fis = new FileInputStream(fileName);
					result = new Properties();
					result.load(fis);
				} finally {
					if(fis != null)
						fis.close();		
				}
				
				propertiesMap.put(fileName, result);
		}
		return result;
	}
	
	public static String getPropertiesFilePath() {
		String str = null;

		try {
			str = new File(".").getCanonicalPath();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return str;
	}
}
