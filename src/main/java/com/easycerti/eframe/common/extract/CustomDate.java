package com.easycerti.eframe.common.extract;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class CustomDate {
	
	public static void main (String args[]){
		
	}
	
	/**
	 * 현재 날짜 및 시간을 SimpleDateFormat에 맞춰서 String 형으로 반환
	 * @param SimpleDateFormat
	 * @return
	 */
	public static String getCurrentDate(String SimpleDateFormat){
		java.util.Calendar cal = java.util.Calendar.getInstance();
		java.text.SimpleDateFormat sDateFormat = new java.text.SimpleDateFormat(SimpleDateFormat);
		String getCurrentDate = sDateFormat.format(cal.getTime());
		return getCurrentDate;
	}
	
	public static Date getCurrentDate(){
		java.util.Calendar cal = java.util.Calendar.getInstance();		
		Date getCurrentDate = cal.getTime();
		return getCurrentDate;
	}
	
	/**
	 * String형의 값을 SimpleDateFormat 형식에 맞춰서 Date형으로 변환
	 * @param DateValue
	 * @param SimpleDateFormat
	 * @return
	 * @throws ParseException
	 */
	public static Date StringToDate(String DateValue,String SimpleDateFormat) throws ParseException{		
		Date tmpDate;
		SimpleDateFormat sDateFormat = new SimpleDateFormat(SimpleDateFormat);					
		tmpDate = sDateFormat.parse(DateValue);
		return tmpDate;
	}
	
	/**
	 * Date 형의 값을 SimpleDateFormat 형식에 맞춰서  String형으로 변환
	 * @param DateValue
	 * @param SimpleDateFormat
	 * @return
	 */
	public static String DateToString(Date DateValue,String SimpleDateFormat){		
		SimpleDateFormat sDateFormat = new SimpleDateFormat(SimpleDateFormat);
		String tmpDate = sDateFormat.format(DateValue);		
		return tmpDate;		
	}
	
	/**
	 * 날짜 더하기 함수 : Date 형과 리턴받을 SimpleDateFormat 유형을 설정하고 더할 날짜값을 입력 받는다.
	 * @param currentDate
	 * @param SimpleDateFormat
	 * @param addDay
	 * @return
	 */
	public static String addDay(Date currentDate,String SimpleDateFormat,int addDay){	
		
		Calendar cal = Calendar.getInstance();		
		cal.setTime(currentDate);		
		cal.add(Calendar.DATE, addDay);
		SimpleDateFormat sDateFormat = new SimpleDateFormat(SimpleDateFormat);		
		String tmpDate = sDateFormat.format(cal.getTime());
		return tmpDate;
		
	}
	
	/**
	 * 날짜 더하기 함수 : 문자열 날짜와 리턴받을 SimpleDateFormat 유형을 설정하고 더할 날짜값을 입력 받는다.
	 * @param currentDate
	 * @param SimpleDateFormat
	 * @param addDay
	 * @return
	 */
	public static String addDay(String date,String SimpleDateFormat,int addDay){	
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
		try {
			cal.setTime( simpleDateFormat.parse(date) );
			cal.add(Calendar.DATE, addDay);
		} catch (ParseException e) {
			System.out.println( e.getMessage() );
		}
		return simpleDateFormat.format( cal.getTime() );
	}
	public static String addMonth(String date,String SimpleDateFormat,int addMonth){	
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
		try {
			cal.setTime( simpleDateFormat.parse(date) );
			cal.add(Calendar.MONTH, addMonth);
		} catch (ParseException e) {
			System.out.println( e.getMessage() );
		}
		return simpleDateFormat.format( cal.getTime() );
	}

}
