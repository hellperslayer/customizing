package com.easycerti.eframe.common.extract;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CustomString {
	/**
	 * 특정 문자열을 구분 기호로 분할하여 인자값 반환
	 * @param str		입력문자열
	 * @param regex		구분기호
	 * @param getNum	추출할 index
	 * @return
	 */
	public static boolean isHangul(String str){
		char c;
		boolean bRet =false;
		
		if(str.length() > 0){
			c = str.substring(0, 1).charAt(0); 
			if( c < 0xAC00 || c > 0xD7A3){
				bRet =false;
			}
			else{
				bRet = true;
			}			
		}
		return bRet;
	}
	public static boolean isHangul(char c){
		boolean bRet =false;		
		if( c < 0xAC00 || c > 0xD7A3){
			bRet =false;
		}
		else{
			bRet = true;
		}
		return bRet;
	}
	public static String split(String str, String regex,int getNum){
		
		String arrStr[] = str.split(regex);
		
		if(getNum > arrStr.length-1){
			getNum = arrStr.length-1;			
		}
		else if(getNum < 0){
			getNum = 0;
		}
		
		String tmpStr = arrStr[getNum];
		
		return tmpStr;
		
	}
	
	/**
	 * 특정 문자열을 구분 기호로 분할하여 인자값 반환
	 * @param str		입력문자열
	 * @param regex		구분기호
	 * @param getNum	추출할 index
	 * @param order		정렬방식(asc/desc)
	 * @return
	 */
	public static String split(String str, String regex,int getNum,String order){
		
		String tmpStr;
		String arrStr[] = str.split(regex);
		
		if(getNum > arrStr.length-1){
			getNum = arrStr.length-1;			
		}
		else if(getNum < 0){
			getNum = 0;
		}
		
		if(order.equals("desc")){
			tmpStr = arrStr[arrStr.length-1-getNum];
		}
		else { // asc			
			tmpStr = arrStr[getNum];
		}		
		return tmpStr;
		
	}
	
	/**
	 * Str 문자열이 regex 정규표현식이 맞는지 여부를 검토하여 체크된 결과값을 리턴하는 함수 
	 * @param str	정규표현식에 맞는지 볼 스트링 문자열
	 * @param regex	정규 표현식
	 * @return		정규 표현식 체크 결과
	 */
	public static boolean chkRegex(String str, String regex){
		boolean bRet = false;
		Pattern pattern = Pattern.compile(regex);			
	    Matcher m = pattern.matcher(str);	    
	    if(m.find()){		    	
	    	bRet = true;
	    }
		return bRet;
	}

	public static int StringToInt(String strNum) {
		int retVal = 0;
		if (strNum != null && !strNum.equals("")) {
			retVal = Integer.parseInt(strNum);
		}
		return retVal;
	}
	
	public static String createResultFileName(String scheduleSeq, String agencyCd, String MIDDLE, String LogType, String ScheduleDate){
		return String.format("%s_%s_%s_%s_%s", scheduleSeq, agencyCd, MIDDLE, LogType, ScheduleDate);
	}
	
	public static String findRegex(String str, String pattern, int[] findNum){
		String retStr = "";
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(str);
		if(m.find()){
			for(int i : findNum){
				retStr += m.group(i);
			}
		}
		return retStr;
	}
}
