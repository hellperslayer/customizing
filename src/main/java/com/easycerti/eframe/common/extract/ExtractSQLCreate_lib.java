package com.easycerti.eframe.common.extract;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.easycerti.eframe.common.extract.CustomDate;
import com.easycerti.eframe.common.extract.CustomString;
import com.easycerti.eframe.psm.setup.vo.ExtrtBaseSetup;

public class ExtractSQLCreate_lib {
	
	/**
	 * 날짜 더하기 함수 : Date 형과 리턴받을 SimpleDateFormat 유형을 설정하고 더할 날짜값을 입력 받는다.
	 * @param currentDate
	 * @param SimpleDateFormat
	 * @param addDay
	 * @return
	 */
	public static String addDay(Date currentDate,String SimpleDateFormat,int addDay){	
		
		Calendar cal = Calendar.getInstance();		
		cal.setTime(currentDate);		
		cal.add(Calendar.DATE, addDay);		
		SimpleDateFormat sDateFormat = new SimpleDateFormat(SimpleDateFormat);		
		String tmpDate = sDateFormat.format(cal.getTime());
		return tmpDate;
	}
	
	
	public static String sqlCreator(ExtrtBaseSetup extrtBaseSetup) {
		String sql = extrtBaseSetup.getScript();
		String extractDate = extrtBaseSetup.getSearch_from();
		
		//##EXTRACT_DAY-1## 에 대한 처리
		String pattern = "\\$\\{EXTRACT_DAY([+|-])(\\d*)\\}";
		int[] findNum = {1,2};
		String strFindNum = CustomString.findRegex(sql, pattern, findNum);
		while(!strFindNum.equals("")){
			strFindNum = strFindNum.replaceAll("\\+", "");
			int num = Integer.parseInt( strFindNum );
			
			sql = sql.replaceFirst(pattern, String.format("%s", CustomDate.addDay(extractDate, "yyyyMMdd", num) ) );
			strFindNum = CustomString.findRegex(sql, pattern, findNum);
		}

		sql = sql.replaceAll("\\$\\{TODAY\\}", String.format("= '%s'", new Object[] { extractDate }));
		sql = sql.replaceAll("\\$\\{EXTRACT_FROM_3MONTH\\}", String.format("'%s'", CustomDate.addMonth(extractDate, "yyyyMMdd", -3))); 
		sql = sql.replaceAll("\\$\\{EXTRACT_FROM\\}", String.format("'%s'", CustomDate.addMonth(extractDate, "yyyyMMdd", -1))); 
		sql = sql.replaceAll("\\$\\{EXTRACT_TO\\}", String.format("'%s'", new Object[] { extractDate }));
		sql = sql.replaceAll("\\$\\{EXTRACT_TO_1DAY\\}", String.format("'%s'", CustomDate.addDay(extractDate, "yyyyMMdd", -1)));
		sql = sql.replaceAll("\\$\\{EXTRACT_WEEK_FROM\\}", String.format("'%s'", CustomDate.addDay(extractDate, "yyyyMMdd", -6)));
		sql = sql.replaceAll("\\$\\{EXTRACT_WEEK_TO\\}", String.format("'%s'", new Object[] { extractDate }));

		sql = sql.replaceAll("\\$\\{RULE_CD\\}", String.valueOf(extrtBaseSetup.getRule_seq()));

		sql = sql.replaceAll("\\$\\{LIMIT\\}", String.valueOf(extrtBaseSetup.getLimit_cnt()));
		
		sql = sql.replaceAll("\\$\\{PRIVACY_SEQ\\}", extrtBaseSetup.getPrivacy_seq());
		
		String ref_val = extrtBaseSetup.getRef_val();
		if(ref_val!=null&&!ref_val.equals("")) {
			String[] refValArr = ref_val.split(",");
			for(String a : refValArr) {
				String[] val = a.split("\\|");
				sql = sql.replaceAll("\\$\\{"+val[0]+"\\}", val[1]);
			}
		}
		
		String[] systemSeqArr =  extrtBaseSetup.getSystem_seq().split(",");
		String systemSeq = null;
		for(String a : systemSeqArr) {
			if(systemSeq == null) {
				systemSeq = "'"+a+"'";
			} else {
				systemSeq = systemSeq + "," + "'"+a+"'";
			}
		}
		
		sql = sql.replaceAll("\\$\\{SYSTEM_SEQ\\}", systemSeq);
		
		sql = sql.replaceAll("RULE_EXTRACT_TEMP", "RULE_EXTRACT_TEMP_TEST");
		sql = sql.replaceAll("rule_extract_temp", "rule_extract_temp_test");
		
		String annotaionPattern = "(/\\*)(.*)(\\*/)";
		sql = sql.replaceAll(annotaionPattern, "");
		return sql;
	}
	
	public static String empDetailsqlCreator(ExtrtBaseSetup extrtBaseSetup) {
		String logType = extrtBaseSetup.getLog_delimiter().trim();
		String search_from = extrtBaseSetup.getSearch_from();
	    String log_table = "";
	    if( "DN".equals( logType ) ) {
	    	log_table = "DOWNLOAD_LOG";
	    }
	    else if( "DB".equals( logType ) ) {
	    	log_table = "DBAC_LOG";
	    } else {
	    	log_table = "BIZ_LOG_SUMMARY_" + search_from;
	    }
	    // limit_type 4 or 5
	    int limit_type = extrtBaseSetup.getLimit_type();
	    double limit_type_cnt = extrtBaseSetup.getLimit_type_cnt();
	    if(limit_type==5) {
	    	limit_type_cnt = limit_type_cnt/100;
	    }
		
	    String rule_view_type = extrtBaseSetup.getRule_view_type();
	    String rule_result_type = extrtBaseSetup.getRule_result_type();
	    
	    String sql = "";
	    if(rule_result_type.equals("1")) { // 1:N 일때 
	    	if(limit_type==5) {
	    		sql = "with t as (select b.proc_date, b.dept_id, b.emp_user_id, b.emp_user_name, \n" + 
	    				" b.dept_name, sum(a.rule_cnt) as rule_cnt, b.system_seq, a.limit_cnt, a.result_content \n" + 
	    				"FROM rule_extract_temp_test a, "+log_table+" b\n" + 
	    				"where (b.proc_date, b.log_seq) = (a.occr_dt, a.log_seq) AND b.proc_date = '"+search_from+"' \n" +
	    				"GROUP BY b.proc_date, b.dept_id, b.emp_user_id, b.emp_user_name, b.dept_name, b.system_seq, a.limit_cnt, a.result_content order by rule_cnt desc) \n" +
	    				" insert into simulate_emp_detail (occr_dt, dept_id, emp_user_id, emp_user_name, dept_name, rule_cnt, system_seq, limit_cnt, result_content) \n" +
	    				" select * from t limit (select trunc(count(*)*"+limit_type_cnt+",0) from t) ";
	    	} else if(limit_type==4) {
	    		sql = "insert into simulate_emp_detail (occr_dt, dept_id, emp_user_id, emp_user_name, dept_name, rule_cnt, system_seq, limit_cnt, result_content)" +
	    				"select b.proc_date, b.dept_id, b.emp_user_id, b.emp_user_name, \n" + 
	    				" b.dept_name, sum(a.rule_cnt) as rule_cnt, b.system_seq, a.limit_cnt, a.result_content \n" + 
	    				"FROM rule_extract_temp_test a, "+log_table+" b\n" + 
	    				"where (b.proc_date, b.log_seq) = (a.occr_dt, a.log_seq) AND b.proc_date = '"+search_from+"' \n" +
	    				"GROUP BY b.proc_date, b.dept_id, b.emp_user_id, b.emp_user_name, b.dept_name, b.system_seq, a.limit_cnt, a.result_content \n" +
	    				"order by rule_cnt desc limit "+limit_type_cnt;
	    	} else {
	    		sql = "insert into simulate_emp_detail (occr_dt, dept_id, emp_user_id, emp_user_name, dept_name, rule_cnt, system_seq, limit_cnt, result_content) \n" +
	    				"select b.proc_date, b.dept_id, b.emp_user_id, b.emp_user_name, \n" + 
	    				" b.dept_name, sum(a.rule_cnt) as rule_cnt, b.system_seq, a.limit_cnt, a.result_content \n" + 
	    				"FROM rule_extract_temp_test a, "+log_table+" b\n" + 
	    				"where (b.proc_date, b.log_seq) = (a.occr_dt, a.log_seq) AND b.proc_date = '"+search_from+"' \n" +
	    				"GROUP BY b.proc_date, b.dept_id, b.emp_user_id, b.emp_user_name, b.dept_name, b.system_seq, a.limit_cnt, a.result_content order by rule_cnt desc ";
	    	}
	    } else { // N:1 일 때
			
	    	if(limit_type==5) {
				sql = "with t as ( select '"+search_from+"' as proc_date, 'DEPT99999' as dept_id, 'ZZZZ99999' as emp_user_id, 'ZZZZ99999' as emp_user_name, \n" + 
						" 'DEPT99999' as dept_name, sum(a.rule_cnt) as rule_cnt, '00' as system_seq, a.limit_cnt, a.result_content \n" + 
						"FROM rule_extract_temp_test a \n" + 
						"group by a.rule_cd, a.limit_cnt, a.result_content order by rule_cnt desc ) \n" +
						"insert into simulate_emp_detail (occr_dt, dept_id, emp_user_id, emp_user_name, dept_name, rule_cnt, system_seq, limit_cnt, result_content) \n" +
						"select * from t limit (select trunc(count(*)*"+limit_type_cnt+",0) from t) ";
			} else if(limit_type==4) {
				sql = "insert into simulate_emp_detail (occr_dt, dept_id, emp_user_id, emp_user_name, dept_name, rule_cnt, system_seq, limit_cnt, result_content) \n" +
						" select '"+search_from+"' as proc_date, 'DEPT99999' as dept_id, 'ZZZZ99999' as emp_user_id, 'ZZZZ99999' as emp_user_name, \n" + 
						" 'DEPT99999' as dept_name, sum(a.rule_cnt) as rule_cnt, '00' as system_seq, a.limit_cnt, a.result_content \n" + 
						"FROM rule_extract_temp_test a \n" + 
						"group by a.rule_cd, a.limit_cnt, a.result_content order by rule_cnt desc limit "+limit_type_cnt;
			} else {
				sql = "insert into simulate_emp_detail (occr_dt, dept_id, emp_user_id, emp_user_name, dept_name, rule_cnt, system_seq, limit_cnt, result_content) \n" +
						" select '"+search_from+"' as proc_date, 'DEPT99999' as dept_id, 'ZZZZ99999' as emp_user_id, 'ZZZZ99999' as emp_user_name, \n" + 
						" 'DEPT99999' as dept_name, sum(a.rule_cnt) as rule_cnt, '00' as system_seq, a.limit_cnt, a.result_content \n" + 
						"FROM rule_extract_temp_test a \n" + 
						"group by a.rule_cd, a.limit_cnt, a.result_content order by rule_cnt desc ";
			}
	    	/*
	    	
	    	sql = "insert into simulate_emp_detail (occr_dt, dept_id, emp_user_id, emp_user_name, dept_name, rule_cnt, system_seq, limit_cnt, result_content) \n" +
					" select '"+search_from+"' as proc_date, 'DEPT99999' as dept_id, 'ZZZZ99999' as emp_user_id, 'ZZZZ99999' as emp_user_name, \n" + 
					" 'DEPT99999' as dept_name, sum(a.rule_cnt) as rule_cnt, '00' as system_seq, a.limit_cnt, a.result_content \n" + 
					"FROM rule_extract_temp_test a \n" + 
					"group by a.limit_cnt, a.result_content ";*/
	    }
	    
		return sql;
	}
}




















