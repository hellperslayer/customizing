package com.easycerti.eframe.common.resource;

import java.util.regex.Pattern;

import com.easycerti.eframe.common.vo.CommonResourceBean;

public class CommonResource extends CommonResourceBean {
	public static final String SESSION_ARRTIBUTE_NAME = "userSession";
	public static final String DETAIL_TYPE_ADD = "detailAdd";
	public static final String DETAIL_TYPE_SAVE = "detailSave";
	public static final String DETAIL_TYPE_REMOVE = "detailRemove";
	public static final String EXCEL_DOWNLOAD_VIEW = "excelView";
	public static final String EXCEL_DOWNLOAD_VIEW2 = "excelView2";
	public static final String CSV_DOWNLOAD_VIEW = "csvView";
	public static final String EXCEL_DATATYPE_MAPLIST = "excelMapListType";
	public static final String EXCEL_DATATYPE_LIST = "excelListType";
	public static final String IMAGE_UPLOAD_VIEW = "imageView";
	public static final String FILE_DOWNLOAD_VIEW = "download";
	
	/**
	 * 관리자 감사로그 타입</br>
	 * - 요청
	 */
	public static final String ADMIN_LOG_TYPE_REQUEST = "request";
	
	/**
	 * 관리자 감사로그 타입</br>
	 * - 에러
	 */
	public static final String ADMIN_LOG_TYPE_SUCCESS = "success";
	
	/**
	 * 관리자 감사로그 타입</br>
	 * - 에러
	 */
	public static final String ADMIN_LOG_TYPE_ERROR = "error";
	
	public static final Pattern PATTERN_REQUEST_ACCEPT_JSON_TYPE = Pattern.compile("^.*application/json.*"); 
}