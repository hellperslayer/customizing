
-- Sequence: add_emp_detail_seq

-- DROP SEQUENCE add_emp_detail_seq;

CREATE SEQUENCE add_emp_detail_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 277
  CACHE 1;
ALTER TABLE add_emp_detail_seq
  OWNER TO psm;

-- Sequence: add_url_seq

-- DROP SEQUENCE add_url_seq;

CREATE SEQUENCE add_url_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 12
  CACHE 1;
ALTER TABLE add_url_seq
  OWNER TO psm;

-- Sequence: agent_active_active_id_seq

-- DROP SEQUENCE agent_active_active_id_seq;

CREATE SEQUENCE agent_active_active_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 104
  CACHE 1;
ALTER TABLE agent_active_active_id_seq
  OWNER TO psm;

-- Sequence: biz_log_result_seq

-- DROP SEQUENCE biz_log_result_seq;

CREATE SEQUENCE biz_log_result_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 73970976
  CACHE 1;
ALTER TABLE biz_log_result_seq
  OWNER TO psm;

-- Sequence: biz_log_seq

-- DROP SEQUENCE biz_log_seq;

CREATE SEQUENCE biz_log_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 16429133
  CACHE 1;
ALTER TABLE biz_log_seq
  OWNER TO psm;

-- Sequence: calling_demand_cll_dmnd_id_seq

-- DROP SEQUENCE calling_demand_cll_dmnd_id_seq;

CREATE SEQUENCE calling_demand_cll_dmnd_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 313
  CACHE 1;
ALTER TABLE calling_demand_cll_dmnd_id_seq
  OWNER TO psm;

-- Sequence: calling_judge_cll_jdg_id_seq

-- DROP SEQUENCE calling_judge_cll_jdg_id_seq;

CREATE SEQUENCE calling_judge_cll_jdg_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 82
  CACHE 1;
ALTER TABLE calling_judge_cll_jdg_id_seq
  OWNER TO psm;

-- Sequence: emp_detail_emp_detail_seq_seq

-- DROP SEQUENCE emp_detail_emp_detail_seq_seq;

CREATE SEQUENCE emp_detail_emp_detail_seq_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 124
  CACHE 1;
ALTER TABLE emp_detail_emp_detail_seq_seq
  OWNER TO psm;

-- Sequence: emp_detail_seq

-- DROP SEQUENCE emp_detail_seq;

CREATE SEQUENCE emp_detail_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 93
  CACHE 1;
ALTER TABLE emp_detail_seq
  OWNER TO psm;

-- Sequence: schedule_log_id_seq

-- DROP SEQUENCE schedule_log_id_seq;

CREATE SEQUENCE schedule_log_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 411
  CACHE 1;
ALTER TABLE schedule_log_id_seq
  OWNER TO psm;

-- Sequence: schedule_schid_seq

-- DROP SEQUENCE schedule_schid_seq;

CREATE SEQUENCE schedule_schid_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 4
  CACHE 1;
ALTER TABLE schedule_schid_seq
  OWNER TO psm;


-- Table: add_url

-- DROP TABLE add_url;

CREATE TABLE add_url
(
  seq integer NOT NULL DEFAULT nextval('add_url_seq'::regclass),
  except_url character varying(100) NOT NULL,
  url_desc character varying(100),
  use_yn character varying(2) NOT NULL,
  url_code character varying(2),
  CONSTRAINT add_url_pkey PRIMARY KEY (seq)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE add_url
  OWNER TO psm;

  -- Table: admin_user

-- DROP TABLE admin_user;

CREATE TABLE admin_user
(
  admin_user_id character varying(50) NOT NULL,
  admin_user_name character varying(100) NOT NULL,
  auth_id character varying(30),
  dept_id character varying(30),
  password character varying(50) NOT NULL,
  pwd_check integer NOT NULL DEFAULT 5,
  last_pwd_change_datetime timestamp without time zone,
  last_login_success_datetime timestamp without time zone,
  last_login_fail_datetime timestamp without time zone,
  grade integer NOT NULL,
  email_address character varying(50),
  mobile_number character varying(50),
  del_flag character(1) NOT NULL,
  alarm_flag character(1) NOT NULL DEFAULT 'N'::bpchar,
  description character varying(1000),
  insert_user_id character varying(50),
  insert_datetime timestamp without time zone,
  update_user_id character varying(50),
  update_datetime timestamp without time zone,
  CONSTRAINT admin_user_pkey PRIMARY KEY (admin_user_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE admin_user
  OWNER TO psm;
GRANT SELECT(admin_user_id), UPDATE(admin_user_id), INSERT(admin_user_id), REFERENCES(admin_user_id) ON admin_user TO psm WITH GRANT OPTION;
GRANT SELECT(admin_user_name), UPDATE(admin_user_name), INSERT(admin_user_name), REFERENCES(admin_user_name) ON admin_user TO psm WITH GRANT OPTION;
GRANT SELECT(auth_id), UPDATE(auth_id), INSERT(auth_id), REFERENCES(auth_id) ON admin_user TO psm WITH GRANT OPTION;
GRANT SELECT(dept_id), UPDATE(dept_id), INSERT(dept_id), REFERENCES(dept_id) ON admin_user TO psm WITH GRANT OPTION;
GRANT SELECT(password), UPDATE(password), INSERT(password), REFERENCES(password) ON admin_user TO psm WITH GRANT OPTION;
GRANT SELECT(pwd_check), UPDATE(pwd_check), INSERT(pwd_check), REFERENCES(pwd_check) ON admin_user TO psm WITH GRANT OPTION;
GRANT SELECT(last_pwd_change_datetime), UPDATE(last_pwd_change_datetime), INSERT(last_pwd_change_datetime), REFERENCES(last_pwd_change_datetime) ON admin_user TO psm WITH GRANT OPTION;
GRANT SELECT(last_login_success_datetime), UPDATE(last_login_success_datetime), INSERT(last_login_success_datetime), REFERENCES(last_login_success_datetime) ON admin_user TO psm WITH GRANT OPTION;
GRANT SELECT(last_login_fail_datetime), UPDATE(last_login_fail_datetime), INSERT(last_login_fail_datetime), REFERENCES(last_login_fail_datetime) ON admin_user TO psm WITH GRANT OPTION;
GRANT SELECT(grade), UPDATE(grade), INSERT(grade), REFERENCES(grade) ON admin_user TO psm WITH GRANT OPTION;
GRANT SELECT(email_address), UPDATE(email_address), INSERT(email_address), REFERENCES(email_address) ON admin_user TO psm WITH GRANT OPTION;
GRANT SELECT(mobile_number), UPDATE(mobile_number), INSERT(mobile_number), REFERENCES(mobile_number) ON admin_user TO psm WITH GRANT OPTION;
GRANT SELECT(del_flag), UPDATE(del_flag), INSERT(del_flag), REFERENCES(del_flag) ON admin_user TO psm WITH GRANT OPTION;
GRANT SELECT(alarm_flag), UPDATE(alarm_flag), INSERT(alarm_flag), REFERENCES(alarm_flag) ON admin_user TO psm WITH GRANT OPTION;
GRANT SELECT(description), UPDATE(description), INSERT(description), REFERENCES(description) ON admin_user TO psm WITH GRANT OPTION;
GRANT SELECT(insert_user_id), UPDATE(insert_user_id), INSERT(insert_user_id), REFERENCES(insert_user_id) ON admin_user TO psm WITH GRANT OPTION;
GRANT SELECT(insert_datetime), UPDATE(insert_datetime), INSERT(insert_datetime), REFERENCES(insert_datetime) ON admin_user TO psm WITH GRANT OPTION;
GRANT SELECT(update_user_id), UPDATE(update_user_id), INSERT(update_user_id), REFERENCES(update_user_id) ON admin_user TO psm WITH GRANT OPTION;
GRANT SELECT(update_datetime), UPDATE(update_datetime), INSERT(update_datetime), REFERENCES(update_datetime) ON admin_user TO psm WITH GRANT OPTION;

-- Table: agent_active

-- DROP TABLE agent_active;

CREATE TABLE agent_active
(
  active_id serial NOT NULL,
  agent_seq integer,
  active_time timestamp without time zone,
  active_type integer,
  active_contents character varying(256),
  CONSTRAINT agent_active_pkey PRIMARY KEY (active_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE agent_active
  OWNER TO psm;

-- Table: agent_master

-- DROP TABLE agent_master;

CREATE TABLE agent_master
(
  agent_seq character varying(25) NOT NULL,
  server_seq character varying(25) NOT NULL,
  agent_name character varying(32),
  ip character varying(15),
  port character varying(15),
  description character varying(200),
  agent_type character varying(32),
  CONSTRAINT agent_master_pkey PRIMARY KEY (agent_seq),
  CONSTRAINT r_16 FOREIGN KEY (server_seq)
      REFERENCES server_master (server_seq) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE agent_master
  OWNER TO psm;
GRANT SELECT(agent_seq), UPDATE(agent_seq), INSERT(agent_seq), REFERENCES(agent_seq) ON agent_master TO psm WITH GRANT OPTION;
GRANT SELECT(server_seq), UPDATE(server_seq), INSERT(server_seq), REFERENCES(server_seq) ON agent_master TO psm WITH GRANT OPTION;
GRANT SELECT(agent_name), UPDATE(agent_name), INSERT(agent_name), REFERENCES(agent_name) ON agent_master TO psm WITH GRANT OPTION;
GRANT SELECT(ip), UPDATE(ip), INSERT(ip), REFERENCES(ip) ON agent_master TO psm WITH GRANT OPTION;
GRANT SELECT(port), UPDATE(port), INSERT(port), REFERENCES(port) ON agent_master TO psm WITH GRANT OPTION;
GRANT SELECT(description), UPDATE(description), INSERT(description), REFERENCES(description) ON agent_master TO psm WITH GRANT OPTION;

-- Table: agent_master_hist

-- DROP TABLE agent_master_hist;

CREATE TABLE agent_master_hist
(
  action_log_id character varying(32) NOT NULL,
  agent_seq character varying(25) NOT NULL,
  server_seq character varying(25) NOT NULL,
  command_datetime timestamp without time zone,
  action character varying(10),
  message character varying(10),
  CONSTRAINT agent_master_hist_pkey PRIMARY KEY (action_log_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE agent_master_hist
  OWNER TO psm;
GRANT SELECT(action_log_id), UPDATE(action_log_id), INSERT(action_log_id), REFERENCES(action_log_id) ON agent_master_hist TO psm WITH GRANT OPTION;
GRANT SELECT(agent_seq), UPDATE(agent_seq), INSERT(agent_seq), REFERENCES(agent_seq) ON agent_master_hist TO psm WITH GRANT OPTION;
GRANT SELECT(server_seq), UPDATE(server_seq), INSERT(server_seq), REFERENCES(server_seq) ON agent_master_hist TO psm WITH GRANT OPTION;
GRANT SELECT(command_datetime), UPDATE(command_datetime), INSERT(command_datetime), REFERENCES(command_datetime) ON agent_master_hist TO psm WITH GRANT OPTION;
GRANT SELECT(action), UPDATE(action), INSERT(action), REFERENCES(action) ON agent_master_hist TO psm WITH GRANT OPTION;
GRANT SELECT(message), UPDATE(message), INSERT(message), REFERENCES(message) ON agent_master_hist TO psm WITH GRANT OPTION;

-- Table: agent_status

-- DROP TABLE agent_status;

CREATE TABLE agent_status
(
  agent_seq character varying(25) NOT NULL,
  server_seq integer,
  command_time timestamp without time zone,
  last_updatetime timestamp without time zone,
  active_status integer,
  command integer
)
WITH (
  OIDS=FALSE
);
ALTER TABLE agent_status
  OWNER TO psm;

-- Table: auth

-- DROP TABLE auth;

CREATE TABLE auth
(
  auth_id character varying(30) NOT NULL,
  auth_name character varying(100) NOT NULL,
  use_flag character(1) NOT NULL,
  insert_user_id character varying(50),
  insert_datetime timestamp without time zone,
  update_user_id character varying(50),
  update_datetime timestamp without time zone,
  CONSTRAINT auth_pkey PRIMARY KEY (auth_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE auth
  OWNER TO psm;
GRANT SELECT(auth_id), UPDATE(auth_id), INSERT(auth_id), REFERENCES(auth_id) ON auth TO psm WITH GRANT OPTION;
GRANT SELECT(auth_name), UPDATE(auth_name), INSERT(auth_name), REFERENCES(auth_name) ON auth TO psm WITH GRANT OPTION;
GRANT SELECT(use_flag), UPDATE(use_flag), INSERT(use_flag), REFERENCES(use_flag) ON auth TO psm WITH GRANT OPTION;
GRANT SELECT(insert_user_id), UPDATE(insert_user_id), INSERT(insert_user_id), REFERENCES(insert_user_id) ON auth TO psm WITH GRANT OPTION;
GRANT SELECT(insert_datetime), UPDATE(insert_datetime), INSERT(insert_datetime), REFERENCES(insert_datetime) ON auth TO psm WITH GRANT OPTION;
GRANT SELECT(update_user_id), UPDATE(update_user_id), INSERT(update_user_id), REFERENCES(update_user_id) ON auth TO psm WITH GRANT OPTION;
GRANT SELECT(update_datetime), UPDATE(update_datetime), INSERT(update_datetime), REFERENCES(update_datetime) ON auth TO psm WITH GRANT OPTION;

-- Table: backup_hist

-- DROP TABLE backup_hist;

CREATE TABLE backup_hist
(
  backup_log_id character varying(32) NOT NULL,
  backup_datetime timestamp without time zone NOT NULL,
  backup_agent_id character varying(3),
  backup_type character varying(10) NOT NULL,
  origin_data character varying(256),
  backup_data character varying(256),
  log_size character varying(256),
  system_seq character varying(25),
  CONSTRAINT backup_hist_pkey PRIMARY KEY (backup_log_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE backup_hist
  OWNER TO psm;

-- Table: biz_log

-- DROP TABLE biz_log;

CREATE TABLE biz_log
(
  log_seq integer NOT NULL,
  proc_date character varying(8) NOT NULL,
  proc_time character varying(6) NOT NULL,
  emp_user_id character varying(50),
  emp_user_name character varying(50),
  dept_id character varying(50),
  dept_name character varying(50),
  user_ip character varying(15),
  user_id character varying(50),
  scrn_id character varying(50),
  scrn_name character varying(50),
  req_type character varying(5),
  system_seq character varying(25),
  server_seq character varying(25),
  req_context character varying(128),
  req_url character varying(2000),
  req_end_time character varying(16),
  result_type character varying(30),
  CONSTRAINT biz_log_pkey PRIMARY KEY (log_seq)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE biz_log
  OWNER TO psm;
GRANT SELECT(log_seq), UPDATE(log_seq), INSERT(log_seq), REFERENCES(log_seq) ON biz_log TO psm WITH GRANT OPTION;
GRANT SELECT(proc_date), UPDATE(proc_date), INSERT(proc_date), REFERENCES(proc_date) ON biz_log TO psm WITH GRANT OPTION;
GRANT SELECT(proc_time), UPDATE(proc_time), INSERT(proc_time), REFERENCES(proc_time) ON biz_log TO psm WITH GRANT OPTION;
GRANT SELECT(emp_user_id), UPDATE(emp_user_id), INSERT(emp_user_id), REFERENCES(emp_user_id) ON biz_log TO psm WITH GRANT OPTION;
GRANT SELECT(emp_user_name), UPDATE(emp_user_name), INSERT(emp_user_name), REFERENCES(emp_user_name) ON biz_log TO psm WITH GRANT OPTION;
GRANT SELECT(dept_id), UPDATE(dept_id), INSERT(dept_id), REFERENCES(dept_id) ON biz_log TO psm WITH GRANT OPTION;
GRANT SELECT(dept_name), UPDATE(dept_name), INSERT(dept_name), REFERENCES(dept_name) ON biz_log TO psm WITH GRANT OPTION;
GRANT SELECT(user_ip), UPDATE(user_ip), INSERT(user_ip), REFERENCES(user_ip) ON biz_log TO psm WITH GRANT OPTION;
GRANT SELECT(user_id), UPDATE(user_id), INSERT(user_id), REFERENCES(user_id) ON biz_log TO psm WITH GRANT OPTION;
GRANT SELECT(scrn_id), UPDATE(scrn_id), INSERT(scrn_id), REFERENCES(scrn_id) ON biz_log TO psm WITH GRANT OPTION;
GRANT SELECT(scrn_name), UPDATE(scrn_name), INSERT(scrn_name), REFERENCES(scrn_name) ON biz_log TO psm WITH GRANT OPTION;
GRANT SELECT(req_type), UPDATE(req_type), INSERT(req_type), REFERENCES(req_type) ON biz_log TO psm WITH GRANT OPTION;
GRANT SELECT(system_seq), UPDATE(system_seq), INSERT(system_seq), REFERENCES(system_seq) ON biz_log TO psm WITH GRANT OPTION;
GRANT SELECT(server_seq), UPDATE(server_seq), INSERT(server_seq), REFERENCES(server_seq) ON biz_log TO psm WITH GRANT OPTION;
GRANT SELECT(req_context), UPDATE(req_context), INSERT(req_context), REFERENCES(req_context) ON biz_log TO psm WITH GRANT OPTION;
GRANT SELECT(req_url), UPDATE(req_url), INSERT(req_url), REFERENCES(req_url) ON biz_log TO psm WITH GRANT OPTION;
GRANT SELECT(req_end_time), UPDATE(req_end_time), INSERT(req_end_time), REFERENCES(req_end_time) ON biz_log TO psm WITH GRANT OPTION;
GRANT SELECT(result_type), UPDATE(result_type), INSERT(result_type), REFERENCES(result_type) ON biz_log TO psm WITH GRANT OPTION;


-- Index: biz_log_emp_user_id_key

-- DROP INDEX biz_log_emp_user_id_key;

CREATE INDEX biz_log_emp_user_id_key
  ON biz_log
  USING btree
  (emp_user_id COLLATE pg_catalog."default");

-- Index: biz_log_proc_date_key

-- DROP INDEX biz_log_proc_date_key;

CREATE INDEX biz_log_proc_date_key
  ON biz_log
  USING btree
  (proc_date COLLATE pg_catalog."default" DESC NULLS LAST);
ALTER TABLE biz_log CLUSTER ON biz_log_proc_date_key;

-- Index: biz_log_proc_time_key

-- DROP INDEX biz_log_proc_time_key;

CREATE INDEX biz_log_proc_time_key
  ON biz_log
  USING btree
  (proc_time COLLATE pg_catalog."default" DESC NULLS LAST);

-- Index: biz_log_user_id_key

-- DROP INDEX biz_log_user_id_key;

CREATE INDEX biz_log_user_id_key
  ON biz_log
  USING btree
  (user_id COLLATE pg_catalog."default");

-- Index: biz_log_user_ip_key

-- DROP INDEX biz_log_user_ip_key;

CREATE INDEX biz_log_user_ip_key
  ON biz_log
  USING btree
  (user_ip COLLATE pg_catalog."default");

-- Table: biz_log_extract_temp

-- DROP TABLE biz_log_extract_temp;

CREATE TABLE biz_log_extract_temp
(
  log_seq integer,
  proc_date character varying(8),
  proc_time character varying(6),
  emp_user_id character varying(50),
  emp_user_name character varying(50),
  dept_id character varying(50),
  dept_name character varying(50),
  user_ip character varying(15),
  user_id character varying(50),
  scrn_id character varying(50),
  scrn_name character varying(50),
  req_type character varying(2),
  system_seq character varying(25),
  server_seq character varying(25),
  req_context character varying(128),
  req_url character varying(2000),
  req_end_time character varying(16),
  result_type character varying(30)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE biz_log_extract_temp
  OWNER TO psm;

-- Table: biz_log_extract_temp_rt

-- DROP TABLE biz_log_extract_temp_rt;

CREATE TABLE biz_log_extract_temp_rt
(
  log_seq integer,
  proc_date character varying(8),
  proc_time character varying(6),
  emp_user_id character varying(50),
  emp_user_name character varying(50),
  dept_id character varying(50),
  dept_name character varying(50),
  user_ip character varying(15),
  user_id character varying(50),
  scrn_id character varying(50),
  scrn_name character varying(50),
  req_type character varying(2),
  system_seq character varying(25),
  server_seq character varying(25),
  req_context character varying(128),
  req_url character varying(2000),
  req_end_time character varying(16),
  result_type character varying(30)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE biz_log_extract_temp_rt
  OWNER TO psm;

-- Table: biz_log_result

-- DROP TABLE biz_log_result;

CREATE TABLE biz_log_result
(
  biz_log_result_seq integer NOT NULL,
  log_seq integer NOT NULL,
  proc_date character varying(8) NOT NULL,
  result_type character varying(2),
  result_content character varying(256),
  CONSTRAINT biz_log_result_pkey PRIMARY KEY (biz_log_result_seq)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE biz_log_result
  OWNER TO psm;

-- Index: biz_log_result_result_type_key

-- DROP INDEX biz_log_result_result_type_key;

CREATE INDEX biz_log_result_result_type_key
  ON biz_log_result
  USING btree
  (result_type COLLATE pg_catalog."default");

-- Index: idx_biz_log_result_proc_date

-- DROP INDEX idx_biz_log_result_proc_date;

CREATE INDEX idx_biz_log_result_proc_date
  ON biz_log_result
  USING btree
  (proc_date COLLATE pg_catalog."default");

-- Index: idx_log_seq

-- DROP INDEX idx_log_seq;

CREATE INDEX idx_log_seq
  ON biz_log_result
  USING btree
  (log_seq);

-- Table: calling_approval

-- DROP TABLE calling_approval;

CREATE TABLE calling_approval
(
  cll_dmnd_id integer NOT NULL,
  approval_seq smallint NOT NULL,
  approver_id character varying(50),
  approver_dept_id character varying(30),
  is_approved character(1) DEFAULT 'N'::bpchar,
  inserter_id character varying(50),
  inserted_date timestamp without time zone,
  updater_id character varying(50),
  updated_date timestamp without time zone,
  CONSTRAINT calling_approval_pkey PRIMARY KEY (cll_dmnd_id, approval_seq)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE calling_approval
  OWNER TO psm;

-- Table: calling_biz_log

-- DROP TABLE calling_biz_log;

CREATE TABLE calling_biz_log
(
  log_seq integer NOT NULL,
  proc_date character varying(8) NOT NULL,
  proc_time character varying(6) NOT NULL,
  user_ip character varying(15),
  scrn_id character varying(50),
  scrn_name character varying(50),
  req_type character varying(2),
  system_seq character varying(25),
  server_seq character varying(25),
  req_context character varying(128),
  req_url character varying(2000),
  req_end_time character varying(16),
  inserter_id character varying(50),
  inserted_date timestamp without time zone,
  CONSTRAINT calling_biz_log_pkey PRIMARY KEY (log_seq)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE calling_biz_log
  OWNER TO psm;

-- Table: calling_biz_log_result

-- DROP TABLE calling_biz_log_result;

CREATE TABLE calling_biz_log_result
(
  biz_log_result_seq integer NOT NULL,
  log_seq integer NOT NULL,
  result_type character varying(30) NOT NULL,
  result_content character varying(256),
  inserted_date timestamp without time zone,
  CONSTRAINT calling_biz_log_result_pkey PRIMARY KEY (biz_log_result_seq)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE calling_biz_log_result
  OWNER TO psm;

-- Index: calling_biz_log_result_log_seq_key

-- DROP INDEX calling_biz_log_result_log_seq_key;

CREATE INDEX calling_biz_log_result_log_seq_key
  ON calling_biz_log_result
  USING btree
  (log_seq);

-- Table: calling_demand

-- DROP TABLE calling_demand;

CREATE TABLE calling_demand
(
  cll_dmnd_id serial NOT NULL,
  repeat_cnt smallint NOT NULL,
  emp_user_id character varying(50) NOT NULL,
  emp_user_name character varying(50),
  emp_ip character varying(15),
  emp_dept_id character varying(50),
  emp_dept_name character varying(50),
  use_approval character(1),
  status character(1),
  inserter_id character varying(50),
  inserter_dept_id character varying(30),
  inserted_date timestamp without time zone,
  updater_id character varying(50),
  updated_date timestamp without time zone,
  CONSTRAINT calling_demand_pkey PRIMARY KEY (cll_dmnd_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE calling_demand
  OWNER TO psm;

-- Table: calling_demand_reason

-- DROP TABLE calling_demand_reason;

CREATE TABLE calling_demand_reason
(
  cll_dmnd_id integer NOT NULL,
  repeat_cnt smallint NOT NULL,
  reason character varying(4000),
  inserter_id character varying(50),
  inserter_dept_id character varying(30),
  inserted_date timestamp without time zone,
  updater_id character varying(30),
  updated_date timestamp without time zone,
  CONSTRAINT calling_demand_reason_pkey PRIMARY KEY (cll_dmnd_id, repeat_cnt)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE calling_demand_reason
  OWNER TO psm;

-- Table: calling_judge

-- DROP TABLE calling_judge;

CREATE TABLE calling_judge
(
  cll_jdg_id serial NOT NULL,
  cll_dmnd_id integer NOT NULL,
  repeat_cnt smallint NOT NULL,
  approval_seq smallint NOT NULL,
  judge character varying(4000),
  judge_result character(1),
  inserter_id character varying(50),
  inserter_dept_id character varying(30),
  inserted_date timestamp without time zone,
  updater_id character varying(30),
  updated_date timestamp without time zone,
  CONSTRAINT calling_judge_pkey PRIMARY KEY (cll_jdg_id),
  CONSTRAINT calling_judge_cll_dmnd_id_repeat_cnt_approval_seq_key UNIQUE (cll_dmnd_id, repeat_cnt, approval_seq)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE calling_judge
  OWNER TO psm;

-- Table: calling_reply

-- DROP TABLE calling_reply;

CREATE TABLE calling_reply
(
  cll_dmnd_id integer NOT NULL,
  repeat_cnt smallint NOT NULL,
  reply character varying(4000),
  inserter_id character varying(50),
  inserter_dept_id character varying(30),
  inserted_date timestamp without time zone,
  updater_id character varying(30),
  updated_date timestamp without time zone,
  CONSTRAINT calling_reply_pkey PRIMARY KEY (cll_dmnd_id, repeat_cnt)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE calling_reply
  OWNER TO psm;

-- Table: calling_rule_biz

-- DROP TABLE calling_rule_biz;

CREATE TABLE calling_rule_biz
(
  emp_detail_seq integer NOT NULL,
  log_seq integer NOT NULL,
  inserted_date timestamp without time zone,
  CONSTRAINT calling_rule_biz_pkey PRIMARY KEY (emp_detail_seq, log_seq)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE calling_rule_biz
  OWNER TO psm;

-- Table: danger_status

-- DROP TABLE danger_status;

CREATE TABLE danger_status
(
  dng_cd character varying(2) NOT NULL,
  dng_val integer NOT NULL,
  CONSTRAINT danger_status_pkey PRIMARY KEY (dng_cd)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE danger_status
  OWNER TO psm;

-- Table: dbac_log

-- DROP TABLE dbac_log;

CREATE TABLE dbac_log
(
  log_seq integer NOT NULL,
  proc_date character varying(8) NOT NULL,
  proc_time character varying(6) NOT NULL,
  emp_user_id character varying(16),
  emp_user_name character varying(50),
  dept_id character varying(50),
  dept_name character varying(50),
  user_ip character varying(16),
  user_id character varying(50),
  scrn_id character varying(50),
  scrn_name character varying(50),
  req_type character varying(2),
  system_seq character varying(25),
  server_seq character varying(25),
  req_context character varying(128),
  req_url character varying(2000),
  req_end_time character varying(16),
  result_type character varying(30),
  CONSTRAINT dbac_log_pkey PRIMARY KEY (proc_date, proc_time, log_seq)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE dbac_log
  OWNER TO psm;
GRANT SELECT(log_seq), UPDATE(log_seq), INSERT(log_seq), REFERENCES(log_seq) ON dbac_log TO psm WITH GRANT OPTION;
GRANT SELECT(proc_date), UPDATE(proc_date), INSERT(proc_date), REFERENCES(proc_date) ON dbac_log TO psm WITH GRANT OPTION;
GRANT SELECT(proc_time), UPDATE(proc_time), INSERT(proc_time), REFERENCES(proc_time) ON dbac_log TO psm WITH GRANT OPTION;
GRANT SELECT(emp_user_id), UPDATE(emp_user_id), INSERT(emp_user_id), REFERENCES(emp_user_id) ON dbac_log TO psm WITH GRANT OPTION;
GRANT SELECT(emp_user_name), UPDATE(emp_user_name), INSERT(emp_user_name), REFERENCES(emp_user_name) ON dbac_log TO psm WITH GRANT OPTION;
GRANT SELECT(dept_id), UPDATE(dept_id), INSERT(dept_id), REFERENCES(dept_id) ON dbac_log TO psm WITH GRANT OPTION;
GRANT SELECT(dept_name), UPDATE(dept_name), INSERT(dept_name), REFERENCES(dept_name) ON dbac_log TO psm WITH GRANT OPTION;
GRANT SELECT(user_ip), UPDATE(user_ip), INSERT(user_ip), REFERENCES(user_ip) ON dbac_log TO psm WITH GRANT OPTION;
GRANT SELECT(user_id), UPDATE(user_id), INSERT(user_id), REFERENCES(user_id) ON dbac_log TO psm WITH GRANT OPTION;
GRANT SELECT(scrn_id), UPDATE(scrn_id), INSERT(scrn_id), REFERENCES(scrn_id) ON dbac_log TO psm WITH GRANT OPTION;
GRANT SELECT(scrn_name), UPDATE(scrn_name), INSERT(scrn_name), REFERENCES(scrn_name) ON dbac_log TO psm WITH GRANT OPTION;
GRANT SELECT(req_type), UPDATE(req_type), INSERT(req_type), REFERENCES(req_type) ON dbac_log TO psm WITH GRANT OPTION;
GRANT SELECT(system_seq), UPDATE(system_seq), INSERT(system_seq), REFERENCES(system_seq) ON dbac_log TO psm WITH GRANT OPTION;
GRANT SELECT(server_seq), UPDATE(server_seq), INSERT(server_seq), REFERENCES(server_seq) ON dbac_log TO psm WITH GRANT OPTION;
GRANT SELECT(req_context), UPDATE(req_context), INSERT(req_context), REFERENCES(req_context) ON dbac_log TO psm WITH GRANT OPTION;
GRANT SELECT(req_url), UPDATE(req_url), INSERT(req_url), REFERENCES(req_url) ON dbac_log TO psm WITH GRANT OPTION;
GRANT SELECT(req_end_time), UPDATE(req_end_time), INSERT(req_end_time), REFERENCES(req_end_time) ON dbac_log TO psm WITH GRANT OPTION;
GRANT SELECT(result_type), UPDATE(result_type), INSERT(result_type), REFERENCES(result_type) ON dbac_log TO psm WITH GRANT OPTION;

-- Table: dbac_log_result

-- DROP TABLE dbac_log_result;

CREATE TABLE dbac_log_result
(
  log_result_seq integer NOT NULL,
  log_sql_seq integer NOT NULL,
  log_seq integer NOT NULL,
  proc_date character varying(8) NOT NULL,
  result_type character varying(4),
  result_content character varying(256)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE dbac_log_result
  OWNER TO psm;
GRANT SELECT(log_result_seq), UPDATE(log_result_seq), INSERT(log_result_seq), REFERENCES(log_result_seq) ON dbac_log_result TO psm WITH GRANT OPTION;
GRANT SELECT(log_sql_seq), UPDATE(log_sql_seq), INSERT(log_sql_seq), REFERENCES(log_sql_seq) ON dbac_log_result TO psm WITH GRANT OPTION;
GRANT SELECT(log_seq), UPDATE(log_seq), INSERT(log_seq), REFERENCES(log_seq) ON dbac_log_result TO psm WITH GRANT OPTION;
GRANT SELECT(proc_date), UPDATE(proc_date), INSERT(proc_date), REFERENCES(proc_date) ON dbac_log_result TO psm WITH GRANT OPTION;
GRANT SELECT(result_type), UPDATE(result_type), INSERT(result_type), REFERENCES(result_type) ON dbac_log_result TO psm WITH GRANT OPTION;
GRANT SELECT(result_content), UPDATE(result_content), INSERT(result_content), REFERENCES(result_content) ON dbac_log_result TO psm WITH GRANT OPTION;

-- Table: dbac_log_sql

-- DROP TABLE dbac_log_sql;

CREATE TABLE dbac_log_sql
(
  log_sql_seq integer NOT NULL,
  proc_date character varying(10) NOT NULL,
  log_seq integer NOT NULL,
  reqsql character varying(80000),
  result_cut_flag character varying(25),
  result_cut_count integer,
  result_total_count integer,
  proc_time character varying(6) NOT NULL,
  CONSTRAINT dbac_log_sql_pkey PRIMARY KEY (proc_date, proc_time, log_seq, log_sql_seq)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE dbac_log_sql
  OWNER TO psm;
GRANT SELECT(log_sql_seq), UPDATE(log_sql_seq), INSERT(log_sql_seq), REFERENCES(log_sql_seq) ON dbac_log_sql TO psm WITH GRANT OPTION;
GRANT SELECT(proc_date), UPDATE(proc_date), INSERT(proc_date), REFERENCES(proc_date) ON dbac_log_sql TO psm WITH GRANT OPTION;
GRANT SELECT(log_seq), UPDATE(log_seq), INSERT(log_seq), REFERENCES(log_seq) ON dbac_log_sql TO psm WITH GRANT OPTION;
GRANT SELECT(reqsql), UPDATE(reqsql), INSERT(reqsql), REFERENCES(reqsql) ON dbac_log_sql TO psm WITH GRANT OPTION;
GRANT SELECT(result_cut_flag), UPDATE(result_cut_flag), INSERT(result_cut_flag), REFERENCES(result_cut_flag) ON dbac_log_sql TO psm WITH GRANT OPTION;
GRANT SELECT(result_cut_count), UPDATE(result_cut_count), INSERT(result_cut_count), REFERENCES(result_cut_count) ON dbac_log_sql TO psm WITH GRANT OPTION;
GRANT SELECT(result_total_count), UPDATE(result_total_count), INSERT(result_total_count), REFERENCES(result_total_count) ON dbac_log_sql TO psm WITH GRANT OPTION;
GRANT SELECT(proc_time), UPDATE(proc_time), INSERT(proc_time), REFERENCES(proc_time) ON dbac_log_sql TO psm WITH GRANT OPTION;

-- Table: department

-- DROP TABLE department;

CREATE TABLE department
(
  dept_id character varying(30) NOT NULL,
  parent_dept_id character varying(30) NOT NULL,
  dept_name character varying(100) NOT NULL,
  use_flag character(1) NOT NULL,
  sort_order integer,
  dept_depth integer,
  description character varying(1000),
  insert_user_id character varying(50),
  insert_datetime timestamp without time zone,
  update_user_id character varying(50),
  update_datetime timestamp without time zone,
  CONSTRAINT department_pkey PRIMARY KEY (dept_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE department
  OWNER TO psm;
GRANT SELECT(dept_id), UPDATE(dept_id), INSERT(dept_id), REFERENCES(dept_id) ON department TO postgres WITH GRANT OPTION;
GRANT SELECT(parent_dept_id), UPDATE(parent_dept_id), INSERT(parent_dept_id), REFERENCES(parent_dept_id) ON department TO postgres WITH GRANT OPTION;
GRANT SELECT(dept_name), UPDATE(dept_name), INSERT(dept_name), REFERENCES(dept_name) ON department TO postgres WITH GRANT OPTION;
GRANT SELECT(use_flag), UPDATE(use_flag), INSERT(use_flag), REFERENCES(use_flag) ON department TO postgres WITH GRANT OPTION;
GRANT SELECT(sort_order), UPDATE(sort_order), INSERT(sort_order), REFERENCES(sort_order) ON department TO postgres WITH GRANT OPTION;
GRANT SELECT(dept_depth), UPDATE(dept_depth), INSERT(dept_depth), REFERENCES(dept_depth) ON department TO postgres WITH GRANT OPTION;
GRANT SELECT(description), UPDATE(description), INSERT(description), REFERENCES(description) ON department TO postgres WITH GRANT OPTION;
GRANT SELECT(insert_user_id), UPDATE(insert_user_id), INSERT(insert_user_id), REFERENCES(insert_user_id) ON department TO postgres WITH GRANT OPTION;
GRANT SELECT(insert_datetime), UPDATE(insert_datetime), INSERT(insert_datetime), REFERENCES(insert_datetime) ON department TO postgres WITH GRANT OPTION;
GRANT SELECT(update_user_id), UPDATE(update_user_id), INSERT(update_user_id), REFERENCES(update_user_id) ON department TO postgres WITH GRANT OPTION;
GRANT SELECT(update_datetime), UPDATE(update_datetime), INSERT(update_datetime), REFERENCES(update_datetime) ON department TO postgres WITH GRANT OPTION;

-- Table: emp_detail

-- DROP TABLE emp_detail;

CREATE TABLE emp_detail
(
  emp_detail_seq serial NOT NULL,
  occr_dt character varying(8) NOT NULL,
  dept_id character varying(10) NOT NULL,
  emp_user_id character varying(16) NOT NULL,
  rule_cd integer NOT NULL,
  rule_cnt integer NOT NULL,
  dng_val numeric(7,2) NOT NULL,
  dept_name character varying(40) NOT NULL,
  emp_user_name character varying(50) NOT NULL,
  rule_nm character varying(40) NOT NULL,
  log_delimiter character varying(10) NOT NULL,
  cll_dmnd_id integer DEFAULT 0,
  CONSTRAINT emp_detail_pkey PRIMARY KEY (emp_detail_seq)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE emp_detail
  OWNER TO psm;

-- Table: emp_user

-- DROP TABLE emp_user;

CREATE TABLE emp_user
(
  emp_user_id character varying(50) NOT NULL,
  system_seq character varying(25) NOT NULL,
  emp_user_name character varying(100),
  dept_id character varying(30),
  email_address character varying(50),
  mobile_number character varying(50),
  ip character varying(15),
  user_id character varying(50),
  emp_user_login_pw character varying(50),
  status character(1) NOT NULL,
  insert_user_id character varying(50),
  insert_datetime timestamp without time zone,
  update_user_id character varying(50),
  update_datetime timestamp without time zone,
  CONSTRAINT emp_user_pkey1 PRIMARY KEY (emp_user_id, system_seq)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE emp_user
  OWNER TO psm;
GRANT SELECT(emp_user_id), UPDATE(emp_user_id), INSERT(emp_user_id), REFERENCES(emp_user_id) ON emp_user TO psm WITH GRANT OPTION;
GRANT SELECT(system_seq), UPDATE(system_seq), INSERT(system_seq), REFERENCES(system_seq) ON emp_user TO psm WITH GRANT OPTION;
GRANT SELECT(emp_user_name), UPDATE(emp_user_name), INSERT(emp_user_name), REFERENCES(emp_user_name) ON emp_user TO psm WITH GRANT OPTION;
GRANT SELECT(dept_id), UPDATE(dept_id), INSERT(dept_id), REFERENCES(dept_id) ON emp_user TO psm WITH GRANT OPTION;
GRANT SELECT(email_address), UPDATE(email_address), INSERT(email_address), REFERENCES(email_address) ON emp_user TO psm WITH GRANT OPTION;
GRANT SELECT(mobile_number), UPDATE(mobile_number), INSERT(mobile_number), REFERENCES(mobile_number) ON emp_user TO psm WITH GRANT OPTION;
GRANT SELECT(ip), UPDATE(ip), INSERT(ip), REFERENCES(ip) ON emp_user TO psm WITH GRANT OPTION;
GRANT SELECT(user_id), UPDATE(user_id), INSERT(user_id), REFERENCES(user_id) ON emp_user TO psm WITH GRANT OPTION;
GRANT SELECT(emp_user_login_pw), UPDATE(emp_user_login_pw), INSERT(emp_user_login_pw), REFERENCES(emp_user_login_pw) ON emp_user TO psm WITH GRANT OPTION;
GRANT SELECT(status), UPDATE(status), INSERT(status), REFERENCES(status) ON emp_user TO psm WITH GRANT OPTION;
GRANT SELECT(insert_user_id), UPDATE(insert_user_id), INSERT(insert_user_id), REFERENCES(insert_user_id) ON emp_user TO psm WITH GRANT OPTION;
GRANT SELECT(insert_datetime), UPDATE(insert_datetime), INSERT(insert_datetime), REFERENCES(insert_datetime) ON emp_user TO psm WITH GRANT OPTION;
GRANT SELECT(update_user_id), UPDATE(update_user_id), INSERT(update_user_id), REFERENCES(update_user_id) ON emp_user TO psm WITH GRANT OPTION;
GRANT SELECT(update_datetime), UPDATE(update_datetime), INSERT(update_datetime), REFERENCES(update_datetime) ON emp_user TO psm WITH GRANT OPTION;

-- Table: group_code

-- DROP TABLE group_code;

CREATE TABLE group_code
(
  group_code_id character varying(30) NOT NULL,
  group_code_name character varying(100) NOT NULL,
  description character varying(1000),
  use_flag character(1) NOT NULL,
  insert_user_id character varying(50),
  insert_datetime timestamp without time zone,
  update_user_id character varying(50),
  update_datetime timestamp without time zone,
  CONSTRAINT group_code_pkey PRIMARY KEY (group_code_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE group_code
  OWNER TO psm;
GRANT SELECT(group_code_id), UPDATE(group_code_id), INSERT(group_code_id), REFERENCES(group_code_id) ON group_code TO postgres WITH GRANT OPTION;
GRANT SELECT(group_code_name), UPDATE(group_code_name), INSERT(group_code_name), REFERENCES(group_code_name) ON group_code TO postgres WITH GRANT OPTION;
GRANT SELECT(description), UPDATE(description), INSERT(description), REFERENCES(description) ON group_code TO postgres WITH GRANT OPTION;
GRANT SELECT(use_flag), UPDATE(use_flag), INSERT(use_flag), REFERENCES(use_flag) ON group_code TO postgres WITH GRANT OPTION;
GRANT SELECT(insert_user_id), UPDATE(insert_user_id), INSERT(insert_user_id), REFERENCES(insert_user_id) ON group_code TO postgres WITH GRANT OPTION;
GRANT SELECT(insert_datetime), UPDATE(insert_datetime), INSERT(insert_datetime), REFERENCES(insert_datetime) ON group_code TO postgres WITH GRANT OPTION;
GRANT SELECT(update_user_id), UPDATE(update_user_id), INSERT(update_user_id), REFERENCES(update_user_id) ON group_code TO postgres WITH GRANT OPTION;
GRANT SELECT(update_datetime), UPDATE(update_datetime), INSERT(update_datetime), REFERENCES(update_datetime) ON group_code TO postgres WITH GRANT OPTION;

-- Table: code

-- DROP TABLE code;

CREATE TABLE code
(
  group_code_id character varying(30) NOT NULL,
  code_id character varying(30) NOT NULL,
  sort_order integer,
  use_flag character(1) NOT NULL,
  code_type character(6) NOT NULL,
  code_name character varying(100) NOT NULL,
  description character varying(1000),
  insert_user_id character varying(50),
  insert_datetime timestamp without time zone,
  update_user_id character varying(50),
  update_datetime timestamp without time zone,
  CONSTRAINT code_pkey PRIMARY KEY (group_code_id, code_id),
  CONSTRAINT r_16 FOREIGN KEY (group_code_id)
      REFERENCES group_code (group_code_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE code
  OWNER TO psm;
GRANT SELECT(group_code_id), UPDATE(group_code_id), INSERT(group_code_id), REFERENCES(group_code_id) ON code TO postgres WITH GRANT OPTION;
GRANT SELECT(code_id), UPDATE(code_id), INSERT(code_id), REFERENCES(code_id) ON code TO postgres WITH GRANT OPTION;
GRANT SELECT(sort_order), UPDATE(sort_order), INSERT(sort_order), REFERENCES(sort_order) ON code TO postgres WITH GRANT OPTION;
GRANT SELECT(use_flag), UPDATE(use_flag), INSERT(use_flag), REFERENCES(use_flag) ON code TO postgres WITH GRANT OPTION;
GRANT SELECT(code_type), UPDATE(code_type), INSERT(code_type), REFERENCES(code_type) ON code TO postgres WITH GRANT OPTION;
GRANT SELECT(code_name), UPDATE(code_name), INSERT(code_name), REFERENCES(code_name) ON code TO postgres WITH GRANT OPTION;
GRANT SELECT(description), UPDATE(description), INSERT(description), REFERENCES(description) ON code TO postgres WITH GRANT OPTION;
GRANT SELECT(insert_user_id), UPDATE(insert_user_id), INSERT(insert_user_id), REFERENCES(insert_user_id) ON code TO postgres WITH GRANT OPTION;
GRANT SELECT(insert_datetime), UPDATE(insert_datetime), INSERT(insert_datetime), REFERENCES(insert_datetime) ON code TO postgres WITH GRANT OPTION;
GRANT SELECT(update_user_id), UPDATE(update_user_id), INSERT(update_user_id), REFERENCES(update_user_id) ON code TO postgres WITH GRANT OPTION;
GRANT SELECT(update_datetime), UPDATE(update_datetime), INSERT(update_datetime), REFERENCES(update_datetime) ON code TO postgres WITH GRANT OPTION;

-- Table: holiday

-- DROP TABLE holiday;

CREATE TABLE holiday
(
  holi_dt character varying(8) NOT NULL,
  holi_nm character varying(20),
  holi_yn character varying(1) NOT NULL,
  work_start_tm character varying(6),
  work_end_tm character varying(6)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE holiday
  OWNER TO psm;

-- Table: manager_act_hist

-- DROP TABLE manager_act_hist;

CREATE TABLE manager_act_hist
(
  log_auth_id character varying(32) NOT NULL,
  admin_user_id character varying(50) NOT NULL,
  ip_address character varying(128) NOT NULL,
  log_datetime timestamp without time zone NOT NULL,
  menu_id character varying(30),
  l_category character varying(100),
  m_category character varying(100),
  s_category character varying(100),
  log_message character varying(1000) NOT NULL,
  log_action character varying(50),
  CONSTRAINT manager_act_hist_pkey PRIMARY KEY (log_auth_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE manager_act_hist
  OWNER TO psm;
GRANT SELECT(log_auth_id), UPDATE(log_auth_id), INSERT(log_auth_id), REFERENCES(log_auth_id) ON manager_act_hist TO psm WITH GRANT OPTION;
GRANT SELECT(admin_user_id), UPDATE(admin_user_id), INSERT(admin_user_id), REFERENCES(admin_user_id) ON manager_act_hist TO psm WITH GRANT OPTION;
GRANT SELECT(ip_address), UPDATE(ip_address), INSERT(ip_address), REFERENCES(ip_address) ON manager_act_hist TO psm WITH GRANT OPTION;
GRANT SELECT(log_datetime), UPDATE(log_datetime), INSERT(log_datetime), REFERENCES(log_datetime) ON manager_act_hist TO psm WITH GRANT OPTION;
GRANT SELECT(menu_id), UPDATE(menu_id), INSERT(menu_id), REFERENCES(menu_id) ON manager_act_hist TO psm WITH GRANT OPTION;
GRANT SELECT(l_category), UPDATE(l_category), INSERT(l_category), REFERENCES(l_category) ON manager_act_hist TO psm WITH GRANT OPTION;
GRANT SELECT(m_category), UPDATE(m_category), INSERT(m_category), REFERENCES(m_category) ON manager_act_hist TO psm WITH GRANT OPTION;
GRANT SELECT(s_category), UPDATE(s_category), INSERT(s_category), REFERENCES(s_category) ON manager_act_hist TO psm WITH GRANT OPTION;
GRANT SELECT(log_message), UPDATE(log_message), INSERT(log_message), REFERENCES(log_message) ON manager_act_hist TO psm WITH GRANT OPTION;
GRANT SELECT(log_action), UPDATE(log_action), INSERT(log_action), REFERENCES(log_action) ON manager_act_hist TO psm WITH GRANT OPTION;

-- Table: menu

-- DROP TABLE menu;

CREATE TABLE menu
(
  menu_id character varying(30) NOT NULL,
  menu_name character varying(100) NOT NULL,
  parent_menu_id character varying(30) NOT NULL,
  menu_url character varying(1000),
  menu_depth integer,
  description character varying(1000),
  use_flag character(1) NOT NULL,
  sort_order integer,
  insert_user_id character varying(50),
  insert_datetime timestamp without time zone,
  update_user_id character varying(50),
  update_datetime timestamp without time zone,
  CONSTRAINT menu2_pkey PRIMARY KEY (menu_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE menu
  OWNER TO psm;
GRANT SELECT(menu_id), UPDATE(menu_id), INSERT(menu_id), REFERENCES(menu_id) ON menu TO psm WITH GRANT OPTION;
GRANT SELECT(menu_name), UPDATE(menu_name), INSERT(menu_name), REFERENCES(menu_name) ON menu TO psm WITH GRANT OPTION;
GRANT SELECT(parent_menu_id), UPDATE(parent_menu_id), INSERT(parent_menu_id), REFERENCES(parent_menu_id) ON menu TO psm WITH GRANT OPTION;
GRANT SELECT(menu_url), UPDATE(menu_url), INSERT(menu_url), REFERENCES(menu_url) ON menu TO psm WITH GRANT OPTION;
GRANT SELECT(menu_depth), UPDATE(menu_depth), INSERT(menu_depth), REFERENCES(menu_depth) ON menu TO psm WITH GRANT OPTION;
GRANT SELECT(description), UPDATE(description), INSERT(description), REFERENCES(description) ON menu TO psm WITH GRANT OPTION;
GRANT SELECT(use_flag), UPDATE(use_flag), INSERT(use_flag), REFERENCES(use_flag) ON menu TO psm WITH GRANT OPTION;
GRANT SELECT(sort_order), UPDATE(sort_order), INSERT(sort_order), REFERENCES(sort_order) ON menu TO psm WITH GRANT OPTION;
GRANT SELECT(insert_user_id), UPDATE(insert_user_id), INSERT(insert_user_id), REFERENCES(insert_user_id) ON menu TO psm WITH GRANT OPTION;
GRANT SELECT(insert_datetime), UPDATE(insert_datetime), INSERT(insert_datetime), REFERENCES(insert_datetime) ON menu TO psm WITH GRANT OPTION;
GRANT SELECT(update_user_id), UPDATE(update_user_id), INSERT(update_user_id), REFERENCES(update_user_id) ON menu TO psm WITH GRANT OPTION;
GRANT SELECT(update_datetime), UPDATE(update_datetime), INSERT(update_datetime), REFERENCES(update_datetime) ON menu TO psm WITH GRANT OPTION;

-- Table: menu_auth

-- DROP TABLE menu_auth;

CREATE TABLE menu_auth
(
  auth_id character varying(30) NOT NULL,
  menu_id character varying(30) NOT NULL,
  insert_user_id character varying(50),
  insert_datetime timestamp without time zone,
  update_user_id character varying(50),
  update_datetime timestamp without time zone,
  CONSTRAINT menu_auth_pkey PRIMARY KEY (auth_id, menu_id),
  CONSTRAINT r_11 FOREIGN KEY (auth_id)
      REFERENCES auth (auth_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT r_13 FOREIGN KEY (menu_id)
      REFERENCES menu (menu_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE menu_auth
  OWNER TO psm;
GRANT SELECT(auth_id), UPDATE(auth_id), INSERT(auth_id), REFERENCES(auth_id) ON menu_auth TO psm WITH GRANT OPTION;
GRANT SELECT(menu_id), UPDATE(menu_id), INSERT(menu_id), REFERENCES(menu_id) ON menu_auth TO psm WITH GRANT OPTION;
GRANT SELECT(insert_user_id), UPDATE(insert_user_id), INSERT(insert_user_id), REFERENCES(insert_user_id) ON menu_auth TO psm WITH GRANT OPTION;
GRANT SELECT(insert_datetime), UPDATE(insert_datetime), INSERT(insert_datetime), REFERENCES(insert_datetime) ON menu_auth TO psm WITH GRANT OPTION;
GRANT SELECT(update_user_id), UPDATE(update_user_id), INSERT(update_user_id), REFERENCES(update_user_id) ON menu_auth TO psm WITH GRANT OPTION;
GRANT SELECT(update_datetime), UPDATE(update_datetime), INSERT(update_datetime), REFERENCES(update_datetime) ON menu_auth TO psm WITH GRANT OPTION;

-- Table: privacy_type

-- DROP TABLE privacy_type;

CREATE TABLE privacy_type
(
  privacy_type_id character varying(10) NOT NULL, -- 개인정보ID
  privacy_flag character(1) NOT NULL, -- 개인정보구분
  privacy_name character varying(100) NOT NULL, -- 개인정보명
  description character(18), -- 설명
  use_flag character(1) NOT NULL, -- 사용유무
  sort_order integer, -- 정렬순서
  insert_user_id character varying(50), -- 등록자
  insert_datetime timestamp without time zone, -- 등록일자
  update_user_id character varying(50), -- 수정자
  update_datetime timestamp without time zone, -- 수정일자
  CONSTRAINT privacy_type_pkey PRIMARY KEY (privacy_type_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE privacy_type
  OWNER TO psm;
COMMENT ON COLUMN privacy_type.privacy_type_id IS '개인정보ID';
COMMENT ON COLUMN privacy_type.privacy_flag IS '개인정보구분';
COMMENT ON COLUMN privacy_type.privacy_name IS '개인정보명';
COMMENT ON COLUMN privacy_type.description IS '설명';
COMMENT ON COLUMN privacy_type.use_flag IS '사용유무';
COMMENT ON COLUMN privacy_type.sort_order IS '정렬순서';
COMMENT ON COLUMN privacy_type.insert_user_id IS '등록자';
COMMENT ON COLUMN privacy_type.insert_datetime IS '등록일자';
COMMENT ON COLUMN privacy_type.update_user_id IS '수정자';
COMMENT ON COLUMN privacy_type.update_datetime IS '수정일자';

GRANT SELECT(privacy_type_id), UPDATE(privacy_type_id), INSERT(privacy_type_id), REFERENCES(privacy_type_id) ON privacy_type TO postgres WITH GRANT OPTION;
GRANT SELECT(privacy_flag), UPDATE(privacy_flag), INSERT(privacy_flag), REFERENCES(privacy_flag) ON privacy_type TO postgres WITH GRANT OPTION;
GRANT SELECT(privacy_name), UPDATE(privacy_name), INSERT(privacy_name), REFERENCES(privacy_name) ON privacy_type TO postgres WITH GRANT OPTION;
GRANT SELECT(description), UPDATE(description), INSERT(description), REFERENCES(description) ON privacy_type TO postgres WITH GRANT OPTION;
GRANT SELECT(use_flag), UPDATE(use_flag), INSERT(use_flag), REFERENCES(use_flag) ON privacy_type TO postgres WITH GRANT OPTION;
GRANT SELECT(sort_order), UPDATE(sort_order), INSERT(sort_order), REFERENCES(sort_order) ON privacy_type TO postgres WITH GRANT OPTION;
GRANT SELECT(insert_user_id), UPDATE(insert_user_id), INSERT(insert_user_id), REFERENCES(insert_user_id) ON privacy_type TO postgres WITH GRANT OPTION;
GRANT SELECT(insert_datetime), UPDATE(insert_datetime), INSERT(insert_datetime), REFERENCES(insert_datetime) ON privacy_type TO postgres WITH GRANT OPTION;
GRANT SELECT(update_user_id), UPDATE(update_user_id), INSERT(update_user_id), REFERENCES(update_user_id) ON privacy_type TO postgres WITH GRANT OPTION;
GRANT SELECT(update_datetime), UPDATE(update_datetime), INSERT(update_datetime), REFERENCES(update_datetime) ON privacy_type TO postgres WITH GRANT OPTION;

-- Table: regular_expression_mng

-- DROP TABLE regular_expression_mng;

CREATE TABLE regular_expression_mng
(
  privacy_seq integer NOT NULL,
  privacy_type character varying(5) NOT NULL,
  privacy_desc character varying(50) NOT NULL,
  regular_expression character varying(8000),
  use_flag character varying(1) NOT NULL DEFAULT 'Y'::character varying,
  CONSTRAINT regular_expression_mng_pkey PRIMARY KEY (privacy_seq)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE regular_expression_mng
  OWNER TO psm;
GRANT SELECT(privacy_seq), UPDATE(privacy_seq), INSERT(privacy_seq), REFERENCES(privacy_seq) ON regular_expression_mng TO psm WITH GRANT OPTION;
GRANT SELECT(privacy_type), UPDATE(privacy_type), INSERT(privacy_type), REFERENCES(privacy_type) ON regular_expression_mng TO psm WITH GRANT OPTION;
GRANT SELECT(privacy_desc), UPDATE(privacy_desc), INSERT(privacy_desc), REFERENCES(privacy_desc) ON regular_expression_mng TO psm WITH GRANT OPTION;
GRANT SELECT(regular_expression), UPDATE(regular_expression), INSERT(regular_expression), REFERENCES(regular_expression) ON regular_expression_mng TO psm WITH GRANT OPTION;
GRANT SELECT(use_flag), UPDATE(use_flag), INSERT(use_flag), REFERENCES(use_flag) ON regular_expression_mng TO psm WITH GRANT OPTION;

-- Table: role

-- DROP TABLE role;

CREATE TABLE role
(
  role_id character varying(30) NOT NULL,
  role_name character varying(100) NOT NULL,
  use_flag character(1) NOT NULL,
  insert_user_id character varying(50),
  insert_datetime timestamp without time zone,
  update_user_id character varying(50),
  update_datetime timestamp without time zone,
  CONSTRAINT role_pkey PRIMARY KEY (role_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE role
  OWNER TO psm;
GRANT SELECT(role_id), UPDATE(role_id), INSERT(role_id), REFERENCES(role_id) ON role TO postgres WITH GRANT OPTION;
GRANT SELECT(role_name), UPDATE(role_name), INSERT(role_name), REFERENCES(role_name) ON role TO postgres WITH GRANT OPTION;
GRANT SELECT(use_flag), UPDATE(use_flag), INSERT(use_flag), REFERENCES(use_flag) ON role TO postgres WITH GRANT OPTION;
GRANT SELECT(insert_user_id), UPDATE(insert_user_id), INSERT(insert_user_id), REFERENCES(insert_user_id) ON role TO postgres WITH GRANT OPTION;
GRANT SELECT(insert_datetime), UPDATE(insert_datetime), INSERT(insert_datetime), REFERENCES(insert_datetime) ON role TO postgres WITH GRANT OPTION;
GRANT SELECT(update_user_id), UPDATE(update_user_id), INSERT(update_user_id), REFERENCES(update_user_id) ON role TO postgres WITH GRANT OPTION;
GRANT SELECT(update_datetime), UPDATE(update_datetime), INSERT(update_datetime), REFERENCES(update_datetime) ON role TO postgres WITH GRANT OPTION;

-- Table: rule_biz

-- DROP TABLE rule_biz;

CREATE TABLE rule_biz
(
  emp_detail_seq integer NOT NULL,
  occr_dt character varying(8) NOT NULL,
  log_seq integer NOT NULL,
  CONSTRAINT rule_biz_pkey PRIMARY KEY (emp_detail_seq, log_seq)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE rule_biz
  OWNER TO psm;
-- Index: rule_biz_occr_dt_key

-- DROP INDEX rule_biz_occr_dt_key;

CREATE INDEX rule_biz_occr_dt_key
  ON rule_biz
  USING btree
  (occr_dt COLLATE pg_catalog."default" DESC);
  
  
-- Table: rule_extract_temp

-- DROP TABLE rule_extract_temp;

CREATE TABLE rule_extract_temp
(
  occr_dt character varying(8) NOT NULL,
  log_seq integer NOT NULL,
  rule_cd integer NOT NULL,
  emp_user_id character varying(16) NOT NULL,
  CONSTRAINT rule_extract_temp_pkey PRIMARY KEY (log_seq, rule_cd, emp_user_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE rule_extract_temp
  OWNER TO psm;
GRANT SELECT(occr_dt), UPDATE(occr_dt), INSERT(occr_dt), REFERENCES(occr_dt) ON rule_extract_temp TO psm WITH GRANT OPTION;
GRANT SELECT(log_seq), UPDATE(log_seq), INSERT(log_seq), REFERENCES(log_seq) ON rule_extract_temp TO psm WITH GRANT OPTION;
GRANT SELECT(rule_cd), UPDATE(rule_cd), INSERT(rule_cd), REFERENCES(rule_cd) ON rule_extract_temp TO psm WITH GRANT OPTION;
GRANT SELECT(emp_user_id), UPDATE(emp_user_id), INSERT(emp_user_id), REFERENCES(emp_user_id) ON rule_extract_temp TO psm WITH GRANT OPTION;

-- Table: rule_extract_temp_rt

-- DROP TABLE rule_extract_temp_rt;

CREATE TABLE rule_extract_temp_rt
(
  occr_dt character varying(8),
  log_seq integer,
  rule_cd integer,
  emp_user_id character varying(16)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE rule_extract_temp_rt
  OWNER TO psm;

-- Table: ruletbl

-- DROP TABLE ruletbl;

CREATE TABLE ruletbl
(
  rule_seq integer,
  rule_nm character varying(50),
  rule_desc character varying(4000),
  log_delimiter character varying(10),
  dng_val integer,
  script character varying(8000),
  limit_cnt integer,
  is_realtime_extract character varying(3),
  realtime_extract_time character varying(14),
  use_yn character varying(1),
  user_limit_cnt_yn character varying(1) NOT NULL DEFAULT 'N'::character varying
)
WITH (
  OIDS=FALSE
);
ALTER TABLE ruletbl
  OWNER TO psm;
GRANT SELECT(rule_seq), UPDATE(rule_seq), INSERT(rule_seq), REFERENCES(rule_seq) ON ruletbl TO psm WITH GRANT OPTION;
GRANT SELECT(rule_nm), UPDATE(rule_nm), INSERT(rule_nm), REFERENCES(rule_nm) ON ruletbl TO psm WITH GRANT OPTION;
GRANT SELECT(rule_desc), UPDATE(rule_desc), INSERT(rule_desc), REFERENCES(rule_desc) ON ruletbl TO psm WITH GRANT OPTION;
GRANT SELECT(log_delimiter), UPDATE(log_delimiter), INSERT(log_delimiter), REFERENCES(log_delimiter) ON ruletbl TO psm WITH GRANT OPTION;
GRANT SELECT(dng_val), UPDATE(dng_val), INSERT(dng_val), REFERENCES(dng_val) ON ruletbl TO psm WITH GRANT OPTION;
GRANT SELECT(script), UPDATE(script), INSERT(script), REFERENCES(script) ON ruletbl TO psm WITH GRANT OPTION;
GRANT SELECT(limit_cnt), UPDATE(limit_cnt), INSERT(limit_cnt), REFERENCES(limit_cnt) ON ruletbl TO psm WITH GRANT OPTION;
GRANT SELECT(is_realtime_extract), UPDATE(is_realtime_extract), INSERT(is_realtime_extract), REFERENCES(is_realtime_extract) ON ruletbl TO psm WITH GRANT OPTION;
GRANT SELECT(realtime_extract_time), UPDATE(realtime_extract_time), INSERT(realtime_extract_time), REFERENCES(realtime_extract_time) ON ruletbl TO psm WITH GRANT OPTION;
GRANT SELECT(use_yn), UPDATE(use_yn), INSERT(use_yn), REFERENCES(use_yn) ON ruletbl TO psm WITH GRANT OPTION;
GRANT SELECT(user_limit_cnt_yn), UPDATE(user_limit_cnt_yn), INSERT(user_limit_cnt_yn), REFERENCES(user_limit_cnt_yn) ON ruletbl TO psm WITH GRANT OPTION;

-- Table: schedule

-- DROP TABLE schedule;

CREATE TABLE schedule
(
  schid serial NOT NULL,
  month character varying(2),
  day character varying(2),
  hour character varying(2),
  minute character varying(2),
  beanid character varying(200),
  enabled character(1) DEFAULT 'N'::bpchar,
  descr character varying(1000),
  inserter_id character varying(50),
  inserted_date timestamp without time zone,
  updater_id character varying(50),
  updated_date timestamp without time zone,
  CONSTRAINT schedule_pkey PRIMARY KEY (schid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE schedule
  OWNER TO psm;

-- Table: schedule_log

-- DROP TABLE schedule_log;

CREATE TABLE schedule_log
(
  id serial NOT NULL,
  beanid character varying(200),
  descr character varying(1000),
  startdate timestamp without time zone,
  enddate timestamp without time zone,
  success character varying(1),
  CONSTRAINT schedule_log_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE schedule_log
  OWNER TO psm;

-- Table: server

-- DROP TABLE server;

CREATE TABLE server
(
  name character varying(20) NOT NULL,
  ip character varying(15),
  scheduling character(1) DEFAULT 'N'::bpchar,
  result character varying(20),
  CONSTRAINT server_pkey PRIMARY KEY (name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE server
  OWNER TO psm;

-- Table: summary_a01

-- DROP TABLE summary_a01;

CREATE TABLE summary_a01
(
  proc_date character varying(8),
  emp_cd character varying(16),
  result_type character varying(4),
  cnt bigint
)
WITH (
  OIDS=FALSE
);
ALTER TABLE summary_a01
  OWNER TO psm;

-- Table: summary_a02

-- DROP TABLE summary_a02;

CREATE TABLE summary_a02
(
  proc_date character varying(8),
  emp_cd character varying(16),
  user_ip character varying(16),
  result_type character varying(4),
  cnt bigint
)
WITH (
  OIDS=FALSE
);
ALTER TABLE summary_a02
  OWNER TO psm;

-- Table: summary_a03

-- DROP TABLE summary_a03;

CREATE TABLE summary_a03
(
  proc_date character varying(8),
  emp_user_id character varying(16),
  system_seq character varying(4),
  result_type character varying(4),
  cnt bigint,
  dept_id character varying(30)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE summary_a03
  OWNER TO psm;

-- Table: summary_a04

-- DROP TABLE summary_a04;

CREATE TABLE summary_a04
(
  proc_date character varying(8),
  emp_cd character varying(16),
  user_ip character varying(16),
  sys_cd character varying(4),
  result_type character varying(4),
  cnt bigint
)
WITH (
  OIDS=FALSE
);
ALTER TABLE summary_a04
  OWNER TO psm;

-- Table: summary_t01

-- DROP TABLE summary_t01;

CREATE TABLE summary_t01
(
  proc_date character varying(8) NOT NULL,
  object_type character varying(10) NOT NULL,
  object_name character varying(30) NOT NULL,
  object_size integer,
  data_size integer,
  index_size integer,
  rowcount integer,
  CONSTRAINT summary_t01_pkey PRIMARY KEY (proc_date, object_type, object_name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE summary_t01
  OWNER TO psm;

-- Table: system_info

-- DROP TABLE system_info;

CREATE TABLE system_info
(
  server_seq integer NOT NULL,
  update_time timestamp without time zone NOT NULL,
  cpu_usage_perc integer,
  cpu_idle_perc integer,
  mem_usage_perc integer,
  mem_free_perc integer,
  mem_total character varying(20),
  filesystem_usage_perc integer,
  filesystem_total character varying(20),
  CONSTRAINT system_info_pkey PRIMARY KEY (server_seq, update_time)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE system_info
  OWNER TO psm;

-- Table: t_encryptkey

-- DROP TABLE t_encryptkey;

CREATE TABLE t_encryptkey
(
  key character varying(25)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE t_encryptkey
  OWNER TO psm;

-- Table: t_tag

-- DROP TABLE t_tag;

CREATE TABLE t_tag
(
  tag_seq integer NOT NULL,
  url character varying(1000) NOT NULL,
  url_type character varying(1000) DEFAULT NULL::character varying,
  parameter character varying(1000) DEFAULT NULL::character varying,
  menu_name character varying(100) DEFAULT NULL::character varying,
  crud_type character varying(3) DEFAULT NULL::character varying,
  use_yn character varying(1) DEFAULT NULL::character varying,
  system_seq character varying(25),
  CONSTRAINT t_tag_pkey PRIMARY KEY (tag_seq)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE t_tag
  OWNER TO psm;
GRANT SELECT(tag_seq), UPDATE(tag_seq), INSERT(tag_seq), REFERENCES(tag_seq) ON t_tag TO psm WITH GRANT OPTION;
GRANT SELECT(url), UPDATE(url), INSERT(url), REFERENCES(url) ON t_tag TO psm WITH GRANT OPTION;
GRANT SELECT(url_type), UPDATE(url_type), INSERT(url_type), REFERENCES(url_type) ON t_tag TO psm WITH GRANT OPTION;
GRANT SELECT(parameter), UPDATE(parameter), INSERT(parameter), REFERENCES(parameter) ON t_tag TO psm WITH GRANT OPTION;
GRANT SELECT(menu_name), UPDATE(menu_name), INSERT(menu_name), REFERENCES(menu_name) ON t_tag TO psm WITH GRANT OPTION;
GRANT SELECT(crud_type), UPDATE(crud_type), INSERT(crud_type), REFERENCES(crud_type) ON t_tag TO psm WITH GRANT OPTION;
GRANT SELECT(use_yn), UPDATE(use_yn), INSERT(use_yn), REFERENCES(use_yn) ON t_tag TO psm WITH GRANT OPTION;
GRANT SELECT(system_seq), UPDATE(system_seq), INSERT(system_seq), REFERENCES(system_seq) ON t_tag TO psm WITH GRANT OPTION;

-- Table: system_master

-- DROP TABLE system_master;

CREATE TABLE system_master
(
  system_seq character varying(25) NOT NULL,
  system_name character varying(32),
  system_type character varying(32),
  description character varying(200),
  CONSTRAINT system_master_pkey PRIMARY KEY (system_seq)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE system_master
  OWNER TO psm;
GRANT SELECT(system_seq), UPDATE(system_seq), INSERT(system_seq), REFERENCES(system_seq) ON system_master TO psm WITH GRANT OPTION;
GRANT SELECT(system_name), UPDATE(system_name), INSERT(system_name), REFERENCES(system_name) ON system_master TO psm WITH GRANT OPTION;
GRANT SELECT(system_type), UPDATE(system_type), INSERT(system_type), REFERENCES(system_type) ON system_master TO psm WITH GRANT OPTION;
GRANT SELECT(description), UPDATE(description), INSERT(description), REFERENCES(description) ON system_master TO psm WITH GRANT OPTION;

-- Table: server_master

-- DROP TABLE server_master;

CREATE TABLE server_master
(
  server_seq character varying(25) NOT NULL,
  system_seq character varying(25) NOT NULL,
  server_name character varying(32),
  host_name character varying(50),
  ip character varying(15),
  description character varying(200),
  CONSTRAINT server_master_pkey PRIMARY KEY (server_seq),
  CONSTRAINT r_16 FOREIGN KEY (system_seq)
      REFERENCES system_master (system_seq) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE server_master
  OWNER TO psm;
GRANT SELECT(server_seq), UPDATE(server_seq), INSERT(server_seq), REFERENCES(server_seq) ON server_master TO psm WITH GRANT OPTION;
GRANT SELECT(system_seq), UPDATE(system_seq), INSERT(system_seq), REFERENCES(system_seq) ON server_master TO psm WITH GRANT OPTION;
GRANT SELECT(server_name), UPDATE(server_name), INSERT(server_name), REFERENCES(server_name) ON server_master TO psm WITH GRANT OPTION;
GRANT SELECT(host_name), UPDATE(host_name), INSERT(host_name), REFERENCES(host_name) ON server_master TO psm WITH GRANT OPTION;
GRANT SELECT(ip), UPDATE(ip), INSERT(ip), REFERENCES(ip) ON server_master TO psm WITH GRANT OPTION;
GRANT SELECT(description), UPDATE(description), INSERT(description), REFERENCES(description) ON server_master TO psm WITH GRANT OPTION;

-- Table: agent_master

-- DROP TABLE agent_master;

CREATE TABLE agent_master
(
  agent_seq character varying(25) NOT NULL,
  server_seq character varying(25) NOT NULL,
  agent_name character varying(32),
  ip character varying(15),
  port character varying(15),
  description character varying(200),
  agent_type character varying(32),
  CONSTRAINT agent_master_pkey PRIMARY KEY (agent_seq),
  CONSTRAINT r_16 FOREIGN KEY (server_seq)
      REFERENCES server_master (server_seq) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE agent_master
  OWNER TO psm;
GRANT SELECT(agent_seq), UPDATE(agent_seq), INSERT(agent_seq), REFERENCES(agent_seq) ON agent_master TO psm WITH GRANT OPTION;
GRANT SELECT(server_seq), UPDATE(server_seq), INSERT(server_seq), REFERENCES(server_seq) ON agent_master TO psm WITH GRANT OPTION;
GRANT SELECT(agent_name), UPDATE(agent_name), INSERT(agent_name), REFERENCES(agent_name) ON agent_master TO psm WITH GRANT OPTION;
GRANT SELECT(ip), UPDATE(ip), INSERT(ip), REFERENCES(ip) ON agent_master TO psm WITH GRANT OPTION;
GRANT SELECT(port), UPDATE(port), INSERT(port), REFERENCES(port) ON agent_master TO psm WITH GRANT OPTION;
GRANT SELECT(description), UPDATE(description), INSERT(description), REFERENCES(description) ON agent_master TO psm WITH GRANT OPTION;


-- View: dept_tree

-- DROP VIEW dept_tree;

CREATE OR REPLACE VIEW dept_tree AS 
 WITH RECURSIVE dept_hierarchy(dept_id, parent_dept_id, dept_name, use_flag, sort_order, full_dept_name, simple_dept_name, level) AS (
                 SELECT department.dept_id, department.parent_dept_id, 
                    department.dept_name, department.use_flag, 
                    department.sort_order, 
                    ''::text || department.dept_name::text AS full_dept_name, 
                    ''::text || department.dept_name::text AS simple_dept_name, 
                    0
                   FROM department
                  WHERE department.parent_dept_id::text = 'DEPT00000'::text
        UNION ALL 
                 SELECT d.dept_id, d.parent_dept_id, d.dept_name, d.use_flag, 
                    d.sort_order, 
                    (dh.full_dept_name || ' '::text) || d.dept_name::text, 
                    (lpad('-------------  '::text, (dh.level + 1) * 1) || ' '::text) || d.dept_name::text, 
                    dh.level + 1
                   FROM department d
              JOIN dept_hierarchy dh ON d.parent_dept_id::text = dh.dept_id::text
        )
 SELECT dept_hierarchy.dept_id, dept_hierarchy.parent_dept_id, 
    dept_hierarchy.dept_name, dept_hierarchy.use_flag, 
    dept_hierarchy.sort_order, dept_hierarchy.full_dept_name, 
    dept_hierarchy.simple_dept_name, dept_hierarchy.level AS dept_depth
   FROM dept_hierarchy
  ORDER BY dept_hierarchy.full_dept_name, dept_hierarchy.sort_order;

ALTER TABLE dept_tree
  OWNER TO psm;


-- View: menu_tree

-- DROP VIEW menu_tree;

CREATE OR REPLACE VIEW menu_tree AS 
 WITH RECURSIVE menu_hierarchy(menu_id, parent_menu_id, menu_name, menu_url, use_flag, sort_order, full_menu_name, simple_menu_name, level) AS (
                 SELECT menu.menu_id, menu.parent_menu_id, menu.menu_name, 
                    menu.menu_url, menu.use_flag, menu.sort_order, 
                    ''::text || menu.menu_name::text AS full_menu_name, 
                    ''::text || menu.menu_name::text AS simple_menu_name, 0
                   FROM menu menu
                  WHERE menu.parent_menu_id::text = 'MENU'::text
        UNION ALL 
                 SELECT m.menu_id, m.parent_menu_id, m.menu_name, m.menu_url, 
                    m.use_flag, m.sort_order, 
                    ((mh.full_menu_name || ' '::text) || m.sort_order::text) || m.menu_name::text, 
                    lpad('-------------'::text, (mh.level + 1) * 1) || m.menu_name::text, 
                    mh.level + 1
                   FROM menu m
              JOIN menu_hierarchy mh ON m.parent_menu_id::text = mh.menu_id::text
        )
 SELECT menu_hierarchy.menu_id, menu_hierarchy.parent_menu_id, 
    menu_hierarchy.menu_name, menu_hierarchy.menu_url, menu_hierarchy.use_flag, 
    menu_hierarchy.sort_order, menu_hierarchy.full_menu_name, 
    menu_hierarchy.simple_menu_name, menu_hierarchy.level AS menu_depth
   FROM menu_hierarchy
  ORDER BY menu_hierarchy.full_menu_name, menu_hierarchy.sort_order;

ALTER TABLE menu_tree
  OWNER TO psm;


-- View: v_summary_a01

-- DROP VIEW v_summary_a01;

CREATE OR REPLACE VIEW v_summary_a01 AS 
 SELECT summary_a01.proc_date, summary_a01.emp_cd, 
    sum(
        CASE summary_a01.result_type
            WHEN '1'::text THEN summary_a01.cnt
            ELSE 0::bigint
        END) AS type1, 
    sum(
        CASE summary_a01.result_type
            WHEN '2'::text THEN summary_a01.cnt
            ELSE 0::bigint
        END) AS type2, 
    sum(
        CASE summary_a01.result_type
            WHEN '3'::text THEN summary_a01.cnt
            ELSE 0::bigint
        END) AS type3, 
    sum(
        CASE summary_a01.result_type
            WHEN '4'::text THEN summary_a01.cnt
            ELSE 0::bigint
        END) AS type4, 
    sum(
        CASE summary_a01.result_type
            WHEN '5'::text THEN summary_a01.cnt
            ELSE 0::bigint
        END) AS type5, 
    sum(
        CASE summary_a01.result_type
            WHEN '6'::text THEN summary_a01.cnt
            ELSE 0::bigint
        END) AS type6, 
    sum(
        CASE summary_a01.result_type
            WHEN '7'::text THEN summary_a01.cnt
            ELSE 0::bigint
        END) AS type7, 
    sum(
        CASE summary_a01.result_type
            WHEN '8'::text THEN summary_a01.cnt
            ELSE 0::bigint
        END) AS type8, 
    sum(
        CASE summary_a01.result_type
            WHEN '9'::text THEN summary_a01.cnt
            ELSE 0::bigint
        END) AS type9, 
    sum(
        CASE summary_a01.result_type
            WHEN '10'::text THEN summary_a01.cnt
            ELSE 0::bigint
        END) AS type10
   FROM summary_a01
  GROUP BY summary_a01.proc_date, summary_a01.emp_cd
  ORDER BY summary_a01.proc_date, summary_a01.emp_cd;

ALTER TABLE v_summary_a01
  OWNER TO psm;

-- View: v_summary_a02

-- DROP VIEW v_summary_a02;

CREATE OR REPLACE VIEW v_summary_a02 AS 
 SELECT summary_a02.proc_date, summary_a02.emp_cd, summary_a02.user_ip, 
    sum(
        CASE summary_a02.result_type
            WHEN '1'::text THEN summary_a02.cnt
            ELSE 0::bigint
        END) AS type1, 
    sum(
        CASE summary_a02.result_type
            WHEN '2'::text THEN summary_a02.cnt
            ELSE 0::bigint
        END) AS type2, 
    sum(
        CASE summary_a02.result_type
            WHEN '3'::text THEN summary_a02.cnt
            ELSE 0::bigint
        END) AS type3, 
    sum(
        CASE summary_a02.result_type
            WHEN '4'::text THEN summary_a02.cnt
            ELSE 0::bigint
        END) AS type4, 
    sum(
        CASE summary_a02.result_type
            WHEN '5'::text THEN summary_a02.cnt
            ELSE 0::bigint
        END) AS type5, 
    sum(
        CASE summary_a02.result_type
            WHEN '6'::text THEN summary_a02.cnt
            ELSE 0::bigint
        END) AS type6, 
    sum(
        CASE summary_a02.result_type
            WHEN '7'::text THEN summary_a02.cnt
            ELSE 0::bigint
        END) AS type7, 
    sum(
        CASE summary_a02.result_type
            WHEN '8'::text THEN summary_a02.cnt
            ELSE 0::bigint
        END) AS type8, 
    sum(
        CASE summary_a02.result_type
            WHEN '9'::text THEN summary_a02.cnt
            ELSE 0::bigint
        END) AS type9, 
    sum(
        CASE summary_a02.result_type
            WHEN '10'::text THEN summary_a02.cnt
            ELSE 0::bigint
        END) AS type10
   FROM summary_a02
  GROUP BY summary_a02.proc_date, summary_a02.emp_cd, summary_a02.user_ip
  ORDER BY summary_a02.proc_date, summary_a02.emp_cd;

ALTER TABLE v_summary_a02
  OWNER TO psm;

-- View: v_summary_a03

-- DROP VIEW v_summary_a03;

CREATE OR REPLACE VIEW v_summary_a03 AS 
 SELECT summary_a03.proc_date, summary_a03.emp_user_id, summary_a03.system_seq, 
    summary_a03.dept_id, 
    sum(
        CASE summary_a03.result_type
            WHEN '1'::text THEN summary_a03.cnt
            ELSE 0::bigint
        END) AS type1, 
    sum(
        CASE summary_a03.result_type
            WHEN '2'::text THEN summary_a03.cnt
            ELSE 0::bigint
        END) AS type2, 
    sum(
        CASE summary_a03.result_type
            WHEN '3'::text THEN summary_a03.cnt
            ELSE 0::bigint
        END) AS type3, 
    sum(
        CASE summary_a03.result_type
            WHEN '4'::text THEN summary_a03.cnt
            ELSE 0::bigint
        END) AS type4, 
    sum(
        CASE summary_a03.result_type
            WHEN '5'::text THEN summary_a03.cnt
            ELSE 0::bigint
        END) AS type5, 
    sum(
        CASE summary_a03.result_type
            WHEN '6'::text THEN summary_a03.cnt
            ELSE 0::bigint
        END) AS type6, 
    sum(
        CASE summary_a03.result_type
            WHEN '7'::text THEN summary_a03.cnt
            ELSE 0::bigint
        END) AS type7, 
    sum(
        CASE summary_a03.result_type
            WHEN '8'::text THEN summary_a03.cnt
            ELSE 0::bigint
        END) AS type8, 
    sum(
        CASE summary_a03.result_type
            WHEN '9'::text THEN summary_a03.cnt
            ELSE 0::bigint
        END) AS type9, 
    sum(
        CASE summary_a03.result_type
            WHEN '10'::text THEN summary_a03.cnt
            ELSE 0::bigint
        END) AS type10
   FROM summary_a03
  GROUP BY summary_a03.proc_date, summary_a03.emp_user_id, summary_a03.system_seq, summary_a03.dept_id
  ORDER BY summary_a03.proc_date, summary_a03.emp_user_id, summary_a03.system_seq, summary_a03.dept_id;

ALTER TABLE v_summary_a03
  OWNER TO psm;
