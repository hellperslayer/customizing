
-- DATA

-- admin_user --

INSERT INTO public.admin_user(admin_user_id, admin_user_name, auth_id, dept_id, password, pwd_check, last_pwd_change_datetime, last_login_success_datetime, last_login_fail_datetime, grade, email_address, mobile_number, del_flag, alarm_flag, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('admin2', '관리자2', 'AUTH00001', 'DEPT00001', '21232f297a57a5a743894a0e4a801fc3', 5, '2015-07-01 15:30:21.449293', NULL, NULL, 1, '', '', 'N', 'N', '관리자', 'easycerti', '2015-04-14 11:08:53.0', 'easycerti', '2015-07-01 10:34:34.0');

INSERT INTO public.admin_user(admin_user_id, admin_user_name, auth_id, dept_id, password, pwd_check, last_pwd_change_datetime, last_login_success_datetime, last_login_fail_datetime, grade, email_address, mobile_number, del_flag, alarm_flag, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('admin1', '관리자1', 'AUTH00001', 'DEPT00001', '6fad132bb26df43694a7dea1035c3ff5', 5, '2015-07-01 15:30:21.449293', NULL, '2015-06-13 16:41:37.0', 1, '', '', 'N', 'N', '관리자', 'easycerti', '2015-04-14 11:08:53.0', 'easycerti', '2015-07-01 10:38:53.0');

INSERT INTO public.admin_user(admin_user_id, admin_user_name, auth_id, dept_id, password, pwd_check, last_pwd_change_datetime, last_login_success_datetime, last_login_fail_datetime, grade, email_address, mobile_number, del_flag, alarm_flag, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('admin', '관리자', 'AUTH00001', 'DEPT00001', '6fad132bb26df43694a7dea1035c3ff5', 5, '2015-07-04 13:50:20.133677', '2015-07-13 17:59:00.66', '2015-07-08 15:56:29.604', 1, '', '', 'N', 'N', '관리자', 'easycerti', '2015-04-14 11:08:53.0', 'admin', '2015-07-04 13:50:20.133677')
;
INSERT INTO public.admin_user(admin_user_id, admin_user_name, auth_id, dept_id, password, pwd_check, last_pwd_change_datetime, last_login_success_datetime, last_login_fail_datetime, grade, email_address, mobile_number, del_flag, alarm_flag, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('easycerti', '슈퍼관리자', 'AUTH00000', 'DEPT00001', '6fad132bb26df43694a7dea1035c3ff5', 5, '2015-07-14 13:56:19.948085', '2015-07-14 22:55:31.225', '2015-07-13 21:19:43.677', 1, '', '02-6942-9900', 'N', 'Y', '슈퍼관리자', 'easycerti', '2015-04-14 11:08:53.0', 'easycerti', '2015-07-14 13:56:19.948085')
;

-- agent_status --
CREATE TABLE public.agent_status  ( 
	agent_seq      	varchar(25) NOT NULL,
	server_seq     	int4 NULL,
	command_time   	timestamp NULL,
	last_updatetime	timestamp NULL,
	active_status  	int4 NULL,
	command        	int4 NULL 
	)
;

-- auth --

INSERT INTO public.auth(auth_id, auth_name, use_flag, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00003', '운영자', 'Y', 'admin', '2014-03-05 17:36:23.0', NULL, NULL)
;
INSERT INTO public.auth(auth_id, auth_name, use_flag, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00002', '운영관리자', 'Y', 'admin', '2014-03-04 14:40:20.0', NULL, NULL)
;
INSERT INTO public.auth(auth_id, auth_name, use_flag, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', '슈퍼관리자', 'Y', 'admin', '2014-03-04 14:40:20.0', 'admin', '2015-04-17 11:20:31.0')
;
INSERT INTO public.auth(auth_id, auth_name, use_flag, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', '관리자', 'Y', 'admin', '2014-03-04 14:40:20.0', 'easycerti', '2015-04-21 14:36:14.235011')
;
INSERT INTO public.auth(auth_id, auth_name, use_flag, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00004', '사용자', 'Y', 'admin', '2015-05-14 20:27:33.277312', 'admin', '2015-05-15 16:52:59.39791')
;


-- group_code --

INSERT INTO public.group_code(group_code_id, group_code_name, description, use_flag, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('REQ_TYPE', '화면 유형', '* 화면 종류 그룹코드* 관련 테이블 : biz_log', 'Y', 'easycerti', '2015-04-14 11:08:09.0', 'easycerti', '2015-04-24 14:14:45.0')
;
INSERT INTO public.group_code(group_code_id, group_code_name, description, use_flag, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('RESULT_TYPE', '개인정보 유형', '* 개인정보 유형 그룹코드
* 관련 테이블 : regular_expression_mng, biz_log, biz_log_result', 'Y', 'easycerti', '2015-04-14 11:08:09.128853', 'easycerti', '2015-04-21 09:55:43.350246')
;
INSERT INTO public.group_code(group_code_id, group_code_name, description, use_flag, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('EMP_USER_STATUS', '사원 상태', '* 사원 상태 그룹코드
* 관련 테이블 : emp_user', 'Y', 'easycerti', '2015-04-21 17:33:40.692014', 'easycerti', '2015-04-22 15:31:31.543452')
;
INSERT INTO public.group_code(group_code_id, group_code_name, description, use_flag, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('DANGER_STATUS', '위험도 상태', '* 위험도 상태 그룹코드
* 관련 테이블 : danger_status', 'Y', 'easycerti', '2015-04-24 14:02:07.895643', 'easycerti', '2015-04-24 14:03:23.180164')
;
INSERT INTO public.group_code(group_code_id, group_code_name, description, use_flag, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('URL_TYPE', 'URL 유형', '* URL 유형 그룹코드
* 관련 테이블 : add_url', 'Y', 'easycerti', '2015-04-24 14:06:08.324359', NULL, NULL)
;
INSERT INTO public.group_code(group_code_id, group_code_name, description, use_flag, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('BACKUP_TYPE', '백업 유형', '* 백업 종류 그룹코드
* 관련 테이블 : backup_hist', 'Y', 'easycerti', '2015-04-23 16:24:49.148813', 'easycerti', '2015-04-24 14:14:54.089925')
;


-- code --

INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('BACKUP_TYPE', '0', 1, 'Y', 'SYSTEM', '원본로그', '원본로그', 'easycerti', '2015-06-12 13:59:07.120086', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('BACKUP_TYPE', '2', 3, 'Y', 'SYSTEM', '파일로그', '파일로그', 'easycerti', '2015-06-12 14:00:02.392673', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('BACKUP_TYPE', '3', 4, 'Y', 'SYSTEM', '파일압축', '파일압축', 'easycerti', '2015-06-12 14:00:18.599535', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('BACKUP_TYPE', '1', 2, 'Y', 'SYSTEM', 'DB로그', 'DB로그', 'easycerti', '2015-06-12 13:59:35.439242', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('BACKUP_TYPE', '8', 9, 'Y', 'SYSTEM', 'FILE로그수집', 'FILE로그수집', 'easycerti', '2015-06-12 14:01:30.528138', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('EMP_USER_STATUS', 'W', 1, 'Y', 'SYSTEM', '재직', '재직 상태', 'easycerti', '2015-04-21 17:34:33.463751', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('EMP_USER_STATUS', 'R', 2, 'Y', 'SYSTEM', '퇴직', '퇴직 상태', 'easycerti', '2015-04-21 17:34:50.062185', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('EMP_USER_STATUS', 'L', 3, 'Y', 'SYSTEM', '휴직', '휴직 상태', 'easycerti', '2015-04-21 17:35:03.008259', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('EMP_USER_STATUS', 'V', 4, 'Y', 'SYSTEM', '휴가', '휴가 상태', 'easycerti', '2015-04-21 17:35:18.76641', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('EMP_USER_STATUS', 'X', 5, 'N', 'SYSTEM', '사용 안함', '사용 안함', 'easycerti', '2015-04-21 17:35:51.029914', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('DANGER_STATUS', '1', 1, 'Y', 'SYSTEM', '심각', '심각 상태', 'easycerti', '2015-04-24 14:02:24.798151', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('DANGER_STATUS', '2', 2, 'Y', 'SYSTEM', '경계', '경계 상태', 'easycerti', '2015-04-24 14:02:36.397429', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('DANGER_STATUS', '3', 3, 'Y', 'SYSTEM', '주의', '주의 상태', 'easycerti', '2015-04-24 14:02:46.517133', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('DANGER_STATUS', '4', 4, 'Y', 'SYSTEM', '관심', '관심 상태', 'easycerti', '2015-04-24 14:02:58.62987', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('DANGER_STATUS', '5', 5, 'Y', 'SYSTEM', '정상', '정상 상태', 'easycerti', '2015-04-24 14:03:10.244748', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('URL_TYPE', 'SE', 2, 'Y', 'SYSTEM', '엑셀 저장 URL', '엑셀 저장 URL', 'easycerti', '2015-04-24 14:07:42.681672', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('URL_TYPE', 'BE', 3, 'Y', 'SYSTEM', '대용량 엑셀 URL', '대용량 엑셀 URL', 'easycerti', '2015-04-24 14:08:00.282183', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('URL_TYPE', 'EC', 1, 'Y', 'SYSTEM', '제외 URL', '제외 URL', 'easycerti', '2015-04-24 14:07:17.996104', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('RESULT_TYPE', '3', 3, 'Y', 'SYSTEM', '여권번호', '여권번호', 'easycerti', '2015-04-14 11:08:29.222882', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('RESULT_TYPE', '7', 7, 'Y', 'SYSTEM', '이메일', '이메일', 'easycerti', '2015-04-14 11:08:29.222882', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('RESULT_TYPE', '5', 5, 'Y', 'SYSTEM', '건강보험번호', '건강보험번호', 'easycerti', '2015-04-14 11:08:29.222882', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('RESULT_TYPE', '8', 8, 'Y', 'SYSTEM', '휴대폰번호', '휴대폰번호', 'easycerti', '2015-04-14 11:08:29.222882', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('RESULT_TYPE', '4', 4, 'Y', 'SYSTEM', '신용카드번호', '신용카드번호', 'easycerti', '2015-04-14 11:08:29.222882', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('RESULT_TYPE', '6', 6, 'Y', 'SYSTEM', '전화번호', '전화번호', 'easycerti', '2015-04-14 11:08:29.222882', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('RESULT_TYPE', '1', 1, 'Y', 'SYSTEM', '주민등록번호', '주민등록번호', 'easycerti', '2015-04-14 11:08:29.222882', 'admin', '2015-04-17 15:38:29.744806')
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('RESULT_TYPE', '2', 2, 'Y', 'SYSTEM', '운전면허번호', '운전면허번호', 'easycerti', '2015-04-14 11:08:29.222882', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('BACKUP_TYPE', '5', 6, 'Y', 'SYSTEM', 'DB삭제', 'DB삭제', 'easycerti', '2015-06-12 14:00:58.490492', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('BACKUP_TYPE', '9', 10, 'Y', 'SYSTEM', '파일생성', '파일생성', 'easycerti', '2015-06-12 14:01:47.065179', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('BACKUP_TYPE', '7', 8, 'Y', 'SYSTEM', 'DB로그수집', 'DB로그수집', 'easycerti', '2015-06-12 14:01:12.632104', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('REQ_TYPE', 'RD', 1, 'Y', 'SYSTEM', '조회', '조회', 'easycerti', '2015-04-14 11:08:29.0', 'admin', '2015-04-17 16:30:09.0')
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('REQ_TYPE', 'CR', 2, 'Y', 'SYSTEM', '등록', '등록', 'easycerti', '2015-04-14 11:08:29.0', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('REQ_TYPE', 'DL', 4, 'Y', 'SYSTEM', '삭제', '삭제', 'easycerti', '2015-04-14 11:08:29.0', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('REQ_TYPE', 'UD', 3, 'Y', 'SYSTEM', '수정', '수정', 'easycerti', '2015-04-14 11:08:29.0', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('REQ_TYPE', 'PR', 6, 'Y', 'SYSTEM', '출력', '출력', 'easycerti', '2015-04-14 11:08:29.0', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('REQ_TYPE', 'EX', 5, 'Y', 'SYSTEM', '다운로드', '다운로드', 'easycerti', '2015-04-14 11:08:29.0', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('BACKUP_TYPE', '6', 7, 'Y', 'SYSTEM', '전체로그삭제', '전체로그삭제', 'easycerti', '2015-06-12 14:13:48.099373', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('BACKUP_TYPE', '4', 5, 'Y', 'SYSTEM', '파일압축실패', '파일압축실패', 'easycerti', '2015-06-12 14:12:19.124929', 'easycerti', '2015-06-12 14:12:50.811661')
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('RESULT_TYPE', '9', 9, 'Y', 'SYSTEM', '계좌번호', '계좌번호', 'easycerti', '2015-04-14 11:08:29.222882', NULL, NULL)
;
INSERT INTO public.code(group_code_id, code_id, sort_order, use_flag, code_type, code_name, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('RESULT_TYPE', '10', 10, 'Y', 'SYSTEM', '외국인등록번호', '외국인등록번호', 'easycerti', '2015-04-14 11:08:29.222882', NULL, NULL)
;

-- danger_status --

INSERT INTO public.danger_status(dng_cd, dng_val)
  VALUES('1', 100)
;
INSERT INTO public.danger_status(dng_cd, dng_val)
  VALUES('2', 98)
;
INSERT INTO public.danger_status(dng_cd, dng_val)
  VALUES('3', 77)
;
INSERT INTO public.danger_status(dng_cd, dng_val)
  VALUES('4', 70)
;
INSERT INTO public.danger_status(dng_cd, dng_val)
  VALUES('5', 60)
;

-- department --

INSERT INTO public.department(dept_id, parent_dept_id, dept_name, use_flag, sort_order, dept_depth, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('DEPT00002', 'DEPT00001', '솔루션 개발실', 'Y', NULL, NULL, '솔루션 개발실', 'easycerti', '2015-04-13 08:40:07.0', NULL, NULL)
;
INSERT INTO public.department(dept_id, parent_dept_id, dept_name, use_flag, sort_order, dept_depth, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('DEPT00003', 'DEPT00001', '경영기획팀', 'Y', NULL, NULL, '경영기획팀', 'easycerti', '2015-04-13 08:40:21.0', NULL, NULL)
;
INSERT INTO public.department(dept_id, parent_dept_id, dept_name, use_flag, sort_order, dept_depth, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('DEPT00004', 'DEPT00002', '서비스 개발팀', 'Y', NULL, NULL, '서비스 개발팀', 'admin', '2015-04-14 17:01:37.0', NULL, NULL)
;
INSERT INTO public.department(dept_id, parent_dept_id, dept_name, use_flag, sort_order, dept_depth, description, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('DEPT00001', 'DEPT00000', '이지서티', 'Y', NULL, NULL, '최상위부서', 'easycerti', '2015-04-13 08:40:07.0', 'admin', '2015-04-20 14:08:58.0')
;

-- menu --

INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00101', '스케쥴러', 'MENU00100', '/scheduler/list.html', 2, '스케쥴러', 'Y', 1, 'easycerti', '2015-04-10 10:53:32.0', 'easycerti', '2015-06-27 15:13:09.548138')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00102', '스케쥴상세', 'MENU00101', '/scheduler/detail.html', 3, '스케쥴상세', 'Y', 1, 'easycerti', '2015-04-10 10:53:32.0', 'easycerti', '2015-06-27 15:13:09.548138')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00105', '표준정의', 'MENU00100', '/control/samples.html', 2, '샘플', 'Y', 5, 'easycerti', '2015-04-10 10:53:32.0', 'easycerti', '2015-07-04 17:13:55.753919')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00042', 'DB접근이력조회', 'MENU00040', '/dbAccessInq/list.html', 2, 'DB로그조회', 'Y', 2, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-06-30 20:32:32.797296')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00060', '소명관리', 'MENU00000', '/callingDemand/list.html', 1, '소명관리', 'Y', 5, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-07-06 09:35:17.827724')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00043', '오남용현황조회', 'MENU00050', '/extrtCondbyInq/list.html', 2, '추출조건별조회', 'Y', 1, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-06-30 20:56:20.711227')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00041', '개인정보조회이력', 'MENU00040', '/allLogInq/list.html', 2, '전체로그조회', 'Y', 1, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-06-30 20:58:41.559908')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00081', '개인정보식별', 'MENU00080', '/indvinfoTypeSetup/list.html', 2, '개인정보유형설정', 'Y', 1, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-06-30 21:05:38.200429')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00087', '개인정보식별상세', 'MENU00081', '/indvinfoTypeSetup/detail.html', 3, '개인정보유형상세', 'Y', 1, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-06-30 21:05:53.30583')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00071', '기간별유형통계', 'MENU00070', '/statistics/list_perdbyTypeAnals.html', 2, '기간별유형분석', 'Y', 1, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-06-30 21:14:19.547357')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00072', '시스템별유형통계', 'MENU00070', '/statistics/list_sysbyTypeAnals.html', 2, '시스템별유형분석', 'Y', 2, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-06-30 21:14:26.894135')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00073', '부서별유형통계', 'MENU00070', '/statistics/list_deptbyTypeAnals.html', 2, '부서별유형분석', 'Y', 3, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-06-30 21:14:34.059992')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00052', '백업이력조회', 'MENU00040', '/backupHist/list.html', 2, '백업이력', 'Y', 4, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-07-06 09:39:08.321716')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00000', 'ROOT', 'MENU', NULL, 0, '최상위메뉴', 'Y', 1, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-04-10 10:53:32.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00023', '관리자상세', 'MENU00016', '/adminUserMngt/detail.html', 3, '관리자상세', 'Y', 1, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-04-10 10:53:32.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00070', '통계및보고', 'MENU00000', '/statistics/list_perdbyTypeAnals.html', 1, '통계', 'Y', 3, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-05-18 15:53:21.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00061', '전체조회', 'MENU00060', '/calling/list.html', 2, '전체조회', 'Y', 2, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-06-21 12:24:31.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00021', '권한상세', 'MENU00013', '/authMngt/detail.html', 3, '권한상세', 'Y', 1, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-04-10 10:53:32.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00013', '권한관리', 'MENU00010', '/authMngt/list.html', 2, '권한관리', 'Y', 3, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-04-10 10:53:32.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00012', '메뉴관리', 'MENU00010', '/menuMngt/list.html', 2, '메뉴관리', 'Y', 2, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-04-10 10:53:32.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00066', '소명(사용자)상세', 'MENU00063', '/callingReplyUser/detail.psm', 2, '소명(사용자)상세', 'Y', 1, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-05-14 22:00:26.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00063', '소명(사용자)', 'MENU00000', '/callingReplyUser/list.psm', 1, '소명(사용자)', 'Y', 8, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-05-18 15:53:43.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00017', 'AGENT관리', 'MENU00010', '/agentMngt/list.html', 2, 'AGENT관리', 'Y', 8, 'easycerti', '2015-04-10 10:53:32.0', 'easycerti', '2015-05-18 15:47:21.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00016', '관리자관리', 'MENU00010', '/adminUserMngt/list.html', 2, '관리자관리', 'Y', 6, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-04-10 10:53:32.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00019', '그룹코드상세', 'MENU00011', '/groupCodeMngt/detail.html', 3, '그룹코드상세', 'Y', 1, 'easycerti', '2015-04-10 10:53:32.0', 'easycerti', '2015-04-20 15:47:03.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00020', '하위코드상세', 'MENU00011', '/codeMngt/detail.html', 3, '하위코드상세', 'Y', 2, 'easycerti', '2015-04-10 10:53:32.0', 'easycerti', '2015-04-20 15:47:07.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00046', '추출로그상세', 'MENU00043', '/extrtCondbyInq/detail.html', 3, '추출로그상세', 'Y', 1, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-04-10 10:53:32.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00045', 'DB로그상세', 'MENU00042', '/dbAccessInq/detail.html', 3, 'DB로그상세', 'Y', 1, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-04-10 10:53:32.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00022', '사원상세', 'MENU00015', '/empUserMngt/detail.html', 3, '사원상세', 'Y', 1, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-04-10 10:53:32.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00018', '상황판관리', 'MENU00010', NULL, 2, '상황판관리', 'Y', 8, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-04-10 10:53:32.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00082', '추출기준설정', 'MENU00080', '/extrtBaseSetup/list.html', 2, '추출기준설정', 'Y', 2, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-04-10 10:53:32.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00084', '휴일설정', 'MENU00080', '/holiSetup/list.html', 2, '휴일설정', 'Y', 4, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-04-10 10:53:32.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00088', '추출기준상세', 'MENU00082', '/extrtBaseSetup/detail.html', 3, '추출기준상세', 'Y', 1, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-04-10 10:53:32.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00089', 'URL상세', 'MENU00085', '/addUrlSetup/detail.html', 3, 'URL상세', 'Y', 1, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-04-10 10:53:32.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00083', '위험도기준설정', 'MENU00080', '/riskrateBaseSetup/list.html', 2, '위험도기준설정', 'Y', 3, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-04-10 10:53:32.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00085', 'URL설정', 'MENU00080', '/addUrlSetup/list.html', 2, 'URL설정', 'Y', 5, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-04-10 10:53:32.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00090', '태그상세', 'MENU00086', '/menuMappSetup/detail.html', 3, '태그상세', 'Y', 1, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-04-10 10:53:32.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00086', '메뉴매핑설정', 'MENU00080', '/menuMappSetup/list.html', 2, '태그설정', 'Y', 6, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-05-14 21:56:55.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00044', '전체로그상세', 'MENU00041', '/allLogInq/detail.html', 3, '전체로그상세', 'Y', 1, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-05-18 15:53:47.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00031', '대시보드&토폴로지', 'MENU00030', '/dashboard/init_dashboardView.html', 2, '대시보드', 'Y', 1, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-04-10 10:53:32.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00091', '휴일상세', 'MENU00084', '/holiSetup/detail.html', 3, '휴일상세', 'Y', 1, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-05-10 19:16:50.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00011', '코드관리', 'MENU00010', '/groupCodeMngt/list.html', 2, '코드관리', 'Y', 1, 'easycerti', '2015-04-10 10:53:32.0', 'easycerti', '2015-04-20 15:46:57.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00015', '사원관리', 'MENU00010', '/empUserMngt/list.html', 2, '사원관리', 'Y', 5, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-04-10 10:53:32.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00048', '위험도별조회상세', 'MENU00047', '/empDetailInq/detail.html', 3, '위험도별조회상세', 'Y', 1, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-04-10 10:53:32.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00064', '소명상세', 'MENU00061', '/calling/detail.html', 3, '소명상세', 'Y', 1, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-06-21 12:26:00.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00067', '소명요청', 'MENU00060', '/callingDemand/list.html', 2, '소명요청', 'Y', 1, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-06-21 12:25:05.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00062', '판정대기', 'MENU00060', '/callingJudge/list.html', 2, '판정대기', 'Y', 3, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-06-21 12:28:13.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00075', '위험지표분석', 'MENU00070', '/report/list_riskidxAnals.html', 2, '위험지표분석', 'Y', 5, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-05-14 22:53:46.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00077', 'IP위험도분석', 'MENU00070', '/report/list_ipRiskrateAnals.html', 2, 'IP위험도분석', 'Y', 6, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-05-14 22:53:53.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00076', '시간외위험도분석', 'MENU00070', '/report/list_ovtimeRiskrateAnals.html', 2, '시간외위험도분석', 'Y', 7, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-05-14 22:54:01.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00030', '모니터링', 'MENU00000', '/dashboard/init_dashboardView.html', 1, '상황판', 'Y', 4, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-05-18 15:53:25.0')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00050', '접근기록분석', 'MENU00000', '/extrtCondbyInq/list.html', 1, '이력관리', 'Y', 2, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-06-30 20:54:55.198996')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00047', '위험도기준조회', 'MENU00050', '/empDetailInq/list.html', 2, '위험도별조회', 'Y', 2, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-06-30 20:56:50.861667')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00014', '조직관리', 'MENU00010', '/departmentMngt/list.html', 2, '부서관리', 'Y', 4, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-06-30 20:57:09.354202')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00051', '감사이력', 'MENU00010', '/managerActHist/list.html', 2, '관리자행위이력', 'Y', 7, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-06-30 20:57:28.704029')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00040', '접근기록조회', 'MENU00000', '/allLogInq/list.html', 1, '통합검색', 'Y', 1, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-06-30 20:58:19.117287')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00074', '개인별유형통계', 'MENU00070', '/statistics/list_indvbyTypeAnals.html', 2, '개인별유형분석', 'Y', 4, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-06-30 21:14:43.217673')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00080', '정책설정관리', 'MENU00000', '/indvinfoTypeSetup/list.html', 1, '설정', 'Y', 6, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-06-30 20:34:23.881842')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00010', '환경설정', 'MENU00000', '', 1, '시스템관리', 'Y', 8, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-06-30 20:35:09.580293')
;
INSERT INTO public.menu(menu_id, menu_name, parent_menu_id, menu_url, menu_depth, description, use_flag, sort_order, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('MENU00100', '제어센터', 'MENU00000', '/scheduler/list.html', 1, '제어센터', 'Y', 9, 'easycerti', '2015-04-10 10:53:32.0', 'admin', '2015-07-04 16:49:27.690003')
;

-- menu_auth --

INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00000', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00012', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00013', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00015', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00016', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00002', 'MENU00061', 'admin', '2015-06-21 12:24:31.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00021', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00022', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00023', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00061', 'admin', '2015-06-21 12:24:31.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00031', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00086', 'admin', '2015-05-14 21:56:55.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00004', 'MENU00066', 'admin', '2015-05-14 22:00:26.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00067', 'admin', '2015-06-21 12:25:05.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00045', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00046', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00064', 'admin', '2015-06-21 12:26:00.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00075', 'admin', '2015-05-14 22:53:46.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00077', 'admin', '2015-05-14 22:53:53.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00076', 'admin', '2015-05-14 22:54:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00002', 'MENU00062', 'admin', '2015-06-21 12:28:13.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00082', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00083', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00084', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00085', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00062', 'admin', '2015-06-21 12:28:13.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00088', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00089', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00090', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00017', 'easycerti', '2015-05-18 15:47:21.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00070', 'admin', '2015-05-18 15:53:21.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00000', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00010', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00030', 'admin', '2015-05-18 15:53:25.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00013', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00014', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00015', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00016', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00017', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00021', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00022', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00023', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00030', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00031', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00040', 'easycerti', '2015-04-20 13:19:09.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00041', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00042', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00043', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00044', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00045', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00046', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00050', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00051', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00052', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00060', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00061', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00062', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00070', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00071', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00072', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00073', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00080', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00081', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00082', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00083', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00084', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00085', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00086', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00087', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00088', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00089', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00090', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00100', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00011', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00012', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00004', 'MENU00063', 'admin', '2015-05-18 15:53:43.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00044', 'admin', '2015-05-18 15:53:47.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00019', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00020', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00091', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00074', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00047', 'easycerti', '2015-05-05 18:47:08.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00048', 'easycerti', '2015-05-05 18:47:08.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00048', 'easycerti', '2015-05-05 18:47:08.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00091', 'admin', '2015-05-10 19:16:50.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00002', 'MENU00000', 'admin', '2015-05-12 15:06:22.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00075', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00077', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00076', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00067', 'admin', '2015-06-21 12:25:05.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00002', 'MENU00067', 'admin', '2015-06-21 12:25:05.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00064', 'admin', '2015-06-21 12:26:00.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00003', 'MENU00101', 'easycerti', '2015-06-27 15:13:09.522923', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00002', 'MENU00101', 'easycerti', '2015-06-27 15:13:09.5312', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00101', 'easycerti', '2015-06-27 15:13:09.539487', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00102', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00101', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00042', 'admin', '2015-06-30 20:32:32.797296', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00080', 'admin', '2015-06-30 20:34:23.881842', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00010', 'admin', '2015-06-30 20:35:09.580293', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00050', 'admin', '2015-06-30 20:54:55.198996', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00043', 'admin', '2015-06-30 20:56:20.711227', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00047', 'admin', '2015-06-30 20:56:50.861667', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00014', 'admin', '2015-06-30 20:57:09.354202', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00051', 'admin', '2015-06-30 20:57:28.704029', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00040', 'admin', '2015-06-30 20:58:19.117287', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00041', 'admin', '2015-06-30 20:58:41.559908', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00081', 'admin', '2015-06-30 21:05:38.200429', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00087', 'admin', '2015-06-30 21:05:53.30583', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00071', 'admin', '2015-06-30 21:14:19.547357', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00072', 'admin', '2015-06-30 21:14:26.894135', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00073', 'admin', '2015-06-30 21:14:34.059992', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00074', 'admin', '2015-06-30 21:14:43.217673', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00000', 'MENU00105', 'easycerti', '2015-04-17 11:17:01.0', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00002', 'MENU00060', 'admin', '2015-07-06 09:35:17.827724', NULL, NULL)
;
INSERT INTO public.menu_auth(auth_id, menu_id, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('AUTH00001', 'MENU00060', 'admin', '2015-07-06 09:35:17.827724', NULL, NULL)
;

-- role --

INSERT INTO public.role(role_id, role_name, use_flag, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('ROLE00003', '운영자', 'Y', 'admin', '2014-03-05 17:36:23.0', NULL, NULL)
;
INSERT INTO public.role(role_id, role_name, use_flag, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('ROLE00004', 'Role_1-1', 'Y', 'admin', '2015-03-31 13:02:46.616487', 'admin', '2015-04-14 17:01:08.88625')
;
INSERT INTO public.role(role_id, role_name, use_flag, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('ROLE00002', '운영관리자', 'Y', 'admin', '2014-03-04 14:40:20.0', NULL, NULL)
;
INSERT INTO public.role(role_id, role_name, use_flag, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('ROLE00001', '관리자', 'N', 'admin', '2014-03-04 14:40:20.0', 'admin', '2015-04-17 11:20:31.615172')
;
INSERT INTO public.role(role_id, role_name, use_flag, insert_user_id, insert_datetime, update_user_id, update_datetime)
  VALUES('ROLE00005', 'ROLE00005', 'Y', 'admin', '2015-04-20 10:11:05.901965', NULL, NULL)
;

-- ruletbl --

INSERT INTO public.ruletbl(rule_seq, rule_nm, rule_desc, log_delimiter, dng_val, script, limit_cnt, is_realtime_extract, realtime_extract_time, use_yn, user_limit_cnt_yn)
  VALUES(4002, 'IP 도용', '동일한 IP에서 다른 ID로 접근한 경우', 'BA', 20, 'INSERT INTO RULE_EXTRACT_TEMP(OCCR_DT, LOG_SEQ, RULE_CD, EMP_USER_ID)
SELECT BL.PROC_DATE, BL.LOG_SEQ, ${RULE_CD}, BL.EMP_USER_ID
FROM BIZ_LOG BL, (
	SELECT PROC_DATE, MIN(LOG_SEQ) AS LOG_SEQ, EMP_USER_ID, USER_IP
	FROM BIZ_LOG BL
	WHERE BL.PROC_DATE ${TODAY}
		AND (USER_IP, SYSTEM_SEQ) IN (
			SELECT USER_IP, SYSTEM_SEQ
			FROM BIZ_LOG
			WHERE PROC_DATE ${TODAY}
				AND EMP_USER_ID != USER_IP
			GROUP BY USER_IP, SYSTEM_SEQ
			HAVING COUNT(DISTINCT EMP_USER_ID) >= 2
		)
		GROUP BY PROC_DATE, EMP_USER_ID, USER_IP
) T
WHERE BL.PROC_DATE = T.PROC_DATE
	AND BL.LOG_SEQ = T.LOG_SEQ', 0, 'Y', '20150715101101', 'Y', 'Y')
;
INSERT INTO public.ruletbl(rule_seq, rule_nm, rule_desc, log_delimiter, dng_val, script, limit_cnt, is_realtime_extract, realtime_extract_time, use_yn, user_limit_cnt_yn)
  VALUES(3003, '특정 시간대 업무 처리', '업무 시간 외 임계치 이상 접근한 경우', 'BA', 20, 'CREATE TABLE BIZ_LOG_EXTRACT_TEMP_${RULE_CD} AS
SELECT BL.*
FROM BIZ_LOG BL, HOLIDAY H
WHERE BL.PROC_DATE = H.HOLI_DT
	AND H.HOLI_DT ${TODAY}
	AND H.HOLI_YN = ''N''
	AND SUBSTR(BL.PROC_TIME, 0, 5) <= H.WORK_START_TM 
	AND H.WORK_END_TM >= SUBSTR(BL.PROC_TIME, 0, 5)
UNION
SELECT BL.*
FROM BIZ_LOG BL, HOLIDAY H
WHERE BL.PROC_DATE = H.HOLI_DT
	AND H.HOLI_DT ${TODAY} 
	AND H.HOLI_YN = ''Y''
##SQL_DELIMETER##
INSERT INTO BIZ_LOG_EXTRACT_TEMP
SELECT *
FROM BIZ_LOG_EXTRACT_TEMP_${RULE_CD}
WHERE EMP_USER_ID IN (
	SELECT EMP_USER_ID
	FROM BIZ_LOG_EXTRACT_TEMP_${RULE_CD}
	GROUP BY EMP_USER_ID
	HAVING COUNT(*) >= ${LIMIT}
)
##SQL_DELIMETER##
DROP TABLE BIZ_LOG_EXTRACT_TEMP_${RULE_CD}
##SQL_DELIMETER##
INSERT INTO RULE_EXTRACT_TEMP(OCCR_DT, LOG_SEQ, RULE_CD, EMP_USER_ID)
SELECT PROC_DATE, LOG_SEQ, ${RULE_CD}, EMP_USER_ID
FROM BIZ_LOG_EXTRACT_TEMP', 5, 'Y', '20150715101101', 'Y', 'Y')
;
INSERT INTO public.ruletbl(rule_seq, rule_nm, rule_desc, log_delimiter, dng_val, script, limit_cnt, is_realtime_extract, realtime_extract_time, use_yn, user_limit_cnt_yn)
  VALUES(1202, '(운전면허번호) 다수 사용자의 특정인 개인정보 접근', '다수의 사용자가 동일한 개인정보(운전면허번호)에 대하여 임계치 이상 접근한 경우', 'BA', 5, 'INSERT INTO RULE_EXTRACT_TEMP(OCCR_DT, LOG_SEQ, RULE_CD, EMP_USER_ID)
SELECT PROC_DATE, LOG_SEQ, ${RULE_CD}, EMP_USER_ID
FROM (
	SELECT BL.PROC_DATE, MAX(BL.LOG_SEQ) AS LOG_SEQ, BL.EMP_USER_ID, BLR.RESULT_CONTENT
	FROM BIZ_LOG BL, BIZ_LOG_RESULT BLR
	WHERE BL.PROC_DATE = BLR.PROC_DATE
		AND BL.LOG_SEQ = BLR.LOG_SEQ
		AND BL.PROC_DATE ${TODAY}
		AND BLR.RESULT_CONTENT IN (
			SELECT BLR.RESULT_CONTENT
			FROM BIZ_LOG BL, BIZ_LOG_RESULT BLR
			WHERE BL.PROC_DATE = BLR.PROC_DATE
				AND BL.LOG_SEQ = BLR.LOG_SEQ
				AND BL.PROC_DATE ${TODAY}
				AND BLR.RESULT_TYPE = ''2''
			GROUP BY BLR.RESULT_CONTENT
			HAVING COUNT(DISTINCT BL.EMP_USER_ID) >= ${LIMIT}
		)
	GROUP BY BL.PROC_DATE, BL.EMP_USER_ID, BLR.RESULT_CONTENT
	ORDER BY BLR.RESULT_CONTENT, BL.EMP_USER_ID
) T1
GROUP BY PROC_DATE, LOG_SEQ, EMP_USER_ID', 3, 'Y', '20150715101101', 'Y', 'Y')
;
INSERT INTO public.ruletbl(rule_seq, rule_nm, rule_desc, log_delimiter, dng_val, script, limit_cnt, is_realtime_extract, realtime_extract_time, use_yn, user_limit_cnt_yn)
  VALUES(1104, '(계좌번호) 특정인 개인정보 접근', '동일한 개인정보(계좌번호)에 대하여 임계치 이상 접근한 경우', 'BA', 5, 'INSERT INTO RULE_EXTRACT_TEMP(OCCR_DT, LOG_SEQ, RULE_CD, EMP_USER_ID)
SELECT BL.PROC_DATE AS OCCR_DT, BL.LOG_SEQ AS LOG_SEQ, ''${RULE_CD}'' AS RULE_CD, BL.EMP_USER_ID AS EMP_USER_ID
FROM BIZ_LOG BL, BIZ_LOG_RESULT BLR
WHERE BL.PROC_DATE = BLR.PROC_DATE
	AND BL.LOG_SEQ = BLR.LOG_SEQ
	AND BL.PROC_DATE ${TODAY}
	AND (BL.EMP_USER_ID, BLR.RESULT_CONTENT) IN (
		SELECT BL.EMP_USER_ID, BLR.RESULT_CONTENT
		FROM BIZ_LOG BL, BIZ_LOG_RESULT BLR
		WHERE BL.PROC_DATE = BLR.PROC_DATE
			AND BL.LOG_SEQ = BLR.LOG_SEQ
			AND BL.PROC_DATE ${TODAY}
			AND BLR.RESULT_TYPE = ''9''
		GROUP BY BL.EMP_USER_ID, BLR.RESULT_CONTENT
		HAVING COUNT(*) >= ${LIMIT}
)
GROUP BY BL.PROC_DATE, BL.LOG_SEQ
ORDER BY BL.LOG_SEQ', 3, 'Y', '20150715101101', 'Y', 'Y')
;
INSERT INTO public.ruletbl(rule_seq, rule_nm, rule_desc, log_delimiter, dng_val, script, limit_cnt, is_realtime_extract, realtime_extract_time, use_yn, user_limit_cnt_yn)
  VALUES(4001, 'ID 도용', '동일한 ID가 다른 IP에서 접근한 경우', 'BA', 10, 'INSERT INTO RULE_EXTRACT_TEMP(OCCR_DT, LOG_SEQ, RULE_CD, EMP_USER_ID)
SELECT BL.PROC_DATE, BL.LOG_SEQ, ${RULE_CD}, BL.EMP_USER_ID
FROM BIZ_LOG BL, (
	SELECT PROC_DATE, MIN(LOG_SEQ) AS LOG_SEQ, EMP_USER_ID, USER_IP
	FROM BIZ_LOG BL
	WHERE BL.PROC_DATE ${TODAY}
		AND (EMP_USER_ID, SYSTEM_SEQ) IN (
			SELECT EMP_USER_ID, SYSTEM_SEQ
			FROM BIZ_LOG
			WHERE PROC_DATE ${TODAY}
			GROUP BY EMP_USER_ID, SYSTEM_SEQ
			HAVING COUNT(DISTINCT USER_IP) >= 2
		)
	GROUP BY PROC_DATE, EMP_USER_ID, USER_IP
) T
WHERE BL.PROC_DATE = T.PROC_DATE 
	AND BL.LOG_SEQ = T.LOG_SEQ', 0, 'Y', '20150715101101', 'Y', 'Y')
;
INSERT INTO public.ruletbl(rule_seq, rule_nm, rule_desc, log_delimiter, dng_val, script, limit_cnt, is_realtime_extract, realtime_extract_time, use_yn, user_limit_cnt_yn)
  VALUES(1103, '(여권번호) 특정인 개인정보 접근', '동일한 개인정보(여권번호)에 대하여 임계치 이상 접근한 경우', 'BA', 5, 'INSERT INTO RULE_EXTRACT_TEMP(OCCR_DT, LOG_SEQ, RULE_CD, EMP_USER_ID)
SELECT BL.PROC_DATE AS OCCR_DT, BL.LOG_SEQ AS LOG_SEQ, ''${RULE_CD}'' AS RULE_CD, BL.EMP_USER_ID AS EMP_USER_ID
FROM BIZ_LOG BL, BIZ_LOG_RESULT BLR
WHERE BL.PROC_DATE = BLR.PROC_DATE
	AND BL.LOG_SEQ = BLR.LOG_SEQ
	AND BL.PROC_DATE ${TODAY}
	AND (BL.EMP_USER_ID, BLR.RESULT_CONTENT) IN (
		SELECT BL.EMP_USER_ID, BLR.RESULT_CONTENT
		FROM BIZ_LOG BL, BIZ_LOG_RESULT BLR
		WHERE BL.PROC_DATE = BLR.PROC_DATE
			AND BL.LOG_SEQ = BLR.LOG_SEQ
			AND BL.PROC_DATE ${TODAY}
			AND BLR.RESULT_TYPE = ''3''
		GROUP BY BL.EMP_USER_ID, BLR.RESULT_CONTENT
		HAVING COUNT(*) >= ${LIMIT}
	)
GROUP BY BL.PROC_DATE, BL.LOG_SEQ
ORDER BY BL.LOG_SEQ', 3, 'Y', '20150715101102', 'Y', 'Y')
;
INSERT INTO public.ruletbl(rule_seq, rule_nm, rule_desc, log_delimiter, dng_val, script, limit_cnt, is_realtime_extract, realtime_extract_time, use_yn, user_limit_cnt_yn)
  VALUES(1203, '(여권번호) 다수 사용자의 특정인 개인정보 접근', '다수의 사용자가 동일한 개인정보(여권번호)에 대하여 임계치 이상 접근한 경우', 'BA', 5, 'INSERT INTO RULE_EXTRACT_TEMP(OCCR_DT, LOG_SEQ, RULE_CD, EMP_USER_ID)
SELECT PROC_DATE, LOG_SEQ, ${RULE_CD}, EMP_USER_ID
FROM (
	SELECT BL.PROC_DATE, MAX(BL.LOG_SEQ) AS LOG_SEQ, BL.EMP_USER_ID, BLR.RESULT_CONTENT
	FROM BIZ_LOG BL, BIZ_LOG_RESULT BLR
	WHERE BL.PROC_DATE = BLR.PROC_DATE
		AND BL.LOG_SEQ = BLR.LOG_SEQ
		AND BL.PROC_DATE ${TODAY}
		AND BLR.RESULT_CONTENT IN (
			SELECT BLR.RESULT_CONTENT
			FROM BIZ_LOG BL, BIZ_LOG_RESULT BLR
			WHERE BL.PROC_DATE = BLR.PROC_DATE
				AND BL.LOG_SEQ = BLR.LOG_SEQ
				AND BL.PROC_DATE ${TODAY}
				AND BLR.RESULT_TYPE = ''3''
			GROUP BY BLR.RESULT_CONTENT
			HAVING COUNT(DISTINCT BL.EMP_USER_ID) >= ${LIMIT}
		)
	GROUP BY BL.PROC_DATE, BL.EMP_USER_ID, BLR.RESULT_CONTENT
	ORDER BY BLR.RESULT_CONTENT, BL.EMP_USER_ID
) T1
GROUP BY PROC_DATE, LOG_SEQ, EMP_USER_ID', 3, 'Y', '20150715101102', 'Y', 'N')
;
INSERT INTO public.ruletbl(rule_seq, rule_nm, rule_desc, log_delimiter, dng_val, script, limit_cnt, is_realtime_extract, realtime_extract_time, use_yn, user_limit_cnt_yn)
  VALUES(1205, '(신용카드) 다수 사용자의 특정인 개인정보 접근', '다수의 사용자가 동일한 개인정보(신용카드)에 대하여 임계치 이상 접근한 경우', 'BA', 5, 'INSERT INTO RULE_EXTRACT_TEMP(OCCR_DT, LOG_SEQ, RULE_CD, EMP_USER_ID)
SELECT PROC_DATE, LOG_SEQ, ${RULE_CD}, EMP_USER_ID
FROM (
	SELECT BL.PROC_DATE, MAX(BL.LOG_SEQ) AS LOG_SEQ, BL.EMP_USER_ID, BLR.RESULT_CONTENT
	FROM BIZ_LOG BL, BIZ_LOG_RESULT BLR
	WHERE BL.PROC_DATE = BLR.PROC_DATE
		AND BL.LOG_SEQ = BLR.LOG_SEQ
		AND BL.PROC_DATE ${TODAY}
		AND BLR.RESULT_CONTENT IN (
			SELECT BLR.RESULT_CONTENT
			FROM BIZ_LOG BL, BIZ_LOG_RESULT BLR
			WHERE BL.PROC_DATE = BLR.PROC_DATE
				AND BL.LOG_SEQ = BLR.LOG_SEQ
				AND BL.PROC_DATE ${TODAY}
				AND BLR.RESULT_TYPE = ''4''
			GROUP BY BLR.RESULT_CONTENT
			HAVING COUNT(DISTINCT BL.EMP_USER_ID) >= ${LIMIT}
		)
	GROUP BY BL.PROC_DATE, BL.EMP_USER_ID, BLR.RESULT_CONTENT
	ORDER BY BLR.RESULT_CONTENT, BL.EMP_USER_ID
) T1
GROUP BY PROC_DATE, LOG_SEQ, EMP_USER_ID', 3, 'Y', '20150715101102', 'Y', 'Y')
;
INSERT INTO public.ruletbl(rule_seq, rule_nm, rule_desc, log_delimiter, dng_val, script, limit_cnt, is_realtime_extract, realtime_extract_time, use_yn, user_limit_cnt_yn)
  VALUES(1101, '(주민등록번호) 특정인 개인정보 접근', '동일한 개인정보(주민등록번호)에 대하여 임계치 이상 접근한 경우', 'BA', 20, 'INSERT INTO RULE_EXTRACT_TEMP(OCCR_DT, LOG_SEQ, RULE_CD, EMP_USER_ID)
SELECT BL.PROC_DATE AS OCCR_DT, BL.LOG_SEQ AS LOG_SEQ, ''${RULE_CD}'' AS RULE_CD, BL.EMP_USER_ID AS EMP_USER_ID
FROM BIZ_LOG BL, BIZ_LOG_RESULT BLR
WHERE BL.PROC_DATE = BLR.PROC_DATE
	AND BL.LOG_SEQ = BLR.LOG_SEQ
	AND BL.PROC_DATE ${TODAY}
	AND (BL.EMP_USER_ID, BLR.RESULT_CONTENT) IN (
		SELECT BL.EMP_USER_ID, BLR.RESULT_CONTENT
		FROM BIZ_LOG BL, BIZ_LOG_RESULT BLR
		WHERE BL.PROC_DATE = BLR.PROC_DATE
			AND BL.LOG_SEQ = BLR.LOG_SEQ
			AND BL.PROC_DATE ${TODAY}
			AND BLR.RESULT_TYPE = ''1''
		GROUP BY BL.EMP_USER_ID, BLR.RESULT_CONTENT
		HAVING COUNT(*) >= ${LIMIT}
	)
GROUP BY BL.PROC_DATE, BL.LOG_SEQ
ORDER BY BL.LOG_SEQ', 3, 'Y', '20150715101102', 'Y', 'Y')
;
INSERT INTO public.ruletbl(rule_seq, rule_nm, rule_desc, log_delimiter, dng_val, script, limit_cnt, is_realtime_extract, realtime_extract_time, use_yn, user_limit_cnt_yn)
  VALUES(1201, '(주민등록번호) 다수 사용자의 특정인 개인정보 접근', '다수의 사용자가 동일한 개인정보(주민등록번호)에 대하여 임계치 이상 접근한 경우', 'BA', 10, 'INSERT INTO RULE_EXTRACT_TEMP(OCCR_DT, LOG_SEQ, RULE_CD, EMP_USER_ID)
SELECT PROC_DATE, LOG_SEQ, ${RULE_CD}, EMP_USER_ID
FROM (
	SELECT BL.PROC_DATE, MAX(BL.LOG_SEQ) AS LOG_SEQ, BL.EMP_USER_ID, BLR.RESULT_CONTENT
	FROM BIZ_LOG BL, BIZ_LOG_RESULT BLR
	WHERE BL.PROC_DATE = BLR.PROC_DATE
		AND BL.LOG_SEQ = BLR.LOG_SEQ
		AND BL.PROC_DATE ${TODAY}
		AND BLR.RESULT_CONTENT IN (
			SELECT BLR.RESULT_CONTENT
			FROM BIZ_LOG BL, BIZ_LOG_RESULT BLR
			WHERE BL.PROC_DATE = BLR.PROC_DATE
				AND BL.LOG_SEQ = BLR.LOG_SEQ
				AND BL.PROC_DATE ${TODAY}
				AND BLR.RESULT_TYPE = ''1''
			GROUP BY BLR.RESULT_CONTENT
			HAVING COUNT(DISTINCT BL.EMP_USER_ID) >= ${LIMIT}
		)
	GROUP BY BL.PROC_DATE, BL.EMP_USER_ID, BLR.RESULT_CONTENT
	ORDER BY BLR.RESULT_CONTENT, BL.EMP_USER_ID
) T1
GROUP BY PROC_DATE, LOG_SEQ, EMP_USER_ID', 3, 'Y', '20150715101102', 'Y', 'Y')
;
INSERT INTO public.ruletbl(rule_seq, rule_nm, rule_desc, log_delimiter, dng_val, script, limit_cnt, is_realtime_extract, realtime_extract_time, use_yn, user_limit_cnt_yn)
  VALUES(1204, '(계좌번호) 다수 사용자의 특정인 개인정보 접근', '다수의 사용자가 동일한 개인정보(계좌번호)에 대하여 임계치 이상 접근한 경우', 'BA', 5, 'INSERT INTO RULE_EXTRACT_TEMP(OCCR_DT, LOG_SEQ, RULE_CD, EMP_USER_ID)
SELECT PROC_DATE, LOG_SEQ, ${RULE_CD}, EMP_USER_ID
FROM (
	SELECT BL.PROC_DATE, MAX(BL.LOG_SEQ) AS LOG_SEQ, BL.EMP_USER_ID, BLR.RESULT_CONTENT 
	FROM BIZ_LOG BL, BIZ_LOG_RESULT BLR
	WHERE BL.PROC_DATE = BLR.PROC_DATE
		AND BL.LOG_SEQ = BLR.LOG_SEQ
		AND BL.PROC_DATE ${TODAY}
		AND BLR.RESULT_CONTENT IN (
			SELECT BLR.RESULT_CONTENT
			FROM BIZ_LOG BL, BIZ_LOG_RESULT BLR
			WHERE BL.PROC_DATE = BLR.PROC_DATE
				AND BL.LOG_SEQ = BLR.LOG_SEQ
				AND BL.PROC_DATE ${TODAY}
				AND BLR.RESULT_TYPE = ''9''
			GROUP BY BLR.RESULT_CONTENT
			HAVING COUNT(DISTINCT BL.EMP_USER_ID) >= ${LIMIT}
		)
	GROUP BY BL.PROC_DATE, BL.EMP_USER_ID, BLR.RESULT_CONTENT
	ORDER BY BLR.RESULT_CONTENT, BL.EMP_USER_ID
) T1
GROUP BY PROC_DATE, LOG_SEQ, EMP_USER_ID', 3, 'Y', '20150715101102', 'Y', 'Y')
;

INSERT INTO public.regular_expression_mng(privacy_seq, privacy_type, privacy_desc, regular_expression, use_flag)
  VALUES(11, '11', '엑셀다운로드', NULL, 'Y')
;
INSERT INTO public.regular_expression_mng(privacy_seq, privacy_type, privacy_desc, regular_expression, use_flag)
  VALUES(3, '3', '여권번호', '(?<![a-zA-Z])((AY|BS|CB|CS|DG|EP|GB|GD|GG|GJ|GK|GN|GP|GS|GW|GY|HD|IC|JB|JG|JJ|JN|JR|JU|KJ|KN|KR|KY|MP|NW|SC|SJ|SM|SQ|SR|TJ|TM|UL|YP|YS|S)\\d{7}|(AE|AF|AG|AN|AQ|AR|AU|AV|BN|BO|BR|BT|BU|CA|CD|CG|CH|CL|CN|CO|CP|CZ|DJ|DK|DM|EQ|ER|ES|ET|FJ|FK|FN|FR|FU|GA|GE|GH|GM|GR|GU|GV|GZ|HC|HG|HI|HK|HL|HO|HU|ID|IR|IS|IT|IV|JA|JO|KA|KO|KU|KZ|LA|LB|LP|LS|MA|MG|MN|MO|MU|MZ|NA|ND|NG|NI|NJ|NP|NR|NY|NZ|OC|OM|OS|PA|PD|PG|PH|PM|PN|PO|PU|QD|QT|RA|RF|RM|SA|SB|SD|SE|SF|SG|SH|SK|SL|SN|SO|SP|SS|ST|SW|SY|SZ|TH|TN|TP|TR|TU|TZ|UA|UK|UN|UR|US|UZ|VC|VE|VL|VT|VZ|YG|YO|ZB)\\d{7}|(M|S|D|G)\\d{8})(?!(\\d+|.\\w{3}))', 'Y')
;
INSERT INTO public.regular_expression_mng(privacy_seq, privacy_type, privacy_desc, regular_expression, use_flag)
  VALUES(6, '6', '전화번호', '(?<!\\d)0(2|31|32|33|41|42|43|63|51|52|53|55|61|62|54|64)(( ?-? ?)(\\d{2}( ?-? ?| )(\\d{3}|\\d{4})|\\d{3}( ?-? ?| )(\\d{3}|\\d{4})|\\d{4}( ?-? ?| )(\\d{3}|\\d{4}))|( ?\\) ?)(\\d{2}( ?-? ?| )(\\d{3}|\\d{4})|\\d{3}( ?-? ?| )(\\d{3}|\\d{4})|\\d{4}( ?-? ?| )(\\d{3}|\\d{4})))(?!\\d+)', 'Y')
;
INSERT INTO public.regular_expression_mng(privacy_seq, privacy_type, privacy_desc, regular_expression, use_flag)
  VALUES(12, '12', '외국인번호', 'asdf', 'Y')
;
INSERT INTO public.regular_expression_mng(privacy_seq, privacy_type, privacy_desc, regular_expression, use_flag)
  VALUES(10, '10', '외국인등록번호', '(?<!\\d|\\.)\\d[\\s&nbsp;]*\\d[\\s&nbsp;]*(0[\\s&nbsp;]*[1-9]|1[\\s&nbsp;]*[0-2])[\\s&nbsp;]*(0[\\s&nbsp;]*[1-9]|[12][\\s&nbsp;]*[0-9]|3[\\s&nbsp;]*[0-1])((\\s)|(\\-)|(\\+)|(~)|(&nbsp;)|(<br[ /]*>))*[56789][\\s&nbsp;]*(\\d{1}[\\s&nbsp;]*){6}(?!\\d)', 'Y')
;
INSERT INTO public.regular_expression_mng(privacy_seq, privacy_type, privacy_desc, regular_expression, use_flag)
  VALUES(4, '4', '신용카드번호', '(?<!\\d)(30[0-5]\\d{1}|3095|35(2[89]|[3-8]\\d{1}|92)|4\\d{3}|60(11|48|60)|5(021|409)|51(00|55)|53(10|77|88)|55(21|51|8[59])|51(2[0346]|4[089]|6[034]|7[6-9])|62(0[0-28]|1[0246]|2[1-9]|[56]\\d{1}|8[1-8])|63(6[01]|96)|64[4-9]\\d{1}|65\\d{2}|9(035|289|350|490|5(30|58)|720|806|999)|94(0[0379]|1[0-25]|2[0156]|3[012569]|4[013-6]|5[025-8]|6[0-578]|[78][05]))( ?\\- ?| |­)\\d{4}\\14\\d{4}\\14\\d{4}(?!\\d+)|(?<!\\d)(30([0-5]\\d{1}|95)|3[689]\\d{2})( ?\\- ?| |­)\\d{6}\\15\\d{4}(?!\\d+)|(?<!\\d)(34|37)\\d{2}( ?\\- ?| |­)\\d{6}\\16\\d{5}(?!\\d+)', 'Y')
;
INSERT INTO public.regular_expression_mng(privacy_seq, privacy_type, privacy_desc, regular_expression, use_flag)
  VALUES(5, '5', '건강보험번호', '(?<!\\d)[1-8]( ?- ?| )\\d{10}(?!\\d+)', 'Y')
;
INSERT INTO public.regular_expression_mng(privacy_seq, privacy_type, privacy_desc, regular_expression, use_flag)
  VALUES(7, '7', '이메일', '(\\w+\\.)*\\w+@(\\w+\\.)+[A-Za-z]+', 'Y')
;
INSERT INTO public.regular_expression_mng(privacy_seq, privacy_type, privacy_desc, regular_expression, use_flag)
  VALUES(8, '8', '휴대폰번호', '(?<!\\d)01(0|1|6|7|8|9)(\\-?|\\))(\\d{4}|\\d{3})-?\\d{4}(?!\\d+)', 'Y')
;
INSERT INTO public.regular_expression_mng(privacy_seq, privacy_type, privacy_desc, regular_expression, use_flag)
  VALUES(9, '9', '계좌번호', '(?<!\\d)(\\d{3} ?- ?(0[^0]|1[1-357-9]|2\\d{1}|3[3-8]|4[0-79]|5[12489]|6[1-46-8]|7[03-57-9]|8[1-57]|9[39]) ?- ?\\d{5} ?- ?\\d{1}|1(0[1-4679]|1[1-46-8]|2[1347]|49|53) ?- ?\\d{4} ?- ?\\d{4} ?- ?\\d{2}|\\d{3} ?- ?\\d{8} ?- ?\\d{3}|(0(1[0139]|2[02]|3[1-5]|7[^8])|1(4[37]|(5|6)[1248])|270) ?- ?\\d{7} ?- ?\\d{1} ?- ?\\d{3}|\\d{3} ?- ?(1[1359]|2[027]|4[37]|(5|6)[1248]) ?- ?\\d{1} ?- ?\\d{4} ?- ?\\d{1}|\\d{3} ?- ?\\d{6} ?- ?(0[1-467]|1[4579]|2[08]|7[47]|8[1-3]|9[367]) ?- ?\\d{3}|\\d{3} ?- ?\\d{8} ?- ?93 ?- ?68\\d{1}|\\d{4} ?- ?(09|1[03]) ?- ?\\d{6} ?- ?\\d{1}|\\d{4} ?- ?(0[124-6]|1[02478]|2[148]|3[14]|4[5-739]|[57]9|8[016-8]) ?- ?\\d{6}|\\d{4} ?- ?(0[1-37]|2[35-7]|37|4[58]|5[1-36-8]|9[02]) ?- ?\\d{8}|9[1-46]\\d{1} ?- ?\\d{2} ?- ?\\d{7} ?- ?\\d{1}|\\d{3} ?- ?81 ?- ?\\d{7} ?- ?\\d{1}|\\d{3} ?- ?(0[1-578]|1[1-589]|[23]\\d{1}|4[1249]|5[1-8]|6[0-25-7]|9[01]) ?- ?\\d{7}|\\d{3} ?- ?(1[056]|[2379]0|4[689]|5[3-68]) ?- ?\\d{9}|\\d{3} ?- ?(04|1[13589]|2[2-469]|3[38]|7[3-5079]|87) ?- ?\\d{5} ?- ?\\d{3}|(6[0-3][01]|7(0[3-5079]|1[0-24-6])|8(1[01457]|42)|9[0-39]\\d{1}) ?- ?\\d{6} ?- ?\\d{3}|(1([0-5]\\d{1}|6[01]|7[05])|2(0[12689]|2[13]|3[094-6]|4[026-8]|7[0-25-7]|8(0|[2-4])|90)|9(4[3-579]|7[458]|8[1-5]|93)) ?- ?\\d{8} ?- ?\\d{1}|0 ?- ?\\d{11}|(0(28|31|4[36]|79|8[16-8])|3(0[124-6]|1[0247]|2[14]|34|4[579]|5[1294-6]|60|8[04]|9[48])) ?- ?\\d{4} ?- ?\\d{4} ?- ?\\d{1} ?- ?\\d{1}|\\d{6} ?- ?(0[1-6]|1[02-8]|2[148]|3[14]|4[0395-7]|5[124-69]|60|79|8[0146-8]|9[48]) ?- ?\\d{6}|\\d{3} ?- ?(0[1-9]|1[1-9]|2[013-57-9]|3[02]|4[19]|53|6[0-24-689]|7[03]) ?- ?\\d{6} ?- ?\\d{1}|\\d{1}(0(0[234567]|20|4[0-367]|7[1-3]|9[36])|1(0[46-9]|1[04-6]|[289]1)) ?- ?\\d{3} ?- ?\\d{6}|\\d{3} ?- ?\\d{5} ?- ?\\d{1} ?- ?(0[1-467]|1[23568]|24|3[01389]|4[35]|5[278]|6[1-7]|82|9[25]) ?- ?\\d{3}|[05] ?- ?\\d{6} ?- ?\\d{2} ?- ?\\d{1}|\\d{2} ?- ?([0156]\\d{1}|2[01]|3[02-8]|4[0-68]|[78][013-8]|9[1-69]) ?- ?\\d{5} ?- ?\\d{1}|\\d{3} ?- ?\\d{5} ?- ?(0[135-7]|1[135689]|2[0193-7]|3[137-9]|4[1-3]|5[183-5]|6[1367]|71|8[1-4]|9[0-39]) ?- ?\\d{1} ?- ?\\d{2}|\\d{3} ?- ?\\d{5} ?- ?(0[135-7]|1[135689]|2[0193-7]|3[137-9]|4[1-3]|5[183-5]|6[1367]|71|8[1-4]|9[0-39]) ?- ?\\d{1}|9 ?- ?\\d{9} ?- ?\\d{2}|(0[135-7]|1[135689]|2[0193-7]|3[137-9]|4[1-3]|5[183-5]|6[1367]|71|8[1-4]|9[0-39]) ?- ?\\d{2} ?- ?\\d{9}|\\d{4} ?- ?9[1-3] ?- ?\\d{7} ?- ?\\d{1}|9 ?- ?\\d{2} ?- ?\\d{2} ?- ?\\d{9}|5(0[12458]|19|2[0-2578]|6[0-2468]|7[03]) ?- ?\\d{2} ?- ?\\d{6} ?- ?\\d{1}|\\d{3} ?- ?(0[1256]|1[19]) ?- ?\\d{2} ?- ?\\d{5} ?- ?\\d{1}|\\d{2} ?- ?(0[1-57]|1[3-7]|2[01]|5[5-9]|6[1-4]) ?- ?\\d{6}|9(00[2-5]|2(0[0257-9]|1[02])) ?- ?\\d{8} ?- ?\\d{1}|\\d{5} ?- ?1[23] ?- ?\\d{6}|\\d{5} ?- ?14 ?- ?\\d{7}|\\d{3} ?- ?\\d{2} ?- ?(13|2[1-3]|3[0-26-94]) ?- ?\\d{6} ?- ?\\d{1}| //\\d{4} ?- ?\\d{5} ?- ?\\d{3}|\\d{4} ?- ?\\d{10}|\\d{6} ?- ?\\d{6} ?- ?\\d{1} ?- ?(0[1-356]|1[2-8]|40|5[12])|\\d{3}(0[1-46-9]|1[1345]|24|3[19567]|42|89) ?- ?\\d{7}|\\d{3} ?- ?\\d{6} ?- ?\\d{3} ?- ?(0[124578]|13|2[1356]|5[03579]|94)|\\d{3} ?- ?9\\d{8} ?- ?(0[124578]|13|2[1356]|5[03579])|140 ?- ?9100(0022[6-9]|002[3-9]\\d{1}|00[3-9]\\d{2}|0[1-9]\\d{3}|[1-9]\\d{4}) ?- ?94|140 ?- ?910([1-7]|9)\\d{5} ?- ?94|140 ?- ?9108(0\\d{4}|1[0-7]\\d{3}|18[0-2]\\d{2}|183[0-5]\\d{1}|1836[0-4]|1842[6-9]|184[3-9]\\d{1}|18[5-9]\\d{2}|19\\d{3}|[2-9]\\d{4}) ?- ?94|140 ?- ?91([12]\\d{6}|300\\d{4}|301[0-7]\\d{3}|3018[0-3]\\d{2}|301840\\d{1}|3018410) ?- ?94|358 ?- ?91(0\\d{4}(59|[6-9]\\d{1})|0[^0][^0][^0][^0]([0-4]\\d{1}|5[0-8])|[1-46-9]\\d{6}) ?- ?94|358 ?- ?915([1-46-9])\\d{5} ?- ?94|358 ?- ?9155(000([0-3]\\d{1}|4[0-2])|[6-9]\\d{4}) ?- ?94|358 ?- ?91555([6-9]\\d{3}|5(9\\d{2}|8([7-9]\\d{1}|6[0-2]))) ?- ?94|358 ?- ?92(0\\d{6}|10([0-4]\\d{4}|5[0-4]\\d{3}|55([0-7]\\d{2}|8([0-2]\\d{1}|3[0-5])))) ?- ?94|\\d{3} ?- ?[1-467] ?- ?\\d{5} ?- ?\\d{1}|\\d{3} ?- ?(0[^0]|1[0-37]|2[013-8]|3[367]|4[0-6]|61|9[79]) ?- ?\\d{5} ?- ?\\d{1}|\\d{3} ?- ?82 ?- ?\\d{8}|\\d{3} ?- ?901 ?- ?\\d{7} ?- ?\\d{1}|56[1-9] ?- ?([0-8]\\d{2}|9[1-9]\\d{1}|90[02-9]) ?- ?\\d{7} ?- ?\\d{1}|560 ?- ?([0-8]\\d{2}|9[1-9]\\d{1}|90[02-9]) ?- ?\\d{8}|\\d{3} ?- ?(1[056]|[2379]0|4[689]|5[3-68]) ? - ?\\d{9}|\\d{3} ?- ?\\d{3} ?- ?\\d{6}|\\d{3} ?- ?\\d{2} ?- ?\\d{5}|\\d{3} ?- ?\\d{2} ?- ?\\d{6} ?- ?\\d{3}|\\d{3} ?- ?\\d{2} ?- ?\\d{6}|\\d{3} ?- ?\\d{8}|\\d{8} ?- ?\\d{2} ?- ?\\d{3}|\\d{8} ?- ?\\d{2} ?- ?\\d{4}|\\d{8} ?- ?\\d{2}|\\d{1} ?- ?\\d{5} ?- ?\\d{8}|\\d{3} ?- ?\\d{3} ?- ?\\d{3} ?- ?\\d{2}|\\d{3} ?- ?\\d{3} ?- ?\\d{3}|\\d{8} ?- ?\\d{4}|\\d{7} ?- ?\\d{1} ?- ?([156]1|2[1-36]|30)|\\d{7} ?- ?\\d{1}|\\d{4} ?- ?\\d{4} ?- ?\\d{2}|\\d{9} ?- ?\\d{2}|\\d{3} ?- ?\\d{6} ?- ?\\d{2}|\\d{4} ?- ?\\d{4} ?- ?\\d{4} ?- ?\\d{1}| \\d{3} ?- ?\\d{6})(?!\\d+)', 'Y')
;
INSERT INTO public.regular_expression_mng(privacy_seq, privacy_type, privacy_desc, regular_expression, use_flag)
  VALUES(2, '2', '운전면허번호', '(?<!\\\\d)\\\\d{2} ?- ?\\\\d{6} ?- ?\\\\d{2}(?!\\\\d+)', 'Y')
;
INSERT INTO public.regular_expression_mng(privacy_seq, privacy_type, privacy_desc, regular_expression, use_flag)
  VALUES(1, '1', '주민등록번호', '(?<!\\d|\\.)\\d[\\s ]*\\d[\\s ]*(0[\\s ]*[1-9]|1[\\s ]*[0-2])[\\s ]*(0[\\s ]*[1-9]|[12][\\s ]*[0-9]|3[\\s ]*[0-1])((\\s)|(\\-)|(\\+)|(~)|( )|(<br[ /]*>))*[1234][\\s ]*(\\d{1}[\\s ]*){6}(?!\\d)', 'Y')
;

-- system_master --

INSERT INTO public.system_master(system_seq, system_name, system_type, description)
  VALUES('01', 'PSM', 'PSM', 'PSM')
;
INSERT INTO public.system_master(system_seq, system_name, system_type, description)
  VALUES('03', '시스템2', 'BIZ_LOG', '고객사 시스템2')
;

-- t_encryptkey --

INSERT INTO public.t_encryptkey(key)
  VALUES('easycerti1!')
;


-- 암호화 모듈 적용

create extension pgcrypto;
