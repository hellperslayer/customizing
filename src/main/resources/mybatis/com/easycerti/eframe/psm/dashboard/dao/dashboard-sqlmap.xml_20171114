<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper
	PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
	"http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="com.easycerti.eframe.psm.dashboard.dao.DashboardDao">

	<!-- 
		대시보드 1 - 최근 전체 로그 건수 > 전체 수집 로그 건수, 개인정보 처리 로그 건수
	 -->
	<select id="findDashboardList_chart1" parameterType="dashboard" resultType="dashboard">
		SELECT 
		(
			SELECT
				COUNT(LOG_SEQ)
			FROM
				BIZ_LOG
			WHERE
				PROC_DATE = TO_CHAR(NOW(), 'YYYYMMDD')
			<if test="system_seq != null and system_seq != '' and system_seq != 'all'">
				AND	SYSTEM_SEQ = #{system_seq}
			</if>
		) AS CNT1
		, 
		(
			SELECT
				COUNT(*)
			FROM
				RULE_BIZ RB, BIZ_LOG BL
			WHERE RB.LOG_SEQ = BL.LOG_SEQ
			  AND RB.OCCR_DT = TO_CHAR(NOW(), 'YYYYMMDD')					
			<if test="system_seq != null and system_seq != '' and system_seq != 'all'">
				AND	BL.SYSTEM_SEQ = #{system_seq}
			</if>
		) AS CNT2
	</select>
	
	<!-- 
		대시보드 2 - 최근 수집 로그 TOP10
	 -->
	<select id="findDashboardList_chart2" parameterType="dashboard" resultType="allLogInq">
		SELECT
			BL.PROC_DATE || BL.PROC_TIME AS PROC_DATE
			,SYM.SYSTEM_NAME AS SYSTEM_NAME
			,BL.EMP_USER_ID
			,BL.EMP_USER_NAME
			,BL.DEPT_NAME
			,BL.USER_IP
		FROM
			BIZ_LOG BL, SYSTEM_MASTER SYM
		WHERE BL.SYSTEM_SEQ = SYM.SYSTEM_SEQ
		  AND BL.EMP_USER_NAME != ''
		  AND BL.PROC_DATE = #{proc_date}			
		<if test="system_seq != null and system_seq != '' and system_seq != 'all'">
			AND
				BL.SYSTEM_SEQ = #{system_seq}
		</if>
		ORDER BY BL.PROC_TIME DESC
	</select>
	
		<!-- 
		대시보드 2 - 최근 수집 로그 TOP10
	 -->
	<select id="findDashboardList_recent" parameterType="dashboard" resultType="allLogInq">
		<!-- SELECT PROC_DATE || PROC_TIME AS PROC_DATE ,SYSTEM_NAME, EMP_USER_ID ,EMP_USER_NAME, DEPT_NAME, USER_IP

		FROM
			(SELECT *
			 FROM BIZ_LOG BL, SYSTEM_MASTER SYM 
			 WHERE BL.PROC_DATE = to_char(now(),'YYYYMMDD') 
			 AND BL.SYSTEM_SEQ = SYM.SYSTEM_SEQ
			 <if test="system_seq != null and system_seq != '' and system_seq != 'all'">
			AND
				BL.SYSTEM_SEQ = #{system_seq}
			</if>
			AND BL.EMP_USER_NAME != ''	
			 ORDER BY BL.PROC_DATE DESC, BL.PROC_TIME DESC LIMIT 10) a
			 
			 ORDER BY PROC_DATE ASC, PROC_TIME ASC -->
	<!--	
		SELECT 
			PROC_DATE, EMP_USER_ID, EMP_USER_NAME, DEPT_NAME, USER_IP, count(PROC_DATE) as CNT 
			FROM 
				(
				SELECT 0 as log_seq,
	            A.PROC_DATE, A.PROC_TIME, A.EMP_USER_ID, A.EMP_USER_NAME, A.DEPT_ID, A.DEPT_NAME as DEPT_NAME, A.USER_IP, A.USER_ID, A.SCRN_ID
	            ,SCRN_NAME, A.REQ_TYPE, A.SERVER_SEQ, A.REQ_CONTEXT, A.REQ_URL, A.REQ_END_TIME
	            ,A.RESULT_TYPE
	            FROM 
	            	BIZ_LOG A
	        	WHERE 
	        	<if test="proc_date != null and proc_date != ''">
	        		A.PROC_DATE = #{proc_date}
	        	</if>
	        	<if test="proc_date == null or proc_date == ''">
	        		A.PROC_DATE = TO_CHAR(NOW(), 'YYYYMMDD')
	        	</if>
	        	<if test="system_seq != null and system_seq != '' and system_seq != 'all'">
				AND
					A.SYSTEM_SEQ = #{system_seq}
				</if>
				AND A.EMP_USER_NAME != ''
				GROUP BY 
					PROC_DATE, PROC_TIME, EMP_USER_ID, A.EMP_USER_NAME, A.DEPT_ID, DEPT_NAME, A.USER_IP, A.USER_ID, A.SCRN_ID,SCRN_NAME, A.REQ_TYPE, A.SERVER_SEQ, A.REQ_CONTEXT, A.REQ_URL, A.REQ_END_TIME,  A.RESULT_TYPE
				)AA 
			GROUP BY PROC_DATE, EMP_USER_ID, EMP_USER_NAME,  DEPT_NAME, USER_IP
			ORDER BY CNT DESC LIMIT 10
-->
SELECT
	PROC_DATE,
	EMP_USER_ID,
	EMP_USER_NAME,
	DEPT_NAME,
	USER_IP,
	COUNT(PROC_DATE) AS CNT 
FROM
	BIZ_LOG_SUMMARY A 
WHERE
	<if test="proc_date != null and proc_date != ''">
		A.PROC_DATE = #{proc_date}
	</if>
	<if test="proc_date == null or proc_date == ''">
		A.PROC_DATE = TO_CHAR(NOW(), 'YYYYMMDD')
	</if>
	<if test="system_seq != null and system_seq != '' and system_seq != 'all'">
		AND A.SYSTEM_SEQ = #{system_seq}
	</if>
	AND A.EMP_USER_NAME != '' 

GROUP BY
	PROC_DATE,
	EMP_USER_ID,
	EMP_USER_NAME,
	DEPT_NAME,
	USER_IP 
ORDER BY
	CNT DESC LIMIT 10
	</select>
	
	<!-- 
		대시보드 3 - 시스템별 로그 수집 현황
	 -->
	<select id="findDashboardList_chart3" parameterType="dashboard" resultType="dashboard">
		SELECT
			SYM.SYSTEM_NAME AS DATA1
			,SYM.SYSTEM_SEQ AS DATA2
			,COUNT(LOG_SEQ) AS CNT1
		FROM
			BIZ_LOG_SUMMARY BL, SYSTEM_MASTER SYM
		WHERE
		<if test="search_from == null or search_from == ''">
			BL.PROC_DATE = TO_CHAR(NOW(), 'YYYYMMDD')
		AND
		</if>
		<if test="search_from != null and search_from != ''">
			BL.PROC_DATE BETWEEN replace(#{search_from}, '-' , '') AND replace(#{search_to}, '-' , '')
		AND
		</if>
		
			BL.SYSTEM_SEQ = SYM.SYSTEM_SEQ
		<if test="system_seq != null and system_seq != '' and system_seq != 'all'">
			AND
				BL.SYSTEM_SEQ = #{system_seq}
		</if>
		GROUP BY
			SYM.SYSTEM_SEQ, SYM.SYSTEM_NAME
		ORDER BY
			SYM.SYSTEM_SEQ
		LIMIT 10
	</select>
	
	<!-- 
		대시보드 4 - 개인정보 유형별 로그 수집 현황
	 -->
	<select id="findDashboardList_chart4" parameterType="dashboard" resultType="dashboard">
	
	SELECT 
	(SELECT C.CODE_NAME FROM CODE C WHERE C.GROUP_CODE_ID = 'RESULT_TYPE' AND C.CODE_ID = R.RESULT_TYPE) AS DATA1
	, COUNT(*) AS CNT1
FROM 
	BIZ_LOG_SUMMARY L, BIZ_LOG_RESULT R
WHERE 1=1
	<if test="system_seq != null and system_seq != '' and system_seq != 'all'">
	AND
				L.SYSTEM_SEQ = #{system_seq}
	</if>
	<if test="search_from == null or search_from == ''">
	AND
		L.PROC_DATE = TO_CHAR(NOW(), 'YYYYMMDD')
		R.PROC_DATE = TO_CHAR(NOW(), 'YYYYMMDD')
	</if>
	<if test="search_from != null and search_from != ''">
	AND
		L.PROC_DATE BETWEEN replace(#{search_from}, '-' , '') AND replace(#{search_to}, '-' , '')
		and R.PROC_DATE BETWEEN replace(#{search_from}, '-' , '') AND replace(#{search_to}, '-' , '')
	</if>
	AND
	L.LOG_SEQ = R.LOG_SEQ
<!-- AND  
	R.RESULT_TYPE NOT IN ('2', '3', '5', '11', '12', '13') -->
GROUP BY 
	R.RESULT_TYPE 
ORDER BY R.RESULT_TYPE::INT

	</select>
	
	<!-- 
		대시보드 5 - 추출 로그 추이 분석 현황
	 -->
	 <select id="findDashboardList_chart5" parameterType="dashboard" resultType="dashboard">
	 <!-- 튜닝쿼리 -->
	 	SELECT 
			TO_CHAR(TO_DATE(RB.OCCR_DT, 'YYYYMMDD'), 'MM-DD') AS PROC_DATE, 
			COUNT(RULE_CNT) AS CNT1
			FROM EMP_DETAIL RB
		<!-- SELECT 
			TO_CHAR(TO_DATE(RB.OCCR_DT, 'YYYYMMDD'), 'MM-DD') AS PROC_DATE, 
			COUNT(*) AS CNT1
		FROM 
			RULE_BIZ RB 
		JOIN
			BIZ_LOG BL 
		ON 
			BL.LOG_SEQ = RB.LOG_SEQ -->		
		WHERE 
			<if test="search_from == null or search_from == ''">
			RB.OCCR_DT BETWEEN TO_CHAR(CURRENT_DATE-6, 'YYYYMMDD') AND TO_CHAR(NOW(), 'YYYYMMDD')
			</if>
			<if test="search_from != null and search_from != ''">
			RB.OCCR_DT BETWEEN replace(#{search_from}, '-' , '') AND replace(#{search_to}, '-' , '')		
			</if>
			<if test="system_seq != null and system_seq != '' and system_seq != 'all'">
			AND	BL.SYSTEM_SEQ = #{system_seq}
			</if>
		GROUP BY 
			OCCR_DT
		ORDER BY 
			OCCR_DT
	 
	 <!-- 원래쿼리 -->
	 <!-- 
	 	SELECT
	 		SUBSTR(OCCR_DT, 5, 2) || '-' || SUBSTR(OCCR_DT, 7, 2) AS PROC_DATE
	 		,COUNT(BL.LOG_SEQ) CNT1
	 	FROM
	 		RULE_BIZ RB, BIZ_LOG BL
	 	WHERE
	 		RB.OCCR_DT BETWEEN TO_CHAR(CURRENT_DATE-6, 'YYYYMMDD') AND TO_CHAR(NOW(), 'YYYYMMDD')
	 	AND
	 		BL.PROC_DATE BETWEEN TO_CHAR(CURRENT_DATE-6, 'YYYYMMDD') AND TO_CHAR(NOW(), 'YYYYMMDD')
	 	AND
	 		RB.LOG_SEQ = BL.LOG_SEQ
	 	<if test="system_seq != null and system_seq != '' and system_seq != 'all'">
			AND
				BL.SYSTEM_SEQ = #{system_seq}
		</if>
		GROUP BY 
			SUBSTR(OCCR_DT, 5, 2) || '-' || SUBSTR(OCCR_DT, 7, 2)
		ORDER BY 
			SUBSTR(OCCR_DT, 5, 2) || '-' || SUBSTR(OCCR_DT, 7, 2) -->
	 </select>
	
	<!-- 
		대시보드 6 - 추출조건별 추출로그 현황
	 -->
	 <select id="findDashboardList_chart6" parameterType="dashboard" resultType="dashboard">
	 	<!-- SELECT
	 		ED.RULE_CD AS DATA1
	 		,RT.RULE_NM AS DATA2
	 		,COUNT(RB.LOG_SEQ) CNT1
	 	FROM
	 		EMP_DETAIL ED, RULETBL RT, RULE_BIZ RB, BIZ_LOG BL
	 	WHERE ED.EMP_DETAIL_SEQ = RB.EMP_DETAIL_SEQ	 	
	 	    AND ED.RULE_CD = RT.RULE_SEQ
	 	    AND RB.LOG_SEQ = BL.LOG_SEQ
			AND RB.OCCR_DT = TO_CHAR(NOW(), 'YYYYMMDD')
	 	<if test="system_seq != null and system_seq != '' and system_seq != 'all'">
			AND
				BL.SYSTEM_SEQ = #{system_seq}
		</if>
		GROUP BY 
			ED.RULE_CD, RT.RULE_NM
		ORDER BY
			COUNT(RB.LOG_SEQ) DESC -->
			
		<!-- 시나리오 테이블 추가로 쿼리 변경 -->
		select 
		   sr.scen_name AS DATA1, 
		   count(rb.emp_detail_seq) AS nPrivCount
		from
		    emp_detail ed, rule_biz rb, ruletbl rt, scenario sr, biz_log_summary bl
		where 
		    ed.emp_detail_seq = rb.emp_detail_seq
		    and ed.rule_cd = rt.rule_seq
		    and rt.scen_seq = sr.scen_seq
		    AND bl.proc_date = replace(#{search_to}, '-' , '')
		    and bl.log_seq=rb.log_seq
		    <if test="system_seq != null and system_seq != '' and system_seq != 'all'">
			AND
				bl.SYSTEM_SEQ = #{system_seq}
			</if>
		group by sr.scen_name
		order by nPrivCount desc
	 </select>
	
	<!-- 
		대시보드 7 - 부서별 로그 수집 현황 (순위 TOP 9)
	 -->
	 <select id="findDashboardList_chart7" parameterType="dashboard" resultType="dashboard">
		WITH A AS (   
			SELECT
				BL.DEPT_NAME
				,BL.DEPT_ID
				,COUNT(BL.LOG_SEQ) AS CNT
			FROM
				BIZ_LOG_SUMMARY BL
			WHERE
			BL.EMP_USER_NAME != ''	
			AND BL.DEPT_NAME != ''
			<if test="search_from == null or search_from == ''">
			AND BL.PROC_DATE = TO_CHAR(NOW(), 'YYYYMMDD')
			
			</if>
			<if test="search_from != null and search_from != ''">
			AND BL.PROC_DATE BETWEEN replace(#{search_from}, '-' , '') AND replace(#{search_to}, '-' , '')	
			</if>
			<if test="system_seq != null and system_seq != '' and system_seq != 'all'">
			AND	BL.SYSTEM_SEQ = #{system_seq}
			</if>
			GROUP BY
				BL.DEPT_NAME, BL.DEPT_ID
		),
		B AS (
			SELECT
				BL.DEPT_ID
				,COUNT(BIZ_LOG_RESULT_SEQ) AS CNT
			FROM
				BIZ_LOG_RESULT BLR, BIZ_LOG_SUMMARY BL
			WHERE
			BL.EMP_USER_NAME != ''
			AND BL.DEPT_NAME != ''
			<if test="search_from == null or search_from == ''">
			AND BL.PROC_DATE = TO_CHAR(NOW(), 'YYYYMMDD')
			AND BLR.PROC_DATE = TO_CHAR(NOW(), 'YYYYMMDD')
			</if>
			<if test="search_from != null and search_from != ''">
			AND BL.PROC_DATE BETWEEN replace(#{search_from}, '-' , '') AND replace(#{search_to}, '-' , '')
			AND BLR.PROC_DATE BETWEEN replace(#{search_from}, '-' , '') AND replace(#{search_to}, '-' , '')
			</if>
			
			AND	BLR.LOG_SEQ = BL.LOG_SEQ
			<if test="system_seq != null and system_seq != '' and system_seq != 'all'">
				AND
					BL.SYSTEM_SEQ = #{system_seq}
			</if>
			GROUP BY
				BL.DEPT_NAME, BL.DEPT_ID
		)
		
		SELECT
			A.DEPT_NAME AS DATA1
			,A.DEPT_ID AS DATA2
			,A.CNT AS CNT1
		    ,B.CNT AS CNT2
		FROM
			A, B
		WHERE
			A.DEPT_ID = B.DEPT_ID
		ORDER BY
			B.CNT DESC
	 </select>
	 
	<!-- 
		대시보드 8 - 수집 서버 현황
	 -->
	 <select id="findDashboardList_chart8" parameterType="dashboard" resultType="dashboard">
	 	SELECT
	 		CPU_USAGE_PERC AS CNT1
	 		,MEM_USAGE_PERC AS CNT2
	 		,FILESYSTEM_USAGE_PERC AS CNT3
	 		,MEM_TOTAL AS DATA1 
	 		,FILESYSTEM_TOTAL AS DATA2
	 	FROM
	 		SYSTEM_INFO
		ORDER BY
			UPDATE_TIME DESC
		LIMIT 1
	 </select>

	<!-- 
		대시보드 - 비정상행위 실시간 현황
	 -->
	 <select id="findDashboardList_chart_emp" parameterType="dashboard" resultType="ExtrtCondbyInq">
	 	SELECT
	 		<!-- COUNT(EMP_DETAIL_SEQ) AS CNT -->
	 		EMP_DETAIL_SEQ, RULE_CD, OCCR_DT, EMP_USER_ID, EMP_USER_NAME, DEPT_NAME, 
            (SELECT SCEN_NAME FROM SCENARIO WHERE SCEN_SEQ = B.SCEN_SEQ) AS SCEN_NAME
	 	FROM
	 		EMP_DETAIL A, RULETBL B
	 	WHERE 
	 		A.RULE_CD = B.RULE_SEQ 
	 		AND OCCR_DT = #{proc_date}
	 		AND IS_CHECK = 'N'
	 	ORDER BY EMP_DETAIL_SEQ DESC LIMIT 1
	 </select>
	
	<select id="findDashboardList" resultType="dashboard">
	 	SELECT COORD_ID,COORD_NAME,COORD_TYPE,COORD_DATE,COORDINATE,COORD_MEMO 
		FROM 
			GOOGLE_INFO
	 </select>
	 
	 <select id = "extract_chart" parameterType="dashboard" resultType="dashboard">
	 	select 
  			 a.occr_dt
  			 , a.rule_cd
 			 , a.rule_nm
 			 , sum(cnt) as cnt
		from
   			(select * , 1 as cnt from emp_detail where occr_dt between #{search_from} and #{search_to}) a
   		where a.emp_user_name is not null and a.emp_user_name != ''
		group by a.occr_dt, a.rule_cd, a.rule_nm
		order by occr_dt asc;
	 </select>
	 
	 <select id = "extract_name" parameterType="dashboard" resultType="dashboard">
	 	select 
  			 a.rule_cd, a.rule_nm
	 	from
   			(select * , 1 as cnt from emp_detail where occr_dt between #{search_from} and #{search_to}) a
		group by a.rule_cd, a.rule_nm
		order by a.rule_cd asc;
	 </select>
	 
	 <select id = "getNewDashboardChart1" resultType="dashboard">
	 	select a.proc_date, a.system_seq,b.system_name, count(*) as nPrivCount  from biz_log a
		left join system_master b on a.system_seq= b.system_seq
		where a.proc_date BETWEEN TO_CHAR(current_date-7, 'YYYYMMDD') AND TO_CHAR(NOW(), 'YYYYMMDD')
		group by a.proc_date, a.system_seq, b.system_name
		order by a.proc_date asc , a.system_seq asc
	 </select>
	 
	 <select id = "getNewDashboardChart2" resultType="dashboard">
	 	select a.emp_user_id, a.emp_user_name, count(*) as nPrivCount from biz_log a
		left join biz_log_result b on a.log_seq=b.log_seq
		where a.proc_date BETWEEN TO_CHAR(NOW(), 'YYYYMMDD') AND TO_CHAR(NOW(), 'YYYYMMDD')
		and a.emp_user_id is not null  and a.emp_user_name is not null  and a.emp_user_name !='' 
		group by a.emp_user_id, a.emp_user_name
		order by nPrivCount desc 
		limit 10
	 </select>
	 
	 <select id = "getNewDashboardChart3" resultType="dashboard">
	 	select a.system_seq, c.system_name, b.result_type, d.privacy_desc, count(*) as nPrivCount from biz_log a
		left join biz_log_result b on a.log_seq=b.log_seq
		left join system_master c on a.system_seq=c.system_seq
		left join regular_expression_mng d on b.result_type = d.privacy_type
		where a.proc_date BETWEEN TO_CHAR(NOW(), 'YYYYMMDD') AND TO_CHAR(NOW(), 'YYYYMMDD')
		and c.system_name is not null
		group by a.system_seq, c.system_name, b.result_type , d.privacy_desc 
		order by a.system_seq  asc, b.result_type asc 
	 </select>
	 
	 <select id = "getNewDashboardChart4" resultType="dashboard">
	 	select c.privacy_seq, c.privacy_desc, count(*) as nPrivCount from biz_log a 
		left join biz_log_result b on a.log_seq=b.log_seq
		left join regular_expression_mng c on b.result_type=c.privacy_type
		where a.proc_date BETWEEN TO_CHAR(NOW(), 'YYYYMMDD') AND TO_CHAR(NOW(), 'YYYYMMDD')
		and privacy_desc is not null
		group by c.privacy_seq, c.privacy_desc
		order by c.privacy_seq asc 
	 </select>
	 
	 <select id = "getNewDashboardChart5" resultType="dashboard">
	 	select a.proc_date, c.privacy_seq, c.privacy_desc, count(*) as nPrivCount from biz_log a 
		left join biz_log_result b on a.log_seq=b.log_seq
		left join regular_expression_mng c on b.result_type=c.privacy_type
		where 
		a.proc_date BETWEEN TO_CHAR(current_date-7, 'YYYYMMDD') AND TO_CHAR(NOW(), 'YYYYMMDD')
		and privacy_desc is not null
		group by a.proc_date, c.privacy_seq, c.privacy_desc
		order by a.proc_date asc,  c.privacy_seq asc
	 </select>
	 
	  <select id = "getNewDashboardChart6" resultType="dashboard">
	 	select c.dept_name, count(*) as nPrivCount from biz_log a
		left join biz_log_result b on a.log_seq=b.log_seq
		left join department c on a.dept_id=c.dept_id
		where 
		<!-- a.proc_date BETWEEN TO_CHAR(NOW(), 'YYYYMMDD') AND TO_CHAR(NOW(), 'YYYYMMDD') -->
		a.proc_date = TO_CHAR(NOW(), 'YYYYMMDD')
		and c.dept_name is not null
		group by a.dept_id, c.dept_name 
		order by nPrivCount desc limit 5
	 </select>
	 
	 <select id = "getNewDashboardChart7" resultType="dashboard">
	 	select substring (a.proc_time, 0,3 ) as proc_date ,a.system_seq, b.system_name, count(*) as cnt1 from biz_log a
		left join system_master b on a.system_seq=b.system_seq
		where a.proc_date BETWEEN TO_CHAR(current_date-7, 'YYYYMMDD') AND TO_CHAR(NOW(), 'YYYYMMDD')
		and emp_user_name != '' 
		group by substring (a.proc_time, 0,3 ), b.system_name,a.system_seq
		order by substring (a.proc_time, 0,3 ) asc, a.system_seq asc
	 </select>
	 
	  <select id = "getNewDashboardChart8" resultType="dashboard">
	 	select a.proc_date, a.system_seq,b.system_name, count(*) as nPrivCount  from biz_log a
        left join biz_log_result c on a.log_seq=c.log_seq
		left join system_master b on a.system_seq= b.system_seq
		where a.proc_date BETWEEN TO_CHAR(current_date-7, 'YYYYMMDD') AND TO_CHAR(NOW(), 'YYYYMMDD')
		group by a.proc_date, a.system_seq, b.system_name
		order by a.proc_date asc , a.system_seq asc

	 </select>
	 
	 <select id = "getNewDashboardChart9" resultType="dashboard">
	 	select to_char(update_time, 'YYYYMMDD') as proc_date,  
        to_char(update_time, 'HH24') as proc_time,  
        sum(cpu_usage_perc)/count(*) as cnt1,
        sum(mem_usage_perc)/count(*) as cnt2,
        sum(filesystem_usage_perc)/count(*) as cnt3
		from system_info
		where to_char(update_time, 'YYYYMMDD') BETWEEN TO_CHAR(NOW(), 'YYYYMMDD') AND TO_CHAR(NOW(), 'YYYYMMDD')
		group by to_char(update_time, 'YYYYMMDD'), to_char(update_time, 'HH24')
		order by to_char(update_time, 'HH24') asc
	 </select>
	 
	 <select id = "getNewDashboardChart10" resultType="dashboard">
	 	SELECT substring (BL.proc_time, 0,3 ) as proc_time, ED.emp_user_name,count(*) as nPrivCount
	 	FROM
	 		EMP_DETAIL ED, RULETBL RT, RULE_BIZ RB, BIZ_LOG BL
	 	WHERE ED.EMP_DETAIL_SEQ = RB.EMP_DETAIL_SEQ	 	
	 	    AND ED.RULE_CD = RT.RULE_SEQ
	 	    AND RB.LOG_SEQ = BL.LOG_SEQ
			AND ED.OCCR_DT = TO_CHAR(NOW(), 'YYYYMMDD')
         group by substring (BL.proc_time, 0,3 ), ED.emp_user_name 
	 </select>
	 
	  <select id = "getNewDashboardChart11" resultType="dashboard">
	 	SELECT
	 		ED.RULE_CD AS DATA1
	 		,RT.RULE_NM AS DATA2
            ,dt.DEPT_NAME AS DATA3
	 		,SUM(ED.RULE_CNT) nPrivCount
	 	FROM
	 		EMP_DETAIL ED, RULETBL RT, BIZ_LOG_SUMMARY BL, department dt
	 	WHERE ED.RULE_CD = RT.RULE_SEQ
	 	    AND RB.LOG_SEQ = BL.LOG_SEQ
            AND ED.DEPT_ID = dt.DEPT_ID
			AND ED.OCCR_DT = TO_CHAR(NOW(), 'YYYYMMDD')
            and ( ED.DEPT_ID is not null or ED.DEPT_ID  != '' )
		GROUP BY 
					dt.DEPT_NAME,ED.RULE_CD, RT.RULE_NM
				ORDER BY
					ED.RULE_CD asc, SUM(ED.RULE_CNT) DESC
	 </select>
	 
	  <select id = "getNewDashboardChart12" resultType="dashboard">
	 	SELECT
	 		ED.RULE_CD AS DATA1
	 		,RT.RULE_NM AS DATA2
	 		,SUM(ED.RULE_CNT) nPrivCount
	 	FROM
	 		EMP_DETAIL ED, RULETBL RT, BIZ_LOG_SUMMARY BL
	 	WHERE ED.RULE_CD = RT.RULE_SEQ
	 	    AND RB.LOG_SEQ = BL.LOG_SEQ
			AND ED.OCCR_DT = TO_CHAR(NOW(), 'YYYYMMDD')
		GROUP BY 
					ED.RULE_CD, RT.RULE_NM
				ORDER BY
					SUM(ED.RULE_CNT) DESC
	 </select>
	 
	 <select id = "getNewDashboardChart13" resultType="dashboard">
	 	select substring (a.proc_time, 0,3 ) as proc_time, count(*) as nPrivCount from biz_log a 
		left join biz_log_result b on a.log_seq=b.log_seq
		where 
		a.proc_date BETWEEN TO_CHAR(NOW(), 'YYYYMMDD') AND TO_CHAR(NOW(), 'YYYYMMDD')
		group by substring (a.proc_time, 0,3 )
		order by substring (a.proc_time, 0,3 ) asc
	 </select>
</mapper>

